/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgrhguiajax.server.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail;
import org.cocktail.fwkcktlgrh.common.metier.EOPoste;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

/**
 * 
 * Composant de gestion des détails d'une affectation.
 * Il s'agit d'objets de type {@link EOAffectationDetail}.
 * 
 * @binding affectation un objet {@link EOAffectation} dont on veut éditer les détails
 * 
 * @see ACktlAjaxGRHComponent
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class DetailsAffectationUI extends ACktlAjaxGRHComponent {

    private static final long serialVersionUID = 1827174687432383985L;
    private static final String BINDING_AFFECTATION = "affectation";

    private static final String POSTE_KEY = ERXQ.keyPath(EOAffectationDetail.TO_POSTE_KEY, EOPoste.POS_LIBELLE_KEY);
    private static final String CODE_POSTE_KEY = ERXQ.keyPath(EOAffectationDetail.TO_POSTE_KEY, EOPoste.POS_CODE_KEY);
    private static final String D_DEBUT_KEY = EOAffectationDetail.ADE_D_DEBUT_KEY;
    private static final String D_FIN_KEY = EOAffectationDetail.ADE_D_FIN_KEY;
    private static final String DETAIL = "currentDetail";
    
    private EOAffectation affectation;
    private String detailContainerId;
    private String dialogId;
    private String dialogContainerId;
    private ERXDisplayGroup displayGroup;
    private EOAffectationDetail currentDetail;
    private NSArray<CktlAjaxTableViewColumn> colonnes;
    private NSArray<EOPoste> postes;
    private EOPoste currentPoste;
    
    public DetailsAffectationUI(WOContext context) {
        super(context);
    }
    
    public WOActionResults ajouter() {
        EOAffectationDetail detailAffectation = EOAffectationDetail.create(edc(), 0, null, null, new NSTimestamp(), new NSTimestamp(), getAffectation(), null);
        addObjectToDg(detailAffectation);
        getDisplayGroup().selectObject(detailAffectation);
//        setFormEditing(true);
        return null;
    }

    public void addObjectToDg(Object obj) {
        getDisplayGroup().insertObjectAtIndex(obj, 0);
    }
    
    public void deleteObjectFromDg(Object obj) {
        int index = getDisplayGroup().allObjects().indexOf(obj);
        getDisplayGroup().deleteObjectAtIndex(index);
    }
    
    public WOActionResults enregistrer() {
        boolean success = saveChanges();
        if (success) CktlAjaxWindow.close(context());
        return null;
    }
    
    public WOActionResults annuler() {
        edc().revert();
        CktlAjaxWindow.close(context());
        return null;
    }
    
    public WOActionResults supprimer() {
        EOEnterpriseObject selected = (EOEnterpriseObject)getDisplayGroup().selectedObject();
        edc().deleteObject(selected);
        saveChanges();
//        if (success) deleteObjectFromDg(selected);
        return null;
    }
    
    public EOAffectation getAffectation() {
        EOAffectation affectationParent = (EOAffectation)valueForBinding(BINDING_AFFECTATION);
        if (affectation == null || eoHasChanged(affectation, affectationParent)) {
            affectation = affectationParent.localInstanceIn(edc());
        }
        return affectation;
    }
    
    public String getDetailContainerId() {
        if (detailContainerId == null)
            detailContainerId = ERXWOContext.safeIdentifierName(context(), true);
        return detailContainerId;
    }
    
    public String getDialogId() {
        if (dialogId == null)
            dialogId = ERXWOContext.safeIdentifierName(context(), true);
        return dialogId;
    }
    
    public String getDialogContainerId() {
        if (dialogContainerId == null)
            dialogContainerId = ERXWOContext.safeIdentifierName(context(), true);
        return dialogContainerId;
    }
    
    public ERXDisplayGroup getDisplayGroup() {
        if (displayGroup == null) {
            displayGroup = new ERXDisplayGroup();
            displayGroup.setObjectArray(getAffectation().toDetails());
        }
        return displayGroup;
    }
    
    public NSArray<CktlAjaxTableViewColumn> getColonnes() {
        if (colonnes == null) {
            NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();
            // Colonne Poste
            CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
            col.setLibelle("Poste");
            col.setOrderKeyPath(POSTE_KEY);
            String keyPath = ERXQ.keyPath(DETAIL, POSTE_KEY);
            CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Poste
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Poste");
            col.setOrderKeyPath(POSTE_KEY);
            keyPath = ERXQ.keyPath(DETAIL, CODE_POSTE_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Debut
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Date début");
            col.setOrderKeyPath(D_DEBUT_KEY);
            keyPath = ERXQ.keyPath(DETAIL, D_DEBUT_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            ass.setDateformat("%d/%m/%Y");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Fin
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Date fin");
            col.setOrderKeyPath(D_FIN_KEY);
            keyPath = ERXQ.keyPath(DETAIL, D_FIN_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            ass.setDateformat("%d/%m/%Y");
            col.setAssociations(ass);
            colTmp.add(col);
            colonnes = colTmp.immutableClone();
        }
        return colonnes;
    }
    
    public boolean noDetailSelected() {
        return getSelectedDetail() == null;
    }
    
    public EOAffectationDetail getSelectedDetail() {
        return (EOAffectationDetail)getDisplayGroup().selectedObject();
    }
    
    public EOAffectationDetail getCurrentDetail() {
        return currentDetail;
    }
    
    public void setCurrentDetail(EOAffectationDetail currentDetail) {
        this.currentDetail = currentDetail;
    }
    
    public NSArray<EOPoste> getPostes() {
        if (postes == null)
            postes = EOPoste.fetchAll(edc(), ERXS.descInsensitive(EOPoste.POS_CODE_KEY).array());
        return postes;
    }
    
    public EOPoste getCurrentPoste() {
        return currentPoste;
    }
    
    public void setCurrentPoste(EOPoste currentPoste) {
        this.currentPoste = currentPoste;
    }
    
}