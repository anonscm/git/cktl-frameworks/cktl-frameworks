/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrhguiajax.server.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.extensions.appserver.ERXWOContext;

/**
 * 
 * Composant de sélection d'une structure de rattachement pour un individu hébergé.
 * 
 * @binding selection "get et set" d'une EOStructure de rattachement pour l'hébergé
 * @binding didSelectStructure action callback appelée lorsqu'une structure a été sélectionnée
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class CktlAjaxStructureHebergeSelect extends AComponent {

    private static final long serialVersionUID = 9198817243785013235L;
    private static final String BINDING_SELECTION = "selection";
    private static final String BINDING_DID_SELECT_STRUCTURE = "didSelectStructure";
    
    private String containerId;
    private String modalWindowId;

    public CktlAjaxStructureHebergeSelect(WOContext context) {
        super(context);
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    
    public WOActionResults valider() {
        CktlAjaxWindow.close(context(), getModalWindowId());
        return (WOActionResults)valueForBinding(BINDING_DID_SELECT_STRUCTURE);
    }
    
    public EOStructure getSelection() {
        return (EOStructure)valueForBinding(BINDING_SELECTION);
    }
    
    public void setSelection(EOStructure value) {
        setValueForBinding(value, BINDING_SELECTION);
    }
    
    public String getContainerId() {
        if (containerId == null)
            containerId = ERXWOContext.safeIdentifierName(context(), true);
        return containerId;
    }
    
    public String getModalWindowId() {
        if (modalWindowId == null)
            modalWindowId = ERXWOContext.safeIdentifierName(context(), true);
        return modalWindowId;
    }
    
    public String getFormId() {
		return getComponentId()+"_StructHebergerSelectForm";
	}
    
}