/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrhguiajax.server.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlgrh.common.metier.EOIndividuDiplome;
import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlpersonne.common.metier.EODiplome;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOTitulaireDiplome;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXQ;

/**
 * 
 * Composant d'affichage et d'edition des diplômes obtenus par un individu.
 * 
 * 
 * @binding individu le {@link EOIndividu} pour lequel on veut gérer les diplômes.
 * @binding editingContext un {@link EOEditingContext} sur lequel un nested sera crée.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 * 
 */
public class IndividuDiplomesUI extends ACktlAjaxGRHComponent {

    private static final long serialVersionUID = 8481450642177694121L;
    private static final String INDIV_DIP = "currentIndividuDiplome";
    private static final String LIBELLE_KEY = ERXQ.keyPath(
            EOIndividuDiplome.TO_DIPLOME_KEY, EODiplome.LL_DIPLOME_KEY);
    private static final String DATE_KEY = EOIndividuDiplome.D_DIPLOME_KEY;
    private static final String LIEU_KEY = EOIndividuDiplome.LIEU_DIPLOME_KEY;
    private static final String SPEC_KEY = EOIndividuDiplome.SPECIALITE_KEY;

    private static final String BINDING_INDIVIDU = "individu";
    private String diplomeDialogId;
    private String rechercheDiplomeDialogId;
    private String diplomeModifDialogId;
    private String containerId;
    private String detailContainerId;
    private ERXDisplayGroup displayGroup;
    private EOIndividuDiplome currentIndividuDiplome;
    private NSArray<CktlAjaxTableViewColumn> colonnes;
    private EOIndividu individu;

    private NSArray<EOTitulaireDiplome> titulaires;
    private EOTitulaireDiplome currentTitulaire;
    private EODiplome selectedDiplome;
    private String specialite;
    private NSTimestamp dateObtention;
    private EOCommune lieuObtention;
    private String lieuObtentionFree;
    private EOTitulaireDiplome selectedTitulaire;
    private EOTitulaireDiplome defaultTitulaire;
    
    private boolean canEdit = false;
    
    public IndividuDiplomesUI(WOContext context) {
        super(context);
    }

    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }

    public WOActionResults resetDialog() {
        setSelectedDiplome(null);
        setSpecialite(null);
        setDateObtention(null);
        setLieuObtention(null);
        setSelectedTitulaire(getDefaultTitulaire());
        setLieuObtentionFree(null);
        return null;
    }
    
    public WOActionResults open() {
        resetDialog();
        return null;
    }
    
    public WOActionResults modifier() {
    	setCanEdit(true);
    	return null;
    }
    
    public WOActionResults ajouter() {
//    	getSelectedDiplome().localInstanceIn(edc());
    	EODiplome localDiplome = getSelectedDiplome().localInstanceIn(edc());
    	
  
        EOIndividuDiplome indivDip = EOIndividuDiplome.create(edc(),
                new NSTimestamp(), new NSTimestamp(), null, "O", localDiplome,
//                new NSTimestamp(), new NSTimestamp(), null, "O", getSelectedDiplome(),
                getIndividu(), getSelectedTitulaire());
        indivDip.setSpecialite(getSpecialite());
        indivDip.setDDiplome(getDateObtention());
        indivDip.setLieuDiplome(getLieuObtention() != null ? getLieuObtention().llCom() : getLieuObtentionFree());
        indivDip.setNoIndividu(getIndividu().noIndividu());
        boolean success = saveChanges();
        if (success) {
            addObjectToDg(indivDip);
            getDisplayGroup().selectObject(indivDip);
            CktlAjaxWindow.close(context(), getDiplomeDialogId());
        } else {
            edc().revert();
        }
        return null;
    }
    
    public WOActionResults annuler() {
    	edc().revert();
    	setCanEdit(false);
    	return null;
    }
    
    public WOActionResults annulerRecherche() {
    	edc().revert();
    	CktlAjaxWindow.close(context(), getRechercheDiplomeDialogId());
    	return null;
    }
    
    public WOActionResults valider() {
    	saveChanges();
    	setCanEdit(false);
    	return null;
    }
    
    public WOActionResults validerRecherche() {
    	saveChanges();
    	CktlAjaxWindow.close(context(), getRechercheDiplomeDialogId());
    	return null;
    }

    public WOActionResults supprimer() {
        EOIndividuDiplome indivDip = (EOIndividuDiplome) getDisplayGroup().selectedObject();
        edc().deleteObject(indivDip);
        // FIXME a enlever une fois que le "bug" que j'ai reporté sera corrigé : 
        // [Wonder-disc] ERXEOControlUtilities.objectsWithQualifier and in memory filtering
        indivDip.setNoIndividu(null);
        // END
        saveChanges();
        deleteObjectFromDg(indivDip);
        return null;
    }
    
    public void addObjectToDg(Object obj) {
        getDisplayGroup().insertObjectAtIndex(obj, 0);
    }
    
    public void deleteObjectFromDg(Object obj) {
        int index = getDisplayGroup().allObjects().indexOf(obj);
        getDisplayGroup().deleteObjectAtIndex(index);
    }
    
    public boolean noIndivDiplomeSelected() {
        return getDisplayGroup().selectedObject() == null;
    }
    
    public boolean isButtonDisabled() {
    	return !noIndivDiplomeSelected();
    }
    
    public EOIndividu getIndividu() {
        EOIndividu indivTmp = (EOIndividu) valueForBinding(BINDING_INDIVIDU);
        if (individu == null || eoHasChanged(individu, indivTmp)) {
			individu = indivTmp.localInstanceIn(edc());
		}
        return individu;
    }

    public String getDiplomeDialogId() {
        if (diplomeDialogId == null) {
			diplomeDialogId = "DipDiag_"
                    + ERXWOContext.safeIdentifierName(context(), true);
		}
        return diplomeDialogId;
    }
    
    public String getRechercheDiplomeDialogId() {
        if (rechercheDiplomeDialogId == null) {
			rechercheDiplomeDialogId = "SearchDipDiag_"
                    + ERXWOContext.safeIdentifierName(context(), true);
		}
        return rechercheDiplomeDialogId;
    }
    
    public String getContainerId() {
        if (containerId == null) {
			containerId = "DipCont_"
                    + ERXWOContext.safeIdentifierName(context(), true);
		}
        return containerId;
    }
    
    public String getDetailContainerId() {
        if (detailContainerId == null) {
			detailContainerId = "DipDetailCont_"
                    + ERXWOContext.safeIdentifierName(context(), true);
		}
        return detailContainerId;
    }

    public ERXDisplayGroup getDisplayGroup() {
        if (displayGroup == null) {
            displayGroup = new ERXDisplayGroup();
            displayGroup.setObjectArray(EOIndividuDiplome.fetchAllForIndividu(
                    edc(), getIndividu()));
        }
        return displayGroup;
    }

    public NSArray<CktlAjaxTableViewColumn> getColonnes() {
        if (colonnes == null) {
            NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();
            // Colonne Libelle
            CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
            col.setLibelle("Libellé");
            col.setOrderKeyPath(LIBELLE_KEY);
            String keyPath = ERXQ.keyPath(INDIV_DIP, LIBELLE_KEY);
            CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Date obtention
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Date d'obtention");
            col.setOrderKeyPath(DATE_KEY);
            keyPath = ERXQ.keyPath(INDIV_DIP, DATE_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            ass.setDateformat("%d/%m/%Y");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Lieu
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Lieu d'obtention");
            col.setOrderKeyPath(LIEU_KEY);
            keyPath = ERXQ.keyPath(INDIV_DIP, LIEU_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Specialite
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Specialité");
            col.setOrderKeyPath(SPEC_KEY);
            keyPath = ERXQ.keyPath(INDIV_DIP, SPEC_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            colonnes = colTmp.immutableClone();
        }
        return colonnes;
    }

    @SuppressWarnings("unchecked")
    public NSArray<EOTitulaireDiplome> getTitulaires() {
        if (titulaires == null) {
			titulaires = EOTitulaireDiplome.fetchAll(edc());
		}
        return titulaires;
    }
    
    public EOTitulaireDiplome getDefaultTitulaire() {
        if (defaultTitulaire == null) {
			defaultTitulaire = EOTitulaireDiplome.getTitulaire(edc());
		}
        return defaultTitulaire;
    }
    
    public EOIndividuDiplome getCurrentIndividuDiplome() {
        return currentIndividuDiplome;
    }

    public void setCurrentIndividuDiplome(
            EOIndividuDiplome currentIndividuDiplome) {
        this.currentIndividuDiplome = currentIndividuDiplome;
    }

    public EODiplome getSelectedDiplome() {
        return selectedDiplome;
    }
    
    public void setSelectedDiplome(EODiplome selectedDiplome) {
        this.selectedDiplome = selectedDiplome;
    }
    
    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public NSTimestamp getDateObtention() {
        return dateObtention;
    }

    public void setDateObtention(NSTimestamp dateObtention) {
        this.dateObtention = dateObtention;
    }

    public EOCommune getLieuObtention() {
        return lieuObtention;
    }
    
    public void setLieuObtention(EOCommune lieuObtention) {
        this.lieuObtention = lieuObtention;
    }

    public EOTitulaireDiplome getSelectedTitulaire() {
        return selectedTitulaire;
    }
    
    public void setSelectedTitulaire(EOTitulaireDiplome selectedTitulaire) {
        this.selectedTitulaire = selectedTitulaire;
    }
    
    public EOTitulaireDiplome getCurrentTitulaire() {
        return currentTitulaire;
    }
    
    public void setCurrentTitulaire(EOTitulaireDiplome currentTitulaire) {
        this.currentTitulaire = currentTitulaire;
    }
    
    public String getLieuObtentionFree() {
        return lieuObtentionFree;
    }
    
    public void setLieuObtentionFree(String lieuObtentionFree) {
        this.lieuObtentionFree = lieuObtentionFree;
    }
    
    public String getFormId() {
		return getComponentId() + "_IndividuDiplomSelectForm";
	}

	public boolean isCanEdit() {
		return canEdit;
	}

	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}
	
	public EOIndividuDiplome getSelectedIndividuDiplome() {
		return (EOIndividuDiplome) displayGroup.selectedObject();
	}
    
}