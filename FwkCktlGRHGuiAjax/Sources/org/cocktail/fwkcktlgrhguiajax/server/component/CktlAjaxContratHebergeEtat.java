package org.cocktail.fwkcktlgrhguiajax.server.component;

import org.cocktail.fwkcktlgrh.common.metier.EOContratHeberge;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

/**
 * 
 * Simple composant indiquant l'état d'un contrat d'hébergé :
 * en cours de validation, validé, annulé.
 * 
 * @binding contrat le {@link EOContratHeberge} dont on veut afficher l'état.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class CktlAjaxContratHebergeEtat extends WOComponent {

    private static final long serialVersionUID = -273804623025470107L;
    private static final String BINDING_CONTRAT = "contrat";

    public CktlAjaxContratHebergeEtat(WOContext context) {
        super(context);
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    
    public EOContratHeberge contrat() {
        return (EOContratHeberge)valueForBinding(BINDING_CONTRAT);
    }
    
}