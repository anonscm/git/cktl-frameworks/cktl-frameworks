/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrhguiajax.server.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EOContratHeberge;
import org.cocktail.fwkcktlgrh.common.metier.InfoCarriereContrat;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXQ;

/**
 * Composant d'édition des affectation d'un individu.
 * 
 * @binding editingContext un {@link EOEditingContext} surlequel un nested sera crée
 * @binding utilisateurPersId l'id de l'utilisateur effectuant l'édition
 * @binding individu {@link EOIndividu} individu pourlequel on veut éditer les affectations
 * 
 * @see ACktlAjaxGRHComponent
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class IndividuAffectationsUI extends ACktlAjaxGRHComponent {

    private static final long serialVersionUID = -7415495415748073985L;
    private static final String AFFECTATION = "currentAffectation";
    private static final String STRUCTURE_KEY = ERXQ.keyPath(EOAffectation.TO_STRUCTURE_KEY, EOStructure.LL_STRUCTURE_KEY);
    private static final String D_DEBUT_KEY = EOAffectation.D_DEB_AFFECTATION_KEY;
    private static final String D_FIN_KEY = EOAffectation.D_FIN_AFFECTATION_KEY;
    private static final String QUOTITE_KEY = EOAffectation.NUM_QUOT_AFFECTATION_KEY;
    private static final String ETAT_AFFECTATION = EOAffectation.ETAT_AFFECTATION_KEY;
    
    private static final String BINDING_UTILISATEUR_PERSID = "utilisateurPersId"; 
    private static final String BINDING_INDIVIDU = "individu";

    private String containerId;
    private String detailContainerId;
    private EOAffectation currentAffectation;
    private ERXDisplayGroup displayGroup;
    private EOIndividu individu;
    private NSArray<CktlAjaxTableViewColumn> colonnes;
    private boolean isFormEditing;
    private boolean isAjoutMode;
    private boolean resetForm;
    public NSTimestamp dateDebutContrat = null;
    
    public IndividuAffectationsUI(WOContext context) {
        super(context);
    }
    
    public WOActionResults ajouter() {
    	NSArray<InfoCarriereContrat> infos;
    	// Mise en donnée général pour être utilisée comme contrôle dans les affectations
//    	NSTimestamp dateDebutContrat = null;
        NSTimestamp dateFinContrat = null;
    	if ( getContratHebergeEnCours().count() == 0){
    		// On récupère le contrat en cours le plus récent
//    		NSArray<InfoCarriereContrat> infos = 
    		infos =	InfoCarriereContrat.carrieresContratsPourPeriode(edc(), getIndividu(), null,null, true);
//    		infos = ERXS.sorted(infos, ERXS.desc(InfoCarriereContrat.DATE_DEBUT_KEY));
    		dateDebutContrat = null;
            dateFinContrat = null;
    	} else {
    		
    		NSMutableArray<InfoCarriereContrat> carrieresContrats = new NSMutableArray<InfoCarriereContrat>();
    		NSArray<EOContratHeberge> contratsHeberges = getContratHebergeEnCours();
            for (EOContratHeberge contratHeberge : contratsHeberges) {
                InfoCarriereContrat cc = new InfoCarriereContrat(contratHeberge);
                carrieresContrats.addObject(cc);
            }
            infos = carrieresContrats.immutableClone();
            // On revérifie qu'il y a bien au moins un contrat hébergé
            // et on sette les dates de débuts et de fin avec les dates du contrat.
            if (!infos.isEmpty()) {
            	dateDebutContrat = infos.objectAtIndex(0).dateDebut();
            	dateFinContrat = infos.objectAtIndex(0).dateFin();
            }
    	}
        

        EOAffectation newAffectation = EOAffectation.create(edc(), null, new NSTimestamp(), null, new NSTimestamp(), null, 100, null, "O", getIndividu(), null);
        newAffectation.setPersIdCreation(getUtilisateurPersId());
        newAffectation.setDDebAffectation(dateDebutContrat);
        newAffectation.setDFinAffectation(dateFinContrat);
        addObjectToDg(newAffectation);
        getDisplayGroup().selectObject(newAffectation);
        setFormEditing(true);
        isAjoutMode = true;
        // On veut repartir avec un formulaire clean (nouvel ec...)
        setResetForm(true);
        return null;
    }

    public WOActionResults enregistrer() {
        getSelectedAffectation().setPersIdModification(getUtilisateurPersId());
        getSelectedAffectation().setDModification(new NSTimestamp());
        saveChanges();
        isAjoutMode = false;
        return null;
    }
    
    public WOActionResults supprimer() {
        getSelectedAffectation().annuler();
        boolean success = saveChanges();
        if (success) deleteObjectFromDg(getSelectedAffectation());
        return null;
    }
    
    public WOActionResults annulerNouveau() {
        EOAffectation affectation = (EOAffectation)getDisplayGroup().selectedObject();
        if (isAjoutMode) {
            deleteObjectFromDg(affectation);
            getDisplayGroup().setSelectedObject(null);
        }
        isAjoutMode = false;
        return null;
    }
    
    public boolean noContratSelected() {
//    	if (getDisplayGroup().selectedObject() != null && getContratHebergeEnCours().count() > 0){
//    		if ( getAffectationsContratHeberge().contains((EOAffectation)getDisplayGroup().selectedObject()) ){
//    			return false;
//    		} else {
//    			return true;
//    		}
//    	} else{
//    		return false;
//    	}

    	return getDisplayGroup().selectedObject() == null;
    }
    
    public String getContainerId() {
        if (containerId == null)
            containerId = "Ctr_"
                    + ERXWOContext.safeIdentifierName(context(), true);
        return containerId;
    }
    
    public String getDetailContainerId() {
        if (detailContainerId == null)
            detailContainerId = "Ctr_"
                    + ERXWOContext.safeIdentifierName(context(), true);
        return detailContainerId;
    }
    
    public ERXDisplayGroup getDisplayGroup() {
    	if (getContratHebergeEnCours().count() != 0) {
    		if (displayGroup == null) {
    			displayGroup = new ERXDisplayGroup();
    			displayGroup.setObjectArray(getAffectationsContratHeberge());
    		}
    	}
    	
        return displayGroup;
    }
    
    public void addObjectToDg(Object obj) {
        getDisplayGroup().insertObjectAtIndex(obj, 0);
    }
    
    public void deleteObjectFromDg(Object obj) {
        int index = getDisplayGroup().allObjects().indexOf(obj);
        getDisplayGroup().deleteObjectAtIndex(index);
    }
    
    public EOIndividu getIndividu() {
//    	if (hasBinding(BINDING_INDIVIDU)){
//    		 EOIndividu indivTmp = (EOIndividu) valueForBinding(BINDING_INDIVIDU);
//    	        if ((individu == null && indivTmp != null) || eoHasChanged(individu, indivTmp))
//    	            individu = indivTmp.localInstanceIn(edc());
//    	        return individu;
//    	} else {
//    		return null;
//    	}
        EOIndividu indivTmp = (EOIndividu) valueForBinding(BINDING_INDIVIDU);
        if (individu == null || eoHasChanged(individu, indivTmp))
            individu = indivTmp.localInstanceIn(edc());
        return individu;
    }
    
    public NSArray<CktlAjaxTableViewColumn> getColonnes() {
        if (colonnes == null) {
            NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();
            // Colonne Structure
            CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
            col.setLibelle("Structure");
            col.setOrderKeyPath(STRUCTURE_KEY);
            String keyPath = ERXQ.keyPath(AFFECTATION, STRUCTURE_KEY);
            CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            
            //Colonne Etat de l'affectation
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Etat");
            keyPath = ERXQ.keyPath(AFFECTATION, ETAT_AFFECTATION);
            ass = new CktlAjaxTableViewColumnAssociation(keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            
            // Colonne Debut
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Date début");
            col.setOrderKeyPath(D_DEBUT_KEY);
            keyPath = ERXQ.keyPath(AFFECTATION, D_DEBUT_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            ass.setDateformat("%d/%m/%Y");
            col.setAssociations(ass);
            colTmp.add(col);
            
            // Colonne Fin
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Date fin");
            col.setOrderKeyPath(D_FIN_KEY);
            keyPath = ERXQ.keyPath(AFFECTATION, D_FIN_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            ass.setDateformat("%d/%m/%Y");
            col.setAssociations(ass);
            colTmp.add(col);
            
            // Colonne Quotite
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Quotité");
            col.setOrderKeyPath(QUOTITE_KEY);
            keyPath = ERXQ.keyPath(AFFECTATION, QUOTITE_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            
            colonnes = colTmp.immutableClone();
        }
        return colonnes;
    }
    
    public EOAffectation getSelectedAffectation() {
        return (EOAffectation)getDisplayGroup().selectedObject();
    }
    
    public void setSelectedAffectation(EOAffectation affectation) {
        getDisplayGroup().setSelectedObject(affectation);
    }
    
    private Integer getUtilisateurPersId() {
        return (Integer)valueForBinding(BINDING_UTILISATEUR_PERSID);
    }
    
    public EOAffectation getCurrentAffectation() {
        return currentAffectation;
    }
    
    public void setCurrentAffectation(EOAffectation currentAffectation) {
        this.currentAffectation = currentAffectation;
    }
    
    public boolean showForm() {
        return getSelectedAffectation() != null;
    }
    
    public boolean isFormEditing() {
        return isFormEditing;
    }
    
    public void setFormEditing(boolean isFormEditing) {
        this.isFormEditing = isFormEditing;
    }

    public boolean resetForm() {
        return resetForm;
    }

    public void setResetForm(boolean resetForm) {
        this.resetForm = resetForm;
    }
    
    /**
     * Retourne la liste des contrats hebergés courants de l'individu
     * @return liste des contrats hebergés
     */
    public NSArray<EOContratHeberge> getContratHebergeEnCours() {
    	if (getIndividu() != null) {
    		return EOContratHeberge.rechercherContratsPourIndividuEtPeriode(edc(), getIndividu(), DateCtrl.now(), null);
    	} 
    		
    	return null;
    }
    
    /**
     * Renvoie la liste des affectations liés à un individu en fonction des dates 
     * de début et de fin du contrat hebergé
     * @return liste des affectations
     */
    public NSArray<EOAffectation> getAffectationsContratHeberge() {
    	NSArray<EOAffectation> affectationsContratHeberge = null;
    	NSArray<EOContratHeberge> listeContrats = getContratHebergeEnCours();
    	
    	for (EOContratHeberge contrat : listeContrats) {
    		affectationsContratHeberge = EOAffectation.rechercherAffectationsPourIndividuEtPeriode(edc(), getIndividu(), contrat.dateDebut(), contrat.dateFin(), true);
    	}
    	
    	return affectationsContratHeberge;
    }
}