package org.cocktail.fwkcktlgrhguiajax.server.component.Listes;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlgrh.common.metier.EOAbsence;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;

/**
 * Composant d'affichage des absences d'un agent 
 * @author alainmalaplate
 *
 */
public class AffichageAbsences extends AComponent {
	
	private static final long serialVersionUID = 7558897L;

	private static final Integer NUMBER_OF_OBJECT_PER_BATCH = 10;

	private static final String BINDING_CURRENT_ABSENCE = "absences";
	private static final String BINDING_INDIVIDU = "individu";
	
	private EOAbsence currentAbsence;

	private Date dDebAbsence;
	private Date dFinAbsence;
	
	private ERXDisplayGroup<EOAbsence> displayGroupAbsences = null;
	private ERXDatabaseDataSource absencesDatasource = null;
	private EOIndividu individu;
	
	/**
	 * Constructeur du composant d'affichage des absences d'un agent
	 * @param context Contexte du composant
	 */
    public AffichageAbsences(WOContext context) {
        super(context);
    }
    
    /**
	 * @return the displayGroupAbsences
	 */
	public ERXDisplayGroup<EOAbsence> getDisplayGroupAbsences() {
		if (displayGroupAbsences == null) {
			displayGroupAbsences = new ERXDisplayGroup<EOAbsence>();
			displayGroupAbsences.setDataSource(absencesDatasource());
			displayGroupAbsences.setDelegate(new DisplayGroupModServiceDelegate());


			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOAbsence.DATE_DEBUT_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
			displayGroupAbsences.setSortOrderings(sortOrderings);

			displayGroupAbsences.setSelectsFirstObjectAfterFetch(true);
			displayGroupAbsences.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupAbsences.fetch();

		}

		return displayGroupAbsences;
	}

	/**
	 * delegate class handle the current selected item
	 */
	public class DisplayGroupModServiceDelegate {

	}

	/**
	 * @return datasource des absences
	 */
	private ERXDatabaseDataSource absencesDatasource() {
		if (absencesDatasource == null) {
			absencesDatasource = new ERXDatabaseDataSource(edc(), EOAbsence.ENTITY_NAME);
			absencesDatasource.setAuxiliaryQualifier(getQualifier());
		}
		return absencesDatasource;
	}

	/**
	 * @return the qualifier sur les absences
	 */
	public EOQualifier getQualifier() {
		return EOAbsence.qualifierAbsenceValide(getIndividu(), true);
	}
	

	/**
	 * @return the currentAbsence
	 */
	public EOAbsence getCurrentAbsence() {
		if (hasBinding(BINDING_CURRENT_ABSENCE)) {
			currentAbsence = (EOAbsence) valueForBinding(BINDING_CURRENT_ABSENCE);
		}
		return currentAbsence;
	}

	/**
	 * @param currentAbsence the currentAbsence to set
	 */
	public void setCurrentAbsence(EOAbsence currentAbsence) {
		setValueForBinding(currentAbsence, BINDING_CURRENT_ABSENCE);
		this.currentAbsence = currentAbsence;
	}

	/**
	 * 
	 * @return l'ID du composant
	 */
	public String getListeAbsencesTableViewId() {
		return getComponentId() + "_listeAbsencesTableViewId";
	}
	
	

	/* **************************************************************************************************** */
	/*			    	 	Méthodes d'accès aux informations des absences, résultats de la recherche 	 	*/
	/* **************************************************************************************************** */

	/**
	 * @return le libellé court du type d'absence
	 */
	public String getTypeAbsence() {
		return getCurrentAbsence().typeAbsence().lcTypeAbsence();
	}
	
	/**
	 * 
	 * @return la durée totale de l'absence
	 */
	public String getDureeTotaleAbsence() {
		return getCurrentAbsence().dureeTotale();
	}
	
	/**
	 * @return la date de début de l'absence
	 */
	public String getDateDebAbsence() {
		if (getCurrentAbsence() != null) {
			dDebAbsence = getCurrentAbsence().dateDebut();
		}
		return getDateFormatter(dDebAbsence);
	}

	/**
	 * @return la date de fin de l'absence
	 */
	public String getDateFinAbsence() {
		if (getCurrentAbsence() != null) {
			dFinAbsence = getCurrentAbsence().dateFin();
		}
		if (dFinAbsence != null) {
			return getDateFormatter(dFinAbsence);
		} else {
			return null;
		}
	}

	/**
	 * Test de la nullité ou non de la date de fin
	 * @return true si la date de fin est nulle
	 */
	public boolean isDateFinNulle() {
		if (getCurrentAbsence().dateFin() == null) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Retourne la String de format des dates
	 * @param dt1 la date passée pour être formatée
	 * @return le format par défaut pour les dates
	 */
	public String getDateFormatter(Date dt1) {
		String dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		dt = sdf.format(dt1);
		return dt;
	}

	/**
	 * @return individu
	 */
	public EOIndividu getIndividu() {
		individu = (EOIndividu) valueForBinding(BINDING_INDIVIDU);
		return individu;
	}

	public void setIndividu(EOIndividu individu) {
		this.individu = individu;
	}	


}