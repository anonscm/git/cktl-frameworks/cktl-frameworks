package org.cocktail.fwkcktlgrhguiajax.server.component.Listes;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

/**
 * Composant d'affichage des élèments de carrière d'un agent 
 * @author alainmalaplate
 *
 */
public class AffichageElementsCarriere extends AComponent {
	
	private static final long serialVersionUID = 7778897L;
	
	private static final Integer NUMBER_OF_OBJECT_PER_BATCH = 10;
	
	private static final String BINDING_CURRENT_ELEMENT = "element";
	private static final String BINDING_QUALIFIER = "qualifier";
	
	private EOElements currentElement;
	
	private ERXDisplayGroup<EOElements> displayGroupElements = null;
	private ERXDatabaseDataSource contratDatasource = null;
	private EOQualifier qualifier;
	
	private String labelEmploi;
	
	private String bap;
	private String echelon;
	private String indice = "";
	private String grade;
	private Date dDebElt;
	private Date dFinElt;
	
	/**
	 * Constructeur du composant d'affichage des élèments de carrière d'un agent
	 * @param context Contexte du composant
	 */
    public AffichageElementsCarriere(WOContext context) {
        super(context);
    }
    
    /**
	 * @return the displayGroupElements
	 */
	public ERXDisplayGroup<EOElements> getDisplayGroupElements() {
		if (displayGroupElements == null) {
			displayGroupElements = new ERXDisplayGroup<EOElements>();
			displayGroupElements.setDataSource(contratDatasource());
			displayGroupElements.setDelegate(new DisplayGroupContratDelegate());
			
			
			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOElements.D_EFFET_ELEMENT_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
			displayGroupElements.setSortOrderings(sortOrderings);

			displayGroupElements.setSelectsFirstObjectAfterFetch(true);
			displayGroupElements.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupElements.fetch();
			
		}

		return displayGroupElements;
	}
	
	/**
	 * delegate class handle the current selected item
	 */
	public class DisplayGroupContratDelegate {

		/**
		 * @param group : groupe d'élement sélectionné
		 */
//		public void displayGroupDidChangeSelectedObjects(final WODisplayGroup group) {
//			@SuppressWarnings("unchecked")
//			ERXDisplayGroup<EOElements> groupe = (ERXDisplayGroup<EOElements>) group;
//		}
	}

	/**
	 * @return datasource des élèments de carrière
	 */
	private ERXDatabaseDataSource contratDatasource() {
		if (contratDatasource == null) {
			contratDatasource = new ERXDatabaseDataSource(edc(), EOElements.ENTITY_NAME);
			contratDatasource.setAuxiliaryQualifier(getQualifier());
		}
		return contratDatasource;
	}
    
	/**
	 * @return the qualifier
	 */
	public EOQualifier getQualifier() {
		qualifier = (EOQualifier) valueForBinding(BINDING_QUALIFIER);
		qualifier = ERXQ.and(new NSArray<EOQualifier>(new EOQualifier[] {
				qualifier,
				ERXQ.equals(EOElements.TEM_VALIDE_KEY, "O")
		}));
		return qualifier;
	}
    
    
    /**
	 * @return the currentElement
	 */
    public EOElements getCurrentElement() {
    	if (hasBinding(BINDING_CURRENT_ELEMENT)) {
			currentElement = (EOElements) valueForBinding(BINDING_CURRENT_ELEMENT);
		}
		return currentElement;
	}

    /**
	 * @param currentElement the currentElement to set
	 */
	public void setCurrentElement(EOElements currentElement) {
		setValueForBinding(currentElement, BINDING_CURRENT_ELEMENT);
		this.currentElement = currentElement;
	}

	/**
	 * 
	 * @return l'ID du composant
	 */
	public String getListeElementsTableViewId() {
		return getComponentId() + "_listeElementsTableViewId";
	}
	
	/* **************************************************************************************************** */
	/*			Méthodes d'accès aux informations de l'élément de carrière, résultats de la recherche		*/
	/* **************************************************************************************************** */
	
	/**
	 * @return le label de l'emploi
	 */
	public String getLabelEmploi() {
		//permet lors du 1er affichage si getEmploi ne renvoie rien, d'avoir un titre pour la colonne concernée
		if (labelEmploi == null) {
			labelEmploi = "Code emploi";
		}
		
		return labelEmploi;
	}

	/**
	 * 
	 * @param labelEmploi label associé au type de l'emploi
	 */
	public void setLabelEmploi(String labelEmploi) {
		this.labelEmploi = labelEmploi;
	}
	
	/**
	 * @return le grade de cet élèment de carrière
	 */
	public String getGrade() {
		if (getCurrentElement() != null) {
			grade = getCurrentElement().toGrade().llGrade();
		}
		return grade;
	}
	
	/**
	 * 
	 * @return l'échelon de cet élèment de carrière
	 */
	public String getEchelon() {
		if (getCurrentElement() != null) {
			echelon = getCurrentElement().cEchelon();
		}
		return echelon;
	}
	
	/**
	 * 
	 * @return l'indice de cet élèment de carrière
	 */
	public String getIndice() {
		indice = null;
		if (getCurrentElement() != null) {
			if (getCurrentElement().inmEffectif() != null) {
				indice = getCurrentElement().inmEffectif().toString();
			} else if (getCurrentElement().indiceMajore() != null) {
				indice = getCurrentElement().indiceMajore().toString();
			}
		}
		return indice;
	}
	
	/**
	 * 
	 * @return le code emploi ou le N° de CNU ou la discipline du 2nd degré de cet élèment de carrière
	 */
	public String getEmploi() {
		bap = "";
		
		if (getCurrentElement() != null) {
			if (getCurrentElement().codeemploi() != null) {
				setLabelEmploi("Code emploi");
				bap = getCurrentElement().codeemploi();
			} else if (getCurrentElement().toCnu() != null) {
				setLabelEmploi("CNU");
				bap = getCurrentElement().toCnu().lcSectionCnu();
			} else if (getCurrentElement().toDiscSecondDegre() != null) {
				setLabelEmploi("Disc. 2nd Degré");
				bap = getCurrentElement().toDiscSecondDegre().lcDiscSecondDegre();
			} 
		}
		return bap;
	}
	
	
	/**
	 * @return la date de début de l'élèment de carrière
	 */
	public String getDateEffetElement() {
		if (getCurrentElement() != null) {
			dDebElt = getCurrentElement().dEffetElement();
		}
		return getDateFormatter(dDebElt);
	}
	
	/**
	 * @return la date de fin de l'élèment de carrière
	 */
	public String getDateFinElement() {
		if (getCurrentElement() != null) {
			dFinElt = getCurrentElement().dFinElement();
		}
		if (dFinElt != null) {
			return getDateFormatter(dFinElt);
		} else {
			return null;
		}
	}

	/**
	 * Test de la nullité ou non de la date de fin
	 * @return true si la date de fin est nulle
	 */
	public boolean isDateFinNulle() {
		if (getCurrentElement().dFinElement() == null) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Retourne la String de format des dates
	 * @param dt1 la date passée pour être formatée
	 * @return le format par défaut pour les dates
	 */
	public String getDateFormatter(Date dt1) {
		String dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		dt = sdf.format(dt1);
		return dt;
	}	
	
}