package org.cocktail.fwkcktlgrhguiajax.server.component.Listes;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlgrh.common.metier.EOModalitesService;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

/**
 * Composant d'affichage des modalités de service d'un agent 
 * @author alainmalaplate
 *
 */
public class AffichageModalitesService extends AComponent {


	private static final long serialVersionUID = 7668897L;

	private static final Integer NUMBER_OF_OBJECT_PER_BATCH = 10;

	private static final String BINDING_CURRENT_MOD_SERVICE = "modaliteService";
	private static final String BINDING_QUALIFIER = "qualifier";

	private EOModalitesService currentModService;

	private Date dDebModServ;
	private Date dFinModServ;

	private ERXDisplayGroup<EOModalitesService> displayGroupModService = null;
	private ERXDatabaseDataSource modServiceDatasource = null;
	private EOQualifier qualifier;

	/**
	 * Constructeur du composant d'affichage des contrats d'un agent
	 * @param context Contexte du composant
	 */
	public AffichageModalitesService(WOContext context) {
		super(context);
	}

	/**
	 * @return the displayGroupModService
	 */
	public ERXDisplayGroup<EOModalitesService> getDisplayGroupModService() {
		if (displayGroupModService == null) {
			displayGroupModService = new ERXDisplayGroup<EOModalitesService>();
			displayGroupModService.setDataSource(modalitesServiceDatasource());
			displayGroupModService.setDelegate(new DisplayGroupModServiceDelegate());


			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOModalitesService.DATE_DEBUT_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
			displayGroupModService.setSortOrderings(sortOrderings);

			displayGroupModService.setSelectsFirstObjectAfterFetch(true);
			displayGroupModService.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupModService.fetch();

		}

		return displayGroupModService;
	}

	/**
	 * delegate class handle the current selected item
	 */
	public class DisplayGroupModServiceDelegate {

	}

	/**
	 * @return datasource des modalités de service
	 */
	private ERXDatabaseDataSource modalitesServiceDatasource() {
		if (modServiceDatasource == null) {
			modServiceDatasource = new ERXDatabaseDataSource(edc(), EOModalitesService.ENTITY_NAME);
			modServiceDatasource.setAuxiliaryQualifier(getQualifier());
		}
		return modServiceDatasource;
	}

	/**
	 * @return the qualifier sur les modalités de service
	 */
	public EOQualifier getQualifier() {
		qualifier = (EOQualifier) valueForBinding(BINDING_QUALIFIER);
		qualifier = ERXQ.and(new NSArray<EOQualifier>(new EOQualifier[] {
				qualifier,
				ERXQ.equals(EOModalitesService.VALIDE_KEY, "O")
		}));
		return qualifier;
	}

	/**
	 * @return the currentModService
	 */
	public EOModalitesService getCurrentModService() {
		if (hasBinding(BINDING_CURRENT_MOD_SERVICE)) {
			currentModService = (EOModalitesService) valueForBinding(BINDING_CURRENT_MOD_SERVICE);
		}
		return currentModService;
	}

	/**
	 * @param currentModService the currentModService to set
	 */
	public void setCurrentModService(EOModalitesService currentModService) {
		setValueForBinding(currentModService, BINDING_CURRENT_MOD_SERVICE);
		this.currentModService = currentModService;
	}

	/**
	 * 
	 * @return l'ID du composant
	 */
	public String getListeModServiceTableViewId() {
		return getComponentId() + "_listeModServiceTableViewId";
	}

	/* **************************************************************************************************** */
	/*			Méthodes d'accès aux informations des modalités de service, résultats de la recherche		*/
	/* **************************************************************************************************** */

	/**
	 * @return le libellé de la modalité de service
	 */
	public String getModLibelle() {
		return getCurrentModService().libelle();
	}
	
	/**
	 * 
	 * @return la quotité de la modalité de service
	 */
	public String getModQuotite() {
		return getCurrentModService().quotite().toString();
	}
	
	/**
	 * 
	 * @return les infos sur cette modalité de service
	 */
	public String getModInfos() {
		return getCurrentModService().commentaire();
	}
	
	/**
	 * @return la date de début de la modalité de service
	 */
	public String getDateDebModServ() {
		if (getCurrentModService() != null) {
			dDebModServ = getCurrentModService().dateDebut();
		}
		return getDateFormatter(dDebModServ);
	}

	/**
	 * @return la date de fin de la modalité de service
	 */
	public String getDateFinModServ() {
		if (getCurrentModService() != null) {
			dFinModServ = getCurrentModService().dateFin();
		}
		if (dFinModServ != null) {
			return getDateFormatter(dFinModServ);
		} else {
			return null;
		}
	}

	/**
	 * Test de la nullité ou non de la date de fin
	 * @return true si la date de fin est nulle
	 */
	public boolean isDateFinNulle() {
		if (getCurrentModService().dateFin() == null) {
			return true;
		}
		
		return false;
	}

	/**
	 * Retourne la String de format des dates
	 * @param dt1 la date passée pour être formatée
	 * @return le format par défaut pour les dates
	 */
	public String getDateFormatter(Date dt1) {
		String dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		dt = sdf.format(dt1);
		return dt;
	}	

}