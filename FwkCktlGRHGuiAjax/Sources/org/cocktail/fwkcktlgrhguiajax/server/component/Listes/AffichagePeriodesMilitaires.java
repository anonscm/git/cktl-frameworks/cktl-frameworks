package org.cocktail.fwkcktlgrhguiajax.server.component.Listes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlgrh.common.metier.EOPeriodesMilitaires;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

/**
 * Composant d'affichage des périodes militaires d'un agent 
 * @author alainmalaplate
 *
 */
public class AffichagePeriodesMilitaires extends AComponent {
	
	
	private static final long serialVersionUID = 7448897L;

	private static final Integer NUMBER_OF_OBJECT_PER_BATCH = 10;

	private static final String BINDING_CURRENT_PERIODE_MILITAIRE = "periodesMilitaires";
	private static final String BINDING_QUALIFIER = "qualifier";
	
	private EOPeriodesMilitaires currentPeriodeMilitaire;

	private Date dDebPeriodeMilit;
	private Date dFinPeriodeMilit;
	

	private ERXDisplayGroup<EOPeriodesMilitaires> displayGroupPeriodesMilitaires = null;
	private ERXDatabaseDataSource periodeMilitaireDatasource = null;
	private EOQualifier qualifier;
	
	/**
	 * Constructeur du composant d'affichage des périodes militaires d'un agent
	 * @param context Contexte du composant
	 */
    public AffichagePeriodesMilitaires(WOContext context) {
        super(context);
    }
    
    
    /**
	 * @return the displayGroupPeriodesMilitaires
	 */
	public ERXDisplayGroup<EOPeriodesMilitaires> getDisplayGroupPeriodesMilitaires() {
		if (displayGroupPeriodesMilitaires == null) {
			displayGroupPeriodesMilitaires = new ERXDisplayGroup<EOPeriodesMilitaires>();
			displayGroupPeriodesMilitaires.setDataSource(periodesMilitairesDatasource());
			displayGroupPeriodesMilitaires.setDelegate(new DisplayGroupPeriodesMilitairesDelegate());


			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOPeriodesMilitaires.DATE_DEBUT_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
			displayGroupPeriodesMilitaires.setSortOrderings(sortOrderings);

			displayGroupPeriodesMilitaires.setSelectsFirstObjectAfterFetch(true);
			displayGroupPeriodesMilitaires.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupPeriodesMilitaires.fetch();

		}

		return displayGroupPeriodesMilitaires;
	}

	/**
	 * delegate class handle the current selected item
	 */
	public class DisplayGroupPeriodesMilitairesDelegate {

	}

	/**
	 * @return datasource des absences
	 */
	private ERXDatabaseDataSource periodesMilitairesDatasource() {
		if (periodeMilitaireDatasource == null) {
			periodeMilitaireDatasource = new ERXDatabaseDataSource(edc(), EOPeriodesMilitaires.ENTITY_NAME);
			periodeMilitaireDatasource.setAuxiliaryQualifier(getQualifier());
		}
		return periodeMilitaireDatasource;
	}

	/**
	 * @return the qualifier sur les absences
	 */
	public EOQualifier getQualifier() {
		qualifier = (EOQualifier) valueForBinding(BINDING_QUALIFIER);
		qualifier = ERXQ.and(new NSArray<EOQualifier>(new EOQualifier[] {
				qualifier,
				ERXQ.equals(EOPeriodesMilitaires.TEM_VALIDE_KEY, "O")
		}));
		return qualifier;
	}
	

	/**
	 * @return the currentPeriodeMilitaire
	 */
	public EOPeriodesMilitaires getCurrentPeriodeMilitaire() {
		if (hasBinding(BINDING_CURRENT_PERIODE_MILITAIRE)) {
			currentPeriodeMilitaire = (EOPeriodesMilitaires) valueForBinding(BINDING_CURRENT_PERIODE_MILITAIRE);
		}
		return currentPeriodeMilitaire;
	}

	/**
	 * @param currentPeriodeMilitaire the currentPeriodeMilitaire to set
	 */
	public void setCurrentPeriodeMilitaire(EOPeriodesMilitaires currentPeriodeMilitaire) {
		setValueForBinding(currentPeriodeMilitaire, BINDING_CURRENT_PERIODE_MILITAIRE);
		this.currentPeriodeMilitaire = currentPeriodeMilitaire;
	}

	/**
	 * 
	 * @return l'ID du composant
	 */
	public String getListePeriodesMilitairesTableViewId() {
		return getComponentId() + "_listePeriodesMilitairesTableViewId";
	}
	
	

	/* **************************************************************************************************** */
	/*			Méthodes d'accès aux informations des périodes militaires, résultats de la recherche		*/
	/* **************************************************************************************************** */

	/**
	 * @return le libellé long du type de période militaire
	 */
	public String getTypePeriodeMilitaire() {
		return getCurrentPeriodeMilitaire().toTypePeriodeMilit().llTypePeriodMilit();
	}
	
	
	/**
	 * @return la date de début de la période militaire
	 */
	public String getDateDebPeriodeMilitaire() {
		if (getCurrentPeriodeMilitaire() != null) {
			dDebPeriodeMilit = getCurrentPeriodeMilitaire().dateDebut();
		}
		return getDateFormatter(dDebPeriodeMilit);
	}

	/**
	 * @return la date de fin de la période militaire
	 */
	public String getDateFinPeriodeMilitaire() {
		if (getCurrentPeriodeMilitaire() != null) {
			dFinPeriodeMilit = getCurrentPeriodeMilitaire().dateFin();
		}
		return getDateFormatter(dFinPeriodeMilit);
	}

	/**
	 * Test de la nullité ou non de la date de fin
	 * @return true si la date de fin est nulle
	 */
	public boolean isDateFinNulle() {
		if (getCurrentPeriodeMilitaire().dateFin() == null) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Retourne la String de format des dates
	 * @param dt1 la date passée pour être formatée
	 * @return le format par défaut pour les dates
	 */
	public String getDateFormatter(Date dt1) {
		String dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		dt = sdf.format(dt1);
		return dt;
	}	
}