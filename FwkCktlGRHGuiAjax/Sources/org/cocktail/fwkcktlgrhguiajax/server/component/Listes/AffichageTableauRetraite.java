package org.cocktail.fwkcktlgrhguiajax.server.component.Listes;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlgrh.modele.FicheAnciennete;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXDisplayGroup;

/**
 * Permet l'affichage d'un tableau listant les éléments liés à la retraite d'un individu passé en binding 
 * 
 * @binding individu Individu dont les éléments de retraite sont affichés
 * @author Frédéric Favreau
 */
public class AffichageTableauRetraite extends AComponent {
	private static final String INDIVIDU_KEY = "individu";
	private FicheAnciennete currentElement = null;
	private ERXDisplayGroup<FicheAnciennete> displayGroupElements = null;

	public AffichageTableauRetraite(WOContext context) {
		super(context);
	}

	/**
	 * Retourne la String de format des dates
	 * 
	 * @param dt1 la date passée pour être formatée
	 * @return le format par défaut pour les dates
	 */
	public String getDateFormatter(Date dt1) {
		String dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		dt = sdf.format(dt1);
		return dt;
	}


	/**
	 * 
	 * @return l'individu à afficher
	 */
	public EOIndividu getIndividu() {
		return (EOIndividu) valueForBinding(INDIVIDU_KEY);
	}

	/**
	 * 
	 * @return la position de l'élément de retraite
	 */
	public String getPosition() {
		if (currentElement == null || currentElement.position() == null)
			return "";
		return currentElement.position();
	}

	/**
	 * 
	 * @return la localisation de l'élément de retraite
	 */
	public String getLocalisation() {
		if (currentElement == null || currentElement.localisation() == null)
			return "";
		return currentElement.localisation();
	}

	/**
	 * 
	 * @return la date de début de la période
	 */
	public String getDebut() {
		String res = "";
		if (currentElement != null && currentElement.periodeDeb() != null)
			res = getDateFormatter(currentElement.periodeDeb());
		return res;
	}

	/**
	 * 
	 * @return la date de fin de la période
	 */
	public String getFin() {
		String res = "";
		if (currentElement != null && currentElement.periodeFin() != null)
			res = getDateFormatter(currentElement.periodeFin());
		return res;
	}

	/**
	 * 
	 * @return la quotité affichée liée à l'élément de retraite
	 */
	public String getQuotite() {
		if (currentElement == null || currentElement.quotite() == null)
			return "";
		return currentElement.quotite().toString();
	}

	/**
	 * 
	 * @return le nombre d'années affiché lié à l'élément de retraite
	 */
	public String getAnnees() {
		if (currentElement == null || currentElement.ancServAnnees() == null)
			return "";
		return currentElement.ancServAnnees().toString();
	}

	/**
	 * 
	 * @return le nombre de mois affiché lié à l'élément de retraite
	 */
	public String getMois() {
		if (currentElement == null || currentElement.ancServMois() == null)
			return "";
		return currentElement.ancServMois().toString();
	}

	/**
	 * 
	 * @return le nombre de jours affiché lié à l'élément de retraite
	 */
	public String getJours() {
		if (currentElement == null || currentElement.ancServJours() == null)
			return "";
		return currentElement.ancServJours().toString();
	}

	/**
	 * 
	 * @return l'ID du composant
	 */
	public String listeElementsRetraiteTableViewId() {
		return getComponentId() + "_listeElementsRetraiteTableViewId";
	}

	/**
	 * @return the currentElement
	 */
	public FicheAnciennete getCurrentElement() {
		return currentElement;
	}

	/**
	 * @param currentElement the currentElement to set
	 */
	public void setCurrentElement(FicheAnciennete currentElement) {
		this.currentElement = currentElement;
	}

	/**
	 * @return the displayGroupElements
	 */
	public ERXDisplayGroup<FicheAnciennete> getDisplayGroupElements() {
		if (displayGroupElements == null) {
			displayGroupElements = new ERXDisplayGroup<FicheAnciennete>();
			if (getIndividu() != null) {
				NSTimestamp now = new NSTimestamp();
				NSArray<FicheAnciennete> list = FicheAnciennete.calculerAnciennete(edc(), getIndividu(), now);
				
				displayGroupElements.setObjectArray(list);
			}
		}
		return displayGroupElements;
	}

	/**
	 * @return String indiquant le nombre d'années et jours du service auxiliaire effectif
	 */
	public String getServiceAuxiliairesEffectifs() {
		NSArray<FicheAnciennete> fiches=getDisplayGroupElements().displayedObjects();
		if (fiches!=null)
			return FicheAnciennete.calculerTotal(fiches,FicheAnciennete.ANCIENNETE_AUXILIAIRE);
		else
			return "";
	}

	/**
	 * @return String indiquant le nombre d'années et jours du service auxiliaire validé
	 */
	public String getServiceAuxiliairesValides() {
		NSArray<FicheAnciennete> fiches=getDisplayGroupElements().displayedObjects();
		if (fiches!=null)
			return FicheAnciennete.calculerTotal(fiches,FicheAnciennete.ANCIENNETE_VALIDES);
		else
			return "";
	}

	/**
	 * @return String indiquant le nombre d'années et jours du service titulaire effectif
	 */
	public String getServiceTitulairesEffectifs() {
		NSArray<FicheAnciennete> fiches=getDisplayGroupElements().displayedObjects();
		if (fiches!=null)
			return FicheAnciennete.calculerTotal(fiches,FicheAnciennete.ANCIENNETE_TITULAIRE);
		else
			return "";
	}

	/**
	 * @return String indiquant le nombre d'années et jours du total de l'ancienneté
	 */
	public String getTotal() {
		NSArray<FicheAnciennete> fiches=getDisplayGroupElements().displayedObjects();
		if (fiches!=null)
			return FicheAnciennete.calculerTotal(fiches,FicheAnciennete.TOUTE_ANCIENNETE);
		else
			return "";
	}

	/**
	 * @return String indiquant le nombre d'années et jours du total de l'ancienneté général
	 */
	public String getTotalAncienneteGenerale() {
		NSArray<FicheAnciennete> fiches=getDisplayGroupElements().displayedObjects();
		if (fiches!=null)
			return FicheAnciennete.ancienneteGenerale(fiches);
		else
			return "";
	}
}