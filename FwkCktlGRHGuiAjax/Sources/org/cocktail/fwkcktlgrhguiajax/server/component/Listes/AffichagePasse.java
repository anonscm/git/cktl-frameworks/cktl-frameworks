package org.cocktail.fwkcktlgrhguiajax.server.component.Listes;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlgrh.common.metier.EOChangementPosition;
import org.cocktail.fwkcktlgrh.common.metier.EOPasse;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

/**
 * Composant d'affichage des passés d'un agent 
 * @author alainmalaplate
 *
 */
public class AffichagePasse extends AComponent {
	
	private static final long serialVersionUID = 7118897L;

	private static final Integer NUMBER_OF_OBJECT_PER_BATCH = 10;

	private static final String BINDING_CURRENT_PASSE = "passe";
	private static final String BINDING_QUALIFIER = "qualifier";	
	
	private EOPasse currentPasse;
	
	private Date dDebPasse;
	private Date dFinPasse;

	private ERXDisplayGroup<EOPasse> displayGroupPasse = null;
	private ERXDatabaseDataSource passeDatasource = null;
	private EOQualifier qualifier;
	
	/**
	 * Constructeur du composant d'affichage des passés d'un agent
	 * @param context Contexte du composant
	 */
    public AffichagePasse(WOContext context) {
        super(context);
    }

    /**
	 * @return the displayGroupChgtPosition
	 */
	public ERXDisplayGroup<EOPasse> getDisplayGroupPasses() {
		if (displayGroupPasse == null) {
			displayGroupPasse = new ERXDisplayGroup<EOPasse>();
			displayGroupPasse.setDataSource(passesDatasource());
			displayGroupPasse.setDelegate(new DisplayGroupPasseDelegate());


			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOPasse.DATE_DEBUT_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
			displayGroupPasse.setSortOrderings(sortOrderings);

			displayGroupPasse.setSelectsFirstObjectAfterFetch(true);
			displayGroupPasse.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupPasse.fetch();

		}

		return displayGroupPasse;
	}

	/**
	 * delegate class handle the current selected item
	 */
	public class DisplayGroupPasseDelegate {

	}

	/**
	 * @return datasource des changements de position
	 */
	private ERXDatabaseDataSource passesDatasource() {
		if (passeDatasource == null) {
			passeDatasource = new ERXDatabaseDataSource(edc(), EOPasse.ENTITY_NAME);
			passeDatasource.setAuxiliaryQualifier(getQualifier());
		}
		return passeDatasource;
	}

	/**
	 * @return the qualifier sur les changements de position
	 */
	public EOQualifier getQualifier() {
		qualifier = (EOQualifier) valueForBinding(BINDING_QUALIFIER);
		qualifier = ERXQ.and(new NSArray<EOQualifier>(new EOQualifier[] {
				qualifier,
				ERXQ.equals(EOPasse.TEM_VALIDE_KEY, "O")
		}));
		return qualifier;
	}
	

	/**
	 * @return the currentPasse
	 */
	public EOPasse getCurrentPasse() {
		if (hasBinding(BINDING_CURRENT_PASSE)) {
			currentPasse = (EOPasse) valueForBinding(BINDING_CURRENT_PASSE);
		}
		return currentPasse;
	}

	/**
	 * @param currentPasse the currentPasse to set
	 */
	public void setCurrentPasse(EOPasse currentPasse) {
		setValueForBinding(currentPasse, BINDING_CURRENT_PASSE);
		this.currentPasse = currentPasse;
	}

	/**
	 * 
	 * @return l'ID du composant
	 */
	public String getListePassesTableViewId() {
		return getComponentId() + "_listePassesTableViewId";
	}

	/* **************************************************************************************************** */
	/*					Méthodes d'accès aux informations des passés, résultats de la recherche				*/
	/* **************************************************************************************************** */
	
	/**
	 * @return le type de service dans lequel l'agent était lors de cet élèment du passé
	 */
	public String getTypeService() {
//		return getCurrentPasse().pasTypeService();
		
		String typeService = "";
		
		if (getCurrentPasse().pasTypeService() == null) {
			typeService = "";
		}
		if (getCurrentPasse().pasTypeService().equals("S")) {
			typeService = "Service non titulaire validé";
		}
		if (getCurrentPasse().pasTypeService().equals("E")) {
			typeService = "Engagé";
		}
		if (getCurrentPasse().pasTypeService().equals("C")) {
			typeService = "Carrière militaire";
		}
		if (getCurrentPasse().pasTypeService().equals("T")) {
			typeService = "Etat authentique des services";
		}
		if (getCurrentPasse().pasTypeService().equals("L")) {
			typeService = "Elève avant titularisation";
		}
		return typeService;
		
	}
	
	/**
	 * 
	 * @return la quotité cotisation de cet élèment du passé
	 */
	public String getQuotiteCotisationPasse() {
		if (getCurrentPasse().pasQuotiteCotisation() != null) {
			return getCurrentPasse().pasQuotiteCotisation().toString();
		} else {
			return "";
		}
		
	}
	
	public String getDureePasse() {
		String res = "";
		if (getCurrentPasse().dureeValideeAnnees() != null) {
			res = res + getCurrentPasse().dureeValideeAnnees().toString() + " année(s) ";
		}
		if (getCurrentPasse().dureeValideeMois() != null) {
			res = res + getCurrentPasse().dureeValideeMois().toString() + " mois ";
		}
		if (getCurrentPasse().dureeValideeJours() != null) {
			res = res + getCurrentPasse().dureeValideeJours().toString() + " jour(s)";
		}
		return res; 
	}
	
	
	/**
	 * 
	 * @return l'établissement dans lequel l'agent était lors de cet élèment du passé
	 */
	public String getEtabPasse() {
		String lieuPasse = "";
		
		if (getCurrentPasse().pasMinistere() != null) {
			lieuPasse = getCurrentPasse().pasMinistere();
			
			if  (getCurrentPasse().etablissementPasse() != null) {
				lieuPasse = lieuPasse + " - " + getCurrentPasse().etablissementPasse();
			}
		} else {
			lieuPasse = getCurrentPasse().etablissementPasse();
		}
		
		return lieuPasse;
	}
	
	
	/**
	 * @return la date de début de cet élément passé
	 */
	public String getDateDebPasse() {
		if (getCurrentPasse() != null) {
			dDebPasse = getCurrentPasse().dateDebut();
		}
		return getDateFormatter(dDebPasse);
	}

	/**
	 * @return la date de fin de cet élément passé
	 */
	public String getDateFinPasse() {
		if (getCurrentPasse() != null) {
			dFinPasse = getCurrentPasse().dateFin();
		}
		if (dFinPasse != null) {
			return getDateFormatter(dFinPasse);
		} else {
			return null;
		}
	}

	/**
	 * Test de la nullité ou non de la date de fin
	 * @return true si la date de fin est nulle
	 */
	public boolean isDateFinNulle() {
		if (getCurrentPasse().dateFin() == null) {
			return true;
		}
		return false;
	}

	
	public String getDateValidationPasse() {
		if (getCurrentPasse() != null && getCurrentPasse().dValidationService() != null) {
			return getDateFormatter(getCurrentPasse().dValidationService());
		}
		return "";
	}
	
	/**
	 * Retourne la String de format des dates
	 * @param dt1 la date passée pour être formatée
	 * @return le format par défaut pour les dates
	 */
	public String getDateFormatter(Date dt1) {
		String dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		dt = sdf.format(dt1);
		return dt;
	}
}