package org.cocktail.fwkcktlgrhguiajax.server.component.Listes;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlgrh.common.metier.EOChangementPosition;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

/**
 * Composant d'affichage des changements de position d'un agent 
 * @author alainmalaplate
 *
 */
public class AffichageChangementPosition extends AComponent {
	
	private static final long serialVersionUID = 7228897L;

	private static final Integer NUMBER_OF_OBJECT_PER_BATCH = 10;

	private static final String BINDING_CURRENT_CHGT_POSITION = "chgtPosition";
	private static final String BINDING_QUALIFIER = "qualifier";	
	
	private EOChangementPosition currentPosition;
	
	private Date dDebPosition;
	private Date dFinPosition;

	private ERXDisplayGroup<EOChangementPosition> displayGroupChgtPosition = null;
	private ERXDatabaseDataSource positionDatasource = null;
	private EOQualifier qualifier;
	
	/**
	 * Constructeur du composant d'affichage des bonifications d'un agent
	 * @param context Contexte du composant
	 */
    public AffichageChangementPosition(WOContext context) {
        super(context);
    }

    /**
	 * @return the displayGroupChgtPosition
	 */
	public ERXDisplayGroup<EOChangementPosition> getDisplayGroupChgtPosition() {
		if (displayGroupChgtPosition == null) {
			displayGroupChgtPosition = new ERXDisplayGroup<EOChangementPosition>();
			displayGroupChgtPosition.setDataSource(chgtsPositionDatasource());
			displayGroupChgtPosition.setDelegate(new DisplayGroupChgtPositionDelegate());


			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOChangementPosition.D_DEB_POSITION_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
			displayGroupChgtPosition.setSortOrderings(sortOrderings);

			displayGroupChgtPosition.setSelectsFirstObjectAfterFetch(true);
			displayGroupChgtPosition.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupChgtPosition.fetch();

		}

		return displayGroupChgtPosition;
	}

	/**
	 * delegate class handle the current selected item
	 */
	public class DisplayGroupChgtPositionDelegate {

	}

	/**
	 * @return datasource des changements de position
	 */
	private ERXDatabaseDataSource chgtsPositionDatasource() {
		if (positionDatasource == null) {
			positionDatasource = new ERXDatabaseDataSource(edc(), EOChangementPosition.ENTITY_NAME);
			positionDatasource.setAuxiliaryQualifier(getQualifier());
		}
		return positionDatasource;
	}

	/**
	 * @return the qualifier sur les changements de position
	 */
	public EOQualifier getQualifier() {
		qualifier = (EOQualifier) valueForBinding(BINDING_QUALIFIER);
		qualifier = ERXQ.and(new NSArray<EOQualifier>(new EOQualifier[] {
				qualifier,
				ERXQ.equals(EOChangementPosition.TEM_VALIDE_KEY, "O")
		}));
		return qualifier;
	}
	

	/**
	 * @return the currentPosition
	 */
	public EOChangementPosition getCurrentPosition() {
		if (hasBinding(BINDING_CURRENT_CHGT_POSITION)) {
			currentPosition = (EOChangementPosition) valueForBinding(BINDING_CURRENT_CHGT_POSITION);
		}
		return currentPosition;
	}

	/**
	 * @param currentPosition the currentPosition to set
	 */
	public void setCurrentPosition(EOChangementPosition currentPosition) {
		setValueForBinding(currentPosition, BINDING_CURRENT_CHGT_POSITION);
		this.currentPosition = currentPosition;
	}

	/**
	 * 
	 * @return l'ID du composant
	 */
	public String getListeChgtPositionTableViewId() {
		return getComponentId() + "_listeChgtPositionTableViewId";
	}

	/* **************************************************************************************************** */
	/*		Méthodes d'accès aux informations des changements de position, résultats de la recherche		*/
	/* **************************************************************************************************** */
	
	/**
	 * @return le libellé court de la position
	 */
	public String getLibellePosition() {
		return getCurrentPosition().libellePosition();
	}
	
	/**
	 * 
	 * @return le motif du changement de position
	 */
	public String getMotifPosition() {
		if (getCurrentPosition().toMotifPosition() != null) {
			return getCurrentPosition().toMotifPosition().lcMotifPosition();
		} else {
			return "";
		}
	}
	
	
	/**
	 * 
	 * @return le lieu où s'effectue cette position
	 */
	public String getLieuPosition() {
		return getCurrentPosition().lieuDestin();
	}
	
	
	public String getPcAcquitee() {
		if ("O".equals(getCurrentPosition().temPcAcquitee())) {
			return "Oui";
		}
		return "Non";
	}
	
	/**
	 * @return la date de début de la position
	 */
	public String getDateDebPosition() {
		if (getCurrentPosition() != null) {
			dDebPosition = getCurrentPosition().dDebPosition();
		}
		return getDateFormatter(dDebPosition);
	}

	/**
	 * @return la date de fin de la position
	 */
	public String getDateFinPosition() {
		if (getCurrentPosition() != null) {
			dFinPosition = getCurrentPosition().dFinPosition();
		}
		if (dFinPosition != null) {
			return getDateFormatter(dFinPosition);
		} else {
			return null;
		}
	}

	/**
	 * Test de la nullité ou non de la date de fin
	 * @return true si la date de fin est nulle
	 */
	public boolean isDateFinNulle() {
		if (getCurrentPosition().dFinPosition() == null) {
			return true;
		}
		return false;
	}

	
	/**
	 * Retourne la String de format des dates
	 * @param dt1 la date passée pour être formatée
	 * @return le format par défaut pour les dates
	 */
	public String getDateFormatter(Date dt1) {
		String dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		dt = sdf.format(dt1);
		return dt;
	}	
}