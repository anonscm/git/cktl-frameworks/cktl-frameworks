package org.cocktail.fwkcktlgrhguiajax.server.component.Listes;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlgrh.common.metier.EOBonifications;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

/**
 * Composant d'affichage des bonifications d'un agent 
 * @author alainmalaplate
 *
 */
public class AffichageBonifications extends AComponent {
	
	private static final long serialVersionUID = 7338897L;

	private static final Integer NUMBER_OF_OBJECT_PER_BATCH = 10;

	private static final String BINDING_CURRENT_BONIFICATIONS = "bonifications";
	private static final String BINDING_QUALIFIER = "qualifier";
	

	private EOBonifications currentBonification;

	private Date dDebBonification;
	private Date dFinBonification;
	private Date dArriveeTerritoireBonification;

	private ERXDisplayGroup<EOBonifications> displayGroupBonifications = null;
	private ERXDatabaseDataSource bonificationDatasource = null;
	private EOQualifier qualifier;
	
	/**
	 * Constructeur du composant d'affichage des bonifications d'un agent
	 * @param context Contexte du composant
	 */
    public AffichageBonifications(WOContext context) {
        super(context);
    }
    
    /**
	 * @return the displayGroupBonifications
	 */
	public ERXDisplayGroup<EOBonifications> getDisplayGroupBonifications() {
		if (displayGroupBonifications == null) {
			displayGroupBonifications = new ERXDisplayGroup<EOBonifications>();
			displayGroupBonifications.setDataSource(bonificationsDatasource());
			displayGroupBonifications.setDelegate(new DisplayGroupBonificationsDelegate());


			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOBonifications.DATE_DEBUT_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
			displayGroupBonifications.setSortOrderings(sortOrderings);

			displayGroupBonifications.setSelectsFirstObjectAfterFetch(true);
			displayGroupBonifications.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupBonifications.fetch();

		}

		return displayGroupBonifications;
	}

	/**
	 * delegate class handle the current selected item
	 */
	public class DisplayGroupBonificationsDelegate {

	}

	/**
	 * @return datasource des bonifications
	 */
	private ERXDatabaseDataSource bonificationsDatasource() {
		if (bonificationDatasource == null) {
			bonificationDatasource = new ERXDatabaseDataSource(edc(), EOBonifications.ENTITY_NAME);
			bonificationDatasource.setAuxiliaryQualifier(getQualifier());
		}
		return bonificationDatasource;
	}

	/**
	 * @return the qualifier sur les bonifications
	 */
	public EOQualifier getQualifier() {
		qualifier = (EOQualifier) valueForBinding(BINDING_QUALIFIER);
		qualifier = ERXQ.and(new NSArray<EOQualifier>(new EOQualifier[] {
				qualifier,
				ERXQ.equals(EOBonifications.TEM_VALIDE_KEY, "O")
		}));
		return qualifier;
	}
	

	/**
	 * @return the currentBonification
	 */
	public EOBonifications getCurrentBonification() {
		if (hasBinding(BINDING_CURRENT_BONIFICATIONS)) {
			currentBonification = (EOBonifications) valueForBinding(BINDING_CURRENT_BONIFICATIONS);
		}
		return currentBonification;
	}

	/**
	 * @param currentBonification the currentBonification to set
	 */
	public void setCurrentBonification(EOBonifications currentBonification) {
		setValueForBinding(currentBonification, BINDING_CURRENT_BONIFICATIONS);
		this.currentBonification = currentBonification;
	}

	/**
	 * 
	 * @return l'ID du composant
	 */
	public String getListeBonificationsTableViewId() {
		return getComponentId() + "_listeBonificationsTableViewId";
	}
	
	

	/* **************************************************************************************************** */
	/*					Méthodes d'accès aux informations des bonifications, résultats de la recherche		*/
	/* **************************************************************************************************** */
	
	/**
	 * @return le libellé court de la nature de la bonification
	 */
	public String getNatureBonification() {
		return getCurrentBonification().toNatureBonification().nboLibelleCourt();
	}
	
	/**
	 * 
	 * @return la durée de la bonification
	 */
	public String getDureeBonification() {
		return getCurrentBonification().boniDuree();
	}
	
	/**
	 * 
	 * @return le taux de la bonification
	 */
	public String getTauxBonification() {
		if (getCurrentBonification().toTerritoire() != null && getCurrentBonification().toTerritoire().toTaux() != null) {
			return getCurrentBonification().toTerritoire().toTaux().nbtLibelle();
		}
		return "";
	}
	
	/**
	 * 
	 * @return libellé du territoire
	 */
	public String getTerritoireBonification() {
		if (getCurrentBonification().toTerritoire() != null) {
			return getCurrentBonification().toTerritoire().nbotLibelle();
		}
		return "";
	}
	
	/**
	 * 
	 * @return la dte d'arrivée sur le territoire
	 */
	public String getDArriveeTerritoire() {
		if (getCurrentBonification() != null) {
			dArriveeTerritoireBonification = getCurrentBonification().dateArrivee();
		}
		if (dArriveeTerritoireBonification != null) {
			return getDateFormatter(dArriveeTerritoireBonification);
		} else {
			return null;
		}
	}
	
	/**
	 * @return la date de début de la bonification
	 */
	public String getDateDebBonification() {
		if (getCurrentBonification() != null) {
			dDebBonification = getCurrentBonification().dateDebut();
		}
		return getDateFormatter(dDebBonification);
	}

	/**
	 * @return la date de fin de la bonification
	 */
	public String getDateFinBonification() {
		if (getCurrentBonification() != null) {
			dFinBonification = getCurrentBonification().dateFin();
		}
		if (dFinBonification != null) {
			return getDateFormatter(dFinBonification);
		} else {
			return null;
		}
	}

	/**
	 * Test de la nullité ou non de la date de fin
	 * @return true si la date de fin est nulle
	 */
	public boolean isDateFinNulle() {
		if (getCurrentBonification().dateFin() == null) {
			return true;
		}
		return false;
	}

	
	/**
	 * Retourne la String de format des dates
	 * @param dt1 la date passée pour être formatée
	 * @return le format par défaut pour les dates
	 */
	public String getDateFormatter(Date dt1) {
		String dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		dt = sdf.format(dt1);
		return dt;
	}	
	
}