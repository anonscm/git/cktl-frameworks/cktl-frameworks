package org.cocktail.fwkcktlgrhguiajax.server.component.Listes;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

/**
 * Composant d'affichage des affectations RH pour un agent 
 * @author alainmalaplate
 *
 */
public class AffichageAffectations extends AComponent {
	
private static final long serialVersionUID = 77788777L;
	
	private static final Integer NUMBER_OF_OBJECT_PER_BATCH = 10;
	
	private static final String BINDING_CURRENT_AFFECTATION = "affectation";
	private static final String BINDING_QUALIFIER = "qualifier";
	
	private EOAffectation currentAffectation;
	
	private String nomStructure;
	private String quotite;
	private Date dDebAff;
	private Date dFinAff;
	
	private ERXDisplayGroup<EOAffectation> displayGroupAffectations = null;
	private ERXDatabaseDataSource affectationDatasource = null;
	private EOQualifier qualifier;

	/**
	 * Constructeur du composant d'affichage des affectations d'un agent
	 * @param context contexte du composant
	 */
    public AffichageAffectations(WOContext context) {
        super(context);
    }
    
    /**
	 * @return the displayGroupAffectations
	 */
	public ERXDisplayGroup<EOAffectation> getDisplayGroupAffectations() {
		if (displayGroupAffectations == null) {
			displayGroupAffectations = new ERXDisplayGroup<EOAffectation>();
			displayGroupAffectations.setDataSource(affectationDatasource());
			displayGroupAffectations.setDelegate(new DisplayGroupAffectationDelegate());
			
			
			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOAffectation.D_DEB_AFFECTATION_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
			displayGroupAffectations.setSortOrderings(sortOrderings);

			displayGroupAffectations.setSelectsFirstObjectAfterFetch(true);
			displayGroupAffectations.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupAffectations.fetch();
			
		}

		return displayGroupAffectations;
	}
	
	/**
	 * delegate class handle the current selected item
	 */
	public class DisplayGroupAffectationDelegate {

		/**
		 * @param group : groupe d'élement sélectionné
		 */
//		public void displayGroupDidChangeSelectedObjects(final WODisplayGroup group) {
//			@SuppressWarnings("unchecked")
//			ERXDisplayGroup<EOAffectation> groupe = (ERXDisplayGroup<EOAffectation>) group;
//		}
	}

	/**
	 * @return datasource des affectations
	 */
	private ERXDatabaseDataSource affectationDatasource() {
		if (affectationDatasource == null) {
			affectationDatasource = new ERXDatabaseDataSource(edc(), EOAffectation.ENTITY_NAME);
			affectationDatasource.setAuxiliaryQualifier(getQualifier());
		}
		return affectationDatasource;
	}
    
	/**
	 * @return the qualifier
	 */
	public EOQualifier getQualifier() {
		qualifier = (EOQualifier) valueForBinding(BINDING_QUALIFIER);
		qualifier = ERXQ.and(new NSArray<EOQualifier>(new EOQualifier[] {
				qualifier,
				ERXQ.equals(EOAffectation.TEM_VALIDE_KEY, "O")
		}));
		return qualifier;
	}
	
	
    /**
	 * @return the currentAffectation
	 */
	public EOAffectation getCurrentAffectation() {
		if (hasBinding(BINDING_CURRENT_AFFECTATION)) {
			currentAffectation = (EOAffectation) valueForBinding(BINDING_CURRENT_AFFECTATION);
		}
		return currentAffectation;
	}

	/**
	 * @param currentAffectation the currentAffectation to set
	 */
	public void setCurrentAffectation(EOAffectation currentAffectation) {
		setValueForBinding(currentAffectation, BINDING_CURRENT_AFFECTATION);
		this.currentAffectation = currentAffectation;
	}
    
	/**
	 * 
	 * @return l'ID du composant
	 */
    public String getListeAffectationsTableViewId() {
		return getComponentId() + "_listeAffectationsTableViewId";
	}

	
	/* **************************************************************************************************** */
	/*					Méthodes d'accès aux informations des affectations, résultats de la recherche		*/
	/* **************************************************************************************************** */
    /**
	 * @return the name of a structure
	 */
	public String getNomStructure() {
		if (getCurrentAffectation() != null) {
			nomStructure = getCurrentAffectation().toStructure().llStructure();
		}
		return nomStructure;
	}
    
	/**
	 * @return la quotité
	 */
	public String getQuotite() {
		if (getCurrentAffectation() != null) {
			quotite = getCurrentAffectation().numQuotAffectation().toString();
		}
		return quotite;
	}
    
	/**
	 * @return la date de début de l'affectation
	 */
	public String getDateDebAffectation() {
		if (getCurrentAffectation() != null) {
			dDebAff = getCurrentAffectation().dDebAffectation();
		}
		return getDateFormatter(dDebAff);
	}
	
	/**
	 * @return la date de fin de l'affectation
	 */
	public String getDateFinAffectation() {
		if (getCurrentAffectation() != null) {
			dFinAff = getCurrentAffectation().dFinAffectation();
		}
		if (dFinAff != null) {
			return getDateFormatter(dFinAff);
		} else {
			return null;
		}
	}
	
	/**
	 * Test de nullité de la date de fin d'affectation
	 * @return true si la fin d'affectation est nulle
	 */
	public boolean isDateFinNulle() {
		if (getCurrentAffectation().dFinAffectation() == null) {
			return true;
		}
		
		return false;
	}

	
	/**
	 * Retourne la String de format des dates
	 * @param dt1 la date passée pour être formatée
	 * @return le format par défaut pour les dates
	 */
	public String getDateFormatter(Date dt1) {
		String dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		dt = sdf.format(dt1);
		return dt;
	}	
	
}