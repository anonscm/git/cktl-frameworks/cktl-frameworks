package org.cocktail.fwkcktlgrhguiajax.server.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.InfoCarriereContrat;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

/**
 * 
 * Formulaire d'édition d'une affectation.
 * 
 * @binding affectation l'EOAffectation que l'on souhaite éditer
 * @binding displayedAffectations NSArray d'EOAffectation :
 *                                les affectations déjà affichées dans le cadre d'un master detail. Cela sert à vérifier
 *                                en mémoire s'il n'y a pas de conflits d'affectation.
 * @binding utilisateurPersId l'id de l'utilisateur effectuant l'édition
 * @binding didSave action callback appelée lorsqu'une affectation a été sauvegardée par le composant
 * @binding didCancel action callback appelée lorsque l'enregistrement de l'affectation a été annulé.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class AffectationForm extends ACktlAjaxGRHComponent {

    private static final long serialVersionUID = 1645495128194997359L;
    private static final String BINDING_AFFECTATION = "affectation";
    private static final String BINDING_DISPLAYED_AFFECTATIONS = "displayedAffectations";
    private static final String BINDING_UTILISATEUR_PERSID = "utilisateurPersId"; 
    private static final String BINDING_DID_SAVE = "didSave";
    private static final String BINDING_DID_CANCEL = "didCancel";
    private static final String BINDING_DATE_CONTROLE = "dateControle";
    
    private EOAffectation affectation;
    private EOStructure oldStructure;
    private EOAssociation selectedAssociation;
    private EORepartAssociation currentRepartAssociation;
    private NSArray<EORepartAssociation> selectedsRepartAssociation;
    private String infoCarriereContrat;
    private String associationTreeViewDialogID;
    private String associationContainerID;
	private NSArray<InfoCarriereContrat> infosCarriereContratListe;
	
	private final String STRUCTURE_NULL = "Vous devez renseigner une structure"; 
    
    public AffectationForm(WOContext context) {
        super(context);
        NSNotificationCenter.defaultCenter().addObserver(
                this, Notifications.getSelector("resetComponent"), 
                Notifications.CONTRATS_SAVED_NOTIF, session());
    }
    
    public void resetComponent(NSNotification notif) {
        editingContext = null;
        affectation = null;
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    
    public EOAffectation getAffectation() {
        EOAffectation affectationParent = (EOAffectation)valueForBinding(BINDING_AFFECTATION);
        if (affectation == null || wantReset() || eoHasChanged(affectation, affectationParent)) {
            affectation = affectationParent.localInstanceIn(edc());
            infoCarriereContrat = genererInfoCarriereContrat(affectation);
        }
        return affectation;
    }
    
    private String genererInfoCarriereContrat(EOAffectation affectation) {
        String result = null;
        if (affectation.dDebAffectation() != null)  {
            infosCarriereContratListe = calculerInfoCarriereContratsForAffectation(affectation);
            result = infosCarriereContratListe.componentsJoinedByString("<br/>");
        } else {
        	infosCarriereContratListe = NSArray.emptyArray();
        }
        return result;
    }

	private NSArray<InfoCarriereContrat> calculerInfoCarriereContratsForAffectation(EOAffectation affectation) {
		NSArray<InfoCarriereContrat> infos = 
		    InfoCarriereContrat.carrieresContratsPourPeriode(
		            edc(), affectation.toIndividu(), affectation.dDebAffectation(), affectation.dFinAffectation(), true);
		return infos;
	}
    
    private Integer getUtilisateurPersId() {
        return (Integer)valueForBinding(BINDING_UTILISATEUR_PERSID);
    }

    public WOActionResults recalculerInfosCarriere() {
        infoCarriereContrat = genererInfoCarriereContrat(affectation);
        return null;
    }
    
    public WOActionResults enregistrer() {
        getAffectation().setPersIdModification(getUtilisateurPersId());
        if (getAffectation().cStructure() == null) {
        	
        	if (getStructure() == null) {
        		mySession().addSimpleErrorMessage("Erreur", STRUCTURE_NULL);
        		return null;
        	}
        	
        	getAffectation().setCStructure(getStructure().cStructure());
        }
        
        boolean success = saveChanges();
        if (success) { 
            infoCarriereContrat = genererInfoCarriereContrat(getAffectation());
            setEditing(false);
            if (hasBinding(BINDING_DID_SAVE)) {
                return (WOActionResults)valueForBinding(BINDING_DID_SAVE);
            }
        }
        return null;
    }
    
    @Override
    protected void beforeSaveChanges() throws ValidationException {
        super.beforeSaveChanges();
        // Si la structure a changé on supprime 
        if (oldStructure != null && !ERXEOControlUtilities.eoEquals(oldStructure, getAffectation().toStructure()))
            EOAffectation.supprimerRepartsStructureExistantesSansAssociationEtAssociationsHeberge(
                    edc(), getAffectation().toIndividu(), oldStructure, getUtilisateurPersId());
        // On force une premiere verif !
        getAffectation().validateForSave();
        EOAffectation.verifQuotiteAffectationInMemory(getAffectation(), getDisplayedAffectations());
    }
    
    public WOActionResults annuler() {
        edc().revert();
        setEditing(false);
        if (hasBinding(BINDING_DID_CANCEL))
            return (WOActionResults)valueForBinding(BINDING_DID_CANCEL);
        return null;
    }
    
    public WOActionResults editer() {
        setEditing(true);
        return null;
    }
    
    public WOActionResults ajouterRepartAssociation() {
        EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(edc());
        repartAssociation.setPersIdCreation(getUtilisateurPersId());
        repartAssociation.setPersIdModification(getUtilisateurPersId());
        repartAssociation.setToAssociationRelationship(getSelectedAssociation());
        repartAssociation.setRasDOuverture(getAffectation().dDebAffectation());
        repartAssociation.setRasDFermeture(getAffectation().dFinAffectation());
        getRepartStructure().addToToRepartAssociationsRelationship(repartAssociation);
        getRepartStructure().setPersIdModification(getUtilisateurPersId());
        CktlAjaxWindow.close(context(), getAssociationTreeViewDialogID());
        return null;
    }
    
    public WOActionResults supprimerRepartAssociation() {
        if (getSelectedsRepartAssociation() != null && !getSelectedsRepartAssociation().isEmpty()) {
            EORepartAssociation repartAsso = getSelectedsRepartAssociation().lastObject();
            getRepartStructure().removeFromToRepartAssociationsRelationship(repartAsso);
            getRepartStructure().setPersIdModification(getUtilisateurPersId());
            edc().deleteObject(repartAsso);
        }
        return null;
    }
    
    public WOActionResults creerRepartStructure() {
    	EORepartStructure.creerInstanceSiNecessaire(edc(), getAffectation().toIndividu(), getStructure(), getUtilisateurPersId());
    	mySession().addSimpleInfoMessage("Affectation", "Création de la Repart structure");
        AjaxUpdateContainer.updateContainerWithID(getAssociationContainerID(), context());
        return null;
    }
    
    public String getInfoCarriereContrat() {
        return infoCarriereContrat;
    }
    
    @SuppressWarnings("unchecked")
    private NSArray<EOAffectation> getDisplayedAffectations() {
        return (NSArray<EOAffectation>)valueForBinding(BINDING_DISPLAYED_AFFECTATIONS);
    }
    
    public EOStructure getStructure() {
        return getAffectation().toStructure();
    }
    
    public void setStructure(EOStructure structure) {
        if (getAffectation().toStructure() != null) {
            this.oldStructure = getAffectation().toStructure();
            LOG.info("Passage de la structure " + oldStructure + " a la structure " + structure +
                    "pour l'affectation " + getAffectation());
        }
        if (structure != null)
            structure = structure.localInstanceIn(edc());
        getAffectation().setToStructureRelationship(structure);    
    }
    
    @SuppressWarnings("unchecked")
    public String getRepartAssociationsAsStr() {
        String associations = null;
//        EORepartStructure repart = getRepartStructure();
        NSArray<EORepartAssociation> rpassos = getRepartAssociations();
        
        if (rpassos!=null && !rpassos.isEmpty()) {
        	NSArray<String> assocsStr = (NSArray<String>) rpassos.valueForKeyPath(ERXQ.keyPath(EORepartAssociation.TO_ASSOCIATION_KEY, EOAssociation.ASS_LIBELLE_KEY));
        	associations = assocsStr.componentsJoinedByString(", ");
		}
//        if (repart != null) {
//            NSArray<String> assocsStr = (NSArray<String>)repart.toRepartAssociations().valueForKeyPath(
//                    ERXQ.keyPath(EORepartAssociation.TO_ASSOCIATION_KEY, EOAssociation.ASS_LIBELLE_KEY));
//            associations = assocsStr.componentsJoinedByString(", ");
//        }
        return associations;
    }
    
    public EOQualifier notHebergeQualifier(){
    	EOQualifier qual = ERXQ.notEquals(EOAssociation.ASS_LIBELLE_KEY, EOAssociation.ASS_CODE_HEBERGE);
    	return qual;
    }
    
    private EOQualifier notHebergeQualifierRepartAsso(){
    	EOQualifier qual = ERXQ.notEquals(EORepartAssociation.TO_ASSOCIATION_KEY+"."+EOAssociation.ASS_LIBELLE_KEY, EOAssociation.ASS_CODE_HEBERGE);
    	return qual;
    }
    
    
    @SuppressWarnings("unchecked")
    public NSArray<EORepartAssociation> getRepartAssociations() {
        EORepartStructure repart = getRepartStructure();
        NSArray<EORepartAssociation> associations = null;
//        NSMutableArray<EORepartAssociation> assoTemp = new NSMutableArray<EORepartAssociation>();
        if (repart != null) {
        	EOQualifier qual = ERXQ.greaterThanOrEqualTo(EORepartAssociation.RAS_D_OUVERTURE_KEY, getAffectation().dDebAffectation())
        							.and(ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, getAffectation().dFinAffectation()).or(ERXQ.isNull(EORepartAssociation.RAS_D_FERMETURE_KEY)))
        							.and(notHebergeQualifierRepartAsso());
//        					   ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, getAffectation().dFinAffectation()));
//        	ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, getAffectation().dFinAffectation())).and(notHebergeQualifier());
        	associations = repart.toRepartAssociations(qual);
//            for (EORepartAssociation reponse : associations){
//            	if (!reponse.toAssociation().assLibelle().equals("HEBERGE")){
//            		assoTemp.addObject(reponse);
//            	}
//            }
        }
        return associations;
    }
    
    @SuppressWarnings("unchecked")
    private EORepartStructure getRepartStructure() {
    	EOQualifier qual = ERXQ.is(EORepartStructure.TO_STRUCTURE_GROUPE_KEY, getAffectation().toStructure() );
    	NSArray<EORepartStructure> reps = getAffectation().toIndividu().toRepartStructures(qual);
    	return reps.isEmpty() ? null : reps.lastObject();
    }
    
    
    public EOAssociation getSelectedAssociation() {
        return selectedAssociation;
    }
    
    public void setSelectedAssociation(EOAssociation selectedAssociation) {
        this.selectedAssociation = selectedAssociation;
    }
    
    public String getAssociationTreeViewDialogID() {
        if (associationTreeViewDialogID == null)
            associationTreeViewDialogID = ERXWOContext.safeIdentifierName(context(), true);
        return associationTreeViewDialogID;
    }
    
    public String getAssociationContainerID() {
        if (associationContainerID == null)
            associationContainerID = ERXWOContext.safeIdentifierName(context(), true);
        return associationContainerID;
    }
    
    public EORepartAssociation getCurrentRepartAssociation() {
        return currentRepartAssociation;
    }
    
    public void setCurrentRepartAssociation(EORepartAssociation currentRepartAssociation) {
        this.currentRepartAssociation = currentRepartAssociation;
    }
    
    public NSArray<EORepartAssociation> getSelectedsRepartAssociation() {
        return selectedsRepartAssociation;
    }
    
    public void setSelectedsRepartAssociation(NSArray<EORepartAssociation> selectedsRepartAssociation) {
        this.selectedsRepartAssociation = selectedsRepartAssociation;
    }
    
    public boolean noFonctionSelected() {
        return getSelectedsRepartAssociation() == null || getSelectedsRepartAssociation().isEmpty();
    }

	public NSArray<InfoCarriereContrat> getInfosCarriereContratListe() {
		if (infosCarriereContratListe==null) {
			genererInfoCarriereContrat(affectation);
		}
		return infosCarriereContratListe;
	}
    
	/**
	 * Création d'un intermédiare pour setter les dates de début et lancer les actions
	 */
	public void setDateDebut(NSTimestamp newDateDebut){
		if (hasBinding(BINDING_DATE_CONTROLE)){
			NSTimestamp dateControle = (NSTimestamp) valueForBinding(BINDING_DATE_CONTROLE);
			if (dateControle != null){
				if ( newDateDebut.before(dateControle) ){
					newDateDebut = dateControle;
					mySession().addSimpleErrorMessage("Date Affectation", "Ne pas saisir une date début antérieure au début du contrat");
				}
			}

		}
		getAffectation().setDDebAffectation(newDateDebut);
		recalculerInfosCarriere();
	}
	
	public NSTimestamp getDateDebut(){
		return getAffectation().dDebAffectation();
	}
	
	/**
	 * Création d'un intermédiare pour setter les date de fins et lancer les actions
	 */
	public void setDateFin(NSTimestamp newDateFin){
		getAffectation().setDFinAffectation(newDateFin);
		recalculerInfosCarriere();
	}
	
	public NSTimestamp getDateFin(){
		return getAffectation().dFinAffectation();
	}
	
}