/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrhguiajax.server.component;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlgrh.common.GRHApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSValidation;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;

/**
 * Composant de base pour l'édition d'objets métiers RH.
 * 
 * @binding editingContext un {@link EOEditingContext} surlequel un nested sera crée
 * @binding editing flag indiquant si l'on est en mode édition ou non
 * @binding individu {@link EOIndividu} individu pourlequel on veut éditer ses propriéts RH (affectations...)
 * @binding wantReset flag indiquant si l'on doit remettre à zéro l'état interne du composant. Utile lorsqu'on
 *                    reste toujours sur la meme page mais que l'on veut repartir de zéro.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class ACktlAjaxGRHComponent extends CktlAjaxWOComponent {

    private static final long serialVersionUID = 5598571736191721899L;
    protected static final Logger LOG = Logger.getLogger(ACktlAjaxGRHComponent.class);
    private static final String DEFAULT_FORMATTER = "%d/%m/%Y";
    public static final String BINDING_EDC = "editingContext";
    public static final String BINDING_EDITING = "editing";
    public static final String BINDING_INDIVIDU = "individu";
    public static final String BINDING_WANT_RESET = "wantReset";
    
    protected EOEditingContext editingContext;
    private String messageErreur;
    private String erreurContainerId;
    
    public ACktlAjaxGRHComponent(WOContext context) {
        super(context);
    }

    @Override
    public void sleep() {
        setMessageErreur(null);
        super.sleep();
    }
    
    public boolean eoHasChanged(EOEnterpriseObject cachedEo, EOEnterpriseObject newEo) {
        return !ERXEOControlUtilities.eoEquals(cachedEo, newEo);
    }
    
    @Override
    public EOEditingContext edc() {
        if (hasBinding(BINDING_EDC) && valueForBinding(BINDING_EDC) != null) {
            if (editingContext == null || wantReset()) {
                editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_EDC));
                // FIX du BUG No snapshot....
                editingContext.setRetainsRegisteredObjects(true);
                setWantReset(false);
            }
            return editingContext;
        } else {
            throw new IllegalArgumentException("le binding editingContext est obligatoire");
        }
    }
    
    public boolean saveChanges() {
        boolean success = false;
        try {
            beforeSaveChanges();
            edc().saveChanges();
            success = true;
        } catch (NSValidation.ValidationException e) {
            LOG.warn(e.getMessage(), e);
            setMessageErreur(e.getMessage());
        } catch (Exception e) {
            edc().revert();
            LOG.error(e.getMessage(), e);
            throw new NSForwardException(e);
        }
        return success;
    }
    
    protected void beforeSaveChanges() throws NSValidation.ValidationException {
        
    }
    
    public Boolean editing() {
        return booleanValueForBinding(BINDING_EDITING, Boolean.FALSE);
    }
    
    public void setEditing(Boolean value) {
        if (hasBinding(BINDING_EDITING) && canSetValueForBinding(BINDING_EDITING))
            setValueForBinding(value, BINDING_EDITING);
    }
    
    public String getMessageErreur() {
        return messageErreur;
    }
    
    public void setMessageErreur(String messageErreur) {
        this.messageErreur = messageErreur;
    }
    
    public String getDefaultFormatter() {
        return DEFAULT_FORMATTER;
    }
    
    public String getErreurContainerId() {
        if (erreurContainerId == null)
            erreurContainerId = ERXWOContext.safeIdentifierName(context(), true);
        return erreurContainerId;
    }
    
    public GRHApplicationUser getGrhApplicationUser() {
        GRHApplicationUser grhApplicationUser = GRHApplicationUser.retrieveFromSession(session());
        if (grhApplicationUser == null) {
        	LOG.error("L'utilisateur courant n'est pas enregistré pour pouvoir gérer les agents");
//            throw new IllegalStateException();
        }
        return grhApplicationUser;
    }
    
    public EOIndividu getIndividu() {
        EOIndividu indiv = (EOIndividu)valueForBinding(BINDING_INDIVIDU);
        if (indiv == null)
            throw new RuntimeException("Le binding " + BINDING_INDIVIDU + " est obligatoire");
        return indiv.localInstanceIn(edc());
    }
    
    public boolean wantReset() {
        return booleanValueForBinding(BINDING_WANT_RESET, false);
    }
    
    public void setWantReset(boolean value) {
        if (hasBinding(BINDING_WANT_RESET) && canSetValueForBinding(BINDING_WANT_RESET))
            setValueForBinding(false, BINDING_WANT_RESET);
    }
    
}
