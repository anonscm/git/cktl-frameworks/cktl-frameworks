/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrhguiajax.server.component;

import org.cocktail.fwkcktlgrh.common.metier.EOContratHeberge;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXWOContext;

/**
 * Composant d'édition d'un contrat hébergé.
 * 
 * @binding contrat un {@link EOContratHeberge} à éditer
 * @binding displayedContrats un NSArray de {@link EOContratHeberge} : correspondant aux
 *                            contrats hébergés déjà affichés, cela permet de faire des vérifications
 *                            en mémoire.
 * @binding didSave action callback appelée lorsqu'un contrat a été enregistré
 * @binding didCancel action callback appelée lorsque l'enregistrement d'un contrat a été annulé
 * 
 * @see ACktlAjaxGRHComponent
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class ContratHebergeForm extends ACktlAjaxGRHComponent {

    private static final long serialVersionUID = 2794324147623350670L;
    private static final String BINDING_CONTRAT = "contrat";
    private static final String BINDING_DISPLAYED_CONTRATS = "displayedContrats";
    private static final String DID_SAVE = "didSave";
    private static final String BINDING_DID_CANCEL = "didCancel";
    
    private EOContratHeberge contrat;
    private String containerId;
    
    public ContratHebergeForm(WOContext context) {
        super(context);
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    
    public EOContratHeberge getContrat() {
        EOContratHeberge contratParent = (EOContratHeberge)valueForBinding(BINDING_CONTRAT);
        if (contrat == null || wantReset() || eoHasChanged(contrat, contratParent)) {
            contrat = contratParent.localInstanceIn(edc());
        }
        return contrat;
    }
    
    @SuppressWarnings("unchecked")
    private NSArray<EOContratHeberge> getDisplayedContrats() {
        return (NSArray<EOContratHeberge>)valueForBinding(BINDING_DISPLAYED_CONTRATS);
    }
    
    public WOActionResults enregistrer() {
        boolean success = saveChanges();
        if (success) { 
            setEditing(false);
            if (hasBinding(DID_SAVE))
                return (WOActionResults)valueForBinding(DID_SAVE);
        } else {
            edc().revert();
        }
        return null;
    }
    
    @Override
    protected void beforeSaveChanges() throws ValidationException {
        super.beforeSaveChanges();
        EOContratHeberge.verifContratsHebergeInMemory(getContrat(), getDisplayedContrats());
    }
    
    public WOActionResults annuler() {
        edc().revert();
        setEditing(false);
        if (hasBinding(BINDING_DID_CANCEL))
            return (WOActionResults)valueForBinding(BINDING_DID_CANCEL);
        return null;
    }
    
    public WOActionResults editer() {
        setEditing(true);
        return null;
    }
    
    public String getContainerId() {
        if (containerId == null)
            containerId = ERXWOContext.safeIdentifierName(context(), true);
        return containerId;
    }

	public WOActionResults updateAction() {
		EOCorps corps = getContrat().toCorps();
		EOGrade grade = getContrat().toGrade();
    	if (corps!=null && grade !=null && !grade.toCorps().equals(corps)) {
    		getContrat().setToGradeRelationship(null);
		}
		return null;
	}
    
}