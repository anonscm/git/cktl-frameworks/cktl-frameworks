/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrhguiajax.server.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlgrh.common.metier.EOContratHeberge;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

/**
 * Composant de haut niveau de gestion des contrats hébergé.
 * 
 * @binding displayGroup "will set" un displayGroup pour afficher les contrats hébergés
 * @binding updateContainerIDonAjout id d'un AjaxUpdateContainer à rafraîchir après l'ajout d'un contrat 
 * @binding updateContainerIDonDelete id d'un AjaxUpdateContainer à rafraîchir après la suppression d'un contrat 
 * @binding didSave action callbakc appelée lorsqu'un contrat a été sauvegardé
 * 
 * @see ACktlAjaxGRHComponent
 * @see ContratHebergeForm
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class ContratsHebergeUI extends ACktlAjaxGRHComponent {
    
    private static final long serialVersionUID = -2881445416499174943L;
    private static final String DATE_DEB_KEY = EOContratHeberge.DATE_DEBUT_KEY;
    private static final String DATE_FIN_KEY = EOContratHeberge.DATE_FIN_KEY;
    private static final String ORG_RATT_KEY = ERXQ.keyPath(EOContratHeberge.TO_STRUCTURE_KEY, EOStructure.LL_STRUCTURE_KEY);
    private static final String UAI_KEY = ERXQ.keyPath(EOContratHeberge.TO_RNE_KEY, EORne.LL_RNE_KEY);
    private static final String CONTRAT_KEY = "currentContrat";
    private static final String BINDING_INDIVIDU = "individu";
    private static final String BINDING_DISPLAY_GROUP = "displayGroup";
    private static final String BINDING_UPDATE_CONTAINER_ID_ON_AJOUT = "updateContainerIDonAjout";
    private static final String BINDING_UPDATE_CONTAINER_ID_ON_DELETE = "updateContainerIDonDelete";
    private static final String DID_SAVE = "didSave";
    
    private String containerId;
    private String detailContainerId;
    private String contratDialogId;
    private EOContratHeberge currentContrat;
    private ERXDisplayGroup displayGroup;
    private EOIndividu individu;

    private NSArray<CktlAjaxTableViewColumn> colonnes;
    private boolean isFormEditing;
    private boolean isAjoutMode;
    private boolean resetForm;
    
    public ContratsHebergeUI(WOContext context) {
        super(context);
    }
    
    public WOActionResults ajouter() {
        EOContratHeberge nouveauContrat = EOContratHeberge.creerHeberger(edc(), getIndividu());
        nouveauContrat.setDateDebut(new NSTimestamp());
        nouveauContrat.setDCreation(new NSTimestamp());
        nouveauContrat.setDModification(new NSTimestamp());
        addObjectToDg(nouveauContrat);
        getDisplayGroup().selectObject(nouveauContrat);
        setFormEditing(true);
        isAjoutMode = true;
        // On veut repartir avec un formulaire clean (nouvel ec...)
        setResetForm(true);
        return null;
    }

    public WOActionResults enregistrer() {
        boolean success = saveChanges();
        if (success) {
            NSNotificationCenter.defaultCenter().postNotification(Notifications.CONTRATS_SAVED_NOTIF, session(), null);
            if (hasBinding(BINDING_UPDATE_CONTAINER_ID_ON_AJOUT)) {
                String updateContainerID = stringValueForBinding(BINDING_UPDATE_CONTAINER_ID_ON_AJOUT, null);
                if (updateContainerID != null)
                    AjaxUpdateContainer.updateContainerWithID(updateContainerID, context());
            }
            isAjoutMode = false;
            if (hasBinding(DID_SAVE))
                return (WOActionResults)valueForBinding(DID_SAVE);
        }
        return null;
    }
    
    public WOActionResults supprimer() {
        EOContratHeberge selected = (EOContratHeberge)getDisplayGroup().selectedObject();
        if (ERXEOControlUtilities.isNewObject(selected.toPersonnel())) {
            edc().deleteObject(selected.toPersonnel());
        }
        edc().deleteObject(selected);
        boolean success = saveChanges();
        if (success) deleteObjectFromDg(selected);
        if (hasBinding(BINDING_UPDATE_CONTAINER_ID_ON_DELETE)) {
            String updateContainerID = stringValueForBinding(BINDING_UPDATE_CONTAINER_ID_ON_DELETE, null);
            if (updateContainerID != null)
                AjaxUpdateContainer.updateContainerWithID(updateContainerID, context());
        }
        return null;
    }
    
    public WOActionResults annulerNouveau() {
        EOContratHeberge contrat = (EOContratHeberge)getDisplayGroup().selectedObject();
        if (isAjoutMode) {
            deleteObjectFromDg(contrat);
            getDisplayGroup().setSelectedObject(null);
        }
        isAjoutMode = false;
        return null;
    }
    
    public boolean noContratSelected() {
        return getDisplayGroup().selectedObject() == null;
    }
    
    public String getContainerId() {
        if (containerId == null)
            containerId = "Ctr_"
                    + ERXWOContext.safeIdentifierName(context(), true);
        return containerId;
    }
    
    public String getDetailContainerId() {
        if (detailContainerId == null)
            detailContainerId = "Ctr_"
                    + ERXWOContext.safeIdentifierName(context(), true);
        return detailContainerId;
    }
    
    public String getContratDialogId() {
        if (contratDialogId == null)
            contratDialogId = "CtrDiag_"
                    + ERXWOContext.safeIdentifierName(context(), true);
        return contratDialogId;
    }
    
    public ERXDisplayGroup getDisplayGroup() {
        if (displayGroup == null) {
            displayGroup = new ERXDisplayGroup();
            displayGroup.setDelegate(new DgDelegate());
            //Récupère les contrats hebergés courants de l'individu
            displayGroup.setObjectArray(EOContratHeberge.rechercherContratsPourIndividuEtPeriode(edc(), getIndividu(), DateCtrl.now(), null));
            setValueForBinding(displayGroup, BINDING_DISPLAY_GROUP);
        }
        return displayGroup;
    }
    
    public void addObjectToDg(Object obj) {
        getDisplayGroup().insertObjectAtIndex(obj, 0);
    }
    
    public void deleteObjectFromDg(Object obj) {
        int index = getDisplayGroup().allObjects().indexOf(obj);
        getDisplayGroup().deleteObjectAtIndex(index);
    }
    
    public EOIndividu getIndividu() {
        EOIndividu indivTmp = (EOIndividu) valueForBinding(BINDING_INDIVIDU);
        if (individu == null || eoHasChanged(individu, indivTmp))
            individu = indivTmp.localInstanceIn(edc());
        return individu;
    }
    
    public NSArray<CktlAjaxTableViewColumn> getColonnes() {
        if (colonnes == null) {
            NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();
            // Colonne Libelle
            CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
            col.setLibelle("Date début");
            col.setOrderKeyPath(DATE_DEB_KEY);
            String keyPath = ERXQ.keyPath(CONTRAT_KEY, DATE_DEB_KEY);
            CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            ass.setDateformat("%d/%m/%Y");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Date obtention
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Date fin");
            col.setOrderKeyPath(DATE_FIN_KEY);
            keyPath = ERXQ.keyPath(CONTRAT_KEY, DATE_FIN_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            ass.setDateformat("%d/%m/%Y");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Lieu
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Organisation de rattachement");
            col.setOrderKeyPath(ORG_RATT_KEY);
            keyPath = ERXQ.keyPath(CONTRAT_KEY, ORG_RATT_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Specialite
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("UAI");
            col.setOrderKeyPath(UAI_KEY);
            keyPath = ERXQ.keyPath(CONTRAT_KEY, UAI_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            colonnes = colTmp.immutableClone();
        }
        return colonnes;
    }
    
    public EOContratHeberge getCurrentContrat() {
        return currentContrat;
    }
    
    public void setCurrentContrat(EOContratHeberge currentContrat) {
        this.currentContrat = currentContrat;
    }
    
    public EOContratHeberge getSelectedContrat() {
        return (EOContratHeberge)getDisplayGroup().selectedObject();
    }
    
    public void setSelectedContrat(EOContratHeberge contrat) {
        getDisplayGroup().setSelectedObject(contrat);
    }
    
    public boolean showForm() {
        return getSelectedContrat() != null;
    }
    
    public boolean isFormEditing() {
        return isFormEditing;
    }
    
    public void setFormEditing(boolean isFormEditing) {
        this.isFormEditing = isFormEditing;
    }
    
    public boolean resetForm() {
        return resetForm;
    }

    public void setResetForm(boolean resetForm) {
        this.resetForm = resetForm;
    }

    protected class DgDelegate {

        public void displayGroupDidChangeSelection(WODisplayGroup dg) {
            setFormEditing(false);
        }
        
    }
    
    /**
     * Renvoie si le contrat hebergé est valide au niveau RH
     * @return vrai/faux
     */
    public boolean isContratValide() {
    	if (getSelectedContrat() != null) {
    		if (getSelectedContrat().temValide().equals("O")) {
    			return false;
    		}
    	}
    	return true;
    }
}