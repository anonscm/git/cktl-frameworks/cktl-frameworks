package org.cocktail.fwkcktlgrhguiajax.server.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXWOContext;

public class CktlAjaxServiceHebergeSelect extends AComponent {
	private static final long serialVersionUID = 8912445343486795731L;
	private static final String BINDING_SELECTION = "selection";
    private static final String BINDING_DID_SELECT_SERVICE = "didSelectService";
    
    private String containerId;
    private String modalWindowId;
    private EOQualifier serviceQualifier;

    public CktlAjaxServiceHebergeSelect(WOContext context) {
        super(context);
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    public WOActionResults valider() {
    	CktlAjaxWindow.close(context(), getModalWindowId());
    	return (WOActionResults)valueForBinding(BINDING_DID_SELECT_SERVICE);
    }
    
    public EOStructure getSelection() {
        return (EOStructure)valueForBinding(BINDING_SELECTION);
    }
    
    public void setSelection(EOStructure value) {
        setValueForBinding(value, BINDING_SELECTION);
    }
    
    public String getContainerId() {
        if (containerId == null)
            containerId = ERXWOContext.safeIdentifierName(context(), true)+"_cktlservicehebergeselect_container";
        return containerId;
    }
    
    public String getModalWindowId() {
        if (modalWindowId == null)
            modalWindowId = ERXWOContext.safeIdentifierName(context(), true)+"_cktlservicehebergeselect_modal";
        return modalWindowId;
    }
    
    public String getFormId() {
		return getComponentId()+"_ServiceHebergerSelectForm";
	}

	/**
	 * @return the serviceQualifier
	 */
	public EOQualifier getServiceQualifier() {
		if (serviceQualifier==null) {
			serviceQualifier=EOStructureForGroupeSpec.QUAL_GROUPES_SERVICES;
		}
		return serviceQualifier;
	}
	
	public WOActionResults closeWindow() {
		CktlAjaxWindow.close(context(), getModalWindowId());
		return null;
	}
    
}