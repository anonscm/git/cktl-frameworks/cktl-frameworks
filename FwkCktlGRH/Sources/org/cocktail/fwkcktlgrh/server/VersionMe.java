package org.cocktail.fwkcktlgrh.server;

public class VersionMe {
 // Nom de l'appli
    public static final String APPLICATIONFINALNAME = "FwkCktlGRH";
    public static final String APPLICATIONINTERNALNAME = "FwkCktlGRH";
    public static final String APPLICATION_STRID = "FWKCKTLGRH";

    // Version appli
    public static final long SERIALVERSIONUID = 1000;

    public static final int VERSIONNUMMAJ = 1;
    public static final int VERSIONNUMMIN = 0;
    public static final int VERSIONNUMPATCH = 0;
    public static final int VERSIONNUMBUILD = 1;

    public static final String VERSIONDATE = "04/12/2012";
    public static final String COMMENT = null;

    /***
     * Ne pas modifier cette methode, elle est utilisee pour recuperer le n° de version formate a partir d'une tache ant.
     */
    public static void main(String[] args) {
        System.out.println("" + VERSIONNUMMAJ + "." + VERSIONNUMMIN + "." + VERSIONNUMPATCH + "." + VERSIONNUMBUILD);
    }
}
