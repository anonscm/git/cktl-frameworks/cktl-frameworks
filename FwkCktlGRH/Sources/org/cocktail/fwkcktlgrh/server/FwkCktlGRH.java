package org.cocktail.fwkcktlgrh.server;

import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.ModuleRegister;

import er.extensions.ERXFrameworkPrincipal;

public class FwkCktlGRH extends ERXFrameworkPrincipal {

	public static final String MODEL_NAME = "FwkCktlGRH";
	
    static {
        setUpFrameworkPrincipalClass(FwkCktlGRH.class);
    }
	
	@Override
	public void finishInitialization() {
        ModuleRegister moduleRegister = CktlWebApplication.application().getModuleRegister();
        moduleRegister.addModule(new FwkCktlGRHModule());
	}

}
