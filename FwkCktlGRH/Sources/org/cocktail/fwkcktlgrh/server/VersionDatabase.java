package org.cocktail.fwkcktlgrh.server;

import org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleUser;

/**
 * Permet de controler les versions de votre user base de donnees. A adapter a
 * votre configuration.
 * 
 * @see org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleUser
 */

public class VersionDatabase extends CktlVersionOracleUser {
    private static String NAME = "BD USER MANGUE";
    private static String DB_USER_TABLE_NAME = "MANGUE.DB_VERSION";
    private static String DB_VERSION_DATE_COLUMN_NAME = "DB_VERSION_DATE";
    private static String DB_VERSION_ID_COLUMN_NAME = "DB_VERSION_LIBELLE";

    @Override
    public String dbUserTableName() {
        return DB_USER_TABLE_NAME;
    }

    @Override
    public String dbVersionDateColumnName() {
        return DB_VERSION_DATE_COLUMN_NAME;
    }

    @Override
    public String dbVersionIdColumnName() {
        return DB_VERSION_ID_COLUMN_NAME;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String sqlRestriction() {
        return "";
    }

    @Override
    public CktlVersionRequirements[] dependencies() {
        return null;
    }

}
