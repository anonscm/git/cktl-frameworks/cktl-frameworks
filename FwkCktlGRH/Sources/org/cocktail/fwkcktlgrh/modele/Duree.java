/*
 * Created on 14 avr. 2005
 *
 * Classe de base pour modéliser les durées
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.fwkcktlgrh.modele;

import java.util.Enumeration;

import org.cocktail.fwkcktlgrh.common.utilities.DateCtrl;
//import org.cocktail.mangue.modele.grhum.EOParamCongesAbsences;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
//import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EOMiTpsTherap;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Classe de base pour modeliser les durees correspondant a des conges ou des modalites de service.
 *
 */
// 30/06/2010  ajout d'une méthode indiquant si la durée est annulée
public abstract class Duree extends PeriodePourIndividu implements IDureePourIndividu  {
	private static EOGenericRecord parametresConges;

	public Duree() {
		super();
	}
	// interface avec durée
	/** a surcharger par les sous-classes pour indiquer le type absence associe a cette duree
	 */
	public abstract String typeEvenement();

	/** a surcharger par les sous-classes si elles gerent plusieurs types d'absences et si la duree doit
	 * etre ajoutee dans la table des absences */
	public boolean supportePlusieursTypesEvenement() {
		return false;
	}
	// Autres
	/** utilis&eacute; pour la communication avec GRhum */
	public String parametreOccupation() {
		return "";
	}
	
	
	/** Retourne false. Doit etre surchargee par les sous-classes ayant des arretes d'annulation */
	public boolean estAnnule() {
		return false;
	}

	// Méthodes statiques
	/** Calcule le nombre de jours ecoules entre deux dates
	 * @param date1
	 * @param ampmDebut	"am" si avant midi, "pm" sinon
	 * @param date2
	 * @param ampmFin "am" si avant midi, "pm" sinon
	 * @return nbJours ecoules entre deux dates
	 */
	public static float nbJoursEntre(NSTimestamp date1, String ampmDebut,NSTimestamp date2, String ampmFin) {
		if (date2 == null) {
			return 0;
		}
		float nbJours = DateCtrl.nbJoursEntre(date1,date2,true);
		if (ampmDebut == null || ampmDebut.length() == 0) {
			ampmDebut = "am";
		}
		if (ampmFin == null || ampmFin.length() == 0) {
			ampmFin = "pm";
		}
		if (ampmDebut.equals("am")) {
			if (ampmFin.equals("am")) {
				if (nbJours > 0) {
					nbJours -= 0.5;
				} else {
					nbJours = (float)0.5;
				}
			} else if (nbJours == 0) {
				nbJours = (float)1.0;
			}
		} else {
			if (ampmFin.equals("am")) {
				if (nbJours > 0) {
					nbJours -= 1.0;
				} else {
					nbJours = (float)1.0;
				}
			} else {
				if (nbJours > 0) {
					nbJours -= 0.5;
				} else {
					nbJours = (float)0.5;
				}
			}
		}
		return nbJours;
	}
	/** methode qui retourne les visas (textes de lois, arretes,…) qui doivent etre ajoutes lors de l'impression des arretes.<BR>
	Cette methode doit etre surchargee par toutes les sous-classes gerant des arretes */
	public NSArray visas() {
		return null;
	}

	/** calcule la dure de conges pour une personne
	 * @param ec editing context
	 * @param nomEntite nom entite dans le modele
	 * @param dateDebut date debut conge
	 * @param dateFin date fin conge
	 * @return retourne le nombre de jours ecoules entre la date debut et la date fin
	 */
	public static Number calculerCongesPourIndividuEtDates(EOEditingContext ec, String nomEntite,EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSMutableArray args = new NSMutableArray();

		args.addObject(individu);
		args.addObject(dateDebut);
		args.addObject(dateFin);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("individu = %@ and dateDebut <=%@ and dateFin >= %@",args);
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite, qual,null);
		NSArray conges = ec.objectsWithFetchSpecification(fs);

		int total = 0;
		for (int i = 0;i < conges.count();i++)
		{
			Duree conge = (Duree)conges.objectAtIndex(i);
			total += DateCtrl.nbJoursEntre(conge.dateDebut(),conge.dateFin(),true);	

		}
		return new Integer(total);
	}
	/** Recherche tous les conges dont le nom d'entite est dans le tableau typesConges
	 * @param editingContext
	 * @param typesConges types des conges recherches (fournir les noms d'entites du modele)
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode	 peut etre nulle
	 * @return conges trouves classes par ordre de date debut croissant
	 */
	public static NSArray rechercherCongesPourTypesIndividuEtPeriode(EOEditingContext editingContext,NSArray typesConges,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray resultat = new NSMutableArray();
		for (Enumeration e = typesConges.objectEnumerator();e.hasMoreElements();) {
			Object nomEntite = e.nextElement();
			if (nomEntite != NSKeyValueCoding.NullValue) {	// valeur retournée par eof quand null trouvé dans une table de la base
				NSArray conges = rechercherDureesPourIndividuEtPeriode(editingContext,(String)nomEntite,individu,debutPeriode,finPeriode);
				if (conges != null && conges.count() > 0) {
					resultat.addObjectsFromArray(conges);
				}
			}
		}
		EOSortOrdering.sortArrayUsingKeyOrderArray(resultat,new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareAscending)));
		return resultat;
	}

	/** retourne le nom de toute les entites provocant une suspension de cet evenement
	 * @param editingContext
	 * @param entiteEnCours entite pour laquelle on recherche les incompatibilites 
	 * @return un tableau contenant le nom des entites
	 */
	public static NSArray rechercherEntitesPourSuspensionEntiteEnCours(EOEditingContext edc,String entiteEnCours) {
		return rechercherEntitesPourTypeCompatibiliteEtEntiteEnCours(edc,"S",entiteEnCours);
	}
	/** retourne le nom de toute les entites provocant une interruption de cet evenement
	 *  @param editingContext
	 * @param entiteEnCours entite pour laquelle on recherche les incompatibilites
	 * @return un tableau contenant le nom des entites
	 */
	public static NSArray rechercherEntitesPourInterruptionEntiteEnCours(EOEditingContext edc,String entiteEnCours) {
		return rechercherEntitesPourTypeCompatibiliteEtEntiteEnCours(edc,"I",entiteEnCours);
	}

	/**
	 * 
	 * @param editingContext
	 * @param typeCompatibilite
	 * @param entiteEnCours
	 * @return
	 */
	private static NSArray rechercherEntitesPourTypeCompatibiliteEtEntiteEnCours(EOEditingContext editingContext,String typeCompatibilite,String entiteEnCours) {
		NSMutableArray args = new NSMutableArray(entiteEnCours);
		args.addObject(typeCompatibilite);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("entiteEnCoursEof = %@ AND temInterruption = %@",args);
		EOFetchSpecification myFetch = new EOFetchSpecification("CompatCgeMod",qualifier,null);
		return (NSArray)editingContext.objectsWithFetchSpecification(myFetch).valueForKey("entiteDebutantEof");
	}
}
