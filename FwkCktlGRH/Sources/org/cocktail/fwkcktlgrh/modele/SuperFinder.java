//
//  SuperFinder
//  ObjetsUniversite
//
//  Created by Christine Buttin on Fri Mar 11 2005.
//  Copyright (c) 2005 __MyCompanyName__. All rights reserved.
// contient toutes les méthodes de recherche sur des EOGenericRecord ou des méthodes générales
//
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.fwkcktlgrh.modele;
 


import org.cocktail.fwkcktlgrh.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

/**
 * @author christine<BR>
 *
 */
public class SuperFinder {

	/** Retourne le qualifier pour determiner les objets valides pour une periode
	 * @param champDateDebut	nom du champ de date debut sur lequel construire le qualifier
	 * @param debutPeriode	date de debut de periode (ne peut pas &ecirc;tre nulle)
	 * @param champDateFin	nom du champ de date fin sur lequel construire le qualifier
	 * @param finPeriode	date de fin de periode
	 * @return qualifier construit ou null si date debut est nulle
	 */
	public static EOQualifier qualifierPourPeriode(String champDateDebut,NSTimestamp debutPeriode,String champDateFin,NSTimestamp finPeriode) {

		try {

			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(champDateFin + " = nil OR " + champDateFin + " >=%@", new NSArray(debutPeriode)));
			
			if (finPeriode != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(champDateDebut + " <=%@", new NSArray(finPeriode)));

			return new EOAndQualifier(qualifiers);//EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);			
		}
		catch (Exception e) {
			return null;
		}		
	}
	
	/** retourne une date formatee sous forme de String : le format est jj/mm/aaaa
	 * @param record	record pour lequel on doit formatter une date
	 * @param cle cle du champ date
	 * @return date formatee
	 */
	public static String dateFormatee(EOGenericRecord record,String cle) {
		if (record.valueForKey(cle) == null) {
			return "";
		} else {	
			NSTimestampFormatter formatter=new NSTimestampFormatter("%d/%m/%Y");
			return formatter.format(record.valueForKey(cle));
		}
	}
	
	/** transforme une date fournie sous forme de string en une NSTimestamp. Si la date n'est pas complete (i.e jj/mm/aaaa), elle
	 * sera automatiquement completee
	 * @param record record pour lequel on doit fournir une date (NSTimestamp)
	 * @param cle cle du champ date
	 * @param uneDate valeur de la date (String)
	 */
	public static void setDateFormatee(EOGenericRecord record,String cle,String uneDate) {
		if (uneDate == null) {
			record.takeValueForKey(null,cle);
		}
		String myDate = DateCtrl.dateCompletion((String)uneDate);
		if (myDate.equals("")) {
	        record.takeValueForKey(null,cle);
	    } else {
	    		record.takeValueForKey(DateCtrl.stringToDate(myDate),cle);
	    } 
	}

}
