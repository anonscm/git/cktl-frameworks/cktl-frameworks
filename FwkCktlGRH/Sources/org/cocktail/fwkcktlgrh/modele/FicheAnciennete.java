//FicheAnciennete.java
//Created on Wed Sep 21 11:46:58 Europe/Paris 2005 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.fwkcktlgrh.modele;

//import org.cocktail.common.LogManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlgrh.common.utilities.DateCtrl;
import org.cocktail.fwkcktlgrh.common.utilities.Utilitaires.IntervalleTemps;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence;
import org.cocktail.fwkcktlgrh.common.metier.EOCarriere;
import org.cocktail.fwkcktlgrh.common.metier.EOChangementPosition;
import org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlgrh.common.metier.EOPasse;
import org.cocktail.fwkcktlgrh.common.metier.EOPeriodesMilitaires;
import org.cocktail.fwkcktlgrh.common.metier.EOTempsPartiel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/** Gere les donnees relatives a une periode d'anciennete<BR>
 * Anciennete de service : anciennete calcule a partir des dates<BR>
 * Anciennete effective : anciennete prenant en compte la quotite (de temps partiel, de changement de position, de contrat)<BR>
 * Prend en compte :
 * <UL> les contrats et leurs quotites en ce qui concerne l'anciennete effective</UL>
 * <UL> les segments de carriere et les changements de position</UL>
 * <UL> les temps partiels en ce qui concerne l'anciennete effective</UL>
 * <UL> le passe hors Education Nationale mais dans Fonction Publique</UL>
 * <UL> les periodes militaires</UL>
 * Dans le cas des LA-TA, les temps partiels et les CPA sont comptes a temps plein et les periodes de conge
 * parental sont comptees a 50%
 *
 */

public class FicheAnciennete extends Object implements NSKeyValueCoding {
	public static int TOUTE_ANCIENNETE = 0;
	public static int ANCIENNETE_AUXILIAIRE = 1;
	public static int ANCIENNETE_TITULAIRE = 2;
	public static int ANCIENNETE_VALIDES = 3;

	private String localisation;
	private String cPosition;
	private String position;
	private String categorie;
	private String typePopulation;
	private Number quotite;
	private String corps, grade;
	private NSTimestamp periodeDeb,periodeFin;
	private Number ancServAnnees,ancServMois,ancServJours;
	private Number ancGenAnnees,ancGenMois,ancGenJours;
	private boolean estTitulaire;
	private EOIndividu individu;
	
	public final static String SERVICES_VALIDES = "SVAL";
	public final static String PASSE_HORS_EN = "HEN";
	public final static String CDD = "CDD";
	public final static String CDI = "CDI";
	public final static String MAD = "MAD";
	public final static String MILITAIRE = "MIL";
	public final static String SERVICE_NATIONAL = "SNAT";

	public FicheAnciennete() {
		super();
	}

	// Acesseurs
	public Number quotite() {
		return quotite;
	}
	public void setQuotite(Number value) {
		quotite = value;
	}
	public String corps() {
		return corps;
	}
	public void setCorps(String corps) {
		this.corps = corps;
	}
	public String grade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String typePopulation() {
		return typePopulation;
	}
	public void setTypePopulation(String typePopulation) {
		this.typePopulation = typePopulation;
	}
	public boolean estTitulaire() {
		return estTitulaire;
	}
	public void setEstTitulaire(boolean aBool) {
		this.estTitulaire = aBool;
	}
	public Number ancServAnnees() {
		return ancServAnnees;
	}
	public void setAncServAnnees(Number value) {
		ancServAnnees = value;
	}
	public Number ancServMois() {
		return ancServMois;
	}
	public void setAncServMois(Number value) {
		ancServMois = value;
	}
	public Number ancServJours() {
		return ancServJours;
	}
	public void setAncServJours(Number value) {
		ancServJours = value;
	}
	public Number ancGenAnnees() {
		return ancGenAnnees;
	}
	public void setAncGenAnnees(Number value) {
		ancGenAnnees = value;
		if (value == null)
			setAncGenAnnees(new Integer(0));
	}
	public Number ancGenMois() {
		return ancGenMois;
	}
	public void setAncGenMois(Number value) {
		ancGenMois = value;
		if (value == null)
			setAncGenMois(new Integer(0));
	}
	public Number ancGenJours() {
		return ancGenJours;
	}
	public void setAncGenJours(Number value) {
		ancGenJours = value;
		if (value == null)
			setAncGenJours(new Integer(0));
	}
	public NSTimestamp periodeDeb() {
		return periodeDeb;
	}
	public void setPeriodeDeb(NSTimestamp value) {
		periodeDeb = value;
	}
	public NSTimestamp periodeFin() {
		return periodeFin;
	}
	public void setPeriodeFin(NSTimestamp value) {
		periodeFin = value;
	}
	public String localisation() {
		return localisation;
	}
	public void setLocalisation(String value) {
		localisation = value;
	}
	public String cPosition() {
		return cPosition;
	}
	public void setCPosition(String value) {
		cPosition = value;
	}
	/** Retourne le libell&eacute; court de la position */
	public String position() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String categorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public EOIndividu individu() {
		return individu;
	}
	public void setIndividu(EOIndividu value) {
		individu = value;
	}
	// méthodes ajoutées
	public void initAvecIndividu(EOIndividu individu) {
		this.individu = individu;
		setQuotite(new Integer(100));
	}

	// interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	public String toString() {
		String texte = "cPosition : " + cPosition();
		if (corps() != null) {
			texte += ", corps : " + corps();
		}
		if (grade() != null) {
			texte += ", grade : " + grade();
		}
		if (categorie() != null) {
			texte += ", catégorie de corps : " + categorie();
		}
		if (typePopulation() != null) {
			texte += ", type population : " + typePopulation();
		}
		if (ancGenAnnees() != null) {
			texte += ", ancGenAnnees : " + ancGenAnnees();
		}
		if (ancGenMois() != null) {
			texte += ", ancGenMois : " + ancGenMois();
		}
		if (ancGenJours() != null) {
			texte += ", ancGenJours : " + ancGenJours();
		}
		if (ancServAnnees() != null) {
			texte += ", ancServAnnees : " + ancServAnnees();
		}
		if (ancServMois() != null) {
			texte += ", ancServMois : " + ancServMois();
		}
		if (ancServJours() != null) {
			texte += ", ancServJours : " + ancServJours();
		}
		if (quotite() != null) {
			texte += ", quotite : " + quotite();
		}
		texte +=  ", periodeDeb : " + DateCtrl.dateToString(periodeDeb()) +  ", periodeFin : " + DateCtrl.dateToString(periodeFin());
		return texte;
	}
	public String caracteristiques() {
		String texte = "cPosition : " + cPosition();
		if (corps() != null) {
			texte += ", corps : " + corps();
		}
		if (grade() != null) {
			texte += ", grade : " + grade();
		}
		if (localisation() != null) {
			texte += ", localisation : " + localisation();
		}
		texte += ", quotite : " + quotite();
		return texte;
	}
	public static String formaterFichesAnciennetes(NSArray tableau) {

		String texte = "";
		for (java.util.Enumeration e = tableau.objectEnumerator();e.hasMoreElements();) {
			texte += (e.nextElement() + "\n");
		}
		return texte;
	}
	// méthodes statiques
	/** Calcule l'anciennete d'un individu &agrave; la date de reference en regroupant sur les corps, les positions et
	 * la contiguite des dates
	 */
	public static NSArray calculerAncienneteAvecSyntheseMaximum(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateReference) {
		NSArray fichesAnciennete = calculerAnciennete(editingContext, individu, dateReference, true);
		return synthese(fichesAnciennete,true);
	}
	public static NSArray calculerAnciennete(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateReference) {
		return calculerAnciennete(editingContext, individu, dateReference,true);
	}
	/** calcule l'anciennete d'un individu a la date de reference
	 * 
	 * @param editingContext
	 * @param individu
	 * @param dateReference
	 * @param estAncienneteComplete true si on veut tout le detail d'anciennete
	 * @return
	 */
	public static NSArray calculerAnciennete(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateReference,boolean estAncienneteComplete) {
		return calculerAnciennete(editingContext, individu, dateReference,estAncienneteComplete,false);
	}
	/** calcule l'anciennete d'un individu a la date de reference
	 * 
	 * @param editingContext
	 * @param individu
	 * @param dateReference
	 * @param estAncienneteComplete true si on veut tout le detail d'anciennete
	 * @param estPromotion true si il s'agit de la promotion des LA-TA (en quel cas on ne prend pas en compte la quotite de temps partiel
	 * @return
	 */
	public static NSArray<FicheAnciennete> calculerAnciennete(EOEditingContext editingContext,EOIndividu individu,
			NSTimestamp dateReference,
			boolean estAncienneteComplete,boolean estPromotion) {

		try {
			
			NSMutableArray<FicheAnciennete>	arrayAnciennete = new NSMutableArray<FicheAnciennete>();

			arrayAnciennete.addObjectsFromArray(getArrayCarrieres(editingContext,individu,dateReference,estPromotion));
			arrayAnciennete.addObjectsFromArray(getArrayContratsAvenants(editingContext,individu,dateReference, estPromotion));
			arrayAnciennete.addObjectsFromArray(getArrayPasse(editingContext,individu,dateReference));
			arrayAnciennete.addObjectsFromArray(getArrayPeriodesMilitaires(editingContext,individu,dateReference));

			EOSortOrdering.sortArrayUsingKeyOrderArray(arrayAnciennete,new NSArray(EOSortOrdering.sortOrderingWithKey("periodeDeb",EOSortOrdering.CompareDescending)));

			// On parcourt chaque fiche et on enregistre les anciennetes
			for (FicheAnciennete fiche : arrayAnciennete) {
				calculerAnneesMoisJoursAnciennete(fiche,estPromotion);
			}

			if (estAncienneteComplete) {
				return arrayAnciennete;
			} else {
				if (arrayAnciennete.count() > 0) {
					// retourner une vue synthétique en regroupant toutes les activités avec des dates consécutives
					return synthese(arrayAnciennete,false);
				} else {
					return arrayAnciennete;
				}
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}
	}
	/** retourne le calcul d'anciennete total */
	public static String calculerTotal(NSArray<FicheAnciennete> fiches,int type) {
		int annees= 0, mois = 0;
		int jours = 0;	
		
		for (FicheAnciennete fiche : fiches) {
		
			if (fiche.cPosition() != null) {
				
				boolean estContractuel = fiche.cPosition().equals(CDD) || fiche.cPosition().equals(CDI);

				boolean estMilitaire = fiche.cPosition().equals(MILITAIRE) || fiche.cPosition().equals(SERVICE_NATIONAL);

				if (type == TOUTE_ANCIENNETE ||
						(type == ANCIENNETE_AUXILIAIRE && (estContractuel || estMilitaire)) ||
						(type == ANCIENNETE_TITULAIRE && !estContractuel && !estMilitaire && fiche.cPosition().equals(SERVICES_VALIDES) == false)) {
					// les valeurs ancServJours, ancServMois et ancServAnnees issues de displayGroup() sont affectées de la quotite
					jours = jours +  fiche.ancServJours().intValue();
					mois = mois +  fiche.ancServMois().intValue();
					annees = annees + fiche.ancServAnnees().intValue();
				}
				
				if (type == ANCIENNETE_AUXILIAIRE && fiche.cPosition().equals(PASSE_HORS_EN)) {
					if (fiche.ancServAnnees().intValue() == 0 && fiche.ancServMois().intValue() == 0 && fiche.ancServJours().intValue() == 0) {
						jours = jours +  fiche.ancGenJours().intValue();
						mois = mois +  fiche.ancGenMois().intValue();
						annees = annees + fiche.ancGenAnnees().intValue();
					}
				}
				if (type == ANCIENNETE_VALIDES &&  fiche.cPosition().equals(SERVICES_VALIDES)) {
					jours = jours +  fiche.ancGenJours().intValue();
					mois = mois +  fiche.ancGenMois().intValue();
					annees = annees + fiche.ancGenAnnees().intValue();
				}
			}
		}
		return dureeToString(annees, mois, jours);
	}

	/** calcule l'anciennete generale et retourne le resultat sous la forme d'une string en annees - mois - jours */
	public static String ancienneteGenerale(NSArray<FicheAnciennete> fiches) {
		int annees= 0, mois = 0,jours = 0;

		NSTimestamp dateDebut = null, dateFin = null;

		for (FicheAnciennete fiche : fiches) {
			if (fiche.cPosition() != null) {
				if ((dateDebut == null && dateFin == null) || IntervalleTemps.intersectionPeriodes(dateDebut, dateFin, fiche.periodeDeb(), fiche.periodeFin()) == null) {
					jours = jours +  fiche.ancGenJours().intValue();
					mois = mois +  fiche.ancGenMois().intValue();
					annees = annees + fiche.ancGenAnnees().intValue();
				}
				dateDebut = fiche.periodeDeb();
				dateFin = fiche.periodeFin();
			}
		}
		return dureeToString(annees, mois, jours);

	}
	

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param dateReference
	 * @param estPromotion
	 * @return
	 */
	private static NSArray<FicheAnciennete> getArrayCarrieres(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateReference,boolean estPromotion) {
		NSMutableArray<FicheAnciennete> resultArray = new NSMutableArray<FicheAnciennete>();
		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresPourIndividuAnterieursADate(editingContext,individu,dateReference);
		// Les trier par ordre de date croissant
		carrieres = EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieres, EOCarriere.SORT_ARRAY_DATE_DEBUT_ASC);
		for (EOCarriere segment : carrieres) {

			NSTimestamp dateFin = dateReference;
			if (segment.dFinCarriere() != null && DateCtrl.isBefore(segment.dFinCarriere(),dateReference)) {
				dateFin = segment.dFinCarriere();
			}
			// on récupère les changements de position sur la période par ordre croissant
			NSArray<EOChangementPosition> changementsPosition = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(editingContext, individu, segment.dDebCarriere(), dateFin);

			for (EOChangementPosition positionCourante : changementsPosition) {

				NSTimestamp dateDebutPourTempsPartiel = segment.dDebCarriere();
				NSTimestamp dateFinPourTempsPartiel = dateFin;
				if (DateCtrl.isBefore(segment.dDebCarriere(), positionCourante.dDebPosition())) {
					dateDebutPourTempsPartiel = positionCourante.dDebPosition();
				} 
				if (positionCourante.dFinPosition() != null && DateCtrl.isBefore(positionCourante.dFinPosition(),dateFin)) {
					dateFinPourTempsPartiel = positionCourante.dFinPosition();
				}
				
				// vérifier si il n'y a pas de temps partiel pendant cette période pour les quotités
				NSArray<EOTempsPartiel> tempsPartiels = Duree.rechercherDureesPourIndividuEtPeriode(editingContext,EOTempsPartiel.ENTITY_NAME,individu,dateDebutPourTempsPartiel,dateFinPourTempsPartiel);

				if (tempsPartiels.count() > 0 && estPromotion == false) {
					// trier par ordre de date croissant
					tempsPartiels = EOSortOrdering.sortedArrayUsingKeyOrderArray(tempsPartiels, EOTempsPartiel.SORT_ARRAY_DATE_DEBUT_ASC);
					NSTimestamp debutPeriode = dateDebutPourTempsPartiel;

					for (EOTempsPartiel tempsPartiel : tempsPartiels) {

						if (DateCtrl.isBefore(debutPeriode,tempsPartiel.dateDebut())) {
							// il y a une période à temps plein avant avec comme quotité effective, la quotité de position
							/*FicheAnciennete fiche = creerFichePourIndividu(individu,debutPeriode,DateCtrl.jourPrecedent(tempsPartiel.dateDebut()),positionCourante);*/
							//LogManager.logDetail(">>> fiches temps plein avant temps partiel");
							NSArray fiches = creerFichesPourCarrierePeriodePositionEtQuotite(segment,debutPeriode,DateCtrl.jourPrecedent(tempsPartiel.dateDebut()),positionCourante, new Integer(100),estPromotion);
							resultArray.addObjectsFromArray(fiches);
							debutPeriode = tempsPartiel.dateDebut();
						}
						NSTimestamp finPeriode = dateFinPourTempsPartiel;
						// Si le temps partiel se termine avant, prendre comme date de fin de période, la date de fin du temps partiel
						if (tempsPartiel.dateFin() != null && DateCtrl.isBefore(tempsPartiel.dateFin(),dateFinPourTempsPartiel)) {
							finPeriode = tempsPartiel.dateFin();
						}
						// Prendre en compte les interruptions de temps partiel pour le calcul de service effectif
						if (tempsPartiel.dFinExecution() != null && DateCtrl.isBefore(tempsPartiel.dFinExecution(), finPeriode)) {
							finPeriode = tempsPartiel.dFinExecution();
						}
						// temps partiel sur toute la période
						//LogManager.logDetail(">>> fiches temps partiel");
						NSArray fiches = creerFichesPourCarrierePeriodePositionEtQuotite(segment,debutPeriode,finPeriode,positionCourante,tempsPartiel.numQuotite(),estPromotion);
						resultArray.addObjectsFromArray(fiches);
						/*FicheAnciennete fiche = creerFichePourIndividu(individu,debutPeriode,finPeriode,positionCourante);
						fiche.setQuotite(tempsPartiel.numQuotite());*/
						// Si le temps partiel se termine après la date de fin en cours, on en a fini avec les temps partiels
						if (finPeriode == null || DateCtrl.isAfter(finPeriode,dateFinPourTempsPartiel)) {
							debutPeriode = null;
							break;	// on a passé la date de fin
						} else {
							debutPeriode = DateCtrl.jourSuivant(finPeriode);
						}
					}
					if (debutPeriode != null && DateCtrl.isBefore(debutPeriode, dateFinPourTempsPartiel)) {
						//LogManager.logDetail(">>> fiches temps plein après temps partiel");
						// il reste encore une période de temps plein puisqu'on n'a pas dépassé la date de fin
						NSArray fiches = creerFichesPourCarrierePeriodePositionEtQuotite(segment,debutPeriode,dateFinPourTempsPartiel,positionCourante, new Integer(100),estPromotion);
						resultArray.addObjectsFromArray(fiches);
						/*		FicheAnciennete fiche = creerFichePourIndividu(individu,debutPeriode,dateFinPourTempsPartiel,positionCourante);*/
					}
				} else {
					// pas de temps partiel, prendre toute la période du changement de position en compte
					//LogManager.logDetail(">>> fiches temps plein sans temps partiel ou sans prise en compte des temps partiels");
					NSArray fiches = creerFichesPourCarrierePeriodePositionEtQuotite(segment,dateDebutPourTempsPartiel,dateFinPourTempsPartiel,positionCourante, new Integer(100),estPromotion);
					resultArray.addObjectsFromArray(fiches);
					/*FicheAnciennete fiche = creerFichePourIndividu(individu,dateDebutPourTempsPartiel,dateFinPourTempsPartiel,positionCourante);*/					
				}
			}
		}

		return 	resultArray.immutableClone();
		
	}


	// GET ARRAY CONTRATS : renvoie un array contenant les donnees des contrats a prendre en consideration
	private static NSArray<FicheAnciennete> getArrayContratsAvenants(EOEditingContext edc,EOIndividu individu,NSTimestamp dateReference, boolean estPromotion) {
		NSMutableArray<FicheAnciennete> resultArray = new NSMutableArray<FicheAnciennete>();
		NSArray<EOContratAvenant> contrats = EOContratAvenant.rechercherAvenantsRemunerationPrincipaleAnterieursADate(edc,individu,dateReference);
		for (EOContratAvenant avenant : contrats) {

			if (avenant.toContrat().toTypeContratTravail().estServicePublic()) {
				
				// Si on a une notion de SERVICE VALIDE on continue - La periode sera déclarée dans PASSE.
				EOPasse passe = EOPasse.rechercherPassePourAvenant(edc, avenant);
				if (passe != null)
					continue;
				
				// L'avenant doit etre valide
				if (avenant.estAnnule() == false && DateCtrl.isBefore(avenant.dDebAvenant(), dateReference) ) {
					FicheAnciennete fiche = new FicheAnciennete();
					fiche.initAvecIndividu(individu);
					fiche.setPeriodeDeb(avenant.dDebAvenant());
					// 28/09/09 - prise en compte de la date de fin anticipée du contrat
					NSTimestamp dateFin = avenant.dFinAvenant();
					if (avenant.toContrat().dFinAnticipee() != null && (dateFin == null 
							|| (dateFin != null && avenant.toContrat().dFinAnticipee() != null 
									&& DateCtrl.isBefore(avenant.toContrat().dFinAnticipee(), dateFin)))) {
						dateFin = avenant.toContrat().dFinAnticipee();
					}
					if (dateFin != null) {
						if (DateCtrl.isBefore(dateFin,dateReference)) {
							fiche.setPeriodeFin(dateFin);
						} else {
							fiche.setPeriodeFin(dateReference);
						}
					} else {
						fiche.setPeriodeFin(dateReference);
					}
					if (avenant.numQuotRecrutement() != null) {
						// Dans le cas des promotions, on ne prend la quotité de recrutement pour les temps incomplets
						// que si elle est inférieure à 50%
						double quotite = avenant.numQuotRecrutement().doubleValue();
						if (estPromotion && quotite >= 50.00) {
							quotite = 100.00;
						}
						fiche.setQuotite(new Double(quotite));
					}
					if (avenant.toGrade() != null) {
						fiche.setGrade(avenant.toGrade().llGrade());
					}
					if (avenant.fonctionCtrAvenant() != null) {
						fiche.setCorps(avenant.fonctionCtrAvenant());
					}
					if (avenant.toCategorie() != null) {
						fiche.setCategorie(avenant.toCategorie().cCategorie());
					}
					// 29/03/2010 - gestion des cdis
					if (avenant.toContrat().toTypeContratTravail().estCdi()) {
						fiche.setCPosition(CDI);
						fiche.setPosition(CDI);
					} else {
						fiche.setCPosition(CDD);
						fiche.setPosition(CDD);
					}
					if (avenant.toContrat().toRne() != null) {
						fiche.setLocalisation(avenant.toContrat().toRne().llRne());
					}
					resultArray.addObject(fiche);
				}
			}
		}

		return resultArray.immutableClone();
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param dateReference
	 * @return
	 */
	private static NSArray<FicheAnciennete> getArrayPasse(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateReference) {
		
		NSMutableArray<FicheAnciennete> resultArray = new NSMutableArray();
		
		NSArray<EOPasse> passesHorsEN = Duree.rechercherDureesPourIndividuAnterieuresADate(editingContext,EOPasse.ENTITY_NAME,individu,dateReference);
		for (EOPasse passeHorsEN : passesHorsEN) {

			if (passeHorsEN.estSecteurPublic()) {// && passeHorsEN.contratAvenant() == null) {
				
				FicheAnciennete fiche = new FicheAnciennete();
				fiche.initAvecIndividu(individu);
				fiche.setPeriodeDeb(passeHorsEN.dateDebut());
				if (DateCtrl.isBefore(passeHorsEN.dateFin(),dateReference)) {
					fiche.setPeriodeFin(passeHorsEN.dateFin());
				} else {
					fiche.setPeriodeFin(dateReference);
				}

				if (passeHorsEN.pasQuotiteCotisation() != null) {
					fiche.setQuotite(new Integer(passeHorsEN.pasQuotiteCotisation().intValue()));
				} else {
					fiche.setQuotite(new Integer (100));
				}

				fiche.setEstTitulaire(passeHorsEN.estTitulaire());
				
				if (passeHorsEN.estTypeServiceValide()) {
					fiche.setCPosition(SERVICES_VALIDES);				
					fiche.setPosition("SVAL");
				}
				else {
					fiche.setCPosition(PASSE_HORS_EN);
					if (passeHorsEN.estTypeServiceEAS()) {
						fiche.setCPosition("EAS");
						fiche.setPosition("EAS");
					}
					else
						if (passeHorsEN.estTypeServiceEngage())
								fiche.setCPosition("Engagé");
							else
								fiche.setPosition("Passé");
				}

				
				if (passeHorsEN.toCategorie() != null) {
					fiche.setCategorie(passeHorsEN.toCategorie().cCategorie());
				}
				if (passeHorsEN.toTypePopulation() != null) {
					fiche.setTypePopulation(passeHorsEN.toTypePopulation().cTypePopulation());
				}
				
				if (passeHorsEN.etablissementPasse() != null)
					fiche.setLocalisation(passeHorsEN.etablissementPasse());
				else
					fiche.setLocalisation(passeHorsEN.pasMinistere());

				fiche.setAncGenAnnees(passeHorsEN.dureeValideeAnnees());
				fiche.setAncGenMois(passeHorsEN.dureeValideeMois());
				fiche.setAncGenJours(passeHorsEN.dureeValideeJours());
				fiche.setAncServAnnees(passeHorsEN.dureeValideeAnnees());
				fiche.setAncServMois(passeHorsEN.dureeValideeMois());
				fiche.setAncServJours(passeHorsEN.dureeValideeJours());

				resultArray.addObject(fiche);
			}
		}
		
		return 	resultArray.immutableClone();
		
	}
	

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param dateReference
	 * @return
	 */
	private static NSArray<FicheAnciennete> getArrayPeriodesMilitaires(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateReference) {
		NSMutableArray resultArray = new NSMutableArray();
		NSArray<EOPeriodesMilitaires> mads = EOPeriodesMilitaires.rechercherDureesPourIndividuAnterieuresADate(editingContext,"Fwkgrh_PeriodesMilitaires",individu,dateReference);
		for (EOPeriodesMilitaires periode : mads) {
			if (EOChangementPosition.individuAuServiceMilitairePourPeriode(editingContext, individu, periode.dateDebut(), periode.dateFin()) == false) {
				FicheAnciennete fiche = new FicheAnciennete();
				fiche.initAvecIndividu(individu);
				fiche.setPeriodeDeb(periode.dateDebut());
				if (DateCtrl.isBefore(periode.dateFin(),dateReference)) {
					fiche.setPeriodeFin(periode.dateFin());
				} else {
					fiche.setPeriodeFin(dateReference);
				}
				fiche.setLocalisation((String)periode.toTypePeriodeMilit().valueForKey("lcTypePeriodMilit"));
				fiche.setCPosition(MILITAIRE);
				fiche.setPosition("Svc Militaire");
				resultArray.addObject(fiche);
			}
		}
		//LogManager.logDetail("Resultat du calcul d'anciennete periodes militaires\n" + formaterFichesAnciennetes(resultArray));
		return 	(NSArray)resultArray;
	}

	//	CALCULER ANNEES MOIS JOURS ANCIENNETE  : calcul le nbre d'annees, mois, jours en fonction de la position
	// général et en service (prend en compte pour ce dernier calcul la quotité)
	private static void calculerAnneesMoisJoursAnciennete(FicheAnciennete fiche,boolean estPromotion)  {
		//System.out.println("fiche avant " + fiche);
		// Pas de calcul d'ancienneté pour les changements de position suivants :
		// DISPonibilte ,  HCAD (Hors cadre), CGST (conge stagiaire)
		// Le congé parental est pris en compte dans le cas de l'ancienneté LA-TA
		if (fiche.cPosition() != null) {
			if ("DISP".equals(fiche.cPosition()) || 
					"HCAD".equals(fiche.cPosition()) || 
					"CGST".equals(fiche.cPosition()) 
					|| (!estPromotion && EOTypeAbsence.TYPE_CONGE_PARENTAL.equals(fiche.cPosition()))) {
				fiche.setAncServAnnees(new Integer(0));
				fiche.setAncServMois(new Integer(0));
				fiche.setAncServJours(new Integer(0));
				fiche.setAncGenAnnees(new Integer(0));
				fiche.setAncGenMois(new Integer(0));
				fiche.setAncGenJours(new Integer(0));
			} else {
				
				if (fiche.cPosition().equals(PASSE_HORS_EN) == false && fiche.cPosition().equals(SERVICES_VALIDES) == false) {
					
					DateCtrl.IntRef anneesRef = new DateCtrl.IntRef(),moisRef = new DateCtrl.IntRef(),joursRef = new DateCtrl.IntRef();
					// calculer le nombre d'années, mois jours en incluant les bornes et en durée comptable
					DateCtrl.joursMoisAnneesEntre(fiche.periodeDeb(),fiche.periodeFin(),anneesRef,moisRef,joursRef,true,true); 
					int annees = anneesRef.value, mois = moisRef.value, jours = joursRef.value;
					fiche.setAncGenAnnees(new Integer(annees));
					fiche.setAncGenMois(new Integer(mois));
					fiche.setAncGenJours(new Integer(jours));
					if (estPromotion) {	// 09/09/09 on prend la durée réelle dans le cas des LA-TA
						fiche.setAncServAnnees(new Integer(annees));
						fiche.setAncServMois(new Integer(mois));
						fiche.setAncServJours(new Integer(jours));
					} else {
						// 23/07/09 }
						// Calculer la quotité effective de service
						float quotite = 1;
						if (fiche.quotite() != null) {
							quotite = fiche.quotite().floatValue() / 100;
						}
						// ANNEES
						float anneesQuotite = new Float(fiche.ancGenAnnees().floatValue() * quotite).floatValue();
						float moisSupp = (anneesQuotite - (int)anneesQuotite) * 12;

						// MOIS
						float moisQuotite = new Float(fiche.ancGenMois().floatValue() * quotite).floatValue();
						moisQuotite = moisQuotite + moisSupp;
						float joursSupp = (moisQuotite - (int)moisQuotite) * 30;

						// JOURS
						float joursQuotite = new Float(fiche.ancGenJours().floatValue() * quotite).floatValue();
						joursQuotite = joursQuotite + joursSupp;

						// Conversion finale en année (de 12 mois), mois (de 30 jours), jours
						if (fiche.cPosition().equals(PASSE_HORS_EN) == false || fiche.estTitulaire()) {
							fiche.setAncServJours(new Integer((int)joursQuotite % 30));
							fiche.setAncServMois(new Integer(((int)moisQuotite + ((int)joursQuotite / 30)) % 12));
							fiche.setAncServAnnees(new Integer((int)anneesQuotite + (((int)moisQuotite + ((int)joursQuotite / 30)) / 12)));
						} else {
							fiche.setAncServAnnees(new Integer(0));
							fiche.setAncServMois(new Integer(0));
							fiche.setAncServJours(new Integer(0));
						}
					}
				}
				else { // CAS DU PASSE

					calculerAnneesMoisJoursAnciennetePasse(fiche);

				}
			}
		}

	}

	/**
	 * 
	 * @param fiche
	 * @param estPromotion
	 * 
	 */
	private static void calculerAnneesMoisJoursAnciennetePasse(FicheAnciennete fiche)  {

		DateCtrl.IntRef anneesRef = new DateCtrl.IntRef(),moisRef = new DateCtrl.IntRef(),joursRef = new DateCtrl.IntRef();

		if ( (fiche.ancGenAnnees() == null || fiche.ancGenAnnees().intValue() == 0)
				&& (fiche.ancGenMois() == null || fiche.ancGenMois().intValue() == 0 )
				&& (fiche.ancGenJours() == null || fiche.ancGenJours().intValue() == 0) ) {

			DateCtrl.joursMoisAnneesEntre(fiche.periodeDeb(),fiche.periodeFin(),anneesRef,moisRef,joursRef,true,true); 
			int annees = anneesRef.value, mois = moisRef.value, jours = joursRef.value;
			fiche.setAncGenAnnees(new Integer(annees));
			fiche.setAncGenMois(new Integer(mois));
			fiche.setAncGenJours(new Integer(jours));

			if (fiche.estTitulaire() == false) {

				fiche.setAncServAnnees(new Integer(0));
				fiche.setAncServMois(new Integer(0));
				fiche.setAncServJours(new Integer(0));

			}
			else {
				
				float quotite = 1;
				if (fiche.quotite() != null) {
					quotite = fiche.quotite().floatValue() / 100;
				}			// ANNEES
				float anneesQuotite = new Float(fiche.ancGenAnnees().floatValue() * quotite).floatValue();
				float moisSupp = (anneesQuotite - (int)anneesQuotite) * 12;
				// MOIS
				float moisQuotite = new Float(fiche.ancGenMois().floatValue() * quotite).floatValue();
				moisQuotite = moisQuotite + moisSupp;
				float joursSupp = (moisQuotite - (int)moisQuotite) * 30;
				// JOURS
				float joursQuotite = new Float(fiche.ancGenJours().floatValue() * quotite).floatValue();
				joursQuotite = joursQuotite + joursSupp;
				fiche.setAncServJours(new Integer((int)joursQuotite % 30));
				fiche.setAncServMois(new Integer(((int)moisQuotite + ((int)joursQuotite / 30)) % 12));
				fiche.setAncServAnnees(new Integer((int)anneesQuotite + (((int)moisQuotite + ((int)joursQuotite / 30)) / 12)));
				
			}
		}

		if (fiche.ancGenAnnees() == null)
			fiche.setAncGenAnnees(new Integer(0));
		if (fiche.ancGenMois() == null)
			fiche.setAncGenMois(new Integer(0));
		if (fiche.ancGenJours() == null)
			fiche.setAncGenJours(new Integer(0));
		if (fiche.ancServAnnees() == null)
			fiche.setAncServAnnees(new Integer(0));
		if (fiche.ancServMois() == null)
			fiche.setAncServMois(new Integer(0));
		if (fiche.ancServJours() == null)
			fiche.setAncServJours(new Integer(0));
	}

	/** 
	 * @param annees
	 * @param mois
	 * @param jours
	 * @return 	
	 */
	private static String dureeToString(int annees, int mois, int jours) {

		String chaineAnnees = "", chaineMois = "", chaineJours = "";
		
		int nbJours, nbMois,nbAnnees;

		nbJours = jours % 30; 		
		nbMois = (mois + (jours / 30)) % 12;
		nbAnnees = annees + ((mois + (jours / 30)) / 12);

		if (nbAnnees > 1)
			chaineAnnees = nbAnnees + " ans ";
		else
			if (nbAnnees == 1)
				chaineAnnees = "1 an ";
		
		if (nbMois > 1)
			chaineMois = nbMois + " mois ";
		else
			if (nbMois == 1)
				chaineMois = "1 mois ";
		
		if (nbJours > 1)
			chaineJours = nbJours + " jours";
		else
			if (nbJours == 1)
				chaineJours = "1 jour";
			else
				if (nbJours == 0)
					chaineJours = "0 Jours";
		
		return chaineAnnees + chaineMois + chaineJours;
	}

	
	/**
	 * 
	 * @param segment
	 * @param dateDebut
	 * @param dateFin
	 * @param position
	 * @param quotite
	 * @param estPromotion
	 * @return
	 */
	private static NSArray<FicheAnciennete> creerFichesPourCarrierePeriodePositionEtQuotite(EOCarriere segment,NSTimestamp dateDebut,NSTimestamp dateFin,EOChangementPosition position,Number quotite, boolean estPromotion) {
		
		NSMutableArray<FicheAnciennete> fiches = new NSMutableArray<FicheAnciennete>();

		NSArray<EOElements> elementsCarriere = segment.elementsPourPeriode(dateDebut, dateFin); // ils sont triés par date croissante
		NSTimestamp debutPeriode = null, finPeriode = null;
		
		if (elementsCarriere == null) {
			return fiches;
		}
		for (EOElements element : elementsCarriere) {

			if (DateCtrl.isAfter(element.dEffetElement(),dateDebut)) {
				debutPeriode = element.dEffetElement();
			} else {
				debutPeriode = dateDebut;
			}
			if (element.dFinElement() != null && DateCtrl.isBefore(element.dFinElement(), dateFin)) {
				finPeriode = element.dFinElement(); 
			} else {
				finPeriode = dateFin;
			}
			// Créer une fiche avec le corps et le grade de l'élément
			FicheAnciennete fiche = creerFichePourIndividu(segment.toIndividu(),debutPeriode,finPeriode,position);
			// dans le cas des promotions, on prend comme quotité la quotité d'avancement * la quotité du changement de position
			if (estPromotion) {
				double quotiteReelle = quotite.doubleValue();
				quotiteReelle = quotiteReelle * (position.toPosition().prctDroitAvctGrad().doubleValue() / 100);
				quotite = new Double(quotiteReelle);
			}
			fiche.setQuotite(quotite);
			fiche.setCorps(element.toCorps().lcCorps());
			fiche.setGrade(element.toGrade().llGrade());
			fiche.setTypePopulation(element.toCarriere().toTypePopulation().cTypePopulation());
			if (element.toGrade().toCategorie() != null) {
				fiche.setCategorie(element.toGrade().toCategorie().cCategorie());
			} else if (element.toCorps().cCategorie() != null) {
				fiche.setCategorie(element.toCorps().cCategorie());
			}
			if (position.toRne() != null) {
				fiche.setLocalisation(position.toRne().llRne());
			}
			else {
				if (position.lieuDestin() != null)
					fiche.setLocalisation(position.lieuDestin());
			}

			fiches.addObject(fiche);
		}
		return fiches;
	}
	
	/**
	 * 
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @param dernierePosition
	 * @return
	 */
	private static FicheAnciennete creerFichePourIndividu(EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin,EOChangementPosition dernierePosition) {
		FicheAnciennete fiche = new FicheAnciennete();
		fiche.initAvecIndividu(individu);
		fiche.setPeriodeDeb(dateDebut);
		fiche.setPeriodeFin(dateFin);
		if (dernierePosition != null) {
			fiche.setCPosition(dernierePosition.toPosition().cPosition());
			fiche.setPosition(dernierePosition.toPosition().lcPosition());

			if (dernierePosition.toRne() != null) {
				fiche.setLocalisation(dernierePosition.toRne().llRne());
			}
			else {
				if (dernierePosition.lieuDestin() != null)
					fiche.setLocalisation(dernierePosition.lieuOrigine());
			}
			if (dernierePosition != null) {
				fiche.setQuotite(new Integer(100));//dernierePosition.quotitePosition());
			}
		}
		return fiche;
	}
	
	/**
	 * 
	 * @param fiche
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	private static FicheAnciennete creerFichePourFicheEtDates(FicheAnciennete fiche,NSTimestamp dateDebut,NSTimestamp dateFin) {
		FicheAnciennete nouvelleFiche = new FicheAnciennete();
		nouvelleFiche.initAvecIndividu(fiche.individu());
		nouvelleFiche.setPeriodeDeb(dateDebut);
		nouvelleFiche.setPeriodeFin(dateFin);
		nouvelleFiche.setCategorie(fiche.categorie());
		nouvelleFiche.setCPosition(fiche.cPosition());
		nouvelleFiche.setPosition(fiche.position());
		nouvelleFiche.setQuotite(fiche.quotite());
		return nouvelleFiche;
	}
	
	/**
	 * 
	 * @param fichesAnciennete
	 * @param estSyntheseMaximum
	 * @return
	 */
	private static NSArray synthese(NSArray fichesAnciennete,boolean estSyntheseMaximum) {
		// Dans le cas d'une synthèse maximum, on ne s'intéresse pas aux grade et à la localisation
		fichesAnciennete = EOSortOrdering.sortedArrayUsingKeyOrderArray(fichesAnciennete,new NSArray(EOSortOrdering.sortOrderingWithKey("periodeDeb",EOSortOrdering.CompareAscending)));
		// les fiches sont classées par ordre d'ancienneté croissante, on regroupe ensemble tous les CDDs
		// et toutes les positions en activité contigus et de même quotité
		NSMutableArray nouvellesFiches = new NSMutableArray();
		FicheAnciennete ficheDebut = ((FicheAnciennete)fichesAnciennete.objectAtIndex(0));
		NSTimestamp dateDebut = ficheDebut.periodeDeb(), dateFin = ficheDebut.periodeFin();
		for (int i = 1; i < fichesAnciennete.count(); i++) {
			FicheAnciennete fiche = (FicheAnciennete)fichesAnciennete.objectAtIndex(i);
			if (ficheDebut.memesCaracteristiques(fiche,estSyntheseMaximum) == false) {
				if (dateDebut != null && dateFin != null) {
					//LogManager.logDetail("Changement des caractéristiques");
					//LogManager.logDetail("Données précédentes\nDébut : " + DateCtrl.dateToString(dateDebut) + ", fin : " + DateCtrl.dateToString(dateFin) + "\n" + ficheDebut.caracteristiques());
					//LogManager.logDetail("Fiche courante\nDébut : " + DateCtrl.dateToString(fiche.periodeDeb()) + ", fin : " + DateCtrl.dateToString(fiche.periodeFin()) + "\n" + fiche.caracteristiques());
					FicheAnciennete nouvelleFiche = creerFichePourFicheEtDates(ficheDebut, dateDebut, dateFin);
					if (ficheDebut.cPosition().equals(PASSE_HORS_EN)) {
						// Recopier toutes les données d'ancienneté
						nouvelleFiche.setAncGenAnnees(ficheDebut.ancGenAnnees());
						nouvelleFiche.setAncGenMois(ficheDebut.ancGenMois());
						nouvelleFiche.setAncGenJours(ficheDebut.ancGenJours());
						nouvelleFiche.setAncServAnnees(ficheDebut.ancServAnnees());
						nouvelleFiche.setAncServMois(ficheDebut.ancServMois());
						nouvelleFiche.setAncServJours(ficheDebut.ancServJours());
					} else {
						calculerAnneesMoisJoursAnciennete(nouvelleFiche,false);
					}
					nouvelleFiche.setCorps(ficheDebut.corps());
					if (!estSyntheseMaximum) {
						nouvelleFiche.setGrade(ficheDebut.grade());
						nouvelleFiche.setLocalisation(ficheDebut.localisation());
					}
					nouvellesFiches.addObject(nouvelleFiche);
					ficheDebut = fiche;
					dateDebut = ficheDebut.periodeDeb();
					dateFin = ficheDebut.periodeFin();
				}
			} else {
				// les fiches ont les mêmes caractéristiques, vérifier si elles sont contigües ou non
				if (DateCtrl.dateToString(DateCtrl.jourSuivant(dateFin)).equals(DateCtrl.dateToString(fiche.periodeDeb()))) {
					dateFin = fiche.periodeFin();
				} else {
					// il faut créer une nouvelle fiche pour la période en cours
					//LogManager.logDetail("Fiches non contigues");
					//LogManager.logDetail("Données précédentes\nDébut : " + DateCtrl.dateToString(dateDebut) + ", fin : " + DateCtrl.dateToString(dateFin) + "\n" + ficheDebut.caracteristiques());
					//LogManager.logDetail("Fiche courante\nDébut : " + DateCtrl.dateToString(fiche.periodeDeb()) + ", fin : " + DateCtrl.dateToString(fiche.periodeFin()) + "\n" + fiche.caracteristiques());
					FicheAnciennete nouvelleFiche = creerFichePourFicheEtDates(ficheDebut, dateDebut, dateFin);
					calculerAnneesMoisJoursAnciennete(nouvelleFiche,false);
					nouvelleFiche.setCorps(ficheDebut.corps());
					if (!estSyntheseMaximum) {
						nouvelleFiche.setGrade(ficheDebut.grade());
						nouvelleFiche.setLocalisation(ficheDebut.localisation());
					}
					nouvellesFiches.addObject(nouvelleFiche);
					ficheDebut = fiche;
					dateDebut = ficheDebut.periodeDeb();
					dateFin = ficheDebut.periodeFin();
				}
			}
		}
		// il reste une fiche
		//LogManager.logDetail("Dernière fiche\nDébut : " + DateCtrl.dateToString(dateDebut) + ", fin : " + DateCtrl.dateToString(dateFin) + "\n" + ficheDebut.caracteristiques());
		FicheAnciennete nouvelleFiche = creerFichePourFicheEtDates(ficheDebut, dateDebut, dateFin);
		calculerAnneesMoisJoursAnciennete(nouvelleFiche,false);
		nouvelleFiche.setCorps(ficheDebut.corps());
		nouvelleFiche.setGrade(ficheDebut.grade());
		nouvelleFiche.setLocalisation(ficheDebut.localisation());
		nouvellesFiches.addObject(nouvelleFiche);

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(nouvellesFiches,new NSArray(EOSortOrdering.sortOrderingWithKey("periodeDeb",EOSortOrdering.CompareDescending)));
	}

	private boolean memesCaracteristiques(FicheAnciennete fiche,boolean estSyntheseMaximum) {
		if (cPosition().equals(fiche.cPosition()) == false) {
			return false;
		}
		if ((corps() != null && fiche.corps() != null && corps().equals(fiche.corps()) == false) ||
				(corps() == null && fiche.corps() != null) || (corps() != null && fiche.corps() == null)) {
			return false;
		}
		if (quotite().intValue() != fiche.quotite().intValue()) {
			return false;
		}
		if (!estSyntheseMaximum) {
			if ((localisation() != null && fiche.localisation() != null && localisation().equals(fiche.localisation()) == false) ||
					(localisation() == null && fiche.localisation() != null) || (localisation() != null && fiche.localisation() == null)) {
				return false;
			}
			if ((grade() != null && fiche.grade() != null && grade().equals(fiche.grade()) == false) ||
					(grade() == null && fiche.grade() != null) || (grade() != null && fiche.grade() == null)) {
				return false;
			}
		}
		return true;
	}

	
}
