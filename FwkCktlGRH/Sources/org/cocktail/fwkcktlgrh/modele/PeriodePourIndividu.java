/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.fwkcktlgrh.modele;

import org.cocktail.fwkcktlgrh.common.utilities.CocktailConstantes;
import org.cocktail.fwkcktlgrh.common.utilities.DateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Modelise une periode pour un individu avec une date debut et fin et une relation vers l'individu */

public abstract class PeriodePourIndividu extends EOGenericRecord {

	private static final long serialVersionUID = 1L;
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	public static final NSArray SORT_ARRAY_DATE_DEBUT_ASC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));
	public static final NSArray SORT_ARRAY_DATE_DEBUT_DESC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));

	public static final EOSortOrdering SORT_DATE_DEBUT_ASC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_DATE_DEBUT_DESC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending);

	public static final String INDIVIDU_KEY = "toIndividu";

	public NSTimestamp dateDebut() {
		return (NSTimestamp)storedValueForKey(DATE_DEBUT_KEY);
	}

	public void setDateDebut(NSTimestamp value) {
		takeStoredValueForKey(value, DATE_DEBUT_KEY);
	}

	public NSTimestamp dateFin() {
		return (NSTimestamp)storedValueForKey(DATE_FIN_KEY);
	}

	public void setDateFin(NSTimestamp value) {
		takeStoredValueForKey(value, DATE_FIN_KEY);
	}

	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}


	public EOIndividu individu() {
		return (EOIndividu)storedValueForKey(INDIVIDU_KEY);
	}

	public void setIndividuRelationship(EOIndividu value) {
		if (value == null) {
			EOIndividu oldValue = individu();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
		}
	}

	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,DATE_DEBUT_KEY);
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null) {
			setDateDebut(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_DEBUT_KEY,uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,DATE_FIN_KEY);
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null) {
			setDateFin(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_FIN_KEY,uneDate);
		}
	}
	public void initAvecDates(NSTimestamp dateDebut,NSTimestamp dateFin) {
		setDateDebut(dateDebut);
		setDateFin(dateFin);
	}
	public void initAvecIndividu(EOIndividu individu) {
		init();
		setIndividuRelationship(individu);
	}
	public void supprimerRelations() {
		setIndividuRelationship(null);
	}
	/** la sous-classe doit absolument invoquer cette methode */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		// La date de fin ne doit pas etre inferieure a la date de debut
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début doit être définie !");
		} else if (dateFin() != null && dateDebut().after(dateFin())) {
			throw new NSValidation.ValidationException("La date de fin doit être postérieure à la date de début (FIN : " + dateFinFormatee() + " , DEBUT : " + dateDebutFormatee() + ")");
		}
		if (individu() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un individu");
		}
	}
	/** duree en nombre de jours */
	public String nbJours() {
		return new Integer(DateCtrl.nbJoursEntre(dateDebut(),dateFin(),true)).toString();
	}
	// méthodes protégées
	/** methode d'initialisation que les sous-classes doivent surcharger */
	protected abstract void init();

	// méthodes statiques
	/** recherche les objets de type duree selon certains criteres
	 * @param editingContext
	 * @param entite nom entite concernee
	 * @param qualifier
	 * @param shouldRefresh true si le fetch doit raffraichir les objets 
	 * @return tableau des objets trouves
	 */
	// 13/07/2010 - Ajout d'une méthode pour ne pas faire systématiquement les refresh des objets
	public static NSArray rechercherDureePourEntiteAvecCriteres(EOEditingContext editingContext,String entite,EOQualifier qualifier,boolean shouldRefresh) {
		EOFetchSpecification myFetch = new EOFetchSpecification(entite,qualifier,null);
		myFetch.setRefreshesRefetchedObjects(shouldRefresh);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/** recherche les objets de type duree selon certains criteres : le fetch raffraichit les objets
	 * @param editingContext
	 * @param entite nom entite concernee
	 * @param qualifier
	 * @return tableau des objets trouves
	 */
	public static NSArray rechercherDureePourEntiteAvecCriteres(EOEditingContext editingContext,String entite,EOQualifier qualifier) {
		return rechercherDureePourEntiteAvecCriteres(editingContext,entite,qualifier,true);
	}
	/** Recherche des objets de type dure pour un individu 
	 * @param editingContext
	 * @param nomEntite
	 * @param individu
	 * @param shouldRefresh true si le fetch doit raffraichir les objets 
	 */
	public static NSArray rechercherDureesPourIndividu(EOEditingContext editingContext, String nomEntite,EOIndividu individu,boolean shouldRefresh) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@", new NSArray(individu));
		return rechercherDureePourEntiteAvecCriteres(editingContext,nomEntite,qual,shouldRefresh);	
	}
	/** Recherche des objets de type dure pour un individu  : le fetch raffraichit les objets
	 * @param editingContext
	 * @param nomEntite
	 * @param individu */
	public static NSArray rechercherDureesPourIndividu(EOEditingContext editingContext, String nomEntite,EOIndividu individu) {
		return rechercherDureesPourIndividu(editingContext, nomEntite, individu,true);
	}
	/** Recherche l'objet de type duree pour un individu commencant a une date donnee */
	public static NSArray rechercherDureesPourIndividuAvecDateDebut(EOEditingContext editingContext, String entite,EOIndividu individu, NSTimestamp dateDebut) {
		NSMutableArray args = new NSMutableArray();
		args.addObject(individu);
		args.addObject(dateDebut);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ and dateDebut = %@", args);
		return rechercherDureePourEntiteAvecCriteres(editingContext,entite,qual);
	}
	/** Recherche l'objet de type dure pour un individu  commencant a une date donnee */
	public static Duree rechercherDureePourIndividuAvecDateDebut(EOEditingContext editingContext, String entite,EOIndividu individu, NSTimestamp dateDebut) {
		try {
			return (Duree)rechercherDureesPourIndividuAvecDateDebut(editingContext,entite,individu,dateDebut).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** Recherche les durees pour un individu  commencant avant une date donnee */
	public static NSArray rechercherDureesPourIndividuAnterieuresADate(EOEditingContext editingContext, String entite,EOIndividu individu, NSTimestamp date) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temValide = 'O'", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " <= %@ ", new NSArray(date)));

		return rechercherDureePourEntiteAvecCriteres(editingContext,entite, new EOAndQualifier(qualifiers));
	}
	/** Recherche les durees pour un individu  commencant apres une date donnee */
	public static NSArray rechercherDureesPourIndividuPosterieuresADate(EOEditingContext editingContext, String entite,EOIndividu individu, NSTimestamp date) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " >= %@ ", new NSArray(date)));

		return rechercherDureePourEntiteAvecCriteres(editingContext,entite, new EOAndQualifier(qualifiers));
	}


	/** recherche une duree valide pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param entite	nom de l'entite sur lequel faire le fetch
	 * @param debutPeriode
	 * @param finPeriode	 peut etre nulle
	 * @return durees trouves
	 */
	public static NSArray rechercherDureesPourIndividuEtQualifier(EOEditingContext editingContext,String entite,EOIndividu individu, EOQualifier qualifier) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ ", new NSArray(individu)));

		qualifiers.addObject(qualifier);

		//		EOQualifier qualifier = SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode, DATE_FIN_KEY,finPeriode);
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}

		return rechercherDureePourEntiteAvecCriteres(editingContext,entite, new EOAndQualifier(qualifiers));
	}


	/** recherche une duree valide pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param entite	nom de l'entite sur lequel faire le fetch
	 * @param debutPeriode
	 * @param finPeriode	 peut etre nulle
	 * @return durees trouves
	 */
	public static NSArray rechercherDureesPourIndividuEtPeriode(EOEditingContext editingContext,String entite,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray qualifiers = new NSMutableArray();

		try {
			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode, DATE_FIN_KEY,finPeriode));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temValide" + " = %@ ", new NSArray(CocktailConstantes.VRAI)));
			return rechercherDureePourEntiteAvecCriteres(editingContext,entite, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			System.out
			.println("PeriodePourIndividu.rechercherDureesPourIndividuEtPeriode() SANS TEM VALIDE");
			e.printStackTrace();
			qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode, DATE_FIN_KEY,finPeriode));

			return rechercherDureePourEntiteAvecCriteres(editingContext,entite, new EOAndQualifier(qualifiers));			
		}

	}

	/** recherche une duree valide pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param entite	nom de l'entite sur lequel faire le fetch
	 * @param debutPeriode
	 * @param finPeriode	 peut etre nulle
	 * @param shouldRefresh true si les donnees sont raffraichies
	 * @return durees trouves
	 */
	public static NSArray rechercherDureesPourIndividuEtPeriode(EOEditingContext editingContext,String entite,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean shouldRefresh) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY ,debutPeriode, DATE_FIN_KEY,finPeriode));

			return rechercherDureePourEntiteAvecCriteres(editingContext,entite,new EOAndQualifier(qualifiers),shouldRefresh);
		}
		catch (Exception e) {
			//e.printStackTrace();
			return new NSArray();
		}
	}

	/** recherche les objets dont le nom d'entite est passe en parametre, d'un individu commencant apres la date fournie en parametre */
	public static NSArray rechercherDureesPourEntitePosterieuresADate(EOEditingContext editingContext,String nomEntite,EOIndividu individu,NSTimestamp date) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temValide = 'O' ", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " > %@ ", new NSArray(date)));

		EOFetchSpecification myFetch = new EOFetchSpecification(nomEntite,new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	/** recherche les objets dont le nom d'entite est passe en parametre, d'un individu se terminant ant avant la date fournie en parametre */
	public static NSArray rechercherDureesPourEntiteAnterieuresADate(EOEditingContext editingContext,String nomEntite,EOIndividu individu,NSTimestamp date) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temValide = 'O' ", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_KEY + " < %@ ", new NSArray(date)));

		EOFetchSpecification myFetch = new EOFetchSpecification(nomEntite,new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
}
