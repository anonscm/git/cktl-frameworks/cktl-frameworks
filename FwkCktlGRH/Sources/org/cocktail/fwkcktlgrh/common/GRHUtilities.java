package org.cocktail.fwkcktlgrh.common;

import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EORelationship;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.eof.ERXKey;

public class GRHUtilities {
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
	private static final String GRHUM_ANNEE_UNIVERSITAIRE_DOIT_ETRE_RENSEIGNE = "Veuillez renseigner le parametre GRHUM_ANNEE_UNIVERSITAIRE dans " 
			+ "la table GRHUM_PARAMETRES"; 

    /** Retourne le qualifier pour d&eacute;terminer les objets valides pour une p&eacute;riode
     * @param champDateDebut    nom du champ de date d&eacute;but sur lequel construire le qualifier
     * @param debutPeriode  date de d&eacute;but de p&eacute;riode (ne peut pas &ecirc;tre nulle)
     * @param champDateFin  nom du champ de date fin sur lequel construire le qualifier
     * @param finPeriode    date de fin de p&eacute;riode
     * @return qualifier construit ou null si date d&eacute;but est nulle
     */
    public static EOQualifier qualifierPourPeriode(String champDateDebut, NSTimestamp debutPeriode, String champDateFin, NSTimestamp finPeriode) {
        if (debutPeriode == null) {
            return null;
        }
        String stringQualifier = "";
        NSMutableArray args = new NSMutableArray(debutPeriode);
        // champDateFin = nil or champDateFin >= debutPeriode
        stringQualifier = "(" + champDateFin + " = nil OR " + champDateFin + " >= %@)";
        if (finPeriode != null) {
            args.addObject(finPeriode);
            // champDateDebut <= finPeriode
            stringQualifier = stringQualifier + " AND (" + champDateDebut + " <= %@)";
        }
        return EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
    }
    
    /** Permet de d&eacute;terminer l'intervalle de temps sur lequel une information est valide */
    public static class IntervalleTemps implements NSKeyValueCoding {
        private NSTimestamp debutPeriode;
        private NSTimestamp finPeriode;

        public IntervalleTemps(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
            this.debutPeriode = debutPeriode;
            this.finPeriode = finPeriode;
        }
        
        // Acesseurs
        public NSTimestamp debutPeriode() {
            return debutPeriode;
        }
        
        public NSTimestamp finPeriode() {
            return finPeriode;
        }
        
        /** 2 intervalles sont identiques si ils ont les m&ecirc;mes date de d&eacute;but et de fin */
        public boolean estIdentique(IntervalleTemps intervalle) {
            if (DateCtrl.isSameDay(debutPeriode(), intervalle.debutPeriode())) {
                return (finPeriode() == null && intervalle.finPeriode() == null)
                 || (finPeriode() != null && intervalle.finPeriode() != null && DateCtrl.isSameDay(finPeriode(), intervalle.finPeriode()));
            } else {
                return false;
            }
        }
        
        public void takeValueForKey(Object valeur, String cle) {
            NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);
        }
        
        public Object valueForKey(String cle) {
            return NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
        }
        
        @Override
		public String toString() {
            if (debutPeriode() == null) {
                return "[]";
            }
            String texte = "[" + DateCtrl.dateToString(debutPeriode()) + ",";
            if (finPeriode() == null) {
                texte += "...]";
            } else {
                texte += DateCtrl.dateToString(finPeriode()) + "]";
            }
            return texte;
        }
        
        /** Retourne l'intersection de deux p&eacute;riodes de temps sous la forme d'un intervalle */
        public static IntervalleTemps intersectionPeriodes(NSTimestamp debutPeriode1,NSTimestamp finPeriode1,NSTimestamp debutPeriode2,NSTimestamp finPeriode2) {
            // Si la deuxiÃ¨me pÃ©riode se termine avant la premiÃ¨re ou la deuxiÃ¨me pÃ©riode commence aprÃ¨s la fin de la premiÃ¨re
            // il n'y a pas d'intervalle commun
            if ((finPeriode2 != null && debutPeriode1 != null && DateCtrl.isBefore(finPeriode2, debutPeriode1))
                    || (finPeriode1 != null && debutPeriode2 != null && DateCtrl.isAfter(debutPeriode2, finPeriode1))) {
                return null;
            }
            // Chevauchement des pÃ©riodes
            NSTimestamp debut = debutPeriode1;
            if (debut == null || (debutPeriode2 != null && DateCtrl.isAfter(debutPeriode2, debut))) {
                debut = debutPeriode2;
            }
            NSTimestamp fin = finPeriode1;
            if ((fin == null && finPeriode2 != null) || (fin != null && finPeriode2 != null && DateCtrl.isBefore(finPeriode2, fin))) {
                fin = finPeriode2;
            }
            return new IntervalleTemps(debut, fin);
        }
    }
    
    public static boolean isModuleGRHInUse (EOEditingContext ec) {
    	String isModuleGRHInUse;
    	
    	isModuleGRHInUse = EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_RH, "OUI");
    	return (isModuleGRHInUse.startsWith("OUI") || isModuleGRHInUse.startsWith("O"));
    }
    
    /**
     * Renvoie un qualifier avec la date donnée en paramètre comprise pendant la période universitaire 
     * @param ec : editing context
     * @param key : paramètre de date à vérifier
     * @return : un qualifier 
     */
    //TODO :TU 
    public static EOQualifier qualifierDateDansAnneeUniversitaire(EOEditingContext ec, ERXKey<?> key) {
    	String  annee = EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_ANNEE_UNIVERSITAIRE);
    	
    	if (!StringUtils.isEmpty(annee)) {
	    	int anneeUniversitaire = Integer.parseInt(annee);
	    	try {
		    	NSTimestamp debut = new NSTimestamp(DATE_FORMAT.parse(String.format("01/09/%d", anneeUniversitaire)));
		    	NSTimestamp fin = new NSTimestamp(DATE_FORMAT.parse(String.format("31/08/%d", anneeUniversitaire + 1)));
		    	return key.after(debut).and(key.before(fin));
	    	} catch (Exception e) {
	    		System.err.println(e.getMessage());
	    		return null;
	    	}
    	} else {
    		throw new ValidationException(GRHUM_ANNEE_UNIVERSITAIRE_DOIT_ETRE_RENSEIGNE);
    	}
    }
    
    /**
     * Renvoie un qualifier avec la date donnée en paramètre comprise pendant la période universitaire 
     * @param ec : editing context
     * @param key : paramètre de date à vérifier
     * @return : un qualifier 
     */
    //TODO :TU 
    public static EOQualifier qualifierDateDansAnneeUniversitaire(ERXKey<?> key, String annee) {
    	
    	if (!StringUtils.isEmpty(annee)) {
	    	int anneeUniversitaire = Integer.parseInt(annee);
	    	try {
		    	NSTimestamp debut = new NSTimestamp(DATE_FORMAT.parse(String.format("01/09/%d", anneeUniversitaire)));
		    	NSTimestamp fin = new NSTimestamp(DATE_FORMAT.parse(String.format("31/08/%d", anneeUniversitaire + 1)));
		    	return key.after(debut).and(key.before(fin));
	    	} catch (Exception e) {
	    		System.err.println(e.getMessage());
	    		return null;
	    	}
    	} else {
    		throw new ValidationException(GRHUM_ANNEE_UNIVERSITAIRE_DOIT_ETRE_RENSEIGNE);
    	}
    }
    
    /**
	 * permet de faire un fetch en une seule ligne
	 * 
	 * @param ec
	 * @param entity
	 * @param qual
	 * @param arraySort
	 * @return NSArray des objets issus du fetch
	 */
	public static NSArray fetchArray(EOEditingContext ec, String entity, EOQualifier qual, NSArray arraySort) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(entity, qual, arraySort);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	/**
	 * permet de faire un fetch en une seule ligne
	 * 
	 * @param ec
	 * @param entity
	 * @param qual
	 * @param arraySort
	 * @param refresh
	 * @return NSArray des objets issus du fetch
	 */
	public static NSArray fetchArray(EOEditingContext ec, String entity, EOQualifier qual, NSArray arraySort, boolean refresh) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(entity, qual, arraySort);
		fetchSpec.setRefreshesRefetchedObjects(refresh);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	/**
	 * permet de sauvegarder un editing context
	 * 
	 * @param ec
	 * @param message
	 *          : le message d'erreur si probleme
	 */
	public static boolean save(EOEditingContext ec, String message)
			throws Exception {
		ec.lock();
		try {
			ec.saveChanges();
			ec.unlock();
			return true;
		} catch (Exception e) {
			ec.revert();
			ec.unlock();
			e.printStackTrace();
			throw e;
		}
	}
	
	/**
	 * Récupère le nom d'une relation "proprement"
	 * @param entitySimpleName le nom de l'entité
	 * @param relationshipName le nom de sa relation
	 * @return nom de la relation
	 */
	public static String relationshipName(String entitySimpleName, String relationshipName) {
		EOEntity entity = ERXEOAccessUtilities.entityNamed(null, entitySimpleName); 
		EORelationship relationship = entity.relationshipNamed(relationshipName);
		return relationship.destinationAttributes().lastObject().name();
	}
	
}
