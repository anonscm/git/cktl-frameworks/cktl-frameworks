package org.cocktail.fwkcktlgrh.common;

import org.cocktail.fwkcktlgrh.common.metier.EOAgentPersonnel;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOSession;
import com.webobjects.eocontrol.EOEditingContext;

public class GRHApplicationUser {

    private EOAgentPersonnel agentPersonnel;
    public static final String SESSION_STORING_KEY = "GRHApplicationUserSessionKey";
    private boolean isModuleGRHInUse;
    private PersonneApplicationUser personneApp;
    
    private CktlWebApplication app;
    
    public GRHApplicationUser(EOAgentPersonnel agentPersonnel) {
        this.agentPersonnel = agentPersonnel;
    }
    
    public GRHApplicationUser(EOEditingContext ec, EOIndividu individu) {
//    	initGrhAppUser(ec, individu.persId());
//        this.agentPersonnel = EOAgentPersonnel.rechercherAgentAvecIndividu(ec, individu);
//        if (this.agentPersonnel == null)
//            throw new IllegalStateException("Aucun enregistrement dans Agent Personnel !");
    	this(ec, individu.persId());
    }
    
    public GRHApplicationUser(EOEditingContext ec, Integer persId) {
    	initGrhAppUser(ec, persId);
        this.agentPersonnel = EOAgentPersonnel.rechercherAgentAvecPersId(ec, persId);
//        if (this.agentPersonnel == null)
//            throw new IllegalStateException("Aucun enregistrement dans Agent Personnel !");
    }
    
    public void initGrhAppUser(EOEditingContext ec, Integer persId) {
		this.personneApp = new PersonneApplicationUser(ec, persId);
    	isModuleGRHInUse = GRHUtilities.isModuleGRHInUse(ec);
	}
    
    public static void registerIntoSession(GRHApplicationUser user, WOSession session) {
        session.setObjectForKey(user, SESSION_STORING_KEY);
    }
    
    public static GRHApplicationUser retrieveFromSession(WOSession session) {
        return (GRHApplicationUser) session.objectForKey(SESSION_STORING_KEY);
    }
    
    public boolean hasDroitRH() {
    	return agentPersonnel != null;
    }
    
    public boolean peutGererHeberges() {
    	return (hasDroitRH() && agentPersonnel.gereHeberges()) || (!isModuleGRHInUse() && personneApp.hasDroitTous());
//    	if (!isModuleGRHInUse()) {
//    		return personneApp.hasDroitTous();
//		}
//        return agentPersonnel.gereHeberges();
    }
    
    public boolean peutGererVacataires() {
    	return (hasDroitRH() && agentPersonnel.gereVacataires()) || (!isModuleGRHInUse() && personneApp.hasDroitTous());
    }
    
    public boolean peutGererNonEnseignants() {
    	return (hasDroitRH() && agentPersonnel.gereNonEnseignants()) || (!isModuleGRHInUse() && personneApp.hasDroitTous());
    }
    
    public boolean peutGererEnseignants() {
    	return (hasDroitRH() && agentPersonnel.gereEnseignants()) || (!isModuleGRHInUse() && personneApp.hasDroitTous());
    }
    
    public boolean peutGererAffectations() {
    	return (hasDroitRH() && agentPersonnel.peutGererOccupation()) || (!isModuleGRHInUse() && personneApp.hasDroitTous());

//    	if (!isModuleGRHInUse()) {
//    		return personneApp.hasDroitTous();
//		}
//        return agentPersonnel.peutGererOccupation();
    }
    
    public boolean peutGererDiplomes() {
    	return (hasDroitRH() && agentPersonnel.peutGererIndividu()) || (!isModuleGRHInUse() && personneApp.hasDroitTous());

//    	if (!isModuleGRHInUse()) {
//    		return personneApp.hasDroitTous();
//		}
//        return agentPersonnel.peutGererIndividu();
    }
    
    public boolean isModuleGRHInUse() {
        return this.isModuleGRHInUse;
    }

    
}
