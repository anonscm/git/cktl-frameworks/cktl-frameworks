/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.utilities.CocktailConstantes;
import org.cocktail.fwkcktlgrh.common.utilities.DateCtrl;
import org.cocktail.fwkcktlgrh.modele.SuperFinder;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOTempsPartiel extends _EOTempsPartiel {
	private static Logger log = Logger.getLogger(EOTempsPartiel.class);

	public static final NSArray<EOSortOrdering> SORT_ARRAY_DATE_DEBUT_ASC = new NSArray<EOSortOrdering>(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	
	/**
	 * Recherche les temps partiels d'un individu sur une période donnée
	 * @param edc un editing context
	 * @param noIndividu numéro individu
	 * @param dateDebut date de Début 
	 * @param dateFin date de fin
	 * @return List<EOTempsPartiel>
	 */
	public static List<EOTempsPartiel> rechercherTempsPartielPourIndividu(EOEditingContext edc, Integer noIndividu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		return fetchAll(edc, qualifierPourIndividuSurPeriode(noIndividu, dateDebut, dateFin), SORT_ARRAY_DATE_DEBUT_ASC);
	}

	private static EOQualifier qualifierPourIndividuSurPeriode(
			Integer noIndividu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		EOQualifier qualifier = ERXQ.and(ERXQ.equals(TO_INDIVIDU.dot(EOIndividu.NO_INDIVIDU_KEY).key(), noIndividu), 
								SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, dateDebut, DATE_FIN_KEY, dateFin),
								ERXQ.equals(TEM_VALIDE_KEY, Constantes.VRAI));
		return qualifier;
	}
	
	
	/** Retourne la date de fin reelle d'un temps partiel a partir de la date de fin d'execution
	 * @return
	 */
	public NSTimestamp dateFinReelle() {
		NSTimestamp dateFin = dateFin();
		if (dFinExecution() != null && (dateFin() == null || DateCtrl.isBefore(dFinExecution(), dateFin()))) {
			dateFin = dFinExecution();
		}
		return dateFin;
	}
	
	public boolean aReprisTempsPlein() {
		return temRepriseTempsPlein() != null && temRepriseTempsPlein().equals(CocktailConstantes.VRAI);
	}
	
	
}
