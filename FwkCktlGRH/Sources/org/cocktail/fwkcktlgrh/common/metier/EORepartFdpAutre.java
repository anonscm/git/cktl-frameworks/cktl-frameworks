/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IRepartFdpAutre;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_PkVisible;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_RepartCompetence;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EORepartFdpAutre extends _EORepartFdpAutre implements I_PkVisible, I_RepartCompetence, IRepartFdpAutre {
  private static Logger log = Logger.getLogger(EORepartFdpAutre.class);

  public static final String TYPE_ACTIVITE = "A";
  public static final String TYPE_COMPETENCE = "C";
  
  
  public EORepartFdpAutre() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }

  
  // Methodes rajoutees
  
  private void checkRfaPosition() {
	  // on met la position max par defaut
	  NSArray lesAutresRepart = toFicheDePoste().tosRepartFdpAutre();
	  if (lesAutresRepart.count() > 0) {
		  Number position = (Number) lesAutresRepart.valueForKey("@max." + FAU_POSITION_KEY);
		  if (position != null) {
			  setFauPosition(new Integer(position.intValue() + 1));
		  }
	  }
	  if (fauPosition() == null) {
		  setFauPosition(new Integer(1));
	  }
  }

  /**
   * Le composé de la clé primaire
   */
  public String id() {
	  NSDictionary dico = EOUtilities.primaryKeyForObject(editingContext(), this);
	  return ENTITY_NAME + "-" + dico.valueForKey("fauKey");
  }

  public NSArray othersRecords() {
	  NSArray othersRecords = null;

	  if (fauType().equals(TYPE_ACTIVITE)) {
		  othersRecords = toFicheDePoste().tosRepartFdpActivitesAutres();
	  } else if (fauType().equals(TYPE_COMPETENCE)) {
		  othersRecords = toFicheDePoste().tosRepartFdpCompetencesAutres();
	  }

	  return othersRecords;
  }

  public String positionKey() {
	  return FAU_POSITION_KEY;
  }

  private Boolean isEnCoursDeModification;

  public boolean isEnCoursDeModification() {
	  if (isEnCoursDeModification == null) {
		  isEnCoursDeModification = new Boolean(false);
	  }
	  return isEnCoursDeModification.booleanValue();
  }

  public void setIsEnCoursDeModification(boolean value) {
	  isEnCoursDeModification = new Boolean(value);
  }

  public String competenceDisplay() {
	  return fauChampLibre(); 
  }

  private static EORepartFdpAutre newDefaultRecordInContext(EOEditingContext ec) {
	  EORepartFdpAutre record = new EORepartFdpAutre();
	  ec.insertObject(record);
	  return record;
  }

  public static EORepartFdpAutre newDefaultRecordInContext(
		  EOEditingContext ec, 
		  String texteChampLibre,
		  String type
  ) {
	  EORepartFdpAutre newRecord = EORepartFdpAutre.newDefaultRecordInContext(ec);

	  newRecord.setFauChampLibre(texteChampLibre);
	  newRecord.setFauType(type);
	  
	  return newRecord;
  }
  
  public static EORepartFdpAutre newRecordInContext(
		  EOEditingContext ec, 
		  EOFicheDePoste ficheDePoste,
		  String texteChampLibre,
		  String type
  ) {
	  EORepartFdpAutre newRecord = EORepartFdpAutre.newDefaultRecordInContext(ec);

	  newRecord.setFauChampLibre(texteChampLibre);
	  newRecord.setFauType(type);
	  
	  ficheDePoste.addToTosRepartFdpAutreRelationship(newRecord);

	  return newRecord;
  }
  
	/**
	 * Remonter
	 */
		
  	public void up() {
		// la position actuelle
		int position = (Integer) valueForKey(positionKey());
		// on ne remonte pas si c'est le premier element
	    if (position != 0) {
	    	// retrouver l'element precedent
	    	NSArray recsPrev = EOQualifier.filteredArrayWithQualifier(othersRecords(),
	    			CktlDataBus.newCondition(positionKey() + "=" + (position - 1)));
	    	if (recsPrev.count() == 1) {
	    		EORepartFdpAutre recPrev = (EORepartFdpAutre) recsPrev.objectAtIndex(0);
	    		recPrev.takeValueForKey(new Integer(position), positionKey());
	    		this.takeValueForKey(new Integer(position - 1), positionKey());
	    	}
	    }
	}


	/**
	 * Redescendre
	 */
	public void down() {
			// la position actuelle
		int position = (Integer) valueForKey(positionKey());
		// on ne descend pas si c'est le dernier element (on cherche les elements suivants)
		NSArray recsNext = EOQualifier.filteredArrayWithQualifier(othersRecords(),
				CktlDataBus.newCondition(positionKey() + ">" + position));
		if (recsNext.count() > 0) {
		// recuperer l'element suivant
		recsNext = EOQualifier.filteredArrayWithQualifier(othersRecords(),
			CktlDataBus.newCondition(positionKey() + "=" + (position + 1)));
			if (recsNext.count() == 1) {
				EORepartFdpAutre recNext = (EORepartFdpAutre) recsNext.objectAtIndex(0);
				recNext.takeValueForKey(new Integer(position), positionKey());
				this.takeValueForKey(new Integer(position + 1), positionKey());
			}
		}
	}
  
}
