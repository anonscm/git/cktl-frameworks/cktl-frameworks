/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPasse.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPasse extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Passe";

	// Attributes
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DUREE_VALIDEE_ANNEES_KEY = "dureeValideeAnnees";
	public static final String DUREE_VALIDEE_JOURS_KEY = "dureeValideeJours";
	public static final String DUREE_VALIDEE_MOIS_KEY = "dureeValideeMois";
	public static final String D_VALIDATION_SERVICE_KEY = "dValidationService";
	public static final String ETABLISSEMENT_PASSE_KEY = "etablissementPasse";
	public static final String FONCTION_PASSE_KEY = "fonctionPasse";
	public static final String PAS_MINISTERE_KEY = "pasMinistere";
	public static final String PAS_PC_ACQUITEE_KEY = "pasPcAcquitee";
	public static final String PAS_QUOTITE_KEY = "pasQuotite";
	public static final String PAS_QUOTITE_COTISATION_KEY = "pasQuotiteCotisation";
	public static final String PAS_TYPE_FCT_PUBLIQUE_KEY = "pasTypeFctPublique";
	public static final String PAS_TYPE_SERVICE_KEY = "pasTypeService";
	public static final String PAS_TYPE_TEMPS_KEY = "pasTypeTemps";
	public static final String SECTEUR_PUBLIC_KEY = "secteurPublic";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> DUREE_VALIDEE_ANNEES = new ERXKey<Integer>("dureeValideeAnnees");
	public static final ERXKey<Integer> DUREE_VALIDEE_JOURS = new ERXKey<Integer>("dureeValideeJours");
	public static final ERXKey<Integer> DUREE_VALIDEE_MOIS = new ERXKey<Integer>("dureeValideeMois");
	public static final ERXKey<NSTimestamp> D_VALIDATION_SERVICE = new ERXKey<NSTimestamp>("dValidationService");
	public static final ERXKey<String> ETABLISSEMENT_PASSE = new ERXKey<String>("etablissementPasse");
	public static final ERXKey<String> FONCTION_PASSE = new ERXKey<String>("fonctionPasse");
	public static final ERXKey<String> PAS_MINISTERE = new ERXKey<String>("pasMinistere");
	public static final ERXKey<String> PAS_PC_ACQUITEE = new ERXKey<String>("pasPcAcquitee");
	public static final ERXKey<java.math.BigDecimal> PAS_QUOTITE = new ERXKey<java.math.BigDecimal>("pasQuotite");
	public static final ERXKey<java.math.BigDecimal> PAS_QUOTITE_COTISATION = new ERXKey<java.math.BigDecimal>("pasQuotiteCotisation");
	public static final ERXKey<String> PAS_TYPE_FCT_PUBLIQUE = new ERXKey<String>("pasTypeFctPublique");
	public static final ERXKey<String> PAS_TYPE_SERVICE = new ERXKey<String>("pasTypeService");
	public static final ERXKey<String> PAS_TYPE_TEMPS = new ERXKey<String>("pasTypeTemps");
	public static final ERXKey<String> SECTEUR_PUBLIC = new ERXKey<String>("secteurPublic");
	public static final ERXKey<String> TEM_TITULAIRE = new ERXKey<String>("temTitulaire");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_CATEGORIE_KEY = "toCategorie";
	public static final String TO_CONTRAT_AVENANT_KEY = "toContratAvenant";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_TYPE_POPULATION_KEY = "toTypePopulation";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCategorie> TO_CATEGORIE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCategorie>("toCategorie");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant> TO_CONTRAT_AVENANT = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant>("toContratAvenant");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation> TO_TYPE_POPULATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation>("toTypePopulation");

  private static Logger LOG = Logger.getLogger(_EOPasse.class);

  public EOPasse localInstanceIn(EOEditingContext editingContext) {
    EOPasse localInstance = (EOPasse)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer dureeValideeAnnees() {
    return (Integer) storedValueForKey("dureeValideeAnnees");
  }

  public void setDureeValideeAnnees(Integer value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dureeValideeAnnees from " + dureeValideeAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeValideeAnnees");
  }

  public Integer dureeValideeJours() {
    return (Integer) storedValueForKey("dureeValideeJours");
  }

  public void setDureeValideeJours(Integer value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dureeValideeJours from " + dureeValideeJours() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeValideeJours");
  }

  public Integer dureeValideeMois() {
    return (Integer) storedValueForKey("dureeValideeMois");
  }

  public void setDureeValideeMois(Integer value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dureeValideeMois from " + dureeValideeMois() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeValideeMois");
  }

  public NSTimestamp dValidationService() {
    return (NSTimestamp) storedValueForKey("dValidationService");
  }

  public void setDValidationService(NSTimestamp value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating dValidationService from " + dValidationService() + " to " + value);
    }
    takeStoredValueForKey(value, "dValidationService");
  }

  public String etablissementPasse() {
    return (String) storedValueForKey("etablissementPasse");
  }

  public void setEtablissementPasse(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating etablissementPasse from " + etablissementPasse() + " to " + value);
    }
    takeStoredValueForKey(value, "etablissementPasse");
  }

  public String fonctionPasse() {
    return (String) storedValueForKey("fonctionPasse");
  }

  public void setFonctionPasse(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating fonctionPasse from " + fonctionPasse() + " to " + value);
    }
    takeStoredValueForKey(value, "fonctionPasse");
  }

  public String pasMinistere() {
    return (String) storedValueForKey("pasMinistere");
  }

  public void setPasMinistere(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasMinistere from " + pasMinistere() + " to " + value);
    }
    takeStoredValueForKey(value, "pasMinistere");
  }

  public String pasPcAcquitee() {
    return (String) storedValueForKey("pasPcAcquitee");
  }

  public void setPasPcAcquitee(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasPcAcquitee from " + pasPcAcquitee() + " to " + value);
    }
    takeStoredValueForKey(value, "pasPcAcquitee");
  }

  public java.math.BigDecimal pasQuotite() {
    return (java.math.BigDecimal) storedValueForKey("pasQuotite");
  }

  public void setPasQuotite(java.math.BigDecimal value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasQuotite from " + pasQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "pasQuotite");
  }

  public java.math.BigDecimal pasQuotiteCotisation() {
    return (java.math.BigDecimal) storedValueForKey("pasQuotiteCotisation");
  }

  public void setPasQuotiteCotisation(java.math.BigDecimal value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasQuotiteCotisation from " + pasQuotiteCotisation() + " to " + value);
    }
    takeStoredValueForKey(value, "pasQuotiteCotisation");
  }

  public String pasTypeFctPublique() {
    return (String) storedValueForKey("pasTypeFctPublique");
  }

  public void setPasTypeFctPublique(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasTypeFctPublique from " + pasTypeFctPublique() + " to " + value);
    }
    takeStoredValueForKey(value, "pasTypeFctPublique");
  }

  public String pasTypeService() {
    return (String) storedValueForKey("pasTypeService");
  }

  public void setPasTypeService(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasTypeService from " + pasTypeService() + " to " + value);
    }
    takeStoredValueForKey(value, "pasTypeService");
  }

  public String pasTypeTemps() {
    return (String) storedValueForKey("pasTypeTemps");
  }

  public void setPasTypeTemps(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating pasTypeTemps from " + pasTypeTemps() + " to " + value);
    }
    takeStoredValueForKey(value, "pasTypeTemps");
  }

  public String secteurPublic() {
    return (String) storedValueForKey("secteurPublic");
  }

  public void setSecteurPublic(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating secteurPublic from " + secteurPublic() + " to " + value);
    }
    takeStoredValueForKey(value, "secteurPublic");
  }

  public String temTitulaire() {
    return (String) storedValueForKey("temTitulaire");
  }

  public void setTemTitulaire(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temTitulaire");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
    	_EOPasse.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCategorie toCategorie() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCategorie)storedValueForKey("toCategorie");
  }

  public void setToCategorieRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCategorie value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
      _EOPasse.LOG.debug("updating toCategorie from " + toCategorie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCategorie oldValue = toCategorie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCategorie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCategorie");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant toContratAvenant() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant)storedValueForKey("toContratAvenant");
  }

  public void setToContratAvenantRelationship(org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
      _EOPasse.LOG.debug("updating toContratAvenant from " + toContratAvenant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant oldValue = toContratAvenant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toContratAvenant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toContratAvenant");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
      _EOPasse.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation toTypePopulation() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation)storedValueForKey("toTypePopulation");
  }

  public void setToTypePopulationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation value) {
    if (_EOPasse.LOG.isDebugEnabled()) {
      _EOPasse.LOG.debug("updating toTypePopulation from " + toTypePopulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation oldValue = toTypePopulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypePopulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypePopulation");
    }
  }
  

  public static EOPasse create(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String pasMinistere
, String secteurPublic
, String temTitulaire
, String temValide
) {
    EOPasse eo = (EOPasse) EOUtilities.createAndInsertInstance(editingContext, _EOPasse.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPasMinistere(pasMinistere);
		eo.setSecteurPublic(secteurPublic);
		eo.setTemTitulaire(temTitulaire);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOPasse> fetchAll(EOEditingContext editingContext) {
    return _EOPasse.fetchAll(editingContext, null);
  }

  public static NSArray<EOPasse> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPasse.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPasse> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPasse.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPasse> eoObjects = (NSArray<EOPasse>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPasse fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPasse.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPasse fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPasse> eoObjects = _EOPasse.fetch(editingContext, qualifier, null);
    EOPasse eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPasse)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Passe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPasse fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPasse.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPasse fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPasse eoObject = _EOPasse.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Passe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPasse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPasse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPasse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPasse)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPasse localInstanceIn(EOEditingContext editingContext, EOPasse eo) {
    EOPasse localInstance = (eo == null) ? null : (EOPasse)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
