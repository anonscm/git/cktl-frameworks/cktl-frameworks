/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOIndividuDiplome.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOIndividuDiplome extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_IndividuDiplome";

	// Attributes
	public static final String CODE_UAI_OBTENTION_KEY = "codeUAIObtention";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DIPLOME_KEY = "dDiplome";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIEU_DIPLOME_KEY = "lieuDiplome";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String SPECIALITE_KEY = "specialite";
	public static final String TEMOIN_VALIDE_KEY = "temoinValide";

	public static final ERXKey<String> CODE_UAI_OBTENTION = new ERXKey<String>("codeUAIObtention");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DIPLOME = new ERXKey<NSTimestamp>("dDiplome");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> LIEU_DIPLOME = new ERXKey<String>("lieuDiplome");
	public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
	public static final ERXKey<String> SPECIALITE = new ERXKey<String>("specialite");
	public static final ERXKey<String> TEMOIN_VALIDE = new ERXKey<String>("temoinValide");
	// Relationships
	public static final String TO_DIPLOME_KEY = "toDiplome";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_TITULAIRE_DIPLOME_KEY = "toTitulaireDiplome";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODiplome> TO_DIPLOME = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODiplome>("toDiplome");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTitulaireDiplome> TO_TITULAIRE_DIPLOME = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTitulaireDiplome>("toTitulaireDiplome");

  private static Logger LOG = Logger.getLogger(_EOIndividuDiplome.class);

  public EOIndividuDiplome localInstanceIn(EOEditingContext editingContext) {
    EOIndividuDiplome localInstance = (EOIndividuDiplome)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeUAIObtention() {
    return (String) storedValueForKey("codeUAIObtention");
  }

  public void setCodeUAIObtention(String value) {
    if (_EOIndividuDiplome.LOG.isDebugEnabled()) {
    	_EOIndividuDiplome.LOG.debug( "updating codeUAIObtention from " + codeUAIObtention() + " to " + value);
    }
    takeStoredValueForKey(value, "codeUAIObtention");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuDiplome.LOG.isDebugEnabled()) {
    	_EOIndividuDiplome.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDiplome() {
    return (NSTimestamp) storedValueForKey("dDiplome");
  }

  public void setDDiplome(NSTimestamp value) {
    if (_EOIndividuDiplome.LOG.isDebugEnabled()) {
    	_EOIndividuDiplome.LOG.debug( "updating dDiplome from " + dDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, "dDiplome");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuDiplome.LOG.isDebugEnabled()) {
    	_EOIndividuDiplome.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lieuDiplome() {
    return (String) storedValueForKey("lieuDiplome");
  }

  public void setLieuDiplome(String value) {
    if (_EOIndividuDiplome.LOG.isDebugEnabled()) {
    	_EOIndividuDiplome.LOG.debug( "updating lieuDiplome from " + lieuDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuDiplome");
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey("noIndividu");
  }

  public void setNoIndividu(Integer value) {
    if (_EOIndividuDiplome.LOG.isDebugEnabled()) {
    	_EOIndividuDiplome.LOG.debug( "updating noIndividu from " + noIndividu() + " to " + value);
    }
    takeStoredValueForKey(value, "noIndividu");
  }

  public String specialite() {
    return (String) storedValueForKey("specialite");
  }

  public void setSpecialite(String value) {
    if (_EOIndividuDiplome.LOG.isDebugEnabled()) {
    	_EOIndividuDiplome.LOG.debug( "updating specialite from " + specialite() + " to " + value);
    }
    takeStoredValueForKey(value, "specialite");
  }

  public String temoinValide() {
    return (String) storedValueForKey("temoinValide");
  }

  public void setTemoinValide(String value) {
    if (_EOIndividuDiplome.LOG.isDebugEnabled()) {
    	_EOIndividuDiplome.LOG.debug( "updating temoinValide from " + temoinValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinValide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EODiplome toDiplome() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EODiplome)storedValueForKey("toDiplome");
  }

  public void setToDiplomeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODiplome value) {
    if (_EOIndividuDiplome.LOG.isDebugEnabled()) {
      _EOIndividuDiplome.LOG.debug("updating toDiplome from " + toDiplome() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EODiplome oldValue = toDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toDiplome");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toDiplome");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOIndividuDiplome.LOG.isDebugEnabled()) {
      _EOIndividuDiplome.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTitulaireDiplome toTitulaireDiplome() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTitulaireDiplome)storedValueForKey("toTitulaireDiplome");
  }

  public void setToTitulaireDiplomeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTitulaireDiplome value) {
    if (_EOIndividuDiplome.LOG.isDebugEnabled()) {
      _EOIndividuDiplome.LOG.debug("updating toTitulaireDiplome from " + toTitulaireDiplome() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTitulaireDiplome oldValue = toTitulaireDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTitulaireDiplome");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTitulaireDiplome");
    }
  }
  

  public static EOIndividuDiplome create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noIndividu
, String temoinValide
, org.cocktail.fwkcktlpersonne.common.metier.EODiplome toDiplome, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlpersonne.common.metier.EOTitulaireDiplome toTitulaireDiplome) {
    EOIndividuDiplome eo = (EOIndividuDiplome) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuDiplome.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoIndividu(noIndividu);
		eo.setTemoinValide(temoinValide);
    eo.setToDiplomeRelationship(toDiplome);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToTitulaireDiplomeRelationship(toTitulaireDiplome);
    return eo;
  }

  public static NSArray<EOIndividuDiplome> fetchAll(EOEditingContext editingContext) {
    return _EOIndividuDiplome.fetchAll(editingContext, null);
  }

  public static NSArray<EOIndividuDiplome> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuDiplome.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOIndividuDiplome> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuDiplome.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuDiplome> eoObjects = (NSArray<EOIndividuDiplome>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuDiplome fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuDiplome.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuDiplome fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuDiplome> eoObjects = _EOIndividuDiplome.fetch(editingContext, qualifier, null);
    EOIndividuDiplome eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuDiplome)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_IndividuDiplome that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuDiplome fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuDiplome.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOIndividuDiplome fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuDiplome eoObject = _EOIndividuDiplome.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_IndividuDiplome that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOIndividuDiplome fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOIndividuDiplome fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOIndividuDiplome eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOIndividuDiplome)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOIndividuDiplome localInstanceIn(EOEditingContext editingContext, EOIndividuDiplome eo) {
    EOIndividuDiplome localInstance = (eo == null) ? null : (EOIndividuDiplome)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
