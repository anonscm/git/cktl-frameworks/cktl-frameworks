/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOObjectif.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOObjectif extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Objectif";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String OBJ_MESURE_KEY = "objMesure";
	public static final String OBJ_MOYEN_KEY = "objMoyen";
	public static final String OBJ_OBJECTIF_KEY = "objObjectif";
	public static final String OBJ_OBSERVATION_KEY = "objObservation";
	public static final String OBJ_POSITION_KEY = "objPosition";
	public static final String OBJ_RESULTAT_KEY = "objResultat";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> OBJ_MESURE = new ERXKey<String>("objMesure");
	public static final ERXKey<String> OBJ_MOYEN = new ERXKey<String>("objMoyen");
	public static final ERXKey<String> OBJ_OBJECTIF = new ERXKey<String>("objObjectif");
	public static final ERXKey<String> OBJ_OBSERVATION = new ERXKey<String>("objObservation");
	public static final ERXKey<Integer> OBJ_POSITION = new ERXKey<Integer>("objPosition");
	public static final ERXKey<String> OBJ_RESULTAT = new ERXKey<String>("objResultat");
	// Relationships
	public static final String TO_EVALUATION_KEY = "toEvaluation";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> TO_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>("toEvaluation");

  private static Logger LOG = Logger.getLogger(_EOObjectif.class);

  public EOObjectif localInstanceIn(EOEditingContext editingContext) {
    EOObjectif localInstance = (EOObjectif)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOObjectif.LOG.isDebugEnabled()) {
    	_EOObjectif.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOObjectif.LOG.isDebugEnabled()) {
    	_EOObjectif.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String objMesure() {
    return (String) storedValueForKey("objMesure");
  }

  public void setObjMesure(String value) {
    if (_EOObjectif.LOG.isDebugEnabled()) {
    	_EOObjectif.LOG.debug( "updating objMesure from " + objMesure() + " to " + value);
    }
    takeStoredValueForKey(value, "objMesure");
  }

  public String objMoyen() {
    return (String) storedValueForKey("objMoyen");
  }

  public void setObjMoyen(String value) {
    if (_EOObjectif.LOG.isDebugEnabled()) {
    	_EOObjectif.LOG.debug( "updating objMoyen from " + objMoyen() + " to " + value);
    }
    takeStoredValueForKey(value, "objMoyen");
  }

  public String objObjectif() {
    return (String) storedValueForKey("objObjectif");
  }

  public void setObjObjectif(String value) {
    if (_EOObjectif.LOG.isDebugEnabled()) {
    	_EOObjectif.LOG.debug( "updating objObjectif from " + objObjectif() + " to " + value);
    }
    takeStoredValueForKey(value, "objObjectif");
  }

  public String objObservation() {
    return (String) storedValueForKey("objObservation");
  }

  public void setObjObservation(String value) {
    if (_EOObjectif.LOG.isDebugEnabled()) {
    	_EOObjectif.LOG.debug( "updating objObservation from " + objObservation() + " to " + value);
    }
    takeStoredValueForKey(value, "objObservation");
  }

  public Integer objPosition() {
    return (Integer) storedValueForKey("objPosition");
  }

  public void setObjPosition(Integer value) {
    if (_EOObjectif.LOG.isDebugEnabled()) {
    	_EOObjectif.LOG.debug( "updating objPosition from " + objPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "objPosition");
  }

  public String objResultat() {
    return (String) storedValueForKey("objResultat");
  }

  public void setObjResultat(String value) {
    if (_EOObjectif.LOG.isDebugEnabled()) {
    	_EOObjectif.LOG.debug( "updating objResultat from " + objResultat() + " to " + value);
    }
    takeStoredValueForKey(value, "objResultat");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluation)storedValueForKey("toEvaluation");
  }

  public void setToEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation value) {
    if (_EOObjectif.LOG.isDebugEnabled()) {
      _EOObjectif.LOG.debug("updating toEvaluation from " + toEvaluation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluation oldValue = toEvaluation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluation");
    }
  }
  

  public static EOObjectif create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer objPosition
, org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation) {
    EOObjectif eo = (EOObjectif) EOUtilities.createAndInsertInstance(editingContext, _EOObjectif.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setObjPosition(objPosition);
    eo.setToEvaluationRelationship(toEvaluation);
    return eo;
  }

  public static NSArray<EOObjectif> fetchAll(EOEditingContext editingContext) {
    return _EOObjectif.fetchAll(editingContext, null);
  }

  public static NSArray<EOObjectif> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOObjectif.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOObjectif> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOObjectif.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOObjectif> eoObjects = (NSArray<EOObjectif>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOObjectif fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOObjectif.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOObjectif fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOObjectif> eoObjects = _EOObjectif.fetch(editingContext, qualifier, null);
    EOObjectif eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOObjectif)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Objectif that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOObjectif fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOObjectif.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOObjectif fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOObjectif eoObject = _EOObjectif.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Objectif that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOObjectif fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOObjectif fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOObjectif eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOObjectif)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOObjectif localInstanceIn(EOEditingContext editingContext, EOObjectif eo) {
    EOObjectif localInstance = (eo == null) ? null : (EOObjectif)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
