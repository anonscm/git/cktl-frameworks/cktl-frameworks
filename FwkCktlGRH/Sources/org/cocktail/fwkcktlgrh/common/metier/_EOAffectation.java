/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOAffectation.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOAffectation extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Affectation";

	// Attributes
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_AFFECTATION_KEY = "dDebAffectation";
	public static final String DEN_QUOT_AFFECTATION_KEY = "denQuotAffectation";
	public static final String D_FIN_AFFECTATION_KEY = "dFinAffectation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_SEQ_AFFECTATION_KEY = "noSeqAffectation";
	public static final String NUM_QUOT_AFFECTATION_KEY = "numQuotAffectation";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String TEM_PRINCIPALE_KEY = "temPrincipale";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_AFFECTATION = new ERXKey<NSTimestamp>("dDebAffectation");
	public static final ERXKey<Integer> DEN_QUOT_AFFECTATION = new ERXKey<Integer>("denQuotAffectation");
	public static final ERXKey<NSTimestamp> D_FIN_AFFECTATION = new ERXKey<NSTimestamp>("dFinAffectation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> NO_SEQ_AFFECTATION = new ERXKey<Integer>("noSeqAffectation");
	public static final ERXKey<Integer> NUM_QUOT_AFFECTATION = new ERXKey<Integer>("numQuotAffectation");
	public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
	public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
	public static final ERXKey<String> TEM_PRINCIPALE = new ERXKey<String>("temPrincipale");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_DETAILS_KEY = "toDetails";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_STRUCTURE_KEY = "toStructure";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail> TO_DETAILS = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail>("toDetails");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");

  private static Logger LOG = Logger.getLogger(_EOAffectation.class);

  public EOAffectation localInstanceIn(EOEditingContext editingContext) {
    EOAffectation localInstance = (EOAffectation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cStructure() {
    return (String) storedValueForKey("cStructure");
  }

  public void setCStructure(String value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "cStructure");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebAffectation() {
    return (NSTimestamp) storedValueForKey("dDebAffectation");
  }

  public void setDDebAffectation(NSTimestamp value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating dDebAffectation from " + dDebAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebAffectation");
  }

  public Integer denQuotAffectation() {
    return (Integer) storedValueForKey("denQuotAffectation");
  }

  public void setDenQuotAffectation(Integer value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating denQuotAffectation from " + denQuotAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "denQuotAffectation");
  }

  public NSTimestamp dFinAffectation() {
    return (NSTimestamp) storedValueForKey("dFinAffectation");
  }

  public void setDFinAffectation(NSTimestamp value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating dFinAffectation from " + dFinAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinAffectation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer noSeqAffectation() {
    return (Integer) storedValueForKey("noSeqAffectation");
  }

  public void setNoSeqAffectation(Integer value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating noSeqAffectation from " + noSeqAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqAffectation");
  }

  public Integer numQuotAffectation() {
    return (Integer) storedValueForKey("numQuotAffectation");
  }

  public void setNumQuotAffectation(Integer value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating numQuotAffectation from " + numQuotAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotAffectation");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public String temPrincipale() {
    return (String) storedValueForKey("temPrincipale");
  }

  public void setTemPrincipale(String value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating temPrincipale from " + temPrincipale() + " to " + value);
    }
    takeStoredValueForKey(value, "temPrincipale");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
    	_EOAffectation.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
      _EOAffectation.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey("toStructure");
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
      _EOAffectation.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructure");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail> toDetails() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail>)storedValueForKey("toDetails");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail> toDetails(EOQualifier qualifier) {
    return toDetails(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail> toDetails(EOQualifier qualifier, boolean fetch) {
    return toDetails(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail> toDetails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail.TO_AFFECTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toDetails();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToDetailsRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail object) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
      _EOAffectation.LOG.debug("adding " + object + " to toDetails relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toDetails");
  }

  public void removeFromToDetailsRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail object) {
    if (_EOAffectation.LOG.isDebugEnabled()) {
      _EOAffectation.LOG.debug("removing " + object + " from toDetails relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetails");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail createToDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_AffectationDetail");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toDetails");
    return (org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail) eo;
  }

  public void deleteToDetailsRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetails");
    editingContext().deleteObject(object);
  }

  public void deleteAllToDetailsRelationships() {
    Enumeration objects = toDetails().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToDetailsRelationship((org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail)objects.nextElement());
    }
  }


  public static EOAffectation create(EOEditingContext editingContext, String cStructure
, NSTimestamp dCreation
, NSTimestamp dDebAffectation
, NSTimestamp dModification
, Integer noSeqAffectation
, Integer numQuotAffectation
, String temPrincipale
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure) {
    EOAffectation eo = (EOAffectation) EOUtilities.createAndInsertInstance(editingContext, _EOAffectation.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setDCreation(dCreation);
		eo.setDDebAffectation(dDebAffectation);
		eo.setDModification(dModification);
		eo.setNoSeqAffectation(noSeqAffectation);
		eo.setNumQuotAffectation(numQuotAffectation);
		eo.setTemPrincipale(temPrincipale);
		eo.setTemValide(temValide);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToStructureRelationship(toStructure);
    return eo;
  }

  public static NSArray<EOAffectation> fetchAll(EOEditingContext editingContext) {
    return _EOAffectation.fetchAll(editingContext, null);
  }

  public static NSArray<EOAffectation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAffectation.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOAffectation> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOAffectation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAffectation> eoObjects = (NSArray<EOAffectation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOAffectation fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAffectation.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAffectation fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAffectation> eoObjects = _EOAffectation.fetch(editingContext, qualifier, null);
    EOAffectation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOAffectation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Affectation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAffectation fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAffectation.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOAffectation fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAffectation eoObject = _EOAffectation.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Affectation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOAffectation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAffectation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAffectation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAffectation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOAffectation localInstanceIn(EOEditingContext editingContext, EOAffectation eo) {
    EOAffectation localInstance = (eo == null) ? null : (EOAffectation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
