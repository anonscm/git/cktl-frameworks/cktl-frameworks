/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.droits.DroitService;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_Accreditation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;

public class EOFeveDroit extends _EOFeveDroit implements I_Accreditation {
	private static Logger log = Logger.getLogger(EOFeveDroit.class);

	
	public final static String TO_PERSONNE_TITULAIRE_KEY = "toPersonneTitulaire";
	public final static String TO_PERSONNE_CIBLE_KEY = "toPersonneCible";
	public static final ERXKey<Integer> DRO_ID = new ERXKey<Integer>("droId");
	
	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	
	public static NSArray<EOFeveDroit> getAllEODroit(EOEditingContext ec, Integer persIdTitulaire) {
		EOQualifier qual = ERXQ.equals(PERS_ID_TITULAIRE_KEY, persIdTitulaire);
		return fetchAll(ec, qual, null);
	}
	
	
	/**
	 * Construteur sans date de creation / modification
	 * 
	 * @param editingContext
	 * @param droTypeCible
	 * @param droType
	 * @param toIndividu
	 * @return
	 */
	public static EOFeveDroit create(
				EOEditingContext editingContext,
				IPersonne personneTitulaire,
				IPersonne personneCible,
				boolean isCibleCdc,
				boolean isCibleHeritage,
				EOFeveTypeNiveauDroit typeNiveauDroit
				) {
		
		NSTimestamp now = DateCtrl.now();
		
		EOFeveDroit droit = create(editingContext, now, now, personneCible.persId(), personneTitulaire.persId(), NON, NON, typeNiveauDroit);

		if (personneCible.isStructure()) {
			if (isCibleCdc) {
				droit.setTemCibleCdc(OUI);
			}
			if (isCibleHeritage) {
				droit.setTemCibleHeritage(OUI);
			}
		}

		return droit;
	}
	
	
	
	/**
	 * Liste des {@link EOIndividu} titulaires déduis. Si c'est un groupe, alors
	 * on prend les membres via GRHUM.REPART_STRUCTURE
	 * 
	 * @return
	 */
	public NSArray<EOIndividu> getEoIndividuTitulaireDeduitArray(EOEditingContext ec) {
		NSMutableArray<EOIndividu> array = new NSMutableArray<EOIndividu>();

		if (toPersonneTitulaire().isIndividu()) {
			// individu
			array.addObject((EOIndividu) toPersonneTitulaire());
		} else {
			// groupe
			EOStructure eoStructure = (EOStructure) toPersonneTitulaire();
			array.addObjectsFromArray(DroitService.getEoIndividuMembre(eoStructure, false));

		}

		return array.immutableClone();
	}
	
	
	/**
	 * @return
	 */
	public IPersonne toPersonneTitulaire() {
		IPersonne titulaire = null;

		if (tosIndividuTitulaire().count() > 0) {
			titulaire = tosIndividuTitulaire().objectAtIndex(0);
		} else if (tosStructureTitulaire().count() > 0) {
			titulaire = tosStructureTitulaire().objectAtIndex(0);
		}

		return titulaire;
	}

	/**
	 * @return
	 */
	public IPersonne toPersonneCible() {
		IPersonne cible = null;

		if (tosIndividuCible().count() > 0) {
			cible = tosIndividuCible().objectAtIndex(0);
		} else if (tosStructureCible().count() > 0) {
			cible = tosStructureCible().objectAtIndex(0);
		}

		return cible;
	}
	
	/**
	 * Indique si la cible est un chef de service. La cible doit être de type
	 * structure et le témoin cdc doit être a oui
	 * 
	 * @return
	 */
	public boolean isCibleChefDeService() {
		boolean isCibleChefDeService = false;

		if (temCibleCdc().equals(OUI) &&
				toPersonneCible().isStructure()) {
			isCibleChefDeService = true;
		}

		return isCibleChefDeService;
	}
	
	/**
	 * Indique si la cible est un service et ses sous services. La cible doit être
	 * de type structure et le témoin heritage doit être a oui
	 * 
	 * @return
	 */
	public boolean isCibleHeritage() {
		boolean isCibleHeritage = false;

		if (temCibleHeritage().equals(OUI) &&
				toPersonneCible().isStructure()) {
			isCibleHeritage = true;
		}

		return isCibleHeritage;
	}
	
	/**
	 * Indique si la cible est un service. La cible doit être de type structure et
	 * le temoin cdc à non.
	 * 
	 * @return
	 */
	public boolean isCibleServiceSimple() {
		boolean isCibleServiceSimple = false;

		if (temCibleCdc().equals(NON) &&
				toPersonneCible().isStructure()) {
			isCibleServiceSimple = true;
		}

		return isCibleServiceSimple;
	}

	public boolean isAnnuaire() {
		return false;
	}
	
	
	/**
	 * @return le globalID de l'objet à partir de l'editingContext associ̩. Null
	 *         si pas d'editingContext.
	 */
	public EOGlobalID globalID() {
		if (editingContext() == null) {
			return null;
		}
		return globalID(this.editingContext());
	}

	/**
	 * @param ec
	 * @return le globalID de l'objet à partir d'ec.
	 */
	public EOGlobalID globalID(EOEditingContext ec) {
		return ec.globalIDForObject(this);
	}
	
	
	public boolean isPersonneCibleStructure() {
		return this.toPersonneCible().isStructure();
	}
	
	public boolean isPersonneCibleIndividu() {
		return this.toPersonneCible().isIndividu();
	}
	
	public static NSArray<EOSortOrdering> getTriParDefaut() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOFeveDroit.TO_PERSONNE_TITULAIRE_KEY + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY, 
				EOSortOrdering.CompareAscending));
	}

	@Override
	public String toString() {
		String toString = "";

		toString += toPersonneTitulaire().getNomPrenomAffichage();
		toString += " ";
		toString += toTypeNiveauDroit().tndLibelle();

		if (toPersonneCible() != null) {
			toString += " sur ";
			toString += toPersonneCible().getNomPrenomAffichage();
		}

		return toString;
	}

	public String libelleTypeNiveauDroit() {
		return this.toTypeNiveauDroit().tndLibelle();
	}
	
	
	public static NSArray<EOFeveDroit> rechercherAllDroitTitulaire(EOEditingContext edc, NSArray<Integer> listeIds) {
		
		String sql = "select d.dro_id  from feve.droit d WHERE d.pers_id_titulaire IN (" + listeIds.componentsJoinedByString(", ") + ")";
		
		@SuppressWarnings("rawtypes")
		NSArray result = EOUtilities.rawRowsForSQL(edc, "FwkCktlGRH", sql.toString(), null);
		
		@SuppressWarnings("unchecked")
		EOQualifier qualifier = EOFeveDroit.DRO_ID.in((NSArray<Integer>) result.valueForKey("DRO_ID"));
		
		NSArray<EOFeveDroit> listeDroit = EOFeveDroit.fetchAll(edc, qualifier, null);
		
		return listeDroit;
	}
	
	
	
}
