/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.CocktailConstantes;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOEtudesRachetees extends _EOEtudesRachetees {
  private static Logger log = Logger.getLogger(EOEtudesRachetees.class);
  
  public static NSArray SORT_DATE_DEBUT_ASC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));

  public EOEtudesRachetees() {
      super();
  }
  
  
  public String dureeLiquidable() {
  	if (erDureeLiquidable() != null) {
  		return StringCtrl.stringCompletion(erDureeLiquidable().toString(), 4, "0", "G");
  	}
  	return null;
  }
  public String dureeAssurance() {
  	if (erDureeConstitutive() != null) {
  		return StringCtrl.stringCompletion(erDureeAssurance().toString(), 4, "0", "G");
  	}
  	return null;
  }
  public String dureeConstitutive() {
  	if (erDureeConstitutive() != null) {
  		return StringCtrl.stringCompletion(erDureeConstitutive().toString(), 4, "0", "G");
  	}
  	return null;
  }
  public String retenuesAcquittees() {
  	if (temPcAcquitees().equals("O")) {
  		return "1";
  	}
  	
  	return "0";
  }
  
  
	public static NSArray findForIndividu(EOEditingContext ec, EOIndividu individu) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));

		return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_DATE_DEBUT_ASC);
	}

/**
 * 
 * @throws NSValidation.ValidationException
 */
public void validateForInsert() throws NSValidation.ValidationException {
    this.validateObjectMetier();
    validateBeforeTransactionSave();
    super.validateForInsert();
}

/**
 * 
 * @throws NSValidation.ValidationException
 */
public void validateForUpdate() throws NSValidation.ValidationException {
    this.validateObjectMetier();
    validateBeforeTransactionSave();
    super.validateForUpdate();
}

/**
 * @throws NSValidation.ValidationException
 */
public void validateForDelete() throws NSValidation.ValidationException {
    super.validateForDelete();
}



/**
 * Peut etre appele à partir des factories.
 * @throws NSValidation.ValidationException
 */
public void validateObjectMetier() throws NSValidation.ValidationException {
	
}

/**
 * A appeler par les validateforsave, forinsert, forupdate.
 *
 */
public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
	
}

}
