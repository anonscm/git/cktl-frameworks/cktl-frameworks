/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPoste.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPoste extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Poste";

	// Attributes
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String POS_CODE_KEY = "posCode";
	public static final String POS_D_DEBUT_KEY = "posDDebut";
	public static final String POS_D_FIN_KEY = "posDFin";
	public static final String POS_LIBELLE_KEY = "posLibelle";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> POS_CODE = new ERXKey<String>("posCode");
	public static final ERXKey<NSTimestamp> POS_D_DEBUT = new ERXKey<NSTimestamp>("posDDebut");
	public static final ERXKey<NSTimestamp> POS_D_FIN = new ERXKey<NSTimestamp>("posDFin");
	public static final ERXKey<String> POS_LIBELLE = new ERXKey<String>("posLibelle");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TOS_AFFECTATION_DETAIL_KEY = "tosAffectationDetail";
	public static final String TOS_FICHE_DE_POSTE_KEY = "tosFicheDePoste";
	public static final String TOS_FICHE_LOLF_KEY = "tosFicheLolf";
	public static final String TO_STRUCTURE_KEY = "toStructure";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail> TOS_AFFECTATION_DETAIL = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail>("tosAffectationDetail");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste> TOS_FICHE_DE_POSTE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste>("tosFicheDePoste");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf> TOS_FICHE_LOLF = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf>("tosFicheLolf");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");

  private static Logger LOG = Logger.getLogger(_EOPoste.class);

  public EOPoste localInstanceIn(EOEditingContext editingContext) {
    EOPoste localInstance = (EOPoste)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cStructure() {
    return (String) storedValueForKey("cStructure");
  }

  public void setCStructure(String value) {
    if (_EOPoste.LOG.isDebugEnabled()) {
    	_EOPoste.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "cStructure");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPoste.LOG.isDebugEnabled()) {
    	_EOPoste.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPoste.LOG.isDebugEnabled()) {
    	_EOPoste.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String posCode() {
    return (String) storedValueForKey("posCode");
  }

  public void setPosCode(String value) {
    if (_EOPoste.LOG.isDebugEnabled()) {
    	_EOPoste.LOG.debug( "updating posCode from " + posCode() + " to " + value);
    }
    takeStoredValueForKey(value, "posCode");
  }

  public NSTimestamp posDDebut() {
    return (NSTimestamp) storedValueForKey("posDDebut");
  }

  public void setPosDDebut(NSTimestamp value) {
    if (_EOPoste.LOG.isDebugEnabled()) {
    	_EOPoste.LOG.debug( "updating posDDebut from " + posDDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "posDDebut");
  }

  public NSTimestamp posDFin() {
    return (NSTimestamp) storedValueForKey("posDFin");
  }

  public void setPosDFin(NSTimestamp value) {
    if (_EOPoste.LOG.isDebugEnabled()) {
    	_EOPoste.LOG.debug( "updating posDFin from " + posDFin() + " to " + value);
    }
    takeStoredValueForKey(value, "posDFin");
  }

  public String posLibelle() {
    return (String) storedValueForKey("posLibelle");
  }

  public void setPosLibelle(String value) {
    if (_EOPoste.LOG.isDebugEnabled()) {
    	_EOPoste.LOG.debug( "updating posLibelle from " + posLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "posLibelle");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOPoste.LOG.isDebugEnabled()) {
    	_EOPoste.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey("toStructure");
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOPoste.LOG.isDebugEnabled()) {
      _EOPoste.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructure");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail> tosAffectationDetail() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail>)storedValueForKey("tosAffectationDetail");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail> tosAffectationDetail(EOQualifier qualifier) {
    return tosAffectationDetail(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail> tosAffectationDetail(EOQualifier qualifier, boolean fetch) {
    return tosAffectationDetail(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail> tosAffectationDetail(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail.TO_POSTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosAffectationDetail();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosAffectationDetailRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail object) {
    if (_EOPoste.LOG.isDebugEnabled()) {
      _EOPoste.LOG.debug("adding " + object + " to tosAffectationDetail relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosAffectationDetail");
  }

  public void removeFromTosAffectationDetailRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail object) {
    if (_EOPoste.LOG.isDebugEnabled()) {
      _EOPoste.LOG.debug("removing " + object + " from tosAffectationDetail relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosAffectationDetail");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail createTosAffectationDetailRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_AffectationDetail");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosAffectationDetail");
    return (org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail) eo;
  }

  public void deleteTosAffectationDetailRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosAffectationDetail");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosAffectationDetailRelationships() {
    Enumeration objects = tosAffectationDetail().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosAffectationDetailRelationship((org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste> tosFicheDePoste() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste>)storedValueForKey("tosFicheDePoste");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste> tosFicheDePoste(EOQualifier qualifier) {
    return tosFicheDePoste(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste> tosFicheDePoste(EOQualifier qualifier, boolean fetch) {
    return tosFicheDePoste(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste> tosFicheDePoste(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste.TO_POSTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosFicheDePoste();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosFicheDePosteRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste object) {
    if (_EOPoste.LOG.isDebugEnabled()) {
      _EOPoste.LOG.debug("adding " + object + " to tosFicheDePoste relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosFicheDePoste");
  }

  public void removeFromTosFicheDePosteRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste object) {
    if (_EOPoste.LOG.isDebugEnabled()) {
      _EOPoste.LOG.debug("removing " + object + " from tosFicheDePoste relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosFicheDePoste");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste createTosFicheDePosteRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_FicheDePoste");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosFicheDePoste");
    return (org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste) eo;
  }

  public void deleteTosFicheDePosteRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosFicheDePoste");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosFicheDePosteRelationships() {
    Enumeration objects = tosFicheDePoste().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosFicheDePosteRelationship((org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf> tosFicheLolf() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf>)storedValueForKey("tosFicheLolf");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf> tosFicheLolf(EOQualifier qualifier) {
    return tosFicheLolf(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf> tosFicheLolf(EOQualifier qualifier, boolean fetch) {
    return tosFicheLolf(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf> tosFicheLolf(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf.TO_POSTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosFicheLolf();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosFicheLolfRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf object) {
    if (_EOPoste.LOG.isDebugEnabled()) {
      _EOPoste.LOG.debug("adding " + object + " to tosFicheLolf relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosFicheLolf");
  }

  public void removeFromTosFicheLolfRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf object) {
    if (_EOPoste.LOG.isDebugEnabled()) {
      _EOPoste.LOG.debug("removing " + object + " from tosFicheLolf relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosFicheLolf");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf createTosFicheLolfRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_FicheLolf");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosFicheLolf");
    return (org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf) eo;
  }

  public void deleteTosFicheLolfRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosFicheLolf");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosFicheLolfRelationships() {
    Enumeration objects = tosFicheLolf().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosFicheLolfRelationship((org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf)objects.nextElement());
    }
  }


  public static EOPoste create(EOEditingContext editingContext, String cStructure
, NSTimestamp dCreation
, NSTimestamp dModification
, String posCode
, NSTimestamp posDDebut
, String posLibelle
, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure) {
    EOPoste eo = (EOPoste) EOUtilities.createAndInsertInstance(editingContext, _EOPoste.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPosCode(posCode);
		eo.setPosDDebut(posDDebut);
		eo.setPosLibelle(posLibelle);
    eo.setToStructureRelationship(toStructure);
    return eo;
  }

  public static NSArray<EOPoste> fetchAll(EOEditingContext editingContext) {
    return _EOPoste.fetchAll(editingContext, null);
  }

  public static NSArray<EOPoste> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPoste.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPoste> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPoste.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPoste> eoObjects = (NSArray<EOPoste>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPoste fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPoste.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPoste fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPoste> eoObjects = _EOPoste.fetch(editingContext, qualifier, null);
    EOPoste eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPoste)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Poste that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPoste fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPoste.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPoste fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPoste eoObject = _EOPoste.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Poste that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPoste eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPoste)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPoste localInstanceIn(EOEditingContext editingContext, EOPoste eo) {
    EOPoste localInstance = (eo == null) ? null : (EOPoste)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
  public static NSArray<org.cocktail.fwkcktlgrh.common.metier.EOPoste> fetchFetchPoste(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchPoste", "Fwkgrh_Poste");
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.fwkcktlgrh.common.metier.EOPoste> fetchFetchPoste(EOEditingContext editingContext,
	String cStructureBinding,
	String cStructure00Binding,
	String cStructure01Binding,
	String cStructure02Binding,
	String cStructure03Binding,
	String cStructure04Binding,
	String cStructure05Binding,
	String cStructure06Binding,
	String cStructure07Binding,
	String cStructure08Binding,
	String cStructure09Binding,
	String cStructure10Binding,
	String cStructure11Binding,
	String cStructure12Binding,
	String cStructure13Binding,
	String cStructure14Binding,
	String cStructure15Binding,
	String cStructure16Binding,
	String cStructure17Binding,
	String cStructure18Binding,
	String cStructure19Binding,
	String cStructure20Binding,
	String cStructure21Binding,
	String cStructure22Binding,
	String cStructure23Binding,
	String cStructure24Binding,
	String cStructure25Binding,
	String cStructure26Binding,
	String cStructure27Binding,
	String cStructure28Binding,
	String cStructure29Binding,
	String cStructure30Binding,
	String cStructure31Binding,
	String cStructure32Binding,
	String cStructure33Binding,
	String cStructure34Binding,
	String cStructure35Binding,
	String cStructure36Binding,
	String cStructure37Binding,
	String cStructure38Binding,
	String cStructure39Binding,
	String cStructure40Binding,
	String cStructure41Binding,
	String cStructure42Binding,
	String cStructure43Binding,
	String cStructure44Binding,
	String cStructure45Binding,
	String cStructure46Binding,
	String cStructure47Binding,
	String cStructure48Binding,
	String cStructure49Binding,
	String cStructure50Binding,
	String cStructure51Binding,
	String cStructure52Binding,
	String cStructure53Binding,
	String cStructure54Binding,
	String cStructure55Binding,
	String cStructure56Binding,
	String cStructure57Binding,
	String cStructure58Binding,
	String cStructure59Binding,
	String cStructure60Binding,
	String cStructure61Binding,
	String cStructure62Binding,
	String cStructure63Binding,
	String cStructure64Binding,
	String cStructure65Binding,
	String cStructure66Binding,
	String cStructure67Binding,
	String cStructure68Binding,
	String cStructure69Binding,
	String cStructure70Binding,
	String cStructure71Binding,
	String cStructure72Binding,
	String cStructure73Binding,
	String cStructure74Binding,
	String cStructure75Binding,
	String cStructure76Binding,
	String cStructure77Binding,
	String cStructure78Binding,
	String cStructure79Binding,
	String cStructure80Binding,
	String cStructure81Binding,
	String cStructure82Binding,
	String cStructure83Binding,
	String cStructure84Binding,
	String cStructure85Binding,
	String cStructure86Binding,
	String cStructure87Binding,
	String cStructure88Binding,
	String cStructure89Binding,
	String cStructure90Binding,
	String cStructure91Binding,
	String cStructure92Binding,
	String cStructure93Binding,
	String cStructure94Binding,
	String cStructure95Binding,
	String cStructure96Binding,
	String cStructure97Binding,
	String cStructure98Binding,
	String cStructure99Binding,
	String mot0Binding,
	String mot1Binding,
	String mot2Binding,
	Integer posKey000Binding,
	Integer posKey001Binding,
	Integer posKey002Binding,
	Integer posKey003Binding,
	Integer posKey004Binding,
	Integer posKey005Binding,
	Integer posKey006Binding,
	Integer posKey007Binding,
	Integer posKey008Binding,
	Integer posKey009Binding,
	Integer posKey010Binding,
	Integer posKey011Binding,
	Integer posKey012Binding,
	Integer posKey013Binding,
	Integer posKey014Binding,
	Integer posKey015Binding,
	Integer posKey016Binding,
	Integer posKey017Binding,
	Integer posKey018Binding,
	Integer posKey019Binding,
	Integer posKey020Binding,
	Integer posKey021Binding,
	Integer posKey022Binding,
	Integer posKey023Binding,
	Integer posKey024Binding,
	Integer posKey025Binding,
	Integer posKey026Binding,
	Integer posKey027Binding,
	Integer posKey028Binding,
	Integer posKey029Binding,
	Integer posKey030Binding,
	Integer posKey031Binding,
	Integer posKey032Binding,
	Integer posKey033Binding,
	Integer posKey034Binding,
	Integer posKey035Binding,
	Integer posKey036Binding,
	Integer posKey037Binding,
	Integer posKey038Binding,
	Integer posKey039Binding,
	Integer posKey040Binding,
	Integer posKey041Binding,
	Integer posKey042Binding,
	Integer posKey043Binding,
	Integer posKey044Binding,
	Integer posKey045Binding,
	Integer posKey046Binding,
	Integer posKey047Binding,
	Integer posKey048Binding,
	Integer posKey049Binding,
	Integer posKey050Binding,
	Integer posKey051Binding,
	Integer posKey052Binding,
	Integer posKey053Binding,
	Integer posKey054Binding,
	Integer posKey055Binding,
	Integer posKey056Binding,
	Integer posKey057Binding,
	Integer posKey058Binding,
	Integer posKey059Binding,
	Integer posKey060Binding,
	Integer posKey061Binding,
	Integer posKey062Binding,
	Integer posKey063Binding,
	Integer posKey064Binding,
	Integer posKey065Binding,
	Integer posKey066Binding,
	Integer posKey067Binding,
	Integer posKey068Binding,
	Integer posKey069Binding,
	Integer posKey070Binding,
	Integer posKey071Binding,
	Integer posKey072Binding,
	Integer posKey073Binding,
	Integer posKey074Binding,
	Integer posKey075Binding,
	Integer posKey076Binding,
	Integer posKey077Binding,
	Integer posKey078Binding,
	Integer posKey079Binding,
	Integer posKey080Binding,
	Integer posKey081Binding,
	Integer posKey082Binding,
	Integer posKey083Binding,
	Integer posKey084Binding,
	Integer posKey085Binding,
	Integer posKey086Binding,
	Integer posKey087Binding,
	Integer posKey088Binding,
	Integer posKey089Binding,
	Integer posKey090Binding,
	Integer posKey091Binding,
	Integer posKey092Binding,
	Integer posKey093Binding,
	Integer posKey094Binding,
	Integer posKey095Binding,
	Integer posKey096Binding,
	Integer posKey097Binding,
	Integer posKey098Binding,
	Integer posKey099Binding)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchPoste", "Fwkgrh_Poste");
    NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
    bindings.takeValueForKey(cStructureBinding, "cStructure");
    bindings.takeValueForKey(cStructure00Binding, "cStructure00");
    bindings.takeValueForKey(cStructure01Binding, "cStructure01");
    bindings.takeValueForKey(cStructure02Binding, "cStructure02");
    bindings.takeValueForKey(cStructure03Binding, "cStructure03");
    bindings.takeValueForKey(cStructure04Binding, "cStructure04");
    bindings.takeValueForKey(cStructure05Binding, "cStructure05");
    bindings.takeValueForKey(cStructure06Binding, "cStructure06");
    bindings.takeValueForKey(cStructure07Binding, "cStructure07");
    bindings.takeValueForKey(cStructure08Binding, "cStructure08");
    bindings.takeValueForKey(cStructure09Binding, "cStructure09");
    bindings.takeValueForKey(cStructure10Binding, "cStructure10");
    bindings.takeValueForKey(cStructure11Binding, "cStructure11");
    bindings.takeValueForKey(cStructure12Binding, "cStructure12");
    bindings.takeValueForKey(cStructure13Binding, "cStructure13");
    bindings.takeValueForKey(cStructure14Binding, "cStructure14");
    bindings.takeValueForKey(cStructure15Binding, "cStructure15");
    bindings.takeValueForKey(cStructure16Binding, "cStructure16");
    bindings.takeValueForKey(cStructure17Binding, "cStructure17");
    bindings.takeValueForKey(cStructure18Binding, "cStructure18");
    bindings.takeValueForKey(cStructure19Binding, "cStructure19");
    bindings.takeValueForKey(cStructure20Binding, "cStructure20");
    bindings.takeValueForKey(cStructure21Binding, "cStructure21");
    bindings.takeValueForKey(cStructure22Binding, "cStructure22");
    bindings.takeValueForKey(cStructure23Binding, "cStructure23");
    bindings.takeValueForKey(cStructure24Binding, "cStructure24");
    bindings.takeValueForKey(cStructure25Binding, "cStructure25");
    bindings.takeValueForKey(cStructure26Binding, "cStructure26");
    bindings.takeValueForKey(cStructure27Binding, "cStructure27");
    bindings.takeValueForKey(cStructure28Binding, "cStructure28");
    bindings.takeValueForKey(cStructure29Binding, "cStructure29");
    bindings.takeValueForKey(cStructure30Binding, "cStructure30");
    bindings.takeValueForKey(cStructure31Binding, "cStructure31");
    bindings.takeValueForKey(cStructure32Binding, "cStructure32");
    bindings.takeValueForKey(cStructure33Binding, "cStructure33");
    bindings.takeValueForKey(cStructure34Binding, "cStructure34");
    bindings.takeValueForKey(cStructure35Binding, "cStructure35");
    bindings.takeValueForKey(cStructure36Binding, "cStructure36");
    bindings.takeValueForKey(cStructure37Binding, "cStructure37");
    bindings.takeValueForKey(cStructure38Binding, "cStructure38");
    bindings.takeValueForKey(cStructure39Binding, "cStructure39");
    bindings.takeValueForKey(cStructure40Binding, "cStructure40");
    bindings.takeValueForKey(cStructure41Binding, "cStructure41");
    bindings.takeValueForKey(cStructure42Binding, "cStructure42");
    bindings.takeValueForKey(cStructure43Binding, "cStructure43");
    bindings.takeValueForKey(cStructure44Binding, "cStructure44");
    bindings.takeValueForKey(cStructure45Binding, "cStructure45");
    bindings.takeValueForKey(cStructure46Binding, "cStructure46");
    bindings.takeValueForKey(cStructure47Binding, "cStructure47");
    bindings.takeValueForKey(cStructure48Binding, "cStructure48");
    bindings.takeValueForKey(cStructure49Binding, "cStructure49");
    bindings.takeValueForKey(cStructure50Binding, "cStructure50");
    bindings.takeValueForKey(cStructure51Binding, "cStructure51");
    bindings.takeValueForKey(cStructure52Binding, "cStructure52");
    bindings.takeValueForKey(cStructure53Binding, "cStructure53");
    bindings.takeValueForKey(cStructure54Binding, "cStructure54");
    bindings.takeValueForKey(cStructure55Binding, "cStructure55");
    bindings.takeValueForKey(cStructure56Binding, "cStructure56");
    bindings.takeValueForKey(cStructure57Binding, "cStructure57");
    bindings.takeValueForKey(cStructure58Binding, "cStructure58");
    bindings.takeValueForKey(cStructure59Binding, "cStructure59");
    bindings.takeValueForKey(cStructure60Binding, "cStructure60");
    bindings.takeValueForKey(cStructure61Binding, "cStructure61");
    bindings.takeValueForKey(cStructure62Binding, "cStructure62");
    bindings.takeValueForKey(cStructure63Binding, "cStructure63");
    bindings.takeValueForKey(cStructure64Binding, "cStructure64");
    bindings.takeValueForKey(cStructure65Binding, "cStructure65");
    bindings.takeValueForKey(cStructure66Binding, "cStructure66");
    bindings.takeValueForKey(cStructure67Binding, "cStructure67");
    bindings.takeValueForKey(cStructure68Binding, "cStructure68");
    bindings.takeValueForKey(cStructure69Binding, "cStructure69");
    bindings.takeValueForKey(cStructure70Binding, "cStructure70");
    bindings.takeValueForKey(cStructure71Binding, "cStructure71");
    bindings.takeValueForKey(cStructure72Binding, "cStructure72");
    bindings.takeValueForKey(cStructure73Binding, "cStructure73");
    bindings.takeValueForKey(cStructure74Binding, "cStructure74");
    bindings.takeValueForKey(cStructure75Binding, "cStructure75");
    bindings.takeValueForKey(cStructure76Binding, "cStructure76");
    bindings.takeValueForKey(cStructure77Binding, "cStructure77");
    bindings.takeValueForKey(cStructure78Binding, "cStructure78");
    bindings.takeValueForKey(cStructure79Binding, "cStructure79");
    bindings.takeValueForKey(cStructure80Binding, "cStructure80");
    bindings.takeValueForKey(cStructure81Binding, "cStructure81");
    bindings.takeValueForKey(cStructure82Binding, "cStructure82");
    bindings.takeValueForKey(cStructure83Binding, "cStructure83");
    bindings.takeValueForKey(cStructure84Binding, "cStructure84");
    bindings.takeValueForKey(cStructure85Binding, "cStructure85");
    bindings.takeValueForKey(cStructure86Binding, "cStructure86");
    bindings.takeValueForKey(cStructure87Binding, "cStructure87");
    bindings.takeValueForKey(cStructure88Binding, "cStructure88");
    bindings.takeValueForKey(cStructure89Binding, "cStructure89");
    bindings.takeValueForKey(cStructure90Binding, "cStructure90");
    bindings.takeValueForKey(cStructure91Binding, "cStructure91");
    bindings.takeValueForKey(cStructure92Binding, "cStructure92");
    bindings.takeValueForKey(cStructure93Binding, "cStructure93");
    bindings.takeValueForKey(cStructure94Binding, "cStructure94");
    bindings.takeValueForKey(cStructure95Binding, "cStructure95");
    bindings.takeValueForKey(cStructure96Binding, "cStructure96");
    bindings.takeValueForKey(cStructure97Binding, "cStructure97");
    bindings.takeValueForKey(cStructure98Binding, "cStructure98");
    bindings.takeValueForKey(cStructure99Binding, "cStructure99");
    bindings.takeValueForKey(mot0Binding, "mot0");
    bindings.takeValueForKey(mot1Binding, "mot1");
    bindings.takeValueForKey(mot2Binding, "mot2");
    bindings.takeValueForKey(posKey000Binding, "posKey000");
    bindings.takeValueForKey(posKey001Binding, "posKey001");
    bindings.takeValueForKey(posKey002Binding, "posKey002");
    bindings.takeValueForKey(posKey003Binding, "posKey003");
    bindings.takeValueForKey(posKey004Binding, "posKey004");
    bindings.takeValueForKey(posKey005Binding, "posKey005");
    bindings.takeValueForKey(posKey006Binding, "posKey006");
    bindings.takeValueForKey(posKey007Binding, "posKey007");
    bindings.takeValueForKey(posKey008Binding, "posKey008");
    bindings.takeValueForKey(posKey009Binding, "posKey009");
    bindings.takeValueForKey(posKey010Binding, "posKey010");
    bindings.takeValueForKey(posKey011Binding, "posKey011");
    bindings.takeValueForKey(posKey012Binding, "posKey012");
    bindings.takeValueForKey(posKey013Binding, "posKey013");
    bindings.takeValueForKey(posKey014Binding, "posKey014");
    bindings.takeValueForKey(posKey015Binding, "posKey015");
    bindings.takeValueForKey(posKey016Binding, "posKey016");
    bindings.takeValueForKey(posKey017Binding, "posKey017");
    bindings.takeValueForKey(posKey018Binding, "posKey018");
    bindings.takeValueForKey(posKey019Binding, "posKey019");
    bindings.takeValueForKey(posKey020Binding, "posKey020");
    bindings.takeValueForKey(posKey021Binding, "posKey021");
    bindings.takeValueForKey(posKey022Binding, "posKey022");
    bindings.takeValueForKey(posKey023Binding, "posKey023");
    bindings.takeValueForKey(posKey024Binding, "posKey024");
    bindings.takeValueForKey(posKey025Binding, "posKey025");
    bindings.takeValueForKey(posKey026Binding, "posKey026");
    bindings.takeValueForKey(posKey027Binding, "posKey027");
    bindings.takeValueForKey(posKey028Binding, "posKey028");
    bindings.takeValueForKey(posKey029Binding, "posKey029");
    bindings.takeValueForKey(posKey030Binding, "posKey030");
    bindings.takeValueForKey(posKey031Binding, "posKey031");
    bindings.takeValueForKey(posKey032Binding, "posKey032");
    bindings.takeValueForKey(posKey033Binding, "posKey033");
    bindings.takeValueForKey(posKey034Binding, "posKey034");
    bindings.takeValueForKey(posKey035Binding, "posKey035");
    bindings.takeValueForKey(posKey036Binding, "posKey036");
    bindings.takeValueForKey(posKey037Binding, "posKey037");
    bindings.takeValueForKey(posKey038Binding, "posKey038");
    bindings.takeValueForKey(posKey039Binding, "posKey039");
    bindings.takeValueForKey(posKey040Binding, "posKey040");
    bindings.takeValueForKey(posKey041Binding, "posKey041");
    bindings.takeValueForKey(posKey042Binding, "posKey042");
    bindings.takeValueForKey(posKey043Binding, "posKey043");
    bindings.takeValueForKey(posKey044Binding, "posKey044");
    bindings.takeValueForKey(posKey045Binding, "posKey045");
    bindings.takeValueForKey(posKey046Binding, "posKey046");
    bindings.takeValueForKey(posKey047Binding, "posKey047");
    bindings.takeValueForKey(posKey048Binding, "posKey048");
    bindings.takeValueForKey(posKey049Binding, "posKey049");
    bindings.takeValueForKey(posKey050Binding, "posKey050");
    bindings.takeValueForKey(posKey051Binding, "posKey051");
    bindings.takeValueForKey(posKey052Binding, "posKey052");
    bindings.takeValueForKey(posKey053Binding, "posKey053");
    bindings.takeValueForKey(posKey054Binding, "posKey054");
    bindings.takeValueForKey(posKey055Binding, "posKey055");
    bindings.takeValueForKey(posKey056Binding, "posKey056");
    bindings.takeValueForKey(posKey057Binding, "posKey057");
    bindings.takeValueForKey(posKey058Binding, "posKey058");
    bindings.takeValueForKey(posKey059Binding, "posKey059");
    bindings.takeValueForKey(posKey060Binding, "posKey060");
    bindings.takeValueForKey(posKey061Binding, "posKey061");
    bindings.takeValueForKey(posKey062Binding, "posKey062");
    bindings.takeValueForKey(posKey063Binding, "posKey063");
    bindings.takeValueForKey(posKey064Binding, "posKey064");
    bindings.takeValueForKey(posKey065Binding, "posKey065");
    bindings.takeValueForKey(posKey066Binding, "posKey066");
    bindings.takeValueForKey(posKey067Binding, "posKey067");
    bindings.takeValueForKey(posKey068Binding, "posKey068");
    bindings.takeValueForKey(posKey069Binding, "posKey069");
    bindings.takeValueForKey(posKey070Binding, "posKey070");
    bindings.takeValueForKey(posKey071Binding, "posKey071");
    bindings.takeValueForKey(posKey072Binding, "posKey072");
    bindings.takeValueForKey(posKey073Binding, "posKey073");
    bindings.takeValueForKey(posKey074Binding, "posKey074");
    bindings.takeValueForKey(posKey075Binding, "posKey075");
    bindings.takeValueForKey(posKey076Binding, "posKey076");
    bindings.takeValueForKey(posKey077Binding, "posKey077");
    bindings.takeValueForKey(posKey078Binding, "posKey078");
    bindings.takeValueForKey(posKey079Binding, "posKey079");
    bindings.takeValueForKey(posKey080Binding, "posKey080");
    bindings.takeValueForKey(posKey081Binding, "posKey081");
    bindings.takeValueForKey(posKey082Binding, "posKey082");
    bindings.takeValueForKey(posKey083Binding, "posKey083");
    bindings.takeValueForKey(posKey084Binding, "posKey084");
    bindings.takeValueForKey(posKey085Binding, "posKey085");
    bindings.takeValueForKey(posKey086Binding, "posKey086");
    bindings.takeValueForKey(posKey087Binding, "posKey087");
    bindings.takeValueForKey(posKey088Binding, "posKey088");
    bindings.takeValueForKey(posKey089Binding, "posKey089");
    bindings.takeValueForKey(posKey090Binding, "posKey090");
    bindings.takeValueForKey(posKey091Binding, "posKey091");
    bindings.takeValueForKey(posKey092Binding, "posKey092");
    bindings.takeValueForKey(posKey093Binding, "posKey093");
    bindings.takeValueForKey(posKey094Binding, "posKey094");
    bindings.takeValueForKey(posKey095Binding, "posKey095");
    bindings.takeValueForKey(posKey096Binding, "posKey096");
    bindings.takeValueForKey(posKey097Binding, "posKey097");
    bindings.takeValueForKey(posKey098Binding, "posKey098");
    bindings.takeValueForKey(posKey099Binding, "posKey099");
	fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.fwkcktlgrh.common.metier.EOPoste> fetchFetchSuivi(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchSuivi", "Fwkgrh_Poste");
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.fwkcktlgrh.common.metier.EOPoste> fetchFetchSuivi(EOEditingContext editingContext,
	String cStructureBinding)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchSuivi", "Fwkgrh_Poste");
    NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
    bindings.takeValueForKey(cStructureBinding, "cStructure");
	fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
}
