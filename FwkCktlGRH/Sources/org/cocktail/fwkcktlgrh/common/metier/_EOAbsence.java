/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOAbsence.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOAbsence extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Absence";

	// Attributes
	public static final String ABSENCE_AMPM_DEBUT_KEY = "absenceAmpmDebut";
	public static final String ABSENCE_AMPM_FIN_KEY = "absenceAmpmFin";
	public static final String C_TYPE_ABSENCE_KEY = "cTypeAbsence";
	public static final String C_TYPE_EXCLUSION_KEY = "cTypeExclusion";
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String DUREE_HEURES_KEY = "dureeHeures";
	public static final String DUREE_TOTALE_KEY = "dureeTotale";
	public static final String MOTIF_KEY = "motif";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String TEM_ACTIVE_KEY = "temActive";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> ABSENCE_AMPM_DEBUT = new ERXKey<String>("absenceAmpmDebut");
	public static final ERXKey<String> ABSENCE_AMPM_FIN = new ERXKey<String>("absenceAmpmFin");
	public static final ERXKey<String> C_TYPE_ABSENCE = new ERXKey<String>("cTypeAbsence");
	public static final ERXKey<String> C_TYPE_EXCLUSION = new ERXKey<String>("cTypeExclusion");
	public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
	public static final ERXKey<java.math.BigDecimal> DUREE_HEURES = new ERXKey<java.math.BigDecimal>("dureeHeures");
	public static final ERXKey<String> DUREE_TOTALE = new ERXKey<String>("dureeTotale");
	public static final ERXKey<String> MOTIF = new ERXKey<String>("motif");
	public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
	public static final ERXKey<String> TEM_ACTIVE = new ERXKey<String>("temActive");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_TYPE_EXCLUSION_KEY = "toTypeExclusion";
	public static final String TYPE_ABSENCE_KEY = "typeAbsence";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("individu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion> TO_TYPE_EXCLUSION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion>("toTypeExclusion");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence> TYPE_ABSENCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence>("typeAbsence");

  private static Logger LOG = Logger.getLogger(_EOAbsence.class);

  public EOAbsence localInstanceIn(EOEditingContext editingContext) {
    EOAbsence localInstance = (EOAbsence)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String absenceAmpmDebut() {
    return (String) storedValueForKey("absenceAmpmDebut");
  }

  public void setAbsenceAmpmDebut(String value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating absenceAmpmDebut from " + absenceAmpmDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "absenceAmpmDebut");
  }

  public String absenceAmpmFin() {
    return (String) storedValueForKey("absenceAmpmFin");
  }

  public void setAbsenceAmpmFin(String value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating absenceAmpmFin from " + absenceAmpmFin() + " to " + value);
    }
    takeStoredValueForKey(value, "absenceAmpmFin");
  }

  public String cTypeAbsence() {
    return (String) storedValueForKey("cTypeAbsence");
  }

  public void setCTypeAbsence(String value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating cTypeAbsence from " + cTypeAbsence() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeAbsence");
  }

  public String cTypeExclusion() {
    return (String) storedValueForKey("cTypeExclusion");
  }

  public void setCTypeExclusion(String value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating cTypeExclusion from " + cTypeExclusion() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeExclusion");
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey("dateCreation");
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dateCreation");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey("dateModification");
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dateModification");
  }

  public java.math.BigDecimal dureeHeures() {
    return (java.math.BigDecimal) storedValueForKey("dureeHeures");
  }

  public void setDureeHeures(java.math.BigDecimal value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating dureeHeures from " + dureeHeures() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeHeures");
  }

  public String dureeTotale() {
    return (String) storedValueForKey("dureeTotale");
  }

  public void setDureeTotale(String value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating dureeTotale from " + dureeTotale() + " to " + value);
    }
    takeStoredValueForKey(value, "dureeTotale");
  }

  public String motif() {
    return (String) storedValueForKey("motif");
  }

  public void setMotif(String value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating motif from " + motif() + " to " + value);
    }
    takeStoredValueForKey(value, "motif");
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey("noIndividu");
  }

  public void setNoIndividu(Integer value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating noIndividu from " + noIndividu() + " to " + value);
    }
    takeStoredValueForKey(value, "noIndividu");
  }

  public String temActive() {
    return (String) storedValueForKey("temActive");
  }

  public void setTemActive(String value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating temActive from " + temActive() + " to " + value);
    }
    takeStoredValueForKey(value, "temActive");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
    	_EOAbsence.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
      _EOAbsence.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion toTypeExclusion() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion)storedValueForKey("toTypeExclusion");
  }

  public void setToTypeExclusionRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
      _EOAbsence.LOG.debug("updating toTypeExclusion from " + toTypeExclusion() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion oldValue = toTypeExclusion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeExclusion");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeExclusion");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence typeAbsence() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence)storedValueForKey("typeAbsence");
  }

  public void setTypeAbsenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence value) {
    if (_EOAbsence.LOG.isDebugEnabled()) {
      _EOAbsence.LOG.debug("updating typeAbsence from " + typeAbsence() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence oldValue = typeAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "typeAbsence");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "typeAbsence");
    }
  }
  

  public static EOAbsence create(EOEditingContext editingContext, NSTimestamp dateDebut
, Integer noIndividu
, String temActive
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu) {
    EOAbsence eo = (EOAbsence) EOUtilities.createAndInsertInstance(editingContext, _EOAbsence.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setNoIndividu(noIndividu);
		eo.setTemActive(temActive);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOAbsence> fetchAll(EOEditingContext editingContext) {
    return _EOAbsence.fetchAll(editingContext, null);
  }

  public static NSArray<EOAbsence> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAbsence.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOAbsence> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOAbsence.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAbsence> eoObjects = (NSArray<EOAbsence>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOAbsence fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAbsence.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAbsence fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAbsence> eoObjects = _EOAbsence.fetch(editingContext, qualifier, null);
    EOAbsence eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOAbsence)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Absence that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAbsence fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAbsence.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOAbsence fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAbsence eoObject = _EOAbsence.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Absence that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOAbsence fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAbsence fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAbsence eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAbsence)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOAbsence localInstanceIn(EOEditingContext editingContext, EOAbsence eo) {
    EOAbsence localInstance = (eo == null) ? null : (EOAbsence)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
