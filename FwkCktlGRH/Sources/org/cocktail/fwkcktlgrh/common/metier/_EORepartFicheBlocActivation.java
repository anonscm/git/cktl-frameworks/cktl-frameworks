/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartFicheBlocActivation.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartFicheBlocActivation extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_RepartFicheBlocActivation";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	// Relationships
	public static final String TO_EVALUATION_KEY = "toEvaluation";
	public static final String TO_FICHE_DE_POSTE_KEY = "toFicheDePoste";
	public static final String TO_FICHE_LOLF_KEY = "toFicheLolf";
	public static final String TO_TPL_BLOC_KEY = "toTplBloc";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> TO_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>("toEvaluation");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste> TO_FICHE_DE_POSTE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste>("toFicheDePoste");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf> TO_FICHE_LOLF = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf>("toFicheLolf");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc> TO_TPL_BLOC = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc>("toTplBloc");

  private static Logger LOG = Logger.getLogger(_EORepartFicheBlocActivation.class);

  public EORepartFicheBlocActivation localInstanceIn(EOEditingContext editingContext) {
    EORepartFicheBlocActivation localInstance = (EORepartFicheBlocActivation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartFicheBlocActivation.LOG.isDebugEnabled()) {
    	_EORepartFicheBlocActivation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartFicheBlocActivation.LOG.isDebugEnabled()) {
    	_EORepartFicheBlocActivation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluation)storedValueForKey("toEvaluation");
  }

  public void setToEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation value) {
    if (_EORepartFicheBlocActivation.LOG.isDebugEnabled()) {
      _EORepartFicheBlocActivation.LOG.debug("updating toEvaluation from " + toEvaluation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluation oldValue = toEvaluation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluation");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste toFicheDePoste() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste)storedValueForKey("toFicheDePoste");
  }

  public void setToFicheDePosteRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste value) {
    if (_EORepartFicheBlocActivation.LOG.isDebugEnabled()) {
      _EORepartFicheBlocActivation.LOG.debug("updating toFicheDePoste from " + toFicheDePoste() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste oldValue = toFicheDePoste();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFicheDePoste");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFicheDePoste");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf toFicheLolf() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf)storedValueForKey("toFicheLolf");
  }

  public void setToFicheLolfRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf value) {
    if (_EORepartFicheBlocActivation.LOG.isDebugEnabled()) {
      _EORepartFicheBlocActivation.LOG.debug("updating toFicheLolf from " + toFicheLolf() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf oldValue = toFicheLolf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFicheLolf");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFicheLolf");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOTplBloc toTplBloc() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplBloc)storedValueForKey("toTplBloc");
  }

  public void setToTplBlocRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplBloc value) {
    if (_EORepartFicheBlocActivation.LOG.isDebugEnabled()) {
      _EORepartFicheBlocActivation.LOG.debug("updating toTplBloc from " + toTplBloc() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOTplBloc oldValue = toTplBloc();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTplBloc");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTplBloc");
    }
  }
  

  public static EORepartFicheBlocActivation create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlgrh.common.metier.EOTplBloc toTplBloc) {
    EORepartFicheBlocActivation eo = (EORepartFicheBlocActivation) EOUtilities.createAndInsertInstance(editingContext, _EORepartFicheBlocActivation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToTplBlocRelationship(toTplBloc);
    return eo;
  }

  public static NSArray<EORepartFicheBlocActivation> fetchAll(EOEditingContext editingContext) {
    return _EORepartFicheBlocActivation.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartFicheBlocActivation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartFicheBlocActivation.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartFicheBlocActivation> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartFicheBlocActivation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartFicheBlocActivation> eoObjects = (NSArray<EORepartFicheBlocActivation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartFicheBlocActivation fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartFicheBlocActivation.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartFicheBlocActivation fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartFicheBlocActivation> eoObjects = _EORepartFicheBlocActivation.fetch(editingContext, qualifier, null);
    EORepartFicheBlocActivation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartFicheBlocActivation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartFicheBlocActivation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartFicheBlocActivation fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartFicheBlocActivation.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartFicheBlocActivation fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartFicheBlocActivation eoObject = _EORepartFicheBlocActivation.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartFicheBlocActivation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartFicheBlocActivation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartFicheBlocActivation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartFicheBlocActivation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartFicheBlocActivation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartFicheBlocActivation localInstanceIn(EOEditingContext editingContext, EORepartFicheBlocActivation eo) {
    EORepartFicheBlocActivation localInstance = (eo == null) ? null : (EORepartFicheBlocActivation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
