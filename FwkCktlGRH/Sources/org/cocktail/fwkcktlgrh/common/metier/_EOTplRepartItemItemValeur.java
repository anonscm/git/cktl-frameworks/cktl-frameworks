/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOTplRepartItemItemValeur.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOTplRepartItemItemValeur extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_TplRepartItemItemValeur";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TIV_POSITION_KEY = "tivPosition";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_VAL = new ERXKey<NSTimestamp>("dDebVal");
	public static final ERXKey<NSTimestamp> D_FIN_VAL = new ERXKey<NSTimestamp>("dFinVal");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> TIV_POSITION = new ERXKey<Integer>("tivPosition");
	// Relationships
	public static final String TO_TPL_ITEM_KEY = "toTplItem";
	public static final String TO_TPL_ITEM_VALEUR_KEY = "toTplItemValeur";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplItem> TO_TPL_ITEM = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplItem>("toTplItem");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur> TO_TPL_ITEM_VALEUR = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur>("toTplItemValeur");

  private static Logger LOG = Logger.getLogger(_EOTplRepartItemItemValeur.class);

  public EOTplRepartItemItemValeur localInstanceIn(EOEditingContext editingContext) {
    EOTplRepartItemItemValeur localInstance = (EOTplRepartItemItemValeur)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTplRepartItemItemValeur.LOG.isDebugEnabled()) {
    	_EOTplRepartItemItemValeur.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey("dDebVal");
  }

  public void setDDebVal(NSTimestamp value) {
    if (_EOTplRepartItemItemValeur.LOG.isDebugEnabled()) {
    	_EOTplRepartItemItemValeur.LOG.debug( "updating dDebVal from " + dDebVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVal");
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey("dFinVal");
  }

  public void setDFinVal(NSTimestamp value) {
    if (_EOTplRepartItemItemValeur.LOG.isDebugEnabled()) {
    	_EOTplRepartItemItemValeur.LOG.debug( "updating dFinVal from " + dFinVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVal");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTplRepartItemItemValeur.LOG.isDebugEnabled()) {
    	_EOTplRepartItemItemValeur.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer tivPosition() {
    return (Integer) storedValueForKey("tivPosition");
  }

  public void setTivPosition(Integer value) {
    if (_EOTplRepartItemItemValeur.LOG.isDebugEnabled()) {
    	_EOTplRepartItemItemValeur.LOG.debug( "updating tivPosition from " + tivPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "tivPosition");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOTplItem toTplItem() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplItem)storedValueForKey("toTplItem");
  }

  public void setToTplItemRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplItem value) {
    if (_EOTplRepartItemItemValeur.LOG.isDebugEnabled()) {
      _EOTplRepartItemItemValeur.LOG.debug("updating toTplItem from " + toTplItem() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOTplItem oldValue = toTplItem();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTplItem");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTplItem");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur> toTplItemValeur() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur>)storedValueForKey("toTplItemValeur");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur> toTplItemValeur(EOQualifier qualifier) {
    return toTplItemValeur(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur> toTplItemValeur(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur> results;
      results = toTplItemValeur();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToTplItemValeurRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur object) {
    if (_EOTplRepartItemItemValeur.LOG.isDebugEnabled()) {
      _EOTplRepartItemItemValeur.LOG.debug("adding " + object + " to toTplItemValeur relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toTplItemValeur");
  }

  public void removeFromToTplItemValeurRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur object) {
    if (_EOTplRepartItemItemValeur.LOG.isDebugEnabled()) {
      _EOTplRepartItemItemValeur.LOG.debug("removing " + object + " from toTplItemValeur relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toTplItemValeur");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur createToTplItemValeurRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_TplItemValeur");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toTplItemValeur");
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur) eo;
  }

  public void deleteToTplItemValeurRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toTplItemValeur");
    editingContext().deleteObject(object);
  }

  public void deleteAllToTplItemValeurRelationships() {
    Enumeration objects = toTplItemValeur().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToTplItemValeurRelationship((org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur)objects.nextElement());
    }
  }


  public static EOTplRepartItemItemValeur create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlgrh.common.metier.EOTplItem toTplItem) {
    EOTplRepartItemItemValeur eo = (EOTplRepartItemItemValeur) EOUtilities.createAndInsertInstance(editingContext, _EOTplRepartItemItemValeur.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToTplItemRelationship(toTplItem);
    return eo;
  }

  public static NSArray<EOTplRepartItemItemValeur> fetchAll(EOEditingContext editingContext) {
    return _EOTplRepartItemItemValeur.fetchAll(editingContext, null);
  }

  public static NSArray<EOTplRepartItemItemValeur> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTplRepartItemItemValeur.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOTplRepartItemItemValeur> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTplRepartItemItemValeur.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTplRepartItemItemValeur> eoObjects = (NSArray<EOTplRepartItemItemValeur>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTplRepartItemItemValeur fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTplRepartItemItemValeur.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTplRepartItemItemValeur fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTplRepartItemItemValeur> eoObjects = _EOTplRepartItemItemValeur.fetch(editingContext, qualifier, null);
    EOTplRepartItemItemValeur eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTplRepartItemItemValeur)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_TplRepartItemItemValeur that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTplRepartItemItemValeur fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTplRepartItemItemValeur.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOTplRepartItemItemValeur fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTplRepartItemItemValeur eoObject = _EOTplRepartItemItemValeur.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_TplRepartItemItemValeur that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOTplRepartItemItemValeur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTplRepartItemItemValeur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTplRepartItemItemValeur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTplRepartItemItemValeur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOTplRepartItemItemValeur localInstanceIn(EOEditingContext editingContext, EOTplRepartItemItemValeur eo) {
    EOTplRepartItemItemValeur localInstance = (eo == null) ? null : (EOTplRepartItemItemValeur)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
