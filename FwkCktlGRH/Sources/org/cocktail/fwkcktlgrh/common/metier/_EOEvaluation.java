/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOEvaluation.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOEvaluation extends org.cocktail.fwkcktlgrh.common.metier.util.UtilEOEvaluationKeyValueCoding {
	public static final String ENTITY_NAME = "Fwkgrh_Evaluation";

	// Attributes
	public static final String COMPETENCE_EVOLUTION_COMMENTAIRE_KEY = "competenceEvolutionCommentaire";
	public static final String COMPETENCE_FORMATION_COMMENTAIRE_KEY = "competenceFormationCommentaire";
	public static final String COMPETENCE_POSTE_COMMENTAIRE_KEY = "competencePosteCommentaire";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_TENUE_ENTRETIEN_KEY = "dTenueEntretien";
	public static final String D_VISA_RESPONSABLE_RH_KEY = "dVisaResponsableRh";
	public static final String EPE_KEY_VISIBLE_KEY = "epeKeyVisible";
	public static final String EVA_CHAMP_LIBRE_KEY = "evaChampLibre";
	public static final String EVA_EVOLUTION_ENVIS_KEY = "evaEvolutionEnvis";
	public static final String EVA_EVOLUTION_PROPO_KEY = "evaEvolutionPropo";
	public static final String EVA_KEY_VISIBLE_KEY = "evaKeyVisible";
	public static final String FONCTION_EVALUATEUR_KEY = "fonctionEvaluateur";
	public static final String FORMATION_PREVUS_KEY = "formationPrevus";
	public static final String MODILISER_DIF_KEY = "modiliserDif";
	public static final String NO_INDIVIDU_RESP_KEY = "noIndividuResp";
	public static final String NO_INDIVIDU_VISIBLE_KEY = "noIndividuVisible";

	public static final ERXKey<String> COMPETENCE_EVOLUTION_COMMENTAIRE = new ERXKey<String>("competenceEvolutionCommentaire");
	public static final ERXKey<String> COMPETENCE_FORMATION_COMMENTAIRE = new ERXKey<String>("competenceFormationCommentaire");
	public static final ERXKey<String> COMPETENCE_POSTE_COMMENTAIRE = new ERXKey<String>("competencePosteCommentaire");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<NSTimestamp> D_TENUE_ENTRETIEN = new ERXKey<NSTimestamp>("dTenueEntretien");
	public static final ERXKey<NSTimestamp> D_VISA_RESPONSABLE_RH = new ERXKey<NSTimestamp>("dVisaResponsableRh");
	public static final ERXKey<Integer> EPE_KEY_VISIBLE = new ERXKey<Integer>("epeKeyVisible");
	public static final ERXKey<String> EVA_CHAMP_LIBRE = new ERXKey<String>("evaChampLibre");
	public static final ERXKey<String> EVA_EVOLUTION_ENVIS = new ERXKey<String>("evaEvolutionEnvis");
	public static final ERXKey<String> EVA_EVOLUTION_PROPO = new ERXKey<String>("evaEvolutionPropo");
	public static final ERXKey<Integer> EVA_KEY_VISIBLE = new ERXKey<Integer>("evaKeyVisible");
	public static final ERXKey<String> FONCTION_EVALUATEUR = new ERXKey<String>("fonctionEvaluateur");
	public static final ERXKey<String> FORMATION_PREVUS = new ERXKey<String>("formationPrevus");
	public static final ERXKey<String> MODILISER_DIF = new ERXKey<String>("modiliserDif");
	public static final ERXKey<Integer> NO_INDIVIDU_RESP = new ERXKey<Integer>("noIndividuResp");
	public static final ERXKey<Integer> NO_INDIVIDU_VISIBLE = new ERXKey<Integer>("noIndividuVisible");
	// Relationships
	public static final String TO_EVALUATION_PERIODE_KEY = "toEvaluationPeriode";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_INDIVIDU_RESP_KEY = "toIndividuResp";
	public static final String TOS_EVALUATION_NOTICE_PROMOTION_KEY = "tosEvaluationNoticePromotion";
	public static final String TOS_OBJECTIF_KEY = "tosObjectif";
	public static final String TOS_REPART_APPRECIATION_GENERALS_KEY = "tosRepartAppreciationGenerals";
	public static final String TOS_REPART_COMPETENCE_EVOLUTIONS_KEY = "tosRepartCompetenceEvolutions";
	public static final String TOS_REPART_COMPETENCE_POSTES_KEY = "tosRepartCompetencePostes";
	public static final String TOS_REPART_EVA_NOUVELLE_COMP_KEY = "tosRepartEvaNouvelleComp";
	public static final String TOS_REPART_FORMATION_SOUHAITEE_KEY = "tosRepartFormationSouhaitee";
	public static final String TOS_REPART_NIVEAU_COMPS_KEY = "tosRepartNiveauComps";
	public static final String TOS_REPART_PERSPECTIVE_FORMATIONS_KEY = "tosRepartPerspectiveFormations";
	public static final String TOS_SITU_ACTIVITE_KEY = "tosSituActivite";
	public static final String TO_V_CANDIDAT_EVALUATION_KEY = "toVCandidatEvaluation";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode> TO_EVALUATION_PERIODE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode>("toEvaluationPeriode");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU_RESP = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividuResp");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion> TOS_EVALUATION_NOTICE_PROMOTION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion>("tosEvaluationNoticePromotion");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOObjectif> TOS_OBJECTIF = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOObjectif>("tosObjectif");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral> TOS_REPART_APPRECIATION_GENERALS = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral>("tosRepartAppreciationGenerals");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution> TOS_REPART_COMPETENCE_EVOLUTIONS = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution>("tosRepartCompetenceEvolutions");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste> TOS_REPART_COMPETENCE_POSTES = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste>("tosRepartCompetencePostes");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp> TOS_REPART_EVA_NOUVELLE_COMP = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp>("tosRepartEvaNouvelleComp");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee> TOS_REPART_FORMATION_SOUHAITEE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee>("tosRepartFormationSouhaitee");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp> TOS_REPART_NIVEAU_COMPS = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp>("tosRepartNiveauComps");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation> TOS_REPART_PERSPECTIVE_FORMATIONS = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation>("tosRepartPerspectiveFormations");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOSituActivite> TOS_SITU_ACTIVITE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOSituActivite>("tosSituActivite");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation> TO_V_CANDIDAT_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation>("toVCandidatEvaluation");

  private static Logger LOG = Logger.getLogger(_EOEvaluation.class);

  public EOEvaluation localInstanceIn(EOEditingContext editingContext) {
    EOEvaluation localInstance = (EOEvaluation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String competenceEvolutionCommentaire() {
    return (String) storedValueForKey("competenceEvolutionCommentaire");
  }

  public void setCompetenceEvolutionCommentaire(String value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating competenceEvolutionCommentaire from " + competenceEvolutionCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "competenceEvolutionCommentaire");
  }

  public String competenceFormationCommentaire() {
    return (String) storedValueForKey("competenceFormationCommentaire");
  }

  public void setCompetenceFormationCommentaire(String value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating competenceFormationCommentaire from " + competenceFormationCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "competenceFormationCommentaire");
  }

  public String competencePosteCommentaire() {
    return (String) storedValueForKey("competencePosteCommentaire");
  }

  public void setCompetencePosteCommentaire(String value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating competencePosteCommentaire from " + competencePosteCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "competencePosteCommentaire");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dTenueEntretien() {
    return (NSTimestamp) storedValueForKey("dTenueEntretien");
  }

  public void setDTenueEntretien(NSTimestamp value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating dTenueEntretien from " + dTenueEntretien() + " to " + value);
    }
    takeStoredValueForKey(value, "dTenueEntretien");
  }

  public NSTimestamp dVisaResponsableRh() {
    return (NSTimestamp) storedValueForKey("dVisaResponsableRh");
  }

  public void setDVisaResponsableRh(NSTimestamp value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating dVisaResponsableRh from " + dVisaResponsableRh() + " to " + value);
    }
    takeStoredValueForKey(value, "dVisaResponsableRh");
  }

  public Integer epeKeyVisible() {
    return (Integer) storedValueForKey("epeKeyVisible");
  }

  public void setEpeKeyVisible(Integer value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating epeKeyVisible from " + epeKeyVisible() + " to " + value);
    }
    takeStoredValueForKey(value, "epeKeyVisible");
  }

  public String evaChampLibre() {
    return (String) storedValueForKey("evaChampLibre");
  }

  public void setEvaChampLibre(String value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating evaChampLibre from " + evaChampLibre() + " to " + value);
    }
    takeStoredValueForKey(value, "evaChampLibre");
  }

  public String evaEvolutionEnvis() {
    return (String) storedValueForKey("evaEvolutionEnvis");
  }

  public void setEvaEvolutionEnvis(String value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating evaEvolutionEnvis from " + evaEvolutionEnvis() + " to " + value);
    }
    takeStoredValueForKey(value, "evaEvolutionEnvis");
  }

  public String evaEvolutionPropo() {
    return (String) storedValueForKey("evaEvolutionPropo");
  }

  public void setEvaEvolutionPropo(String value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating evaEvolutionPropo from " + evaEvolutionPropo() + " to " + value);
    }
    takeStoredValueForKey(value, "evaEvolutionPropo");
  }

  public Integer evaKeyVisible() {
    return (Integer) storedValueForKey("evaKeyVisible");
  }

  public void setEvaKeyVisible(Integer value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating evaKeyVisible from " + evaKeyVisible() + " to " + value);
    }
    takeStoredValueForKey(value, "evaKeyVisible");
  }

  public String fonctionEvaluateur() {
    return (String) storedValueForKey("fonctionEvaluateur");
  }

  public void setFonctionEvaluateur(String value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating fonctionEvaluateur from " + fonctionEvaluateur() + " to " + value);
    }
    takeStoredValueForKey(value, "fonctionEvaluateur");
  }

  public String formationPrevus() {
    return (String) storedValueForKey("formationPrevus");
  }

  public void setFormationPrevus(String value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating formationPrevus from " + formationPrevus() + " to " + value);
    }
    takeStoredValueForKey(value, "formationPrevus");
  }

  public String modiliserDif() {
    return (String) storedValueForKey("modiliserDif");
  }

  public void setModiliserDif(String value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating modiliserDif from " + modiliserDif() + " to " + value);
    }
    takeStoredValueForKey(value, "modiliserDif");
  }

  public Integer noIndividuResp() {
    return (Integer) storedValueForKey("noIndividuResp");
  }

  public void setNoIndividuResp(Integer value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating noIndividuResp from " + noIndividuResp() + " to " + value);
    }
    takeStoredValueForKey(value, "noIndividuResp");
  }

  public Integer noIndividuVisible() {
    return (Integer) storedValueForKey("noIndividuVisible");
  }

  public void setNoIndividuVisible(Integer value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
    	_EOEvaluation.LOG.debug( "updating noIndividuVisible from " + noIndividuVisible() + " to " + value);
    }
    takeStoredValueForKey(value, "noIndividuVisible");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode toEvaluationPeriode() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode)storedValueForKey("toEvaluationPeriode");
  }

  public void setToEvaluationPeriodeRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("updating toEvaluationPeriode from " + toEvaluationPeriode() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode oldValue = toEvaluationPeriode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluationPeriode");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluationPeriode");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividuResp() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividuResp");
  }

  public void setToIndividuRespRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("updating toIndividuResp from " + toIndividuResp() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividuResp();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividuResp");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividuResp");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation toVCandidatEvaluation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation)storedValueForKey("toVCandidatEvaluation");
  }

  public void setToVCandidatEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation value) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("updating toVCandidatEvaluation from " + toVCandidatEvaluation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation oldValue = toVCandidatEvaluation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toVCandidatEvaluation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toVCandidatEvaluation");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion> tosEvaluationNoticePromotion() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion>)storedValueForKey("tosEvaluationNoticePromotion");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion> tosEvaluationNoticePromotion(EOQualifier qualifier) {
    return tosEvaluationNoticePromotion(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion> tosEvaluationNoticePromotion(EOQualifier qualifier, boolean fetch) {
    return tosEvaluationNoticePromotion(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion> tosEvaluationNoticePromotion(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion.TO_EVALUATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosEvaluationNoticePromotion();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosEvaluationNoticePromotionRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("adding " + object + " to tosEvaluationNoticePromotion relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosEvaluationNoticePromotion");
  }

  public void removeFromTosEvaluationNoticePromotionRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("removing " + object + " from tosEvaluationNoticePromotion relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosEvaluationNoticePromotion");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion createTosEvaluationNoticePromotionRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_EvaluationNoticePromotion");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosEvaluationNoticePromotion");
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion) eo;
  }

  public void deleteTosEvaluationNoticePromotionRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosEvaluationNoticePromotion");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosEvaluationNoticePromotionRelationships() {
    Enumeration objects = tosEvaluationNoticePromotion().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosEvaluationNoticePromotionRelationship((org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOObjectif> tosObjectif() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOObjectif>)storedValueForKey("tosObjectif");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOObjectif> tosObjectif(EOQualifier qualifier) {
    return tosObjectif(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOObjectif> tosObjectif(EOQualifier qualifier, boolean fetch) {
    return tosObjectif(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOObjectif> tosObjectif(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOObjectif> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOObjectif.TO_EVALUATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOObjectif.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosObjectif();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOObjectif>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOObjectif>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosObjectifRelationship(org.cocktail.fwkcktlgrh.common.metier.EOObjectif object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("adding " + object + " to tosObjectif relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosObjectif");
  }

  public void removeFromTosObjectifRelationship(org.cocktail.fwkcktlgrh.common.metier.EOObjectif object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("removing " + object + " from tosObjectif relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosObjectif");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOObjectif createTosObjectifRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_Objectif");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosObjectif");
    return (org.cocktail.fwkcktlgrh.common.metier.EOObjectif) eo;
  }

  public void deleteTosObjectifRelationship(org.cocktail.fwkcktlgrh.common.metier.EOObjectif object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosObjectif");
  }

  public void deleteAllTosObjectifRelationships() {
    Enumeration objects = tosObjectif().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosObjectifRelationship((org.cocktail.fwkcktlgrh.common.metier.EOObjectif)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral> tosRepartAppreciationGenerals() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral>)storedValueForKey("tosRepartAppreciationGenerals");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral> tosRepartAppreciationGenerals(EOQualifier qualifier) {
    return tosRepartAppreciationGenerals(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral> tosRepartAppreciationGenerals(EOQualifier qualifier, boolean fetch) {
    return tosRepartAppreciationGenerals(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral> tosRepartAppreciationGenerals(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral.TO_EVALUATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartAppreciationGenerals();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartAppreciationGeneralsRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("adding " + object + " to tosRepartAppreciationGenerals relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartAppreciationGenerals");
  }

  public void removeFromTosRepartAppreciationGeneralsRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("removing " + object + " from tosRepartAppreciationGenerals relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartAppreciationGenerals");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral createTosRepartAppreciationGeneralsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartAppreciationGeneral");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartAppreciationGenerals");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral) eo;
  }

  public void deleteTosRepartAppreciationGeneralsRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartAppreciationGenerals");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartAppreciationGeneralsRelationships() {
    Enumeration objects = tosRepartAppreciationGenerals().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartAppreciationGeneralsRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution> tosRepartCompetenceEvolutions() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution>)storedValueForKey("tosRepartCompetenceEvolutions");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution> tosRepartCompetenceEvolutions(EOQualifier qualifier) {
    return tosRepartCompetenceEvolutions(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution> tosRepartCompetenceEvolutions(EOQualifier qualifier, boolean fetch) {
    return tosRepartCompetenceEvolutions(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution> tosRepartCompetenceEvolutions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution.TO_EVALUATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartCompetenceEvolutions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartCompetenceEvolutionsRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("adding " + object + " to tosRepartCompetenceEvolutions relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartCompetenceEvolutions");
  }

  public void removeFromTosRepartCompetenceEvolutionsRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("removing " + object + " from tosRepartCompetenceEvolutions relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartCompetenceEvolutions");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution createTosRepartCompetenceEvolutionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartCompetenceEvolution");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartCompetenceEvolutions");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution) eo;
  }

  public void deleteTosRepartCompetenceEvolutionsRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartCompetenceEvolutions");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartCompetenceEvolutionsRelationships() {
    Enumeration objects = tosRepartCompetenceEvolutions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartCompetenceEvolutionsRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste> tosRepartCompetencePostes() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste>)storedValueForKey("tosRepartCompetencePostes");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste> tosRepartCompetencePostes(EOQualifier qualifier) {
    return tosRepartCompetencePostes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste> tosRepartCompetencePostes(EOQualifier qualifier, boolean fetch) {
    return tosRepartCompetencePostes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste> tosRepartCompetencePostes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste.TO_EVALUATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartCompetencePostes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartCompetencePostesRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("adding " + object + " to tosRepartCompetencePostes relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartCompetencePostes");
  }

  public void removeFromTosRepartCompetencePostesRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("removing " + object + " from tosRepartCompetencePostes relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartCompetencePostes");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste createTosRepartCompetencePostesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartCompetencePoste");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartCompetencePostes");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste) eo;
  }

  public void deleteTosRepartCompetencePostesRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartCompetencePostes");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartCompetencePostesRelationships() {
    Enumeration objects = tosRepartCompetencePostes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartCompetencePostesRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp> tosRepartEvaNouvelleComp() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp>)storedValueForKey("tosRepartEvaNouvelleComp");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp> tosRepartEvaNouvelleComp(EOQualifier qualifier) {
    return tosRepartEvaNouvelleComp(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp> tosRepartEvaNouvelleComp(EOQualifier qualifier, boolean fetch) {
    return tosRepartEvaNouvelleComp(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp> tosRepartEvaNouvelleComp(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp.TO_EVALUATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartEvaNouvelleComp();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartEvaNouvelleCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("adding " + object + " to tosRepartEvaNouvelleComp relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartEvaNouvelleComp");
  }

  public void removeFromTosRepartEvaNouvelleCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("removing " + object + " from tosRepartEvaNouvelleComp relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartEvaNouvelleComp");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp createTosRepartEvaNouvelleCompRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartEvaNouvelleComp");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartEvaNouvelleComp");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp) eo;
  }

  public void deleteTosRepartEvaNouvelleCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartEvaNouvelleComp");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartEvaNouvelleCompRelationships() {
    Enumeration objects = tosRepartEvaNouvelleComp().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartEvaNouvelleCompRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee> tosRepartFormationSouhaitee() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee>)storedValueForKey("tosRepartFormationSouhaitee");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee> tosRepartFormationSouhaitee(EOQualifier qualifier) {
    return tosRepartFormationSouhaitee(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee> tosRepartFormationSouhaitee(EOQualifier qualifier, boolean fetch) {
    return tosRepartFormationSouhaitee(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee> tosRepartFormationSouhaitee(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee.TO_EVALUATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartFormationSouhaitee();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartFormationSouhaiteeRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("adding " + object + " to tosRepartFormationSouhaitee relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartFormationSouhaitee");
  }

  public void removeFromTosRepartFormationSouhaiteeRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("removing " + object + " from tosRepartFormationSouhaitee relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFormationSouhaitee");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee createTosRepartFormationSouhaiteeRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartFormationSouhaitee");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartFormationSouhaitee");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee) eo;
  }

  public void deleteTosRepartFormationSouhaiteeRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFormationSouhaitee");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartFormationSouhaiteeRelationships() {
    Enumeration objects = tosRepartFormationSouhaitee().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartFormationSouhaiteeRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp> tosRepartNiveauComps() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp>)storedValueForKey("tosRepartNiveauComps");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp> tosRepartNiveauComps(EOQualifier qualifier) {
    return tosRepartNiveauComps(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp> tosRepartNiveauComps(EOQualifier qualifier, boolean fetch) {
    return tosRepartNiveauComps(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp> tosRepartNiveauComps(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp.TO_EVALUATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartNiveauComps();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartNiveauCompsRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("adding " + object + " to tosRepartNiveauComps relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartNiveauComps");
  }

  public void removeFromTosRepartNiveauCompsRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("removing " + object + " from tosRepartNiveauComps relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartNiveauComps");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp createTosRepartNiveauCompsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartNiveauComp");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartNiveauComps");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp) eo;
  }

  public void deleteTosRepartNiveauCompsRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartNiveauComps");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartNiveauCompsRelationships() {
    Enumeration objects = tosRepartNiveauComps().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartNiveauCompsRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation> tosRepartPerspectiveFormations() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation>)storedValueForKey("tosRepartPerspectiveFormations");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation> tosRepartPerspectiveFormations(EOQualifier qualifier) {
    return tosRepartPerspectiveFormations(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation> tosRepartPerspectiveFormations(EOQualifier qualifier, boolean fetch) {
    return tosRepartPerspectiveFormations(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation> tosRepartPerspectiveFormations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation.TO_EVALUATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartPerspectiveFormations();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartPerspectiveFormationsRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("adding " + object + " to tosRepartPerspectiveFormations relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartPerspectiveFormations");
  }

  public void removeFromTosRepartPerspectiveFormationsRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("removing " + object + " from tosRepartPerspectiveFormations relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartPerspectiveFormations");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation createTosRepartPerspectiveFormationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartPerspectiveFormation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartPerspectiveFormations");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation) eo;
  }

  public void deleteTosRepartPerspectiveFormationsRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartPerspectiveFormations");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartPerspectiveFormationsRelationships() {
    Enumeration objects = tosRepartPerspectiveFormations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartPerspectiveFormationsRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOSituActivite> tosSituActivite() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOSituActivite>)storedValueForKey("tosSituActivite");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOSituActivite> tosSituActivite(EOQualifier qualifier) {
    return tosSituActivite(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOSituActivite> tosSituActivite(EOQualifier qualifier, boolean fetch) {
    return tosSituActivite(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOSituActivite> tosSituActivite(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOSituActivite> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOSituActivite.TO_EVALUATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOSituActivite.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosSituActivite();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOSituActivite>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOSituActivite>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosSituActiviteRelationship(org.cocktail.fwkcktlgrh.common.metier.EOSituActivite object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("adding " + object + " to tosSituActivite relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosSituActivite");
  }

  public void removeFromTosSituActiviteRelationship(org.cocktail.fwkcktlgrh.common.metier.EOSituActivite object) {
    if (_EOEvaluation.LOG.isDebugEnabled()) {
      _EOEvaluation.LOG.debug("removing " + object + " from tosSituActivite relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosSituActivite");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOSituActivite createTosSituActiviteRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_SituActivite");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosSituActivite");
    return (org.cocktail.fwkcktlgrh.common.metier.EOSituActivite) eo;
  }

  public void deleteTosSituActiviteRelationship(org.cocktail.fwkcktlgrh.common.metier.EOSituActivite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosSituActivite");
  }

  public void deleteAllTosSituActiviteRelationships() {
    Enumeration objects = tosSituActivite().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosSituActiviteRelationship((org.cocktail.fwkcktlgrh.common.metier.EOSituActivite)objects.nextElement());
    }
  }


  public static EOEvaluation create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noIndividuVisible
) {
    EOEvaluation eo = (EOEvaluation) EOUtilities.createAndInsertInstance(editingContext, _EOEvaluation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoIndividuVisible(noIndividuVisible);
    return eo;
  }

  public static NSArray<EOEvaluation> fetchAll(EOEditingContext editingContext) {
    return _EOEvaluation.fetchAll(editingContext, null);
  }

  public static NSArray<EOEvaluation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEvaluation.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOEvaluation> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOEvaluation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEvaluation> eoObjects = (NSArray<EOEvaluation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOEvaluation fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEvaluation.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEvaluation fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEvaluation> eoObjects = _EOEvaluation.fetch(editingContext, qualifier, null);
    EOEvaluation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOEvaluation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Evaluation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEvaluation fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEvaluation.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOEvaluation fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEvaluation eoObject = _EOEvaluation.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Evaluation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOEvaluation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEvaluation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEvaluation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEvaluation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOEvaluation localInstanceIn(EOEditingContext editingContext, EOEvaluation eo) {
    EOEvaluation localInstance = (eo == null) ? null : (EOEvaluation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
