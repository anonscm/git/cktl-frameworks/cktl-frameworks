/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EORepartLolfSilland extends _EORepartLolfSilland {
	
  private static Logger log = Logger.getLogger(EORepartLolfSilland.class);
  
  public static final String OUI = "O";
  public static final String NON = "N";

  public EORepartLolfSilland() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }
  
  
  // METHODES AJOUTEES
  
  public NSArray<EORepartLolfSilland> tosRepartSilLolf() {
//	    return (NSArray)storedValueForKey( SIL_KEY_KEY);
	  return null;
	  }
  
  /**
	 * Les repartition associées à un exercice comptable
	 * 
	 * @param exercice
	 * @return
	 */
	public NSArray tosRepartSilLolf(EOExercice exercice) {
		return tosRepartSilLolf(CktlDataBus.newCondition(
							TO_EXERCICE_KEY + "=%@",
							// EORepartSillandLolf.TO_TYPE_ACTION_KEY + "." +
							// EOTypeAction.EXERCICE_KEY + "=%@",
				new NSArray(exercice)));
	}

	/**
	 * Les destination associées a un exercice comptable
	 * 
	 * @param exercice
	 * @return
	 */
	public NSArray tosTypeAction(EOExercice exercice) {
		// return (NSArray)
		// tosRepartSilLolf(exercice).valueForKey(EORepartSillandLolf.TO_TYPE_ACTION_KEY);
		return (NSArray) tosRepartSilLolf(exercice).valueForKey(TO_LOLF_NOMENCLATURE_DEPENSE_KEY);
	}

	/**
	 * Indique si la fonction est affectee dans l'exercice.
	 * 
	 * @param exercice
	 * @return
	 */
	public boolean isDeclaree(EOExercice exercice) {
		return tosRepartSilLolf(
					CktlDataBus.newCondition(
							// EORepartSillandLolf.TO_TYPE_ACTION_KEY + "." +
							// EOTypeAction.EXERCICE_KEY + "=%@",
							TO_EXERCICE_KEY + "=%@",
							new NSArray(exercice))).count() > 0;
	}
  
	/**
	 * liste de toutes les fonction silland declarees pour un exercice et pour un
	 * type de poste
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findAllFctSillandExerciceInContext(EOEditingContext ec, EOExercice exercice, EOPoste poste) {
		// String strQual = EORepartSillandLolf.TO_TYPE_ACTION_KEY + "." +
		// EOTypeAction.EXERCICE_KEY + "=%@";
		String strQual = TO_EXERCICE_KEY + "=%@";
		if (poste.isEnseignant()) {
			strQual += " and " + TO_FCT_SILLAND_KEY + "." + EOFctSilland.SIL_ENSEIGNANT_KEY + "='" + OUI + "'";
		} else {
			strQual += " and " + TO_FCT_SILLAND_KEY + "." + EOFctSilland.SIL_NON_ENSEIGNANT_KEY + "='" + OUI + "'";
		}
		NSArray recsRepartSillandLolf = fetchAll(
					ec,
					CktlDataBus.newCondition(strQual, new NSArray(exercice)),
					CktlSort.newSort(
							TO_FCT_SILLAND_KEY + "." + EOFctSilland.SIL_LIBELLE_KEY));
		return NSArrayCtrl.removeDuplicate((NSArray) recsRepartSillandLolf.valueForKey(TO_FCT_SILLAND_KEY));
	}
  

	 public NSArray<EORepartLolfSilland> tosRepartSilLolf(EOQualifier qualifier) {
		    return tosRepartSilLolf(qualifier, null, false);
		  }

		  public NSArray<EORepartLolfSilland> tosRepartSilLolf(EOQualifier qualifier, boolean fetch) {
		    return tosRepartSilLolf(qualifier, null, fetch);
		  }

		  public NSArray<EORepartLolfSilland> tosRepartSilLolf(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		    NSArray results;
		    if (fetch) {
		      EOQualifier fullQualifier;
		      EOQualifier inverseQualifier = new EOKeyValueQualifier(TO_FCT_SILLAND_KEY, EOQualifier.QualifierOperatorEqual, this);
		    	
		      if (qualifier == null) {
		        fullQualifier = inverseQualifier;
		      }
		      else {
		        NSMutableArray qualifiers = new NSMutableArray();
		        qualifiers.addObject(qualifier);
		        qualifiers.addObject(inverseQualifier);
		        fullQualifier = new EOAndQualifier(qualifiers);
		      }

		      results = EORepartLolfSilland.fetchAll(editingContext(), fullQualifier, sortOrderings);
		    }
		    else {
		      results = tosRepartSilLolf();
		      if (qualifier != null) {
		        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		      }
		      if (sortOrderings != null) {
		        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		      }
		    }
		    return results;
		  }
	
	
}
