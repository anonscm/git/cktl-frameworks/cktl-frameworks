/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOVacatairesAffectation.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOVacatairesAffectation extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_VacatairesAffectation";

	// Attributes
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String NB_HEURES_KEY = "nbHeures";
	public static final String TEMOIN_PRINCIPALE_KEY = "temoinPrincipale";

	public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
	public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
	public static final ERXKey<Double> NB_HEURES = new ERXKey<Double>("nbHeures");
	public static final ERXKey<String> TEMOIN_PRINCIPALE = new ERXKey<String>("temoinPrincipale");
	// Relationships
	public static final String TO_STRUCTURE_KEY = "toStructure";
	public static final String TO_VACATAIRE_KEY = "toVacataire";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOVacataires> TO_VACATAIRE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOVacataires>("toVacataire");

  private static Logger LOG = Logger.getLogger(_EOVacatairesAffectation.class);

  public EOVacatairesAffectation localInstanceIn(EOEditingContext editingContext) {
    EOVacatairesAffectation localInstance = (EOVacatairesAffectation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey("dateCreation");
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOVacatairesAffectation.LOG.isDebugEnabled()) {
    	_EOVacatairesAffectation.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dateCreation");
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey("dateModification");
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOVacatairesAffectation.LOG.isDebugEnabled()) {
    	_EOVacatairesAffectation.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dateModification");
  }

  public Double nbHeures() {
    return (Double) storedValueForKey("nbHeures");
  }

  public void setNbHeures(Double value) {
    if (_EOVacatairesAffectation.LOG.isDebugEnabled()) {
    	_EOVacatairesAffectation.LOG.debug( "updating nbHeures from " + nbHeures() + " to " + value);
    }
    takeStoredValueForKey(value, "nbHeures");
  }

  public String temoinPrincipale() {
    return (String) storedValueForKey("temoinPrincipale");
  }

  public void setTemoinPrincipale(String value) {
    if (_EOVacatairesAffectation.LOG.isDebugEnabled()) {
    	_EOVacatairesAffectation.LOG.debug( "updating temoinPrincipale from " + temoinPrincipale() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinPrincipale");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey("toStructure");
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOVacatairesAffectation.LOG.isDebugEnabled()) {
      _EOVacatairesAffectation.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructure");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOVacataires toVacataire() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOVacataires)storedValueForKey("toVacataire");
  }

  public void setToVacataireRelationship(org.cocktail.fwkcktlgrh.common.metier.EOVacataires value) {
    if (_EOVacatairesAffectation.LOG.isDebugEnabled()) {
      _EOVacatairesAffectation.LOG.debug("updating toVacataire from " + toVacataire() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOVacataires oldValue = toVacataire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toVacataire");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toVacataire");
    }
  }
  

  public static EOVacatairesAffectation create(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, Double nbHeures
, String temoinPrincipale
, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure, org.cocktail.fwkcktlgrh.common.metier.EOVacataires toVacataire) {
    EOVacatairesAffectation eo = (EOVacatairesAffectation) EOUtilities.createAndInsertInstance(editingContext, _EOVacatairesAffectation.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setNbHeures(nbHeures);
		eo.setTemoinPrincipale(temoinPrincipale);
    eo.setToStructureRelationship(toStructure);
    eo.setToVacataireRelationship(toVacataire);
    return eo;
  }

  public static NSArray<EOVacatairesAffectation> fetchAll(EOEditingContext editingContext) {
    return _EOVacatairesAffectation.fetchAll(editingContext, null);
  }

  public static NSArray<EOVacatairesAffectation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVacatairesAffectation.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOVacatairesAffectation> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOVacatairesAffectation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVacatairesAffectation> eoObjects = (NSArray<EOVacatairesAffectation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOVacatairesAffectation fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVacatairesAffectation.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVacatairesAffectation fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVacatairesAffectation> eoObjects = _EOVacatairesAffectation.fetch(editingContext, qualifier, null);
    EOVacatairesAffectation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOVacatairesAffectation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_VacatairesAffectation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVacatairesAffectation fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVacatairesAffectation.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOVacatairesAffectation fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVacatairesAffectation eoObject = _EOVacatairesAffectation.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_VacatairesAffectation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOVacatairesAffectation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVacatairesAffectation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVacatairesAffectation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVacatairesAffectation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOVacatairesAffectation localInstanceIn(EOEditingContext editingContext, EOVacatairesAffectation eo) {
    EOVacatairesAffectation localInstance = (eo == null) ? null : (EOVacatairesAffectation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
