/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOIndividuFormations.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOIndividuFormations extends org.cocktail.fwkcktlgrh.common.metier.util.UtilDescriptionFormation {
	public static final String ENTITY_NAME = "Fwkgrh_IndividuFormations";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_FORMATION_KEY = "dDebFormation";
	public static final String D_FIN_FORMATION_KEY = "dFinFormation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DUREE_KEY = "duree";
	public static final String LL_FORMATION_KEY = "llFormation";
	public static final String MIS_EN_OEUVRE_DANS_POSTE_KEY = "misEnOeuvreDansPoste";
	public static final String NO_INDIVIDU_KEY = "noIndividu";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_FORMATION = new ERXKey<NSTimestamp>("dDebFormation");
	public static final ERXKey<NSTimestamp> D_FIN_FORMATION = new ERXKey<NSTimestamp>("dFinFormation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> DUREE = new ERXKey<String>("duree");
	public static final ERXKey<String> LL_FORMATION = new ERXKey<String>("llFormation");
	public static final ERXKey<String> MIS_EN_OEUVRE_DANS_POSTE = new ERXKey<String>("misEnOeuvreDansPoste");
	public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
	// Relationships
	public static final String TO_FORMATION_PERSONNEL_KEY = "toFormationPersonnel";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_TYPE_UNITE_TEMPS_KEY = "toTypeUniteTemps";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> TO_FORMATION_PERSONNEL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel>("toFormationPersonnel");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeUniteTemps> TO_TYPE_UNITE_TEMPS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeUniteTemps>("toTypeUniteTemps");

  private static Logger LOG = Logger.getLogger(_EOIndividuFormations.class);

  public EOIndividuFormations localInstanceIn(EOEditingContext editingContext) {
    EOIndividuFormations localInstance = (EOIndividuFormations)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOIndividuFormations.LOG.isDebugEnabled()) {
    	_EOIndividuFormations.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebFormation() {
    return (NSTimestamp) storedValueForKey("dDebFormation");
  }

  public void setDDebFormation(NSTimestamp value) {
    if (_EOIndividuFormations.LOG.isDebugEnabled()) {
    	_EOIndividuFormations.LOG.debug( "updating dDebFormation from " + dDebFormation() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebFormation");
  }

  public NSTimestamp dFinFormation() {
    return (NSTimestamp) storedValueForKey("dFinFormation");
  }

  public void setDFinFormation(NSTimestamp value) {
    if (_EOIndividuFormations.LOG.isDebugEnabled()) {
    	_EOIndividuFormations.LOG.debug( "updating dFinFormation from " + dFinFormation() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinFormation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOIndividuFormations.LOG.isDebugEnabled()) {
    	_EOIndividuFormations.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String duree() {
    return (String) storedValueForKey("duree");
  }

  public void setDuree(String value) {
    if (_EOIndividuFormations.LOG.isDebugEnabled()) {
    	_EOIndividuFormations.LOG.debug( "updating duree from " + duree() + " to " + value);
    }
    takeStoredValueForKey(value, "duree");
  }

  public String llFormation() {
    return (String) storedValueForKey("llFormation");
  }

  public void setLlFormation(String value) {
    if (_EOIndividuFormations.LOG.isDebugEnabled()) {
    	_EOIndividuFormations.LOG.debug( "updating llFormation from " + llFormation() + " to " + value);
    }
    takeStoredValueForKey(value, "llFormation");
  }

  public String misEnOeuvreDansPoste() {
    return (String) storedValueForKey("misEnOeuvreDansPoste");
  }

  public void setMisEnOeuvreDansPoste(String value) {
    if (_EOIndividuFormations.LOG.isDebugEnabled()) {
    	_EOIndividuFormations.LOG.debug( "updating misEnOeuvreDansPoste from " + misEnOeuvreDansPoste() + " to " + value);
    }
    takeStoredValueForKey(value, "misEnOeuvreDansPoste");
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey("noIndividu");
  }

  public void setNoIndividu(Integer value) {
    if (_EOIndividuFormations.LOG.isDebugEnabled()) {
    	_EOIndividuFormations.LOG.debug( "updating noIndividu from " + noIndividu() + " to " + value);
    }
    takeStoredValueForKey(value, "noIndividu");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel toFormationPersonnel() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel)storedValueForKey("toFormationPersonnel");
  }

  public void setToFormationPersonnelRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel value) {
    if (_EOIndividuFormations.LOG.isDebugEnabled()) {
      _EOIndividuFormations.LOG.debug("updating toFormationPersonnel from " + toFormationPersonnel() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel oldValue = toFormationPersonnel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFormationPersonnel");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFormationPersonnel");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOIndividuFormations.LOG.isDebugEnabled()) {
      _EOIndividuFormations.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeUniteTemps toTypeUniteTemps() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeUniteTemps)storedValueForKey("toTypeUniteTemps");
  }

  public void setToTypeUniteTempsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeUniteTemps value) {
    if (_EOIndividuFormations.LOG.isDebugEnabled()) {
      _EOIndividuFormations.LOG.debug("updating toTypeUniteTemps from " + toTypeUniteTemps() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeUniteTemps oldValue = toTypeUniteTemps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeUniteTemps");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeUniteTemps");
    }
  }
  

  public static EOIndividuFormations create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebFormation
, NSTimestamp dModification
, Integer noIndividu
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu) {
    EOIndividuFormations eo = (EOIndividuFormations) EOUtilities.createAndInsertInstance(editingContext, _EOIndividuFormations.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebFormation(dDebFormation);
		eo.setDModification(dModification);
		eo.setNoIndividu(noIndividu);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  public static NSArray<EOIndividuFormations> fetchAll(EOEditingContext editingContext) {
    return _EOIndividuFormations.fetchAll(editingContext, null);
  }

  public static NSArray<EOIndividuFormations> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOIndividuFormations.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOIndividuFormations> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOIndividuFormations.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOIndividuFormations> eoObjects = (NSArray<EOIndividuFormations>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOIndividuFormations fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFormations.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOIndividuFormations fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOIndividuFormations> eoObjects = _EOIndividuFormations.fetch(editingContext, qualifier, null);
    EOIndividuFormations eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOIndividuFormations)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_IndividuFormations that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOIndividuFormations fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOIndividuFormations.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOIndividuFormations fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOIndividuFormations eoObject = _EOIndividuFormations.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_IndividuFormations that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOIndividuFormations fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOIndividuFormations fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOIndividuFormations eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOIndividuFormations)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOIndividuFormations localInstanceIn(EOEditingContext editingContext, EOIndividuFormations eo) {
    EOIndividuFormations localInstance = (eo == null) ? null : (EOIndividuFormations)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
