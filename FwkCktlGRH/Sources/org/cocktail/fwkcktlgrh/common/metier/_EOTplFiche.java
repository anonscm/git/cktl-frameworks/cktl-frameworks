/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOTplFiche.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOTplFiche extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_TplFiche";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TFI_CODE_KEY = "tfiCode";
	public static final String TFI_COMMENTAIRE_KEY = "tfiCommentaire";
	public static final String TFI_LIBELLE_KEY = "tfiLibelle";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> TFI_CODE = new ERXKey<String>("tfiCode");
	public static final ERXKey<String> TFI_COMMENTAIRE = new ERXKey<String>("tfiCommentaire");
	public static final ERXKey<String> TFI_LIBELLE = new ERXKey<String>("tfiLibelle");
	// Relationships
	public static final String TOS_TPL_ONGLET_KEY = "tosTplOnglet";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet> TOS_TPL_ONGLET = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet>("tosTplOnglet");

  private static Logger LOG = Logger.getLogger(_EOTplFiche.class);

  public EOTplFiche localInstanceIn(EOEditingContext editingContext) {
    EOTplFiche localInstance = (EOTplFiche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTplFiche.LOG.isDebugEnabled()) {
    	_EOTplFiche.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTplFiche.LOG.isDebugEnabled()) {
    	_EOTplFiche.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String tfiCode() {
    return (String) storedValueForKey("tfiCode");
  }

  public void setTfiCode(String value) {
    if (_EOTplFiche.LOG.isDebugEnabled()) {
    	_EOTplFiche.LOG.debug( "updating tfiCode from " + tfiCode() + " to " + value);
    }
    takeStoredValueForKey(value, "tfiCode");
  }

  public String tfiCommentaire() {
    return (String) storedValueForKey("tfiCommentaire");
  }

  public void setTfiCommentaire(String value) {
    if (_EOTplFiche.LOG.isDebugEnabled()) {
    	_EOTplFiche.LOG.debug( "updating tfiCommentaire from " + tfiCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "tfiCommentaire");
  }

  public String tfiLibelle() {
    return (String) storedValueForKey("tfiLibelle");
  }

  public void setTfiLibelle(String value) {
    if (_EOTplFiche.LOG.isDebugEnabled()) {
    	_EOTplFiche.LOG.debug( "updating tfiLibelle from " + tfiLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "tfiLibelle");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet> tosTplOnglet() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet>)storedValueForKey("tosTplOnglet");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet> tosTplOnglet(EOQualifier qualifier) {
    return tosTplOnglet(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet> tosTplOnglet(EOQualifier qualifier, boolean fetch) {
    return tosTplOnglet(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet> tosTplOnglet(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet.TO_TPL_FICHE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosTplOnglet();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosTplOngletRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet object) {
    if (_EOTplFiche.LOG.isDebugEnabled()) {
      _EOTplFiche.LOG.debug("adding " + object + " to tosTplOnglet relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosTplOnglet");
  }

  public void removeFromTosTplOngletRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet object) {
    if (_EOTplFiche.LOG.isDebugEnabled()) {
      _EOTplFiche.LOG.debug("removing " + object + " from tosTplOnglet relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosTplOnglet");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet createTosTplOngletRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_TplOnglet");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosTplOnglet");
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet) eo;
  }

  public void deleteTosTplOngletRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosTplOnglet");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosTplOngletRelationships() {
    Enumeration objects = tosTplOnglet().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosTplOngletRelationship((org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet)objects.nextElement());
    }
  }


  public static EOTplFiche create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String tfiCode
, String tfiLibelle
) {
    EOTplFiche eo = (EOTplFiche) EOUtilities.createAndInsertInstance(editingContext, _EOTplFiche.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTfiCode(tfiCode);
		eo.setTfiLibelle(tfiLibelle);
    return eo;
  }

  public static NSArray<EOTplFiche> fetchAll(EOEditingContext editingContext) {
    return _EOTplFiche.fetchAll(editingContext, null);
  }

  public static NSArray<EOTplFiche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTplFiche.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOTplFiche> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTplFiche.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTplFiche> eoObjects = (NSArray<EOTplFiche>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTplFiche fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTplFiche.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTplFiche fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTplFiche> eoObjects = _EOTplFiche.fetch(editingContext, qualifier, null);
    EOTplFiche eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTplFiche)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_TplFiche that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTplFiche fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTplFiche.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOTplFiche fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTplFiche eoObject = _EOTplFiche.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_TplFiche that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOTplFiche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTplFiche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTplFiche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTplFiche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOTplFiche localInstanceIn(EOEditingContext editingContext, EOTplFiche eo) {
    EOTplFiche localInstance = (eo == null) ? null : (EOTplFiche)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
