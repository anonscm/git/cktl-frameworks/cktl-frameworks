/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOBonifications.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOBonifications extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Bonifications";

	// Attributes
	public static final String BONI_DUREE_KEY = "boniDuree";
	public static final String DATE_ARRIVEE_KEY = "dateArrivee";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> BONI_DUREE = new ERXKey<String>("boniDuree");
	public static final ERXKey<NSTimestamp> DATE_ARRIVEE = new ERXKey<NSTimestamp>("dateArrivee");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_NATURE_BONIFICATION_KEY = "toNatureBonification";
	public static final String TO_TERRITOIRE_KEY = "toTerritoire";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification> TO_NATURE_BONIFICATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification>("toNatureBonification");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire> TO_TERRITOIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire>("toTerritoire");

  private static Logger LOG = Logger.getLogger(_EOBonifications.class);

  public EOBonifications localInstanceIn(EOEditingContext editingContext) {
    EOBonifications localInstance = (EOBonifications)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String boniDuree() {
    return (String) storedValueForKey("boniDuree");
  }

  public void setBoniDuree(String value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating boniDuree from " + boniDuree() + " to " + value);
    }
    takeStoredValueForKey(value, "boniDuree");
  }

  public NSTimestamp dateArrivee() {
    return (NSTimestamp) storedValueForKey("dateArrivee");
  }

  public void setDateArrivee(NSTimestamp value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating dateArrivee from " + dateArrivee() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrivee");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
    	_EOBonifications.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
      _EOBonifications.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification toNatureBonification() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification)storedValueForKey("toNatureBonification");
  }

  public void setToNatureBonificationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
      _EOBonifications.LOG.debug("updating toNatureBonification from " + toNatureBonification() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification oldValue = toNatureBonification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toNatureBonification");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toNatureBonification");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire toTerritoire() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire)storedValueForKey("toTerritoire");
  }

  public void setToTerritoireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire value) {
    if (_EOBonifications.LOG.isDebugEnabled()) {
      _EOBonifications.LOG.debug("updating toTerritoire from " + toTerritoire() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EONatureBonifTerritoire oldValue = toTerritoire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTerritoire");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTerritoire");
    }
  }
  

  public static EOBonifications create(EOEditingContext editingContext, String boniDuree
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlpersonne.common.metier.EONatureBonification toNatureBonification) {
    EOBonifications eo = (EOBonifications) EOUtilities.createAndInsertInstance(editingContext, _EOBonifications.ENTITY_NAME);    
		eo.setBoniDuree(boniDuree);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToNatureBonificationRelationship(toNatureBonification);
    return eo;
  }

  public static NSArray<EOBonifications> fetchAll(EOEditingContext editingContext) {
    return _EOBonifications.fetchAll(editingContext, null);
  }

  public static NSArray<EOBonifications> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOBonifications.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOBonifications> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOBonifications.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOBonifications> eoObjects = (NSArray<EOBonifications>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOBonifications fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBonifications.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBonifications fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOBonifications> eoObjects = _EOBonifications.fetch(editingContext, qualifier, null);
    EOBonifications eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOBonifications)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Bonifications that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBonifications fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBonifications.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOBonifications fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOBonifications eoObject = _EOBonifications.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Bonifications that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOBonifications fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBonifications fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBonifications eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBonifications)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOBonifications localInstanceIn(EOEditingContext editingContext, EOBonifications eo) {
    EOBonifications localInstance = (eo == null) ? null : (EOBonifications)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
