/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartFormationSouhaitee.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartFormationSouhaitee extends org.cocktail.fwkcktlgrh.common.metier.util.UtilDescriptionFormation {
	public static final String ENTITY_NAME = "Fwkgrh_RepartFormationSouhaitee";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EVA_KEY_KEY = "evaKey";
	public static final String RFS_FORMATION_CHAMP_LIBRE_KEY = "rfsFormationChampLibre";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> EVA_KEY = new ERXKey<Integer>("evaKey");
	public static final ERXKey<String> RFS_FORMATION_CHAMP_LIBRE = new ERXKey<String>("rfsFormationChampLibre");
	// Relationships
	public static final String TO_EVALUATION_KEY = "toEvaluation";
	public static final String TO_FORMATION_PERSONNEL_KEY = "toFormationPersonnel";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> TO_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>("toEvaluation");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel> TO_FORMATION_PERSONNEL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel>("toFormationPersonnel");

  private static Logger LOG = Logger.getLogger(_EORepartFormationSouhaitee.class);

  public EORepartFormationSouhaitee localInstanceIn(EOEditingContext editingContext) {
    EORepartFormationSouhaitee localInstance = (EORepartFormationSouhaitee)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartFormationSouhaitee.LOG.isDebugEnabled()) {
    	_EORepartFormationSouhaitee.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartFormationSouhaitee.LOG.isDebugEnabled()) {
    	_EORepartFormationSouhaitee.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer evaKey() {
    return (Integer) storedValueForKey("evaKey");
  }

  public void setEvaKey(Integer value) {
    if (_EORepartFormationSouhaitee.LOG.isDebugEnabled()) {
    	_EORepartFormationSouhaitee.LOG.debug( "updating evaKey from " + evaKey() + " to " + value);
    }
    takeStoredValueForKey(value, "evaKey");
  }

  public String rfsFormationChampLibre() {
    return (String) storedValueForKey("rfsFormationChampLibre");
  }

  public void setRfsFormationChampLibre(String value) {
    if (_EORepartFormationSouhaitee.LOG.isDebugEnabled()) {
    	_EORepartFormationSouhaitee.LOG.debug( "updating rfsFormationChampLibre from " + rfsFormationChampLibre() + " to " + value);
    }
    takeStoredValueForKey(value, "rfsFormationChampLibre");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluation)storedValueForKey("toEvaluation");
  }

  public void setToEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation value) {
    if (_EORepartFormationSouhaitee.LOG.isDebugEnabled()) {
      _EORepartFormationSouhaitee.LOG.debug("updating toEvaluation from " + toEvaluation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluation oldValue = toEvaluation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluation");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel toFormationPersonnel() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel)storedValueForKey("toFormationPersonnel");
  }

  public void setToFormationPersonnelRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel value) {
    if (_EORepartFormationSouhaitee.LOG.isDebugEnabled()) {
      _EORepartFormationSouhaitee.LOG.debug("updating toFormationPersonnel from " + toFormationPersonnel() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel oldValue = toFormationPersonnel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFormationPersonnel");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFormationPersonnel");
    }
  }
  

  public static EORepartFormationSouhaitee create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EORepartFormationSouhaitee eo = (EORepartFormationSouhaitee) EOUtilities.createAndInsertInstance(editingContext, _EORepartFormationSouhaitee.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EORepartFormationSouhaitee> fetchAll(EOEditingContext editingContext) {
    return _EORepartFormationSouhaitee.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartFormationSouhaitee> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartFormationSouhaitee.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartFormationSouhaitee> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartFormationSouhaitee.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartFormationSouhaitee> eoObjects = (NSArray<EORepartFormationSouhaitee>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartFormationSouhaitee fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartFormationSouhaitee.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartFormationSouhaitee fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartFormationSouhaitee> eoObjects = _EORepartFormationSouhaitee.fetch(editingContext, qualifier, null);
    EORepartFormationSouhaitee eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartFormationSouhaitee)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartFormationSouhaitee that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartFormationSouhaitee fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartFormationSouhaitee.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartFormationSouhaitee fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartFormationSouhaitee eoObject = _EORepartFormationSouhaitee.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartFormationSouhaitee that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartFormationSouhaitee fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartFormationSouhaitee fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartFormationSouhaitee eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartFormationSouhaitee)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartFormationSouhaitee localInstanceIn(EOEditingContext editingContext, EORepartFormationSouhaitee eo) {
    EORepartFormationSouhaitee localInstance = (eo == null) ? null : (EORepartFormationSouhaitee)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
