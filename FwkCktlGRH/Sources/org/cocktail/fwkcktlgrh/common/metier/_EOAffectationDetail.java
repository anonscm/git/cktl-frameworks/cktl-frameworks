/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOAffectationDetail.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOAffectationDetail extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_AffectationDetail";

	// Attributes
	public static final String ADE_DATE_DIFF_AFFECTATION_KEY = "adeDateDiffAffectation";
	public static final String ADE_D_DEBUT_KEY = "adeDDebut";
	public static final String ADE_D_FIN_KEY = "adeDFin";
	public static final String AFF_QUOTITE_KEY = "affQuotite";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	public static final ERXKey<Integer> ADE_DATE_DIFF_AFFECTATION = new ERXKey<Integer>("adeDateDiffAffectation");
	public static final ERXKey<NSTimestamp> ADE_D_DEBUT = new ERXKey<NSTimestamp>("adeDDebut");
	public static final ERXKey<NSTimestamp> ADE_D_FIN = new ERXKey<NSTimestamp>("adeDFin");
	public static final ERXKey<java.math.BigDecimal> AFF_QUOTITE = new ERXKey<java.math.BigDecimal>("affQuotite");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	// Relationships
	public static final String TO_AFFECTATION_KEY = "toAffectation";
	public static final String TO_POSTE_KEY = "toPoste";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAffectation> TO_AFFECTATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAffectation>("toAffectation");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOPoste> TO_POSTE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOPoste>("toPoste");

  private static Logger LOG = Logger.getLogger(_EOAffectationDetail.class);

  public EOAffectationDetail localInstanceIn(EOEditingContext editingContext) {
    EOAffectationDetail localInstance = (EOAffectationDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer adeDateDiffAffectation() {
    return (Integer) storedValueForKey("adeDateDiffAffectation");
  }

  public void setAdeDateDiffAffectation(Integer value) {
    if (_EOAffectationDetail.LOG.isDebugEnabled()) {
    	_EOAffectationDetail.LOG.debug( "updating adeDateDiffAffectation from " + adeDateDiffAffectation() + " to " + value);
    }
    takeStoredValueForKey(value, "adeDateDiffAffectation");
  }

  public NSTimestamp adeDDebut() {
    return (NSTimestamp) storedValueForKey("adeDDebut");
  }

  public void setAdeDDebut(NSTimestamp value) {
    if (_EOAffectationDetail.LOG.isDebugEnabled()) {
    	_EOAffectationDetail.LOG.debug( "updating adeDDebut from " + adeDDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "adeDDebut");
  }

  public NSTimestamp adeDFin() {
    return (NSTimestamp) storedValueForKey("adeDFin");
  }

  public void setAdeDFin(NSTimestamp value) {
    if (_EOAffectationDetail.LOG.isDebugEnabled()) {
    	_EOAffectationDetail.LOG.debug( "updating adeDFin from " + adeDFin() + " to " + value);
    }
    takeStoredValueForKey(value, "adeDFin");
  }

  public java.math.BigDecimal affQuotite() {
    return (java.math.BigDecimal) storedValueForKey("affQuotite");
  }

  public void setAffQuotite(java.math.BigDecimal value) {
    if (_EOAffectationDetail.LOG.isDebugEnabled()) {
    	_EOAffectationDetail.LOG.debug( "updating affQuotite from " + affQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "affQuotite");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOAffectationDetail.LOG.isDebugEnabled()) {
    	_EOAffectationDetail.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOAffectationDetail.LOG.isDebugEnabled()) {
    	_EOAffectationDetail.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOAffectation toAffectation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOAffectation)storedValueForKey("toAffectation");
  }

  public void setToAffectationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAffectation value) {
    if (_EOAffectationDetail.LOG.isDebugEnabled()) {
      _EOAffectationDetail.LOG.debug("updating toAffectation from " + toAffectation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOAffectation oldValue = toAffectation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAffectation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toAffectation");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOPoste toPoste() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOPoste)storedValueForKey("toPoste");
  }

  public void setToPosteRelationship(org.cocktail.fwkcktlgrh.common.metier.EOPoste value) {
    if (_EOAffectationDetail.LOG.isDebugEnabled()) {
      _EOAffectationDetail.LOG.debug("updating toPoste from " + toPoste() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOPoste oldValue = toPoste();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPoste");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPoste");
    }
  }
  

  public static EOAffectationDetail create(EOEditingContext editingContext, Integer adeDateDiffAffectation
, NSTimestamp adeDDebut
, java.math.BigDecimal affQuotite
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlgrh.common.metier.EOAffectation toAffectation, org.cocktail.fwkcktlgrh.common.metier.EOPoste toPoste) {
    EOAffectationDetail eo = (EOAffectationDetail) EOUtilities.createAndInsertInstance(editingContext, _EOAffectationDetail.ENTITY_NAME);    
		eo.setAdeDateDiffAffectation(adeDateDiffAffectation);
		eo.setAdeDDebut(adeDDebut);
		eo.setAffQuotite(affQuotite);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToAffectationRelationship(toAffectation);
    eo.setToPosteRelationship(toPoste);
    return eo;
  }

  public static NSArray<EOAffectationDetail> fetchAll(EOEditingContext editingContext) {
    return _EOAffectationDetail.fetchAll(editingContext, null);
  }

  public static NSArray<EOAffectationDetail> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAffectationDetail.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOAffectationDetail> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOAffectationDetail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAffectationDetail> eoObjects = (NSArray<EOAffectationDetail>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOAffectationDetail fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAffectationDetail.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAffectationDetail fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAffectationDetail> eoObjects = _EOAffectationDetail.fetch(editingContext, qualifier, null);
    EOAffectationDetail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOAffectationDetail)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_AffectationDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAffectationDetail fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAffectationDetail.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOAffectationDetail fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAffectationDetail eoObject = _EOAffectationDetail.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_AffectationDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOAffectationDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAffectationDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAffectationDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAffectationDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOAffectationDetail localInstanceIn(EOEditingContext editingContext, EOAffectationDetail eo) {
    EOAffectationDetail localInstance = (eo == null) ? null : (EOAffectationDetail)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
