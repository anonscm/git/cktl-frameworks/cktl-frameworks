/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EODetailCrct.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EODetailCrct extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_DetailCrct";

	// Attributes
	public static final String CRCT_ORDRE_KEY = "crctOrdre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_CRCT_KEY = "dDebCrct";
	public static final String D_FIN_CRCT_KEY = "dFinCrct";
	public static final String D_MODIFICATION_KEY = "dModification";

	public static final ERXKey<Integer> CRCT_ORDRE = new ERXKey<Integer>("crctOrdre");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_CRCT = new ERXKey<NSTimestamp>("dDebCrct");
	public static final ERXKey<NSTimestamp> D_FIN_CRCT = new ERXKey<NSTimestamp>("dFinCrct");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	// Relationships
	public static final String TO_CRCT_KEY = "toCrct";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOCrct> TO_CRCT = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOCrct>("toCrct");

  private static Logger LOG = Logger.getLogger(_EODetailCrct.class);

  public EODetailCrct localInstanceIn(EOEditingContext editingContext) {
    EODetailCrct localInstance = (EODetailCrct)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer crctOrdre() {
    return (Integer) storedValueForKey("crctOrdre");
  }

  public void setCrctOrdre(Integer value) {
    if (_EODetailCrct.LOG.isDebugEnabled()) {
    	_EODetailCrct.LOG.debug( "updating crctOrdre from " + crctOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "crctOrdre");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODetailCrct.LOG.isDebugEnabled()) {
    	_EODetailCrct.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebCrct() {
    return (NSTimestamp) storedValueForKey("dDebCrct");
  }

  public void setDDebCrct(NSTimestamp value) {
    if (_EODetailCrct.LOG.isDebugEnabled()) {
    	_EODetailCrct.LOG.debug( "updating dDebCrct from " + dDebCrct() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebCrct");
  }

  public NSTimestamp dFinCrct() {
    return (NSTimestamp) storedValueForKey("dFinCrct");
  }

  public void setDFinCrct(NSTimestamp value) {
    if (_EODetailCrct.LOG.isDebugEnabled()) {
    	_EODetailCrct.LOG.debug( "updating dFinCrct from " + dFinCrct() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinCrct");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODetailCrct.LOG.isDebugEnabled()) {
    	_EODetailCrct.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOCrct toCrct() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOCrct)storedValueForKey("toCrct");
  }

  public void setToCrctRelationship(org.cocktail.fwkcktlgrh.common.metier.EOCrct value) {
    if (_EODetailCrct.LOG.isDebugEnabled()) {
      _EODetailCrct.LOG.debug("updating toCrct from " + toCrct() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOCrct oldValue = toCrct();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCrct");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCrct");
    }
  }
  

  public static EODetailCrct create(EOEditingContext editingContext, Integer crctOrdre
, NSTimestamp dCreation
, NSTimestamp dDebCrct
, NSTimestamp dFinCrct
, NSTimestamp dModification
, org.cocktail.fwkcktlgrh.common.metier.EOCrct toCrct) {
    EODetailCrct eo = (EODetailCrct) EOUtilities.createAndInsertInstance(editingContext, _EODetailCrct.ENTITY_NAME);    
		eo.setCrctOrdre(crctOrdre);
		eo.setDCreation(dCreation);
		eo.setDDebCrct(dDebCrct);
		eo.setDFinCrct(dFinCrct);
		eo.setDModification(dModification);
    eo.setToCrctRelationship(toCrct);
    return eo;
  }

  public static NSArray<EODetailCrct> fetchAll(EOEditingContext editingContext) {
    return _EODetailCrct.fetchAll(editingContext, null);
  }

  public static NSArray<EODetailCrct> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODetailCrct.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EODetailCrct> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODetailCrct.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODetailCrct> eoObjects = (NSArray<EODetailCrct>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODetailCrct fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EODetailCrct.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODetailCrct fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODetailCrct> eoObjects = _EODetailCrct.fetch(editingContext, qualifier, null);
    EODetailCrct eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODetailCrct)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_DetailCrct that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODetailCrct fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EODetailCrct.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EODetailCrct fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EODetailCrct eoObject = _EODetailCrct.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_DetailCrct that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EODetailCrct fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODetailCrct fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODetailCrct eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODetailCrct)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EODetailCrct localInstanceIn(EOEditingContext editingContext, EODetailCrct eo) {
    EODetailCrct localInstance = (eo == null) ? null : (EODetailCrct)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
