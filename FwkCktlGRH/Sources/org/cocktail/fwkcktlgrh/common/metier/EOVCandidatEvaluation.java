/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVCandidatEvaluation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOVCandidatEvaluation extends _EOVCandidatEvaluation implements IVCandidatEvaluation {
  private static Logger log = Logger.getLogger(EOVCandidatEvaluation.class);

  public EOVCandidatEvaluation() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }

	// ajouts

	public static final String D_TENUE_ENTRETIEN_KEY = "dTenueEntretien";
	public static final String D_VISA_RESPONSABLE_RH_KEY = "dVisaResponsableRh";

	// les classements possibles
	public static final String SORT_AGENT = TO_INDIVIDU_KEY + "." + EOIndividu.NOM_PRENOM_KEY;
	public static final String SORT_DATE_ENTRETIEN = D_TENUE_ENTRETIEN_KEY;
	public static final String SORT_DATE_VISA_RH = D_VISA_RESPONSABLE_RH_KEY;

	/**
	 * Indique si l'enregistrement EOEvaluation existe
	 */
	private boolean isEOEvaluationCreee() {
		return toEvaluation() != null;
	}

	/**
   * 
   */
	public boolean isViseParResponsableRh() {
		boolean result = false;
		if (isEOEvaluationCreee()) {
			result = toEvaluation().isViseParResponsableRh();
		}
		return result;
	}

	/**
   * 
   */
	public NSTimestamp dTenueEntretien() {
		NSTimestamp result = null;
		if (isEOEvaluationCreee()) {
			result = toEvaluation().dTenueEntretien();
		}
		return result;
	}

	/**
   * 
   */
	public NSTimestamp dVisaResponsableRh() {
		NSTimestamp result = null;
		if (isViseParResponsableRh()) {
			result = toEvaluation().dVisaResponsableRh();
		}
		return result;
	}

	/**
	 * La fenetre volante sur la case à cocher visa RH
	 */
	public String tipChkDVisaResponsableRh() {
		String str = "";

		if (isEOEvaluationCreee() && toEvaluation().isViseParResponsableRh()) {
			str = "Visé RH le " + DateCtrl.dateToString(toEvaluation().dVisaResponsableRh());
		} else {
			str = "Pas encore visé";
		}

		return str;
	}

	public String display() {
		return toIndividu().display() + " du "
				+ DateCtrl.dateToString(toEvaluationPeriode().epeDDebut()) + " au "
				+ DateCtrl.dateToString(toEvaluationPeriode().epeDFin());
	}

	/**
	 * Retourne l'enregistrement associé à un individu et une periode
	 * 
	 * @param individu
	 * @param periode
	 * @return
	 */
	public static EOVCandidatEvaluation findRecordForIndividuAndPeriode(EOIndividu individu, EOEvaluationPeriode periode) {
		EOEditingContext ec = individu.editingContext();
		return fetchRequired(ec,
					CktlDataBus.newCondition(TO_INDIVIDU_KEY + "=%@ and " + TO_EVALUATION_PERIODE_KEY + " =%@",
							new NSArray(new Object[] { individu, periode })));
	}

	@Override
	public EOVCandidatEvaluation toVCandidatEvaluation() {
		return this;
	}

	public void setIsViseParResponsableRh(boolean value) {
		if (toEvaluation() != null) {
			toEvaluation().setIsViseParResponsableRh(value);
		}
	}
  
  
}
