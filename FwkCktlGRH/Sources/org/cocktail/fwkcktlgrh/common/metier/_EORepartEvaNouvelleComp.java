/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartEvaNouvelleComp.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartEvaNouvelleComp extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_RepartEvaNouvelleComp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	// Relationships
	public static final String TO_COMPETENCE_KEY = "toCompetence";
	public static final String TO_EVALUATION_KEY = "toEvaluation";
	public static final String TO_REFERENS_COMPETENCES_KEY = "toReferensCompetences";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence> TO_COMPETENCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence>("toCompetence");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> TO_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>("toEvaluation");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences> TO_REFERENS_COMPETENCES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences>("toReferensCompetences");

  private static Logger LOG = Logger.getLogger(_EORepartEvaNouvelleComp.class);

  public EORepartEvaNouvelleComp localInstanceIn(EOEditingContext editingContext) {
    EORepartEvaNouvelleComp localInstance = (EORepartEvaNouvelleComp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartEvaNouvelleComp.LOG.isDebugEnabled()) {
    	_EORepartEvaNouvelleComp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartEvaNouvelleComp.LOG.isDebugEnabled()) {
    	_EORepartEvaNouvelleComp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCompetence toCompetence() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCompetence)storedValueForKey("toCompetence");
  }

  public void setToCompetenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompetence value) {
    if (_EORepartEvaNouvelleComp.LOG.isDebugEnabled()) {
      _EORepartEvaNouvelleComp.LOG.debug("updating toCompetence from " + toCompetence() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCompetence oldValue = toCompetence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCompetence");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCompetence");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluation)storedValueForKey("toEvaluation");
  }

  public void setToEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation value) {
    if (_EORepartEvaNouvelleComp.LOG.isDebugEnabled()) {
      _EORepartEvaNouvelleComp.LOG.debug("updating toEvaluation from " + toEvaluation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluation oldValue = toEvaluation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluation");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences toReferensCompetences() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences)storedValueForKey("toReferensCompetences");
  }

  public void setToReferensCompetencesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences value) {
    if (_EORepartEvaNouvelleComp.LOG.isDebugEnabled()) {
      _EORepartEvaNouvelleComp.LOG.debug("updating toReferensCompetences from " + toReferensCompetences() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences oldValue = toReferensCompetences();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toReferensCompetences");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toReferensCompetences");
    }
  }
  

  public static EORepartEvaNouvelleComp create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation) {
    EORepartEvaNouvelleComp eo = (EORepartEvaNouvelleComp) EOUtilities.createAndInsertInstance(editingContext, _EORepartEvaNouvelleComp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToEvaluationRelationship(toEvaluation);
    return eo;
  }

  public static NSArray<EORepartEvaNouvelleComp> fetchAll(EOEditingContext editingContext) {
    return _EORepartEvaNouvelleComp.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartEvaNouvelleComp> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartEvaNouvelleComp.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartEvaNouvelleComp> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartEvaNouvelleComp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartEvaNouvelleComp> eoObjects = (NSArray<EORepartEvaNouvelleComp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartEvaNouvelleComp fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartEvaNouvelleComp.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartEvaNouvelleComp fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartEvaNouvelleComp> eoObjects = _EORepartEvaNouvelleComp.fetch(editingContext, qualifier, null);
    EORepartEvaNouvelleComp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartEvaNouvelleComp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartEvaNouvelleComp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartEvaNouvelleComp fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartEvaNouvelleComp.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartEvaNouvelleComp fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartEvaNouvelleComp eoObject = _EORepartEvaNouvelleComp.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartEvaNouvelleComp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartEvaNouvelleComp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartEvaNouvelleComp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartEvaNouvelleComp eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartEvaNouvelleComp)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartEvaNouvelleComp localInstanceIn(EOEditingContext editingContext, EORepartEvaNouvelleComp eo) {
    EORepartEvaNouvelleComp localInstance = (eo == null) ? null : (EORepartEvaNouvelleComp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
