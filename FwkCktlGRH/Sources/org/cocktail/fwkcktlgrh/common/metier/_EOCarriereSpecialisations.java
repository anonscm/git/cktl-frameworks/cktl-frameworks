/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOCarriereSpecialisations.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOCarriereSpecialisations extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_CarriereSpecialisations";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String SPEC_DEBUT_KEY = "specDebut";
	public static final String SPEC_FIN_KEY = "specFin";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
	public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
	public static final ERXKey<NSTimestamp> SPEC_DEBUT = new ERXKey<NSTimestamp>("specDebut");
	public static final ERXKey<NSTimestamp> SPEC_FIN = new ERXKey<NSTimestamp>("specFin");
	// Relationships
	public static final String TO_BAP_KEY = "toBap";
	public static final String TO_CARRIERE_KEY = "toCarriere";
	public static final String TO_CNU_KEY = "toCnu";
	public static final String TO_DISC_SECOND_DEGRE_KEY = "toDiscSecondDegre";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_REFERENS_EMPLOIS_KEY = "toReferensEmplois";
	public static final String TO_SPECIALITE_ATOS_KEY = "toSpecialiteAtos";
	public static final String TO_SPECIALITE_ITARF_KEY = "toSpecialiteItarf";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOBap> TO_BAP = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOBap>("toBap");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOCarriere> TO_CARRIERE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOCarriere>("toCarriere");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> TO_CNU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu>("toCnu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODiscSecondDegre> TO_DISC_SECOND_DEGRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODiscSecondDegre>("toDiscSecondDegre");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> TO_REFERENS_EMPLOIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>("toReferensEmplois");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos> TO_SPECIALITE_ATOS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos>("toSpecialiteAtos");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteItarf> TO_SPECIALITE_ITARF = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteItarf>("toSpecialiteItarf");

  private static Logger LOG = Logger.getLogger(_EOCarriereSpecialisations.class);

  public EOCarriereSpecialisations localInstanceIn(EOEditingContext editingContext) {
    EOCarriereSpecialisations localInstance = (EOCarriereSpecialisations)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Integer value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Integer value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public NSTimestamp specDebut() {
    return (NSTimestamp) storedValueForKey("specDebut");
  }

  public void setSpecDebut(NSTimestamp value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating specDebut from " + specDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "specDebut");
  }

  public NSTimestamp specFin() {
    return (NSTimestamp) storedValueForKey("specFin");
  }

  public void setSpecFin(NSTimestamp value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
    	_EOCarriereSpecialisations.LOG.debug( "updating specFin from " + specFin() + " to " + value);
    }
    takeStoredValueForKey(value, "specFin");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOBap toBap() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOBap)storedValueForKey("toBap");
  }

  public void setToBapRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOBap value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
      _EOCarriereSpecialisations.LOG.debug("updating toBap from " + toBap() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOBap oldValue = toBap();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toBap");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toBap");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOCarriere toCarriere() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.fwkcktlgrh.common.metier.EOCarriere value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
      _EOCarriereSpecialisations.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCnu toCnu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCnu)storedValueForKey("toCnu");
  }

  public void setToCnuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCnu value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
      _EOCarriereSpecialisations.LOG.debug("updating toCnu from " + toCnu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCnu oldValue = toCnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCnu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCnu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EODiscSecondDegre toDiscSecondDegre() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EODiscSecondDegre)storedValueForKey("toDiscSecondDegre");
  }

  public void setToDiscSecondDegreRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODiscSecondDegre value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
      _EOCarriereSpecialisations.LOG.debug("updating toDiscSecondDegre from " + toDiscSecondDegre() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EODiscSecondDegre oldValue = toDiscSecondDegre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toDiscSecondDegre");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toDiscSecondDegre");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
      _EOCarriereSpecialisations.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois toReferensEmplois() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois)storedValueForKey("toReferensEmplois");
  }

  public void setToReferensEmploisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
      _EOCarriereSpecialisations.LOG.debug("updating toReferensEmplois from " + toReferensEmplois() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois oldValue = toReferensEmplois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toReferensEmplois");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toReferensEmplois");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos toSpecialiteAtos() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos)storedValueForKey("toSpecialiteAtos");
  }

  public void setToSpecialiteAtosRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
      _EOCarriereSpecialisations.LOG.debug("updating toSpecialiteAtos from " + toSpecialiteAtos() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos oldValue = toSpecialiteAtos();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSpecialiteAtos");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSpecialiteAtos");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteItarf toSpecialiteItarf() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteItarf)storedValueForKey("toSpecialiteItarf");
  }

  public void setToSpecialiteItarfRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteItarf value) {
    if (_EOCarriereSpecialisations.LOG.isDebugEnabled()) {
      _EOCarriereSpecialisations.LOG.debug("updating toSpecialiteItarf from " + toSpecialiteItarf() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteItarf oldValue = toSpecialiteItarf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSpecialiteItarf");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSpecialiteItarf");
    }
  }
  

  public static EOCarriereSpecialisations create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, NSTimestamp specDebut
, org.cocktail.fwkcktlgrh.common.metier.EOCarriere toCarriere, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu) {
    EOCarriereSpecialisations eo = (EOCarriereSpecialisations) EOUtilities.createAndInsertInstance(editingContext, _EOCarriereSpecialisations.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setSpecDebut(specDebut);
    eo.setToCarriereRelationship(toCarriere);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  public static NSArray<EOCarriereSpecialisations> fetchAll(EOEditingContext editingContext) {
    return _EOCarriereSpecialisations.fetchAll(editingContext, null);
  }

  public static NSArray<EOCarriereSpecialisations> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCarriereSpecialisations.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOCarriereSpecialisations> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCarriereSpecialisations.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCarriereSpecialisations> eoObjects = (NSArray<EOCarriereSpecialisations>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCarriereSpecialisations fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCarriereSpecialisations.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCarriereSpecialisations fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCarriereSpecialisations> eoObjects = _EOCarriereSpecialisations.fetch(editingContext, qualifier, null);
    EOCarriereSpecialisations eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCarriereSpecialisations)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_CarriereSpecialisations that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCarriereSpecialisations fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCarriereSpecialisations.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOCarriereSpecialisations fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCarriereSpecialisations eoObject = _EOCarriereSpecialisations.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_CarriereSpecialisations that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOCarriereSpecialisations fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCarriereSpecialisations fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCarriereSpecialisations eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCarriereSpecialisations)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOCarriereSpecialisations localInstanceIn(EOEditingContext editingContext, EOCarriereSpecialisations eo) {
    EOCarriereSpecialisations localInstance = (eo == null) ? null : (EOCarriereSpecialisations)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
