/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOModalitesService.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOModalitesService extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_ModalitesService";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String ID_KEY = "id";
	public static final String LIBELLE_KEY = "libelle";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String QUOTITE_KEY = "quotite";
	public static final String TYPE_KEY = "type";
	public static final String VALIDE_KEY = "valide";

	public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
	public static final ERXKey<NSTimestamp> DATE_ARRETE = new ERXKey<NSTimestamp>("dateArrete");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
	public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
	public static final ERXKey<String> NO_ARRETE = new ERXKey<String>("noArrete");
	public static final ERXKey<Integer> QUOTITE = new ERXKey<Integer>("quotite");
	public static final ERXKey<String> TYPE = new ERXKey<String>("type");
	public static final ERXKey<String> VALIDE = new ERXKey<String>("valide");
	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_TYPE_ABSENCE_KEY = "toTypeAbsence";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("individu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence> TO_TYPE_ABSENCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence>("toTypeAbsence");

  private static Logger LOG = Logger.getLogger(_EOModalitesService.class);

  public EOModalitesService localInstanceIn(EOEditingContext editingContext) {
    EOModalitesService localInstance = (EOModalitesService)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOModalitesService.LOG.isDebugEnabled()) {
    	_EOModalitesService.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOModalitesService.LOG.isDebugEnabled()) {
    	_EOModalitesService.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOModalitesService.LOG.isDebugEnabled()) {
    	_EOModalitesService.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOModalitesService.LOG.isDebugEnabled()) {
    	_EOModalitesService.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public Integer id() {
    return (Integer) storedValueForKey("id");
  }

  public void setId(Integer value) {
    if (_EOModalitesService.LOG.isDebugEnabled()) {
    	_EOModalitesService.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, "id");
  }

  public String libelle() {
    return (String) storedValueForKey("libelle");
  }

  public void setLibelle(String value) {
    if (_EOModalitesService.LOG.isDebugEnabled()) {
    	_EOModalitesService.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, "libelle");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOModalitesService.LOG.isDebugEnabled()) {
    	_EOModalitesService.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public Integer quotite() {
    return (Integer) storedValueForKey("quotite");
  }

  public void setQuotite(Integer value) {
    if (_EOModalitesService.LOG.isDebugEnabled()) {
    	_EOModalitesService.LOG.debug( "updating quotite from " + quotite() + " to " + value);
    }
    takeStoredValueForKey(value, "quotite");
  }

  public String type() {
    return (String) storedValueForKey("type");
  }

  public void setType(String value) {
    if (_EOModalitesService.LOG.isDebugEnabled()) {
    	_EOModalitesService.LOG.debug( "updating type from " + type() + " to " + value);
    }
    takeStoredValueForKey(value, "type");
  }

  public String valide() {
    return (String) storedValueForKey("valide");
  }

  public void setValide(String value) {
    if (_EOModalitesService.LOG.isDebugEnabled()) {
    	_EOModalitesService.LOG.debug( "updating valide from " + valide() + " to " + value);
    }
    takeStoredValueForKey(value, "valide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOModalitesService.LOG.isDebugEnabled()) {
      _EOModalitesService.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence toTypeAbsence() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence)storedValueForKey("toTypeAbsence");
  }

  public void setToTypeAbsenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence value) {
    if (_EOModalitesService.LOG.isDebugEnabled()) {
      _EOModalitesService.LOG.debug("updating toTypeAbsence from " + toTypeAbsence() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence oldValue = toTypeAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeAbsence");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeAbsence");
    }
  }
  

  public static EOModalitesService create(EOEditingContext editingContext, NSTimestamp dateDebut
, Integer id
, String libelle
, Integer quotite
, String type
, String valide
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu, org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence toTypeAbsence) {
    EOModalitesService eo = (EOModalitesService) EOUtilities.createAndInsertInstance(editingContext, _EOModalitesService.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setQuotite(quotite);
		eo.setType(type);
		eo.setValide(valide);
    eo.setIndividuRelationship(individu);
    eo.setToTypeAbsenceRelationship(toTypeAbsence);
    return eo;
  }

  public static NSArray<EOModalitesService> fetchAll(EOEditingContext editingContext) {
    return _EOModalitesService.fetchAll(editingContext, null);
  }

  public static NSArray<EOModalitesService> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOModalitesService.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOModalitesService> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOModalitesService.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOModalitesService> eoObjects = (NSArray<EOModalitesService>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOModalitesService fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOModalitesService.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOModalitesService fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOModalitesService> eoObjects = _EOModalitesService.fetch(editingContext, qualifier, null);
    EOModalitesService eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOModalitesService)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_ModalitesService that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOModalitesService fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOModalitesService.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOModalitesService fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOModalitesService eoObject = _EOModalitesService.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_ModalitesService that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOModalitesService fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOModalitesService fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOModalitesService eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOModalitesService)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOModalitesService localInstanceIn(EOEditingContext editingContext, EOModalitesService eo) {
    EOModalitesService localInstance = (eo == null) ? null : (EOModalitesService)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
