/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IRepartFdpComp;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_RepartCompetence;
import org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EORepartFdpComp extends _EORepartFdpComp implements I_RepartCompetence, IRepartFdpComp {
  private static Logger log = Logger.getLogger(EORepartFdpComp.class);

  public EORepartFdpComp() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }


//// Methodes rajoutees
	//
	// private void checkRfcPosition() {
	// // on met la position max par defaut
	// NSArray<EORepartFdpComp> lesAutresRepart =
	// toFicheDePoste().tosRepartFdpComp(null, false);
	// if (lesAutresRepart.count() > 0) {
	// Number position = (Number) lesAutresRepart.valueForKey("@max." +
	// RFC_POSITION_KEY);
	// if (position != null) {
	// setRfcPosition(new Integer(position.intValue() + 1));
	// }
	// }
	// if (rfcPosition() == null) {
	// setRfcPosition(new Integer(1));
	// }
	// }

	public NSArray othersRecords() {
		return toFicheDePoste().tosRepartFdpComp();
	}

	public String positionKey() {
		return RFC_POSITION_KEY;
	}

	public String competenceDisplay() {
		return toReferensCompetences().displayLong();
	}

	/**
	 * @param ec
	 * @param ficheDePoste
	 * @param competence
	 * @return
	 */
	protected static EORepartFdpComp newRecordInContext(
				EOEditingContext ec,
				EOFicheDePoste ficheDePoste,
				EOReferensCompetences competence
			) {
		/*
		 * EORepartFdpComp newRecord =
		 * EORepartFdpComp.newDefaultRecordInContext(ec);
		 * 
		 * newRecord.setToReferensCompetencesRelationship(competence);
		 * newRecord.setToFicheDePosteRelationship(ficheDePoste);
		 * ficheDePoste.addToTosRepartFdpCompRelationship(newRecord);
		 */

		// newRecord.checkRfcPosition();

		EORepartFdpComp eoRepartFdpComp = create(ec, DateCtrl.now(), DateCtrl.now(), ficheDePoste);
		eoRepartFdpComp.setToReferensCompetencesRelationship(competence);

		return eoRepartFdpComp;
	}

	/**
	 * Remonter
	 */
		
  	public void up() {
			// la position actuelle
			int position = (Integer) valueForKey(positionKey());
			// on ne remonte pas si c'est le premier element
		    if (position != 0) {
		    	// retrouver l'element precedent
		    	NSArray recsPrev = EOQualifier.filteredArrayWithQualifier(othersRecords(),
		    			CktlDataBus.newCondition(positionKey() + "=" + (position - 1)));
		    	if (recsPrev.count() == 1) {
		    		EORepartFdpComp recPrev = (EORepartFdpComp) recsPrev.objectAtIndex(0);
		    		recPrev.takeValueForKey(new Integer(position), positionKey());
		    		this.takeValueForKey(new Integer(position - 1), positionKey());
		    	}
		    }
		}


	/**
	 * Redescendre
	 */
	public void down() {
			// la position actuelle
		int position = (Integer) valueForKey(positionKey());
		// on ne descend pas si c'est le dernier element (on cherche les elements suivants)
		NSArray recsNext = EOQualifier.filteredArrayWithQualifier(othersRecords(),
				CktlDataBus.newCondition(positionKey() + ">" + position));
		if (recsNext.count() > 0) {
		// recuperer l'element suivant
		recsNext = EOQualifier.filteredArrayWithQualifier(othersRecords(),
			CktlDataBus.newCondition(positionKey() + "=" + (position + 1)));
			if (recsNext.count() == 1) {
				EORepartFdpComp recNext = (EORepartFdpComp) recsNext.objectAtIndex(0);
				recNext.takeValueForKey(new Integer(position), positionKey());
				this.takeValueForKey(new Integer(position + 1), positionKey());
			}
		}
	}
  
}
