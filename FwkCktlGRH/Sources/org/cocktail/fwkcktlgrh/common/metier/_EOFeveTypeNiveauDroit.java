/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOFeveTypeNiveauDroit.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOFeveTypeNiveauDroit extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_FeveTypeNiveauDroit";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PR_ID_KEY = "prId";
	public static final String TND_CODE_KEY = "tndCode";
	public static final String TND_LIBELLE_KEY = "tndLibelle";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> PR_ID = new ERXKey<Integer>("prId");
	public static final ERXKey<String> TND_CODE = new ERXKey<String>("tndCode");
	public static final ERXKey<String> TND_LIBELLE = new ERXKey<String>("tndLibelle");
	// Relationships
	public static final String TO_GD_PROFIL_KEY = "toGdProfil";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> TO_GD_PROFIL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>("toGdProfil");

  private static Logger LOG = Logger.getLogger(_EOFeveTypeNiveauDroit.class);

  public EOFeveTypeNiveauDroit localInstanceIn(EOEditingContext editingContext) {
    EOFeveTypeNiveauDroit localInstance = (EOFeveTypeNiveauDroit)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOFeveTypeNiveauDroit.LOG.isDebugEnabled()) {
    	_EOFeveTypeNiveauDroit.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOFeveTypeNiveauDroit.LOG.isDebugEnabled()) {
    	_EOFeveTypeNiveauDroit.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer prId() {
    return (Integer) storedValueForKey("prId");
  }

  public void setPrId(Integer value) {
    if (_EOFeveTypeNiveauDroit.LOG.isDebugEnabled()) {
    	_EOFeveTypeNiveauDroit.LOG.debug( "updating prId from " + prId() + " to " + value);
    }
    takeStoredValueForKey(value, "prId");
  }

  public String tndCode() {
    return (String) storedValueForKey("tndCode");
  }

  public void setTndCode(String value) {
    if (_EOFeveTypeNiveauDroit.LOG.isDebugEnabled()) {
    	_EOFeveTypeNiveauDroit.LOG.debug( "updating tndCode from " + tndCode() + " to " + value);
    }
    takeStoredValueForKey(value, "tndCode");
  }

  public String tndLibelle() {
    return (String) storedValueForKey("tndLibelle");
  }

  public void setTndLibelle(String value) {
    if (_EOFeveTypeNiveauDroit.LOG.isDebugEnabled()) {
    	_EOFeveTypeNiveauDroit.LOG.debug( "updating tndLibelle from " + tndLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "tndLibelle");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil toGdProfil() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil)storedValueForKey("toGdProfil");
  }

  public void setToGdProfilRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil value) {
    if (_EOFeveTypeNiveauDroit.LOG.isDebugEnabled()) {
      _EOFeveTypeNiveauDroit.LOG.debug("updating toGdProfil from " + toGdProfil() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil oldValue = toGdProfil();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toGdProfil");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toGdProfil");
    }
  }
  

  public static EOFeveTypeNiveauDroit create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer prId
, String tndCode
, String tndLibelle
, org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil toGdProfil) {
    EOFeveTypeNiveauDroit eo = (EOFeveTypeNiveauDroit) EOUtilities.createAndInsertInstance(editingContext, _EOFeveTypeNiveauDroit.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPrId(prId);
		eo.setTndCode(tndCode);
		eo.setTndLibelle(tndLibelle);
    eo.setToGdProfilRelationship(toGdProfil);
    return eo;
  }

  public static NSArray<EOFeveTypeNiveauDroit> fetchAll(EOEditingContext editingContext) {
    return _EOFeveTypeNiveauDroit.fetchAll(editingContext, null);
  }

  public static NSArray<EOFeveTypeNiveauDroit> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOFeveTypeNiveauDroit.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOFeveTypeNiveauDroit> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOFeveTypeNiveauDroit.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOFeveTypeNiveauDroit> eoObjects = (NSArray<EOFeveTypeNiveauDroit>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOFeveTypeNiveauDroit fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFeveTypeNiveauDroit.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFeveTypeNiveauDroit fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOFeveTypeNiveauDroit> eoObjects = _EOFeveTypeNiveauDroit.fetch(editingContext, qualifier, null);
    EOFeveTypeNiveauDroit eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOFeveTypeNiveauDroit)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_FeveTypeNiveauDroit that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFeveTypeNiveauDroit fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFeveTypeNiveauDroit.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOFeveTypeNiveauDroit fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOFeveTypeNiveauDroit eoObject = _EOFeveTypeNiveauDroit.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_FeveTypeNiveauDroit that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOFeveTypeNiveauDroit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFeveTypeNiveauDroit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFeveTypeNiveauDroit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFeveTypeNiveauDroit)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOFeveTypeNiveauDroit localInstanceIn(EOEditingContext editingContext, EOFeveTypeNiveauDroit eo) {
    EOFeveTypeNiveauDroit localInstance = (eo == null) ? null : (EOFeveTypeNiveauDroit)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
