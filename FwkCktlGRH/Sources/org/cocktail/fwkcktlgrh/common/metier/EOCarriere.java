/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import java.util.Date;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.ICarriere;
import org.cocktail.fwkcktlgrh.common.metier.services.CarrieresService;
import org.cocktail.fwkcktlgrh.common.utilities.CocktailConstantes;
import org.cocktail.fwkcktlgrh.common.utilities.DateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EOCarriere extends _EOCarriere implements ICarriere {

	public static EOSortOrdering SORT_DATE_DEBUT_ASC = new EOSortOrdering(D_DEB_CARRIERE_KEY, EOSortOrdering.CompareAscending);
	public static EOSortOrdering SORT_DATE_DEBUT_DESC = new EOSortOrdering(D_DEB_CARRIERE_KEY, EOSortOrdering.CompareDescending);

	public static NSArray SORT_ARRAY_DATE_DEBUT_ASC = new NSArray(SORT_DATE_DEBUT_ASC);
	public static NSArray SORT_ARRAY_DATE_DEBUT_DESC = new NSArray(SORT_DATE_DEBUT_DESC);
	
	private static final long serialVersionUID = -8879569880222036539L;
	private static Logger log = Logger.getLogger(EOCarriere.class);


	private static CarrieresService service = new CarrieresService();
	
	
	/** retourne true si un individu a une carri&egrave;re en cours pendant la p&eacute;riode
	 * @param editingContext
	 * @param individu
	 * @param dateDebut 
	 * @param dateFin   peut etre nul
	 */
	public static boolean aCarriereEnCoursSurPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin) {
		return rechercherCarrieresSurPeriode(editingContext, individu, dateDebut, dateFin, false).count() > 0;
	}

	/** retourne les carri&egrave;res valides d'un individu pendant la p&eacute;riode fournie
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode peut &ecirc;tre nulle
	 * @param finPeriode     peut &ecirc;tre nulle
	 * @param prefetch true si effectuer les prefetchs
	 */
	public static NSArray<EOCarriere> rechercherCarrieresSurPeriode(EOEditingContext editingContext, EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean prefetch) {
		return rechercherCarrieresSurPeriode(editingContext, individu.noIndividu(),
				debutPeriode, finPeriode, prefetch);
	}

	public static NSArray<EOCarriere> rechercherCarrieresSurPeriode(
			EOEditingContext editingContext, Integer noIndividu,
			NSTimestamp debutPeriode, NSTimestamp finPeriode, boolean prefetch) {
		EOQualifier qualifier = qualifierPourPeriode(noIndividu, debutPeriode, finPeriode);
		ERXFetchSpecification<EOCarriere> myFetch = new ERXFetchSpecification<EOCarriere>(EOCarriere.ENTITY_NAME, qualifier, null);
		if (prefetch) {
			NSMutableArray relations = new NSMutableArray("elements");
			relations.addObject("changementsPosition");
			myFetch.setPrefetchingRelationshipKeyPaths(relations);
		}
		myFetch.setRefreshesRefetchedObjects(true);
		myFetch.setIncludeEditingContextChanges(false);
		return myFetch.fetchObjects(editingContext);
	}
	
	
	public static EOCarriere rechercherCarriereSurPeriode(EOEditingContext editingContext, EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		EOQualifier qualifier = qualifierPourPeriode(individu.noIndividu(), debutPeriode, finPeriode);
		return fetchFirstByQualifier(editingContext, qualifier, null);		
	}
	
	public static EOCarriere rechercherCarriereSurPeriode(EOEditingContext editingContext, Integer noIndividu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		EOQualifier qualifier = qualifierPourPeriode(noIndividu, debutPeriode, finPeriode);
		return fetchFirstByQualifier(editingContext, qualifier, null);		
	}
	
	public static EOCarriere rechercherCarriereEnseignantSurPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		EOQualifier qualifierPeriode = qualifierPourPeriode(individu.noIndividu(), debutPeriode, finPeriode);
		EOQualifier qualifier = ERXQ.and(qualifierPeriode, TO_TYPE_POPULATION.dot(EOTypePopulation.TEM_ENSEIGNANT_KEY).eq("O"));
		return fetchFirstByQualifier(editingContext, qualifier, null);		
	}
	
	public static NSArray<EOCarriere> rechercherCarrieresDeTypePourDate(EOEditingContext editingContext,NSTimestamp dateRef, EOTypePopulation typePop) {

		EOQualifier qualifier = 
			ERXQ.and(
					ERXQ.equals(TEM_VALIDE_KEY, "O"),
					ERXQ.equals(TO_TYPE_POPULATION_KEY, typePop),
					ERXQ.lessThanOrEqualTo(D_DEB_CARRIERE_KEY, dateRef),
					ERXQ.or(
							ERXQ.isNull(D_FIN_CARRIERE_KEY), 
							ERXQ.greaterThanOrEqualTo(D_FIN_CARRIERE_KEY, dateRef))
			);

		ERXFetchSpecification<EOCarriere> myFetch = new ERXFetchSpecification<EOCarriere>(EOCarriere.ENTITY_NAME, qualifier, null);

		myFetch.setRefreshesRefetchedObjects(true);
		myFetch.setIncludeEditingContextChanges(false);
		return myFetch.fetchObjects(editingContext);
	}




	/** retourne le qualifier pour d&eacute;terminer les segments de carri&egrave;re sur une p&eacute;riode
	 * @param individu
	 * @param debutPeriode peut etre nulle
	 * @param finPeriode     peut etre nulle
	 */
	private static EOQualifier qualifierPourPeriode(Integer noIndividu,
			NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		NSMutableArray args = new NSMutableArray(noIndividu);
		EOQualifier qualifier = ERXQ.equals(ERXQ.keyPath(TO_PERSONNEL_KEY, EOPersonnel.TO_INDIVIDU_KEY, EOIndividu.NO_INDIVIDU_KEY), noIndividu).and(ERXQ.equals(TEM_VALIDE_KEY, "O"));
		qualifiers.addObject(qualifier);
		if (debutPeriode != null) {
			qualifier = GRHUtilities.qualifierPourPeriode("dDebCarriere", debutPeriode, "dFinCarriere", finPeriode);
			qualifiers.addObject(qualifier);
			qualifier = new EOAndQualifier(qualifiers);
		}
		return qualifier;
	}

	/**
	 * renvoie toutes les carrières valides d'un individu
	 * @param ec
	 * @param ind
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOCarriere> carrieresForIndividu(EOEditingContext ec, EOIndividu ind) {
		if (ind == null) {
			return null;
		}	
		EOQualifier qual = 
			ERXQ.and(
					ERXQ.equals(
							ERXQ.keyPath(TO_PERSONNEL_KEY, EOPersonnel.TO_INDIVIDU_KEY), ind), 
							ERXQ.equals(EOCarriere.TEM_VALIDE_KEY, "O") 
			);
		NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(D_DEB_CARRIERE_KEY).array();
		return fetchAll(ec, qual, sorts);
	}

	public Date getDtDebut() {
		return dDebCarriere();
	}
	
	public Date getDtFin() {
		return dFinCarriere();
	}
	
	public String dateDebCarriere() {
		return service.formatDate(getDtDebut());
	}

	public String dateFinCarriere() {
		return service.formatDate(getDtFin());
	}

	public NSArray<EOElements> elementsTries() {    	
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(toElements(), EOElements.SORT_ARRAY_DATE_DEBUT_DESC); 
	}

	public EOElements elementActuel() {

		EOQualifier qualifier = 
			ERXQ.and(
					ERXQ.lessThanOrEqualTo(EOElements.D_EFFET_ELEMENT_KEY, MyDateCtrl.getDateJour()),
					ERXQ.or(
							ERXQ.isNull(EOElements.D_FIN_ELEMENT_KEY),
							ERXQ.greaterThanOrEqualTo(EOElements.D_FIN_ELEMENT_KEY, MyDateCtrl.getDateJour())
					)
			);
		NSArray<EOElements> elements = super.toElements(qualifier);
		if (!elements.isEmpty()) {
			return elements.get(0);
		}
		return null;
	}  

	/**
	 * retourne le qualifier pour d&eacute;terminer la carrière en cours valide d'un
	 * individu
	 * @param individu
	 * @return
	 */
	public static EOQualifier qualifierCarriereEnCours(EOIndividu individu) {
		EOQualifier qualifier = 
			ERXQ.and(
					ERXQ.lessThanOrEqualTo(EOCarriere.D_DEB_CARRIERE_KEY, MyDateCtrl.getDateJour()),
					ERXQ.or(
							ERXQ.isNull(EOCarriere.D_FIN_CARRIERE_KEY), 
							ERXQ.greaterThanOrEqualTo(EOCarriere.D_FIN_CARRIERE_KEY, MyDateCtrl.getDateJour())),
							ERXQ.equals(TEM_VALIDE_KEY, "O"),
							ERXQ.equals(NO_DOSSIER_PERS_KEY, individu.getNumeroInt())
			);

		return qualifier;
	}

	/**
	 * recherche la carrière en cours d'un individu
	 * @param editingContext
	 * @param individu
	 * @return
	 */
	public static EOCarriere rechercherCarriereEnCours(EOEditingContext editingContext, 
			EOIndividu individu) {
		EOQualifier qualifier = qualifierCarriereEnCours(individu);
		return fetch(editingContext, qualifier);
	}

	/** recherche les segments de carriere valides d'un individu anterieurs a la date fournie
	 * @param editingContext
	 * @param individu
	 * @return tableau des elements de carriere
	 */
	public static NSArray<EOCarriere> rechercherCarrieresPourIndividuAnterieursADate(EOEditingContext ec,EOIndividu individu,NSTimestamp date) {

		try {

			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_DEB_CARRIERE_KEY + "<=%@", new NSArray(date)));

			return fetchAll(ec, new EOAndQualifier(qualifiers), null);			
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/** Retourne les elements de carriere valides pendant la periode passee en parametre
	 * @param dateDebut
	 * @param dateFin peut etre nulle
	 * @return tableau d'elements ou null
	 */
	public NSArray elementsPourPeriode(NSTimestamp dateDebut,NSTimestamp dateFin) {

		if (dateDebut == null || toElements() == null || toElements().count() == 0) {
			return null;
		}
		NSArray<EOElements> elementsTries = EOSortOrdering.sortedArrayUsingKeyOrderArray(toElements(), EOElements.SORT_ARRAY_DATE_DEBUT_ASC);
		NSMutableArray<EOElements> results = new NSMutableArray<EOElements>();
		java.util.Enumeration<EOElements> e = elementsTries.objectEnumerator();
		while (e.hasMoreElements()) {
			EOElements element = e.nextElement();
			if (element.estValide()) {

				if (dateFin == null) {
					if (DateCtrl.isAfterEq(element.dEffetElement(),dateDebut) || element.dFinElement() == null || 
							DateCtrl.isAfterEq(element.dFinElement(),dateDebut)) {
						results.addObject(element);
					}
				} else {
					if (!((element.dFinElement() != null && DateCtrl.isBefore(element.dFinElement(),dateDebut)) || DateCtrl.isAfter(element.dEffetElement(),dateFin))) {
						results.addObject((element));
					}
				}
			}
		}
		return results;
	}
}
