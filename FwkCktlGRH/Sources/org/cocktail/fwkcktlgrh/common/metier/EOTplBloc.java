/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.ITplBloc;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOTplBloc extends _EOTplBloc implements ITplBloc {
  private static Logger log = Logger.getLogger(EOTplBloc.class);
  
  //noms des blocs
  public static final String TPL_BLOC_COMPETENCES_PROFESSIONNELLES_ET_TECHNICITE_CODE = "COMPPROTEC";
  public static final String TPL_BLOC_MANAGEMENT_CODE = "MANAGEMENT";
//  public static final String TPL_BLOC_CONTRIBUTION_A_L_ACTIVITE_DU_SERVICE_CODE = "CONACTSERV";
//  public static final String TPL_BLOC_QUALITES_PERSONNELLES_ET_RELATIONNELLES_CODE = "QUAPERSREL";

  public EOTplBloc() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }

	// methodes rajoutees

	public boolean isFacultatif() {
		return tblFacultatif().equals(OUI);
	}

	/**
	 * Surcharge du lien vers tosTplItem pour avoir un classement selon le temoin
	 * <code>titPosition</code>
	 */
	public NSArray<EOTplItem> tosTplItemSorted() {
		NSArray<EOTplItem> tosTplItem = tosTplItem();

		tosTplItem = CktlSort.sortedArray(tosTplItem, EOTplItem.TIT_POSITION_KEY);

		return tosTplItem;
	}

	/**
	 * Definition de l'ancre HTML associee au bloc (chaine + clé primaire)
	 */
	public String anchorName() {
		Number tblKey = (Number) EOUtilities.primaryKeyForObject(editingContext(), this).valueForKey("tblKey");
		return "tplBloc-" + Integer.toString(tblKey.intValue());
	}

	/**
	 * Bloc formation : geres par l'interface <code>CompTplBlocFormation</code>
	 * 
	 * @return
	 */
	public boolean isBlocNatureFormation() {
		boolean isFormation = false;
		if (toTplBlocNature().tbnLibelle().equals(EOTplBlocNature.TPL_BLOC_NATURE_FORMATION)) {
			isFormation = true;
		}
		return isFormation;
	}

	/**
	 * Bloc management 
	 * 
	 * @return
	 */

	public boolean isBlocManagementCode() {	
		return EOTplBloc.TPL_BLOC_MANAGEMENT_CODE.equals(tblCode());		
	}
	
	/**
	 * Bloc formation souhaitee : geres par l'interface
	 * <code>CompTplBlocFormation</code>
	 * 
	 * @return
	 */
	public boolean isBlocNatureFormationSouhaitee() {
		boolean isBlocNatureFormationSouhaitee = false;
		if (toTplBlocNature().tbnLibelle().equals(EOTplBlocNature.TPL_BLOC_NATURE_FORMATION_SOUHAITEE)) {
			isBlocNatureFormationSouhaitee = true;
		}
		return isBlocNatureFormationSouhaitee;
	}

	/**
	 * Bloc notice de promotions : geres par l'interface
	 * {@link CompNoticePromotion}
	 * 
	 * @return
	 */
	public boolean isBlocNatureNoticeDePromotions() {
		boolean isBlocNatureNoticeDePromotions = false;
		if (toTplBlocNature().tbnLibelle().equals(EOTplBlocNature.TPL_BLOC_NOTICE_DE_PROMOTIONS)) {
			isBlocNatureNoticeDePromotions = true;
		}
		return isBlocNatureNoticeDePromotions;
	}

	/**
	 * Bloc dynamiques : geres par <code>CompTplOnglet</code>
	 * 
	 * @return
	 */
	public boolean isBlocNatureDynamique() {
		boolean isDynamique = false;
		if (toTplBlocNature().tbnLibelle().equals(EOTplBlocNature.TPL_BLOC_NATURE_DYNAMIQUE)) {
			isDynamique = true;
		}
		return isDynamique;
	}

	/**
	 * TODO faire generique recFiche : la fiche (fiche de poste, fiche LOLF ou
	 * fiche d'evaluation) Retrouver un enregistrement associe.
	 * 
	 * @param ec
	 * @param recTplBloc
	 *          : le bloc facultatif
	 * @param recEvaluation
	 *          : la fiche (fiche de poste, fiche LOLF ou fiche d'evaluation)
	 * @return <em>null</em> si non trouvé.
	 */
	public EORepartFicheBlocActivation getActivationRecord(EOEvaluation recEvaluation) {
		EORepartFicheBlocActivation result = null;

		EOQualifier qual = CktlDataBus.newCondition(
					EORepartFicheBlocActivation.TO_EVALUATION_KEY + " = %@",
					new NSArray<EOEvaluation>(recEvaluation));

		NSArray<EORepartFicheBlocActivation> array = tosRepartFicheBlocActivation(qual);

		if (array.count() > 0) {
			result = array.objectAtIndex(0);
		}

		return result;
	}  
  
  
}
