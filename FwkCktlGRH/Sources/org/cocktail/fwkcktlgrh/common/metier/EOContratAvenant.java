/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.utilities.CocktailConstantes;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;


public class EOContratAvenant extends _EOContratAvenant {
	
	public static final String TEM_ANNULATION_NON = "N";
	public static final String TEM_ANNULATION_OUI = "O";
	
	public static final NSArray<EOSortOrdering> SORT_ARRAY_DATE_DEBUT_ASC = new NSArray<EOSortOrdering>(new EOSortOrdering(D_DEB_AVENANT_KEY, EOSortOrdering.CompareAscending));
	
	
	private static final long serialVersionUID = 8285029701099797097L;

	public EOContratAvenant() {
        super();
    }

    /**
     * Renvoie l'avenant courant pour un contrat
     * @param ec
     * @param contrat
     * @return Avenant courant
     */
    public static EOContratAvenant getAvenantCourantPourContrat(EOEditingContext ec, EOContrat contrat) {
    	if (contrat == null) {
    		return null;
    	}
    	EOQualifier qualifier = 
    			ERXQ.and(
    					ERXQ.lessThanOrEqualTo(EOContratAvenant.D_DEB_AVENANT_KEY, MyDateCtrl.getDateJour()),
    					ERXQ.or(
    							ERXQ.isNull(EOContratAvenant.D_FIN_AVENANT_KEY),
    							ERXQ.greaterThanOrEqualTo(EOContratAvenant.D_FIN_AVENANT_KEY, MyDateCtrl.getDateJour())
    							),
    					ERXQ.equals(TEM_ANNULATION_KEY, "N"),
    					ERXQ.equals(TO_CONTRAT_KEY, contrat)
    					);
    	
    	NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOContratAvenant.D_DEB_AVENANT_KEY, EOSortOrdering.CompareDescending));
    	
        return fetchFirstByQualifier(ec, qualifier, sortOrderings);
    }
    
    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }
    
    
    
    
	/**
	 * liste des elements de carriere d'un individu pour une periode donnee
	 * @param ec editing context
	 * @param individu l'individu
	 * @param dateDebut date de début préiode
	 * @param dateFin date de fin période
	 * @return les élements
	 */
	public static NSArray<EOContratAvenant> findSortedContratForIndividuAndDateInContext(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		String strQual = TO_CONTRAT_KEY + "." + EOContrat.TEM_ANNULATION_KEY + "='N' and ";
		NSArray args = null;
		if (dateFin != null) {
			strQual +=
					"toContrat.toIndividu = %@ AND (" +
							"("+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " = nil) OR" +
							"("+ EOContratAvenant.D_DEB_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " <= %@) OR" +
							"("+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@) OR" +
							"("+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " <= %@) OR" +
							"("+ EOContratAvenant.D_DEB_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@) OR" +
							"("+ EOContratAvenant.D_DEB_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " = nil)" +
							")";
			args = new NSArray(new Object[] {
						individu,
						dateDebut,
						dateDebut, dateFin,
						dateDebut, dateFin,
						dateDebut, dateDebut, dateFin,
						dateDebut, dateFin, dateFin,
						dateDebut, dateFin });
		} else {
			strQual +=
						"toContrat.toIndividu = %@ AND (" +
								"("+ EOContratAvenant.D_FIN_AVENANT_KEY + " = nil) OR" +
								"("+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@) OR" +
								"("+ EOContratAvenant.D_DEB_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@)" +
								")";
			args = new NSArray(new Object[] { individu, dateDebut, dateDebut, dateDebut, dateDebut });
		}
		NSArray sortOrderings = new NSArray(EOSortOrdering.sortOrderingWithKey(EOContratAvenant.D_FIN_AVENANT_KEY, EOSortOrdering.CompareAscending));
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(strQual, args);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOContratAvenant.ENTITY_NAME, qual, sortOrderings);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}
    
	/** retourne tous les details de contrat non annules, de remuneration principale d'un individu commencant avant la date
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @return details trouves
	 */
	public static NSArray rechercherAvenantsRemunerationPrincipaleAnterieursADate(EOEditingContext edc,EOIndividu individu,NSTimestamp date) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_ANNULATION_KEY + "=%@", new NSArray(CocktailConstantes.FAUX)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CONTRAT_KEY+"."+EOContrat.TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_DEB_AVENANT_KEY + "<=%@", new NSArray(date)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CONTRAT_KEY + "." + EOContrat.TO_TYPE_CONTRAT_TRAVAIL_KEY + "."+ EOTypeContratTravail.TEM_INVITE_ASSOCIE_KEY + "=%@", new NSArray(CocktailConstantes.FAUX)));
		
		return fetchAll(edc, new EOAndQualifier(qualifiers), null);

	}
	public boolean estAnnule() {
		return temAnnulation() != null && temAnnulation().equals(CocktailConstantes.VRAI);
	}
    

	public static NSArray<EOContratAvenant> findForIndividuAndPeriode(EOEditingContext editingContext,Integer noIndividu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
	
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		
		qualifiers.addObject(ERXQ.and(ERXQ.equals(TO_CONTRAT.dot(EOContrat.TO_INDIVIDU).dot(EOIndividu.NO_INDIVIDU_KEY).key(), noIndividu), 
				ERXQ.equals(TO_CONTRAT.dot(EOContrat.TEM_ANNULATION_KEY).key(), EOContrat.TEM_ANNULATION_NON),
				GRHUtilities.qualifierPourPeriode(D_DEB_AVENANT_KEY,debutPeriode , D_FIN_AVENANT_KEY ,finPeriode)));
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CONTRAT_KEY + "."+ EOContrat.TO_INDIVIDU_KEY + "." + EOIndividu.NO_INDIVIDU_KEY + " = %@", new NSArray(noIndividu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CONTRAT_KEY + "."+ EOContrat.TEM_ANNULATION_KEY + " = %@", new NSArray(Constantes.FAUX)));
		qualifiers.addObject(GRHUtilities.qualifierPourPeriode(D_DEB_AVENANT_KEY,debutPeriode , D_FIN_AVENANT_KEY ,finPeriode));

		return fetchAll(editingContext, ERXQ.and(qualifiers), SORT_ARRAY_DATE_DEBUT_ASC);		
		
	}
	
}
