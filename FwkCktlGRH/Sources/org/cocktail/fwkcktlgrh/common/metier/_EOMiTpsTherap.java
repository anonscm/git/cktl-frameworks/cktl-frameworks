/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOMiTpsTherap.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOMiTpsTherap extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_MiTpsTherap";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_COM_MED_MTTH_KEY = "dComMedMtth";
	public static final String D_COM_MED_SUP_MTTH_KEY = "dComMedSupMtth";
	public static final String D_COM_REFORME_KEY = "dComReforme";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String MTT_QUOTITE_KEY = "mttQuotite";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
	public static final ERXKey<NSTimestamp> D_ANNULATION = new ERXKey<NSTimestamp>("dAnnulation");
	public static final ERXKey<NSTimestamp> DATE_ARRETE = new ERXKey<NSTimestamp>("dateArrete");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_COM_MED_MTTH = new ERXKey<NSTimestamp>("dComMedMtth");
	public static final ERXKey<NSTimestamp> D_COM_MED_SUP_MTTH = new ERXKey<NSTimestamp>("dComMedSupMtth");
	public static final ERXKey<NSTimestamp> D_COM_REFORME = new ERXKey<NSTimestamp>("dComReforme");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> MTT_QUOTITE = new ERXKey<Integer>("mttQuotite");
	public static final ERXKey<String> NO_ARRETE = new ERXKey<String>("noArrete");
	public static final ERXKey<String> NO_ARRETE_ANNULATION = new ERXKey<String>("noArreteAnnulation");
	public static final ERXKey<String> TEM_CONFIRME = new ERXKey<String>("temConfirme");
	public static final ERXKey<String> TEM_GEST_ETAB = new ERXKey<String>("temGestEtab");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_ABSENCE_KEY = "toAbsence";
	public static final String TO_INDIVIDU_KEY = "toIndividu";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAbsence> TO_ABSENCE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAbsence>("toAbsence");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");

  private static Logger LOG = Logger.getLogger(_EOMiTpsTherap.class);

  public EOMiTpsTherap localInstanceIn(EOEditingContext editingContext) {
    EOMiTpsTherap localInstance = (EOMiTpsTherap)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dComMedMtth() {
    return (NSTimestamp) storedValueForKey("dComMedMtth");
  }

  public void setDComMedMtth(NSTimestamp value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating dComMedMtth from " + dComMedMtth() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedMtth");
  }

  public NSTimestamp dComMedSupMtth() {
    return (NSTimestamp) storedValueForKey("dComMedSupMtth");
  }

  public void setDComMedSupMtth(NSTimestamp value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating dComMedSupMtth from " + dComMedSupMtth() + " to " + value);
    }
    takeStoredValueForKey(value, "dComMedSupMtth");
  }

  public NSTimestamp dComReforme() {
    return (NSTimestamp) storedValueForKey("dComReforme");
  }

  public void setDComReforme(NSTimestamp value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating dComReforme from " + dComReforme() + " to " + value);
    }
    takeStoredValueForKey(value, "dComReforme");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer mttQuotite() {
    return (Integer) storedValueForKey("mttQuotite");
  }

  public void setMttQuotite(Integer value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating mttQuotite from " + mttQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "mttQuotite");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
    	_EOMiTpsTherap.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOAbsence toAbsence() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOAbsence)storedValueForKey("toAbsence");
  }

  public void setToAbsenceRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAbsence value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
      _EOMiTpsTherap.LOG.debug("updating toAbsence from " + toAbsence() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOAbsence oldValue = toAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAbsence");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toAbsence");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOMiTpsTherap.LOG.isDebugEnabled()) {
      _EOMiTpsTherap.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  

  public static EOMiTpsTherap create(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer mttQuotite
, String temConfirme
, String temGestEtab
, String temValide
) {
    EOMiTpsTherap eo = (EOMiTpsTherap) EOUtilities.createAndInsertInstance(editingContext, _EOMiTpsTherap.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setMttQuotite(mttQuotite);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOMiTpsTherap> fetchAll(EOEditingContext editingContext) {
    return _EOMiTpsTherap.fetchAll(editingContext, null);
  }

  public static NSArray<EOMiTpsTherap> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMiTpsTherap.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOMiTpsTherap> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOMiTpsTherap.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMiTpsTherap> eoObjects = (NSArray<EOMiTpsTherap>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOMiTpsTherap fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMiTpsTherap.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMiTpsTherap fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMiTpsTherap> eoObjects = _EOMiTpsTherap.fetch(editingContext, qualifier, null);
    EOMiTpsTherap eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOMiTpsTherap)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_MiTpsTherap that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMiTpsTherap fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMiTpsTherap.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOMiTpsTherap fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMiTpsTherap eoObject = _EOMiTpsTherap.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_MiTpsTherap that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOMiTpsTherap fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMiTpsTherap fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMiTpsTherap eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMiTpsTherap)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOMiTpsTherap localInstanceIn(EOEditingContext editingContext, EOMiTpsTherap eo) {
    EOMiTpsTherap localInstance = (eo == null) ? null : (EOMiTpsTherap)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
