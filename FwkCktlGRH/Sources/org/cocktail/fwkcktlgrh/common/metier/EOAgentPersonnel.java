/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import java.util.Enumeration;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IAgentPersonnel;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

public class EOAgentPersonnel extends _EOAgentPersonnel implements IAgentPersonnel {
    private static Logger log = Logger.getLogger(EOAgentPersonnel.class);
    public static final String TYPE_POPULATION_ENSEIGNANT = "E";
    public static final String TYPE_POPULATION_NON_ENSEIGNANT = "N";
    public static final String TYPE_POPULATION_VACATAIRE = "V";
    public static final String TYPE_POPULATION_HEBERGE = "H";

    public String agtLogin() {
        return toCompte().cptLogin();
    }
    public boolean gereEnseignants() {
        return agtTypePopulation() != null && agtTypePopulation().indexOf(TYPE_POPULATION_ENSEIGNANT) >= 0;
    }
    public void setGereEnseignants(boolean aBool) {
        modifierTypePopulation(TYPE_POPULATION_ENSEIGNANT, aBool);
    }
    public boolean gereNonEnseignants() {
        return agtTypePopulation() != null && agtTypePopulation().indexOf(TYPE_POPULATION_NON_ENSEIGNANT) >= 0;
    }
    public void setGereNonEnseignants(boolean aBool) {
        modifierTypePopulation(TYPE_POPULATION_NON_ENSEIGNANT, aBool);
    }
    public boolean gereVacataires() {
        return agtTypePopulation() != null && agtTypePopulation().indexOf(TYPE_POPULATION_VACATAIRE) >= 0;
    }
    public void setGereVacataires(boolean aBool) {
        modifierTypePopulation(TYPE_POPULATION_VACATAIRE, aBool);
    }
    public boolean gereHeberges() {
        return agtTypePopulation() != null && agtTypePopulation().indexOf(TYPE_POPULATION_HEBERGE) >= 0;
    }
    public void setGereHeberges(boolean aBool) {
        modifierTypePopulation(TYPE_POPULATION_HEBERGE, aBool);
    }
    public String agtGereHeberge() {
        if (gereHeberges()) {
            return Constantes.VRAI;
        } else {
            return Constantes.FAUX;
        }
    }
    public boolean gereTousAgents() {
        return gereNonEnseignants() && gereEnseignants() && gereVacataires() && gereHeberges();
    }
    public boolean gereTouteStructure() {
        return agtTout() != null && agtTout().equals(Constantes.VRAI);
    }
    public void setGereTouteStructure(boolean aBool) {
        if (aBool) {
            setAgtTout(Constantes.VRAI);
        } else {
            setAgtTout(Constantes.FAUX);
        }
    }
    /** Retourne la liste des services g&eacute;r&eacute;s par l'utilisateur avec les structures filles */
    public NSArray structuresGereesEtFilles() {
        NSMutableArray structuresCompletes = new NSMutableArray();
        java.util.Enumeration e = structuresGerees().objectEnumerator();
        while (e.hasMoreElements()) {
            EOStructure structure = (EOStructure) e.nextElement();
            if (structuresCompletes.containsObject(structure) == false) {
                structuresCompletes.addObject(structure);
                NSArray filles = structure.structuresFils();
                java.util.Enumeration e1 = filles.objectEnumerator();
                while (e1.hasMoreElements()) {
                    EOStructure structure1 = (EOStructure) e1.nextElement();
                    if (structuresCompletes.containsObject(structure1) == false) {
                        structuresCompletes.addObject(structure1);
                    }
                }
            }
        }
        return new NSArray(structuresCompletes);
    }
    /** Retourne la liste des services g&eacute;r&eacute;s par l'utilisateur */
    public NSArray structuresGerees() {
        return filtrerStructures(true);
    }
    /** Retourne la liste des groupes g&eacute;r&eacute;s par l'utilisateur */
    public NSArray groupesGeres() {
        return filtrerStructures(false);
    }
    public boolean peutConsulterDossier() {
        return agtConsDossier() != null && agtConsDossier().equals(Constantes.VRAI);
    }
    public void setPeutConsulterDossier(boolean aBool) {
        if (aBool) {
            setAgtConsDossier(Constantes.VRAI);
        } else {
            setAgtConsDossier(Constantes.FAUX);
        }
    }

    public boolean peutAfficherEmplois() {
        return agtConsEmplois() != null && agtConsEmplois().equals(Constantes.VRAI);
    }
    public void setPeutAfficherEmplois(boolean aBool) {
        if (aBool) {
            setAgtConsEmplois(Constantes.VRAI);
        } else {
            setAgtConsEmplois(Constantes.FAUX);
        }
    }
    public boolean peutGererEmplois() {
        return agtUpdEmplois() != null && agtUpdEmplois().equals(Constantes.VRAI);
    }
    public void setPeutGererEmplois(boolean aBool) {
        if (aBool) {
            setAgtUpdEmplois(Constantes.VRAI);
        } else {
            setAgtUpdEmplois(Constantes.FAUX);
        }
    }
    public boolean peutAfficherPostes() {
        return agtConsPostes() != null && agtConsPostes().equals(Constantes.VRAI);
    }
    public void setPeutAfficherPostes(boolean aBool) {
        if (aBool) {
            setAgtConsPostes(Constantes.VRAI);
        } else {
            setAgtConsPostes(Constantes.FAUX);
        }
    }
    public boolean peutGererPostes() {
        return agtUpdPostes() != null && agtUpdPostes().equals(Constantes.VRAI);
    }
    public void setPeutGererPostes(boolean aBool) {
        if (aBool) {
            setAgtUpdPostes(Constantes.VRAI);
        } else {
            setAgtUpdPostes(Constantes.FAUX);
        }
    }
    public boolean peutAfficherIndividu() {
        return agtConsIdentite() != null && agtConsIdentite().equals(Constantes.VRAI);
    }
    public void setPeutAfficherIndividu(boolean aBool) {
        if (aBool) {
            setAgtConsIdentite(Constantes.VRAI);
        } else {
            setAgtConsIdentite(Constantes.FAUX);
        }
    }
    public boolean peutGererIndividu() {
        return agtUpdIdentite() != null && agtUpdIdentite().equals(Constantes.VRAI);
    }
    public void setPeutGererIndividu(boolean aBool) {
        if (aBool) {
            setAgtUpdIdentite(Constantes.VRAI);
        } else {
            setAgtUpdIdentite(Constantes.FAUX);
        }
    }
    public boolean peutAfficherInfosPerso() {
        return agtInfosPerso() != null && agtInfosPerso().equals(Constantes.VRAI);
    }
    public void setPeutAfficherInfosPerso(boolean aBool) {
        if (aBool) {
            setAgtInfosPerso(Constantes.VRAI);
        } else {
            setAgtInfosPerso(Constantes.FAUX);
        }
    }
    public boolean peutAfficherContrats() {
        return agtConsContrat() != null && agtConsContrat().equals(Constantes.VRAI);
    }
    public void setPeutAfficherContrats(boolean aBool) {
        if (aBool) {
            setAgtConsContrat(Constantes.VRAI);
        } else {
            setAgtConsContrat(Constantes.FAUX);
        }
    }
    public boolean peutGererContrats() {
        return agtUpdContrat() != null && agtUpdContrat().equals(Constantes.VRAI);
    }
    public void setPeutGererContrats(boolean aBool) {
        if (aBool) {
            setAgtUpdContrat(Constantes.VRAI);
        } else {
            setAgtUpdContrat(Constantes.FAUX);
        }
    }
    public boolean peutAfficherCarrieres() {
        return agtConsCarriere() != null && agtConsCarriere().equals(Constantes.VRAI);
    }
    public void setPeutAfficherCarrieres(boolean aBool) {
        if (aBool) {
            setAgtConsCarriere(Constantes.VRAI);
        } else {
            setAgtConsCarriere(Constantes.FAUX);
        }
    }
    public boolean peutGererCarrieres() {
        return agtUpdCarriere() != null && agtUpdCarriere().equals(Constantes.VRAI);
    }
    public void setPeutGererCarrieres(boolean aBool) {
        if (aBool) {
            setAgtUpdCarriere(Constantes.VRAI);
        } else {
            setAgtUpdCarriere(Constantes.FAUX);
        }
    }
    public boolean peutAfficherOccupation() {
        return agtConsOccupation() != null && agtConsOccupation().equals(Constantes.VRAI);
    }
    public void setPeutAfficherOccupation(boolean aBool) {
        if (aBool) {
            setAgtConsOccupation(Constantes.VRAI);
        } else {
            setAgtConsOccupation(Constantes.FAUX);
        }
    }
    public boolean peutGererOccupation() {
        return agtUpdOccupation() != null && agtUpdOccupation().equals(Constantes.VRAI);
    }
    public void setPeutGererOccupation(boolean aBool) {
        if (aBool) {
            setAgtUpdOccupation(Constantes.VRAI);
        } else {
            setAgtUpdOccupation(Constantes.FAUX);
        }
    }
    public boolean peutAfficherConges() {
        return agtConsConges() != null && agtConsConges().equals(Constantes.VRAI);
    }
    public void setPeutAfficherConges(boolean aBool) {
        if (aBool) {
            setAgtConsConges(Constantes.VRAI);
        } else {
            setAgtConsConges(Constantes.FAUX);
        }
    }  
    public boolean peutGererConges() {
        return agtUpdConges() != null && agtUpdConges().equals(Constantes.VRAI);
    }
    public void setPeutGererConges(boolean aBool) {
        if (aBool) {
            setAgtUpdConges(Constantes.VRAI);
        } else {
            setAgtUpdConges(Constantes.FAUX);
        }
    }
    public boolean peutUtiliserOutils() {
        return agtOutils() != null && agtOutils().equals(Constantes.VRAI);
    }
    public void setPeutUtiliserOutils(boolean aBool) {
        if (aBool) {
            setAgtOutils(Constantes.VRAI);
        } else {
            setAgtOutils(Constantes.FAUX);
        }
    }
    public boolean peutAdministrer() {
        return agtAdministration() != null && agtAdministration().equals(Constantes.VRAI);
    }
    public void setPeutAdministrer(boolean aBool) {
        if (aBool) {
            setAgtAdministration(Constantes.VRAI);
        } else {
            setAgtAdministration(Constantes.FAUX);
        }
    }
    public boolean peutUtiliserRequetes() {
        return agtEditionRequetes() != null && agtEditionRequetes().equals(Constantes.VRAI);
    }
    public void setPeutUtiliserRequetes(boolean aBool) {
        if (aBool) {
            setAgtEditionRequetes(Constantes.VRAI);
        } else {
            setAgtEditionRequetes(Constantes.FAUX);
        }
    }
    public boolean peutUtiliserEditions() {
        return agtEditionFos() != null && agtEditionFos().equals(Constantes.VRAI);
    }
    public void setPeutUtiliserEditions(boolean aBool) {
        if (aBool) {
            setAgtEditionFos(Constantes.VRAI);
        } else {
            setAgtEditionFos(Constantes.FAUX);
        }
    }
    public boolean peutAfficherAgents() {
        return agtConsAgents() != null && agtConsAgents().equals(Constantes.VRAI);
    }
    public void setPeutAfficherAgents(boolean aBool) {
        if (aBool) {
            setAgtConsAgents(Constantes.VRAI);
        } else {
            setAgtConsAgents(Constantes.FAUX);
        }
    }
    public boolean peutGererAgents() {
        return agtUpdAgents() != null && agtUpdAgents().equals(Constantes.VRAI);
    }
    public void setPeutGererAgents(boolean aBool) {
        if (aBool) {
            setAgtUpdAgents(Constantes.VRAI);
        } else {
            setAgtUpdAgents(Constantes.FAUX);
        }
    }
    public boolean peutAfficherAccidentTravail() {
        return agtConsAccident() != null && agtConsAccident().equals(Constantes.VRAI);
    }
    public void setPeutAfficherAccidentTravail(boolean aBool) {
        if (aBool) {
            setAgtConsAccident(Constantes.VRAI);
        } else {
            setAgtConsAccident(Constantes.FAUX);
        }
    }
    public boolean peutGererAccidentTravail() {
        return agtUpdAccident() != null && agtUpdAccident().equals(Constantes.VRAI);
    }
    public void setPeutGererAccidentTravail(boolean aBool) {
        if (aBool) {
            setAgtUpdAccident(Constantes.VRAI);
        } else {
            setAgtUpdAccident(Constantes.FAUX);
        }
    }
    public boolean peutCreerListesElectorales() {
        return agtElection() != null && agtElection().equals(Constantes.VRAI);
    }
    public void setPeutCreerListesElectorales(boolean aBool) {
        if (aBool) {
            setAgtElection(Constantes.VRAI);
        } else {
            setAgtElection(Constantes.FAUX);
        }
    }
    public boolean peutGererPromouvabilites() {
        return agtPromouvable() != null && agtPromouvable().equals(Constantes.VRAI);
    }
    public void setPeutGererPromouvabilites(boolean aBool) {
        if (aBool) {
            setAgtPromouvable(Constantes.VRAI);
        } else {
            setAgtPromouvable(Constantes.FAUX);
        }
    }
    public boolean peutGererPrimes() {
        return agtPrime() != null && agtPrime().equals(Constantes.VRAI);
    }
    public void setPeutGererPrimes(boolean aBool) {
        if (aBool) {
            setAgtPrime(Constantes.VRAI);
        } else {
            setAgtPrime(Constantes.FAUX);
        }
    }
    public boolean gerePlusieursCategoriesPersonnel() {
        return agtTypePopulation() != null && agtTypePopulation().length() > 1;
    }
    public void init() {
        setAgtConsAgents(Constantes.FAUX);
        setAgtConsCarriere(Constantes.FAUX);
        setAgtConsConges(Constantes.FAUX);
        setAgtConsContrat(Constantes.FAUX);
        setAgtConsDossier(Constantes.FAUX);
        setAgtConsEmplois(Constantes.FAUX);
        setAgtConsPostes(Constantes.FAUX);
        setAgtConsIdentite(Constantes.FAUX);
        setAgtConsOccupation(Constantes.FAUX);
        setAgtEditionFos(Constantes.FAUX);
        setAgtEditionRequetes(Constantes.FAUX);
        setAgtInfosPerso(Constantes.FAUX);
        setAgtOutils(Constantes.FAUX);
        setAgtTout(Constantes.FAUX);
        setAgtUpdAgents(Constantes.FAUX);
        setAgtUpdCarriere(Constantes.FAUX);
        setAgtUpdConges(Constantes.FAUX);
        setAgtUpdContrat(Constantes.FAUX);
        setAgtUpdEmplois(Constantes.FAUX);
        setAgtUpdPostes(Constantes.FAUX);
        setAgtUpdIdentite(Constantes.FAUX);
        setAgtUpdAccident(Constantes.FAUX);
        setAgtUpdOccupation(Constantes.FAUX);
        setAgtElection(Constantes.FAUX);
        setAgtAdministration(Constantes.FAUX);
        setAgtPromouvable(Constantes.FAUX);
        setAgtPrime(Constantes.FAUX);
    }
    public void initPourPersonnel() {
        init();
        setAgtConsCarriere(Constantes.VRAI);
        setAgtConsConges(Constantes.VRAI);
        setAgtConsContrat(Constantes.VRAI);
        setAgtConsDossier(Constantes.VRAI);
        setAgtConsOccupation(Constantes.VRAI);
        setAgtInfosPerso(Constantes.VRAI);
        setAgtConsAccident(Constantes.VRAI);
        setAgtTypePopulation(TYPE_POPULATION_ENSEIGNANT + TYPE_POPULATION_NON_ENSEIGNANT);
    }
    public void transformerEnPersonnel() {
        init();
        supprimerRelations();   // Supprime les relations sur les services
        initPourPersonnel();
    }
    public void supprimerRelations() {
        NSArray droitsServices = new NSArray(toAgentsDroitsServices());
        Enumeration e = droitsServices.objectEnumerator();
        while (e.hasMoreElements()) {
            EOAgentsDroitsServices droitService = (EOAgentsDroitsServices) e.nextElement();
            removeObjectFromBothSidesOfRelationshipWithKey(droitService, "toAgentsDroitsServices");
            editingContext().deleteObject(droitService);
        }
    }
    // mÃ©thodes privÃ©es
    private void modifierTypePopulation(String uneStr, boolean aBool) {
        if (aBool) {
            if (agtTypePopulation() == null) {
                setAgtTypePopulation(uneStr);
            } else if (agtTypePopulation().indexOf(uneStr) < 0) {
                // type de population non encore pris en compte
                setAgtTypePopulation(agtTypePopulation() + uneStr);
            }
        } else {
            if (agtTypePopulation() != null && agtTypePopulation().indexOf(uneStr) >= 0) {
                // supprimer du type de population
                setAgtTypePopulation(agtTypePopulation().replaceAll(uneStr, ""));
            }
        }
    }
    private NSArray filtrerStructures(boolean estService) {
        if (toAgentsDroitsServices() == null) {
            return null;
        }
        NSMutableArray<EOStructure> structures = new NSMutableArray();
        for (java.util.Enumeration<EOAgentsDroitsServices> e = toAgentsDroitsServices().objectEnumerator(); e.hasMoreElements();) {
            EOAgentsDroitsServices droit = (EOAgentsDroitsServices) e.nextElement();
            if (estService) {
                if (droit.toStructure().isService()) {
                    structures.addObject(droit.toStructure());
                }
            } else {
                if (!droit.toStructure().isService()) {
                    structures.addObject(droit.toStructure());
                }
            }
        }
        return structures;
    }
    // methodes statiques
    /** retourne l'agent ayant le login fourni en parametre
     * @param editingContext
     * @param login
     * @return null si pas d'agent trouve
     */
    public static EOAgentPersonnel rechercherAgentAvecLogin(EOEditingContext editingContext,String login) {
        EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(TO_COMPTE_KEY
        							+ "." + EOCompte.CPT_LOGIN_KEY + " = '" + login.toLowerCase() + "' or toCompte.cptLogin = '" + login.toUpperCase() + "'", null);
        EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);
        myFetch.setPrefetchingRelationshipKeyPaths(new NSArray("toIndividu"));
        NSArray results = editingContext.objectsWithFetchSpecification(myFetch);
        try {
            return (EOAgentPersonnel) results.objectAtIndex(0);
        } catch (Exception e) {
            return null;
        }
    }
    
    public static EOAgentPersonnel rechercherAgentAvecIndividu(EOEditingContext ec, EOIndividu individu) {
        EOAgentPersonnel agent = null;
        
        if (individu != null) {
            agent = fetchFirstByQualifier(ec, ERXQ.is(TO_INDIVIDU_KEY, individu));
        }
        return agent;
    }
    
    public static EOAgentPersonnel rechercherAgentAvecPersId(EOEditingContext ec, Integer persId) {
        EOAgentPersonnel agent = null;
        if (persId != null) {
            agent = fetchFirstByQualifier(ec, ERXQ.is(ERXQ.keyPath(TO_INDIVIDU_KEY, EOIndividu.PERS_ID_KEY), persId));
        }
        return agent;
    }
    
    public static NSArray rechercherGestionnairesMangue(EOEditingContext editingContext) {
        return rechercherAgents(editingContext, AGT_CONS_DOSSIER_KEY, Constantes.FAUX);
    }
    public static NSArray rechercherPersonnels(EOEditingContext editingContext) {
        return rechercherAgents(editingContext, AGT_CONS_DOSSIER_KEY, Constantes.VRAI);
    }
    // Methodes privees
    // Retourne les agents dont l'attribut passe en parametre a la valeur passee en parametre
    private static NSArray rechercherAgents(EOEditingContext editingContext, String nomAttribut, String valeurAttribut) {
        if (nomAttribut == null || valeurAttribut == null) {
            return new NSArray();
        }
        
        EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(nomAttribut + " = '" + valeurAttribut + "'", null);
        
        EOFetchSpecification myFetch = new EOFetchSpecification ("AgentPersonnel", myQualifier,null);
        myFetch.setPrefetchingRelationshipKeyPaths(new NSArray("toIndividu"));
        myFetch.setRefreshesRefetchedObjects(true);
        return editingContext.objectsWithFetchSpecification(myFetch);
    }


}
