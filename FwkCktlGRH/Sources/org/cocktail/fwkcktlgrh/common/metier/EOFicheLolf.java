/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IFicheLolf;
import org.cocktail.fwkcktlgrh.common.metier.services.FinderTypeActionFeveService;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOFicheLolf extends _EOFicheLolf implements IFicheLolf {
  private static Logger log = Logger.getLogger(EOFicheLolf.class);
  
  public static final String ENTITY_TABLE_NAME = "MANGUE.FICHE_LOLF";

  public EOFicheLolf() {
      super();
 	  typeFiche = FICHE_LOLF;
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }

//methodes rajoutees

	private Boolean voirDetail = new Boolean(false);

	public boolean voirDetail() {
		return voirDetail.booleanValue();
	}

	public void setVoirDetail(boolean value) {
		voirDetail = new Boolean(value);
	}

	/**
   *
   */
	public boolean hasWarning() {
		boolean hasWarning = false;

		// calcul des sommes totales des fonctions silland
		Double totalSilland = new Double(((Number) tosRepartFloSilland().valueForKey("@sum." + EORepartFloSilland.RFS_QUOTITE_KEY)).doubleValue());
		if (totalSilland.compareTo(new Double(100)) != 0) {
			hasWarning = true;
		}

		if (!hasWarning) {
			for (int i = 0; i < tosRepartFloSilland().count(); i++) {
				EORepartFloSilland uneRepart = (EORepartFloSilland) tosRepartFloSilland().objectAtIndex(i);
				Double totalLolfNomenclature = new Double(((Number) uneRepart.tosRepartFloLolfNomen().valueForKey("@sum." + EORepartFloLolfNomen.RRF_QUOTITE_KEY)).doubleValue());
				if (totalLolfNomenclature.compareTo(new Double(100)) != 0) {
					hasWarning = true;
				}
			}
		}

		return hasWarning;
	}

	/**
   * 
   */
	public String htmlWarnMessage() {

		String message = "";

		// calcul des sommes totales des fonctions silland
		Double totalSilland = new Double(((Number) tosRepartFloSilland().valueForKey("@sum." + EORepartFloSilland.RFS_QUOTITE_KEY)).doubleValue());
		if (totalSilland.compareTo(new Double(100)) != 0) {
			message += "La somme des pourcentages de toutes les fonctions SILLAND n'est pas &eacute;gale &agrave; 100% (ici : <b><font color='red'>" + totalSilland + "</font></b>)<BR><BR>";
		}

		// calcul des sous sommes par rapport aux destinations jefycos
		for (int i = 0; i < tosRepartFloSilland().count(); i++) {
			EORepartFloSilland uneRepart = (EORepartFloSilland) tosRepartFloSilland().objectAtIndex(i);
			Double totalLolfNomenclature = new Double(((Number) uneRepart.tosRepartFloLolfNomen().valueForKey("@sum." + EORepartFloLolfNomen.RRF_QUOTITE_KEY)).doubleValue());
			if (totalLolfNomenclature.compareTo(new Double(100)) != 0) {
				message +=
						"Fonction SILLAND : '" + uneRepart.toFctSilland().display() + "' :<br>"
								+ "La somme des pourcentage de toutes les destinations LOLF n'est pas &eacute;gale &agrave; 100% (ici : <font color='red'>"
								+ totalLolfNomenclature + "</font>)"
								+ "<br><br>";
			}
		}

		return message;

	}

	/**
	 * Creer une fiche de poste a partir d'une fiche existante
	 * 
	 * @param fiche
	 *          : la fiche a dupliquer
	 * @param dDebut
	 * @param dFin
	 * @return
	 * @throws Exception
	 */
	public static EOFicheLolf newRecordInContextFrom(
				EOFicheLolf fiche, EOExercice exercice) throws Exception {
		EOEditingContext ec = fiche.editingContext();

		// creer le record FicheDePoste
		EOFicheLolf newRecord = newDefaultRecordInContext(ec);
		newRecord.addObjectToBothSidesOfRelationshipWithKey(fiche.toPoste(), "toPoste");

		// erreur si l'exercice est le meme
		if (exercice == fiche.toExercice()) {
			throw new Exception("Impossible de créer 2 fiches pour un meme poste sur un meme exercice !");
		}

		NSTimestamp dDebutExercice = DateCtrl.stringToDate("01/01/" + exercice.exeExercice().intValue());
		NSTimestamp dFinExercice = DateCtrl.stringToDate("31/12/" + exercice.exeExercice().intValue());
		newRecord.setFloDDebut(dDebutExercice);
		newRecord.setFloDFin(dFinExercice);

		newRecord.setToExerciceRelationship(exercice);

		//
		NSArray recsRepartFloSilland = fiche.tosRepartFloSilland();
		for (int i = 0; i < recsRepartFloSilland.count(); i++) {

			EORepartFloSilland recRepartFloSilland = (EORepartFloSilland) recsRepartFloSilland.objectAtIndex(i);

			// est-ce que la fonction silland est associée dans le nouvel exercice
			if (recRepartFloSilland.isDeclaree(recRepartFloSilland.toFctSilland(), exercice)) {

				// affectation a la fiche dans le nouvel exercice
				EORepartFloSilland newRecRepart = EORepartFloSilland.create(
							ec,
							DateCtrl.now(),
							DateCtrl.now(),
							1,
							recRepartFloSilland.rfsQuotite(),
							1,
							recRepartFloSilland.toFctSilland(),
							newRecord);

				NSArray recsRepartFloLolfNomen = recRepartFloSilland.tosRepartFloLolfNomen();

				int totalLolfNomenclature = 0;

				for (int j = 0; j < recsRepartFloLolfNomen.count(); j++) {
					EORepartFloLolfNomen recRepartFloLolfNomen = (EORepartFloLolfNomen) recsRepartFloLolfNomen.objectAtIndex(j);

					// la destination est affectee a cette fonction silland dans le nouvel
					// exercice ?
					// if (FinderTypeActionFeve.isDeclareeInExercice(ec,
					// recRepartFloLolfNomen.toTypeAction(), exercice,
					// recRepartFloSilland.toFctSilland())) {
					FinderTypeActionFeveService typeActionFeve = new FinderTypeActionFeveService();
					if (typeActionFeve.isDeclareeInExercice(ec, recRepartFloLolfNomen.toLolfNomenclatureDepense(), exercice, recRepartFloSilland.toFctSilland())) {

						EOLolfNomenclatureDepense typeActionInExercice = typeActionFeve.findTypeActionForExercice(
									// ec, recRepartFloLolfNomen.toTypeAction(), exercice);
								ec, recRepartFloLolfNomen.toLolfNomenclatureDepense(), exercice);

						/*
						 * EORepartFloLolfNomen.createRepartFloLolfNomen( ec,
						 * DateCtrl.now(), DateCtrl.now(),
						 * recRepartFloLolfNomen.rrfQuotite(), newRecRepart,
						 * typeActionInExercice);
						 */
						EORepartFloLolfNomen.create(
									ec, DateCtrl.now(), DateCtrl.now(), recRepartFloLolfNomen.rrfQuotite(), exercice, typeActionInExercice, newRecRepart);

						totalLolfNomenclature++;
					}
				}

				// on vire l'enregistrement s'il n'y a pas de destination
				if (totalLolfNomenclature == 0) {
					ec.deleteObject(newRecRepart);
				}
			}

		}
		return newRecord;
	}

	public static EOFicheLolf newRecordInContext(
				EOEditingContext ec,
				EOPoste poste,
				EOExercice exercice
			) {
		EOFicheLolf newRecord = newDefaultRecordInContext(ec);

		newRecord.setToPosteRelationship(poste);
		newRecord.setToExerciceRelationship(exercice);
		newRecord.setExeOrdre(exercice.exeExercice().intValue());
		NSTimestamp dDebutExercice = DateCtrl.stringToDate("01/01/" + exercice.exeExercice().intValue());
		NSTimestamp dFinExercice = DateCtrl.stringToDate("31/12/" + exercice.exeExercice().intValue());
		newRecord.setFloDDebut(dDebutExercice);
		newRecord.setFloDFin(dFinExercice);

		return newRecord;
	}

	private static EOFicheLolf newDefaultRecordInContext(EOEditingContext ec) {
		EOFicheLolf record = new EOFicheLolf();
		ec.insertObject(record);
		return record;
	}

}
