/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EODif.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EODif extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Dif";

	// Attributes
	public static final String ANNEE_CALCUL_KEY = "anneeCalcul";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DIF_CREDIT_KEY = "difCredit";
	public static final String DIF_DEBIT_KEY = "difDebit";
	public static final String D_MODIFICATION_KEY = "dModification";

	public static final ERXKey<Integer> ANNEE_CALCUL = new ERXKey<Integer>("anneeCalcul");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<Integer> DIF_CREDIT = new ERXKey<Integer>("difCredit");
	public static final ERXKey<Integer> DIF_DEBIT = new ERXKey<Integer>("difDebit");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	// Relationships
	public static final String DIF_DETAILS_KEY = "difDetails";
	public static final String INDIVIDU_KEY = "individu";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EODifDetail> DIF_DETAILS = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EODifDetail>("difDetails");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("individu");

  private static Logger LOG = Logger.getLogger(_EODif.class);

  public EODif localInstanceIn(EOEditingContext editingContext) {
    EODif localInstance = (EODif)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer anneeCalcul() {
    return (Integer) storedValueForKey("anneeCalcul");
  }

  public void setAnneeCalcul(Integer value) {
    if (_EODif.LOG.isDebugEnabled()) {
    	_EODif.LOG.debug( "updating anneeCalcul from " + anneeCalcul() + " to " + value);
    }
    takeStoredValueForKey(value, "anneeCalcul");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EODif.LOG.isDebugEnabled()) {
    	_EODif.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EODif.LOG.isDebugEnabled()) {
    	_EODif.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODif.LOG.isDebugEnabled()) {
    	_EODif.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public Integer difCredit() {
    return (Integer) storedValueForKey("difCredit");
  }

  public void setDifCredit(Integer value) {
    if (_EODif.LOG.isDebugEnabled()) {
    	_EODif.LOG.debug( "updating difCredit from " + difCredit() + " to " + value);
    }
    takeStoredValueForKey(value, "difCredit");
  }

  public Integer difDebit() {
    return (Integer) storedValueForKey("difDebit");
  }

  public void setDifDebit(Integer value) {
    if (_EODif.LOG.isDebugEnabled()) {
    	_EODif.LOG.debug( "updating difDebit from " + difDebit() + " to " + value);
    }
    takeStoredValueForKey(value, "difDebit");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODif.LOG.isDebugEnabled()) {
    	_EODif.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EODif.LOG.isDebugEnabled()) {
      _EODif.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EODifDetail> difDetails() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EODifDetail>)storedValueForKey("difDetails");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EODifDetail> difDetails(EOQualifier qualifier) {
    return difDetails(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EODifDetail> difDetails(EOQualifier qualifier, boolean fetch) {
    return difDetails(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EODifDetail> difDetails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EODifDetail> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EODifDetail.DIF_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EODifDetail.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = difDetails();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EODifDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EODifDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDifDetailsRelationship(org.cocktail.fwkcktlgrh.common.metier.EODifDetail object) {
    if (_EODif.LOG.isDebugEnabled()) {
      _EODif.LOG.debug("adding " + object + " to difDetails relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "difDetails");
  }

  public void removeFromDifDetailsRelationship(org.cocktail.fwkcktlgrh.common.metier.EODifDetail object) {
    if (_EODif.LOG.isDebugEnabled()) {
      _EODif.LOG.debug("removing " + object + " from difDetails relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "difDetails");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EODifDetail createDifDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_DifDetail");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "difDetails");
    return (org.cocktail.fwkcktlgrh.common.metier.EODifDetail) eo;
  }

  public void deleteDifDetailsRelationship(org.cocktail.fwkcktlgrh.common.metier.EODifDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "difDetails");
    editingContext().deleteObject(object);
  }

  public void deleteAllDifDetailsRelationships() {
    Enumeration objects = difDetails().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDifDetailsRelationship((org.cocktail.fwkcktlgrh.common.metier.EODifDetail)objects.nextElement());
    }
  }


  public static EODif create(EOEditingContext editingContext, Integer anneeCalcul
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu) {
    EODif eo = (EODif) EOUtilities.createAndInsertInstance(editingContext, _EODif.ENTITY_NAME);    
		eo.setAnneeCalcul(anneeCalcul);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EODif> fetchAll(EOEditingContext editingContext) {
    return _EODif.fetchAll(editingContext, null);
  }

  public static NSArray<EODif> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODif.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EODif> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODif.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODif> eoObjects = (NSArray<EODif>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODif fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EODif.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODif fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODif> eoObjects = _EODif.fetch(editingContext, qualifier, null);
    EODif eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODif)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Dif that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODif fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EODif.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EODif fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EODif eoObject = _EODif.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Dif that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EODif fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODif fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODif eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODif)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EODif localInstanceIn(EOEditingContext editingContext, EODif eo) {
    EODif localInstance = (eo == null) ? null : (EODif)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
