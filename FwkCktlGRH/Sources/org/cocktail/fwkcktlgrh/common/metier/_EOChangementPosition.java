/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOChangementPosition.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOChangementPosition extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_ChangementPosition";

	// Attributes
	public static final String C_POSITION_KEY = "cPosition";
	public static final String D_ARRETE_POSITION_KEY = "dArretePosition";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_POSITION_KEY = "dDebPosition";
	public static final String D_FIN_POSITION_KEY = "dFinPosition";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIEU_POSITION_KEY = "lieuPosition";
	public static final String LIEU_POSITION_ORIG_KEY = "lieuPositionOrig";
	public static final String NO_ARRETE_POSITION_KEY = "noArretePosition";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String QUOTITE_POSITION_KEY = "quotitePosition";
	public static final String TEMOIN_POSITION_PREV_KEY = "temoinPositionPrev";
	public static final String TEM_PC_ACQUITEE_KEY = "temPcAcquitee";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String TXT_POSITION_KEY = "txtPosition";
	public static final String TXT_POSITION_ORIG_KEY = "txtPositionOrig";

	public static final ERXKey<String> C_POSITION = new ERXKey<String>("cPosition");
	public static final ERXKey<NSTimestamp> D_ARRETE_POSITION = new ERXKey<NSTimestamp>("dArretePosition");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_POSITION = new ERXKey<NSTimestamp>("dDebPosition");
	public static final ERXKey<NSTimestamp> D_FIN_POSITION = new ERXKey<NSTimestamp>("dFinPosition");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> LIEU_POSITION = new ERXKey<String>("lieuPosition");
	public static final ERXKey<String> LIEU_POSITION_ORIG = new ERXKey<String>("lieuPositionOrig");
	public static final ERXKey<String> NO_ARRETE_POSITION = new ERXKey<String>("noArretePosition");
	public static final ERXKey<Integer> NO_DOSSIER_PERS = new ERXKey<Integer>("noDossierPers");
	public static final ERXKey<Integer> QUOTITE_POSITION = new ERXKey<Integer>("quotitePosition");
	public static final ERXKey<String> TEMOIN_POSITION_PREV = new ERXKey<String>("temoinPositionPrev");
	public static final ERXKey<String> TEM_PC_ACQUITEE = new ERXKey<String>("temPcAcquitee");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	public static final ERXKey<String> TXT_POSITION = new ERXKey<String>("txtPosition");
	public static final ERXKey<String> TXT_POSITION_ORIG = new ERXKey<String>("txtPositionOrig");
	// Relationships
	public static final String TO_ABSENCE_KEY = "toAbsence";
	public static final String TO_CARRIERE_KEY = "toCarriere";
	public static final String TO_ENFANT_KEY = "toEnfant";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_MOTIF_POSITION_KEY = "toMotifPosition";
	public static final String TO_PAYS_KEY = "toPays";
	public static final String TO_POSITION_KEY = "toPosition";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_RNE_ORIGINE_KEY = "toRneOrigine";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAbsence> TO_ABSENCE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAbsence>("toAbsence");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOCarriere> TO_CARRIERE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOCarriere>("toCarriere");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant> TO_ENFANT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant>("toEnfant");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> TO_MOTIF_POSITION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition>("toMotifPosition");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_PAYS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toPays");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPosition> TO_POSITION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPosition>("toPosition");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRne");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE_ORIGINE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRneOrigine");

  private static Logger LOG = Logger.getLogger(_EOChangementPosition.class);

  public EOChangementPosition localInstanceIn(EOEditingContext editingContext) {
    EOChangementPosition localInstance = (EOChangementPosition)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cPosition() {
    return (String) storedValueForKey("cPosition");
  }

  public void setCPosition(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating cPosition from " + cPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "cPosition");
  }

  public NSTimestamp dArretePosition() {
    return (NSTimestamp) storedValueForKey("dArretePosition");
  }

  public void setDArretePosition(NSTimestamp value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating dArretePosition from " + dArretePosition() + " to " + value);
    }
    takeStoredValueForKey(value, "dArretePosition");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebPosition() {
    return (NSTimestamp) storedValueForKey("dDebPosition");
  }

  public void setDDebPosition(NSTimestamp value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating dDebPosition from " + dDebPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebPosition");
  }

  public NSTimestamp dFinPosition() {
    return (NSTimestamp) storedValueForKey("dFinPosition");
  }

  public void setDFinPosition(NSTimestamp value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating dFinPosition from " + dFinPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinPosition");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lieuPosition() {
    return (String) storedValueForKey("lieuPosition");
  }

  public void setLieuPosition(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating lieuPosition from " + lieuPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuPosition");
  }

  public String lieuPositionOrig() {
    return (String) storedValueForKey("lieuPositionOrig");
  }

  public void setLieuPositionOrig(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating lieuPositionOrig from " + lieuPositionOrig() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuPositionOrig");
  }

  public String noArretePosition() {
    return (String) storedValueForKey("noArretePosition");
  }

  public void setNoArretePosition(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating noArretePosition from " + noArretePosition() + " to " + value);
    }
    takeStoredValueForKey(value, "noArretePosition");
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey("noDossierPers");
  }

  public void setNoDossierPers(Integer value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating noDossierPers from " + noDossierPers() + " to " + value);
    }
    takeStoredValueForKey(value, "noDossierPers");
  }

  public Integer quotitePosition() {
    return (Integer) storedValueForKey("quotitePosition");
  }

  public void setQuotitePosition(Integer value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating quotitePosition from " + quotitePosition() + " to " + value);
    }
    takeStoredValueForKey(value, "quotitePosition");
  }

  public String temoinPositionPrev() {
    return (String) storedValueForKey("temoinPositionPrev");
  }

  public void setTemoinPositionPrev(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating temoinPositionPrev from " + temoinPositionPrev() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinPositionPrev");
  }

  public String temPcAcquitee() {
    return (String) storedValueForKey("temPcAcquitee");
  }

  public void setTemPcAcquitee(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating temPcAcquitee from " + temPcAcquitee() + " to " + value);
    }
    takeStoredValueForKey(value, "temPcAcquitee");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public String txtPosition() {
    return (String) storedValueForKey("txtPosition");
  }

  public void setTxtPosition(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating txtPosition from " + txtPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "txtPosition");
  }

  public String txtPositionOrig() {
    return (String) storedValueForKey("txtPositionOrig");
  }

  public void setTxtPositionOrig(String value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
    	_EOChangementPosition.LOG.debug( "updating txtPositionOrig from " + txtPositionOrig() + " to " + value);
    }
    takeStoredValueForKey(value, "txtPositionOrig");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOAbsence toAbsence() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOAbsence)storedValueForKey("toAbsence");
  }

  public void setToAbsenceRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAbsence value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating toAbsence from " + toAbsence() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOAbsence oldValue = toAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAbsence");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toAbsence");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOCarriere toCarriere() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.fwkcktlgrh.common.metier.EOCarriere value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOEnfant toEnfant() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEnfant)storedValueForKey("toEnfant");
  }

  public void setToEnfantRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEnfant value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating toEnfant from " + toEnfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOEnfant oldValue = toEnfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEnfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEnfant");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition toMotifPosition() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition)storedValueForKey("toMotifPosition");
  }

  public void setToMotifPositionRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating toMotifPosition from " + toMotifPosition() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition oldValue = toMotifPosition();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMotifPosition");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMotifPosition");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPays toPays() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toPays");
  }

  public void setToPaysRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating toPays from " + toPays() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toPays();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPays");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPays");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPosition toPosition() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPosition)storedValueForKey("toPosition");
  }

  public void setToPositionRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPosition value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating toPosition from " + toPosition() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPosition oldValue = toPosition();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPosition");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPosition");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey("toRne");
  }

  public void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating toRne from " + toRne() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRne");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toRne");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRneOrigine() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey("toRneOrigine");
  }

  public void setToRneOrigineRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (_EOChangementPosition.LOG.isDebugEnabled()) {
      _EOChangementPosition.LOG.debug("updating toRneOrigine from " + toRneOrigine() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRneOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRneOrigine");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toRneOrigine");
    }
  }
  

  public static EOChangementPosition create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebPosition
, NSTimestamp dModification
, Integer noDossierPers
, Integer quotitePosition
, String temoinPositionPrev
, String temPcAcquitee
, String temValide
, org.cocktail.fwkcktlgrh.common.metier.EOCarriere toCarriere, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu) {
    EOChangementPosition eo = (EOChangementPosition) EOUtilities.createAndInsertInstance(editingContext, _EOChangementPosition.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebPosition(dDebPosition);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setQuotitePosition(quotitePosition);
		eo.setTemoinPositionPrev(temoinPositionPrev);
		eo.setTemPcAcquitee(temPcAcquitee);
		eo.setTemValide(temValide);
    eo.setToCarriereRelationship(toCarriere);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  public static NSArray<EOChangementPosition> fetchAll(EOEditingContext editingContext) {
    return _EOChangementPosition.fetchAll(editingContext, null);
  }

  public static NSArray<EOChangementPosition> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOChangementPosition.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOChangementPosition> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOChangementPosition.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOChangementPosition> eoObjects = (NSArray<EOChangementPosition>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOChangementPosition fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChangementPosition.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChangementPosition fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOChangementPosition> eoObjects = _EOChangementPosition.fetch(editingContext, qualifier, null);
    EOChangementPosition eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOChangementPosition)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_ChangementPosition that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChangementPosition fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChangementPosition.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOChangementPosition fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOChangementPosition eoObject = _EOChangementPosition.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_ChangementPosition that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOChangementPosition fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOChangementPosition fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOChangementPosition eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOChangementPosition)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOChangementPosition localInstanceIn(EOEditingContext editingContext, EOChangementPosition eo) {
    EOChangementPosition localInstance = (eo == null) ? null : (EOChangementPosition)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
