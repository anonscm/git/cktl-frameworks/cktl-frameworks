/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOAgentPersonnel.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOAgentPersonnel extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_AgentPersonnel";

	// Attributes
	public static final String AGT_ADMINISTRATION_KEY = "agtAdministration";
	public static final String AGT_CONS_ACCIDENT_KEY = "agtConsAccident";
	public static final String AGT_CONS_AGENTS_KEY = "agtConsAgents";
	public static final String AGT_CONS_CARRIERE_KEY = "agtConsCarriere";
	public static final String AGT_CONS_CONGES_KEY = "agtConsConges";
	public static final String AGT_CONS_CONTRAT_KEY = "agtConsContrat";
	public static final String AGT_CONS_DOSSIER_KEY = "agtConsDossier";
	public static final String AGT_CONS_EMPLOIS_KEY = "agtConsEmplois";
	public static final String AGT_CONS_IDENTITE_KEY = "agtConsIdentite";
	public static final String AGT_CONS_OCCUPATION_KEY = "agtConsOccupation";
	public static final String AGT_CONS_POSTES_KEY = "agtConsPostes";
	public static final String AGT_EDITION_FOS_KEY = "agtEditionFos";
	public static final String AGT_EDITION_REQUETES_KEY = "agtEditionRequetes";
	public static final String AGT_ELECTION_KEY = "agtElection";
	public static final String AGT_INFOS_PERSO_KEY = "agtInfosPerso";
	public static final String AGT_NOMENCLATURES_KEY = "agtNomenclatures";
	public static final String AGT_OUTILS_KEY = "agtOutils";
	public static final String AGT_PRIME_KEY = "agtPrime";
	public static final String AGT_PROMOUVABLE_KEY = "agtPromouvable";
	public static final String AGT_TOUT_KEY = "agtTout";
	public static final String AGT_TYPE_POPULATION_KEY = "agtTypePopulation";
	public static final String AGT_UPD_ACCIDENT_KEY = "agtUpdAccident";
	public static final String AGT_UPD_AGENTS_KEY = "agtUpdAgents";
	public static final String AGT_UPD_CARRIERE_KEY = "agtUpdCarriere";
	public static final String AGT_UPD_CONGES_KEY = "agtUpdConges";
	public static final String AGT_UPD_CONTRAT_KEY = "agtUpdContrat";
	public static final String AGT_UPD_EMPLOIS_KEY = "agtUpdEmplois";
	public static final String AGT_UPD_IDENTITE_KEY = "agtUpdIdentite";
	public static final String AGT_UPD_OCCUPATION_KEY = "agtUpdOccupation";
	public static final String AGT_UPD_POSTES_KEY = "agtUpdPostes";
	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	public static final ERXKey<String> AGT_ADMINISTRATION = new ERXKey<String>("agtAdministration");
	public static final ERXKey<String> AGT_CONS_ACCIDENT = new ERXKey<String>("agtConsAccident");
	public static final ERXKey<String> AGT_CONS_AGENTS = new ERXKey<String>("agtConsAgents");
	public static final ERXKey<String> AGT_CONS_CARRIERE = new ERXKey<String>("agtConsCarriere");
	public static final ERXKey<String> AGT_CONS_CONGES = new ERXKey<String>("agtConsConges");
	public static final ERXKey<String> AGT_CONS_CONTRAT = new ERXKey<String>("agtConsContrat");
	public static final ERXKey<String> AGT_CONS_DOSSIER = new ERXKey<String>("agtConsDossier");
	public static final ERXKey<String> AGT_CONS_EMPLOIS = new ERXKey<String>("agtConsEmplois");
	public static final ERXKey<String> AGT_CONS_IDENTITE = new ERXKey<String>("agtConsIdentite");
	public static final ERXKey<String> AGT_CONS_OCCUPATION = new ERXKey<String>("agtConsOccupation");
	public static final ERXKey<String> AGT_CONS_POSTES = new ERXKey<String>("agtConsPostes");
	public static final ERXKey<String> AGT_EDITION_FOS = new ERXKey<String>("agtEditionFos");
	public static final ERXKey<String> AGT_EDITION_REQUETES = new ERXKey<String>("agtEditionRequetes");
	public static final ERXKey<String> AGT_ELECTION = new ERXKey<String>("agtElection");
	public static final ERXKey<String> AGT_INFOS_PERSO = new ERXKey<String>("agtInfosPerso");
	public static final ERXKey<String> AGT_NOMENCLATURES = new ERXKey<String>("agtNomenclatures");
	public static final ERXKey<String> AGT_OUTILS = new ERXKey<String>("agtOutils");
	public static final ERXKey<String> AGT_PRIME = new ERXKey<String>("agtPrime");
	public static final ERXKey<String> AGT_PROMOUVABLE = new ERXKey<String>("agtPromouvable");
	public static final ERXKey<String> AGT_TOUT = new ERXKey<String>("agtTout");
	public static final ERXKey<String> AGT_TYPE_POPULATION = new ERXKey<String>("agtTypePopulation");
	public static final ERXKey<String> AGT_UPD_ACCIDENT = new ERXKey<String>("agtUpdAccident");
	public static final ERXKey<String> AGT_UPD_AGENTS = new ERXKey<String>("agtUpdAgents");
	public static final ERXKey<String> AGT_UPD_CARRIERE = new ERXKey<String>("agtUpdCarriere");
	public static final ERXKey<String> AGT_UPD_CONGES = new ERXKey<String>("agtUpdConges");
	public static final ERXKey<String> AGT_UPD_CONTRAT = new ERXKey<String>("agtUpdContrat");
	public static final ERXKey<String> AGT_UPD_EMPLOIS = new ERXKey<String>("agtUpdEmplois");
	public static final ERXKey<String> AGT_UPD_IDENTITE = new ERXKey<String>("agtUpdIdentite");
	public static final ERXKey<String> AGT_UPD_OCCUPATION = new ERXKey<String>("agtUpdOccupation");
	public static final ERXKey<String> AGT_UPD_POSTES = new ERXKey<String>("agtUpdPostes");
	public static final ERXKey<Integer> CPT_ORDRE = new ERXKey<Integer>("cptOrdre");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	// Relationships
	public static final String TO_AGENTS_DROITS_SERVICES_KEY = "toAgentsDroitsServices";
	public static final String TO_COMPTE_KEY = "toCompte";
	public static final String TO_INDIVIDU_KEY = "toIndividu";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices> TO_AGENTS_DROITS_SERVICES = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices>("toAgentsDroitsServices");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> TO_COMPTE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>("toCompte");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");

  private static Logger LOG = Logger.getLogger(_EOAgentPersonnel.class);

  public EOAgentPersonnel localInstanceIn(EOEditingContext editingContext) {
    EOAgentPersonnel localInstance = (EOAgentPersonnel)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String agtAdministration() {
    return (String) storedValueForKey("agtAdministration");
  }

  public void setAgtAdministration(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtAdministration from " + agtAdministration() + " to " + value);
    }
    takeStoredValueForKey(value, "agtAdministration");
  }

  public String agtConsAccident() {
    return (String) storedValueForKey("agtConsAccident");
  }

  public void setAgtConsAccident(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtConsAccident from " + agtConsAccident() + " to " + value);
    }
    takeStoredValueForKey(value, "agtConsAccident");
  }

  public String agtConsAgents() {
    return (String) storedValueForKey("agtConsAgents");
  }

  public void setAgtConsAgents(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtConsAgents from " + agtConsAgents() + " to " + value);
    }
    takeStoredValueForKey(value, "agtConsAgents");
  }

  public String agtConsCarriere() {
    return (String) storedValueForKey("agtConsCarriere");
  }

  public void setAgtConsCarriere(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtConsCarriere from " + agtConsCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "agtConsCarriere");
  }

  public String agtConsConges() {
    return (String) storedValueForKey("agtConsConges");
  }

  public void setAgtConsConges(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtConsConges from " + agtConsConges() + " to " + value);
    }
    takeStoredValueForKey(value, "agtConsConges");
  }

  public String agtConsContrat() {
    return (String) storedValueForKey("agtConsContrat");
  }

  public void setAgtConsContrat(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtConsContrat from " + agtConsContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "agtConsContrat");
  }

  public String agtConsDossier() {
    return (String) storedValueForKey("agtConsDossier");
  }

  public void setAgtConsDossier(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtConsDossier from " + agtConsDossier() + " to " + value);
    }
    takeStoredValueForKey(value, "agtConsDossier");
  }

  public String agtConsEmplois() {
    return (String) storedValueForKey("agtConsEmplois");
  }

  public void setAgtConsEmplois(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtConsEmplois from " + agtConsEmplois() + " to " + value);
    }
    takeStoredValueForKey(value, "agtConsEmplois");
  }

  public String agtConsIdentite() {
    return (String) storedValueForKey("agtConsIdentite");
  }

  public void setAgtConsIdentite(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtConsIdentite from " + agtConsIdentite() + " to " + value);
    }
    takeStoredValueForKey(value, "agtConsIdentite");
  }

  public String agtConsOccupation() {
    return (String) storedValueForKey("agtConsOccupation");
  }

  public void setAgtConsOccupation(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtConsOccupation from " + agtConsOccupation() + " to " + value);
    }
    takeStoredValueForKey(value, "agtConsOccupation");
  }

  public String agtConsPostes() {
    return (String) storedValueForKey("agtConsPostes");
  }

  public void setAgtConsPostes(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtConsPostes from " + agtConsPostes() + " to " + value);
    }
    takeStoredValueForKey(value, "agtConsPostes");
  }

  public String agtEditionFos() {
    return (String) storedValueForKey("agtEditionFos");
  }

  public void setAgtEditionFos(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtEditionFos from " + agtEditionFos() + " to " + value);
    }
    takeStoredValueForKey(value, "agtEditionFos");
  }

  public String agtEditionRequetes() {
    return (String) storedValueForKey("agtEditionRequetes");
  }

  public void setAgtEditionRequetes(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtEditionRequetes from " + agtEditionRequetes() + " to " + value);
    }
    takeStoredValueForKey(value, "agtEditionRequetes");
  }

  public String agtElection() {
    return (String) storedValueForKey("agtElection");
  }

  public void setAgtElection(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtElection from " + agtElection() + " to " + value);
    }
    takeStoredValueForKey(value, "agtElection");
  }

  public String agtInfosPerso() {
    return (String) storedValueForKey("agtInfosPerso");
  }

  public void setAgtInfosPerso(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtInfosPerso from " + agtInfosPerso() + " to " + value);
    }
    takeStoredValueForKey(value, "agtInfosPerso");
  }

  public String agtNomenclatures() {
    return (String) storedValueForKey("agtNomenclatures");
  }

  public void setAgtNomenclatures(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtNomenclatures from " + agtNomenclatures() + " to " + value);
    }
    takeStoredValueForKey(value, "agtNomenclatures");
  }

  public String agtOutils() {
    return (String) storedValueForKey("agtOutils");
  }

  public void setAgtOutils(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtOutils from " + agtOutils() + " to " + value);
    }
    takeStoredValueForKey(value, "agtOutils");
  }

  public String agtPrime() {
    return (String) storedValueForKey("agtPrime");
  }

  public void setAgtPrime(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtPrime from " + agtPrime() + " to " + value);
    }
    takeStoredValueForKey(value, "agtPrime");
  }

  public String agtPromouvable() {
    return (String) storedValueForKey("agtPromouvable");
  }

  public void setAgtPromouvable(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtPromouvable from " + agtPromouvable() + " to " + value);
    }
    takeStoredValueForKey(value, "agtPromouvable");
  }

  public String agtTout() {
    return (String) storedValueForKey("agtTout");
  }

  public void setAgtTout(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtTout from " + agtTout() + " to " + value);
    }
    takeStoredValueForKey(value, "agtTout");
  }

  public String agtTypePopulation() {
    return (String) storedValueForKey("agtTypePopulation");
  }

  public void setAgtTypePopulation(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtTypePopulation from " + agtTypePopulation() + " to " + value);
    }
    takeStoredValueForKey(value, "agtTypePopulation");
  }

  public String agtUpdAccident() {
    return (String) storedValueForKey("agtUpdAccident");
  }

  public void setAgtUpdAccident(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtUpdAccident from " + agtUpdAccident() + " to " + value);
    }
    takeStoredValueForKey(value, "agtUpdAccident");
  }

  public String agtUpdAgents() {
    return (String) storedValueForKey("agtUpdAgents");
  }

  public void setAgtUpdAgents(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtUpdAgents from " + agtUpdAgents() + " to " + value);
    }
    takeStoredValueForKey(value, "agtUpdAgents");
  }

  public String agtUpdCarriere() {
    return (String) storedValueForKey("agtUpdCarriere");
  }

  public void setAgtUpdCarriere(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtUpdCarriere from " + agtUpdCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "agtUpdCarriere");
  }

  public String agtUpdConges() {
    return (String) storedValueForKey("agtUpdConges");
  }

  public void setAgtUpdConges(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtUpdConges from " + agtUpdConges() + " to " + value);
    }
    takeStoredValueForKey(value, "agtUpdConges");
  }

  public String agtUpdContrat() {
    return (String) storedValueForKey("agtUpdContrat");
  }

  public void setAgtUpdContrat(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtUpdContrat from " + agtUpdContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "agtUpdContrat");
  }

  public String agtUpdEmplois() {
    return (String) storedValueForKey("agtUpdEmplois");
  }

  public void setAgtUpdEmplois(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtUpdEmplois from " + agtUpdEmplois() + " to " + value);
    }
    takeStoredValueForKey(value, "agtUpdEmplois");
  }

  public String agtUpdIdentite() {
    return (String) storedValueForKey("agtUpdIdentite");
  }

  public void setAgtUpdIdentite(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtUpdIdentite from " + agtUpdIdentite() + " to " + value);
    }
    takeStoredValueForKey(value, "agtUpdIdentite");
  }

  public String agtUpdOccupation() {
    return (String) storedValueForKey("agtUpdOccupation");
  }

  public void setAgtUpdOccupation(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtUpdOccupation from " + agtUpdOccupation() + " to " + value);
    }
    takeStoredValueForKey(value, "agtUpdOccupation");
  }

  public String agtUpdPostes() {
    return (String) storedValueForKey("agtUpdPostes");
  }

  public void setAgtUpdPostes(String value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating agtUpdPostes from " + agtUpdPostes() + " to " + value);
    }
    takeStoredValueForKey(value, "agtUpdPostes");
  }

  public Integer cptOrdre() {
    return (Integer) storedValueForKey("cptOrdre");
  }

  public void setCptOrdre(Integer value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating cptOrdre from " + cptOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "cptOrdre");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
    	_EOAgentPersonnel.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCompte toCompte() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCompte)storedValueForKey("toCompte");
  }

  public void setToCompteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompte value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
      _EOAgentPersonnel.LOG.debug("updating toCompte from " + toCompte() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCompte oldValue = toCompte();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCompte");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCompte");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
      _EOAgentPersonnel.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices> toAgentsDroitsServices() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices>)storedValueForKey("toAgentsDroitsServices");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices> toAgentsDroitsServices(EOQualifier qualifier) {
    return toAgentsDroitsServices(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices> toAgentsDroitsServices(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices> results;
      results = toAgentsDroitsServices();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToAgentsDroitsServicesRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices object) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
      _EOAgentPersonnel.LOG.debug("adding " + object + " to toAgentsDroitsServices relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toAgentsDroitsServices");
  }

  public void removeFromToAgentsDroitsServicesRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices object) {
    if (_EOAgentPersonnel.LOG.isDebugEnabled()) {
      _EOAgentPersonnel.LOG.debug("removing " + object + " from toAgentsDroitsServices relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toAgentsDroitsServices");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices createToAgentsDroitsServicesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_AgentsDroitsServices");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toAgentsDroitsServices");
    return (org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices) eo;
  }

  public void deleteToAgentsDroitsServicesRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toAgentsDroitsServices");
    editingContext().deleteObject(object);
  }

  public void deleteAllToAgentsDroitsServicesRelationships() {
    Enumeration objects = toAgentsDroitsServices().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToAgentsDroitsServicesRelationship((org.cocktail.fwkcktlgrh.common.metier.EOAgentsDroitsServices)objects.nextElement());
    }
  }


  public static EOAgentPersonnel create(EOEditingContext editingContext, String agtNomenclatures
, Integer cptOrdre
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlpersonne.common.metier.EOCompte toCompte, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu) {
    EOAgentPersonnel eo = (EOAgentPersonnel) EOUtilities.createAndInsertInstance(editingContext, _EOAgentPersonnel.ENTITY_NAME);    
		eo.setAgtNomenclatures(agtNomenclatures);
		eo.setCptOrdre(cptOrdre);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToCompteRelationship(toCompte);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  public static NSArray<EOAgentPersonnel> fetchAll(EOEditingContext editingContext) {
    return _EOAgentPersonnel.fetchAll(editingContext, null);
  }

  public static NSArray<EOAgentPersonnel> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAgentPersonnel.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOAgentPersonnel> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOAgentPersonnel.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAgentPersonnel> eoObjects = (NSArray<EOAgentPersonnel>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOAgentPersonnel fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAgentPersonnel.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAgentPersonnel fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAgentPersonnel> eoObjects = _EOAgentPersonnel.fetch(editingContext, qualifier, null);
    EOAgentPersonnel eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOAgentPersonnel)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_AgentPersonnel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAgentPersonnel fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAgentPersonnel.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOAgentPersonnel fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAgentPersonnel eoObject = _EOAgentPersonnel.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_AgentPersonnel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOAgentPersonnel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAgentPersonnel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAgentPersonnel eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAgentPersonnel)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOAgentPersonnel localInstanceIn(EOEditingContext editingContext, EOAgentPersonnel eo) {
    EOAgentPersonnel localInstance = (eo == null) ? null : (EOAgentPersonnel)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
