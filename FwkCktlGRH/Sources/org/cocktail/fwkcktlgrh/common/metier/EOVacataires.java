/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import java.util.List;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOCnu;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOProfession;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Classe d'une vacation
 * 
 * @author Chama LAATIK
 */
public class EOVacataires extends _EOVacataires implements IVacataires {
	
	private static final long serialVersionUID = 3779085711100068386L;

	/**
	 * @return le libellé du type de vacation
	 */
	public String getLibelleTypeVacation() {
		String libelle = null;
		if (isTitulaire()) {
			libelle = IVacataires.VACATAIRE_FONCTIONNAIRE;
		} else {
			libelle = IVacataires.VACATAIRE_NON_FONCTIONNAIRE;
		}
		
		return libelle;
	}
	
	/**
	 * Est-ce que la vacation concerne un enseignant?
	 * @return Oui/Non
	 */
	public boolean isEnseignant() {
		return getTemoinEnseignant() != null && getTemoinEnseignant().equals(Constantes.VRAI);
	}
	
	/**
	 * Est-ce que le vacataire est payé?
	 * @return Oui/Non
	 */
	public boolean isPaye() {
		return getTemoinPaye() != null && getTemoinPaye().equals(Constantes.VRAI);
	}
	
	/**
	 * Est-ce que la vacation est signée?
	 * @return Oui/Non
	 */
	public boolean isVacationSigne() {
		return getTemoinSigne() != null && getTemoinSigne().equals(Constantes.VRAI);
	}
	
	/**
	 * Est-ce que la vacation concerne un titulaire?
	 * @return Oui/Non
	 */
	public boolean isTitulaire() {
		return getTemoinTitulaire() != null && getTemoinTitulaire().equals(Constantes.VRAI);
	}
	
	/**
	 * Est-ce que la vacation est valide?
	 * @return Oui/Non
	 */
	public boolean isValide() {
		return getTemoinValide() != null && getTemoinValide().equals(Constantes.VRAI);
	}
	
	/**
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException{
	}

	public NSTimestamp getDateArrete() {
		return dateArrete();
	}

	public NSTimestamp getDateCreation() {
		return dateCreation();
	}

	public NSTimestamp getDateDebut() {
		return dateDebut();
	}

	public NSTimestamp getDateFin() {
		return dateFin();
	}

	public NSTimestamp getDateModification() {
		return dateModification();
	}

	public String getLibelleEnseignement() {
		return libelleEnseignement();
	}

	public Double getNbHeures() {
		return nbHeures();
	}

	public Double getNbHeuresRealisees() {
		return nbHeuresRealisees();
	}

	public String getNoArrete() {
		return noArrete();
	}

	public String getObservations() {
		return observations();
	}

	public Double getTauxHoraire() {
		return tauxHoraire();
	}

	public Double getTauxHoraireRealise() {
		return tauxHoraireRealise();
	}

	public String getTemoinEnseignant() {
		return temoinEnseignant();
	}

	public String getTemoinPaye() {
		return temoinPaye();
	}

	public String getTemoinSigne() {
		return temoinSigne();
	}

	public String getTemoinTitulaire() {
		return temoinTitulaire();
	}

	public String getTemoinValide() {
		return temoinValide();
	}

	public EOAdresse getToAdresse() {
		return toAdresse();
	}

	public EOCnu getToCnu() {
		return toCnu();
	}

	public EOCorps getToCorps() {
		return toCorps();
	}

	public EOGrade getToGrade() {
		return toGrade();
	}

	public IIndividu getToIndividuVacataire() {
		return toIndividuVacataire();
	}
	
	public EOTypeContratTravail getToTypeContratTravail() {
		return toTypeContratTravail();
	}

	/**
	 *  Setter pour un individu vacataire
	 * @param value : un vacataire {@link IIndividu}
	 **/
	public void setToIndividuVacataireRelationship(IIndividu value) {
		super.setToIndividuVacataireRelationship((EOIndividu) value);
	}

	public EOPersonne getToPersonneCreation() {
		return toPersonneCreation();
	}

	public EOPersonne getToPersonneModification() {
		return toPersonneModification();
	}

	public EOProfession getToProfession() {
		return toProfession();
	}

	public IStructure getToStructure() {
		return toStructure();
	}

	/**
	 *  Setter pour une structure 
	 * @param value : une structure {@link IStructure}
	 **/
	public void setToStructureRelationship(IStructure value) {
		super.setToStructureRelationship((EOStructure) value);
	}
	
	public List<EOVacatairesAffectation> getListeVacatairesAffectations() {
		return listeVacatairesAffectations();
	}

}
