/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOFeveParametres.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOFeveParametres extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_FeveParametres";

	// Attributes
	public static final String PARAM_COMMENTAIRES_KEY = "paramCommentaires";
	public static final String PARAM_KEY_KEY = "paramKey";
	public static final String PARAM_VALUE_KEY = "paramValue";

	public static final ERXKey<String> PARAM_COMMENTAIRES = new ERXKey<String>("paramCommentaires");
	public static final ERXKey<String> PARAM_KEY = new ERXKey<String>("paramKey");
	public static final ERXKey<String> PARAM_VALUE = new ERXKey<String>("paramValue");
	// Relationships


  private static Logger LOG = Logger.getLogger(_EOFeveParametres.class);

  public EOFeveParametres localInstanceIn(EOEditingContext editingContext) {
    EOFeveParametres localInstance = (EOFeveParametres)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String paramCommentaires() {
    return (String) storedValueForKey("paramCommentaires");
  }

  public void setParamCommentaires(String value) {
    if (_EOFeveParametres.LOG.isDebugEnabled()) {
    	_EOFeveParametres.LOG.debug( "updating paramCommentaires from " + paramCommentaires() + " to " + value);
    }
    takeStoredValueForKey(value, "paramCommentaires");
  }

  public String paramKey() {
    return (String) storedValueForKey("paramKey");
  }

  public void setParamKey(String value) {
    if (_EOFeveParametres.LOG.isDebugEnabled()) {
    	_EOFeveParametres.LOG.debug( "updating paramKey from " + paramKey() + " to " + value);
    }
    takeStoredValueForKey(value, "paramKey");
  }

  public String paramValue() {
    return (String) storedValueForKey("paramValue");
  }

  public void setParamValue(String value) {
    if (_EOFeveParametres.LOG.isDebugEnabled()) {
    	_EOFeveParametres.LOG.debug( "updating paramValue from " + paramValue() + " to " + value);
    }
    takeStoredValueForKey(value, "paramValue");
  }


  public static EOFeveParametres create(EOEditingContext editingContext, String paramKey
) {
    EOFeveParametres eo = (EOFeveParametres) EOUtilities.createAndInsertInstance(editingContext, _EOFeveParametres.ENTITY_NAME);    
		eo.setParamKey(paramKey);
    return eo;
  }

  public static NSArray<EOFeveParametres> fetchAll(EOEditingContext editingContext) {
    return _EOFeveParametres.fetchAll(editingContext, null);
  }

  public static NSArray<EOFeveParametres> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOFeveParametres.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOFeveParametres> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOFeveParametres.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOFeveParametres> eoObjects = (NSArray<EOFeveParametres>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOFeveParametres fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFeveParametres.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFeveParametres fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOFeveParametres> eoObjects = _EOFeveParametres.fetch(editingContext, qualifier, null);
    EOFeveParametres eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOFeveParametres)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_FeveParametres that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFeveParametres fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFeveParametres.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOFeveParametres fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOFeveParametres eoObject = _EOFeveParametres.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_FeveParametres that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOFeveParametres fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFeveParametres fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFeveParametres eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFeveParametres)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOFeveParametres localInstanceIn(EOEditingContext editingContext, EOFeveParametres eo) {
    EOFeveParametres localInstance = (eo == null) ? null : (EOFeveParametres)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
