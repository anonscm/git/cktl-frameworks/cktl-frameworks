/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IChangementPosition;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPosition;
import org.cocktail.fwkcktlgrh.common.utilities.CocktailConstantes;
import org.cocktail.fwkcktlgrh.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EOChangementPosition extends _EOChangementPosition implements IChangementPosition {
	public static EOSortOrdering SORT_DATE_DEBUT_ASC = new EOSortOrdering(D_DEB_POSITION_KEY, EOSortOrdering.CompareAscending);
	public static EOSortOrdering SORT_DATE_DEBUT_DESC = new EOSortOrdering(D_DEB_POSITION_KEY, EOSortOrdering.CompareDescending);
	public static EOSortOrdering SORT_DATE_FIN_ASC = new EOSortOrdering(D_FIN_POSITION_KEY, EOSortOrdering.CompareAscending);

	public static NSArray<EOSortOrdering> SORT_ARRAY_DATE_DEBUT_ASC = new NSArray<EOSortOrdering>(SORT_DATE_DEBUT_ASC);
	public static NSArray<EOSortOrdering> SORT_ARRAY_DATE_DEBUT_DESC = new NSArray<EOSortOrdering>(SORT_DATE_DEBUT_DESC);

	private static Logger log = Logger.getLogger(EOChangementPosition.class);

	/**
	 * @return le N° de dossier de l'agent
	 */
	public Integer noDossierPers() {
		return toIndividu().noIndividu();
	}

	/**
	 * 
	 * @return le libellé de la position
	 */
	public String libellePosition() {
		return toPosition().lcPosition();
	}

	/**
	 * 
	 * @return libellé court de l'établissement où la position est exercée
	 */
	public String lcEtabPosition() {
		String libelleRne = "";

		if (toRne() != null) {
			libelleRne = toRne().llRne();
		}

		return libelleRne;
	}

	/**
	 * 
	 * @return libellé court de l'établissement chargé de gérer un agent détaché
	 */
	public String lcEtabOrigPosition() {
		return toRneOrigine().lcRne();
	}

	/**
	 * 
	 * @return le lieu où se fait la position
	 */
	public String lieuDestin() {
		if (toRne() != null) {
			return toRne().llRne();
		} else {
			return lieuPosition();
		}
	}

	/**
	 * 
	 * @return le lieu où se fait le détachement
	 */
	public String lieuOrigine() {
		if (toRneOrigine() != null) {
			return toRneOrigine().llRne();
		} else {
			return lieuPositionOrig();
		}
	}

	/**
	 * 
	 * @param ec editing context
	 * @param ind l'individu pour la recherche
	 * @return les positions valides de cet individu
	 */
	public static NSArray<EOChangementPosition> positionsValidesForIndividu(EOEditingContext ec, EOIndividu ind) {
		if (ind == null) {
			return null;
		}
		EOQualifier qual = ERXQ.equals(EOChangementPosition.TO_INDIVIDU_KEY, ind);
		qual = ERXQ.and(qual, ERXQ.equals(TEM_VALIDE_KEY, Constantes.VRAI));
		NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(D_DEB_POSITION_KEY).array();
		return fetchAll(ec, qual, sorts);
	}

	/**
	 * 
	 * @param ec editing context
	 * @param ind l'individu pour la recherche
	 * @return la derniere position valide de cet individu
	 */
	public static EOChangementPosition dernierePositionValideCouranteForIndividu(EOEditingContext ec, EOIndividu ind) {
		if (ind == null) {
			return null;
		}

		EOQualifier qualifier = ERXQ.and(EOChangementPosition.TO_INDIVIDU.eq(ind), EOChangementPosition.TEM_VALIDE.eq(Constantes.VRAI), EOChangementPosition.D_DEB_POSITION.lessThanOrEqualTo(MyDateCtrl.getDateJour()),
				ERXQ.or(EOChangementPosition.D_FIN_POSITION.isNull(), EOChangementPosition.D_FIN_POSITION.greaterThanOrEqualTo(MyDateCtrl.getDateJour())));

		NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(D_DEB_POSITION_KEY).array();
		return fetchFirstByQualifier(ec, qualifier, sorts);
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * Recherche les changements de position d'un individu pour une periode
	 * classes par ordre de debut croissant
	 * 
	 * @param individu
	 * @param debutPeriode debut de la periode recherchee (peut etre nulle)
	 * @param finPeriode fin de la periode recherchee (peut etre nulle)
	 */
	public static NSArray<EOChangementPosition> rechercherChangementsPourIndividuEtPeriode(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));

		if (debutPeriode != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(D_DEB_POSITION_KEY, debutPeriode, D_FIN_POSITION_KEY, finPeriode));
		}

		return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_ASC);

	}
	
	/** Recherche si un individu a une position de Service Nationale pendant une periode */
	public static boolean individuAuServiceMilitairePourPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return individuOccupePositionPendantPeriode(editingContext, individu, "SNAT", debutPeriode, finPeriode);
	}

	// return true si l'individu occupe la position pendant la période
	private static boolean individuOccupePositionPendantPeriode(EOEditingContext editingContext,EOIndividu individu,String position,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSArray changements = positionsPourTypePendantPeriode(editingContext, individu, position, debutPeriode, finPeriode);
		return changements != null && changements.count() > 0;
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param position
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	private static NSArray positionsPourTypePendantPeriode(EOEditingContext editingContext,EOIndividu individu,String position,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray args = new NSMutableArray(individu);
		args.addObject(position);
		String stringQualifier = TO_INDIVIDU_KEY + "=%@ AND "+TEM_VALIDE_KEY+" = 'O' AND "+TO_POSITION_KEY+"."+EOPosition.C_POSITION_KEY+" = %@";
		NSMutableArray qualifiers = new NSMutableArray();
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		qualifiers.addObject(qualifier);
		if (debutPeriode != null) {
			qualifier = SuperFinder.qualifierPourPeriode(D_DEB_POSITION_KEY,debutPeriode,D_FIN_POSITION_KEY,finPeriode);
			qualifiers.addObject(qualifier);
		}
		qualifier = new EOAndQualifier(qualifiers);
		NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey(D_DEB_POSITION_KEY,EOSortOrdering.CompareDescending));
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,sorts);
		NSArray changements = editingContext.objectsWithFetchSpecification(myFetch);
		return changements;
	}

}
