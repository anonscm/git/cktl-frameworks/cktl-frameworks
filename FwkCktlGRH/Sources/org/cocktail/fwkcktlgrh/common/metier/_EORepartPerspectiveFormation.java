/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartPerspectiveFormation.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartPerspectiveFormation extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_RepartPerspectiveFormation";

	// Attributes
	public static final String ECHEANCE_KEY = "echeance";
	public static final String EVOLUTION_KEY_KEY = "evolutionKey";
	public static final String LIBELLE_KEY = "libelle";

	public static final ERXKey<String> ECHEANCE = new ERXKey<String>("echeance");
	public static final ERXKey<Integer> EVOLUTION_KEY = new ERXKey<Integer>("evolutionKey");
	public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
	// Relationships
	public static final String TO_EVALUATION_KEY = "toEvaluation";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> TO_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>("toEvaluation");

  private static Logger LOG = Logger.getLogger(_EORepartPerspectiveFormation.class);

  public EORepartPerspectiveFormation localInstanceIn(EOEditingContext editingContext) {
    EORepartPerspectiveFormation localInstance = (EORepartPerspectiveFormation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String echeance() {
    return (String) storedValueForKey("echeance");
  }

  public void setEcheance(String value) {
    if (_EORepartPerspectiveFormation.LOG.isDebugEnabled()) {
    	_EORepartPerspectiveFormation.LOG.debug( "updating echeance from " + echeance() + " to " + value);
    }
    takeStoredValueForKey(value, "echeance");
  }

  public Integer evolutionKey() {
    return (Integer) storedValueForKey("evolutionKey");
  }

  public void setEvolutionKey(Integer value) {
    if (_EORepartPerspectiveFormation.LOG.isDebugEnabled()) {
    	_EORepartPerspectiveFormation.LOG.debug( "updating evolutionKey from " + evolutionKey() + " to " + value);
    }
    takeStoredValueForKey(value, "evolutionKey");
  }

  public String libelle() {
    return (String) storedValueForKey("libelle");
  }

  public void setLibelle(String value) {
    if (_EORepartPerspectiveFormation.LOG.isDebugEnabled()) {
    	_EORepartPerspectiveFormation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, "libelle");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluation)storedValueForKey("toEvaluation");
  }

  public void setToEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation value) {
    if (_EORepartPerspectiveFormation.LOG.isDebugEnabled()) {
      _EORepartPerspectiveFormation.LOG.debug("updating toEvaluation from " + toEvaluation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluation oldValue = toEvaluation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluation");
    }
  }
  

  public static EORepartPerspectiveFormation create(EOEditingContext editingContext, String echeance
, Integer evolutionKey
, String libelle
, org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation) {
    EORepartPerspectiveFormation eo = (EORepartPerspectiveFormation) EOUtilities.createAndInsertInstance(editingContext, _EORepartPerspectiveFormation.ENTITY_NAME);    
		eo.setEcheance(echeance);
		eo.setEvolutionKey(evolutionKey);
		eo.setLibelle(libelle);
    eo.setToEvaluationRelationship(toEvaluation);
    return eo;
  }

  public static NSArray<EORepartPerspectiveFormation> fetchAll(EOEditingContext editingContext) {
    return _EORepartPerspectiveFormation.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartPerspectiveFormation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartPerspectiveFormation.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartPerspectiveFormation> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartPerspectiveFormation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartPerspectiveFormation> eoObjects = (NSArray<EORepartPerspectiveFormation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartPerspectiveFormation fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartPerspectiveFormation.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartPerspectiveFormation fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartPerspectiveFormation> eoObjects = _EORepartPerspectiveFormation.fetch(editingContext, qualifier, null);
    EORepartPerspectiveFormation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartPerspectiveFormation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartPerspectiveFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartPerspectiveFormation fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartPerspectiveFormation.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartPerspectiveFormation fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartPerspectiveFormation eoObject = _EORepartPerspectiveFormation.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartPerspectiveFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartPerspectiveFormation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartPerspectiveFormation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartPerspectiveFormation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartPerspectiveFormation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartPerspectiveFormation localInstanceIn(EOEditingContext editingContext, EORepartPerspectiveFormation eo) {
    EORepartPerspectiveFormation localInstance = (eo == null) ? null : (EORepartPerspectiveFormation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
