/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartFloLolfNomen.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartFloLolfNomen extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_RepartFloLolfNomen";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String RRF_QUOTITE_KEY = "rrfQuotite";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Double> RRF_QUOTITE = new ERXKey<Double>("rrfQuotite");
	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_LOLF_NOMENCLATURE_DEPENSE_KEY = "toLolfNomenclatureDepense";
	public static final String TO_REPART_FLO_SILLAND_KEY = "toRepartFloSilland";

	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense> TO_LOLF_NOMENCLATURE_DEPENSE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense>("toLolfNomenclatureDepense");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland> TO_REPART_FLO_SILLAND = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland>("toRepartFloSilland");

  private static Logger LOG = Logger.getLogger(_EORepartFloLolfNomen.class);

  public EORepartFloLolfNomen localInstanceIn(EOEditingContext editingContext) {
    EORepartFloLolfNomen localInstance = (EORepartFloLolfNomen)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartFloLolfNomen.LOG.isDebugEnabled()) {
    	_EORepartFloLolfNomen.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartFloLolfNomen.LOG.isDebugEnabled()) {
    	_EORepartFloLolfNomen.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Double rrfQuotite() {
    return (Double) storedValueForKey("rrfQuotite");
  }

  public void setRrfQuotite(Double value) {
    if (_EORepartFloLolfNomen.LOG.isDebugEnabled()) {
    	_EORepartFloLolfNomen.LOG.debug( "updating rrfQuotite from " + rrfQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "rrfQuotite");
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey("toExercice");
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (_EORepartFloLolfNomen.LOG.isDebugEnabled()) {
      _EORepartFloLolfNomen.LOG.debug("updating toExercice from " + toExercice() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toExercice");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toExercice");
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense toLolfNomenclatureDepense() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense)storedValueForKey("toLolfNomenclatureDepense");
  }

  public void setToLolfNomenclatureDepenseRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense value) {
    if (_EORepartFloLolfNomen.LOG.isDebugEnabled()) {
      _EORepartFloLolfNomen.LOG.debug("updating toLolfNomenclatureDepense from " + toLolfNomenclatureDepense() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense oldValue = toLolfNomenclatureDepense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toLolfNomenclatureDepense");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toLolfNomenclatureDepense");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland toRepartFloSilland() {
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland)storedValueForKey("toRepartFloSilland");
  }

  public void setToRepartFloSillandRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland value) {
    if (_EORepartFloLolfNomen.LOG.isDebugEnabled()) {
      _EORepartFloLolfNomen.LOG.debug("updating toRepartFloSilland from " + toRepartFloSilland() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland oldValue = toRepartFloSilland();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRepartFloSilland");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toRepartFloSilland");
    }
  }
  

  public static EORepartFloLolfNomen create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Double rrfQuotite
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice, org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense toLolfNomenclatureDepense, org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland toRepartFloSilland) {
    EORepartFloLolfNomen eo = (EORepartFloLolfNomen) EOUtilities.createAndInsertInstance(editingContext, _EORepartFloLolfNomen.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setRrfQuotite(rrfQuotite);
    eo.setToExerciceRelationship(toExercice);
    eo.setToLolfNomenclatureDepenseRelationship(toLolfNomenclatureDepense);
    eo.setToRepartFloSillandRelationship(toRepartFloSilland);
    return eo;
  }

  public static NSArray<EORepartFloLolfNomen> fetchAll(EOEditingContext editingContext) {
    return _EORepartFloLolfNomen.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartFloLolfNomen> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartFloLolfNomen.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartFloLolfNomen> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartFloLolfNomen.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartFloLolfNomen> eoObjects = (NSArray<EORepartFloLolfNomen>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartFloLolfNomen fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartFloLolfNomen.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartFloLolfNomen fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartFloLolfNomen> eoObjects = _EORepartFloLolfNomen.fetch(editingContext, qualifier, null);
    EORepartFloLolfNomen eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartFloLolfNomen)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartFloLolfNomen that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartFloLolfNomen fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartFloLolfNomen.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartFloLolfNomen fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartFloLolfNomen eoObject = _EORepartFloLolfNomen.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartFloLolfNomen that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartFloLolfNomen fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartFloLolfNomen fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartFloLolfNomen eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartFloLolfNomen)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartFloLolfNomen localInstanceIn(EOEditingContext editingContext, EORepartFloLolfNomen eo) {
    EORepartFloLolfNomen localInstance = (eo == null) ? null : (EORepartFloLolfNomen)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
