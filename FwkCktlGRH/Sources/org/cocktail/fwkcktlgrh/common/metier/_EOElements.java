/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOElements.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOElements extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_ElementCarriere";

	// Attributes
	public static final String C_BAP_KEY = "cBap";
	public static final String C_CHEVRON_KEY = "cChevron";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String CODEEMPLOI_KEY = "codeemploi";
	public static final String C_SPECIALITE_KEY = "cSpecialite";
	public static final String C_SPECIALITE_ATOS_KEY = "cSpecialiteAtos";
	public static final String C_TYPE_ACCES_KEY = "cTypeAcces";
	public static final String D_ARRETE_ANNULATION_KEY = "dArreteAnnulation";
	public static final String D_ARRETE_CARRIERE_KEY = "dArreteCarriere";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_EFFET_ELEMENT_KEY = "dEffetElement";
	public static final String D_FIN_ELEMENT_KEY = "dFinElement";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INM_EFFECTIF_KEY = "inmEffectif";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String NO_ARRETE_CARRIERE_KEY = "noArreteCarriere";
	public static final String QUOTITE_ELEMENT_KEY = "quotiteElement";
	public static final String REP_ANC_ECH_ANNEES_KEY = "repAncEchAnnees";
	public static final String REP_ANC_ECH_JOURS_KEY = "repAncEchJours";
	public static final String REP_ANC_ECH_MOIS_KEY = "repAncEchMois";
	public static final String TEM_ARRETE_SIGNE_KEY = "temArreteSigne";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_PROVISOIRE_KEY = "temProvisoire";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> C_BAP = new ERXKey<String>("cBap");
	public static final ERXKey<String> C_CHEVRON = new ERXKey<String>("cChevron");
	public static final ERXKey<String> C_CORPS = new ERXKey<String>("cCorps");
	public static final ERXKey<String> C_ECHELON = new ERXKey<String>("cEchelon");
	public static final ERXKey<String> CODEEMPLOI = new ERXKey<String>("codeemploi");
	public static final ERXKey<String> C_SPECIALITE = new ERXKey<String>("cSpecialite");
	public static final ERXKey<String> C_SPECIALITE_ATOS = new ERXKey<String>("cSpecialiteAtos");
	public static final ERXKey<String> C_TYPE_ACCES = new ERXKey<String>("cTypeAcces");
	public static final ERXKey<NSTimestamp> D_ARRETE_ANNULATION = new ERXKey<NSTimestamp>("dArreteAnnulation");
	public static final ERXKey<NSTimestamp> D_ARRETE_CARRIERE = new ERXKey<NSTimestamp>("dArreteCarriere");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_EFFET_ELEMENT = new ERXKey<NSTimestamp>("dEffetElement");
	public static final ERXKey<NSTimestamp> D_FIN_ELEMENT = new ERXKey<NSTimestamp>("dFinElement");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> INM_EFFECTIF = new ERXKey<Integer>("inmEffectif");
	public static final ERXKey<String> NO_ARRETE_ANNULATION = new ERXKey<String>("noArreteAnnulation");
	public static final ERXKey<String> NO_ARRETE_CARRIERE = new ERXKey<String>("noArreteCarriere");
	public static final ERXKey<Integer> QUOTITE_ELEMENT = new ERXKey<Integer>("quotiteElement");
	public static final ERXKey<Integer> REP_ANC_ECH_ANNEES = new ERXKey<Integer>("repAncEchAnnees");
	public static final ERXKey<Integer> REP_ANC_ECH_JOURS = new ERXKey<Integer>("repAncEchJours");
	public static final ERXKey<Integer> REP_ANC_ECH_MOIS = new ERXKey<Integer>("repAncEchMois");
	public static final ERXKey<String> TEM_ARRETE_SIGNE = new ERXKey<String>("temArreteSigne");
	public static final ERXKey<String> TEM_GEST_ETAB = new ERXKey<String>("temGestEtab");
	public static final ERXKey<String> TEM_PROVISOIRE = new ERXKey<String>("temProvisoire");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_CARRIERE_KEY = "toCarriere";
	public static final String TO_CNU_KEY = "toCnu";
	public static final String TO_CORPS_KEY = "toCorps";
	public static final String TO_DISC_SECOND_DEGRE_KEY = "toDiscSecondDegre";
	public static final String TO_GRADE_KEY = "toGrade";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_REFERENS_EMPLOIS_KEY = "toReferensEmplois";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOCarriere> TO_CARRIERE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOCarriere>("toCarriere");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> TO_CNU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu>("toCnu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> TO_CORPS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps>("toCorps");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODiscSecondDegre> TO_DISC_SECOND_DEGRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODiscSecondDegre>("toDiscSecondDegre");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> TO_GRADE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGrade>("toGrade");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> TO_REFERENS_EMPLOIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>("toReferensEmplois");

  private static Logger LOG = Logger.getLogger(_EOElements.class);

  public EOElements localInstanceIn(EOEditingContext editingContext) {
    EOElements localInstance = (EOElements)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cBap() {
    return (String) storedValueForKey("cBap");
  }

  public void setCBap(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating cBap from " + cBap() + " to " + value);
    }
    takeStoredValueForKey(value, "cBap");
  }

  public String cChevron() {
    return (String) storedValueForKey("cChevron");
  }

  public void setCChevron(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating cChevron from " + cChevron() + " to " + value);
    }
    takeStoredValueForKey(value, "cChevron");
  }

  public String cCorps() {
    return (String) storedValueForKey("cCorps");
  }

  public void setCCorps(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating cCorps from " + cCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "cCorps");
  }

  public String cEchelon() {
    return (String) storedValueForKey("cEchelon");
  }

  public void setCEchelon(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating cEchelon from " + cEchelon() + " to " + value);
    }
    takeStoredValueForKey(value, "cEchelon");
  }

  public String codeemploi() {
    return (String) storedValueForKey("codeemploi");
  }

  public void setCodeemploi(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating codeemploi from " + codeemploi() + " to " + value);
    }
    takeStoredValueForKey(value, "codeemploi");
  }

  public String cSpecialite() {
    return (String) storedValueForKey("cSpecialite");
  }

  public void setCSpecialite(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating cSpecialite from " + cSpecialite() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialite");
  }

  public String cSpecialiteAtos() {
    return (String) storedValueForKey("cSpecialiteAtos");
  }

  public void setCSpecialiteAtos(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating cSpecialiteAtos from " + cSpecialiteAtos() + " to " + value);
    }
    takeStoredValueForKey(value, "cSpecialiteAtos");
  }

  public String cTypeAcces() {
    return (String) storedValueForKey("cTypeAcces");
  }

  public void setCTypeAcces(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating cTypeAcces from " + cTypeAcces() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeAcces");
  }

  public NSTimestamp dArreteAnnulation() {
    return (NSTimestamp) storedValueForKey("dArreteAnnulation");
  }

  public void setDArreteAnnulation(NSTimestamp value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating dArreteAnnulation from " + dArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dArreteAnnulation");
  }

  public NSTimestamp dArreteCarriere() {
    return (NSTimestamp) storedValueForKey("dArreteCarriere");
  }

  public void setDArreteCarriere(NSTimestamp value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating dArreteCarriere from " + dArreteCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "dArreteCarriere");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dEffetElement() {
    return (NSTimestamp) storedValueForKey("dEffetElement");
  }

  public void setDEffetElement(NSTimestamp value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating dEffetElement from " + dEffetElement() + " to " + value);
    }
    takeStoredValueForKey(value, "dEffetElement");
  }

  public NSTimestamp dFinElement() {
    return (NSTimestamp) storedValueForKey("dFinElement");
  }

  public void setDFinElement(NSTimestamp value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating dFinElement from " + dFinElement() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinElement");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer inmEffectif() {
    return (Integer) storedValueForKey("inmEffectif");
  }

  public void setInmEffectif(Integer value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating inmEffectif from " + inmEffectif() + " to " + value);
    }
    takeStoredValueForKey(value, "inmEffectif");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public String noArreteCarriere() {
    return (String) storedValueForKey("noArreteCarriere");
  }

  public void setNoArreteCarriere(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating noArreteCarriere from " + noArreteCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteCarriere");
  }

  public Integer quotiteElement() {
    return (Integer) storedValueForKey("quotiteElement");
  }

  public void setQuotiteElement(Integer value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating quotiteElement from " + quotiteElement() + " to " + value);
    }
    takeStoredValueForKey(value, "quotiteElement");
  }

  public Integer repAncEchAnnees() {
    return (Integer) storedValueForKey("repAncEchAnnees");
  }

  public void setRepAncEchAnnees(Integer value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating repAncEchAnnees from " + repAncEchAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "repAncEchAnnees");
  }

  public Integer repAncEchJours() {
    return (Integer) storedValueForKey("repAncEchJours");
  }

  public void setRepAncEchJours(Integer value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating repAncEchJours from " + repAncEchJours() + " to " + value);
    }
    takeStoredValueForKey(value, "repAncEchJours");
  }

  public Integer repAncEchMois() {
    return (Integer) storedValueForKey("repAncEchMois");
  }

  public void setRepAncEchMois(Integer value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating repAncEchMois from " + repAncEchMois() + " to " + value);
    }
    takeStoredValueForKey(value, "repAncEchMois");
  }

  public String temArreteSigne() {
    return (String) storedValueForKey("temArreteSigne");
  }

  public void setTemArreteSigne(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating temArreteSigne from " + temArreteSigne() + " to " + value);
    }
    takeStoredValueForKey(value, "temArreteSigne");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temProvisoire() {
    return (String) storedValueForKey("temProvisoire");
  }

  public void setTemProvisoire(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating temProvisoire from " + temProvisoire() + " to " + value);
    }
    takeStoredValueForKey(value, "temProvisoire");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOElements.LOG.isDebugEnabled()) {
    	_EOElements.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOCarriere toCarriere() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOCarriere)storedValueForKey("toCarriere");
  }

  public void setToCarriereRelationship(org.cocktail.fwkcktlgrh.common.metier.EOCarriere value) {
    if (_EOElements.LOG.isDebugEnabled()) {
      _EOElements.LOG.debug("updating toCarriere from " + toCarriere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCarriere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCarriere");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCnu toCnu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCnu)storedValueForKey("toCnu");
  }

  public void setToCnuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCnu value) {
    if (_EOElements.LOG.isDebugEnabled()) {
      _EOElements.LOG.debug("updating toCnu from " + toCnu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCnu oldValue = toCnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCnu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCnu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCorps toCorps() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCorps)storedValueForKey("toCorps");
  }

  public void setToCorpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCorps value) {
    if (_EOElements.LOG.isDebugEnabled()) {
      _EOElements.LOG.debug("updating toCorps from " + toCorps() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCorps oldValue = toCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCorps");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCorps");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EODiscSecondDegre toDiscSecondDegre() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EODiscSecondDegre)storedValueForKey("toDiscSecondDegre");
  }

  public void setToDiscSecondDegreRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODiscSecondDegre value) {
    if (_EOElements.LOG.isDebugEnabled()) {
      _EOElements.LOG.debug("updating toDiscSecondDegre from " + toDiscSecondDegre() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EODiscSecondDegre oldValue = toDiscSecondDegre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toDiscSecondDegre");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toDiscSecondDegre");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOGrade toGrade() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOGrade)storedValueForKey("toGrade");
  }

  public void setToGradeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGrade value) {
    if (_EOElements.LOG.isDebugEnabled()) {
      _EOElements.LOG.debug("updating toGrade from " + toGrade() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOGrade oldValue = toGrade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toGrade");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toGrade");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOElements.LOG.isDebugEnabled()) {
      _EOElements.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois toReferensEmplois() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois)storedValueForKey("toReferensEmplois");
  }

  public void setToReferensEmploisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois value) {
    if (_EOElements.LOG.isDebugEnabled()) {
      _EOElements.LOG.debug("updating toReferensEmplois from " + toReferensEmplois() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois oldValue = toReferensEmplois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toReferensEmplois");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toReferensEmplois");
    }
  }
  

  public static EOElements create(EOEditingContext editingContext, String cCorps
, NSTimestamp dCreation
, NSTimestamp dEffetElement
, NSTimestamp dModification
, String temProvisoire
, String temValide
, org.cocktail.fwkcktlgrh.common.metier.EOCarriere toCarriere, org.cocktail.fwkcktlpersonne.common.metier.EOCorps toCorps, org.cocktail.fwkcktlpersonne.common.metier.EOGrade toGrade, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu) {
    EOElements eo = (EOElements) EOUtilities.createAndInsertInstance(editingContext, _EOElements.ENTITY_NAME);    
		eo.setCCorps(cCorps);
		eo.setDCreation(dCreation);
		eo.setDEffetElement(dEffetElement);
		eo.setDModification(dModification);
		eo.setTemProvisoire(temProvisoire);
		eo.setTemValide(temValide);
    eo.setToCarriereRelationship(toCarriere);
    eo.setToCorpsRelationship(toCorps);
    eo.setToGradeRelationship(toGrade);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  public static NSArray<EOElements> fetchAll(EOEditingContext editingContext) {
    return _EOElements.fetchAll(editingContext, null);
  }

  public static NSArray<EOElements> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOElements.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOElements> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOElements.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOElements> eoObjects = (NSArray<EOElements>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOElements fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOElements.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOElements fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOElements> eoObjects = _EOElements.fetch(editingContext, qualifier, null);
    EOElements eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOElements)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_ElementCarriere that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOElements fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOElements.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOElements fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOElements eoObject = _EOElements.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_ElementCarriere that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOElements fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOElements fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOElements eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOElements)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOElements localInstanceIn(EOEditingContext editingContext, EOElements eo) {
    EOElements localInstance = (eo == null) ? null : (EOElements)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
