/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOStructureInfo extends _EOStructureInfo {
  private static Logger log = Logger.getLogger(EOStructureInfo.class);

  public EOStructureInfo() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }

  // methodes rajoutees

  public final static NSArray arraySort = new NSArray(
		  new Object[] {
				  EOSortOrdering.sortOrderingWithKey(EOStructureInfo.TO_STRUCTURE_KEY+"."+EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareAscending),
				  EOSortOrdering.sortOrderingWithKey(EOStructureInfo.SIN_D_DEBUT_KEY, EOSortOrdering.CompareAscending)
		  }
  );


  public String display() {
	  String display = toStructure().display() + " ";
	  if (sinDFin() != null) {
		  display += "du " + DateCtrl.dateToString(sinDDebut()) + " au " + DateCtrl.dateToString(sinDFin());
	  } else {
		  display += " à partir du " + DateCtrl.dateToString(sinDDebut());  
	  }
	  return display;
  }


  public static EOStructureInfo findStructureInfoForStructureAndDateAndTypeInContext
  (EOEditingContext ec, EOStructure structure, NSTimestamp dateRef, Integer type) {
	  EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
			  EOStructureInfo.TO_STRUCTURE_KEY +" = %@ AND "+EOStructureInfo.SIN_TYPE_KEY+" = %@ AND "+ EOStructureInfo.SIN_D_DEBUT_KEY
			  +" <= %@ AND ( "+EOStructureInfo.SIN_D_FIN_KEY+" >= %@ OR "+ EOStructureInfo.SIN_D_FIN_KEY+" = nil)",
			  new NSArray(new Object[] {structure, type, dateRef, dateRef})
	  );
	  NSArray records = GRHUtilities.fetchArray(ec, EOStructureInfo.ENTITY_NAME, qual, null);
	  EOStructureInfo record = null;
	  if (records.count() > 0) {
		  record = (EOStructureInfo) records.lastObject();
	  }
	  return record;
  }

  private static EOStructureInfo newDefaultRecordInContext(EOEditingContext ec) {
	  EOStructureInfo record = new EOStructureInfo();
	  ec.insertObject(record);
	  return record;
  }

  public static EOStructureInfo newRecordInContext(
		  EOEditingContext ec, 
		  EOStructure structure,
		  NSTimestamp debut,
		  NSTimestamp fin,
		  String libelle,
		  Integer type
  ) {
	  EOStructureInfo newRecord = EOStructureInfo.newDefaultRecordInContext(ec);

	  newRecord.addObjectToBothSidesOfRelationshipWithKey(structure,   "toStructure");
	  if (debut == null) {
		  debut = DateCtrl.now();
	  }
	  newRecord.setSinDDebut(debut);      
	  newRecord.setSinDFin(fin);
	  newRecord.setSinType(type);
	  newRecord.setSinLibelle(libelle);

	  return newRecord;
  }


	public static Integer getTYPE_MISSION_COMPOSANTE() {
		return TYPE_MISSION_COMPOSANTE;
	}

	public static void setTYPE_MISSION_COMPOSANTE(Integer tYPE_MISSION_COMPOSANTE) {
		TYPE_MISSION_COMPOSANTE = tYPE_MISSION_COMPOSANTE;
	}

	public static Integer getTYPE_MISSION_SERVICE() {
		return TYPE_MISSION_SERVICE;
	}

	public static void setTYPE_MISSION_SERVICE(Integer tYPE_MISSION_SERVICE) {
		TYPE_MISSION_SERVICE = tYPE_MISSION_SERVICE;
	}

	public static Integer getTYPE_PROJET_SERVICE() {
		return TYPE_PROJET_SERVICE;
	}

	public static void setTYPE_PROJET_SERVICE(Integer tYPE_PROJET_SERVICE) {
		TYPE_PROJET_SERVICE = tYPE_PROJET_SERVICE;
	}

	private static Integer TYPE_MISSION_COMPOSANTE = new Integer(1);
	private static Integer TYPE_MISSION_SERVICE = new Integer(2);
	private static Integer TYPE_PROJET_SERVICE = new Integer(3);
  
  
}
