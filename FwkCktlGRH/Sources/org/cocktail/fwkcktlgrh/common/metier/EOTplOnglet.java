/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.ITplOnglet;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOTplOnglet extends _EOTplOnglet implements ITplOnglet {
  private static Logger log = Logger.getLogger(EOTplOnglet.class);

	// les codes des onglets (ne sont pas encore completement gérés via les tables
	// de template)
	private static final String TPL_ONGLET_EVA_AGENT_CODE = "EVAAGENT";
	private static final String TPL_ONGLET_EVA_OBJ_PRECEDENT_CODE = "EVAOBJPREC";
	private static final String TPL_ONGLET_EVA_OBJ_SUIVANT_CODE = "EVAOBJSUIV";
	public static final String SITUATION_ACTIVITE_CODE = "EVASITU";
	private static final String TPL_ONGLET_EVA_COMPETENCE_CODE = "EVACOMP";
	public static final String TPL_ONGLET_EVA_APTITUDE_CODE = "EVAAPTI";
	private static final String TPL_ONGLET_EVA_EVOLUTION_CODE = "EVAEVOL";
	private static final String TPL_ONGLET_EVA_FIN_CODE = "EVAFIN";
	private static final String TPL_ONGLET_EVA_NOTICE_PROMOTION = "NOTICEPROM";  
	
	private static final String TPL_ONGLET_EVA_APPRECIATION_GENERALE = "SYNTVALPRO"; 
  
  public EOTplOnglet() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }
  
	// methodes rajoutees

	/**
	 * Surcharge du lien vers tosTplBloc pour avoir un classement selon le temoin
	 * <code>tblPosition</code>
	 */
	public NSArray<EOTplBloc> tosTplBlocSortedByPosition(
				EOEvaluationPeriode eoPeriode) {

		//
		EOQualifier qual = getValiditeQualifier(
					EOTplBloc.D_DEB_VAL_KEY, EOTplBloc.D_FIN_VAL_KEY, eoPeriode.epeDDebut(), eoPeriode.epeDFin());

		NSArray<EOTplBloc> tosTplBloc = tosTplBloc(qual);

		tosTplBloc = CktlSort.sortedArray(tosTplBloc, EOTplBloc.TBL_POSITION_KEY);

		return tosTplBloc;
	}

	/**
	 * Retourner l'enregistrement <code>EOTplOnglet</code> associe a un libelle
	 * <code>ongletLabel</code>
	 * 
	 * @param ec
	 * @param ongletLabel
	 * @return
	 */
	public static EOTplOnglet ongletForLabelInArray(NSArray<EOTplOnglet> eoTplOngletArray, String ongletLabel) {
		EOTplOnglet result = null;

		EOQualifier qual = CktlDataBus.newCondition(
					EOTplOnglet.TON_LIBELLE_KEY + "='" + ongletLabel + "'");

		NSArray<EOTplOnglet> array = EOQualifier.filteredArrayWithQualifier(eoTplOngletArray, qual);
		if (array.count() > 0) {
			result = array.objectAtIndex(0);
		}

		return result;
	}

	// nature des onglets

	public boolean isAgent() {
		return tonCode().equals(TPL_ONGLET_EVA_AGENT_CODE);
	}

	public boolean isObjectifsPrecedents() {
		return tonCode().equals(TPL_ONGLET_EVA_OBJ_PRECEDENT_CODE);
	}

	public boolean isObjectifsSuivants() {
		return tonCode().equals(TPL_ONGLET_EVA_OBJ_SUIVANT_CODE);
	}

	public boolean isSituation() {
		return tonCode().equals(SITUATION_ACTIVITE_CODE);
	}

	public boolean isCompetence() {
		return tonCode().equals(TPL_ONGLET_EVA_COMPETENCE_CODE);
	}

	public boolean isEvolution() {
		return tonCode().equals(TPL_ONGLET_EVA_EVOLUTION_CODE);
	}

	public boolean isFin() {
		return tonCode().equals(TPL_ONGLET_EVA_FIN_CODE);
	}
	
	public boolean isAppreciatonGenerale() {
		return tonCode().equals(TPL_ONGLET_EVA_APPRECIATION_GENERALE);
	}
}
