package org.cocktail.fwkcktlgrh.common.metier.workflow;
import org.cocktail.fwkcktlgrh.common.metier.EOContratHeberge;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.workflow.IWorkflow;
import org.cocktail.fwkcktlpersonne.common.metier.workflow.IWorkflowItem;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSForwardException;

import er.extensions.eof.ERXEC;


/**
 * Implémentation du workflow de validation d'un hébergé.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class WorkflowHeberge implements IWorkflow {

    private EOStructure groupesValides;
    private EOStructure groupesEnCours;
    private EOStructure groupesAnnules;
    private EOEditingContext ec;
    
    private WorkflowHeberge() {
        try {
            this.ec = ERXEC.newEditingContext();
            groupesValides = EOStructureForGroupeSpec.getGroupeForParamKey(ec, "ANNUAIRE_HEBERGE_VALIDE");
            groupesEnCours = EOStructureForGroupeSpec.getGroupeForParamKey(ec, "ANNUAIRE_HEBERGE_ENCOURS_VALIDE");
            groupesAnnules = EOStructureForGroupeSpec.getGroupeForParamKey(ec, "ANNUAIRE_HEBERGE_ARCHIVES");
        } catch (Exception e) {
            throw new NSForwardException(e, "Certains groupes ne sont pas parametres pour le workflow");
        }
    }
    
    public static WorkflowHeberge instance() {
        return new WorkflowHeberge();
    }
    
    public boolean estAnnule(IWorkflowItem item) {
        return contratHeberge(item).estAnnule();
    }

    public boolean estEnCoursDeValidation(IWorkflowItem item) {
        return contratHeberge(item).estEnCoursDeValidation();
    }

    public boolean estValide(IWorkflowItem item) {
        return contratHeberge(item).estValide();
    }

    public void annuler(IWorkflowItem item, Integer persIdModificateur) {
       
    }
    
    public void passerEnCoursDeValidation(IWorkflowItem item, Integer persIdModificateur) {
        EOContratHeberge contrat = contratHeberge(item);
        EOEditingContext ec = contrat.editingContext();
        // On met l'état à archivé
        contrat.setEtat(EOContratHeberge.TEM_VALIDATION_EN_COURS);
        EOIndividu individu = contrat.toPersonnel().toIndividu();
        // On change de groupe la personne, on met dans le groupe des annulés
        individu.supprimerAffectationAUnGroupe(ec, individu.persId(), groupesAnnules);
        individu.supprimerAffectationAUnGroupe(ec, individu.persId(), groupesValides);
        EOStructureForGroupeSpec.sharedInstance().affecterPersonneDansGroupeForce(
                ec, individu, groupesEnCours.localInstanceIn(ec), persIdModificateur);
    }

    public void valider(IWorkflowItem item, Integer persIdModificateur) {

    }

    private EOContratHeberge contratHeberge(IWorkflowItem item) {
        return (EOContratHeberge)item;
    }
    
}
