/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOTempsPartiel.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOTempsPartiel extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_TempsPartiel";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEN_QUOTITE_KEY = "denQuotite";
	public static final String D_FIN_EXECUTION_KEY = "dFinExecution";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_ARRETE_ANNULATION_KEY = "noArreteAnnulation";
	public static final String NUM_QUOTITE_KEY = "numQuotite";
	public static final String PERIODICITE_KEY = "periodicite";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_REPRISE_TEMPS_PLEIN_KEY = "temRepriseTempsPlein";
	public static final String TEM_SURCOTISATION_KEY = "temSurcotisation";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
	public static final ERXKey<NSTimestamp> D_ANNULATION = new ERXKey<NSTimestamp>("dAnnulation");
	public static final ERXKey<NSTimestamp> DATE_ARRETE = new ERXKey<NSTimestamp>("dateArrete");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<Integer> DEN_QUOTITE = new ERXKey<Integer>("denQuotite");
	public static final ERXKey<NSTimestamp> D_FIN_EXECUTION = new ERXKey<NSTimestamp>("dFinExecution");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> NO_ARRETE = new ERXKey<String>("noArrete");
	public static final ERXKey<String> NO_ARRETE_ANNULATION = new ERXKey<String>("noArreteAnnulation");
	public static final ERXKey<Integer> NUM_QUOTITE = new ERXKey<Integer>("numQuotite");
	public static final ERXKey<Integer> PERIODICITE = new ERXKey<Integer>("periodicite");
	public static final ERXKey<String> TEM_CONFIRME = new ERXKey<String>("temConfirme");
	public static final ERXKey<String> TEM_GEST_ETAB = new ERXKey<String>("temGestEtab");
	public static final ERXKey<String> TEM_REPRISE_TEMPS_PLEIN = new ERXKey<String>("temRepriseTempsPlein");
	public static final ERXKey<String> TEM_SURCOTISATION = new ERXKey<String>("temSurcotisation");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_ABSENCE_KEY = "toAbsence";
	public static final String TO_ENFANT_KEY = "toEnfant";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_MOTIF_TEMPS_PARTIEL_KEY = "toMotifTempsPartiel";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAbsence> TO_ABSENCE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAbsence>("toAbsence");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant> TO_ENFANT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEnfant>("toEnfant");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel> TO_MOTIF_TEMPS_PARTIEL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel>("toMotifTempsPartiel");

  private static Logger LOG = Logger.getLogger(_EOTempsPartiel.class);

  public EOTempsPartiel localInstanceIn(EOEditingContext editingContext) {
    EOTempsPartiel localInstance = (EOTempsPartiel)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public Integer denQuotite() {
    return (Integer) storedValueForKey("denQuotite");
  }

  public void setDenQuotite(Integer value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating denQuotite from " + denQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "denQuotite");
  }

  public NSTimestamp dFinExecution() {
    return (NSTimestamp) storedValueForKey("dFinExecution");
  }

  public void setDFinExecution(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dFinExecution from " + dFinExecution() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinExecution");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noArreteAnnulation() {
    return (String) storedValueForKey("noArreteAnnulation");
  }

  public void setNoArreteAnnulation(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating noArreteAnnulation from " + noArreteAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "noArreteAnnulation");
  }

  public Integer numQuotite() {
    return (Integer) storedValueForKey("numQuotite");
  }

  public void setNumQuotite(Integer value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating numQuotite from " + numQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotite");
  }

  public Integer periodicite() {
    return (Integer) storedValueForKey("periodicite");
  }

  public void setPeriodicite(Integer value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating periodicite from " + periodicite() + " to " + value);
    }
    takeStoredValueForKey(value, "periodicite");
  }

  public String temConfirme() {
    return (String) storedValueForKey("temConfirme");
  }

  public void setTemConfirme(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating temConfirme from " + temConfirme() + " to " + value);
    }
    takeStoredValueForKey(value, "temConfirme");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temRepriseTempsPlein() {
    return (String) storedValueForKey("temRepriseTempsPlein");
  }

  public void setTemRepriseTempsPlein(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating temRepriseTempsPlein from " + temRepriseTempsPlein() + " to " + value);
    }
    takeStoredValueForKey(value, "temRepriseTempsPlein");
  }

  public String temSurcotisation() {
    return (String) storedValueForKey("temSurcotisation");
  }

  public void setTemSurcotisation(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating temSurcotisation from " + temSurcotisation() + " to " + value);
    }
    takeStoredValueForKey(value, "temSurcotisation");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
    	_EOTempsPartiel.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOAbsence toAbsence() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOAbsence)storedValueForKey("toAbsence");
  }

  public void setToAbsenceRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAbsence value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
      _EOTempsPartiel.LOG.debug("updating toAbsence from " + toAbsence() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOAbsence oldValue = toAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAbsence");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toAbsence");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOEnfant toEnfant() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEnfant)storedValueForKey("toEnfant");
  }

  public void setToEnfantRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEnfant value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
      _EOTempsPartiel.LOG.debug("updating toEnfant from " + toEnfant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOEnfant oldValue = toEnfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEnfant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEnfant");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
      _EOTempsPartiel.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel toMotifTempsPartiel() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel)storedValueForKey("toMotifTempsPartiel");
  }

  public void setToMotifTempsPartielRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel value) {
    if (_EOTempsPartiel.LOG.isDebugEnabled()) {
      _EOTempsPartiel.LOG.debug("updating toMotifTempsPartiel from " + toMotifTempsPartiel() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel oldValue = toMotifTempsPartiel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMotifTempsPartiel");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMotifTempsPartiel");
    }
  }
  

  public static EOTempsPartiel create(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, Integer denQuotite
, NSTimestamp dModification
, Integer numQuotite
, Integer periodicite
, String temConfirme
, String temGestEtab
, String temRepriseTempsPlein
, String temSurcotisation
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel toMotifTempsPartiel) {
    EOTempsPartiel eo = (EOTempsPartiel) EOUtilities.createAndInsertInstance(editingContext, _EOTempsPartiel.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDenQuotite(denQuotite);
		eo.setDModification(dModification);
		eo.setNumQuotite(numQuotite);
		eo.setPeriodicite(periodicite);
		eo.setTemConfirme(temConfirme);
		eo.setTemGestEtab(temGestEtab);
		eo.setTemRepriseTempsPlein(temRepriseTempsPlein);
		eo.setTemSurcotisation(temSurcotisation);
		eo.setTemValide(temValide);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToMotifTempsPartielRelationship(toMotifTempsPartiel);
    return eo;
  }

  public static NSArray<EOTempsPartiel> fetchAll(EOEditingContext editingContext) {
    return _EOTempsPartiel.fetchAll(editingContext, null);
  }

  public static NSArray<EOTempsPartiel> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTempsPartiel.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOTempsPartiel> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTempsPartiel.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTempsPartiel> eoObjects = (NSArray<EOTempsPartiel>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTempsPartiel fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTempsPartiel.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTempsPartiel fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTempsPartiel> eoObjects = _EOTempsPartiel.fetch(editingContext, qualifier, null);
    EOTempsPartiel eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTempsPartiel)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_TempsPartiel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTempsPartiel fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTempsPartiel.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOTempsPartiel fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTempsPartiel eoObject = _EOTempsPartiel.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_TempsPartiel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOTempsPartiel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTempsPartiel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTempsPartiel eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTempsPartiel)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOTempsPartiel localInstanceIn(EOEditingContext editingContext, EOTempsPartiel eo) {
    EOTempsPartiel localInstance = (eo == null) ? null : (EOTempsPartiel)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
