/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividuGrhService;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOTplFiche extends _EOTplFiche {
  private static Logger log = Logger.getLogger(EOTplFiche.class);

  public EOTplFiche() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }
  
	// ajouts

	// les noms des templates des fiches utilisees
	public static final String TPL_FICHE_EVALUATION_CODE = "EVALUATION";

	/**
	 * La liste des onglets contenus dans la période, selon l'evaluation (selon
	 * l'agent des agents peuvent êtres en plus ou en moins)
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public NSArray<EOTplOnglet> tosOnglet(
			EOEvaluationPeriode eoPeriode, EOEvaluation eoEvaluation) {

		IndividuGrhService individuGrhService = new IndividuGrhService();
		
		//
		EOQualifier qual = getValiditeQualifier(
				EOTplOnglet.D_DEB_VAL_KEY, EOTplOnglet.D_FIN_VAL_KEY, eoPeriode.epeDDebut(), eoPeriode.epeDFin());

		NSArray<EOTplOnglet> result = tosTplOnglet(qual);

		// traitement si l'évaluation est précisée : on affiche les onglets selon le
		// statut de l'agent
		if (eoEvaluation != null) {

			NSTimestamp dDebut = eoEvaluation.toEvaluationPeriode().epeDDebut();
			NSTimestamp dFin = eoEvaluation.toEvaluationPeriode().epeDFin();

			EOQualifier qualStatut = null;
			
			if (individuGrhService.isFonctionnaire(eoEvaluation.toIndividu(), eoEvaluation.editingContext(), dDebut, dFin) 
					|| individuGrhService.isStagiaire(eoEvaluation.toIndividu(), eoEvaluation.editingContext(), dFin)) {
				qualStatut = ERXQ.equals(
						EOTplOnglet.TEM_TITULAIRE_KEY, OUI);
			} else if (individuGrhService.isContractuel(eoEvaluation.toIndividu(), eoEvaluation.editingContext(), dDebut, dFin)) {
				qualStatut = ERXQ.equals(
						EOTplOnglet.TEM_CONTRACTUEL_KEY, OUI);
			}

			if (qualStatut != null) {
				result = EOQualifier.filteredArrayWithQualifier(result, qualStatut);
			}

		}

		result = CktlSort.sortedArray(result, EOTplOnglet.TON_POSITION_KEY);

		return result;
	}

	/**
	 * A voir si cela peut servir plus tard : afficher les onglet masqués mais
	 * pour lesquels il y a des données de saisies (par exemple, lorqu'on décidé
	 * de ne plus l'afficher à un fonctionnaire en cours d'année)
	 * 
	 * @param eoPeriode
	 * @param eoEvaluation
	 * @return
	 */
	private NSArray<EOTplOnglet> tosOngletHistorique(
			EOEvaluationPeriode eoPeriode, EOEvaluation eoEvaluation) {

		// gestion de l'historique pour les onglets dynamiques : on affiche les
		// onglets pour lesquels il y a des données (pour avoir l'affichage suite
		// à un masquage d'onglet postérieur)
		// TODO voir pour créer une table d'affiche qui contiendrait les dates de
		// validité et les statuts afin d'autoriser la gestion des onglets non
		// dynamiques
		// liste des onglets à rajouter

		NSMutableArray<EOTplOnglet> arrayOngletSupp = new NSMutableArray<EOTplOnglet>();
		boolean isOngletSuivantATraiter = false;
		// TODO mettre ce tableau en cache
		NSArray<EOTplOnglet> arrayOnglet = tosOnglet(eoPeriode, null);
		for (int i = 0; i < arrayOnglet.count(); i++) {
			isOngletSuivantATraiter = false;
			EOTplOnglet eoOnglet = arrayOnglet.objectAtIndex(i);
			NSArray<EOTplBloc> arrayBloc = eoOnglet.tosTplBlocSortedByPosition(eoPeriode);
			for (int j = 0; j < arrayBloc.count() && !isOngletSuivantATraiter; j++) {
				EOTplBloc eoBloc = arrayBloc.objectAtIndex(j);
				NSArray<EOTplItem> arrayItem = eoBloc.tosTplItemSorted();
				for (int k = 0; k < arrayItem.count() && !isOngletSuivantATraiter; k++) {
					EOTplItem eoItem = arrayItem.objectAtIndex(k);
					if (eoItem.isListe()) {
						EORepartFicheItem eoRepartFicheItem = eoItem.getRepartItemForEvaluation(eoEvaluation);
						if (eoRepartFicheItem != null && eoRepartFicheItem.toTplItemValeur() != null) {
							isOngletSuivantATraiter = true;
							arrayOngletSupp.addObject(eoOnglet);
						}
					} else if (eoItem.isChampLibre()) {
						if (!StringCtrl.isEmpty(eoItem.getStrChampLibre(eoEvaluation))) {
							isOngletSuivantATraiter = true;
							arrayOngletSupp.addObject(eoOnglet);
						}
					}
				}
			}
		}

		return arrayOngletSupp;
	}
}
