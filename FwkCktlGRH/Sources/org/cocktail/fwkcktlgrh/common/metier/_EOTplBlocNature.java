/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOTplBlocNature.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOTplBlocNature extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_TplBlocNature";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TBN_LIBELLE_KEY = "tbnLibelle";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> TBN_LIBELLE = new ERXKey<String>("tbnLibelle");
	// Relationships


  private static Logger LOG = Logger.getLogger(_EOTplBlocNature.class);

  public EOTplBlocNature localInstanceIn(EOEditingContext editingContext) {
    EOTplBlocNature localInstance = (EOTplBlocNature)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTplBlocNature.LOG.isDebugEnabled()) {
    	_EOTplBlocNature.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTplBlocNature.LOG.isDebugEnabled()) {
    	_EOTplBlocNature.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String tbnLibelle() {
    return (String) storedValueForKey("tbnLibelle");
  }

  public void setTbnLibelle(String value) {
    if (_EOTplBlocNature.LOG.isDebugEnabled()) {
    	_EOTplBlocNature.LOG.debug( "updating tbnLibelle from " + tbnLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "tbnLibelle");
  }


  public static EOTplBlocNature create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String tbnLibelle
) {
    EOTplBlocNature eo = (EOTplBlocNature) EOUtilities.createAndInsertInstance(editingContext, _EOTplBlocNature.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTbnLibelle(tbnLibelle);
    return eo;
  }

  public static NSArray<EOTplBlocNature> fetchAll(EOEditingContext editingContext) {
    return _EOTplBlocNature.fetchAll(editingContext, null);
  }

  public static NSArray<EOTplBlocNature> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTplBlocNature.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOTplBlocNature> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTplBlocNature.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTplBlocNature> eoObjects = (NSArray<EOTplBlocNature>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTplBlocNature fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTplBlocNature.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTplBlocNature fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTplBlocNature> eoObjects = _EOTplBlocNature.fetch(editingContext, qualifier, null);
    EOTplBlocNature eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTplBlocNature)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_TplBlocNature that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTplBlocNature fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTplBlocNature.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOTplBlocNature fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTplBlocNature eoObject = _EOTplBlocNature.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_TplBlocNature that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOTplBlocNature fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTplBlocNature fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTplBlocNature eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTplBlocNature)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOTplBlocNature localInstanceIn(EOEditingContext editingContext, EOTplBlocNature eo) {
    EOTplBlocNature localInstance = (eo == null) ? null : (EOTplBlocNature)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
