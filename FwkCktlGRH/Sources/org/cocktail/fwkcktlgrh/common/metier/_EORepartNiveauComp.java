/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartNiveauComp.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartNiveauComp extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_RepartNiveauComp";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	// Relationships
	public static final String TO_EVALUATION_KEY = "toEvaluation";
	public static final String TO_NIVEAU_COMPETENCE_KEY = "toNiveauCompetence";
	public static final String TO_REPART_FDP_AUTRE_KEY = "toRepartFdpAutre";
	public static final String TO_REPART_FDP_COMP_KEY = "toRepartFdpComp";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> TO_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>("toEvaluation");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONiveauCompetence> TO_NIVEAU_COMPETENCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONiveauCompetence>("toNiveauCompetence");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre> TO_REPART_FDP_AUTRE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre>("toRepartFdpAutre");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp> TO_REPART_FDP_COMP = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp>("toRepartFdpComp");

  private static Logger LOG = Logger.getLogger(_EORepartNiveauComp.class);

  public EORepartNiveauComp localInstanceIn(EOEditingContext editingContext) {
    EORepartNiveauComp localInstance = (EORepartNiveauComp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartNiveauComp.LOG.isDebugEnabled()) {
    	_EORepartNiveauComp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartNiveauComp.LOG.isDebugEnabled()) {
    	_EORepartNiveauComp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluation)storedValueForKey("toEvaluation");
  }

  public void setToEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation value) {
    if (_EORepartNiveauComp.LOG.isDebugEnabled()) {
      _EORepartNiveauComp.LOG.debug("updating toEvaluation from " + toEvaluation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluation oldValue = toEvaluation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluation");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EONiveauCompetence toNiveauCompetence() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EONiveauCompetence)storedValueForKey("toNiveauCompetence");
  }

  public void setToNiveauCompetenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EONiveauCompetence value) {
    if (_EORepartNiveauComp.LOG.isDebugEnabled()) {
      _EORepartNiveauComp.LOG.debug("updating toNiveauCompetence from " + toNiveauCompetence() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EONiveauCompetence oldValue = toNiveauCompetence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toNiveauCompetence");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toNiveauCompetence");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre toRepartFdpAutre() {
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre)storedValueForKey("toRepartFdpAutre");
  }

  public void setToRepartFdpAutreRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre value) {
    if (_EORepartNiveauComp.LOG.isDebugEnabled()) {
      _EORepartNiveauComp.LOG.debug("updating toRepartFdpAutre from " + toRepartFdpAutre() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre oldValue = toRepartFdpAutre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRepartFdpAutre");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toRepartFdpAutre");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp toRepartFdpComp() {
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp)storedValueForKey("toRepartFdpComp");
  }

  public void setToRepartFdpCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp value) {
    if (_EORepartNiveauComp.LOG.isDebugEnabled()) {
      _EORepartNiveauComp.LOG.debug("updating toRepartFdpComp from " + toRepartFdpComp() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp oldValue = toRepartFdpComp();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRepartFdpComp");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toRepartFdpComp");
    }
  }
  

  public static EORepartNiveauComp create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation) {
    EORepartNiveauComp eo = (EORepartNiveauComp) EOUtilities.createAndInsertInstance(editingContext, _EORepartNiveauComp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToEvaluationRelationship(toEvaluation);
    return eo;
  }

  public static NSArray<EORepartNiveauComp> fetchAll(EOEditingContext editingContext) {
    return _EORepartNiveauComp.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartNiveauComp> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartNiveauComp.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartNiveauComp> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartNiveauComp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartNiveauComp> eoObjects = (NSArray<EORepartNiveauComp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartNiveauComp fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartNiveauComp.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartNiveauComp fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartNiveauComp> eoObjects = _EORepartNiveauComp.fetch(editingContext, qualifier, null);
    EORepartNiveauComp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartNiveauComp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartNiveauComp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartNiveauComp fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartNiveauComp.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartNiveauComp fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartNiveauComp eoObject = _EORepartNiveauComp.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartNiveauComp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartNiveauComp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartNiveauComp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartNiveauComp eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartNiveauComp)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartNiveauComp localInstanceIn(EOEditingContext editingContext, EORepartNiveauComp eo) {
    EORepartNiveauComp localInstance = (eo == null) ? null : (EORepartNiveauComp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
