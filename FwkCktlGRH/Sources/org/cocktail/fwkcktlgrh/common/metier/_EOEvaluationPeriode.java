/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOEvaluationPeriode.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOEvaluationPeriode extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_EvaluationPeriode";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EPE_D_DEBUT_KEY = "epeDDebut";
	public static final String EPE_D_FIN_KEY = "epeDFin";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<NSTimestamp> EPE_D_DEBUT = new ERXKey<NSTimestamp>("epeDDebut");
	public static final ERXKey<NSTimestamp> EPE_D_FIN = new ERXKey<NSTimestamp>("epeDFin");
	// Relationships
	public static final String TOS_EVALUATION_KEY = "tosEvaluation";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> TOS_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>("tosEvaluation");

  private static Logger LOG = Logger.getLogger(_EOEvaluationPeriode.class);

  public EOEvaluationPeriode localInstanceIn(EOEditingContext editingContext) {
    EOEvaluationPeriode localInstance = (EOEvaluationPeriode)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOEvaluationPeriode.LOG.isDebugEnabled()) {
    	_EOEvaluationPeriode.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOEvaluationPeriode.LOG.isDebugEnabled()) {
    	_EOEvaluationPeriode.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp epeDDebut() {
    return (NSTimestamp) storedValueForKey("epeDDebut");
  }

  public void setEpeDDebut(NSTimestamp value) {
    if (_EOEvaluationPeriode.LOG.isDebugEnabled()) {
    	_EOEvaluationPeriode.LOG.debug( "updating epeDDebut from " + epeDDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "epeDDebut");
  }

  public NSTimestamp epeDFin() {
    return (NSTimestamp) storedValueForKey("epeDFin");
  }

  public void setEpeDFin(NSTimestamp value) {
    if (_EOEvaluationPeriode.LOG.isDebugEnabled()) {
    	_EOEvaluationPeriode.LOG.debug( "updating epeDFin from " + epeDFin() + " to " + value);
    }
    takeStoredValueForKey(value, "epeDFin");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> tosEvaluation() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>)storedValueForKey("tosEvaluation");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> tosEvaluation(EOQualifier qualifier) {
    return tosEvaluation(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> tosEvaluation(EOQualifier qualifier, boolean fetch) {
    return tosEvaluation(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> tosEvaluation(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation.TO_EVALUATION_PERIODE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOEvaluation.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosEvaluation();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation object) {
    if (_EOEvaluationPeriode.LOG.isDebugEnabled()) {
      _EOEvaluationPeriode.LOG.debug("adding " + object + " to tosEvaluation relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosEvaluation");
  }

  public void removeFromTosEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation object) {
    if (_EOEvaluationPeriode.LOG.isDebugEnabled()) {
      _EOEvaluationPeriode.LOG.debug("removing " + object + " from tosEvaluation relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosEvaluation");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluation createTosEvaluationRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_Evaluation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosEvaluation");
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluation) eo;
  }

  public void deleteTosEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosEvaluation");
  }

  public void deleteAllTosEvaluationRelationships() {
    Enumeration objects = tosEvaluation().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosEvaluationRelationship((org.cocktail.fwkcktlgrh.common.metier.EOEvaluation)objects.nextElement());
    }
  }


  public static EOEvaluationPeriode create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, NSTimestamp epeDDebut
, NSTimestamp epeDFin
) {
    EOEvaluationPeriode eo = (EOEvaluationPeriode) EOUtilities.createAndInsertInstance(editingContext, _EOEvaluationPeriode.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setEpeDDebut(epeDDebut);
		eo.setEpeDFin(epeDFin);
    return eo;
  }

  public static NSArray<EOEvaluationPeriode> fetchAll(EOEditingContext editingContext) {
    return _EOEvaluationPeriode.fetchAll(editingContext, null);
  }

  public static NSArray<EOEvaluationPeriode> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEvaluationPeriode.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOEvaluationPeriode> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOEvaluationPeriode.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEvaluationPeriode> eoObjects = (NSArray<EOEvaluationPeriode>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOEvaluationPeriode fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEvaluationPeriode.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEvaluationPeriode fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEvaluationPeriode> eoObjects = _EOEvaluationPeriode.fetch(editingContext, qualifier, null);
    EOEvaluationPeriode eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOEvaluationPeriode)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_EvaluationPeriode that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEvaluationPeriode fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEvaluationPeriode.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOEvaluationPeriode fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEvaluationPeriode eoObject = _EOEvaluationPeriode.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_EvaluationPeriode that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOEvaluationPeriode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEvaluationPeriode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEvaluationPeriode eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEvaluationPeriode)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOEvaluationPeriode localInstanceIn(EOEditingContext editingContext, EOEvaluationPeriode eo) {
    EOEvaluationPeriode localInstance = (eo == null) ? null : (EOEvaluationPeriode)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
