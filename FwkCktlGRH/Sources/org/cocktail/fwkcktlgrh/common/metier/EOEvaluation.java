/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.services.FinderFeveService;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividuGrhService;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOEvaluation extends _EOEvaluation implements IEvaluation {
  private static Logger log = Logger.getLogger(EOEvaluation.class);

  public EOEvaluation() {
      super();
  }
  
  public static EOEvaluation createEOEvaluation(EOEditingContext editingContext, NSTimestamp dCreation
		  , NSTimestamp dModification , Integer noIndividuVisible) {
	  return create(editingContext, dCreation, dModification, noIndividuVisible);
  }
  
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }

  
	// methodes rajoutees
  
  

	// methodes rajoutees

	/**
 * 
 */
	public boolean isViseParResponsableRh() {
		boolean isViseParResponsableRh = false;

		if (dVisaResponsableRh() != null) {
			isViseParResponsableRh = true;
		}

		return isViseParResponsableRh;
	}

	/**
 * 
 */
	public void setIsViseParResponsableRh(boolean value) {
		if (value == true) {
			setDVisaResponsableRh(DateCtrl.now());
		} else {
			setDVisaResponsableRh(null);
		}
	}

	/**
	 * @deprecated
	 * @see #tosLastRepartFdpComp() les repart associees aux competences des
	 *      fiches de poste utilises pour l'evaluation des competences
	 */
	public NSArray tosRepartFdpComp() {
		return NSArrayCtrl.flattenArray((NSArray) tosFicheDePoste().valueForKey("tosRepartFdpComp"));
	}

	/**
	 * les repart associees aux competences des fiches de poste utilises pour
	 * l'evaluation des competences
	 */
	public NSArray tosLastRepartFdpComp() {
		return NSArrayCtrl.flattenArray((NSArray) tosLastFicheDePoste().valueForKey(EOFicheDePoste.TOS_REPART_FDP_COMP_KEY));
	}

	public String display() {
		return toIndividu().display() + " du " + DateCtrl.dateToString(evaDDebut()) + " au " + DateCtrl.dateToString(evaDFin());
	}

	/**
	 * @deprecated
	 * @see #tosLastFicheDePoste() les fiches de poste sur lesquelles portent
	 *      cette evaluation -> filtrage par rapport aux dates de l'evaluation
	 */
	public NSArray tosFicheDePoste() {
		IndividuGrhService individuService = new IndividuGrhService();
		return individuService.tosFicheDePostePourDate(toIndividu(), this.editingContext(), evaDDebut(), evaDFin());
	}

	public final static NSArray ARRAY_SORT_INDIVIDU = new NSArray(
			new EOSortOrdering[] {
					EOSortOrdering.sortOrderingWithKey("toIndividu.nomUsuel", EOSortOrdering.CompareAscending),
					EOSortOrdering.sortOrderingWithKey("toIndividu.prenom", EOSortOrdering.CompareAscending)
			}
			);

	// deplacement de la date vers la periode

	/**
	 * @deprecated utiliser la periode
	 * @see #toEvaluationPeriode()
	 */
	public NSTimestamp evaDDebut() {
		// return (NSTimestamp) storedValueForKey("evaDDebut");
		return toEvaluationPeriode().epeDDebut();
	}

	/**
	 * @deprecated utiliser la periode
	 * @see #toEvaluationPeriode()
	 */
	public NSTimestamp evaDFin() {
		// return (NSTimestamp) storedValueForKey("evaDFin");
		return toEvaluationPeriode().epeDFin();
	}

	/**
	 * Cas particulier d'un enregistrement "tout frais", cette valeur est a null
	 * ... on prend donc la valeur sur la to-one toIndividu
	 * 
	 * @return
	 */
	public Integer noIndividuVisible() {
		Integer noIndividuVisible = super.noIndividuVisible();
		if (noIndividuVisible == null) {
			if (toIndividu() == null) {
				noIndividuVisible = null;
			} else {
				noIndividuVisible = new Integer(toIndividu().noIndividu().intValue());
			}
		}
		return noIndividuVisible;
	}

	/**
	 * les fiches de poste (les dernieres) sur lesquelles portent cette evaluation
	 * -> filtrage par rapport aux dates de l'evaluation
	 */
	public NSArray tosLastFicheDePoste() {
		NSArray recsFicheDePoste = new NSArray();
		IndividuGrhService individuService = new IndividuGrhService();
		NSArray records = individuService.tosFicheDePostePourDate(toIndividu(), this.editingContext(), evaDDebut(), evaDFin());
		// classement chronologique
		records = CktlSort.sortedArray(records, EOFicheDePoste.FDP_D_DEBUT_KEY);
		NSArray recsPoste = NSArrayCtrl.removeDuplicate((NSArray) records.valueForKey(EOFicheDePoste.TO_POSTE_KEY));

		
		// ne conserver que les dernieres fiches en date
		for (int i = 0; i < recsPoste.count(); i++) {
			EOPoste poste = (EOPoste) recsPoste.objectAtIndex(i);
			NSArray recs = EOQualifier.filteredArrayWithQualifier(
					records, CktlDataBus.newCondition(EOFicheDePoste.TO_POSTE_KEY + "=%@", new NSArray(poste)));
			// on ne conserve que le dernier
			if (recs.count() > 0) {
				recsFicheDePoste = recsFicheDePoste.arrayByAddingObject(recs.lastObject());
			}
		}

		// ne conserver que celles dont l'occupation du poste est la deniere
		NSArray result = new NSArray();

		NSArray recsAffDet = NSArrayCtrl.flattenArray((NSArray) recsFicheDePoste.valueForKeyPath(EOFicheDePoste.TO_POSTE_KEY + "." + EOPoste.TOS_AFFECTATION_DETAIL_KEY));

		// d'abord celle non fermees sur la periode
		NSArray recsAffDetNonFermees = new NSArray();
		for (int i = 0; i < recsAffDet.count(); i++) {
			EOAffectationDetail recAffDet = (EOAffectationDetail) recsAffDet.objectAtIndex(i);
			if (recAffDet.toAffectation().toIndividu().noIndividu().intValue() == noIndividuVisible().intValue() && (
					recAffDet.dFin() == null || DateCtrl.isAfterEq(recAffDet.dFin(), evaDFin()))) {
				recsAffDetNonFermees = recsAffDetNonFermees.arrayByAddingObject(recAffDet);
			}
		}


		// ce eoqualifier marche pas ???
		/*
		 * EOQualifier.filteredArrayWithQualifier( recsAffDet,
		 * CktlDataBus.newCondition( EOAffectationDetail.D_FIN_AFFECTATION_DETAIL +
		 * "=%@ OR " + EOAffectationDetail.D_FIN_AFFECTATION_DETAIL + ">=%@", new
		 * NSArray(new Object[]{NSKeyValueCoding.NullValue, evaDFin()})));
		 */
		if (recsAffDetNonFermees.count() > 0) {
			// on prend les fiches ayant ces occupation
			for (int i = 0; i < recsFicheDePoste.count(); i++) {
				EOFicheDePoste recFicheDePoste = (EOFicheDePoste) recsFicheDePoste.objectAtIndex(i);
				for (int j = 0; j < recsAffDetNonFermees.count(); j++) {
					EOAffectationDetail recAffDet = (EOAffectationDetail) recsAffDetNonFermees.objectAtIndex(j);
					if (recFicheDePoste.tosAffectationDetail().containsObject(recAffDet)) {
						result = result.arrayByAddingObject(recFicheDePoste);
						break;
					}
				}
			}
		} else {
			// on prend alors les dernieres en date
			result = recsFicheDePoste;
		}
		result = NSArrayCtrl.removeDuplicate(result);
		return result;
	}

	/**
	 * La liste des services de rattachement de cette evaluation
	 * 
	 * @return
	 */
	public NSArray<EOStructure> tosStructure() {
		return (NSArray<EOStructure>) tosPoste().valueForKey(EOPoste.TO_STRUCTURE_KEY);
	}

	/**
	 * La liste des postes de rattachement de cette evaluation
	 * 
	 * @return
	 */
	public NSArray<EOPoste> tosPoste() {
		return (NSArray<EOPoste>) valueForKeyPath(TOS_LAST_FICHE_DE_POSTE_KEY + "." + EOFicheDePoste.TO_POSTE_KEY);
	}

	/**
	 * Retourne l'objet <code>EOVCandidatEvaluation</code> associe a cet
	 * enregistrement.
	 */
	public EOVCandidatEvaluation toVCandidatEvaluationPeriode() {
		return EOVCandidatEvaluation.fetchRequired(
				editingContext(), EOVCandidatEvaluation.TO_EVALUATION_KEY, this);
	}

	/**
	 * La liste des objectifs classés par ordre de la position
	 */
	public NSArray<EOObjectif> tosObjectif() {
		NSArray<EOObjectif> tosObjectif = super.tosObjectif();
		return CktlSort.sortedArray(tosObjectif, EOObjectif.OBJ_POSITION_KEY);
	}

	private final static int DEFAULT_OBJECTIF_COUNT = 4;

	/**
	 * Creation d'un enregistrement avec un certain nombre d'objectifs vierges
	 * 
	 * @return
	 */
	public static EOEvaluation createWithObjectif(
			EOEditingContext ec, EOIndividu individu, EOEvaluationPeriode evaluationPeriode) {
		NSTimestamp now = DateCtrl.now();
		EOEvaluation evaluation = createEOEvaluation(
				ec,
				now,
				now,
				(Integer) individu.noIndividu());
		evaluation.setToEvaluationPeriodeRelationship(evaluationPeriode);
		evaluation.setToIndividuRelationship(individu);

		// les objectifs
		for (int i = 0; i < DEFAULT_OBJECTIF_COUNT; i++) {
			EOObjectif unObjectif = EOObjectif.createEOObjectif(ec, now, now, i, evaluation);
			evaluation.addToTosObjectifRelationship(unObjectif);
		}

		return evaluation;
	}

	/**
	 * Indique si l'entretien a déjà été tenu
	 * 
	 * @return
	 */
	public boolean isEntretienTenu() {
		boolean isEntretienTenu = false;

		if (dTenueEntretien() != null) {
			isEntretienTenu = true;
		}

		return isEntretienTenu;
	}

	/**
	 * Gestion de la selection des évaluations dans parmi une liste : cette
	 * méthode va créer les enregistrements nécéssaires si besoin.
	 * 
	 * Attention, cette méthode fait la sauvegarde, car il faut rafraichir
	 * certiains objets via les globalIds ... dispos uniquement apres sauvegarde
	 * 
	 * @return
	 * @throws Throwable
	 */
	public static EOEvaluation doSelectEvaluation(
			EOEditingContext editingContext,
			EOVCandidatEvaluation eoVCandidatEvaluation,
			EOEvaluationPeriode eoEvaluationPeriode,
			EOIndividu eoIndividuSelectionneur, EOIndividu individuResp) throws Throwable {

		EOEvaluation eoEvaluation = null;

		// faut-il créer l'enregistrement N ?
		if (eoVCandidatEvaluation.toEvaluation() != null) {
			eoEvaluation = eoVCandidatEvaluation.toEvaluation();
		} else {
			if (eoEvaluationPeriode != null) {
				eoEvaluation = EOEvaluation.createWithObjectif(
						editingContext, eoVCandidatEvaluation.toIndividu(), eoEvaluationPeriode);
				eoEvaluation.setToIndividuRespRelationship(individuResp);
			}
		}

		// rustine pour les évaluations existantes - DT 7938
		if (eoEvaluation.toIndividuResp() == null) {
			eoEvaluation.setToIndividuRespRelationship(individuResp);
		}
		
		
		if (GRHUtilities.save(editingContext, "")) {
			// forcer le rafraichissement de objets
			NSMutableArray<EOGlobalID> eoGlobalIDArray = new NSMutableArray<EOGlobalID>();
			eoGlobalIDArray.add(editingContext.globalIDForObject(eoVCandidatEvaluation));
			eoGlobalIDArray.add(editingContext.globalIDForObject(eoEvaluation));
			editingContext.invalidateObjectsWithGlobalIDs(eoGlobalIDArray);
		}
		return eoEvaluation;
	}

	/**
	 * Indique si le dernier statut connu de l'agent sur la période de l'entretien
	 * est AENES
	 * 
	 * @return
	 */
	public boolean isAenes() {
		boolean isAenes = false;
		IndividuGrhService individuService = new IndividuGrhService();
		
//		isAenes = toIndividu().isAenes(toEvaluationPeriode().epeDDebut(), toEvaluationPeriode().epeDFin());
		isAenes = individuService.isAenes(toIndividu(), this.editingContext(), toEvaluationPeriode().epeDDebut(), toEvaluationPeriode().epeDFin());

		return isAenes;
	}

	/**
	 * Indique si le dernier statut connu de l'agent sur la période de l'entretien
	 * est ITRF
	 * 
	 * @return
	 */
	public boolean isItrf() {
		boolean isItrf = false;
		IndividuGrhService individuService = new IndividuGrhService();
		
		isItrf = individuService.isItrf(toIndividu(), this.editingContext(), toEvaluationPeriode().epeDDebut(), toEvaluationPeriode().epeDFin());

		return isItrf;
	}

	/**
	 * Indique si le dernier statut connu de l'agent sur la période de l'entretien
	 * est AENES
	 * 
	 * @return
	 */
	public boolean isBu() {
		boolean isBu = false;
		IndividuGrhService individuService = new IndividuGrhService();
		
		isBu = individuService.isBu(toIndividu(), this.editingContext(), toEvaluationPeriode().epeDDebut(), toEvaluationPeriode().epeDFin());

		return isBu;
	}

	private static final String LIBELLE_POPULATION_AENES = "Administration de l'Education Nationale et de l'Enseignement (AENES)";
	private static final String LIBELLE_POPULATION_ITRF = "Ingénieurs Techniques, de Recherche et de Formation (ITRF)";
	private static final String LIBELLE_POPULATION_BU = "Bibliothèque";

	/**
	 * Le libelle de la population selon le statut de l'agent
	 * 
	 * @return
	 */
	public String getLibellePopulation() {
		String libelle = "LIBELLE_POPULATION_BU";

		if (isAenes()) {
			libelle = LIBELLE_POPULATION_AENES;
		} else if (isItrf()) {
			libelle = LIBELLE_POPULATION_ITRF;
		} else if (isBu()) {
			libelle = LIBELLE_POPULATION_BU;
		}

		return libelle;
	}

	// dico des données

	private NSMutableDictionary<String, Object> dicoAgent;

	public final NSDictionary<String, Object> getDicoAgent() {
		IndividuGrhService individuService = new IndividuGrhService();
		FinderFeveService feveService = new FinderFeveService();
		if (dicoAgent == null) {
			dicoAgent = new NSMutableDictionary<String, Object>();
//			dicoAgent.addEntriesFromDictionary(EOIndividu.findDicoAgentGepetoInContext(editingContext(), this));
			dicoAgent.addEntriesFromDictionary(individuService.findDicoAgentGepetoInContext(editingContext(), this));
			dicoAgent.addEntriesFromDictionary(feveService.findDicoEvaluationInContext(editingContext(), this));
		}
		return dicoAgent;
	}

	/**
	 * Récupere la liste classées par date des évaluations d'un individu.
	 * @param edc l'editing context
	 * @param individu l'individu dont on veut récuperer les évaluations
	 * @return les évaluations
	 */
	public static NSArray<EOEvaluation> getEvaluationsForIndividu(EOEditingContext edc, EOIndividu individu) {
		EOQualifier qualifier = EOEvaluation.TO_INDIVIDU.eq(individu);
		NSArray<EOEvaluation> evaluationsAvantTrie = EOEvaluation.fetchAll(edc, qualifier, new NSArray<EOSortOrdering>());
		
		//on trie les évaluations qui n'ont pas d'évaluationsPeriode
		NSArray<EOEvaluation> evaluations = new NSMutableArray();
		for (EOEvaluation evaluation : evaluationsAvantTrie) {
			if (evaluation.toEvaluationPeriode() != null) {
				evaluations.add(evaluation);
			}
		}
		
		return evaluations;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getNomPrenomEvaluateur() {
		if (this.toIndividuResp() != null) {
			if (this.toIndividuResp().nomPrenom() != null) {
				return this.toIndividuResp().nomPrenom();
			}
		}		
		return StringUtils.EMPTY;
	}
	
	public String getFonctionEvaluateur() {
		if (this.fonctionEvaluateur() != null) {
			return this.fonctionEvaluateur();
		}
		if (this.toIndividuResp() != null) {
			if (this.toIndividuResp().indQualite() != null) {
				return this.toIndividuResp().indQualite();
			}
		}
		return StringUtils.EMPTY;
	}
	
}
