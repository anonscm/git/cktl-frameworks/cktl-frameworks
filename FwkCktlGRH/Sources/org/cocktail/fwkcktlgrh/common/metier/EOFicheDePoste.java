/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IFicheDePoste;
import org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites;
import org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOFicheDePoste extends _EOFicheDePoste implements IFicheDePoste {
	
	
  /**
	 * 
	 */
	private static final long serialVersionUID = 456456456L;

private static Logger log = Logger.getLogger(EOFicheDePoste.class);
  
  public static final String ENTITY_TABLE_NAME = "MANGUE.FICHE_DE_POSTE";

  public EOFicheDePoste() {
	  super();
  	  typeFiche = FICHE_DE_POSTE;
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }

//methodes rajoutees

	public NSArray tosReferensActivites() {
		return (NSArray) tosRepartFdpActi().valueForKey(EORepartFdpActi.TO_REFERENS_ACTIVITES_KEY);
	}

	public NSArray tosReferensCompetences() {
		return (NSArray) tosRepartFdpComp().valueForKey(EORepartFdpComp.TO_REFERENS_COMPETENCES_KEY);
	}

	public NSArray tosRepartFdpActi() {
		NSArray tosRepartFdpActi = super.tosRepartFdpActi();
		return CktlSort.sortedArray(tosRepartFdpActi, EORepartFdpActi.RFA_POSITION_KEY);
	}

	/**
	 * La liste des activités autres
	 * 
	 * @return
	 */
	public NSArray tosRepartFdpActivitesAutres() {
		NSArray tosRepartFdpActivitesAutres = super.tosRepartFdpAutre(
					CktlDataBus.newCondition(EORepartFdpAutre.FAU_TYPE_KEY + "='" + EORepartFdpAutre.TYPE_ACTIVITE + "'"));
		return CktlSort.sortedArray(tosRepartFdpActivitesAutres, EORepartFdpAutre.FAU_POSITION_KEY);
	}

	/**
	 * La liste des competences autres
	 * 
	 * @return
	 */
	public NSArray tosRepartFdpCompetencesAutres() {
		NSArray tosRepartFdpCompetencesAutres = super.tosRepartFdpAutre(
					CktlDataBus.newCondition(EORepartFdpAutre.FAU_TYPE_KEY + "='" + EORepartFdpAutre.TYPE_COMPETENCE + "'"));
		return CktlSort.sortedArray(tosRepartFdpCompetencesAutres, EORepartFdpAutre.FAU_POSITION_KEY);
	}

	public NSArray<EORepartFdpComp> tosRepartFdpComp() {
		// return super.tosRepartFdpComp(null,
		// CktlSort.newSort(EORepartFdpComp.RFC_POSITION_KEY), false);
		NSArray<EORepartFdpComp> tosRepartFdpComp = super.tosRepartFdpComp();
		return CktlSort.sortedArray(tosRepartFdpComp, EORepartFdpComp.RFC_POSITION_KEY);
	}

	public static final String IS_VALIDEE_AGENT = "fdpVisaAgentBool";
	public static final String IS_VALIDEE_RESPONSABLE = "fdpVisaRespBool";
	public static final String IS_VALIDEE_DIRECTEUR = "fdpVisaDirecBool";

	public boolean fdpVisaAgentBool() {
		return OUI.equals(fdpVisaAgent());
	}

	public void setFdpVisaAgentBool(boolean value) {
		setFdpVisaAgent(NON);
		if (value == true)
			setFdpVisaAgent(OUI);
	}

	public boolean fdpVisaRespBool() {
		return OUI.equals(fdpVisaResp());
	}

	public void setFdpVisaRespBool(boolean value) {
		setFdpVisaResp(NON);
		if (value == true)
			setFdpVisaResp(OUI);
	}

	public boolean fdpVisaDirecBool() {
		return OUI.equals(fdpVisaDirec());
	}

	public void setFdpVisaDirecBool(boolean value) {
		setFdpVisaDirec(NON);
		if (value == true)
			setFdpVisaDirec(OUI);
	}

	/**
	 * Indique si les visa ne sont pas faites sur cette fiche
	 */
	public boolean hasWarning() {
		return !fdpVisaAgentBool() || !fdpVisaDirecBool() || !fdpVisaRespBool();
	}

	public String htmlWarnMessage() {
		StringBuffer buff = new StringBuffer();
		if (!fdpVisaAgentBool()) {
			buff.append("Fiche de poste ").append(display()).
					append(" non vis&eacute;e par l'agent.<br>");
		}
		if (!fdpVisaRespBool()) {
			buff.append("Fiche de poste ").append(display()).
					append(" non vis&eacute;e par le responsable.<br>");
		}
		if (!fdpVisaDirecBool()) {
			buff.append("Fiche de poste ").append(display()).
					append(" non vis&eacute;e par le directeur de composante.<br>");
		}
		return buff.toString();
	}

	// setters silencieux pour ne pas planter lors d'acces non prévu

	public void setTosReferensActivites(NSArray value) {

	}

	public void setTosReferensCompetences(NSArray value) {

	}

	
	/**
	 * est-ce qu'une activité autre est en cours de modification
	 * 
	 * @return
	 */
	public boolean isAuMoinsUneActiviteAutrefEstEnCoursDeModification() {
		boolean result = false;
		int i = 0;
		while (!result && i < tosRepartFdpActivitesAutres().count()) {
			EORepartFdpAutre repart = (EORepartFdpAutre) tosRepartFdpActivitesAutres().objectAtIndex(i);
			if (repart.isEnCoursDeModification()) {
				result = true;
			}
			i++;
		}
		return result;
	}

	/**
	 * désactiver les activités autre est en cours de modification
	 * 
	 * @return
	 */
	public void desactiverTouteActiviteAutrefEnCoursDeModification() {
		for (int i = 0; i < tosRepartFdpActivitesAutres().count(); i++) {
			EORepartFdpAutre repart = (EORepartFdpAutre) tosRepartFdpActivitesAutres().objectAtIndex(i);
			repart.setIsEnCoursDeModification(false);
		}
	}

	/**
	 * est-ce qu'une competence autre est en cours de modification
	 * 
	 * @return
	 */
	public boolean isAuMoinsUneCompetenceAutrefEstEnCoursDeModification() {
		boolean result = false;
		int i = 0;
		while (!result && i < tosRepartFdpCompetencesAutres().count()) {
			EORepartFdpAutre repart = (EORepartFdpAutre) tosRepartFdpCompetencesAutres().objectAtIndex(i);
			if (repart.isEnCoursDeModification()) {
				result = true;
			}
			i++;
		}
		return result;
	}

	/**
	 * désactiver les competences autre est en cours de modification
	 * 
	 * @return
	 */
	public void desactiverTouteCompetenceAutrefEnCoursDeModification() {
		for (int i = 0; i < tosRepartFdpCompetencesAutres().count(); i++) {
			EORepartFdpAutre repart = (EORepartFdpAutre) tosRepartFdpCompetencesAutres().objectAtIndex(i);
			repart.setIsEnCoursDeModification(false);
		}
	}

	/**
	 * trouver un enregistrement a partir de la cle primaire
	 */
	public static EOFicheDePoste findFicheDePosteForFdpKeyInContext(EOEditingContext ec, int fdpKey) {
		EOFicheDePoste record = null;
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("fdpKey = %@", new NSArray(new Integer(fdpKey)));
		record = fetchRequired(ec, qual);
		return record;
	}

	/**
	 * Creer une fiche de poste a partir d'une fiche existante.
	 * 
	 * @param prevFiche
	 *          : la fiche a dupliquer
	 * @param dDebut
	 * @param dFin
	 * @param isDupliquerContenuActiviteCompetencesEmploiType
	 * @param eoPoste
	 *          le poste surlequel afficher la nouvelle fiche
	 * @return
	 */
	public static EOFicheDePoste dupliquerFiche(
				EOFicheDePoste prevFiche,
				NSTimestamp dDebut,
				NSTimestamp dFin,
				EOPoste eoPoste) {
		EOEditingContext ec = prevFiche.editingContext();

		// creer le record FicheDePoste
		EOFicheDePoste newEoFicheDePoste = newDefaultRecordInContext(ec);

		newEoFicheDePoste.setToPosteRelationship(eoPoste);
		newEoFicheDePoste.setFdpDDebut(dDebut);
		newEoFicheDePoste.setFdpDFin(dFin);
		newEoFicheDePoste.setFdpContexteTravail(prevFiche.fdpContexteTravail());
		newEoFicheDePoste.setFdpMissionPoste(prevFiche.fdpMissionPoste());

		if (prevFiche.isEmploiSurAncienneNomenclature() == false) {

			newEoFicheDePoste.setToReferensEmploisRelationship(prevFiche.toReferensEmplois());

			// creer les records RepartFdpActi
			for (int i = 0; i < prevFiche.tosRepartFdpActi().count(); i++) {
				EORepartFdpActi recRepart = (EORepartFdpActi) prevFiche.tosRepartFdpActi().objectAtIndex(i);
				EORepartFdpActi newRepart = EORepartFdpActi.newRecordInContext(
							ec, newEoFicheDePoste, recRepart.toReferensActivites());
				newRepart.setRfaPosition(recRepart.rfaPosition());
			}

			// creer les records RepartFdpComp
			for (int i = 0; i < prevFiche.tosRepartFdpComp().count(); i++) {
				EORepartFdpComp recRepart = (EORepartFdpComp) prevFiche.tosRepartFdpComp().objectAtIndex(i);
				EORepartFdpComp newRepart = EORepartFdpComp.newRecordInContext(
							ec, newEoFicheDePoste, recRepart.toReferensCompetences());
				newRepart.setRfcPosition(recRepart.rfcPosition());
			}

			// creer les records RepartFdpAutre
			for (int i = 0; i < prevFiche.tosRepartFdpAutre().count(); i++) {
				// nouvel enregistrement activite autre et comeptence autre
				EORepartFdpAutre recRepart = (EORepartFdpAutre) prevFiche.tosRepartFdpAutre().objectAtIndex(i);
				EORepartFdpAutre newRepart = EORepartFdpAutre.newRecordInContext(
							ec, newEoFicheDePoste, recRepart.fauChampLibre(), recRepart.fauType());
				newRepart.setFauPosition(recRepart.fauPosition());
			}
		}

		return newEoFicheDePoste;
	}

	public static EOFicheDePoste newRecordInContext(
				EOEditingContext ec,
				EOPoste poste,
				NSTimestamp dDebut,
				NSTimestamp dFin
			) {
		EOFicheDePoste newRecord = newDefaultRecordInContext(ec);

		newRecord.setToPosteRelationship(poste);
		newRecord.setFdpDDebut(dDebut);
		newRecord.setFdpDFin(dFin);

		return newRecord;
	}

	/**
	 * Constructeur par défaut
	 * 
	 * @param ec
	 * @return
	 */
	private static EOFicheDePoste newDefaultRecordInContext(EOEditingContext ec) {
		EOFicheDePoste record = new EOFicheDePoste();
		record.setFdpVisaAgent(NON);
		record.setFdpVisaDirec(NON);
		record.setFdpVisaResp(NON);
		record.setFonctionConduiteProjet("N");
		record.setFonctionEncadrement("N");
		ec.insertObject(record);
		return record;
	}

	/**
	 * Indique si la fiche est sur l'ancienne nomenclature ou non
	 * 
	 * @return
	 */
	public boolean isEmploiSurAncienneNomenclature() {
		boolean isEmploiSurAncienneNomenclature = false;

		if (toReferensEmplois() != null
					&& toReferensEmplois().isArchive()) {
			isEmploiSurAncienneNomenclature = true;
		}

		return isEmploiSurAncienneNomenclature;

	}

	/**
	 * Affecter des competences à la fiche de poste
	 */
	public NSArray<EORepartFdpComp> addReferensCompetencesArray(
			NSArray<EOReferensCompetences> eoReferensCompetencesArray) {
		NSArray<EORepartFdpComp> array = new NSArray<EORepartFdpComp>();

		// positionner en dernier par défaut
		NSArray<EORepartFdpComp> repartArray = tosRepartFdpComp();
		int position = 1;
		if (repartArray.count() > 0) {
			position = repartArray.lastObject().rfcPosition().intValue() + 1;
		}

		for (EOReferensCompetences eoReferensCompetences : eoReferensCompetencesArray) {
			EORepartFdpComp eoRepartFdpComp = EORepartFdpComp.newRecordInContext(
					editingContext(), this, eoReferensCompetences);
			eoRepartFdpComp.setRfcPosition(position);
			position++;
		}

		return array;
	}

	/**
	 * Affecter une competence à la fiche de poste
	 */
	public EORepartFdpComp addReferensCompetences(
			EOReferensCompetences eoReferensCompetence) {
		EORepartFdpComp eoRepartFdpComp = null;

		// positionner en dernier par défaut
		NSArray<EORepartFdpComp> repartArray = tosRepartFdpComp();
		int position = 1;
		if (repartArray.count() > 0) {
			position = repartArray.lastObject().rfcPosition().intValue() + 1;
		}

		eoRepartFdpComp = EORepartFdpComp.newRecordInContext(
				editingContext(), this, eoReferensCompetence);
		eoRepartFdpComp.setRfcPosition(position);

		return eoRepartFdpComp;
	}

	/**
	 * Affecter des competences à la fiche de poste
	 */
	public NSArray<EORepartFdpActi> addReferensActivitesArray(
			NSArray<EOReferensActivites> eoReferensActivitesArray) {
		NSArray<EORepartFdpActi> array = new NSArray<EORepartFdpActi>();

		// positionner en dernier par défaut
		NSArray<EORepartFdpActi> repartArray = tosRepartFdpActi();
		int position = 1;
		if (repartArray.count() > 0) {
			position = repartArray.lastObject().rfaPosition().intValue() + 1;
		}

		for (EOReferensActivites eoReferensActivites : eoReferensActivitesArray) {
			EORepartFdpActi eoRepartFdpActi = EORepartFdpActi.newRecordInContext(
					editingContext(), this, eoReferensActivites);
			eoRepartFdpActi.setRfaPosition(position);
			position++;
		}

		return array;
	}

	/**
	 * Affecter une competence à la fiche de poste
	 */
	public EORepartFdpActi addReferensActivites(
			EOReferensActivites eoReferensActivites) {
		EORepartFdpActi eoRepartFdpActi = null;

		// positionner en dernier par défaut
		NSArray<EORepartFdpActi> repartArray = tosRepartFdpActi();
		int position = 1;
		if (repartArray.count() > 0) {
			position = repartArray.lastObject().rfaPosition().intValue() + 1;
		}

		eoRepartFdpActi = EORepartFdpActi.newRecordInContext(
				editingContext(), this, eoReferensActivites);
		eoRepartFdpActi.setRfaPosition(position);

		return eoRepartFdpActi;
	}

}
