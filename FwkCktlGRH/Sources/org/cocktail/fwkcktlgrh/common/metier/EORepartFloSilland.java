/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IRepartFloSilland;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EORepartFloSilland extends _EORepartFloSilland implements IRepartFloSilland {
  private static Logger log = Logger.getLogger(EORepartFloSilland.class);

  public EORepartFloSilland() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }

  // methodes rajoutees
  
  private Boolean isEnCoursDeModification;
  
  public boolean isEnCoursDeModification() {
      if (isEnCoursDeModification == null) {
          isEnCoursDeModification = new Boolean(false);
      }
      return isEnCoursDeModification.booleanValue();
  }
  
  public void setIsEnCoursDeModification(boolean value) {
      isEnCoursDeModification = new Boolean(value);
  }
  
  private Boolean isEnCoursDAjoutDeDestin;
  
  public boolean isEnCoursDAjoutDeLolfNomenclature() {
      if (isEnCoursDAjoutDeDestin == null) {
        isEnCoursDAjoutDeDestin = new Boolean(false);
      }
      return isEnCoursDAjoutDeDestin.booleanValue();
  }
  
  public void setIsEnCoursDAjoutDeLolfNomenclature(boolean value) {
    isEnCoursDAjoutDeDestin = new Boolean(value);
  }

  /**
	 * Indique si la fonction est affectee dans l'exercice.
	 * 
	 * @param exercice
	 * @return
	 */
	public boolean isDeclaree(EOFctSilland fctSilland, EOExercice exercice) {
		return tosRepartSilLolf(
					fctSilland,
					CktlDataBus.newCondition(
							// EORepartSillandLolf.TO_TYPE_ACTION_KEY + "." +
							// EOTypeAction.EXERCICE_KEY + "=%@",
							EORepartLolfSilland.TO_EXERCICE_KEY + "=%@",
							new NSArray(exercice)), null).count() > 0;
	}
  
	public NSArray<EORepartLolfSilland> tosRepartSilLolf(EOFctSilland silland, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray results;
	      EOQualifier fullQualifier;
	      EOQualifier qual = new EOKeyValueQualifier(EORepartLolfSilland.TO_FCT_SILLAND_KEY, EOQualifier.QualifierOperatorEqual, silland);
	    	
	      if (qualifier == null) {
	        fullQualifier = qual;
	      } else {
	    	  fullQualifier = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
		    			qual, qualifier
		    	}));
	      }

	      results = EORepartLolfSilland.fetchAll(silland.editingContext(), fullQualifier, sortOrderings);
	    return results;
	  }
	
}
