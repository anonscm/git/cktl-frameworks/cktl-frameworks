/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOFicheLolf.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOFicheLolf extends org.cocktail.fwkcktlgrh.common.metier.util.UtilFiche {
	public static final String ENTITY_NAME = "Fwkgrh_FicheLolf";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FLO_D_DEBUT_KEY = "floDDebut";
	public static final String FLO_D_FIN_KEY = "floDFin";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> EXE_ORDRE = new ERXKey<Integer>("exeOrdre");
	public static final ERXKey<NSTimestamp> FLO_D_DEBUT = new ERXKey<NSTimestamp>("floDDebut");
	public static final ERXKey<NSTimestamp> FLO_D_FIN = new ERXKey<NSTimestamp>("floDFin");
	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_POSTE_KEY = "toPoste";
	public static final String TOS_REPART_FLO_SILLAND_KEY = "tosRepartFloSilland";

	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOPoste> TO_POSTE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOPoste>("toPoste");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland> TOS_REPART_FLO_SILLAND = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland>("tosRepartFloSilland");

  private static Logger LOG = Logger.getLogger(_EOFicheLolf.class);

  public EOFicheLolf localInstanceIn(EOEditingContext editingContext) {
    EOFicheLolf localInstance = (EOFicheLolf)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOFicheLolf.LOG.isDebugEnabled()) {
    	_EOFicheLolf.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOFicheLolf.LOG.isDebugEnabled()) {
    	_EOFicheLolf.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey("exeOrdre");
  }

  public void setExeOrdre(Integer value) {
    if (_EOFicheLolf.LOG.isDebugEnabled()) {
    	_EOFicheLolf.LOG.debug( "updating exeOrdre from " + exeOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "exeOrdre");
  }

  public NSTimestamp floDDebut() {
    return (NSTimestamp) storedValueForKey("floDDebut");
  }

  public void setFloDDebut(NSTimestamp value) {
    if (_EOFicheLolf.LOG.isDebugEnabled()) {
    	_EOFicheLolf.LOG.debug( "updating floDDebut from " + floDDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "floDDebut");
  }

  public NSTimestamp floDFin() {
    return (NSTimestamp) storedValueForKey("floDFin");
  }

  public void setFloDFin(NSTimestamp value) {
    if (_EOFicheLolf.LOG.isDebugEnabled()) {
    	_EOFicheLolf.LOG.debug( "updating floDFin from " + floDFin() + " to " + value);
    }
    takeStoredValueForKey(value, "floDFin");
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey("toExercice");
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (_EOFicheLolf.LOG.isDebugEnabled()) {
      _EOFicheLolf.LOG.debug("updating toExercice from " + toExercice() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toExercice");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toExercice");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOPoste toPoste() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOPoste)storedValueForKey("toPoste");
  }

  public void setToPosteRelationship(org.cocktail.fwkcktlgrh.common.metier.EOPoste value) {
    if (_EOFicheLolf.LOG.isDebugEnabled()) {
      _EOFicheLolf.LOG.debug("updating toPoste from " + toPoste() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOPoste oldValue = toPoste();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPoste");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPoste");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland> tosRepartFloSilland() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland>)storedValueForKey("tosRepartFloSilland");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland> tosRepartFloSilland(EOQualifier qualifier) {
    return tosRepartFloSilland(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland> tosRepartFloSilland(EOQualifier qualifier, boolean fetch) {
    return tosRepartFloSilland(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland> tosRepartFloSilland(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland.TO_FICHE_LOLF_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartFloSilland();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartFloSillandRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland object) {
    if (_EOFicheLolf.LOG.isDebugEnabled()) {
      _EOFicheLolf.LOG.debug("adding " + object + " to tosRepartFloSilland relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartFloSilland");
  }

  public void removeFromTosRepartFloSillandRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland object) {
    if (_EOFicheLolf.LOG.isDebugEnabled()) {
      _EOFicheLolf.LOG.debug("removing " + object + " from tosRepartFloSilland relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFloSilland");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland createTosRepartFloSillandRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartFloSilland");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartFloSilland");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland) eo;
  }

  public void deleteTosRepartFloSillandRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFloSilland");
  }

  public void deleteAllTosRepartFloSillandRelationships() {
    Enumeration objects = tosRepartFloSilland().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartFloSillandRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartFloSilland)objects.nextElement());
    }
  }


  public static EOFicheLolf create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer exeOrdre
, NSTimestamp floDDebut
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice, org.cocktail.fwkcktlgrh.common.metier.EOPoste toPoste) {
    EOFicheLolf eo = (EOFicheLolf) EOUtilities.createAndInsertInstance(editingContext, _EOFicheLolf.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setExeOrdre(exeOrdre);
		eo.setFloDDebut(floDDebut);
    eo.setToExerciceRelationship(toExercice);
    eo.setToPosteRelationship(toPoste);
    return eo;
  }

  public static NSArray<EOFicheLolf> fetchAll(EOEditingContext editingContext) {
    return _EOFicheLolf.fetchAll(editingContext, null);
  }

  public static NSArray<EOFicheLolf> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOFicheLolf.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOFicheLolf> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOFicheLolf.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOFicheLolf> eoObjects = (NSArray<EOFicheLolf>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOFicheLolf fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFicheLolf.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFicheLolf fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOFicheLolf> eoObjects = _EOFicheLolf.fetch(editingContext, qualifier, null);
    EOFicheLolf eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOFicheLolf)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_FicheLolf that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFicheLolf fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFicheLolf.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOFicheLolf fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOFicheLolf eoObject = _EOFicheLolf.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_FicheLolf that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOFicheLolf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFicheLolf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFicheLolf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFicheLolf)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOFicheLolf localInstanceIn(EOEditingContext editingContext, EOFicheLolf eo) {
    EOFicheLolf localInstance = (eo == null) ? null : (EOFicheLolf)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
  public static NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf> fetchFetchSuivi(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchSuivi", "Fwkgrh_FicheLolf");
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf> fetchFetchSuivi(EOEditingContext editingContext,
	String cStructureBinding,
	String cStructureComposanteBinding,
	Integer exeOrdreBinding)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchSuivi", "Fwkgrh_FicheLolf");
    NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
    bindings.takeValueForKey(cStructureBinding, "cStructure");
    bindings.takeValueForKey(cStructureComposanteBinding, "cStructureComposante");
    bindings.takeValueForKey(exeOrdreBinding, "exeOrdre");
	fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
}
