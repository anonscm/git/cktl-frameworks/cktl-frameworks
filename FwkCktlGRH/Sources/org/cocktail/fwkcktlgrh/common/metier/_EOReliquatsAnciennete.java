/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOReliquatsAnciennete.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOReliquatsAnciennete extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_ReliquatsAnciennete";

	// Attributes
	public static final String ANC_ANNEE_KEY = "ancAnnee";
	public static final String ANC_NB_ANNEES_KEY = "ancNbAnnees";
	public static final String ANC_NB_JOURS_KEY = "ancNbJours";
	public static final String ANC_NB_MOIS_KEY = "ancNbMois";
	public static final String D_ARRETE_KEY = "dArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String NO_SEQ_ELEMENT_KEY = "noSeqElement";
	public static final String TEM_UTILISE_KEY = "temUtilise";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<Integer> ANC_ANNEE = new ERXKey<Integer>("ancAnnee");
	public static final ERXKey<Integer> ANC_NB_ANNEES = new ERXKey<Integer>("ancNbAnnees");
	public static final ERXKey<Integer> ANC_NB_JOURS = new ERXKey<Integer>("ancNbJours");
	public static final ERXKey<Integer> ANC_NB_MOIS = new ERXKey<Integer>("ancNbMois");
	public static final ERXKey<NSTimestamp> D_ARRETE = new ERXKey<NSTimestamp>("dArrete");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> NO_ARRETE = new ERXKey<String>("noArrete");
	public static final ERXKey<Integer> NO_DOSSIER_PERS = new ERXKey<Integer>("noDossierPers");
	public static final ERXKey<Integer> NO_SEQ_CARRIERE = new ERXKey<Integer>("noSeqCarriere");
	public static final ERXKey<Integer> NO_SEQ_ELEMENT = new ERXKey<Integer>("noSeqElement");
	public static final ERXKey<String> TEM_UTILISE = new ERXKey<String>("temUtilise");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships


  private static Logger LOG = Logger.getLogger(_EOReliquatsAnciennete.class);

  public EOReliquatsAnciennete localInstanceIn(EOEditingContext editingContext) {
    EOReliquatsAnciennete localInstance = (EOReliquatsAnciennete)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer ancAnnee() {
    return (Integer) storedValueForKey("ancAnnee");
  }

  public void setAncAnnee(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating ancAnnee from " + ancAnnee() + " to " + value);
    }
    takeStoredValueForKey(value, "ancAnnee");
  }

  public Integer ancNbAnnees() {
    return (Integer) storedValueForKey("ancNbAnnees");
  }

  public void setAncNbAnnees(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating ancNbAnnees from " + ancNbAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "ancNbAnnees");
  }

  public Integer ancNbJours() {
    return (Integer) storedValueForKey("ancNbJours");
  }

  public void setAncNbJours(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating ancNbJours from " + ancNbJours() + " to " + value);
    }
    takeStoredValueForKey(value, "ancNbJours");
  }

  public Integer ancNbMois() {
    return (Integer) storedValueForKey("ancNbMois");
  }

  public void setAncNbMois(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating ancNbMois from " + ancNbMois() + " to " + value);
    }
    takeStoredValueForKey(value, "ancNbMois");
  }

  public NSTimestamp dArrete() {
    return (NSTimestamp) storedValueForKey("dArrete");
  }

  public void setDArrete(NSTimestamp value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating dArrete from " + dArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey("noDossierPers");
  }

  public void setNoDossierPers(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating noDossierPers from " + noDossierPers() + " to " + value);
    }
    takeStoredValueForKey(value, "noDossierPers");
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey("noSeqCarriere");
  }

  public void setNoSeqCarriere(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating noSeqCarriere from " + noSeqCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqCarriere");
  }

  public Integer noSeqElement() {
    return (Integer) storedValueForKey("noSeqElement");
  }

  public void setNoSeqElement(Integer value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating noSeqElement from " + noSeqElement() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqElement");
  }

  public String temUtilise() {
    return (String) storedValueForKey("temUtilise");
  }

  public void setTemUtilise(String value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating temUtilise from " + temUtilise() + " to " + value);
    }
    takeStoredValueForKey(value, "temUtilise");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOReliquatsAnciennete.LOG.isDebugEnabled()) {
    	_EOReliquatsAnciennete.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }


  public static EOReliquatsAnciennete create(EOEditingContext editingContext, Integer ancAnnee
, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noDossierPers
, Integer noSeqCarriere
, Integer noSeqElement
, String temValide
) {
    EOReliquatsAnciennete eo = (EOReliquatsAnciennete) EOUtilities.createAndInsertInstance(editingContext, _EOReliquatsAnciennete.ENTITY_NAME);    
		eo.setAncAnnee(ancAnnee);
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setNoSeqCarriere(noSeqCarriere);
		eo.setNoSeqElement(noSeqElement);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOReliquatsAnciennete> fetchAll(EOEditingContext editingContext) {
    return _EOReliquatsAnciennete.fetchAll(editingContext, null);
  }

  public static NSArray<EOReliquatsAnciennete> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOReliquatsAnciennete.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOReliquatsAnciennete> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOReliquatsAnciennete.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOReliquatsAnciennete> eoObjects = (NSArray<EOReliquatsAnciennete>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOReliquatsAnciennete fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReliquatsAnciennete.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReliquatsAnciennete fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOReliquatsAnciennete> eoObjects = _EOReliquatsAnciennete.fetch(editingContext, qualifier, null);
    EOReliquatsAnciennete eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOReliquatsAnciennete)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_ReliquatsAnciennete that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReliquatsAnciennete fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReliquatsAnciennete.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOReliquatsAnciennete fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOReliquatsAnciennete eoObject = _EOReliquatsAnciennete.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_ReliquatsAnciennete that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOReliquatsAnciennete fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReliquatsAnciennete fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReliquatsAnciennete eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReliquatsAnciennete)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOReliquatsAnciennete localInstanceIn(EOEditingContext editingContext, EOReliquatsAnciennete eo) {
    EOReliquatsAnciennete localInstance = (eo == null) ? null : (EOReliquatsAnciennete)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
