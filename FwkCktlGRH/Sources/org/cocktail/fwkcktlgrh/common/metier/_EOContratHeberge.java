/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOContratHeberge.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOContratHeberge extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_ContratHeberge";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String CTRH_INM_KEY = "ctrhInm";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
	public static final ERXKey<Long> CTRH_INM = new ERXKey<Long>("ctrhInm");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_ADRESSE_KEY = "toAdresse";
	public static final String TO_AFFECTATION_KEY = "toAffectation";
	public static final String TO_CORPS_KEY = "toCorps";
	public static final String TO_GRADE_KEY = "toGrade";
	public static final String TO_PERSONNEL_KEY = "toPersonnel";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_STRUCTURE_KEY = "toStructure";
	public static final String TO_TYPE_CONTRAT_TRAVAIL_KEY = "toTypeContratTravail";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_ADRESSE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toAdresse");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAffectation> TO_AFFECTATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAffectation>("toAffectation");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> TO_CORPS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps>("toCorps");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> TO_GRADE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGrade>("toGrade");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> TO_PERSONNEL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel>("toPersonnel");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRne");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> TO_TYPE_CONTRAT_TRAVAIL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail>("toTypeContratTravail");

  private static Logger LOG = Logger.getLogger(_EOContratHeberge.class);

  public EOContratHeberge localInstanceIn(EOEditingContext editingContext) {
    EOContratHeberge localInstance = (EOContratHeberge)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
    	_EOContratHeberge.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public Long ctrhInm() {
    return (Long) storedValueForKey("ctrhInm");
  }

  public void setCtrhInm(Long value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
    	_EOContratHeberge.LOG.debug( "updating ctrhInm from " + ctrhInm() + " to " + value);
    }
    takeStoredValueForKey(value, "ctrhInm");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
    	_EOContratHeberge.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
    	_EOContratHeberge.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
    	_EOContratHeberge.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
    	_EOContratHeberge.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
    	_EOContratHeberge.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toAdresse() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse)storedValueForKey("toAdresse");
  }

  public void setToAdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
      _EOContratHeberge.LOG.debug("updating toAdresse from " + toAdresse() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAdresse");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toAdresse");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOAffectation toAffectation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOAffectation)storedValueForKey("toAffectation");
  }

  public void setToAffectationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAffectation value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
      _EOContratHeberge.LOG.debug("updating toAffectation from " + toAffectation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOAffectation oldValue = toAffectation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAffectation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toAffectation");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCorps toCorps() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCorps)storedValueForKey("toCorps");
  }

  public void setToCorpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCorps value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
      _EOContratHeberge.LOG.debug("updating toCorps from " + toCorps() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCorps oldValue = toCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCorps");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCorps");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOGrade toGrade() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOGrade)storedValueForKey("toGrade");
  }

  public void setToGradeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGrade value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
      _EOContratHeberge.LOG.debug("updating toGrade from " + toGrade() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOGrade oldValue = toGrade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toGrade");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toGrade");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel toPersonnel() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel)storedValueForKey("toPersonnel");
  }

  public void setToPersonnelRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
      _EOContratHeberge.LOG.debug("updating toPersonnel from " + toPersonnel() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel oldValue = toPersonnel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPersonnel");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPersonnel");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey("toRne");
  }

  public void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
      _EOContratHeberge.LOG.debug("updating toRne from " + toRne() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRne");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toRne");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey("toStructure");
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
      _EOContratHeberge.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructure");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail toTypeContratTravail() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail)storedValueForKey("toTypeContratTravail");
  }

  public void setToTypeContratTravailRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail value) {
    if (_EOContratHeberge.LOG.isDebugEnabled()) {
      _EOContratHeberge.LOG.debug("updating toTypeContratTravail from " + toTypeContratTravail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail oldValue = toTypeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeContratTravail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeContratTravail");
    }
  }
  

  public static EOContratHeberge create(EOEditingContext editingContext, NSTimestamp dateDebut
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel toPersonnel) {
    EOContratHeberge eo = (EOContratHeberge) EOUtilities.createAndInsertInstance(editingContext, _EOContratHeberge.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setTemValide(temValide);
    eo.setToPersonnelRelationship(toPersonnel);
    return eo;
  }

  public static NSArray<EOContratHeberge> fetchAll(EOEditingContext editingContext) {
    return _EOContratHeberge.fetchAll(editingContext, null);
  }

  public static NSArray<EOContratHeberge> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOContratHeberge.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOContratHeberge> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOContratHeberge.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOContratHeberge> eoObjects = (NSArray<EOContratHeberge>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOContratHeberge fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContratHeberge.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOContratHeberge fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOContratHeberge> eoObjects = _EOContratHeberge.fetch(editingContext, qualifier, null);
    EOContratHeberge eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOContratHeberge)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_ContratHeberge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOContratHeberge fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContratHeberge.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOContratHeberge fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOContratHeberge eoObject = _EOContratHeberge.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_ContratHeberge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOContratHeberge fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOContratHeberge fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOContratHeberge eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOContratHeberge)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOContratHeberge localInstanceIn(EOEditingContext editingContext, EOContratHeberge eo) {
    EOContratHeberge localInstance = (eo == null) ? null : (EOContratHeberge)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
