/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartCompetencePoste.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartCompetencePoste extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_RepartCompetencePoste";

	// Attributes
	public static final String EVOLUTION_KEY_KEY = "evolutionKey";
	public static final String LIBELLE_KEY = "libelle";
	public static final String PERIODE_KEY = "periode";

	public static final ERXKey<Integer> EVOLUTION_KEY = new ERXKey<Integer>("evolutionKey");
	public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
	public static final ERXKey<String> PERIODE = new ERXKey<String>("periode");
	// Relationships
	public static final String TO_EVALUATION_KEY = "toEvaluation";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> TO_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>("toEvaluation");

  private static Logger LOG = Logger.getLogger(_EORepartCompetencePoste.class);

  public EORepartCompetencePoste localInstanceIn(EOEditingContext editingContext) {
    EORepartCompetencePoste localInstance = (EORepartCompetencePoste)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer evolutionKey() {
    return (Integer) storedValueForKey("evolutionKey");
  }

  public void setEvolutionKey(Integer value) {
    if (_EORepartCompetencePoste.LOG.isDebugEnabled()) {
    	_EORepartCompetencePoste.LOG.debug( "updating evolutionKey from " + evolutionKey() + " to " + value);
    }
    takeStoredValueForKey(value, "evolutionKey");
  }

  public String libelle() {
    return (String) storedValueForKey("libelle");
  }

  public void setLibelle(String value) {
    if (_EORepartCompetencePoste.LOG.isDebugEnabled()) {
    	_EORepartCompetencePoste.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, "libelle");
  }

  public String periode() {
    return (String) storedValueForKey("periode");
  }

  public void setPeriode(String value) {
    if (_EORepartCompetencePoste.LOG.isDebugEnabled()) {
    	_EORepartCompetencePoste.LOG.debug( "updating periode from " + periode() + " to " + value);
    }
    takeStoredValueForKey(value, "periode");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluation)storedValueForKey("toEvaluation");
  }

  public void setToEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation value) {
    if (_EORepartCompetencePoste.LOG.isDebugEnabled()) {
      _EORepartCompetencePoste.LOG.debug("updating toEvaluation from " + toEvaluation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluation oldValue = toEvaluation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluation");
    }
  }
  

  public static EORepartCompetencePoste create(EOEditingContext editingContext, Integer evolutionKey
, String libelle
, String periode
, org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation) {
    EORepartCompetencePoste eo = (EORepartCompetencePoste) EOUtilities.createAndInsertInstance(editingContext, _EORepartCompetencePoste.ENTITY_NAME);    
		eo.setEvolutionKey(evolutionKey);
		eo.setLibelle(libelle);
		eo.setPeriode(periode);
    eo.setToEvaluationRelationship(toEvaluation);
    return eo;
  }

  public static NSArray<EORepartCompetencePoste> fetchAll(EOEditingContext editingContext) {
    return _EORepartCompetencePoste.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartCompetencePoste> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartCompetencePoste.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartCompetencePoste> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartCompetencePoste.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartCompetencePoste> eoObjects = (NSArray<EORepartCompetencePoste>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartCompetencePoste fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartCompetencePoste.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartCompetencePoste fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartCompetencePoste> eoObjects = _EORepartCompetencePoste.fetch(editingContext, qualifier, null);
    EORepartCompetencePoste eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartCompetencePoste)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartCompetencePoste that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartCompetencePoste fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartCompetencePoste.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartCompetencePoste fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartCompetencePoste eoObject = _EORepartCompetencePoste.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartCompetencePoste that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartCompetencePoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartCompetencePoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartCompetencePoste eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartCompetencePoste)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartCompetencePoste localInstanceIn(EOEditingContext editingContext, EORepartCompetencePoste eo) {
    EORepartCompetencePoste localInstance = (eo == null) ? null : (EORepartCompetencePoste)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
