/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOTplBloc.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOTplBloc extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_TplBloc";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TBL_CODE_KEY = "tblCode";
	public static final String TBL_COMMENTAIRE_KEY = "tblCommentaire";
	public static final String TBL_FACULTATIF_KEY = "tblFacultatif";
	public static final String TBL_LIBELLE_KEY = "tblLibelle";
	public static final String TBL_POSITION_KEY = "tblPosition";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_VAL = new ERXKey<NSTimestamp>("dDebVal");
	public static final ERXKey<NSTimestamp> D_FIN_VAL = new ERXKey<NSTimestamp>("dFinVal");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> TBL_CODE = new ERXKey<String>("tblCode");
	public static final ERXKey<String> TBL_COMMENTAIRE = new ERXKey<String>("tblCommentaire");
	public static final ERXKey<String> TBL_FACULTATIF = new ERXKey<String>("tblFacultatif");
	public static final ERXKey<String> TBL_LIBELLE = new ERXKey<String>("tblLibelle");
	public static final ERXKey<Integer> TBL_POSITION = new ERXKey<Integer>("tblPosition");
	// Relationships
	public static final String TOS_REPART_FICHE_BLOC_ACTIVATION_KEY = "tosRepartFicheBlocActivation";
	public static final String TOS_TPL_ITEM_KEY = "tosTplItem";
	public static final String TO_TPL_BLOC_NATURE_KEY = "toTplBlocNature";
	public static final String TO_TPL_ONGLET_KEY = "toTplOnglet";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation> TOS_REPART_FICHE_BLOC_ACTIVATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation>("tosRepartFicheBlocActivation");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplItem> TOS_TPL_ITEM = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplItem>("tosTplItem");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplBlocNature> TO_TPL_BLOC_NATURE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplBlocNature>("toTplBlocNature");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet> TO_TPL_ONGLET = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet>("toTplOnglet");

  private static Logger LOG = Logger.getLogger(_EOTplBloc.class);

  public EOTplBloc localInstanceIn(EOEditingContext editingContext) {
    EOTplBloc localInstance = (EOTplBloc)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
    	_EOTplBloc.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey("dDebVal");
  }

  public void setDDebVal(NSTimestamp value) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
    	_EOTplBloc.LOG.debug( "updating dDebVal from " + dDebVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVal");
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey("dFinVal");
  }

  public void setDFinVal(NSTimestamp value) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
    	_EOTplBloc.LOG.debug( "updating dFinVal from " + dFinVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVal");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
    	_EOTplBloc.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String tblCode() {
    return (String) storedValueForKey("tblCode");
  }

  public void setTblCode(String value) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
    	_EOTplBloc.LOG.debug( "updating tblCode from " + tblCode() + " to " + value);
    }
    takeStoredValueForKey(value, "tblCode");
  }

  public String tblCommentaire() {
    return (String) storedValueForKey("tblCommentaire");
  }

  public void setTblCommentaire(String value) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
    	_EOTplBloc.LOG.debug( "updating tblCommentaire from " + tblCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "tblCommentaire");
  }

  public String tblFacultatif() {
    return (String) storedValueForKey("tblFacultatif");
  }

  public void setTblFacultatif(String value) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
    	_EOTplBloc.LOG.debug( "updating tblFacultatif from " + tblFacultatif() + " to " + value);
    }
    takeStoredValueForKey(value, "tblFacultatif");
  }

  public String tblLibelle() {
    return (String) storedValueForKey("tblLibelle");
  }

  public void setTblLibelle(String value) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
    	_EOTplBloc.LOG.debug( "updating tblLibelle from " + tblLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "tblLibelle");
  }

  public Integer tblPosition() {
    return (Integer) storedValueForKey("tblPosition");
  }

  public void setTblPosition(Integer value) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
    	_EOTplBloc.LOG.debug( "updating tblPosition from " + tblPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "tblPosition");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOTplBlocNature toTplBlocNature() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplBlocNature)storedValueForKey("toTplBlocNature");
  }

  public void setToTplBlocNatureRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplBlocNature value) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
      _EOTplBloc.LOG.debug("updating toTplBlocNature from " + toTplBlocNature() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOTplBlocNature oldValue = toTplBlocNature();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTplBlocNature");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTplBlocNature");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet toTplOnglet() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet)storedValueForKey("toTplOnglet");
  }

  public void setToTplOngletRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet value) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
      _EOTplBloc.LOG.debug("updating toTplOnglet from " + toTplOnglet() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet oldValue = toTplOnglet();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTplOnglet");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTplOnglet");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation> tosRepartFicheBlocActivation() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation>)storedValueForKey("tosRepartFicheBlocActivation");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation> tosRepartFicheBlocActivation(EOQualifier qualifier) {
    return tosRepartFicheBlocActivation(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation> tosRepartFicheBlocActivation(EOQualifier qualifier, boolean fetch) {
    return tosRepartFicheBlocActivation(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation> tosRepartFicheBlocActivation(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation.TO_TPL_BLOC_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartFicheBlocActivation();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartFicheBlocActivationRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation object) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
      _EOTplBloc.LOG.debug("adding " + object + " to tosRepartFicheBlocActivation relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartFicheBlocActivation");
  }

  public void removeFromTosRepartFicheBlocActivationRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation object) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
      _EOTplBloc.LOG.debug("removing " + object + " from tosRepartFicheBlocActivation relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFicheBlocActivation");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation createTosRepartFicheBlocActivationRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartFicheBlocActivation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartFicheBlocActivation");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation) eo;
  }

  public void deleteTosRepartFicheBlocActivationRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFicheBlocActivation");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartFicheBlocActivationRelationships() {
    Enumeration objects = tosRepartFicheBlocActivation().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartFicheBlocActivationRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItem> tosTplItem() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItem>)storedValueForKey("tosTplItem");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItem> tosTplItem(EOQualifier qualifier) {
    return tosTplItem(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItem> tosTplItem(EOQualifier qualifier, boolean fetch) {
    return tosTplItem(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItem> tosTplItem(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItem> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOTplItem.TO_TPL_BLOC_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOTplItem.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosTplItem();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItem>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplItem>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosTplItemRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplItem object) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
      _EOTplBloc.LOG.debug("adding " + object + " to tosTplItem relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosTplItem");
  }

  public void removeFromTosTplItemRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplItem object) {
    if (_EOTplBloc.LOG.isDebugEnabled()) {
      _EOTplBloc.LOG.debug("removing " + object + " from tosTplItem relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosTplItem");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOTplItem createTosTplItemRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_TplItem");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosTplItem");
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplItem) eo;
  }

  public void deleteTosTplItemRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplItem object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosTplItem");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosTplItemRelationships() {
    Enumeration objects = tosTplItem().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosTplItemRelationship((org.cocktail.fwkcktlgrh.common.metier.EOTplItem)objects.nextElement());
    }
  }


  public static EOTplBloc create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String tblCode
, String tblFacultatif
, String tblLibelle
, Integer tblPosition
, org.cocktail.fwkcktlgrh.common.metier.EOTplBlocNature toTplBlocNature, org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet toTplOnglet) {
    EOTplBloc eo = (EOTplBloc) EOUtilities.createAndInsertInstance(editingContext, _EOTplBloc.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTblCode(tblCode);
		eo.setTblFacultatif(tblFacultatif);
		eo.setTblLibelle(tblLibelle);
		eo.setTblPosition(tblPosition);
    eo.setToTplBlocNatureRelationship(toTplBlocNature);
    eo.setToTplOngletRelationship(toTplOnglet);
    return eo;
  }

  public static NSArray<EOTplBloc> fetchAll(EOEditingContext editingContext) {
    return _EOTplBloc.fetchAll(editingContext, null);
  }

  public static NSArray<EOTplBloc> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTplBloc.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOTplBloc> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTplBloc.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTplBloc> eoObjects = (NSArray<EOTplBloc>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTplBloc fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTplBloc.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTplBloc fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTplBloc> eoObjects = _EOTplBloc.fetch(editingContext, qualifier, null);
    EOTplBloc eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTplBloc)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_TplBloc that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTplBloc fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTplBloc.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOTplBloc fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTplBloc eoObject = _EOTplBloc.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_TplBloc that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOTplBloc fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTplBloc fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTplBloc eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTplBloc)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOTplBloc localInstanceIn(EOEditingContext editingContext, EOTplBloc eo) {
    EOTplBloc localInstance = (eo == null) ? null : (EOTplBloc)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
