/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IRepartFicheItem;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_RepartFormation;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_ToEvaluation;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EORepartFicheItem extends _EORepartFicheItem implements I_RepartFormation, I_ToEvaluation, IRepartFicheItem {
  private static Logger log = Logger.getLogger(EORepartFicheItem.class);

  public EORepartFicheItem() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }

  //ajouts

	public String libelleFormation() {
		return rfiValeurLibre();
	}

	public boolean isNomenclature() {
		return false;
	}

	/**
	 * TODO faire generique recFiche : la fiche (fiche de poste, fiche LOLF ou
	 * fiche d'evaluation) Retrouver l'enregistrement associe a une fiche pour un
	 * item
	 * 
	 * @param ec
	 * @param itemCode
	 *          : le code du type de l'item
	 * @param recEvaluation
	 *          : la fiche (fiche de poste, fiche LOLF ou fiche d'evaluation)
	 * @return <em>null</em> si non trouvÈ.
	 */
	public static EORepartFicheItem findRecordForItemCodeInContext(
				EOEditingContext ec, String itemCode, EOEvaluation recEvaluation) {
		EORepartFicheItem result = null;
		try {
		result = EORepartFicheItem.fetchRequired(
						ec, CktlDataBus.newCondition(
								EORepartFicheItem.TO_TPL_ITEM_KEY + "." + EOTplItem.TIT_CODE_KEY + " = %@ AND " + EORepartFicheItem.TO_EVALUATION_KEY + " = %@",
								new NSArray<Object>(
										new Object[] {
												itemCode, recEvaluation })));
		} catch (NoSuchElementException e) {
			// si pas d'élément, catch de l'exception 
		}
		return result;
	}
}
