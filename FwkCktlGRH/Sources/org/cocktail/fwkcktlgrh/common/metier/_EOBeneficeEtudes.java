/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOBeneficeEtudes.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOBeneficeEtudes extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_BeneficeEtudes";

	// Attributes
	public static final String BETU_DUREE_KEY = "betuDuree";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> BETU_DUREE = new ERXKey<String>("betuDuree");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_ECOLE_MILITAIRE_KEY = "toEcoleMilitaire";
	public static final String TO_INDIVIDU_KEY = "toIndividu";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires> TO_ECOLE_MILITAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires>("toEcoleMilitaire");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");

  private static Logger LOG = Logger.getLogger(_EOBeneficeEtudes.class);

  public EOBeneficeEtudes localInstanceIn(EOEditingContext editingContext) {
    EOBeneficeEtudes localInstance = (EOBeneficeEtudes)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String betuDuree() {
    return (String) storedValueForKey("betuDuree");
  }

  public void setBetuDuree(String value) {
    if (_EOBeneficeEtudes.LOG.isDebugEnabled()) {
    	_EOBeneficeEtudes.LOG.debug( "updating betuDuree from " + betuDuree() + " to " + value);
    }
    takeStoredValueForKey(value, "betuDuree");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOBeneficeEtudes.LOG.isDebugEnabled()) {
    	_EOBeneficeEtudes.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOBeneficeEtudes.LOG.isDebugEnabled()) {
    	_EOBeneficeEtudes.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOBeneficeEtudes.LOG.isDebugEnabled()) {
    	_EOBeneficeEtudes.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOBeneficeEtudes.LOG.isDebugEnabled()) {
    	_EOBeneficeEtudes.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOBeneficeEtudes.LOG.isDebugEnabled()) {
    	_EOBeneficeEtudes.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires toEcoleMilitaire() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires)storedValueForKey("toEcoleMilitaire");
  }

  public void setToEcoleMilitaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires value) {
    if (_EOBeneficeEtudes.LOG.isDebugEnabled()) {
      _EOBeneficeEtudes.LOG.debug("updating toEcoleMilitaire from " + toEcoleMilitaire() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires oldValue = toEcoleMilitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEcoleMilitaire");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEcoleMilitaire");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOBeneficeEtudes.LOG.isDebugEnabled()) {
      _EOBeneficeEtudes.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  

  public static EOBeneficeEtudes create(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires toEcoleMilitaire, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu) {
    EOBeneficeEtudes eo = (EOBeneficeEtudes) EOUtilities.createAndInsertInstance(editingContext, _EOBeneficeEtudes.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setToEcoleMilitaireRelationship(toEcoleMilitaire);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  public static NSArray<EOBeneficeEtudes> fetchAll(EOEditingContext editingContext) {
    return _EOBeneficeEtudes.fetchAll(editingContext, null);
  }

  public static NSArray<EOBeneficeEtudes> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOBeneficeEtudes.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOBeneficeEtudes> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOBeneficeEtudes.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOBeneficeEtudes> eoObjects = (NSArray<EOBeneficeEtudes>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOBeneficeEtudes fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBeneficeEtudes.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBeneficeEtudes fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOBeneficeEtudes> eoObjects = _EOBeneficeEtudes.fetch(editingContext, qualifier, null);
    EOBeneficeEtudes eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOBeneficeEtudes)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_BeneficeEtudes that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBeneficeEtudes fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBeneficeEtudes.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOBeneficeEtudes fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOBeneficeEtudes eoObject = _EOBeneficeEtudes.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_BeneficeEtudes that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOBeneficeEtudes fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBeneficeEtudes fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBeneficeEtudes eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBeneficeEtudes)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOBeneficeEtudes localInstanceIn(EOEditingContext editingContext, EOBeneficeEtudes eo) {
    EOBeneficeEtudes localInstance = (eo == null) ? null : (EOBeneficeEtudes)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
