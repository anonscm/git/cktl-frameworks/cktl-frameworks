/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOVacataires.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOVacataires extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Vacataires";

	// Attributes
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String LIBELLE_ENSEIGNEMENT_KEY = "libelleEnseignement";
	public static final String NB_HEURES_KEY = "nbHeures";
	public static final String NB_HEURES_REALISEES_KEY = "nbHeuresRealisees";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String OBSERVATIONS_KEY = "observations";
	public static final String TAUX_HORAIRE_KEY = "tauxHoraire";
	public static final String TAUX_HORAIRE_REALISE_KEY = "tauxHoraireRealise";
	public static final String TEMOIN_ENSEIGNANT_KEY = "temoinEnseignant";
	public static final String TEMOIN_PAYE_KEY = "temoinPaye";
	public static final String TEMOIN_PERSONNEL_ETABLISSEMENT_KEY = "temoinPersonnelEtablissement";
	public static final String TEMOIN_SIGNE_KEY = "temoinSigne";
	public static final String TEMOIN_TITULAIRE_KEY = "temoinTitulaire";
	public static final String TEMOIN_VALIDE_KEY = "temoinValide";

	public static final ERXKey<NSTimestamp> DATE_ARRETE = new ERXKey<NSTimestamp>("dateArrete");
	public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
	public static final ERXKey<String> LIBELLE_ENSEIGNEMENT = new ERXKey<String>("libelleEnseignement");
	public static final ERXKey<Double> NB_HEURES = new ERXKey<Double>("nbHeures");
	public static final ERXKey<Double> NB_HEURES_REALISEES = new ERXKey<Double>("nbHeuresRealisees");
	public static final ERXKey<String> NO_ARRETE = new ERXKey<String>("noArrete");
	public static final ERXKey<String> OBSERVATIONS = new ERXKey<String>("observations");
	public static final ERXKey<Double> TAUX_HORAIRE = new ERXKey<Double>("tauxHoraire");
	public static final ERXKey<Double> TAUX_HORAIRE_REALISE = new ERXKey<Double>("tauxHoraireRealise");
	public static final ERXKey<String> TEMOIN_ENSEIGNANT = new ERXKey<String>("temoinEnseignant");
	public static final ERXKey<String> TEMOIN_PAYE = new ERXKey<String>("temoinPaye");
	public static final ERXKey<String> TEMOIN_PERSONNEL_ETABLISSEMENT = new ERXKey<String>("temoinPersonnelEtablissement");
	public static final ERXKey<String> TEMOIN_SIGNE = new ERXKey<String>("temoinSigne");
	public static final ERXKey<String> TEMOIN_TITULAIRE = new ERXKey<String>("temoinTitulaire");
	public static final ERXKey<String> TEMOIN_VALIDE = new ERXKey<String>("temoinValide");
	// Relationships
	public static final String LISTE_VACATAIRES_AFFECTATIONS_KEY = "listeVacatairesAffectations";
	public static final String TO_ADRESSE_KEY = "toAdresse";
	public static final String TO_CNU_KEY = "toCnu";
	public static final String TO_CORPS_KEY = "toCorps";
	public static final String TO_GRADE_KEY = "toGrade";
	public static final String TO_INDIVIDU_VACATAIRE_KEY = "toIndividuVacataire";
	public static final String TO_PERSONNE_CREATION_KEY = "toPersonneCreation";
	public static final String TO_PERSONNE_MODIFICATION_KEY = "toPersonneModification";
	public static final String TO_PROFESSION_KEY = "toProfession";
	public static final String TO_STRUCTURE_KEY = "toStructure";
	public static final String TO_TYPE_CONTRAT_TRAVAIL_KEY = "toTypeContratTravail";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation> LISTE_VACATAIRES_AFFECTATIONS = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation>("listeVacatairesAffectations");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_ADRESSE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toAdresse");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> TO_CNU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu>("toCnu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> TO_CORPS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps>("toCorps");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> TO_GRADE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGrade>("toGrade");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU_VACATAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividuVacataire");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneCreation");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneModification");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession> TO_PROFESSION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession>("toProfession");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> TO_TYPE_CONTRAT_TRAVAIL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail>("toTypeContratTravail");

  private static Logger LOG = Logger.getLogger(_EOVacataires.class);

  public EOVacataires localInstanceIn(EOEditingContext editingContext) {
    EOVacataires localInstance = (EOVacataires)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey("dateCreation");
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dateCreation");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey("dateModification");
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dateModification");
  }

  public String libelleEnseignement() {
    return (String) storedValueForKey("libelleEnseignement");
  }

  public void setLibelleEnseignement(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating libelleEnseignement from " + libelleEnseignement() + " to " + value);
    }
    takeStoredValueForKey(value, "libelleEnseignement");
  }

  public Double nbHeures() {
    return (Double) storedValueForKey("nbHeures");
  }

  public void setNbHeures(Double value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating nbHeures from " + nbHeures() + " to " + value);
    }
    takeStoredValueForKey(value, "nbHeures");
  }

  public Double nbHeuresRealisees() {
    return (Double) storedValueForKey("nbHeuresRealisees");
  }

  public void setNbHeuresRealisees(Double value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating nbHeuresRealisees from " + nbHeuresRealisees() + " to " + value);
    }
    takeStoredValueForKey(value, "nbHeuresRealisees");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String observations() {
    return (String) storedValueForKey("observations");
  }

  public void setObservations(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating observations from " + observations() + " to " + value);
    }
    takeStoredValueForKey(value, "observations");
  }

  public Double tauxHoraire() {
    return (Double) storedValueForKey("tauxHoraire");
  }

  public void setTauxHoraire(Double value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating tauxHoraire from " + tauxHoraire() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxHoraire");
  }

  public Double tauxHoraireRealise() {
    return (Double) storedValueForKey("tauxHoraireRealise");
  }

  public void setTauxHoraireRealise(Double value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating tauxHoraireRealise from " + tauxHoraireRealise() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxHoraireRealise");
  }

  public String temoinEnseignant() {
    return (String) storedValueForKey("temoinEnseignant");
  }

  public void setTemoinEnseignant(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temoinEnseignant from " + temoinEnseignant() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinEnseignant");
  }

  public String temoinPaye() {
    return (String) storedValueForKey("temoinPaye");
  }

  public void setTemoinPaye(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temoinPaye from " + temoinPaye() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinPaye");
  }

  public String temoinPersonnelEtablissement() {
    return (String) storedValueForKey("temoinPersonnelEtablissement");
  }

  public void setTemoinPersonnelEtablissement(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temoinPersonnelEtablissement from " + temoinPersonnelEtablissement() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinPersonnelEtablissement");
  }

  public String temoinSigne() {
    return (String) storedValueForKey("temoinSigne");
  }

  public void setTemoinSigne(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temoinSigne from " + temoinSigne() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinSigne");
  }

  public String temoinTitulaire() {
    return (String) storedValueForKey("temoinTitulaire");
  }

  public void setTemoinTitulaire(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temoinTitulaire from " + temoinTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinTitulaire");
  }

  public String temoinValide() {
    return (String) storedValueForKey("temoinValide");
  }

  public void setTemoinValide(String value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
    	_EOVacataires.LOG.debug( "updating temoinValide from " + temoinValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temoinValide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toAdresse() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse)storedValueForKey("toAdresse");
  }

  public void setToAdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toAdresse from " + toAdresse() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAdresse");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toAdresse");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCnu toCnu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCnu)storedValueForKey("toCnu");
  }

  public void setToCnuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCnu value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toCnu from " + toCnu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCnu oldValue = toCnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCnu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCnu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCorps toCorps() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCorps)storedValueForKey("toCorps");
  }

  public void setToCorpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCorps value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toCorps from " + toCorps() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCorps oldValue = toCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCorps");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCorps");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOGrade toGrade() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOGrade)storedValueForKey("toGrade");
  }

  public void setToGradeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGrade value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toGrade from " + toGrade() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOGrade oldValue = toGrade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toGrade");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toGrade");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividuVacataire() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividuVacataire");
  }

  public void setToIndividuVacataireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toIndividuVacataire from " + toIndividuVacataire() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividuVacataire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividuVacataire");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividuVacataire");
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey("toPersonneCreation");
  }

  public void setToPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toPersonneCreation from " + toPersonneCreation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPersonneCreation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPersonneCreation");
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey("toPersonneModification");
  }

  public void setToPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toPersonneModification from " + toPersonneModification() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPersonneModification");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPersonneModification");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOProfession toProfession() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOProfession)storedValueForKey("toProfession");
  }

  public void setToProfessionRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOProfession value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toProfession from " + toProfession() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOProfession oldValue = toProfession();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toProfession");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toProfession");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey("toStructure");
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructure");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail toTypeContratTravail() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail)storedValueForKey("toTypeContratTravail");
  }

  public void setToTypeContratTravailRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail value) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("updating toTypeContratTravail from " + toTypeContratTravail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail oldValue = toTypeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeContratTravail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeContratTravail");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation> listeVacatairesAffectations() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation>)storedValueForKey("listeVacatairesAffectations");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation> listeVacatairesAffectations(EOQualifier qualifier) {
    return listeVacatairesAffectations(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation> listeVacatairesAffectations(EOQualifier qualifier, boolean fetch) {
    return listeVacatairesAffectations(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation> listeVacatairesAffectations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation.TO_VACATAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = listeVacatairesAffectations();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToListeVacatairesAffectationsRelationship(org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation object) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("adding " + object + " to listeVacatairesAffectations relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "listeVacatairesAffectations");
  }

  public void removeFromListeVacatairesAffectationsRelationship(org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation object) {
    if (_EOVacataires.LOG.isDebugEnabled()) {
      _EOVacataires.LOG.debug("removing " + object + " from listeVacatairesAffectations relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "listeVacatairesAffectations");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation createListeVacatairesAffectationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_VacatairesAffectation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "listeVacatairesAffectations");
    return (org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation) eo;
  }

  public void deleteListeVacatairesAffectationsRelationship(org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "listeVacatairesAffectations");
    editingContext().deleteObject(object);
  }

  public void deleteAllListeVacatairesAffectationsRelationships() {
    Enumeration objects = listeVacatairesAffectations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteListeVacatairesAffectationsRelationship((org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation)objects.nextElement());
    }
  }


  public static EOVacataires create(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dateModification
, String temoinEnseignant
, String temoinPaye
, String temoinPersonnelEtablissement
, String temoinSigne
, String temoinTitulaire
, String temoinValide
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividuVacataire, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification) {
    EOVacataires eo = (EOVacataires) EOUtilities.createAndInsertInstance(editingContext, _EOVacataires.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDateModification(dateModification);
		eo.setTemoinEnseignant(temoinEnseignant);
		eo.setTemoinPaye(temoinPaye);
		eo.setTemoinPersonnelEtablissement(temoinPersonnelEtablissement);
		eo.setTemoinSigne(temoinSigne);
		eo.setTemoinTitulaire(temoinTitulaire);
		eo.setTemoinValide(temoinValide);
    eo.setToIndividuVacataireRelationship(toIndividuVacataire);
    eo.setToPersonneCreationRelationship(toPersonneCreation);
    eo.setToPersonneModificationRelationship(toPersonneModification);
    return eo;
  }

  public static NSArray<EOVacataires> fetchAll(EOEditingContext editingContext) {
    return _EOVacataires.fetchAll(editingContext, null);
  }

  public static NSArray<EOVacataires> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVacataires.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOVacataires> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOVacataires.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVacataires> eoObjects = (NSArray<EOVacataires>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOVacataires fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVacataires.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVacataires fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVacataires> eoObjects = _EOVacataires.fetch(editingContext, qualifier, null);
    EOVacataires eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOVacataires)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Vacataires that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVacataires fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVacataires.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOVacataires fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVacataires eoObject = _EOVacataires.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Vacataires that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOVacataires fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVacataires fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVacataires eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVacataires)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOVacataires localInstanceIn(EOEditingContext editingContext, EOVacataires eo) {
    EOVacataires localInstance = (eo == null) ? null : (EOVacataires)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
