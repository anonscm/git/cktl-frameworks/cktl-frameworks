/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.ITplItem;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOTplItem extends _EOTplItem implements ITplItem {
  private static Logger log = Logger.getLogger(EOTplItem.class);

  public EOTplItem() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }
  
	// methodes rajoutees

	// noms des items
	public static final String CODE_FORMATIONS_SOUHAITEES = "LISFORMSOU";
	public static final String CODE_APPRECIATION_GENERALE = "APGENVALAB";

	public boolean isListe() {
		return toTplItemNature().tinLibelle().equals(EOTplItemNature.TPL_ITEM_NATURE_LISTE);
	}

	public boolean isChampLibre() {
		return toTplItemNature().tinLibelle().equals(EOTplItemNature.TPL_ITEM_NATURE_CHAMP_LIBRE);
	}

	public boolean isTexteStatique() {
		return toTplItemNature().tinLibelle().equals(EOTplItemNature.TPL_ITEM_NATURE_TEXTE_STATIQUE);
	}

	/**
	 * La liste des repartitions associées sur une période
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public NSArray<EOTplRepartItemItemValeur> tosTplRepartItemItemValeur(NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSArray<EOTplRepartItemItemValeur> result = null;

		//
		EOQualifier qual = getValiditeQualifier(
				EOTplRepartItemItemValeur.D_DEB_VAL_KEY, EOTplRepartItemItemValeur.D_FIN_VAL_KEY, dateDebut, dateFin);

		result = tosTplRepartItemItemValeur(qual);

		result = CktlSort.sortedArray(result, EOTplRepartItemItemValeur.TIV_POSITION_KEY);

		return result;
	}

	/**
	 * TODO faire generique recFiche : la fiche (fiche de poste, fiche LOLF ou
	 * fiche d'evaluation) Retrouver un enregistrement associ à une évaluation
	 * 
	 * @param recEvaluation
	 *          : la fiche (fiche de poste, fiche LOLF ou fiche d'evaluation)
	 * @return <em>null</em> si non trouvé.
	 * @return
	 */
	public EORepartFicheItem getRepartItemForEvaluation(
			EOEvaluation eoEvalution) {
		EORepartFicheItem result = null;

		EOQualifier qual = CktlDataBus.newCondition(
				EORepartFicheItem.TO_EVALUATION_KEY + "=%@",
				new NSArray<EOEvaluation>(eoEvalution));

		NSArray<EORepartFicheItem> array = tosRepartFicheItem(qual);

		if (array.count() > 0) {
			result = array.objectAtIndex(0);
		}

		return result;
	}

	/**
	 * Retrouver la valeur d'un item de type champ libre. Se base sur un
	 * enregistrement du type <code>EORepartFicheItem</code>. Si non trouvé, alors
	 * la chaine vide est retournee.
	 */
	public String getStrChampLibre(EOEvaluation eoEvaluation) {
		String strResult = StringCtrl.emptyString();
		// trouver l'enregistrement pour cette fiche
		EORepartFicheItem recRepartFicheItem = getRepartItemForEvaluation(eoEvaluation);
		// si existant, alors on va retourner la valeur associee
		if (recRepartFicheItem != null) {
			strResult = recRepartFicheItem.rfiValeurLibre();
		}
		return strResult;
	}
  
}
