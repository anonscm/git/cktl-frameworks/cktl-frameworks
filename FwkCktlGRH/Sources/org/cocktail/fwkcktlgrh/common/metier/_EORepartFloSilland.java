/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartFloSilland.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartFloSilland extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_RepartFloSilland";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String FLO_KEY_KEY = "floKey";
	public static final String RFS_QUOTITE_KEY = "rfsQuotite";
	public static final String SIL_KEY_KEY = "silKey";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> FLO_KEY = new ERXKey<Integer>("floKey");
	public static final ERXKey<Double> RFS_QUOTITE = new ERXKey<Double>("rfsQuotite");
	public static final ERXKey<Integer> SIL_KEY = new ERXKey<Integer>("silKey");
	// Relationships
	public static final String TO_FCT_SILLAND_KEY = "toFctSilland";
	public static final String TO_FICHE_LOLF_KEY = "toFicheLolf";
	public static final String TOS_REPART_FLO_LOLF_NOMEN_KEY = "tosRepartFloLolfNomen";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland> TO_FCT_SILLAND = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland>("toFctSilland");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf> TO_FICHE_LOLF = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf>("toFicheLolf");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen> TOS_REPART_FLO_LOLF_NOMEN = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen>("tosRepartFloLolfNomen");

  private static Logger LOG = Logger.getLogger(_EORepartFloSilland.class);

  public EORepartFloSilland localInstanceIn(EOEditingContext editingContext) {
    EORepartFloSilland localInstance = (EORepartFloSilland)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartFloSilland.LOG.isDebugEnabled()) {
    	_EORepartFloSilland.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartFloSilland.LOG.isDebugEnabled()) {
    	_EORepartFloSilland.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer floKey() {
    return (Integer) storedValueForKey("floKey");
  }

  public void setFloKey(Integer value) {
    if (_EORepartFloSilland.LOG.isDebugEnabled()) {
    	_EORepartFloSilland.LOG.debug( "updating floKey from " + floKey() + " to " + value);
    }
    takeStoredValueForKey(value, "floKey");
  }

  public Double rfsQuotite() {
    return (Double) storedValueForKey("rfsQuotite");
  }

  public void setRfsQuotite(Double value) {
    if (_EORepartFloSilland.LOG.isDebugEnabled()) {
    	_EORepartFloSilland.LOG.debug( "updating rfsQuotite from " + rfsQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "rfsQuotite");
  }

  public Integer silKey() {
    return (Integer) storedValueForKey("silKey");
  }

  public void setSilKey(Integer value) {
    if (_EORepartFloSilland.LOG.isDebugEnabled()) {
    	_EORepartFloSilland.LOG.debug( "updating silKey from " + silKey() + " to " + value);
    }
    takeStoredValueForKey(value, "silKey");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland toFctSilland() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland)storedValueForKey("toFctSilland");
  }

  public void setToFctSillandRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland value) {
    if (_EORepartFloSilland.LOG.isDebugEnabled()) {
      _EORepartFloSilland.LOG.debug("updating toFctSilland from " + toFctSilland() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland oldValue = toFctSilland();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFctSilland");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFctSilland");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf toFicheLolf() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf)storedValueForKey("toFicheLolf");
  }

  public void setToFicheLolfRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf value) {
    if (_EORepartFloSilland.LOG.isDebugEnabled()) {
      _EORepartFloSilland.LOG.debug("updating toFicheLolf from " + toFicheLolf() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf oldValue = toFicheLolf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFicheLolf");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFicheLolf");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen> tosRepartFloLolfNomen() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen>)storedValueForKey("tosRepartFloLolfNomen");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen> tosRepartFloLolfNomen(EOQualifier qualifier) {
    return tosRepartFloLolfNomen(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen> tosRepartFloLolfNomen(EOQualifier qualifier, boolean fetch) {
    return tosRepartFloLolfNomen(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen> tosRepartFloLolfNomen(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen.TO_REPART_FLO_SILLAND_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartFloLolfNomen();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartFloLolfNomenRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen object) {
    if (_EORepartFloSilland.LOG.isDebugEnabled()) {
      _EORepartFloSilland.LOG.debug("adding " + object + " to tosRepartFloLolfNomen relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartFloLolfNomen");
  }

  public void removeFromTosRepartFloLolfNomenRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen object) {
    if (_EORepartFloSilland.LOG.isDebugEnabled()) {
      _EORepartFloSilland.LOG.debug("removing " + object + " from tosRepartFloLolfNomen relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFloLolfNomen");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen createTosRepartFloLolfNomenRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartFloLolfNomen");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartFloLolfNomen");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen) eo;
  }

  public void deleteTosRepartFloLolfNomenRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFloLolfNomen");
  }

  public void deleteAllTosRepartFloLolfNomenRelationships() {
    Enumeration objects = tosRepartFloLolfNomen().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartFloLolfNomenRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartFloLolfNomen)objects.nextElement());
    }
  }


  public static EORepartFloSilland create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer floKey
, Double rfsQuotite
, Integer silKey
, org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland toFctSilland, org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf toFicheLolf) {
    EORepartFloSilland eo = (EORepartFloSilland) EOUtilities.createAndInsertInstance(editingContext, _EORepartFloSilland.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setFloKey(floKey);
		eo.setRfsQuotite(rfsQuotite);
		eo.setSilKey(silKey);
    eo.setToFctSillandRelationship(toFctSilland);
    eo.setToFicheLolfRelationship(toFicheLolf);
    return eo;
  }

  public static NSArray<EORepartFloSilland> fetchAll(EOEditingContext editingContext) {
    return _EORepartFloSilland.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartFloSilland> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartFloSilland.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartFloSilland> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartFloSilland.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartFloSilland> eoObjects = (NSArray<EORepartFloSilland>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartFloSilland fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartFloSilland.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartFloSilland fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartFloSilland> eoObjects = _EORepartFloSilland.fetch(editingContext, qualifier, null);
    EORepartFloSilland eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartFloSilland)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartFloSilland that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartFloSilland fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartFloSilland.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartFloSilland fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartFloSilland eoObject = _EORepartFloSilland.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartFloSilland that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartFloSilland fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartFloSilland fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartFloSilland eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartFloSilland)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartFloSilland localInstanceIn(EOEditingContext editingContext, EORepartFloSilland eo) {
    EORepartFloSilland localInstance = (eo == null) ? null : (EORepartFloSilland)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
