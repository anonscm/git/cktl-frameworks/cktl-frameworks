/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOEvaluationNoticePromotion.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOEvaluationNoticePromotion extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_EvaluationNoticePromotion";

	// Attributes
	public static final String ENP_APPRECIATION_GENERALE_KEY = "enpAppreciationGenerale";
	public static final String ENP_PROMOTION_CORPS_KEY = "enpPromotionCorps";
	public static final String ENP_PROMOTION_CORPS_REFUS_MOTIF_KEY = "enpPromotionCorpsRefusMotif";
	public static final String ENP_PROMOTION_GRADE_KEY = "enpPromotionGrade";
	public static final String ENP_PROMOTION_GRADE_REFUS_MOTIF_KEY = "enpPromotionGradeRefusMotif";
	public static final String ENP_REDUCTION_ECHELON_KEY = "enpReductionEchelon";
	public static final String ENP_REDUCTION_ECHELON_REFUS_MOTIF_KEY = "enpReductionEchelonRefusMotif";

	public static final ERXKey<String> ENP_APPRECIATION_GENERALE = new ERXKey<String>("enpAppreciationGenerale");
	public static final ERXKey<Integer> ENP_PROMOTION_CORPS = new ERXKey<Integer>("enpPromotionCorps");
	public static final ERXKey<String> ENP_PROMOTION_CORPS_REFUS_MOTIF = new ERXKey<String>("enpPromotionCorpsRefusMotif");
	public static final ERXKey<Integer> ENP_PROMOTION_GRADE = new ERXKey<Integer>("enpPromotionGrade");
	public static final ERXKey<String> ENP_PROMOTION_GRADE_REFUS_MOTIF = new ERXKey<String>("enpPromotionGradeRefusMotif");
	public static final ERXKey<Integer> ENP_REDUCTION_ECHELON = new ERXKey<Integer>("enpReductionEchelon");
	public static final ERXKey<String> ENP_REDUCTION_ECHELON_REFUS_MOTIF = new ERXKey<String>("enpReductionEchelonRefusMotif");
	// Relationships
	public static final String TO_EVALUATION_KEY = "toEvaluation";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> TO_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>("toEvaluation");

  private static Logger LOG = Logger.getLogger(_EOEvaluationNoticePromotion.class);

  public EOEvaluationNoticePromotion localInstanceIn(EOEditingContext editingContext) {
    EOEvaluationNoticePromotion localInstance = (EOEvaluationNoticePromotion)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String enpAppreciationGenerale() {
    return (String) storedValueForKey("enpAppreciationGenerale");
  }

  public void setEnpAppreciationGenerale(String value) {
    if (_EOEvaluationNoticePromotion.LOG.isDebugEnabled()) {
    	_EOEvaluationNoticePromotion.LOG.debug( "updating enpAppreciationGenerale from " + enpAppreciationGenerale() + " to " + value);
    }
    takeStoredValueForKey(value, "enpAppreciationGenerale");
  }

  public Integer enpPromotionCorps() {
    return (Integer) storedValueForKey("enpPromotionCorps");
  }

  public void setEnpPromotionCorps(Integer value) {
    if (_EOEvaluationNoticePromotion.LOG.isDebugEnabled()) {
    	_EOEvaluationNoticePromotion.LOG.debug( "updating enpPromotionCorps from " + enpPromotionCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "enpPromotionCorps");
  }

  public String enpPromotionCorpsRefusMotif() {
    return (String) storedValueForKey("enpPromotionCorpsRefusMotif");
  }

  public void setEnpPromotionCorpsRefusMotif(String value) {
    if (_EOEvaluationNoticePromotion.LOG.isDebugEnabled()) {
    	_EOEvaluationNoticePromotion.LOG.debug( "updating enpPromotionCorpsRefusMotif from " + enpPromotionCorpsRefusMotif() + " to " + value);
    }
    takeStoredValueForKey(value, "enpPromotionCorpsRefusMotif");
  }

  public Integer enpPromotionGrade() {
    return (Integer) storedValueForKey("enpPromotionGrade");
  }

  public void setEnpPromotionGrade(Integer value) {
    if (_EOEvaluationNoticePromotion.LOG.isDebugEnabled()) {
    	_EOEvaluationNoticePromotion.LOG.debug( "updating enpPromotionGrade from " + enpPromotionGrade() + " to " + value);
    }
    takeStoredValueForKey(value, "enpPromotionGrade");
  }

  public String enpPromotionGradeRefusMotif() {
    return (String) storedValueForKey("enpPromotionGradeRefusMotif");
  }

  public void setEnpPromotionGradeRefusMotif(String value) {
    if (_EOEvaluationNoticePromotion.LOG.isDebugEnabled()) {
    	_EOEvaluationNoticePromotion.LOG.debug( "updating enpPromotionGradeRefusMotif from " + enpPromotionGradeRefusMotif() + " to " + value);
    }
    takeStoredValueForKey(value, "enpPromotionGradeRefusMotif");
  }

  public Integer enpReductionEchelon() {
    return (Integer) storedValueForKey("enpReductionEchelon");
  }

  public void setEnpReductionEchelon(Integer value) {
    if (_EOEvaluationNoticePromotion.LOG.isDebugEnabled()) {
    	_EOEvaluationNoticePromotion.LOG.debug( "updating enpReductionEchelon from " + enpReductionEchelon() + " to " + value);
    }
    takeStoredValueForKey(value, "enpReductionEchelon");
  }

  public String enpReductionEchelonRefusMotif() {
    return (String) storedValueForKey("enpReductionEchelonRefusMotif");
  }

  public void setEnpReductionEchelonRefusMotif(String value) {
    if (_EOEvaluationNoticePromotion.LOG.isDebugEnabled()) {
    	_EOEvaluationNoticePromotion.LOG.debug( "updating enpReductionEchelonRefusMotif from " + enpReductionEchelonRefusMotif() + " to " + value);
    }
    takeStoredValueForKey(value, "enpReductionEchelonRefusMotif");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluation)storedValueForKey("toEvaluation");
  }

  public void setToEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation value) {
    if (_EOEvaluationNoticePromotion.LOG.isDebugEnabled()) {
      _EOEvaluationNoticePromotion.LOG.debug("updating toEvaluation from " + toEvaluation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluation oldValue = toEvaluation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluation");
    }
  }
  

  public static EOEvaluationNoticePromotion create(EOEditingContext editingContext, org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation) {
    EOEvaluationNoticePromotion eo = (EOEvaluationNoticePromotion) EOUtilities.createAndInsertInstance(editingContext, _EOEvaluationNoticePromotion.ENTITY_NAME);    
    eo.setToEvaluationRelationship(toEvaluation);
    return eo;
  }

  public static NSArray<EOEvaluationNoticePromotion> fetchAll(EOEditingContext editingContext) {
    return _EOEvaluationNoticePromotion.fetchAll(editingContext, null);
  }

  public static NSArray<EOEvaluationNoticePromotion> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEvaluationNoticePromotion.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOEvaluationNoticePromotion> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOEvaluationNoticePromotion.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEvaluationNoticePromotion> eoObjects = (NSArray<EOEvaluationNoticePromotion>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOEvaluationNoticePromotion fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEvaluationNoticePromotion.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEvaluationNoticePromotion fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEvaluationNoticePromotion> eoObjects = _EOEvaluationNoticePromotion.fetch(editingContext, qualifier, null);
    EOEvaluationNoticePromotion eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOEvaluationNoticePromotion)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_EvaluationNoticePromotion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEvaluationNoticePromotion fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEvaluationNoticePromotion.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOEvaluationNoticePromotion fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEvaluationNoticePromotion eoObject = _EOEvaluationNoticePromotion.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_EvaluationNoticePromotion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOEvaluationNoticePromotion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEvaluationNoticePromotion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEvaluationNoticePromotion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEvaluationNoticePromotion)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOEvaluationNoticePromotion localInstanceIn(EOEditingContext editingContext, EOEvaluationNoticePromotion eo) {
    EOEvaluationNoticePromotion localInstance = (eo == null) ? null : (EOEvaluationNoticePromotion)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
