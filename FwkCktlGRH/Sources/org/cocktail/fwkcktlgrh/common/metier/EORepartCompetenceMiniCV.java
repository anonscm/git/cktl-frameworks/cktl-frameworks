/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EORepartCompetenceMiniCV extends _EORepartCompetenceMiniCV {
  private static Logger log = Logger.getLogger(EORepartCompetenceMiniCV.class);

/**
 * 
 * @throws NSValidation.ValidationException
 */
public void validateForInsert() throws NSValidation.ValidationException {
    this.validateObjectMetier();
    validateBeforeTransactionSave();
    super.validateForInsert();
}

/**
 * 
 * @throws NSValidation.ValidationException
 */
public void validateForUpdate() throws NSValidation.ValidationException {
    this.validateObjectMetier();
    validateBeforeTransactionSave();
    super.validateForUpdate();
}

/**
 * @throws NSValidation.ValidationException
 */
public void validateForDelete() throws NSValidation.ValidationException {
    super.validateForDelete();
}



/**
 * Peut etre appele à partir des factories.
 * @throws NSValidation.ValidationException
 */
public void validateObjectMetier() throws NSValidation.ValidationException {
	
}

/**
 * A appeler par les validateforsave, forinsert, forupdate.
 *
 */
public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
	
}

/**
 * Remonter
 */
	
	public void up() {
		int position = position();
		System.err.println("position: " + position());
	    if (position != 1) {
	    	EOQualifier qualCompPrecendante = EORepartCompetenceMiniCV.POSITION.eq(position() -1).and(EORepartCompetenceMiniCV.INDIVIDU.eq(this.individu()));
	    	EORepartCompetenceMiniCV recPrev = EORepartCompetenceMiniCV.fetch(editingContext(), qualCompPrecendante);
	    		
	    		recPrev.setPosition(position);
	    		this.setPosition(position - 1);
	    }
	}


/**
 * Redescendre
 */
	public void down() {

		EOQualifier qualComp = EORepartCompetenceMiniCV.POSITION.eq(position() + 1).and(EORepartCompetenceMiniCV.INDIVIDU.eq(this.individu()));
		EORepartCompetenceMiniCV recSuivante = null;
		recSuivante = EORepartCompetenceMiniCV.fetch(editingContext(), qualComp);

//		int position = recSuivante.position();

		if (recSuivante != null) {
			int position = recSuivante.position();
			recSuivante.setPosition(position -1);
			this.setPosition(position);
		}
	}

}
