/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOContratAvenant.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOContratAvenant extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_ContratAvenant";

	// Attributes
	public static final String C_DEA_KEY = "cDea";
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String CTRA_DUREE_KEY = "ctraDuree";
	public static final String CTRA_DUREE_VALIDEE_ANNEES_KEY = "ctraDureeValideeAnnees";
	public static final String CTRA_DUREE_VALIDEE_JOURS_KEY = "ctraDureeValideeJours";
	public static final String CTRA_DUREE_VALIDEE_MOIS_KEY = "ctraDureeValideeMois";
	public static final String CTRA_ORDRE_PREC_KEY = "ctraOrdrePrec";
	public static final String CTRA_PC_ACQUITEES_KEY = "ctraPcAcquitees";
	public static final String CTRA_QUOTITE_COTISATION_KEY = "ctraQuotiteCotisation";
	public static final String CTRA_TYPE_TEMPS_KEY = "ctraTypeTemps";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_AVENANT_KEY = "dDebAvenant";
	public static final String D_FIN_AVENANT_KEY = "dFinAvenant";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_REF_CTR_AVENANT_KEY = "dRefCtrAvenant";
	public static final String D_VAL_CONTRAT_AV_KEY = "dValContratAv";
	public static final String FONCTION_CTR_AVENANT_KEY = "fonctionCtrAvenant";
	public static final String INDICE_CONTRAT_KEY = "indiceContrat";
	public static final String MONTANT_KEY = "montant";
	public static final String NBR_UNITE_KEY = "nbrUnite";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_AVENANT_KEY = "noAvenant";
	public static final String NUM_QUOT_RECRUTEMENT_KEY = "numQuotRecrutement";
	public static final String POURCENT_SMIC_KEY = "pourcentSmic";
	public static final String REFERENCE_CONTRAT_KEY = "referenceContrat";
	public static final String TAUX_HORAIRE_KEY = "tauxHoraire";
	public static final String TEM_ANNULATION_KEY = "temAnnulation";
	public static final String TEM_ARRETE_SIGNE_KEY = "temArreteSigne";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_PAIEMENT_PONCTUEL_KEY = "temPaiementPonctuel";
	public static final String TYPE_MONTANT_KEY = "typeMontant";

	public static final ERXKey<String> C_DEA = new ERXKey<String>("cDea");
	public static final ERXKey<String> C_ECHELON = new ERXKey<String>("cEchelon");
	public static final ERXKey<Double> CTRA_DUREE = new ERXKey<Double>("ctraDuree");
	public static final ERXKey<Integer> CTRA_DUREE_VALIDEE_ANNEES = new ERXKey<Integer>("ctraDureeValideeAnnees");
	public static final ERXKey<Integer> CTRA_DUREE_VALIDEE_JOURS = new ERXKey<Integer>("ctraDureeValideeJours");
	public static final ERXKey<Integer> CTRA_DUREE_VALIDEE_MOIS = new ERXKey<Integer>("ctraDureeValideeMois");
	public static final ERXKey<Integer> CTRA_ORDRE_PREC = new ERXKey<Integer>("ctraOrdrePrec");
	public static final ERXKey<String> CTRA_PC_ACQUITEES = new ERXKey<String>("ctraPcAcquitees");
	public static final ERXKey<Double> CTRA_QUOTITE_COTISATION = new ERXKey<Double>("ctraQuotiteCotisation");
	public static final ERXKey<String> CTRA_TYPE_TEMPS = new ERXKey<String>("ctraTypeTemps");
	public static final ERXKey<NSTimestamp> DATE_ARRETE = new ERXKey<NSTimestamp>("dateArrete");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_AVENANT = new ERXKey<NSTimestamp>("dDebAvenant");
	public static final ERXKey<NSTimestamp> D_FIN_AVENANT = new ERXKey<NSTimestamp>("dFinAvenant");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<NSTimestamp> D_REF_CTR_AVENANT = new ERXKey<NSTimestamp>("dRefCtrAvenant");
	public static final ERXKey<NSTimestamp> D_VAL_CONTRAT_AV = new ERXKey<NSTimestamp>("dValContratAv");
	public static final ERXKey<String> FONCTION_CTR_AVENANT = new ERXKey<String>("fonctionCtrAvenant");
	public static final ERXKey<String> INDICE_CONTRAT = new ERXKey<String>("indiceContrat");
	public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
	public static final ERXKey<Double> NBR_UNITE = new ERXKey<Double>("nbrUnite");
	public static final ERXKey<String> NO_ARRETE = new ERXKey<String>("noArrete");
	public static final ERXKey<String> NO_AVENANT = new ERXKey<String>("noAvenant");
	public static final ERXKey<Integer> NUM_QUOT_RECRUTEMENT = new ERXKey<Integer>("numQuotRecrutement");
	public static final ERXKey<Double> POURCENT_SMIC = new ERXKey<Double>("pourcentSmic");
	public static final ERXKey<String> REFERENCE_CONTRAT = new ERXKey<String>("referenceContrat");
	public static final ERXKey<java.math.BigDecimal> TAUX_HORAIRE = new ERXKey<java.math.BigDecimal>("tauxHoraire");
	public static final ERXKey<String> TEM_ANNULATION = new ERXKey<String>("temAnnulation");
	public static final ERXKey<String> TEM_ARRETE_SIGNE = new ERXKey<String>("temArreteSigne");
	public static final ERXKey<String> TEM_GEST_ETAB = new ERXKey<String>("temGestEtab");
	public static final ERXKey<String> TEM_PAIEMENT_PONCTUEL = new ERXKey<String>("temPaiementPonctuel");
	public static final ERXKey<String> TYPE_MONTANT = new ERXKey<String>("typeMontant");
	// Relationships
	public static final String TO_BAP_KEY = "toBap";
	public static final String TO_CATEGORIE_KEY = "toCategorie";
	public static final String TO_CNU_KEY = "toCnu";
	public static final String TO_CONTRAT_KEY = "toContrat";
	public static final String TO_GRADE_KEY = "toGrade";
	public static final String TO_REFERENS_EMPLOIS_KEY = "toReferensEmplois";
	public static final String TO_TYPE_UNITE_TEMPS_KEY = "toTypeUniteTemps";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOBap> TO_BAP = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOBap>("toBap");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCategorie> TO_CATEGORIE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCategorie>("toCategorie");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> TO_CNU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu>("toCnu");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOContrat> TO_CONTRAT = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOContrat>("toContrat");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> TO_GRADE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGrade>("toGrade");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> TO_REFERENS_EMPLOIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>("toReferensEmplois");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeUniteTemps> TO_TYPE_UNITE_TEMPS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeUniteTemps>("toTypeUniteTemps");

  private static Logger LOG = Logger.getLogger(_EOContratAvenant.class);

  public EOContratAvenant localInstanceIn(EOEditingContext editingContext) {
    EOContratAvenant localInstance = (EOContratAvenant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cDea() {
    return (String) storedValueForKey("cDea");
  }

  public void setCDea(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cDea from " + cDea() + " to " + value);
    }
    takeStoredValueForKey(value, "cDea");
  }

  public String cEchelon() {
    return (String) storedValueForKey("cEchelon");
  }

  public void setCEchelon(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating cEchelon from " + cEchelon() + " to " + value);
    }
    takeStoredValueForKey(value, "cEchelon");
  }

  public Double ctraDuree() {
    return (Double) storedValueForKey("ctraDuree");
  }

  public void setCtraDuree(Double value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctraDuree from " + ctraDuree() + " to " + value);
    }
    takeStoredValueForKey(value, "ctraDuree");
  }

  public Integer ctraDureeValideeAnnees() {
    return (Integer) storedValueForKey("ctraDureeValideeAnnees");
  }

  public void setCtraDureeValideeAnnees(Integer value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctraDureeValideeAnnees from " + ctraDureeValideeAnnees() + " to " + value);
    }
    takeStoredValueForKey(value, "ctraDureeValideeAnnees");
  }

  public Integer ctraDureeValideeJours() {
    return (Integer) storedValueForKey("ctraDureeValideeJours");
  }

  public void setCtraDureeValideeJours(Integer value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctraDureeValideeJours from " + ctraDureeValideeJours() + " to " + value);
    }
    takeStoredValueForKey(value, "ctraDureeValideeJours");
  }

  public Integer ctraDureeValideeMois() {
    return (Integer) storedValueForKey("ctraDureeValideeMois");
  }

  public void setCtraDureeValideeMois(Integer value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctraDureeValideeMois from " + ctraDureeValideeMois() + " to " + value);
    }
    takeStoredValueForKey(value, "ctraDureeValideeMois");
  }

  public Integer ctraOrdrePrec() {
    return (Integer) storedValueForKey("ctraOrdrePrec");
  }

  public void setCtraOrdrePrec(Integer value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctraOrdrePrec from " + ctraOrdrePrec() + " to " + value);
    }
    takeStoredValueForKey(value, "ctraOrdrePrec");
  }

  public String ctraPcAcquitees() {
    return (String) storedValueForKey("ctraPcAcquitees");
  }

  public void setCtraPcAcquitees(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctraPcAcquitees from " + ctraPcAcquitees() + " to " + value);
    }
    takeStoredValueForKey(value, "ctraPcAcquitees");
  }

  public Double ctraQuotiteCotisation() {
    return (Double) storedValueForKey("ctraQuotiteCotisation");
  }

  public void setCtraQuotiteCotisation(Double value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctraQuotiteCotisation from " + ctraQuotiteCotisation() + " to " + value);
    }
    takeStoredValueForKey(value, "ctraQuotiteCotisation");
  }

  public String ctraTypeTemps() {
    return (String) storedValueForKey("ctraTypeTemps");
  }

  public void setCtraTypeTemps(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating ctraTypeTemps from " + ctraTypeTemps() + " to " + value);
    }
    takeStoredValueForKey(value, "ctraTypeTemps");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebAvenant() {
    return (NSTimestamp) storedValueForKey("dDebAvenant");
  }

  public void setDDebAvenant(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dDebAvenant from " + dDebAvenant() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebAvenant");
  }

  public NSTimestamp dFinAvenant() {
    return (NSTimestamp) storedValueForKey("dFinAvenant");
  }

  public void setDFinAvenant(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dFinAvenant from " + dFinAvenant() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinAvenant");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dRefCtrAvenant() {
    return (NSTimestamp) storedValueForKey("dRefCtrAvenant");
  }

  public void setDRefCtrAvenant(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dRefCtrAvenant from " + dRefCtrAvenant() + " to " + value);
    }
    takeStoredValueForKey(value, "dRefCtrAvenant");
  }

  public NSTimestamp dValContratAv() {
    return (NSTimestamp) storedValueForKey("dValContratAv");
  }

  public void setDValContratAv(NSTimestamp value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating dValContratAv from " + dValContratAv() + " to " + value);
    }
    takeStoredValueForKey(value, "dValContratAv");
  }

  public String fonctionCtrAvenant() {
    return (String) storedValueForKey("fonctionCtrAvenant");
  }

  public void setFonctionCtrAvenant(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating fonctionCtrAvenant from " + fonctionCtrAvenant() + " to " + value);
    }
    takeStoredValueForKey(value, "fonctionCtrAvenant");
  }

  public String indiceContrat() {
    return (String) storedValueForKey("indiceContrat");
  }

  public void setIndiceContrat(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating indiceContrat from " + indiceContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "indiceContrat");
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey("montant");
  }

  public void setMontant(java.math.BigDecimal value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating montant from " + montant() + " to " + value);
    }
    takeStoredValueForKey(value, "montant");
  }

  public Double nbrUnite() {
    return (Double) storedValueForKey("nbrUnite");
  }

  public void setNbrUnite(Double value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating nbrUnite from " + nbrUnite() + " to " + value);
    }
    takeStoredValueForKey(value, "nbrUnite");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String noAvenant() {
    return (String) storedValueForKey("noAvenant");
  }

  public void setNoAvenant(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating noAvenant from " + noAvenant() + " to " + value);
    }
    takeStoredValueForKey(value, "noAvenant");
  }

  public Integer numQuotRecrutement() {
    return (Integer) storedValueForKey("numQuotRecrutement");
  }

  public void setNumQuotRecrutement(Integer value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating numQuotRecrutement from " + numQuotRecrutement() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotRecrutement");
  }

  public Double pourcentSmic() {
    return (Double) storedValueForKey("pourcentSmic");
  }

  public void setPourcentSmic(Double value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating pourcentSmic from " + pourcentSmic() + " to " + value);
    }
    takeStoredValueForKey(value, "pourcentSmic");
  }

  public String referenceContrat() {
    return (String) storedValueForKey("referenceContrat");
  }

  public void setReferenceContrat(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating referenceContrat from " + referenceContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "referenceContrat");
  }

  public java.math.BigDecimal tauxHoraire() {
    return (java.math.BigDecimal) storedValueForKey("tauxHoraire");
  }

  public void setTauxHoraire(java.math.BigDecimal value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating tauxHoraire from " + tauxHoraire() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxHoraire");
  }

  public String temAnnulation() {
    return (String) storedValueForKey("temAnnulation");
  }

  public void setTemAnnulation(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating temAnnulation from " + temAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "temAnnulation");
  }

  public String temArreteSigne() {
    return (String) storedValueForKey("temArreteSigne");
  }

  public void setTemArreteSigne(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating temArreteSigne from " + temArreteSigne() + " to " + value);
    }
    takeStoredValueForKey(value, "temArreteSigne");
  }

  public String temGestEtab() {
    return (String) storedValueForKey("temGestEtab");
  }

  public void setTemGestEtab(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating temGestEtab from " + temGestEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "temGestEtab");
  }

  public String temPaiementPonctuel() {
    return (String) storedValueForKey("temPaiementPonctuel");
  }

  public void setTemPaiementPonctuel(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating temPaiementPonctuel from " + temPaiementPonctuel() + " to " + value);
    }
    takeStoredValueForKey(value, "temPaiementPonctuel");
  }

  public String typeMontant() {
    return (String) storedValueForKey("typeMontant");
  }

  public void setTypeMontant(String value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
    	_EOContratAvenant.LOG.debug( "updating typeMontant from " + typeMontant() + " to " + value);
    }
    takeStoredValueForKey(value, "typeMontant");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOBap toBap() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOBap)storedValueForKey("toBap");
  }

  public void setToBapRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOBap value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
      _EOContratAvenant.LOG.debug("updating toBap from " + toBap() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOBap oldValue = toBap();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toBap");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toBap");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCategorie toCategorie() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCategorie)storedValueForKey("toCategorie");
  }

  public void setToCategorieRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCategorie value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
      _EOContratAvenant.LOG.debug("updating toCategorie from " + toCategorie() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCategorie oldValue = toCategorie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCategorie");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCategorie");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCnu toCnu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCnu)storedValueForKey("toCnu");
  }

  public void setToCnuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCnu value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
      _EOContratAvenant.LOG.debug("updating toCnu from " + toCnu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCnu oldValue = toCnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCnu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCnu");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOContrat toContrat() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOContrat)storedValueForKey("toContrat");
  }

  public void setToContratRelationship(org.cocktail.fwkcktlgrh.common.metier.EOContrat value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
      _EOContratAvenant.LOG.debug("updating toContrat from " + toContrat() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOContrat oldValue = toContrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toContrat");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toContrat");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOGrade toGrade() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOGrade)storedValueForKey("toGrade");
  }

  public void setToGradeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGrade value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
      _EOContratAvenant.LOG.debug("updating toGrade from " + toGrade() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOGrade oldValue = toGrade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toGrade");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toGrade");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois toReferensEmplois() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois)storedValueForKey("toReferensEmplois");
  }

  public void setToReferensEmploisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
      _EOContratAvenant.LOG.debug("updating toReferensEmplois from " + toReferensEmplois() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois oldValue = toReferensEmplois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toReferensEmplois");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toReferensEmplois");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeUniteTemps toTypeUniteTemps() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeUniteTemps)storedValueForKey("toTypeUniteTemps");
  }

  public void setToTypeUniteTempsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeUniteTemps value) {
    if (_EOContratAvenant.LOG.isDebugEnabled()) {
      _EOContratAvenant.LOG.debug("updating toTypeUniteTemps from " + toTypeUniteTemps() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeUniteTemps oldValue = toTypeUniteTemps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeUniteTemps");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeUniteTemps");
    }
  }
  

  public static EOContratAvenant create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebAvenant
, NSTimestamp dFinAvenant
, NSTimestamp dModification
, Integer numQuotRecrutement
, org.cocktail.fwkcktlgrh.common.metier.EOContrat toContrat) {
    EOContratAvenant eo = (EOContratAvenant) EOUtilities.createAndInsertInstance(editingContext, _EOContratAvenant.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebAvenant(dDebAvenant);
		eo.setDFinAvenant(dFinAvenant);
		eo.setDModification(dModification);
		eo.setNumQuotRecrutement(numQuotRecrutement);
    eo.setToContratRelationship(toContrat);
    return eo;
  }

  public static NSArray<EOContratAvenant> fetchAll(EOEditingContext editingContext) {
    return _EOContratAvenant.fetchAll(editingContext, null);
  }

  public static NSArray<EOContratAvenant> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOContratAvenant.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOContratAvenant> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOContratAvenant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOContratAvenant> eoObjects = (NSArray<EOContratAvenant>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOContratAvenant fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContratAvenant.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOContratAvenant fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOContratAvenant> eoObjects = _EOContratAvenant.fetch(editingContext, qualifier, null);
    EOContratAvenant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOContratAvenant)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_ContratAvenant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOContratAvenant fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContratAvenant.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOContratAvenant fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOContratAvenant eoObject = _EOContratAvenant.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_ContratAvenant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOContratAvenant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOContratAvenant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOContratAvenant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOContratAvenant)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOContratAvenant localInstanceIn(EOEditingContext editingContext, EOContratAvenant eo) {
    EOContratAvenant localInstance = (eo == null) ? null : (EOContratAvenant)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
