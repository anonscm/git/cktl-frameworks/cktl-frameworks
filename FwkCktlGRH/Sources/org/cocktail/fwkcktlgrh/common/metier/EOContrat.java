/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IContrat;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation;

import com.webobjects.eoaccess.EORelationship;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.eof.qualifiers.ERXQualifierInSubquery;

public class EOContrat extends _EOContrat implements IContrat {

	private static final long serialVersionUID = 9011278124723996540L;
	private static Logger log = Logger.getLogger(EOContrat.class);

	public static final String TEM_ANNULATION_NON = "N";
	public static final String TEM_ANNULATION_OUI = "O";
	
	
	public boolean estVacataire() {
		return toTypeContratTravail() != null && toTypeContratTravail().estVacataire();
	}

	/**
	 * retourne true si un individu a un contrat en cours pendant la
	 * p&eacute;riode
	 * 
	 * @param editingContext
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 *            peut etre nul
	 */
	public static boolean aContratEnCoursSurPeriode(EOEditingContext editingContext, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		return rechercherContratsPourIndividuEtPeriode(editingContext, individu, dateDebut, dateFin, false).count() > 0;
	}

	/**
	 * retourne true si un individu a un contrat (y compris de vacataire) en
	 * cours pendant la p&eacute;riode
	 * 
	 * @param editingContext
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 *            peut etre nul
	 */
	public static boolean aContratToutTypeEnCoursSurPeriode(EOEditingContext editingContext, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		return rechercherTousContratsPourIndividuEtPeriode(editingContext, individu, dateDebut, dateFin, false).count() > 0;
	}

	/**
	 * recherche les contrats de remuneration principale valides d'un individu
	 * dont les dates de d&eacute;but et fin sont &agrave; cheval sur deux dates
	 * 
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode
	 *            peut etre nulle
	 * @param prefetch
	 *            true si prefetcher les donnees liees
	 * @return contrats trouves
	 */
	public static NSArray rechercherContratsPourIndividuEtPeriode(EOEditingContext editingContext, EOIndividu individu, NSTimestamp debutPeriode,
			NSTimestamp finPeriode, boolean prefetch) {
		NSMutableArray qualifiers = new NSMutableArray(qualifierPourPeriode(individu, debutPeriode, finPeriode));
		EOQualifier qualifierValidite = ERXQ.notEquals(TEM_ANNULATION_KEY, "O").and(
				ERXQ.equals(ERXQ.keyPath(TO_TYPE_CONTRAT_TRAVAIL_KEY, EOTypeContratTravail.TEM_REMUNERATION_PRINCIPALE_KEY), "O"));
		qualifiers.addObject(qualifierValidite);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		// if (prefetch) {
		// NSMutableArray prefetches = new NSMutableArray("avenants");
		// prefetches.addObject("toContratVacataires");
		// myFetch.setPrefetchingRelationshipKeyPaths(prefetches);
		// }
		myFetch.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/**
	 * retourne le qualifier pour d&eacute;terminer les contrats sur une
	 * p&eacute;riode (on prend en compte la date de fin anticip&eacute;e
	 * 
	 * @param individu
	 * @param debutPeriode
	 *            peut &ecirc;tre nulle
	 * @param finPeriode
	 *            peut &ecirc;tre nulle
	 */
	public static EOQualifier qualifierPourPeriode(EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		NSMutableArray args = new NSMutableArray(individu.noIndividu());
		EOQualifier qualifier = ERXQ.equals(ERXQ.keyPath(TO_PERSONNEL_KEY, EOPersonnel.TO_INDIVIDU_KEY), individu).and(ERXQ.notEquals(TEM_ANNULATION_KEY, "O"));
		qualifiers.addObject(qualifier);
		qualifier = GRHUtilities.qualifierPourPeriode("dDebContratTrav", debutPeriode, "dFinContratTrav", finPeriode);
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}
		if (finPeriode == null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dFinAnticipee = NIL", null));
		} else {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dFinAnticipee = NIL OR dFinAnticipee >= %@", new NSArray(finPeriode)));
		}
		qualifier = new EOAndQualifier(qualifiers);
		return qualifier;
	}

	/**
	 * recherche les contrats valides d'un individu dont les dates de
	 * d&eacute;but et fin sont &agrave; cheval sur deux dates
	 * 
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode
	 *            peut &ecirc;tre nulle
	 * @param prefetch
	 *            true si prefetcher les donn&eacute;es li&eacute;es
	 * @return contrats trouv&eacute;s
	 */
	public static NSArray rechercherTousContratsPourIndividuEtPeriode(EOEditingContext editingContext, EOIndividu individu, NSTimestamp debutPeriode,
			NSTimestamp finPeriode, boolean prefetch) {
		EOQualifier qualifier = qualifierPourPeriode(individu, debutPeriode, finPeriode);
		qualifier = ERXQ.and(qualifier, ERXQ.notEquals(TEM_ANNULATION_KEY, "O"));
		EOFetchSpecification myFetch = new EOFetchSpecification(EOContrat.ENTITY_NAME, qualifier, null);
		// relations pas encore modélisées
		// if (prefetch) {
		// NSMutableArray prefetches = new NSMutableArray("avenants");
		// prefetches.addObject("toContratVacataires");
		// myFetch.setPrefetchingRelationshipKeyPaths(prefetches);
		// }
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	@SuppressWarnings("unchecked")
	public static NSArray<EOContrat> contratsForIndividu(EOEditingContext ec, EOIndividu ind) {
		if (ind == null) {
			return null;
		}
		EOQualifier qual = ERXQ.equals(ERXQ.keyPath(EOContrat.TO_PERSONNEL_KEY, EOPersonnel.TO_INDIVIDU_KEY), ind).and(ERXQ.equals(TEM_ANNULATION_KEY, "N"));
		NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(D_DEB_CONTRAT_TRAV_KEY).array();
		return fetchAll(ec, qual, sorts);
	}
	
	@SuppressWarnings("unchecked")
	public static NSArray<EOContrat> contratsActuelsForIndividu(EOEditingContext ec, EOIndividu ind) {
		if (ind == null) {
			return null;
		}
		EOQualifier qualifier =
				ERXQ.and(
						EOContrat.TO_INDIVIDU.eq(ind),
						EOContrat.TEM_ANNULATION.eq(EOContrat.TEM_ANNULATION_NON),
						EOContrat.D_DEB_CONTRAT_TRAV.lessThanOrEqualTo(MyDateCtrl.getDateJour()),
						ERXQ.or(
								EOContrat.D_FIN_CONTRAT_TRAV.isNull(), 
								EOContrat.D_FIN_CONTRAT_TRAV.greaterThanOrEqualTo(MyDateCtrl.getDateJour()),
								EOContrat.D_FIN_ANTICIPEE.greaterThanOrEqualTo(MyDateCtrl.getDateJour())
								)
						);
		NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(D_DEB_CONTRAT_TRAV_KEY).array();
		return fetchAll(ec, qualifier, sorts);
	}

	
	/**
	 * Renvoie l'avenant courant et valide du contrat.
	 * @return avenant du contrat
	 */
	public EOContratAvenant avenantActuel() {
		EOQualifier qualifier =
				ERXQ.and(
						ERXQ.lessThanOrEqualTo(EOContratAvenant.D_DEB_AVENANT_KEY, MyDateCtrl.getDateJour()),
						ERXQ.or(
								ERXQ.isNull(EOContratAvenant.D_FIN_AVENANT_KEY),
								ERXQ.greaterThanOrEqualTo(EOContratAvenant.D_FIN_AVENANT_KEY, MyDateCtrl.getDateJour())
						),
						ERXQ.equals(EOContratAvenant.TEM_ANNULATION_KEY, Constantes.FAUX)
				);

		NSArray<EOContratAvenant> avenants = toContratAvenants(qualifier, EOContratAvenant.D_DEB_AVENANT.descs(), false);

		if (!avenants.isEmpty()) {
			return avenants.get(0);
		}

		return null;
	}
	
	/**
	 * Renvoie l'avenant courant et valide du contrat sur une periode.
	 * @param dateDebut : début de la période
	 * @param dateFin : fin de la période
	 * @return avenant du contrat
	 */

	public EOContratAvenant avenantSuivantPeriode(NSTimestamp dateDebut, NSTimestamp dateFin) {
		EOQualifier qualifier =
				ERXQ.and(
						ERXQ.lessThanOrEqualTo(EOContratAvenant.D_DEB_AVENANT_KEY, dateFin),
						ERXQ.or(
								ERXQ.isNull(EOContratAvenant.D_FIN_AVENANT_KEY),
								ERXQ.greaterThanOrEqualTo(EOContratAvenant.D_FIN_AVENANT_KEY,  dateDebut)
						),
						ERXQ.equals(EOContratAvenant.TEM_ANNULATION_KEY, Constantes.FAUX)
				);
		
		NSArray<EOContratAvenant> avenants = toContratAvenants(qualifier, EOContratAvenant.D_DEB_AVENANT.descs(), false);
		
		if (!avenants.isEmpty()) {
			return avenants.get(0);
		}

		return null;
	}
	
	/**
	 * Renvoie les avenants valides du contrat sur une periode.
	 * @param dateDebut : début de la période
	 * @param dateFin : fin de la période
	 * @return avenants du contrat
	 */
	public NSArray<EOContratAvenant> avenantsSuivantPeriode(NSTimestamp dateDebut, NSTimestamp dateFin) {
		EOQualifier qualifier =
			ERXQ.and(
					ERXQ.lessThanOrEqualTo(EOContratAvenant.D_DEB_AVENANT_KEY, dateFin),
					ERXQ.or(
							ERXQ.isNull(EOContratAvenant.D_FIN_AVENANT_KEY),
							ERXQ.greaterThanOrEqualTo(EOContratAvenant.D_FIN_AVENANT_KEY,  dateDebut)
					),
					ERXQ.equals(EOContratAvenant.TEM_ANNULATION_KEY, Constantes.FAUX)
			);
		NSArray<EOContratAvenant> avenants = toContratAvenants(qualifier, EOContratAvenant.D_DEB_AVENANT.descs(), false);
		
		if (!avenants.isEmpty()) {
			return avenants;
		}

		return null;
	}

	/**
	 * retourne le qualifier pour d&eacute;terminer les contrats courants et valides d'un
	 * individu
	 * @param individu : individu
	 * @param dateDebut : début de la période
	 * @param dateFin : fin de la période
	 * @return qualifier
	 */
	public static EOQualifier qualifierTousContratsCourants(EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		return qualifierTousContratsCourants(individu.getNumeroInt(), dateDebut, dateFin);
	}

	private static EOQualifier qualifierTousContratsCourants(
			Integer noIndividu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.lessThanOrEqualTo(EOContrat.D_DEB_CONTRAT_TRAV_KEY, dateDebut),
						ERXQ.or(
								ERXQ.isNull(EOContrat.D_FIN_CONTRAT_TRAV_KEY), 
								ERXQ.greaterThanOrEqualTo(EOContrat.D_FIN_CONTRAT_TRAV_KEY, dateFin),
								ERXQ.greaterThanOrEqualTo(EOContrat.D_FIN_ANTICIPEE_KEY, dateFin)
						),
						ERXQ.notEquals(TEM_ANNULATION_KEY, "O"),
						ERXQ.equals(NO_DOSSIER_PERS_KEY, noIndividu)
				);
		return qualifier;
	}

	/**
	 * @param editingContext : editing Context
	 * @param individu : individu
	 * @param dateDebut : début de la période
	 * @param dateFin : fin de la période
	 * @return les contrats courants d'un individu
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOContrat> rechercherContratsPourIndividu(EOEditingContext editingContext, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		return fetchAll(editingContext, qualifierTousContratsCourants(individu, dateDebut, dateFin), null);
	}
	
	/**
	 * @param editingContext : editing Context
	 * @param noIndividu : numéro individu
	 * @param dateDebut : début de la période
	 * @param dateFin : fin de la période
	 * @return les contrats courants d'un individu
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOContrat> rechercherContratsPourIndividu(EOEditingContext editingContext, Integer noIndividu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		return fetchAll(editingContext, qualifierTousContratsCourants(noIndividu, dateDebut, dateFin), null);
	}
	
	
	/**
	 * Renvoie le nombre d'heures de l'avenant du contrat sur periode
	 * @param dateDebut : début de la période
	 * @param dateFin : fin de la période
	 * @return nombre d'heures de l'avenant du contrat
	 */
	public double nbHeuresAvenantContratsurPeriode(NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSArray<EOContratAvenant> avenants = avenantsSuivantPeriode(dateDebut, dateFin);

		double somme = 0;
		if (avenants != null) {
			for (EOContratAvenant avenant : avenants) {
				if (avenant.ctraDuree() != null) {
					somme += avenant.ctraDuree();
				}
			}
		}

		return somme;
	}
	
	/**
	 * @param editingContext : editing Context
	 * @param individu : individu
	 * @param dateDebut : début de la période
	 * @param dateFin : fin de la période
	 * @return les contrats courants typés Enseignant d'un individu
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOContrat> rechercherContratsEnseignantsPourIndividu(EOEditingContext editingContext, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {	
		//Récupère les avenants dont le grade est enseignant ou doctorant avec enseignement
		EOQualifier contratAvenantsQualifier = 
				ERXQ.and(
						EOContratAvenant.D_DEB_AVENANT.lessThanOrEqualTo(dateFin),
						ERXQ.or(
								EOContratAvenant.D_FIN_AVENANT.isNull(),
								EOContratAvenant.D_FIN_AVENANT.greaterThanOrEqualTo(dateDebut)
						),
						EOContratAvenant.TEM_ANNULATION.eq(Constantes.FAUX),
						ERXQ.or(
								EOContratAvenant.TO_GRADE.dot(EOGrade.C_GRADE).eq(EOGrade.CODE_GRADE_DOC_ENS),
								EOContratAvenant.TO_GRADE.dot(EOGrade.TO_CORPS).dot(EOCorps.TO_TYPE_POPULATION)
									.dot(EOTypePopulation.TEM_ENSEIGNANT).eq(Constantes.VRAI)
						)
								
				);
		
		//On crée la relation entre EOContrat et EOContratAvenant pour pouvoir déterminer les attributs source et destination qu'on va utiliser
		//dans la sous-requête. Cette relation est nécessaire car on n'a pas accès à la clé primaire.
		EORelationship contratRelationship = ERXEOAccessUtilities.entityNamed(null, EOContratAvenant.ENTITY_NAME)
												.relationshipNamed(EOContratAvenant.TO_CONTRAT_KEY);
		
		String destinationAttributeName = contratRelationship.sourceAttributes().lastObject().name();
		String sourceAttributeName = contratRelationship.destinationAttributes().lastObject().name();
		
		//Sous-requête pour les contrats avenants
		EOQualifier qualifierInSubQuery = new ERXQualifierInSubquery(contratAvenantsQualifier, EOContratAvenant.ENTITY_NAME, 
				sourceAttributeName, destinationAttributeName);
		
		//Qualifier pour récupérer les contrats enseignants de l'individu
		EOQualifier qualifier = 
				ERXQ.and(
						qualifierTousContratsCourants(individu, dateFin, dateDebut),
						ERXQ.or(
								//Soit le type de contrat est enseignant
								ERXQ.equals(EOContrat.TO_TYPE_CONTRAT_TRAVAIL.dot(EOTypeContratTravail.TEM_ENSEIGNANT_KEY).key(), Constantes.VRAI),
								//Soit le grade de l'avenant du contrat est enseignant
								qualifierInSubQuery
						)
				);	
		
		return fetchAll(editingContext, qualifier, null);
	}
	
	
	public NSTimestamp dateFinReelle() {
		if (dFinAnticipee() != null) {
			return dFinAnticipee();
		} else {
			return dFinContratTrav();
		}
	}
	

}
