/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOCarriere.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOCarriere extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Carriere";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_CARRIERE_KEY = "dDebCarriere";
	public static final String D_FIN_CARRIERE_KEY = "dFinCarriere";
	public static final String D_MIN_ELEM_CAR_KEY = "dMinElemCar";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_CARRIERE = new ERXKey<NSTimestamp>("dDebCarriere");
	public static final ERXKey<NSTimestamp> D_FIN_CARRIERE = new ERXKey<NSTimestamp>("dFinCarriere");
	public static final ERXKey<NSTimestamp> D_MIN_ELEM_CAR = new ERXKey<NSTimestamp>("dMinElemCar");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> NO_DOSSIER_PERS = new ERXKey<Integer>("noDossierPers");
	public static final ERXKey<Integer> NO_SEQ_CARRIERE = new ERXKey<Integer>("noSeqCarriere");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_ELEMENTS_KEY = "toElements";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_PERSONNEL_KEY = "toPersonnel";
	public static final String TO_TYPE_POPULATION_KEY = "toTypePopulation";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOElements> TO_ELEMENTS = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOElements>("toElements");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> TO_PERSONNEL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel>("toPersonnel");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation> TO_TYPE_POPULATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation>("toTypePopulation");

  private static Logger LOG = Logger.getLogger(_EOCarriere.class);

  public EOCarriere localInstanceIn(EOEditingContext editingContext) {
    EOCarriere localInstance = (EOCarriere)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebCarriere() {
    return (NSTimestamp) storedValueForKey("dDebCarriere");
  }

  public void setDDebCarriere(NSTimestamp value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating dDebCarriere from " + dDebCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebCarriere");
  }

  public NSTimestamp dFinCarriere() {
    return (NSTimestamp) storedValueForKey("dFinCarriere");
  }

  public void setDFinCarriere(NSTimestamp value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating dFinCarriere from " + dFinCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinCarriere");
  }

  public NSTimestamp dMinElemCar() {
    return (NSTimestamp) storedValueForKey("dMinElemCar");
  }

  public void setDMinElemCar(NSTimestamp value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating dMinElemCar from " + dMinElemCar() + " to " + value);
    }
    takeStoredValueForKey(value, "dMinElemCar");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey("noDossierPers");
  }

  public void setNoDossierPers(Integer value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating noDossierPers from " + noDossierPers() + " to " + value);
    }
    takeStoredValueForKey(value, "noDossierPers");
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey("noSeqCarriere");
  }

  public void setNoSeqCarriere(Integer value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating noSeqCarriere from " + noSeqCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqCarriere");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
    	_EOCarriere.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
      _EOCarriere.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel toPersonnel() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel)storedValueForKey("toPersonnel");
  }

  public void setToPersonnelRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
      _EOCarriere.LOG.debug("updating toPersonnel from " + toPersonnel() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel oldValue = toPersonnel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPersonnel");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPersonnel");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation toTypePopulation() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation)storedValueForKey("toTypePopulation");
  }

  public void setToTypePopulationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation value) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
      _EOCarriere.LOG.debug("updating toTypePopulation from " + toTypePopulation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation oldValue = toTypePopulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypePopulation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypePopulation");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOElements> toElements() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOElements>)storedValueForKey("toElements");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOElements> toElements(EOQualifier qualifier) {
    return toElements(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOElements> toElements(EOQualifier qualifier, boolean fetch) {
    return toElements(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOElements> toElements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOElements> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOElements.TO_CARRIERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOElements.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toElements();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOElements>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOElements>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToElementsRelationship(org.cocktail.fwkcktlgrh.common.metier.EOElements object) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
      _EOCarriere.LOG.debug("adding " + object + " to toElements relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toElements");
  }

  public void removeFromToElementsRelationship(org.cocktail.fwkcktlgrh.common.metier.EOElements object) {
    if (_EOCarriere.LOG.isDebugEnabled()) {
      _EOCarriere.LOG.debug("removing " + object + " from toElements relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toElements");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOElements createToElementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_ElementCarriere");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toElements");
    return (org.cocktail.fwkcktlgrh.common.metier.EOElements) eo;
  }

  public void deleteToElementsRelationship(org.cocktail.fwkcktlgrh.common.metier.EOElements object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toElements");
    editingContext().deleteObject(object);
  }

  public void deleteAllToElementsRelationships() {
    Enumeration objects = toElements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToElementsRelationship((org.cocktail.fwkcktlgrh.common.metier.EOElements)objects.nextElement());
    }
  }


  public static EOCarriere create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebCarriere
, NSTimestamp dModification
, Integer noDossierPers
, Integer noSeqCarriere
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel toPersonnel, org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation toTypePopulation) {
    EOCarriere eo = (EOCarriere) EOUtilities.createAndInsertInstance(editingContext, _EOCarriere.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebCarriere(dDebCarriere);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setNoSeqCarriere(noSeqCarriere);
		eo.setTemValide(temValide);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToPersonnelRelationship(toPersonnel);
    eo.setToTypePopulationRelationship(toTypePopulation);
    return eo;
  }

  public static NSArray<EOCarriere> fetchAll(EOEditingContext editingContext) {
    return _EOCarriere.fetchAll(editingContext, null);
  }

  public static NSArray<EOCarriere> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCarriere.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOCarriere> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCarriere.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCarriere> eoObjects = (NSArray<EOCarriere>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCarriere fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCarriere.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCarriere fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCarriere> eoObjects = _EOCarriere.fetch(editingContext, qualifier, null);
    EOCarriere eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCarriere)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Carriere that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCarriere fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCarriere.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOCarriere fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCarriere eoObject = _EOCarriere.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Carriere that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOCarriere fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCarriere fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCarriere eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCarriere)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOCarriere localInstanceIn(EOEditingContext editingContext, EOCarriere eo) {
    EOCarriere localInstance = (eo == null) ? null : (EOCarriere)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
