/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPeriodesMilitaires.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPeriodesMilitaires extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_PeriodesMilitaires";

	// Attributes
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_TYPE_PERIODE_MILIT_KEY = "toTypePeriodeMilit";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypePeriodeMilit> TO_TYPE_PERIODE_MILIT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypePeriodeMilit>("toTypePeriodeMilit");

  private static Logger LOG = Logger.getLogger(_EOPeriodesMilitaires.class);

  public EOPeriodesMilitaires localInstanceIn(EOEditingContext editingContext) {
    EOPeriodesMilitaires localInstance = (EOPeriodesMilitaires)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
    	_EOPeriodesMilitaires.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
      _EOPeriodesMilitaires.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypePeriodeMilit toTypePeriodeMilit() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypePeriodeMilit)storedValueForKey("toTypePeriodeMilit");
  }

  public void setToTypePeriodeMilitRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypePeriodeMilit value) {
    if (_EOPeriodesMilitaires.LOG.isDebugEnabled()) {
      _EOPeriodesMilitaires.LOG.debug("updating toTypePeriodeMilit from " + toTypePeriodeMilit() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypePeriodeMilit oldValue = toTypePeriodeMilit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypePeriodeMilit");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypePeriodeMilit");
    }
  }
  

  public static EOPeriodesMilitaires create(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlpersonne.common.metier.EOTypePeriodeMilit toTypePeriodeMilit) {
    EOPeriodesMilitaires eo = (EOPeriodesMilitaires) EOUtilities.createAndInsertInstance(editingContext, _EOPeriodesMilitaires.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToTypePeriodeMilitRelationship(toTypePeriodeMilit);
    return eo;
  }

  public static NSArray<EOPeriodesMilitaires> fetchAll(EOEditingContext editingContext) {
    return _EOPeriodesMilitaires.fetchAll(editingContext, null);
  }

  public static NSArray<EOPeriodesMilitaires> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPeriodesMilitaires.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPeriodesMilitaires> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPeriodesMilitaires.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPeriodesMilitaires> eoObjects = (NSArray<EOPeriodesMilitaires>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPeriodesMilitaires fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPeriodesMilitaires.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPeriodesMilitaires fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPeriodesMilitaires> eoObjects = _EOPeriodesMilitaires.fetch(editingContext, qualifier, null);
    EOPeriodesMilitaires eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPeriodesMilitaires)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_PeriodesMilitaires that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPeriodesMilitaires fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPeriodesMilitaires.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPeriodesMilitaires fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPeriodesMilitaires eoObject = _EOPeriodesMilitaires.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_PeriodesMilitaires that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPeriodesMilitaires fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPeriodesMilitaires fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPeriodesMilitaires eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPeriodesMilitaires)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPeriodesMilitaires localInstanceIn(EOEditingContext editingContext, EOPeriodesMilitaires eo) {
    EOPeriodesMilitaires localInstance = (eo == null) ? null : (EOPeriodesMilitaires)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
