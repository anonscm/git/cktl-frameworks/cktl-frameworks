/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOFicheDePoste.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOFicheDePoste extends org.cocktail.fwkcktlgrh.common.metier.util.UtilFiche {
	public static final String ENTITY_NAME = "Fwkgrh_FicheDePoste";

	// Attributes
	public static final String CODE_EMPLOI_KEY = "codeEmploi";
	public static final String COTATION_PART_F_KEY = "cotationPartF";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ETY_CODE_KEY = "etyCode";
	public static final String FDP_CONTEXTE_TRAVAIL_KEY = "fdpContexteTravail";
	public static final String FDP_D_DEBUT_KEY = "fdpDDebut";
	public static final String FDP_D_FIN_KEY = "fdpDFin";
	public static final String FDP_MISSION_POSTE_KEY = "fdpMissionPoste";
	public static final String FDP_VISA_AGENT_KEY = "fdpVisaAgent";
	public static final String FDP_VISA_DIREC_KEY = "fdpVisaDirec";
	public static final String FDP_VISA_RESP_KEY = "fdpVisaResp";
	public static final String FONCTION_CONDUITE_PROJET_KEY = "fonctionConduiteProjet";
	public static final String FONCTION_ENCADREMENT_KEY = "fonctionEncadrement";
	public static final String NB_AGENTS_ENCADRES_KEY = "nbAgentsEncadres";
	public static final String NB_AGENTS_ENCADRES_CATEGORIE_A_KEY = "nbAgentsEncadresCategorieA";
	public static final String NB_AGENTS_ENCADRES_CATEGORIE_B_KEY = "nbAgentsEncadresCategorieB";
	public static final String NB_AGENTS_ENCADRES_CATEGORIE_C_KEY = "nbAgentsEncadresCategorieC";

	public static final ERXKey<String> CODE_EMPLOI = new ERXKey<String>("codeEmploi");
	public static final ERXKey<java.math.BigDecimal> COTATION_PART_F = new ERXKey<java.math.BigDecimal>("cotationPartF");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> ETY_CODE = new ERXKey<String>("etyCode");
	public static final ERXKey<String> FDP_CONTEXTE_TRAVAIL = new ERXKey<String>("fdpContexteTravail");
	public static final ERXKey<NSTimestamp> FDP_D_DEBUT = new ERXKey<NSTimestamp>("fdpDDebut");
	public static final ERXKey<NSTimestamp> FDP_D_FIN = new ERXKey<NSTimestamp>("fdpDFin");
	public static final ERXKey<String> FDP_MISSION_POSTE = new ERXKey<String>("fdpMissionPoste");
	public static final ERXKey<String> FDP_VISA_AGENT = new ERXKey<String>("fdpVisaAgent");
	public static final ERXKey<String> FDP_VISA_DIREC = new ERXKey<String>("fdpVisaDirec");
	public static final ERXKey<String> FDP_VISA_RESP = new ERXKey<String>("fdpVisaResp");
	public static final ERXKey<String> FONCTION_CONDUITE_PROJET = new ERXKey<String>("fonctionConduiteProjet");
	public static final ERXKey<String> FONCTION_ENCADREMENT = new ERXKey<String>("fonctionEncadrement");
	public static final ERXKey<Integer> NB_AGENTS_ENCADRES = new ERXKey<Integer>("nbAgentsEncadres");
	public static final ERXKey<Integer> NB_AGENTS_ENCADRES_CATEGORIE_A = new ERXKey<Integer>("nbAgentsEncadresCategorieA");
	public static final ERXKey<Integer> NB_AGENTS_ENCADRES_CATEGORIE_B = new ERXKey<Integer>("nbAgentsEncadresCategorieB");
	public static final ERXKey<Integer> NB_AGENTS_ENCADRES_CATEGORIE_C = new ERXKey<Integer>("nbAgentsEncadresCategorieC");
	// Relationships
	public static final String TO_EMPLOI_TYPE_KEY = "toEmploiType";
	public static final String TO_POSTE_KEY = "toPoste";
	public static final String TO_REFERENS_EMPLOIS_KEY = "toReferensEmplois";
	public static final String TOS_REPART_FDP_ACTI_KEY = "tosRepartFdpActi";
	public static final String TOS_REPART_FDP_AUTRE_KEY = "tosRepartFdpAutre";
	public static final String TOS_REPART_FDP_COMP_KEY = "tosRepartFdpComp";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> TO_EMPLOI_TYPE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType>("toEmploiType");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOPoste> TO_POSTE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOPoste>("toPoste");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> TO_REFERENS_EMPLOIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>("toReferensEmplois");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi> TOS_REPART_FDP_ACTI = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi>("tosRepartFdpActi");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre> TOS_REPART_FDP_AUTRE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre>("tosRepartFdpAutre");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp> TOS_REPART_FDP_COMP = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp>("tosRepartFdpComp");

  private static Logger LOG = Logger.getLogger(_EOFicheDePoste.class);

  public EOFicheDePoste localInstanceIn(EOEditingContext editingContext) {
    EOFicheDePoste localInstance = (EOFicheDePoste)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeEmploi() {
    return (String) storedValueForKey("codeEmploi");
  }

  public void setCodeEmploi(String value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating codeEmploi from " + codeEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "codeEmploi");
  }

  public java.math.BigDecimal cotationPartF() {
    return (java.math.BigDecimal) storedValueForKey("cotationPartF");
  }

  public void setCotationPartF(java.math.BigDecimal value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating cotationPartF from " + cotationPartF() + " to " + value);
    }
    takeStoredValueForKey(value, "cotationPartF");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String etyCode() {
    return (String) storedValueForKey("etyCode");
  }

  public void setEtyCode(String value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating etyCode from " + etyCode() + " to " + value);
    }
    takeStoredValueForKey(value, "etyCode");
  }

  public String fdpContexteTravail() {
    return (String) storedValueForKey("fdpContexteTravail");
  }

  public void setFdpContexteTravail(String value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating fdpContexteTravail from " + fdpContexteTravail() + " to " + value);
    }
    takeStoredValueForKey(value, "fdpContexteTravail");
  }

  public NSTimestamp fdpDDebut() {
    return (NSTimestamp) storedValueForKey("fdpDDebut");
  }

  public void setFdpDDebut(NSTimestamp value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating fdpDDebut from " + fdpDDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "fdpDDebut");
  }

  public NSTimestamp fdpDFin() {
    return (NSTimestamp) storedValueForKey("fdpDFin");
  }

  public void setFdpDFin(NSTimestamp value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating fdpDFin from " + fdpDFin() + " to " + value);
    }
    takeStoredValueForKey(value, "fdpDFin");
  }

  public String fdpMissionPoste() {
    return (String) storedValueForKey("fdpMissionPoste");
  }

  public void setFdpMissionPoste(String value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating fdpMissionPoste from " + fdpMissionPoste() + " to " + value);
    }
    takeStoredValueForKey(value, "fdpMissionPoste");
  }

  public String fdpVisaAgent() {
    return (String) storedValueForKey("fdpVisaAgent");
  }

  public void setFdpVisaAgent(String value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating fdpVisaAgent from " + fdpVisaAgent() + " to " + value);
    }
    takeStoredValueForKey(value, "fdpVisaAgent");
  }

  public String fdpVisaDirec() {
    return (String) storedValueForKey("fdpVisaDirec");
  }

  public void setFdpVisaDirec(String value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating fdpVisaDirec from " + fdpVisaDirec() + " to " + value);
    }
    takeStoredValueForKey(value, "fdpVisaDirec");
  }

  public String fdpVisaResp() {
    return (String) storedValueForKey("fdpVisaResp");
  }

  public void setFdpVisaResp(String value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating fdpVisaResp from " + fdpVisaResp() + " to " + value);
    }
    takeStoredValueForKey(value, "fdpVisaResp");
  }

  public String fonctionConduiteProjet() {
    return (String) storedValueForKey("fonctionConduiteProjet");
  }

  public void setFonctionConduiteProjet(String value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating fonctionConduiteProjet from " + fonctionConduiteProjet() + " to " + value);
    }
    takeStoredValueForKey(value, "fonctionConduiteProjet");
  }

  public String fonctionEncadrement() {
    return (String) storedValueForKey("fonctionEncadrement");
  }

  public void setFonctionEncadrement(String value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating fonctionEncadrement from " + fonctionEncadrement() + " to " + value);
    }
    takeStoredValueForKey(value, "fonctionEncadrement");
  }

  public Integer nbAgentsEncadres() {
    return (Integer) storedValueForKey("nbAgentsEncadres");
  }

  public void setNbAgentsEncadres(Integer value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating nbAgentsEncadres from " + nbAgentsEncadres() + " to " + value);
    }
    takeStoredValueForKey(value, "nbAgentsEncadres");
  }

  public Integer nbAgentsEncadresCategorieA() {
    return (Integer) storedValueForKey("nbAgentsEncadresCategorieA");
  }

  public void setNbAgentsEncadresCategorieA(Integer value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating nbAgentsEncadresCategorieA from " + nbAgentsEncadresCategorieA() + " to " + value);
    }
    takeStoredValueForKey(value, "nbAgentsEncadresCategorieA");
  }

  public Integer nbAgentsEncadresCategorieB() {
    return (Integer) storedValueForKey("nbAgentsEncadresCategorieB");
  }

  public void setNbAgentsEncadresCategorieB(Integer value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating nbAgentsEncadresCategorieB from " + nbAgentsEncadresCategorieB() + " to " + value);
    }
    takeStoredValueForKey(value, "nbAgentsEncadresCategorieB");
  }

  public Integer nbAgentsEncadresCategorieC() {
    return (Integer) storedValueForKey("nbAgentsEncadresCategorieC");
  }

  public void setNbAgentsEncadresCategorieC(Integer value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
    	_EOFicheDePoste.LOG.debug( "updating nbAgentsEncadresCategorieC from " + nbAgentsEncadresCategorieC() + " to " + value);
    }
    takeStoredValueForKey(value, "nbAgentsEncadresCategorieC");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType toEmploiType() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType)storedValueForKey("toEmploiType");
  }

  public void setToEmploiTypeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
      _EOFicheDePoste.LOG.debug("updating toEmploiType from " + toEmploiType() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType oldValue = toEmploiType();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEmploiType");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEmploiType");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOPoste toPoste() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOPoste)storedValueForKey("toPoste");
  }

  public void setToPosteRelationship(org.cocktail.fwkcktlgrh.common.metier.EOPoste value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
      _EOFicheDePoste.LOG.debug("updating toPoste from " + toPoste() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOPoste oldValue = toPoste();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPoste");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPoste");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois toReferensEmplois() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois)storedValueForKey("toReferensEmplois");
  }

  public void setToReferensEmploisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois value) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
      _EOFicheDePoste.LOG.debug("updating toReferensEmplois from " + toReferensEmplois() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois oldValue = toReferensEmplois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toReferensEmplois");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toReferensEmplois");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi> tosRepartFdpActi() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi>)storedValueForKey("tosRepartFdpActi");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi> tosRepartFdpActi(EOQualifier qualifier) {
    return tosRepartFdpActi(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi> tosRepartFdpActi(EOQualifier qualifier, boolean fetch) {
    return tosRepartFdpActi(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi> tosRepartFdpActi(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi.TO_FICHE_DE_POSTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartFdpActi();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartFdpActiRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi object) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
      _EOFicheDePoste.LOG.debug("adding " + object + " to tosRepartFdpActi relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartFdpActi");
  }

  public void removeFromTosRepartFdpActiRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi object) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
      _EOFicheDePoste.LOG.debug("removing " + object + " from tosRepartFdpActi relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFdpActi");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi createTosRepartFdpActiRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartFdpActi");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartFdpActi");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi) eo;
  }

  public void deleteTosRepartFdpActiRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFdpActi");
  }

  public void deleteAllTosRepartFdpActiRelationships() {
    Enumeration objects = tosRepartFdpActi().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartFdpActiRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre> tosRepartFdpAutre() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre>)storedValueForKey("tosRepartFdpAutre");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre> tosRepartFdpAutre(EOQualifier qualifier) {
    return tosRepartFdpAutre(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre> tosRepartFdpAutre(EOQualifier qualifier, boolean fetch) {
    return tosRepartFdpAutre(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre> tosRepartFdpAutre(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre.TO_FICHE_DE_POSTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartFdpAutre();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartFdpAutreRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre object) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
      _EOFicheDePoste.LOG.debug("adding " + object + " to tosRepartFdpAutre relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartFdpAutre");
  }

  public void removeFromTosRepartFdpAutreRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre object) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
      _EOFicheDePoste.LOG.debug("removing " + object + " from tosRepartFdpAutre relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFdpAutre");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre createTosRepartFdpAutreRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartFdpAutre");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartFdpAutre");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre) eo;
  }

  public void deleteTosRepartFdpAutreRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFdpAutre");
  }

  public void deleteAllTosRepartFdpAutreRelationships() {
    Enumeration objects = tosRepartFdpAutre().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartFdpAutreRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp> tosRepartFdpComp() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp>)storedValueForKey("tosRepartFdpComp");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp> tosRepartFdpComp(EOQualifier qualifier) {
    return tosRepartFdpComp(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp> tosRepartFdpComp(EOQualifier qualifier, boolean fetch) {
    return tosRepartFdpComp(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp> tosRepartFdpComp(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp.TO_FICHE_DE_POSTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartFdpComp();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartFdpCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp object) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
      _EOFicheDePoste.LOG.debug("adding " + object + " to tosRepartFdpComp relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartFdpComp");
  }

  public void removeFromTosRepartFdpCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp object) {
    if (_EOFicheDePoste.LOG.isDebugEnabled()) {
      _EOFicheDePoste.LOG.debug("removing " + object + " from tosRepartFdpComp relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFdpComp");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp createTosRepartFdpCompRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartFdpComp");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartFdpComp");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp) eo;
  }

  public void deleteTosRepartFdpCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFdpComp");
  }

  public void deleteAllTosRepartFdpCompRelationships() {
    Enumeration objects = tosRepartFdpComp().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartFdpCompRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp)objects.nextElement());
    }
  }


  public static EOFicheDePoste create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, NSTimestamp fdpDDebut
, String fdpVisaAgent
, String fdpVisaDirec
, String fdpVisaResp
, String fonctionConduiteProjet
, String fonctionEncadrement
, org.cocktail.fwkcktlgrh.common.metier.EOPoste toPoste) {
    EOFicheDePoste eo = (EOFicheDePoste) EOUtilities.createAndInsertInstance(editingContext, _EOFicheDePoste.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setFdpDDebut(fdpDDebut);
		eo.setFdpVisaAgent(fdpVisaAgent);
		eo.setFdpVisaDirec(fdpVisaDirec);
		eo.setFdpVisaResp(fdpVisaResp);
		eo.setFonctionConduiteProjet(fonctionConduiteProjet);
		eo.setFonctionEncadrement(fonctionEncadrement);
    eo.setToPosteRelationship(toPoste);
    return eo;
  }

  public static NSArray<EOFicheDePoste> fetchAll(EOEditingContext editingContext) {
    return _EOFicheDePoste.fetchAll(editingContext, null);
  }

  public static NSArray<EOFicheDePoste> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOFicheDePoste.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOFicheDePoste> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOFicheDePoste.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOFicheDePoste> eoObjects = (NSArray<EOFicheDePoste>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOFicheDePoste fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFicheDePoste.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFicheDePoste fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOFicheDePoste> eoObjects = _EOFicheDePoste.fetch(editingContext, qualifier, null);
    EOFicheDePoste eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOFicheDePoste)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_FicheDePoste that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFicheDePoste fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFicheDePoste.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOFicheDePoste fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOFicheDePoste eoObject = _EOFicheDePoste.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_FicheDePoste that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOFicheDePoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFicheDePoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFicheDePoste eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFicheDePoste)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOFicheDePoste localInstanceIn(EOEditingContext editingContext, EOFicheDePoste eo) {
    EOFicheDePoste localInstance = (eo == null) ? null : (EOFicheDePoste)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
  public static NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste> fetchFetchSuivi(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchSuivi", "Fwkgrh_FicheDePoste");
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste> fetchFetchSuivi(EOEditingContext editingContext,
	String cStructureBinding)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchSuivi", "Fwkgrh_FicheDePoste");
    NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
    bindings.takeValueForKey(cStructureBinding, "cStructure");
	fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
}
