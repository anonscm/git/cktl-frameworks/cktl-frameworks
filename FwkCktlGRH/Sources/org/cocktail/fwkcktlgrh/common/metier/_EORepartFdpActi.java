/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartFdpActi.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartFdpActi extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_RepartFdpActi";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String RFA_POSITION_KEY = "rfaPosition";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> RFA_POSITION = new ERXKey<Integer>("rfaPosition");
	// Relationships
	public static final String TO_ACTIVITE_PROFESSIONNELLE_KEY = "toActiviteProfessionnelle";
	public static final String TO_FICHE_DE_POSTE_KEY = "toFicheDePoste";
	public static final String TO_REFERENS_ACTIVITES_KEY = "toReferensActivites";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOActiviteProfessionnelle> TO_ACTIVITE_PROFESSIONNELLE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOActiviteProfessionnelle>("toActiviteProfessionnelle");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste> TO_FICHE_DE_POSTE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste>("toFicheDePoste");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> TO_REFERENS_ACTIVITES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites>("toReferensActivites");

  private static Logger LOG = Logger.getLogger(_EORepartFdpActi.class);

  public EORepartFdpActi localInstanceIn(EOEditingContext editingContext) {
    EORepartFdpActi localInstance = (EORepartFdpActi)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartFdpActi.LOG.isDebugEnabled()) {
    	_EORepartFdpActi.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartFdpActi.LOG.isDebugEnabled()) {
    	_EORepartFdpActi.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer rfaPosition() {
    return (Integer) storedValueForKey("rfaPosition");
  }

  public void setRfaPosition(Integer value) {
    if (_EORepartFdpActi.LOG.isDebugEnabled()) {
    	_EORepartFdpActi.LOG.debug( "updating rfaPosition from " + rfaPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "rfaPosition");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOActiviteProfessionnelle toActiviteProfessionnelle() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOActiviteProfessionnelle)storedValueForKey("toActiviteProfessionnelle");
  }

  public void setToActiviteProfessionnelleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOActiviteProfessionnelle value) {
    if (_EORepartFdpActi.LOG.isDebugEnabled()) {
      _EORepartFdpActi.LOG.debug("updating toActiviteProfessionnelle from " + toActiviteProfessionnelle() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOActiviteProfessionnelle oldValue = toActiviteProfessionnelle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toActiviteProfessionnelle");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toActiviteProfessionnelle");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste toFicheDePoste() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste)storedValueForKey("toFicheDePoste");
  }

  public void setToFicheDePosteRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste value) {
    if (_EORepartFdpActi.LOG.isDebugEnabled()) {
      _EORepartFdpActi.LOG.debug("updating toFicheDePoste from " + toFicheDePoste() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste oldValue = toFicheDePoste();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFicheDePoste");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFicheDePoste");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites toReferensActivites() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites)storedValueForKey("toReferensActivites");
  }

  public void setToReferensActivitesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites value) {
    if (_EORepartFdpActi.LOG.isDebugEnabled()) {
      _EORepartFdpActi.LOG.debug("updating toReferensActivites from " + toReferensActivites() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites oldValue = toReferensActivites();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toReferensActivites");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toReferensActivites");
    }
  }
  

  public static EORepartFdpActi create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste toFicheDePoste) {
    EORepartFdpActi eo = (EORepartFdpActi) EOUtilities.createAndInsertInstance(editingContext, _EORepartFdpActi.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToFicheDePosteRelationship(toFicheDePoste);
    return eo;
  }

  public static NSArray<EORepartFdpActi> fetchAll(EOEditingContext editingContext) {
    return _EORepartFdpActi.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartFdpActi> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartFdpActi.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartFdpActi> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartFdpActi.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartFdpActi> eoObjects = (NSArray<EORepartFdpActi>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartFdpActi fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartFdpActi.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartFdpActi fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartFdpActi> eoObjects = _EORepartFdpActi.fetch(editingContext, qualifier, null);
    EORepartFdpActi eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartFdpActi)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartFdpActi that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartFdpActi fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartFdpActi.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartFdpActi fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartFdpActi eoObject = _EORepartFdpActi.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartFdpActi that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartFdpActi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartFdpActi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartFdpActi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartFdpActi)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartFdpActi localInstanceIn(EOEditingContext editingContext, EORepartFdpActi eo) {
    EORepartFdpActi localInstance = (eo == null) ? null : (EORepartFdpActi)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
