/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOTplItem.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOTplItem extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_TplItem";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TIT_CODE_KEY = "titCode";
	public static final String TIT_COMMENTAIRE_KEY = "titCommentaire";
	public static final String TIT_KEY_KEY = "titKey";
	public static final String TIT_LIBELLE_KEY = "titLibelle";
	public static final String TIT_POSITION_KEY = "titPosition";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> TIT_CODE = new ERXKey<String>("titCode");
	public static final ERXKey<String> TIT_COMMENTAIRE = new ERXKey<String>("titCommentaire");
	public static final ERXKey<Integer> TIT_KEY = new ERXKey<Integer>("titKey");
	public static final ERXKey<String> TIT_LIBELLE = new ERXKey<String>("titLibelle");
	public static final ERXKey<Integer> TIT_POSITION = new ERXKey<Integer>("titPosition");
	// Relationships
	public static final String TOS_REPART_FICHE_ITEM_KEY = "tosRepartFicheItem";
	public static final String TOS_TPL_REPART_ITEM_ITEM_VALEUR_KEY = "tosTplRepartItemItemValeur";
	public static final String TO_TPL_BLOC_KEY = "toTplBloc";
	public static final String TO_TPL_ITEM_NATURE_KEY = "toTplItemNature";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem> TOS_REPART_FICHE_ITEM = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem>("tosRepartFicheItem");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur> TOS_TPL_REPART_ITEM_ITEM_VALEUR = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur>("tosTplRepartItemItemValeur");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc> TO_TPL_BLOC = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc>("toTplBloc");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplItemNature> TO_TPL_ITEM_NATURE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplItemNature>("toTplItemNature");

  private static Logger LOG = Logger.getLogger(_EOTplItem.class);

  public EOTplItem localInstanceIn(EOEditingContext editingContext) {
    EOTplItem localInstance = (EOTplItem)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
    	_EOTplItem.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
    	_EOTplItem.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String titCode() {
    return (String) storedValueForKey("titCode");
  }

  public void setTitCode(String value) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
    	_EOTplItem.LOG.debug( "updating titCode from " + titCode() + " to " + value);
    }
    takeStoredValueForKey(value, "titCode");
  }

  public String titCommentaire() {
    return (String) storedValueForKey("titCommentaire");
  }

  public void setTitCommentaire(String value) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
    	_EOTplItem.LOG.debug( "updating titCommentaire from " + titCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "titCommentaire");
  }

  public Integer titKey() {
    return (Integer) storedValueForKey("titKey");
  }

  public void setTitKey(Integer value) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
    	_EOTplItem.LOG.debug( "updating titKey from " + titKey() + " to " + value);
    }
    takeStoredValueForKey(value, "titKey");
  }

  public String titLibelle() {
    return (String) storedValueForKey("titLibelle");
  }

  public void setTitLibelle(String value) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
    	_EOTplItem.LOG.debug( "updating titLibelle from " + titLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "titLibelle");
  }

  public Integer titPosition() {
    return (Integer) storedValueForKey("titPosition");
  }

  public void setTitPosition(Integer value) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
    	_EOTplItem.LOG.debug( "updating titPosition from " + titPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "titPosition");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOTplBloc toTplBloc() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplBloc)storedValueForKey("toTplBloc");
  }

  public void setToTplBlocRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplBloc value) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
      _EOTplItem.LOG.debug("updating toTplBloc from " + toTplBloc() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOTplBloc oldValue = toTplBloc();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTplBloc");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTplBloc");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOTplItemNature toTplItemNature() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplItemNature)storedValueForKey("toTplItemNature");
  }

  public void setToTplItemNatureRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplItemNature value) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
      _EOTplItem.LOG.debug("updating toTplItemNature from " + toTplItemNature() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOTplItemNature oldValue = toTplItemNature();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTplItemNature");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTplItemNature");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem> tosRepartFicheItem() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem>)storedValueForKey("tosRepartFicheItem");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem> tosRepartFicheItem(EOQualifier qualifier) {
    return tosRepartFicheItem(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem> tosRepartFicheItem(EOQualifier qualifier, boolean fetch) {
    return tosRepartFicheItem(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem> tosRepartFicheItem(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem.TO_TPL_ITEM_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartFicheItem();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartFicheItemRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem object) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
      _EOTplItem.LOG.debug("adding " + object + " to tosRepartFicheItem relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartFicheItem");
  }

  public void removeFromTosRepartFicheItemRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem object) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
      _EOTplItem.LOG.debug("removing " + object + " from tosRepartFicheItem relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFicheItem");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem createTosRepartFicheItemRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartFicheItem");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartFicheItem");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem) eo;
  }

  public void deleteTosRepartFicheItemRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartFicheItem");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartFicheItemRelationships() {
    Enumeration objects = tosRepartFicheItem().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartFicheItemRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur> tosTplRepartItemItemValeur() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur>)storedValueForKey("tosTplRepartItemItemValeur");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur> tosTplRepartItemItemValeur(EOQualifier qualifier) {
    return tosTplRepartItemItemValeur(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur> tosTplRepartItemItemValeur(EOQualifier qualifier, boolean fetch) {
    return tosTplRepartItemItemValeur(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur> tosTplRepartItemItemValeur(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur.TO_TPL_ITEM_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosTplRepartItemItemValeur();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosTplRepartItemItemValeurRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur object) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
      _EOTplItem.LOG.debug("adding " + object + " to tosTplRepartItemItemValeur relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosTplRepartItemItemValeur");
  }

  public void removeFromTosTplRepartItemItemValeurRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur object) {
    if (_EOTplItem.LOG.isDebugEnabled()) {
      _EOTplItem.LOG.debug("removing " + object + " from tosTplRepartItemItemValeur relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosTplRepartItemItemValeur");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur createTosTplRepartItemItemValeurRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_TplRepartItemItemValeur");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosTplRepartItemItemValeur");
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur) eo;
  }

  public void deleteTosTplRepartItemItemValeurRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosTplRepartItemItemValeur");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosTplRepartItemItemValeurRelationships() {
    Enumeration objects = tosTplRepartItemItemValeur().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosTplRepartItemItemValeurRelationship((org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur)objects.nextElement());
    }
  }


  public static EOTplItem create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer titKey
, String titLibelle
, Integer titPosition
, org.cocktail.fwkcktlgrh.common.metier.EOTplBloc toTplBloc, org.cocktail.fwkcktlgrh.common.metier.EOTplItemNature toTplItemNature) {
    EOTplItem eo = (EOTplItem) EOUtilities.createAndInsertInstance(editingContext, _EOTplItem.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTitKey(titKey);
		eo.setTitLibelle(titLibelle);
		eo.setTitPosition(titPosition);
    eo.setToTplBlocRelationship(toTplBloc);
    eo.setToTplItemNatureRelationship(toTplItemNature);
    return eo;
  }

  public static NSArray<EOTplItem> fetchAll(EOEditingContext editingContext) {
    return _EOTplItem.fetchAll(editingContext, null);
  }

  public static NSArray<EOTplItem> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTplItem.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOTplItem> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTplItem.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTplItem> eoObjects = (NSArray<EOTplItem>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTplItem fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTplItem.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTplItem fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTplItem> eoObjects = _EOTplItem.fetch(editingContext, qualifier, null);
    EOTplItem eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTplItem)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_TplItem that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTplItem fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTplItem.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOTplItem fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTplItem eoObject = _EOTplItem.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_TplItem that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOTplItem fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTplItem fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTplItem eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTplItem)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOTplItem localInstanceIn(EOEditingContext editingContext, EOTplItem eo) {
    EOTplItem localInstance = (eo == null) ? null : (EOTplItem)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
