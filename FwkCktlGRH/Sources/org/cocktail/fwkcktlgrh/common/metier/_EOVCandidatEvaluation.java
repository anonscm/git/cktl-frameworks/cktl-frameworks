/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOVCandidatEvaluation.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOVCandidatEvaluation extends org.cocktail.fwkcktlgrh.common.metier.util.UtilEOEvaluationKeyValueCoding {
	public static final String ENTITY_NAME = "Fwkgrh_VCandidatEvaluation";

	// Attributes
	public static final String EPE_KEY_VISIBLE_KEY = "epeKeyVisible";
	public static final String EVA_KEY_VISIBLE_KEY = "evaKeyVisible";
	public static final String NO_INDIVIDU_VISIBLE_KEY = "noIndividuVisible";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PRENOM_KEY = "prenom";

	public static final ERXKey<Integer> EPE_KEY_VISIBLE = new ERXKey<Integer>("epeKeyVisible");
	public static final ERXKey<Integer> EVA_KEY_VISIBLE = new ERXKey<Integer>("evaKeyVisible");
	public static final ERXKey<Integer> NO_INDIVIDU_VISIBLE = new ERXKey<Integer>("noIndividuVisible");
	public static final ERXKey<String> NOM_USUEL = new ERXKey<String>("nomUsuel");
	public static final ERXKey<String> PRENOM = new ERXKey<String>("prenom");
	// Relationships
	public static final String TO_EVALUATION_KEY = "toEvaluation";
	public static final String TO_EVALUATION_PERIODE_KEY = "toEvaluationPeriode";
	public static final String TO_INDIVIDU_KEY = "toIndividu";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> TO_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>("toEvaluation");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode> TO_EVALUATION_PERIODE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode>("toEvaluationPeriode");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");

  private static Logger LOG = Logger.getLogger(_EOVCandidatEvaluation.class);

  public EOVCandidatEvaluation localInstanceIn(EOEditingContext editingContext) {
    EOVCandidatEvaluation localInstance = (EOVCandidatEvaluation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer epeKeyVisible() {
    return (Integer) storedValueForKey("epeKeyVisible");
  }

  public void setEpeKeyVisible(Integer value) {
    if (_EOVCandidatEvaluation.LOG.isDebugEnabled()) {
    	_EOVCandidatEvaluation.LOG.debug( "updating epeKeyVisible from " + epeKeyVisible() + " to " + value);
    }
    takeStoredValueForKey(value, "epeKeyVisible");
  }

  public Integer evaKeyVisible() {
    return (Integer) storedValueForKey("evaKeyVisible");
  }

  public void setEvaKeyVisible(Integer value) {
    if (_EOVCandidatEvaluation.LOG.isDebugEnabled()) {
    	_EOVCandidatEvaluation.LOG.debug( "updating evaKeyVisible from " + evaKeyVisible() + " to " + value);
    }
    takeStoredValueForKey(value, "evaKeyVisible");
  }

  public Integer noIndividuVisible() {
    return (Integer) storedValueForKey("noIndividuVisible");
  }

  public void setNoIndividuVisible(Integer value) {
    if (_EOVCandidatEvaluation.LOG.isDebugEnabled()) {
    	_EOVCandidatEvaluation.LOG.debug( "updating noIndividuVisible from " + noIndividuVisible() + " to " + value);
    }
    takeStoredValueForKey(value, "noIndividuVisible");
  }

  public String nomUsuel() {
    return (String) storedValueForKey("nomUsuel");
  }

  public void setNomUsuel(String value) {
    if (_EOVCandidatEvaluation.LOG.isDebugEnabled()) {
    	_EOVCandidatEvaluation.LOG.debug( "updating nomUsuel from " + nomUsuel() + " to " + value);
    }
    takeStoredValueForKey(value, "nomUsuel");
  }

  public String prenom() {
    return (String) storedValueForKey("prenom");
  }

  public void setPrenom(String value) {
    if (_EOVCandidatEvaluation.LOG.isDebugEnabled()) {
    	_EOVCandidatEvaluation.LOG.debug( "updating prenom from " + prenom() + " to " + value);
    }
    takeStoredValueForKey(value, "prenom");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluation)storedValueForKey("toEvaluation");
  }

  public void setToEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation value) {
    if (_EOVCandidatEvaluation.LOG.isDebugEnabled()) {
      _EOVCandidatEvaluation.LOG.debug("updating toEvaluation from " + toEvaluation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluation oldValue = toEvaluation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluation");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode toEvaluationPeriode() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode)storedValueForKey("toEvaluationPeriode");
  }

  public void setToEvaluationPeriodeRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode value) {
    if (_EOVCandidatEvaluation.LOG.isDebugEnabled()) {
      _EOVCandidatEvaluation.LOG.debug("updating toEvaluationPeriode from " + toEvaluationPeriode() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode oldValue = toEvaluationPeriode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluationPeriode");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluationPeriode");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOVCandidatEvaluation.LOG.isDebugEnabled()) {
      _EOVCandidatEvaluation.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  

  public static EOVCandidatEvaluation create(EOEditingContext editingContext, Integer epeKeyVisible
, Integer evaKeyVisible
, String nomUsuel
, String prenom
, org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode toEvaluationPeriode, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu) {
    EOVCandidatEvaluation eo = (EOVCandidatEvaluation) EOUtilities.createAndInsertInstance(editingContext, _EOVCandidatEvaluation.ENTITY_NAME);    
		eo.setEpeKeyVisible(epeKeyVisible);
		eo.setEvaKeyVisible(evaKeyVisible);
		eo.setNomUsuel(nomUsuel);
		eo.setPrenom(prenom);
    eo.setToEvaluationPeriodeRelationship(toEvaluationPeriode);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  public static NSArray<EOVCandidatEvaluation> fetchAll(EOEditingContext editingContext) {
    return _EOVCandidatEvaluation.fetchAll(editingContext, null);
  }

  public static NSArray<EOVCandidatEvaluation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVCandidatEvaluation.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOVCandidatEvaluation> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOVCandidatEvaluation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVCandidatEvaluation> eoObjects = (NSArray<EOVCandidatEvaluation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOVCandidatEvaluation fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVCandidatEvaluation.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVCandidatEvaluation fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVCandidatEvaluation> eoObjects = _EOVCandidatEvaluation.fetch(editingContext, qualifier, null);
    EOVCandidatEvaluation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOVCandidatEvaluation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_VCandidatEvaluation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVCandidatEvaluation fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVCandidatEvaluation.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOVCandidatEvaluation fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVCandidatEvaluation eoObject = _EOVCandidatEvaluation.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_VCandidatEvaluation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOVCandidatEvaluation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVCandidatEvaluation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVCandidatEvaluation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVCandidatEvaluation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOVCandidatEvaluation localInstanceIn(EOEditingContext editingContext, EOVCandidatEvaluation eo) {
    EOVCandidatEvaluation localInstance = (eo == null) ? null : (EOVCandidatEvaluation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
  public static NSArray<org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation> fetchFetchSuivi(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchSuivi", "Fwkgrh_VCandidatEvaluation");
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation> fetchFetchSuivi(EOEditingContext editingContext,
	org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode periodeBinding)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchSuivi", "Fwkgrh_VCandidatEvaluation");
    NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
    bindings.takeValueForKey(periodeBinding, "periode");
	fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
}
