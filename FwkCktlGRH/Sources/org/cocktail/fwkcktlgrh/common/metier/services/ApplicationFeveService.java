package org.cocktail.fwkcktlgrh.common.metier.services;

import org.cocktail.fwkcktlgrh.common.metier.EOFeveParametres;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class ApplicationFeveService {

	private EOEditingContext editingContext;

	// TODO:
	// à refaire avec un tableau et tous les paramêtres pour FEVE

	private String key_FEV_EVALUATION_SAISIE_D_DEBUT;
	private String key_FEV_EVALUATION_SAISIE_D_FIN;
	private String key_FEV_FICHE_LOLF_SAISIE_D_DEBUT;
	private String key_FEV_FICHE_LOLF_SAISIE_D_FIN;

	// gestion des variables globales a l'application et de leur cache

	public ApplicationFeveService(EOEditingContext ec) {
		editingContext=ec;
		NSMutableArray<EOFeveParametres> val= EOFeveParametres.getParametres(editingContext,EOFeveParametres.KEY_FEV_EVALUATION_SAISIE_D_DEBUT,EOFeveParametres.PARAM_KEY_KEY);
		if (val.size()>0) {
			key_FEV_EVALUATION_SAISIE_D_DEBUT=val.get(0).paramValue(); 
		}
		val= EOFeveParametres.getParametres(editingContext,EOFeveParametres.KEY_FEV_EVALUATION_SAISIE_D_FIN,EOFeveParametres.PARAM_KEY_KEY);
		if (val.size()>0) {
			key_FEV_EVALUATION_SAISIE_D_FIN=val.get(0).paramValue(); 
		}
		val= EOFeveParametres.getParametres(editingContext,EOFeveParametres.KEY_FEV_FICHE_LOLF_SAISIE_D_DEBUT,EOFeveParametres.PARAM_KEY_KEY);
		if (val.size()>0) {
			key_FEV_FICHE_LOLF_SAISIE_D_DEBUT=val.get(0).paramValue(); 
		}
		val= EOFeveParametres.getParametres(editingContext,EOFeveParametres.KEY_FEV_FICHE_LOLF_SAISIE_D_FIN,EOFeveParametres.PARAM_KEY_KEY);
		if (val.size()>0) {
			key_FEV_FICHE_LOLF_SAISIE_D_FIN=val.get(0).paramValue(); 
		}

	}

	public NSTimestamp getEvaluationSaisieDDebut() {
		return DateCtrl.stringToDate(key_FEV_EVALUATION_SAISIE_D_DEBUT);
	}

	public NSTimestamp getEvaluationSaisieDFin() {
		return DateCtrl.stringToDate(key_FEV_EVALUATION_SAISIE_D_FIN);
	}

	public NSTimestamp getFicheLolfSaisieDDebut() {
		return DateCtrl.stringToDate(key_FEV_FICHE_LOLF_SAISIE_D_DEBUT);
	}

	public NSTimestamp getFicheLolfSaisieDFin() {
		return DateCtrl.stringToDate(key_FEV_FICHE_LOLF_SAISIE_D_FIN);
	}

	/**
	 * Indique si l'application autorise la saisie des fiches LOLF
	 * 
	 * @return
	 */
	public boolean isPeriodeLolfOuverte() {
		return (DateCtrl.isAfterEq(DateCtrl.now(), getFicheLolfSaisieDDebut()) && DateCtrl.isBeforeEq(DateCtrl.now(), getFicheLolfSaisieDFin().timestampByAddingGregorianUnits(0, 0, 0, 23, 59, 59)));
	}

	/**
	 * Indique si l'application autorise la saisie des fiches LOLF
	 * 
	 * @return
	 */
	public boolean isPeriodeEvaluationOuverte() {
		return (DateCtrl.isAfterEq(DateCtrl.now(), getEvaluationSaisieDDebut()) && DateCtrl.isBeforeEq(DateCtrl.now(), getEvaluationSaisieDFin().timestampByAddingGregorianUnits(0, 0, 0, 23, 59, 59)));
	}

	public boolean isCodeActivationActif(String key) {
		NSMutableArray<EOFeveParametres> val= EOFeveParametres.getParametres(editingContext,key,EOFeveParametres.PARAM_KEY_KEY);
		if (val.size()>0) {
			return CktlParamManager.booleanForValue(val.get(0).paramValue()); 
		}
		return false;		
	}
	

}
