package org.cocktail.fwkcktlgrh.common.metier.services;

import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EOStructureInfo;
import org.cocktail.fwkcktlgrh.common.metier.finder.Finder;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class FinderFeveService extends Finder {

	/**
	 * liste tous les champ libres associes a la fiche de poste (emploi type,
	 * mission composante, service ...)
	 */
	public NSDictionary findDicoFicheDePosteInContext(EOEditingContext ec, EOFicheDePoste fiche) {
		NSMutableDictionary dico = new NSMutableDictionary();

		if (fiche != null) {
			// la date pour recuperer les infos des structure
			// - si la date est la fiche en cours : today
			// - si date passe, on prend la fin de la fiche
			NSTimestamp dateRef = null;
			if (fiche.fdpDFin() != null && DateCtrl.isAfter(DateCtrl.now(), fiche.fdpDFin())) {
				dateRef = fiche.fdpDFin();
			} else {
				dateRef = DateCtrl.now();
			}

			EOStructureInfo laMissionComposante = EOStructureInfo.findStructureInfoForStructureAndDateAndTypeInContext(
					ec,
					fiche.toPoste().toStructure().toComposante(),
					dateRef,
					EOStructureInfo.getTYPE_MISSION_COMPOSANTE()
					);
			if (laMissionComposante != null && !StringCtrl.isEmpty(laMissionComposante.sinLibelle())) {
				dico.setObjectForKey(laMissionComposante.sinLibelle(), "missionComposante");
			} else {
				dico.setObjectForKey("", "missionComposante");
			}

			EOStructureInfo laMissionService = EOStructureInfo.findStructureInfoForStructureAndDateAndTypeInContext(
					ec,
					fiche.toPoste().toStructure(),
					dateRef,
					EOStructureInfo.getTYPE_MISSION_SERVICE()
					);
			if (laMissionService != null && !StringCtrl.isEmpty(laMissionService.sinLibelle())) {
				dico.setObjectForKey(laMissionService.sinLibelle(), "missionService");
			} else {
				dico.setObjectForKey("", "missionService");
			}

			EOStructureInfo leProjetService = EOStructureInfo.findStructureInfoForStructureAndDateAndTypeInContext(
					ec,
					fiche.toPoste().toStructure(),
					dateRef,
					EOStructureInfo.getTYPE_PROJET_SERVICE()
					);
			if (leProjetService != null && !StringCtrl.isEmpty(leProjetService.sinLibelle())) {
				dico.setObjectForKey(leProjetService.sinLibelle(), "projetService");
			} else {
				dico.setObjectForKey("", "projetService");
			}

			if (!StringCtrl.isEmpty(fiche.fdpMissionPoste())) {
				dico.setObjectForKey(fiche.fdpMissionPoste(), "missionPoste");
			} else {
				dico.setObjectForKey("", "missionPoste");
			}

			if (!StringCtrl.isEmpty(fiche.fdpContexteTravail())) {
				dico.setObjectForKey(fiche.fdpContexteTravail(), "contexte");
			} else {
				dico.setObjectForKey("", "contexte");
			}

			// composante - service
			dico.setObjectForKey(fiche.toPoste().toStructure().toComposante().llStructure(), "composante");
			dico.setObjectForKey(fiche.toPoste().toStructure().llStructure(), "structure");

			// dcp - famille - emploi type
			if (fiche.toReferensEmplois() != null) {
				dico.setObjectForKey(fiche.toReferensEmplois().intitulEmploi(), "emploiType");
				dico.setObjectForKey(fiche.toReferensEmplois().toReferensFp().intitulFp(), "famillePro");
				dico.setObjectForKey(fiche.toReferensEmplois().toReferensFp().toReferensDcp().intitulDcp(), "dcp");
			} else {
				dico.setObjectForKey("", "emploiType");
				dico.setObjectForKey("", "famillePro");
				dico.setObjectForKey("", "dcp");
			}

			// libelle
			dico.setObjectForKey(fiche.display(), "libelle");
		}

		return dico.immutableClone();
	}

	/**
	 * le dictionnaire contenant toutes les infos pour l'impression de
	 * l'evaluation (objectifs, niveau des competences ...)
	 */
	public NSDictionary<String, Object> findDicoEvaluationInContext(EOEditingContext ec, EOEvaluation evaluation) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();

		if (evaluation != null) {
			
			dico.setObjectForKey(evaluation.toEvaluationPeriode().strAnneeDebutAnneeFin(), "periode");

			NSArray fiches = evaluation.tosLastFicheDePoste();
			
			NSArray services = evaluation.tosStructure();
			String strServices = "";
			for (int i = 0; i < services.count(); i++) {
				EOStructure unService = (EOStructure) services.objectAtIndex(i);
				strServices += unService.llStructure();
				if (i != services.count() - 1) {
					strServices += " ; ";
				}
			}
			dico.setObjectForKey(strServices, "services");

			// emplois types
			NSArray emplois = new NSArray();
			String strEmplois = "";
			for (int i = 0; i < fiches.count(); i++) {
				EOFicheDePoste uneFicheDePoste = (EOFicheDePoste) fiches.objectAtIndex(i);
				if (uneFicheDePoste.toReferensEmplois() != null) {
					strEmplois += uneFicheDePoste.toReferensEmplois().intitulEmploi();
					if (i < emplois.count() - 1) {
						strEmplois += " ; ";
					}
				}
			}
			dico.setObjectForKey(strEmplois, "emplois");

			// evaluation precedente
			EOEvaluation evaluationPrec = evaluation.toEvaluationPrecedente();
			if (evaluationPrec != null) {
				dico.setObjectForKey(evaluationPrec, "lEvaluationPrecedente");
			}

			// evaluation actuelle
			dico.setObjectForKey(evaluation, "lEvaluationEnCours");

			// evaluation suivante
			EOEvaluation evaluationSuiv = evaluation.toEvaluationSuivante();
			if (evaluationSuiv != null) {
				dico.setObjectForKey(evaluationSuiv, "lEvaluationSuivante");
			}
		}

		return dico.immutableClone();
	}

	/**
	 * trouver un record pour un individu selon un nom et/ou un prenom
	 * 
	 * @param ec
	 * @param nomOuPrenom
	 * @param entity
	 * @param prefix
	 * @return
	 */
	public NSArray filterIndividuForNomOrPrenomInArray(NSArray array, String nomOuPrenom, String prefix) {
		// mise en caps
		nomOuPrenom = nomOuPrenom.toUpperCase();
		NSArray records = new NSArray();
		NSArray mots = NSArray.componentsSeparatedByString(nomOuPrenom, " ");
		String premierMot, deuxiemeMot;
		premierMot = deuxiemeMot = "";
		String localPrefix = (prefix != null ? prefix : StringCtrl.emptyString());
		if (mots.count() > 0) {
			premierMot = (String) mots.objectAtIndex(0);
			if (mots.count() > 1) {
				deuxiemeMot = (String) mots.objectAtIndex(1);
			}
			NSArray args = new NSArray(new String[] { "*" + nomOuPrenom + "*", "*" + nomOuPrenom + "*", "*" + premierMot + "*", "*" + deuxiemeMot + "*", "*" + premierMot + "*", "*" + deuxiemeMot + "*" });
			String strQual = localPrefix + "nomUsuel like %@ OR " + localPrefix + "prenom like %@ OR ("
							+ localPrefix + "nomUsuel like %@ AND " + localPrefix + "prenom like %@) OR ("
							+ localPrefix + "prenom like %@ AND " + localPrefix + "nomUsuel like %@)";
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(strQual, args);
			NSArray arraySort = new NSArray(EOSortOrdering.sortOrderingWithKey(localPrefix + "nomUsuel", EOSortOrdering.CompareAscending));
			records = EOQualifier.filteredArrayWithQualifier(array, qual);
			records = EOSortOrdering.sortedArrayUsingKeyOrderArray(records, arraySort);
			records = NSArrayCtrl.removeDuplicate(records);
		}
		return records;
	}
	
}
