package org.cocktail.fwkcktlgrh.common.metier.services;

import java.util.GregorianCalendar;

import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant;
import org.cocktail.fwkcktlgrh.common.metier.EOContratHeberge;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlgrh.common.metier.finder.CarriereFinder;
import org.cocktail.fwkcktlgrh.common.metier.finder.VacatairesFinder;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public class IndividusGradesService {

	private CarriereFinder carriereFinder;
	private EOEditingContext edc;

	private IndividusGradesService(EOEditingContext edc) {
		this.edc = edc;
		this.carriereFinder = new CarriereFinder();
		this.carriereFinder.setEdc(edc);
	}

	private EOEditingContext edc() {
		return edc;
	}

	public static IndividusGradesService creerNouvelleInstance(EOEditingContext edc) {
		return new IndividusGradesService(edc);
	}

	public EOGrade gradePourIndividu(EOIndividu individu, NSTimestamp date) {

		NSDictionary<EOIndividu, EOGrade> grades = gradesPourIndividus(new NSArray<EOIndividu>(individu), date);
		if (grades.isEmpty()) {
			return null;
		} else {
			return grades.objectForKey(individu);
		}

	}

	public NSDictionary<EOIndividu, EOGrade> gradesPourIndividus(NSArray<EOIndividu> individus, NSTimestamp date) {
		NSMutableDictionary<EOIndividu, EOGrade> gradesPourIndividus = new NSMutableDictionary<EOIndividu, EOGrade>();

		NSArray<EOContrat> contrats = ContratsService.creerNouvelleInstance(edc()).contratsPourIndividusEtDate(individus, date);
		NSArray<EOElements> elementsDeCarriere = carriereFinder.elementsDeCarrierePourIndividusEtDate(individus, date);
		NSArray<EOContratHeberge> contratsHeberges = ContratsHebergesService.creerNouvelleInstance(edc()).contratsHebergesPourIndividusEtDate(individus, date);

		for (EOIndividu individu : individus) {
			EOGrade grade = null;

			grade = rechercherGradeIndividuDansContratsPourDate(contrats, individu, date, false);
			if (grade == null || NSKeyValueCoding.NullValue.equals(grade)) {
				grade = rechercherGradeIndividuDansElementsDeCarriere(elementsDeCarriere, individu);
				if (grade == null || NSKeyValueCoding.NullValue.equals(grade)) {
					grade = rechercherGradeIndividuDansContratsHeberges(contratsHeberges, individu);
					if (grade == null || NSKeyValueCoding.NullValue.equals(grade)) {
						grade = rechercherGradeIndividuDansContratsPourDate(contrats, individu, date, true);
					}
				}
			}
			if (grade != null && !grade.equals(NSKeyValueCoding.NullValue)) {
				gradesPourIndividus.put(individu, grade);
			}
		}

		return gradesPourIndividus.immutableClone();
	}
	
	/**
	 * @param contrats : liste des contrats
	 * @param individu : individu
	 * @param date : date 
	 * @param vacataire : Est vacataire? oui/non
	 * @return le grade de l'agent pour une date donnée
	 */
	private EOGrade rechercherGradeIndividuDansContratsPourDate(NSArray<EOContrat> contrats, EOIndividu individu, NSTimestamp date, Boolean vacataire) {
		NSArray<EOContrat> contratsIndividu = ERXQ.filtered(contrats,
				ERXQ.equals(ERXQ.keyPath(EOContrat.TO_PERSONNEL_KEY, EOPersonnel.TO_INDIVIDU_KEY, EOIndividu.NO_INDIVIDU_KEY), individu.noIndividu()));

		if (!contratsIndividu.isEmpty()) {
			EOContrat contratIndividu = ERXArrayUtilities.firstObject(contratsIndividu);

			if (!vacataire) {
				EOQualifier qualifierContratAvenants = 
						ERXQ.and(
								ERXQ.lessThanOrEqualTo(EOContratAvenant.D_DEB_AVENANT_KEY, date),
								ERXQ.or(ERXQ.isNull(EOContratAvenant.D_FIN_AVENANT_KEY), ERXQ.greaterThanOrEqualTo(EOContratAvenant.D_FIN_AVENANT_KEY, date)),
								ERXQ.equals(EOContratAvenant.TEM_ANNULATION_KEY, EOContratAvenant.TEM_ANNULATION_NON)
						);

				NSArray<EOContratAvenant> contratAvenants = ERXQ.filtered(contratIndividu.toContratAvenants(), qualifierContratAvenants);

				if (!contratAvenants.isEmpty()) {
					return ERXArrayUtilities.firstObject(contratAvenants).toGrade();
				} 
			} else {
				if (!VacatairesFinder.sharedInstance().getListeVacationsValidesPourIndividuEtPeriode(edc, individu, date, null).isEmpty()) {
					VacatairesFinder.sharedInstance().getListeVacationsValidesPourIndividuEtPeriode(edc, individu, date, null).get(0).getToGrade();
				}
			}
		}
			
		return null;
	}

	private EOGrade rechercherGradeIndividuDansElementsDeCarriere(NSArray<EOElements> elementsDeCarriere, EOIndividu individu) {
		NSArray<EOElements> elementsIndividu = ERXQ.filtered(elementsDeCarriere, ERXQ.equals(EOElements.TO_INDIVIDU_KEY, individu));
		if (elementsIndividu.isEmpty()) {
			return null;
		} else {
			return ERXArrayUtilities.firstObject(elementsIndividu).toGrade();
		}
	}

	private EOGrade rechercherGradeIndividuDansContratsHeberges(NSArray<EOContratHeberge> contratsHeberges, EOIndividu individu) {
		NSArray<EOContratHeberge> contratsIndividu = ERXQ.filtered(contratsHeberges,
				ERXQ.equals(ERXQ.keyPath(EOContratHeberge.TO_PERSONNEL_KEY, EOPersonnel.TO_INDIVIDU_KEY, EOIndividu.NO_INDIVIDU_KEY), individu.noIndividu()));
		if (contratsIndividu.isEmpty()) {
			return null;
		} else {
			return ERXArrayUtilities.firstObject(contratsIndividu).toGrade();
		}
	}

	public void setCarriereFinder(CarriereFinder carriereFinder) {
		this.carriereFinder = carriereFinder;
	}

}
