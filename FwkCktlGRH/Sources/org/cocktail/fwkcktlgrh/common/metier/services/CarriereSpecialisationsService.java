package org.cocktail.fwkcktlgrh.common.metier.services;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlgrh.common.metier.EOCarriere;
import org.cocktail.fwkcktlgrh.common.metier.EOCarriereSpecialisations;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * Classe de services de CarriereSpecialisations
 */
public class CarriereSpecialisationsService {

	private EOEditingContext edc;

	/**
	 * Constructeur
	 * @param edc editing context
	 */
	public CarriereSpecialisationsService(EOEditingContext edc) {
		this.edc = edc;
	}
	
	/**
	 * @return editing context
	 */
	private EOEditingContext edc() {
		return edc;
	}

	/**
	 * @param individu l'individu dont on cherche les infos
	 * @return la carriere specialisation courante
	 */
	public EOCarriereSpecialisations getCurrentCarriereSpecialisation(EOIndividu individu) {
		EOCarriere carriere = EOCarriere.rechercherCarriereEnCours(edc(), individu);
			
		if (carriere == null) {
			return null;
		}
		
		EOQualifier qualifier = 
				ERXQ.and(
						EOCarriereSpecialisations.TO_INDIVIDU.eq(individu),
						EOCarriereSpecialisations.TO_CARRIERE.eq(carriere),
						EOCarriereSpecialisations.SPEC_DEBUT.lessThanOrEqualTo(MyDateCtrl.getDateJour()),
						ERXQ.or(
								EOCarriereSpecialisations.SPEC_FIN.isNull(),
								EOCarriereSpecialisations.SPEC_FIN.greaterThanOrEqualTo(MyDateCtrl.getDateJour())
								)
						);
		
		return EOCarriereSpecialisations.fetchFirstByQualifier(edc(), qualifier);
	}
}
