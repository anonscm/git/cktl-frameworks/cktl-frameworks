package org.cocktail.fwkcktlgrh.common.metier.services;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CarrieresService {
	
	
	public CarrieresService() {
		
	}
	
	
	public String formatDate(Date dt) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		if (dt == null) {
			return "";
		}
		
		return sdf.format(dt);
	}
}
