package org.cocktail.fwkcktlgrh.common.metier.services;

import org.cocktail.fwkcktlgrh.common.metier.EOContratHeberge;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

public class ContratsHebergesService {
	
	private EOEditingContext edc;

	private ContratsHebergesService(EOEditingContext edc) {
		this.edc = edc;
	}
	
	public EOEditingContext edc() {
		return edc;
	}

	public static ContratsHebergesService creerNouvelleInstance(EOEditingContext edc) {
		return new ContratsHebergesService(edc);
	}
	
	public NSArray<EOContratHeberge> contratsHebergesPourIndividusEtDate(NSArray<EOIndividu> individus, NSTimestamp date) {
		NSArray<EOContratHeberge> contratsHeberges;
		
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.lessThanOrEqualTo(EOContratHeberge.DATE_DEBUT_KEY, date),
						ERXQ.or(
								ERXQ.isNull(EOContratHeberge.DATE_FIN_KEY),
								ERXQ.greaterThanOrEqualTo(EOContratHeberge.DATE_FIN_KEY, date)
							),
						ERXQ.in(ERXQ.keyPath(EOContratHeberge.TO_PERSONNEL_KEY, EOPersonnel.TO_INDIVIDU_KEY), individus),
						ERXQ.equals(EOContratHeberge.TEM_VALIDE_KEY, EOContratHeberge.TEM_VALIDATION_VALIDE)
					);				
		
		NSMutableArray<String> prefetchingKeyPaths = new NSMutableArray<String>();
		prefetchingKeyPaths.add(EOContratHeberge.TO_GRADE_KEY);
		prefetchingKeyPaths.add(ERXQ.keyPath(EOContratHeberge.TO_PERSONNEL_KEY, EOPersonnel.TO_INDIVIDU_KEY));

		ERXFetchSpecification<EOContratHeberge> fetchSpecification = new ERXFetchSpecification<EOContratHeberge>(EOContratHeberge.ENTITY_NAME, qualifier, null);
		fetchSpecification.setPrefetchingRelationshipKeyPaths(prefetchingKeyPaths);
		
		contratsHeberges = fetchSpecification.fetchObjects(edc());			
		
		return contratsHeberges;
	}
	
}
