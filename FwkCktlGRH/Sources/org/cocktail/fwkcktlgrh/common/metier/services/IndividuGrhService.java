package org.cocktail.fwkcktlgrh.common.metier.services;

import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail;
import org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant;
import org.cocktail.fwkcktlgrh.common.metier.EODif;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf;
import org.cocktail.fwkcktlgrh.common.metier.EOPoste;
import org.cocktail.fwkcktlgrh.common.metier.EOStage;
import org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.util.UtilFiche;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelEns;
import org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelActuelNonEns;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

public class IndividuGrhService {

	
	/**
	 * retourne vrai si l'individu appartient a la structure via RepartStructure
	 */
	public boolean belongStructureRepartStructure(EOEditingContext edc, EOIndividu individu, String cStructure) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("persId = %@ AND cStructure = %@",
				new NSArray(new Object[] { individu.persId(), cStructure }));
		NSArray records = GRHUtilities.fetchArray(edc, EORepartStructure.ENTITY_NAME, qual, null);
		return (records.count() > 0);
	}
	
	
    /**
	 * Le corps d'appartenance d'un titulaire sur une période données
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	private EOCorps getTitulaireCorpsForPeriode(EOIndividu individu, EOEditingContext edc, NSTimestamp dateDebut, NSTimestamp dateFin) {

		NSArray<EOElements> lesElementsCarriere = EOElements.findSortedElementCarriereForIndividuAndDateInContext(
				edc, individu, dateDebut, dateFin);

		// corps d'appartenance
		EOCorps corps = null;
		if (lesElementsCarriere.count() > 0) {
			corps = (((EOElements) lesElementsCarriere.lastObject())).toCorps();
		}

		return corps;
	}
    
	/**
	 * Indique si l'individu est fonctionnaire sur une période de référence
	 * 
	 * @param dateReference
	 * @return
	 */
	public boolean isFonctionnaire(EOIndividu individu, EOEditingContext edc, NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSArray<EOElements> lesElementsCarriere = EOElements.findSortedElementCarriereForIndividuAndDateInContext(
				edc, individu, dateDebut, dateFin);
		return lesElementsCarriere.count() > 0;
	}

	/**
	 * Indique si l'individu est fonctionnaire stagiaire sur une période de
	 * référence
	 * 
	 * @param dateReference
	 * @return
	 */
	public boolean isStagiaire(EOIndividu individu, EOEditingContext edc, NSTimestamp dateReference) {
		StageService stageService = new StageService();
		NSArray<EOStage> lesStages = stageService.findStageForIndividuAndDateInContext(
				edc, individu, dateReference);
		return lesStages.count() > 0;
	}

	/**
	 * Indique si l'individu est contractuel sur une période de référence
	 * 
	 * @param dateReference
	 * @return
	 */
	public boolean isContractuel(EOIndividu individu, EOEditingContext edc, NSTimestamp dateDebut, NSTimestamp dateFin) {
		ContratAvenantService contratAvenantService = new ContratAvenantService();
		NSArray<EOContratAvenant> lesContrats = contratAvenantService.findSortedContratForIndividuAndDateInContext(
				edc, individu, dateDebut, dateFin);
		return lesContrats.count() > 0;
	}
	
	
	
	
	
	/**
	 * Indique si l'individu fait partie de l'ANES sur une période de référence
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public boolean isAenes(EOIndividu individu, EOEditingContext edc, NSTimestamp dateDebut, NSTimestamp dateFin) {
		boolean isAenes = false;

		EOCorps eoCorps = getTitulaireCorpsForPeriode(individu, edc, dateDebut, dateFin);
		if (eoCorps != null
				&& eoCorps.toTypePopulation() != null
				&& eoCorps.toTypePopulation().isAenes()) {
			isAenes = true;
		}

		return isAenes;
	}

	/**
	 * Indique si l'individu fait partie de l'ITRF sur une période de référence
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public boolean isItrf(EOIndividu individu, EOEditingContext edc, NSTimestamp dateDebut, NSTimestamp dateFin) {
		boolean isItrf = false;

		EOCorps eoCorps = getTitulaireCorpsForPeriode(individu, edc, dateDebut, dateFin);
		if (eoCorps != null
				&& eoCorps.toTypePopulation() != null
				&& eoCorps.toTypePopulation().isItrf()) {
			isItrf = true;
		}

		return isItrf;
	}

	/**
	 * Indique si l'individu fait partie de la BU sur une période de référence
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public boolean isBu(EOIndividu individu, EOEditingContext edc, NSTimestamp dateDebut, NSTimestamp dateFin) {
		boolean isBu = false;

		EOCorps eoCorps = getTitulaireCorpsForPeriode(individu, edc, dateDebut, dateFin);
		if (eoCorps != null
				&& eoCorps.toTypePopulation() != null
				&& eoCorps.toTypePopulation().isBu()) {
			isBu = true;
		}

		return isBu;
	}

	/**
	 * retourne un dico contenant les informations GEPETO d'un agent
	 * 
	 * @param ec
	 * @param record
	 * @return
	 */
	public NSDictionary<String, Object> findDicoAgentGepetoInContext(EOEditingContext ec, EOGenericRecord record) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();

		if (record != null) {

			EOIndividu individu = null;
			NSTimestamp debut = null;
			NSTimestamp fin = null;
			if (record instanceof EOAffectationDetail) {
				individu = ((EOAffectationDetail) record).toAffectation().toIndividu();
				debut = ((EOAffectationDetail) record).dDebut();
				fin = ((EOAffectationDetail) record).dFin();
				if (fin == null) {
					fin = DateCtrl.now();
				}
			} else if (record instanceof EOEvaluation) {
				individu = ((EOEvaluation) record).toIndividu();
				debut = ((EOEvaluation) record).toEvaluationPeriode().epeDDebut();
				fin = ((EOEvaluation) record).toEvaluationPeriode().epeDFin();
			}

			// contractuel ou (fonctionnaire ou fonctionnaire stagiaire)
			String statut = "";
			if (isFonctionnaire(individu, ec, debut, fin)) {
				statut = "Fonctionnaire";
				if (isStagiaire(individu, ec, fin)) {
					statut += " Stagiaire";
				}
			} else {
				statut = "Contractuel";
			}

			// corps d'appartenance
			EOCorps eoCorps = getCorpsForPeriode(individu, ec, debut, fin);

			// grade de l'individu
			EOGrade eoGrade = getGradeForPeriode(individu, ec, debut, fin);
			String echelon = getEchelonForPeriode(individu, ec, debut, fin);				
			dico.setObjectForKey(Integer.toString(individu.noIndividu().intValue()), "identifiant");
			dico.setObjectForKey(individu.nomUsuel(), "nomUsuel");
			if (individu.nomPatronymique() != null) {
				dico.setObjectForKey(individu.nomPatronymique(), "nomFamille");
			} else {
				dico.setObjectForKey(individu.nomUsuel(), "nomFamille");
			}
			dico.setObjectForKey(individu.prenom(), "prenom");
			dico.setObjectForKey(DateCtrl.dateToString(individu.dNaissance()), "dNaissance");
			if (echelon != null) {
				dico.setObjectForKey(echelon, "echelon");	
			}
			
			
			NSTimestamp dateEchelon = getDateEchelonForPeriode(individu, ec, debut, fin);
			if (dateEchelon != null) {
				dico.setObjectForKey(DateCtrl.dateToString(dateEchelon), "dEchelon");
			}
			
			dico.setObjectForKey(statut, "statut");

			dico.setObjectForKey("", "corps");	
			if (eoCorps != null) {
				if (eoCorps.llCorps() != null) {
					dico.setObjectForKey(eoCorps.llCorps(), "corps");
				} else if (eoCorps.lcCorps() != null) {
					dico.setObjectForKey(eoCorps.lcCorps(), "corps");
				} 
			}

			dico.setObjectForKey("", "grade");
			if (eoGrade != null) {
				dico.setObjectForKey(eoGrade.llGrade(), "grade");
			}
			
			EOQualifier qualifier = EODif.INDIVIDU.eq(individu);
			
			
			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EODif.DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));
		
			int totalDif=0;
			
			EODif eodif =EODif.fetchFirstByQualifier(ec, qualifier,sortOrderings);
						
			if (eodif!=null)
			{
				totalDif=eodif.balance().intValue();
			}
			dico.setObjectForKey(totalDif+"h", "balanceDif");			
		}

		return dico.immutableClone();
	}
	
	/**
	 * tourver un individu par son persId
	 * 
	 * @param ec
	 * @param persId
	 * @return
	 */
	public EOIndividu findIndividuForPersIdInContext(EOEditingContext ec, Number persId) {
		return EOIndividu.individuWithPersId(ec, persId.intValue());
	}

	/**
	 * trouve un individu d'apres son numero
	 * 
	 * @param ec
	 * @param noIndividu
	 * @return
	 */
	public EOIndividu findIndividuForNoIndividuInContext(EOEditingContext ec, Number noIndividu) {
		return (EOIndividu)EOIndividu.individuPourNumero(ec, noIndividu.intValue());
	}

	
	
	public NSArray<EOIndividu> findAllIndividuActuelNonEnsInContext(EOEditingContext ec) {
		NSArray<EOIndividu> records = (NSArray<EOIndividu>) GRHUtilities.fetchArray(ec, EOVPersonnelActuelNonEns.ENTITY_NAME, null, null).valueForKey("toIndividu");
		// records = EOSortOrdering.sortedArrayUsingKeyOrderArray(records,
		// Individu.arraySort);
		return records;
	}

	public NSArray<EOIndividu> findAllIndividuActuelEnsInContext(EOEditingContext ec) {
		NSArray<EOIndividu> records = (NSArray<EOIndividu>) GRHUtilities.fetchArray(ec, EOVPersonnelActuelEns.ENTITY_NAME, null, null).valueForKey("toIndividu");
		// records = EOSortOrdering.sortedArrayUsingKeyOrderArray(records,
		// Individu.arraySort);
		return records;
	}
	
	
	
	/**
	 * Grade de l'agent sur la période
	 * 
	 * @param debut
	 * @param fin
	 * @return
	 */
	public EOGrade getGradeForPeriode(EOIndividu individu, EOEditingContext edc, NSTimestamp debut, NSTimestamp fin) {
		EOGrade eoGrade = null;

		NSArray<EOElements> eoElementCarriereArray = EOElements.findSortedElementCarriereForIndividuAndDateInContext(
				edc, individu, debut, fin);

		if (eoElementCarriereArray.count() > 0) {
			EOElements eoElementCarriere = eoElementCarriereArray.lastObject();
			eoGrade = eoElementCarriere.toGrade();			
		} else {
			// rechercher parmi les contrats
			ContratAvenantService contratAvenantService = new ContratAvenantService();
			NSArray<EOContratAvenant> eoContratAvenantArray = contratAvenantService.findSortedContratForIndividuAndDateInContext(
					edc, individu, debut, fin);
			if (eoContratAvenantArray.count() > 0) {
				EOContratAvenant eoContratAvenant = eoContratAvenantArray.lastObject();
				eoGrade = eoContratAvenant.toGrade();
			}
		}

		return eoGrade;
	}

	/**
	 * Echelon de l'agent sur la période
	 * 
	 * @param debut
	 * @param fin
	 * @return
	 */	
	public String getEchelonForPeriode(EOIndividu individu, EOEditingContext edc, NSTimestamp debut, NSTimestamp fin) {
		Object echelon;
		NSArray<EOElements> eoElementCarriereArray = EOElements.findSortedElementCarriereEchelonForIndividuAndDateInContext(
				edc, individu, debut, fin);

		if (eoElementCarriereArray.count() > 0) {
			//echelon=((NSArray<EOElements>)(eoElementCarriereArray.valueForKeyPath(EOElements.C_ECHELON_KEY))).lastObject();
			EOElements eoElementCarriere = eoElementCarriereArray.lastObject();
			return eoElementCarriere.cEchelon();
		} else {
			// rechercher parmi les contrats
			ContratAvenantService contratAvenantService = new ContratAvenantService();
			NSArray<EOContratAvenant> eoContratAvenantArray = contratAvenantService.findSortedContratEchelonForIndividuAndDateInContext(edc, individu, debut, fin);
			if (eoContratAvenantArray.count() > 0) {
				//echelon=((NSArray<EOContratAvenant>)(eoContratAvenantArray.valueForKeyPath(EOContratAvenant.C_ECHELON_KEY))).lastObject();
				EOContratAvenant eoContratAvenant = eoContratAvenantArray.lastObject();
				return eoContratAvenant.cEchelon();
			}
		}

		return "";
	}

	public NSTimestamp getDateEchelonForPeriode(EOIndividu individu, EOEditingContext edc, NSTimestamp debut, NSTimestamp fin) {
		Object echelon;
		NSArray<EOElements> eoElementCarriereArray = EOElements.findSortedElementCarriereEchelonForIndividuAndDateInContext(
				edc, individu, debut, fin);

		if (eoElementCarriereArray.count() > 0) {
			//echelon=((NSArray<EOElements>)(eoElementCarriereArray.valueForKeyPath(EOElements.C_ECHELON_KEY))).lastObject();
			EOElements eoElementCarriere = eoElementCarriereArray.lastObject();
			return eoElementCarriere.dEffetElement();
		} else {
			// rechercher parmi les contrats
			ContratAvenantService contratAvenantService = new ContratAvenantService();
			NSArray<EOContratAvenant> eoContratAvenantArray = contratAvenantService.findSortedContratEchelonForIndividuAndDateInContext(edc, individu, debut, fin);
			if (eoContratAvenantArray.count() > 0) {
				//echelon=((NSArray<EOContratAvenant>)(eoContratAvenantArray.valueForKeyPath(EOContratAvenant.C_ECHELON_KEY))).lastObject();
				EOContratAvenant eoContratAvenant = eoContratAvenantArray.lastObject();
				return eoContratAvenant.dDebAvenant();
			}
		}

		return null;
	}
	
	/**
	 * Corps de l'agent sur la période
	 * 
	 * @param debut
	 * @param fin
	 * @return
	 */
	public EOCorps getCorpsForPeriode(EOIndividu individu, EOEditingContext edc, NSTimestamp debut, NSTimestamp fin) {
		EOCorps eoCorps = null;

		NSArray<EOElements> eoElementCarriereArray = EOElements.findSortedElementCarriereForIndividuAndDateInContext(
				edc, individu, debut, fin);

		if (eoElementCarriereArray.count() > 0) {
			EOElements eoElementCarriere = eoElementCarriereArray.lastObject();
			eoCorps = eoElementCarriere.toCorps();
		} else {
			// rechercher parmi les contrats
			ContratAvenantService contratAvenantService = new ContratAvenantService();
			NSArray<EOContratAvenant> eoContratAvenantArray = contratAvenantService.findSortedContratForIndividuAndDateInContext(
					edc, individu, debut, fin);
			if (eoContratAvenantArray.count() > 0) {
				EOContratAvenant eoContratAvenant = eoContratAvenantArray.lastObject();
				if (eoContratAvenant.toGrade() != null) {
					eoCorps = eoContratAvenant.toGrade().toCorps();
				}
			}
		}

		return eoCorps;
	}

	
	
	
	
	/**
	 * liste des fiches de poste actuellement occupees par cet individu
	 * 
	 * @return
	 */
	public NSArray tosFicheDePosteActuelle(EOIndividu individu, EOEditingContext edc) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				"fdpDDebut <= %@ AND (fdpDFin >= %@ OR fdpDFin = nil)",
				new NSArray(new NSTimestamp[] { DateCtrl.now(), DateCtrl.now() }));
		return EOQualifier.filteredArrayWithQualifier(tosFicheDePoste(individu, edc), qual);
	}

	/**
	 * liste des fiches LOLF actuellement occupees par cet individu
	 * 
	 * @return
	 */
	public NSArray tosFicheLolfActuelle(EOIndividu individu, EOEditingContext edc) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				"fdpDDebut <= %@ AND (fdpDFin >= %@ OR fdpDFin = nil)",
				new NSArray(new NSTimestamp[] { DateCtrl.now(), DateCtrl.now() }));
		return EOQualifier.filteredArrayWithQualifier(tosFicheLolf(individu, edc), qual);
	}

	/**
	 * liste des poste actuellement occupes par cet individu
	 * 
	 * @return
	 */
	public NSArray tosPosteActuel(EOIndividu individu, EOEditingContext edc) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				"posDDebut <= %@ AND (posDFin >= %@ OR posDFin = nil)",
				new NSArray(new NSTimestamp[] { DateCtrl.now(), DateCtrl.now() }));
		return EOQualifier.filteredArrayWithQualifier(tosPoste(individu, edc, null, null), qual);
	}

	/**
	 * La liste des structures des postes sans doublons
	 */
	public NSArray<EOStructure> tosPosteStructure(EOIndividu individu, EOEditingContext edc) {
		NSArray<EOPoste> tosPoste = tosPoste(individu, edc, null, null);
		return NSArrayCtrl.removeDuplicate((NSArray<EOStructure>) tosPoste.valueForKey(EOPoste.TO_STRUCTURE_KEY));
	}
	
	
	/**
	 * tous les fiches occupees pendant une periode
	 * 
	 * @param debut
	 * @param fin
	 * @return
	 */
	public NSArray tosFicheDePostePourDate(EOIndividu individu, EOEditingContext edc, NSTimestamp debut, NSTimestamp fin) {
		String strQual = "("
						+ "(" + EOFicheDePoste.FDP_D_DEBUT_KEY + " <= %@ AND (" + EOFicheDePoste.FDP_D_FIN_KEY + " >= %@ OR " + EOFicheDePoste.FDP_D_FIN_KEY + " = nil)) OR "
						+ "(" + EOFicheDePoste.FDP_D_DEBUT_KEY + " >= %@ AND " + EOFicheDePoste.FDP_D_DEBUT_KEY + " <= %@ AND " + EOFicheDePoste.FDP_D_FIN_KEY + " => %@ AND "
						+ EOFicheDePoste.FDP_D_FIN_KEY + " <= %@) OR " + "(" + EOFicheDePoste.FDP_D_DEBUT_KEY + " <= %@ and " + EOFicheDePoste.FDP_D_FIN_KEY + " <= %@ AND "
						+ EOFicheDePoste.FDP_D_FIN_KEY + " >= %@) OR " + "(" + EOFicheDePoste.FDP_D_DEBUT_KEY + " >= %@ AND "
						+ EOFicheDePoste.FDP_D_DEBUT_KEY + " <= %@ AND (" + EOFicheDePoste.FDP_D_FIN_KEY + " >= %@ OR " + EOFicheDePoste.FDP_D_FIN_KEY + " = nil)))";

		NSArray args = new NSArray(
				new NSTimestamp[] {
						debut, fin,
						debut, fin, debut, fin,
						debut, fin, debut,
						debut, fin, fin,
						debut, fin,
						debut, fin, debut, fin,
						debut, fin, debut,
						debut, fin, fin });
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(strQual, args);

		// on a les fiches en dates de validite
		NSArray recsFiche = EOQualifier.filteredArrayWithQualifier(tosFicheDePoste(individu, edc), qual);

		// on ne conserve maintenant que celle dont les AffectationDetail
		// correspondent aussi a ces dates
		strQual = "((" + EOAffectationDetail.D_DEBUT_AFFECTATION_DETAIL + " <= %@ AND (" + EOAffectationDetail.D_FIN_AFFECTATION_DETAIL + " >= %@ OR "
					+ EOAffectationDetail.D_FIN_AFFECTATION_DETAIL + " = nil)) OR " + "(" + EOAffectationDetail.D_DEBUT_AFFECTATION_DETAIL + " >= %@ AND "
					+ EOAffectationDetail.D_DEBUT_AFFECTATION_DETAIL + " <= %@ AND " + EOAffectationDetail.D_FIN_AFFECTATION_DETAIL + " => %@ AND "
					+ EOAffectationDetail.D_FIN_AFFECTATION_DETAIL + " <= %@) OR " + "(" + EOAffectationDetail.D_DEBUT_AFFECTATION_DETAIL + " <= %@ and "
					+ EOAffectationDetail.D_FIN_AFFECTATION_DETAIL + " <= %@ AND " + EOAffectationDetail.D_FIN_AFFECTATION_DETAIL + " >= %@) OR "
					+ "(" + EOAffectationDetail.D_DEBUT_AFFECTATION_DETAIL + " >= %@ AND " + EOAffectationDetail.D_DEBUT_AFFECTATION_DETAIL
					+ " <= %@ AND (" + EOAffectationDetail.D_FIN_AFFECTATION_DETAIL + " >= %@ OR " + EOAffectationDetail.D_FIN_AFFECTATION_DETAIL + " = nil)))";
		qual = EOQualifier.qualifierWithQualifierFormat(strQual, args);

		// on a les occupations en dates de validite
		NSArray recsAffDet = EOQualifier.filteredArrayWithQualifier(tosAffectationDetail(individu, edc, null, null), qual);

		NSArray recsFicheForDate = new NSArray();

		for (int i = 0; i < recsFiche.count(); i++) {
			EOFicheDePoste ficheDePoste = (EOFicheDePoste) recsFiche.objectAtIndex(i);
			for (int j = 0; j < recsAffDet.count(); j++) {
				EOAffectationDetail affDet = (EOAffectationDetail) recsAffDet.objectAtIndex(j);
				NSArray recsAffDetFicheDePoste = NSArrayCtrl.flattenArray((NSArray) affDet.valueForKeyPath(EOAffectationDetail.TO_POSTE_KEY + "." + EOPoste.TOS_FICHE_DE_POSTE_KEY));
				if (recsAffDetFicheDePoste.containsObject(ficheDePoste)) {
					recsFicheForDate = recsFicheForDate.arrayByAddingObject(ficheDePoste);
					break;
				}
			}
		}

		return recsFicheForDate;
	}

	/**
	 * l'evaluation en cours de l'individu
	 * 
	 * @return
	 */
	public EOEvaluation toEvaluationActuelle(EOIndividu individu, EOEditingContext edc) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				"evaDDebut <= %@ AND (evaDFin >= %@ OR evaDFin = nil)",
				new NSArray(new NSTimestamp[] { DateCtrl.now(), DateCtrl.now() }));
		NSArray records = EOQualifier.filteredArrayWithQualifier(tosEvaluation(individu, edc, null, null), qual);
		EOEvaluation record = null;
		if (records.count() > 0) {
			record = (EOEvaluation) records.lastObject();
		}
		return record;
	}

	/**
	 * La liste des fiches de postes "occupees" par l'agent. On ne conserve que
	 * les fiches dont les dates coincident avec l'affectation du poste et de
	 * l'agent.
	 */
	private NSArray<EOPoste> tosFiche(EOIndividu individu, EOEditingContext edc, String entity) {
		NSArray result = new NSArray();
		boolean isFicheDePoste = entity.equals(EOFicheDePoste.ENTITY_NAME);
		// construction du nom des attributes a fetcher
		String attrDeb = isFicheDePoste ? EOFicheDePoste.FDP_D_DEBUT_KEY : EOFicheLolf.FLO_D_DEBUT_KEY;
		String attrFin = isFicheDePoste ? EOFicheDePoste.FDP_D_FIN_KEY : EOFicheLolf.FLO_D_FIN_KEY;
		//
		NSArray occupations = tosAffectationDetail(individu, edc, null, null);
		for (int i = 0; i < occupations.count(); i++) {
			EOAffectationDetail occ = (EOAffectationDetail) occupations.objectAtIndex(i);
			// lors de la suppression d'un poste les toPoste de AffectationDetail
			// ne pas mis a jour ...
			if (occ.toPoste() != null) {
				NSArray fiches = isFicheDePoste ? occ.toPoste().tosFicheDePoste() : occ.toPoste().tosFicheLolf();
				EOQualifier qual = null;
				if (occ.dFin() == null) {
					qual = EOQualifier.qualifierWithQualifierFormat(
							attrFin + " >= %@ OR dFin = nil", new NSArray(occ.dDebut()));
				} else {
					qual = new EONotQualifier(EOQualifier.qualifierWithQualifierFormat(
							attrDeb + " > %@ OR " + attrFin + " < %@",
							new NSArray(new NSTimestamp[] { occ.dFin(), occ.dDebut() })));
				}
				result = result.arrayByAddingObjectsFromArray(
						EOQualifier.filteredArrayWithQualifier(fiches, qual));
			}
		}
		result = NSArrayCtrl.removeDuplicate(result);
		// classement par date de debut d'occupation
		result = CktlSort.sortedArray(result, UtilFiche.D_DEBUT);
		//
		return result;
	}

	/**
	 * La liste des fiches de postes "occupees" par l'agent. On ne conserve que
	 * les fiches dont les dates coincident avec l'affectation du poste et de
	 * l'agent.
	 */
	public NSArray tosFicheDePoste(EOIndividu individu, EOEditingContext edc) {
		return tosFiche(individu, edc, EOFicheDePoste.ENTITY_NAME);
	}

	/**
	 * La liste des fiches de LOLF "occupees" par l'agent. On ne conserve que les
	 * fiches dont les dates coincident avec l'affectation du poste et de l'agent.
	 */
	public NSArray<EOPoste> tosFicheLolf(EOIndividu individu, EOEditingContext edc) {
		return tosFiche(individu, edc, EOFicheLolf.ENTITY_NAME);
	}


	/**
	 * La liste des postes sans doublons
	 */
	public NSArray<EOPoste> tosPosteOLD(EOIndividu individu, EOEditingContext edc, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray<EOPoste> results;
		EOQualifier qual = new EOKeyValueQualifier(
				EOPoste.TO_STRUCTURE.dot(EOStructure.TO_REPART_STRUCTURES.dot(EORepartStructure.PERS_ID_KEY).key()).key(),
				EOQualifier.QualifierOperatorEqual,
				individu.persId());
		results = EOPoste.fetchAll(edc, qual, null);
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
		return NSArrayCtrl.removeDuplicate(results);
	}

	
	public NSArray<EOPoste> tosPoste(EOIndividu individu, EOEditingContext edc, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray<EOPoste> results= new NSMutableArray<EOPoste>();
		
		EOQualifier qual1 = EORepartStructure.PERS_ID.eq(individu.persId());
		NSArray<EORepartStructure> listeRepartStruct = EORepartStructure.fetchAll(edc,qual1,null);
		
		EOQualifier qual2;
		
		for (EORepartStructure repartStruct : listeRepartStruct) {
			qual2 = EOPoste.TO_STRUCTURE.dot(EOStructure.C_STRUCTURE_KEY).eq(repartStruct.cStructure());
			results.addAll(EOPoste.fetchAll(edc,qual2,null));						
		}
			
		// SELECT t0.C_LOGE, t0.C_STRUCTURE, t0.D_CREATION, t0.D_MODIFICATION, t0.POS_CODE, t0.POS_D_DEBUT, t0.POS_D_FIN, 
		// t0.POS_KEY, t0.POS_LIBELLE, t0.TEM_VALIDE 
		// FROM MANGUE.POSTE t0, GRHUM.REPART_STRUCTURE T2, GRHUM.STRUCTURE_ULR T1 
		// WHERE t0.C_STRUCTURE = T1.C_STRUCTURE 
		// AND T1.C_STRUCTURE = T2.C_STRUCTURE  
		// AND (t0.TEM_VALIDE = 'O' AND T2.PERS_ID = 1132)

		if (qualifier != null) {
			results = EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
		return NSArrayCtrl.removeDuplicate(results);
	}

	public NSArray<EOAffectationDetail> tosAffectationDetail(EOIndividu individu, EOEditingContext edc, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray<EOAffectationDetail> results;
	      EOQualifier qual = new EOKeyValueQualifier(
					EOAffectationDetail.TO_AFFECTATION.dot(EOAffectation.TO_INDIVIDU_KEY).key(),
					EOQualifier.QualifierOperatorEqual,
					individu);
			results = EOAffectationDetail.fetchAll(edc, qual, null);
	      if (qualifier != null) {
	        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	      }
	      if (sortOrderings != null) {
	        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	      }
	    return results;
	  }
	
	/**
	 * La liste des evaluations classées par dates
	 * 
	 * @return
	 */
	public NSArray<EOEvaluation> tosEvaluationSorted(EOIndividu individu, EOEditingContext edc) {
		NSArray<EOEvaluation> tosEvaluation = tosEvaluation(individu, edc, null, null);

		tosEvaluation = CktlSort.sortedArray(
				tosEvaluation, EOEvaluation.TO_EVALUATION_PERIODE_KEY + "." + EOEvaluationPeriode.EPE_D_DEBUT_KEY);

		// TODO faire du filtrage plus en amont ou bien pouvoir gérer sans plantage
		NSMutableArray<EOEvaluation> array = new NSMutableArray<EOEvaluation>();

		// ze mégabidouille de sagouin ...
		for (int i = 0; i < tosEvaluation.count(); i++) {
			EOEvaluation eoEvaluation = tosEvaluation.objectAtIndex(i);
			try {
				EOVCandidatEvaluation eoVCandidiatEvaluation = eoEvaluation.toVCandidatEvaluation();
				EOEvaluation eoEvaluation2 = eoVCandidiatEvaluation.toEvaluation();
				array.addObject(eoEvaluation);
			} catch (Exception e) {

			}
		}

		tosEvaluation = array.immutableClone();

		return tosEvaluation;
	}

	public NSArray<EOEvaluation> tosEvaluation(EOIndividu individu, EOEditingContext edc, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray<EOEvaluation> results;
	      EOQualifier fullQualifier;
	      EOQualifier qual = new EOKeyValueQualifier(EOEvaluation.TO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, individu);
	    	
	      if (qualifier == null) {
	        fullQualifier = qual;
	      } else {
	    	  fullQualifier = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
						qual, qualifier
				}));
	      }

	      results = EOEvaluation.fetchAll(edc, fullQualifier, sortOrderings);
	    
	    return results;
	  }
	


	public NSArray<EOVCandidatEvaluation> tosVCandidatEvaluation(EOIndividu individu, EOEditingContext edc, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray<EOVCandidatEvaluation> results;
		EOQualifier fullQualifier;
		EOQualifier qual = new EOKeyValueQualifier(EOVCandidatEvaluation.TO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, individu);

		if (qualifier == null) {
			fullQualifier = qual;
		} else {
			fullQualifier = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					qual, qualifier
			}));
		}

		results = EOVCandidatEvaluation.fetchAll(edc, fullQualifier, sortOrderings);
		return results;
	}

	
	/**
	 * Retourne l'evaluation de l'individu pour la periode donnee
	 * 
	 * @param periode
	 * @return
	 */
	public EOVCandidatEvaluation toVCandidatEvaluationForPeriode(EOIndividu individu, EOEditingContext edc, EOEvaluationPeriode periode) {
		EOVCandidatEvaluation result = null;
		NSArray<EOVCandidatEvaluation> records;
		if (periode != null) {
			records = tosVCandidatEvaluation(
						individu,
						edc,
						ERXQ.equals(EOVCandidatEvaluation.TO_EVALUATION_PERIODE_KEY, periode),
					null);
		} else {
			records = tosVCandidatEvaluation(
					individu,
					edc,
					ERXQ.notEquals(EOVCandidatEvaluation.TO_EVALUATION_PERIODE_KEY, null),
					null);
		}
		if (records.count() > 0) {
			result = records.objectAtIndex(0);
		}
		return result;
	}
}
