package org.cocktail.fwkcktlgrh.common.metier.services;


import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode;
import org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

public class VCandidatEvaluationService {

	/**
	 * Retourne l'evaluation de l'individu pour la periode donnee
	 * 
	 * @param periode
	 * @return
	 */
	public EOVCandidatEvaluation toVCandidatEvaluationForPeriode(EOIndividu individu, EOEditingContext edc, EOEvaluationPeriode periode) {
		EOVCandidatEvaluation result = null;
		NSArray<EOVCandidatEvaluation> records = tosVCandidatEvaluation(
				individu,
				edc,
				ERXQ.equals(EOVCandidatEvaluation.TO_EVALUATION_PERIODE_KEY, periode),
				null);
		if (records.count() > 0) {
			result = records.objectAtIndex(0);
		}
		return result;
	}

	public NSArray<EOVCandidatEvaluation> tosVCandidatEvaluation(EOIndividu individu, EOEditingContext edc, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray<EOVCandidatEvaluation> results;
		EOQualifier fullQualifier;
		EOQualifier qual = new EOKeyValueQualifier(EOVCandidatEvaluation.TO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, individu);

		if (qualifier == null) {
			fullQualifier = qual;
		} else {
			fullQualifier = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					qual, qualifier
			}));
		}
		results = EOVCandidatEvaluation.fetchAll(edc, fullQualifier, sortOrderings);

		return results;
	}
}
