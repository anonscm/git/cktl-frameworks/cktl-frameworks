package org.cocktail.fwkcktlgrh.common.metier.services;

import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.EOStage;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class StageService {

	/**
	 * liste des stage d'un individu pour une periode donnee
	 * 
	 * @param ec
	 * @param individu
	 * @param date
	 * @return
	 */
	public NSArray<EOStage> findStageForIndividuAndDateInContext(EOEditingContext ec, EOIndividu individu, NSTimestamp dateRef) {
		String strQual = null;
		NSArray args = null;/*
												 * if (dateFin != null) strQual =
												 * "toIndividu = %@ AND (" +
												 * "(dDebStage <= %@ AND dFinStage = nil) OR" +
												 * "(dDebStage >= %@ AND dFinStage <= %@) OR" +
												 * "(dDebStage <= %@ AND dFinStage >= %@) OR" +
												 * "(dDebStage <= %@ AND dFinStage >= %@ AND dFinStage <= %@) OR"
												 * +
												 * "(dDebStage >= %@ AND dDebStage <= %@ AND dFinStage >= %@) OR"
												 * +
												 * "(dDebStage >= %@ AND dDebStage <= %@ AND dFinStage = nil)"
												 * + ")"; args = new NSArray(new Object[]{ individu,
												 * dateDebut, dateDebut, dateFin, dateDebut, dateFin,
												 * dateDebut, dateDebut, dateFin, dateDebut, dateFin,
												 * dateFin, dateDebut, dateFin}); } else { strQual =
												 * "toIndividu = %@ AND (" + "(dFinStage = nil) OR" +
												 * "(dDebStage <= %@ AND dFinStage >= %@) OR" +
												 * "(dDebStage >= %@ AND dFinStage >= %@)" + ")"; args =
												 * new NSArray(new Object[]{ individu, dateDebut,
												 * dateDebut, dateDebut, dateDebut}); }
												 */
		strQual = "toIndividu = %@ AND dDebStage <= %@ AND (dFinStage >= %@ OR dFinStage = nil)";
		args = new NSArray(new Object[] { individu, dateRef, dateRef });
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(strQual, args);
		return GRHUtilities.fetchArray(ec, EOStage.ENTITY_NAME, qual, null);
	}

	
}
