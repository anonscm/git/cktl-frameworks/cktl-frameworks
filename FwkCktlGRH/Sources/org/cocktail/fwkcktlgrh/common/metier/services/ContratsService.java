package org.cocktail.fwkcktlgrh.common.metier.services;

import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

public class ContratsService {

	private EOEditingContext edc;

	private ContratsService(EOEditingContext edc) {
		this.edc = edc;
	}
	
	public EOEditingContext edc() {
		return edc;
	}

	public static ContratsService creerNouvelleInstance(EOEditingContext edc) {
		return new ContratsService(edc);
	}
	
	//Les vacations ne sont plus gérés depuis EOContrat voir IVacataires
	public NSArray<EOContrat> contratsPourIndividusEtDate(NSArray<EOIndividu> individus, NSTimestamp date) {
		
		NSArray<EOContrat> contrats;		
		
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.lessThanOrEqualTo(EOContrat.D_DEB_CONTRAT_TRAV_KEY, date),
						ERXQ.or(
								ERXQ.isNull(EOContrat.D_FIN_CONTRAT_TRAV_KEY),
								ERXQ.greaterThanOrEqualTo(EOContrat.D_FIN_CONTRAT_TRAV_KEY, date)
							),
						ERXQ.in(ERXQ.keyPath(EOContrat.TO_PERSONNEL_KEY, EOPersonnel.TO_INDIVIDU_KEY), individus),
						ERXQ.equals(EOContrat.TEM_ANNULATION_KEY, EOContrat.TEM_ANNULATION_NON)
				);
		
		NSMutableArray<String> prefetchingKeyPaths = new NSMutableArray<String>();
		prefetchingKeyPaths.add(ERXQ.keyPath(EOContrat.TO_CONTRAT_AVENANTS_KEY, EOContratAvenant.TO_GRADE_KEY));
		prefetchingKeyPaths.add(ERXQ.keyPath(EOContrat.TO_PERSONNEL_KEY, EOPersonnel.TO_INDIVIDU_KEY));
		prefetchingKeyPaths.add(EOContrat.TO_TYPE_CONTRAT_TRAVAIL_KEY);
		
		ERXFetchSpecification<EOContrat> fetchSpecification = new ERXFetchSpecification<EOContrat>(EOContrat.ENTITY_NAME, qualifier, null);
		fetchSpecification.setPrefetchingRelationshipKeyPaths(prefetchingKeyPaths);
		
		contrats = fetchSpecification.fetchObjects(edc());
		
		
		return contrats;
	}
	
	
}
