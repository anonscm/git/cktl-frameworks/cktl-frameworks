package org.cocktail.fwkcktlgrh.common.metier.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.finder.Finder;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns;
import org.cocktail.fwkcktlpersonne.common.metier.EOVService;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class StructureGrhService {

	public static final NSArray<Object> ARRAY_SORT = new NSArray<Object>(new Object[] {
			EOSortOrdering.sortOrderingWithKey(EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareAscending) });
	
	
	/**
	 * Trouver la racine de l'annuaire
	 * 
	 * @param ec
	 * @return
	 */
	public EOStructure findRacineInContext(EOEditingContext ec) {
		EOStructure racine = null;

		EOQualifier qual = CktlDataBus.newCondition(
				EOStructure.C_STRUCTURE_KEY + "=" + EOStructure.TO_STRUCTURE_PERE_KEY + "." + EOStructure.C_STRUCTURE_KEY + " and " + EOStructure.C_TYPE_STRUCTURE_KEY + "='E'");
		NSArray records = GRHUtilities.fetchArray(ec, EOStructure.ENTITY_NAME, qual, null);
		if (records.count() > 0) {
			racine = (EOStructure) records.lastObject();
		}
		return racine;
	}
	
	
	public EOStructure getStructureAffectationCourante(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
    	
		List<EOAffectation> affs = EOAffectation.fetchAll(edc, EOAffectation.getQualifierAffectationCourantePrincipale(individu, debutPeriode, finPeriode), null);
		if (CollectionUtils.isNotEmpty(affs)) {
			return affs.get(0).toStructure();
		} 
		return null;
		
		
    	
    }
	

	/**
	 * liste de tous les "sous-services" (recursivement) pour une structure donnee
	 * 
	 * @param ec
	 * @return
	 */
	public NSArray findSousServiceForStructureInContext(EOEditingContext ec, EOStructure structure) {
		NSArray datasDico = new NSArray();
		NSArray records = new NSArray();
		// cas particulier de la racine, le CONNECT BY PRIOR pete une erreur oracle
		// CONNECT BY NOCYCLE PRIOR ne marche qu'avec oracle 10g
		String requete =
				"SELECT UNIQUE PERS_ID "
						+ "FROM "+ EOVService.ENTITY_TABLE_NAME + " "
						+ "START WITH pers_id = %@ "
						+ "CONNECT BY PRIOR c_structure = TO_NUMBER(c_structure_pere)";
		if (structure == structure.toStructurePere()) {
			for (int i = 0; i < structure.tosStructuresFilles().count(); i++) {
				EOStructure uneFille = (EOStructure) structure.tosStructuresFilles().objectAtIndex(i);
				if (uneFille != structure) {
					String uneRequete = StringCtrl.replace(requete, "%@", uneFille.persId().toString());
					datasDico = datasDico.arrayByAddingObjectsFromArray(Finder.rawRowsForSQL(ec, "FwkCktlPersonne", uneRequete));
				}
			}
			// on oublie pas de la compter si elle est elle-meme un service
			if (structure.isService()) {
				NSMutableDictionary dicoRacine = new NSMutableDictionary();
				dicoRacine.setObjectForKey(structure.persId(), "PERS_ID");
				datasDico = datasDico.arrayByAddingObject(dicoRacine.immutableClone());
			}
		} else {
			requete = StringCtrl.replace(requete, "%@", structure.persId().toString());
			datasDico = datasDico.arrayByAddingObjectsFromArray(Finder.rawRowsForSQL(ec, "FwkCktlPersonne", requete));
		}

		for (int i = 0; i < datasDico.count(); i++) {
			NSDictionary dico = (NSDictionary) datasDico.objectAtIndex(i);
			Number persId = (Number) dico.valueForKey("PERS_ID");
			if (!((Integer) persId.intValue()).equals(structure.persId())) {
				records = records.arrayByAddingObject(findStructureForPersIdInContext(ec, persId));
			}
		}
		NSArray sortOrdering = new NSArray(ARRAY_SORT);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(records, sortOrdering);
	}
	
	/**
	 * trouver une structure par son persId
	 * 
	 * @param ec
	 * @param persId
	 * @return
	 */
	public EOStructure findStructureForPersIdInContext(EOEditingContext ec, Number persId) {
		EOStructure structure = null;
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(persId));
		NSArray records = GRHUtilities.fetchArray(ec, EOStructure.ENTITY_NAME, qual, null);
		if (records.count() > 0) {
			structure = (EOStructure) records.lastObject();
		}
		return structure;
	}

	/**
	 * trouver la composante dont dépend un service
	 */
	public NSArray findServicesForComposante(EOStructure composante) {
		NSArray records = GRHUtilities.fetchArray(composante.editingContext(), EOVService.ENTITY_NAME, null, null);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("toComposante = %@", new NSArray(composante));
		return (NSArray) EOQualifier.filteredArrayWithQualifier(records, qual).valueForKey("toStructure");
	}


	

	/**
	 * Liste des agents non enseignants actuels affectés à la structure
	 * 
	 * @return
	 */
	public NSArray<EOIndividu> getIndividuAffecteVPersonnelNonEns(EOEditingContext edc, EOStructure structure) {
		NSArray<EOIndividu> result = null;

		NSTimestamp now = DateCtrl.now();

		String toVPersonnelActuelKey = EOAffectation.TO_INDIVIDU_KEY + "." + EOIndividu.TOS_V_PERSONNEL_NON_ENS_KEY;

		EOQualifier qual = CktlDataBus.newCondition(
					EOAffectation.TO_STRUCTURE_KEY + "=%@ and " + EOAffectation.D_DEB_AFFECTATION_KEY + "<=%@ and ("
							+ EOAffectation.D_FIN_AFFECTATION_KEY + " >= %@ or " + EOAffectation.D_FIN_AFFECTATION_KEY + "=nil) and "
							+ toVPersonnelActuelKey + "." + EOVPersonnelNonEns.TO_INDIVIDU_KEY + "." + EOIndividu.PERS_ID_KEY + "<> nil and "
							+ toVPersonnelActuelKey + "." + EOVPersonnelNonEns.D_DEBUT_KEY + "<=%@ and ("
							+ toVPersonnelActuelKey + "." + EOVPersonnelNonEns.D_FIN_KEY + " >= %@ or " + toVPersonnelActuelKey + "." + EOVPersonnelNonEns.D_FIN_KEY + "=nil)",
					new NSArray<Object>(new Object[] {
							structure, now, now, now, now }));

		result = (NSArray<EOIndividu>) EOAffectation.fetchAll(edc, qual, null).valueForKey(EOAffectation.TO_INDIVIDU_KEY);

		return result;
	}

	/**
	 * Obtenir le qualifier permettant de faire un filtrage sur le libelle
	 * 
	 * @param filtreComposante
	 * @return
	 */
	public EOQualifier getFiltreQualifier(String filtreComposante) {
		EOQualifier qual = null;

		qual = CktlDataBus.newCondition(
					EOStructure.LC_STRUCTURE_KEY + " caseInsensitiveLike '*" + filtreComposante + "*' or "
					+ EOStructure.LL_STRUCTURE_KEY + " caseInsensitiveLike '*" + filtreComposante + "*'");

		return qual;
	}
	
	
	public NSArray<EOIndividu> tosIndividuAffecte(EOStructure structure, EOEditingContext edc) {
		NSMutableArray<EOIndividu> results = new NSMutableArray<EOIndividu>();
		
		//EOAffectation eoAffectation =  new EOAffectation();
		//results = eoAffectation.getIndividuAffecteVPersonnelNonEns(edc, structure);
		
		
		NSArray<EORepartStructure> reparts = EORepartStructure.getRepartStructureWithIndividusValidesMembresDe(edc, structure);
		if (reparts != null) {
			for (int i = 0; i < reparts.count(); i++) {
				if ( (reparts.get(i)!=null) && (reparts.get(i).toIndividuElts() != null)) {
					results.addAll(reparts.get(i).toIndividuElts());
				}
			}
		}
		return results;
	}
	
	/**
	 * Récupère la structure de plus haut niveau dans une liste
	 * @param listeStructures
	 * @return
	 */
	public List<EOStructure> recupererStructuresPere(List<EOStructure> listeStructures) {
		List<EOStructure> listeStructuresPere = new ArrayList<EOStructure>();
		
		for (EOStructure eoStructure : listeStructures) {
			if (!hasStructurePereDansListe(eoStructure, listeStructures)) {
				listeStructuresPere.add(eoStructure);
			}
		}
		
		
		return listeStructuresPere;
		
	}


	private boolean hasStructurePereDansListe(EOStructure eoStructure, List<EOStructure> listeStructures) {
		while (!eoStructure.toStructurePere().equals(eoStructure)) {
			eoStructure = eoStructure.toStructurePere();
			if (listeStructures.contains(eoStructure)) {
				return true;
			}
		}
		return false;		
	}
	
	
}
