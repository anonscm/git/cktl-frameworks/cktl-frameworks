package org.cocktail.fwkcktlgrh.common.metier.services;

import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class ContratAvenantService {

	/**
	 * liste des elements de carriere d'un individu pour une periode donnee
	 * 
	 * @param ec
	 * @param individu
	 * @param date
	 * @return
	 */
	public NSArray<EOContratAvenant> findSortedContratForIndividuAndDateInContext(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		String strQual = EOContratAvenant.TO_CONTRAT_KEY + "." + EOContrat.TEM_ANNULATION_KEY + "='N' and ";
		NSArray args = null;
		if (dateFin != null) {
			strQual +=
					"toContrat.toIndividu = %@ AND ("
							+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " = nil) OR"
							+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " <= %@) OR"
							+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@) OR"
							+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " <= %@) OR"
							+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@) OR"
							+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " = nil)"
							+ ")";
			args = new NSArray(new Object[] {
						individu,
						dateDebut,
						dateDebut, dateFin,
						dateDebut, dateFin,
						dateDebut, dateDebut, dateFin,
						dateDebut, dateFin, dateFin,
						dateDebut, dateFin });
		} else {
			strQual +=
						"toContrat.toIndividu = %@ AND ("
								+ "("+ EOContratAvenant.D_FIN_AVENANT_KEY + " = nil) OR"
								+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@) OR"
								+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@)"
								+ ")";
			args = new NSArray(new Object[] { individu, dateDebut, dateDebut, dateDebut, dateDebut });
		}
		NSArray sortOrderings = new NSArray(EOSortOrdering.sortOrderingWithKey(EOContratAvenant.D_FIN_AVENANT_KEY, EOSortOrdering.CompareAscending));
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(strQual, args);
		return GRHUtilities.fetchArray(ec, EOContratAvenant.ENTITY_NAME, qual, sortOrderings);
	}

	public NSArray<EOContratAvenant> findSortedContratEchelonForIndividuAndDateInContext(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		String strQual = EOContratAvenant.TO_CONTRAT_KEY + "." + EOContrat.TEM_ANNULATION_KEY + "='N' and ";
		NSArray args = null;
		if (dateFin != null) {
			strQual +=
					"toContrat.toIndividu = %@ AND "+ EOContratAvenant.C_ECHELON_KEY+" <> nil AND ("
							+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " = nil) OR"
							+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " <= %@) OR"
							+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@) OR"
							+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " <= %@) OR"
							+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@) OR"
							+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " = nil)"
							+ ")";
			args = new NSArray(new Object[] {
						individu,
						dateDebut,
						dateDebut, dateFin,
						dateDebut, dateFin,
						dateDebut, dateDebut, dateFin,
						dateDebut, dateFin, dateFin,
						dateDebut, dateFin });
		} else {
			strQual +=
						"toContrat.toIndividu = %@ AND ("
								+ "("+ EOContratAvenant.D_FIN_AVENANT_KEY + " = nil) OR"
								+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " <= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@) OR"
								+ "("+ EOContratAvenant.D_DEB_AVENANT_KEY + " >= %@ AND "+ EOContratAvenant.D_FIN_AVENANT_KEY + " >= %@)"
								+ ")";
			args = new NSArray(new Object[] { individu, dateDebut, dateDebut, dateDebut, dateDebut });
		}
		NSArray sortOrderings = new NSArray(EOSortOrdering.sortOrderingWithKey(EOContratAvenant.D_FIN_AVENANT_KEY, EOSortOrdering.CompareAscending));
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(strQual, args);
		return GRHUtilities.fetchArray(ec, EOContratAvenant.ENTITY_NAME, qual, sortOrderings);
	}
	
}
