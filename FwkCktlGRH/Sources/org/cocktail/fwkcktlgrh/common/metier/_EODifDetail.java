/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EODifDetail.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EODifDetail extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_DifDetail";

	// Attributes
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DIF_DETAIL_DEBIT_KEY = "difDetailDebit";
	public static final String DIFDETAIL_LIBELLE_KEY = "difdetailLibelle";
	public static final String D_MODIFICATION_KEY = "dModification";

	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<Integer> DIF_DETAIL_DEBIT = new ERXKey<Integer>("difDetailDebit");
	public static final ERXKey<String> DIFDETAIL_LIBELLE = new ERXKey<String>("difdetailLibelle");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	// Relationships
	public static final String DIF_KEY = "dif";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EODif> DIF = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EODif>("dif");

  private static Logger LOG = Logger.getLogger(_EODifDetail.class);

  public EODifDetail localInstanceIn(EOEditingContext editingContext) {
    EODifDetail localInstance = (EODifDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EODifDetail.LOG.isDebugEnabled()) {
    	_EODifDetail.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EODifDetail.LOG.isDebugEnabled()) {
    	_EODifDetail.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODifDetail.LOG.isDebugEnabled()) {
    	_EODifDetail.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public Integer difDetailDebit() {
    return (Integer) storedValueForKey("difDetailDebit");
  }

  public void setDifDetailDebit(Integer value) {
    if (_EODifDetail.LOG.isDebugEnabled()) {
    	_EODifDetail.LOG.debug( "updating difDetailDebit from " + difDetailDebit() + " to " + value);
    }
    takeStoredValueForKey(value, "difDetailDebit");
  }

  public String difdetailLibelle() {
    return (String) storedValueForKey("difdetailLibelle");
  }

  public void setDifdetailLibelle(String value) {
    if (_EODifDetail.LOG.isDebugEnabled()) {
    	_EODifDetail.LOG.debug( "updating difdetailLibelle from " + difdetailLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "difdetailLibelle");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODifDetail.LOG.isDebugEnabled()) {
    	_EODifDetail.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EODif dif() {
    return (org.cocktail.fwkcktlgrh.common.metier.EODif)storedValueForKey("dif");
  }

  public void setDifRelationship(org.cocktail.fwkcktlgrh.common.metier.EODif value) {
    if (_EODifDetail.LOG.isDebugEnabled()) {
      _EODifDetail.LOG.debug("updating dif from " + dif() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EODif oldValue = dif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "dif");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "dif");
    }
  }
  

  public static EODifDetail create(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlgrh.common.metier.EODif dif) {
    EODifDetail eo = (EODifDetail) EOUtilities.createAndInsertInstance(editingContext, _EODifDetail.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setDifRelationship(dif);
    return eo;
  }

  public static NSArray<EODifDetail> fetchAll(EOEditingContext editingContext) {
    return _EODifDetail.fetchAll(editingContext, null);
  }

  public static NSArray<EODifDetail> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODifDetail.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EODifDetail> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODifDetail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODifDetail> eoObjects = (NSArray<EODifDetail>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODifDetail fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EODifDetail.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODifDetail fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODifDetail> eoObjects = _EODifDetail.fetch(editingContext, qualifier, null);
    EODifDetail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODifDetail)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_DifDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODifDetail fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EODifDetail.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EODifDetail fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EODifDetail eoObject = _EODifDetail.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_DifDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EODifDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODifDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODifDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODifDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EODifDetail localInstanceIn(EOEditingContext editingContext, EODifDetail eo) {
    EODifDetail localInstance = (eo == null) ? null : (EODifDetail)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
