/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartLolfSilland.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartLolfSilland extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_RepartLolfSilland";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String LOLF_ID_KEY = "lolfId";
	public static final String SIL_KEY_KEY = "silKey";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> EXE_ORDRE = new ERXKey<Integer>("exeOrdre");
	public static final ERXKey<Integer> LOLF_ID = new ERXKey<Integer>("lolfId");
	public static final ERXKey<Integer> SIL_KEY = new ERXKey<Integer>("silKey");
	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_FCT_SILLAND_KEY = "toFctSilland";
	public static final String TO_LOLF_NOMENCLATURE_DEPENSE_KEY = "toLolfNomenclatureDepense";

	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland> TO_FCT_SILLAND = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland>("toFctSilland");
	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense> TO_LOLF_NOMENCLATURE_DEPENSE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense>("toLolfNomenclatureDepense");

  private static Logger LOG = Logger.getLogger(_EORepartLolfSilland.class);

  public EORepartLolfSilland localInstanceIn(EOEditingContext editingContext) {
    EORepartLolfSilland localInstance = (EORepartLolfSilland)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartLolfSilland.LOG.isDebugEnabled()) {
    	_EORepartLolfSilland.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartLolfSilland.LOG.isDebugEnabled()) {
    	_EORepartLolfSilland.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey("exeOrdre");
  }

  public void setExeOrdre(Integer value) {
    if (_EORepartLolfSilland.LOG.isDebugEnabled()) {
    	_EORepartLolfSilland.LOG.debug( "updating exeOrdre from " + exeOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "exeOrdre");
  }

  public Integer lolfId() {
    return (Integer) storedValueForKey("lolfId");
  }

  public void setLolfId(Integer value) {
    if (_EORepartLolfSilland.LOG.isDebugEnabled()) {
    	_EORepartLolfSilland.LOG.debug( "updating lolfId from " + lolfId() + " to " + value);
    }
    takeStoredValueForKey(value, "lolfId");
  }

  public Integer silKey() {
    return (Integer) storedValueForKey("silKey");
  }

  public void setSilKey(Integer value) {
    if (_EORepartLolfSilland.LOG.isDebugEnabled()) {
    	_EORepartLolfSilland.LOG.debug( "updating silKey from " + silKey() + " to " + value);
    }
    takeStoredValueForKey(value, "silKey");
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey("toExercice");
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (_EORepartLolfSilland.LOG.isDebugEnabled()) {
      _EORepartLolfSilland.LOG.debug("updating toExercice from " + toExercice() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toExercice");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toExercice");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland toFctSilland() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland)storedValueForKey("toFctSilland");
  }

  public void setToFctSillandRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland value) {
    if (_EORepartLolfSilland.LOG.isDebugEnabled()) {
      _EORepartLolfSilland.LOG.debug("updating toFctSilland from " + toFctSilland() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland oldValue = toFctSilland();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFctSilland");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFctSilland");
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense toLolfNomenclatureDepense() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense)storedValueForKey("toLolfNomenclatureDepense");
  }

  public void setToLolfNomenclatureDepenseRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense value) {
    if (_EORepartLolfSilland.LOG.isDebugEnabled()) {
      _EORepartLolfSilland.LOG.debug("updating toLolfNomenclatureDepense from " + toLolfNomenclatureDepense() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense oldValue = toLolfNomenclatureDepense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toLolfNomenclatureDepense");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toLolfNomenclatureDepense");
    }
  }
  

  public static EORepartLolfSilland create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer exeOrdre
, Integer lolfId
, Integer silKey
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice, org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland toFctSilland, org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense toLolfNomenclatureDepense) {
    EORepartLolfSilland eo = (EORepartLolfSilland) EOUtilities.createAndInsertInstance(editingContext, _EORepartLolfSilland.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setExeOrdre(exeOrdre);
		eo.setLolfId(lolfId);
		eo.setSilKey(silKey);
    eo.setToExerciceRelationship(toExercice);
    eo.setToFctSillandRelationship(toFctSilland);
    eo.setToLolfNomenclatureDepenseRelationship(toLolfNomenclatureDepense);
    return eo;
  }

  public static NSArray<EORepartLolfSilland> fetchAll(EOEditingContext editingContext) {
    return _EORepartLolfSilland.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartLolfSilland> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartLolfSilland.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartLolfSilland> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartLolfSilland.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartLolfSilland> eoObjects = (NSArray<EORepartLolfSilland>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartLolfSilland fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartLolfSilland.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartLolfSilland fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartLolfSilland> eoObjects = _EORepartLolfSilland.fetch(editingContext, qualifier, null);
    EORepartLolfSilland eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartLolfSilland)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartLolfSilland that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartLolfSilland fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartLolfSilland.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartLolfSilland fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartLolfSilland eoObject = _EORepartLolfSilland.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartLolfSilland that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartLolfSilland fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartLolfSilland fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartLolfSilland eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartLolfSilland)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartLolfSilland localInstanceIn(EOEditingContext editingContext, EORepartLolfSilland eo) {
    EORepartLolfSilland localInstance = (eo == null) ? null : (EORepartLolfSilland)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
