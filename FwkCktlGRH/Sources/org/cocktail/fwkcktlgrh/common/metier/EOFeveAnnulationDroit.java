/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_Accreditation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOFeveAnnulationDroit extends _EOFeveAnnulationDroit implements I_Accreditation{
	private static Logger log = Logger.getLogger(EOFeveAnnulationDroit.class);


	public final static String TO_PERSONNE_TITULAIRE_KEY = "toPersonneTitulaire";
	public final static String TO_PERSONNE_CIBLE_KEY = "toPersonneCible";

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}
	
	public static EOQualifier getQualifierTitulaireCibleNiveauDroit(IPersonne personneTitulaire, IPersonne personneCible, EOFeveTypeNiveauDroit typeNiveauDroit) {
		
		return ERXQ.and(
					ERXQ.equals(EOFeveAnnulationDroit.TO_PERSONNE_TITULAIRE_KEY + "." + IPersonne.PERSID_KEY, personneTitulaire.persId()),
					ERXQ.equals(EOFeveAnnulationDroit.TO_PERSONNE_CIBLE_KEY + "." + IPersonne.PERSID_KEY, personneCible.persId()),
					ERXQ.equals(EOFeveAnnulationDroit.TO_TYPE_NIVEAU_DROIT_KEY + "." + EOFeveTypeNiveauDroit.TND_CODE_KEY, typeNiveauDroit.tndCode())
				);
		
	}
	
	public static EOQualifier getQualifierNiveauDroit(EOFeveTypeNiveauDroit typeNiveauDroit) {
		return ERXQ.equals(EOFeveAnnulationDroit.TO_TYPE_NIVEAU_DROIT_KEY + "." + EOFeveTypeNiveauDroit.TND_CODE_KEY, typeNiveauDroit.tndCode());
	}
	
	public static EOQualifier getQualifierPersonneCible(EOStructure structure) {
		return ERXQ.equals(EOFeveAnnulationDroit.TO_PERSONNE_CIBLE_KEY + "." + IPersonne.PERSID_KEY, structure.persId());
	}
	
	public static EOQualifier getQualifierPersonneTitulaire(Integer persIdTitulaire) {
		return ERXQ.equals(EOFeveAnnulationDroit.TO_PERSONNE_TITULAIRE_KEY + "." + IPersonne.PERSID_KEY, persIdTitulaire);
	}
	
	
	
	/**
	 * @return
	 */
	public IPersonne toPersonneTitulaire() {
		IPersonne titulaire = null;

		if (tosIndividuTitulaire().count() > 0) {
			titulaire = tosIndividuTitulaire().objectAtIndex(0);
		} else if (tosStructureTitulaire().count() > 0) {
			titulaire = tosStructureTitulaire().objectAtIndex(0);
		}

		return titulaire;
	}

	/**
	 * @return
	 */
	public IPersonne toPersonneCible() {
		IPersonne cible = null;

		if (tosIndividuCible().count() > 0) {
			cible = tosIndividuCible().objectAtIndex(0);
		} else if (tosStructureCible().count() > 0) {
			cible = tosStructureCible().objectAtIndex(0);
		}

		return cible;
	}
	
	/**
	 * 
	 * @param editingContext
	 * @param personneCible
	 * @param personneTitulaire
	 * @param typeNiveauDroit
	 * @return
	 */
	public static EOFeveAnnulationDroit create(EOEditingContext editingContext, IPersonne personneCible, IPersonne personneTitulaire, EOFeveTypeNiveauDroit typeNiveauDroit){
		
		NSTimestamp now = DateCtrl.now();
		
		return create(editingContext, now, now, personneCible.persId(), personneTitulaire.persId(), typeNiveauDroit);
		
	}
	
	public static boolean existsAnnulationDroitDansListe(final I_Accreditation accreditation, List<EOFeveAnnulationDroit> listeAnnulationDroit) {
		Predicate pred = buildPredicateTousChamps(accreditation);
		return CollectionUtils.exists(listeAnnulationDroit, pred);
	}
	
	public static boolean existsAnnulationDroitDansListe(final Integer persIdTitulaire, final IPersonne personneCible, final EOFeveTypeNiveauDroit typeNiveauDroit, List<EOFeveAnnulationDroit> listeAnnulationDroit) {
		Predicate pred = buildPredicateTousChamps(persIdTitulaire, personneCible, typeNiveauDroit);
		return CollectionUtils.exists(listeAnnulationDroit, pred);
	}
	
	public static EOFeveAnnulationDroit getAnnulationDroitDansListe(final I_Accreditation accreditation, List<EOFeveAnnulationDroit> listeAnnulationDroit) {
		Predicate pred = buildPredicateTousChamps(accreditation);
		return (EOFeveAnnulationDroit) CollectionUtils.find(listeAnnulationDroit, pred);
	}

	private static Predicate buildPredicateTousChamps(
			final I_Accreditation accreditation) {
		Predicate pred = new Predicate() {
			public boolean evaluate(Object object) {
				return accreditation.toPersonneTitulaire().equals(((I_Accreditation) object).toPersonneTitulaire())
						&& accreditation.toPersonneCible().equals(((I_Accreditation) object).toPersonneCible())
						&& accreditation.toTypeNiveauDroit().equals(((I_Accreditation) object).toTypeNiveauDroit());
			}
		};
		return pred;
	}
	
	
	private static Predicate buildPredicateTousChamps(
			final Integer persIdTitulaire, final IPersonne personneCible, final EOFeveTypeNiveauDroit typeNiveauDroit) {
		Predicate pred = new Predicate() {
			public boolean evaluate(Object object) {
				return persIdTitulaire.equals(((I_Accreditation) object).toPersonneTitulaire().persId())
						&& personneCible.equals(((I_Accreditation) object).toPersonneCible())
						&& typeNiveauDroit.equals(((I_Accreditation) object).toTypeNiveauDroit());
			}
		};
		return pred;
	}
	

	public boolean isAnnuaire() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isCibleChefDeService() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isCibleHeritage() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isCibleServiceSimple() {
		// TODO Auto-generated method stub
		return false;
	}

	public String libelleTypeNiveauDroit() {
		// TODO Auto-generated method stub
		return null;
	}

	
	/**
	 * @return le globalID de l'objet à partir de l'editingContext associ̩. Null
	 *         si pas d'editingContext.
	 */
	public EOGlobalID globalID() {
		if (editingContext() == null) {
			return null;
		}
		return globalID(this.editingContext());
	}

	/**
	 * @param ec
	 * @return le globalID de l'objet à partir d'ec.
	 */
	public EOGlobalID globalID(EOEditingContext ec) {
		return ec.globalIDForObject(this);
	}
	
	@Override
	public String toString() {
		String toString = "";

		toString += toPersonneTitulaire().getNomPrenomAffichage();
		toString += " ";
		toString += toTypeNiveauDroit().tndLibelle();

		if (toPersonneCible() != null) {
			toString += " sur ";
			toString += toPersonneCible().getNomPrenomAffichage();
		}

		return toString;
	}
	
	

}
