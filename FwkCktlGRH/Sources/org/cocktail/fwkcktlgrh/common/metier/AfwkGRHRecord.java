/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXGenericRecord;


/**
 * Classe abstraite pour les entités métier du framework FwkCktlGrh.
 * cette classe permet de gérer automatiquement les dates de creation et
 * modification des enregistrements. La liste des entités concernées est donné
 * par le tableau {@link #ENTITIES_DATE_SENSITIVE}
 * 
 * @author ctarade, Laurent PRINGOT
 * 
 */

public abstract class AfwkGRHRecord extends ERXGenericRecord {

	public static final String OUI = "O";
	public static final String NON = "N";

	public static final String IS_MARKED_TO_DELETE_KEY = "isMarkedToDelete";

	private boolean isMarkedToDelete = false;

	public final synchronized boolean getIsMarkedToDelete() {
		return isMarkedToDelete;
	}

	public final synchronized void setIsMarkedToDelete(boolean markedToDelete) {
		isMarkedToDelete = markedToDelete;
	}
	
	private static final NSArray<String> ENTITIES_DATE_SENSITIVE = new NSArray<String>(new String[] {
			// modele mangue
			EOAffectationDetail.ENTITY_NAME,
			EOEnqueteFormation.ENTITY_NAME,
			EOFeveDroit.ENTITY_NAME,
			EOEvaluation.ENTITY_NAME,
			EOEvaluationPeriode.ENTITY_NAME,
			EOFicheDePoste.ENTITY_NAME,
			EOFicheLolf.ENTITY_NAME,
			EOIndividuFormations.ENTITY_NAME,
			EOObjectif.ENTITY_NAME,
			EOPoste.ENTITY_NAME,
			EOFevRepartEnqComp.ENTITY_NAME,
			EORepartEvaNouvelleComp.ENTITY_NAME,
			EORepartFdpActi.ENTITY_NAME,
			EORepartFdpAutre.ENTITY_NAME,
			EORepartFdpComp.ENTITY_NAME,
			EORepartFdpActi.ENTITY_NAME,
			EORepartFicheBlocActivation.ENTITY_NAME,
			EORepartFicheItem.ENTITY_NAME,
			EORepartFloLolfNomen.ENTITY_NAME,
			EORepartFloSilland.ENTITY_NAME,
			EORepartFormationSouhaitee.ENTITY_NAME,
			EORepartNiveauComp.ENTITY_NAME,
			EOSituActivite.ENTITY_NAME,
			EOStructureInfo.ENTITY_NAME,
			EOTplBloc.ENTITY_NAME,
			EOTplBlocNature.ENTITY_NAME,
			EOTplFiche.ENTITY_NAME,
			EOTplItem.ENTITY_NAME,
			EOTplItemNature.ENTITY_NAME,
			EOTplItemValeur.ENTITY_NAME,
			EOTplOnglet.ENTITY_NAME,
			EOTplRepartItemItemValeur.ENTITY_NAME,
			EOFeveTypeNiveauDroit.ENTITY_NAME,
			// modele grhum
			EORepartStructure.ENTITY_NAME,
			EOFormationPersonnel.ENTITY_NAME,
			EORepartCompetenceMiniCV.ENTITY_NAME,
			EORepartLolfSilland.ENTITY_NAME });

	private Boolean _shouldProcessDate;

	private boolean shouldProcessDate() {
		if (_shouldProcessDate == null) {
			_shouldProcessDate = new Boolean(ENTITIES_DATE_SENSITIVE.containsObject(entityName()));
		}
		return _shouldProcessDate;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		if (shouldProcessDate()) {
			setDCreation(DateCtrl.now());
			setDModification(DateCtrl.now());
		}
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		if (shouldProcessDate()) {

			boolean shouldChangeDModification = false;
			// on met a jour la date uniquement si des attributs ou des to one ont
			// changé
			NSDictionary dico = changesFromSnapshot(editingContext().committedSnapshotForObject(this));
			NSArray keys = dico.allKeys();
			NSArray attributeKeys = attributeKeys();
			NSArray toOneRelationshipKeys = toOneRelationshipKeys();
			for (int i = 0; i < keys.count() && !shouldChangeDModification; i++) {
				String key = (String) keys.objectAtIndex(i);
				// l'attribut dModification seul n'est pas un critere de mise à jour
				if (attributeKeys.containsObject(key) && !key.equals(D_MODIFICATION)) {
					shouldChangeDModification = true;
				} else if (toOneRelationshipKeys.contains(key)) {
					shouldChangeDModification = true;
				}
			}
			if (shouldChangeDModification) {
				setDModification(DateCtrl.now());
			}
		}
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		super.validateForSave();
	}

	private final static String D_CREATION = "dCreation";

	private void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, D_CREATION);
	}

	private final static String D_MODIFICATION = "dModification";

	private void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, D_MODIFICATION);
	}


	/**
	 * Donne le qualifier attendu pour obtenir des enregistrements inclus dans une
	 * période
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public EOQualifier getValiditeQualifier(
			String dDebValKey, String dFinValKey, NSTimestamp dateDebut, NSTimestamp dateFin) {
		EOQualifier qual = CktlDataBus.newCondition(
				"(" + dDebValKey + "=nil and " + dFinValKey + "=nil) or " +
						"(" + dFinValKey + ">=%@ and (" + dDebValKey + "<=%@ or " + dDebValKey + "=nil )) or " +
						"(" + dDebValKey + "<=%@ and (" + dFinValKey + ">=%@ or " + dFinValKey + "=nil )) or " +
						"(" + dDebValKey + ">=%@ and " + dFinValKey + "<=%@ )",
						new NSArray<NSTimestamp>(new NSTimestamp[] {
								dateDebut, dateDebut,
								dateFin, dateFin,
								dateDebut, dateFin }));
		return qual;
	}

	/**
	 * Test si la longueur de la valeur saisie ne depasse pas la valeur maximale
	 * autorisee. Si dépassement, alors la méthode le nombre d'octets en trop, 0
	 * si ok.
	 */
	public int depassement(
			String attributeName,
			String attributeValue)
					throws NSValidation.ValidationException {

		int depassement = 0;

		int maxLen = maxLengthForAttribute(entityName(),attributeName);
		int realLen = (attributeValue == null) ? 0 : attributeValue.length();

		if (realLen > maxLen) {
			depassement = realLen - maxLen;
		}

		return depassement;
	}

	/**
	 * Retourne la longueur maximal en octets de la valeur autorisee pour
	 * l'attribut <code>attributeName</code> de la table <code>tableName</code>.
	 * 
	 * <p>Retourne -1 si la table ou l'attribut n'existe pas ou si l'attribut est 
	 * de type numerique ou date.</p>
	 */
	public static int maxLengthForAttribute(String tableName, String attributeName) {
		EOEntity entity = EOModelGroup.defaultGroup().entityNamed(tableName);
		if (entity != null) {
			EOAttribute attribute = entity.attributeNamed(attributeName);
			if ((attribute != null) && (attribute.width() > 0))
				return attribute.width(); 
		}
		return -1;
	}

	  /**
	   * Retourne la longueur maximal en octets de la valeur autorisee pour
	   * l'attribut <code>attributeName</code> de cet enregistrement.
	   * 
	   * <p>Retourne -1 si l'attribut n'existe pas dans cet enregistrement ou
	   * s'il est de type numerique ou date.</p>
	   */
	  public int maxLengthForAttribute(String attributeName) {
	    return maxLengthForAttribute(entityName(), attributeName);
	  }  
	  
	
	
}
