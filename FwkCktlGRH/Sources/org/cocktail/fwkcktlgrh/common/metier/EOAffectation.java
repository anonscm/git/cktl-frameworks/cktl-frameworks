/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.GRHUtilities.IntervalleTemps;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IAffectation;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOVPersonnelNonEns;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.validation.ERXValidationFactory;

/**
 * 
 * Représente une affectation d'un personnel à une structure sur une période avec une quotité.
 * Attention, les affectations sont propres à la RH mais doivent enregistrer et synchroniser des 
 * {@link EORepartStructure}.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class EOAffectation extends _EOAffectation implements IAffectation {
    private static Logger LOG = Logger.getLogger(EOAffectation.class);
    private static final String NoContratException = "NoContrat";
    private static final String BadQuotiteException = "BadQuotite";
    private static final String DoubleAffectationException = "DoubleAffectation";
    
    //Renvoie l'état de l'affectation en fonction de la date courante
	public static final String ETAT_AFFECTATION_KEY = "etatAffectation";

    public static NSArray<EOAffectation> affectationsForIndividu(EOEditingContext ec, EOIndividu ind) {
    	if (ind == null) {
    		return null;
    	}
    	EOQualifier qual = ERXQ.equals(EOAffectation.TO_INDIVIDU_KEY, ind);
        NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(D_DEB_AFFECTATION_KEY).array();
        return fetchAll(ec, qual, sorts);
    	
    }

    public static NSArray<EOAffectation> affectationsValidesForIndividu(EOEditingContext ec, EOIndividu ind) {
        if (ind == null) {
            return null;
        }
        EOQualifier qual = ERXQ.equals(EOAffectation.TO_INDIVIDU_KEY, ind);
        qual = ERXQ.and(qual, ERXQ.equals(TEM_VALIDE_KEY, Constantes.VRAI));
        NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(D_DEB_AFFECTATION_KEY).array();
        return fetchAll(ec, qual, sorts);
    }
    
	/**
	 * Renvoie l affectation courante valide d'un individu.
	 * @param ec
	 * @param individu
	 * @return
	 */
	public  static EOAffectation affectationCourante(EOEditingContext ec, EOIndividu individu) {

		EOQualifier qualifier = getQualifierPourAffectationCourante(individu);

		return fetchFirstByQualifier(ec, qualifier);  
		
	}
	
	/**
	 * Renvoie les affectations courantes valides d'un individu.
	 * @param ec
	 * @param individu
	 * @return
	 */
	public  static NSArray<EOAffectation> affectationCourantes(EOEditingContext ec, EOIndividu individu) {

		EOQualifier qualifier = getQualifierPourAffectationCourante(individu);

		NSMutableArray<String> prefetchingKeyPaths = new NSMutableArray<String>();
		prefetchingKeyPaths.add(EOAffectation.TO_STRUCTURE_KEY);
		
		ERXFetchSpecification<EOAffectation> fetchSpecification = new ERXFetchSpecification<EOAffectation>(EOAffectation.ENTITY_NAME, qualifier, null);
		fetchSpecification.setPrefetchingRelationshipKeyPaths(prefetchingKeyPaths);
		
		return (NSArray<EOAffectation>)  fetchSpecification.fetchObjects(ec);
		
		  
		
	}

	private static EOQualifier getQualifierPourAffectationCourante(
			EOIndividu individu) {
		EOQualifier qualifier = 
			ERXQ.and(
					ERXQ.equals(TEM_VALIDE_KEY, Constantes.VRAI),
					ERXQ.equals(TO_INDIVIDU_KEY, individu),
					ERXQ.lessThanOrEqualTo(D_DEB_AFFECTATION_KEY, MyDateCtrl.getDateJour()),
					ERXQ.or(
							ERXQ.isNull(D_FIN_AFFECTATION_KEY),
							ERXQ.greaterThanOrEqualTo(D_FIN_AFFECTATION_KEY, MyDateCtrl.getDateJour())
					)
			);
		
		
		
		return qualifier;
	}

    
    @Override
    public void validateForSave() throws ValidationException {
    	verifDatesAffectation();
        super.validateForSave();
    }

    private void verifDatesAffectation() {
    	// Modification du 29/08/11
    	// On vérifie que la date de début et renseignée
    	// puis que la date de fin est bien après la date de début.
		if (dDebAffectation() == null) {
			throw new NSValidation.ValidationException("La date de début de l'affectation ne peut pas être nulle.");
		} else if (dFinAffectation() != null && dFinAffectation().before(dDebAffectation()) == true ){
			throw new NSValidation.ValidationException("La date de fin ne peut pas être antérieure à la date de début.");
		}
	}

	@Override
    public void validateForInsert() throws ValidationException {
        super.validateForInsert();
        verifCarriereOuContrat();
        verifQuotiteAndPremiereAffectation();
        LOG.info(" ValidateForInsert de l'affectation : " + this + ", va ajouter les reparts structure et association");
        ajouterRepartStructureEtAssociations(true);
    }

    @Override
    public void validateForUpdate() throws ValidationException {
        super.validateForUpdate();
        // A faire uniquement si l'affectation est valide
        if (Constantes.VRAI.equals(temValide())) {
            verifCarriereOuContrat();
            verifQuotiteAndPremiereAffectation();
            ajouterRepartStructureEtAssociations(false);
        }
    }

    /**
     * On ne supprime pas les affectations, on passe juste le témoin de validitié à "N"
     */
    public void annuler() {
        setTemValide(Constantes.FAUX);
        supprimerRepartsStructureExistantesSansAssociationEtAssociationsHeberge(true);
    }
    
    private void ajouterRepartStructureEtAssociations(boolean modeCreation) {
        // On fait ces ajouts juste avant l'enregistrement en base
        if (editingContext().parentObjectStore() instanceof EOObjectStoreCoordinator) {
            // Récupération des infos de carrière et contrats
            NSArray<InfoCarriereContrat> infos = InfoCarriereContrat.carrieresContratsPourPeriode(
                    editingContext(), toIndividu(), dDebAffectation(), dFinAffectation(), true);
            // Si la carrière ou le contrat est valide
            boolean contratValide = InfoCarriereContrat.carriereOuContratPrincipalValide(editingContext(), infos);
            if (contratValide) {
                // On supprime les repart structures existantes qu'il faut (ne devrait pas être nécessaire...)
                // supprimerRepartsStructureExistantesSansAssociationEtAssociationsHeberge(false);
                // Si une repart structure existe déjà on la récupère, sinon on la crée
                EORepartStructure repartStructure = EORepartStructure.creerInstanceSiNecessaire(
                        editingContext(), toIndividu(), toStructure(), persIdModification());
                // Récupération des contrats hébergés correspondant à l'affectation
                NSArray<EOContratHeberge> contrats = InfoCarriereContrat.contratsHebergesPourAffectation(editingContext(), this, infos);
                if (!contrats.isEmpty()) {
                    // Suppression de toutes les reparts association existantes de type HEBERGE
                    majRepartAssociations(contrats, repartStructure, modeCreation);
                }
            }
        }
    }
    
    /**
     * Supprime les reparts structures correspondant à cette affectation:
     *  - qui n'ont pas de reparts association
     *  - qui ont des reparts associations de type heberge (et les supprime)
     * @param immediate vrai si l'on doit faire le traitement immediatement, faux si on doit faire le traitement 
     *        avant d'enregistrer en base.
     */
    @SuppressWarnings("unchecked")
    private void supprimerRepartsStructureExistantesSansAssociationEtAssociationsHeberge(boolean immediate) {
        if (immediate || editingContext().parentObjectStore() instanceof EOObjectStoreCoordinator) {
            supprimerRepartsStructureExistantesSansAssociationEtAssociationsHeberge(editingContext(), toIndividu(), toStructure(), persIdModification());
        }
    }

    /**
     * Supprime les reparts structures correspondant à cette affectation:
     *  - qui n'ont pas de reparts association
     *  - qui ont des reparts associations de type heberge (et les supprime)
     * @param ec un editing context
     * @param individu l'individu
     * @param structure la structure
     * @param persIdModification le persid du modificateur
     */
    public static void supprimerRepartsStructureExistantesSansAssociationEtAssociationsHeberge(EOEditingContext ec, 
            EOIndividu individu,
            EOStructure structure,
            Integer persIdModification) {
        NSArray<EORepartStructure> reparts = EORepartStructure.rechercherRepartStructuresPourIndividuEtStructure(
                ec, individu, structure);
        for (EORepartStructure repart : reparts) {
            NSArray<EORepartAssociation> repartsHeberges = repart.toRepartAssociations(
                    ERXQ.equals(ERXQ.keyPath(EORepartAssociation.TO_ASSOCIATION_KEY, EOAssociation.ASS_CODE_KEY), "HEBER"));
            for (EORepartAssociation repartHeberge : repartsHeberges) {
                repart.removeFromToRepartAssociationsRelationship(repartHeberge);
                ec.deleteObject(repartHeberge);
            }
            // S'il n'y a plus d'autres reparts association on supprime le repart
            if (repart.toRepartAssociations() == null || repart.toRepartAssociations().isEmpty()) {
                ec.deleteObject(repart);
            } else {
                repart.setPersIdModification(persIdModification);
            }
        }
    }
    
    /**
     * Suprimme les reparts structures existantes si aucune  repart association associée ou si une seule association 
     * heberge.
     * @param ec l'editing context
     * @param individu un individu
     * @param structure une structure
     */
    public static void supprimerRepartsStructureExistantesAvecAssociationsHeberge(EOEditingContext ec, 
                                                                                  EOIndividu individu,
                                                                                  EOStructure structure) {
        NSArray<EORepartStructure> reparts = EORepartStructure.rechercherRepartStructuresPourIndividuEtStructure(
                ec, individu, structure);
        for (EORepartStructure repart : reparts) {
            if (repart.toRepartAssociations() == null || repart.toRepartAssociations().isEmpty())
                ec.deleteObject(repart);
            else {
                NSArray<EORepartAssociation> repartsHeberges = repart.toRepartAssociations(
                        ERXQ.equals(ERXQ.keyPath(EORepartAssociation.TO_ASSOCIATION_KEY, EOAssociation.ASS_CODE_KEY), "HEBER"));
                for (EORepartAssociation repartHeberge : repartsHeberges)
                    ec.deleteObject(repartHeberge);
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private void majRepartAssociations(NSArray<EOContratHeberge> contratsHeberges, 
                                       EORepartStructure repartStructure, 
                                       boolean estNouvelle) {
        EOAssociation associationHeberge = EOAssociation.fetchFirstRequiredByQualifier(
                editingContext(), ERXQ.equals(EOAssociation.ASS_CODE_KEY, "HEBERGE"));
        if (!estNouvelle) {
            // Supprimer de repartStructure toutes les repartsAssociation de type heberge
            java.util.Enumeration e = repartStructure.toRepartAssociations().objectEnumerator();
            NSMutableArray<EORepartAssociation> repartsToDelete = new NSMutableArray<EORepartAssociation>();
            while (e.hasMoreElements()) {
                EORepartAssociation repart = (EORepartAssociation)e.nextElement();
                if (repart.toAssociation() == associationHeberge) {
                    repartsToDelete.addObject(repart);
                }
            }
            for (EORepartAssociation repart : repartsToDelete) {
                repart.removeObjectFromBothSidesOfRelationshipWithKey(repart.toAssociation(), EORepartAssociation.TO_ASSOCIATION_KEY);
                repartStructure.removeObjectFromBothSidesOfRelationshipWithKey(repart, EORepartStructure.TO_REPART_ASSOCIATIONS_KEY);
                editingContext().deleteObject(repart);
            }
        }
        // Creer les reparts association pour ce repart structure et pour une date qui est l'intersection des contrats heberges
        // et de l'affectation
        java.util.Enumeration e = contratsHeberges.objectEnumerator();
        while (e.hasMoreElements()) {
            EOContratHeberge contrat = (EOContratHeberge)e.nextElement();
            IntervalleTemps intervalle = GRHUtilities.IntervalleTemps.intersectionPeriodes(
                    this.dDebAffectation(), this.dFinAffectation(), contrat.dateDebut(), contrat.dateFin());
            EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(editingContext());
            repartAssociation.setToAssociationRelationship(associationHeberge);
            repartAssociation.setToStructureRelationship(toStructure());
            repartAssociation.setToPersonne(toIndividu());
            repartAssociation.setPersIdCreation(persIdModification());
            repartAssociation.setPersIdModification(persIdModification());
            repartAssociation.setRasDOuverture(intervalle.debutPeriode());
            repartAssociation.setRasDFermeture(intervalle.finPeriode());
            repartStructure.addToToRepartAssociationsRelationship(repartAssociation);
        }
    }
    
    /**
     * Méthode à utiliser pour effectuer une vérification en mémoire avant de rajouter une affectation.
     * @param affectation l'affectation à rajouter 
     * @param autres les affectations déjà présentes
     * @throws NSValidation.ValidationException si une affectation existe déjà sur la période de l'affectation à rajouter
     *          ou si la somme des quotités de toutes les affectations est supérieure à 100.
     */
    public static void verifQuotiteAffectationInMemory(EOAffectation affectation, NSArray<EOAffectation> autres) 
                                                                               throws NSValidation.ValidationException {
        autres = ERXQ.filtered(autres, qualifierForIndividuEtPeriode(affectation.toIndividu(), affectation.dDebAffectation(), 
                                                                     affectation.dFinAffectation()));
        Integer quotite = 0;
        for (EOAffectation current : autres) {
            // Vérification si l'affectation est en double...
            if (!ERXEOControlUtilities.isNewObject(current) && 
                !ERXEOControlUtilities.eoEquals(affectation, current) && 
                ERXEOControlUtilities.eoEquals(affectation.toStructure(), current.toStructure())) {
                throw ERXValidationFactory.defaultFactory().createCustomException(affectation, DoubleAffectationException);
            }
        }
        if (affectation.numQuotAffectation() != null)
            quotite += affectation.numQuotAffectation();
        if (quotite > 100)
            throw ERXValidationFactory.defaultFactory().createCustomException(affectation, BadQuotiteException);
    }
    
    
    
    /**
     * Calcule la somme des quotités de l'individu associé à cette affectation.
     * Si cette somme + la quotité de cette affectation dépasse 100, une erreur
     * de validation est levée.
     * 
     */
    private void verifQuotiteAndPremiereAffectation() {
            // On récupère les quotites existantes : celles en base + celles en
            // mémoire
            EOIndividu individu = toIndividu();
            if (individu != null) {
                NSArray<EOAffectation> affectations = rechercherAffectationsPourIndividuEtPeriode(
                        individu.editingContext(), individu, dDebAffectation(), dFinAffectation(), true);
                Integer quotite = 0;
                for (EOAffectation affectation : affectations) {
                    // On comptera la quotité de this à la fin pour éviter de la
                    // compter 2 fois si elle est déjà présente en base...
                    if (!ERXEOControlUtilities.eoEquals(this, affectation)) {
                        if (affectation.numQuotAffectation() != null)
                            quotite += affectation.numQuotAffectation();
                    }
                    // Vérification si l'affectation est en double...
                    if (!ERXEOControlUtilities.isNewObject(affectation) && 
                            !ERXEOControlUtilities.eoEquals(affectation, this) && 
                            ERXEOControlUtilities.eoEquals(affectation.toStructure(), this.toStructure())) {
                        throw ERXValidationFactory.defaultFactory().createCustomException(this, DoubleAffectationException);
                    }
                }
                if (this.numQuotAffectation() != null)
                    quotite += this.numQuotAffectation();
                if (quotite > 100)
                    throw ERXValidationFactory.defaultFactory().createCustomException(this, BadQuotiteException);
            }
    }

    private void verifCarriereOuContrat() {
            if (!EOCarriere.aCarriereEnCoursSurPeriode(editingContext(), toIndividu(), dDebAffectation(),
                    dFinAffectation())
                    && !EOContrat.aContratToutTypeEnCoursSurPeriode(editingContext(), toIndividu(), dDebAffectation(),
                            dFinAffectation())
                    && !EOContratHeberge.aContratHeberge(editingContext(), toIndividu(), dDebAffectation(),
                            dFinAffectation())) {
                throw ERXValidationFactory.defaultFactory().createCustomException(this, NoContratException);
            }
    }

    /**
     * Recherche des affectations d'un individu pendant une p&eacute;riode
     * donn&eacute;e. Les donn&eacute;es ne sont pas raffra&icirc;chies
     * 
     * @param individu
     * @param debutPeriode
     *            d&eacute;but p&eacute;riode
     * @param finPeriode
     *            fin p&eacute;riode (peut &ecirc;tre nulle)
     */
    public static NSArray<EOAffectation> rechercherAffectationsPourIndividuEtPeriode(EOEditingContext editingContext,
            EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
        return rechercherAffectationsPourIndividuEtPeriode(editingContext, individu, debutPeriode, finPeriode, false);
    }

    /**
     * Recherche des affectations d'un individu pendant une p&eacute;riode
     * donn&eacute;e
     * 
     * @param editingContext 
     * @param individu individu sur lequel la recherche d'affectation est faite
     * @param debutPeriode
     *            d&eacute;but p&eacute;riode
     * @param finPeriode
     *            fin p&eacute;riode (peut &ecirc;tre nulle)
     * @param forceRefresh
     *            true si on veut forcer le raffra&icirc;chissement
     * 
     * @return un tableau des affectations valides d'un individu sur une période donnée
     */
    public static NSArray<EOAffectation> rechercherAffectationsPourIndividuEtPeriode(EOEditingContext editingContext,
            EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode, boolean forceRefresh) {
        EOQualifier qual = qualifierForIndividuEtPeriode(individu, debutPeriode, finPeriode);
        ERXQ.and(qual, ERXQ.equals(TEM_VALIDE_KEY, Constantes.VRAI));
        ERXFetchSpecification<EOAffectation> fspec = new ERXFetchSpecification<EOAffectation>(
                EOAffectation.ENTITY_NAME, qual, null);
//        EOAffectation.ENTITY_NAME, null, null);
        
        fspec.setRefreshesRefetchedObjects(forceRefresh);
        fspec.setIncludeEditingContextChanges(false);
        return fspec.fetchObjects(editingContext);
    }

    /**
     * @param individu
     * @param debutPeriode
     * @param finPeriode
     * @return un qualifier correspondant à la période et l'individu
     */
    private static EOQualifier qualifierForIndividuEtPeriode(EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
        EOQualifier qual = ERXQ.equals(ERXQ.keyPath(EOAffectation.TO_INDIVIDU_KEY, EOIndividu.NO_INDIVIDU_KEY), individu.noIndividu());
        qual = ERXQ.and(qual, ERXQ.equals(EOAffectation.TEM_VALIDE_KEY, "O"));
        EOQualifier periodeQual = GRHUtilities.qualifierPourPeriode("dDebAffectation", debutPeriode, "dFinAffectation",
                finPeriode);
        qual = ERXQ.and(qual, periodeQual);
        return qual;
    }
    
    /**
     * Renvoie l'état de l'affectation en fonction de la date courante
     * @return etat de l'affectation
     */
    public String getEtatAffectation() {
    	String etatAffectation = "";
    	
    	NSTimestamp dateCourante = DateCtrl.now();
    	NSTimestamp dateDebut = this.dDebAffectation();
    	NSTimestamp dateFin = this.dFinAffectation();
    	
    	if (dateDebut != null) {
	    	if (dateFin != null && DateCtrl.isBefore(dateFin, dateCourante)) {
	    		etatAffectation = "Cloturée";
	    	} else if (DateCtrl.isBefore(dateCourante, dateDebut)) {
	    		etatAffectation = "Future";
	    	} else {
	    		etatAffectation = "En cours";
	    	}
    	}
    	
    	return etatAffectation;
    }
    
// methodes rajoutee
    
    public String display() {
      String display = toIndividu().display() + " ";
      if (dFinAffectation() != null) {
        display += "du " + DateCtrl.dateToString(dDebAffectation()) + " au " + DateCtrl.dateToString(dFinAffectation());
      } else {
        display += "a partir du " + DateCtrl.dateToString(dDebAffectation());        
      }
      display += " (" + numQuotAffectation() + "%)";
      return display;
    }
    
    public static EOQualifier getQualifierAffectationCourantePrincipale(EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
    	return ERXQ.and(
    			qualifierForIndividuEtPeriode(individu, debutPeriode, finPeriode),
				ERXQ.equals((EOAffectation.TEM_PRINCIPALE).key(), Constantes.VRAI));
    	
    }
    
    
    /**
	 * Liste des agents non enseignants actuels affectés à la structure
	 * 
	 * @return
	 */
	public static NSArray<EOIndividu> getIndividuAffecteVPersonnelNonEns(EOEditingContext ec,EOStructure structure) {
		NSArray<EOIndividu> result = null;

		NSTimestamp now = DateCtrl.now();

		String toVPersonnelActuelKey = EOAffectation.TO_INDIVIDU_KEY + "." + EOIndividu.TOS_V_PERSONNEL_NON_ENS_KEY;

		EOQualifier qual = CktlDataBus.newCondition(
					EOAffectation.TO_STRUCTURE_KEY + "=%@ and "+ EOAffectation.TEM_VALIDE_KEY + "=%@" +" and " + EOAffectation.D_DEB_AFFECTATION_KEY + "<=%@ and (" +
							EOAffectation.D_FIN_AFFECTATION_KEY + " >= %@ or " + EOAffectation.D_FIN_AFFECTATION_KEY + "=nil) and " +
							toVPersonnelActuelKey + "." + EOVPersonnelNonEns.TO_INDIVIDU_KEY + "." + EOIndividu.PERS_ID_KEY + "<> nil and " +
							toVPersonnelActuelKey + "." + EOVPersonnelNonEns.D_DEBUT_KEY + "<=%@ and (" +
							toVPersonnelActuelKey + "." + EOVPersonnelNonEns.D_FIN_KEY + " >= %@ or " + toVPersonnelActuelKey + "." + EOVPersonnelNonEns.D_FIN_KEY + "=nil)"
									,
					new NSArray<Object>(new Object[] {
							structure, Constantes.VRAI, now, now, now, now}));
		result = (NSArray<EOIndividu>) EOAffectation.fetchAll(ec, qual, null).valueForKey(EOAffectation.TO_INDIVIDU_KEY);

		return result;
	}
}