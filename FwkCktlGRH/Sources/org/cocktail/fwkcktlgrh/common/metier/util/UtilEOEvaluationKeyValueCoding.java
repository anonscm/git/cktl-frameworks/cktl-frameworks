package org.cocktail.fwkcktlgrh.common.metier.util;

import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EOPoste;
import org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividuGrhService;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;


/**
 * Le motif des classes representant une evaluation.
 * 
 * @see EOEvaluation
 * @see EOVCandidatEvaluation
 * 
 *      Ces objets sont filtres par des qualifier, il faut donc des methodes
 *      communes.
 * 
 * @author ctarade
 */
public abstract class UtilEOEvaluationKeyValueCoding extends AfwkGRHRecord {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3333333333333333333L;
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TOS_STRUCTURE_KEY = "tosStructure";
	public static final String TO_EVALUATION_PERIODE_KEY = "toEvaluationPeriode";
	public static final String TOS_LAST_FICHE_DE_POSTE_KEY = "tosLastFicheDePoste";

	/**
	 * L'agent a evaluer
	 * 
	 * @return
	 */
	public abstract EOIndividu toIndividu();

	/**
	 * La cle primaire de l'agent a evaluer
	 * 
	 * @return
	 */
	public abstract Integer noIndividuVisible();

	/**
	 * La cle primaire de la periode
	 * 
	 * @return
	 */
	public abstract Integer epeKeyVisible();

	/**
	 * La periode d'evaluation concernee
	 * 
	 * @return
	 */
	public abstract EOEvaluationPeriode toEvaluationPeriode();

	/**
	 * Retourne l'objet <code>EOVCandidatEvaluation</code> associe a cet
	 * enregistrement.
	 */
	public abstract EOVCandidatEvaluation toVCandidatEvaluation();

	/**
	 * 
	 * @param value
	 */
	public abstract void setIsViseParResponsableRh(boolean value);

	/**
	 * les fiches de poste (les dernieres) sur lesquelles portent cette evaluation
	 * -> filtrage par rapport aux dates de l'evaluation
	 */
	public NSArray tosLastFicheDePoste() {
		IndividuGrhService individuService = new IndividuGrhService();
		NSArray records = individuService.tosFicheDePostePourDate(toIndividu(), editingContext(),
				toEvaluationPeriode().epeDDebut(), toEvaluationPeriode().epeDFin());
		// classement chronologique
		records = CktlSort.sortedArray(records, EOFicheDePoste.FDP_D_DEBUT_KEY);
		NSArray recsPoste = NSArrayCtrl.removeDuplicate((NSArray) records.valueForKey(EOFicheDePoste.TO_POSTE_KEY));

		NSArray recsFicheDePoste = new NSArray();
		// ne conserver que les dernieres fiches en date
		for (int i = 0; i < recsPoste.count(); i++) {
			EOPoste poste = (EOPoste) recsPoste.objectAtIndex(i);
			NSArray recs = EOQualifier.filteredArrayWithQualifier(
					records, CktlDataBus.newCondition(EOFicheDePoste.TO_POSTE_KEY + "=%@", new NSArray(poste)));
			// on ne conserve que le dernier
			if (recs.count() > 0) {
				recsFicheDePoste = recsFicheDePoste.arrayByAddingObject(recs.lastObject());
			}
		}

		// ne conserver que celles dont l'occupation du poste est la deniere
		NSArray result = new NSArray();

		NSArray recsAffDet = NSArrayCtrl.flattenArray((NSArray) recsFicheDePoste.valueForKeyPath(EOFicheDePoste.TO_POSTE_KEY + "." + EOPoste.TOS_AFFECTATION_DETAIL_KEY));

		// d'abord celle non fermees sur la periode
		NSArray recsAffDetNonFermees = new NSArray();
		for (int i = 0; i < recsAffDet.count(); i++) {
			EOAffectationDetail recAffDet = (EOAffectationDetail) recsAffDet.objectAtIndex(i);
			if (recAffDet.toAffectation().toIndividu().noIndividu().intValue() == noIndividuVisible().intValue() && (
					recAffDet.dFin() == null || DateCtrl.isAfterEq(recAffDet.dFin(), toEvaluationPeriode().epeDFin()))) {
				recsAffDetNonFermees = recsAffDetNonFermees.arrayByAddingObject(recAffDet);
			}
		}

		// ce eoqualifier marche pas ???
		/*
		 * EOQualifier.filteredArrayWithQualifier( recsAffDet,
		 * CktlDataBus.newCondition( EOAffectationDetail.D_FIN_AFFECTATION_DETAIL +
		 * "=%@ OR " + EOAffectationDetail.D_FIN_AFFECTATION_DETAIL + ">=%@", new
		 * NSArray(new Object[]{NSKeyValueCoding.NullValue, evaDFin()})));
		 */
		if (recsAffDetNonFermees.count() > 0) {
			// on prend les fiches ayant ces occupation
			for (int i = 0; i < recsFicheDePoste.count(); i++) {
				EOFicheDePoste recFicheDePoste = (EOFicheDePoste) recsFicheDePoste.objectAtIndex(i);
				for (int j = 0; j < recsAffDetNonFermees.count(); j++) {
					EOAffectationDetail recAffDet = (EOAffectationDetail) recsAffDetNonFermees.objectAtIndex(j);
					if (recFicheDePoste.tosAffectationDetail().containsObject(recAffDet)) {
						result = result.arrayByAddingObject(recFicheDePoste);
						break;
					}
				}
			}
		} else {
			// on prend alors les dernieres en date
			result = recsFicheDePoste;
		}

		return result;
	}

	/**
	 * La liste des postes de rattachement de cette evaluation
	 * 
	 * @return
	 */
	public NSArray tosPoste() {
		return (NSArray) tosLastFicheDePoste().valueForKey(EOFicheDePoste.TO_POSTE_KEY);
	}

	/**
	 * La liste des services de rattachement de cette evaluation
	 * 
	 * @return
	 */
	public NSArray<EOStructure> tosStructure() {
		return (NSArray<EOStructure>) tosPoste().valueForKey(EOPoste.TO_STRUCTURE_KEY);
	}

	public final static CktlSort ARRAY_SORT =
					CktlSort.newSort(
							TO_EVALUATION_PERIODE_KEY + "." + EOEvaluationPeriode.EPE_D_DEBUT_KEY);

	/**
	 * retourne l'evaluation suivante de l'individu (celle dont la date de debut
	 * arrive en premier apres la date de fin de celle ci)
	 * 
	 * @return
	 */
	public EOEvaluation toEvaluationSuivante() {
		IndividuGrhService individuService = new IndividuGrhService();
		NSArray<EOEvaluation> evaluations =  individuService.tosEvaluation(toIndividu(), this.editingContext(), null, null);
		EOQualifier qual = ERXQ.greaterThan(
				TO_EVALUATION_PERIODE_KEY + "." + EOEvaluationPeriode.EPE_D_DEBUT_KEY, toEvaluationPeriode().epeDFin());
		NSArray<EOEvaluation> records = EOQualifier.filteredArrayWithQualifier(evaluations, qual);
		EOEvaluation record = null;
		if (records.count() > 0) {
			records = EOSortOrdering.sortedArrayUsingKeyOrderArray(records, ARRAY_SORT);
			record = records.objectAtIndex(0);
		}
		return record;
	}

	/**
	 * retourne l'evaluation precedente de l'individu (celle dont la date de fin
	 * arrive en dernier apres la date de debut de celle ci)
	 * 
	 * @return
	 */
	public EOEvaluation toEvaluationPrecedente() {
		IndividuGrhService individuService = new IndividuGrhService();
		NSArray<EOEvaluation> evaluations = individuService.tosEvaluation(toIndividu(), this.editingContext(), null, null);
		EOQualifier qual = ERXQ.lessThan(
				TO_EVALUATION_PERIODE_KEY + "." + EOEvaluationPeriode.EPE_D_FIN_KEY, toEvaluationPeriode().epeDDebut());
		NSArray<EOEvaluation> records = EOQualifier.filteredArrayWithQualifier(evaluations, qual);
		EOEvaluation record = null;
		if (records.count() > 0) {
			records = EOSortOrdering.sortedArrayUsingKeyOrderArray(records, ARRAY_SORT);
			record = records.lastObject();
		}
		return record;
	}

	/**
	 * TODO : suppression
	 * 
	 * @param individuResp
	 * @return
	 */
	public boolean isModificationObjPrecAutorisee(EOIndividu individuResp) {
		return false;
	}






}
