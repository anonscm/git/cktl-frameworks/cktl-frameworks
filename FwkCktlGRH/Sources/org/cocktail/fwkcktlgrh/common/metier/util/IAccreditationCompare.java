package org.cocktail.fwkcktlgrh.common.metier.util;

import java.util.Comparator;

import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_Accreditation;

public class IAccreditationCompare implements Comparator<I_Accreditation> {

	public int compare(I_Accreditation o1, I_Accreditation o2) {
		if (o1.toPersonneTitulaire().getNomPrenomAffichage().equals(o2.toPersonneTitulaire().getNomPrenomAffichage())) {
			return o1.toPersonneCible().getNomPrenomAffichage().compareTo(o2.toPersonneCible().getNomPrenomAffichage());
		}
		return o1.toPersonneTitulaire().getNomPrenomAffichage().compareTo(o2.toPersonneTitulaire().getNomPrenomAffichage());
	}

	
	
	
}
