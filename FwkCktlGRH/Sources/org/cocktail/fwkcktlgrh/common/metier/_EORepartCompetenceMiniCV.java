/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartCompetenceMiniCV.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartCompetenceMiniCV extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_RepartCompetenceMiniCV";

	// Attributes
	public static final String CHAMP_LIBRE_KEY = "champLibre";
	public static final String CODE_EMPLOI_KEY = "codeEmploi";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INDIVIDU_KEY = "individu";
	public static final String POSITION_KEY = "position";

	public static final ERXKey<String> CHAMP_LIBRE = new ERXKey<String>("champLibre");
	public static final ERXKey<String> CODE_EMPLOI = new ERXKey<String>("codeEmploi");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> INDIVIDU = new ERXKey<Integer>("individu");
	public static final ERXKey<Integer> POSITION = new ERXKey<Integer>("position");
	// Relationships
	public static final String TO_REFERENS_COMPETENCES_KEY = "toReferensCompetences";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences> TO_REFERENS_COMPETENCES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences>("toReferensCompetences");

  private static Logger LOG = Logger.getLogger(_EORepartCompetenceMiniCV.class);

  public EORepartCompetenceMiniCV localInstanceIn(EOEditingContext editingContext) {
    EORepartCompetenceMiniCV localInstance = (EORepartCompetenceMiniCV)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String champLibre() {
    return (String) storedValueForKey("champLibre");
  }

  public void setChampLibre(String value) {
    if (_EORepartCompetenceMiniCV.LOG.isDebugEnabled()) {
    	_EORepartCompetenceMiniCV.LOG.debug( "updating champLibre from " + champLibre() + " to " + value);
    }
    takeStoredValueForKey(value, "champLibre");
  }

  public String codeEmploi() {
    return (String) storedValueForKey("codeEmploi");
  }

  public void setCodeEmploi(String value) {
    if (_EORepartCompetenceMiniCV.LOG.isDebugEnabled()) {
    	_EORepartCompetenceMiniCV.LOG.debug( "updating codeEmploi from " + codeEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "codeEmploi");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartCompetenceMiniCV.LOG.isDebugEnabled()) {
    	_EORepartCompetenceMiniCV.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartCompetenceMiniCV.LOG.isDebugEnabled()) {
    	_EORepartCompetenceMiniCV.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer individu() {
    return (Integer) storedValueForKey("individu");
  }

  public void setIndividu(Integer value) {
    if (_EORepartCompetenceMiniCV.LOG.isDebugEnabled()) {
    	_EORepartCompetenceMiniCV.LOG.debug( "updating individu from " + individu() + " to " + value);
    }
    takeStoredValueForKey(value, "individu");
  }

  public Integer position() {
    return (Integer) storedValueForKey("position");
  }

  public void setPosition(Integer value) {
    if (_EORepartCompetenceMiniCV.LOG.isDebugEnabled()) {
    	_EORepartCompetenceMiniCV.LOG.debug( "updating position from " + position() + " to " + value);
    }
    takeStoredValueForKey(value, "position");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences toReferensCompetences() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences)storedValueForKey("toReferensCompetences");
  }

  public void setToReferensCompetencesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences value) {
    if (_EORepartCompetenceMiniCV.LOG.isDebugEnabled()) {
      _EORepartCompetenceMiniCV.LOG.debug("updating toReferensCompetences from " + toReferensCompetences() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences oldValue = toReferensCompetences();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toReferensCompetences");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toReferensCompetences");
    }
  }
  

  public static EORepartCompetenceMiniCV create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer individu
, Integer position
) {
    EORepartCompetenceMiniCV eo = (EORepartCompetenceMiniCV) EOUtilities.createAndInsertInstance(editingContext, _EORepartCompetenceMiniCV.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIndividu(individu);
		eo.setPosition(position);
    return eo;
  }

  public static NSArray<EORepartCompetenceMiniCV> fetchAll(EOEditingContext editingContext) {
    return _EORepartCompetenceMiniCV.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartCompetenceMiniCV> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartCompetenceMiniCV.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartCompetenceMiniCV> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartCompetenceMiniCV.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartCompetenceMiniCV> eoObjects = (NSArray<EORepartCompetenceMiniCV>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartCompetenceMiniCV fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartCompetenceMiniCV.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartCompetenceMiniCV fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartCompetenceMiniCV> eoObjects = _EORepartCompetenceMiniCV.fetch(editingContext, qualifier, null);
    EORepartCompetenceMiniCV eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartCompetenceMiniCV)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartCompetenceMiniCV that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartCompetenceMiniCV fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartCompetenceMiniCV.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartCompetenceMiniCV fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartCompetenceMiniCV eoObject = _EORepartCompetenceMiniCV.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartCompetenceMiniCV that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartCompetenceMiniCV fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartCompetenceMiniCV fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartCompetenceMiniCV eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartCompetenceMiniCV)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartCompetenceMiniCV localInstanceIn(EOEditingContext editingContext, EORepartCompetenceMiniCV eo) {
    EORepartCompetenceMiniCV localInstance = (eo == null) ? null : (EORepartCompetenceMiniCV)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
