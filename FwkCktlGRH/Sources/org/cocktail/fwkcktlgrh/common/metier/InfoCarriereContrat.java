package org.cocktail.fwkcktlgrh.common.metier;

import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXS;

public class InfoCarriereContrat {
    
    public final static String DATE_DEBUT_KEY = "dateDebut";
    
    private NSTimestamp dateDebut;
    private NSTimestamp dateFin;
    private EOGlobalID globalIDObjet;
    private boolean estCarriere;
    private boolean estVacation;
    private boolean estHeberge;

    // constructeurs
    public InfoCarriereContrat(EOCarriere carriere) {
        this.dateDebut = carriere.dDebCarriere();
        this.dateFin = carriere.dFinCarriere();
        this.globalIDObjet = carriere.editingContext().globalIDForObject(carriere);
        this.estCarriere = true;
        this.estVacation = false;
        this.estHeberge = false;
    }

    public InfoCarriereContrat(EOContrat contrat) {
        this.dateDebut = contrat.dDebContratTrav();
        this.dateFin = contrat.dFinContratTrav();
        if (contrat.dFinAnticipee() != null) {
            this.dateFin = contrat.dFinAnticipee();
        }
        this.globalIDObjet = contrat.editingContext().globalIDForObject(contrat);
        this.estVacation = contrat.estVacataire();
        this.estCarriere = false;
        this.estHeberge = false;
    }

    public InfoCarriereContrat(EOContratHeberge contrat) {
        this.dateDebut = contrat.dateDebut();
        this.dateFin = contrat.dateFin();
        this.globalIDObjet = contrat.editingContext().globalIDForObject(contrat);
        this.estVacation = false;
        this.estCarriere = false;
        this.estHeberge = true;
    }

    // accesseurs
    public NSTimestamp dateDebut() {
        return dateDebut;
    }

    public NSTimestamp dateFin() {
        return dateFin;
    }

    public boolean estCarriere() {
        return estCarriere;
    }

    public boolean estVacation() {
        return estVacation;
    }

    public boolean estHeberge() {
        return estHeberge;
    }

    public EOGlobalID globalIDObjet() {
        return globalIDObjet;
    }

    public String libelle() {
        if (estCarriere()) {
            return "Carriere";
        } else if (estHeberge()) {
            return "Contrat Hébergé";
        } else if (estVacation()) {
            return "Vacation";
        } else {
            return "Contrat";
        }
    }

    public String toString() {
        String texte = libelle() + " du " + DateCtrl.dateToString(dateDebut());
        if (dateFin() != null) {
            texte = texte + " au " + DateCtrl.dateToString(dateFin());
        }
        return texte;
    }

    // interface keyValueCoding
    public void takeValueForKey(Object valeur, String cle) {
        NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);
    }

    public Object valueForKey(String cle) {
        return NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
    }

    // M&thodes statiques
    /**
     * retourne la liste des carri&egrave;res/contrats (objets de type
     * InfoCarriereContrat) d'un individu pour la p&eacute;riode ordonn&eacute;s
     * par ordre d&eacute;croissant de date d&eacute;but
     */
    public static NSArray<InfoCarriereContrat> carrieresContratsPourPeriode(EOEditingContext editingContext, EOIndividu individu,
            NSTimestamp dateDebut, NSTimestamp dateFin, boolean estAffectation) {
        NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext, individu, dateDebut, dateFin,
                false);
        NSMutableArray<InfoCarriereContrat> carrieresContrats = new NSMutableArray<InfoCarriereContrat>();
        for (EOCarriere carriere : carrieres) {
            InfoCarriereContrat cc = new InfoCarriereContrat(carriere);
            carrieresContrats.addObject(cc);
        }
        NSArray<EOContrat> contrats = null;
        if (carrieresContrats.count() > 0) {
            // Pour les titulaires, on n'indique pas les contrats de vacation
            contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext, individu, dateDebut, dateFin,
                    false);
        } else {
            contrats = EOContrat.rechercherTousContratsPourIndividuEtPeriode(editingContext, individu, dateDebut,
                    dateFin, false);
        }
        for (EOContrat contrat : contrats) {
            InfoCarriereContrat cc = new InfoCarriereContrat(contrat);
            carrieresContrats.addObject(cc);
        }
        if (estAffectation) {
            // Rechercher les contrats d'heberges
            NSArray<EOContratHeberge> contratsHeberges = EOContratHeberge.rechercherContratsPourIndividuEtPeriode(editingContext,
                    individu, dateDebut, dateFin);
            for (EOContratHeberge contratHeberge : contratsHeberges) {
                InfoCarriereContrat cc = new InfoCarriereContrat(contratHeberge);
                carrieresContrats.addObject(cc);
            }
        }
        return ERXS.sorted(carrieresContrats, ERXS.desc("dateDebut"));
    }

    /**
     * @param ec
     * @param infos
     * @return
     */
    public static boolean carriereOuContratPrincipalValide(EOEditingContext editingContext, 
                                                           NSArray<InfoCarriereContrat> infos) {
        for (InfoCarriereContrat info : infos) {
            if (info.estCarriere() || info.estHeberge()) {
                return true;
            } else {
                EOContrat contrat = (EOContrat)ERXEOControlUtilities.objectsForGlobalIDs(editingContext, new NSArray(info.globalIDObjet())).lastObject();
                return contrat.estVacataire() == false;
            }
        }
        return false;
    }

    public static NSArray<EOContratHeberge> contratsHebergesPourAffectation(EOEditingContext ec,
            EOAffectation affectation, NSArray<InfoCarriereContrat> infos) {
        // Verifier si il existe un ou plusieurs contrats d'heberge avec les
        // memes dates que l'affectation
        NSMutableArray<EOContratHeberge> contratsHeberges = new NSMutableArray<EOContratHeberge>();
        for (InfoCarriereContrat info : infos) {
            if (info.estHeberge()) {
                EOContratHeberge contrat = (EOContratHeberge)ERXEOControlUtilities.objectsForGlobalIDs(ec, new NSArray(info.globalIDObjet())).lastObject();
                // Si il y a une intersection commune, on ajoute ce contrat
                if (GRHUtilities.IntervalleTemps.intersectionPeriodes(affectation.dDebAffectation(),
                        affectation.dFinAffectation(), contrat.dateDebut(), contrat.dateFin()) != null) {
                    contratsHeberges.addObject(contrat);
                }
            }
        }
        return contratsHeberges;
    }

}
