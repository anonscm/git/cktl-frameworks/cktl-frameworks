/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOStructureInfo.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOStructureInfo extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_StructureInfo";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String SIN_D_DEBUT_KEY = "sinDDebut";
	public static final String SIN_D_FIN_KEY = "sinDFin";
	public static final String SIN_LIBELLE_KEY = "sinLibelle";
	public static final String SIN_TYPE_KEY = "sinType";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<NSTimestamp> SIN_D_DEBUT = new ERXKey<NSTimestamp>("sinDDebut");
	public static final ERXKey<NSTimestamp> SIN_D_FIN = new ERXKey<NSTimestamp>("sinDFin");
	public static final ERXKey<String> SIN_LIBELLE = new ERXKey<String>("sinLibelle");
	public static final ERXKey<Integer> SIN_TYPE = new ERXKey<Integer>("sinType");
	// Relationships
	public static final String TO_STRUCTURE_KEY = "toStructure";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");

  private static Logger LOG = Logger.getLogger(_EOStructureInfo.class);

  public EOStructureInfo localInstanceIn(EOEditingContext editingContext) {
    EOStructureInfo localInstance = (EOStructureInfo)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOStructureInfo.LOG.isDebugEnabled()) {
    	_EOStructureInfo.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOStructureInfo.LOG.isDebugEnabled()) {
    	_EOStructureInfo.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp sinDDebut() {
    return (NSTimestamp) storedValueForKey("sinDDebut");
  }

  public void setSinDDebut(NSTimestamp value) {
    if (_EOStructureInfo.LOG.isDebugEnabled()) {
    	_EOStructureInfo.LOG.debug( "updating sinDDebut from " + sinDDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "sinDDebut");
  }

  public NSTimestamp sinDFin() {
    return (NSTimestamp) storedValueForKey("sinDFin");
  }

  public void setSinDFin(NSTimestamp value) {
    if (_EOStructureInfo.LOG.isDebugEnabled()) {
    	_EOStructureInfo.LOG.debug( "updating sinDFin from " + sinDFin() + " to " + value);
    }
    takeStoredValueForKey(value, "sinDFin");
  }

  public String sinLibelle() {
    return (String) storedValueForKey("sinLibelle");
  }

  public void setSinLibelle(String value) {
    if (_EOStructureInfo.LOG.isDebugEnabled()) {
    	_EOStructureInfo.LOG.debug( "updating sinLibelle from " + sinLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "sinLibelle");
  }

  public Integer sinType() {
    return (Integer) storedValueForKey("sinType");
  }

  public void setSinType(Integer value) {
    if (_EOStructureInfo.LOG.isDebugEnabled()) {
    	_EOStructureInfo.LOG.debug( "updating sinType from " + sinType() + " to " + value);
    }
    takeStoredValueForKey(value, "sinType");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey("toStructure");
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOStructureInfo.LOG.isDebugEnabled()) {
      _EOStructureInfo.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructure");
    }
  }
  

  public static EOStructureInfo create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, NSTimestamp sinDDebut
, Integer sinType
, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure) {
    EOStructureInfo eo = (EOStructureInfo) EOUtilities.createAndInsertInstance(editingContext, _EOStructureInfo.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setSinDDebut(sinDDebut);
		eo.setSinType(sinType);
    eo.setToStructureRelationship(toStructure);
    return eo;
  }

  public static NSArray<EOStructureInfo> fetchAll(EOEditingContext editingContext) {
    return _EOStructureInfo.fetchAll(editingContext, null);
  }

  public static NSArray<EOStructureInfo> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOStructureInfo.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOStructureInfo> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOStructureInfo.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOStructureInfo> eoObjects = (NSArray<EOStructureInfo>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOStructureInfo fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStructureInfo.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStructureInfo fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOStructureInfo> eoObjects = _EOStructureInfo.fetch(editingContext, qualifier, null);
    EOStructureInfo eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOStructureInfo)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_StructureInfo that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStructureInfo fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStructureInfo.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOStructureInfo fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOStructureInfo eoObject = _EOStructureInfo.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_StructureInfo that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOStructureInfo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOStructureInfo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOStructureInfo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOStructureInfo)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOStructureInfo localInstanceIn(EOEditingContext editingContext, EOStructureInfo eo) {
    EOStructureInfo localInstance = (eo == null) ? null : (EOStructureInfo)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
