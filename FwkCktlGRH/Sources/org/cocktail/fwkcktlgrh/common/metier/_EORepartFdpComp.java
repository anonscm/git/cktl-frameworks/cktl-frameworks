/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartFdpComp.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartFdpComp extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_RepartFdpComp";

	// Attributes
	public static final String CODE_EMPLOI_KEY = "codeEmploi";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String RFC_POSITION_KEY = "rfcPosition";

	public static final ERXKey<String> CODE_EMPLOI = new ERXKey<String>("codeEmploi");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> RFC_POSITION = new ERXKey<Integer>("rfcPosition");
	// Relationships
	public static final String TO_COMPETENCE_KEY = "toCompetence";
	public static final String TO_FICHE_DE_POSTE_KEY = "toFicheDePoste";
	public static final String TO_REFERENS_COMPETENCES_KEY = "toReferensCompetences";
	public static final String TOS_REPART_NIVEAU_COMP_KEY = "tosRepartNiveauComp";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence> TO_COMPETENCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence>("toCompetence");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste> TO_FICHE_DE_POSTE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste>("toFicheDePoste");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences> TO_REFERENS_COMPETENCES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences>("toReferensCompetences");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp> TOS_REPART_NIVEAU_COMP = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp>("tosRepartNiveauComp");

  private static Logger LOG = Logger.getLogger(_EORepartFdpComp.class);

  public EORepartFdpComp localInstanceIn(EOEditingContext editingContext) {
    EORepartFdpComp localInstance = (EORepartFdpComp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeEmploi() {
    return (String) storedValueForKey("codeEmploi");
  }

  public void setCodeEmploi(String value) {
    if (_EORepartFdpComp.LOG.isDebugEnabled()) {
    	_EORepartFdpComp.LOG.debug( "updating codeEmploi from " + codeEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "codeEmploi");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartFdpComp.LOG.isDebugEnabled()) {
    	_EORepartFdpComp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartFdpComp.LOG.isDebugEnabled()) {
    	_EORepartFdpComp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer rfcPosition() {
    return (Integer) storedValueForKey("rfcPosition");
  }

  public void setRfcPosition(Integer value) {
    if (_EORepartFdpComp.LOG.isDebugEnabled()) {
    	_EORepartFdpComp.LOG.debug( "updating rfcPosition from " + rfcPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "rfcPosition");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCompetence toCompetence() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCompetence)storedValueForKey("toCompetence");
  }

  public void setToCompetenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompetence value) {
    if (_EORepartFdpComp.LOG.isDebugEnabled()) {
      _EORepartFdpComp.LOG.debug("updating toCompetence from " + toCompetence() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCompetence oldValue = toCompetence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCompetence");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCompetence");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste toFicheDePoste() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste)storedValueForKey("toFicheDePoste");
  }

  public void setToFicheDePosteRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste value) {
    if (_EORepartFdpComp.LOG.isDebugEnabled()) {
      _EORepartFdpComp.LOG.debug("updating toFicheDePoste from " + toFicheDePoste() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste oldValue = toFicheDePoste();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFicheDePoste");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFicheDePoste");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences toReferensCompetences() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences)storedValueForKey("toReferensCompetences");
  }

  public void setToReferensCompetencesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences value) {
    if (_EORepartFdpComp.LOG.isDebugEnabled()) {
      _EORepartFdpComp.LOG.debug("updating toReferensCompetences from " + toReferensCompetences() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences oldValue = toReferensCompetences();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toReferensCompetences");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toReferensCompetences");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp> tosRepartNiveauComp() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp>)storedValueForKey("tosRepartNiveauComp");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp> tosRepartNiveauComp(EOQualifier qualifier) {
    return tosRepartNiveauComp(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp> tosRepartNiveauComp(EOQualifier qualifier, boolean fetch) {
    return tosRepartNiveauComp(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp> tosRepartNiveauComp(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp.TO_REPART_FDP_COMP_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartNiveauComp();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartNiveauCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp object) {
    if (_EORepartFdpComp.LOG.isDebugEnabled()) {
      _EORepartFdpComp.LOG.debug("adding " + object + " to tosRepartNiveauComp relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartNiveauComp");
  }

  public void removeFromTosRepartNiveauCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp object) {
    if (_EORepartFdpComp.LOG.isDebugEnabled()) {
      _EORepartFdpComp.LOG.debug("removing " + object + " from tosRepartNiveauComp relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartNiveauComp");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp createTosRepartNiveauCompRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_RepartNiveauComp");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartNiveauComp");
    return (org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp) eo;
  }

  public void deleteTosRepartNiveauCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartNiveauComp");
  }

  public void deleteAllTosRepartNiveauCompRelationships() {
    Enumeration objects = tosRepartNiveauComp().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartNiveauCompRelationship((org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp)objects.nextElement());
    }
  }


  public static EORepartFdpComp create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste toFicheDePoste) {
    EORepartFdpComp eo = (EORepartFdpComp) EOUtilities.createAndInsertInstance(editingContext, _EORepartFdpComp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToFicheDePosteRelationship(toFicheDePoste);
    return eo;
  }

  public static NSArray<EORepartFdpComp> fetchAll(EOEditingContext editingContext) {
    return _EORepartFdpComp.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartFdpComp> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartFdpComp.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartFdpComp> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartFdpComp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartFdpComp> eoObjects = (NSArray<EORepartFdpComp>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartFdpComp fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartFdpComp.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartFdpComp fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartFdpComp> eoObjects = _EORepartFdpComp.fetch(editingContext, qualifier, null);
    EORepartFdpComp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartFdpComp)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartFdpComp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartFdpComp fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartFdpComp.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartFdpComp fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartFdpComp eoObject = _EORepartFdpComp.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartFdpComp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartFdpComp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartFdpComp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartFdpComp eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartFdpComp)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartFdpComp localInstanceIn(EOEditingContext editingContext, EORepartFdpComp eo) {
    EORepartFdpComp localInstance = (eo == null) ? null : (EORepartFdpComp)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
