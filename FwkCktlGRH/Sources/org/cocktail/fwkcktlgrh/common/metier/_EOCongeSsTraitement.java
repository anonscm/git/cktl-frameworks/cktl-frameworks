/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOCongeSsTraitement.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOCongeSsTraitement extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_CongeSsTraitement";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
	public static final ERXKey<NSTimestamp> DATE_ARRETE = new ERXKey<NSTimestamp>("dateArrete");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> NO_ARRETE = new ERXKey<String>("noArrete");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_ABSENCE_KEY = "toAbsence";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_MOTIF_CG_STAGIAIRE_KEY = "toMotifCgStagiaire";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAbsence> TO_ABSENCE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAbsence>("toAbsence");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMotifCgStagiaire> TO_MOTIF_CG_STAGIAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMotifCgStagiaire>("toMotifCgStagiaire");

  private static Logger LOG = Logger.getLogger(_EOCongeSsTraitement.class);

  public EOCongeSsTraitement localInstanceIn(EOEditingContext editingContext) {
    EOCongeSsTraitement localInstance = (EOCongeSsTraitement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOCongeSsTraitement.LOG.isDebugEnabled()) {
    	_EOCongeSsTraitement.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOCongeSsTraitement.LOG.isDebugEnabled()) {
    	_EOCongeSsTraitement.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOCongeSsTraitement.LOG.isDebugEnabled()) {
    	_EOCongeSsTraitement.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOCongeSsTraitement.LOG.isDebugEnabled()) {
    	_EOCongeSsTraitement.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCongeSsTraitement.LOG.isDebugEnabled()) {
    	_EOCongeSsTraitement.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCongeSsTraitement.LOG.isDebugEnabled()) {
    	_EOCongeSsTraitement.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EOCongeSsTraitement.LOG.isDebugEnabled()) {
    	_EOCongeSsTraitement.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOCongeSsTraitement.LOG.isDebugEnabled()) {
    	_EOCongeSsTraitement.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOAbsence toAbsence() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOAbsence)storedValueForKey("toAbsence");
  }

  public void setToAbsenceRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAbsence value) {
    if (_EOCongeSsTraitement.LOG.isDebugEnabled()) {
      _EOCongeSsTraitement.LOG.debug("updating toAbsence from " + toAbsence() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOAbsence oldValue = toAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAbsence");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toAbsence");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOCongeSsTraitement.LOG.isDebugEnabled()) {
      _EOCongeSsTraitement.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOMotifCgStagiaire toMotifCgStagiaire() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOMotifCgStagiaire)storedValueForKey("toMotifCgStagiaire");
  }

  public void setToMotifCgStagiaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOMotifCgStagiaire value) {
    if (_EOCongeSsTraitement.LOG.isDebugEnabled()) {
      _EOCongeSsTraitement.LOG.debug("updating toMotifCgStagiaire from " + toMotifCgStagiaire() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOMotifCgStagiaire oldValue = toMotifCgStagiaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMotifCgStagiaire");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMotifCgStagiaire");
    }
  }
  

  public static EOCongeSsTraitement create(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
) {
    EOCongeSsTraitement eo = (EOCongeSsTraitement) EOUtilities.createAndInsertInstance(editingContext, _EOCongeSsTraitement.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOCongeSsTraitement> fetchAll(EOEditingContext editingContext) {
    return _EOCongeSsTraitement.fetchAll(editingContext, null);
  }

  public static NSArray<EOCongeSsTraitement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCongeSsTraitement.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOCongeSsTraitement> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOCongeSsTraitement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCongeSsTraitement> eoObjects = (NSArray<EOCongeSsTraitement>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOCongeSsTraitement fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeSsTraitement.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCongeSsTraitement fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCongeSsTraitement> eoObjects = _EOCongeSsTraitement.fetch(editingContext, qualifier, null);
    EOCongeSsTraitement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOCongeSsTraitement)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_CongeSsTraitement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCongeSsTraitement fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCongeSsTraitement.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOCongeSsTraitement fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCongeSsTraitement eoObject = _EOCongeSsTraitement.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_CongeSsTraitement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOCongeSsTraitement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCongeSsTraitement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCongeSsTraitement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCongeSsTraitement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOCongeSsTraitement localInstanceIn(EOEditingContext editingContext, EOCongeSsTraitement eo) {
    EOCongeSsTraitement localInstance = (eo == null) ? null : (EOCongeSsTraitement)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
