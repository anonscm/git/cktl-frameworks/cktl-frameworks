/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndice;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPassageChevron;
import org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation;
import org.cocktail.fwkcktlgrh.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EOElements extends _EOElements {
	
	public static EOSortOrdering SORT_DATE_DEBUT_ASC = new EOSortOrdering(D_EFFET_ELEMENT_KEY, EOSortOrdering.CompareAscending);
	public static EOSortOrdering SORT_DATE_FIN_ASC = new EOSortOrdering(D_FIN_ELEMENT_KEY, EOSortOrdering.CompareAscending);
	public static EOSortOrdering SORT_DATE_DEBUT_DESC = new EOSortOrdering(D_EFFET_ELEMENT_KEY, EOSortOrdering.CompareDescending);
	public static EOSortOrdering SORT_DATE_FIN_DESC = new EOSortOrdering(D_FIN_ELEMENT_KEY, EOSortOrdering.CompareDescending);

	public static NSArray<EOSortOrdering> SORT_ARRAY_DATE_DEBUT_ASC = new NSArray(SORT_DATE_DEBUT_ASC);
	public static NSArray<EOSortOrdering> SORT_ARRAY_DATE_FIN_ASC = new NSArray(SORT_DATE_FIN_ASC);
	public static NSArray<EOSortOrdering> SORT_ARRAY_DATE_DEBUT_DESC = new NSArray(SORT_DATE_DEBUT_DESC);
	public static NSArray<EOSortOrdering> SORT_ARRAY_DATE_FIN_DESC = new NSArray(SORT_DATE_FIN_DESC);

	public static final String TEM_VALIDE_OUI = "O";
	public static final String TEM_VALIDE_NON = "N";


	public EOElements() {
		super();
	}

	/**
	 * Renvoie l element de carriere courant d un individu. (En tenant compte des temoins provisoire et valide.
	 * @param ec
	 * @param individu
	 * @return
	 */
	public  static EOElements elementCourant(EOEditingContext ec, EOIndividu individu) {
		EOQualifier qualifier = 
			ERXQ.and(
					ERXQ.equals(TEM_VALIDE_KEY, "O"),
					ERXQ.equals(TO_CARRIERE.dot(EOCarriere.TEM_VALIDE_KEY).key(),"O"),
					ERXQ.equals(TEM_PROVISOIRE_KEY, "N"),
					ERXQ.equals(TO_INDIVIDU_KEY, individu),
					ERXQ.lessThanOrEqualTo(D_EFFET_ELEMENT_KEY, MyDateCtrl.getDateJour()),
					ERXQ.or(
							ERXQ.isNull(EOElements.D_FIN_ELEMENT_KEY),
							ERXQ.greaterThanOrEqualTo(EOElements.D_FIN_ELEMENT_KEY, MyDateCtrl.getDateJour())
					)
			);

		return fetchFirstByQualifier(ec, qualifier);    	
	}
	
	/**
	 * Renvoie l element de carriere courant d un individu sur la periode. (En tenant compte des temoins provisoire et valide.
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static EOElements elementsurPeriode(EOEditingContext ec, EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin) {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(TEM_VALIDE_KEY, "O"),
						ERXQ.equals(TO_CARRIERE.dot(EOCarriere.TEM_VALIDE_KEY).key(),"O"),
						ERXQ.equals(TEM_PROVISOIRE_KEY, "N"),
						ERXQ.equals(TO_INDIVIDU_KEY, individu),
						ERXQ.lessThanOrEqualTo(D_EFFET_ELEMENT_KEY, dateDebut),
						ERXQ.or(
								ERXQ.isNull(EOElements.D_FIN_ELEMENT_KEY),
								ERXQ.greaterThanOrEqualTo(EOElements.D_FIN_ELEMENT_KEY, dateFin)
								)
						);
		return fetchFirstByQualifier(ec, qualifier);    	
	}

	/**
	 * Renvoie l element de carriere courant d un individu sur la periode. (En tenant compte des temoins provisoire et valide
	 * et type de population enseignant.
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static EOElements elementEnseignantsurPeriode(EOEditingContext ec, EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin) {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(TEM_VALIDE_KEY, "O"),
						ERXQ.equals(TO_CARRIERE.dot(EOCarriere.TEM_VALIDE_KEY).key(), "O"),
						ERXQ.equals(TEM_PROVISOIRE_KEY, "N"),
						ERXQ.equals(TO_INDIVIDU_KEY, individu),
						ERXQ.equals(TO_CORPS.dot(EOCorps.TO_TYPE_POPULATION_KEY).dot(EOTypePopulation.TEM_ENSEIGNANT_KEY).key(), "O"),
						ERXQ.lessThanOrEqualTo(D_EFFET_ELEMENT_KEY, dateDebut),
						ERXQ.or(
								ERXQ.isNull(EOElements.D_FIN_ELEMENT_KEY),
								ERXQ.greaterThanOrEqualTo(EOElements.D_FIN_ELEMENT_KEY, dateFin)
								)
						);
		return fetchFirstByQualifier(ec, qualifier);    	
	}

	
	/**
	 * Renvoie l element de carriere courant d un individu sur la periode. (En tenant compte des temoins provisoire et valide.
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static EOElements elementsurPeriode(EOEditingContext ec, EOCarriere carriere, EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin) {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(TEM_VALIDE_KEY, "O"),
						ERXQ.equals(TO_CARRIERE.dot(EOCarriere.TEM_VALIDE_KEY).key(),"O"),
						ERXQ.equals(TEM_PROVISOIRE_KEY, "N"),
						ERXQ.equals(TO_INDIVIDU_KEY, individu),
						ERXQ.lessThanOrEqualTo(D_EFFET_ELEMENT_KEY, dateDebut),
						ERXQ.or(
								ERXQ.isNull(EOElements.D_FIN_ELEMENT_KEY),
								ERXQ.greaterThanOrEqualTo(EOElements.D_FIN_ELEMENT_KEY, dateFin)
								)
						);
		return fetchFirstByQualifier(ec, qualifier);    	
	}
	
	/**
	 *  renvoie toutes les éléments carrières valides d'un individu sur la periode. (En tenant compte des temoins provisoire et valide.
	 *  et du type de population enseignant
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOElements> elementsEnseignantSurPeriode(EOEditingContext ec, EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin) {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(TEM_VALIDE_KEY, "O"),
						ERXQ.equals(TO_CARRIERE.dot(EOCarriere.TEM_VALIDE_KEY).key(),"O"),
						ERXQ.equals(TEM_PROVISOIRE_KEY, "N"),
						ERXQ.equals(TO_INDIVIDU_KEY, individu),
						ERXQ.equals(TO_CORPS.dot(EOCorps.TO_TYPE_POPULATION_KEY).dot(EOTypePopulation.TEM_ENSEIGNANT_KEY).key(), "O"),						
						ERXQ.lessThanOrEqualTo(D_EFFET_ELEMENT_KEY, dateDebut),
						ERXQ.or(
								ERXQ.isNull(EOElements.D_FIN_ELEMENT_KEY),
								ERXQ.greaterThanOrEqualTo(EOElements.D_FIN_ELEMENT_KEY, dateFin)
								)
						);
		NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(D_EFFET_ELEMENT_KEY).array();
		return fetchAll(ec, qualifier, sorts);	
	}
	
	public static NSArray<EOElements> elementsSurPeriode(EOEditingContext ec, EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin) {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(TEM_VALIDE_KEY, "O"),
						ERXQ.equals(TO_CARRIERE.dot(EOCarriere.TEM_VALIDE_KEY).key(),"O"),
						ERXQ.equals(TEM_PROVISOIRE_KEY, "N"),
						ERXQ.equals(TO_INDIVIDU_KEY, individu),
						ERXQ.lessThanOrEqualTo(D_EFFET_ELEMENT_KEY, dateDebut),
						ERXQ.or(
								ERXQ.isNull(EOElements.D_FIN_ELEMENT_KEY),
								ERXQ.greaterThanOrEqualTo(EOElements.D_FIN_ELEMENT_KEY, dateFin)
								)
						);
		NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(D_EFFET_ELEMENT_KEY).array();
		return fetchAll(ec, qualifier, sorts);	
	}
	/**
	 * renvoie toutes les éléments carrières valides d'un individu
	 * @param ec
	 * @param ind
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOCarriere> eltsCarrieresForIndividu(EOEditingContext ec, EOIndividu ind) {
		if (ind == null) {
			return null;
		}	
		EOQualifier qual = 
			ERXQ.and(
					ERXQ.equals(TEM_VALIDE_KEY, "O"),
					ERXQ.equals(TO_CARRIERE.dot(EOCarriere.TEM_VALIDE_KEY).key(),"O"),
					ERXQ.equals(TEM_PROVISOIRE_KEY, "N"),
					ERXQ.equals(TO_INDIVIDU_KEY, ind) 
			);
		NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(D_EFFET_ELEMENT_KEY).array();
		return fetchAll(ec, qual, sorts);
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}


	//METHODES AJOUTEES
	/**
	 * liste des elements de carriere d'un individu pour une periode donnee
	 * 
	 * @param ec
	 * @param individu
	 * @param date
	 * @return
	 */
	public static NSArray<EOElements> findSortedElementCarriereForIndividuAndDateInContext(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		// String strQual =
		// "toIndividu = %@ AND ((dFinElement = nil) or ((dEffetElement < %@) AND (dFinElement > %@)) ";
		// strQual +=
		// "or ((dFinElement >= %@) AND (dFinElement <= %@)) or ((dEffetElement >= %@) and (dEffetElement <= %@))))";
		// NSArray args = new NSArray(new Object[]{individu, dateDebut, dateFin,
		// dateDebut, dateFin, dateDebut, dateFin});

		String strQual = null;
		NSArray args = null;
		
		if (dateFin != null) {
			strQual =
				"toIndividu = %@ AND temValide = 'O' AND temProvisoire = 'N' "
				+ "AND toCarriere.temValide = 'O' AND ("
				+ "(dEffetElement <= %@ AND dFinElement = nil) OR"
				+ "(dEffetElement >= %@ AND dFinElement <= %@) OR"
				+ "(dEffetElement <= %@ AND dFinElement >= %@) OR"
				+ "(dEffetElement <= %@ AND dFinElement >= %@ AND dFinElement <= %@) OR"
				+ "(dEffetElement >= %@ AND dEffetElement <= %@ AND dFinElement >= %@) OR"
				+ "(dEffetElement >= %@ AND dEffetElement <= %@ AND dFinElement = nil)"
				+ ")";
			args = new NSArray(new Object[] {
					individu,
					dateDebut,
					dateDebut, dateFin,
					dateDebut, dateFin,
					dateDebut, dateDebut, dateFin,
					dateDebut, dateFin, dateFin,
					dateDebut, dateFin });
		} else {
			strQual =
				"toIndividu = %@ AND temValide = 'O' AND temProvisoire = 'N' "
				+ "AND toCarriere.temValide = 'O' AND ("
				+ "(dFinElement = nil) OR"
				+ "(dEffetElement <= %@ AND dFinElement >= %@) OR"
				+ "(dEffetElement >= %@ AND dFinElement >= %@)"
				+ ")";
			args = new NSArray(new Object[] { individu, dateDebut, dateDebut, dateDebut, dateDebut });
		}

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(strQual, args);
		NSArray sortOrderings = new NSArray(EOSortOrdering.sortOrderingWithKey("dFinElement", EOSortOrdering.CompareAscending));
		return GRHUtilities.fetchArray(ec, EOElements.ENTITY_NAME, qual, sortOrderings);
	}
	
	
	public static NSArray<EOElements> findSortedElementCarriereEchelonForIndividuAndDateInContext(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		// String strQual =
		// "toIndividu = %@ AND ((dFinElement = nil) or ((dEffetElement < %@) AND (dFinElement > %@)) ";
		// strQual +=
		// "or ((dFinElement >= %@) AND (dFinElement <= %@)) or ((dEffetElement >= %@) and (dEffetElement <= %@))))";
		// NSArray args = new NSArray(new Object[]{individu, dateDebut, dateFin,
		// dateDebut, dateFin, dateDebut, dateFin});

		String strQual = null;
		NSArray args = null;
		if (dateFin != null) {
			strQual =
				"toIndividu = %@ AND cEchelon <> nil AND temValide = 'O' AND temProvisoire = 'N' "
				+ "AND toCarriere.temValide = 'O' AND ("
				+ "(dEffetElement <= %@ AND dFinElement = nil) OR"
				+ "(dEffetElement >= %@ AND dFinElement <= %@) OR"
				+ "(dEffetElement <= %@ AND dFinElement >= %@) OR"
				+ "(dEffetElement <= %@ AND dFinElement >= %@ AND dFinElement <= %@) OR"
				+ "(dEffetElement >= %@ AND dEffetElement <= %@ AND dFinElement >= %@) OR"
				+ "(dEffetElement >= %@ AND dEffetElement <= %@ AND dFinElement = nil)"
				+ ")";
			args = new NSArray(new Object[] {
					individu,
					dateDebut,
					dateDebut, dateFin,
					dateDebut, dateFin,
					dateDebut, dateDebut, dateFin,
					dateDebut, dateFin, dateFin,
					dateDebut, dateFin });
		} else {
			strQual =
				"toIndividu = %@ AND temValide = 'O' AND temProvisoire = 'N' "
				+ "toCarriere.temValide = 'O' AND ("
				+ "(dFinElement = nil) OR"
				+ "(dEffetElement <= %@ AND dFinElement >= %@) OR"
				+ "(dEffetElement >= %@ AND dFinElement >= %@)"
				+ ")";
			args = new NSArray(new Object[] { individu, dateDebut, dateDebut, dateDebut, dateDebut });
		}

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(strQual, args);
		NSArray sortOrderings = new NSArray(EOSortOrdering.sortOrderingWithKey("dFinElement", EOSortOrdering.CompareAscending));
		return GRHUtilities.fetchArray(ec, EOElements.ENTITY_NAME, qual, sortOrderings);
	}
	
	public String indiceBrut() {

		if (toGrade() == null || cEchelon() == null) {
			return null;
		} else {
			if (cChevron() != null) {
				NSArray passagesChevron = EOPassageChevron.rechercherPassagesChevronOuvertsPourGradeEchelonEtChevron(editingContext(), toGrade().cGrade(), cEchelon(), cChevron(), dFinElement());
				if (passagesChevron.count() > 0) {
					EOGenericRecord passage = (EOGenericRecord) passagesChevron.objectAtIndex(0);
					String indiceBrut = (String) passage.valueForKey(EOPassageEchelon.C_INDICE_BRUT_KEY);
					if (indiceBrut != null) {
						return indiceBrut;
					}
				}
			}
			// On n'a pas de chevron ou on n'a pas trouv√© de passageChevron
			NSArray passagesEchelon = EOPassageEchelon.rechercherPassageEchelonOuvertPourGradeEtEchelon(editingContext(), toGrade().cGrade(), cEchelon(), dFinElement(),false);
			if (passagesEchelon.count() == 0) {
				return null;
			} else {
				EOPassageEchelon passage = (EOPassageEchelon) passagesEchelon.objectAtIndex(0);
				return passage.cIndiceBrut();
			}
		}
	}

	public Integer indiceMajore() {

		String indiceBrut = indiceBrut();
		if (indiceBrut == null) {
			return null;
		} else {
			EOIndice indice = EOIndice.indiceMajorePourIndiceBrutEtDate(editingContext(), indiceBrut, dFinElement());

			if (indice == null) {
				return null;
			} else {
				return indice.cIndiceMajore();
			}
		}

	}

	
	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI) && temProvisoire() != null && temProvisoire().equals ("N");
	}





}
