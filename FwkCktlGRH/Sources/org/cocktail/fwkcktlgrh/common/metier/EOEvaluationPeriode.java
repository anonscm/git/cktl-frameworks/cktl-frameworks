/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IEvaluationPeriode;
import org.cocktail.fwkcktlpersonne.common.metier.EONiveauCompetence;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOEvaluationPeriode extends _EOEvaluationPeriode implements IEvaluationPeriode {
  private static Logger log = Logger.getLogger(EOEvaluationPeriode.class);

  public EOEvaluationPeriode() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }

  
	// methodes rajoutees

  /**
   * Créer une instance de EOEvaluationPeriode avec les champs et relations obligatoires et l'insere dans l'editingContext.
   */
    public static  EOEvaluationPeriode createEOEvaluationPeriode(EOEditingContext editingContext, NSTimestamp dCreation
  , NSTimestamp dModification
  , NSTimestamp epeDDebut
  , NSTimestamp epeDFin
  			) {
      EOEvaluationPeriode eo = (EOEvaluationPeriode) EOUtilities.createAndInsertInstance(editingContext, _EOEvaluationPeriode.ENTITY_NAME);    
  		eo.setDCreation(dCreation);
  		eo.setDModification(dModification);
  		eo.setEpeDDebut(epeDDebut);
  		eo.setEpeDFin(epeDFin);
      return eo;
    }
  
  
  
	/**
	 * Affichage dd1/mm1/yyyy1-dd2/mm2/yyyy2
	 */
	public String strDateDebutDateFin() {
		return DateCtrl.dateToString(epeDDebut()) + "-" + DateCtrl.dateToString(epeDFin());
	}

	private final static String DATE_FORMAT_ANNEE = "%Y";

	/**
	 * Affichage yyyy1/yyyy2
	 */
	public String strAnneeDebutAnneeFin() {
		return DateCtrl.dateToString(epeDDebut(), DATE_FORMAT_ANNEE) + "/" + DateCtrl.dateToString(epeDFin(), DATE_FORMAT_ANNEE);
	}

	public Number epeKey() {
		return (Number) EOUtilities.primaryKeyForObject(editingContext(), this).valueForKey("epeKey");
	}

	/**
	 * Obtenir l'enregistrement <code>EOEvaluationPeriode</code> actuel.
	 * 
	 * @return
	 */
	public static EOEvaluationPeriode getCurrentPeriode(EOEditingContext ec) {
		EOQualifier qualCurrentPeriode = CktlDataBus.newCondition(
					EPE_D_DEBUT_KEY + "<=%@ and " + EPE_D_FIN_KEY + ">=%@", new NSArray(new NSTimestamp[] {
							DateCtrl.now(), DateCtrl.now() }));
		NSArray records = fetchAll(ec, qualCurrentPeriode, null);
		EOEvaluationPeriode result = null;
		if (records.count() > 0) {
			result = (EOEvaluationPeriode) records.lastObject();
		} else {
			// le dernier en date
			records = fetchAll(ec, null, CktlSort.newSort(EPE_D_DEBUT_KEY));
			if (records.count() > 0) {
				result = (EOEvaluationPeriode) records.lastObject();
			}
		}
		return result;
	}

	public static EOEvaluationPeriode getCurrentPeriodeElseLast(EOEditingContext ec) {
		EOEvaluationPeriode evp = getCurrentPeriode(ec);
		if (evp == null) {
			evp = getLastPeriode(ec);
		}
		return evp;
	}
	
	
	/**
	 * Obtenir l'enregistrement <code>EOEvaluationPeriode</code> dernier en date
	 * 
	 * @return
	 */
	public static EOEvaluationPeriode getLastPeriode(EOEditingContext ec) {
		NSArray records = fetchAll(ec, null, CktlSort.newSort(EPE_D_DEBUT_KEY));
		EOEvaluationPeriode result = null;
		if (records.count() > 0) {
			result = (EOEvaluationPeriode) records.lastObject();
		}
		return result;
	}

	/**
	 * L'enregistrement <code>EOEvaluationPeriode</code> qui suit <em>this</em>
	 * 
	 * @return
	 */
	public EOEvaluationPeriode toNextPeriode() {
		NSArray records = fetchAll(editingContext(), null, CktlSort.newSort(EPE_D_DEBUT_KEY));
		EOEvaluationPeriode result = null;
		boolean shouldBeNext = false;
		for (int i = 0; i < records.count(); i++) {
			EOEvaluationPeriode periode = (EOEvaluationPeriode) records.objectAtIndex(i);
			if (shouldBeNext) {
				result = periode;
				break;
			}
			if (periode.epeKey().intValue() == epeKey().intValue()) {
				shouldBeNext = true;
			}
		}
		return result;
	}

	/**
	 * L'enregistrement <code>EOEvaluationPeriode</code> qui precede <em>this</em>
	 * 
	 * @return
	 */
	public EOEvaluationPeriode toPrevPeriode() {
		NSArray records = fetchAll(editingContext(), null, CktlSort.newSort(EPE_D_DEBUT_KEY, CktlSort.Descending));
		EOEvaluationPeriode result = null;
		boolean shouldBeNext = false;
		for (int i = 0; i < records.count(); i++) {
			EOEvaluationPeriode periode = (EOEvaluationPeriode) records.objectAtIndex(i);
			if (shouldBeNext) {
				result = periode;
				break;
			}
			if (periode == this) {
				shouldBeNext = true;
			}
		}
		return result;
	}

	/**
	 * Retrouver une periode d'apres sa clé primaire
	 * 
	 * @param editingContext
	 * @param epeKey
	 * @return
	 */
	public static EOEvaluationPeriode findPeriodeForEpeKeyInContext(
				EOEditingContext editingContext, Number epeKey) {
		NSArray allPeriode = fetchAll(editingContext);
		EOEvaluationPeriode result = null;
		int i = 0;
		while (result == null && i < allPeriode.count()) {
			EOEvaluationPeriode periode = (EOEvaluationPeriode) allPeriode.objectAtIndex(i);
			Number periodeEpeKey = (Number) com.webobjects.eoaccess.EOUtilities.primaryKeyForObject(editingContext, periode).valueForKey("epeKey");
			if (periodeEpeKey != null && periodeEpeKey.intValue() == epeKey.intValue()) {
				result = periode;
			}
			i++;
		}
		return result;
	}

	// la liste des niveaux de competences associés

	private NSArray<EONiveauCompetence> niveauCompetenceList;

	/**
	 * 
	 * @return
	 */
	public NSArray<EONiveauCompetence> niveauCompetenceList() {
		if (niveauCompetenceList == null) {
			niveauCompetenceList = getNiveauCompetenceForPeriode(this);
		}
		return niveauCompetenceList;
	}

	/**
	 * Creation d'un enregistrement avec une date de debut et une date de fin
	 * definie
	 * 
	 * @param ec
	 * @param dDebut
	 * @param dFin
	 * @return
	 */
	public static EOEvaluationPeriode createEvaluationPeriode(EOEditingContext ec, NSTimestamp dDebut, NSTimestamp dFin) {
		EOEvaluationPeriode newRecord = EOEvaluationPeriode.createEOEvaluationPeriode(
					ec, DateCtrl.now(), DateCtrl.now(), dDebut, dFin);
		return newRecord;
	}

	// ajouts

	/**
	 * Donne la liste des {@link EONiveauCompetence} potentiels utilisable pour la
	 * periode passée en paramètre. On fait un croisement avec les dates de
	 * validités.
	 */
	public  NSArray getNiveauCompetenceForPeriode(EOEvaluationPeriode periode) {
		NSArray result = null;

		//
		EOQualifier qual = CktlDataBus.newCondition(
				"(" + EONiveauCompetence.D_DEB_VAL_KEY + "<=%@ and (" + EONiveauCompetence.D_FIN_VAL_KEY + ">=%@ or " + EONiveauCompetence.D_FIN_VAL_KEY + "=nil )) or " +
						"(" + EONiveauCompetence.D_DEB_VAL_KEY + "<=%@ and (" + EONiveauCompetence.D_FIN_VAL_KEY + ">=%@ or "
						+ EONiveauCompetence.D_FIN_VAL_KEY + "=nil )) or " + "(" + EONiveauCompetence.D_DEB_VAL_KEY + ">=%@ and " + EONiveauCompetence.D_FIN_VAL_KEY + "<=%@ )",
				new NSArray(new NSTimestamp[] {
						periode.epeDDebut(), periode.epeDDebut(),
						periode.epeDFin(), periode.epeDFin(),
						periode.epeDDebut(), periode.epeDFin() }));

		result = EONiveauCompetence.fetchAll(
				periode.editingContext(), qual, CktlSort.newSort(EONiveauCompetence.NCP_POSITION_KEY));

		return result;
	}
}
