/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOFeveParametres extends _EOFeveParametres {
	private static Logger log = Logger.getLogger(EOFeveParametres.class);

  	public static final String KEY_FEV_EVALUATION_SAISIE_D_DEBUT = "FEV_EVALUATION_SAISIE_D_DEBUT";
	public static final String KEY_FEV_EVALUATION_SAISIE_D_FIN = "FEV_EVALUATION_SAISIE_D_FIN";
	public static final String KEY_FEV_FICHE_LOLF_SAISIE_D_DEBUT = "FEV_FICHE_LOLF_SAISIE_D_DEBUT";
	public static final String KEY_FEV_FICHE_LOLF_SAISIE_D_FIN = "FEV_FICHE_LOLF_SAISIE_D_FIN";

	// gestion des activites autres dans la fiche de poste O/N
	public static final String KEY_FEV_FICHE_DE_POSTE_SAISIE_ACTIVITES_AUTRES = "FEV_FICHE_DE_POSTE_SAISIE_ACTIVITES_AUTRES";
	// gestion des competences autres dans la fiche de poste O/N
	public static final String KEY_FEV_FICHE_DE_POSTE_SAISIE_COMPETENCES_AUTRES = "FEV_FICHE_DE_POSTE_SAISIE_COMPETENCES_AUTRES";
	// edition de la fiche de poste : disposition des 2 blocs activités
	// compétences REFERENS
	public static final String KEY_FEV_EDITION_FICHE_DE_POSTE_DISPOSITION_ACT_COMP_REFERENS_VERTICAL = "FEV_EDITION_FICHE_DE_POSTE_DISPOSITION_ACT_COMP_REFERENS_VERTICAL";
	// motif du nom par défaut d'un nouveau poste
	public static final String KEY_FEV_LIBELLE_CREATION_POSTE_VALEUR_PAR_DEFAUT = "LIBELLE_CREATION_POSTE_VALEUR_PAR_DEFAUT";

	// Durée minimum cumulées des affectations d''un agent pour pouvoir être
	// évalué sur une période (en jour)
	public static final String KEY_FEV_DUREE_MINIMUM_AFFECTATION_POUR_EVALUATION = "FEV_DUREE_MINIMUM_AFFECTATION_POUR_EVALUATION";
	public static final String KEY_FEV_IS_VISA_DIRECTEUR_FICHE_DE_POSTE = "KEY_FEV_IS_VISA_DIRECTEUR_FICHE_DE_POSTE";
	
	public static final String KEY_FEV_IS_LIBELLE_SIGNATURE_AGENT_FICHE_DE_POSTE= "FEV_IS_LIBELLE_SIGNATURE_AGENT_FICHE_DE_POSTE";
	public static final String KEY_FEV_IS_LIBELLE_SIGNATURE_SUPP_AGENT_FICHE_DE_POSTE= "FEV_IS_LIBELLE_SIGNATURE_SUPP_AGENT_FICHE_DE_POSTE";


	// documentations (0 à n documentations)
	public static final String PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_UTILISATION = "FEV_DOCUMENTATION_GUIDE_UTILISATION_";
	public static final String PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_ENTRETIEN = "FEV_DOCUMENTATION_GUIDE_ENTRETIEN_";

	// autoriser la modification des formations suivies dans la fiche d'évaluation
	public static final String KEY_FEV_MODIFICATION_FORMATIONS_SUIVIES_DANS_FEVE = "FEV_MODIFICATION_FORMATIONS_SUIVIES_DANS_FEVE";

	/** la liste des cles retournant une valeur booleenne */
	public static final NSArray<String> ARRAY_KEY_BOOLEAN = new NSArray<String>(new String[] {
			KEY_FEV_FICHE_DE_POSTE_SAISIE_ACTIVITES_AUTRES,
			KEY_FEV_FICHE_DE_POSTE_SAISIE_COMPETENCES_AUTRES,
			KEY_FEV_EDITION_FICHE_DE_POSTE_DISPOSITION_ACT_COMP_REFERENS_VERTICAL,
			KEY_FEV_MODIFICATION_FORMATIONS_SUIVIES_DANS_FEVE
	});



	/**
	 * les cles du dictionnaire produit par la methode
	 * {@link #getDicoTypeAccesCibleForParamKey(String)}
	 */
	public static final String DICO_KEY_TYPE_DROIT_ACCES = "DICO_KEY_TYPE_DROIT_ACCES";
	public static final String DICO_KEY_TYPE_DROIT_CIBLE = "DICO_KEY_TYPE_DROIT_CIBLE";
  
  
  
  
	public static EOFeveParametres createEOFeveParametres(EOEditingContext editingContext, String paramKey) {
		return create(editingContext, paramKey);
	}
  
	public EOFeveParametres() {
		super();
	}

  
  
	/**
	 * Classe maison permettant la transalation d'un objet
	 * {@link EOFeveParametres} de documentation en objet gérable simplement par
	 * l'interface. Le format suivant est utilisé : [position]'|'[libelle du
	 * document]'|'[url fichier]
	 * 
	 * @author ctarade
	 */
	public class ObjetInterfaceDocumentation {

		private final static String SEPARATEUR = "|";

		private int position;
		private String libelle;
		private String url;

		private boolean isEnModification;

		/** le paramKey prefix */
		private String baseParamKey;

		public ObjetInterfaceDocumentation() {
			super();
			toItem();
			isEnModification = false;
		}

		/**
		 * 
		 */
		private void toItem() {
			if (!StringCtrl.isEmpty(paramValue())) {
				NSArray<String> tokens = NSArray.componentsSeparatedByString(paramValue(), SEPARATEUR);
				setLibelle(tokens.objectAtIndex(0));
				setUrl(tokens.objectAtIndex(1));
				String strPosition = paramKey();
				strPosition = strPosition.substring(strPosition.lastIndexOf("_") + 1);
				if (strPosition.startsWith("0")) {
					strPosition = strPosition.substring(1);
				}
				setPosition(Integer.parseInt(strPosition));
				setBaseParamKey(paramKey().substring(0, paramKey().lastIndexOf("_") + 1));
			}
		}
		
		
		public final String getLibelle() {
			return libelle;
		}

		public final void setLibelle(String libelle) {
			this.libelle = libelle;
		}

		public final String getUrl() {
			return url;
		}

		public final void setUrl(String url) {
			this.url = url;
		}

		public final int getPosition() {
			return position;
		}

		public final void setPosition(int position) {
			this.position = position;
		}
		
		public void majObjetMetier() {
			if (getPosition() < 10) {
				setParamKey(getBaseParamKey() + "0" + getPosition());
			} else {
				setParamKey(getBaseParamKey() + getPosition());
			}
			setParamValue(getLibelle() + SEPARATEUR + getUrl());
		}

		public final boolean isEnModification() {
			return isEnModification;
		}

		public final void setEnModification(boolean isEnModification) {
			this.isEnModification = isEnModification;
		}

		public WOActionResults modifier() {
			setEnModification(true);
			return null;
		}

		
		/**
		 * Validation du parametre
		 * 
		 * @return
		 * @throws NSValidation.ValidationException
		 */
		private void valider()
				throws NSValidation.ValidationException {
			// il faut un libelle
			if (StringCtrl.isEmpty(getLibelle())) {
				throw new NSValidation.ValidationException(
						"Le libellé est obligatoire");
			}
			// et une URL
			if (StringCtrl.isEmpty(getUrl())) {
				throw new NSValidation.ValidationException(
						"L'URL est obligatoire");
			}
			// tester que la taille totale ne dépasse pas la valeur maximum autorisée
			int depassement = depassement(PARAM_VALUE_KEY, paramValue());
			if (depassement > 0) {
				throw new NSValidation.ValidationException(
						"La taille totale du libellé et de l'URL ne doit pas dépasser " + maxLengthForAttribute(PARAM_VALUE_KEY) +
						" caractères. Votre saisie dépasse de " + depassement + " caractères. Veuillez adapter la saisie " +
						"pour continuer");
			}
		}

		/**
		 * Enregistrement dans la base de données
		 */
		public void sauvegarder() {
			clearError();
			try {
				majObjetMetier();
				valider();
				GRHUtilities.save(editingContext(), "");
				setEnModification(false);
			} catch (NSValidation.ValidationException va) {
				setErrorMessage(va.getMessage());
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

		public WOActionResults annuler() {
			clearError();
			setEnModification(false);
			toItem();
			editingContext().revert();
			return null;
		}

		public final String getBaseParamKey() {
			return baseParamKey;
		}

		public final void setBaseParamKey(String baseParamKey) {
			this.baseParamKey = baseParamKey;
		}
		
		
		private String errorMessage;

		private void clearError() {
			setErrorMessage(null);
		}

		public final String getErrorMessage() {
			return errorMessage;
		}

		public final void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}

		public boolean isError() {
			boolean isError = false;

			if (!StringCtrl.isEmpty(getErrorMessage())) {
				isError = true;
			}

			return isError;
		}
	}

	private ObjetInterfaceDocumentation objetInterfaceDocumentation;

	public ObjetInterfaceDocumentation getObjetInterfaceDocumentation() {
		if (objetInterfaceDocumentation == null) {
			objetInterfaceDocumentation = new ObjetInterfaceDocumentation();
		}
		return objetInterfaceDocumentation;
	}

	
	
	/**
	 * Création d'un paramètre
	 * 
	 * @param ec
	 * @param paramKey
	 * @return
	 * @throws Exception
	 */
	public final static EOFeveParametres nouveauParametre(
			EOEditingContext editingContext, String paramKey) throws Exception {

		// on autorise uniquement les documentations pour l'instant ...
		if (!paramKey.startsWith(PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_ENTRETIEN) &&
				!paramKey.startsWith(PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_UTILISATION)) {
			throw new Exception("Impossible de créer des paramètres autres que " +
					PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_ENTRETIEN + " et " + PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_ENTRETIEN);
		}

		// determiner la premiere position libre
		NSArray<EOFeveParametres> parametres = getParametres(editingContext, paramKey, null);
		int position = 0;
		int i = 0;
		while (position == 0 && i < parametres.count() - 1) {
			int positionAttendue = i + 1;
			EOFeveParametres eoFeveParametres = parametres.objectAtIndex(i);
			if (eoFeveParametres.getObjetInterfaceDocumentation().getPosition() != positionAttendue) {
				position = positionAttendue;
			}
			i++;
		}

		// non trouvé, on prend le numéro libre à suivre
		if (position == 0) {
			position = parametres.count() + 1;
		}
		String libelle = "nom de la documentation";
		String url = "http://chemin.de/la/documentation.pdf";

		EOFeveParametres eoFeveParametres = createEOFeveParametres(editingContext, paramKey);
		eoFeveParametres.getObjetInterfaceDocumentation().setPosition(position);
		eoFeveParametres.getObjetInterfaceDocumentation().setLibelle(libelle);
		eoFeveParametres.getObjetInterfaceDocumentation().setUrl(url);
		eoFeveParametres.getObjetInterfaceDocumentation().setBaseParamKey(paramKey);
		eoFeveParametres.getObjetInterfaceDocumentation().majObjetMetier();
		eoFeveParametres.getObjetInterfaceDocumentation().setEnModification(true);

		return eoFeveParametres;
	}


	/**
	 * Suppression d'un enregistrement (gestion du décalage des positions)
	 * 
	 * @param eoFeveParametres
	 */
	public final static void supprimerParametre(
			EOFeveParametres eoFeveParametres) {

		// trouver la position actuelle de l'objet
		int position = eoFeveParametres.getObjetInterfaceDocumentation().getPosition();
		String baseParamKey = eoFeveParametres.getObjetInterfaceDocumentation().getBaseParamKey();

		EOEditingContext ec = eoFeveParametres.editingContext();

		// suppression
		eoFeveParametres.editingContext().deleteObject(eoFeveParametres);

		// sauvegarde obligatoire sinon pb de contrainte unique sur paramKey
		try {
			GRHUtilities.save(ec, "");
		} catch (Throwable e) {
			ec.revert();
			e.printStackTrace();
			return;
		}

		// décalage des objets suivants
		NSArray<EOFeveParametres> parametres = getParametres(ec, baseParamKey, null);
		for (int i = 0; i < parametres.count(); i++) {
			EOFeveParametres parametre = parametres.objectAtIndex(i);
			int positionCourante = parametre.getObjetInterfaceDocumentation().getPosition();
			if (positionCourante > position) {
				parametre.getObjetInterfaceDocumentation().setPosition(positionCourante - 1);
				parametre.getObjetInterfaceDocumentation().majObjetMetier();
			}
		}

	}
  
  
	/**
	 * Liste de paramètres répondant à une clé. La recheche se fait aussi dans le
	 * {@link EOEditingContext} si jamais l'objet n'est pas encore en base de
	 * données.
	 * 
	 * @param editingContext
	 * @param paramKeyPrefix
	 * @return
	 */
	public final static NSMutableArray<EOFeveParametres> getParametres(
			EOEditingContext editingContext, String paramKeyPrefix, String sortKeys) {
		NSMutableArray<EOFeveParametres> array = new NSMutableArray<EOFeveParametres>();

		CktlSort sort = null;
		if (!StringCtrl.isEmpty(sortKeys)) {
			sort = CktlSort.newSort(sortKeys);
		}
		// les objets de la base
		array.addObjectsFromArray(
				EOFeveParametres.fetchAll(
						editingContext,
						ERXQ.startsWith(
								EOFeveParametres.PARAM_KEY_KEY,
								paramKeyPrefix),
								sort));

		// suppression d'éventuels doublons
		NSArrayCtrl.removeDuplicatesInNSArray(array);

		return array;
	}
  
  

	/**
	 * Mettre à jour la position des objets metiers en rapport avec la position
	 * dans le tableau. Il faut faire un traitement spécial car il y a une
	 * contrainte unique sur paramKey qui fait qu'il faut faire un update
	 * séquentiel
	 */
	public final static void majPosition(
			EOEditingContext ec,
			NSArray<EOFeveParametres> eoFeveParametresArray) {
		int position1 = -1;
		int position2 = -1;
		EOFeveParametres eoFeveParametres1 = null;
		EOFeveParametres eoFeveParametres2 = null;
		for (int i = 0; i < eoFeveParametresArray.count(); i++) {
			EOFeveParametres eoFeveParametres = eoFeveParametresArray.objectAtIndex(i);
			int nouvellePosition = eoFeveParametresArray.indexOfIdenticalObject(eoFeveParametres) + 1;
			if (eoFeveParametres.getObjetInterfaceDocumentation().getPosition() != nouvellePosition) {
				if (eoFeveParametres1 == null) {
					// le premier
					eoFeveParametres1 = eoFeveParametres;
					position1 = eoFeveParametres1.getObjetInterfaceDocumentation().getPosition();
				} else {
					// le second
					eoFeveParametres2 = eoFeveParametres;
					position2 = eoFeveParametres2.getObjetInterfaceDocumentation().getPosition();
				}
			}
		}
		// permutation en passant par une sauvegarde intermédiare
		if (eoFeveParametres1 != null && eoFeveParametres2 != null) {
			eoFeveParametres1.getObjetInterfaceDocumentation().setPosition(0);
			eoFeveParametres1.getObjetInterfaceDocumentation().sauvegarder();
			eoFeveParametres2.getObjetInterfaceDocumentation().setPosition(position1);
			eoFeveParametres2.getObjetInterfaceDocumentation().sauvegarder();
			eoFeveParametres1.getObjetInterfaceDocumentation().setPosition(position2);
			eoFeveParametres1.getObjetInterfaceDocumentation().sauvegarder();
		}
	}
  
  
  
  
  
  
  
  
  
  /*---------------------------------------------------------------------------------------------------------------------------*/
/**
 * 
 * @throws NSValidation.ValidationException
 */
public void validateForInsert() throws NSValidation.ValidationException {
    this.validateObjectMetier();
    validateBeforeTransactionSave();
    super.validateForInsert();
}

/**
 * 
 * @throws NSValidation.ValidationException
 */
public void validateForUpdate() throws NSValidation.ValidationException {
    this.validateObjectMetier();
    validateBeforeTransactionSave();
    super.validateForUpdate();
}

/**
 * @throws NSValidation.ValidationException
 */
public void validateForDelete() throws NSValidation.ValidationException {
    super.validateForDelete();
}



/**
 * Peut etre appele à partir des factories.
 * @throws NSValidation.ValidationException
 */
public void validateObjectMetier() throws NSValidation.ValidationException {
	
}

/**
 * A appeler par les validateforsave, forinsert, forupdate.
 *
 */
public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
	
}

}
