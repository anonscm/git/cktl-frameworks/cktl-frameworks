/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOStage.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOStage extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Stage";

	// Attributes
	public static final String C_CORPS_KEY = "cCorps";
	public static final String DATE_TITULARISATION_KEY = "dateTitularisation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_STAGE_KEY = "dDebStage";
	public static final String D_FIN_STAGE_KEY = "dFinStage";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String TEM_RENOUVELLEMENT_KEY = "temRenouvellement";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> C_CORPS = new ERXKey<String>("cCorps");
	public static final ERXKey<NSTimestamp> DATE_TITULARISATION = new ERXKey<NSTimestamp>("dateTitularisation");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_STAGE = new ERXKey<NSTimestamp>("dDebStage");
	public static final ERXKey<NSTimestamp> D_FIN_STAGE = new ERXKey<NSTimestamp>("dFinStage");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> NO_DOSSIER_PERS = new ERXKey<Integer>("noDossierPers");
	public static final ERXKey<Integer> NO_SEQ_CARRIERE = new ERXKey<Integer>("noSeqCarriere");
	public static final ERXKey<String> TEM_RENOUVELLEMENT = new ERXKey<String>("temRenouvellement");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_CORPS_KEY = "toCorps";
	public static final String TO_INDIVIDU_KEY = "toIndividu";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> TO_CORPS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps>("toCorps");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");

  private static Logger LOG = Logger.getLogger(_EOStage.class);

  public EOStage localInstanceIn(EOEditingContext editingContext) {
    EOStage localInstance = (EOStage)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cCorps() {
    return (String) storedValueForKey("cCorps");
  }

  public void setCCorps(String value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating cCorps from " + cCorps() + " to " + value);
    }
    takeStoredValueForKey(value, "cCorps");
  }

  public NSTimestamp dateTitularisation() {
    return (NSTimestamp) storedValueForKey("dateTitularisation");
  }

  public void setDateTitularisation(NSTimestamp value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating dateTitularisation from " + dateTitularisation() + " to " + value);
    }
    takeStoredValueForKey(value, "dateTitularisation");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebStage() {
    return (NSTimestamp) storedValueForKey("dDebStage");
  }

  public void setDDebStage(NSTimestamp value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating dDebStage from " + dDebStage() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebStage");
  }

  public NSTimestamp dFinStage() {
    return (NSTimestamp) storedValueForKey("dFinStage");
  }

  public void setDFinStage(NSTimestamp value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating dFinStage from " + dFinStage() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinStage");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey("noDossierPers");
  }

  public void setNoDossierPers(Integer value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating noDossierPers from " + noDossierPers() + " to " + value);
    }
    takeStoredValueForKey(value, "noDossierPers");
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey("noSeqCarriere");
  }

  public void setNoSeqCarriere(Integer value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating noSeqCarriere from " + noSeqCarriere() + " to " + value);
    }
    takeStoredValueForKey(value, "noSeqCarriere");
  }

  public String temRenouvellement() {
    return (String) storedValueForKey("temRenouvellement");
  }

  public void setTemRenouvellement(String value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating temRenouvellement from " + temRenouvellement() + " to " + value);
    }
    takeStoredValueForKey(value, "temRenouvellement");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOStage.LOG.isDebugEnabled()) {
    	_EOStage.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCorps toCorps() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCorps)storedValueForKey("toCorps");
  }

  public void setToCorpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCorps value) {
    if (_EOStage.LOG.isDebugEnabled()) {
      _EOStage.LOG.debug("updating toCorps from " + toCorps() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCorps oldValue = toCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCorps");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCorps");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOStage.LOG.isDebugEnabled()) {
      _EOStage.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  

  public static EOStage create(EOEditingContext editingContext, String cCorps
, NSTimestamp dateTitularisation
, NSTimestamp dCreation
, NSTimestamp dDebStage
, NSTimestamp dModification
, Integer noDossierPers
, Integer noSeqCarriere
, String temRenouvellement
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOCorps toCorps, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu) {
    EOStage eo = (EOStage) EOUtilities.createAndInsertInstance(editingContext, _EOStage.ENTITY_NAME);    
		eo.setCCorps(cCorps);
		eo.setDateTitularisation(dateTitularisation);
		eo.setDCreation(dCreation);
		eo.setDDebStage(dDebStage);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setNoSeqCarriere(noSeqCarriere);
		eo.setTemRenouvellement(temRenouvellement);
		eo.setTemValide(temValide);
    eo.setToCorpsRelationship(toCorps);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  public static NSArray<EOStage> fetchAll(EOEditingContext editingContext) {
    return _EOStage.fetchAll(editingContext, null);
  }

  public static NSArray<EOStage> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOStage.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOStage> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOStage.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOStage> eoObjects = (NSArray<EOStage>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOStage fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStage.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStage fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOStage> eoObjects = _EOStage.fetch(editingContext, qualifier, null);
    EOStage eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOStage)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Stage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStage fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStage.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOStage fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOStage eoObject = _EOStage.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Stage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOStage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOStage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOStage eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOStage)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOStage localInstanceIn(EOEditingContext editingContext, EOStage eo) {
    EOStage localInstance = (eo == null) ? null : (EOStage)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
