package org.cocktail.fwkcktlgrh.common.metier.finder;

import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

public class CarriereFinder {


	private EOEditingContext edc;

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}
	
	public EOEditingContext edc() {
		return edc;
	}

	public NSArray<EOElements> elementsDeCarrierePourIndividusEtDate(NSArray<EOIndividu> individus, NSTimestamp date) {
		
		NSArray<EOElements> elements;
		
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.lessThanOrEqualTo(EOElements.D_EFFET_ELEMENT_KEY, date),
						ERXQ.or(
								ERXQ.isNull(EOElements.D_FIN_ELEMENT_KEY),
								ERXQ.greaterThanOrEqualTo(EOElements.D_FIN_ELEMENT_KEY, date)
							),
						ERXQ.in(EOElements.TO_INDIVIDU_KEY, individus),
						ERXQ.equals(EOElements.TEM_VALIDE_KEY, EOElements.TEM_VALIDE_OUI)
					);
		
		NSMutableArray<String> prefetchingKeyPaths = new NSMutableArray<String>();
		prefetchingKeyPaths.add(EOElements.TO_GRADE_KEY);
		prefetchingKeyPaths.add(EOElements.TO_INDIVIDU_KEY);

		ERXFetchSpecification<EOElements> fetchSpecification = new ERXFetchSpecification<EOElements>(EOElements.ENTITY_NAME, qualifier, null);
		fetchSpecification.setPrefetchingRelationshipKeyPaths(prefetchingKeyPaths);
		
		elements = fetchSpecification.fetchObjects(edc());		
		
		return elements;
	}
	
}
