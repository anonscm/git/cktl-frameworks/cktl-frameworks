package org.cocktail.fwkcktlgrh.common.metier.finder;

import java.util.List;

import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.metier.EOVacatairesAffectation;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacatairesAffectation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * Classe de recherche des affectations d'une vacation
 * 
 * @author Chama LAATIK
 */
public final class VacatairesAffectationFinder {

	/** L'instance singleton de cette classe */
	private static VacatairesAffectationFinder sharedInstance = new VacatairesAffectationFinder();

	private VacatairesAffectationFinder() {
		// Pas d'instanciation possible
	}

	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre
	 * classe
	 * 
	 * Ex : VacatairesAffectationFinder.sharedInstance().methode
	 * 
	 * @return une instance de la classe
	 */
	public static VacatairesAffectationFinder sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new VacatairesAffectationFinder();
		}
		return sharedInstance;
	}

	/**
	 * @param edc : editingContext
	 * @param vacation : la vacation voulue
	 * @return Liste des affectations pour une vacation donnée
	 */
	@SuppressWarnings("unchecked")
	public List<IVacatairesAffectation> findForVacation(EOEditingContext edc, IVacataires vacation) {
		EOQualifier qualifier = ERXQ.equals(EOVacatairesAffectation.TO_VACATAIRE_KEY, vacation);

		return EOVacatairesAffectation.fetchAll(edc, qualifier, null);
	}
	
	/**
	 * @param edc : editingContext
	 * @param vacation : la vacation voulue
	 * @return Affectation principale pour une vacation donnée
	 */
	public IVacatairesAffectation findAffectationPrincipale(EOEditingContext edc, IVacataires vacation) {
		EOQualifier qualifier = 
				ERXQ.and(
					ERXQ.equals(EOVacatairesAffectation.TEMOIN_PRINCIPALE_KEY, Constantes.VRAI),
					ERXQ.equals(EOVacatairesAffectation.TO_VACATAIRE_KEY, vacation)
				);

		return EOVacatairesAffectation.fetchFirstByQualifier(edc, qualifier, null);
	}

}
