package org.cocktail.fwkcktlgrh.common.metier.finder;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * @author ctarade
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Finder {

    /** 
     * execute la requette sql et recupere les valeurs des colonnes demandees dans le tableau 
     */
    public static NSArray rawRowsForSQL(EOEditingContext ec, String eomodelName, String query) {
      NSArray rawRowsForSQL = null;
      try {
        rawRowsForSQL = EOUtilities.rawRowsForSQL(ec, eomodelName, query, null);
      } catch (Throwable th) {
        th.printStackTrace();
        rawRowsForSQL = new NSArray();
      }
      return rawRowsForSQL;
    } 

}
