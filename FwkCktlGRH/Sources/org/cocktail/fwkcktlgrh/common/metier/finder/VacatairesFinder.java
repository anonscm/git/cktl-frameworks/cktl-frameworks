package org.cocktail.fwkcktlgrh.common.metier.finder;

import java.util.List;

import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.EOVacataires;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Classe de recherche des vacations
 * 
 * @author Chama LAATIK
 */
public final class VacatairesFinder {

	/** L'instance singleton de cette classe */
	private static VacatairesFinder sharedInstance = new VacatairesFinder();

	private VacatairesFinder() {
		// Pas d'instanciation possible
	}

	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : VacatairesFinder.sharedInstance().methode
	 * 
	 * @return une instance de la classe
	 */
	public static VacatairesFinder sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new VacatairesFinder();
		}
		return sharedInstance;
	}
	
	/**
	 * retourne le qualifier pour déterminer les vacations sur une période
	 * 
	 * @param debutPeriode : début de la vacation
	 * @param finPeriode : fin de période
	 * @return qualifier de la période
	 */
	public EOQualifier qualifierPourPeriode(NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSTimestamp fin = null;
		if (finPeriode != null) {
			fin = finPeriode;
		}
		return GRHUtilities.qualifierPourPeriode(EOVacataires.DATE_DEBUT_KEY, debutPeriode, EOVacataires.DATE_FIN_KEY, fin);
	}
	
	/**
	 * @param edc : editingContext
	 * @param individu : vacataire concerné
	 * @param debutPeriode : Début de la période
	 * @param finPeriode : Fin de la période
	 * @return liste des vacations valides pour un individu et une période donné
	 */
	@SuppressWarnings("unchecked")
	public List<IVacataires> getListeVacationsValidesPourIndividuEtPeriode(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(EOVacataires.TEMOIN_VALIDE_KEY, Constantes.VRAI),
						ERXQ.equals(ERXQ.keyPath(EOVacataires.TO_INDIVIDU_VACATAIRE_KEY, EOIndividu.NO_INDIVIDU_KEY), individu.noIndividu()),
						qualifierPourPeriode(debutPeriode, finPeriode)
				);
		
		return EOVacataires.fetchAll(edc, qualifier, getSortDateDebutDesc());
	}
	
	/**
	 * @param edc : editingContext
	 * @param individu : vacataire concerné
	 * @param debutPeriode : Début de la période
	 * @param finPeriode : Fin de la période
	 * @return liste des vacations de type 'Enseignant' valides pour un individu et une période donné
	 */
	@SuppressWarnings("unchecked")
	public List<IVacataires> getListeVacationsTypeEnseignantPourIndividuEtPeriode(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(EOVacataires.TEMOIN_VALIDE_KEY, Constantes.VRAI),
						ERXQ.equals(EOVacataires.TEMOIN_ENSEIGNANT_KEY, Constantes.VRAI),
						ERXQ.equals(ERXQ.keyPath(EOVacataires.TO_INDIVIDU_VACATAIRE_KEY, EOIndividu.NO_INDIVIDU_KEY), individu.noIndividu()),
						qualifierPourPeriode(debutPeriode, finPeriode)
				);
		
		return EOVacataires.fetchAll(edc, qualifier, getSortDateDebutDesc());
	}
	
	/**
	 * @return tri descendant selon la date de début
	 */
	public NSArray<EOSortOrdering> getSortDateDebutDesc() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOVacataires.DATE_DEBUT_KEY, 
				EOSortOrdering.CompareDescending));
	}
}
