package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import java.util.Date;

import org.cocktail.fwkcktlgrh.common.metier._EOPasse;



/**
 * definit le contrat d'un Objet Passé
 */
public interface IPasse {

	// Getters appelant une valeur au travers d'une relation
	/**
	 * @return le N° de dossier de l'agent
	 */
	Integer noDossierPers();
	
	/**
	 * 
	 * @return le libellé de la catégorie
	 */
	String libelleCategorie();
	
	/**
	 * 
	 * @return le libellé du type de population
	 */
	String libelleTypePopulation();

		  
	
	// Getters issus de _EOPasse
	/**
	 * @return la date de début de cet élément du passé de l'agent
	 */
	Date dateDebut();

	/**
	 * @return la date de fin de cet élément du passé de l'agent
	 */
	Date dateFin(); 


	/**
	 * @return la durée validée en année de cet élément du passé de l'agent
	 */
	Integer dureeValideeAnnees();

	/**
	 * @return la durée validée en jour de cet élément du passé de l'agent
	 */
	Integer dureeValideeJours();


	/**
	 * @return la durée validée en mois de cet élément du passé de l'agent
	 */
	Integer dureeValideeMois();


	/**
	 * @return la date de validation pour le service de cet élément du passé de l'agent
	 */
	Date dValidationService();


	/**
	 * @return la nom de l'établissement d'accueil de cet élément du passé de l'agent
	 */
	String etablissementPasse();

	/**
	 * @return la  exercée de cet élément du passé de l'agent
	 */
	String fonctionPasse();

	/**
	 * @return le ministère auquel était rattaché cet élément du passé de l'agent
	 */
	String pasMinistere();


	/**
	 * @return le témoin PC acquittée pour cet élément du passé de l'agent
	 */
	String pasPcAcquitee();

	/**
	 * @return la quotité de travail pour la période de cet élément du passé de l'agent
	 */
	java.math.BigDecimal pasQuotite();

	/**
	 * @return la quotité de cotisation lors du service de cet élément du passé de l'agent
	 */
	java.math.BigDecimal pasQuotiteCotisation();

	/**
	 * @return le témoin de type de fonction du service public pour cet élément du passé de l'agent
	 */
	String pasTypeFctPublique();

	/**
	 * @return le témoin de type de service pour cet élément du passé de l'agent
	 */
	String pasTypeService();


	/**
	 * @return le témoin de type de temps pour le service pour cet élément du passé de l'agent
	 */
	String pasTypeTemps();

	/**
	 * @return le témoin de type de secteur (public,privé) pour cet élément du passé de l'agent
	 */
	String secteurPublic();

	/**
	 * @return le témoin titulaire pour cet élément du passé de l'agent
	 */
	String temTitulaire();

	/**
	 * @return le témoin de validité pour cet élément du passé de l'agent
	 */
	String temValide();

	
}
