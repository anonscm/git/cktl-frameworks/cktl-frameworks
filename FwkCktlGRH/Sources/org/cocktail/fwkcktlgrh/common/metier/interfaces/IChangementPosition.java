package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import java.util.Date;



/**
 * definit le contrat d'un Changement de Position
 */
public interface IChangementPosition {

	// Getters appelant une valeur au travers d'une relation
	/**
	 * @return le N° de dossier de l'agent
	 */
	Integer noDossierPers();
	
	/**
	 * 
	 * @return le libellé de la position
	 */
	String libellePosition();
	
	/**
	 * 
	 * @return libellé court de l'établissement où la position est exercée
	 */
	String lcEtabPosition();
	
	/**
	 * 
	 * @return libellé court de l'établissement chargé de gérer un agent détaché
	 */
	String lcEtabOrigPosition();

		  
	
	// Getters issus de _EOChangementPosition
	/**
	 * 
	 * @return la date d'arrête du changement de position
	 */
	Date dArretePosition();


	/**
	 * 
	 * @return la date de début de cette position
	 */
	Date dDebPosition();

	/**
	 * 
	 * @return la date de fin de cette position
	 */
	Date dFinPosition();


	/**
	 * 
	 * @return Etablissement, lieu d'accueil d'un agent exercant son activité hors de l'éducation nationale.
	 */
	  String lieuPosition();

	  /**
	   * 
	   * @return Etablissement (hors education nationale) charge de gerer la carriere d'origine d'un agent detache.
	   */
	  String lieuPositionOrig();

	  /**
	   * 
	   * @return Numéro d'arrête de changement de position.
	   */
	  String noArretePosition();

	  /**
	   * 
	   * @return Quotité de la position.
	   */
	  Integer quotitePosition();

	  /**
	   * 
	   * @return Temoin indiquant si la position de l'agent est prévisionnelle ou non (une position prévisionnelle n'est pas considérée comme validée).
	   */
	  String temoinPositionPrev();

	  /**
	   * 
	   * @return Témoin indiquant si  PC est acquitée
	   */
	  String temPcAcquitee();

	  /**
	   * 
	   * @return Validité du changement de position
	   */
	  String temValide();

	  /**
	   * 
	   * @return Complément d'information permettant de joindre un agent dont la carrière d'accueil n'est pas gerée par l'éducation nationale.
	   */
	  String txtPosition();

	  /**
	   * 
	   * @return Complément d'information permettant de joindre un agent dont la carrière d'origine n'est pas gerée par l'éducation nationale.
	   */
	  String txtPositionOrig();
	
}
