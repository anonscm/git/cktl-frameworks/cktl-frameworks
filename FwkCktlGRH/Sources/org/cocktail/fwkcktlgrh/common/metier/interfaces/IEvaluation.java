package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOObjectif;
import org.cocktail.fwkcktlgrh.common.metier.EOPoste;
import org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public interface IEvaluation {


		public boolean isViseParResponsableRh();

		public void setIsViseParResponsableRh(boolean value);

		/**
		 * @deprecated
		 * @see #tosLastRepartFdpComp() les repart associees aux competences des
		 *      fiches de poste utilises pour l'evaluation des competences
		 */
		public NSArray tosRepartFdpComp();

		/**
		 * les repart associees aux competences des fiches de poste utilises pour
		 * l'evaluation des competences
		 */
		public NSArray tosLastRepartFdpComp();

		public String display();

		/**
		 * @deprecated
		 * @see #tosLastFicheDePoste() les fiches de poste sur lesquelles portent
		 *      cette evaluation -> filtrage par rapport aux dates de l'evaluation
		 */
		public NSArray tosFicheDePoste();


		// deplacement de la date vers la periode

		/**
		 * @deprecated utiliser la periode
		 * @see #toEvaluationPeriode()
		 */
		public NSTimestamp evaDDebut();

		/**
		 * @deprecated utiliser la periode
		 * @see #toEvaluationPeriode()
		 */
		public NSTimestamp evaDFin();

		/**
		 * Cas particulier d'un enregistrement "tout frais", cette valeur est a null
		 * ... on prend donc la valeur sur la to-one toIndividu
		 * 
		 * @return
		 */
		public Integer noIndividuVisible();

		/**
		 * les fiches de poste (les dernieres) sur lesquelles portent cette evaluation
		 * -> filtrage par rapport aux dates de l'evaluation
		 */
		public NSArray tosLastFicheDePoste();
		/**
		 * La liste des services de rattachement de cette evaluation
		 * 
		 * @return
		 */
		public NSArray<EOStructure> tosStructure();

		/**
		 * La liste des postes de rattachement de cette evaluation
		 * 
		 * @return
		 */
		public NSArray<EOPoste> tosPoste();

		/**
		 * Retourne l'objet <code>EOVCandidatEvaluation</code> associe a cet
		 * enregistrement.
		 */
		public EOVCandidatEvaluation toVCandidatEvaluationPeriode();

		/**
		 * La liste des objectifs classés par ordre de la position
		 */
		public NSArray<EOObjectif> tosObjectif();


		/**
		 * Indique si l'entretien a déjà été tenu
		 * 
		 * @return
		 */
		public boolean isEntretienTenu();


		/**
		 * Indique si le dernier statut connu de l'agent sur la période de l'entretien
		 * est AENES
		 * 
		 * @return
		 */
		public boolean isAenes();

		/**
		 * Indique si le dernier statut connu de l'agent sur la période de l'entretien
		 * est ITRF
		 * 
		 * @return
		 */
		public boolean isItrf();

		/**
		 * Indique si le dernier statut connu de l'agent sur la période de l'entretien
		 * est AENES
		 * 
		 * @return
		 */
		public boolean isBu();


		/**
		 * Le libelle de la population selon le statut de l'agent
		 * 
		 * @return
		 */
		public String getLibellePopulation();

}
