package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import com.webobjects.foundation.NSArray;

public interface IRepartFdpAutre {

	  /**
	   * Le composé de la clé primaire
	   */
	  public String id();

	  public NSArray othersRecords();

	  public String positionKey();


	  public boolean isEnCoursDeModification();

	  public void setIsEnCoursDeModification(boolean value);
	  
	  public String competenceDisplay();
	
}
