package org.cocktail.fwkcktlgrh.common.metier.interfaces;

public interface IValidite {

	/** Retourne true si un objet est valide */
	public boolean estValide();
	/** Modifie la validite d'un objet */
	public void setEstValide(boolean aBool);
	
}
