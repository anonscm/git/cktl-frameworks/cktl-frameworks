package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import com.webobjects.foundation.NSArray;

public interface IRepartEvaNouvelleComp {
	
	 /**
	   * Le compose de la cle primaire
	   */
	  public String id();

	  public NSArray othersRecords();

	  public String positionKey();

}
