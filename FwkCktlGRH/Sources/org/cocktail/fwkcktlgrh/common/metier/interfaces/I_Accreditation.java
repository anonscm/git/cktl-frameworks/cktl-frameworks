package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

public interface I_Accreditation {

	public boolean isAnnuaire();
	
	public IPersonne toPersonneCible();
	
	public IPersonne toPersonneTitulaire();
	
	public boolean isCibleChefDeService();
	
	public boolean isCibleHeritage();
	
	public boolean isCibleServiceSimple();
	
	public EOFeveTypeNiveauDroit toTypeNiveauDroit();
	
	public String libelleTypeNiveauDroit();
	
}
