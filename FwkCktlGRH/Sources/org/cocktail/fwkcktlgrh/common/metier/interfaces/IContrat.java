package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant;

import com.webobjects.foundation.NSTimestamp;

public interface IContrat {

	public boolean estVacataire();
	
	/**
	 * Renvoie l'avenant courant et valide du contrat.
	 * @return avenant du contrat
	 */
	public EOContratAvenant avenantActuel();


	/**
	 * renvoie le nombre d'heures attribuées dans tous les contrats courants (avenant + vacations) d'un individu
	 * @param ec
	 * @param ind
	 * @return
	 * @deprecated : utiliser nbHeuresAvenantContratCourant ou  nbHeuresVacationsContrat
	 
	public double nbHeuresTousContratsEnCours(EOEditingContext ec, EOIndividu ind);
	*/
	/**
	 * Renvoie le nombre d'heures des avenants du contrat sur une periode
	 * @param eo : editing context
	 * @param dateDebut
	 * @param dateFin
	 * @return nombre d'heures des avenants du contrat
	 */
	public double nbHeuresAvenantContratsurPeriode(NSTimestamp dateDebut, NSTimestamp dateFin);
		
	
	/**
	 * Renvoie le nombre d'heures des vacations d'un contrat
	 * @param eo : editing context
	 * @return nombre d'heures des contrats de vacations
	 
	public double nbHeuresVacationsContrat(EOEditingContext eo);
*/
	
}
