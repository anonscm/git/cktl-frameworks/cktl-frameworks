package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import com.webobjects.appserver._private.WOCheckBox;
import com.webobjects.foundation.NSTimestamp;

public interface IAffectationDetail {
 


 // methodes rajoutees

	public String displayAgent();

	public String display();

	public boolean isActuelle();

	public boolean isAncienne();

	public boolean isFuture();

	/**
	 * La date de debut de l'occupation conditionnee
	 */
	public NSTimestamp dDebut();


	/**
	 * La date de fin de l'occupation conditionnee
	 */
	public NSTimestamp dFin();

	/**
	 * L'affichage d'une occupation. <affectation gepeto> + <dates de
	 * l'affectation detail>
	 * 
	 * On afficher une message d'erreur au cas ou l'affectation gepeto attachee
	 * n'existe plus.
	 */
	public String fullDisplay(boolean isHtml);

	/**
	 * acces a {@link #adeDateDiffAffectation()} en boolean
	 * 
	 * @return
	 */
	public boolean getIsAdeDateDiffAffectation();

	/**
	 * acces a {@link #adeDateDiffAffectation()} en boolean inverse pour les
	 * {@link WOCheckBox}
	 * 
	 * @return
	 */
	public boolean getIsAdeDateSameAffectation();

	/**
	 * acces a {@link #adeDateDiffAffectation()} en boolean pour les
	 * {@link WOCheckBox}
	 * 
	 * @return
	 */
	public void setIsAdeDateSameAffectation(boolean isAdeDateSameAffectation);

	/**
	 * Indique si les dates de cette occupation se chevauchent avec une autre
	 * occupation sur le même poste
	 * 
	 * @return
	 */
	public boolean isChevauchementAutreAffectationDetailMemeAgent();


	/**
	 * Indique s'il faut afficher un message avertissement
	 */
	public boolean hasWarning();

	public String htmlWarnMessage();

}
