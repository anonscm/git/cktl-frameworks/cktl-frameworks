package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;

/**
 * Descriptif d'une repartition entre une évaluation et une formation.
 * Interface crée pour gérer la migration des demandes de formations de 
 * la classe EORepartFicheItem et la classe EORepartFormationSouhaitee
 * @author ctarade
 *
 */
public interface I_RepartFormation {

	// les classements possibles
	String SORT_EVALUATEUR = "toEvaluation.toEvaluateur.nomPrenom";
	String SORT_AGENT = "toEvaluation.toIndividu.nomPrenom";
	String SORT_FORMATION_SOUHAITEE = "libelleFormation";
	String SORT_IS_NOMENCLATURE = "isNomenclature";
	String SORT_DATE_ENTRETIEN = "toEvaluation.dTenueEntretien";
	String SORT_DATE_VISA_RH = "toEvaluation.dVisaResponsableRh";
	
	public String libelleFormation();
	
	public EOEvaluation toEvaluation();
	
	public boolean isNomenclature();
	
}
