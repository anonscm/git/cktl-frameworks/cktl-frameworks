package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp;
import org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites;
import org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences;

import com.webobjects.foundation.NSArray;

public interface IFicheDePoste {

	//methodes rajoutees

	public NSArray tosReferensActivites();

	public NSArray tosReferensCompetences();

	public NSArray tosRepartFdpActi();

	/**
	 * La liste des activités autres
	 * 
	 * @return
	 */
	public NSArray tosRepartFdpActivitesAutres();

	/**
	 * La liste des competences autres
	 * 
	 * @return
	 */
	public NSArray tosRepartFdpCompetencesAutres();

	public NSArray<EORepartFdpComp> tosRepartFdpComp();

	public boolean fdpVisaAgentBool();

	public void setFdpVisaAgentBool(boolean value);

	public boolean fdpVisaRespBool();

	public void setFdpVisaRespBool(boolean value);

	public boolean fdpVisaDirecBool();

	public void setFdpVisaDirecBool(boolean value);

	/**
	 * Indique si les visa ne sont pas faites sur cette fiche
	 */
	public boolean hasWarning();

	public String htmlWarnMessage();

	// setters silencieux pour ne pas planter lors d'acces non prévu

	public void setTosReferensActivites(NSArray value);

	public void setTosReferensCompetences(NSArray value);

	/**
	 * est-ce qu'une activité autre est en cours de modification
	 * 
	 * @return
	 */
	public boolean isAuMoinsUneActiviteAutrefEstEnCoursDeModification();

	/**
	 * désactiver les activités autre est en cours de modification
	 * 
	 * @return
	 */
	public void desactiverTouteActiviteAutrefEnCoursDeModification();

	/**
	 * est-ce qu'une competence autre est en cours de modification
	 * 
	 * @return
	 */
	public boolean isAuMoinsUneCompetenceAutrefEstEnCoursDeModification();

	/**
	 * désactiver les competences autre est en cours de modification
	 * 
	 * @return
	 */
	public void desactiverTouteCompetenceAutrefEnCoursDeModification();

	/**
	 * Indique si la fiche est sur l'ancienne nomenclature ou non
	 * 
	 * @return
	 */
	public boolean isEmploiSurAncienneNomenclature();

	/**
	 * Affecter des competences à la fiche de poste
	 */
	public NSArray<EORepartFdpComp> addReferensCompetencesArray(NSArray<EOReferensCompetences> eoReferensCompetencesArray);

	/**
	 * Affecter une competence à la fiche de poste
	 */
	public EORepartFdpComp addReferensCompetences(EOReferensCompetences eoReferensCompetence);

	/**
	 * Affecter des competences à la fiche de poste
	 */
	public NSArray<EORepartFdpActi> addReferensActivitesArray(NSArray<EOReferensActivites> eoReferensActivitesArray);

	/**
	 * Affecter une competence à la fiche de poste
	 */
	public EORepartFdpActi addReferensActivites(EOReferensActivites eoReferensActivites);

	
}
