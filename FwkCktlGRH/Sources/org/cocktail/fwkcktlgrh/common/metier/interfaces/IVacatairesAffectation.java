package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOVacataires;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

import com.webobjects.foundation.NSTimestamp;

/**
 * Définit le contrat des affectations d'une vacation
 * 
 * @author Chama LAATIK
 *
 */
public interface IVacatairesAffectation {

	/**
	 * @return date de création
	 */
	NSTimestamp getDateCreation();

	void setDateCreation(NSTimestamp value);

	/**
	 * @return date de modification
	 */
	NSTimestamp getDateModification();

	void setDateModification(NSTimestamp value);

	/**
	 * @return nombre d'heures
	 */
	Double getNbHeures();

	void setNbHeures(Double value);
	
	/**
	 * Est-ce que c'est une affectation principale?
	 * @return O/N
	 */
	String getTemoinPrincipale();
	
	void setTemoinPrincipale(String value);
	
	/**
	 * Est-ce que c'est une affectation principale?
	 * @return vrai/faux
	 */
	boolean isAffectationPrincipale();

	/**
	 * @return Structure d'affectation de la vacation
	 */
	IStructure getToStructure();

	void setToStructureRelationship(IStructure value);

	/**
	 * @return la vacation
	 */
	EOVacataires getToVacataire();

	void setToVacataireRelationship(EOVacataires value);

}