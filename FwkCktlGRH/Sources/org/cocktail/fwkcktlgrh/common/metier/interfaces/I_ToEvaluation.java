package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;

/**
 * pointe vers un {@link EOEvaluation}
 * 
 * @author ctarade
 */
public interface I_ToEvaluation {

	// les classements possibles
	String SORT_EVALUATEUR = "toEvaluation.toIndividuResp.nomPrenom";
	String SORT_AGENT = "toEvaluation.toIndividu.nomPrenom";
	String SORT_DATE_ENTRETIEN = "toEvaluation.dTenueEntretien";
	String SORT_DATE_VISA_RH = "toEvaluation.dVisaResponsableRh";
	public EOEvaluation toEvaluation();
	
}
