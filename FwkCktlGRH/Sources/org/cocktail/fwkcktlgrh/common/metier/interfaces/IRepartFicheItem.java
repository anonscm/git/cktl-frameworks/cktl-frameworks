package org.cocktail.fwkcktlgrh.common.metier.interfaces;

public interface IRepartFicheItem {

	public String libelleFormation();

	public boolean isNomenclature();
	
}
