package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;


public interface IDureePourIndividu extends IDuree {

	public EOIndividu individu();
	public String typeEvenement();
	public boolean supportePlusieursTypesEvenement();
	
}
