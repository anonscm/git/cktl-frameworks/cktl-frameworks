package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode;
import org.cocktail.fwkcktlgrh.common.metier.EOTplBloc;

import com.webobjects.foundation.NSArray;

public interface ITplOnglet {

	// methodes rajoutees

	/**
	 * Surcharge du lien vers tosTplBloc pour avoir un classement selon le temoin
	 * <code>tblPosition</code>
	 */
	public NSArray<EOTplBloc> tosTplBlocSortedByPosition(EOEvaluationPeriode eoPeriode);


	// nature des onglets

	public boolean isAgent();

	public boolean isObjectifsPrecedents();

	public boolean isObjectifsSuivants();

	public boolean isSituation();

	public boolean isCompetence();

	public boolean isEvolution();

	public boolean isFin();
	
}
