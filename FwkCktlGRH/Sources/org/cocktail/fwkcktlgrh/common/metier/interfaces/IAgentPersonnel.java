package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import com.webobjects.foundation.NSArray;

public interface IAgentPersonnel {

    public String agtLogin();
    
    public boolean gereEnseignants();
    
    public void setGereEnseignants(boolean aBool);
    
    public boolean gereNonEnseignants();
    
    public void setGereNonEnseignants(boolean aBool);
    
    public boolean gereVacataires();
    
    public void setGereVacataires(boolean aBool);
    
    public boolean gereHeberges();
    
    public void setGereHeberges(boolean aBool);
    
    public String agtGereHeberge();
    
    public boolean gereTousAgents();
    
    public boolean gereTouteStructure();
    
    public void setGereTouteStructure(boolean aBool);
    
    /** Retourne la liste des services g&eacute;r&eacute;s par l'utilisateur avec les structures filles */
    public NSArray structuresGereesEtFilles();
    
    /** Retourne la liste des services g&eacute;r&eacute;s par l'utilisateur */
    public NSArray structuresGerees();
    
    /** Retourne la liste des groupes g&eacute;r&eacute;s par l'utilisateur */
    public NSArray groupesGeres();
    
    public boolean peutConsulterDossier();
    
    public void setPeutConsulterDossier(boolean aBool);

    public boolean peutAfficherEmplois();
    
    public void setPeutAfficherEmplois(boolean aBool);
    
    public boolean peutGererEmplois();
    
    public void setPeutGererEmplois(boolean aBool);
    
    public boolean peutAfficherPostes();
    
    public void setPeutAfficherPostes(boolean aBool);
    
    public boolean peutGererPostes();
    
    public void setPeutGererPostes(boolean aBool);
    
    public boolean peutAfficherIndividu();
    
    public void setPeutAfficherIndividu(boolean aBool);
    
    public boolean peutGererIndividu();
    
    public void setPeutGererIndividu(boolean aBool);
    
    public boolean peutAfficherInfosPerso();
    
    public void setPeutAfficherInfosPerso(boolean aBool);
    
    public boolean peutAfficherContrats();
    
    public void setPeutAfficherContrats(boolean aBool);
    
    public boolean peutGererContrats();
    
    public void setPeutGererContrats(boolean aBool);
    
    public boolean peutAfficherCarrieres();
    
    public void setPeutAfficherCarrieres(boolean aBool);
    
    public boolean peutGererCarrieres();
    
    public void setPeutGererCarrieres(boolean aBool);
    
    public boolean peutAfficherOccupation();
    
    public void setPeutAfficherOccupation(boolean aBool);
    
    public boolean peutGererOccupation();
    
    public void setPeutGererOccupation(boolean aBool);
    
    public boolean peutAfficherConges();
    
    public void setPeutAfficherConges(boolean aBool);
    
    public boolean peutGererConges();
    
    public void setPeutGererConges(boolean aBool);
    
    public boolean peutUtiliserOutils();
    
    public void setPeutUtiliserOutils(boolean aBool);
    
    public boolean peutAdministrer();
    
    public void setPeutAdministrer(boolean aBool);
    
    public boolean peutUtiliserRequetes();
    
    public void setPeutUtiliserRequetes(boolean aBool);
    
    public boolean peutUtiliserEditions();
    
    public void setPeutUtiliserEditions(boolean aBool);
    
    public boolean peutAfficherAgents();
    
    public void setPeutAfficherAgents(boolean aBool);
    
    public boolean peutGererAgents();
    
    public void setPeutGererAgents(boolean aBool);
    
    public boolean peutAfficherAccidentTravail();
    
    public void setPeutAfficherAccidentTravail(boolean aBool);
    
    public boolean peutGererAccidentTravail();
    
    public boolean peutCreerListesElectorales();
    
    public void setPeutCreerListesElectorales(boolean aBool);
    
    public boolean peutGererPromouvabilites();
    
    public void setPeutGererPromouvabilites(boolean aBool);
    
    public boolean peutGererPrimes();
    
    public void setPeutGererPrimes(boolean aBool);
    
    public boolean gerePlusieursCategoriesPersonnel();
    
    public void init();

    public void initPourPersonnel();
    
    public void transformerEnPersonnel();
    
    public void supprimerRelations();
    
	
}
