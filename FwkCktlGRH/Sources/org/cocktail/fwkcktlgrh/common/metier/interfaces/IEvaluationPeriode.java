package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode;
import org.cocktail.fwkcktlpersonne.common.metier.EONiveauCompetence;

import com.webobjects.foundation.NSArray;

public interface IEvaluationPeriode {

	// methodes rajoutees

		/**
		 * Affichage dd1/mm1/yyyy1-dd2/mm2/yyyy2
		 */
		public String strDateDebutDateFin();


		/**
		 * Affichage yyyy1/yyyy2
		 */
		public String strAnneeDebutAnneeFin();

		public Number epeKey();


		/**
		 * L'enregistrement <code>EOEvaluationPeriode</code> qui suit <em>this</em>
		 * 
		 * @return
		 */
		public EOEvaluationPeriode toNextPeriode();

		/**
		 * L'enregistrement <code>EOEvaluationPeriode</code> qui precede <em>this</em>
		 * 
		 * @return
		 */
		public EOEvaluationPeriode toPrevPeriode();


		// la liste des niveaux de competences associés


		/**
		 * 
		 * @return
		 */
		public NSArray<EONiveauCompetence> niveauCompetenceList();

		// ajouts

		/**
		 * Donne la liste des {@link EONiveauCompetence} potentiels utilisable pour la
		 * periode passée en paramètre. On fait un croisement avec les dates de
		 * validités.
		 */
		public  NSArray getNiveauCompetenceForPeriode(EOEvaluationPeriode periode);
	
}
