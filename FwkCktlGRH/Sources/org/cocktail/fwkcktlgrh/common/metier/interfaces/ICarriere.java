package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import java.util.Date;

import org.cocktail.fwkcktlgrh.common.metier.EOElements;

import com.webobjects.foundation.NSArray;

public interface ICarriere {


	public Date getDtDebut();

	public Date getDtFin();

	public NSArray<EOElements> elementsTries();

	public EOElements elementActuel();

	
}
