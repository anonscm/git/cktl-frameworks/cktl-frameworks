package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import com.webobjects.foundation.NSArray;

public interface IObjectif {


	  public boolean isEnCoursDeModification();

	  public void setIsEnCoursDeModification(boolean value);

	  public NSArray othersRecords();

	  public String positionKey();

	  // affichage des sauts de ligne en HTML

	  public String objMesureHtml();

	  public String objMoyenHtml();

	  public String objObjectifHtml();

	  public String objObservationHtml();

	  public String objResultatHtml();


	  /**
	   * Ancre HTML associée à cet objectif
	   * @return
	   */
	  public String ancre();
	
}
