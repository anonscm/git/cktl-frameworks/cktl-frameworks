package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EORepartLolfSilland;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktlpersonne.common.metier.EOFctSilland;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public interface IRepartFloSilland {

	  // methodes rajoutees
	  
	  public boolean isEnCoursDeModification();
	  
	  public void setIsEnCoursDeModification(boolean value);
	  
	  
	  public boolean isEnCoursDAjoutDeLolfNomenclature();
	  
	  public void setIsEnCoursDAjoutDeLolfNomenclature(boolean value);

	  /**
		 * Indique si la fonction est affectee dans l'exercice.
		 * 
		 * @param exercice
		 * @return
		 */
		public boolean isDeclaree(EOFctSilland fctSilland, EOExercice exercice);
	  
		public NSArray<EORepartLolfSilland> tosRepartSilLolf(EOFctSilland silland, EOQualifier qualifier, NSArray sortOrderings);
	
}
