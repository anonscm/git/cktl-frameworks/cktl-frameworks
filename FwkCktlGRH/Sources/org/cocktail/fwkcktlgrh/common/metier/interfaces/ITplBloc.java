package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation;
import org.cocktail.fwkcktlgrh.common.metier.EOTplItem;

import com.webobjects.foundation.NSArray;

public interface ITplBloc {

	// methodes rajoutees

	public boolean isFacultatif();

	/**
	 * Surcharge du lien vers tosTplItem pour avoir un classement selon le temoin
	 * <code>titPosition</code>
	 */
	public NSArray<EOTplItem> tosTplItemSorted();

	/**
	 * Definition de l'ancre HTML associee au bloc (chaine + clé primaire)
	 */
	public String anchorName();

	/**
	 * Bloc formation : geres par l'interface <code>CompTplBlocFormation</code>
	 * 
	 * @return
	 */
	public boolean isBlocNatureFormation();

	/**
	 * Bloc formation souhaitee : geres par l'interface
	 * <code>CompTplBlocFormation</code>
	 * 
	 * @return
	 */
	public boolean isBlocNatureFormationSouhaitee();

	/**
	 * Bloc notice de promotions : geres par l'interface
	 * {@link CompNoticePromotion}
	 * 
	 * @return
	 */
	public boolean isBlocNatureNoticeDePromotions();

	/**
	 * Bloc dynamiques : geres par <code>CompTplOnglet</code>
	 * 
	 * @return
	 */
	public boolean isBlocNatureDynamique();

	/**
	 * TODO faire generique recFiche : la fiche (fiche de poste, fiche LOLF ou
	 * fiche d'evaluation) Retrouver un enregistrement associe.
	 * 
	 * @param ec
	 * @param recTplBloc
	 *          : le bloc facultatif
	 * @param recEvaluation
	 *          : la fiche (fiche de poste, fiche LOLF ou fiche d'evaluation)
	 * @return <em>null</em> si non trouvé.
	 */
	public EORepartFicheBlocActivation getActivationRecord(EOEvaluation recEvaluation);
	
}
