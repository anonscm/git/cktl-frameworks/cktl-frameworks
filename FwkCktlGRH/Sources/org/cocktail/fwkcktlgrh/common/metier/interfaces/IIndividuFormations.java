package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOIndividuFormations;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public interface IIndividuFormations {

	public String champLibre();

	public void setChampLibre(String champLibre);

	public boolean isGestionPeriode();

	public NSTimestamp dDebut();

	public void setDDebut(NSTimestamp dDebut);

	public NSTimestamp dFin();

	public void setDFin(NSTimestamp dFin);

	public boolean isGestionDuree();


	public NSArray<EOIndividuFormations> tosIndividuFormations(EOIndividu individu, EOQualifier qualifier, NSArray sortOrderings);
	
}
