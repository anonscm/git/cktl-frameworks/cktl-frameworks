package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public interface IAffectation {
	   


    /**
     * On ne supprime pas les affectations, on passe juste le témoin de validitié à "N"
     */
    public void annuler();
    
    
    /**
     * @return structure de l'affectation 
     */
    EOStructure toStructure();
    
    
    /**
     * Renvoie l'état de l'affectation en fonction de la date courante
     * @return etat de l'affectation
     */
    public String getEtatAffectation();
    
    // methodes rajoutee
    
    public String display();
    
	
}
