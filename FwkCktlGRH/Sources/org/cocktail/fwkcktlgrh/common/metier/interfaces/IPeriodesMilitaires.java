package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import java.util.Date;



/**
 * definit le contrat d'une Période Militaire
 */
public interface IPeriodesMilitaires {

	// Getters appelant une valeur au travers d'une relation
	/**
	 * @return le N° de dossier de l'agent
	 */
	Integer noDossierPers();
	

		  
	
	// Getters issus de _EOPeriodesMilitaires
	/**
	 * @return la date de début de cette période militaire de l'agent
	 */
	Date dateDebut();


	/**
	 * @return la date de fin de cette période militaire de l'agent
	 */
	Date dateFin();


	/**
	 * @return le témoin de validité pour cette période militaire de l'agent
	 */
	String temValide();
	
}
