package org.cocktail.fwkcktlgrh.common.metier.interfaces;


public interface IEvaluationNoticePromotion {


	/**
	 * 
	 * @return
	 */
	public String enpReductionEchelonLibelle();

	/**
	 * 
	 * @return
	 */
	public String enpPromotionGradeLibelle();

	/**
	 * 
	 * @return
	 */
	public String enpPromotionCorpsLibelle();


	// raccourcis vers la population

	public boolean isAenes();

	public boolean isItrf();

	public boolean isBu();

	public boolean isReductionEchelonOui();

	public boolean isReductionEchelonNon();

	public boolean isPromotionGradeOui();

	public boolean isPromotionGradeNon();

	public boolean isPromotionCorpsOui();

	public boolean isPromotionCorpsNon();

	// Mettre une valeur par défaut en motif si avis défavorable

	public void setEnpReductionEchelon(Integer value);

	public void setEnpPromotionGrade(Integer value);

	public void setEnpPromotionCorps(Integer value);

	// recopie de l'appréciation générale de l'entretien

	/**
	 * Recopier la valeur de l'appréciation saisie dans l'entretien professionnel
	 * pour la positionner dans celle de la notice de promotion
	 */
	public void recupererAppreciationGeneraleEntretien();
  
	
}
