package org.cocktail.fwkcktlgrh.common.metier.interfaces;


public interface IContratHeberge {

    public boolean estValide();
    
    public boolean estAnnule();
    
    public boolean estEnCoursDeValidation();

    public String etat();

    public void setEtat(String etat);

	
}
