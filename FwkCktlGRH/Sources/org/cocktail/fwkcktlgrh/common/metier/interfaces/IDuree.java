package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import com.webobjects.foundation.NSTimestamp;

public interface IDuree {

	public NSTimestamp dateDebut();
	public NSTimestamp dateFin();
	
}
