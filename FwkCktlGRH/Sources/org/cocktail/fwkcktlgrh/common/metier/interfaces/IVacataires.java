package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import java.util.List;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOCnu;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOProfession;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

import com.webobjects.foundation.NSTimestamp;

/**
 * Définit le contrat d'une vacation
 * 
 * @author Chama LAATIK
 *
 */
public interface IVacataires {
	
	String VACATAIRE_FONCTIONNAIRE = "Vacataire fonctionnaire";
	String VACATAIRE_NON_FONCTIONNAIRE = "Vacataire non fonctionnaire";
	
	/**
	 * @return le libellé du type de vacation
	 */
	String getLibelleTypeVacation();
	
	/**
	 * Est-ce que la vacation concerne un enseignant?
	 * @return Oui/Non
	 */
	boolean isEnseignant();
	
	/**
	 * Est-ce que le vacataire est payé?
	 * @return Oui/Non
	 */
	boolean isPaye();
	
	/**
	 * Est-ce que la vacation est signée?
	 * @return Oui/Non
	 */
	boolean isVacationSigne();
	
	/**
	 * Est-ce que la vacation concerne un titulaire?
	 * @return Oui/Non
	 */
	boolean isTitulaire();
	
	/**
	 * Est-ce que la vacation est valide?
	 * @return Oui/Non
	 */
	boolean isValide();

	/**
	 * @return la date d'arrêté
	 */
	NSTimestamp getDateArrete();

	void setDateArrete(NSTimestamp value);

	/**
	 * @return la date de création
	 */
	NSTimestamp getDateCreation();

	void setDateCreation(NSTimestamp value);

	/**
	 * @return la date de début
	 */
	NSTimestamp getDateDebut();

	void setDateDebut(NSTimestamp value);

	/**
	 * @return la date de fin
	 */
	NSTimestamp getDateFin();

	void setDateFin(NSTimestamp value);

	/**
	 * @return la date de modification
	 */
	NSTimestamp getDateModification();

	void setDateModification(NSTimestamp value);

	/**
	 * @return le libellé d'enseignement
	 */
	String getLibelleEnseignement();

	void setLibelleEnseignement(String value);

	/**
	 * @return le nombre d'heures de la vacation
	 */
	Double getNbHeures();

	void setNbHeures(Double value);

	/**
	 * @return le nombre d'heures réalisé de la vacation
	 */
	Double getNbHeuresRealisees();

	void setNbHeuresRealisees(Double value);

	/**
	 * @return le numéro d'arrêté
	 */
	String getNoArrete();

	void setNoArrete(String value);

	/**
	 * @return observations
	 */
	String getObservations();

	void setObservations(String value);

	/**
	 * @return le taux horaire
	 */
	Double getTauxHoraire();

	void setTauxHoraire(Double value);

	/**
	 * @return le taux horaire réalisé de la vacation
	 */
	Double getTauxHoraireRealise();

	void setTauxHoraireRealise(Double value);

	/**
	 * @return Est-ce que la vacation est de type enseignant?
	 */
	String getTemoinEnseignant();

	void setTemoinEnseignant(String value);

	/**
	 * @return Est-ce que le vacataire est payé?
	 */
	String getTemoinPaye();

	void setTemoinPaye(String value);

	/**
	 * @return Est-ce que la vacation est signée?
	 */
	String getTemoinSigne();

	void setTemoinSigne(String value);

	/**
	 * @return Est-ce que la vacation concerne un titulaire?
	 */
	String getTemoinTitulaire();

	void setTemoinTitulaire(String value);

	/**
	 * @return Est-ce que la vacation est valide?
	 */
	String getTemoinValide();

	void setTemoinValide(String value);

	EOAdresse getToAdresse();

	void setToAdresseRelationship(EOAdresse value);

	EOCnu getToCnu();

	void setToCnuRelationship(EOCnu value);

	EOCorps getToCorps();

	void setToCorpsRelationship(EOCorps value);

	EOGrade getToGrade();

	void setToGradeRelationship(EOGrade value);

	IIndividu getToIndividuVacataire();

	void setToIndividuVacataireRelationship(IIndividu value);

	EOPersonne getToPersonneCreation();

	void setToPersonneCreationRelationship(EOPersonne value);

	EOPersonne getToPersonneModification();

	void setToPersonneModificationRelationship(EOPersonne value);

	EOProfession getToProfession();

	void setToProfessionRelationship(EOProfession value);

	IStructure getToStructure();

	void setToStructureRelationship(IStructure value);

	List<? extends IVacatairesAffectation> getListeVacatairesAffectations();
	
	EOTypeContratTravail getToTypeContratTravail();

}