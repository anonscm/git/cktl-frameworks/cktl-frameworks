package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem;
import org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public interface ITplItem {

	// methodes rajoutees

	public boolean isListe();

	public boolean isChampLibre();

	public boolean isTexteStatique();

	/**
	 * La liste des repartitions associées sur une période
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public NSArray<EOTplRepartItemItemValeur> tosTplRepartItemItemValeur(NSTimestamp dateDebut, NSTimestamp dateFin);

	/**
	 * TODO faire generique recFiche : la fiche (fiche de poste, fiche LOLF ou
	 * fiche d'evaluation) Retrouver un enregistrement associ à une évaluation
	 * 
	 * @param recEvaluation
	 *          : la fiche (fiche de poste, fiche LOLF ou fiche d'evaluation)
	 * @return <em>null</em> si non trouvé.
	 * @return
	 */
	public EORepartFicheItem getRepartItemForEvaluation(EOEvaluation eoEvalution);
	
	/**
	 * Retrouver la valeur d'un item de type champ libre. Se base sur un
	 * enregistrement du type <code>EORepartFicheItem</code>. Si non trouvé, alors
	 * la chaine vide est retournee.
	 */
	public String getStrChampLibre(EOEvaluation eoEvaluation);
  
	
}
