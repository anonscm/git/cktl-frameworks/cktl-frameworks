package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation;

import com.webobjects.foundation.NSTimestamp;

public interface IVCandidatEvaluation {

	// ajouts


	/**
   * 
   */
	public boolean isViseParResponsableRh();

	/**
   * 
   */
	public NSTimestamp dTenueEntretien();

	/**
   * 
   */
	public NSTimestamp dVisaResponsableRh();

	/**
	 * La fenetre volante sur la case à cocher visa RH
	 */
	public String tipChkDVisaResponsableRh();

	public String display();

	public EOVCandidatEvaluation toVCandidatEvaluation();

	public void setIsViseParResponsableRh(boolean value);
  
	
}
