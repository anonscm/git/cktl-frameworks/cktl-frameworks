package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import com.webobjects.foundation.NSArray;

public interface IRepartFdpComp {

	public NSArray othersRecords();

	public String positionKey();

	public String competenceDisplay();
}
