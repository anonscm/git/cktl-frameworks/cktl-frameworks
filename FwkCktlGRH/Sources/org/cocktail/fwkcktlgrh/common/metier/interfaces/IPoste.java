package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf;
import org.cocktail.fwkcktlgrh.common.metier.util.UtilFiche;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public interface IPoste {

	  
	  //METHODE AJOUTEES

		// types de postes a afficher

		public Number posKey();

		public String display();
		
		/**
		 * Affichage reduit au code et a l'occupation actuelle si elle existe.
		 * 
		 * @return
		 */
		public String displayCodeOccupationActuelle();

		/**
		 * est-ce que les dates de validites sont actuelles
		 */
		public boolean isOuvert();

		/**
		 * la date de fin est passee
		 */
		public boolean isFerme();

		/**
		 * la date de fin est passee
		 */
		public boolean isVacant();

		/**
		 * la date de fin est passee
		 */
		public boolean isOccupe();

		/**
		 * la date de debut est pas encore atteinte
		 */
		public boolean isFutur();

		/**
		 * Le poste concerne-t-il un personnel non enseignant (la valeur de
		 * l'affectation actuelle en priorité, si non trouvé, on prend la derniere
		 * affectation)
		 * 
		 * @return
		 */
		public boolean isNonEnseignant();

		/**
		 * Le poste concerne-t-il un personnel enseignant (la valeur de l'affectation
		 * actuelle en priorité, si non trouvé, on prend la derniere affectation)
		 * 
		 * @return
		 */
		public boolean isEnseignant();

		/**
		 * La dernier fiche de poste connue du poste
		 */
		public EOFicheDePoste toDerniereFicheDePoste();

		/**
		 * La dernier fiche LOLF connue du poste
		 */
		public EOFicheLolf toDerniereFicheLolf();

		
		/**
		 * occupant du poste : - si le poste est ouvert : l'affectation en cours - si
		 * le poste ou la fiche fermé(e) : la derniere des affectations TODO renommer
		 * en toAffectationDetailCourante
		 */
		public EOAffectationDetail toAffectationDetailActuelle();


		/**
		 * La derniere occupation en date du poste
		 */
		public EOAffectationDetail toAffectationDetailDerniere();

		public EOStructure toComposante();

		/**
		 * Toutes les fiches associees au poste
		 */
		public NSArray<UtilFiche> tosFiche();

		/**
		 * Indique si un ou plusieurs avertissements existent pour ce poste. Exemple :
		 * fiches non visees ...
		 */
		public boolean hasFicheWarning();

		/**
		 * Indique si un ou plusieurs avertissements existent pour ce poste par
		 * rapport a ces occupations
		 */
		public boolean hasOccupationWarning();

		/**
		 * Indique si la fiche de poste actuelle du poste pointe sur un emploi type de
		 * l'ancienne nomenclature
		 * 
		 * @return
		 */
		public boolean hasCurrentFicheDePosteEmploiTypeAncienWarning();

		/**
		 * Le descriptif des avertissements sur les fiches
		 */
		public String htmlFicheWarnMessage();

		/**
		 * Le descriptif des avertissements sur les occupations
		 */
		public String htmlOccupationWarnMessage();

		// recherche

		public String posLibelleBasic();

		public String posCodeBasic();



		/**
		 * Donne la date de début de validité par défaut attendue pour la création
		 * d'une nouvelle fiche de poste pour ce poste :
		 * 
		 * - debut de la période en cours
		 * 
		 * - si derniere fiche existante démarre sur la période en cours, alors date
		 * de fin de derniere fiche + 1
		 * 
		 * - si pas de fiche, alors date de début du poste
		 * 
		 * @return
		 */
		public NSTimestamp getDateDebutParDefautPourNouvelleFicheDePoste();
}
