package org.cocktail.fwkcktlgrh.common.metier.interfaces;

import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf;
import org.cocktail.fwkcktlgrh.common.metier.EOPoste;
import org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Descriptif d'un objet de type droit
 * @see Droit
 * @see EODroit
 * 
 * @author ctarade
 */
public interface I_Droit {

	/** pour des classement alphabetiques */
	public final static String CIBLE_DISPLAY_KEY = "cibleDisplay";

	public EOFicheDePoste toDroitFicheDePoste();

	public EOFicheLolf toDroitFicheLolf();

	public EOPoste toDroitPoste();

	public EOIndividu toDroitIndividu();

	public EOVCandidatEvaluation toDroitVCandidatEvaluation();

	public EOStructure toDroitStructure();

	public EOStructure toDroitComposante();

	public EOEvaluationPeriode toDroitEvaluationPeriode();

	public NSArray tosServiceFlatten();

	public String cibleDisplay();

	public boolean isDroitGlobal();

	public boolean isDroitComposante();

	public boolean isDroitStructure();

	public boolean isDroitPoste();

	public boolean isDroitFicheDePoste();

	public boolean isDroitFicheLolf();

	public boolean isDroitIndividu();

	public boolean isDroitVCandidatEvaluation();

	public EOEditingContext editingContext();
	
}
