/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacatairesAffectation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Classe des affectations d'une vacation
 * 
 * @author Chama LAATIK
 */
public class EOVacatairesAffectation extends _EOVacatairesAffectation implements IVacatairesAffectation {
	
	private static final long serialVersionUID = -5073811536698513328L;

	
	
	public String getTemoinPrincipale() {
		return temoinPrincipale();
	}

	public boolean isAffectationPrincipale() {
		return getTemoinPrincipale() != null && Constantes.VRAI.equals(getTemoinPrincipale());
	}

	public Double getNbHeures() {
		return nbHeures();
	}
	
	public NSTimestamp getDateCreation() {
		return dateCreation();
	}

	public NSTimestamp getDateModification() {
		return dateModification();
	}

	public IStructure getToStructure() {
		return toStructure();
	}

	/**
	 *  Setter pour une structure 
	 * @param value : une structure {@link IStructure}
	 **/
	public void setToStructureRelationship(IStructure value) {
		super.setToStructureRelationship((EOStructure) value);
	}

	public EOVacataires getToVacataire() {
		return toVacataire();
	}
	
	/**
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
