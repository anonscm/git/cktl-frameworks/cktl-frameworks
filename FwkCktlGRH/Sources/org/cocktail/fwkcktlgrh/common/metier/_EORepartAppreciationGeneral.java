/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EORepartAppreciationGeneral.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EORepartAppreciationGeneral extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_RepartAppreciationGeneral";

	// Attributes
	public static final String EVALUATION_KEY_KEY = "evaluationKey";
	public static final String ITEM_TIT_KEY_KEY = "itemTitKey";
	public static final String ITEM_VALEUR_TIT_KEY_KEY = "itemValeurTitKey";

	public static final ERXKey<Integer> EVALUATION_KEY = new ERXKey<Integer>("evaluationKey");
	public static final ERXKey<Integer> ITEM_TIT_KEY = new ERXKey<Integer>("itemTitKey");
	public static final ERXKey<Integer> ITEM_VALEUR_TIT_KEY = new ERXKey<Integer>("itemValeurTitKey");
	// Relationships
	public static final String TO_EVALUATION_KEY = "toEvaluation";
	public static final String TO_TPL_ITEM_KEY = "toTplItem";
	public static final String TO_TPL_ITEM_VALEUR_KEY = "toTplItemValeur";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> TO_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>("toEvaluation");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplItem> TO_TPL_ITEM = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplItem>("toTplItem");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur> TO_TPL_ITEM_VALEUR = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur>("toTplItemValeur");

  private static Logger LOG = Logger.getLogger(_EORepartAppreciationGeneral.class);

  public EORepartAppreciationGeneral localInstanceIn(EOEditingContext editingContext) {
    EORepartAppreciationGeneral localInstance = (EORepartAppreciationGeneral)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer evaluationKey() {
    return (Integer) storedValueForKey("evaluationKey");
  }

  public void setEvaluationKey(Integer value) {
    if (_EORepartAppreciationGeneral.LOG.isDebugEnabled()) {
    	_EORepartAppreciationGeneral.LOG.debug( "updating evaluationKey from " + evaluationKey() + " to " + value);
    }
    takeStoredValueForKey(value, "evaluationKey");
  }

  public Integer itemTitKey() {
    return (Integer) storedValueForKey("itemTitKey");
  }

  public void setItemTitKey(Integer value) {
    if (_EORepartAppreciationGeneral.LOG.isDebugEnabled()) {
    	_EORepartAppreciationGeneral.LOG.debug( "updating itemTitKey from " + itemTitKey() + " to " + value);
    }
    takeStoredValueForKey(value, "itemTitKey");
  }

  public Integer itemValeurTitKey() {
    return (Integer) storedValueForKey("itemValeurTitKey");
  }

  public void setItemValeurTitKey(Integer value) {
    if (_EORepartAppreciationGeneral.LOG.isDebugEnabled()) {
    	_EORepartAppreciationGeneral.LOG.debug( "updating itemValeurTitKey from " + itemValeurTitKey() + " to " + value);
    }
    takeStoredValueForKey(value, "itemValeurTitKey");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluation)storedValueForKey("toEvaluation");
  }

  public void setToEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation value) {
    if (_EORepartAppreciationGeneral.LOG.isDebugEnabled()) {
      _EORepartAppreciationGeneral.LOG.debug("updating toEvaluation from " + toEvaluation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluation oldValue = toEvaluation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluation");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOTplItem toTplItem() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplItem)storedValueForKey("toTplItem");
  }

  public void setToTplItemRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplItem value) {
    if (_EORepartAppreciationGeneral.LOG.isDebugEnabled()) {
      _EORepartAppreciationGeneral.LOG.debug("updating toTplItem from " + toTplItem() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOTplItem oldValue = toTplItem();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTplItem");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTplItem");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur toTplItemValeur() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur)storedValueForKey("toTplItemValeur");
  }

  public void setToTplItemValeurRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur value) {
    if (_EORepartAppreciationGeneral.LOG.isDebugEnabled()) {
      _EORepartAppreciationGeneral.LOG.debug("updating toTplItemValeur from " + toTplItemValeur() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur oldValue = toTplItemValeur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTplItemValeur");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTplItemValeur");
    }
  }
  

  public static EORepartAppreciationGeneral create(EOEditingContext editingContext, Integer evaluationKey
, Integer itemTitKey
, org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation, org.cocktail.fwkcktlgrh.common.metier.EOTplItem toTplItem) {
    EORepartAppreciationGeneral eo = (EORepartAppreciationGeneral) EOUtilities.createAndInsertInstance(editingContext, _EORepartAppreciationGeneral.ENTITY_NAME);    
		eo.setEvaluationKey(evaluationKey);
		eo.setItemTitKey(itemTitKey);
    eo.setToEvaluationRelationship(toEvaluation);
    eo.setToTplItemRelationship(toTplItem);
    return eo;
  }

  public static NSArray<EORepartAppreciationGeneral> fetchAll(EOEditingContext editingContext) {
    return _EORepartAppreciationGeneral.fetchAll(editingContext, null);
  }

  public static NSArray<EORepartAppreciationGeneral> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartAppreciationGeneral.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EORepartAppreciationGeneral> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartAppreciationGeneral.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartAppreciationGeneral> eoObjects = (NSArray<EORepartAppreciationGeneral>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartAppreciationGeneral fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartAppreciationGeneral.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartAppreciationGeneral fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartAppreciationGeneral> eoObjects = _EORepartAppreciationGeneral.fetch(editingContext, qualifier, null);
    EORepartAppreciationGeneral eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartAppreciationGeneral)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_RepartAppreciationGeneral that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartAppreciationGeneral fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartAppreciationGeneral.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EORepartAppreciationGeneral fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartAppreciationGeneral eoObject = _EORepartAppreciationGeneral.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_RepartAppreciationGeneral that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EORepartAppreciationGeneral fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartAppreciationGeneral fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartAppreciationGeneral eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartAppreciationGeneral)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EORepartAppreciationGeneral localInstanceIn(EOEditingContext editingContext, EORepartAppreciationGeneral eo) {
    EORepartAppreciationGeneral localInstance = (eo == null) ? null : (EORepartAppreciationGeneral)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
