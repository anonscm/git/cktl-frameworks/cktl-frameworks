/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IPeriodesMilitaires;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlgrh.modele.Duree;
import org.cocktail.fwkcktlgrh.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EOPeriodesMilitaires extends _EOPeriodesMilitaires implements IPeriodesMilitaires {
	private static Logger log = Logger.getLogger(EOPeriodesMilitaires.class);



	/**
	 * @return le N° de dossier de l'agent
	 */
	public Integer noDossierPers() {
		return toIndividu().noIndividu();
	}

	/**
	 * 
	 * @param ec editing context
	 * @param ind l'individu pour la recherche
	 * @return les périodes militaires valides de cet individu
	 */
	public static NSArray<EOPeriodesMilitaires> periodesMilitairesValidesForIndividu(EOEditingContext ec, EOIndividu ind) {
		if (ind == null) {
			return null;
		}
		EOQualifier qual = ERXQ.equals(EOPeriodesMilitaires.TO_INDIVIDU_KEY, ind);
		qual = ERXQ.and(qual, ERXQ.equals(TEM_VALIDE_KEY, Constantes.VRAI));
		NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(DATE_DEBUT_KEY).array();
		return fetchAll(ec, qual, sorts);
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	// méthodes statiques
	/** recherche les objets de type duree selon certains criteres
	 * @param editingContext
	 * @param entite nom entite concernee
	 * @param qualifier
	 * @param shouldRefresh true si le fetch doit raffraichir les objets 
	 * @return tableau des objets trouves
	 */
	// 13/07/2010 - Ajout d'une méthode pour ne pas faire systématiquement les refresh des objets
	public static NSArray rechercherDureePourEntiteAvecCriteres(EOEditingContext editingContext,String entite,EOQualifier qualifier,boolean shouldRefresh) {
		EOFetchSpecification myFetch = new EOFetchSpecification(entite,qualifier,null);
		myFetch.setRefreshesRefetchedObjects(shouldRefresh);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/** recherche les objets de type duree selon certains criteres : le fetch raffraichit les objets
	 * @param editingContext
	 * @param entite nom entite concernee
	 * @param qualifier
	 * @return tableau des objets trouves
	 */
	public static NSArray rechercherDureePourEntiteAvecCriteres(EOEditingContext editingContext,String entite,EOQualifier qualifier) {
		return rechercherDureePourEntiteAvecCriteres(editingContext,entite,qualifier,true);
	}
	/** Recherche des objets de type dure pour un individu 
	 * @param editingContext
	 * @param nomEntite
	 * @param individu
	 * @param shouldRefresh true si le fetch doit raffraichir les objets 
	 */
	public static NSArray rechercherDureesPourIndividu(EOEditingContext editingContext, String nomEntite,EOIndividu individu,boolean shouldRefresh) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@", new NSArray(individu));
		return rechercherDureePourEntiteAvecCriteres(editingContext,nomEntite,qual,shouldRefresh);	
	}
	/** Recherche des objets de type dure pour un individu  : le fetch raffraichit les objets
	 * @param editingContext
	 * @param nomEntite
	 * @param individu */
	public static NSArray rechercherDureesPourIndividu(EOEditingContext editingContext, String nomEntite,EOIndividu individu) {
		return rechercherDureesPourIndividu(editingContext, nomEntite, individu,true);
	}
	/** Recherche l'objet de type duree pour un individu commencant a une date donnee */
	public static NSArray rechercherDureesPourIndividuAvecDateDebut(EOEditingContext editingContext, String entite,EOIndividu individu, NSTimestamp dateDebut) {
		NSMutableArray args = new NSMutableArray();
		args.addObject(individu);
		args.addObject(dateDebut);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@ and "+DATE_DEBUT_KEY+" = %@", args);
		return rechercherDureePourEntiteAvecCriteres(editingContext,entite,qual);
	}
	/** Recherche l'objet de type dure pour un individu  commencant a une date donnee */
	public static Duree rechercherDureePourIndividuAvecDateDebut(EOEditingContext editingContext, String entite,EOIndividu individu, NSTimestamp dateDebut) {
		try {
			return (Duree)rechercherDureesPourIndividuAvecDateDebut(editingContext,entite,individu,dateDebut).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** Recherche les durees pour un individu  commencant avant une date donnee */
	public static NSArray rechercherDureesPourIndividuAnterieuresADate(EOEditingContext editingContext, String entite,EOIndividu individu, NSTimestamp date) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY+" = '"+Constantes.VRAI+"'", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " <= %@ ", new NSArray(date)));

		return rechercherDureePourEntiteAvecCriteres(editingContext,entite, new EOAndQualifier(qualifiers));
	}
	/** Recherche les durees pour un individu  commencant apres une date donnee */
	public static NSArray rechercherDureesPourIndividuPosterieuresADate(EOEditingContext editingContext, String entite,EOIndividu individu, NSTimestamp date) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " >= %@ ", new NSArray(date)));

		return rechercherDureePourEntiteAvecCriteres(editingContext,entite, new EOAndQualifier(qualifiers));
	}


	/** recherche une duree valide pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param entite	nom de l'entite sur lequel faire le fetch
	 * @param debutPeriode
	 * @param finPeriode	 peut etre nulle
	 * @return durees trouves
	 */
	public static NSArray rechercherDureesPourIndividuEtQualifier(EOEditingContext editingContext,String entite,EOIndividu individu, EOQualifier qualifier) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@ ", new NSArray(individu)));

		qualifiers.addObject(qualifier);

		//		EOQualifier qualifier = SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode, DATE_FIN_KEY,finPeriode);
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}

		return rechercherDureePourEntiteAvecCriteres(editingContext,entite, new EOAndQualifier(qualifiers));
	}


	/** recherche une duree valide pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param entite	nom de l'entite sur lequel faire le fetch
	 * @param debutPeriode
	 * @param finPeriode	 peut etre nulle
	 * @return durees trouves
	 */
	public static NSArray rechercherDureesPourIndividuEtPeriode(EOEditingContext editingContext,String entite,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray qualifiers = new NSMutableArray();

		try {
			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode, DATE_FIN_KEY,finPeriode));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temValide" + " = %@ ", new NSArray(Constantes.VRAI)));
			return rechercherDureePourEntiteAvecCriteres(editingContext,entite, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			System.out
			.println("PeriodePourIndividu.rechercherDureesPourIndividuEtPeriode() SANS TEM VALIDE");
			e.printStackTrace();
			qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode, DATE_FIN_KEY,finPeriode));

			return rechercherDureePourEntiteAvecCriteres(editingContext,entite, new EOAndQualifier(qualifiers));			
		}

	}

	/** recherche une duree valide pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param entite	nom de l'entite sur lequel faire le fetch
	 * @param debutPeriode
	 * @param finPeriode	 peut etre nulle
	 * @param shouldRefresh true si les donnees sont raffraichies
	 * @return durees trouves
	 */
	public static NSArray rechercherDureesPourIndividuEtPeriode(EOEditingContext editingContext,String entite,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean shouldRefresh) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY ,debutPeriode, DATE_FIN_KEY,finPeriode));

			return rechercherDureePourEntiteAvecCriteres(editingContext,entite,new EOAndQualifier(qualifiers),shouldRefresh);
		}
		catch (Exception e) {
			//e.printStackTrace();
			return new NSArray();
		}
	}

	/** recherche les objets dont le nom d'entite est passe en parametre, d'un individu commencant apres la date fournie en parametre */
	public static NSArray rechercherDureesPourEntitePosterieuresADate(EOEditingContext editingContext,String nomEntite,EOIndividu individu,NSTimestamp date) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY+" = '"+Constantes.VRAI+"' ", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " > %@ ", new NSArray(date)));

		EOFetchSpecification myFetch = new EOFetchSpecification(nomEntite,new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	/** recherche les objets dont le nom d'entite est passe en parametre, d'un individu se terminant ant avant la date fournie en parametre */
	public static NSArray rechercherDureesPourEntiteAnterieuresADate(EOEditingContext editingContext,String nomEntite,EOIndividu individu,NSTimestamp date) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY+" = '"+Constantes.VRAI+"' ", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_KEY + " < %@ ", new NSArray(date)));

		EOFetchSpecification myFetch = new EOFetchSpecification(nomEntite,new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

}
