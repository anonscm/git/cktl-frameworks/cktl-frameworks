/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOEnqueteFormation.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOEnqueteFormation extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_EnqueteFormation";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EFO_ANNEE_KEY = "efoAnnee";
	public static final String EFO_CHAMP_LIBRE_KEY = "efoChampLibre";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> EFO_ANNEE = new ERXKey<Integer>("efoAnnee");
	public static final ERXKey<String> EFO_CHAMP_LIBRE = new ERXKey<String>("efoChampLibre");
	// Relationships
	public static final String TO_EVALUATION_KEY = "toEvaluation";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TOS_REPART_ENQ_COMP_KEY = "tosRepartEnqComp";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation> TO_EVALUATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOEvaluation>("toEvaluation");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp> TOS_REPART_ENQ_COMP = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp>("tosRepartEnqComp");

  private static Logger LOG = Logger.getLogger(_EOEnqueteFormation.class);

  public EOEnqueteFormation localInstanceIn(EOEditingContext editingContext) {
    EOEnqueteFormation localInstance = (EOEnqueteFormation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOEnqueteFormation.LOG.isDebugEnabled()) {
    	_EOEnqueteFormation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOEnqueteFormation.LOG.isDebugEnabled()) {
    	_EOEnqueteFormation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer efoAnnee() {
    return (Integer) storedValueForKey("efoAnnee");
  }

  public void setEfoAnnee(Integer value) {
    if (_EOEnqueteFormation.LOG.isDebugEnabled()) {
    	_EOEnqueteFormation.LOG.debug( "updating efoAnnee from " + efoAnnee() + " to " + value);
    }
    takeStoredValueForKey(value, "efoAnnee");
  }

  public String efoChampLibre() {
    return (String) storedValueForKey("efoChampLibre");
  }

  public void setEfoChampLibre(String value) {
    if (_EOEnqueteFormation.LOG.isDebugEnabled()) {
    	_EOEnqueteFormation.LOG.debug( "updating efoChampLibre from " + efoChampLibre() + " to " + value);
    }
    takeStoredValueForKey(value, "efoChampLibre");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOEvaluation)storedValueForKey("toEvaluation");
  }

  public void setToEvaluationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOEvaluation value) {
    if (_EOEnqueteFormation.LOG.isDebugEnabled()) {
      _EOEnqueteFormation.LOG.debug("updating toEvaluation from " + toEvaluation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOEvaluation oldValue = toEvaluation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEvaluation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEvaluation");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOEnqueteFormation.LOG.isDebugEnabled()) {
      _EOEnqueteFormation.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp> tosRepartEnqComp() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp>)storedValueForKey("tosRepartEnqComp");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp> tosRepartEnqComp(EOQualifier qualifier) {
    return tosRepartEnqComp(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp> tosRepartEnqComp(EOQualifier qualifier, boolean fetch) {
    return tosRepartEnqComp(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp> tosRepartEnqComp(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp.TO_ENQUETE_FORMATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartEnqComp();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartEnqCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp object) {
    if (_EOEnqueteFormation.LOG.isDebugEnabled()) {
      _EOEnqueteFormation.LOG.debug("adding " + object + " to tosRepartEnqComp relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartEnqComp");
  }

  public void removeFromTosRepartEnqCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp object) {
    if (_EOEnqueteFormation.LOG.isDebugEnabled()) {
      _EOEnqueteFormation.LOG.debug("removing " + object + " from tosRepartEnqComp relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartEnqComp");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp createTosRepartEnqCompRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_FevRepartEnqComp");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartEnqComp");
    return (org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp) eo;
  }

  public void deleteTosRepartEnqCompRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartEnqComp");
  }

  public void deleteAllTosRepartEnqCompRelationships() {
    Enumeration objects = tosRepartEnqComp().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartEnqCompRelationship((org.cocktail.fwkcktlgrh.common.metier.EOFevRepartEnqComp)objects.nextElement());
    }
  }


  public static EOEnqueteFormation create(EOEditingContext editingContext) {
    EOEnqueteFormation eo = (EOEnqueteFormation) EOUtilities.createAndInsertInstance(editingContext, _EOEnqueteFormation.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EOEnqueteFormation> fetchAll(EOEditingContext editingContext) {
    return _EOEnqueteFormation.fetchAll(editingContext, null);
  }

  public static NSArray<EOEnqueteFormation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEnqueteFormation.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOEnqueteFormation> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOEnqueteFormation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEnqueteFormation> eoObjects = (NSArray<EOEnqueteFormation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOEnqueteFormation fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEnqueteFormation.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEnqueteFormation fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEnqueteFormation> eoObjects = _EOEnqueteFormation.fetch(editingContext, qualifier, null);
    EOEnqueteFormation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOEnqueteFormation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_EnqueteFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEnqueteFormation fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEnqueteFormation.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOEnqueteFormation fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEnqueteFormation eoObject = _EOEnqueteFormation.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_EnqueteFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOEnqueteFormation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEnqueteFormation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEnqueteFormation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEnqueteFormation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOEnqueteFormation localInstanceIn(EOEditingContext editingContext, EOEnqueteFormation eo) {
    EOEnqueteFormation localInstance = (eo == null) ? null : (EOEnqueteFormation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
