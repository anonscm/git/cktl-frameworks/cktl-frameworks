/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOAgentsDroitsServices.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOAgentsDroitsServices extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_AgentsDroitsServices";

	// Attributes
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String NO_INDIVIDU_KEY = "noIndividu";

	public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
	public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
	// Relationships
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_STRUCTURE_KEY = "toStructure";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");

  private static Logger LOG = Logger.getLogger(_EOAgentsDroitsServices.class);

  public EOAgentsDroitsServices localInstanceIn(EOEditingContext editingContext) {
    EOAgentsDroitsServices localInstance = (EOAgentsDroitsServices)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cStructure() {
    return (String) storedValueForKey("cStructure");
  }

  public void setCStructure(String value) {
    if (_EOAgentsDroitsServices.LOG.isDebugEnabled()) {
    	_EOAgentsDroitsServices.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "cStructure");
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey("noIndividu");
  }

  public void setNoIndividu(Integer value) {
    if (_EOAgentsDroitsServices.LOG.isDebugEnabled()) {
    	_EOAgentsDroitsServices.LOG.debug( "updating noIndividu from " + noIndividu() + " to " + value);
    }
    takeStoredValueForKey(value, "noIndividu");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOAgentsDroitsServices.LOG.isDebugEnabled()) {
      _EOAgentsDroitsServices.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey("toStructure");
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOAgentsDroitsServices.LOG.isDebugEnabled()) {
      _EOAgentsDroitsServices.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructure");
    }
  }
  

  public static EOAgentsDroitsServices create(EOEditingContext editingContext, String cStructure
, Integer noIndividu
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure) {
    EOAgentsDroitsServices eo = (EOAgentsDroitsServices) EOUtilities.createAndInsertInstance(editingContext, _EOAgentsDroitsServices.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setNoIndividu(noIndividu);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToStructureRelationship(toStructure);
    return eo;
  }

  public static NSArray<EOAgentsDroitsServices> fetchAll(EOEditingContext editingContext) {
    return _EOAgentsDroitsServices.fetchAll(editingContext, null);
  }

  public static NSArray<EOAgentsDroitsServices> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAgentsDroitsServices.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOAgentsDroitsServices> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOAgentsDroitsServices.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAgentsDroitsServices> eoObjects = (NSArray<EOAgentsDroitsServices>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOAgentsDroitsServices fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAgentsDroitsServices.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAgentsDroitsServices fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAgentsDroitsServices> eoObjects = _EOAgentsDroitsServices.fetch(editingContext, qualifier, null);
    EOAgentsDroitsServices eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOAgentsDroitsServices)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_AgentsDroitsServices that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAgentsDroitsServices fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAgentsDroitsServices.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOAgentsDroitsServices fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAgentsDroitsServices eoObject = _EOAgentsDroitsServices.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_AgentsDroitsServices that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOAgentsDroitsServices fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAgentsDroitsServices fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAgentsDroitsServices eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAgentsDroitsServices)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOAgentsDroitsServices localInstanceIn(EOEditingContext editingContext, EOAgentsDroitsServices eo) {
    EOAgentsDroitsServices localInstance = (eo == null) ? null : (EOAgentsDroitsServices)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
