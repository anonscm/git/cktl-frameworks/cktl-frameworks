/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOContrat.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOContrat extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Contrat";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_CONTRAT_TRAV_KEY = "dDebContratTrav";
	public static final String D_FIN_ANTICIPEE_KEY = "dFinAnticipee";
	public static final String D_FIN_CONTRAT_TRAV_KEY = "dFinContratTrav";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String TEM_ANNULATION_KEY = "temAnnulation";
	public static final String TEM_BUDGET_PROPRE_KEY = "temBudgetPropre";
	public static final String TEM_FONCTIONNAIRE_KEY = "temFonctionnaire";
	public static final String TEM_RECHERCHE_KEY = "temRecherche";
	public static final String TEM_SAUVADET_KEY = "temSauvadet";
	public static final String TOF_CODE_KEY = "tofCode";

	public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_CONTRAT_TRAV = new ERXKey<NSTimestamp>("dDebContratTrav");
	public static final ERXKey<NSTimestamp> D_FIN_ANTICIPEE = new ERXKey<NSTimestamp>("dFinAnticipee");
	public static final ERXKey<NSTimestamp> D_FIN_CONTRAT_TRAV = new ERXKey<NSTimestamp>("dFinContratTrav");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> NO_DOSSIER_PERS = new ERXKey<Integer>("noDossierPers");
	public static final ERXKey<String> TEM_ANNULATION = new ERXKey<String>("temAnnulation");
	public static final ERXKey<String> TEM_BUDGET_PROPRE = new ERXKey<String>("temBudgetPropre");
	public static final ERXKey<String> TEM_FONCTIONNAIRE = new ERXKey<String>("temFonctionnaire");
	public static final ERXKey<String> TEM_RECHERCHE = new ERXKey<String>("temRecherche");
	public static final ERXKey<String> TEM_SAUVADET = new ERXKey<String>("temSauvadet");
	public static final ERXKey<String> TOF_CODE = new ERXKey<String>("tofCode");
	// Relationships
	public static final String TO_CONTRAT_AVENANTS_KEY = "toContratAvenants";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_PERSONNEL_KEY = "toPersonnel";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_TYPE_CONTRAT_TRAVAIL_KEY = "toTypeContratTravail";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant> TO_CONTRAT_AVENANTS = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant>("toContratAvenants");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> TO_PERSONNEL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel>("toPersonnel");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRne");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> TO_TYPE_CONTRAT_TRAVAIL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail>("toTypeContratTravail");

  private static Logger LOG = Logger.getLogger(_EOContrat.class);

  public EOContrat localInstanceIn(EOEditingContext editingContext) {
    EOContrat localInstance = (EOContrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebContratTrav() {
    return (NSTimestamp) storedValueForKey("dDebContratTrav");
  }

  public void setDDebContratTrav(NSTimestamp value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating dDebContratTrav from " + dDebContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebContratTrav");
  }

  public NSTimestamp dFinAnticipee() {
    return (NSTimestamp) storedValueForKey("dFinAnticipee");
  }

  public void setDFinAnticipee(NSTimestamp value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating dFinAnticipee from " + dFinAnticipee() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinAnticipee");
  }

  public NSTimestamp dFinContratTrav() {
    return (NSTimestamp) storedValueForKey("dFinContratTrav");
  }

  public void setDFinContratTrav(NSTimestamp value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating dFinContratTrav from " + dFinContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinContratTrav");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey("noDossierPers");
  }

  public void setNoDossierPers(Integer value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating noDossierPers from " + noDossierPers() + " to " + value);
    }
    takeStoredValueForKey(value, "noDossierPers");
  }

  public String temAnnulation() {
    return (String) storedValueForKey("temAnnulation");
  }

  public void setTemAnnulation(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating temAnnulation from " + temAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "temAnnulation");
  }

  public String temBudgetPropre() {
    return (String) storedValueForKey("temBudgetPropre");
  }

  public void setTemBudgetPropre(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating temBudgetPropre from " + temBudgetPropre() + " to " + value);
    }
    takeStoredValueForKey(value, "temBudgetPropre");
  }

  public String temFonctionnaire() {
    return (String) storedValueForKey("temFonctionnaire");
  }

  public void setTemFonctionnaire(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating temFonctionnaire from " + temFonctionnaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temFonctionnaire");
  }

  public String temRecherche() {
    return (String) storedValueForKey("temRecherche");
  }

  public void setTemRecherche(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating temRecherche from " + temRecherche() + " to " + value);
    }
    takeStoredValueForKey(value, "temRecherche");
  }

  public String temSauvadet() {
    return (String) storedValueForKey("temSauvadet");
  }

  public void setTemSauvadet(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating temSauvadet from " + temSauvadet() + " to " + value);
    }
    takeStoredValueForKey(value, "temSauvadet");
  }

  public String tofCode() {
    return (String) storedValueForKey("tofCode");
  }

  public void setTofCode(String value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
    	_EOContrat.LOG.debug( "updating tofCode from " + tofCode() + " to " + value);
    }
    takeStoredValueForKey(value, "tofCode");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
      _EOContrat.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel toPersonnel() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel)storedValueForKey("toPersonnel");
  }

  public void setToPersonnelRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
      _EOContrat.LOG.debug("updating toPersonnel from " + toPersonnel() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel oldValue = toPersonnel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPersonnel");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPersonnel");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey("toRne");
  }

  public void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
      _EOContrat.LOG.debug("updating toRne from " + toRne() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRne");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toRne");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail toTypeContratTravail() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail)storedValueForKey("toTypeContratTravail");
  }

  public void setToTypeContratTravailRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail value) {
    if (_EOContrat.LOG.isDebugEnabled()) {
      _EOContrat.LOG.debug("updating toTypeContratTravail from " + toTypeContratTravail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail oldValue = toTypeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeContratTravail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeContratTravail");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant> toContratAvenants() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant>)storedValueForKey("toContratAvenants");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant> toContratAvenants(EOQualifier qualifier) {
    return toContratAvenants(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant> toContratAvenants(EOQualifier qualifier, boolean fetch) {
    return toContratAvenants(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant> toContratAvenants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant.TO_CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toContratAvenants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToContratAvenantsRelationship(org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant object) {
    if (_EOContrat.LOG.isDebugEnabled()) {
      _EOContrat.LOG.debug("adding " + object + " to toContratAvenants relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toContratAvenants");
  }

  public void removeFromToContratAvenantsRelationship(org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant object) {
    if (_EOContrat.LOG.isDebugEnabled()) {
      _EOContrat.LOG.debug("removing " + object + " from toContratAvenants relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toContratAvenants");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant createToContratAvenantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_ContratAvenant");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toContratAvenants");
    return (org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant) eo;
  }

  public void deleteToContratAvenantsRelationship(org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toContratAvenants");
    editingContext().deleteObject(object);
  }

  public void deleteAllToContratAvenantsRelationships() {
    Enumeration objects = toContratAvenants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToContratAvenantsRelationship((org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant)objects.nextElement());
    }
  }


  public static EOContrat create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebContratTrav
, NSTimestamp dModification
, Integer noDossierPers
, String temBudgetPropre
, String temFonctionnaire
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel toPersonnel, org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail toTypeContratTravail) {
    EOContrat eo = (EOContrat) EOUtilities.createAndInsertInstance(editingContext, _EOContrat.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebContratTrav(dDebContratTrav);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setTemBudgetPropre(temBudgetPropre);
		eo.setTemFonctionnaire(temFonctionnaire);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToPersonnelRelationship(toPersonnel);
    eo.setToTypeContratTravailRelationship(toTypeContratTravail);
    return eo;
  }

  public static NSArray<EOContrat> fetchAll(EOEditingContext editingContext) {
    return _EOContrat.fetchAll(editingContext, null);
  }

  public static NSArray<EOContrat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOContrat.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOContrat> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOContrat> eoObjects = (NSArray<EOContrat>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOContrat fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContrat.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOContrat fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOContrat> eoObjects = _EOContrat.fetch(editingContext, qualifier, null);
    EOContrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOContrat)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Contrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOContrat fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOContrat.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOContrat fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOContrat eoObject = _EOContrat.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Contrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOContrat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOContrat localInstanceIn(EOEditingContext editingContext, EOContrat eo) {
    EOContrat localInstance = (eo == null) ? null : (EOContrat)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
