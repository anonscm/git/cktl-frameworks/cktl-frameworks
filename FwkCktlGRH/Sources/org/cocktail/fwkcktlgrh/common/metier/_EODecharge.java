/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EODecharge.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EODecharge extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Decharge";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NB_H_DECHARGE_KEY = "nbHDecharge";
	public static final String PERIODE_DECHARGE_KEY = "periodeDecharge";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<java.math.BigDecimal> NB_H_DECHARGE = new ERXKey<java.math.BigDecimal>("nbHDecharge");
	public static final ERXKey<String> PERIODE_DECHARGE = new ERXKey<String>("periodeDecharge");
	// Relationships
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TYPE_DECHARGE_KEY = "typeDecharge";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService> TYPE_DECHARGE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService>("typeDecharge");

  private static Logger LOG = Logger.getLogger(_EODecharge.class);

  public EODecharge localInstanceIn(EOEditingContext editingContext) {
    EODecharge localInstance = (EODecharge)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public java.math.BigDecimal nbHDecharge() {
    return (java.math.BigDecimal) storedValueForKey("nbHDecharge");
  }

  public void setNbHDecharge(java.math.BigDecimal value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating nbHDecharge from " + nbHDecharge() + " to " + value);
    }
    takeStoredValueForKey(value, "nbHDecharge");
  }

  public String periodeDecharge() {
    return (String) storedValueForKey("periodeDecharge");
  }

  public void setPeriodeDecharge(String value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
    	_EODecharge.LOG.debug( "updating periodeDecharge from " + periodeDecharge() + " to " + value);
    }
    takeStoredValueForKey(value, "periodeDecharge");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
      _EODecharge.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService typeDecharge() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService)storedValueForKey("typeDecharge");
  }

  public void setTypeDechargeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService value) {
    if (_EODecharge.LOG.isDebugEnabled()) {
      _EODecharge.LOG.debug("updating typeDecharge from " + typeDecharge() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService oldValue = typeDecharge();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "typeDecharge");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "typeDecharge");
    }
  }
  

  public static EODecharge create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, java.math.BigDecimal nbHDecharge
, String periodeDecharge
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService typeDecharge) {
    EODecharge eo = (EODecharge) EOUtilities.createAndInsertInstance(editingContext, _EODecharge.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNbHDecharge(nbHDecharge);
		eo.setPeriodeDecharge(periodeDecharge);
    eo.setToIndividuRelationship(toIndividu);
    eo.setTypeDechargeRelationship(typeDecharge);
    return eo;
  }

  public static NSArray<EODecharge> fetchAll(EOEditingContext editingContext) {
    return _EODecharge.fetchAll(editingContext, null);
  }

  public static NSArray<EODecharge> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODecharge.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EODecharge> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODecharge.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODecharge> eoObjects = (NSArray<EODecharge>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODecharge fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EODecharge.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODecharge fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODecharge> eoObjects = _EODecharge.fetch(editingContext, qualifier, null);
    EODecharge eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODecharge)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Decharge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODecharge fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EODecharge.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EODecharge fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EODecharge eoObject = _EODecharge.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Decharge that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EODecharge fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODecharge fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODecharge eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODecharge)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EODecharge localInstanceIn(EOEditingContext editingContext, EODecharge eo) {
    EODecharge localInstance = (eo == null) ? null : (EODecharge)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
