/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOFeveDroit.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOFeveDroit extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_FeveDroit";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_CIBLE_KEY = "persIdCible";
	public static final String PERS_ID_TITULAIRE_KEY = "persIdTitulaire";
	public static final String TEM_CIBLE_CDC_KEY = "temCibleCdc";
	public static final String TEM_CIBLE_HERITAGE_KEY = "temCibleHeritage";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> PERS_ID_CIBLE = new ERXKey<Integer>("persIdCible");
	public static final ERXKey<Integer> PERS_ID_TITULAIRE = new ERXKey<Integer>("persIdTitulaire");
	public static final ERXKey<String> TEM_CIBLE_CDC = new ERXKey<String>("temCibleCdc");
	public static final ERXKey<String> TEM_CIBLE_HERITAGE = new ERXKey<String>("temCibleHeritage");
	// Relationships
	public static final String TOS_INDIVIDU_CIBLE_KEY = "tosIndividuCible";
	public static final String TOS_INDIVIDU_TITULAIRE_KEY = "tosIndividuTitulaire";
	public static final String TOS_STRUCTURE_CIBLE_KEY = "tosStructureCible";
	public static final String TOS_STRUCTURE_TITULAIRE_KEY = "tosStructureTitulaire";
	public static final String TO_TYPE_NIVEAU_DROIT_KEY = "toTypeNiveauDroit";

	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TOS_INDIVIDU_CIBLE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("tosIndividuCible");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TOS_INDIVIDU_TITULAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("tosIndividuTitulaire");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TOS_STRUCTURE_CIBLE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("tosStructureCible");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TOS_STRUCTURE_TITULAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("tosStructureTitulaire");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit> TO_TYPE_NIVEAU_DROIT = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit>("toTypeNiveauDroit");

  private static Logger LOG = Logger.getLogger(_EOFeveDroit.class);

  public EOFeveDroit localInstanceIn(EOEditingContext editingContext) {
    EOFeveDroit localInstance = (EOFeveDroit)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
    	_EOFeveDroit.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
    	_EOFeveDroit.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Integer persIdCible() {
    return (Integer) storedValueForKey("persIdCible");
  }

  public void setPersIdCible(Integer value) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
    	_EOFeveDroit.LOG.debug( "updating persIdCible from " + persIdCible() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCible");
  }

  public Integer persIdTitulaire() {
    return (Integer) storedValueForKey("persIdTitulaire");
  }

  public void setPersIdTitulaire(Integer value) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
    	_EOFeveDroit.LOG.debug( "updating persIdTitulaire from " + persIdTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdTitulaire");
  }

  public String temCibleCdc() {
    return (String) storedValueForKey("temCibleCdc");
  }

  public void setTemCibleCdc(String value) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
    	_EOFeveDroit.LOG.debug( "updating temCibleCdc from " + temCibleCdc() + " to " + value);
    }
    takeStoredValueForKey(value, "temCibleCdc");
  }

  public String temCibleHeritage() {
    return (String) storedValueForKey("temCibleHeritage");
  }

  public void setTemCibleHeritage(String value) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
    	_EOFeveDroit.LOG.debug( "updating temCibleHeritage from " + temCibleHeritage() + " to " + value);
    }
    takeStoredValueForKey(value, "temCibleHeritage");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit toTypeNiveauDroit() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit)storedValueForKey("toTypeNiveauDroit");
  }

  public void setToTypeNiveauDroitRelationship(org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit value) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
      _EOFeveDroit.LOG.debug("updating toTypeNiveauDroit from " + toTypeNiveauDroit() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit oldValue = toTypeNiveauDroit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeNiveauDroit");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeNiveauDroit");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividuCible() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)storedValueForKey("tosIndividuCible");
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividuCible(EOQualifier qualifier) {
    return tosIndividuCible(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividuCible(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> results;
      results = tosIndividuCible();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosIndividuCibleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
      _EOFeveDroit.LOG.debug("adding " + object + " to tosIndividuCible relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosIndividuCible");
  }

  public void removeFromTosIndividuCibleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
      _EOFeveDroit.LOG.debug("removing " + object + " from tosIndividuCible relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosIndividuCible");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createTosIndividuCibleRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosIndividuCible");
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteTosIndividuCibleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosIndividuCible");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosIndividuCibleRelationships() {
    Enumeration objects = tosIndividuCible().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosIndividuCibleRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividuTitulaire() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)storedValueForKey("tosIndividuTitulaire");
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividuTitulaire(EOQualifier qualifier) {
    return tosIndividuTitulaire(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividuTitulaire(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> results;
      results = tosIndividuTitulaire();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosIndividuTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
      _EOFeveDroit.LOG.debug("adding " + object + " to tosIndividuTitulaire relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosIndividuTitulaire");
  }

  public void removeFromTosIndividuTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
      _EOFeveDroit.LOG.debug("removing " + object + " from tosIndividuTitulaire relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosIndividuTitulaire");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createTosIndividuTitulaireRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosIndividuTitulaire");
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteTosIndividuTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosIndividuTitulaire");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosIndividuTitulaireRelationships() {
    Enumeration objects = tosIndividuTitulaire().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosIndividuTitulaireRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructureCible() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)storedValueForKey("tosStructureCible");
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructureCible(EOQualifier qualifier) {
    return tosStructureCible(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructureCible(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> results;
      results = tosStructureCible();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosStructureCibleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
      _EOFeveDroit.LOG.debug("adding " + object + " to tosStructureCible relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosStructureCible");
  }

  public void removeFromTosStructureCibleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
      _EOFeveDroit.LOG.debug("removing " + object + " from tosStructureCible relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosStructureCible");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createTosStructureCibleRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosStructureCible");
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteTosStructureCibleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosStructureCible");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosStructureCibleRelationships() {
    Enumeration objects = tosStructureCible().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosStructureCibleRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOStructure)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructureTitulaire() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)storedValueForKey("tosStructureTitulaire");
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructureTitulaire(EOQualifier qualifier) {
    return tosStructureTitulaire(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructureTitulaire(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> results;
      results = tosStructureTitulaire();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosStructureTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
      _EOFeveDroit.LOG.debug("adding " + object + " to tosStructureTitulaire relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosStructureTitulaire");
  }

  public void removeFromTosStructureTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    if (_EOFeveDroit.LOG.isDebugEnabled()) {
      _EOFeveDroit.LOG.debug("removing " + object + " from tosStructureTitulaire relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosStructureTitulaire");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createTosStructureTitulaireRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosStructureTitulaire");
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteTosStructureTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosStructureTitulaire");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosStructureTitulaireRelationships() {
    Enumeration objects = tosStructureTitulaire().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosStructureTitulaireRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOStructure)objects.nextElement());
    }
  }


  public static EOFeveDroit create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persIdCible
, Integer persIdTitulaire
, String temCibleCdc
, String temCibleHeritage
, org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit toTypeNiveauDroit) {
    EOFeveDroit eo = (EOFeveDroit) EOUtilities.createAndInsertInstance(editingContext, _EOFeveDroit.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdCible(persIdCible);
		eo.setPersIdTitulaire(persIdTitulaire);
		eo.setTemCibleCdc(temCibleCdc);
		eo.setTemCibleHeritage(temCibleHeritage);
    eo.setToTypeNiveauDroitRelationship(toTypeNiveauDroit);
    return eo;
  }

  public static NSArray<EOFeveDroit> fetchAll(EOEditingContext editingContext) {
    return _EOFeveDroit.fetchAll(editingContext, null);
  }

  public static NSArray<EOFeveDroit> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOFeveDroit.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOFeveDroit> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOFeveDroit.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOFeveDroit> eoObjects = (NSArray<EOFeveDroit>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOFeveDroit fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFeveDroit.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFeveDroit fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOFeveDroit> eoObjects = _EOFeveDroit.fetch(editingContext, qualifier, null);
    EOFeveDroit eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOFeveDroit)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_FeveDroit that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFeveDroit fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFeveDroit.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOFeveDroit fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOFeveDroit eoObject = _EOFeveDroit.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_FeveDroit that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOFeveDroit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFeveDroit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFeveDroit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFeveDroit)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOFeveDroit localInstanceIn(EOEditingContext editingContext, EOFeveDroit eo) {
    EOFeveDroit localInstance = (eo == null) ? null : (EOFeveDroit)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
