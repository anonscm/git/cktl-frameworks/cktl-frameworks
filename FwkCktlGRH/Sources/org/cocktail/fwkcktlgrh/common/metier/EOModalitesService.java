/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

/**
 * Classe représente les modalités de service d'un agent 
 * 
 * @author Chama LAATIK
 */
public class EOModalitesService extends _EOModalitesService {
  
	private static final long serialVersionUID = 1566854549649164148L;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOModalitesService.class);
	
	/**
	 * Renvoie la liste des modalités de service pour l'année universitaire d'un enseignant 
	 * @param ec : editing context
	 * @param individu : enseignant
	 * @return les modalités de service de l'enseignant
	 */ 
	@SuppressWarnings("unchecked")
	public static NSArray<EOModalitesService> rechercherModaliteServiceCourantePourIndividu(EOEditingContext ec, EOIndividu individu, String annee) {	
		NSArray<EOSortOrdering> sortOrderingsAsc = new NSMutableArray<EOSortOrdering>();
		sortOrderingsAsc.add(EOSortOrdering.sortOrderingWithKey(
				DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));
		
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(VALIDE_KEY, Constantes.VRAI),
						ERXQ.equals(ERXQ.keyPath(EOModalitesService.INDIVIDU_KEY, EOIndividu.NO_INDIVIDU_KEY), individu.noIndividu()), 
						ERXQ.or(
							GRHUtilities.qualifierDateDansAnneeUniversitaire(EOModalitesService.DATE_DEBUT, annee),
							GRHUtilities.qualifierDateDansAnneeUniversitaire(EOModalitesService.DATE_FIN, annee)
						)
				);
		
		return fetchAll(ec, qualifier, sortOrderingsAsc);
	}
	
	/**
	 * Renvoie la liste des modalités courante de service
	 * @param ec : editing context
	 * @param individu : enseignant
	 * @return les modalités de service de l'enseignant
	 */ 
	@SuppressWarnings("unchecked")
	public static EOModalitesService rechercherModaliteServiceCourantePourIndividu(EOEditingContext ec, EOIndividu individu) {	
		NSArray<EOSortOrdering> sortOrderingsAsc = new NSMutableArray<EOSortOrdering>();
		sortOrderingsAsc.add(EOSortOrdering.sortOrderingWithKey(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));
		
		EOQualifier qualifier = 
				ERXQ.and(
					EOModalitesService.VALIDE.eq(Constantes.VRAI),
					EOModalitesService.INDIVIDU.eq(individu),
					EOModalitesService.DATE_DEBUT.lessThanOrEqualTo(MyDateCtrl.getDateJour()),
					ERXQ.or(
						EOModalitesService.DATE_FIN.greaterThanOrEqualTo(MyDateCtrl.getDateJour()),
						EOModalitesService.DATE_FIN.isNull()
					)
				);
		
		return fetchFirstByQualifier(ec, qualifier, sortOrderingsAsc);
	}
	
	/**
	 * 
	 * @param ec editing context
	 * @param ind l'individu pour la recherche
	 * @return les modalités de service validées de cet individu
	 */
	public static NSArray<EOModalitesService> modalitesServiceValidesForIndividu(EOEditingContext ec, EOIndividu ind) {
		if (ind == null) {
			return null;
		}
		EOQualifier qual = ERXQ.equals(EOModalitesService.INDIVIDU_KEY, ind);
		qual = ERXQ.and(qual, ERXQ.equals(VALIDE_KEY, Constantes.VRAI));
		NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(DATE_DEBUT_KEY).array();
		return fetchAll(ec, qual, sorts);
	}
	
	/**
	 * Renvoie l'absence correspondante à la modalité dans la table MANGUE.ABSENCES
	 * @return l'absence
	 */
	public EOAbsence getAbsence() {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(EOAbsence.INDIVIDU_KEY, this.individu()),
						ERXQ.equals(EOAbsence.C_TYPE_ABSENCE_KEY, this.toTypeAbsence().cTypeAbsence()),
						ERXQ.equals(EOAbsence.DATE_DEBUT_KEY, this.dateDebut()),
						ERXQ.equals(EOAbsence.DATE_FIN_KEY, this.dateFin()),
						ERXQ.equals(EOAbsence.TEM_VALIDE_KEY, Constantes.VRAI)
						);
		
		return EOAbsence.fetchFirstByQualifier(this.editingContext(), qualifier);
	}
	
	/**
	 * Est-ce que la modalité est de type CRCT?
	 * @return Vrai/Faux
	 */
	public boolean isTypeCRCT() {
		return EOTypeAbsence.TYPE_CRCT.equals(this.type());
	}
	
	/**
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
	    this.validateObjectMetier();
	    validateBeforeTransactionSave();
	    super.validateForInsert();
	}
	
	/**
	 * 
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
	    this.validateObjectMetier();
	    validateBeforeTransactionSave();
	    super.validateForUpdate();
	}
	
	/**
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
	    super.validateForDelete();
	}
	
	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		
	}
	
	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		
	}
}