/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EODelegation.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EODelegation extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Delegation";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CONV_DELEGATION_KEY = "dConvDelegation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIEU_DELEGATION_KEY = "lieuDelegation";
	public static final String MONTANT_DELEGATION_KEY = "montantDelegation";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NUM_QUOTITE_KEY = "numQuotite";
	public static final String TEM_ANNUEL_DELEGATION_KEY = "temAnnuelDelegation";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
	public static final ERXKey<NSTimestamp> DATE_ARRETE = new ERXKey<NSTimestamp>("dateArrete");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_CONV_DELEGATION = new ERXKey<NSTimestamp>("dConvDelegation");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> LIEU_DELEGATION = new ERXKey<String>("lieuDelegation");
	public static final ERXKey<java.math.BigDecimal> MONTANT_DELEGATION = new ERXKey<java.math.BigDecimal>("montantDelegation");
	public static final ERXKey<String> NO_ARRETE = new ERXKey<String>("noArrete");
	public static final ERXKey<Integer> NUM_QUOTITE = new ERXKey<Integer>("numQuotite");
	public static final ERXKey<String> TEM_ANNUEL_DELEGATION = new ERXKey<String>("temAnnuelDelegation");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_ABSENCE_KEY = "toAbsence";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_TYPE_MOD_DELEGATION_KEY = "toTypeModDelegation";
	public static final String TO_TYPE_MOTIF_DELEGATION_KEY = "toTypeMotifDelegation";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAbsence> TO_ABSENCE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAbsence>("toAbsence");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRne");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeModDelegation> TO_TYPE_MOD_DELEGATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeModDelegation>("toTypeModDelegation");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeMotDelegation> TO_TYPE_MOTIF_DELEGATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeMotDelegation>("toTypeMotifDelegation");

  private static Logger LOG = Logger.getLogger(_EODelegation.class);

  public EODelegation localInstanceIn(EOEditingContext editingContext) {
    EODelegation localInstance = (EODelegation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dConvDelegation() {
    return (NSTimestamp) storedValueForKey("dConvDelegation");
  }

  public void setDConvDelegation(NSTimestamp value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating dConvDelegation from " + dConvDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "dConvDelegation");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lieuDelegation() {
    return (String) storedValueForKey("lieuDelegation");
  }

  public void setLieuDelegation(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating lieuDelegation from " + lieuDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuDelegation");
  }

  public java.math.BigDecimal montantDelegation() {
    return (java.math.BigDecimal) storedValueForKey("montantDelegation");
  }

  public void setMontantDelegation(java.math.BigDecimal value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating montantDelegation from " + montantDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "montantDelegation");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public Integer numQuotite() {
    return (Integer) storedValueForKey("numQuotite");
  }

  public void setNumQuotite(Integer value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating numQuotite from " + numQuotite() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotite");
  }

  public String temAnnuelDelegation() {
    return (String) storedValueForKey("temAnnuelDelegation");
  }

  public void setTemAnnuelDelegation(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating temAnnuelDelegation from " + temAnnuelDelegation() + " to " + value);
    }
    takeStoredValueForKey(value, "temAnnuelDelegation");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
    	_EODelegation.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOAbsence toAbsence() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOAbsence)storedValueForKey("toAbsence");
  }

  public void setToAbsenceRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAbsence value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
      _EODelegation.LOG.debug("updating toAbsence from " + toAbsence() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOAbsence oldValue = toAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAbsence");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toAbsence");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
      _EODelegation.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey("toRne");
  }

  public void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
      _EODelegation.LOG.debug("updating toRne from " + toRne() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRne");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toRne");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeModDelegation toTypeModDelegation() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeModDelegation)storedValueForKey("toTypeModDelegation");
  }

  public void setToTypeModDelegationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeModDelegation value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
      _EODelegation.LOG.debug("updating toTypeModDelegation from " + toTypeModDelegation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeModDelegation oldValue = toTypeModDelegation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeModDelegation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeModDelegation");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeMotDelegation toTypeMotifDelegation() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeMotDelegation)storedValueForKey("toTypeMotifDelegation");
  }

  public void setToTypeMotifDelegationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeMotDelegation value) {
    if (_EODelegation.LOG.isDebugEnabled()) {
      _EODelegation.LOG.debug("updating toTypeMotifDelegation from " + toTypeMotifDelegation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeMotDelegation oldValue = toTypeMotifDelegation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeMotifDelegation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeMotifDelegation");
    }
  }
  

  public static EODelegation create(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlpersonne.common.metier.EOTypeModDelegation toTypeModDelegation, org.cocktail.fwkcktlpersonne.common.metier.EOTypeMotDelegation toTypeMotifDelegation) {
    EODelegation eo = (EODelegation) EOUtilities.createAndInsertInstance(editingContext, _EODelegation.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToTypeModDelegationRelationship(toTypeModDelegation);
    eo.setToTypeMotifDelegationRelationship(toTypeMotifDelegation);
    return eo;
  }

  public static NSArray<EODelegation> fetchAll(EOEditingContext editingContext) {
    return _EODelegation.fetchAll(editingContext, null);
  }

  public static NSArray<EODelegation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODelegation.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EODelegation> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODelegation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODelegation> eoObjects = (NSArray<EODelegation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODelegation fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EODelegation.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODelegation fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODelegation> eoObjects = _EODelegation.fetch(editingContext, qualifier, null);
    EODelegation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODelegation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Delegation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODelegation fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EODelegation.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EODelegation fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EODelegation eoObject = _EODelegation.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Delegation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EODelegation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODelegation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODelegation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODelegation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EODelegation localInstanceIn(EOEditingContext editingContext, EODelegation eo) {
    EODelegation localInstance = (eo == null) ? null : (EODelegation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
