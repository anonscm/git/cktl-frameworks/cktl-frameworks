/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOTplOnglet.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOTplOnglet extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_TplOnglet";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_CONTRACTUEL_KEY = "temContractuel";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TON_CODE_KEY = "tonCode";
	public static final String TON_COMMENTAIRE_KEY = "tonCommentaire";
	public static final String TON_LIBELLE_KEY = "tonLibelle";
	public static final String TON_POSITION_KEY = "tonPosition";

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_VAL = new ERXKey<NSTimestamp>("dDebVal");
	public static final ERXKey<NSTimestamp> D_FIN_VAL = new ERXKey<NSTimestamp>("dFinVal");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> TEM_CONTRACTUEL = new ERXKey<String>("temContractuel");
	public static final ERXKey<String> TEM_TITULAIRE = new ERXKey<String>("temTitulaire");
	public static final ERXKey<String> TON_CODE = new ERXKey<String>("tonCode");
	public static final ERXKey<String> TON_COMMENTAIRE = new ERXKey<String>("tonCommentaire");
	public static final ERXKey<String> TON_LIBELLE = new ERXKey<String>("tonLibelle");
	public static final ERXKey<Integer> TON_POSITION = new ERXKey<Integer>("tonPosition");
	// Relationships
	public static final String TOS_TPL_BLOC_KEY = "tosTplBloc";
	public static final String TO_TPL_FICHE_KEY = "toTplFiche";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc> TOS_TPL_BLOC = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc>("tosTplBloc");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplFiche> TO_TPL_FICHE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOTplFiche>("toTplFiche");

  private static Logger LOG = Logger.getLogger(_EOTplOnglet.class);

  public EOTplOnglet localInstanceIn(EOEditingContext editingContext) {
    EOTplOnglet localInstance = (EOTplOnglet)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
    	_EOTplOnglet.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey("dDebVal");
  }

  public void setDDebVal(NSTimestamp value) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
    	_EOTplOnglet.LOG.debug( "updating dDebVal from " + dDebVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVal");
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey("dFinVal");
  }

  public void setDFinVal(NSTimestamp value) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
    	_EOTplOnglet.LOG.debug( "updating dFinVal from " + dFinVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVal");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
    	_EOTplOnglet.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String temContractuel() {
    return (String) storedValueForKey("temContractuel");
  }

  public void setTemContractuel(String value) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
    	_EOTplOnglet.LOG.debug( "updating temContractuel from " + temContractuel() + " to " + value);
    }
    takeStoredValueForKey(value, "temContractuel");
  }

  public String temTitulaire() {
    return (String) storedValueForKey("temTitulaire");
  }

  public void setTemTitulaire(String value) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
    	_EOTplOnglet.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temTitulaire");
  }

  public String tonCode() {
    return (String) storedValueForKey("tonCode");
  }

  public void setTonCode(String value) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
    	_EOTplOnglet.LOG.debug( "updating tonCode from " + tonCode() + " to " + value);
    }
    takeStoredValueForKey(value, "tonCode");
  }

  public String tonCommentaire() {
    return (String) storedValueForKey("tonCommentaire");
  }

  public void setTonCommentaire(String value) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
    	_EOTplOnglet.LOG.debug( "updating tonCommentaire from " + tonCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "tonCommentaire");
  }

  public String tonLibelle() {
    return (String) storedValueForKey("tonLibelle");
  }

  public void setTonLibelle(String value) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
    	_EOTplOnglet.LOG.debug( "updating tonLibelle from " + tonLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "tonLibelle");
  }

  public Integer tonPosition() {
    return (Integer) storedValueForKey("tonPosition");
  }

  public void setTonPosition(Integer value) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
    	_EOTplOnglet.LOG.debug( "updating tonPosition from " + tonPosition() + " to " + value);
    }
    takeStoredValueForKey(value, "tonPosition");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOTplFiche toTplFiche() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplFiche)storedValueForKey("toTplFiche");
  }

  public void setToTplFicheRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplFiche value) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
      _EOTplOnglet.LOG.debug("updating toTplFiche from " + toTplFiche() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOTplFiche oldValue = toTplFiche();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTplFiche");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTplFiche");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc> tosTplBloc() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc>)storedValueForKey("tosTplBloc");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc> tosTplBloc(EOQualifier qualifier) {
    return tosTplBloc(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc> tosTplBloc(EOQualifier qualifier, boolean fetch) {
    return tosTplBloc(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc> tosTplBloc(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.EOTplBloc.TO_TPL_ONGLET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.EOTplBloc.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosTplBloc();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOTplBloc>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosTplBlocRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplBloc object) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
      _EOTplOnglet.LOG.debug("adding " + object + " to tosTplBloc relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "tosTplBloc");
  }

  public void removeFromTosTplBlocRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplBloc object) {
    if (_EOTplOnglet.LOG.isDebugEnabled()) {
      _EOTplOnglet.LOG.debug("removing " + object + " from tosTplBloc relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosTplBloc");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOTplBloc createTosTplBlocRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_TplBloc");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "tosTplBloc");
    return (org.cocktail.fwkcktlgrh.common.metier.EOTplBloc) eo;
  }

  public void deleteTosTplBlocRelationship(org.cocktail.fwkcktlgrh.common.metier.EOTplBloc object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "tosTplBloc");
    editingContext().deleteObject(object);
  }

  public void deleteAllTosTplBlocRelationships() {
    Enumeration objects = tosTplBloc().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosTplBlocRelationship((org.cocktail.fwkcktlgrh.common.metier.EOTplBloc)objects.nextElement());
    }
  }


  public static EOTplOnglet create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String temContractuel
, String temTitulaire
, String tonCode
, String tonLibelle
, Integer tonPosition
) {
    EOTplOnglet eo = (EOTplOnglet) EOUtilities.createAndInsertInstance(editingContext, _EOTplOnglet.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemContractuel(temContractuel);
		eo.setTemTitulaire(temTitulaire);
		eo.setTonCode(tonCode);
		eo.setTonLibelle(tonLibelle);
		eo.setTonPosition(tonPosition);
    return eo;
  }

  public static NSArray<EOTplOnglet> fetchAll(EOEditingContext editingContext) {
    return _EOTplOnglet.fetchAll(editingContext, null);
  }

  public static NSArray<EOTplOnglet> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTplOnglet.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOTplOnglet> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTplOnglet.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTplOnglet> eoObjects = (NSArray<EOTplOnglet>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTplOnglet fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTplOnglet.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTplOnglet fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTplOnglet> eoObjects = _EOTplOnglet.fetch(editingContext, qualifier, null);
    EOTplOnglet eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTplOnglet)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_TplOnglet that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTplOnglet fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTplOnglet.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOTplOnglet fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTplOnglet eoObject = _EOTplOnglet.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_TplOnglet that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOTplOnglet fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTplOnglet fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTplOnglet eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTplOnglet)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOTplOnglet localInstanceIn(EOEditingContext editingContext, EOTplOnglet eo) {
    EOTplOnglet localInstance = (eo == null) ? null : (EOTplOnglet)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
