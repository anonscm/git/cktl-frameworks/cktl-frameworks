/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IIndividuFormations;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOIndividuFormations extends _EOIndividuFormations implements IIndividuFormations {
  private static Logger log = Logger.getLogger(EOIndividuFormations.class);

  public EOIndividuFormations() {
      super();
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }

  
	// ajouts

	public static final boolean IS_GESTION_PERIODE = true;
	public static final boolean IS_GESTION_DUREE = true;

	/**
	 * Retrouver les formations d'un individu. Les objets retournes sont issus de
	 * l'entite <code>IndividuFormations</code>. Le classement est inversement
	 * chronologique a la date de debut.
	 * 
	 * @param ec
	 * @param recIndividu
	 */
	public static NSArray findRecordsInContext(
				EOEditingContext ec, EOIndividu recIndividu) {
		return EOIndividuFormations.fetchAll(
					ec,
					CktlDataBus.newCondition(
							EOIndividuFormations.TO_INDIVIDU_KEY + " = %@ ",
							new NSArray(recIndividu)),
							CktlSort.newSort(EOIndividuFormations.D_DEB_FORMATION_KEY, CktlSort.Descending));
	}

	@Override
	public String champLibre() {
		return llFormation();
	}

	@Override
	public void setChampLibre(String champLibre) {
		setLlFormation(champLibre);
	}

	@Override
	public boolean isGestionPeriode() {
		return IS_GESTION_PERIODE;
	}

	@Override
	public NSTimestamp dDebut() {
		return dDebFormation();
	}

	@Override
	public void setDDebut(NSTimestamp dDebut) {
		setDDebFormation(dDebut);
	}

	@Override
	public NSTimestamp dFin() {
		return dFinFormation();
	}

	@Override
	public void setDFin(NSTimestamp dFin) {
		setDFinFormation(dFin);
	}

	@Override
	public boolean isGestionDuree() {
		return IS_GESTION_DUREE;
	}


	public NSArray<EOIndividuFormations> tosIndividuFormations(EOIndividu individu, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray<EOIndividuFormations> results;
		EOQualifier fullQualifier;
		EOQualifier qual = new EOKeyValueQualifier(EOIndividuFormations.TO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, individu);

		if (qualifier == null) {
			fullQualifier = qual;
		} else {

			fullQualifier = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					qual, qualifier
			}));
		}

		results = EOIndividuFormations.fetchAll(individu.editingContext(), fullQualifier, sortOrderings);

		return results;
	}
}
