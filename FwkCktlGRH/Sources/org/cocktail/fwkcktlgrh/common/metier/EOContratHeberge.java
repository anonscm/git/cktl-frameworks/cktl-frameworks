/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import static er.extensions.eof.ERXQ.is;
import static er.extensions.eof.ERXQ.keyPath;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IContratHeberge;
import org.cocktail.fwkcktlgrh.common.metier.workflow.WorkflowHeberge;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;
import org.cocktail.fwkcktlpersonne.common.metier.workflow.IWorkflowItem;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXThreadStorage;

public class EOContratHeberge extends _EOContratHeberge implements IWorkflowItem, IContratHeberge {
    private static Logger log = Logger.getLogger(EOContratHeberge.class);

    public static final String TEM_VALIDATION_VALIDE = "O";
    public static final String TEM_VALIDATION_EN_COURS = "N";
    public static final String TEM_VALIDATION_ANNULE = "A";

    @Override
    public void validateForInsert() throws ValidationException {
        super.validateForInsert();
        Integer persIdModif = (Integer) ERXThreadStorage.valueForKey(PersonneApplicationUser.PERS_ID_CURRENT_USER_STORAGE_KEY);
        WorkflowHeberge.instance().passerEnCoursDeValidation(this, persIdModif);
    }
    
    @Override
    public void validateForSave() throws NSValidation.ValidationException {
        super.validateForSave();
        if (commentaire() != null && commentaire().length() > 2000) {
            throw new NSValidation.ValidationException("Le commentaire ne peut dépasser 2000 caractères");
        }
        // Modification du 29/08/11
        // Vérification de la non nullité de la date de début
        if (dateDebut() == null) {
        	throw new NSValidation.ValidationException("La date de début de l'affectation ne peut pas être nulle.");
        }
        // Modification du 29/08/11
        // Vérification que la date de fin n'est pas avant la date de début.
        if (dateFin() != null) {
	        if (dateFin().before(dateDebut()) == true){
	        	throw new NSValidation.ValidationException("La date de fin ne peut pas être antérieure à la date de début.");
	        }
        }
        
        if (EOCarriere.aCarriereEnCoursSurPeriode(editingContext(), toPersonnel().toIndividu(), dateDebut(), dateFin())) {
            throw new NSValidation.ValidationException("Dates invalides, l'individu a un segment de carrière pendant cette période");
        }
        if (EOContrat.aContratEnCoursSurPeriode(editingContext(), toPersonnel().toIndividu(), dateDebut(), dateFin())) {
            throw new NSValidation.ValidationException("Dates invalides, l'individu a un contrat pendant cette période");
        }
        if (editingContext().parentObjectStore() instanceof EOObjectStoreCoordinator) {
            NSArray contratsHeberges = rechercherContratsPourIndividuEtPeriode(editingContext(), toPersonnel().toIndividu(), dateDebut(), dateFin());
            if (contratsHeberges.count() > 1 || (contratsHeberges.count() == 1 && !ERXEOControlUtilities.eoEquals((EOContratHeberge) contratsHeberges.objectAtIndex(0), this))) {
                throw new NSValidation.ValidationException("Dates invalides, l'individu a un autre contrat hébergé pendant cette période");
            }
        }
    }

    public static void verifContratsHebergeInMemory(EOContratHeberge contrat, NSArray<EOContratHeberge> autres) {
        NSArray<EOContratHeberge> contrasPareil = ERXQ.filtered(autres, qualifierForIndividuEtPeriode(
                contrat.toPersonnel().toIndividu(), contrat.dateDebut(), contrat.dateFin()));
        if (contrasPareil.count() > 0 && !ERXEOControlUtilities.eoEquals(contrasPareil.lastObject(), contrat)) {
            throw new NSValidation.ValidationException("Dates invalides, l'individu a un autre contrat hébergé pendant cette période");
        }
    }

    public static EOContratHeberge creerHeberger(EOEditingContext ec, EOIndividu individu) {
    	
//        EOContratHeberge contrat = create(ec, new NSTimestamp(), TEM_VALIDATION_EN_COURS, null);
        EOContratHeberge contrat = create(ec, new NSTimestamp(), TEM_VALIDATION_VALIDE, null);
        
        contrat.setCommentaire("Convention d'hébergé créée dans AGhrum");

        EOPersonnel personnel = EOPersonnel.fetchFirstByQualifier(ec, ERXQ.equals(EOPersonnel.TO_INDIVIDU_KEY, individu));
        if (personnel == null) {
            personnel = EOPersonnel.creerPersonnel(ec, individu);
        }
        contrat.setToPersonnelRelationship(personnel);
        return contrat;
        
    }
    /**
    @Deprecated Utiliser plutot qualifierForIndividuEtPeriodeCourant qui renvoie tous les contrats courants
    * 				sur la période demandée
    */
    private static EOQualifier qualifierForIndividuEtPeriode(EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {
        EOQualifier qual = is(keyPath(EOContratHeberge.TO_PERSONNEL_KEY, 
                                            EOPersonnel.TO_INDIVIDU_KEY, 
                                            EOIndividu.NO_INDIVIDU_KEY), individu.noIndividu()).
                           and(
                                   is(TEM_VALIDE_KEY, TEM_VALIDATION_VALIDE).or(is(TEM_VALIDE_KEY, TEM_VALIDATION_EN_COURS))
                           );
        if (dateDebut != null) {
            qual = ERXQ.and(
            				qual, 
            				GRHUtilities.qualifierPourPeriode("dateDebut", dateDebut, "dateFin", dateFin)
            			);
        }
        return qual;
    }
    
    /** 
     * Retourne les contrats h&eacute;berg&eacute;s d'un individu pour la p&eacute;riode pass&eacute;e en param&egrave;tre 
     */
    public static NSArray<EOContratHeberge> rechercherContratsPourIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin) {
        EOQualifier qual = qualifierForIndividuEtPeriode(individu, dateDebut, dateFin);
        ERXFetchSpecification<EOContratHeberge> fs = new ERXFetchSpecification<EOContratHeberge>(
                EOContratHeberge.ENTITY_NAME, qual, ERXS.desc(DATE_DEBUT_KEY).array());
        fs.setRefreshesRefetchedObjects(true);
        fs.setIncludeEditingContextChanges(false);
        fs.setUsesDistinct(true);
        return fs.fetchObjects(editingContext);
    }   

    public static boolean aContratHeberge(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
        return rechercherContratsPourIndividuEtPeriode(editingContext, individu, debutPeriode, finPeriode).count() > 0;
    }
    
    public boolean estValide() {
        return TEM_VALIDATION_VALIDE.equals(temValide());
    }
    
    public boolean estAnnule() {
        return TEM_VALIDATION_ANNULE.equals(temValide());
    }
    
    public boolean estEnCoursDeValidation() {
        return TEM_VALIDATION_EN_COURS.equals(temValide());
    }

    public String etat() {
        return temValide();
    }

    public void setEtat(String etat) {
        setTemValide(etat);
    }

}
