/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IPasse;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlgrh.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EOPasse extends _EOPasse implements IPasse {
	private static Logger log = Logger.getLogger(EOPasse.class);


	public static final String TYPE_FCT_PUBLIQUE_ETAT = "E";
	public static final String TYPE_FCT_PUBLIQUE_TERRITORIALE = "T";
	public static final String TYPE_FCT_PUBLIQUE_HOSPITALIERE = "H";

	public static final String TYPE_SERVICE_VALIDES = "S";
	public static final String TYPE_SERVICE_EAS = "T";
	public static final String TYPE_SERVICE_ENGAGE = "E";
	public static final String TYPE_SERVICE_ELEVE = "L";
	public static final String TYPE_SERVICE_MILITAIRE = "C";
	
	public static final String TYPE_TEMPS_PARTIEL = "P";
	public static final String TYPE_TEMPS_INCOMPLET = "I";
	public static final String TYPE_TEMPS_COMPLET = "C";
	
	public static final EOSortOrdering SORT_DATE_DESC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_DATE_DESC = new NSArray(SORT_DATE_DESC);


	/**
	 * @return le N° de dossier de l'agent
	 */
	public Integer noDossierPers() {
		return toIndividu().noIndividu();
	}

	/**
	 * 
	 * @return le libellé de la catégorie
	 */
	public String libelleCategorie() {
		String libelleCategorie = "";
		
		if (toCategorie() != null) {
			libelleCategorie = toCategorie().lcCategorie();
		}
		
		return libelleCategorie;
	}


	/**
	 * 
	 * @return le libellé du type de population
	 */
	public String libelleTypePopulation() {
		return toTypePopulation().codeEtLibelle();
	}

	/**
	 * 
	 * @param ec editing context
	 * @param ind l'individu pour la recherche
	 * @return les passés valides de cet individu
	 */
	public static NSArray<EOPasse> passesValidesForIndividu(EOEditingContext ec, EOIndividu ind) {
		if (ind == null) {
			return null;
		}
		EOQualifier qual = ERXQ.equals(EOPasse.TO_INDIVIDU_KEY, ind);
		qual = ERXQ.and(qual, ERXQ.equals(TEM_VALIDE_KEY, Constantes.VRAI));
		NSArray<EOSortOrdering> sorts = ERXS.descInsensitive(DATE_DEBUT_KEY).array();
		return fetchAll(ec, qual, sorts);
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}


	public static EOPasse rechercherPassePourAvenant(EOEditingContext ec, EOContratAvenant avenant) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CONTRAT_AVENANT_KEY + " = %@ ", new NSArray(avenant)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@ ", new NSArray(Constantes.VRAI)));
		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
		
	}



	public boolean estTitulaire() {
		return temTitulaire() != null && temTitulaire().equals(Constantes.VRAI);
	}
	public void setEstTitulaire(boolean aBool) {
		if (aBool) {
			setTemTitulaire(Constantes.VRAI);
		} else {
			setTemTitulaire(Constantes.FAUX);
		}
	}
	
	
	public boolean estFonctionPubliqueEtat() {
		return pasTypeFctPublique() != null && pasTypeFctPublique().equals(TYPE_FCT_PUBLIQUE_ETAT);
	}
	public boolean estFonctionPubliqueHospitaliere() {
		return pasTypeFctPublique() != null && pasTypeFctPublique().equals(TYPE_FCT_PUBLIQUE_HOSPITALIERE);
	}
	public boolean estFonctionPubliqueTerritoriale() {
		return pasTypeFctPublique() != null && pasTypeFctPublique().equals(TYPE_FCT_PUBLIQUE_TERRITORIALE);
	}
	
	public void setEstFonctionPubliqueEtat() {
		setPasTypeFctPublique(TYPE_FCT_PUBLIQUE_ETAT);
	}
	public void setEstFonctionPubliqueTerritoriale() {
		setPasTypeFctPublique(TYPE_FCT_PUBLIQUE_TERRITORIALE);
	}
	public void setEstFonctionPubliqueHospitaliere() {
		setPasTypeFctPublique(TYPE_FCT_PUBLIQUE_HOSPITALIERE);
	}

	
	public boolean estTypeServiceValide() {
		return pasTypeService() != null && pasTypeService().equals(TYPE_SERVICE_VALIDES);
	}
	public boolean estTypeServiceEAS() {
		return pasTypeService().equals(TYPE_SERVICE_EAS);
	}
	public boolean estTypeServiceEngage() {
		return pasTypeService().equals(TYPE_SERVICE_ENGAGE);
	}
	public boolean estTypeServiceMilitaire() {
		return pasTypeService().equals(TYPE_SERVICE_MILITAIRE);
	}
	public boolean estTypeServiceEleve() {
		return pasTypeService().equals(TYPE_SERVICE_ELEVE);
	}

	public void setEstTypeServiceValides() {
		setPasTypeService(TYPE_SERVICE_VALIDES);
	}
	public void setEstTypeServiceEAS() {
		setPasTypeService(TYPE_SERVICE_EAS);
	}
	public void setEstTypeServiceEngage() {
		setPasTypeService(TYPE_SERVICE_ENGAGE);
	}
	public void setEstTypeServiceMilitaire() {
		setPasTypeService(TYPE_SERVICE_MILITAIRE);
	}
	public void setEstTypeServiceEleve() {
		setPasTypeService(TYPE_SERVICE_ELEVE);
	}


	public boolean estSecteurPublic() {
		return secteurPublic().equals(Constantes.VRAI);
	}
	public void setEstSecteurPublic(boolean aBool) {
		if (aBool)
			setSecteurPublic(Constantes.VRAI);
		else
			setSecteurPublic(Constantes.FAUX);
	}


	public boolean estTempsComplet() {
		return pasTypeTemps().equals(TYPE_TEMPS_COMPLET);
	}
	public boolean estTempsIncomplet() {
		return pasTypeTemps().equals(TYPE_TEMPS_INCOMPLET);
	}
	public boolean estTempsPartiel() {
		return pasTypeTemps().equals(TYPE_TEMPS_PARTIEL);
	}


	public void setEstTempsIncomplet(boolean aBool) {
		if (aBool)
			setPasTypeTemps("I");
		else
			setPasTypeTemps("C");
	}

	public boolean estPaiementCompletCotisations() {
		return pasPcAcquitee().equals(Constantes.VRAI);
	}
	public void setEstPaiementCompletCotisations(boolean aBool) {
		if (aBool)
			setPasPcAcquitee(Constantes.VRAI);
		else
			setPasPcAcquitee(Constantes.FAUX);
	}

	public String dateValidationServiceFormatee() {
		return SuperFinder.dateFormatee(this,"dValidationService");
	}
	public void setDateValidationServiceFormatee(String uneDate) {
		if (uneDate == null)
			setDValidationService(null);
		else
			SuperFinder.setDateFormatee(this,"dValidationService",uneDate);
	}

}
