/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOFeveTypeNiveauDroit extends _EOFeveTypeNiveauDroit {
	
	private static Logger log = Logger.getLogger(EOFeveTypeNiveauDroit.class);

	
	public final static String CODE_ADMIN = "ADMIN";
	public final static String CODE_ADMIN_FONC = "ADMIN_FONC";
	public final static String CODE_DRH = "DRH";
	public final static String CODE_RESP_SERV = "RESP_SERV";
	public final static String CODE_RESP_SERV_DEL = "RESP_SERV_DEL";
	public final static String CODE_DEL_EP = "DEL_EP";
	public final static String CODE_AGENT = "AGENT";
	public final static String CODE_OBSERVATEUR = "OBSERVATEUR";
	
	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public static EOFeveTypeNiveauDroit getEOFeveTypeNiveauDroit(EOEditingContext ec, String code) {
		EOQualifier qual =  ERXQ.equals(EOFeveTypeNiveauDroit.TND_CODE_KEY, code);
		return EOFeveTypeNiveauDroit.fetch(ec, qual);
	}
	
	public static EOFeveTypeNiveauDroit getEOFevTypeNiveauDroitAdmin(EOEditingContext ec) {
		return getEOFeveTypeNiveauDroit(ec, CODE_ADMIN);
	}
	
	public static EOFeveTypeNiveauDroit getEOFevTypeNiveauDroitAgent(EOEditingContext ec) {
		return getEOFeveTypeNiveauDroit(ec, CODE_AGENT);
	}
	
	public static EOFeveTypeNiveauDroit getEOFevTypeNiveauDroitAdminFonc(EOEditingContext ec) {
		return getEOFeveTypeNiveauDroit(ec, CODE_ADMIN_FONC);
	}
	
	public static EOFeveTypeNiveauDroit getEOFevTypeNiveauDroitDrh(EOEditingContext ec) {
		return getEOFeveTypeNiveauDroit(ec, CODE_DRH);
	}
	
	public static EOFeveTypeNiveauDroit getEOFevTypeNiveauDroitRespServ(EOEditingContext ec) {
		return getEOFeveTypeNiveauDroit(ec, CODE_RESP_SERV);
	}
	
	public boolean isTypeNiveauDroitResponsable() {
		return this.tndCode().equals(CODE_RESP_SERV);
	}
	
	public boolean isTypeNiveauDroitAvecCibleEtablissement() {
		return this.tndCode().equals(CODE_ADMIN) 
				|| this.tndCode().equals(CODE_ADMIN_FONC) 
				|| this.tndCode().equals(CODE_DRH);
	}
	
	
	public static EOQualifier getQualifierPourTypeNiveauDroitVisible() {
		return ERXQ.or(
					ERXQ.equals(EOFeveTypeNiveauDroit.TND_CODE_KEY, CODE_ADMIN), 
					ERXQ.equals(EOFeveTypeNiveauDroit.TND_CODE_KEY, CODE_ADMIN_FONC),
					ERXQ.equals(EOFeveTypeNiveauDroit.TND_CODE_KEY, CODE_DRH),
					ERXQ.equals(EOFeveTypeNiveauDroit.TND_CODE_KEY, CODE_RESP_SERV),
					ERXQ.equals(EOFeveTypeNiveauDroit.TND_CODE_KEY, CODE_RESP_SERV_DEL),
					ERXQ.equals(EOFeveTypeNiveauDroit.TND_CODE_KEY, CODE_DEL_EP),
					ERXQ.equals(EOFeveTypeNiveauDroit.TND_CODE_KEY, CODE_OBSERVATEUR)
				);
	}
	
	public static EOQualifier getQualifierPourTypeNiveauDroitVisibleNonAdmin() {
		return ERXQ.or(
					ERXQ.equals(EOFeveTypeNiveauDroit.TND_CODE_KEY, CODE_RESP_SERV),
					ERXQ.equals(EOFeveTypeNiveauDroit.TND_CODE_KEY, CODE_RESP_SERV_DEL),
					ERXQ.equals(EOFeveTypeNiveauDroit.TND_CODE_KEY, CODE_DEL_EP),
					ERXQ.equals(EOFeveTypeNiveauDroit.TND_CODE_KEY, CODE_OBSERVATEUR)
				);
	}
	
	
}
