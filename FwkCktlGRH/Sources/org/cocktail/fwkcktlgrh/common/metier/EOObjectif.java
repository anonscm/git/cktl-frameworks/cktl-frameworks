/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IObjectif;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.appserver.WOMessage;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOObjectif extends _EOObjectif implements IObjectif {
  private static Logger log = Logger.getLogger(EOObjectif.class);

  public EOObjectif() {
      super();
  }
  
  public static EOObjectif createEOObjectif(EOEditingContext editingContext, NSTimestamp dCreation
		  , NSTimestamp dModification, Integer objPosition
		  , org.cocktail.fwkcktlgrh.common.metier.EOEvaluation toEvaluation) {
	  return create(editingContext, dCreation, dModification, objPosition, toEvaluation);
  }
  
  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForInsert() throws NSValidation.ValidationException {
	  setDCreation(DateCtrl.now());
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForInsert();
  }

  /**
   * 
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForUpdate() throws NSValidation.ValidationException {
	  this.validateObjectMetier();
	  validateBeforeTransactionSave();
	  super.validateForUpdate();
  }

  /**
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateForDelete() throws NSValidation.ValidationException {
	  super.validateForDelete();
  }



  /**
   * Peut etre appele à partir des factories.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateObjectMetier() throws NSValidation.ValidationException {

  }

  /**
   * A appeler par les validateforsave, forinsert, forupdate.
   * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
   */
  public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

  }

  // methodes rajoutees

  // formulaire de saisie des objectifs : un seul modifiable a la fois

  private Boolean isEnCoursDeModification;

  public boolean isEnCoursDeModification() {
	  if (isEnCoursDeModification == null) {
		  isEnCoursDeModification = new Boolean(false);
	  }
	  return isEnCoursDeModification.booleanValue();
  }

  public void setIsEnCoursDeModification(boolean value) {
	  isEnCoursDeModification = new Boolean(value);
  }

  public NSArray othersRecords() {
	  return toEvaluation().tosObjectif();
  }

  public String positionKey() {
	  return OBJ_POSITION_KEY;
  }

  // affichage des sauts de ligne en HTML

  private static String getHtmlValue(String value) {
	  return StringCtrl.replace(WOMessage.stringByEscapingHTMLAttributeValue(value), "&#10;", "<br>");
  }

  public String objMesureHtml() {
	  return getHtmlValue(super.objMesure());
  }

  public String objMoyenHtml() {
	  return getHtmlValue(super.objMoyen());
  }

  public String objObjectifHtml() {
	  return getHtmlValue(super.objObjectif());
  }

  public String objObservationHtml() {
	  return getHtmlValue(super.objObservation());
  }

  public String objResultatHtml() {
	  return getHtmlValue(super.objResultat());
  }

  public static EOObjectif newRecordInContext(
		  EOEditingContext ec, EOEvaluation evaluation, String objectif, String moyen, String mesure, String resultat, String observation) {
	  EOObjectif newRecord = newDefaultRecordInContext(ec);
	  newRecord.addObjectToBothSidesOfRelationshipWithKey(evaluation, "toEvaluation");
	  newRecord.setObjObjectif(objectif);
	  newRecord.setObjMoyen(moyen);
	  newRecord.setObjMesure(mesure);
	  newRecord.setObjResultat(resultat);
	  newRecord.setObjObservation(observation);
	  Number maxPos = (Number) evaluation.tosObjectif().valueForKey("@max." + EOObjectif.OBJ_POSITION_KEY);
	  if (maxPos == null) {
		  maxPos = new Integer(0);
	  }
	  newRecord.setObjPosition(new Integer(maxPos.intValue() + 1));
	  return newRecord;
  }

  private static EOObjectif newDefaultRecordInContext(EOEditingContext ec) {
	  EOObjectif record = new EOObjectif();
	  ec.insertObject(record);
	  return record;
  }

  private Number objKey;

  /**
   * La clé primaire pour avoir un identifiant unique (pour faire
   * des ancres html par exemple ...)
   * @return
   */
  private Number objKey() {
	  if (objKey == null) {
		  objKey = (Number) EOUtilities.primaryKeyForObject(editingContext(), this).valueForKey("objKey");
	  }
	  return objKey;
  }

  /**
   * Ancre HTML associée à cet objectif
   * @return
   */
  public String ancre() {
	  return ENTITY_NAME + "_" + objKey().intValue();
  }
  

  /**
   * Remonter
   */
	public void up() {
		// la position actuelle
		int position = (Integer) valueForKey(positionKey());
		// on ne remonte pas si c'est le premier element
	    if (position != 0) {
	    	// retrouver l'element precedent
	    	NSArray recsPrev = EOQualifier.filteredArrayWithQualifier(othersRecords(),
	    			CktlDataBus.newCondition(positionKey() + "=" + (position - 1)));
	    	if (recsPrev.count() == 1) {
	    		EOObjectif recPrev = (EOObjectif) recsPrev.objectAtIndex(0);
	    		recPrev.takeValueForKey(new Integer(position), positionKey());
	    		this.takeValueForKey(new Integer(position - 1), positionKey());
	    	}
	    }
	}
  
	/**
	 * Redescendre
	 */
	public void down() {
			// la position actuelle
		int position = (Integer) valueForKey(positionKey());
		// on ne descend pas si c'est le dernier element (on cherche les elements suivants)
		NSArray recsNext = EOQualifier.filteredArrayWithQualifier(othersRecords(),
				CktlDataBus.newCondition(positionKey() + ">" + position));
		if (recsNext.count() > 0) {
		// recuperer l'element suivant
		recsNext = EOQualifier.filteredArrayWithQualifier(othersRecords(),
			CktlDataBus.newCondition(positionKey() + "=" + (position + 1)));
			if (recsNext.count() == 1) {
				EOObjectif recNext = (EOObjectif) recsNext.objectAtIndex(0);
				recNext.takeValueForKey(new Integer(position), positionKey());
				this.takeValueForKey(new Integer(position + 1), positionKey());
			}
		}
	}
}
