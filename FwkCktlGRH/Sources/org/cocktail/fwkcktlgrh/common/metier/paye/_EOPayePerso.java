/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPayePerso.java instead.
package org.cocktail.fwkcktlgrh.common.metier.paye;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPayePerso extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_PayePerso";

	// Attributes
	public static final String PERS_LIBELLE_KEY = "persLibelle";
	public static final String PERS_MONTANT_KEY = "persMontant";
	public static final String TEM_PERMANENT_KEY = "temPermanent";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> PERS_LIBELLE = new ERXKey<String>("persLibelle");
	public static final ERXKey<java.math.BigDecimal> PERS_MONTANT = new ERXKey<java.math.BigDecimal>("persMontant");
	public static final ERXKey<String> TEM_PERMANENT = new ERXKey<String>("temPermanent");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String CONTRAT_KEY = "contrat";
	public static final String RUBRIQUE_KEY = "rubrique";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat> CONTRAT = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat>("contrat");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeRubrique> RUBRIQUE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeRubrique>("rubrique");

  private static Logger LOG = Logger.getLogger(_EOPayePerso.class);

  public EOPayePerso localInstanceIn(EOEditingContext editingContext) {
    EOPayePerso localInstance = (EOPayePerso)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String persLibelle() {
    return (String) storedValueForKey("persLibelle");
  }

  public void setPersLibelle(String value) {
    if (_EOPayePerso.LOG.isDebugEnabled()) {
    	_EOPayePerso.LOG.debug( "updating persLibelle from " + persLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "persLibelle");
  }

  public java.math.BigDecimal persMontant() {
    return (java.math.BigDecimal) storedValueForKey("persMontant");
  }

  public void setPersMontant(java.math.BigDecimal value) {
    if (_EOPayePerso.LOG.isDebugEnabled()) {
    	_EOPayePerso.LOG.debug( "updating persMontant from " + persMontant() + " to " + value);
    }
    takeStoredValueForKey(value, "persMontant");
  }

  public String temPermanent() {
    return (String) storedValueForKey("temPermanent");
  }

  public void setTemPermanent(String value) {
    if (_EOPayePerso.LOG.isDebugEnabled()) {
    	_EOPayePerso.LOG.debug( "updating temPermanent from " + temPermanent() + " to " + value);
    }
    takeStoredValueForKey(value, "temPermanent");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOPayePerso.LOG.isDebugEnabled()) {
    	_EOPayePerso.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat contrat() {
    return (org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat)storedValueForKey("contrat");
  }

  public void setContratRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat value) {
    if (_EOPayePerso.LOG.isDebugEnabled()) {
      _EOPayePerso.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contrat");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contrat");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeRubrique rubrique() {
    return (org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeRubrique)storedValueForKey("rubrique");
  }

  public void setRubriqueRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeRubrique value) {
    if (_EOPayePerso.LOG.isDebugEnabled()) {
      _EOPayePerso.LOG.debug("updating rubrique from " + rubrique() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeRubrique oldValue = rubrique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "rubrique");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "rubrique");
    }
  }
  

  public static EOPayePerso create(EOEditingContext editingContext, String persLibelle
, String temPermanent
, String temValide
, org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat contrat, org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeRubrique rubrique) {
    EOPayePerso eo = (EOPayePerso) EOUtilities.createAndInsertInstance(editingContext, _EOPayePerso.ENTITY_NAME);    
		eo.setPersLibelle(persLibelle);
		eo.setTemPermanent(temPermanent);
		eo.setTemValide(temValide);
    eo.setContratRelationship(contrat);
    eo.setRubriqueRelationship(rubrique);
    return eo;
  }

  public static NSArray<EOPayePerso> fetchAll(EOEditingContext editingContext) {
    return _EOPayePerso.fetchAll(editingContext, null);
  }

  public static NSArray<EOPayePerso> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPayePerso.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPayePerso> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPayePerso.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPayePerso> eoObjects = (NSArray<EOPayePerso>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPayePerso fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayePerso.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPayePerso fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPayePerso> eoObjects = _EOPayePerso.fetch(editingContext, qualifier, null);
    EOPayePerso eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPayePerso)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_PayePerso that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPayePerso fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayePerso.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPayePerso fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPayePerso eoObject = _EOPayePerso.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_PayePerso that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPayePerso fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayePerso fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayePerso eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayePerso)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPayePerso localInstanceIn(EOEditingContext editingContext, EOPayePerso eo) {
    EOPayePerso localInstance = (eo == null) ? null : (EOPayePerso)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
