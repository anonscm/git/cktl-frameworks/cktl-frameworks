/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPayeFonction.java instead.
package org.cocktail.fwkcktlgrh.common.metier.paye;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPayeFonction extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_PayeFonction";

	// Attributes
	public static final String FONC_LIBELLE_KEY = "foncLibelle";
	public static final String FONC_ORDRE_KEY = "foncOrdre";

	public static final ERXKey<String> FONC_LIBELLE = new ERXKey<String>("foncLibelle");
	public static final ERXKey<Integer> FONC_ORDRE = new ERXKey<Integer>("foncOrdre");
	// Relationships


  private static Logger LOG = Logger.getLogger(_EOPayeFonction.class);

  public EOPayeFonction localInstanceIn(EOEditingContext editingContext) {
    EOPayeFonction localInstance = (EOPayeFonction)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String foncLibelle() {
    return (String) storedValueForKey("foncLibelle");
  }

  public void setFoncLibelle(String value) {
    if (_EOPayeFonction.LOG.isDebugEnabled()) {
    	_EOPayeFonction.LOG.debug( "updating foncLibelle from " + foncLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "foncLibelle");
  }

  public Integer foncOrdre() {
    return (Integer) storedValueForKey("foncOrdre");
  }

  public void setFoncOrdre(Integer value) {
    if (_EOPayeFonction.LOG.isDebugEnabled()) {
    	_EOPayeFonction.LOG.debug( "updating foncOrdre from " + foncOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "foncOrdre");
  }


  public static EOPayeFonction create(EOEditingContext editingContext, String foncLibelle
, Integer foncOrdre
) {
    EOPayeFonction eo = (EOPayeFonction) EOUtilities.createAndInsertInstance(editingContext, _EOPayeFonction.ENTITY_NAME);    
		eo.setFoncLibelle(foncLibelle);
		eo.setFoncOrdre(foncOrdre);
    return eo;
  }

  public static NSArray<EOPayeFonction> fetchAll(EOEditingContext editingContext) {
    return _EOPayeFonction.fetchAll(editingContext, null);
  }

  public static NSArray<EOPayeFonction> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPayeFonction.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPayeFonction> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPayeFonction.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPayeFonction> eoObjects = (NSArray<EOPayeFonction>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPayeFonction fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeFonction.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPayeFonction fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPayeFonction> eoObjects = _EOPayeFonction.fetch(editingContext, qualifier, null);
    EOPayeFonction eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPayeFonction)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_PayeFonction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPayeFonction fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeFonction.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPayeFonction fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPayeFonction eoObject = _EOPayeFonction.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_PayeFonction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPayeFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeFonction)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPayeFonction localInstanceIn(EOEditingContext editingContext, EOPayeFonction eo) {
    EOPayeFonction localInstance = (eo == null) ? null : (EOPayeFonction)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
