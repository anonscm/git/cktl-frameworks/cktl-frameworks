/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPayeStatut.java instead.
package org.cocktail.fwkcktlgrh.common.metier.paye;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPayeStatut extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_PayeStatut";

	// Attributes
	public static final String PSTA_ABREGE_KEY = "pstaAbrege";
	public static final String PSTA_CIPDZ_KEY = "pstaCipdz";
	public static final String PSTA_CODEMPLOI_KEY = "pstaCodemploi";
	public static final String PSTA_LIBELLE_KEY = "pstaLibelle";
	public static final String PSTA_MDEBUT_KEY = "pstaMdebut";
	public static final String PSTA_MFIN_KEY = "pstaMfin";
	public static final String PSTA_MINISTERE_KEY = "pstaMinistere";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> PSTA_ABREGE = new ERXKey<String>("pstaAbrege");
	public static final ERXKey<String> PSTA_CIPDZ = new ERXKey<String>("pstaCipdz");
	public static final ERXKey<String> PSTA_CODEMPLOI = new ERXKey<String>("pstaCodemploi");
	public static final ERXKey<String> PSTA_LIBELLE = new ERXKey<String>("pstaLibelle");
	public static final ERXKey<Integer> PSTA_MDEBUT = new ERXKey<Integer>("pstaMdebut");
	public static final ERXKey<Integer> PSTA_MFIN = new ERXKey<Integer>("pstaMfin");
	public static final ERXKey<String> PSTA_MINISTERE = new ERXKey<String>("pstaMinistere");
	public static final ERXKey<String> TEM_TITULAIRE = new ERXKey<String>("temTitulaire");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships


  private static Logger LOG = Logger.getLogger(_EOPayeStatut.class);

  public EOPayeStatut localInstanceIn(EOEditingContext editingContext) {
    EOPayeStatut localInstance = (EOPayeStatut)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String pstaAbrege() {
    return (String) storedValueForKey("pstaAbrege");
  }

  public void setPstaAbrege(String value) {
    if (_EOPayeStatut.LOG.isDebugEnabled()) {
    	_EOPayeStatut.LOG.debug( "updating pstaAbrege from " + pstaAbrege() + " to " + value);
    }
    takeStoredValueForKey(value, "pstaAbrege");
  }

  public String pstaCipdz() {
    return (String) storedValueForKey("pstaCipdz");
  }

  public void setPstaCipdz(String value) {
    if (_EOPayeStatut.LOG.isDebugEnabled()) {
    	_EOPayeStatut.LOG.debug( "updating pstaCipdz from " + pstaCipdz() + " to " + value);
    }
    takeStoredValueForKey(value, "pstaCipdz");
  }

  public String pstaCodemploi() {
    return (String) storedValueForKey("pstaCodemploi");
  }

  public void setPstaCodemploi(String value) {
    if (_EOPayeStatut.LOG.isDebugEnabled()) {
    	_EOPayeStatut.LOG.debug( "updating pstaCodemploi from " + pstaCodemploi() + " to " + value);
    }
    takeStoredValueForKey(value, "pstaCodemploi");
  }

  public String pstaLibelle() {
    return (String) storedValueForKey("pstaLibelle");
  }

  public void setPstaLibelle(String value) {
    if (_EOPayeStatut.LOG.isDebugEnabled()) {
    	_EOPayeStatut.LOG.debug( "updating pstaLibelle from " + pstaLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "pstaLibelle");
  }

  public Integer pstaMdebut() {
    return (Integer) storedValueForKey("pstaMdebut");
  }

  public void setPstaMdebut(Integer value) {
    if (_EOPayeStatut.LOG.isDebugEnabled()) {
    	_EOPayeStatut.LOG.debug( "updating pstaMdebut from " + pstaMdebut() + " to " + value);
    }
    takeStoredValueForKey(value, "pstaMdebut");
  }

  public Integer pstaMfin() {
    return (Integer) storedValueForKey("pstaMfin");
  }

  public void setPstaMfin(Integer value) {
    if (_EOPayeStatut.LOG.isDebugEnabled()) {
    	_EOPayeStatut.LOG.debug( "updating pstaMfin from " + pstaMfin() + " to " + value);
    }
    takeStoredValueForKey(value, "pstaMfin");
  }

  public String pstaMinistere() {
    return (String) storedValueForKey("pstaMinistere");
  }

  public void setPstaMinistere(String value) {
    if (_EOPayeStatut.LOG.isDebugEnabled()) {
    	_EOPayeStatut.LOG.debug( "updating pstaMinistere from " + pstaMinistere() + " to " + value);
    }
    takeStoredValueForKey(value, "pstaMinistere");
  }

  public String temTitulaire() {
    return (String) storedValueForKey("temTitulaire");
  }

  public void setTemTitulaire(String value) {
    if (_EOPayeStatut.LOG.isDebugEnabled()) {
    	_EOPayeStatut.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, "temTitulaire");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOPayeStatut.LOG.isDebugEnabled()) {
    	_EOPayeStatut.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }


  public static EOPayeStatut create(EOEditingContext editingContext, String pstaAbrege
, String pstaLibelle
, Integer pstaMdebut
, Integer pstaMfin
, String pstaMinistere
, String temTitulaire
, String temValide
) {
    EOPayeStatut eo = (EOPayeStatut) EOUtilities.createAndInsertInstance(editingContext, _EOPayeStatut.ENTITY_NAME);    
		eo.setPstaAbrege(pstaAbrege);
		eo.setPstaLibelle(pstaLibelle);
		eo.setPstaMdebut(pstaMdebut);
		eo.setPstaMfin(pstaMfin);
		eo.setPstaMinistere(pstaMinistere);
		eo.setTemTitulaire(temTitulaire);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOPayeStatut> fetchAll(EOEditingContext editingContext) {
    return _EOPayeStatut.fetchAll(editingContext, null);
  }

  public static NSArray<EOPayeStatut> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPayeStatut.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPayeStatut> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPayeStatut.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPayeStatut> eoObjects = (NSArray<EOPayeStatut>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPayeStatut fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeStatut.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPayeStatut fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPayeStatut> eoObjects = _EOPayeStatut.fetch(editingContext, qualifier, null);
    EOPayeStatut eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPayeStatut)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_PayeStatut that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPayeStatut fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeStatut.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPayeStatut fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPayeStatut eoObject = _EOPayeStatut.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_PayeStatut that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPayeStatut fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeStatut fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeStatut eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeStatut)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPayeStatut localInstanceIn(EOEditingContext editingContext, EOPayeStatut eo) {
    EOPayeStatut localInstance = (eo == null) ? null : (EOPayeStatut)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
