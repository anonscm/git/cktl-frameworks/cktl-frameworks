/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPayeContrat.java instead.
package org.cocktail.fwkcktlgrh.common.metier.paye;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPayeContrat extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_PayeContrat";

	// Attributes
	public static final String ABATTEMENT_KEY = "abattement";
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String CLASSIFICATION_EMPLOI_KEY = "classificationEmploi";
	public static final String CODE_STATUT_CATEGORIEL_KEY = "codeStatutCategoriel";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_CONTRAT_TRAV_KEY = "dDebContratTrav";
	public static final String D_FIN_CONTRAT_TRAV_KEY = "dFinContratTrav";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INDICE_CONTRAT_KEY = "indiceContrat";
	public static final String INDICE_ORIGINE_KEY = "indiceOrigine";
	public static final String INDIVIDU_NOM_USUEL_KEY = "individu_nomUsuel";
	public static final String INDIVIDU_PRENOM_KEY = "individu_prenom";
	public static final String MODE_CALCUL_KEY = "modeCalcul";
	public static final String MONTANT_FORFAITAIRE_KEY = "montantForfaitaire";
	public static final String MONTANT_MENSUEL_REMU_KEY = "montantMensuelRemu";
	public static final String MONTANT_PLAFOND_SECU_KEY = "montantPlafondSecu";
	public static final String NB_HEURES_CONTRAT_KEY = "nbHeuresContrat";
	public static final String NB_JOURS_CONTRAT_KEY = "nbJoursContrat";
	public static final String NO_CONTRAT_PRECEDENT_KEY = "noContratPrecedent";
	public static final String NUM_QUOT_RECRUTEMENT_KEY = "numQuotRecrutement";
	public static final String POURCENT_SMIC_KEY = "pourcentSmic";
	public static final String PSEC_ORDRE_KEY = "psecOrdre";
	public static final String REFERENCE_CONTRAT_KEY = "referenceContrat";
	public static final String RISQUE_ACC_TRAV_KEY = "risqueAccTrav";
	public static final String TAUX_ACC_TRAV_KEY = "tauxAccTrav";
	public static final String TAUX_HORAIRE_CONTRAT_KEY = "tauxHoraireContrat";
	public static final String TAUX_PLAFOND_SECU_KEY = "tauxPlafondSecu";
	public static final String TEM_ANNULATION_KEY = "temAnnulation";
	public static final String TEM_COTISE_SOLIDARITE_KEY = "temCotiseSolidarite";
	public static final String TEM_PAIEMENT_PONCTUEL_KEY = "temPaiementPonctuel";
	public static final String TEM_PAYE_LOCALE_KEY = "temPayeLocale";
	public static final String TEM_PAYE_UTILE_KEY = "temPayeUtile";
	public static final String TEM_PLAFOND_REDUIT_KEY = "temPlafondReduit";
	public static final String TEM_PRE_CONTRAT_KEY = "temPreContrat";
	public static final String TEM_TEMPS_PLEIN_KEY = "temTempsPlein";

	public static final ERXKey<Double> ABATTEMENT = new ERXKey<Double>("abattement");
	public static final ERXKey<String> C_ECHELON = new ERXKey<String>("cEchelon");
	public static final ERXKey<String> CLASSIFICATION_EMPLOI = new ERXKey<String>("classificationEmploi");
	public static final ERXKey<String> CODE_STATUT_CATEGORIEL = new ERXKey<String>("codeStatutCategoriel");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_CONTRAT_TRAV = new ERXKey<NSTimestamp>("dDebContratTrav");
	public static final ERXKey<NSTimestamp> D_FIN_CONTRAT_TRAV = new ERXKey<NSTimestamp>("dFinContratTrav");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> INDICE_CONTRAT = new ERXKey<String>("indiceContrat");
	public static final ERXKey<String> INDICE_ORIGINE = new ERXKey<String>("indiceOrigine");
	public static final ERXKey<String> INDIVIDU_NOM_USUEL = new ERXKey<String>("individu_nomUsuel");
	public static final ERXKey<String> INDIVIDU_PRENOM = new ERXKey<String>("individu_prenom");
	public static final ERXKey<String> MODE_CALCUL = new ERXKey<String>("modeCalcul");
	public static final ERXKey<java.math.BigDecimal> MONTANT_FORFAITAIRE = new ERXKey<java.math.BigDecimal>("montantForfaitaire");
	public static final ERXKey<java.math.BigDecimal> MONTANT_MENSUEL_REMU = new ERXKey<java.math.BigDecimal>("montantMensuelRemu");
	public static final ERXKey<java.math.BigDecimal> MONTANT_PLAFOND_SECU = new ERXKey<java.math.BigDecimal>("montantPlafondSecu");
	public static final ERXKey<java.math.BigDecimal> NB_HEURES_CONTRAT = new ERXKey<java.math.BigDecimal>("nbHeuresContrat");
	public static final ERXKey<java.math.BigDecimal> NB_JOURS_CONTRAT = new ERXKey<java.math.BigDecimal>("nbJoursContrat");
	public static final ERXKey<Integer> NO_CONTRAT_PRECEDENT = new ERXKey<Integer>("noContratPrecedent");
	public static final ERXKey<java.math.BigDecimal> NUM_QUOT_RECRUTEMENT = new ERXKey<java.math.BigDecimal>("numQuotRecrutement");
	public static final ERXKey<Double> POURCENT_SMIC = new ERXKey<Double>("pourcentSmic");
	public static final ERXKey<Integer> PSEC_ORDRE = new ERXKey<Integer>("psecOrdre");
	public static final ERXKey<String> REFERENCE_CONTRAT = new ERXKey<String>("referenceContrat");
	public static final ERXKey<String> RISQUE_ACC_TRAV = new ERXKey<String>("risqueAccTrav");
	public static final ERXKey<String> TAUX_ACC_TRAV = new ERXKey<String>("tauxAccTrav");
	public static final ERXKey<java.math.BigDecimal> TAUX_HORAIRE_CONTRAT = new ERXKey<java.math.BigDecimal>("tauxHoraireContrat");
	public static final ERXKey<java.math.BigDecimal> TAUX_PLAFOND_SECU = new ERXKey<java.math.BigDecimal>("tauxPlafondSecu");
	public static final ERXKey<String> TEM_ANNULATION = new ERXKey<String>("temAnnulation");
	public static final ERXKey<String> TEM_COTISE_SOLIDARITE = new ERXKey<String>("temCotiseSolidarite");
	public static final ERXKey<String> TEM_PAIEMENT_PONCTUEL = new ERXKey<String>("temPaiementPonctuel");
	public static final ERXKey<String> TEM_PAYE_LOCALE = new ERXKey<String>("temPayeLocale");
	public static final ERXKey<String> TEM_PAYE_UTILE = new ERXKey<String>("temPayeUtile");
	public static final ERXKey<String> TEM_PLAFOND_REDUIT = new ERXKey<String>("temPlafondReduit");
	public static final ERXKey<String> TEM_PRE_CONTRAT = new ERXKey<String>("temPreContrat");
	public static final ERXKey<String> TEM_TEMPS_PLEIN = new ERXKey<String>("temTempsPlein");
	// Relationships
	public static final String FONCTION_KEY = "fonction";
	public static final String INDIVIDU_KEY = "individu";
	public static final String PERIODES_KEY = "periodes";
	public static final String PERSONNALISATIONS_KEY = "personnalisations";
	public static final String STATUT_KEY = "statut";
	public static final String STRUCTURE_KEY = "structure";
	public static final String STRUCTURE_SIRET_KEY = "structureSiret";
	public static final String TYPE_CONTRAT_TRAVAIL_KEY = "typeContratTravail";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeFonction> FONCTION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeFonction>("fonction");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("individu");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode> PERIODES = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode>("periodes");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso> PERSONNALISATIONS = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso>("personnalisations");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeStatut> STATUT = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeStatut>("statut");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("structure");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> STRUCTURE_SIRET = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("structureSiret");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> TYPE_CONTRAT_TRAVAIL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail>("typeContratTravail");

  private static Logger LOG = Logger.getLogger(_EOPayeContrat.class);

  public EOPayeContrat localInstanceIn(EOEditingContext editingContext) {
    EOPayeContrat localInstance = (EOPayeContrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Double abattement() {
    return (Double) storedValueForKey("abattement");
  }

  public void setAbattement(Double value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating abattement from " + abattement() + " to " + value);
    }
    takeStoredValueForKey(value, "abattement");
  }

  public String cEchelon() {
    return (String) storedValueForKey("cEchelon");
  }

  public void setCEchelon(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating cEchelon from " + cEchelon() + " to " + value);
    }
    takeStoredValueForKey(value, "cEchelon");
  }

  public String classificationEmploi() {
    return (String) storedValueForKey("classificationEmploi");
  }

  public void setClassificationEmploi(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating classificationEmploi from " + classificationEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "classificationEmploi");
  }

  public String codeStatutCategoriel() {
    return (String) storedValueForKey("codeStatutCategoriel");
  }

  public void setCodeStatutCategoriel(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating codeStatutCategoriel from " + codeStatutCategoriel() + " to " + value);
    }
    takeStoredValueForKey(value, "codeStatutCategoriel");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebContratTrav() {
    return (NSTimestamp) storedValueForKey("dDebContratTrav");
  }

  public void setDDebContratTrav(NSTimestamp value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating dDebContratTrav from " + dDebContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebContratTrav");
  }

  public NSTimestamp dFinContratTrav() {
    return (NSTimestamp) storedValueForKey("dFinContratTrav");
  }

  public void setDFinContratTrav(NSTimestamp value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating dFinContratTrav from " + dFinContratTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinContratTrav");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String indiceContrat() {
    return (String) storedValueForKey("indiceContrat");
  }

  public void setIndiceContrat(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating indiceContrat from " + indiceContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "indiceContrat");
  }

  public String indiceOrigine() {
    return (String) storedValueForKey("indiceOrigine");
  }

  public void setIndiceOrigine(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating indiceOrigine from " + indiceOrigine() + " to " + value);
    }
    takeStoredValueForKey(value, "indiceOrigine");
  }

  public String individu_nomUsuel() {
    return (String) storedValueForKey("individu_nomUsuel");
  }

  public void setIndividu_nomUsuel(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating individu_nomUsuel from " + individu_nomUsuel() + " to " + value);
    }
    takeStoredValueForKey(value, "individu_nomUsuel");
  }

  public String individu_prenom() {
    return (String) storedValueForKey("individu_prenom");
  }

  public void setIndividu_prenom(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating individu_prenom from " + individu_prenom() + " to " + value);
    }
    takeStoredValueForKey(value, "individu_prenom");
  }

  public String modeCalcul() {
    return (String) storedValueForKey("modeCalcul");
  }

  public void setModeCalcul(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating modeCalcul from " + modeCalcul() + " to " + value);
    }
    takeStoredValueForKey(value, "modeCalcul");
  }

  public java.math.BigDecimal montantForfaitaire() {
    return (java.math.BigDecimal) storedValueForKey("montantForfaitaire");
  }

  public void setMontantForfaitaire(java.math.BigDecimal value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating montantForfaitaire from " + montantForfaitaire() + " to " + value);
    }
    takeStoredValueForKey(value, "montantForfaitaire");
  }

  public java.math.BigDecimal montantMensuelRemu() {
    return (java.math.BigDecimal) storedValueForKey("montantMensuelRemu");
  }

  public void setMontantMensuelRemu(java.math.BigDecimal value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating montantMensuelRemu from " + montantMensuelRemu() + " to " + value);
    }
    takeStoredValueForKey(value, "montantMensuelRemu");
  }

  public java.math.BigDecimal montantPlafondSecu() {
    return (java.math.BigDecimal) storedValueForKey("montantPlafondSecu");
  }

  public void setMontantPlafondSecu(java.math.BigDecimal value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating montantPlafondSecu from " + montantPlafondSecu() + " to " + value);
    }
    takeStoredValueForKey(value, "montantPlafondSecu");
  }

  public java.math.BigDecimal nbHeuresContrat() {
    return (java.math.BigDecimal) storedValueForKey("nbHeuresContrat");
  }

  public void setNbHeuresContrat(java.math.BigDecimal value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating nbHeuresContrat from " + nbHeuresContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "nbHeuresContrat");
  }

  public java.math.BigDecimal nbJoursContrat() {
    return (java.math.BigDecimal) storedValueForKey("nbJoursContrat");
  }

  public void setNbJoursContrat(java.math.BigDecimal value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating nbJoursContrat from " + nbJoursContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "nbJoursContrat");
  }

  public Integer noContratPrecedent() {
    return (Integer) storedValueForKey("noContratPrecedent");
  }

  public void setNoContratPrecedent(Integer value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating noContratPrecedent from " + noContratPrecedent() + " to " + value);
    }
    takeStoredValueForKey(value, "noContratPrecedent");
  }

  public java.math.BigDecimal numQuotRecrutement() {
    return (java.math.BigDecimal) storedValueForKey("numQuotRecrutement");
  }

  public void setNumQuotRecrutement(java.math.BigDecimal value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating numQuotRecrutement from " + numQuotRecrutement() + " to " + value);
    }
    takeStoredValueForKey(value, "numQuotRecrutement");
  }

  public Double pourcentSmic() {
    return (Double) storedValueForKey("pourcentSmic");
  }

  public void setPourcentSmic(Double value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating pourcentSmic from " + pourcentSmic() + " to " + value);
    }
    takeStoredValueForKey(value, "pourcentSmic");
  }

  public Integer psecOrdre() {
    return (Integer) storedValueForKey("psecOrdre");
  }

  public void setPsecOrdre(Integer value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating psecOrdre from " + psecOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "psecOrdre");
  }

  public String referenceContrat() {
    return (String) storedValueForKey("referenceContrat");
  }

  public void setReferenceContrat(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating referenceContrat from " + referenceContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "referenceContrat");
  }

  public String risqueAccTrav() {
    return (String) storedValueForKey("risqueAccTrav");
  }

  public void setRisqueAccTrav(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating risqueAccTrav from " + risqueAccTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "risqueAccTrav");
  }

  public String tauxAccTrav() {
    return (String) storedValueForKey("tauxAccTrav");
  }

  public void setTauxAccTrav(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating tauxAccTrav from " + tauxAccTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxAccTrav");
  }

  public java.math.BigDecimal tauxHoraireContrat() {
    return (java.math.BigDecimal) storedValueForKey("tauxHoraireContrat");
  }

  public void setTauxHoraireContrat(java.math.BigDecimal value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating tauxHoraireContrat from " + tauxHoraireContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxHoraireContrat");
  }

  public java.math.BigDecimal tauxPlafondSecu() {
    return (java.math.BigDecimal) storedValueForKey("tauxPlafondSecu");
  }

  public void setTauxPlafondSecu(java.math.BigDecimal value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating tauxPlafondSecu from " + tauxPlafondSecu() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxPlafondSecu");
  }

  public String temAnnulation() {
    return (String) storedValueForKey("temAnnulation");
  }

  public void setTemAnnulation(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating temAnnulation from " + temAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "temAnnulation");
  }

  public String temCotiseSolidarite() {
    return (String) storedValueForKey("temCotiseSolidarite");
  }

  public void setTemCotiseSolidarite(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating temCotiseSolidarite from " + temCotiseSolidarite() + " to " + value);
    }
    takeStoredValueForKey(value, "temCotiseSolidarite");
  }

  public String temPaiementPonctuel() {
    return (String) storedValueForKey("temPaiementPonctuel");
  }

  public void setTemPaiementPonctuel(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating temPaiementPonctuel from " + temPaiementPonctuel() + " to " + value);
    }
    takeStoredValueForKey(value, "temPaiementPonctuel");
  }

  public String temPayeLocale() {
    return (String) storedValueForKey("temPayeLocale");
  }

  public void setTemPayeLocale(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating temPayeLocale from " + temPayeLocale() + " to " + value);
    }
    takeStoredValueForKey(value, "temPayeLocale");
  }

  public String temPayeUtile() {
    return (String) storedValueForKey("temPayeUtile");
  }

  public void setTemPayeUtile(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating temPayeUtile from " + temPayeUtile() + " to " + value);
    }
    takeStoredValueForKey(value, "temPayeUtile");
  }

  public String temPlafondReduit() {
    return (String) storedValueForKey("temPlafondReduit");
  }

  public void setTemPlafondReduit(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating temPlafondReduit from " + temPlafondReduit() + " to " + value);
    }
    takeStoredValueForKey(value, "temPlafondReduit");
  }

  public String temPreContrat() {
    return (String) storedValueForKey("temPreContrat");
  }

  public void setTemPreContrat(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating temPreContrat from " + temPreContrat() + " to " + value);
    }
    takeStoredValueForKey(value, "temPreContrat");
  }

  public String temTempsPlein() {
    return (String) storedValueForKey("temTempsPlein");
  }

  public void setTemTempsPlein(String value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
    	_EOPayeContrat.LOG.debug( "updating temTempsPlein from " + temTempsPlein() + " to " + value);
    }
    takeStoredValueForKey(value, "temTempsPlein");
  }

  public org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeFonction fonction() {
    return (org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeFonction)storedValueForKey("fonction");
  }

  public void setFonctionRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeFonction value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
      _EOPayeContrat.LOG.debug("updating fonction from " + fonction() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeFonction oldValue = fonction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "fonction");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "fonction");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("individu");
  }

  public void setIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
      _EOPayeContrat.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "individu");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeStatut statut() {
    return (org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeStatut)storedValueForKey("statut");
  }

  public void setStatutRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeStatut value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
      _EOPayeContrat.LOG.debug("updating statut from " + statut() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeStatut oldValue = statut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "statut");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "statut");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure structure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey("structure");
  }

  public void setStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
      _EOPayeContrat.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structure");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure structureSiret() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey("structureSiret");
  }

  public void setStructureSiretRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
      _EOPayeContrat.LOG.debug("updating structureSiret from " + structureSiret() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = structureSiret();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "structureSiret");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "structureSiret");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail typeContratTravail() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail)storedValueForKey("typeContratTravail");
  }

  public void setTypeContratTravailRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail value) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
      _EOPayeContrat.LOG.debug("updating typeContratTravail from " + typeContratTravail() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail oldValue = typeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "typeContratTravail");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "typeContratTravail");
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode> periodes() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode>)storedValueForKey("periodes");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode> periodes(EOQualifier qualifier) {
    return periodes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode> periodes(EOQualifier qualifier, boolean fetch) {
    return periodes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode> periodes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = periodes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPeriodesRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode object) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
      _EOPayeContrat.LOG.debug("adding " + object + " to periodes relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "periodes");
  }

  public void removeFromPeriodesRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode object) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
      _EOPayeContrat.LOG.debug("removing " + object + " from periodes relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "periodes");
  }

  public org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode createPeriodesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_PayePeriode");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "periodes");
    return (org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode) eo;
  }

  public void deletePeriodesRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "periodes");
    editingContext().deleteObject(object);
  }

  public void deleteAllPeriodesRelationships() {
    Enumeration objects = periodes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePeriodesRelationship((org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso> personnalisations() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso>)storedValueForKey("personnalisations");
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso> personnalisations(EOQualifier qualifier) {
    return personnalisations(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso> personnalisations(EOQualifier qualifier, boolean fetch) {
    return personnalisations(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso> personnalisations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso.fetch(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = personnalisations();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPersonnalisationsRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso object) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
      _EOPayeContrat.LOG.debug("adding " + object + " to personnalisations relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "personnalisations");
  }

  public void removeFromPersonnalisationsRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso object) {
    if (_EOPayeContrat.LOG.isDebugEnabled()) {
      _EOPayeContrat.LOG.debug("removing " + object + " from personnalisations relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "personnalisations");
  }

  public org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso createPersonnalisationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkgrh_PayePerso");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "personnalisations");
    return (org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso) eo;
  }

  public void deletePersonnalisationsRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "personnalisations");
    editingContext().deleteObject(object);
  }

  public void deleteAllPersonnalisationsRelationships() {
    Enumeration objects = personnalisations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePersonnalisationsRelationship((org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso)objects.nextElement());
    }
  }


  public static EOPayeContrat create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebContratTrav
, NSTimestamp dModification
, String temAnnulation
, String temPaiementPonctuel
, String temPayeLocale
, String temPayeUtile
, String temPreContrat
, String temTempsPlein
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu) {
    EOPayeContrat eo = (EOPayeContrat) EOUtilities.createAndInsertInstance(editingContext, _EOPayeContrat.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebContratTrav(dDebContratTrav);
		eo.setDModification(dModification);
		eo.setTemAnnulation(temAnnulation);
		eo.setTemPaiementPonctuel(temPaiementPonctuel);
		eo.setTemPayeLocale(temPayeLocale);
		eo.setTemPayeUtile(temPayeUtile);
		eo.setTemPreContrat(temPreContrat);
		eo.setTemTempsPlein(temTempsPlein);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  public static NSArray<EOPayeContrat> fetchAll(EOEditingContext editingContext) {
    return _EOPayeContrat.fetchAll(editingContext, null);
  }

  public static NSArray<EOPayeContrat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPayeContrat.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPayeContrat> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPayeContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPayeContrat> eoObjects = (NSArray<EOPayeContrat>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPayeContrat fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeContrat.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPayeContrat fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPayeContrat> eoObjects = _EOPayeContrat.fetch(editingContext, qualifier, null);
    EOPayeContrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPayeContrat)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_PayeContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPayeContrat fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeContrat.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPayeContrat fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPayeContrat eoObject = _EOPayeContrat.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_PayeContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPayeContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeContrat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPayeContrat localInstanceIn(EOEditingContext editingContext, EOPayeContrat eo) {
    EOPayeContrat localInstance = (eo == null) ? null : (EOPayeContrat)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
