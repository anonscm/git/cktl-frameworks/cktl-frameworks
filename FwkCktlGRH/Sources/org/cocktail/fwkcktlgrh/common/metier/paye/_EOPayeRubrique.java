/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPayeRubrique.java instead.
package org.cocktail.fwkcktlgrh.common.metier.paye;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPayeRubrique extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_PayeRubrique";

	// Attributes
	public static final String PRUB_BASE_FREQUENCE_KEY = "prubBaseFrequence";
	public static final String PRUB_CLASSEMENT_KEY = "prubClassement";
	public static final String PRUB_FREQUENCE_KEY = "prubFrequence";
	public static final String PRUB_KA_ELEMENT_KEY = "prubKaElement";
	public static final String PRUB_LIBELLE_KEY = "prubLibelle";
	public static final String PRUB_LIBELLE_IMP_KEY = "prubLibelleImp";
	public static final String PRUB_MDEBUT_KEY = "prubMdebut";
	public static final String PRUB_MFIN_KEY = "prubMfin";
	public static final String PRUB_MODE_KEY = "prubMode";
	public static final String PRUB_NOMCLASSE_KEY = "prubNomclasse";
	public static final String PRUB_TYPE_KEY = "prubType";
	public static final String TEM_BASE_ASSIETTE_KEY = "temBaseAssiette";
	public static final String TEM_EST_CALCULE_KEY = "temEstCalcule";
	public static final String TEM_IMPOSABLE_KEY = "temImposable";
	public static final String TEM_IMPUTATION_BRUT_KEY = "temImputationBrut";
	public static final String TEM_MGEN_KEY = "temMgen";
	public static final String TEM_PENSION_CIVILE_KEY = "temPensionCivile";
	public static final String TEM_RAFP_KEY = "temRafp";
	public static final String TEM_RETENUE_KEY = "temRetenue";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<Integer> PRUB_BASE_FREQUENCE = new ERXKey<Integer>("prubBaseFrequence");
	public static final ERXKey<String> PRUB_CLASSEMENT = new ERXKey<String>("prubClassement");
	public static final ERXKey<String> PRUB_FREQUENCE = new ERXKey<String>("prubFrequence");
	public static final ERXKey<String> PRUB_KA_ELEMENT = new ERXKey<String>("prubKaElement");
	public static final ERXKey<String> PRUB_LIBELLE = new ERXKey<String>("prubLibelle");
	public static final ERXKey<String> PRUB_LIBELLE_IMP = new ERXKey<String>("prubLibelleImp");
	public static final ERXKey<Integer> PRUB_MDEBUT = new ERXKey<Integer>("prubMdebut");
	public static final ERXKey<Integer> PRUB_MFIN = new ERXKey<Integer>("prubMfin");
	public static final ERXKey<String> PRUB_MODE = new ERXKey<String>("prubMode");
	public static final ERXKey<String> PRUB_NOMCLASSE = new ERXKey<String>("prubNomclasse");
	public static final ERXKey<String> PRUB_TYPE = new ERXKey<String>("prubType");
	public static final ERXKey<String> TEM_BASE_ASSIETTE = new ERXKey<String>("temBaseAssiette");
	public static final ERXKey<String> TEM_EST_CALCULE = new ERXKey<String>("temEstCalcule");
	public static final ERXKey<String> TEM_IMPOSABLE = new ERXKey<String>("temImposable");
	public static final ERXKey<String> TEM_IMPUTATION_BRUT = new ERXKey<String>("temImputationBrut");
	public static final ERXKey<String> TEM_MGEN = new ERXKey<String>("temMgen");
	public static final ERXKey<String> TEM_PENSION_CIVILE = new ERXKey<String>("temPensionCivile");
	public static final ERXKey<String> TEM_RAFP = new ERXKey<String>("temRafp");
	public static final ERXKey<String> TEM_RETENUE = new ERXKey<String>("temRetenue");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships


  private static Logger LOG = Logger.getLogger(_EOPayeRubrique.class);

  public EOPayeRubrique localInstanceIn(EOEditingContext editingContext) {
    EOPayeRubrique localInstance = (EOPayeRubrique)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer prubBaseFrequence() {
    return (Integer) storedValueForKey("prubBaseFrequence");
  }

  public void setPrubBaseFrequence(Integer value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating prubBaseFrequence from " + prubBaseFrequence() + " to " + value);
    }
    takeStoredValueForKey(value, "prubBaseFrequence");
  }

  public String prubClassement() {
    return (String) storedValueForKey("prubClassement");
  }

  public void setPrubClassement(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating prubClassement from " + prubClassement() + " to " + value);
    }
    takeStoredValueForKey(value, "prubClassement");
  }

  public String prubFrequence() {
    return (String) storedValueForKey("prubFrequence");
  }

  public void setPrubFrequence(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating prubFrequence from " + prubFrequence() + " to " + value);
    }
    takeStoredValueForKey(value, "prubFrequence");
  }

  public String prubKaElement() {
    return (String) storedValueForKey("prubKaElement");
  }

  public void setPrubKaElement(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating prubKaElement from " + prubKaElement() + " to " + value);
    }
    takeStoredValueForKey(value, "prubKaElement");
  }

  public String prubLibelle() {
    return (String) storedValueForKey("prubLibelle");
  }

  public void setPrubLibelle(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating prubLibelle from " + prubLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "prubLibelle");
  }

  public String prubLibelleImp() {
    return (String) storedValueForKey("prubLibelleImp");
  }

  public void setPrubLibelleImp(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating prubLibelleImp from " + prubLibelleImp() + " to " + value);
    }
    takeStoredValueForKey(value, "prubLibelleImp");
  }

  public Integer prubMdebut() {
    return (Integer) storedValueForKey("prubMdebut");
  }

  public void setPrubMdebut(Integer value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating prubMdebut from " + prubMdebut() + " to " + value);
    }
    takeStoredValueForKey(value, "prubMdebut");
  }

  public Integer prubMfin() {
    return (Integer) storedValueForKey("prubMfin");
  }

  public void setPrubMfin(Integer value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating prubMfin from " + prubMfin() + " to " + value);
    }
    takeStoredValueForKey(value, "prubMfin");
  }

  public String prubMode() {
    return (String) storedValueForKey("prubMode");
  }

  public void setPrubMode(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating prubMode from " + prubMode() + " to " + value);
    }
    takeStoredValueForKey(value, "prubMode");
  }

  public String prubNomclasse() {
    return (String) storedValueForKey("prubNomclasse");
  }

  public void setPrubNomclasse(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating prubNomclasse from " + prubNomclasse() + " to " + value);
    }
    takeStoredValueForKey(value, "prubNomclasse");
  }

  public String prubType() {
    return (String) storedValueForKey("prubType");
  }

  public void setPrubType(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating prubType from " + prubType() + " to " + value);
    }
    takeStoredValueForKey(value, "prubType");
  }

  public String temBaseAssiette() {
    return (String) storedValueForKey("temBaseAssiette");
  }

  public void setTemBaseAssiette(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating temBaseAssiette from " + temBaseAssiette() + " to " + value);
    }
    takeStoredValueForKey(value, "temBaseAssiette");
  }

  public String temEstCalcule() {
    return (String) storedValueForKey("temEstCalcule");
  }

  public void setTemEstCalcule(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating temEstCalcule from " + temEstCalcule() + " to " + value);
    }
    takeStoredValueForKey(value, "temEstCalcule");
  }

  public String temImposable() {
    return (String) storedValueForKey("temImposable");
  }

  public void setTemImposable(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating temImposable from " + temImposable() + " to " + value);
    }
    takeStoredValueForKey(value, "temImposable");
  }

  public String temImputationBrut() {
    return (String) storedValueForKey("temImputationBrut");
  }

  public void setTemImputationBrut(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating temImputationBrut from " + temImputationBrut() + " to " + value);
    }
    takeStoredValueForKey(value, "temImputationBrut");
  }

  public String temMgen() {
    return (String) storedValueForKey("temMgen");
  }

  public void setTemMgen(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating temMgen from " + temMgen() + " to " + value);
    }
    takeStoredValueForKey(value, "temMgen");
  }

  public String temPensionCivile() {
    return (String) storedValueForKey("temPensionCivile");
  }

  public void setTemPensionCivile(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating temPensionCivile from " + temPensionCivile() + " to " + value);
    }
    takeStoredValueForKey(value, "temPensionCivile");
  }

  public String temRafp() {
    return (String) storedValueForKey("temRafp");
  }

  public void setTemRafp(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating temRafp from " + temRafp() + " to " + value);
    }
    takeStoredValueForKey(value, "temRafp");
  }

  public String temRetenue() {
    return (String) storedValueForKey("temRetenue");
  }

  public void setTemRetenue(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating temRetenue from " + temRetenue() + " to " + value);
    }
    takeStoredValueForKey(value, "temRetenue");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOPayeRubrique.LOG.isDebugEnabled()) {
    	_EOPayeRubrique.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }


  public static EOPayeRubrique create(EOEditingContext editingContext, String prubClassement
, String prubLibelle
, String prubLibelleImp
, Integer prubMdebut
, Integer prubMfin
, String prubType
, String temBaseAssiette
, String temEstCalcule
, String temImposable
, String temImputationBrut
, String temMgen
, String temPensionCivile
, String temRafp
, String temRetenue
, String temValide
) {
    EOPayeRubrique eo = (EOPayeRubrique) EOUtilities.createAndInsertInstance(editingContext, _EOPayeRubrique.ENTITY_NAME);    
		eo.setPrubClassement(prubClassement);
		eo.setPrubLibelle(prubLibelle);
		eo.setPrubLibelleImp(prubLibelleImp);
		eo.setPrubMdebut(prubMdebut);
		eo.setPrubMfin(prubMfin);
		eo.setPrubType(prubType);
		eo.setTemBaseAssiette(temBaseAssiette);
		eo.setTemEstCalcule(temEstCalcule);
		eo.setTemImposable(temImposable);
		eo.setTemImputationBrut(temImputationBrut);
		eo.setTemMgen(temMgen);
		eo.setTemPensionCivile(temPensionCivile);
		eo.setTemRafp(temRafp);
		eo.setTemRetenue(temRetenue);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOPayeRubrique> fetchAll(EOEditingContext editingContext) {
    return _EOPayeRubrique.fetchAll(editingContext, null);
  }

  public static NSArray<EOPayeRubrique> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPayeRubrique.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPayeRubrique> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPayeRubrique.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPayeRubrique> eoObjects = (NSArray<EOPayeRubrique>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPayeRubrique fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeRubrique.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPayeRubrique fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPayeRubrique> eoObjects = _EOPayeRubrique.fetch(editingContext, qualifier, null);
    EOPayeRubrique eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPayeRubrique)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_PayeRubrique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPayeRubrique fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeRubrique.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPayeRubrique fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPayeRubrique eoObject = _EOPayeRubrique.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_PayeRubrique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPayeRubrique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeRubrique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeRubrique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeRubrique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPayeRubrique localInstanceIn(EOEditingContext editingContext, EOPayeRubrique eo) {
    EOPayeRubrique localInstance = (eo == null) ? null : (EOPayeRubrique)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
