/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPayeParamPerso.java instead.
package org.cocktail.fwkcktlgrh.common.metier.paye;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPayeParamPerso extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_PayeParamPerso";

	// Attributes
	public static final String PPAR_LIBELLE_KEY = "pparLibelle";
	public static final String PPAR_MDEBUT_KEY = "pparMdebut";
	public static final String PPAR_MFIN_KEY = "pparMfin";
	public static final String PPAR_VALEUR_KEY = "pparValeur";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> PPAR_LIBELLE = new ERXKey<String>("pparLibelle");
	public static final ERXKey<Integer> PPAR_MDEBUT = new ERXKey<Integer>("pparMdebut");
	public static final ERXKey<Integer> PPAR_MFIN = new ERXKey<Integer>("pparMfin");
	public static final ERXKey<String> PPAR_VALEUR = new ERXKey<String>("pparValeur");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String CODE_KEY = "code";
	public static final String PERSONNALISATION_KEY = "personnalisation";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeCode> CODE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeCode>("code");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso> PERSONNALISATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso>("personnalisation");

  private static Logger LOG = Logger.getLogger(_EOPayeParamPerso.class);

  public EOPayeParamPerso localInstanceIn(EOEditingContext editingContext) {
    EOPayeParamPerso localInstance = (EOPayeParamPerso)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String pparLibelle() {
    return (String) storedValueForKey("pparLibelle");
  }

  public void setPparLibelle(String value) {
    if (_EOPayeParamPerso.LOG.isDebugEnabled()) {
    	_EOPayeParamPerso.LOG.debug( "updating pparLibelle from " + pparLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "pparLibelle");
  }

  public Integer pparMdebut() {
    return (Integer) storedValueForKey("pparMdebut");
  }

  public void setPparMdebut(Integer value) {
    if (_EOPayeParamPerso.LOG.isDebugEnabled()) {
    	_EOPayeParamPerso.LOG.debug( "updating pparMdebut from " + pparMdebut() + " to " + value);
    }
    takeStoredValueForKey(value, "pparMdebut");
  }

  public Integer pparMfin() {
    return (Integer) storedValueForKey("pparMfin");
  }

  public void setPparMfin(Integer value) {
    if (_EOPayeParamPerso.LOG.isDebugEnabled()) {
    	_EOPayeParamPerso.LOG.debug( "updating pparMfin from " + pparMfin() + " to " + value);
    }
    takeStoredValueForKey(value, "pparMfin");
  }

  public String pparValeur() {
    return (String) storedValueForKey("pparValeur");
  }

  public void setPparValeur(String value) {
    if (_EOPayeParamPerso.LOG.isDebugEnabled()) {
    	_EOPayeParamPerso.LOG.debug( "updating pparValeur from " + pparValeur() + " to " + value);
    }
    takeStoredValueForKey(value, "pparValeur");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOPayeParamPerso.LOG.isDebugEnabled()) {
    	_EOPayeParamPerso.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeCode code() {
    return (org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeCode)storedValueForKey("code");
  }

  public void setCodeRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeCode value) {
    if (_EOPayeParamPerso.LOG.isDebugEnabled()) {
      _EOPayeParamPerso.LOG.debug("updating code from " + code() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeCode oldValue = code();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "code");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "code");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso personnalisation() {
    return (org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso)storedValueForKey("personnalisation");
  }

  public void setPersonnalisationRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso value) {
    if (_EOPayeParamPerso.LOG.isDebugEnabled()) {
      _EOPayeParamPerso.LOG.debug("updating personnalisation from " + personnalisation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso oldValue = personnalisation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "personnalisation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "personnalisation");
    }
  }
  

  public static EOPayeParamPerso create(EOEditingContext editingContext, String pparLibelle
, Integer pparMdebut
, Integer pparMfin
, String pparValeur
, String temValide
, org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeCode code, org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso personnalisation) {
    EOPayeParamPerso eo = (EOPayeParamPerso) EOUtilities.createAndInsertInstance(editingContext, _EOPayeParamPerso.ENTITY_NAME);    
		eo.setPparLibelle(pparLibelle);
		eo.setPparMdebut(pparMdebut);
		eo.setPparMfin(pparMfin);
		eo.setPparValeur(pparValeur);
		eo.setTemValide(temValide);
    eo.setCodeRelationship(code);
    eo.setPersonnalisationRelationship(personnalisation);
    return eo;
  }

  public static NSArray<EOPayeParamPerso> fetchAll(EOEditingContext editingContext) {
    return _EOPayeParamPerso.fetchAll(editingContext, null);
  }

  public static NSArray<EOPayeParamPerso> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPayeParamPerso.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPayeParamPerso> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPayeParamPerso.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPayeParamPerso> eoObjects = (NSArray<EOPayeParamPerso>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPayeParamPerso fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeParamPerso.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPayeParamPerso fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPayeParamPerso> eoObjects = _EOPayeParamPerso.fetch(editingContext, qualifier, null);
    EOPayeParamPerso eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPayeParamPerso)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_PayeParamPerso that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPayeParamPerso fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeParamPerso.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPayeParamPerso fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPayeParamPerso eoObject = _EOPayeParamPerso.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_PayeParamPerso that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPayeParamPerso fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeParamPerso fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeParamPerso eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeParamPerso)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPayeParamPerso localInstanceIn(EOEditingContext editingContext, EOPayeParamPerso eo) {
    EOPayeParamPerso localInstance = (eo == null) ? null : (EOPayeParamPerso)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
