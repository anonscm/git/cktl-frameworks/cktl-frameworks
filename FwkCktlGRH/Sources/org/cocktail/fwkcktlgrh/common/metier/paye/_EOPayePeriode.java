/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPayePeriode.java instead.
package org.cocktail.fwkcktlgrh.common.metier.paye;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPayePeriode extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_PayePeriode";

	// Attributes
	public static final String PPER_NB_HEURE_KEY = "pperNbHeure";
	public static final String PPER_NB_JOUR_KEY = "pperNbJour";
	public static final String TEM_COMPLET_KEY = "temComplet";
	public static final String TEM_POSITIVE_KEY = "temPositive";
	public static final String TEM_PREMIERE_PAYE_KEY = "temPremierePaye";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<java.math.BigDecimal> PPER_NB_HEURE = new ERXKey<java.math.BigDecimal>("pperNbHeure");
	public static final ERXKey<java.math.BigDecimal> PPER_NB_JOUR = new ERXKey<java.math.BigDecimal>("pperNbJour");
	public static final ERXKey<String> TEM_COMPLET = new ERXKey<String>("temComplet");
	public static final ERXKey<String> TEM_POSITIVE = new ERXKey<String>("temPositive");
	public static final ERXKey<String> TEM_PREMIERE_PAYE = new ERXKey<String>("temPremierePaye");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String CONTRAT_KEY = "contrat";
	public static final String MOIS_KEY = "mois";
	public static final String MOIS_TRAITEMENT_KEY = "moisTraitement";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat> CONTRAT = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat>("contrat");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois> MOIS = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois>("mois");
	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois> MOIS_TRAITEMENT = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois>("moisTraitement");

  private static Logger LOG = Logger.getLogger(_EOPayePeriode.class);

  public EOPayePeriode localInstanceIn(EOEditingContext editingContext) {
    EOPayePeriode localInstance = (EOPayePeriode)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal pperNbHeure() {
    return (java.math.BigDecimal) storedValueForKey("pperNbHeure");
  }

  public void setPperNbHeure(java.math.BigDecimal value) {
    if (_EOPayePeriode.LOG.isDebugEnabled()) {
    	_EOPayePeriode.LOG.debug( "updating pperNbHeure from " + pperNbHeure() + " to " + value);
    }
    takeStoredValueForKey(value, "pperNbHeure");
  }

  public java.math.BigDecimal pperNbJour() {
    return (java.math.BigDecimal) storedValueForKey("pperNbJour");
  }

  public void setPperNbJour(java.math.BigDecimal value) {
    if (_EOPayePeriode.LOG.isDebugEnabled()) {
    	_EOPayePeriode.LOG.debug( "updating pperNbJour from " + pperNbJour() + " to " + value);
    }
    takeStoredValueForKey(value, "pperNbJour");
  }

  public String temComplet() {
    return (String) storedValueForKey("temComplet");
  }

  public void setTemComplet(String value) {
    if (_EOPayePeriode.LOG.isDebugEnabled()) {
    	_EOPayePeriode.LOG.debug( "updating temComplet from " + temComplet() + " to " + value);
    }
    takeStoredValueForKey(value, "temComplet");
  }

  public String temPositive() {
    return (String) storedValueForKey("temPositive");
  }

  public void setTemPositive(String value) {
    if (_EOPayePeriode.LOG.isDebugEnabled()) {
    	_EOPayePeriode.LOG.debug( "updating temPositive from " + temPositive() + " to " + value);
    }
    takeStoredValueForKey(value, "temPositive");
  }

  public String temPremierePaye() {
    return (String) storedValueForKey("temPremierePaye");
  }

  public void setTemPremierePaye(String value) {
    if (_EOPayePeriode.LOG.isDebugEnabled()) {
    	_EOPayePeriode.LOG.debug( "updating temPremierePaye from " + temPremierePaye() + " to " + value);
    }
    takeStoredValueForKey(value, "temPremierePaye");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOPayePeriode.LOG.isDebugEnabled()) {
    	_EOPayePeriode.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat contrat() {
    return (org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat)storedValueForKey("contrat");
  }

  public void setContratRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat value) {
    if (_EOPayePeriode.LOG.isDebugEnabled()) {
      _EOPayePeriode.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "contrat");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "contrat");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois mois() {
    return (org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois)storedValueForKey("mois");
  }

  public void setMoisRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois value) {
    if (_EOPayePeriode.LOG.isDebugEnabled()) {
      _EOPayePeriode.LOG.debug("updating mois from " + mois() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "mois");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "mois");
    }
  }
  
  public org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois moisTraitement() {
    return (org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois)storedValueForKey("moisTraitement");
  }

  public void setMoisTraitementRelationship(org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois value) {
    if (_EOPayePeriode.LOG.isDebugEnabled()) {
      _EOPayePeriode.LOG.debug("updating moisTraitement from " + moisTraitement() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois oldValue = moisTraitement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "moisTraitement");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "moisTraitement");
    }
  }
  

  public static EOPayePeriode create(EOEditingContext editingContext, java.math.BigDecimal pperNbHeure
, java.math.BigDecimal pperNbJour
, String temComplet
, String temPositive
, String temPremierePaye
, String temValide
, org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat contrat, org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois mois, org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois moisTraitement) {
    EOPayePeriode eo = (EOPayePeriode) EOUtilities.createAndInsertInstance(editingContext, _EOPayePeriode.ENTITY_NAME);    
		eo.setPperNbHeure(pperNbHeure);
		eo.setPperNbJour(pperNbJour);
		eo.setTemComplet(temComplet);
		eo.setTemPositive(temPositive);
		eo.setTemPremierePaye(temPremierePaye);
		eo.setTemValide(temValide);
    eo.setContratRelationship(contrat);
    eo.setMoisRelationship(mois);
    eo.setMoisTraitementRelationship(moisTraitement);
    return eo;
  }

  public static NSArray<EOPayePeriode> fetchAll(EOEditingContext editingContext) {
    return _EOPayePeriode.fetchAll(editingContext, null);
  }

  public static NSArray<EOPayePeriode> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPayePeriode.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPayePeriode> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPayePeriode.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPayePeriode> eoObjects = (NSArray<EOPayePeriode>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPayePeriode fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayePeriode.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPayePeriode fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPayePeriode> eoObjects = _EOPayePeriode.fetch(editingContext, qualifier, null);
    EOPayePeriode eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPayePeriode)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_PayePeriode that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPayePeriode fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayePeriode.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPayePeriode fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPayePeriode eoObject = _EOPayePeriode.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_PayePeriode that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPayePeriode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayePeriode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayePeriode eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayePeriode)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPayePeriode localInstanceIn(EOEditingContext editingContext, EOPayePeriode eo) {
    EOPayePeriode localInstance = (eo == null) ? null : (EOPayePeriode)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
