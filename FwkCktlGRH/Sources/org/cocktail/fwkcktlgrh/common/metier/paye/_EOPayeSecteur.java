/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPayeSecteur.java instead.
package org.cocktail.fwkcktlgrh.common.metier.paye;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPayeSecteur extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_PayeSecteur";

	// Attributes
	public static final String PSEC_CODE_KEY = "psecCode";
	public static final String PSEC_LIBELLE_KEY = "psecLibelle";
	public static final String TEM_AUTONOME_KEY = "temAutonome";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> PSEC_CODE = new ERXKey<String>("psecCode");
	public static final ERXKey<String> PSEC_LIBELLE = new ERXKey<String>("psecLibelle");
	public static final ERXKey<String> TEM_AUTONOME = new ERXKey<String>("temAutonome");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships


  private static Logger LOG = Logger.getLogger(_EOPayeSecteur.class);

  public EOPayeSecteur localInstanceIn(EOEditingContext editingContext) {
    EOPayeSecteur localInstance = (EOPayeSecteur)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String psecCode() {
    return (String) storedValueForKey("psecCode");
  }

  public void setPsecCode(String value) {
    if (_EOPayeSecteur.LOG.isDebugEnabled()) {
    	_EOPayeSecteur.LOG.debug( "updating psecCode from " + psecCode() + " to " + value);
    }
    takeStoredValueForKey(value, "psecCode");
  }

  public String psecLibelle() {
    return (String) storedValueForKey("psecLibelle");
  }

  public void setPsecLibelle(String value) {
    if (_EOPayeSecteur.LOG.isDebugEnabled()) {
    	_EOPayeSecteur.LOG.debug( "updating psecLibelle from " + psecLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "psecLibelle");
  }

  public String temAutonome() {
    return (String) storedValueForKey("temAutonome");
  }

  public void setTemAutonome(String value) {
    if (_EOPayeSecteur.LOG.isDebugEnabled()) {
    	_EOPayeSecteur.LOG.debug( "updating temAutonome from " + temAutonome() + " to " + value);
    }
    takeStoredValueForKey(value, "temAutonome");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOPayeSecteur.LOG.isDebugEnabled()) {
    	_EOPayeSecteur.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }


  public static EOPayeSecteur create(EOEditingContext editingContext, String psecCode
, String psecLibelle
, String temAutonome
, String temValide
) {
    EOPayeSecteur eo = (EOPayeSecteur) EOUtilities.createAndInsertInstance(editingContext, _EOPayeSecteur.ENTITY_NAME);    
		eo.setPsecCode(psecCode);
		eo.setPsecLibelle(psecLibelle);
		eo.setTemAutonome(temAutonome);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOPayeSecteur> fetchAll(EOEditingContext editingContext) {
    return _EOPayeSecteur.fetchAll(editingContext, null);
  }

  public static NSArray<EOPayeSecteur> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPayeSecteur.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPayeSecteur> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPayeSecteur.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPayeSecteur> eoObjects = (NSArray<EOPayeSecteur>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPayeSecteur fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeSecteur.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPayeSecteur fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPayeSecteur> eoObjects = _EOPayeSecteur.fetch(editingContext, qualifier, null);
    EOPayeSecteur eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPayeSecteur)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_PayeSecteur that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPayeSecteur fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeSecteur.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPayeSecteur fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPayeSecteur eoObject = _EOPayeSecteur.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_PayeSecteur that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPayeSecteur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeSecteur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeSecteur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeSecteur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPayeSecteur localInstanceIn(EOEditingContext editingContext, EOPayeSecteur eo) {
    EOPayeSecteur localInstance = (eo == null) ? null : (EOPayeSecteur)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
