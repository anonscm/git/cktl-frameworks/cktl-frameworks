/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPayeMois.java instead.
package org.cocktail.fwkcktlgrh.common.metier.paye;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPayeMois extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_PayeMois";

	// Attributes
	public static final String MOIS_ANNEE_KEY = "moisAnnee";
	public static final String MOIS_CODE_KEY = "moisCode";
	public static final String MOIS_COMPLET_KEY = "moisComplet";
	public static final String MOIS_DEBUT_KEY = "moisDebut";
	public static final String MOIS_FIN_KEY = "moisFin";
	public static final String MOIS_LIBELLE_KEY = "moisLibelle";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<Integer> MOIS_ANNEE = new ERXKey<Integer>("moisAnnee");
	public static final ERXKey<Integer> MOIS_CODE = new ERXKey<Integer>("moisCode");
	public static final ERXKey<String> MOIS_COMPLET = new ERXKey<String>("moisComplet");
	public static final ERXKey<NSTimestamp> MOIS_DEBUT = new ERXKey<NSTimestamp>("moisDebut");
	public static final ERXKey<NSTimestamp> MOIS_FIN = new ERXKey<NSTimestamp>("moisFin");
	public static final ERXKey<String> MOIS_LIBELLE = new ERXKey<String>("moisLibelle");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships


  private static Logger LOG = Logger.getLogger(_EOPayeMois.class);

  public EOPayeMois localInstanceIn(EOEditingContext editingContext) {
    EOPayeMois localInstance = (EOPayeMois)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer moisAnnee() {
    return (Integer) storedValueForKey("moisAnnee");
  }

  public void setMoisAnnee(Integer value) {
    if (_EOPayeMois.LOG.isDebugEnabled()) {
    	_EOPayeMois.LOG.debug( "updating moisAnnee from " + moisAnnee() + " to " + value);
    }
    takeStoredValueForKey(value, "moisAnnee");
  }

  public Integer moisCode() {
    return (Integer) storedValueForKey("moisCode");
  }

  public void setMoisCode(Integer value) {
    if (_EOPayeMois.LOG.isDebugEnabled()) {
    	_EOPayeMois.LOG.debug( "updating moisCode from " + moisCode() + " to " + value);
    }
    takeStoredValueForKey(value, "moisCode");
  }

  public String moisComplet() {
    return (String) storedValueForKey("moisComplet");
  }

  public void setMoisComplet(String value) {
    if (_EOPayeMois.LOG.isDebugEnabled()) {
    	_EOPayeMois.LOG.debug( "updating moisComplet from " + moisComplet() + " to " + value);
    }
    takeStoredValueForKey(value, "moisComplet");
  }

  public NSTimestamp moisDebut() {
    return (NSTimestamp) storedValueForKey("moisDebut");
  }

  public void setMoisDebut(NSTimestamp value) {
    if (_EOPayeMois.LOG.isDebugEnabled()) {
    	_EOPayeMois.LOG.debug( "updating moisDebut from " + moisDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "moisDebut");
  }

  public NSTimestamp moisFin() {
    return (NSTimestamp) storedValueForKey("moisFin");
  }

  public void setMoisFin(NSTimestamp value) {
    if (_EOPayeMois.LOG.isDebugEnabled()) {
    	_EOPayeMois.LOG.debug( "updating moisFin from " + moisFin() + " to " + value);
    }
    takeStoredValueForKey(value, "moisFin");
  }

  public String moisLibelle() {
    return (String) storedValueForKey("moisLibelle");
  }

  public void setMoisLibelle(String value) {
    if (_EOPayeMois.LOG.isDebugEnabled()) {
    	_EOPayeMois.LOG.debug( "updating moisLibelle from " + moisLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "moisLibelle");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOPayeMois.LOG.isDebugEnabled()) {
    	_EOPayeMois.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }


  public static EOPayeMois create(EOEditingContext editingContext, Integer moisAnnee
, Integer moisCode
, String moisComplet
, NSTimestamp moisDebut
, NSTimestamp moisFin
, String moisLibelle
, String temValide
) {
    EOPayeMois eo = (EOPayeMois) EOUtilities.createAndInsertInstance(editingContext, _EOPayeMois.ENTITY_NAME);    
		eo.setMoisAnnee(moisAnnee);
		eo.setMoisCode(moisCode);
		eo.setMoisComplet(moisComplet);
		eo.setMoisDebut(moisDebut);
		eo.setMoisFin(moisFin);
		eo.setMoisLibelle(moisLibelle);
		eo.setTemValide(temValide);
    return eo;
  }

  public static NSArray<EOPayeMois> fetchAll(EOEditingContext editingContext) {
    return _EOPayeMois.fetchAll(editingContext, null);
  }

  public static NSArray<EOPayeMois> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPayeMois.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPayeMois> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPayeMois.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPayeMois> eoObjects = (NSArray<EOPayeMois>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPayeMois fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeMois.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPayeMois fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPayeMois> eoObjects = _EOPayeMois.fetch(editingContext, qualifier, null);
    EOPayeMois eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPayeMois)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_PayeMois that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPayeMois fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeMois.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPayeMois fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPayeMois eoObject = _EOPayeMois.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_PayeMois that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPayeMois fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeMois fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeMois eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeMois)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPayeMois localInstanceIn(EOEditingContext editingContext, EOPayeMois eo) {
    EOPayeMois localInstance = (eo == null) ? null : (EOPayeMois)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
