/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EOPayeCode.java instead.
package org.cocktail.fwkcktlgrh.common.metier.paye;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EOPayeCode extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_PayeCode";

	// Attributes
	public static final String PCOD_CODE_KEY = "pcodCode";
	public static final String PCOD_LIBELLE_KEY = "pcodLibelle";

	public static final ERXKey<String> PCOD_CODE = new ERXKey<String>("pcodCode");
	public static final ERXKey<String> PCOD_LIBELLE = new ERXKey<String>("pcodLibelle");
	// Relationships


  private static Logger LOG = Logger.getLogger(_EOPayeCode.class);

  public EOPayeCode localInstanceIn(EOEditingContext editingContext) {
    EOPayeCode localInstance = (EOPayeCode)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String pcodCode() {
    return (String) storedValueForKey("pcodCode");
  }

  public void setPcodCode(String value) {
    if (_EOPayeCode.LOG.isDebugEnabled()) {
    	_EOPayeCode.LOG.debug( "updating pcodCode from " + pcodCode() + " to " + value);
    }
    takeStoredValueForKey(value, "pcodCode");
  }

  public String pcodLibelle() {
    return (String) storedValueForKey("pcodLibelle");
  }

  public void setPcodLibelle(String value) {
    if (_EOPayeCode.LOG.isDebugEnabled()) {
    	_EOPayeCode.LOG.debug( "updating pcodLibelle from " + pcodLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "pcodLibelle");
  }


  public static EOPayeCode create(EOEditingContext editingContext, String pcodCode
, String pcodLibelle
) {
    EOPayeCode eo = (EOPayeCode) EOUtilities.createAndInsertInstance(editingContext, _EOPayeCode.ENTITY_NAME);    
		eo.setPcodCode(pcodCode);
		eo.setPcodLibelle(pcodLibelle);
    return eo;
  }

  public static NSArray<EOPayeCode> fetchAll(EOEditingContext editingContext) {
    return _EOPayeCode.fetchAll(editingContext, null);
  }

  public static NSArray<EOPayeCode> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPayeCode.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EOPayeCode> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPayeCode.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPayeCode> eoObjects = (NSArray<EOPayeCode>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPayeCode fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeCode.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPayeCode fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPayeCode> eoObjects = _EOPayeCode.fetch(editingContext, qualifier, null);
    EOPayeCode eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPayeCode)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_PayeCode that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPayeCode fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPayeCode.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EOPayeCode fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPayeCode eoObject = _EOPayeCode.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_PayeCode that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EOPayeCode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeCode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeCode eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeCode)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EOPayeCode localInstanceIn(EOEditingContext editingContext, EOPayeCode eo) {
    EOPayeCode localInstance = (eo == null) ? null : (EOPayeCode)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
