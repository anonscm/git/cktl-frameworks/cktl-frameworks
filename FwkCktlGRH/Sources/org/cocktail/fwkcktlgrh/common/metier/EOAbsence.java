/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgrh.common.metier;

import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

/**
 * Représente les absences d'un agent
 * 
 * @author Chama LAATIK
 */
public class EOAbsence extends _EOAbsence {

	private static final long serialVersionUID = -1017452588354057738L;
	
	public static EOSortOrdering SORT_DEBUT_ASC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_DEBUT_ASC = new NSArray(SORT_DEBUT_ASC);
	public static NSArray SORT_ARRAY_DEBUT_DESC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));

	public static final String C_TYPE_EXCLUSION_DETA = "DETA";
	public static final String C_TYPE_EXCLUSION_DISP = "DISP";
	public static final String C_TYPE_EXCLUSION_SNAT = "SNAT";
	public static final String C_TYPE_EXCLUSION_CGPA = "CGPA";

	public static String MATIN = "am";
	public static String APRES_MIDI = "pm";
	
	public EOAbsence() {
		super();
	}
	

	
	/**
	 * Renvoie la liste des absences validées d'un enseignant pendant l'année universitaire
	 * 
	 * @param ec : editing context
	 * @param individu : enseignant
	 * @return liste des absences
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOAbsence> rechercheAbsenceCouranteAnneeUniversitairePourEnseignant(EOEditingContext ec, EOIndividu individu, String annee) {	
		NSArray<EOSortOrdering> sortOrderingsAsc = new NSMutableArray<EOSortOrdering>();
		sortOrderingsAsc.add(EOSortOrdering.sortOrderingWithKey(
				DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));
		
		EOQualifier qualif = qualifierAbsenceCouranteAnneeUniversitaire(individu, annee);
		
		return fetchAll(ec, qualif, sortOrderingsAsc);
	}
	
	
	/**
	 * Renvoie la liste des absences validées d'un enseignant pendant l'année universitaire 
	 * filtrées selon certains types d'absence
	 * 
	 * @param ec : editing context
	 * @param individu : enseignant 
	 * @param filtreTypeAbsence : qualifier pour filtrer certains types d'absence
	 * @return liste des absences
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOAbsence> rechercheAbsenceCouranteAnneeUniversitairePourEnseignant(EOEditingContext ec, EOIndividu individu, 
			EOQualifier filtreTypeAbsence, String annee) {	
		NSArray<EOSortOrdering> sortOrderingsAsc = new NSMutableArray<EOSortOrdering>();
		sortOrderingsAsc.add(EOSortOrdering.sortOrderingWithKey(
				DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));
		
		EOQualifier qualif = 
				ERXQ.and(
						filtreTypeAbsence,
						qualifierAbsenceCouranteAnneeUniversitaire(individu, annee)
				);
		
		return fetchAll(ec, qualif, sortOrderingsAsc);
	}
	
	/**
	 * Qualifier pour avoir les absences valides d'un individu pendant l'année universitaire
	 * @param ec : editing context
	 * @param individu : enseignant
	 * @return qualifier
	 */
	public static EOQualifier qualifierAbsenceCouranteAnneeUniversitaire(EOIndividu individu, String annee) {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(EOAbsence.TEM_VALIDE_KEY, Constantes.VRAI),
						ERXQ.equals(ERXQ.keyPath(EOAbsence.INDIVIDU_KEY, EOIndividu.NO_INDIVIDU_KEY), individu.noIndividu()), 
						ERXQ.or(
							GRHUtilities.qualifierDateDansAnneeUniversitaire(EOAbsence.DATE_DEBUT, annee),
							GRHUtilities.qualifierDateDansAnneeUniversitaire(EOAbsence.DATE_FIN, annee)
						)
				);
		
		return qualifier;
	}
	
	/**
	 * @param ec editing context
	 * @param individu l'individu pour la recherche
	 * @return les absences valides de cet individu
	 */
	public static NSArray<EOAbsence> absencesValidesForIndividu(EOEditingContext ec, EOIndividu individu) {
		return absencesValidesCIRForIndividu(ec, individu, false);
	}
	
	/**
	 * @param ec editing context
	 * @param individu l'individu pour la recherche
	 * @return les absences valides de cet individu
	 */
	public static NSArray<EOAbsence> absencesValidesCIRForIndividu(EOEditingContext ec, EOIndividu individu) {
		return absencesValidesCIRForIndividu(ec, individu, true);
	}
	
	/**
	 * @param ec editing context
	 * @param individu l'individu pour la recherche
	 * @param absencePourCIR : Vrai/faux => Est-ce qu'on prend en compte les absences remontées au CIR
	 * @return liste des absences valides (uniquement celles remontées au CIR ou pas) pour l'individu concerné 
	 */
	public static NSArray<EOAbsence> absencesValidesCIRForIndividu(EOEditingContext ec, EOIndividu individu, boolean absencePourCIR) {
		if (individu == null) {
			return null;
		}
		
		EOQualifier qualifier = qualifierAbsenceValide(individu, absencePourCIR);
		return fetchAll(ec, qualifier, DATE_DEBUT.descs());
	}

	/**
	 * 
	 * @param individu : l'individu pour la recherche
	 * @param absencePourCIR : Vrai/Faux => Est-ce qu'on prend en compte les absences remontées au CIR
	 * @return qualifier pour les absences valides
	 */
	public static EOQualifier qualifierAbsenceValide(EOIndividu individu, boolean absencePourCIR) {
		EOQualifier qualifierPourCIR = null;
		
		if (absencePourCIR) {
			qualifierPourCIR = ERXQ.equals(ERXQ.keyPath(EOAbsence.TYPE_ABSENCE_KEY, EOTypeAbsence.TEM_CIR_KEY), Constantes.VRAI); 
		}
		
		EOQualifier qualifier = 
				ERXQ.and(
						qualifierPourCIR,
						ERXQ.equals(EOAbsence.INDIVIDU_KEY, individu), 
						ERXQ.equals(EOAbsence.TEM_VALIDE_KEY, Constantes.VRAI)
				);
		
		return qualifier;
	}
	
	/**
	 * @param ec editing context
	 * @param individu l'individu pour la recherche
	 * @return l'absence valide actuelle de cet individu
	 */
	public static EOAbsence absenceValideActuelleForIndividu(EOEditingContext ec, EOIndividu individu) {
		NSArray<EOAbsence> absences = absencesValidesActuellesCIRForIndividu(ec, individu, false);
		
		if (absences.size() == 0) {
			return null;
		}
		
		return absences.get(0);
	}
	
	/**
	 * @param ec editing context
	 * @param individu l'individu pour la recherche
	 * @return les absences valides actuelles de cet individu
	 */
	public static NSArray<EOAbsence> absencesValidesActuellesForIndividu(EOEditingContext ec, EOIndividu individu) {
		return absencesValidesActuellesCIRForIndividu(ec, individu, false);
	}
	
	/**
	 * @param ec editing context
	 * @param individu l'individu pour la recherche
	 * @return les absences valides actuelles de cet individu
	 */
	public static NSArray<EOAbsence> absencesValidesActuellesCIRForIndividu(EOEditingContext ec, EOIndividu individu) {
		return absencesValidesActuellesCIRForIndividu(ec, individu, true);
	}
	
	/**
	 * @param ec editing context
	 * @param individu l'individu pour la recherche
	 * @param absencePourCIR : Vrai/faux => Est-ce qu'on prend en compte les absences remontées au CIR
	 * @return liste des absences valides actuelles (uniquement celles remontées au CIR ou pas) pour l'individu concerné 
	 */
	public static NSArray<EOAbsence> absencesValidesActuellesCIRForIndividu(EOEditingContext ec, EOIndividu individu, boolean absencePourCIR) {
		if (individu == null) {
			return null;
		}
		
		EOQualifier qualifier = qualifierAbsenceActuelleValide(individu, absencePourCIR);
		return fetchAll(ec, qualifier, DATE_DEBUT.descs());
	}
	
	/**
	 * @param individu : l'individu pour la recherche
	 * @param absencePourCIR : Vrai/Faux => Est-ce qu'on prend en compte les absences remontées au CIR
	 * @return qualifier pour les absences valides actuelles
	 */
	public static EOQualifier qualifierAbsenceActuelleValide(EOIndividu individu, boolean absencePourCIR) {
		EOQualifier qualifier = 
				ERXQ.and(
					qualifierAbsenceValide(individu, absencePourCIR),
					EOAbsence.DATE_DEBUT.before(new NSTimestamp()),
					ERXQ.or(
							EOAbsence.DATE_FIN.isNull(),
							EOAbsence.DATE_FIN.after(new NSTimestamp())
							)
					);
		return qualifier;
	}
	
	/**
	 * 
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
	    this.validateObjectMetier();
	    validateBeforeTransactionSave();
	    super.validateForInsert();
	}
	
	/**
	 * 
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
	    this.validateObjectMetier();
	    validateBeforeTransactionSave();
	    super.validateForUpdate();
	}
	
	/**
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
	    super.validateForDelete();
	}
	
	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		majDateModification();
	}
	
	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * @throws NSValidation.ValidationException : exception
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		
	}
	
	/**
	 * Met à jour la date de modification à chaque modification detectée
	 */
	private void majDateModification() {
		if (changesFromCommittedSnapshot() != null) {
			setDateModification(new NSTimestamp());
		}
	}
}
