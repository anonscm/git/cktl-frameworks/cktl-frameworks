/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// $LastChangedRevision: 5074 $ DO NOT EDIT.  Make changes to EODepart.java instead.
package org.cocktail.fwkcktlgrh.common.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.*;
import er.extensions.foundation.*;

import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;

@SuppressWarnings("all")
public abstract class _EODepart extends  AfwkGRHRecord {
	public static final String ENTITY_NAME = "Fwkgrh_Depart";

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CESSATION_SERVICE_KEY = "dCessationService";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_EFFET_RADIATION_KEY = "dEffetRadiation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_RADIATION_EMPLOI_KEY = "dRadiationEmploi";
	public static final String LIEU_DEPART_KEY = "lieuDepart";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_DEPART_PREVISIONNEL_KEY = "temDepartPrevisionnel";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
	public static final ERXKey<NSTimestamp> DATE_ARRETE = new ERXKey<NSTimestamp>("dateArrete");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> D_CESSATION_SERVICE = new ERXKey<NSTimestamp>("dCessationService");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_EFFET_RADIATION = new ERXKey<NSTimestamp>("dEffetRadiation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<NSTimestamp> D_RADIATION_EMPLOI = new ERXKey<NSTimestamp>("dRadiationEmploi");
	public static final ERXKey<String> LIEU_DEPART = new ERXKey<String>("lieuDepart");
	public static final ERXKey<String> NO_ARRETE = new ERXKey<String>("noArrete");
	public static final ERXKey<String> TEM_DEPART_PREVISIONNEL = new ERXKey<String>("temDepartPrevisionnel");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	// Relationships
	public static final String TO_ABSENCE_KEY = "toAbsence";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_MOTIF_DEPART_KEY = "toMotifDepart";
	public static final String TO_RNE_KEY = "toRne";

	public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAbsence> TO_ABSENCE = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAbsence>("toAbsence");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMotifDepart> TO_MOTIF_DEPART = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMotifDepart>("toMotifDepart");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRne");

  private static Logger LOG = Logger.getLogger(_EODepart.class);

  public EODepart localInstanceIn(EOEditingContext editingContext) {
    EODepart localInstance = (EODepart)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey("commentaire");
  }

  public void setCommentaire(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "commentaire");
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey("dateArrete");
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "dateArrete");
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey("dateDebut");
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDebut");
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey("dateFin");
  }

  public void setDateFin(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFin");
  }

  public NSTimestamp dCessationService() {
    return (NSTimestamp) storedValueForKey("dCessationService");
  }

  public void setDCessationService(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dCessationService from " + dCessationService() + " to " + value);
    }
    takeStoredValueForKey(value, "dCessationService");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dEffetRadiation() {
    return (NSTimestamp) storedValueForKey("dEffetRadiation");
  }

  public void setDEffetRadiation(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dEffetRadiation from " + dEffetRadiation() + " to " + value);
    }
    takeStoredValueForKey(value, "dEffetRadiation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public NSTimestamp dRadiationEmploi() {
    return (NSTimestamp) storedValueForKey("dRadiationEmploi");
  }

  public void setDRadiationEmploi(NSTimestamp value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating dRadiationEmploi from " + dRadiationEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, "dRadiationEmploi");
  }

  public String lieuDepart() {
    return (String) storedValueForKey("lieuDepart");
  }

  public void setLieuDepart(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating lieuDepart from " + lieuDepart() + " to " + value);
    }
    takeStoredValueForKey(value, "lieuDepart");
  }

  public String noArrete() {
    return (String) storedValueForKey("noArrete");
  }

  public void setNoArrete(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating noArrete from " + noArrete() + " to " + value);
    }
    takeStoredValueForKey(value, "noArrete");
  }

  public String temDepartPrevisionnel() {
    return (String) storedValueForKey("temDepartPrevisionnel");
  }

  public void setTemDepartPrevisionnel(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating temDepartPrevisionnel from " + temDepartPrevisionnel() + " to " + value);
    }
    takeStoredValueForKey(value, "temDepartPrevisionnel");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EODepart.LOG.isDebugEnabled()) {
    	_EODepart.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOAbsence toAbsence() {
    return (org.cocktail.fwkcktlgrh.common.metier.EOAbsence)storedValueForKey("toAbsence");
  }

  public void setToAbsenceRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAbsence value) {
    if (_EODepart.LOG.isDebugEnabled()) {
      _EODepart.LOG.debug("updating toAbsence from " + toAbsence() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlgrh.common.metier.EOAbsence oldValue = toAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAbsence");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toAbsence");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EODepart.LOG.isDebugEnabled()) {
      _EODepart.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOMotifDepart toMotifDepart() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOMotifDepart)storedValueForKey("toMotifDepart");
  }

  public void setToMotifDepartRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOMotifDepart value) {
    if (_EODepart.LOG.isDebugEnabled()) {
      _EODepart.LOG.debug("updating toMotifDepart from " + toMotifDepart() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOMotifDepart oldValue = toMotifDepart();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMotifDepart");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toMotifDepart");
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey("toRne");
  }

  public void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (_EODepart.LOG.isDebugEnabled()) {
      _EODepart.LOG.debug("updating toRne from " + toRne() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRne");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toRne");
    }
  }
  

  public static EODepart create(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOMotifDepart toMotifDepart) {
    EODepart eo = (EODepart) EOUtilities.createAndInsertInstance(editingContext, _EODepart.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setToMotifDepartRelationship(toMotifDepart);
    return eo;
  }

  public static NSArray<EODepart> fetchAll(EOEditingContext editingContext) {
    return _EODepart.fetchAll(editingContext, null);
  }

  public static NSArray<EODepart> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODepart.fetch(editingContext, (EOQualifier)null, sortOrderings);
  }
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
  fetchSpec.setIsDeep(true);
  fetchSpec.setUsesDistinct(distinct);
  NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
  return eoObjects;
}

  public static NSArray<EODepart> fetch(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODepart.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODepart> eoObjects = (NSArray<EODepart>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODepart fetch(EOEditingContext editingContext, String keyName, Object value) {
    return _EODepart.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODepart fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODepart> eoObjects = _EODepart.fetch(editingContext, qualifier, null);
    EODepart eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODepart)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Fwkgrh_Depart that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODepart fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return _EODepart.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }
  
  public static EODepart fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    EODepart eoObject = _EODepart.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Fwkgrh_Depart that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  
  public static EODepart fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODepart fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODepart eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODepart)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
  public static EODepart localInstanceIn(EOEditingContext editingContext, EODepart eo) {
    EODepart localInstance = (eo == null) ? null : (EODepart)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
