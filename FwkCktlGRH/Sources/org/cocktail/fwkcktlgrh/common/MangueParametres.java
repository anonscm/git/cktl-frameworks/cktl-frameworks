package org.cocktail.fwkcktlgrh.common;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Les paramètres utilisateur de l'application Mangue.
 * 
 * @author LPR
 */

public class MangueParametres {
	
	public static final String PARAM_KEY_GESTION_HU = "org.cocktail.mangue.gestion_hu";	
	
	public static boolean isGestionHu(EOEditingContext editingContext) {
		return StringCtrl.toBool(EOGrhumParametres.parametrePourCle(editingContext, PARAM_KEY_GESTION_HU));		
	}
	
}
