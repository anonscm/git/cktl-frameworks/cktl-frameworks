/*
 * Created on 3 juin 2005
 *
 * classes de méthodes statiques, utilisables par le serveur et le client
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.fwkcktlgrh.common.utilities;

//import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author christine<BR>
 *
 * Classes de méthodes statiques, utilisables par le serveur et le client
 */
public class Utilitaires {
		
	/** Retourne un tableau dans lequel les objets du tableau d'origine ont ete remplaces par leur globalID dans l'editing context
	 * @param tableau d'objets metier
	 * @param editingContext
	 */
	public static NSArray tableauDeGlobalIDs(NSArray tableau,EOEditingContext editingContext) {
		NSMutableArray resultat = new NSMutableArray(tableau.count());
		for (java.util.Enumeration<EOGenericRecord> e = tableau.objectEnumerator();e.hasMoreElements();) {
			EOGenericRecord rec = e.nextElement();
			resultat.addObject(editingContext.globalIDForObject(rec));
		}
		return resultat;
	}

	/** Permet de d&eacute;terminer l'intervalle de temps sur lequel une information est valide */
	public static class IntervalleTemps implements NSKeyValueCoding {
		private NSTimestamp debutPeriode;
		private NSTimestamp finPeriode;

		public IntervalleTemps(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
			this.debutPeriode = debutPeriode;
			this.finPeriode = finPeriode;
		}
		// Acesseurs
		public NSTimestamp debutPeriode() {
			return debutPeriode;
		}
		public NSTimestamp finPeriode() {
			return finPeriode;
		}
		/** 2 intervalles sont identiques si ils ont les m&ecirc;mes date de d&eacute;but et de fin */
		public boolean estIdentique(IntervalleTemps intervalle) {
			if (DateCtrl.isSameDay(debutPeriode(), intervalle.debutPeriode())) {
				return (finPeriode() == null && intervalle.finPeriode() == null) ||
				(finPeriode() != null && intervalle.finPeriode() != null && DateCtrl.isSameDay(finPeriode(), intervalle.finPeriode()));
			} else {
				return false;
			}
		}
		public void takeValueForKey(Object valeur, String cle) {
			NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
		}
		public Object valueForKey(String cle) {
			return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
		}
		public String toString() {
			if (debutPeriode() == null) {
				return "[]";
			}
			String texte = "[" + DateCtrl.dateToString(debutPeriode()) + ",";
			if (finPeriode() == null) {
				texte += "...]";
			} else {
				texte += DateCtrl.dateToString(finPeriode()) + "]";
			}
			return texte;
		}
		/** Retourne l'intersection de deux p&eacute;riodes de temps sous la forme d'un intervalle */
		public static IntervalleTemps intersectionPeriodes(NSTimestamp debutPeriode1,NSTimestamp finPeriode1,NSTimestamp debutPeriode2,NSTimestamp finPeriode2) {
			// Si la deuxième période se termine avant la première ou la deuxième période commence après la fin de la première
			// il n'y a pas d'intervalle commun
			if ((finPeriode2 != null && debutPeriode1 != null && DateCtrl.isBefore(finPeriode2, debutPeriode1)) ||
					(finPeriode1 != null && debutPeriode2 != null && DateCtrl.isAfter(debutPeriode2,finPeriode1))) {
				return null;
			}
			// Chevauchement des périodes
			NSTimestamp debut = debutPeriode1;
			if (debut == null || (debutPeriode2 != null && DateCtrl.isAfter(debutPeriode2,debut))) {
				debut = debutPeriode2;
			}
			NSTimestamp fin = finPeriode1;
			if ((fin == null && finPeriode2 != null) || (fin != null && finPeriode2 != null && DateCtrl.isBefore(finPeriode2, fin))) {
				fin = finPeriode2;
			}
			return new IntervalleTemps(debut,fin);
		}
	}
}