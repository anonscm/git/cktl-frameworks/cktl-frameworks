package org.cocktail.fwkcktlgrh.common.utilities;

import java.awt.Color;

public class CocktailConstantes 
{
	public static final String[] LISTE_MOIS = new String[]{"JANVIER","FEVRIER","MARS","AVRIL","MAI","JUIN",
		"JUILLET","AOUT","SEPTEMBRE","OCTOBRE","NOVEMBRE","DECEMBRE"};
	public static final String [] LETTRES_ALPHABET = new String[] {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};

	public static final Integer[] LISTE_ANNEES_DESC = new Integer[]{new Integer("2015"),new Integer("2014"),new Integer("2013"),new Integer("2012"),
		new Integer("2011"),new Integer("2010"), new Integer("2009"),new Integer("2008"), new Integer("2007"), 
				new Integer("2006"), new Integer("2005"), new Integer("2004")};
	public static final Integer[] LISTE_ANNEES_ASC = new Integer[]{new Integer("2004"),new Integer("2005"),
		new Integer("2006"),new Integer("2007"), new Integer("2008"),new Integer("2009"), new Integer("2010"), 
				new Integer("2011"), new Integer("2012"), new Integer("2013"), new Integer("2014"), new Integer("2015")};

    public static final String DATE_NON_VALIDE_TITRE = "Date non valide";
    public static final String DATE_NON_VALIDE_MESSAGE = "La date saisie n'est pas valide !";
    public static final String ATTENTION = "ATTENTION";
    public static final String ERREUR = "ERREUR";

	public static final String SAUT_DE_LIGNE = "\n";
	public static final String SEPARATEUR_EXPORT = ";";
	public static final String SEPARATEUR_TABULATION = "\t";

	public static final String EXTENSION_CSV = ".csv";
	public static final String EXTENSION_PDF = ".pdf";
	public static final String EXTENSION_JASPER = ".jasper";
	public static final String EXTENSION_XML = ".XML";
	public static final String EXTENSION_EXCEL = ".xls";

	public static final String[] LISTE_ANNEES = new String[]{"2004","2005","2006","2007", "2008","2009"};

	public static final String STRING_EURO = " \u20ac";
	
	// COULEURS
	public static Color BG_COLOR_PINK = new Color(248,192,218);
	public static Color BG_COLOR_WHITE = new Color(255,255,255);
	public static Color BG_COLOR_BLACK = new Color(0,0,0);
	public static Color BG_COLOR_GREY = new Color(100, 100, 100);
	public static Color BG_COLOR_RED = new Color(255,0,0);
	public static Color BG_COLOR_GREEN = new Color(0,255,0);
	public static Color BG_COLOR_BLUE = new Color(0,0,255);
	public static Color BG_COLOR_YELLOW = new Color(255,255,0);
	public static Color BG_COLOR_LIGHT_GREY = new Color(100,100,100);
	public static Color BG_COLOR_CYAN = new Color(224, 255, 255);

	public static Color COULEUR_FOND_INACTIF = new Color(222,222,222);
	public static Color COULEUR_FOND_ACTIF = new Color(255, 255, 255);
	 
	public static final Color COLOR_INACTIVE_BACKGROUND = new Color(222,222,222);
	public static final Color COLOR_ACTIVE_BACKGROUND = new Color(0,0,0);

    public static final Color COLOR_BKG_TABLE_VIEW = new Color(230, 230, 230);
    public static final Color COLOR_SELECTED_ROW = new Color(127,155,165);

    public static final Color COLOR_SELECT_NOMENCLATURES = new Color(100,100,100);
    public static final Color COLOR_FOND_NOMENCLATURES = new Color(220,220,220);
    public static final Color COLOR_FILTRES_NOMENCLATURES = new Color(240,240,240);
	
	public static final String VRAI = "O";
	public static final String FAUX = "N";

	public final static String MONSIEUR = "M.";
	public final static String MADAME = "MME";
	public final static String MADEMOISELLE = "MLLE";
	    
}