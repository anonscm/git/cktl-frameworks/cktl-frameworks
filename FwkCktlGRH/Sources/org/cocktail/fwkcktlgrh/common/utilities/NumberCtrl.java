package org.cocktail.fwkcktlgrh.common.utilities;

import java.math.BigDecimal;

import com.webobjects.foundation.NSArray;

public class NumberCtrl
{

	/** */
	public static final String[] listeChiffres = {"0","1","2","3","4","5","6","7","8","9"};

	public static final String[] listeNumber = {"0","1","2","3","4","5","6","7","8","9",".",",","-"};

	/** */
	public static boolean estUnChiffre(String chaine) {
		NSArray chiffres = new NSArray(listeChiffres);
		if (chiffres.containsObject(chaine))	return true;

		return false;
	}

	public static boolean estUnEntier(BigDecimal myValue) {

			int intValue = myValue.intValue();
			System.out.println("NumberCtrl.estUnEntier() " + myValue);
			System.out.println("NumberCtrl.estUnEntier() " + intValue);
			BigDecimal decimalPartBD = myValue.subtract(BigDecimal.valueOf(intValue)).setScale(2, BigDecimal.ROUND_UP);
			System.out.println("NumberCtrl.estUnEntier() " + decimalPartBD);

			return decimalPartBD.intValue() == 0;
	}
	
	public static Number max(Number n1, Number n2)
	{
		if (n1.intValue() >= n2.intValue())
			return n1;

		return n2;		
	}

	/** Chaine ne contenant que des chiffres, . ou , */
	public static boolean isANumber(String chaine) {

		NSArray nombre = new NSArray(listeNumber);

		for (int i=0;i< chaine.length();i++) {
			if (!nombre.containsObject(""+chaine.charAt(i)))
				return false;
		}

		return true;
	}
}
