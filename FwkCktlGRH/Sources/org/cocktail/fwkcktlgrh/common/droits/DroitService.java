package org.cocktail.fwkcktlgrh.common.droits;

import org.cocktail.fwkcktlgrh.common.metier.services.StructureGrhService;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

/**
 * Ensemble de services pour la gestion des droits GRH
 * @author juliencallewaert
 *
 */
public class DroitService {

	/**
	 * Obtenir les membres d'un groupe jusqu'à arriver à des individus (méthode
	 * récursive). On ignore les structures membres d'elles memes
	 * 
	 * @param eoStructure structure
	 * @return
	 */
	public static NSArray<EOIndividu> getEoIndividuMembreNonEns(EOEditingContext ec, EOStructure eoStructure, boolean rechercheSousService) {
		NSMutableArray<EOIndividu> array = new NSMutableArray<EOIndividu>();

		StructureGrhService structureGrhService = new StructureGrhService();
	
		for (EORepartStructure repart : eoStructure.toRepartStructuresElts()) {
			IPersonne membre = repart.toPersonneElt();
			if (membre.isIndividu()) {
				NSArray<EOIndividu> arrayIndividu = structureGrhService.getIndividuAffecteVPersonnelNonEns(ec, (EOStructure) repart.toStructureGroupe());
				if (!arrayIndividu.containsObject((EOIndividu) membre)) {
					array.addObject((EOIndividu) membre);
				}
			} else if (membre.isStructure() && membre.persId().intValue() != eoStructure.persId().intValue() && rechercheSousService) {
				array.addObjectsFromArray(getEoIndividuMembreNonEns(ec, (EOStructure) membre, rechercheSousService));
			}
		}

		return array.immutableClone();
	}
	
	/**
	 * 
	 * @param eoStructure
	 * @return
	 */
	public static NSArray<EOIndividu> getEoIndividuMembre(EOStructure eoStructure, boolean rechercheSousService) {
		NSMutableArray<EOIndividu> array = new NSMutableArray<EOIndividu>();

		for (EORepartStructure repart : eoStructure.toRepartStructuresElts()) {
			IPersonne membre = repart.toPersonneElt();
			if (membre.isIndividu()) {
				array.addObject((EOIndividu) membre);
			} else if (membre.isStructure() && membre.persId().intValue() != eoStructure.persId().intValue() && rechercheSousService) {
				array.addObjectsFromArray(getEoIndividuMembre((EOStructure) membre, rechercheSousService));
			}
		}

		return array.immutableClone();
	}
	
	
	/**
	 * Obtenir les membres d'un groupe jusqu'à arriver à des individus (sans
	 * récursivité). 
	 * 
	 * @param eoStructure
	 * @return
	 */
	public static NSArray<IPersonne> getEoIndividuMembre(EOStructure eoStructure, EOQualifier qualPourPersonnne) {
		return EOStructureForGroupeSpec.getPersonnesInGroupe(eoStructure, qualPourPersonnne);
	}
	
	
	/**
	 * La liste des structures dont l'agent est responsable via Annuaire
	 * Recherche sur la vue Service
	 * 
	 * @return
	 */
	public static NSArray<EOStructure> getEoStructureResponsableAnnuaireArray(EOEditingContext ec, Integer persId) {
		
		EOQualifier qual = ERXQ.and(ERXQ.equals(EOStructure.TO_RESPONSABLE_KEY, EOIndividu.individuWithPersId(ec, persId)),
				EOStructure.QUAL_STRUCTURES_AS_SERVICES);
		NSArray<EOStructure> serviceRespList = (NSArray<EOStructure>) EOStructure.fetchAll(ec, qual);
		
		NSArray<EOStructure> structureRespList = new NSMutableArray<EOStructure>();
		
		for (EOStructure service : serviceRespList) {
			structureRespList.addAll(service.tosSousServiceDeep(false));
		}
		
		return structureRespList;
	}
	
	
	public static EOStructure getGroupeProfil(EOEditingContext ec, EOGdProfil profil){
		return (EOStructure) ERXEOControlUtilities.objectWithPrimaryKeyValue(ec, EOStructure.ENTITY_NAME, profil.toGroupeDynamique().toRegle().rValue(), null);
	}
	
	
	
}
