package org.cocktail.fwkcktlgrh.common;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSForwardException;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.qualifiers.ERXKeyValueQualifier;

public class ERXEOEqualsQualifier extends ERXKeyValueQualifier {

    public ERXEOEqualsQualifier(String key, EOEnterpriseObject value) {
        super(key, EOQualifier.QualifierOperatorEqual, value);
    }
    
    @Override
    public boolean evaluateWithObject(Object object) {
        if (object != null && object instanceof EOEnterpriseObject) {
            Object target = ((EOEnterpriseObject)object).valueForKeyPath(key());
            if (target instanceof EOEnterpriseObject)
                return ERXEOControlUtilities.eoEquals((EOEnterpriseObject)value(), (EOEnterpriseObject)target);
        }
        return super.evaluateWithObject(object);
    }
    
    public boolean eoQuals(Object left, Object right)
    {
       if (left != null && left instanceof EOEnterpriseObject &&
           right != null && right instanceof EOEnterpriseObject) {
           return ERXEOControlUtilities.eoEquals((EOEnterpriseObject)left, (EOEnterpriseObject)right);
       } else {
           try {
            return (Boolean)EOQualifier.QualifierOperatorEqual.invoke(this, left, right);
        } catch (Throwable t) {
            throw new NSForwardException(t);
        }
       }
    }
    
}
