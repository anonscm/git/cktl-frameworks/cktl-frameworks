package org.cocktail.grh.hamac;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.grh.support.q.mangue.QAffectation;
import org.cocktail.ref.support.q.grhum.QIndividuUlr;
import org.cocktail.ref.support.q.grhum.QRepartTypeGroupe;
import org.cocktail.ref.support.q.grhum.QVPersonnelNonEns;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.Expression;
import com.mysema.query.types.expr.BooleanExpression;

@Repository
public class AffectationMangueHamacReadRepository {

	private QueryDslJdbcTemplate template;
	
	private QVPersonnelNonEns vPersonnelNonEns = QVPersonnelNonEns.vPersonnelNonEns;
	private QRepartTypeGroupe repartTypeGroupe = QRepartTypeGroupe.repartTypeGroupe;
	private QIndividuUlr individuUlr = QIndividuUlr.individuUlr;
	private QAffectation affectation = QAffectation.affectation;
	
	
	@Autowired
	public AffectationMangueHamacReadRepository(QueryDslJdbcTemplate template) {
		this.template = template;
	}
	
	
	/**
	 * Retourne toutes les affectations
	 * @param datePriseEnCharge date
	 * @param cStructure code structure
	 * @param nomAgent nom de l'agent
	 * @return La liste des diplômes
	 */
	public List<AffectationMangueHamacRead> getPersonnelEligibleHamac(Date datePriseEnCharge, String cStructure, String nomAgent) {
		
		SQLQuery query = template.newSqlQuery()
			.from(affectation)
			.innerJoin(vPersonnelNonEns).on(affectation.noDossierPers.eq(vPersonnelNonEns.noDossierPers))
			.innerJoin(repartTypeGroupe).on(affectation.cStructure.eq(repartTypeGroupe.cStructure))
			.innerJoin(individuUlr).on(vPersonnelNonEns.noDossierPers.eq(individuUlr.noIndividu))
			.where(individuUlr.temValide.eq(Constantes.VRAI)
					.and(affectation.temValide.eq(Constantes.VRAI))
					.and(repartTypeGroupe.tgrpCode.eq(EOTypeGroupe.TGRP_CODE_S))
					.and(affectation.cStructure.like(cStructure != null ? cStructure : "%%"))
					.and(individuUlr.nomUsuel.like(nomAgent != null ? StringUtils.upperCase(nomAgent) : "%%"))
					.and(buildClauseDatePriseEnCharge(datePriseEnCharge)))
			.orderBy(affectation.dDebAffectation.asc()).distinct();
		
		Expression<?>[] paths = new Expression<?>[] {
				affectation.noDossierPers,
				individuUlr.persId,
				affectation.cStructure,
				affectation.dDebAffectation,
				affectation.dFinAffectation,
				affectation.numQuotAffectation
		};
		
		AffectationMangueHamacReadMapping affectationMangueHamacReadMapping = new AffectationMangueHamacReadMapping(paths);
		List<AffectationMangueHamacRead> res = template.query(query, affectationMangueHamacReadMapping);
		
		return res;
		
	}

	private BooleanExpression buildClauseDatePriseEnCharge(Date datePriseEnCharge) {
		Timestamp t = null;
		
		if (datePriseEnCharge != null) {
			t = new Timestamp(datePriseEnCharge.getTime());
			return affectation.dFinAffectation.isNull().or(affectation.dFinAffectation.after(t));
		}
		
		return null;
	}
	
	
	
}
