package org.cocktail.grh.hamac;

import org.cocktail.grh.support.q.mangue.QAffectation;
import org.cocktail.ref.support.q.grhum.QIndividuUlr;

import com.mysema.query.Tuple;
import com.mysema.query.types.Expression;
import com.mysema.query.types.MappingProjection;

public class AffectationMangueHamacReadMapping extends MappingProjection<AffectationMangueHamacRead>{

	

	public AffectationMangueHamacReadMapping(Expression<?>[] paths) {
		super(AffectationMangueHamacRead.class, paths);
	}

	@Override
	protected AffectationMangueHamacRead map(Tuple row) {

		AffectationMangueHamacRead affectationMangueHamacRead = new AffectationMangueHamacRead();
		
		affectationMangueHamacRead.setNoIndividu(row.get(QAffectation.affectation.noDossierPers));
		affectationMangueHamacRead.setPersId(row.get(QIndividuUlr.individuUlr.persId));
		affectationMangueHamacRead.setCodeStructureAffectation(row.get(QAffectation.affectation.cStructure));
		affectationMangueHamacRead.setDateDebutAffectation(row.get(QAffectation.affectation.dDebAffectation));
		affectationMangueHamacRead.setDateFinAffectation(row.get(QAffectation.affectation.dFinAffectation));
		affectationMangueHamacRead.setNumQuotAffectation(row.get(QAffectation.affectation.numQuotAffectation));
		
		return affectationMangueHamacRead;
	}

}
