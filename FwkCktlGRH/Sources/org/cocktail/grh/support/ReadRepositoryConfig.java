package org.cocktail.grh.support;

import org.cocktail.grh.hamac.AffectationMangueHamacReadRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;


@Configuration
public class ReadRepositoryConfig {

	@Bean
	public AffectationMangueHamacReadRepository affectationMangueHamacReadRepository(QueryDslJdbcTemplate template) {
		return new AffectationMangueHamacReadRepository(template);
	}
	
}
