package org.cocktail.fwkcktlgrh.common.metier.services;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;


public class CarrieresServiceTest {
	
	private SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
	
	private Date dt;
	
	@Before
	public void setUpDate() throws Exception {
		dt = SDF.parse("01/05/1976");
	}
	
	
	@Test
	public void testFormatDate() {
		CarrieresService service = new CarrieresService();
		
		assertEquals("01 mai 1976", service.formatDate(dt));
		
	}

}
