package org.cocktail.fwkcktlgrh.common.metier.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.metier.EOCarriere;
import org.cocktail.fwkcktlgrh.common.metier.EOCarriereSpecialisations;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.rules.MockEditingContext;

/**
 * Classe de test de la classe de service CarriereSpecialisationsService
 */
public class CarriereSpecialisationsServiceTest {
	
	private static final Integer PERS_ID = 12;
	private static final Integer ID_CARRIERE = 1;
	private static final String DATE_DEBUT_CARRIERE_1 = "01/01/2013";
	private static final String DATE_DEBUT_CARRIERE_2 = "01/01/3015";
	private static final String DATE_FIN_CARRIERE = "31/12/2013";
	private static final String DATE_DU_JOUR = "25/07/2014";
	
	private EOIndividu individu;
	private EOPersonnel personnel;
	private EOTypePopulation typePopulation;
	
	@Rule
	public MockEditingContext editingContext = new MockEditingContext("FwkCktlGRH");
	
	@Before
    public void setUp() {
		typePopulation = getTypePopulation();
		
		individu = getIndividu();
		editingContext.insertObject(individu);
		
		personnel = getPersonnel(individu);
		editingContext.insertObject(personnel);
	}
	
	@Test
	public void controleCarriereExiste() {
		//Arrange
		EOCarriere carriere = getCarriere(typePopulation, individu, personnel, DATE_DEBUT_CARRIERE_1, null);
		editingContext.saveChanges();
		
		//Assert ready to test
		EOCarriere carriereFetch = EOCarriere.fetch(editingContext, EOCarriere.NO_SEQ_CARRIERE.eq(ID_CARRIERE));
		assertNotNull(carriereFetch);
		
		//Act
		EOCarriereSpecialisations carriereSpecialisation = initCarriereSpecialisation(individu, carriere);
		editingContext.saveChanges();
		
		//Verify
		CarriereSpecialisationsService carriereSpecialisationsService = new CarriereSpecialisationsService(editingContext);
		assertEquals(carriereSpecialisation, carriereSpecialisationsService.getCurrentCarriereSpecialisation(individu));
	}
	
	@Test
	public void controleCarrierePasDebutee() {
		//Arrange		
		EOCarriere carriere = getCarriere(typePopulation, individu, personnel, DATE_DEBUT_CARRIERE_2, null);
		editingContext.saveChanges();
		
		//Assert ready to test
		EOCarriere carriereFetch = EOCarriere.fetch(editingContext, EOCarriere.NO_SEQ_CARRIERE.eq(ID_CARRIERE));
		assertNotNull(carriereFetch);
		
		//Act
		initCarriereSpecialisation(individu, carriere);
		editingContext.saveChanges();
		
		//Verify
		CarriereSpecialisationsService carriereSpecialisationsService = new CarriereSpecialisationsService(editingContext);
		assertEquals(null, carriereSpecialisationsService.getCurrentCarriereSpecialisation(individu));
	}
	
	@Test
	public void controleCarriereTerminee() {
		//Arrange		
		EOCarriere carriere = getCarriere(typePopulation, individu, personnel, DATE_DEBUT_CARRIERE_1, DATE_FIN_CARRIERE);
		editingContext.saveChanges();
		
		//Assert ready to test
		EOCarriere carriereFetch = EOCarriere.fetch(editingContext, EOCarriere.NO_SEQ_CARRIERE.eq(ID_CARRIERE));
		assertNotNull(carriereFetch);
		
		//Act
		initCarriereSpecialisation(individu, carriere);
		editingContext.saveChanges();
		
		//Verify
		CarriereSpecialisationsService carriereSpecialisationsService = new CarriereSpecialisationsService(editingContext);
		assertEquals(null, carriereSpecialisationsService.getCurrentCarriereSpecialisation(individu));
	}
	
	private NSTimestamp dateDuJour() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date date = simpleDateFormat.parse(DATE_DU_JOUR);
			return new NSTimestamp(date);
        } catch (ParseException e) {
	        e.printStackTrace();
        }
		return null;
	}
	
	private EOCarriereSpecialisations initCarriereSpecialisation(EOIndividu ind, EOCarriere carriere) {
		
		EOCarriereSpecialisations carriereSpecialisation = new EOCarriereSpecialisations();
		editingContext.insertObject(carriereSpecialisation);
		carriereSpecialisation.setToCarriereRelationship(carriere);
		carriereSpecialisation.setToIndividuRelationship(ind);
		carriereSpecialisation.setSpecDebut(dateDuJour());
		carriereSpecialisation.setSpecFin(null);
		carriereSpecialisation.setDCreation(dateDuJour());
		carriereSpecialisation.setDModification(dateDuJour());
		
		return carriereSpecialisation;
	}
	
	private EOIndividu getIndividu() {
		EOCivilite civilite = editingContext.createSavedObject(EOCivilite.class);
		
		EOIndividu ind = spy(editingContext.createSavedObject(EOIndividu.class));
		doReturn(false).when(ind).objectHasChanged();
		doReturn(PERS_ID).when(ind).persId();
		
		ind.setPrenom("prenom");
		ind.setNomUsuel("nom");
		ind.setToCiviliteRelationship(civilite);
		ind.setDCreation(dateDuJour());
		ind.setDModification(dateDuJour());
		
		return ind;
	}
	
	private EOPersonnel getPersonnel(EOIndividu ind) {
		EOPersonnel pers = editingContext.createSavedObject(EOPersonnel.class);
		pers.setDCreation(dateDuJour());
		pers.setDModification(dateDuJour());
		pers.setIdMinistere(1);
		pers.setTemBudgetEtat(Constantes.VRAI);
		pers.setTemImposable(Constantes.VRAI);
		pers.setTemPaieSecu(Constantes.VRAI);
		pers.setTemTitulaire(Constantes.VRAI);
		pers.setToIndividuRelationship(ind);
		editingContext.insertObject(pers);
		return pers;
	}
	
	private EOCarriere getCarriere(EOTypePopulation typePopulation, EOIndividu ind, EOPersonnel personnel, String dateDebutCarriere, String dateFinCarriere) {
		EOCarriere carriere = new EOCarriere();
		editingContext.insertObject(carriere);
		
		carriere.setDCreation(dateDuJour());
		carriere.setDModification(dateDuJour());
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date date = simpleDateFormat.parse(dateDebutCarriere);
			carriere.setDDebCarriere(new NSTimestamp(date));
        } catch (ParseException e) {
	        e.printStackTrace();
        }

		if (dateFinCarriere != null) {
			try {
				Date date = simpleDateFormat.parse(dateFinCarriere);
				carriere.setDFinCarriere(new NSTimestamp(date));
	        } catch (ParseException e) {
		        e.printStackTrace();
	        }
		}
		
		carriere.setTemValide(Constantes.VRAI);
		carriere.setToTypePopulationRelationship(typePopulation);
		carriere.setNoSeqCarriere(ID_CARRIERE);
		carriere.setToIndividuRelationship(ind);		
		carriere.setToPersonnelRelationship(personnel);
		
		return carriere;
	}

	private EOTypePopulation getTypePopulation() {
	    EOTypePopulation typePop = EOTypePopulation.creerInstance(editingContext);
		typePop.setDCreation(dateDuJour());
		typePop.setDModification(dateDuJour());
		editingContext.insertObject(typePop);
	    return typePop;
    }
	
}
