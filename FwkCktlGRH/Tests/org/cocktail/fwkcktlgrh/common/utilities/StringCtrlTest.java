package org.cocktail.fwkcktlgrh.common.utilities;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class StringCtrlTest {

	@Test
	public void testArrayToStringSeparatedBy() {
		
		String chaine1 = "chaine1";
		String chaine2 = "chaine2";
		String chaine3 = "chaine3";
		String chaine4 = "chaine4";
		String chaine5 = "chaine5";
		
		String sep_point = ".";
		String sep_virgule_espace = ", ";
		
		List<String> listeChaine = new ArrayList<String>();
		listeChaine.add(chaine1);
		listeChaine.add(chaine2);
		listeChaine.add(chaine3);
		listeChaine.add(chaine4);
		listeChaine.add(chaine5);
		
		assertEquals(chaine1 + sep_point + chaine2 + sep_point + chaine3 + sep_point + chaine4 + sep_point + chaine5, 
					StringCtrl.arrayToStringSeparatedBy(listeChaine, sep_point));
		
		listeChaine = new ArrayList<String>();
		listeChaine.add(chaine1);
		assertEquals(chaine1, StringCtrl.arrayToStringSeparatedBy(listeChaine, sep_point));
		
		listeChaine = null;
		assertEquals(null, StringCtrl.arrayToStringSeparatedBy(listeChaine, sep_point));
		
		listeChaine = new ArrayList<String>();
		listeChaine.add(chaine1);
		listeChaine.add(chaine2);
		listeChaine.add(chaine3);
		listeChaine.add(chaine4);
		listeChaine.add(chaine5);
		
		assertEquals(chaine1 + sep_virgule_espace + chaine2 + sep_virgule_espace + chaine3 + sep_virgule_espace + chaine4 + sep_virgule_espace + chaine5, 
					StringCtrl.arrayToStringSeparatedBy(listeChaine, sep_virgule_espace));
		
	}
	
}
