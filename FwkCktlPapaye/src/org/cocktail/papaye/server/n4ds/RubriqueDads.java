/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.n4ds;

import java.util.Enumeration;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author christine
 *
 * Contient toutes les informations utiles pour la generation des rubriques DADS-U
 */
public class RubriqueDads {
	private String nom;
	private String cle;
	private String exceptions;
	private boolean estObligatoire,estSauvegardee;
	private NSMutableArray sousRubriques;		// tableau de sous-rubriques DADS
	
	// constructeur
	/**
	 * @param nom
	 * @param cle
	 * @param estObligatoire
	 */
	public RubriqueDads(String nom, String cle, boolean estObligatoire,boolean estSauvegardee) {
		super();
		this.nom = nom;
		this.cle = cle;
		this.estObligatoire = estObligatoire;
		this.estSauvegardee = estSauvegardee;
		sousRubriques = new NSMutableArray();
	}
	/**
	 * @return Retourne la cle de la rubrique (i.e  le libelle a utiliser dans la DADS)
	 */
	public String cle() {
		return cle;
	}
	
	/**
	 * @return retourne true si une rubrique est obligatoire
	 */
	public boolean estObligatoire() {
		return estObligatoire;
	}

	/**
	 * @return retourne true si une rubrique doit &ecirc;tre enregistree dans la base
	 */
	public boolean estSauvegardee() {
		return estSauvegardee;
	}
	/**
	 * @return Retourne le nom de la rubrique
	 */
	public String nom() {
		return nom;
	}
	/**
	 * @return Retourne la liste des sous-rubriques the sousRubriques.
	 */
	public NSArray sousRubriques() {
		return sousRubriques;
	}
	/**
	 * Ajoute une sous-rubrique a la liste des sous-rubriques
	 * @param sousRubrique sous-rubrique à ajouter
	 */
	public void ajouterSousRubrique(SousRubriqueDads sousRubrique) {
		sousRubriques.addObject(sousRubrique);
	}

	public SousRubriqueDads sousRubriqueAvecEntete(String entete) {
		java.util.Enumeration e = sousRubriques.objectEnumerator();
		while (e.hasMoreElements()) {
			SousRubriqueDads sousRubriqueDADS = (SousRubriqueDads)e.nextElement();
			if (sousRubriqueDADS.cle().equals(entete)) {
				return sousRubriqueDADS;
			}
		}
		return null;
	}
	public String toString() {
		return shortString() + "\nSous-rubriques :\n" + sousRubriques();
	}
	/**
	 * Pre une rubrique i.e toutes les sous-rubriques de celle-ci
	 * @param parametres param&egrave;tres fournis pour l'&acute;valuation de la rubrique
	 * @param classes	classes des param&egrave;tres
	 * @return	tableau de resultat de l'evaluation des sous-rubriques (SousRubriqueAvecValeur)
	 */
	public NSArray preparer(Object parametres[],Object classes[]) throws Exception {
		// on retournera une chaîne de caractères qui est la concaténation de l'évaluation de toutes les rubriques
		NSMutableArray resultats = new NSMutableArray();
		exceptions = "";
		//System.out.println("Preparation de la rubrique " + cle());
		Enumeration e = sousRubriques().objectEnumerator();
		while (e.hasMoreElements()) {
			SousRubriqueDads sousRubrique = (SousRubriqueDads)e.nextElement();
			try {
				SousRubriqueAvecValeur resultatSousRubrique = sousRubrique.preparer(parametres,classes);
				if (resultatSousRubrique != null) {
					// Créer 
					resultats.addObject(resultatSousRubrique);
				}			
			} catch (Exception e1) {
				if (e1.getMessage() == null) {	// exception suite à une erreur
					e1.printStackTrace();
					throw e1;
				} else {
					exceptions = exceptions + "\n" + e1.getMessage();
				}
			}
		}
		// pour l'instant, on se contente d'afficher les exceptions, il faudra ensuite les générer
		if (exceptions.equals("") == false) {
			System.out.println("liste des exceptions" + exceptions);
		}
		// supprimer le dernier paramètre et vérifier que la chaîne n'est pas non vide dans le cas d'une rubrique obligatoire
		if (estObligatoire() && resultats.count() == 0) {
			throw new Exception("Pas de donnees evaluees pour la rubrique " + cle() + " alors qu'elle est obligatoire");
		}
		return resultats;
	}
	/**
	 * @return retourne la liste des exceptions generees pendant la preparation de la rubrique
	 */
	public String getExceptions() {
		return exceptions;
	}
	/**
	 * Retourne une version resum&eaute;e de la rubrique
	 * @return
	 */
	public String shortString() {
		String texte = "nom : " + nom + " cle : " + cle;
		if (estObligatoire) {
			texte = texte + ", obligatoire";
		}
		return texte;
	}
	
}
