/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.n4ds;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Enumeration;

import org.cocktail.papaye.server.common.DateCtrl;
import org.cocktail.papaye.server.metier.grhum.EOIndividu;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author christine
 *
 * permet de limiter la creation de la DADS a certaines categories de personnel
 */
public class EnvironnementDads {
	private static EnvironnementDads sharedInstance;
	private int nbDecimales;
	private boolean multiContrats;
	private boolean estInitialise;
	private int codeCIPDZ;
	private NSArray statuts;
	private NSArray individus;
	private String listeErreurs;
	public static final String FICHIER_ERREUR = "ERREURS_DADS.txt";


	public static EnvironnementDads sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new EnvironnementDads();		
		}
		return sharedInstance;
	}
	public void init() {
		multiContrats = false;
		codeCIPDZ = 0;
		statuts = null;
		individus = null;
		estInitialise = false;
		listeErreurs = "N4DS - Rapport de la generation du " + DateCtrl.dateToString(new NSTimestamp()) + " .\n\n";
	}
	/** initialise l'environnement DADS */
	public void init(boolean isMultiContrats,int codeCIPDZ,NSArray statuts,NSArray agents) {
		setMultiContrats(isMultiContrats);
		setCodeCIPDZ(codeCIPDZ);
		setStatuts(statuts);
		setIndividus(agents);
		estInitialise = true;
		listeErreurs = "";
	}

	/** retourne true si l'environnement a ete initialise */
	public boolean estInitialise() {
		return estInitialise;
	}
	/**
	 * @return retourne le nombre de decimales dans les montants
	 */
	protected int nbDecimales() {
		return nbDecimales;
	}
	/**
	 * @param nbDecimales Modifie le nombre de decimales a utiliser dans les montants
	 */
	protected void setNbDecimales(int nbDecimales) {
		this.nbDecimales = nbDecimales;
	}
	/**
	 * @return retourne la liste des statuts pour lesquels on cree la DADS
	 */
	public NSArray getStatuts() {
		return statuts;
	}
	/**
	 * @param statuts Les statuts pour lesquels on veut creer la DADS
	 */
	public void setStatuts(NSArray statuts) {
		this.statuts = statuts;
		estInitialise = true;
	}
	/**
	 * @return retourne la liste des individus pour lesquels on cree la DADS
	 */
	public NSArray individus() {
		return individus;
	}
	/**
	 * @param individus Les individus pour lesquels on veut creer la DADS
	 */
	public void setIndividus(NSArray individus) {
		this.individus = individus;
	}
	/**
	 * @return retourne le codeCIPDZ.
	 */
	public int getCodeCIPDZ() {
		return codeCIPDZ;
	}
	public void setCodeCIPDZ(int codeCIPDZ) {
		this.codeCIPDZ = codeCIPDZ;
		estInitialise = true;
	}
	/**
	 * @return true si on veut creer une DADS uniquement pour les individus ayant plusieurs contrats
	 */
	public boolean isMultiContrats() {
		return multiContrats;
	}

	public void setMultiContrats(boolean multiContrats) {
		this.multiContrats = multiContrats;
		estInitialise = true;
	}

	/** retourne les messages d'erreur */
	public String listeErreurs() {
		return listeErreurs;
	}
	/** ajoute un message d'erreurs a la liste des erreurs */
	public void ajouterErreur(String texte) {
		listeErreurs = listeErreurs + texte + "\n";
	}
	/** ajoute un message d'erreurs a la liste des erreurs */
	public void ajouterErreurUnique(String texte) {
		if (listeErreurs.indexOf(texte) < 0) {
			ajouterErreur(texte);
		}
	}
	/** enregistre les fichiers d'erreur et d'informations debug */
	public void genererRapports(String path) {
		if (listeErreurs.equals("")) {
			listeErreurs = "Pas d'erreurs identifiées dans la DADS.";
		}
		genererFichier(path,FICHIER_ERREUR,listeErreurs);
	}
	public boolean individuValide(EOIndividu individu,IndividuDads individuDADS) {
		if (estInitialise() == false) { // on veut tous les individus
			return true;
		} else {		
			if (isMultiContrats()) {
				return individuDADS.periodesActivites().count() > 1;
			}
			if (getCodeCIPDZ() > 0) {
				Enumeration e = individuDADS.periodesActivites().objectEnumerator();
				while (e.hasMoreElements()) {
					PeriodeDadsActive periode  = (PeriodeDadsActive)e.nextElement();
					if (periode.contrat().statut().pstaCipdz().equals(new Integer(getCodeCIPDZ()).toString())) {
						return true;
					}
				}
				return false;
			}
			if (statuts != null && statuts.count() > 0) {
				Enumeration e = individuDADS.periodesActivites().objectEnumerator();
				while (e.hasMoreElements()) {
					PeriodeDadsActive periode  = (PeriodeDadsActive)e.nextElement();
					if (statuts.containsObject(periode.contrat().statut())) {
						return true;
					}
				}
				return false;
			}
			if (individus != null && individus.count() > 0) {
				return (individus.containsObject(individu)) ;	
			} else {
				return true;
			}
		}
	}
	public boolean statutPourIndividuDADSValide(EOPayeStatut statut,IndividuDads individu) {
		if (statuts == null) {
			return true;
		} else {
			Enumeration e = individu.periodesActivites().objectEnumerator();
			while (e.hasMoreElements()) {
				PeriodeDadsActive periode  = (PeriodeDadsActive)e.nextElement();
				if (statuts.containsObject(periode.contrat().statut())) {
					return true;
				}
			}
			return false;
		}
	}
	// mÔøΩthodes privÔøΩes
	private void genererFichier(String path,String nom,String texte) {	
		try {
			// crÔøΩer le fichier
			String filePath = new String(path);
			filePath = path.concat(System.getProperty("file.separator"));
			filePath =  filePath.concat(nom);
			BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
			writer.write(texte,0,texte.length());
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

}
