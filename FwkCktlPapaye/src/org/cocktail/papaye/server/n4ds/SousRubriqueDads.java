/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.papaye.server.n4ds;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author christine
 *
 * Decrite une sous-rubrique de la DADS-U
 */
// 25/06/2010 - caractère escape placé devant le point dans replaceAll
public class SousRubriqueDads {
	private String cle;
	private boolean estObligatoire;
	private boolean aLongueurFixe;
	private boolean accepteValeurNulle;
	private String valeur;
	private String nature;
	private String typeDonnee;
	private int longueur;
	private String classeAssociee;
	private static boolean derniereRubriqueNumeriqueNegative;
	private static final int LONGUEUR_MAX = 256;
	// constructeur
	/**
	 * @param cle
	 * @param estObligatoire
	 * @param aLongueurFixe
	 */
	public SousRubriqueDads(String cle, boolean estObligatoire, boolean aLongueurFixe,boolean accepteValeurNulle,int longueur) {
		super();
		this.cle = cle;
		this.estObligatoire = estObligatoire;
		this.aLongueurFixe = aLongueurFixe;
		this.accepteValeurNulle = accepteValeurNulle;
		this.longueur = longueur;
		derniereRubriqueNumeriqueNegative = false;
		classeAssociee = "org.cocktail.papaye.server.n4ds.PeriodeDadsActive"; // nom par défaut pour éviter de le répéter dans le fichier XML
	}
	// acesseurs
	/** Methode de classe
	 * Retourne true si la derni&egrave;re rubrique calculee etait negative
	 **/
	public static boolean derniereRubriqueNumeriqueNegative() {
		return derniereRubriqueNumeriqueNegative;
	}
	/**
	 * @return Retourne l'entite de la base (EOModel) associe
	 */
	public String classeAssociee() {
		return classeAssociee;
	}
	public void setClasseAssociee(String classeAssociee) {
		this.classeAssociee = classeAssociee;
	}
	/**
	 * @return Retourne la longueur.maximale autorisee pour les donnes de cette sous-rubrique
	 */
	public int longueur() {
		return longueur;
	}
	/**
	 * @return Retourne la mani&egrave;re d'evaluer les donnees
	 */
	public String nature() {
		return nature;
	}
	/**
	 * @param nature Definit la mani&egrave;re d'evaluer les donnees
	 */
	public void setNature(String nature) {
		this.nature = nature;
	}
	/**
	 * @return Retourne le type de la donnee
	 */
	public String typeDonnee() {
		return typeDonnee;
	}
	/**
	 * @param typeDonnee TDefinit le type de la donnee
	 */
	public void setTypeDonnee(String typeDonnee) {
		this.typeDonnee = typeDonnee;
	}
	/**
	 * @return Retourne la valeur de la donne ou le nom de l'attribut de l'entite ou la methode a invoquer.
	 */
	public String valeur() {
		return valeur;
	}
	/**
	 * @param valeur Definit la valeur de la donne
	 */
	public void setValeur(String valeur) {
		this.valeur = valeur;
	}
	/**
	 * @return Retourne true si la donnee peut valoir zero
	 */
	public boolean accepteValeurNulle() {
		return accepteValeurNulle;
	}
	/**
	 * @return Retourne true si la donnee est de longueur fixe
	 */
	public boolean aLongueurFixe() {
		return aLongueurFixe;
	}
	/**
	 * @return Retourne la cle de la sous-rubrique
	 */
	public String cle() {
		return cle;
	}
	/**
	 * @return Retourne true si la sous-rubrique est obligatoire
	 */
	public boolean estObligatoire() {
		return estObligatoire;
	}
	
	/**
	 * Prepare une sous-rubrique en evaluant les donnes et retourne le resultat sous la forme d'une string<BR>
	 * 15/11/06 : modifiee pour supporter un plus grand type de donnees
	 * @param parametres param&egrave;tres fournis
	 * @param classes classe des param&egrave;tres fournis
	 * @return String generee
	 */
	public SousRubriqueAvecValeur preparer(Object[] parametres,Object[] classes) throws Exception {
		// pour les rubriques de signe, vérifier la valeur du signe calculée dans la rubrique précédente
		if (valeur().equals("signe")) {
			if (derniereRubriqueNumeriqueNegative()) {
				SousRubriqueAvecValeur valeur = new SousRubriqueAvecValeur(cle(),ConstantesDads.RESULTAT_NEGATIF);
				derniereRubriqueNumeriqueNegative = false;	// Pour ne pas générer des rubriques négatives plusieurs fois de suite
				return valeur;
			} else {
				return null;
			}
		}
		
		Object resultat = null;
		if (nature().equals("constante")) {
			resultat = new String(valeur());
		} else if (nature().equals("champ")) {
			resultat = trouverValeurReelle(parametres,classes);
		} else { 	// méthode
			resultat = invoquerMethode(parametres,classes);
		}
		if (resultat != null) {
			String stringResultat = null;
			// contrôler la conformité du résultat par rapport au type de donnee attendu et convertir en String
			if (typeDonnee().equals("date") == false && typeDonnee().equals("numerique") == false) {
				derniereRubriqueNumeriqueNegative = false;
				if (resultat.getClass().getName().equals("java.lang.String") == false) {
					throw new Exception("\tValeur Texte attendue pour la sous-rubrique " + cle() + " et la valeur " + valeur());
				} else {
					stringResultat = formaterString((String)resultat,typeDonnee());
				}
				if (typeDonnee().equals("adresse_email")) {
					verifierAdresseEmail(stringResultat);
				}
			}
			if (typeDonnee().equals("date")) {
				derniereRubriqueNumeriqueNegative = false;
				if (resultat.getClass().getName().equals("com.webobjects.foundation.NSTimestamp")) {
					stringResultat = convertirDate((NSTimestamp)resultat);
				} else if (resultat.getClass().getName().equals("java.lang.String") == false) {
					throw new Exception("Valeur Date attendue pour la sous-rubrique " + cle() + " et la valeur " + valeur());
				} else {
					stringResultat = (String)resultat;
				}
			}
			if (typeDonnee().equals("numerique")) {
				if (resultat.getClass().getSuperclass().getName().equals("java.lang.Number") == false) {
				throw new Exception("\tValeur Numerique attendue pour la sous-rubrique " + cle() + " et la valeur " + valeur());
				} else {
					stringResultat = convertirNombre((Number)resultat);
				}
			}
			// vérifier si la valeur attendue est obligatoire
			if ((stringResultat == null || stringResultat.equals("")) && estObligatoire()) {
				throw new Exception("\tPour la sous-rubrique " + cle() + " et la valeur " + valeur() + " le resultat ne peut etre nul.");
			}
			// vérifier maintenant si la longueur est conforme à la longueur attendue
			if (stringResultat != null) {
				if (aLongueurFixe()) {
					if (stringResultat.length() != longueur()) {
						throw new Exception("\tLongueur invalide pour la sous-rubrique " + cle() + " et la valeur " + valeur());
					}
				} else if (stringResultat.length() > longueur()) {
					if (typeDonnee().equals("texte")) {
						// tronquer le texte
						stringResultat = stringResultat.substring(0,longueur());
						stringResultat = stringResultat.trim();		// pour enlever les " " en fin de chaîne après tronquage
					} else {
						throw new Exception("Le resultat evalue pour la sous-rubrique " + cle() + " et la valeur " + valeur() + " depasse la longueur attendue.");
					}
				}
				// tous les contrôles sont OK
				SousRubriqueAvecValeur sousRubrique = new SousRubriqueAvecValeur(cle(),stringResultat);
				String texte = sousRubrique.formatDadsSansDelimiteurs();
				if (texte.length() > LONGUEUR_MAX) {
					throw new Exception(texte + " : chaine trop longue (max 256 car.)");
				} else {
					return sousRubrique;
				}
			} else {
				return null;
			}
		} else if (estObligatoire()) {
			throw new Exception("\tVous devez fournir une valeur pour " + valeur() + " pour la sous-rubrique " + cle());
		} else {
			return null;
		}
	}
	public String toString() {
		String texte = shortString() + ", valeur \"" + valeur() + "\" nature \"" + nature() + "\" longueur " + longueur() + " type \"" + typeDonnee();
		if (classeAssociee() != null) {
			texte = texte + "\" classe \"" + classeAssociee() + "\"";
		} else {
			texte = texte + "\"";
		}
		return texte;
	}
	/**
	 * Retourne une version resum&eaute;e de la sous-rrubrique
	 * @return
	 */
	public String shortString() {
		String texte = "cle \"" + cle;
		if (estObligatoire()) {
			texte = texte + "\", obligatoire";
		} else {
			texte = texte + "\"";
		}
		if (aLongueurFixe()) {
			texte = texte + ", longueur fixe";
		}
		return texte;
	}
	
	// méthodes privées
	private Object trouverValeurReelle(Object[] parametres,Object[] classes) throws Exception {
		Object parametre = trouverParametre(parametres,classes);
		if (parametre != null) {
			try {
				Object valeur = ((NSKeyValueCoding)parametre).valueForKey(valeur());
				// supprimer toutes les valeurs vides ou nulles
				if (valeur != null) {
					valeur = formaterValeur(valeur);
				}
				return valeur;
			} catch (Exception e) {
				throw new Exception("La classe " + classeAssociee() + " ne comporte pas de champ " + valeur());
			}
			
		} else {
			// si non trouvé dans les paramètres, lancer une exception
			throw new Exception("\tPas de parametre fourni pour evaluer la valeur de " + valeur() + " de la classe " + classeAssociee() + " pour la sous-rubrique " + cle());
		}
	}
	private Object invoquerMethode(Object[] parametres,Object[] classes) throws Exception {
		Object parametre = trouverParametre(parametres,classes);
		java.lang.reflect.Method methode = null;
		
		try {
			methode = parametre.getClass().getMethod (valeur());

			Object resultat = methode.invoke(parametre);
			if (resultat != null) {
				resultat = formaterValeur(resultat);
			}
			return resultat;
		} catch (SecurityException e1) {
			throw new Exception ("La methode " + valeur() + " de la classe " + parametre.getClass().getName() + " dans la sous-rubrique " + cle() + " n'est pas publique");
		} catch (NoSuchMethodException e1) {
			throw new Exception ("La methode " + valeur() + " de la classe " + parametre.getClass().getName() + " dans la sous-rubrique " + cle() + " inconnue");
		} catch (IllegalArgumentException e) {
			throw new Exception ("Arguments erronnes pour la methode " + valeur() +  " de la classe " + parametre.getClass().getName() + " dans la sous-rubrique " + cle());
		} catch (IllegalAccessException e) {
			throw new Exception ("La methode " + valeur() + " de la classe " + parametre.getClass().getName() + " dans la sous-rubrique " + cle() + " n'est pas publique");
		} catch (InvocationTargetException e) {
			//e.printStackTrace();
			throw new Exception("METHODE : " + methode + " , PARAMETRE : " + parametre + "\n " + e.getCause().getMessage());
		} 
	}
	private Object trouverParametre(Object[]parametres,Object[]classes) throws Exception {
		// on attend tout d'abord qu'une classe soit définie pour évaluer cette valeur
		if (classeAssociee() == null) {
			throw new Exception("Classe non definie pour la sous-rubrique " + cle() + " et la valeur " + valeur());
		}
		// Rechercher le parametre dans la liste des parametres en fonction des classes fournies en parametre
		for (int i= 0; i < classes.length; i++) {
			String nom = (String)classes[i];	
			if (nom.equals(classeAssociee())) {
				return parametres[i];
			}
		}
		// si non trouvé dans les paramètres, lancer une exception
		throw new Exception("Pas de parametre fourni pour evaluer la valeur de " + valeur() + " de la classe " + classeAssociee() + " pour la sous-rubrique " + cle());
	
	}
	// Met une chaîne en majuscules et supprime tous les espaces en début de nom, les doubles espaces, doubles traits d'union
	private String formaterString(String uneChaine,String typeDonnee) {
		if (uneChaine.equals(" ")) { // cas de certaines constantes
			if (typeDonnee.equals("adresse") || typeDonnee.equals("commune") || typeDonnee.equals("bureau_distributeur") || typeDonnee.equals("adresse_email")) {
				return null;
			} else {
				return uneChaine;
			}
		}

		String resultat = uneChaine;
		if (typeDonnee.equals("nom_famille") == false && typeDonnee.equals("prenom") == false && typeDonnee.equals("adresse") == false) {
			// supprimer tous les caractères accentués
			if (resultat.indexOf("ç") >= 0) {
				resultat = resultat.replaceAll("ç","c");
			}
			if (resultat.indexOf("à") >= 0) {
				resultat = resultat.replaceAll("à","a");
			}
			if (resultat.indexOf("â") >= 0 ) {
				resultat = resultat.replaceAll("â","a");
			}
			if (resultat.indexOf("é") >= 0) {
				resultat = resultat.replaceAll("é","e");
			}
			if (resultat.indexOf("è") >= 0) {
				resultat = resultat.replaceAll("è","e");
			}
			if (resultat.indexOf("ê") >= 0) {
				resultat = resultat.replaceAll("ê","e");
			}
			if (resultat.indexOf("ë") >= 0) {
				resultat = resultat.replaceAll("ë","e");
			}
			if (resultat.indexOf("î") >= 0) {
				resultat = resultat.replaceAll("î","i");
			}
			if (resultat.indexOf("ï") >= 0) {
				resultat = resultat.replaceAll("ï","i");
			}
			if (resultat.indexOf("ô") >= 0) {
				resultat = resultat.replaceAll("ô","o");
			}
			if (resultat.indexOf("û") >= 0) {
				resultat = resultat.replaceAll("û","u");
			}
			if (resultat.indexOf("ù") >= 0) {
				resultat = resultat.replaceAll("ù","u");
			}
			if (resultat.indexOf("û") >= 0) {
				resultat = resultat.replaceAll("û","u");
			}
		}
		
		if ( typeDonnee.equals("nom") || typeDonnee.equals("prenom") || typeDonnee.equals("nom_famille") ) {
			
			if (resultat.indexOf(" -") >= 0) {
				resultat = resultat.replaceAll(" -","-");
			}
			if (resultat.indexOf("- ") >= 0) {
				resultat = resultat.replaceAll("- ","-");
			}
			if (resultat.indexOf(" - ") >= 0) {
				resultat = resultat.replaceAll(" - ","-");
			}
		}
		if (typeDonnee.equals("prenom") && resultat.indexOf(" ") >= 0) {
			resultat = resultat.replaceAll(" ","-");
		}
		// Dans tous les cas, modifications des caractères indésirables
		if (resultat.indexOf("ö") >= 0) {
			resultat = resultat.replaceAll("ö","o");
		}
		if (resultat.indexOf("ô") >= 0) {
			resultat = resultat.replaceAll("ô","o");
		}
		if (resultat.indexOf("/") >= 0) {
			resultat = resultat.replaceAll("/","-");
		}
		if (resultat.indexOf("°") >= 0) {
			resultat = resultat.replaceAll("°"," ");
		}
		if (resultat.indexOf(",") >= 0) {
			resultat = resultat.replaceAll(","," ");
		}
		// supprimer les autres caractères accentués
		if (resultat.indexOf("Ç") >= 0) {
			resultat = resultat.replaceAll("Ç","C");
		}
		if (resultat.indexOf("À") >= 0) {
			resultat = resultat.replaceAll("À","A");
		}
		if (resultat.indexOf("É") >= 0) {
			resultat = resultat.replaceAll("É","E");
		}
		if (resultat.indexOf("È") >= 0) {
			resultat = resultat.replaceAll("È","E");
		}
		if (resultat.indexOf("Ê") >= 0) {
			resultat = resultat.replaceAll("Ê","E");
		}
		if (resultat.indexOf("Ë") >= 0) {
			resultat = resultat.replaceAll("Ë","E");
		}
		if (resultat.indexOf("Â") >= 0) {
			resultat = resultat.replaceAll("Â","A");
		}
		if (resultat.indexOf("Î") >= 0) {
			resultat = resultat.replaceAll("Î","I");
		}
		if (resultat.indexOf("Ï") >= 0) {
			resultat = resultat.replaceAll("Ï","I");
		}
		if (resultat.indexOf("Ô") >= 0) {
			resultat = resultat.replaceAll("Ô","O");
		}
		if (resultat.indexOf("Ö") >= 0) {
			resultat = resultat.replaceAll("Ö","O");
		}
		if (resultat.indexOf("Û") >= 0) {
			resultat = resultat.replaceAll("Û","U");
		}
		if (resultat.indexOf("Ü") >= 0) {
			resultat = resultat.replaceAll("Ü","U");
		}
		if (resultat.indexOf(":") >= 0) {
			resultat = resultat.replaceAll(":"," ");
		}
		if (resultat.indexOf("\"") >= 0) {
			resultat = resultat.replaceAll("\"","");
		}
		if (resultat.indexOf("&") >= 0) {
			resultat = resultat.replaceAll("&","");
		}
		if (resultat.indexOf("+") >= 0) {
			resultat = resultat.replaceAll("+","");
		}
		if (resultat.indexOf("=") >= 0) {
			resultat = resultat.replaceAll("=","");
		}
		if (resultat.indexOf(">>") >= 0) {
			resultat = resultat.replaceAll(">>","");
		}
		if (resultat.indexOf("<<") >= 0) {
			resultat = resultat.replaceAll("<<","");
		}
		if (typeDonnee.equals("commune") || typeDonnee.equals("bureau_distributeur") || typeDonnee.equals("adresse_email")) {
			if (resultat.indexOf("'") >= 0) {
				resultat = resultat.replaceAll("'"," ");
			}
		}
		if (typeDonnee.equals("texte") == false && typeDonnee.equals("adresse") == false && typeDonnee.equals("adresse_email") == false) {
			if (resultat.indexOf(".") >= 0) {
				// 25/06/2010 - caractère escape placé devant le point dans replaceAll
				resultat = resultat.replaceAll("\\."," ");
			}
		}
		if (typeDonnee.equals("adresse")) {
			// Supprimer tous les caractères non-alphabétiques ou non-numériques en fin de chaîne
			int i = resultat.length() - 1;
			  for (; i >= 0; i--) {
				  char c = resultat.charAt(i);
		         if (StringCtrl.isBasicLetter(c) || StringCtrl.isBasicDigit(c)) {
		             break ;
		         }
			  }
			  resultat = resultat.substring(0, i + 1);
		}
		
		if (typeDonnee.equals("adresse")) {
			if (resultat.indexOf("' ") >= 0) {
				resultat = resultat.replaceAll("' ","'");
			}			
		}

		
		if (typeDonnee.equals("texte") == false && typeDonnee.equals("adresse") == false && 
			typeDonnee.equals("commune") == false && typeDonnee.equals("adresse_email") == false) {
			// Supprimer tous les chiffres
			resultat = resultat.replaceAll("0","");
			resultat = resultat.replaceAll("1","");
			resultat = resultat.replaceAll("2","");
			resultat = resultat.replaceAll("3","");
			resultat = resultat.replaceAll("4","");
			resultat = resultat.replaceAll("5","");
			resultat = resultat.replaceAll("6","");
			resultat = resultat.replaceAll("7","");
			resultat = resultat.replaceAll("8","");
			resultat = resultat.replaceAll("9","");
		}
		if (typeDonnee.equals("adresse_email") == false) {
			if (resultat.indexOf("_") >= 0) {
				resultat = resultat.replaceAll("_","-");
			}
			if (resultat.indexOf("@") >= 0) {
				resultat = resultat.replaceAll("@","");
			}
		} else if (resultat.indexOf("__") >= 0) {
			resultat = resultat.replaceAll("__","_");
		}
		if (typeDonnee.equals("nom_famille") == false) {
			// le double-tiret est accepté dans les noms de famille pour les noms composés
			if (resultat.indexOf("--") >= 0) {
				resultat = resultat.replaceAll("--","-");
			}
		}
		if (typeDonnee.equals("commune")|| typeDonnee.equals("bureau_distributeur") || 
			typeDonnee.equals("adresse") || typeDonnee.equals("pays")) {
			if (resultat.indexOf("-") >= 0) {
				resultat = resultat.replaceAll("-"," ");
			} 
		}
		if (typeDonnee.equals("pays") == false) {
			resultat = resultat.replaceAll("\\(","");
			resultat = resultat.replaceAll("\\)","");
			resultat = resultat.replaceAll("\\[","");
			resultat = resultat.replaceAll("\\]","");
		}
		if (typeDonnee.equals("pays")|| typeDonnee.equals("bureau_distributeur")) {
			resultat = resultat.toUpperCase();
		}
		resultat = resultat.trim(); // suppression des espaces,… en début et fin de chaîne
		while (resultat.indexOf("  ") >= 0) {
			resultat = resultat.replaceAll("  "," ");
		}
	
		return resultat;
	}
	private String verifierAdresseEmail(String uneString) throws Exception {
		// L’adresse e-mail ne doit pas contenir : des caracte?res espaces, des caracte?res accentue?s, 
		// majuscules ou minuscules, plusieurs caracte?res @, la chaine de 
		// caracte?res suivant @ doit contenir au moins un point (.) 
		if (uneString.indexOf(" ") >= 0) {
			throw new Exception("Une adresse email ne peut pas contenir des espaces");
		}
		int positionArobase = uneString.indexOf("@");
		if (positionArobase < 0) {
			throw new Exception("Le format d'une adresse email est : nom@serveur.domaine");
		} else {
			String temp = uneString.substring(positionArobase + 1);
			if (temp.indexOf("@") >= 0) {	// présence d'un autre arobase
				throw new Exception("Le format d'une adresse email est : nom@serveur.domaine");
			} 
			if (temp.indexOf(".") < 0) {	// présence d'un autre arobase
				throw new Exception("Le format d'une adresse email est : nom@serveur.domaine");
			} 
		}
		return uneString;

	}
	private String convertirDate(NSTimestamp timestamp) {
		// vérifier si il y a des dates sur deux caractères ou sur 4 pour l'année
		GregorianCalendar date = new GregorianCalendar();
		date.setTime(timestamp);
		String mois = new Integer(date.get(Calendar.MONTH) + 1).toString();	// les mois commencent à zéro en java
		String jour = new Integer(date.get(Calendar.DAY_OF_MONTH)).toString();
		String annee = new Integer(date.get(Calendar.YEAR)).toString();
		if (mois.length() == 1) {
			mois = "0" + mois;
		}
		if (jour.length() == 1) {
			jour = "0" + jour;
		}
		if (longueur() == 4) {
		return jour + mois;
		} else {
			return jour + mois + annee;
		}
	}
	private String convertirNombre(Number nombre) {
		double valeur = nombre.doubleValue();
		/*	Vérifier si la valeur 0 est admise pour les résultats numériques
		 * 	if (valeur == 0) {
		 * derniereRubriqueNumeriqueNegative = false;
			return null;
		}  */
		BigDecimal bigNombre = new BigDecimal(valeur).setScale(0,BigDecimal. ROUND_HALF_UP);
		if (valeur < 0) {
			derniereRubriqueNumeriqueNegative = true;
			bigNombre = bigNombre.abs();	// on ne retourne que des valeurs absolues
		} else {
			derniereRubriqueNumeriqueNegative = false;
		}
		
		String resultat = bigNombre.toString();
		// supprimer tous les zéros en début de nombre, sauf si le résultat est zéro
		boolean done = false;
		while (!done) {
			if (resultat.equals("0") == true || resultat.startsWith("0") == false) {
				done = true;
			} else {
				resultat = resultat.substring(1);
			}
		}
		 if (resultat.equals("")) {
		 	return null;
		 } else {
		 	return resultat;
		 }
	}
	// renvoie null si la valeur est une chaîne vide ou un nombre = 0, sinon renvoie la valeur
	private Object formaterValeur(Object valeur) {
		Object resultat = valeur;
		String nomClasse = valeur.getClass().getName();
		if (nomClasse.equals("java.lang.String")) {
			if (accepteValeurNulle == false && resultat.equals("")) {
				resultat = null;
			}
		} else if (nomClasse.equals("java.lang.Integer") || nomClasse.equals("java.lang.Double"))  {
			double montant = ((Number)resultat).doubleValue();
			if (accepteValeurNulle == false && montant == 0.0) {
				resultat = null;
			}
		} else if (nomClasse.equals("java.math.BigDecimal")) {
			double montant = ((BigDecimal)resultat).doubleValue();
			if (accepteValeurNulle == false && montant == 0.0) {
				resultat = null;
			}
		}
		return resultat;
	}
}
