/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.n4ds;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author christine
 *
 * AnalyseurXML
 * Classe dont les methodes sont invoquees comme callbacks lors du parsing du fichier XML contenant les specifications
 * de la DADS
 */
public class AnalyseurXML extends DefaultHandler {
	private RubriqueDads rubriqueCourante;
	private SousRubriqueDads sousRubriqueCourante;
	private String nom;
	private String cle;
	private String attributLu;
	private String attributEnCours;
	private int longueur;
	private boolean estObligatoire;
	private boolean accepteValeurNulle;
	private boolean aLongueurFixe;
	private boolean estSauvegardee;
	private boolean litElement;
	private ListeRubriquesDads listeRubriques;
	
	public AnalyseurXML(ListeRubriquesDads listeRubriques) {
		super();
		this.listeRubriques = listeRubriques;
		litElement = false;
	}
	
	public void startElement(String namespaceURI, String localName, String qName,Attributes atts) {
		attributLu = null;
		litElement = true;
		attributEnCours = "";
		if (localName.equals("Rubrique")) {
			sousRubriqueCourante = null;
			nom = atts.getValue("nom");
			cle = atts.getValue("cle");
			estSauvegardee =  (atts.getValue("sauvegarde") != null && atts.getValue("sauvegarde").equals("oui"));
			estObligatoire = (atts.getValue("obligatoire") != null && atts.getValue("obligatoire").equals("oui"));
			rubriqueCourante = new RubriqueDads(nom,cle,estObligatoire,estSauvegardee);
			listeRubriques.ajouterRubrique(nom,rubriqueCourante);
		} else if (localName.equals("SousRubrique")) {
			cle = atts.getValue("cle");
			estSauvegardee = false;
			estObligatoire = (atts.getValue("obligatoire") != null && atts.getValue("obligatoire").equals("oui"));
			accepteValeurNulle = (atts.getValue("accepte_zero") != null && atts.getValue("accepte_zero").equals("oui"));
			aLongueurFixe = (atts.getValue("fixe") != null && atts.getValue("fixe").equals("oui"));
			longueur = new Integer(atts.getValue("longueur")).intValue();
			sousRubriqueCourante = new SousRubriqueDads(cle, estObligatoire, aLongueurFixe,accepteValeurNulle,longueur);
			rubriqueCourante.ajouterSousRubrique(sousRubriqueCourante);
		} else if (localName.equals("Description") == false){
			attributLu = localName;
		}
				
	}
	
	public void characters(char[] ch,int start,int length) throws SAXException {
		if (litElement && attributLu != null) {		
 			attributEnCours = attributEnCours.concat(new String(ch,start,length));
 		}
	}
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		litElement = false;
		if (attributLu.equals("valeur")) {
			sousRubriqueCourante.setValeur(attributEnCours);
		} else if (attributLu.equals("type")) {
			sousRubriqueCourante.setTypeDonnee(attributEnCours);
		} else if (attributLu.equals("nature")) {
			sousRubriqueCourante.setNature(attributEnCours);
		} else if (attributLu.equals("classe")) {
			sousRubriqueCourante.setClasseAssociee(attributEnCours);
		}
	}
}
