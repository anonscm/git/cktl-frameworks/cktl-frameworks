/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.n4ds;

/**
 * @author christine
 *
 */
public class ConstantesDads {
	/** Nature de la Dads : compl&grave;te */
	public static final String DADS_COMPLETE = "01";
	/** Nature de la Dads : TDS */
	public static final String DADS_TDS = "02";
	/** type de Dads : normale */
	public static final String DADS_NORMALE = "51";
	/** motif d'un contrat demarre anterieurement a l'anne en cours */
	public static final String DEBUT_CONTRAT_AVANT_ANNEE = "097";
	/** motif d'un contrat durant au-delà de l'anne en cours */
	public static final String FIN_CONTRAT_APRES_ANNEE = "098";
	/** motif debut d'un contrat avec embauche */ 
	public static final String MOTIF_EMBAUCHE = "001";
	/** motif fin d'un contrat   */ 
	public static final String MOTIF_FIN_CONTRAT = "008";
	// codes emplois particulier
	/** code emploi droit d'auteur */
	public static final String CODE_DROIT_AUTEUR = "8888";
	/** code emploi de chomeur */
	public static final String CODE_CHOMEUR = "9999";
	/** code emploi artiste */
	public static final String CODE_EMPLOI_ARTISTE = "35";
	
	// codes employeur multiple
	public static final String EMPLOYEUR_UNIQUE = "01";
	public static final String EMPLOYEUR_MULTIPLE = "02";
	public static String EMPLOYEUR_INCONNU = "03";
	// codes décalage de paye
	public static final String PAYE_SANS_DECALAGE = "01";
	// codes contrat de travail
	public static final String TYPE_CDI = "01";
	public static final String TYPE_CDD = "02";
	public static final String TYPE_APPRENTI = "05";
	public static final String TYPE_CEJ = "06";
	public static final String TYPE_CES = "07";
	public static final String TYPE_CAE = "25";
	public static final String TYPE_CONTRAT_AVENIR = "26";
	public static final String SANS_CONTRAT = "90";
	// codes type contrat travail
	public static final String TEMPS_PLEIN = "01";
	public static final String TEMPS_PARTIEL = "02";
	// codes statut professionnel
	/** statut professionnel du medecin */
	public static final String[] HOSPITALIERS = {"52","53","54","56","57","58","59"};
	// codes caisses de cotisation artiste retraite
	public static final String CARCICAS = "C022";
	public static final String CAPRICAS = "A190";
	// codes catégories socio-professionnelles pour artiste
	/** code categorie socio-professionnel artiste cadre */
	public static final String CATEGORIE_ARTISTE_CADRE = "01";
	/** code categorie socio-professionnel artiste non cadre */
	public static final String CATEGORIE_ARTISTE_NON_CADRE = "04";
	// codes bases spécifiques
	/** code base specifique Ircantec */
	public static final String BASE_SPEFICIFIQUE_IRCANTEC = "02";
	/** code base specifique Ircantec Medecin */
	public static final String BASE_SPEFICIFIQUE_IRCANTEC_MEDECIN = "22";
	/** minimum d'heures de travail mensuel */
	public static final double DUREE_MENSUELLE_MINIMUM = 60;
	/** minimum d'heures mensuel au smic horaire */
	public static final double NB_HEURES_SMIC_MENSUEL = 60;
	/** minimum d'heures de travail trimestriel */
	public static final double DUREE_TRIMESTRIEL_MINIMUM = 120;
	/** minimum d'heures trimestriel au smic horaire */
	public static final double NB_HEURES_SMIC_TRIMESTRIEL = 120;
	/** nombre d'heures minimum de travail annuel */
	public static final double DUREE_ANNUELLE_MINIMUM = 1200;
	/** minimum d'heures au smic horaire annuel */
	public static final double NB_HEURES_SMIC_ANNUEL = 2030;
	/** Duree minimum effectuee */
	public static final String CODE_DUREE_MINIMUM_ANNUELLE_OK = "01";
	/** Duree minimum  d&eacuteja evaluee */
	public static final String CODE_DUREE_MINIMUM_VALIDEE = "98";
	/** Duree minimum  non effectuee */
	public static final String CODE_DUREE_MINIMUM_ANNUELLE_NON = "99";

	public static final String CODE_DUREE_MINIMUM_MENSUELLE_NON = "99";

	/** Pas de R&egime Obligatoire */
	public static final String SANS_COTISATION = "901";
	/** Cotisations calculees sur la base brute reelle */
	public static final String BASE_COTISATION_RELLE = "01";
	/** resultat numerique negatif */
	public static final String RESULTAT_NEGATIF = "N";
	// avantages en nature
	/** Code Avantage en Nature pour le remboursement des frais professionnels */
	public static final String REMBOURSEMENT_FRAIS_PROF = "R";
	/** Code Avantage en Nature pour Autres Avantages en Nature */
	public static final String REMBOURSEMENT_AUTRES = "D";
	//	 emplois multiples
	/** Code pour emplois non-multiples pour S41 */
	public static final String PAS_EMPLOIS_MULTIPLES = "01";
	/** Code pour emplois multiples pour S41 */
	public static final String EMPLOIS_MULTIPLES = "02";
	// emplois multiples pour Ircantec/CNRACL
	/** Code pour emplois multiples pour Ircantec/CNRACL */
	public static final String EMPLOIS_MULTIPLES_COMPLEMENTAIRE = "01";
	/** Code pour emplois non-multiples pour Ircantec/CNRACL */
	public static final String PAS_EMPLOIS_MULTIPLES_COMPLEMENTAIRE = "02";
	// code unité de travail pour artiste
	/** unite de travail pour artiste = horaire */
	public static final String DUREE_HORAIRE_ARTISTE = "01";
	/** unite de travail pour artiste = horaire */
	public static final String DUREE_JOUR_ARTISTE = "03";
	/** unite de travail pour artiste = horaire */
	public static final String DUREE_MOIS_ARTISTE = "07";
	/** Pas d'exoneration URSSAF */
	public static final String SANS_EXO_URSSAF = "90";
	/** Code cotisation CNRACL pour le regime de base */
	public static final String REGIME_BASE_CNRACL = "120";
	/** Code cotisation pour le regime general */
	public static final String REGIME_GENERAL = "200";
	/** code DADS avantage en Nature - Nourriture */
	public static final String DADS_AVANTAGE_NOURRITURE = "N";
	/** code DADS avantage en Nature - Logement */
	public static final String DADS_AVANTAGE_LOGEMENT = "L";
	/** code DADS avantage en Nature - Autres */
	public static final String DADS_AVANTAGE_AUTRE = "A";
	/** code DADS avantage en Nature - NTIC */
	public static final String DADS_AVANTAGE_NTIC = "T";
	/** code DADS avantage en Nature - Véhicule */
	public static final String DADS_AVANTAGE_VEHICULE = "V";
	// etat du contrat de travail en fin d'année
	/** contrat actif le dernier vendredi de l'annee */
	public static final String CONTRAT_ACTIF = "01";
	/** contrat termine le dernier vendredi de l'annee */
	public static final String CONTRAT_TERMINE = "02";
	// droit du contrat de travail
	/** contrat de droit prive*/
	public static final String DROIT_PRIVE = "01";
	/** contrat ne relevant pas du droit prive */
	public static final String DROIT_AUTRE = "02";
	// prud'homes
	public static final String COLLEGE_PRUDHOME_SALARIE = "01";
	public static final String SECTION_PRUDHOME_ACTIVITES_DIVERSES = "04";
	// Situations Particulières
	/** motif de situation particuli&egrave;re quand cotisation Ircantec */
	public static final String SITUATION_PARTICULIERE_IRCANTEC = "01";
	/** motif de situation particuli&egrave;re quand cotisation CNRACL */
	public static final String SITUATION_PARTICULIERE_CNRACL = "02";
	/** Type de situation particuli&egrave;re : conge de presence parentale */
	public static final String TYPE_SITUATION_PARTICULIERE_PRESENCE_PARENTALE = "32";
	/** Type de situation particuli&egrave;re : conge parental */
	public static final String TYPE_SITUATION_PARTICULIERE_CONGE_PARENTAL = "33";
	/** code de cessation progressive d'activite pour CNRACL a temps partiel */
	public static final String CODE_CPA_TEMPS_PARTIEL_CNRACL = "01";
	public static final String TYPE_INDEMNITE_LICENCIEMENT = "01";
	/** Etablissement sans salarie */
	public static final String ETABLISSEMENT_SANS_SALARIE = "01";
	/** Type Heures supplementaires exonerees */
	public static final String TYPE_HEURES_SUPP_EXO = "01";
	/** Type Heures complementaires exonerees */
	public static final String TYPE_HEURES_COMP_EXO = "02";
	/** Nature de cotisation NBI pour la rubrique : S43.G01.01 */
	public static final String NATURE_COTISATION_NBI = "20";
	// Catégories de rubrique
	public static final String CATEGORIE_PENSION_CIVILE = "Pension civile";
	public static final String CATEGORIE_TRANSPORT = "Transport";
	public static final String CATEGORIE_VEUVAGE ="URSSAF - Veuvage";
	public static final String CATEGORIE_VIEILLESSE_TOTALE = "URSSAF - Vieillesse Totale";
	public static final String CATEGORIE_ALLOCATIONS_FAMILIALES = "URSSAF - Allocations Familiales";
	public static final String CATEGORIE_MALADIE = "URSSAF - Maladie";
	public static final String CATEGORIE_SOLIDARITE_VIEILLESSE = "URSSAF - Solidarité Vieillesse";
	public static final String CATEGORIE_VIEILLESSE_PLAFONNEE = "URSSAF - Vieillesse Plafonnée";
	public static final String CATEGORIE_FNAL = "FNAL";
	public static final String CATEGORIE_AT = "Accident du travail";
	public static final String CATEGORIE_CSG_CRDS = "CSG CRDS";
	// Catégories de statut
	public static final String CATEGORIE_STAGIAIRE_SANS_CHARGES = "Stagiaire sans Charges Sociales";
	
	// Codes divers (EOPayeCodeDADS)
	/** code dads pour identifier les valeurs possibles de situation salariee */
	public static final String CODE_SITUATION_SALARIEE = "CODSISAL";
	// codes divers (EOPayeCode)
	/** code EOPayeCode Plafond Sécurité Sociale Mensuelle */
	public static final String CODE_PLAFOND_SS_MENSUEL = "PLAFMSSS";
	// codes Avantages en Nature
	/** code EOPayeCode avantage en Nature - Nourriture */
	public static final String AVANTAGE_NOURRITURE = "REMUNNOU";
	/** code EOPayeCode avantage en Nature - Logement */
	public static final String AVANTAGE_LOGEMENT = "REMUNLOG";
	/** code EOPayeCode avantage en Nature - Autres */
	public static final String AVANTAGE_AUTRE = "REMUNAVN";
	/** code EOPayeCode avantage en Nature - NTIC */
	public static final String AVANTAGE_NTIC = "REMUNNTC";
	/** code EOPayeCode avantage en Nature - Véhicule */
	public static final String AVANTAGE_VEHICULE = "REMUNAVV";
	/** code Vieillesse Plafonnee Salariale */
	public static final String CODE_VIEILLESSE_PLAFONNEE_SALARIALE = "COTVIPLS";
	/** code Vieillesse Plafonnee Patronale */
	public static final String CODE_VIEILLESSE_PLAFONNEE_PATRONALE = "COTVIPLF";
	/** code CSG non deductible */
	public static final String CODE_CSG_NON_DEDUCTIBLE = "COTCSGND";
	/** code CSG deductible */
	public static final String CODE_CSG_DEDUCTIBLE = "COTCSGDE";
	/** code CRDS */
	public static final String CODE_CRDS = "COTICRDS";
	/** code MRC */
	public static final String CODE_MRC = "COTMRCHO";
	/** code Contribution Solidariteacute; */
	public static final String CODE_SOLIDARITE = "COTCOSOL";
	/** code Taux Contribution Solidarite */
	public static final String CODE_TAUX_SOLIDARITE = "TXCGCSOL";
	/** code Cotisation RAFP Patronale */
	public static final String CODE_RAFP_PATRONALE = "COTRAFPP";
	/** code Cotisation RAFP Salariale */
	public static final String CODE_RAFP_SALARIALE = "COTRAFPS";
	/** code Taux RAFP Patronale */
	public static final String CODE_TAUX_RAFP_PATRONALE = "TXRAFPFP";
	/** code Taux RAFP Salariale */
	public static final String CODE_TAUX_RAFP_SALARIALE = "TXRAFPFS";
	/** code Ircantec Tranche A Salariale */
	public static final String CODE_IRCANTEC_TRANCHEA_SALARIALE = "COTIRTAS";
	/** code Ircantec Tranche B Salariale */
	//public static final String CODE_IRCANTEC_TRANCHEB_SALARIALE = "COTIRTBS";
	/** code Ircantec Tranche A Salariale */
	public static final String CODE_IRCANTEC_TRANCHEA_PATRONALE = "COTIRTAP";
	/** code Ircantec Tranche B Patronale */
	//public static final String CODE_IRCANTEC_TRANCHEB_PATRONALE = "COTIRTBP";
	/** code Prime Logement TOM */
	public static final String CODE_PRIME_LOGEMENT_TOM = "REMUNLOT";
	/** code Retenue a la Source */
	public static final String CODE_RETENUE_SOURCE = "REMUNRSO";
	// Pension civile
	public static final String CODE_TAUX_PENSION_CIVILE_PATRONALE = "TXPECIVP";
	public static final String CODE_TAUX_PENSION_CIVILE_SALARIALE = "TXPECIVS";
	public static final String CODE_PENSION_CIVILE_PATRONALE = "COTPECIP";
	public static final String CODE_PENSION_CIVILE_SALARIALE = "COTPECIS";
	// ATI
	public static final String CODE_TAUX_ATI = "TXALLTIP";
	public static final String CODE_ATI = "COTALINP";

	// codes des  rémunérations à ne pas prendre en compte pour le calcul des primes de la RAFP
	public static final String REMUN_NBI = "REMUNNBI";
	public final static String REMUN_BRUT = "TRMTBASE";
	public static final String REMUNERATION = "REMUNBRU";
	public static final String REMUN_GMR = "REMUNGMR";
	public static final String INDEMNITE_SUJETION = "REMUNISU";
	/** code Taux Taxe sur Salaire Tranche 1 */
	public static final String CODE_TAUX_TRANCHE1_TAXE = "TAXSSAL1";
	/** code Taxe sur Salaire Tranche 1 */
	public static final String CODE_TAXE_SALAIRE1 = "COTTXSAL";
	/** code Taux Taxe sur Salaire Tranche 2 */
	public static final String CODE_TAUX_TRANCHE2_TAXE = "TAXSSAL2";
	/** code Taxe sur Salaire Tranche 2 */
	public static final String CODE_TAXE_SALAIRE2 = "COTTXSA2";
	/** code Taux Taxe sur Salaire Tranche 3 */
	public static final String CODE_TAUX_TRANCHE3_TAXE = "TAXSSAL3";
	/** code Taxe sur Salaire Tranche 3 */
	public static final String CODE_TAXE_SALAIRE3 = "COTTXSA3";
	/** code Remuneration Conges Payes */
	public static final String CODE_CONGES_PAYES = "REMUNICP";
	/** code Remuneration Conges Payes Meacute;decin */
	public static final String CODE_CONGES_PAYES_MEDECIN = "REMUNCPM";
	/** code Cotisation Accident du Travail */
	public static final String CODE_ACCIDENT_TRAVAIL = "COTACTRA";
	/** code Taux MGEN */
	public static final String CODE_TAUX_MGEN = "TXCOMGEN";
	/** code MGEN */
	public static final String CODE_MGEN = "COTIMGEN";
	/** code Taux Smic */
	public static final String CODE_TAUX_HORAIRE_SMIC = "TXBRSMIC";
	/** code Nombre Heures Travail Hebdomadaire */
	public static final String CODE_NB_HEURES_HEBDO = "NBHEUHBD";
	/** code Nombre Heures Travail Mensuel */
	public static final String CODE_NB_HEURES_MENSUEL = "NBHEULE1";
	/** code Remuneration Heures Supplementaires exonerees */
	public static final String CODE_REMUN_HEURES_SUP_EXO = "REMUNHSE";
	/** code Remuneration Heures Complementaires exonerees */
	public static final String CODE_REMUN_HEURES_COMP_EXO = "REMUNHCE";
	/** code Deduction Heures Supplementaires exonerees pour contractuel */
	public final static String CODE_DEDUCTION_HEURES_SUP_CONTRACTUEL = "TXREDHSU";
	/** code Deduction Heures Supplementaires exonerees pour fonctionnaire */
	public final static String CODE_DEDUCTION_HEURES_SUP_FONCTIONNAIRE = "TXREDHSF";
	/** Indemnite de licenciement */
    public final static String INDEMNITE_LICENCIEMENT = "REMUNILI";
    /** NBPoints NBI */
    public final static String NB_POINTS_NBI = "NBPTSNBI";
    /** Nombre maximum de points NBI */
    public final static int NB_POINTS_NBI_MAX=800;
}
