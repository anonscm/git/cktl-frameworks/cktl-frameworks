/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.n4ds;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.common.DateCtrl;
import org.cocktail.papaye.server.metier.grhum.EOIndice;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCategorieRubrique;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParamPerso;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut;
import org.cocktail.papaye.server.metier.jefy_paye.EORegimeCotisation;
import org.cocktail.papaye.server.metier.jefy_paye.dads.EODadsAgents;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
/**
 * @author christine
 *
 * PeriodeDadsActive : stocke les informations utiles pour creer les informations DADS d'un agent
 */
public class PeriodeDadsActive implements NSKeyValueCoding {

	private NSTimestamp dateDebut;
	private NSTimestamp dateFin;
	private String motifDebutPeriode;
	private String motifFinPeriode;
	private String codeSituationSalariee;	// V08R09 pour gerer CFA, CPA,...
	private EOPayeContrat contrat;	// pour garder une reference sur le contrat le plus important lorsqu'on regroupe plusieurs contrats
	private IndividuDads agent; // garde une référence sur l'agent DADS pour avoir acces a l'ensemble des periodes
	private NSMutableArray historiquesPaye;
	private int anneeDADS = 2011;
	private String codeBaseBruteSpecifique;
	private boolean aEmploisMultiples;	// 07/12/05
	private boolean estConflictuelle;	// 08/02/06
	private boolean conditionHoraireRemplie;	// 21/11/08

	private boolean conditionPremierPeriodeTrimestrielleRemplie;	// 18/10/2011

	private NSMutableDictionary horairesAgent;
	private EOEditingContext ec;

	private static final int REMUNERATION = 1;
	private static final int CHARGE_SALARIALE = 2;
	private static final int CHARGE_PATRONALE = 3;

	// Modifications apportees pour la V08R09 - le motif debut stocke ou le motif debut ou le code situation salarie
	public PeriodeDadsActive(IndividuDads agent,EOPayeContrat contrat,int anneeDADS) {

		this.anneeDADS = anneeDADS;
		ec = contrat.editingContext();

		aEmploisMultiples = false;
		estConflictuelle = false;
		conditionHoraireRemplie = false;
		codeSituationSalariee = null;
		preparerPeriode(contrat,true);
		if (contratAnterieurAnnee(contrat,anneeDADS) && contrat.statut().pstaCipdz().equals(EOPayeStatut.CATEGORIE_TEMPS_PLEIN)) {
			motifDebutPeriode = ConstantesDads.DEBUT_CONTRAT_AVANT_ANNEE;
			if (contrat.motifDebut() != null && contrat.motifDebut().code().pcodCode().equals(ConstantesDads.CODE_SITUATION_SALARIEE)) {
				codeSituationSalariee = contrat.motifDebut().pparValeur();
			}
		} else {
			if (contrat.motifDebut() == null) {
				if (contrat.statut().pstaCipdz().equals(EOPayeStatut.CATEGORIE_OCCASIONNEL) || contrat.statut().pstaCipdz().equals(EOPayeStatut.CATEGORIE_VACATAIRE) ||
						contrat.statut().pstaCipdz().equals(EOPayeStatut.CATEGORIE_TEMPS_PLEIN)){
					motifDebutPeriode = ConstantesDads.MOTIF_EMBAUCHE;
				}
				if (motifDebutPeriode == null) {
					EnvironnementDads.sharedInstance().ajouterErreur("Vous devez définir le motif de début dans un des contrats de " + contrat.individu().identite());
				}
			} else {
				if (contrat.motifDebut().code().pcodCode().equals(ConstantesDads.CODE_SITUATION_SALARIEE)) {
					codeSituationSalariee = contrat.motifDebut().pparValeur();
					motifDebutPeriode = ConstantesDads.MOTIF_EMBAUCHE;
				} else {
					motifDebutPeriode = contrat.motifDebut().pparValeur();
				}
			}
		}
		if (contratPosterieurAnnee(contrat,anneeDADS)) {
			motifFinPeriode = ConstantesDads.FIN_CONTRAT_APRES_ANNEE;
		} else {
			if (contrat.motifFin() == null) {
				if (contrat.statut().pstaCipdz().equals(EOPayeStatut.CATEGORIE_OCCASIONNEL) || contrat.statut().pstaCipdz().equals(EOPayeStatut.CATEGORIE_VACATAIRE) ||
						contrat.statut().pstaCipdz().equals(EOPayeStatut.CATEGORIE_TEMPS_PLEIN)) {
					motifFinPeriode = ConstantesDads.MOTIF_FIN_CONTRAT;
				}
				if (motifFinPeriode == null) {
					EnvironnementDads.sharedInstance().ajouterErreur("Vous devez définir le motif de fin dans un des contrats de " + contrat.individu().identite());
				}
			} else {
				motifFinPeriode = contrat.motifFin().pparValeur();
			}
		}
		historiquesPaye = new NSMutableArray();
		this.agent = agent;
	}
	// interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// accesseurs
	/**
	 * @return Retourne la liste des historiques d'un agent pour la periode
	 */
	public NSMutableArray historiquesPaye() {
		return historiquesPaye;
	}
	/**
	 * @return Retourne la date de debut de periode
	 */
	public NSTimestamp dateDebut() {
		return dateDebut;
	}
	/**
	 * Change la date de debut de periode
	 */
	public void setDateDebut(NSTimestamp debut) {
		dateDebut = debut;
	}
	/**
	 * @return Retourne la date de fin de periode
	 */
	public NSTimestamp dateFin() {
		return dateFin;
	}
	/**
	 * Change la date de fin de periode
	 */
	public void setDateFin(NSTimestamp fin) {
		dateFin = fin;
	}
	/**
	 * @return Returns the codeBaseBruteSpecifique.
	 */
	public String codeBaseBruteSpecifique() {
		return codeBaseBruteSpecifique;
	}
	/**
	 * @param codeBaseBruteSpecifique The codeBaseBruteSpecifique to set.
	 */
	public void setCodeBaseBruteSpecifique(String codeBaseBruteSpecifique) {
		this.codeBaseBruteSpecifique = codeBaseBruteSpecifique;
	}
	/**
	 * @return Retourne le debut de la periode sous la forme JJMM
	 */
	public String debutPeriode() {
		return formaterDate(dateDebut(),true);
	}
	/**
	 * @return Retourne la fin de la periode sous la forme JJMM
	 */
	public String finPeriode() {
		return formaterDate(dateFin(),false);
	}
	/** @return Retourne la date de fin a utiliser pour classer les periodes */
	public NSTimestamp dateFinPourTri() {
		if (dateFin() == null) {
			return DateCtrl.stringToDate("31/12/" + anneeDADS);
		} else {
			return dateFin();
		}
	}
	/** retourne true si la periode se termine pendant l'annee de la DADS ou de l'impression */
	public boolean finitDansAnneeCourante() {
		if (dateFin() != null) {
			GregorianCalendar date = new GregorianCalendar();
			date.setTime(dateFin);
			return (date.get(Calendar.YEAR) <= anneeDADS);
		} else {
			return false;
		}	
	}
	/** retourne true si la periode peut demarrer pendant l'annee de la DADS ou de l'impression */
	public boolean peutDemarrerAnneeCourante() {
		return motifDebutPeriode() != null && motifDebutPeriode().equals(ConstantesDads.DEBUT_CONTRAT_AVANT_ANNEE) == false;
	}
	/** retourne true si la periode a cours pendant le trimestre passe en param&egrave;tre
	 * @param i numero de trimestre
	 */
	public boolean estValableAuTrimestre(int i) {
		int moisDebut = new Integer(debutPeriode().substring(2)).intValue();
		int moisFin = new Integer(finPeriode().substring(2)).intValue();

		return ((moisDebut >= (3 * (i - 1)) + 1 && moisDebut <= 3 * i) || (moisDebut <= (3 * i)  && moisFin >= (3 * (i - 1)) + 1));
	}
	/** @return Retourne le motif de debut de contrat */
	public String motifDebutPeriode() {
		return motifDebutPeriode;
	}
	/** @return Retourne le motif de fin de contrat */
	public String motifFinPeriode() {
		return motifFinPeriode;
	}
	/** @return Retourne le code de situation salariee */
	public String codeSituationSalariee() {
		return codeSituationSalariee;
	}
	/** modifie les informations de fin de periode 
	 * @param contrat contrat fournissant les informations correctes 	
	 */
	public void modifierFinPeriode(EOPayeContrat contrat) {
		dateFin = contrat.dFinContratTrav();
		if (contrat.motifFin() != null) {
			motifFinPeriode = contrat.motifFin().pparValeur();
		}
	}
	// 07/12/05
	public void signalerEmploisMultiples() {
		aEmploisMultiples = true;
	}

	public String codeModaliteExerciceTravail() {

		String code = "10";

		if (aEmploisMultiples)
			code =  "21";

		return code;
	}

	//	08/02/06
	public boolean estConflictuelle() {
		return estConflictuelle;
	}
	public void signalerConflit() {
		estConflictuelle = true;
	}
	/**
	 * ajoute un historique a la liste des historiques
	 */
	public void ajouterHistorique(EOPayeHisto historique) {
		historiquesPaye.addObject(historique);
	}
	public String toString() {
		return ("statut : " + contrat.statut().pstaLibelle() + ", debut periode : " + DateCtrl.dateToString(dateDebut()) + ", fin periode : " + DateCtrl.dateToString(dateFin())  + ", structure : " + contrat.structure().llStructure());
	}
	/** retourne le contrat de la periode */
	public EOPayeContrat contrat() {
		return contrat;
	}
	/** modifie le contrat de la periode */
	public void setContrat(EOPayeContrat contrat) {
		this.contrat = contrat;
	}
	/** prepare la periode pour un contrat 
	 * @param forcerValeurs true si on veut que les informations (contrat et dates) de la periode soient modifiees */
	//	 31/01/07 - pour avoir des dates de contrat correct lors des regroupements de contrat
	public void preparerPeriode(EOPayeContrat contrat,boolean forcerValeurs) {
		if (forcerValeurs) {
			this.contrat = contrat;
		}
		//	System.out.println("contrat date debut " + DateCtrl.dateToString(contrat.dDebContratTrav()) + ", date fin " + DateCtrl.dateToString(contrat.dFinContratTrav()));

		if (dateDebut == null || DateCtrl.isBefore(contrat.dDebContratTrav(), dateDebut) || forcerValeurs) {
			dateDebut = contrat.dDebContratTrav();
		}
		if (forcerValeurs || (dateFin != null && (contrat.dFinContratTrav() == null || 
				(contrat.dFinContratTrav() != null && DateCtrl.isBefore(dateFin, contrat.dFinContratTrav()))))) {
			dateFin = contrat.dFinContratTrav();
		}
		//	System.out.println("date debut " + DateCtrl.dateToString(dateDebut) + ", date fin " + DateCtrl.dateToString(dateFin));
	}
	/** prepare la periode pour un contrat
	 * Les dates de la periode ne sont mises a jour que si la date debut est 
	 * posterieure a celle du contrat et la date de fin anterieure */
	public void preparerPeriode(EOPayeContrat contrat) {
		preparerPeriode(contrat,false);
	}

	public String codePositionStatutaire() {

		if (contrat().statut().regimeCotisation().estSRE())
			return "104";

		// Uniquement pour les titulaires
		if (contrat().statut().temTitulaire().equals("O"))
			return "101";

		return null;
	}

	public String codePositionActiviteSRE() {

		if (contrat().statut().regimeCotisation().estSRE() && codePositionStatutaire() != null 
				&& codePositionStatutaire().equals("101"))
			return "10101";

		return null;
	}

	/** retourne le code contrat de travail 
	 * 
	 * Seules 3 valeurs sont autorisees : 01 CDI , 02 CDD , 03 SANS CONTRAT
	 * @return
	 */
	public String codeContratTravail() {

		// Pas de contrat de travail pour les titulaires
		if (contrat().statut().temTitulaire().equals("O"))
			return null;

		// pas de contrat de travail pour les agents relevant de la CNRACL	
		if (contrat().statut().regimeCotisation().pregRegimeVieillesse().equals(ConstantesDads.REGIME_BASE_CNRACL))
			return ConstantesDads.SANS_CONTRAT;

		if (contrat.statut().pstaAbrege().toUpperCase().equals("CAE") || 
				contrat.statut().pstaAbrege().toUpperCase().equals("CA"))
			return "02";

		if (contrat.statut().pstaAbrege().toUpperCase().equals("APPRENTI"))
			return "04";

		// CDI
		if (dateFin == null)
			return ConstantesDads.TYPE_CDI;

		return ConstantesDads.TYPE_CDD;
	}

	public String codeIntituleContratTravail() {

		if (contrat.statut().pstaAbrege().toUpperCase().equals("CA")) 
			return "40";

		if (contrat.statut().pstaAbrege().toUpperCase().equals("CAE")) 
			return "41";

		if (contrat.statut().pstaAbrege().toUpperCase().equals("APPRENTI"))
			return "90";

		return "90";
	}


	/*******************************************************
	 * CHOMAGE 
	 * 
	 * Assujettissement en fonction de la rubrique Mutualisation Risque Chomage
	 * Salarie SRE non concernes
	 * 
	 * @return
	 ******************************************************/

	/**
	 * 01 Assujettissement obligatoire
	 * 02 Volontaire
	 * 03 Non assujetti
	 */
	public String chomageAssujettissement() {

		if (contrat().statut().regimeCotisation().estSRE() 
				|| contrat.statut().regimeCotisation().pregCodeBasePlafonnee() == null )  
			return "03";

		return "01";

	}


	/**
	 * 01 Exo salariale
	 * 02 Exo Patronale
	 * 03 Exo Salariale et Patronale
	 * 90 Salarie non concerne
	 * @return
	 */
	public String chomageCodeExoneration() {

		if (contrat().statut().regimeCotisation().estSRE() || contrat.statut().regimeCotisation().pregCodeBasePlafonnee() == null)
			return "90";

		// Cotise t on au chomage (MRC)
		NSArray elements = rechercherElements(historiquesPaye(), "TXMRCHOP", null);
		if (elements.count() > 0)
			return "01";

		return "03";
	}

	/**
	 * 
	 * Null si Non Assujetti.
	 * Sinon baseBrute.
	 * 
	 */
	public String chomageSalaireBrut() {

		if (chomageAssujettissement().equals("03"))
			return "0.00";

		if (chomageCodeExoneration().equals("01"))
			return baseBrutImposable();

		return "0.00";	
	}



	/** retourne le type de contrat travail **/
	public String typeContrat() {
		String typeContrat = contrat().statut().pstaCipdz();
		typeContrat = "0" + typeContrat;
		if (typeContrat.equals(ConstantesDads.TEMPS_PLEIN)) {
			if (!contrat().travailATempsPlein()) {
				if (contrat.quotite() == null) {
					typeContrat = ConstantesDads.TEMPS_PLEIN;
				} else {
					typeContrat = ConstantesDads.TEMPS_PARTIEL;
				}
			} else {
				// cas des agents déclarés temps plein mais ayant une quotité < 100
				if (contrat.quotite() != null)
					typeContrat = ConstantesDads.TEMPS_PARTIEL;
			}
		}

		return typeContrat;
	}
	/**
	 * retourne l'indice majore si agent indiciaire, 
	 * l'indice brut si l'agent cotise a la CNRACL ou l'échelon ou "sans classement
	 * conventionnel" si pas d'information. On recherche l'indice valable l'annee de la Dads
	 */
	public String classementConventionnel() throws Exception {

		try {

			// CA
			if (contrat().statut().pstaAbrege().toUpperCase().equals("CA"))
				return "Contrat Avenir";

			// CAE
			if (contrat().statut().pstaAbrege().toUpperCase().equals("CAE"))
				return "Contrat d'accompagnement dans l'emploi";

			// Apprenti
			if (contrat().statut().pstaAbrege().toUpperCase().equals("APPRENTI"))
				return "Apprenti";


			if (contrat().indiceContrat() != null && contrat().indiceContrat().equals("") == false) {
				NSMutableArray args = new NSMutableArray(new Integer(contrat().indiceContrat()));

				if (contrat().dFinContratTrav() != null) {
					args.addObject(contrat().dFinContratTrav());
					args.addObject(contrat().dFinContratTrav());
				}
				else {
					args.addObject(contrat().dDebContratTrav());
					args.addObject(contrat().dDebContratTrav());                                 }

				EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("cIndiceMajore = %@ AND dMajoration <= %@ " +
						"AND (dFermeture = nil OR dFermeture >= %@)",args);

				EOFetchSpecification fs = new EOFetchSpecification(EOIndice.ENTITY_NAME,qualifier,null);
				try {
					String indice = (String)((EOGenericRecord)contrat().editingContext().objectsWithFetchSpecification(fs).objectAtIndex(0)).valueForKey(("cIndiceBrut"));
					// Si il s'agit d'un indice hors échelle, retirer le tiret
					if (indice.indexOf("-") > 0) {
						indice = indice.replaceAll("-", "");
					} else {
						if (indice.length() == 3) {
							indice = "0" + indice;
						}
					}
					return indice;
				} catch (Exception e) {
					e.printStackTrace();
					throw new Exception("Indice " + contrat().indiceContrat() + " non defini dans Grhum pour l'annee " + anneeDADS);
				}
			} else if (contrat().cEchelon() != null && contrat().cEchelon().equals("") == false) {
				return contrat().cEchelon();
			} else {
				return Constantes.SANS_CLASSEMENT_CONVENTIONNEL;
			}
		}
		catch (Exception e) {
			return Constantes.SANS_CLASSEMENT_CONVENTIONNEL;                    
		}             
	}


	public String codeDroitContratTravail() {
		if (contrat.statut().pstaAbrege().toUpperCase().equals("CEJ") || 
				contrat.statut().pstaAbrege().toUpperCase().equals("CES") || 
				contrat.statut().pstaAbrege().toUpperCase().equals("CEC")|| 
				contrat.statut().pstaAbrege().toUpperCase().equals("CAE") ||
				contrat.statut().pstaAbrege().toUpperCase().equals("APPRENTI") || 
				contrat.statut().pstaAbrege().toUpperCase().equals("CA")) {
			return ConstantesDads.DROIT_PRIVE;
		} else
			return ConstantesDads.DROIT_AUTRE;

	}
	/** retourne l'etat du contrat en fin d'annee */
	public String etatContratFinAnnee() {
		if (motifFinPeriode.equals(ConstantesDads.FIN_CONTRAT_APRES_ANNEE)) {
			return ConstantesDads.CONTRAT_ACTIF;
		} else {
			NSTimestamp dateDernierVendredi = DateCtrl.stringToDate("31/12/" + anneeDADS);
			boolean dernierVendredi = false;
			while (!dernierVendredi) {
				GregorianCalendar date = new GregorianCalendar();
				date.setTime(dateDernierVendredi);
				int jour = date.get(Calendar.DAY_OF_WEEK);
				if (jour == GregorianCalendar.FRIDAY) {
					dernierVendredi = true;
					break;
				} else {
					dateDernierVendredi = dateDernierVendredi.timestampByAddingGregorianUnits(0,0,-1,0,0,0);
				}
			}
			if (DateCtrl.isBefore(dateFin(),dateDernierVendredi)) {
				return ConstantesDads.CONTRAT_TERMINE;
			} else {
				return ConstantesDads.CONTRAT_ACTIF;
			}
		}
	}
	public String collegePrudhome() {
		if (etatContratFinAnnee().equals(ConstantesDads.CONTRAT_ACTIF)) {
			return ConstantesDads.COLLEGE_PRUDHOME_SALARIE;
		} else {
			return null;
		}
	}
	public String sectionPrudhome() {
		if (etatContratFinAnnee().equals(ConstantesDads.CONTRAT_ACTIF)) {
			return ConstantesDads.SECTION_PRUDHOME_ACTIVITES_DIVERSES;
		} else {
			return null;
		}
	}
	public BigDecimal montantGratificationStage() {
		return baseBrutTotale();
	}

	/** retourne le nombre d'heures reellement travaillees pendant cette periode
		Non gere */
	public String nbHeuresTravaillees() {

		if (cotiseAccidentTravail()) {

			// V08R08 : Calculer la durée totale et si elle est inférieure à 1200 heures, vérifier si elle est supérieure à 2030 smic horaire
			// Ranger les historiques par ordre de date décroissant

			// il faut ordonner tous les historiques par mois pour calculer les cumuls de rémunération par mois
			NSMutableArray historiques = new NSMutableArray();
			for (Enumeration<PeriodeDadsActive> e = agent.periodesEnCours().objectEnumerator();e.hasMoreElements();)
				historiques.addObjectsFromArray(e.nextElement().historiquesPaye());

			// trier sur le mois code en ordre ascendant. On garde le code du dernier mois où la condition est remplie
			EOSortOrdering.sortArrayUsingKeyOrderArray(historiques,
					new NSArray(EOSortOrdering.sortOrderingWithKey(EOPayeHisto.MOIS_KEY+"."+EOPayeMois.MOIS_CODE_KEY,EOSortOrdering.CompareAscending)));

			horairesAgent = new NSMutableDictionary();
			// On calcule le cumul d'heures et de rémunération totale sur chaque mois
			for (Enumeration<EOPayeHisto> e1 = historiques.objectEnumerator();e1.hasMoreElements();) {

				EOPayeHisto historique = e1.nextElement();
				EOPayeMois mois = historique.mois();

				if (horairesAgent.objectForKey(mois.moisAnnee().toString()) == null)
					horairesAgent.setObjectForKey(historique.payeNbheure(),String.valueOf(anneeDADS) );
				else {				
					BigDecimal heures = (BigDecimal)horairesAgent.objectForKey(String.valueOf(anneeDADS));								
					horairesAgent.setObjectForKey(heures.add(historique.payeNbheure()),String.valueOf(anneeDADS) );				
				}

				if (horairesAgent.objectForKey(mois.moisCode().toString()) == null)
					horairesAgent.setObjectForKey(historique.payeNbheure(),mois.moisCode().toString() );
				else {				
					BigDecimal heures = (BigDecimal)horairesAgent.objectForKey(mois.moisCode().toString());								
					horairesAgent.setObjectForKey(heures.add(historique.payeNbheure()),mois.moisCode().toString() );				
				}
			}

			if (horairesAgent.objectForKey(String.valueOf(anneeDADS)) != null) {

				BigDecimal heuresTravaillees = (BigDecimal)horairesAgent.objectForKey(String.valueOf(anneeDADS));

				return heuresTravaillees.setScale(EnvironnementDads.sharedInstance().nbDecimales(), BigDecimal.ROUND_DOWN).toString();			

			}
		}

		return null;

	}
	/** retourne le nombre d'heures payeees pendant cette periode */
	public Number nbHeuresPayees() {
		double nbHeures = 0;
		for (Enumeration<EOPayeHisto> e = historiquesPaye().objectEnumerator();e.hasMoreElements();) {
			EOPayeHisto historique = (EOPayeHisto)e.nextElement();
			nbHeures = nbHeures + historique.payeNbheure().doubleValue();	
		}
		if (nbHeures < 1)
			return null;
		else
			return new Double(nbHeures);

	}

	/** retourne 01 si le nombre d'heures annuel est superieur a 1200 heures ou 2030 smic, 99 sinon */
	public String codeDureeAnnuelle() throws Exception {

		if (!estDernierePeriode())
			return null;


		// V08R08 : Calculer la durée totale et si elle est inférieure à 1200 heures, vérifier si elle est supérieure à 2030 smic horaire
		// Ranger les historiques par ordre de date décroissant
		Enumeration e = agent.periodesEnCours().objectEnumerator();
		// il faut ordonner tous les historiques par mois pour calculer les cumuls de rémunération par mois
		NSMutableArray historiques = new NSMutableArray();
		while (e.hasMoreElements()) {
			historiques.addObjectsFromArray(((PeriodeDadsActive)e.nextElement()).historiquesPaye());
		}
		// trier sur le mois code en ordre ascendant. On garde le code du dernier mois où la condition est remplie
		EOSortOrdering.sortArrayUsingKeyOrderArray(historiques,
				new NSArray(EOSortOrdering.sortOrderingWithKey(EOPayeHisto.MOIS_KEY+"."+EOPayeMois.MOIS_CODE_KEY,EOSortOrdering.CompareAscending)));

		// rechercher le montant minimum d'heures au taux smic		
		double salaireMinimum = montantSmicPourNbHeures(((EOPayeHisto)historiquesPaye().objectAtIndex(0)).editingContext(),ConstantesDads.NB_HEURES_SMIC_ANNUEL);	
		double nbHeuresAnnuelles = 0, remunTotale = 0;



		horairesAgent = new NSMutableDictionary();
		e = historiques.objectEnumerator();
		// On calcule le cumul d'heures et de rémunération totale sur chaque mois
		while (e.hasMoreElements()) {

			EOPayeHisto historique = (EOPayeHisto)e.nextElement();
			EOPayeMois mois = historique.mois();
			nbHeuresAnnuelles += historique.payeNbheure().doubleValue();
			remunTotale += historique.payeBssmois().doubleValue();

			if (horairesAgent.objectForKey(mois.moisAnnee().toString()) == null)
				horairesAgent.setObjectForKey(historique.payeNbheure(),String.valueOf(anneeDADS) );
			else {				
				BigDecimal heures = (BigDecimal)horairesAgent.objectForKey(String.valueOf(anneeDADS));								
				horairesAgent.setObjectForKey(heures.add(historique.payeNbheure()),String.valueOf(anneeDADS) );				
			}

			if (horairesAgent.objectForKey(mois.moisCode().toString()) == null)
				horairesAgent.setObjectForKey(historique.payeNbheure(),mois.moisCode().toString() );
			else {				
				BigDecimal heures = (BigDecimal)horairesAgent.objectForKey(mois.moisCode().toString());								
				horairesAgent.setObjectForKey(heures.add(historique.payeNbheure()),mois.moisCode().toString() );				
			}
		}

		if (nbHeuresAnnuelles >= ConstantesDads.DUREE_ANNUELLE_MINIMUM || remunTotale >= salaireMinimum) {
			conditionHoraireRemplie = true;
			return ConstantesDads.CODE_DUREE_MINIMUM_ANNUELLE_OK;
		} else {
			return ConstantesDads.CODE_DUREE_MINIMUM_ANNUELLE_NON;
		}
	}
	/** retourne le dernier trimestre de travail &eagrave; 120 heures. */
	public String dernierMoisPourMinimumTrimestriel() throws Exception {
		if (!estDernierePeriode()) {
			return null;
		} else if (conditionHoraireRemplie) {
			return ConstantesDads.CODE_DUREE_MINIMUM_VALIDEE;
		} else {
			return dernierPeriodeAvecValeursMinimum(ConstantesDads.DUREE_TRIMESTRIEL_MINIMUM, ConstantesDads.NB_HEURES_SMIC_TRIMESTRIEL, false);
		}
	}
	/** retourne le dernier mois de travail &eagrave; soixante heures. */
	public String dernierMois60Heures() throws Exception {

		if (!estDernierePeriode())
			return null;
		else 
			if (conditionHoraireRemplie)
				return ConstantesDads.CODE_DUREE_MINIMUM_VALIDEE;
			else {
				return dernierPeriodeAvecValeursMinimum(ConstantesDads.DUREE_MENSUELLE_MINIMUM, ConstantesDads.NB_HEURES_SMIC_MENSUEL, true);
			}

	}
	public String codeSectionAT() throws Exception {

		if (cotiseAccidentTravail())
			return risqueAccidentTravail().substring(0,2);
		else
			return null;

	}
	/** Rertourne le risque accident du travail. Lance une exception si le risque accident du travail n'est pas defini */
	public String risqueAccidentTravail() throws Exception {
		if (cotiseAccidentTravail()) {
			if (contrat().structure().risqueAccidentTravail() == null) {
				throw new Exception("Le risque accident du travail n'est pas défini dans la structure " + contrat().structure().llStructure());
			}
			return contrat().structure().risqueAccidentTravail();
		}

		return null;

	}

	/** Retourne le risque accident du travail de bureau ou null */
	public String codeBureauAccidentTravail() throws Exception {
		String risque = risqueAccidentTravail();
		if (risque != null) {
			String[] codesInvalides = {"753CB","911AA","753CA","401ZA","753CC","631AZ","926CC","926CF","631AB","923AC","926CE","926CD",
					"401ZB","802CA","751CA","802AA","853KA","752EC","853CA","752EB","804CA","752EA","950ZD","950ZC",
					"950ZB","511TG","950ZA","913EG","913EF","526GA","913EE","913ED","913EC","853KC","853KB","752ED","745AB",
					"524RB","853HB","853HA","745BB","745BD","911AE","745BA","853KD","511TH","853KE","853KF","853KG","741GC",
					"801ZA","853KH","853KI","926CH","926CI", "99999"};
			for (int i = 0; i < codesInvalides.length;i++) {
				if (risque.equals(codesInvalides[i])) {
					return null;
				}
			}
			return Constantes.CODE_BUREAU_AT;
		} else {
			return null;
		}
	}
	/** montant du taux (AT) sous la forme d'une string pour la DADS */
	public String valeurTauxAccidentTravail() throws Exception {

		if (cotiseAccidentTravail()) {

			String codeTaux = contrat().structure().tauxAccidentTravail();
			if (codeTaux != null) {

				EOPayeCode code = EOPayeCode.rechercherCode(contrat().editingContext(),codeTaux);
				long moisCode = ((long)anneeDADS * 100) + 12;
				EOPayeParam parametre = EOPayeParam.parametrePourMois(contrat().editingContext(), code, new Long(moisCode));

				if (parametre != null && parametre.pparTaux() != null)
					return ((parametre.pparTaux()).setScale(2, BigDecimal.ROUND_HALF_UP)).toString();
				else
					throw new Exception("Le taux AT est inconnu pour le code " + codeTaux);
			}
		}

		return null;

	}

	/** Calcule le montant total de la base securite sociale */
	public String baseBrutSecu() {
		
		BigDecimal total = calculBaseBruteSecu();
		total = total.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);

		return total.toString();
	}

	public String natureBaseCotisation() {
		return ConstantesDads.BASE_COTISATION_RELLE;
	}
	/** Calcule le montant brut de la base brut pour la retraite complementaire */
	public BigDecimal baseBrutComplementaire() {
		BigDecimal total = calculBaseBruteSecu();
		total = total.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);

		return total;
	}

	/** Calcule l'assiette de la CSG non deductible pour cette periode
	 **/
	public String baseCSGNonReduite() {

		String code = contrat().statut().regimeCotisation().pregCodeCSG();
		if (code != null)
			return (calculerSurElements(code,ConstantesDads.CODE_CSG_NON_DEDUCTIBLE,true,CHARGE_SALARIALE)).toString();

		return "0.00";

	}
	/** retourne l'assiette de la CSG pour des CSG a taux reduit 
	 **/
	public BigDecimal baseCSGReduite() {
		String code = contrat().statut().regimeCotisation().pregCodeCSGReduite();
		if (code != null) {
			BigDecimal result = calculerSurElements(code,ConstantesDads.CODE_CSG_NON_DEDUCTIBLE,true,CHARGE_SALARIALE);
			if (result.doubleValue() == 0) {
				EnvironnementDads.sharedInstance().ajouterErreur("\n" + contrat().individu().identite() + " : le montant de la CSG réduite ne peut être égal à 0");
			}
			return result;
		} else {
			return new BigDecimal(0);
		}
	}

	public BigDecimal baseCRDS() {
		String code = contrat().statut().regimeCotisation().pregCodeCRDS();
		if (code != null) {
			return calculerSurElements(code,ConstantesDads.CODE_CRDS,true,CHARGE_SALARIALE);
		} else {
			return new BigDecimal(0);
		}
	}

	public String salaireBrutMRC() {
		String code = "TXCGCSOL";//contrat().statut().regimeCotisation().pregCodeCRDS();
		if (code != null)
			return (calculerSurElements(code,ConstantesDads.CODE_MRC,true,CHARGE_PATRONALE)).toString();
		return "0.00";
	}


	/** Calcule le montant de la CRDS pour cette periode
	 **/
	public BigDecimal montantCRDS() {
		String code = contrat().statut().regimeCotisation().pregCodeCRDS();
		if (code != null) {
			return calculerSurElements(code,ConstantesDads.CODE_CRDS,false,CHARGE_SALARIALE);
		} else {
			return new BigDecimal(0);
		}
	}
	/** Calcule le montant de la CSG pour cette periode
	 **/
	public BigDecimal montantCSG() {
		String code = contrat().statut().regimeCotisation().pregCodeCSG();
		if (code != null) {
			return calculerMontantCSGSurElements();
		} else {
			code = contrat().statut().regimeCotisation().pregCodeCSGReduite();
			if (code != null) {
				return calculerMontantCSGSurElements();
			} else {
				return new BigDecimal(0);
			}
		}
	}
	/** Pour les agents travaillant a l'etranger */
	public String travailEtranger() {
		return null;
	}
	/** Calcul de la base brute totaleMedecin */
	public BigDecimal baseBrutTotaleMedecin() {
		if (contrat().correspondSommeIsolee(anneeDADS)) {	// 03/12/07 - impératif dans V08R06 mais uniquement pour les médecins
			for (int i = 0; i < ConstantesDads.HOSPITALIERS.length;i++) {
				if (contrat.statut().regimeCotisation().pregStatutProf().equals(ConstantesDads.HOSPITALIERS[i])) {
					return new BigDecimal(0);
				}
			}
			return null;
		}
		if (contrat.statut().regimeCotisation() != null && contrat.statut().regimeCotisation().pregStatutProf() != null) {
			for (int i = 0; i < ConstantesDads.HOSPITALIERS.length;i++) {
				if (contrat.statut().regimeCotisation().pregStatutProf().equals(ConstantesDads.HOSPITALIERS[i]))
					return baseBrutTotale();

			}
			return null;
		} else 
			return null;
	}
	/** Calcul de la base brute totale */
	public BigDecimal baseBrutTotale() {
		BigDecimal total = new BigDecimal(0);
		for (Enumeration<EOPayeHisto> e = historiquesPaye().objectEnumerator();e.hasMoreElements();)
			total = total.add(e.nextElement().payeBrutTotal());

		return total;
	}
	/** Calcul du cout total */
	public BigDecimal coutTotal() {
		BigDecimal total = new BigDecimal(0);
		for (Enumeration<EOPayeHisto> e = historiquesPaye().objectEnumerator();e.hasMoreElements();) 
			total = total.add(e.nextElement().payeCout());

		return total;
	}
	/** Calcul de la base brute imposable : 21/11/08 le resultat est tronque des centimes */
	public String baseBrutImposable() {

		BigDecimal result = calculBaseBruteImposable();

		if (result.doubleValue() < 0)
			result = new BigDecimal(0.00);

		result = result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_DOWN);

		return result.toString();
	}
	/** 30/01/08 - Calcul de la base brute imposable avec un signe positif ou negatif 
	 * 21/11/08 le resultat est tronque des centimes */
	public BigDecimal baseBrutImposableSignee() {
		BigDecimal result = calculBaseBruteImposable();

		result = result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_DOWN);

		return result;
	}
	/** Retourne le montant des avantages en nature.
	 * 21/11/08 - rubrique fiscale, les centimes sont tronques
	 * Pour l'instant rien
	 */
	public BigDecimal avantageEnNature() {
		BigDecimal result = new BigDecimal(0);
		// ajouter l'ensemble des avantages en nature
		result = result.add(calculerSurElements(ConstantesDads.AVANTAGE_NOURRITURE,ConstantesDads.AVANTAGE_NOURRITURE,false,REMUNERATION));
		result = result.add(calculerSurElements(ConstantesDads.AVANTAGE_LOGEMENT,ConstantesDads.AVANTAGE_LOGEMENT,false,REMUNERATION));
		result = result.add(calculerSurElements(ConstantesDads.AVANTAGE_AUTRE,ConstantesDads.AVANTAGE_AUTRE,false,REMUNERATION));
		result = result.add(calculerSurElements(ConstantesDads.AVANTAGE_NTIC,ConstantesDads.AVANTAGE_NTIC,false,REMUNERATION));
		result = result.add(calculerSurElements(ConstantesDads.AVANTAGE_VEHICULE,ConstantesDads.AVANTAGE_VEHICULE,false,REMUNERATION));
		if (result.doubleValue() == 0) {
			return null;
		} else {
			return result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_DOWN);
		}
	}
	public String nourriture() {
		BigDecimal result = new BigDecimal(0);
		// ajouter l'ensemble des avantages en nature
		result = result.add(calculerSurElements(ConstantesDads.AVANTAGE_NOURRITURE,ConstantesDads.AVANTAGE_NOURRITURE,false,REMUNERATION));
		if (result.doubleValue() == 0) {
			return null;
		} else {
			return ConstantesDads.DADS_AVANTAGE_NOURRITURE;
		}
	}
	public String logement() {
		BigDecimal result = new BigDecimal(0);
		// ajouter l'ensemble des avantages en nature
		result = result.add(calculerSurElements(ConstantesDads.AVANTAGE_LOGEMENT,ConstantesDads.AVANTAGE_LOGEMENT,false,REMUNERATION));
		if (result.doubleValue() == 0) {
			return null;
		} else {
			return ConstantesDads.DADS_AVANTAGE_LOGEMENT;
		}
	}
	public String voiture() {
		BigDecimal result = new BigDecimal(0);
		// ajouter l'ensemble des avantages en nature
		result = result.add(calculerSurElements(ConstantesDads.AVANTAGE_VEHICULE,ConstantesDads.AVANTAGE_VEHICULE,false,REMUNERATION));
		if (result.doubleValue() == 0) {
			return null;
		} else {
			return ConstantesDads.DADS_AVANTAGE_VEHICULE;
		}
	}
	public String autresAvantages() {
		BigDecimal result = new BigDecimal(0);
		// ajouter l'ensemble des avantages en nature
		result = result.add(calculerSurElements(ConstantesDads.AVANTAGE_AUTRE,ConstantesDads.AVANTAGE_AUTRE,false,REMUNERATION));
		if (result.doubleValue() == 0) {
			return null;
		} else {
			return ConstantesDads.DADS_AVANTAGE_AUTRE;
		}
	}
	public String avantageNouvelleTechnologie() {
		BigDecimal result = new BigDecimal(0);
		// ajouter l'ensemble des avantages en nature
		result = result.add(calculerSurElements(ConstantesDads.AVANTAGE_NTIC,ConstantesDads.AVANTAGE_NTIC,false,REMUNERATION));
		if (result.doubleValue() == 0) {
			return null;
		} else {
			return ConstantesDads.DADS_AVANTAGE_NTIC;
		}
	}

	/** 21/11/08 - montant fiscal => tronque des centimes */
	public BigDecimal retenueSurSalairePourAvantageEnNature() {
		// retenue sur salaire pour le logement TOM
		BigDecimal result = new BigDecimal(0);
		// ajouter l'ensemble des avantages en nature
		result = result.add(calculerSurElements(ConstantesDads.CODE_PRIME_LOGEMENT_TOM,ConstantesDads.CODE_PRIME_LOGEMENT_TOM,false,REMUNERATION));
		if (result.doubleValue() == 0) {
			return null;
		} else {
			return result.abs().setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_DOWN);
		}
	}

	/** Retourne le montant des frais professionnels
	 * Pour l'instant uniquement les abattements artistes/intermittents
	 * 21/11/08 - montant fiscal => tronque des centimes
	 */
	public BigDecimal fraisProfessionnels() {
		if (contrat().abattement() != null && contrat().abattement().doubleValue() != 0) {
			// artistes-intermittents 
			BigDecimal result = calculBaseBruteImposable();
			// calculer le montant de l'abattement
			double calcul = result.doubleValue() * (contrat().abattement().doubleValue() / 100);
			result = new BigDecimal(calcul).setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_DOWN);

			return result;
		} else {
			return null;
		}
	}
	public String remboursementFraisProfessionnels() {
		return null;
	}
	public String allocationForfaitaire() {
		if (contrat().abattement() != null && contrat().abattement().doubleValue() != 0) {
			// artistes-intermittents
			return Constantes.ALLOCATION_FORFAITAIRE;
		} else {
			return null;
		}
	}
	public String remboursementAutres() {
		return null;
	}
	/** Calcule le montant des retenues à la source pour les personnes domiciliees hors de France
	 *  21/11/08 - montant fiscal => tronque des centimes
	 **/
	public BigDecimal impotsRetenusSource() {
		if (!contrat().individu().estImposableEnFrance()) {
			BigDecimal result = calculerSurElements(ConstantesDads.CODE_RETENUE_SOURCE,ConstantesDads.CODE_RETENUE_SOURCE,false,REMUNERATION);
			return result.abs().setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_DOWN);
		} else
			return null;

	}

	public String indiceBrutSRE() {

		String indiceMajore = indiceMajoreSRE();

		if (indiceMajore != null) {

			EOIndice indiceBrut = EOIndice.indiceBrut(ec, indiceMajore);

			if (indiceBrut != null)
				return indiceBrut.cIndiceBrut();
		}

		return null;

	}

	public String indiceMajoreSRE() {

		if (contrat().statut().regimeCotisation().estSRE()) {

			return contrat().indiceOrigine();

		}

		return null;

	}


	/** Calcule le montant des indemnites d'expatriation : pour l'instant null */
	public BigDecimal indemniteExpatriation() {
		return null;
	}

	/** retourne l'assiette totale de la taxe sur salaire pour la derni&egrave;re periode d'emploi 
	 * on prend en compte l'ensemble des periodes pour le contrat 
	 * 29/01/08 - Comme on prend en compte l'ensemble des periodes, si la taxe sur salaire est negative
	 * on retourne null car non autorise par la norme<BR>
	 * 21/11/08 - montant fiscal => tronque des centimes
	 *  */
	public String baseTotalTaxeSurSalaire() {

		// pas de taxe sur salaire pour les structures soumises à TVA
		if (contrat().structure().estSoumiseTVA() || !estDernierePeriode() )
			return null;

		// il faut cumuler sur toutes les périodes de l'agent pour le cas ou plusieurs périodes
		BigDecimal result = new BigDecimal(0);
		for (Enumeration<PeriodeDadsActive> e = agent.periodesEnCours().objectEnumerator();e.hasMoreElements();) {

			PeriodeDadsActive periode = e.nextElement();
			String code = periode.contrat().statut().regimeCotisation().pregTaxeSurSalaire();
			if (code != null && code.length() > 0) {
				result = result.add(periode.calculerSurElements(ConstantesDads.CODE_TAUX_TRANCHE1_TAXE,ConstantesDads.CODE_TAXE_SALAIRE1,true,CHARGE_PATRONALE));
				if (result.doubleValue() != 0 || agent.aPeriodesConflictuelles()) {	// taxe sur salaire payée
					BigDecimal nouveauRes = periode.calculerSurElements(ConstantesDads.CODE_TAUX_TRANCHE2_TAXE,ConstantesDads.CODE_TAXE_SALAIRE2,true,CHARGE_PATRONALE);
					result = result.add(nouveauRes);
					if (nouveauRes.doubleValue() != 0 || agent.aPeriodesConflictuelles())
						result = result.add(periode.calculerSurElements(ConstantesDads.CODE_TAUX_TRANCHE3_TAXE,ConstantesDads.CODE_TAXE_SALAIRE3,true,CHARGE_PATRONALE));

				}
			}
		}
		if (result.doubleValue() > 0)
			return result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_DOWN).toString();
		else
			return null;

	}
	/** retourne l'assiette pour la tranche 2 de taxe sur salaire pour la derniere periode d'emploi 
	 * on prend en compte l'ensemble des periodes
	 * 29/01/08 - Comme on prend en compte l'ensemble des periodes, si la taxe sur salaire est negative
	 * on retourne null car non autorise par la norme<BR>
	 * 21/11/08 - montant fiscal => tronque des centimes */
	public String baseTaux2TaxeSurSalaire() {

		// pas de taxe sur salaire pour les structures soumises à TVA
		if (contrat().structure().estSoumiseTVA() || !estDernierePeriode())
			return null;

		BigDecimal result = new BigDecimal(0);
		for (Enumeration<PeriodeDadsActive> e = agent.periodesEnCours().objectEnumerator();e.hasMoreElements();) {

			PeriodeDadsActive periode = e.nextElement();
			String code = periode.contrat().statut().regimeCotisation().pregTaxeSurSalaire();
			if (code != null && code.length() > 0)
				result = result.add(
						periode.calculerSurElementsEtHistoriques(historiquesPaye(), 
								ConstantesDads.CODE_TAUX_TRANCHE2_TAXE, ConstantesDads.CODE_TAXE_SALAIRE2, 
								true, CHARGE_PATRONALE, false));
			//				result = result.add(periode.calculerSurElements(ConstantesDads.CODE_TAUX_TRANCHE2_TAXE,ConstantesDads.CODE_TAXE_SALAIRE2,true,CHARGE_PATRONALE));

		}

		if (result.doubleValue() > 0)	// 29/01/08 et 21/11/08
			return result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_DOWN).toString();
		else
			return null;


	}
	/** retourne l'assiette pour la tranche 3 de taxe sur salaire pour la derni&egrave;re periode d'emploi
	 * on prend en compte l'ensemble des periodes 
	 * 29/01/08 - Comme on prend en compte l'ensemble des periodes, si la taxe sur salaire est negative
	 * on retourne null car non autorise par la norme<BR>
	 * 21/11/08 - montant fiscal => tronque des centimes */
	public String baseTaux3TaxeSurSalaire() {

		// pas de taxe sur salaire pour les structures soumises à TVA
		if (contrat().structure().estSoumiseTVA() || !estDernierePeriode())
			return null;

		BigDecimal result = new BigDecimal(0);
		for (Enumeration<PeriodeDadsActive> e = agent.periodesEnCours().objectEnumerator();e.hasMoreElements();) {
			PeriodeDadsActive periode = e.nextElement();
			String code = periode.contrat().statut().regimeCotisation().pregTaxeSurSalaire();
			if (code != null && code.length() > 0) {
				result = result.add(
						periode.calculerSurElementsEtHistoriques(historiquesPaye(), 
								ConstantesDads.CODE_TAUX_TRANCHE3_TAXE, ConstantesDads.CODE_TAXE_SALAIRE3, 
								true, CHARGE_PATRONALE, false));
			}
		}
		if (result.doubleValue() > 0)
			return result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_DOWN).toString();
		else
			return null;

	}

	/** retourne le montant total de taxe sur salaire pour la derni&egrave;re periode d'emploi
	 * on prend en compte l'ensemble des periodes
	 * 29/01/08 - Comme on prend en compte l'ensemble des periodes, si la taxe sur salaire est negative
	 * on retourne null car non autorise par la norme */
	public String montantTotalTaxeSurSalaire() {
		if (contrat().structure().estSoumiseTVA() || !estDernierePeriode())
			// pas de taxe sur salaire pour les structures soumises à TVA
			return null;

		// il faut cumuler sur toutes les périodes de l'agent pour le cas où plusieurs périodes
		// 09/02/06 - on regarde sur toutes les tranches au cas où il y a des périodes conflictuelles
		BigDecimal result = new BigDecimal(0);
		for (Enumeration<PeriodeDadsActive> e = agent.periodesEnCours().objectEnumerator();e.hasMoreElements();) {
			PeriodeDadsActive periode = e.nextElement();
			String code = periode.contrat().statut().regimeCotisation().pregTaxeSurSalaire();
			if (code != null && code.length() > 0) {
				result = result.add(periode.calculerSurElements(ConstantesDads.CODE_TAUX_TRANCHE1_TAXE,ConstantesDads.CODE_TAXE_SALAIRE1,false,CHARGE_PATRONALE));
				if (result.doubleValue() != 0 || agent.aPeriodesConflictuelles()) {	// taxe sur salaire payée
					BigDecimal nouveauRes = periode.calculerSurElements(ConstantesDads.CODE_TAUX_TRANCHE2_TAXE,ConstantesDads.CODE_TAXE_SALAIRE2,false,CHARGE_PATRONALE);
					result = result.add(nouveauRes);
					if (nouveauRes.doubleValue() != 0 || agent.aPeriodesConflictuelles()) {
						result = result.add(periode.calculerSurElements(ConstantesDads.CODE_TAUX_TRANCHE3_TAXE,ConstantesDads.CODE_TAXE_SALAIRE3,false,CHARGE_PATRONALE));
					}
				}
			}
		}
		if (result.doubleValue() > 0)
			return result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP).toString();
		else
			return null;

	}
	/** retourne le montant  de taxe sur salaire Tranche 2 pour la derni&egrave;re periode d'emploi
	 * on prend en compte l'ensemble des periodes */
	public String montantTaux2TaxeSurSalaire() {

		// pas de taxe sur salaire pour les structures soumises à TVA
		if (contrat().structure().estSoumiseTVA() || !estDernierePeriode())
			return null;

		// il faut cumuler sur toutes les périodes de l'agent pour le cas où plusieurs périodes
		BigDecimal result = new BigDecimal(0);
		for (Enumeration<PeriodeDadsActive> e = agent.periodesEnCours().objectEnumerator();e.hasMoreElements();) {
			PeriodeDadsActive periode = e.nextElement();
			String code = periode.contrat().statut().regimeCotisation().pregTaxeSurSalaire();
			if (code != null && code.length() > 0) {
				BigDecimal nouveauRes = periode.calculerSurElements(ConstantesDads.CODE_TAUX_TRANCHE2_TAXE,ConstantesDads.CODE_TAXE_SALAIRE2,false,CHARGE_PATRONALE);
				result = result.add(nouveauRes);	
			}
		}

		if (result.doubleValue() != 0)
			return result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP).toString();
		else
			return null;

	}
	/** retourne le montant  de taxe sur salaire Tranche 3 pour la derni&egrave;re periode d'emploi
	 * on prend en compte l'ensemble des periodes */
	public String montantTaux3TaxeSurSalaire() {
		if (contrat().structure().estSoumiseTVA()) {
			// pas de taxe sur salaire pour les structures soumises à TVA
			return null;
		}
		if (!estDernierePeriode()) {
			return null;
		}
		// il faut cumuler sur toutes les périodes de l'agent pour le cas où plusieurs périodes
		BigDecimal result = new BigDecimal(0);
		for (Enumeration<PeriodeDadsActive> e = agent.periodesEnCours().objectEnumerator();e.hasMoreElements();) {

			PeriodeDadsActive periode = e.nextElement();
			String code = periode.contrat().statut().regimeCotisation().pregTaxeSurSalaire();
			if (code != null && code.length() > 0) {
				BigDecimal nouveauRes = periode.calculerSurElements(ConstantesDads.CODE_TAUX_TRANCHE3_TAXE,ConstantesDads.CODE_TAXE_SALAIRE3,false,CHARGE_PATRONALE);
				result = result.add(nouveauRes);	
			}
		}

		if (result.doubleValue() != 0)
			return result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP).toString();
		else
			return null;

	}

	/** calcule le montant net imposable pour la periode. 21/11/08 - montant fiscal => tronque des centimes */
	public String montantNetImposable() {

		BigDecimal total = new BigDecimal(0.0);
		String message = "";
		for (Enumeration<EOPayeHisto> e = historiquesPaye().objectEnumerator();e.hasMoreElements();) {
			EOPayeHisto histo = e.nextElement();
			total = total.add(histo.payeBimpmois());
			if (histo.payeBimpmois().doubleValue() < 0 && histo.temCompta() != null && histo.temCompta().equals(Constantes.VRAI)) {
				if (message.length() > 0)
					message += ", ";

				message += histo.mois().moisComplet();
			}
		}
		if (message.length() > 0)
			EnvironnementDads.sharedInstance().ajouterErreurUnique("\nATTENTION - " + contrat().individu().identite() + " : le montant du net imposable est negatif pour le(s) mois de " + message);

		if (total.doubleValue() < 0)
			total = new BigDecimal(0.00);

		total = total.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_DOWN);

		return total.toString();
	}

	/** 07/11/06	: V08R04 - uniquement pour le BTP */
	public BigDecimal montantCongesPayes() {
		return null;
	}
	/** Peut-être A CORRIGER pour CPA et CFA */
	public BigDecimal allocationPreRetraite() {
		return null;
	}

	public String montantExoSalariale() {	// 13/05/08. Modifié le 21/11/08

		BigDecimal result = new BigDecimal(montantRemunerationBruteExoneree());
		if (result.doubleValue() == 0)
			return null;

		// 13/05/08 pour n'afficher les erreurs pour les agents qui ne sont pas en RA
		// 21/11/08 moidifer pour calculer les cumuls sur toutes les périodes
		if (contrat().statut().pstaCipdz().equals(EOPayeStatut.CATEGORIE_VACATAIRE) == false) {
			verifierValiditeMontantsHeuresExonerees();
		}
		return result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_DOWN).toString();
	}



	/** Calcul du montant d'heures supplementaires/complementaires exonerees sur la periode. On calcule
	 * les deux d'un coup en sachant que pour un m&circ;me contrat, on ne peut avoir des heures supp. et des heures comp.
	 * Signale une erreur si le  cumul des montants d'heures supplementaires est superieur a 25% du cumul des 
	 * nets imposable.<BR>
	 * 21/11/08 - montant fiscal => tronque des centimes
	 *  */
	public String montantHeuresSupExo() {	// 13/05/08. Modifié le 21/11/08

		BigDecimal result = new BigDecimal(montantRemunerationBruteExoneree());
		if (result.doubleValue() == 0)
			return null;

		// 13/05/08 pour n'afficher les erreurs pour les agents qui ne sont pas en RA
		// 21/11/08 moidifer pour calculer les cumuls sur toutes les périodes
		if (contrat().statut().pstaCipdz().equals(EOPayeStatut.CATEGORIE_VACATAIRE) == false) {
			verifierValiditeMontantsHeuresExonerees();
		}
		return result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_DOWN).toString();
	}
	/** V0R08 - 20/11/08 pas d'exoneration patronale pour l'Etat */
	public String montantDeductionPatronaleHeuresExonerees() {
		return "0.00";
	}
	/** V0R08 - 20/11/08
	 * Calcul le montant de la deduction d'heures supplementaires/complementaires exonerees salariales sur la periode */
	public String montantDeductionSalarialeHeuresExonerees() {
		// On ajoute les deux car c'est forcément l'un ou l'autre sinon il ne s'agirait pas de la même périodes (contrats différents)
		BigDecimal result = calculerSurElements(ConstantesDads.CODE_DEDUCTION_HEURES_SUP_CONTRACTUEL,ConstantesDads.CODE_DEDUCTION_HEURES_SUP_CONTRACTUEL,false,CHARGE_SALARIALE);
		result = result.add(calculerSurElements(ConstantesDads.CODE_DEDUCTION_HEURES_SUP_FONCTIONNAIRE,ConstantesDads.CODE_DEDUCTION_HEURES_SUP_FONCTIONNAIRE,false,CHARGE_SALARIALE));
		// On retourne la valeur opposée car ce sont des déductions sur le BS (=> négatives)
		return (result.negate()).toString();
	}
	/** Retourne le montant brut de la remuneration d'heures supplementaires/complementaires exonerees sur la periode*/
	public String montantRemunerationBruteExoneree() {
		BigDecimal result = calculerSurElements(ConstantesDads.CODE_REMUN_HEURES_SUP_EXO,ConstantesDads.CODE_REMUN_HEURES_SUP_EXO,false,REMUNERATION);
		result = result.add(calculerSurElements(ConstantesDads.CODE_REMUN_HEURES_COMP_EXO,ConstantesDads.CODE_REMUN_HEURES_COMP_EXO,false,REMUNERATION));
		return (result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP)).toString();
	}

	public String typeExonerationHeuresSup() {
		BigDecimal result = calculerSurElements(ConstantesDads.CODE_REMUN_HEURES_SUP_EXO,ConstantesDads.CODE_REMUN_HEURES_SUP_EXO,false,REMUNERATION);
		if (result.doubleValue() != 0) {
			return ConstantesDads.TYPE_HEURES_SUPP_EXO;
		} else {
			return ConstantesDads.TYPE_HEURES_COMP_EXO;
		}
	}
	/** Retourne le nombre total d'heures supp./comp. exonerees x 100) */
	public String nbHeuresSuppExonerees() {
		// L'assiette de cotisation contient le nombre d'heures. On ajoute les deux car c'est forcément l'un ou l'autre
		// sinon il ne s'agirait pas de la même périodes (contrats différents)
		BigDecimal result = calculerSurElementsEtHistoriques(historiquesPaye(),ConstantesDads.CODE_REMUN_HEURES_SUP_EXO,ConstantesDads.CODE_REMUN_HEURES_SUP_EXO,true,REMUNERATION,false);
		result = result.add(calculerSurElementsEtHistoriques(historiquesPaye(),ConstantesDads.CODE_REMUN_HEURES_COMP_EXO,ConstantesDads.CODE_REMUN_HEURES_COMP_EXO,true,REMUNERATION,false));

		return (result.setScale(2, BigDecimal.ROUND_HALF_UP)).toString();
	}
	boolean aAIndemniteDepart() {
		return montantIndemniteRupture().doubleValue() != 0;
	}
	public String typeIndemniteRupture() {
		return ConstantesDads.TYPE_INDEMNITE_LICENCIEMENT;
	}
	public BigDecimal montantIndemniteRupture() {
		return calculerSurElements(ConstantesDads.INDEMNITE_LICENCIEMENT, ConstantesDads.INDEMNITE_LICENCIEMENT, false, REMUNERATION);
	}

	public boolean cotiseAccidentTravail() {

		EORegimeCotisation regime = contrat().statut().regimeCotisation();

		if (regime.pregRegimeAT() != null)
			return regime.pregRegimeAT().equals(ConstantesDads.REGIME_GENERAL);
		else
			return regime.pregRegimeMaladie() != null && regime.pregRegimeMaladie().equals(ConstantesDads.REGIME_GENERAL);

	}

	/** retourne le numero d'organisme pour la retraite complementaire
	 * 07/11/06 : lance une exception si le numero n'est pas conforme &grave; la norme
	 **/
	public String numOrganismeComplementaire() throws Exception {

		if (contrat().statut().regimeCotisation() != null) {
			String codeOrganisme = contrat().statut().regimeCotisation().pregComplementaire();
			if (codeOrganisme == null || codeOrganisme.equals(Constantes.SANS_COMPLEMENTAIRE))
				return null;

			// DADS V08R09 - Pour prendre en compte la pension civile
			if (contrat().statut().regimeCotisation().estRegimeIntermittent() == false && contrat().statut().regimeCotisation().estSRE() == false)
				return contrat().structure().numOrganismeComplementaire(codeOrganisme);
			else
				return null;

		} else
			throw new Exception("Pas de régime de cotisation pour le statut " + contrat().statut().pstaLibelle());

	}
	/** 18/01/2010 : retourne le numero d'organisme de la RAFP<BR>
	 * pour pouvoir generer 2 rubriques G01.01 si l'organisme complementaire est la SRE.
	  Lance une exception si le numero n'est pas conforme a la norme
	 **/
	public String numOrganismeComplementaireRAFP() throws Exception {
		return contrat().structure().numOrganismeComplementaire(Constantes.RAFP);
	}
	/** retourne le montant des remunerations intervenant dans la RAFP en centieme d'euros */
	public String assietteRAFP() {

		BigDecimal result = calculerPrimesPourRafpSurElementsEtHistoriques(historiquesPaye());

		if (result != null)
			return result.setScale(2,BigDecimal.ROUND_HALF_UP).toString();

		return "0.00";

	}

	/** retourne le montant des cotisations patronales RAFP en centieme d'euros */
	public String rafpPatronale() {

		// On veut un résultat avec les centièmes d'euros, on n'arrondit pas
		BigDecimal result = calculerSurElementsEtHistoriques(historiquesPaye(),ConstantesDads.CODE_TAUX_RAFP_PATRONALE,ConstantesDads.CODE_RAFP_PATRONALE,false,CHARGE_PATRONALE,false);
		if (result != null)
			return result.setScale(2,BigDecimal.ROUND_HALF_UP).toString();

		return "0.00";
	}

	/** retourne le montant des cotisations salariales RAFP en centieme d'euros */
	public String rafpSalariale() {

		// On veut un résultat avec les centièmes d'euros, on n'arrondit pas
		BigDecimal result = calculerSurElementsEtHistoriques(historiquesPaye(),ConstantesDads.CODE_TAUX_RAFP_SALARIALE,ConstantesDads.CODE_RAFP_SALARIALE,false,CHARGE_SALARIALE,false);

		if (result != null)
			return result.setScale(2,BigDecimal.ROUND_HALF_UP).toString();

		return "0.00";

	}


	public boolean cotiseIrcantec() {
		return contrat().statut().regimeCotisation() != null && contrat().statut().regimeCotisation().pregCodeIrcantecTrA() != null;
	}
	//	08/11/06 : ajoutée pour déterminer les agents qui cotisent à l'ircantec tranche A
	public boolean cotiseIrcantecTrancheA() {
		return contrat().statut().regimeCotisation() != null && contrat().statut().regimeCotisation().pregCodeIrcantecTrA() != null &&
		contrat().statut().regimeCotisation().pregCodeIrcantecTrA().indexOf("2") == -1;
	}
	/** Calcule le montant de la base brut pour l'Ircantec
	 * 08/11/06 : la base brute ircantec est la base tranche A + la base tranche B => base securite sociale - SFT */
	public String baseBrutIrcantec() {

		try {
			if (contrat().correspondSommeIsolee(anneeDADS)) {	// impératif - correction Mulhouse 08/10/07
				return "0";
			}
			// 10/10/07 - corrigé suite pb Mulhouse pour les formateurs occasionnels
			// l'assiette de cotisations URSAFF n'est pas celle de l'ircantec

			BigDecimal total = calculBaseTotaleIrcantec();			
			total = total.setScale(2,BigDecimal.ROUND_HALF_UP);

			return total.toString();

		} catch (Exception e) {e.printStackTrace();return "0";}
	}
	/** retourne l'assiette globale de la cotisation Ircantec Tranche A
	 * @return
	 */
	public BigDecimal basePlafonneeIrcantec() {

		if (contrat().correspondSommeIsolee(anneeDADS)) {	// impératif - correction Mulhouse 08/10/07
			return new BigDecimal(0);
		} else {
			return calculerIrcantecTrancheA();
		}
	}
	/** retourne le type de somme isolee
	 **/
	public String typeSommeIsolee() {
		if (contrat().statut().regimeCotisation().estIrcantec()) {
			return new String("04");
		} else {
			return new String("02");
		}
	}
	/** retourne l'annee de rattachement d'une somme isolee sur deux caract&egrave;res */
	public String anneeRattachement() {
		String annee = "";

		// prendre la date de fin du contrat pour récupérer l'année
		GregorianCalendar date = new GregorianCalendar();
		if (dateFin() == null) {	// CDSI
			return new Integer(anneeDADS).toString();
		}
		date.setTime(dateFin());
		if (date.get(Calendar.YEAR) > anneeDADS) {
			annee = new Integer(anneeDADS).toString();
		}
		else {
			annee = new Integer(date.get(Calendar.YEAR)).toString();
		}
		return annee;
	}
	/** retourne le montant brut d'une somme isolee 
	 * 08/11/06 : la base brute ircantec est la base tranche A + la base tranche B => base securite sociale - SFT */
	public BigDecimal baseIsoleeBrute() {
		// 10/10/07 - corrigé suite pb Mulhouse pour les formateurs occasionnels
		// l'assiette de cotisations URSAFF n'est pas celle de l'ircantec
		BigDecimal total = calculBaseTotaleIrcantec();
		total = total.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);

		return total;
	}
	/** Calcule le montant total du plafond de secuerite sociale pour cette periode
	 */
	public String basePlafonnee() {	
		if (contrat().statut().regimeCotisation() != null && contrat().statut().regimeCotisation().pregCodeBasePlafonnee() != null) {
			String codeCotisation = contrat().statut().regimeCotisation().pregCodeBasePlafonnee();
			int typeCotisation = CHARGE_SALARIALE;
			String codeRappel = ConstantesDads.CODE_VIEILLESSE_PLAFONNEE_SALARIALE;
			if (codeCotisation.endsWith("P")) {
				typeCotisation = CHARGE_PATRONALE;
				codeRappel = ConstantesDads.CODE_VIEILLESSE_PLAFONNEE_PATRONALE;
			} else if (contrat.individu().paieSecu() == false) {	// 110208 - remplacement de estImposableEnFrance
				codeCotisation = codeCotisation.substring(0,codeCotisation.length() - 1) + "P";
				typeCotisation = CHARGE_PATRONALE;
				codeRappel = ConstantesDads.CODE_VIEILLESSE_PLAFONNEE_PATRONALE;
			}
			BigDecimal montant = calculerSurElements(codeCotisation,codeRappel,true,typeCotisation);
			montant = montant.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
			BigDecimal baseBrute = new BigDecimal(baseBrutSecu());
			if (montant.doubleValue() > baseBrute.doubleValue()) {	// pour limiter les erreurs, on vérifie qu'on ne dépasse pas la base brute
				montant = baseBrute;
			}
			EOEditingContext editingContext = ((EOPayeHisto)historiquesPaye().objectAtIndex(0)).editingContext();
			// rechercher le plafond mensuel de décembre
			EOPayeCode code = EOPayeCode.rechercherCode(editingContext,ConstantesDads.CODE_PLAFOND_SS_MENSUEL);
			int moisCode = anneeDADS * 100 + 12;
			EOPayeParam paramPlafond = EOPayeParam.parametrePourMois(contrat().editingContext(), code, new Integer(moisCode));
			if (paramPlafond != null) {
				if (montant.doubleValue() > (paramPlafond.pparMontant().doubleValue() * 12))	{
					// base plafonnée supérieure à 12 fois le plafond SS mensuel
					return (new BigDecimal(paramPlafond.pparMontant().doubleValue() * 12).setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP)).toString();
				} else
					return montant.toString();
			} else {
				return montant.toString();
			}
		} else {
			return "0.00";
		}
	}

	/** retourne le montant plafonnee d'une somme isolee 
	 */
	public BigDecimal baseIsoleePlafonnee() {
		BigDecimal montant = calculerIrcantecTrancheA();
		BigDecimal baseBrute = baseIsoleeBrute();
		if (montant.doubleValue() > baseBrute.doubleValue()) {
			montant = baseBrute;
		}
		return montant;
	}
	/** retourne le montant de la base exoneree de charges URSSAF
	 */
	public String baseExoneree() {
		String code = contrat().statut().regimeCotisation().pregExoUrssaf();
		if (code.equals(Constantes.CODE_EXONERATION_APPRENTI)) {
			BigDecimal result = calculBaseBruteImposable();
			result = result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
			return result.toString();
		} else if (code.equals(Constantes.CODE_EXONERATION_CEC) || code.equals(Constantes.CODE_EXONERATION_CES) || 
				code.equals(Constantes.CODE_EXONERATION_CA) || code.equals(Constantes.CODE_EXONERATION_CAE)) {
			BigDecimal result = calculBaseBruteImposable();
			result = result.subtract(calculAssietteChargesPatronales());
			result = result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
			return result.toString();
		} else {
			return "0.00";
		}
	}
	/** retourne le montant de la base plafonnee exoneree
	 **/
	public String basePlafonneeExoneree() {
		String code = contrat().statut().regimeCotisation().pregExoUrssaf();
		if (code.equals(Constantes.CODE_EXONERATION_APPRENTI)) {
			BigDecimal result = calculBaseBruteImposable();
			result = result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
			return result.toString();
		} else if (code.equals(Constantes.CODE_EXONERATION_CEC) || code.equals(Constantes.CODE_EXONERATION_CES) || 
				code.equals(Constantes.CODE_EXONERATION_CA) || code.equals(Constantes.CODE_EXONERATION_CAE)) {
			BigDecimal result = calculBaseBruteImposable();
			result = result.subtract(calculAssietteChargesPatronalesPlafonnees());
			result = result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
			return result.toString();
		} else { 
			return "0.00";
		}
	}
	/** retourne le montant de la base plafonnee exoneree **/
	public String montantExonerationUrssaf() {

		return "0.00";

	}
	//	rubriques IRCANTEC et CNRCACL
	/** retourne le nombre d'heures hebdomadaire travaillees * 100
	 * 08/11/06 : uniquement pour les agents relevant de la fonction publique hospitali&egrave;re ou territoriale
	 **/
	public Integer dureeHebdomadaireAgent() {
		if (contrat().statut().regimeCotisation().estFonctionPubliqueHospitaliere() || contrat().statut().regimeCotisation().estFonctionPubliqueTerritoriale()) {
			double nbHeuresHebdoCollectivite = nbHeuresLegal(true);
			double nbHeuresMensuel = nbHeuresLegal(false);
			if (nbHeuresMensuel != 0 && nbHeuresPayees() != null) {
				double nbHeures = ((nbHeuresPayees().doubleValue() / historiquesPaye().count()) / nbHeuresMensuel) * nbHeuresHebdoCollectivite * 100;
				return (new Integer((int)nbHeures));
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	/** retourne le nombre d'heures hebdomadaire du poste * 100 pour un fonctionnaire detache CT, null sinon.
	 * (Ne g&egrave;re pas le cas du fonctionnaire CT sous Ircantec). La duree hebdomadaire doit &ecirc;tre fournie
	 * dans le contrat
	 */
	public Integer dureeHebdomadairePoste() {
		if (contrat().statut().regimeCotisation().estCNRACL()) {
			if (contrat().nbHeuresContrat() != null) {
				double nbHeures = contrat().nbHeuresContrat().doubleValue();
				if (nbHeures != 0) {
					return new Integer((int)(nbHeures * 100));
				} else {
					return dureeHebdomadaireCollectivite();
				}
			} else {
				return dureeHebdomadaireCollectivite();
			}
		} else {
			return null;
		}
	}
	/** retourne le nombre d'heures hebdomadaire de la collectivite * 100
	 * Non gere */
	public Integer dureeHebdomadaireCollectivite() {
		int nbHeures = (int)(nbHeuresLegal(true) * 100);
		if (nbHeures > 0) {
			return new Integer(nbHeures);
		} else {
			return null;
		}
	}


	// INDEMNITE RESIDENCE ET SFT

	public String codeIndemniteResidence() {

		BigDecimal result = calculerSurElementsEtHistoriques(historiquesPaye(),"REMUNIRS",null, false,REMUNERATION,false);
		if (result == null || result.floatValue() == 0.00)
			return null;

		return "101";

	}

	public String montantIndemniteResidence() {

		if (codeIndemniteResidence() == null)
			return null;

		BigDecimal result = calculerSurElementsEtHistoriques(historiquesPaye(),"REMUNIRS",null, false,REMUNERATION,false);
		return result.toString();

	}

	public String codeIndemniteSFT() {

		BigDecimal result = calculerSurElementsEtHistoriques(historiquesPaye(),"REMUNSFT",null, false,REMUNERATION,false);
		if (result == null || result.floatValue() == 0.00)
			return null;

		return "102";

	}

	public String montantIndemniteSFT() {

		if (codeIndemniteSFT() == null)
			return null;

		BigDecimal result = calculerSurElementsEtHistoriques(historiquesPaye(),"REMUNSFT",null, false,REMUNERATION,false);
		return result.toString();

	}

	// PENSION CIVILE

	public String assiettePensionCivile() {
		BigDecimal result = calculerSurElementsEtHistoriques(historiquesPaye(),ConstantesDads.CODE_TAUX_PENSION_CIVILE_PATRONALE,ConstantesDads.CODE_PENSION_CIVILE_PATRONALE,true,CHARGE_PATRONALE,false);
		return (result.setScale(2,BigDecimal.ROUND_HALF_UP)).toString();
	}
	public String pensionCivileSalariale() {
		BigDecimal result = calculerSurElementsEtHistoriques(historiquesPaye(),ConstantesDads.CODE_TAUX_PENSION_CIVILE_SALARIALE,ConstantesDads.CODE_PENSION_CIVILE_SALARIALE,false,CHARGE_SALARIALE,false);
		return (result.setScale(2,BigDecimal.ROUND_HALF_UP)).toString();
	}
	public String pensionCivilePatronale() {
		BigDecimal result = calculerSurElementsEtHistoriques(historiquesPaye(),ConstantesDads.CODE_TAUX_PENSION_CIVILE_PATRONALE,ConstantesDads.CODE_PENSION_CIVILE_PATRONALE,false,CHARGE_PATRONALE,false);
		return (result.setScale(2,BigDecimal.ROUND_HALF_UP)).toString();
	}
	public String contributionATIPatronale() {
		BigDecimal result = calculerSurElementsEtHistoriques(historiquesPaye(),ConstantesDads.CODE_TAUX_ATI,ConstantesDads.CODE_ATI,false,CHARGE_PATRONALE,false);
		return (result.setScale(2,BigDecimal.ROUND_HALF_UP)).toString();
	}



	// DADS V08R09 - il est obligatoire de fournir un taux non nul
	public String tauxPensionCivileSalariale() throws Exception {

		EOPayeCode code = EOPayeCode.rechercherCode(contrat().editingContext(),ConstantesDads.CODE_TAUX_PENSION_CIVILE_SALARIALE);
		if (code == null)
			throw new Exception("Pas de code défini pour le taux de pension civile salariale. Code attendu :" + ConstantesDads.CODE_TAUX_PENSION_CIVILE_SALARIALE);

		long mois = ((long)anneeDADS * 100) + 12;
		EOPayeParam parametre = EOPayeParam.parametrePourMois(contrat().editingContext(), code, new Long(mois));

		return (parametre.pparTaux().setScale(2, BigDecimal.ROUND_HALF_UP)).toString();
	}

	public String tauxPensionCivilePatronale() {

		EOPayeCode code = EOPayeCode.rechercherCode(contrat().editingContext(),ConstantesDads.CODE_TAUX_PENSION_CIVILE_PATRONALE);
		if (code == null)
			return "0.00";

		long mois = ((long)anneeDADS * 100) + 12;
		EOPayeParam parametre = EOPayeParam.parametrePourMois(contrat().editingContext(), code, new Long(mois));

		return (parametre.pparTaux().setScale(2, BigDecimal.ROUND_HALF_UP)).toString();

	}

	public String tauxContributionATI() {

		EOPayeCode code = EOPayeCode.rechercherCode(contrat().editingContext(),ConstantesDads.CODE_TAUX_ATI);
		if (code == null)
			return "0.00";

		long mois = ((long)anneeDADS * 100) + 12;
		EOPayeParam parametre = EOPayeParam.parametrePourMois(contrat().editingContext(), code, new Long(mois));
		double value = parametre.pparTaux().doubleValue();
		boolean shouldAddZero = (value < 1);
		BigDecimal result = new BigDecimal(value);
		result.setScale(0,BigDecimal.ROUND_HALF_UP);
		if (shouldAddZero) {
			return "0" + result.toString();
		} else {
			return result.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
		}
	}

	public BigDecimal montantSurcotation() {
		// Pas utilisé pour l'instant
		return new BigDecimal(0);
	}
	/** retourne une string indiquant si l'agent a eu plusieurs emplois chez ce m&ecirc;me employeur pendant la periode */
	public String emploiMultipleDansPeriode() {
		// pour les agents cotisant à la CNRACL et travaillant à temps partiel, il ne peut y avoir plusieurs employeurs (voir Norme)
		if (contrat().statut().regimeCotisation().estCNRACL() && typeContrat().equals(ConstantesDads.TEMPS_PARTIEL)) {
			return ConstantesDads.PAS_EMPLOIS_MULTIPLES; 
		}
		if (aEmploisMultiples) {
			return ConstantesDads.EMPLOIS_MULTIPLES;
		} else {
			return ConstantesDads.PAS_EMPLOIS_MULTIPLES;
		}
	}

	public String codeCPAPourTempsPartiel() {
		if (contrat().statut().regimeCotisation().estCNRACL() && typeContrat().equals(ConstantesDads.TEMPS_PARTIEL)) {
			return ConstantesDads.CODE_CPA_TEMPS_PARTIEL_CNRACL;
		} else {
			return null;
		}
	}
	// Pour CARCICAS et CAPRICAS
	/** Dure de l'unite de travail pour les caisses de retraite complementaires artistes/intermittents
	 * V08R06 - pour les artistes/intermittents cadres, c'est forcement une duree en jours
	 **/
	public String codeUniteTravailIRC() {
		String codeOrganisme = contrat().statut().regimeCotisation().pregComplementaire();
		if (codeOrganisme.equals(Constantes.CARCICAS)) {
			return ConstantesDads.DUREE_JOUR_ARTISTE; 
		} else if (codeOrganisme.equals(Constantes.CAPRICAS)) {
			if (contrat().nbJoursContrat() != null && contrat().nbJoursContrat().intValue() > 0) {
				return ConstantesDads.DUREE_JOUR_ARTISTE;   
			} else if (contrat().nbHeuresContrat()  != null && contrat().nbHeuresContrat().doubleValue() > 0) {
				return ConstantesDads.DUREE_HORAIRE_ARTISTE;
			} else {
				return ConstantesDads.DUREE_MOIS_ARTISTE;
			}
		} else {
			return null;
		}
	}
	/** Dure de travail pour les caisses de retraite complementaires (Capricas, Carcicas)
	 * V08R06 - retournee en centi&egrave;mes d'heures, de jours, de mois (Capricas), 
	 * en centi&egrave;mes de jours (Carcicas)
	 **/
	public Number dureeTotaleTravailIRC() throws Exception {
		String codeOrganisme = contrat().statut().regimeCotisation().pregComplementaire();
		if (codeOrganisme.equals(Constantes.CAPRICAS) || codeOrganisme.equals(Constantes.CARCICAS)) {
			if (contrat().nbJoursContrat() != null && contrat().nbJoursContrat().intValue() > 0) {
				return new Double(contrat().nbJoursContrat().doubleValue() * 100);   
			} else if (contrat().nbHeuresContrat()  != null && contrat().nbHeuresContrat().doubleValue() > 0) {
				if (codeOrganisme.equals(Constantes.CAPRICAS)) {
					return  new Double(contrat().nbHeuresContrat().doubleValue() * 100);
				} else {	// Carcicas
					// Pour les cadres, on ne peut exprimer qu'une durée en jours
					int moisCalcul = new Integer(debutPeriode().substring(2)).intValue();
					long mois = ((long)anneeDADS * 100) + moisCalcul;
					// rechercher le paramètre du nombre d'heures par jour
					EOPayeCode code = EOPayeCode.rechercherCode(contrat().editingContext(),ConstantesDads.CODE_NB_HEURES_HEBDO);
					if (code == null) {
						throw new Exception("Le code durée horaire hedbomadaire : " + ConstantesDads.CODE_NB_HEURES_HEBDO + " , n'est pas defini");
					}
					EOPayeParam parametre = EOPayeParam.parametrePourMois(contrat().editingContext(), code, new Long(mois));
					// On calcule la durée journalière et et on divise le nombre d'heures du contrat par ce taux
					double dureeJournaliere = parametre.pparMontant().doubleValue() / 5;
					return  new Double((contrat().nbHeuresContrat().doubleValue() / dureeJournaliere) * 100);
				}
			} else {
				return new Integer(nbMoisDansPeriode() * 100);
			}
		} else {
			return null;
		}
	}
	// Pour la CNRACL
	/** Montant des retenues salariales de l'agent hors Nbi pour la CNRACL */
	public BigDecimal montantRetenues() {
		if (contrat().statut().regimeCotisation().estCNRACL()) {
			// Calculer la rémunération en NBI sans arrondir
			BigDecimal remun = calculerSurElementsEtHistoriques(historiquesPaye(),ConstantesDads.REMUN_NBI, ConstantesDads.REMUN_NBI, false, REMUNERATION,false);
			double remunNbi = remun.doubleValue();
			double baseBruteSecu = calculBaseBruteSecu().doubleValue();
			double baseHorsNbi = baseBruteSecu - remunNbi;
			double chargesSalariales = (montantRetenuesSalariales().doubleValue() * baseHorsNbi) / baseBruteSecu;
			return new BigDecimal(chargesSalariales).setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
		} else {
			return null;
		}
	}
	/** Montant des retenues patronales de l'agent hors Nbi pour la CNRACL */
	public BigDecimal montantContributions() {
		if (contrat().statut().regimeCotisation().estCNRACL()) {
			// Calculer la rémunération en NBI sans arrondir
			BigDecimal remun = calculerSurElementsEtHistoriques(historiquesPaye(),ConstantesDads.REMUN_NBI, ConstantesDads.REMUN_NBI, false, REMUNERATION,false);
			double remunNbi = remun.doubleValue();
			double baseBruteSecu = calculBaseBruteSecu().doubleValue();
			double baseHorsNbi = baseBruteSecu - remunNbi;
			double chargesPatronales = (montantRetenuesPatronales().doubleValue() * baseHorsNbi) / baseBruteSecu;
			return new BigDecimal(chargesPatronales).setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
		} else {
			return null;
		}
	}
	/** retourne le montantSFT pour un agent hors Nbi pour la CNRACL
	 * 07/12/07 - modifiee pour verifier que le numero CNRACL de la structure est fourni */
	public BigDecimal montantSFT() throws Exception {
		if (contrat().statut().regimeCotisation() != null && contrat().statut().regimeCotisation().estCNRACL() &&
				nbHeuresPayees() != null) {
			// ne fournir le sft que pour les agents travaillant plus de 28 heures
			if (nbHeuresPayees().doubleValue() < 28) {
				return null;
			} else {
				// le sft étant calculé annuellement, on ne le fournit que pour la dernière période
				if (estDernierePeriodePourCNRACL()) {
					BigDecimal sft = calculerSFT(true);
					if (sft != null && sft.doubleValue() != 0) {
						if (contrat().structure().numCNRACL() == null) {
							throw new Exception("Pour pouvoir declarer le SFT, il faut donner un numero CNRACL à la structure " + contrat.structure().llStructure());
						} else {
							return sft;
						}
					} else {
						return null;
					}
				} else {
					return null;
				}
			}	
		} else {
			return null;
		}
	}
	/** Nature de la cotisation : seules les Nbi sont gerees pour la CNRACL */
	public String natureCotisation() {
		if (contrat().statut().regimeCotisation().estCNRACL()) {
			if (rechercherElements(historiquesPaye(),ConstantesDads.REMUN_NBI,ConstantesDads.REMUN_NBI).count() > 0) {
				return ConstantesDads.NATURE_COTISATION_NBI;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	/** pour la CNRACL seulement */
	public String debutPeriodeNBI() {
		if (contrat().statut().regimeCotisation().estCNRACL()) {
			NSArray elements = rechercherElements(historiquesPaye(),ConstantesDads.REMUN_NBI,ConstantesDads.REMUN_NBI);
			if (elements.count() == 0) {
				return null;
			} else {
				elements = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements, new NSArray(EOSortOrdering.sortOrderingWithKey("pelmMoisCode", EOSortOrdering.CompareAscending)));
				String moisCode = ((EOPayeElement)elements.objectAtIndex(0)).pelmMoisCode().toString();
				String debut = "01" + "/" + moisCode.substring(4,6) + "/" + moisCode.substring(0,4);
				// Si la date de début est antérieure au début de la période d'activité, retourner la date début de
				// la période d'activité
				if (DateCtrl.isBefore(DateCtrl.stringToDate(debut,"%d/%m/%Y"), dateDebut())) {
					return debutPeriode();
				} else {
					return debut.replaceAll("/", "");
				}

			}
		} else {
			return null;
		}
	}

	public String dateEffetGradeEchelon() {

		//return DateCtrl.dateToString(contrat().dDebContratTrav(), "%d%m%Y");
		return "0101"+String.valueOf(anneeDADS);

	}

	public String dateAttributionCodeEmploi() {

		return "0101"+String.valueOf(anneeDADS);

	}


	/** pour la CNRACL seulement */
	public String finPeriodeNBI() {
		if (contrat().statut().regimeCotisation().estCNRACL()) {
			NSArray elements = rechercherElements(historiquesPaye(),ConstantesDads.REMUN_NBI,ConstantesDads.REMUN_NBI);
			if (elements.count() == 0) {
				return null;
			} else {
				elements = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements, new NSArray(EOSortOrdering.sortOrderingWithKey("pelmMoisCode", EOSortOrdering.CompareDescending)));
				String moisCode = ((EOPayeElement)elements.objectAtIndex(0)).pelmMoisCode().toString();
				String debut = "01" + "/" + moisCode.substring(4,6) + "/" + moisCode.substring(0,4);
				String fin = "" + DateCtrl.dernierJourDuMois(DateCtrl.stringToDate(debut,"%d/%m/%Y")) + "/" + moisCode.substring(4,6) + "/" + moisCode.substring(0,4);
				if (DateCtrl.isBefore(dateFin(),DateCtrl.stringToDate(fin, "%d/%m/%Y"))) {
					return finPeriode();
				} else {
					return fin.replaceAll("/", "");
				}
			}
		} else {
			return null;
		}
	}
	public Number nombrePointsNbi() throws Exception {
		if (contrat().statut().regimeCotisation().estCNRACL()) {
			// Rechercher dans les param perso comportant des point de nbi
			Integer debutPeriode = new Integer((anneeDADS * 100) + 1), finPeriode = new Integer((anneeDADS * 100) + 12);
			NSArray paramsPerso = EOPayeParamPerso.rechercherParamPersoPourIndividuCodeEtPeriode(contrat.editingContext(), contrat.individu(), ConstantesDads.NB_POINTS_NBI,debutPeriode,finPeriode);
			if (paramsPerso.count() == 0) {
				return null;
			}
			// On prend la première valeur trouvée
			EOPayeParamPerso param = (EOPayeParamPerso)paramsPerso.objectAtIndex(0);
			int nbPoints = new Integer(param.pparValeur()).intValue() * 100;
			if (nbPoints < 0 || nbPoints > ConstantesDads.NB_POINTS_NBI_MAX * 100) {
				throw new Exception("Le nombre de points de Nbi ne peut être supérieur à " + ConstantesDads.NB_POINTS_NBI_MAX);
			}
			return new Integer(nbPoints);
		} else {
			return null;
		}
	}
	/** pour la CNRACL seulement */
	public Number montantCotisationSalarialeNbi() {
		if (contrat().statut().regimeCotisation().estCNRACL()) {
			// Calculer la rémunération en NBI sans arrondir
			BigDecimal remun = calculerSurElementsEtHistoriques(historiquesPaye(),ConstantesDads.REMUN_NBI, ConstantesDads.REMUN_NBI, false, REMUNERATION,false);
			double remunNbi = remun.doubleValue();
			if (remunNbi != 0) {
				double baseBruteSecu = calculBaseBruteSecu().doubleValue();
				double chargesSalariales = (montantRetenuesSalariales().doubleValue() * remunNbi) / baseBruteSecu;
				return new BigDecimal(chargesSalariales).setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	/** pour la CNRACL seulement */
	public Number montantCotisationPatronaleNbi() {
		if (contrat().statut().regimeCotisation().estCNRACL()) {
			// Calculer la rémunération en NBI sans arrondir
			BigDecimal remun = calculerSurElementsEtHistoriques(historiquesPaye(),ConstantesDads.REMUN_NBI, ConstantesDads.REMUN_NBI, false, REMUNERATION,false);
			double remunNbi = remun.doubleValue();
			if (remunNbi != 0) {
				double baseBruteSecu = calculBaseBruteSecu().doubleValue();
				double chargesPatronales = (montantRetenuesPatronales().doubleValue() * remunNbi) / baseBruteSecu;
				return new BigDecimal(chargesPatronales).setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	/** retourne le montant SFT pour les titulaires des Collectivites Territoriales travaillant moins de 28 heures */
	public BigDecimal montantSFTPourFNC() {
		if (contrat().statut().regimeCotisation() != null && nbHeuresPayees() != null) {
			// ne fournir le sft que pour les agents travaillant plus de 28 heures
			if (contrat().statut().regimeCotisation().estCNRACL() && nbHeuresPayees().doubleValue() >= 28) {
				return null;
			}
			String codeSFT = contrat().statut().regimeCotisation().pregCodeSFT();
			if (codeSFT != null) {
				return calculerSurElements(codeSFT,codeSFT,false,REMUNERATION);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	/**
	 * retourne le montant de la TVA pour les droits d'auteur
	 */
	public BigDecimal montantTVA() {
		return new BigDecimal(0);
	}
	/** retourne la date de cl&ocirc;ture de l'exercice ou null si 31/12 de l'annee courante */
	public String dateClotureExercice() {
		return null;
	}
	/**
	 * retourne le montant des honoraires pour les droits d'auteur */
	public BigDecimal montantHonoraires() {
		return baseBrutTotale();
	}
	public int nbMoisDansPeriode() {
		int debut = new Integer(debutPeriode().substring(2,4)).intValue();
		int fin = new Integer(finPeriode().substring(2,4)).intValue();
		return (fin - debut + 1);
	}

	//	07/02/06 - modifier pour le cas où la dernière période ne comporte aucune information pour la rémunération
	//	09/02/06 - remodifier pour travailler sur les périodes en cours
	// 04/12/09 - remodifier pour supprimer le test sur lastObject et retrier les périodes en cours
	public boolean estDernierePeriode() {
		/*	if (agent.periodesEnCours().lastObject() == this) {
			return true;
		}*/
		//System.out.println("periode courante : " + debutPeriode() + " - " + finPeriode());
		// Trier les périodes par ordre pour être sûr qu'elles soient en bon ordre
		NSMutableArray periodesOrdonnees = new NSMutableArray(agent.periodesEnCours());
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("dateFinPourTri", EOSortOrdering.CompareAscending));
		EOSortOrdering.sortArrayUsingKeyOrderArray(periodesOrdonnees, sorts);

		PeriodeDadsActive dernierePeriode = null;
		// 26/01/2010 - dans le cas où toutes les périodes sont à déclarer, prendre la dernière période déclarée
		boolean aDeclarer = EODadsAgents.dadsObligatoirePourIndividu(contrat.editingContext(), contrat.individu(),anneeDADS);
		if (aDeclarer) {
			return (periodesOrdonnees.lastObject() == this);
		}
		// trouver la dernière période générée dans la DADS en parcourant les périodes en ordre décroissant
		java.util.Enumeration e  = periodesOrdonnees.reverseObjectEnumerator();
		//java.util.Enumeration e = agent.periodesEnCours().objectEnumerator();
		while (e.hasMoreElements()) {
			PeriodeDadsActive periode = (PeriodeDadsActive)e.nextElement();
			BigDecimal baseBrut = calculBaseBruteImposable();
			if (baseBrut != null && baseBrut.doubleValue() != 0) {
				dernierePeriode = periode;
				break;
			}
		}
		// 22/01/2010 - cas des bulletins de régularisation où il y a une seule période et pas de montant brut 
		if (dernierePeriode == null && periodesOrdonnees.count() == 1) {
			return true;
		}
		//System.out.println("derniere periode : " + dernierePeriode.debutPeriode() + " - " + dernierePeriode.finPeriode());
		return dernierePeriode == this;
	}

	//	méthodes privées
	private boolean contratAnterieurAnnee(EOPayeContrat contrat,int anneeDADS) {
		NSTimestamp debut = contrat.dDebContratTrav();
		GregorianCalendar date = new GregorianCalendar();
		date.setTime(debut);
		int annee = date.get(Calendar.YEAR);
		// vérifier si l'année est antérieure à celle de l'année pour laquelle on génère la DADS
		return annee < anneeDADS;
	}
	private boolean contratPosterieurAnnee(EOPayeContrat contrat,int anneeDADS) {
		NSTimestamp fin = contrat.dFinContratTrav();
		if (fin == null) {
			return true;
		}
		GregorianCalendar date = new GregorianCalendar();
		date.setTime(fin);
		int annee = date.get(Calendar.YEAR);
		// vérifier si l'année est postérieure celle de l'année pour laquelle on génère la DADS
		return annee > anneeDADS;
	}
	//	Les dates sont fournies sur 8 caractères
	private String formaterDate(NSTimestamp aDate,boolean estDebutContrat) {
		String anneePourDADS = new Integer(anneeDADS).toString();
		if (aDate != null) {		// contrat de CDD
			GregorianCalendar date = new GregorianCalendar();
			date.setTime(aDate);
			int annee = date.get(Calendar.YEAR);	
			// vérifier si l'année est la même que celle de l'année pour laquelle on génère la DADS
			int anneePaye = ((EOPayeHisto)historiquesPaye().objectAtIndex(0)).payeAnnee().intValue();
			if (anneePaye == annee) {
				String mois = new Integer(date.get(Calendar.MONTH) + 1).toString();	// les mois commencent à zéro en java
				String jour = new Integer(date.get(Calendar.DAY_OF_MONTH)).toString();
				if (mois.length() == 1) {
					mois = "0" + mois;
				}
				if (jour.length() == 1) {
					jour = "0" + jour;
				}
				return jour + mois + anneePourDADS;
			} else if (estDebutContrat) {
				return "0101" + anneePourDADS;
			} else {	// fin de contrat
				if (anneePaye < annee) {
					// contrat qui se poursuit l'année suivante
					return "3112" + anneePourDADS;
				} else {
					// contrat terminé, somme isolée
					return "0101" + anneePourDADS;
				}
			}

		} else {
			return "3112" + anneePourDADS;	// fin décembre
		}
	}
	private BigDecimal calculBaseBruteImposable() {
		BigDecimal total = new BigDecimal(0);
		Enumeration e = historiquesPaye().objectEnumerator();
		while (e.hasMoreElements()) {
			total = total.add(((EOPayeHisto)e.nextElement()).payeBrut());
		}
		return total;	
	}
	private BigDecimal calculBaseBruteSecu() {
		// 20/02/07 on rechercher dans les éléments, la première rubrique de cotisation
		BigDecimal total = new BigDecimal(0.0);
		for (Enumeration<EOPayeHisto> e = historiquesPaye().objectEnumerator();e.hasMoreElements();) {
			EOPayeHisto historique =(EOPayeHisto)e.nextElement();
			// on travaille dans un nouvel editing context pour éviter la saturation mémoire
			EOEditingContext editingContext = new EOEditingContext();
			historique = (EOPayeHisto)editingContext.faultForGlobalID(historique.editingContext().globalIDForObject(historique), editingContext);

			NSArray elements = EOPayeElement.findForHistorique(historique.editingContext(), historique);
			elements = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements,
					new NSArray(EOSortOrdering.sortOrderingWithKey("rubrique.prubClassement",EOSortOrdering.CompareAscending)));

			for (Enumeration<EOPayeElement> e1 = elements.objectEnumerator();e1.hasMoreElements();) {
				EOPayeElement element = e1.nextElement();
				if (element.rubrique().rentreDansCotisation()) {
					int classement = new Integer(element.rubrique().prubClassement()).intValue();
					if (classement < 30550 && element.rubrique().categorie().categorieLibelle().equals("RAFP") == false) {
						total = total.add(element.pelmAssiette());
						break;
					}
				}
			}
		}
		return total;
	}
	private BigDecimal calculAssietteChargesPatronales() {
		BigDecimal total = new BigDecimal(0.0);
		
		for (Enumeration<EOPayeHisto> e = historiquesPaye().objectEnumerator();e.hasMoreElements();) {
			EOPayeHisto historique =(EOPayeHisto)e.nextElement();
			if (historique.payePatron() != null && historique.payePatron().doubleValue() != 0) { // charges patronales nulles quand pas de paiement ircantec
				// rechercher l'élément correspondant à l'assiette maladie et prendre cette valeur

				NSArray elements = EOPayeElement.findForHistorique(historique.editingContext(), historique);
				Enumeration e1 = elements.objectEnumerator();
				while (e1.hasMoreElements()) {
					EOPayeElement element = (EOPayeElement)e1.nextElement();
					if (element.estUneChargePatronale() && element.code().pcodCode().startsWith("TXMA")) { // maladie patronal
						total = total.add(element.pelmAssiette());
						break;
					}
				}
			}
		}
		return total;
	}
	private BigDecimal calculAssietteChargesPatronalesPlafonnees() {
		BigDecimal total = new BigDecimal(0.0);
		Enumeration e = historiquesPaye().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeHisto historique =(EOPayeHisto)e.nextElement();
			if (historique.payePatron() != null && historique.payePatron().doubleValue() != 0) { // charges patronales nulles quand pas de paiement ircantec
				// rechercher l'élément correspondant à l'assiette vieillesse plafonneées et prendre cette valeur

				NSArray elements = EOPayeElement.findForHistorique(historique.editingContext(), historique);
				Enumeration e1 = elements.objectEnumerator();
				while (e1.hasMoreElements()) {
					EOPayeElement element = (EOPayeElement)e1.nextElement();
					if (element.estUneChargePatronale() && element.code().pcodCode().startsWith("TXVP")) {	// vieillesse plafonnée patronale
						total = total.add(element.pelmAssiette());	
						break;
					}
				}
			}
		}
		return total;
	}

	/**
	 * 
	 * Dernier mois du premier trimestre ou le salarie a effectue 120 Heures.
	 * 
	 * 99 Si condition non remplie (Par exemple, moins de 3 mois de travail)
	 * 
	 * @return
	 */
	public String premierePeriodeTrimestrielle_120_H() {

		if (!estDernierePeriode())
			return null;

		conditionPremierPeriodeTrimestrielleRemplie = false;

		NSArray listeMois = EOPayeMois.findForExercice(ec, this.anneeDADS);
		int index = 1;

		BigDecimal cumulHeures = new BigDecimal(0);

		for (int i=0;i<listeMois.count();i++) {

			EOPayeMois mois = (EOPayeMois)listeMois.objectAtIndex(i);

			// On regarde si le cumul horaire trimestriel est superieur à 120H
			if (
					horairesAgent.objectForKey(mois.moisCode().toString()) != null) {
				//					&& ((BigDecimal)horairesAgent.objectForKey(mois.moisCode().toString())).floatValue() > ConstantesDads.DUREE_TRIMESTRIEL_MINIMUM ) {

				cumulHeures = cumulHeures.add((BigDecimal)horairesAgent.objectForKey(mois.moisCode().toString()));

				// On doit avoir au moins 3 mois de fait
				if (index >= 3 && cumulHeures.floatValue() > ConstantesDads.DUREE_TRIMESTRIEL_MINIMUM) {  
					conditionPremierPeriodeTrimestrielleRemplie = true;
					return mois.moisCode().toString().substring(4,6);
				}

				index ++;

			}
			else	// Si on a une rupture de date de contrat, on reinitialise l'index.
				index = 1;


		}

		return ConstantesDads.CODE_DUREE_MINIMUM_ANNUELLE_NON;

	}


	/**
	 * 
	 * Premiere periode mensuelle 60 Heures ou 60 SMIC.
	 * 
	 * @return
	 */
	public String premierePeriodeMensuelle_60_H() {

		if (!estDernierePeriode())
			return null;

		if (conditionPremierPeriodeTrimestrielleRemplie)
			return ConstantesDads.CODE_DUREE_MINIMUM_VALIDEE;

		try {

			// rechercher le montant horaire du smic
			double salaireMinimum = montantSmicPourNbHeures(((EOPayeHisto)historiquesPaye().objectAtIndex(0)).editingContext(), new Double(60));	
			EOSortOrdering.sortArrayUsingKeyOrderArray(historiquesPaye(),
					new NSArray(EOSortOrdering.sortOrderingWithKey(EOPayeHisto.MOIS_KEY+"."+EOPayeMois.MOIS_CODE_KEY,EOSortOrdering.CompareAscending)));
			for (Enumeration<EOPayeHisto> e=historiquesPaye().objectEnumerator();e.hasMoreElements();) {

				EOPayeHisto historique = e.nextElement();

				if (historique.payeBrut().floatValue() > salaireMinimum ||
						historique.payeNbheure().floatValue() > 60 )
					return historique.mois().moisCode().toString().substring(4,6);;

			}

		}
		catch (Exception e) {
			
		}

		return ConstantesDads.CODE_DUREE_MINIMUM_MENSUELLE_NON;

	}


	private String dernierPeriodeAvecValeursMinimum(double nbHeures,double nbHeuresSmic,boolean estMensuel) throws Exception {
		// V08R08 : Modifier pour déterminer le dernier mois pour lequel un certain nombre d'heures minimum ou
		// de smic minimum a été effectué sur une période
		// Ranger les historiques par ordre de date décroissant
		// il faut ordonner tous les historiques par mois pour calculer les cumuls de rémunération par mois/trimestre
		NSMutableArray historiques = new NSMutableArray();
		for (Enumeration<PeriodeDadsActive> e = agent.periodesEnCours().objectEnumerator();e.hasMoreElements();) {
			historiques.addObjectsFromArray(e.nextElement().historiquesPaye());
		}

		// trier sur le mois code en ordre ascendant. On garde le code du dernier mois où la condition est remplie
		EOSortOrdering.sortArrayUsingKeyOrderArray(historiques,
				new NSArray(EOSortOrdering.sortOrderingWithKey(EOPayeHisto.MOIS_KEY+"."+EOPayeMois.MOIS_CODE_KEY,EOSortOrdering.CompareAscending)));

		// rechercher le montant horaire du smic
		double salaireMinimum = montantSmicPourNbHeures(((EOPayeHisto)historiquesPaye().objectAtIndex(0)).editingContext(),nbHeuresSmic);	

		// On calcule le cumul d'heures et de rémunération, et à chaque changement de période, on réinitialise
		Number moisCode = null;
		Number moisCourant = null;
		double nbHeuresTotal = 0;
		double remunTotale = 0;
		for (Enumeration<EOPayeHisto> e1 = historiques.objectEnumerator();e1.hasMoreElements();) {

			EOPayeHisto historique = e1.nextElement();

			if ((moisCourant != null && moisCourant.equals(historique.mois().moisCode())) || (!estMensuel && (historique.mois().numeroDuMois() % 3) != 1)) {
				// même période, cumuler les heures et les rémunérations
				nbHeuresTotal += historique.payeNbheure().doubleValue();
				remunTotale += historique.payeBssmois().doubleValue();
			} else { 
				// changement de période
				// On repart juste avec les heures de l'historique
				nbHeuresTotal = historique.payeNbheure().doubleValue();
				remunTotale = historique.payeBssmois().doubleValue();
				moisCourant = historique.mois().moisCode();
			}

			if (nbHeuresTotal >= nbHeures || remunTotale >= salaireMinimum) {
				moisCode = historique.mois().moisCode();
			}
		}

		if (moisCode != null) {
			conditionHoraireRemplie = true;
			return moisCode.toString().substring(4,6);
		} else {
			return ConstantesDads.CODE_DUREE_MINIMUM_ANNUELLE_NON;
		}
	}
	private double montantSmicPourNbHeures(EOEditingContext editingContext, double nbHeures) throws Exception {
		// rechercher le montant horaire du smic
		EOPayeCode code = EOPayeCode.rechercherCode(editingContext,ConstantesDads.CODE_TAUX_HORAIRE_SMIC);
		if (code == null) {
			throw new Exception("Le code montant horaire du smic : " + ConstantesDads.CODE_TAUX_HORAIRE_SMIC + " , n'est pas defini");
		}
		long mois = ((long)anneeDADS * 100) + 12;
		EOPayeParam parametre = EOPayeParam.parametrePourMois(contrat().editingContext(), code, new Long(mois));
		double taux = parametre.pparMontant().doubleValue();
		return taux * nbHeures;	
	}
	private BigDecimal calculerSurElements(String code,String codeRappel,boolean estAssiette,int typeMontant) {
		return calculerSurElementsEtHistoriques(historiquesPaye(),code,codeRappel,estAssiette,typeMontant);
	}
	private BigDecimal calculerSurElementsEtHistoriques(NSArray historiques,String code,String codeRappel,boolean estAssiette,int typeMontant) {
		return calculerSurElementsEtHistoriques(historiques, code, codeRappel, estAssiette, typeMontant, true);
	}
	private BigDecimal calculerSurElementsEtHistoriques(NSArray historiques,String code,String codeRappel,boolean estAssiette,int typeMontant,boolean arrondir) {
		NSArray elements = rechercherElements(historiques, code, codeRappel);
		BigDecimal result = new BigDecimal(0);

		for (Enumeration<EOPayeElement> e = elements.objectEnumerator();e.hasMoreElements();) {

			EOPayeElement element = (EOPayeElement)e.nextElement();
			if (estAssiette) {
				if ((typeMontant == REMUNERATION && element.estUneRemuneration()) ||
						(typeMontant == CHARGE_SALARIALE && element.estUneDeduction()) ||
						(typeMontant == CHARGE_PATRONALE && element.estUneChargePatronale())) {
					result = result.add(element.pelmAssiette());
				}
			} else {
				if (typeMontant == REMUNERATION) {
					result = result.add(element.pelmApayer());
				} else if (typeMontant == CHARGE_SALARIALE) {
					result = result.add(element.pelmAdeduire());
				} else
					result = result.add(element.pelmPatron());

			}
		}
		if (arrondir)
			return result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
		else
			return result;

	}
	/*private BigDecimal calculerSurElementsPourCategorie(String categorie,boolean estAssiette,int typeMontant) {
		NSMutableArray parametres = new NSMutableArray(categorie);
		String qualifString = "rubrique.categorie.categorieLibelle caseInsensitiveLike %@  AND (";
		parametres.addObjectsFromArray(historiquesPaye());

		for (int i = 0; i < historiquesPaye().count() ; i++) {
			qualifString = qualifString + "historique = %@";
			if (i < historiquesPaye().count() - 1) {
				qualifString = qualifString + " OR ";
			} else {
				qualifString = qualifString + ")";
			}
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(qualifString,parametres);
		EOFetchSpecification fs = new EOFetchSpecification("PayeElement",qualifier,null);
		EOEditingContext editingContext = ((EOPayeHisto)historiquesPaye().objectAtIndex(0)).editingContext();
		NSArray elements = editingContext.objectsWithFetchSpecification(fs);
		Enumeration e = elements.objectEnumerator();
		BigDecimal result = new BigDecimal(0);
		while (e.hasMoreElements()) {
			EOPayeElement element = (EOPayeElement)e.nextElement();
			if (estAssiette) {
				if ((typeMontant == REMUNERATION && element.estUneRemuneration()) ||
					(typeMontant == CHARGE_SALARIALE && element.estUneDeduction())||
					(typeMontant == CHARGE_PATRONALE && element.estUneChargePatronale())) {
					result = result.add(element.pelmAssiette());
				}
			} else {
				if (typeMontant == REMUNERATION) {
					result = result.add(element.pelmApayer());
				} else if (typeMontant == CHARGE_SALARIALE) {
					result = result.add(element.pelmAdeduire());
				} else {
					result = result.add(element.pelmPatron());
				}
			}
		}
		return result.setScale(nbDecimales,BigDecimal.ROUND_HALF_UP);
	}*/
	//	23/11/06 modifier pour prendre en compte le témoin RAFP
	private BigDecimal calculerPrimesPourRafpSurElementsEtHistoriques(NSArray historiques) {
		try {
			// rechercher toutes les rémunérations imposables
			NSMutableArray parametres = new NSMutableArray(EOPayeRubrique.A_PAYER);
			parametres.addObject(Constantes.VRAI);
			parametres.addObject(Constantes.VRAI);
			String qualifString = "rubrique.prubMode = %@ AND rubrique.temImposable = %@ AND rubrique.temRafp = %@ AND (";
			parametres.addObjectsFromArray(historiques);
			for (int i = 0; i < historiques.count() ; i++) {
				qualifString = qualifString + "historique = %@";
				if (i < historiques.count() - 1) {
					qualifString = qualifString + " OR ";
				} else {
					qualifString = qualifString + ")";
				}
			}
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(qualifString,parametres);
			EOFetchSpecification fs = new EOFetchSpecification(EOPayeElement.ENTITY_NAME,qualifier,null);
			EOEditingContext editingContext = ((EOPayeHisto)historiques.objectAtIndex(0)).editingContext();
			NSArray elements = editingContext.objectsWithFetchSpecification(fs);
			Enumeration<EOPayeElement> e = elements.objectEnumerator();
			BigDecimal result = new BigDecimal(0);
			while (e.hasMoreElements()) {
				EOPayeElement element = e.nextElement();
				// on n'a que des rémunérations pour la RAFP
				result = result.add(element.pelmApayer()); 
			}
			return result.setScale(2,BigDecimal.ROUND_HALF_UP);	// On a besoin d'un résultat avec les décimales
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//	12/12/05 - correction pour les sommes isolées Ircantec afin de retourner 0 dans S42 si déclaration de somme isolée
	//	08/11/06 - correction pour ne retourner que la tranche A
	//	13/05/08 - correction pour utiliser la catégorie
	private BigDecimal calculerIrcantecTrancheA() {
		// cette méthode peut être appelée lors de l'impression des cumuls
		// 2/10/07 on ne teste plus sur l'ircantec tranche A mais sur l'ircantec car des individus peuvent avoir
		// un mix de contrats avec de la tranche B et tranche A
		if (!cotiseIrcantec()) {
			return new BigDecimal(0);
		}
		int typeCharge = CHARGE_SALARIALE;
		String code = contrat().statut().regimeCotisation().pregCodeIrcantecTrA();
		if (contrat.individu().paieSecu()) {	// 110208 - remplacement de estImposableEnFrance
			if (code.charAt(code.length() - 1) == 'P') {
				typeCharge = CHARGE_PATRONALE;
			}
		} else {
			// pas de cotisation salariale pour les non-imposables en France, se baser sur la cotisation patronale
			typeCharge = CHARGE_PATRONALE;
			code = code.substring(0,code.length() - 1) + "P";
		}
		NSArray elements = rechercherElementsPourCategories(new NSArray("Ircantec*A*"));
		BigDecimal montant = new BigDecimal(0);
		Enumeration e = elements.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeElement element = (EOPayeElement)e.nextElement();
			if ((typeCharge == CHARGE_SALARIALE && element.estUneDeduction()) ||
					(typeCharge == CHARGE_PATRONALE && element.estUneChargePatronale())) {
				montant = montant.add(element.pelmAssiette());
			}
		}

		montant = montant.setScale(2,BigDecimal.ROUND_HALF_UP);
		EOEditingContext editingContext = ((EOPayeHisto)historiquesPaye().objectAtIndex(0)).editingContext();
		// rechercher le plafond mensuel de décembre
		EOPayeCode codeSS = EOPayeCode.rechercherCode(editingContext,ConstantesDads.CODE_PLAFOND_SS_MENSUEL);
		int moisCode = anneeDADS * 100 + 12;
		EOPayeParam paramPlafond = EOPayeParam.parametrePourMois(contrat().editingContext(), codeSS, new Integer(moisCode));
		if (paramPlafond != null) {
			if (montant.doubleValue() > (paramPlafond.pparMontant().doubleValue() * 12))	{
				// base plafonnée supérieure à 12 fois le plafond SS mensuel
				return new BigDecimal(paramPlafond.pparMontant().doubleValue() * 12).setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
			} else {
				return montant;
			}
		} else {
			return montant;
		}
	}
	//	07/12/05 - SFT annuel calculé
	//	03/10/07 - correction pour l'ircantec, il ne faut prendre que le SFT lié aux historiques concernés
	private BigDecimal calculerSFT(boolean estCNRACL) {
		String codeSFT = contrat().statut().regimeCotisation().pregCodeSFT();
		if (codeSFT != null) {
			NSMutableArray historiques = historiquesPaye();
			if (estCNRACL) {
				historiques = new NSMutableArray();	
				// on veut prendre tous les contrats qui relèvent de la CNRACL car on calcule le SFT annuel
				// parcourir toutes les périodes et ajouter tous les historiques correspondant à un statut dépendant de la CNRACL
				java.util.Enumeration e = agent.periodesEnCours().objectEnumerator();
				while (e.hasMoreElements()) {
					PeriodeDadsActive periode = (PeriodeDadsActive)e.nextElement();
					if (periode.contrat().statut().regimeCotisation().estCNRACL()) {
						historiques.addObjectsFromArray(periode.historiquesPaye());
					}
				}
			}
			return calculerSurElementsEtHistoriques(historiques,codeSFT,codeSFT,false,REMUNERATION);
		} else {
			return null;
		}
	}
	//	07/12/05 - vérifie si c'est la dernière période pour la CNRACL
	private boolean estDernierePeriodePourCNRACL() {
		// on parcourt les périodes dans l'autre sens pour voir si on tombe en premier sur la période courante
		java.util.Enumeration e = agent.periodesEnCours().reverseObjectEnumerator();
		while (e.hasMoreElements()) {
			PeriodeDadsActive periode = (PeriodeDadsActive)e.nextElement();
			if (periode.contrat().statut().regimeCotisation().estCNRACL()) {
				if (periode == this) {
					return true;
				} else {
					return false;
				}
			}
		}
		return true;	// normalement, on ne devrait pas passer là
	}


	private BigDecimal calculerMontantCSGSurElements() {
		// commencer par rechercher tous les codes de CSG
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("pcodCode like 'TX*CSG*'",null);
		EOFetchSpecification fs = new EOFetchSpecification("PayeCode",qualifier,null);
		EOEditingContext editingContext = ((EOPayeHisto)historiquesPaye().objectAtIndex(0)).editingContext();
		NSArray codes = editingContext.objectsWithFetchSpecification(fs);
		NSMutableArray parametres = new NSMutableArray();
		String qualifString = "(";
		for (int i = 0; i < codes.count() ; i++) {
			EOPayeCode code = (EOPayeCode)codes.objectAtIndex(i);
			parametres.addObject(code.pcodCode());
			qualifString = qualifString + "code.pcodCode = %@";
			if (i < codes.count() - 1) {
				qualifString = qualifString + " OR ";
			} else {
				qualifString = qualifString + ")";
			}
		}

		parametres.addObjectsFromArray(historiquesPaye());
		qualifString = qualifString + " AND (";
		for (int i = 0; i < historiquesPaye().count() ; i++) {
			qualifString = qualifString + "historique = %@";
			if (i < historiquesPaye().count() - 1) {
				qualifString = qualifString + " OR ";
			} else {
				qualifString = qualifString + ")";
			}
		}
		qualifier = EOQualifier.qualifierWithQualifierFormat(qualifString,parametres);
		fs = new EOFetchSpecification("PayeElement",qualifier,null);
		NSArray elements = editingContext.objectsWithFetchSpecification(fs);
		BigDecimal result = new BigDecimal(0);

		for (Enumeration<EOPayeElement> e = elements.objectEnumerator();e.hasMoreElements();) {
			EOPayeElement element = e.nextElement();
			result = result.add(element.pelmAdeduire());
		}

		return result.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
	}
	/*	private BigDecimal calculerMontantIrcantecTrancheBSurElements(boolean estAssiette,boolean estSalariale) {
		// commencer par rechercher tous les codes d'Ircantec Tranche B
		String qualifString = "pcodCode like ";
		if (estSalariale) {
			qualifString = qualifString + "'TXIR*2*S' OR pcodCode like 'COTIRTBS'";
		} else {
			qualifString = qualifString + "'TXIR*2*P' OR pcodCode like 'COTIRTBP'";
		}

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(qualifString,null);
		EOFetchSpecification fs = new EOFetchSpecification("PayeCode",qualifier,null);
		EOEditingContext editingContext = ((EOPayeHisto)historiquesPaye().objectAtIndex(0)).editingContext();
		NSArray codes = editingContext.objectsWithFetchSpecification(fs);
		NSMutableArray parametres = new NSMutableArray();
		qualifString = "(";
		for (int i = 0; i < codes.count() ; i++) {
			EOPayeCode code = (EOPayeCode)codes.objectAtIndex(i);
			parametres.addObject(code.pcodCode());
			qualifString = qualifString + "code.pcodCode = %@";
			if (i < codes.count() - 1) {
				qualifString = qualifString + " OR ";
			} else {
				qualifString = qualifString + ")";
			}
		}

		parametres.addObjectsFromArray(historiquesPaye());
		qualifString = qualifString + " AND (";
		for (int i = 0; i < historiquesPaye().count() ; i++) {
			qualifString = qualifString + "historique = %@";
			if (i < historiquesPaye().count() - 1) {
				qualifString = qualifString + " OR ";
			} else {
				qualifString = qualifString + ")";
			}
		}
		qualifier = EOQualifier.qualifierWithQualifierFormat(qualifString,parametres);
		fs = new EOFetchSpecification("PayeElement",qualifier,null);
		NSArray elements = editingContext.objectsWithFetchSpecification(fs);
		Enumeration e = elements.objectEnumerator();
		BigDecimal result = new BigDecimal(0);
		while (e.hasMoreElements()) {
			EOPayeElement element = (EOPayeElement)e.nextElement();
			if (estAssiette) {
				result = result.add(element.pelmAssiette());
			} else {
				if (estSalariale) {
					result = result.add(element.pelmAdeduire());
				} else {
					result = result.add(element.pelmPatron());
				}
			}

		}
		return result.setScale(nbDecimales,BigDecimal.ROUND_HALF_UP);
	}*/
	//	dictionnaire où les clés sont les pcodCode et les valeurs les String à mettre dans la DADS
	/*private void ajouterFraisProfessionnel(String code,String valeur) {
		if (codesFraisProfessionnels == null) {
			codesFraisProfessionnels = new NSMutableDictionary(valeur,code);
		} else {
			codesFraisProfessionnels.setObjectForKey(valeur,code);
		}
	}*/

	private double nbHeuresLegal(boolean estHebdomadaire) {
		EOPayeHisto historique = (EOPayeHisto)historiquesPaye().objectAtIndex(0);
		String nomCode = (estHebdomadaire ? ConstantesDads.CODE_NB_HEURES_HEBDO :ConstantesDads.CODE_NB_HEURES_MENSUEL);
		EOPayeCode code = EOPayeCode.rechercherCode(historique.editingContext(),nomCode);
		if (code == null) {
			System.out.println("Veuillez définir dans les parametres de Papaye le nombre d'heures hebdomdaire legal de l'etablissement");
			return 0;
		}
		int moisCode = (historique.payeAnnee().intValue() * 100) + 12;
		EOPayeParam param = EOPayeParam.parametrePourMois(contrat().editingContext(), code, new Integer(moisCode));
		try {
			return param.pparMontant().doubleValue();
		} catch (Exception e) {
			return 0;
		}
	}
	// 21/11/08 - pour les vérifications des heures supplémentaires totales. On vérifie les cumuls d'heures supp et de base net imposable de l'agent
	private void verifierValiditeMontantsHeuresExonerees() {

		try {
			NSMutableArray historiques = new NSMutableArray();

			for (Enumeration<PeriodeDadsActive> e = agent.periodesEnCours().objectEnumerator();e.hasMoreElements();)
				historiques.addObjectsFromArray(e.nextElement().historiquesPaye());

			BigDecimal result = calculerSurElementsEtHistoriques(historiques,ConstantesDads.CODE_REMUN_HEURES_SUP_EXO,ConstantesDads.CODE_REMUN_HEURES_SUP_EXO,false,REMUNERATION);
			result = result.add(calculerSurElementsEtHistoriques(historiques,ConstantesDads.CODE_REMUN_HEURES_COMP_EXO,ConstantesDads.CODE_REMUN_HEURES_COMP_EXO,false,REMUNERATION));

			BigDecimal total = new BigDecimal(0.0);
			for (Enumeration<EOPayeHisto> e1 = historiques.objectEnumerator();e1.hasMoreElements();)
				total = total.add(e1.nextElement().payeBimpmois());

			if (total.doubleValue() < 0)
				total = new BigDecimal(0.00);

			if (result.doubleValue() > (total.doubleValue() * 25) /100)
				EnvironnementDads.sharedInstance().ajouterErreurUnique("\n" + contrat().individu().identite() + " : le montant des heures supp./comp. exonerees depasse 25% du net imposable");

		} catch(Exception exc) {
			exc.printStackTrace();
		}
	}
	//	10/10/07 - pour le calcul de l'ircantec
	// 13/05/08 - pour utiliser les catégories : l'ircantec est maintenant séparée en deux catégories a et b
	private BigDecimal calculBaseTotaleIrcantec() {
		int typeCharge = CHARGE_SALARIALE;
		String code = contrat().statut().regimeCotisation().pregCodeIrcantecTrA();
		if (contrat.individu().paieSecu()) {	// 110208 - remplacement de estImposableEnFrance
			if (code.charAt(code.length() - 1) == 'P') {
				typeCharge = CHARGE_PATRONALE;
			}
		} else {
			// pas de cotisation salariale pour les non-imposables en France, se baser sur la cotisation patronale
			typeCharge = CHARGE_PATRONALE;
		}
		NSArray elements = rechercherElementsPourCategories(new NSArray("Ircantec*"));

		BigDecimal total = new BigDecimal(0);
		for (Enumeration<EOPayeElement> e = elements.objectEnumerator();e.hasMoreElements();) {
			EOPayeElement element = e.nextElement();
			if ((typeCharge == CHARGE_SALARIALE && element.estUneDeduction()) ||
					(typeCharge == CHARGE_PATRONALE && element.estUneChargePatronale())) {
				total = total.add(element.pelmAssiette());
			}
		}
		return total;	
	}
	//	A VERIFIER LORS DE LA GENERATION DE LA PROCHAINE DADS
	private NSArray rechercherElements(NSArray historiques,String code,String codeRappel) {
		if (historiques.count() == 0) {
			return new NSArray();
		}
		NSMutableArray parametres = new NSMutableArray(code);
		String qualifString = "";
		if (codeRappel == null || codeRappel.equals(code)) {
			qualifString = EOPayeElement.CODE_KEY+"."+EOPayeCode.PCOD_CODE_KEY + " = %@";
		} else {
			parametres.addObject(codeRappel);
			qualifString = "(code.pcodCode = %@ OR code.pcodCode = %@)";
		}

		qualifString = qualifString + " AND (";
		parametres.addObjectsFromArray(historiques);
		for (int i = 0; i < historiques.count() ; i++) {
			qualifString = qualifString + "historique = %@";
			if (i < historiques.count() - 1) {
				qualifString = qualifString + " OR ";
			} else {
				qualifString = qualifString + ")";
			}
		}

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(qualifString,parametres);
		EOFetchSpecification fs = new EOFetchSpecification(EOPayeElement.ENTITY_NAME,qualifier,null);
		EOEditingContext editingContext = ((EOPayeHisto)historiques.objectAtIndex(0)).editingContext();
		return editingContext.objectsWithFetchSpecification(fs);
	}

	public String codeStatutAppartenance() {

		if (contrat().statut().pstaCodemploi().equals("9999") || contrat().statut().pstaCodemploi().equals("99999"))
			return "90";

		return "21";
	}

	public String codeStatutJuridique() {

		// Titulaire
		if ("40".equals(populationEmploi()))
			return "011";

		// CA
		if (contrat().statut().pstaAbrege().toUpperCase().equals("CA"))
			return "120";

		// CAE
		if (contrat().statut().pstaAbrege().toUpperCase().equals("CAE"))
			return "120";

		// Apprenti
		if (contrat().statut().pstaAbrege().toUpperCase().equals("APPRENTI"))
			return "100";

		return "030";

	}

	public String populationEmploi() {

		if (contrat().statut().pstaAbrege().toUpperCase().equals("APPRENTI")||
				contrat().statut().pstaAbrege().toUpperCase().equals("CAE") ||
				contrat().statut().pstaAbrege().toUpperCase().equals("CA") 
		)
			return "13";

		EOPayeHisto historique = (EOPayeHisto)historiquesPaye().objectAtIndex(0);

		if (historique.statut().estTitulaire())
			return  "40";

		return "43";

	}

	private NSArray rechercherElementsPourCategories(NSArray categoriesRubrique) {
		NSMutableArray parametres = new NSMutableArray();
		String qualifString = "(";
		for (int i= 0; i < categoriesRubrique.count(); i++) {
			parametres.addObject(categoriesRubrique.objectAtIndex(i));
			qualifString = qualifString + EOPayeElement.RUBRIQUE_KEY+"."+EOPayeRubrique.CATEGORIE_KEY+"."+EOPayeCategorieRubrique.CATEGORIE_LIBELLE_KEY  + " caseInsensitiveLike %@";
			if (i < categoriesRubrique.count() - 1) {
				qualifString = qualifString + " OR ";
			} else {
				qualifString = qualifString + ") AND (";
			}
		}
		parametres.addObjectsFromArray(historiquesPaye());

		for (int i = 0; i < historiquesPaye().count() ; i++) {
			qualifString = qualifString + EOPayeElement.HISTORIQUE_KEY + " = %@";
			if (i < historiquesPaye().count() - 1) {
				qualifString = qualifString + " OR ";
			} else {
				qualifString = qualifString + ")";
			}
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(qualifString,parametres);
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EOPayeElement.PELM_MOIS_CODE_KEY,EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOPayeElement.PRUB_CLASSEMENT_KEY,EOSortOrdering.CompareAscending));
		EOFetchSpecification fs = new EOFetchSpecification(EOPayeElement.ENTITY_NAME,qualifier,sorts);
		EOEditingContext editingContext = ((EOPayeHisto)historiquesPaye().objectAtIndex(0)).editingContext();
		NSArray elements = editingContext.objectsWithFetchSpecification(fs);

		return elements;
	}



	private BigDecimal montantRetenuesSalariales() {
		BigDecimal total = new BigDecimal(0.0);
		for (Enumeration<EOPayeHisto> e = historiquesPaye().objectEnumerator();e.hasMoreElements();) {
			total = total.add(e.nextElement().payeAdeduire());
		}
		total = total.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
		return total;
	}
	private BigDecimal montantRetenuesPatronales() {
		BigDecimal total = new BigDecimal(0.0);
		for (Enumeration<EOPayeHisto> e = historiquesPaye().objectEnumerator();e.hasMoreElements();)
			total = total.add(e.nextElement().payePatron());

		total = total.setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP);
		return total;
	}

	// PREPARATION IRCANTEC

	public String assietteIrcantecA() {
		// cette méthode peut être appelée lors de l'impression des cumuls
		// 2/10/07 on ne teste plus sur l'ircantec tranche A mais sur l'ircantec car des individus peuvent avoir
		// un mix de contrats avec de la tranche B et tranche A
		if (!cotiseIrcantec()) {
			return "0.00";
		}
		int typeCharge = CHARGE_SALARIALE;
		String code = contrat().statut().regimeCotisation().pregCodeIrcantecTrA();
		if (contrat.individu().paieSecu()) {	// 110208 - remplacement de estImposableEnFrance
			if (code.charAt(code.length() - 1) == 'P') {
				typeCharge = CHARGE_PATRONALE;
			}
		} else {
			// pas de cotisation salariale pour les non-imposables en France, se baser sur la cotisation patronale
			typeCharge = CHARGE_PATRONALE;
			code = code.substring(0,code.length() - 1) + "P";
		}
		NSArray elements = rechercherElementsPourCategories(new NSArray("Ircantec*A*"));
		BigDecimal montant = new BigDecimal(0);
		for (Enumeration<EOPayeElement> e = elements.objectEnumerator();e.hasMoreElements();) {
			EOPayeElement element = e.nextElement();
			if ((typeCharge == CHARGE_SALARIALE && element.estUneDeduction()) ||
					(typeCharge == CHARGE_PATRONALE && element.estUneChargePatronale())) {
				montant = montant.add(element.pelmAssiette());
			}
		}

		montant = montant.setScale(2,BigDecimal.ROUND_HALF_UP);
		EOEditingContext editingContext = ((EOPayeHisto)historiquesPaye().objectAtIndex(0)).editingContext();
		// rechercher le plafond mensuel de décembre
		EOPayeCode codeSS = EOPayeCode.rechercherCode(editingContext,ConstantesDads.CODE_PLAFOND_SS_MENSUEL);
		int moisCode = anneeDADS * 100 + 12;
		EOPayeParam paramPlafond = EOPayeParam.parametrePourMois(contrat().editingContext(), codeSS, new Integer(moisCode));
		if (paramPlafond != null)
			if (montant.doubleValue() > (paramPlafond.pparMontant().doubleValue() * 12))
				// base plafonnée supérieure à 12 fois le plafond SS mensuel
				return (new BigDecimal(paramPlafond.pparMontant().doubleValue() * 12).setScale(EnvironnementDads.sharedInstance().nbDecimales(),BigDecimal.ROUND_HALF_UP)).toString();

		// Montant Assiette IRCANTEC A
		return montant.toString();

	}

	public String assietteIrcantecB() {

		if (!cotiseIrcantec())
			return "00.00";

		NSArray elements = rechercherElementsPourCategories(new NSArray("Ircantec*B*"));

		BigDecimal montant = new BigDecimal(0);
		for (Enumeration<EOPayeElement> e = elements.objectEnumerator();e.hasMoreElements();) {
			EOPayeElement element = e.nextElement();
			if (element.estUneChargePatronale())
				montant = montant.add(element.pelmAssiette());
		}

		// Montant Assiette IRCANTEC B
		return (montant.setScale(2,BigDecimal.ROUND_HALF_UP)).toString();

	}

}
