/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.n4ds;

import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.cocktail.application.serveur.eof.EOUtilisateur;
import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.common.DateCtrl;
import org.cocktail.papaye.server.metier.grhum.EOAdresse;
import org.cocktail.papaye.server.metier.grhum.EOCommune;
import org.cocktail.papaye.server.metier.grhum.EOEffectifStructure;
import org.cocktail.papaye.server.metier.grhum.EOIndividu;
import org.cocktail.papaye.server.metier.grhum.EORepartCompte;
import org.cocktail.papaye.server.metier.grhum.EOStructure;
import org.cocktail.papaye.server.metier.jefy_admin.EOExercice;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeEmployeur;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut;
import org.cocktail.papaye.server.metier.jefy_paye.EORegimeCotisation;
import org.cocktail.papaye.server.metier.jefy_paye.EOSituationParticuliere;
import org.cocktail.papaye.server.metier.jefy_paye.dads.EODadsAgents;
import org.cocktail.papaye.server.metier.jefy_paye.dads.EODadsFichiers;
import org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeParametresDads;
import org.cocktail.papaye.server.utilitaires.ServerThreadManager;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSRange;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author christine
 *
 * Comporte toutes les methodes de classe utiles pour la realisation de la DADS
 */
public class AutomateDads  {
	private static AutomateDads sharedInstance;
	private int anneeDads;
	private ServerThreadManager threadCourant;
	private int nbEnregistrements,nbEtablissements,nbEnregistrementsPourRubrique;	// ce dernier compteur est remis à zéro chaque fois qu'on change de rubrique S10, S20, S30, S70, S80
	private int nbNoInseeInconnus,nbIndividus;
	private ListeRubriquesDads listeRubriques;
	private ListeIndividusPourDads listeIndividus,listeIndividusPourHonoraires;
	private EOExercice exerciceCourant;
	private static NSMutableArray valeursDads;	// pour garder les données générées lors de la préparation de la Dads
	private NSMutableArray structuresAvecSiren; 	// DADSV08R09 - pour connaitre les structures qu'il faut absolument déclarer
	private String typeDads;
	private boolean capricasSansSalarie,carcicasSansSalarie;	// 21/11/08 - Utilisé dans les dads complètes pour signaler qu'aucun salarié n'est déclaré
	private boolean estDadsComplete;		// 21/11/08 - pour savoir si il faut déclarer les rubriques de complémentaires sans salariés
	private static int ACCEPTER = 0;
	private static int INFORMER = 1;
	private static int INTERDIRE = 2;
	private static Boolean cotiseAuChomage = Boolean.FALSE;
	private GregorianCalendar date = new GregorianCalendar();

	// méthodes statiques
	public static AutomateDads sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new AutomateDads();
		}
		return sharedInstance;
	}
	/**
	 * Verifie que le dernier enregistrement du fichier DADS correspond aux attentes
	 * @param path chemin d'acc&egrave;s du fichier
	 * @return true si pas d'erreur rencontree
	 */
	public static boolean validerFichierDADS(String path) {
		boolean verificationOK = false;
		try {
			BufferedReader fichier = FichierDads.fichierEnLecturePourPath(path);
			boolean fini = false;
			int numRubriques = 0;
			int numEtablissements = 0;
			boolean nbEnregistrementsOK = true, nbEtablissementsOK = true;
			try {
				while (!fini) {
					String texte = fichier.readLine();
					numRubriques++;
					if (texte.startsWith("S20.G01.00.001")) {
						numEtablissements++;
					} else if (texte.startsWith("S90.G01.00.001")) {
						nbEnregistrementsOK = FichierDads.verifierEtModifierCompteur(texte,numRubriques + 1,null);	// on ajoute 1 car il faut prendre en compte la rubrique suivante
					} else if (texte.startsWith("S90.G01.00.002")) {
						nbEtablissementsOK = FichierDads.verifierEtModifierCompteur(texte,numEtablissements,null);
						fini = true;
					}
				}
				verificationOK = nbEnregistrementsOK && nbEtablissementsOK;
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				fichier.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return verificationOK;
	}

	private final String getHour() {

		date.setTime(new NSTimestamp());
		return "(" + date.get(Calendar.HOUR_OF_DAY) + ":" + date.get(Calendar.MINUTE)+ ":" + date.get(Calendar.SECOND) + ")"; 
	}

	/**
	 * Retourne l'ensemble des fichiers generes pour la DADS sous la forme d'un dictionnaire.<BR>
	 * 	La cle est le nom du fichier et la valeur est un tableau contenant une entrée par ligne de texte
	 * @param path chemin d'acces des fichiers
	 * @return null si erreur
	 */
	public static NSDictionary recupererFichiersDADS(String path) {
		NSMutableDictionary dict = new NSMutableDictionary();
		FichierDads.ajouterFichierAuDictionnaire(path,FichierDads.NOM_FICHIER_DADS,dict);
		FichierDads.ajouterFichierAuDictionnaire(path,EnvironnementDads.FICHIER_ERREUR,dict);
		return dict;
	}
	/**
	 * Regen&egrave;re le dernier enregistrement du fichier DADS en comptant le nombre de rubriques et d'etablissements
	 * @param path chemin d'accès du fichier
	 * @return true si pas d'erreur rencontree
	 */
	public static boolean regenererEnregistrementFinal(String path) {
		boolean verificationOK = false;
		try {
			// créer le fichier
			RandomAccessFile fichier = FichierDads.randomFichierPourPath(path);
			boolean fini = false;
			int numRubriques = 0;
			int numEtablissements = 0;
			boolean nbEnregistrementsOK = true, nbEtablissementsOK = true;
			try {
				while (!fini) {
					String texte = fichier.readLine();
					numRubriques++;
					if (texte.startsWith("S20.G01.00.001")) {
						numEtablissements++;
					} else if (texte.startsWith("S90.G01.00.001")) {
						nbEnregistrementsOK = FichierDads.verifierEtModifierCompteur(texte,numRubriques + 1,fichier);	// on ajoute 1 car il faut prendre en compte la rubrique suivante
					} else if (texte.startsWith("S90.G01.00.002")) {
						nbEtablissementsOK = FichierDads.verifierEtModifierCompteur(texte,numEtablissements,fichier);
						fini = true;
					}
					verificationOK = nbEnregistrementsOK && nbEtablissementsOK;
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				fichier.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return verificationOK;
	}

	// méthodes publiques
	// Constructeurs
	public AutomateDads() {
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
		nbEnregistrements = 0;
		nbEtablissements = 0;
		EnvironnementDads.sharedInstance().setNbDecimales(2);
	}
	/** indique le d&acute;but de periode de la DADS sous la forme JJMMAAAA : pour DADS-U.txt */
	public String debutPeriode() {
		return "0101" + anneeDads;
	}
	/** indique la fin de periode de la DADS sous la forme JJMMAAAA : pour DADS-U.txt */
	public String finPeriode() {
		return "3112" + anneeDads;
	}
	/** retourne le debut de periode de rattachement des salaires, null pour une declaration normale */
	public String debutPeriodeRattachement() {
		if (typeDads() == null || typeDads().equals(ConstantesDads.DADS_NORMALE)) {
			return null;
		} else {
			return null;
		}
	}
	/** retourne la fin de periode de rattachement des salaires, null pour une declaration normale */
	public String finPeriodeRattachement() {
		if (typeDads() == null || typeDads().equals(ConstantesDads.DADS_NORMALE)) {
			return null;
		} else {
			return null;
		}
	}
	/** retourne le type de la DADS : pour DADS-U.txt */
	public String typeDads() {
		return typeDads;
	}
	/** 21/11/08 V08R08 - Retourne la nature de la DADS : 01 si intermittents/artistes dans l'etablissement, 02 sinon. */
	public String natureDads() {

		estDadsComplete = false;
		return ConstantesDads.DADS_TDS;


		/*		capricasSansSalarie = true;
		carcicasSansSalarie = true;
		EOEditingContext editingContext = new EOEditingContext();
		if (EOPayeHisto.chercherHistoriquesArtistesIntermittentsPourAnnee(editingContext, anneeDads).count() > 0) {
			estDadsComplete = true;
			return ConstantesDads.DADS_COMPLETE;
		} else {
			estDadsComplete = false;
			return ConstantesDads.DADS_TDS;
		}
		 */
	}
	public Number numeroDeclaration() {
		if (typeDads() == null || typeDads().equals(ConstantesDads.DADS_NORMALE))
			return null;
		
		return new Integer(100);

	}
	/** 21/11/08 V08R08 - Caisse de retraite complementaire sans salarie pour les dads completes */
	public String caisseRetraiteSansSalarie() {
		if (capricasSansSalarie) {	// Il faut faire le test dans cet ordre car il correspond à l'ordre dans lequel est généré la rubrique
			return Constantes.CAPRICAS;
		} else if (carcicasSansSalarie) {
			return Constantes.CARCICAS;
		} else {
			return null;
		}
	}
	/**
	 * Retourne le nombre d'enregistrements calcules
	 * @return nbEnregistrements
	 */
	public Integer nbEnregistrementsCalcule() {
		return new Integer(nbEnregistrements + 2); // il y a encore les deux derniers enregistrements à écrire
	}
	/** indique le nombre d'etablissements : pour DADS-u.txt */
	public int nbEtablissements() {
		return nbEtablissements;
	}
	/** prepare une DADS normale ou rectificative
	 * @param editingContext editingContext dans lequel travailler
	 * @param annee Integer annee pour laquelle on cre la DADS sur 4 digits
	 * @param nomModele nom du modele Papaye, si null pas d'enregistrement des donnees dans la base
	 * @param path path du directory dans lequel creer le fichier DADS
	 * @param pathXMLpourDADS chemin d'acces du fichier XML contenant la description de la DADS
	 */
	public void preparerDADS(EOEditingContext editingContext,Integer annee,String nomModele,String path,String pathXMLpourDADS,ServerThreadManager threadCourant) {

		if (annee == null) {
			informerThread("Veuillez fournir l'annee pour laquelle realiser l'impression");
			return;
		}	
		// Supprimer la DADS précente
		String message = supprimerDADSPourAnnee(editingContext,annee);
		if (message != null) {
			informerThread(message);
			return;
		}
		EnvironnementDads.sharedInstance().init();

		informerThread("Debut Preparation N4DS - " + getHour());

		nbEnregistrements = 0;
		nbEtablissements = 0;
		typeDads = ConstantesDads.DADS_NORMALE;
		valeursDads = new NSMutableArray();
		structuresAvecSiren = new NSMutableArray();

		this.anneeDads = annee.intValue();
		exerciceCourant = EOExercice.exercicePourAnnee(editingContext, annee);
		this.threadCourant = threadCourant;


		try {

			date.setTime(new NSTimestamp());
			informerThread("Recuperation de tous les éléments de paye - " + getHour());

			editingContext.lock();
			if (EnvironnementDads.sharedInstance().estInitialise() == false) {
				// on veut créer la DADS pour tous les agents. Optimisation, on fetche tous les éléments de l'année
				fetcherElements(editingContext,annee);
			}
			// charger le fichier de description de la DADS

			date.setTime(new NSTimestamp());
			informerThread("Lecture fichier DADS " + getHour());

			listeRubriques = ParserDads.parseXML(pathXMLpourDADS);
			
			FichierDads.creerFichier(path);

			genererRubriquesGenerales(editingContext);

			genererInformationsAgents(editingContext,anneeDads);

			genererInformationsHonoraires(editingContext,annee);

			genererInformationStructures(editingContext,annee);

			genererEnregistrementFinal(editingContext);

			if (nomModele != null && nomModele.length() > 0)
				enregistrerDonnees(nomModele);

			System.out.println("AutomateDads.preparerDADS() FINAL");
			// Vérifier si le nombre de numéros insee inconnus est > 25 %
			Double nbInconnus = new Double(nbNoInseeInconnus),nbTotal = new Double(nbIndividus);
			if (nbInconnus.doubleValue() / nbTotal.doubleValue() > 0.25) {
				informerThread("Trop de no SS inconnus (1999999999999 ou 2999999999999), la DADS sera rejetee");
				EnvironnementDads.sharedInstance().ajouterErreur("Trop de no SS inconnus (1999999999999 ou 2999999999999), la DADS sera rejetee");
			}
			informerThread("Termine");
			String listeErreurs = EnvironnementDads.sharedInstance().listeErreurs();
			if (listeErreurs.equals("") == false) {
				informerThread(listeErreurs);
			}
			EnvironnementDads.sharedInstance().genererRapports(path);
		} catch (Exception e1) {	// autres exceptions que celles attendues
			signalerExceptionThread(e1);
		} finally {
			date.setTime(new NSTimestamp());
			informerThread("Fin traitement et fermeture fichier." + getHour());
			editingContext.unlock();
			FichierDads.fermerFichier();
		}	
	}
	// méthodes privees
	private String supprimerDADSPourAnnee(EOEditingContext editingContext,Integer annee) {
		informerThread("Supppression de la Dads precedente");
		NSMutableDictionary dict = new NSMutableDictionary("DadsFichiers","01nomtable");
		dict.setObjectForKey(annee, "04exeordre");
		try {
			EOUtilities.executeStoredProcedureNamed(editingContext, "cleanTables", dict);
			return null;
		} catch (Exception e) {
			informerThread("ERREUR SUPPRESSION DADS FICHIERS !!!!!");
			e.printStackTrace();
			return e.getMessage();
		}
	}
	private void fetcherElements(EOEditingContext editingContext,Integer annee) {
		NSMutableArray qualifiers = new NSMutableArray();
		long moisMinimum = (annee.longValue() * 100) + 1;
		long moisMaximum = (annee.longValue() * 100) + 12;
		NSMutableArray valeurs = new NSMutableArray(new Long(moisMinimum));
		valeurs.addObject(new Long(moisMaximum));
		EOQualifier qualifier =  EOQualifier.qualifierWithQualifierFormat("pelmMoisCode >= %@ AND pelmMoisCode <= %@",valeurs);
		qualifiers.addObject(qualifier);
		NSMutableArray orQualifiers = new NSMutableArray();
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
				"rubrique.categorie.categorieLibelle like %@",new NSArray("URSSAF - Vieillesse Plafonnée")));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
				"rubrique.categorie.categorieLibelle like %@",new NSArray("Ircantec")));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
				"rubrique.categorie.categorieLibelle like %@",new NSArray("RAFP")));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
				"rubrique.categorie.categorieLibelle like %@",new NSArray("Taxe Sur Salaire")));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
				"rubrique.categorie.categorieLibelle like %@",new NSArray("CSG - CRDS")));
		qualifiers.addObject(new EOOrQualifier(orQualifiers));

		qualifier =  new EOAndQualifier(qualifiers);
		EOFetchSpecification fs = new EOFetchSpecification(EOPayeElement.ENTITY_NAME,qualifier,null);

		editingContext.objectsWithFetchSpecification(fs);
	}
	/** prepare les agents et periodes a partir de l'historique
	 * @param editingContext dans lequel effectuer les fetchs
	 * @param separeHonoraires true si on doit separer les salaries des honoraires
	 * @return dictionnaire contenant le resultat (3 cles : "agents","honoraires","messages)
	 */
	private void preparerAgents(EOEditingContext editingContext) {
		try {
			editingContext.lock();
			listeIndividus = new ListeIndividusPourDads();
			listeIndividusPourHonoraires = new ListeIndividusPourDads();

			EOIndividu individuCourant = null;
			EOPayeContrat contratCourant = null;
			IndividuDads agentCourant = null;

			date.setTime(new NSTimestamp());
			informerThread("Preparation des donnees Agent - " + getHour());

			// SQL
			NSArray historiques = EOUtilities.rawRowsForSQL(editingContext,"Papaye", EOPayeHisto.sqlQualifierHistoriquesPourAnnee(new Integer(anneeDads)));
			
			date.setTime(new NSTimestamp());
			informerThread("Historiques des agents recuperes - " + getHour());


			PeriodeDadsActive periodeCourante = null;
			for (Enumeration<NSDictionary> e = historiques.objectEnumerator();e.hasMoreElements();) {

				// SQL
				NSDictionary dicoHisto = (NSDictionary)e.nextElement();				
				EOPayeHisto historique = null;
				
				if (dicoHisto.objectForKey("PAYE_ORDRE").getClass().getName().equals(BigDecimal.class.getName())) {
					historique = EOPayeHisto.histoForKey(editingContext, new Integer(dicoHisto.objectForKey("PAYE_ORDRE").toString()));					
				}
				else {
					if (dicoHisto.objectForKey("PAYE_ORDRE").getClass().getName().equals(Double.class.getName())) {
						Double payeOrdre = (Double)dicoHisto.objectForKey("PAYE_ORDRE");
						historique = EOPayeHisto.histoForKey(editingContext, new Integer(payeOrdre.intValue()));						
					}					
				}

				boolean creerPeriodeDADS = false;

				if (historique.contrat().individu() == individuCourant) {
					// il s'agit du même individu, rechercher si c'est un contrat différent
					
					if (historique.contrat() != contratCourant) {
												
						if (contratPourDADS(historique.contrat())) { 	// on ne prend pas en compte les statuts sans régime de cotisation 11/01/06
							periodeCourante = periodePourContrat(agentCourant,historique.contrat());
							if (periodeCourante == null) {
								contratCourant = historique.contrat();
								
								// AUTEURS
								if (contratCourant.statut().pstaCodemploi().equals(ConstantesDads.CODE_DROIT_AUTEUR)) {
									// on regroupe tous les droits d'auteur dans une seule période
									if (listeIndividusPourHonoraires.existeIndividu(individuCourant) == false) {
										// avant de créer les nouvelles données, vérifier si il n'y a pas de périodes de 
										// chevauchement pour l'agent qui était traité
										if (agentCourant != null)
											verifierPeriodes(agentCourant,individuCourant);

										// l'agent n'a pas encore été considéré du point de vue des droits d'auteur
										agentCourant = new IndividuDads();
										listeIndividusPourHonoraires.ajouterIndividu(individuCourant, agentCourant);
										creerPeriodeDADS = true;
									} else {	 // il y en a forcément une créée lors d'un précédent contrat
										periodeCourante = (PeriodeDadsActive)agentCourant.periodesActivites().objectAtIndex(0);
										periodeCourante.ajouterHistorique(historique);
									}
								} else {
									if (agentCourant == null || (listeIndividus.existeIndividu(individuCourant) == false)) {		// 20/02/08 - cas des agents ayant des droits d'auteur et des contrats (les droits d'auteur étant détectés en premier)
										// on a rencontré auparavant un contrat pour cet individu qui n'était pas pris en compte
										agentCourant = new IndividuDads();
										listeIndividus.ajouterIndividu(individuCourant,agentCourant);
									}
									creerPeriodeDADS = true;
								}
							} else {
								contratCourant = historique.contrat();
								periodeCourante.ajouterHistorique(historique);
							}
						}
					} else {
						if (periodeCourante != null) {	// null si on a rejeté un contrat
							periodeCourante.ajouterHistorique(historique);
						}
					}
				} else {
					// avant de créer les nouvelles données, vérifier si il n'y a pas de périodes de chevauchement pour l'agent qui était traité
					if (agentCourant != null) {
						verifierPeriodes(agentCourant,individuCourant);
					}
					contratCourant = historique.contrat();
					individuCourant = contratCourant.individu();
					if (contratPourDADS(contratCourant)) { 	// on ne prend pas en compte les chômeurs
						creerPeriodeDADS = true;
						agentCourant = new IndividuDads();
						if (contratCourant.statut().pstaCodemploi().equals(ConstantesDads.CODE_DROIT_AUTEUR))
							listeIndividusPourHonoraires.ajouterIndividu(individuCourant, agentCourant);
						else
							listeIndividus.ajouterIndividu(individuCourant, agentCourant);
						
					} else {
						
						agentCourant = null;
						periodeCourante = null;
						
					}
				}
				if (creerPeriodeDADS) {
					periodeCourante = new PeriodeDadsActive(agentCourant,contratCourant,anneeDads);
					periodeCourante.ajouterHistorique(historique);
					agentCourant.ajouterPeriodeActivite(periodeCourante);
				}
			}
			int nbTotalAgents = 0;
			if (EnvironnementDads.sharedInstance().estInitialise() == false) {
				nbTotalAgents = listeIndividus.nombreIndividus();
			} else {
				nbTotalAgents = compterAgentsTraites();
			}
			informerThread("Nombre d'agents a traiter ==> " + nbTotalAgents);
		} catch (Exception e) {
			e.printStackTrace();
			signalerExceptionThread(e);
		} finally {
			editingContext.unlock();
		}

	}

	// méthodes privées
	private int compterAgentsTraites() {
		int total = 0;

		for (Enumeration<EOIndividu> e = listeIndividus.individusParOrdreAlphabetique().objectEnumerator();e.hasMoreElements();) {
			EOIndividu individu = e.nextElement();
			if (EnvironnementDads.sharedInstance().individuValide(individu,listeIndividus.individuDADSPourIndividu(individu)))
				total++;

		}
		return total;
	}

	private boolean contratPourDADS(EOPayeContrat contrat) {
		boolean aTraiter = true;
		// vérifier si chômeur
		//aTraiter = contrat.statut().pstaCodemploi().equals(ConstantesDads.CODE_CHOMEUR) == false;
		// correction du 11/01/06 pour la gestion des statuts sans régime de cotisation
		if (aTraiter) {
			// vérifier si il y a un régime de cotisation, sinon ne pas traiter le contrat
			aTraiter = (contrat.statut().regimeCotisation() != null);
		}

		return aTraiter;
	}
	private PeriodeDadsActive periodePourContrat(IndividuDads agent,EOPayeContrat nouveauContrat) {
		if (agent == null) {	// agent non encore créé
			return null;
		}

		// vérifier si il existe un autre contrat pour la même structure
		PeriodeDadsActive periodePossible = null;
		boolean trouve = false;

		for (Enumeration<PeriodeDadsActive> e = agent.periodesActivites().objectEnumerator();e.hasMoreElements();) {

			PeriodeDadsActive periode = e.nextElement();
			EOPayeContrat contrat = periode.contrat();
			
			if  (nouveauContrat.structure().siretStructure().equals(contrat.structure().siretStructure())) {
				// vérifier si ce sont les mêmes statuts
				if (nouveauContrat.statut() == contrat.statut()) {
					/*	System.out.println(contrat.individu().identite());
					System.out.println("date debut contrat " + contrat.dDebContratTrav() + " date fin contrat " + contrat.dFinContratTrav());
					System.out.println("date debut nouveau contrat " + nouveauContrat.dDebContratTrav() + " date fin nouveau contrat " + nouveauContrat.dFinContratTrav());*/
					// vérifier si le contrat est dans cette période
					// les contrats sont triés par date de début de contrat, on ne compare donc que par rapport à la date de fin
					// du contrat en cours
					if (periode.dateFin() == null || nouveauContrat.dDebContratTrav().before(periode.dateFin())) {
						trouve = true;
						if (periode.dateFin() != null && nouveauContrat.dFinContratTrav() != null && periode.dateFin().before(nouveauContrat.dFinContratTrav())) {
							// modifier la fin de la période si le nouveau contrat finit plus tard
							periode.modifierFinPeriode(nouveauContrat);
						}
					}

					if (periode.dateFin() != null) {
						// vérifier si le nouveau contrat commence un jour après la fin de la période
						if (anterieurUnJour(periode.dateFin(),nouveauContrat.dDebContratTrav())) {
							// vérifier si les motifs de début de contrat ne sont pas des changements de contrat ou de régime
							if (nouveauContrat.motifDebut() != null) {

								int motif = new Integer(periode.motifDebutPeriode()).intValue();
								if (motif < 21 || motif > 35)
									trouve = true;

							} else
								trouve = true;

							if (trouve) {
								// modifier la fin de période
								periode.modifierFinPeriode(nouveauContrat);
								// garder le dernier contrat trouvé comme contrat principal pour les cumuls
								periode.setContrat(nouveauContrat);
							}
						} 
					}

					if (trouve)
						return periode;

				}
				// vérifier si ce sont des statuts regroupables
				if (contrat.statut().pstaCodemploi().startsWith(ConstantesDads.CODE_EMPLOI_ARTISTE) && nouveauContrat.statut().pstaCodemploi().startsWith(ConstantesDads.CODE_EMPLOI_ARTISTE)) {
					// dans le cas d'un artiste, vérifier si c'est cumulable (méme base de paiement)
					if (modePaiementIdentique(contrat,nouveauContrat)) {
						if (nouveauContrat.dFinContratTrav() == null || 
								(periode.dateFin() != null && periode.dateFin().before(nouveauContrat.dFinContratTrav()))) {
							// modifier la fin de période
							periode.modifierFinPeriode(nouveauContrat);
							// garder le dernier contrat trouvé comme contrat principal pour les cumuls
							periode.setContrat(nouveauContrat);
						}
						trouve = true;
					}
				} else {
					String categorieContrat = contrat.statut().pstaCipdz();
					String categorieNouveauContrat = nouveauContrat.statut().pstaCipdz();
					if (categorieContrat.equals(categorieNouveauContrat) && 
							(categorieContrat.equals(EOPayeStatut.CATEGORIE_OCCASIONNEL) || categorieContrat.equals(EOPayeStatut.CATEGORIE_VACATAIRE))) {
						if (contrat.statut() == nouveauContrat.statut() ||
								(contrat.statut().regimeCotisation() != null && nouveauContrat.statut().regimeCotisation() != null &&
										contrat.statut().regimeCotisation().regimeIdentique(nouveauContrat.statut().regimeCotisation()))) {
							trouve = true;
							// on regroupe tous les contrats de l'agent vacataire ou travailleur occasionnel ayant le même statut ou 
							// le même régime de cotisation
							if (nouveauContrat.dFinContratTrav() == null ||
									(periode.dateFin() != null && periode.dateFin().before(nouveauContrat.dFinContratTrav()))) {
								// modifier la fin de période
								periode.modifierFinPeriode(nouveauContrat);
							}
						}
					} 
					// dans le cas des contrats à temps complet et des vacations, on regroupe aussi les périodes
					// cas du CEC avec un contrat d'heures supplémentaires ou du contractuel avec une vacation
					if (categorieContrat.equals(EOPayeStatut.CATEGORIE_TEMPS_PLEIN) && 
							(categorieNouveauContrat.equals(EOPayeStatut.CATEGORIE_OCCASIONNEL) || categorieNouveauContrat.equals(EOPayeStatut.CATEGORIE_VACATAIRE)) &&
							(contrat.statut().regimeCotisation() != null && nouveauContrat.statut().regimeCotisation() != null &&
									contrat.statut().regimeCotisation().regimeIdentique(nouveauContrat.statut().regimeCotisation())) &&
									agent.periodesActivites().lastObject() == periode) {
						// on rattache à la derniére période trouvée
						trouve = true;
					}
					if (categorieNouveauContrat.equals(EOPayeStatut.CATEGORIE_TEMPS_PLEIN) && 
							(categorieContrat.equals(EOPayeStatut.CATEGORIE_OCCASIONNEL) || categorieContrat.equals(EOPayeStatut.CATEGORIE_VACATAIRE)) &&
							(contrat.statut().regimeCotisation() != null && nouveauContrat.statut().regimeCotisation() != null &&
									contrat.statut().regimeCotisation().regimeIdentique(nouveauContrat.statut().regimeCotisation())) &&
									agent.periodesActivites().lastObject() == periode) {
						// on rattache à la derniére période trouvée
						// choisir le contrat temps plein comme contrat principal
						periode.preparerPeriode(nouveauContrat);
						trouve = true;
					}
				}
			}
			if (trouve) {
				periodePossible = periode;
				break;
			}
		}
		return periodePossible;
	}
	// vérifie si il n'y a pas de périodes qui se chevauchent pour un agent sinon le signaler
	// 07/12/05 : le chevauchement de périodes est autorisé pour les agents ne travaillant pas à temps plein dans
	// chaque contrat
	// pour les cas interdits, modifier les dates des périodes
	private void verifierPeriodes(IndividuDads agent,EOIndividu individu) {
		
		if (agent.periodesActivites().count() == 1) {
			return;
		}
		try {
			// 18/02/08 - MULHOUSE problème de l'ordre des périodes
			// Trier sur le début et la fin de la période
			NSMutableArray periodesOrdonnees = new NSMutableArray(agent.periodesActivites());
			NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending));
			sorts.addObject(EOSortOrdering.sortOrderingWithKey("dateFinPourTri", EOSortOrdering.CompareAscending));
			EOSortOrdering.sortArrayUsingKeyOrderArray(periodesOrdonnees, sorts);
			//System.out.println("debuts " + periodesOrdonnees.valueForKey("debutPeriode"));
			//System.out.println("fins " + periodesOrdonnees.valueForKey("finPeriode"));
			PeriodeDadsActive periode1 = (PeriodeDadsActive)agent.periodesActivites().objectAtIndex(0);
			NSTimestamp dateFinMax = periode1.dateFin();	// 18/02/08 - MULHOUSE problème de l'ordre des périodes
			boolean informerContratIllegal = false;
			boolean informerChevauchement = false;
			boolean informerChangementPeriode = false;


			for (int i = 1; i < periodesOrdonnees.count();) {
				// 18/02/08 - MULHOUSE Pour être sûr qu'on voit bien un chevauchement quand on a par exemple comme périodes
				// 0101 au 3112, puis 0104 au 3006 puis 0107 au 3112
				if (dateFinMax != null && (periode1.dateFin() == null || dateFinMax.compareTo(periode1.dateFin()) > 0)) {
					dateFinMax = periode1.dateFin();
				}
				PeriodeDadsActive periode2 = (PeriodeDadsActive)periodesOrdonnees.objectAtIndex(i);
				
				// 18/02/08 - MULHOUSE problème de l'organisation des périodes
				if (chevauchementDates(periode1,periode2) || dateFinMax == null || dateFinMax.compareTo(periode2.dateDebut()) >= 0)  {
					// 28/02/06 - vérifier si suite aux modifications de dates pour les périodes précédentes, il n'y a pas de problème
					int resultat = verifierDatesContratIllegal(periode1,periode2);
					if (resultat == INFORMER) {
						informerContratIllegal = true;
					} else {
						resultat = verifierChevauchementPossible(periode1, periode2);
						if (resultat == INFORMER) {
							informerChevauchement = true;
						}
					}
					if (resultat == ACCEPTER || resultat ==  INFORMER) {
						periode1 = periode2;
						i++;	
					} else {	
						//EnvironnementDads.sharedInstance().ajouterErreur(individu.identite() + "\nLe chevauchement de periodes n'est pas possible. L'agent sera généré en double dans la DADS\n");
						periode1.signalerConflit();
						informerChangementPeriode = false;
						//}
						periode1 = periode2;
						i++; 	// on peut passer à la suivante puisque c'est résolu
						//}
					}
				} else {
					// on prend comme période de comparaison la seconde période car les périodes sont triées dans l'ordre croissant
					periode1 = periode2;
					i++;
				}
			}
			System.out.println("AutomateDads.verifierPeriodes()"  + periodesOrdonnees);
			agent.setPeriodesActivites(periodesOrdonnees);	// pour forcer à mettre les périodes dans le bon ordre
			if (informerContratIllegal || informerChevauchement || informerChangementPeriode) {
				if (informerContratIllegal) {
					EnvironnementDads.sharedInstance().ajouterErreur(individu.identite() + "a un contrat de CDI pour une vacation (pas de fin de contrat). Ce n'est pas possible. Verifiez les contrats\n");
				} else 
					if (informerChevauchement) {
					} 
					else {

						EnvironnementDads.sharedInstance().ajouterErreur(individu.identite() + "\nLe chevauchement de periodes n'est pas possible. Les periodes ont ete modifiees. \nNouvelles periodes :");
						for (Enumeration e = agent.periodesActivites().objectEnumerator();e.hasMoreElements();)
							EnvironnementDads.sharedInstance().ajouterErreur(e.nextElement().toString());

					}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// les périodes sont triées dans l'ordre croissant de date début, on vérifie juste si la date de fin de la deuxiéme période est antérieure
	// à la date fin de la premiére
	private boolean chevauchementDates(PeriodeDadsActive periode1,PeriodeDadsActive periode2) {
		if (periode1.dateDebut() == null) {
			//System.out.println("le debut de la periode est nulle pour l'agent " + periode1.contrat().individu().identite());
			//System.out.println("periode 1\n" + periode1);
			return false;
		}
		return ((periode2.dateDebut().compareTo(periode1.dateDebut()) >= 0) && (periode1.dateFin() == null || (periode2.dateDebut().compareTo(periode1.dateFin()) <= 0)));
	}
	private int verifierDatesContratIllegal(PeriodeDadsActive periode1,PeriodeDadsActive periode2) {

		
		String categorieContrat1 = periode1.contrat().statut().pstaCipdz();
		String categorieContrat2 = periode2.contrat().statut().pstaCipdz();
		// les vacations doivent avoir une date de fin de contrat
		if ((periode1.dateFin() == null && categorieContrat1.equals(EOPayeStatut.CATEGORIE_TEMPS_PLEIN) == false) ||
				(periode2.dateFin() == null && categorieContrat2.equals(EOPayeStatut.CATEGORIE_TEMPS_PLEIN) == false)) {
			return INFORMER;
		} else
			return INTERDIRE;
		
		
		
	}
	// retourne true si le chevauchement de période est possible : contrats en CDI, cotisations Ircantec ou 
	// agents ne travaillant pas à temps plein...
	private int verifierChevauchementPossible(PeriodeDadsActive periode1,PeriodeDadsActive periode2) {
		String categorieContrat1 = periode1.contrat().statut().pstaCipdz();
		String categorieContrat2 = periode2.contrat().statut().pstaCipdz();
		int resultat;
		// dans le cas où les contrats cotisent à l'ircantec ou à la CNRACL, on peut autoriser le chevauchement. 
		if ((periode1.contrat().statut().regimeCotisation().estIrcantec() && periode2.contrat().statut().regimeCotisation().estIrcantec()) ||
				(periode1.contrat().statut().regimeCotisation().estCNRACL() && periode2.contrat().statut().regimeCotisation().estCNRACL())) {
			// correction du 24/02/06 pas d'emplois multiples pour des temps plein méme avec des sirets différents
			if  (categorieContrat1.equals(EOPayeStatut.CATEGORIE_TEMPS_PLEIN) == false && categorieContrat2.equals(EOPayeStatut.CATEGORIE_TEMPS_PLEIN) == false) {
				periode1.signalerEmploisMultiples();
				periode2.signalerEmploisMultiples();
				resultat = INFORMER;
			} else {
				resultat = INTERDIRE;
			}
			return resultat;
		} 
		// 05/12/05 modification de la norme pour autoriser les chevauchements de période dans tous les cas de contrats 
		// sauf les temps pleins
		if (categorieContrat1.equals(EOPayeStatut.CATEGORIE_TEMPS_PLEIN) || categorieContrat2.equals(EOPayeStatut.CATEGORIE_TEMPS_PLEIN)) {
			//System.out.println("categorie 1 " + categorieContrat1 + ", categorie 2 " + categorieContrat2 + ", type contrat 1" + periode1.typeContrat() + ", type contrat 2" + periode2.typeContrat());
			resultat = INTERDIRE;
		} else {
			periode1.signalerEmploisMultiples();
			periode2.signalerEmploisMultiples();
			resultat = ACCEPTER;
		} 
		return resultat;
	}
	// retourne une valeur indiquant si un changement de période a eu lieu et dans le cas positif la nature du changement
	/*	private int changerDatesPeriode(PeriodeDadsActive periode1,PeriodeDadsActive periode2) {
		System.out.println(periode1.contrat().individu().identite());
		System.out.println("avant changement, periode 1 debut :" + periode1.dateDebut() + " fin : " + periode1.dateFin() + ", periode 2 debut : " + periode2.dateDebut() + " fin : " + periode2.dateFin());
		System.out.println("motif période " + periode1.motifDebutPeriode());
		System.out.println(" période 1 peut démarrerAnneeCourante " + periode1.peutDemarrerAnneeCourante());
		// 08/02/06 - si une des deux périodes est compléte, on interdit directement
		if ((periode1.debutPeriode().equals("0101") && periode1.finPeriode().equals("3112")) ||
			(periode2.debutPeriode().equals("0101") && periode2.finPeriode().equals("3112"))) {
			return INTERDIRE;
		}
		//07/03/06 - on interdit le changement si la somme des jours de chaque période est > 365 et que un des deux contrats est un temps plein
		// la premiére période est celle qui commence le plus tét
		// si la premiére période s'arréte avant début décembre
		if (periode1.finPeriode().endsWith("12") == false) {
			if (periode2.peutDemarrerAnneeCourante()) {	
				//  on décale la deuxiéme période si ce n'était pas un contrat commencé une année antérieure
				// décaler la date de début de la deuxiéme période au jour suivant la fin de la premiére
				periode2.setDateDebut(periode1.dateFin().timestampByAddingGregorianUnits(0,0,1,0,0,0));
				// décaler la date de fin si nécessaire
				if (periode2.dateFin() != null && periode2.dateFin().compareTo(periode2.dateDebut()) <= 0) {
					if (periode2.debutPeriode().endsWith("12")) {
						// la date début est en décembre, décaler la date fin à la fin du mois de décembre
						NSTimestamp myNSTimestamp = new NSTimestamp(anneeImpression(), 12, 31, 0, 0, 0, NSTimeZone.timeZoneWithName("Europe/Paris", false));
						periode2.setDateFin(myNSTimestamp);
					} else {
        					// décaler la date de fin de la durée de la période
        					//periode2.setDateFin(periode2.dateDebut().timestampByAddingGregorianUnits(0,0,nbJours,0,0,0));
						periode2.setDateFin(periode2.dateDebut().timestampByAddingGregorianUnits(0,1,-1,0,0,0));
					}
				}
				System.out.println("PERIODE1_ANTERIEURE aprés changement, periode 1 debut :" + periode1.dateDebut() + " fin : " + periode1.dateFin() + ", periode 2 debut : " + periode2.dateDebut() + " fin : " + periode2.dateFin());
				return PERIODE1_ANTERIEURE;
			} 
		}		
		if ((periode1.debutPeriode().equals("0101") == false || periode1.peutDemarrerAnneeCourante()) && periode2.finitDansAnneeCourante()) {
			if (periode1.debutPeriode().equals("0101")) {
				// décaler le début de la premiére période au début février
				NSTimestamp myNSTimestamp = new NSTimestamp(anneeImpression(), 2, 1, 0, 0, 0, NSTimeZone.timeZoneWithName("Europe/Paris", false));
				periode1.setDateDebut(myNSTimestamp);
				if (periode1.finPeriode().endsWith("01")) {
					// décaler à fin février
					myNSTimestamp = new NSTimestamp(anneeImpression(), 3, -1, 0, 0, 0, NSTimeZone.timeZoneWithName("Europe/Paris", false));
					periode1.setDateFin(myNSTimestamp);
				}
			}
			// décaler la date de fin de la deuxiéme période au jour précédent le début de la premiére
			periode2.setDateFin(periode1.dateDebut().timestampByAddingGregorianUnits(0,0,-1,0,0,0));
			if (periode2.dateDebut().compareTo(periode2.dateFin()) >= 0) {
				if (periode2.finPeriode().endsWith("01")) {
					// la date de fin est en janvier, décaler la date début au début du mois de janvier
					NSTimestamp myNSTimestamp = new NSTimestamp(anneeImpression(), 1, 1, 0, 0, 0, NSTimeZone.timeZoneWithName("Europe/Paris", false));
					periode2.setDateDebut(myNSTimestamp);
				} else {
					// décaler la date de début d'un mois
					periode2.setDateDebut(periode2.dateFin().timestampByAddingGregorianUnits(0,-1,0,0,0,0));
				}
			}
			System.out.println("PERIODE2_ANTERIEURE aprés changement, periode 1 debut :" + periode1.dateDebut() + " fin : " + periode1.dateFin() + ", periode 2 debut : " + periode2.dateDebut() + " fin : " + periode2.dateFin());
			return PERIODE2_ANTERIEURE;
		} else {
			// pas de possibilité de bouger une des périodes
			System.out.println("INTERDIT");
			return INTERDIRE;
		}
	}*/
	private boolean modePaiementIdentique(EOPayeContrat contrat1,EOPayeContrat contrat2) {
		if (contrat1.abattement() != null && contrat2.abattement() != null && contrat1.abattement().doubleValue() == contrat2.abattement().doubleValue()) {
			if (contrat1.montantMensuelRemu() != null && contrat2.montantMensuelRemu() != null && 
					contrat1.montantMensuelRemu().doubleValue() != 0 && contrat2.montantMensuelRemu().doubleValue() != 0) {
				// rémunération mensuelle
				return true;
			}
			int nbJours1 = 0, nbJours2 = 0;
			if (contrat1.nbJoursContrat() != null && contrat2.nbJoursContrat() != null) {
				nbJours1 = contrat1.nbJoursContrat().intValue();
				nbJours2 = contrat2.nbJoursContrat().intValue();
			}
			if ((nbJours1 == 0 && nbJours2 == 0) ||
					(nbJours1 > 0 && nbJours2 > 0 && nbJours1 < 5 && nbJours2 < 5) || 
					(nbJours1 > 5 && nbJours2 > 5)) {
				// les deux contrats sont payés sur la même base horaire, moins de 5 jours ou journalière
				return true;
			}	
			if ((nbJours1 == 0 && nbJours2 < 5) || (nbJours2 == 0 && nbJours1 < 5)) {
				// les deux contrats sont ramenés à une base horaire
				return true;
			}
			return false;
		} else {
			return false;
		}
	}
	private void genererRubriquesGenerales(EOEditingContext editingContext) {
		NSMutableArray parametres = new NSMutableArray(5);	
		try {

			informerThread("Generation rubriques generales DADS ");

			// préparer la rubrique concernant l'établissement émetteur
			EOStructure etablissement = EOStructure.rechercherEtablissement(editingContext);
			parametres.addObject(etablissement);
			EOAdresse adresseEtablissement = etablissement.adresseEtablissement();
			EOCommune commune = null;
			if (adresseEtablissement == null) {
				EnvironnementDads.sharedInstance().ajouterErreur("L'adresse de l'établissement est invalide");
			} else {
				parametres.addObject(adresseEtablissement);
				if (adresseEtablissement.pays().estFrance())  {
					if (adresseEtablissement.codePostal() != null) {		// commune en France
						commune = EOCommune.rechercherCommune(editingContext,adresseEtablissement.codePostal(),adresseEtablissement.ville());
						if (commune == null)  {
							EnvironnementDads.sharedInstance().ajouterErreur("Pour l'etablissement, la commune est inconnue ");
						}
					} else {
						EnvironnementDads.sharedInstance().ajouterErreur("Veuillez fournir le code postal de l'établissement");
					}
				} else {
					if (adresseEtablissement.pays().codeIso() == null) {
						EnvironnementDads.sharedInstance().ajouterErreur("Table PAYS incomplete, pas de code iso pour le pays " + adresseEtablissement.pays().llPays());
					}
				}
				parametres.addObject(adresseEtablissement.pays());
			}
			EOPayeParametresDads  infoDADS = EOPayeParametresDads.rechercherParametre(editingContext);
			if (infoDADS != null) {
				parametres.addObject(infoDADS);
			}
			EOUtilisateur agentPaie = infoDADS.utilisateur();
			EORepartCompte repartCompte = null;
			if (agentPaie != null) {
				parametres.addObject(agentPaie);
				if (agentPaie.individu() != null && agentPaie.individu().persId() != null) {
					repartCompte = EORepartCompte.chercherRepartCompteAvecPersID(agentPaie.individu().persId(),editingContext);
				}
				if (repartCompte != null) {
					parametres.addObject(repartCompte.compte());
				}
			}
			informerThread("Preparation des donnees Emetteur");
			nbEnregistrementsPourRubrique = 0;
			preparerRubrique("Emetteur",parametres,editingContext,etablissement,null,null);
			// préparer la rubrique Contact_Emetteur
			if (agentPaie != null) {
				parametres = new NSMutableArray(agentPaie);
				if (agentPaie.individu() != null) {
					parametres.addObject(agentPaie.individu());
				}
				if (repartCompte != null) {
					parametres.addObject(repartCompte.compte());
				}
			} else {
				parametres = new NSMutableArray();
			}
			informerThread("Preparation des donnees Contact Emetteur");
			preparerRubrique("Contact_Emetteur",parametres,editingContext,etablissement,null,null);
			// préparer la rubrique concernant l'établissement
			parametres = new NSMutableArray(etablissement);
			parametres.addObject(this);
			if (adresseEtablissement != null) {
				parametres.addObject(adresseEtablissement);
				if (commune != null) {
					parametres.addObject(commune);
				}
				parametres.addObject(adresseEtablissement.pays());
			}
			if (repartCompte != null) {
				parametres.addObject(repartCompte.compte());
			}
			if (infoDADS != null) {
				parametres.addObject(infoDADS);
			}

			date.setTime(new NSTimestamp());
			informerThread("Preparation des donnees Etablissement" + getHour());

			nbEtablissements++;
			System.out.println("AutomateDads.genererRubriquesGenerales() NB ETABLISSEMENTS : " + nbEtablissements);
			
			preparerRubrique("Declaration_Entreprise",parametres,editingContext,etablissement,null,null);
		} catch (Exception e) {
			signalerExceptionThread(e);
		}
	}


	private void genererInformationsAgents(EOEditingContext editingContext,int annee) {

		preparerAgents(editingContext);

		int totalIndividus = listeIndividus.nombreIndividus();
		nbIndividus = totalIndividus;
		nbNoInseeInconnus = 0;

		NSArray individus = listeIndividus.individusParOrdreAlphabetique();		
		for (int i = 0, nbAgents = 0; i < totalIndividus; i++) {

			nbEnregistrementsPourRubrique = 0;	// on recommence à zéro ce compteur pour chaque individu
			EOIndividu agent = (EOIndividu)individus.objectAtIndex(i);
			IndividuDads agentDADS = listeIndividus.individuDADSPourIndividu(agent);
			if (EnvironnementDads.sharedInstance().individuValide(agent,agentDADS)) {
				// Agent potentiellement à traiter, vérifier ses données
				nbAgents++;
				NSMutableArray parametres = new NSMutableArray(agent);
				String messageErreur = "";
				// vérifier si le pays de naissance est connu, sinon signaler le problème
				if (agent.paysNaissanceDADS() == null) {
					messageErreur = "\n\tVeuillez fournir le pays de naissance de l'agent";
				}
				// vérifier si la commune de naissance pour les gens nés en France est nulle
				try {

					if (agent.departementNaissance().equals("99") == false && agent.lieuNaissanceDADS() == null)
						messageErreur = messageErreur + "\n\tVeuillez fournir le lieu de naissance de l'agent";

				} catch (Exception e) {
					// cas où le numéro Insee n'est pas fourni
					messageErreur = messageErreur + "\n\tVeuillez fournir le numero SS de l'agent";	
				}
				// Compter le nombre de numéros insee inconnus. A partir de la V08R06, il doit étre inférieur à 25%
				String noInsee = agent.numeroInsee();
				if (noInsee != null && (noInsee.equals(EOIndividu.NO_INSEE_HOMME_INCONNU) || noInsee.equals(EOIndividu.NO_INSEE_FEMME_INCONNU))) {
					nbNoInseeInconnus++;
				}
				EOAdresse adresse = agent.adresseCourante();
				if (adresse == null) {
					messageErreur = messageErreur + "\n\tCet agent n'a pas une adresse valide";
				} else {
					parametres.addObject(adresse);
					if (adresse.pays().estFrance())  {
						if (adresse.codePostal() != null) {		// commune en France
							EOCommune commune = EOCommune.rechercherCommune(editingContext,adresse.codePostal(),adresse.ville());
							if (commune == null)  {
								messageErreur = messageErreur + "\n\tVeuillez modifier la commune de l'agent";
							}
						} else {
							messageErreur = messageErreur + "\n\tVeuillez fournir le code postal dans l'adresse de l'agent";
						}
					} else {
						if (adresse.pays().codeIso() == null) {
							messageErreur = messageErreur + "\n\tTable PAYS incomplete, pas de code iso pour le pays " + adresse.pays().llPays();
						}
					}
					parametres.addObject(adresse.pays());
				}
				try {
					String information = new Integer(nbAgents).toString() + " " + agent.identite();
					informerThread(information);
					if (verifierPeriodes(editingContext,agent,agentDADS)) {		// vérifier si un agent a bien des périodes rémunérées

						NSArray groupePeriodes = organiserPeriodes(agentDADS);	// 08/02/06 pour isoler les périodes conflictuelles
						// Si un agent a des périodes conflictuelles, on va générer autant de fois l'agent que de sous-groupes
						// conflictuels identifiés

						boolean aPeriodesConflictuelles = groupePeriodes.count() > 1;

						for (Enumeration<NSArray> e = groupePeriodes.objectEnumerator();e.hasMoreElements();) {
							NSArray sousGroupe = e.nextElement();
							if (aPeriodesConflictuelles) {
								agentDADS.setPeriodesEnCours(sousGroupe);	// 09/02/06 pour ne générer la taxe sur salaire qu'aux bons endroits
							}
							genererInformationPeriodes(editingContext,agent,parametres,sousGroupe,aPeriodesConflictuelles);
						}
					}
				} catch (Exception e1) {
					signalerExceptionThread(e1);
				}
				if (messageErreur.equals("") == false) {
					EnvironnementDads.sharedInstance().ajouterErreur("**************************\n" + agent.identite() + messageErreur);
				}
			}
		}
	}
	private void genererInformationPeriodes(EOEditingContext editingContext,EOIndividu agent,NSArray parametresAgent,NSArray periodes,boolean aPeriodesConflictuelles) throws Exception {
		// créer les rubriques de période d'activité
		// si un agent a plusieurs contrats et que dans l'un de ceux-ci, le total brut est zéro (i.e  le contrat a été annulé), on peut le faire disparaétre
		// dans le cas où il a un seul contrat, il a peut-étre une période d'inactivité à déclarer, donc une période d'activité obligatoire
		// sauf si il y a des périodes conflictuelles, en quel cas on rajoutera les périodes d'inactivité à la période conflictuelle (08/02/06)
		// On s'intéresse au total brut signé de maniére à prendre en compte aussi les BS négatifs qui apparaétraien
		// avec une période unique parce que l'agent est déclaré 2 fois
		int numPeriodes = periodes.count();
		boolean estPeriodeUnique = !aPeriodesConflictuelles && (numPeriodes == 1);

		boolean agentPrepare = false;
		String finPeriodePourPrudhome = null;	// V08R06 - on doit mettre la rubrique de prudhomme sur toutes
		// les dernières périodes avec des contrats de droit privé (les dernières si plusieurs se terminent à la même date)
		// Déterminer la derniére date fin sur laquelle déclarer les prud'hommes
		for (int i = 0; i < numPeriodes;i++) {
			PeriodeDadsActive periode = (PeriodeDadsActive)periodes.objectAtIndex(i);
			EOPayeContrat contrat = periode.contrat();
			if ((contrat.statut().pstaAbrege().toUpperCase().equals("CEJ") || 
					contrat.statut().pstaAbrege().toUpperCase().equals("CES") || 
					contrat.statut().pstaAbrege().toUpperCase().equals("CEC") || 
					contrat.statut().pstaAbrege().toUpperCase().equals("APPRENTI") ||
					contrat.statut().pstaAbrege().toUpperCase().equals("CAE") || 
					contrat.statut().pstaAbrege().toUpperCase().equals("CA"))) {
				finPeriodePourPrudhome = periode.finPeriode();
			}
		}
		boolean periodesInactivitesGenerees = false;
		for (int i = 0; i < numPeriodes;i++) {

			PeriodeDadsActive periode = (PeriodeDadsActive)periodes.objectAtIndex(i);
			
//			System.out.println(">>>>>>AutomateDads.genererInformationPeriodes() AGENT : " + periode.contrat().individu().nomUsuel() + " " + periode.contrat().individu().prenom());
//			System.out.println("      AutomateDads.genererInformationPeriodes() DEBUT : " + periode.debutPeriode());
//			System.out.println("      AutomateDads.genererInformationPeriodes() " + periode.finPeriode());
			
			// 30/01/08 Modification pour prendre en compte des BS négatifs qui ne s'annulent pas
			boolean aDeclarer = false;
			if (!estPeriodeUnique) {
				BigDecimal brut = periode.baseBrutImposableSignee();
				aDeclarer = brut != null && brut.doubleValue() != 0;
			}
			if (!aDeclarer) {	// 11/02/08	- pour étre sur que les bulletins contenant les rappels soient pris en compte
				aDeclarer = EODadsAgents.dadsObligatoirePourIndividu(editingContext, agent,anneeDads);
			}
			if (estPeriodeUnique || aDeclarer) {
				if (!agentPrepare) {
					// 13/02/06 préparer maintenant les données agent afin de gérer le problème des périodes conflictuelles
					// ou des agents qui auraient une S30 vide
					// la S30 est créée pour tous les types de DADS
//					System.out
//							.println("AutomateDads.genererInformationPeriodes() IDENTIFICATION_SALARIE");
					preparerRubrique("Identification_Salarie",parametresAgent,editingContext,null,periode,null);	
					agentPrepare = true;
				}
				NSMutableArray parametres = new NSMutableArray(periode);
				// on prend les données comme fonction, code socio-professionnel,à comme celles du premier contrat
				EOPayeContrat contrat = periode.contrat();
				parametres.addObject(contrat);
				// toutes les structures au sein d'une période concerne un même numéro Siret
				parametres.addObject(contrat.structure());
				if (contrat.fonction() == null && contrat.grade() == null) {
					String texte = "\tVeuillez definir dans le contrat la fonction ou le grade de l'agent ";
					EnvironnementDads.sharedInstance().ajouterErreur(texte + agent.identite());
				}

				parametres.addObject(contrat.statut());
				parametres.addObject(agent.personnel());
				EORegimeCotisation regimeCotisation = contrat.statut().regimeCotisation();
				if (regimeCotisation != null && (Object)regimeCotisation != NSKeyValueCoding.NullValue) {

					parametres.addObject(contrat.statut().regimeCotisation());
					try {
						
						// DADS V08R09 20/11/09 - Ajouter la structure si elle comporte un siren
						EOStructure structureAvecSiren = contrat.structure().structureAvecSiren();
						if (structuresAvecSiren.containsObject(structureAvecSiren) == false)
							structuresAvecSiren.addObject(structureAvecSiren);

						preparerRubrique("Periode_Activite",parametres,editingContext,null,periode,null);

						// Droit prive
						if ( contrat.statut().pstaAbrege().toUpperCase().equals("APPRENTI") ||
								contrat.statut().pstaAbrege().toUpperCase().equals("CAE") ||
								contrat.statut().pstaAbrege().toUpperCase().equals("CA") ) {
							
							preparerRubrique("Contrat_Droit_Prive_Situation",parametres,editingContext,null,periode,null);
							preparerRubrique("Contrat_Droit_Prive_Complement",parametres,editingContext,null,periode,null);							
						}
						else // Droit Public
							preparerRubrique("Periode_Activite_Situation",parametres,editingContext,null,periode,null);

						if (regimeCotisation.estSRE()) {
							EOPayeEmployeur employeurOrigine = EOPayeEmployeur.rechercherEmployeurOrigine(contrat.individu(), editingContext);
							if (employeurOrigine == null) {
								// 21/01/2010 - modifié pour mettre un code générique Enseignement scolaire
								System.out.println("Cet agent est un agent detache. En l'absence d'employeur d'origine, la rubrique S41.G10.25 sera preparee avec le code 106");
								employeurOrigine = new EOPayeEmployeur();
								employeurOrigine.addObjectToBothSidesOfRelationshipWithKey(contrat.individu(), "agent");
								employeurOrigine.setPempCode("106");	// Enseignement scolaire
							} 
							parametres.addObject(employeurOrigine);
							preparerRubrique("Situation_Agent_Detache",parametres,editingContext,null,periode,null);

						}

						preparerRubrique("Duree_Travail_Agent",parametres,editingContext,null,periode,null);

						// Droit prive
						if ( contrat.statut().pstaAbrege().toUpperCase().equals("APPRENTI") ||
								contrat.statut().pstaAbrege().toUpperCase().equals("CAE") ||
								contrat.statut().pstaAbrege().toUpperCase().equals("CA") )
							preparerRubrique("Duree_Travail_Secteur_Prive",parametres,editingContext,null,periode,null);
						else	// Droit Public
							preparerRubrique("Duree_Travail_Secteur_Public",parametres,editingContext,null,periode,null);

						preparerRubrique("Periode_Activite_Regime",parametres,editingContext,null,periode,null);

						if (regimeCotisation.pregExoUrssaf() != null && regimeCotisation.pregExoUrssaf().equals(ConstantesDads.SANS_EXO_URSSAF) == false)
							preparerRubrique("Exoneration_URSSAF",parametres,editingContext,null,periode,null);

						if (periode.montantHeuresSupExo() != null)
							preparerRubrique("Heures_Supp_Exonerees_Complement",parametres,editingContext,null,periode,null);

						preparerRubrique("Periode_Activite_Regime_Suite",parametres,editingContext,null,periode,null);

						// REGIME COMPLEMENTAIRE
						// L agent est soit IRCANTEC, RAFP ou SRE (RAFP inclus dans SRE)
						if (regimeCotisation.estIrcantec() && regimeCotisation.pregCodeIrcantecTrA() != null) {
							preparerRubrique("Ircantec",parametres,editingContext,null,periode,null);
							preparerRubrique("Chomage",parametres,editingContext,null,periode,null);
						}
						else								
							if (regimeCotisation.estRAFP()) {
								preparerRubrique("Chomage",parametres,editingContext,null,periode,null);
								preparerRubrique("RAFP",parametres,editingContext,null,periode,null);
							}
							else
								if (regimeCotisation.estSRE()) {
									preparerRubrique("Cotisation_Pour_Pension",parametres,editingContext,null,periode,null);
									preparerRubrique("Chomage",parametres,editingContext,null,periode,null);
									preparerRubrique("RAFP",parametres,editingContext,null,periode,null);
								}
								else
									preparerRubrique("Chomage",parametres,editingContext,null,periode,null);

						if ( periode.finPeriode().equals(finPeriodePourPrudhome))	// on est sur une sur laquelle déclarer les prud'hommes
							preparerRubrique("Prudhome",parametres,editingContext,null,periode,null);

						// A appliquer seulement pour le regime general
						if (regimeCotisation.pregRegimeMaladie().equals(ConstantesDads.REGIME_GENERAL)
								&& !contrat.statut().pstaAbrege().toUpperCase().equals("APPRENTI")
								&& !contrat.statut().pstaAbrege().toUpperCase().equals("CAE")
								&& !contrat.statut().pstaAbrege().toUpperCase().equals("CA")
								)
							preparerRubrique("Droits_Assurance_Maladie",parametres,editingContext,null,periode,null);

						if (regimeCotisation.estSRE())
							preparerRubrique("Contributions_SRE",parametres,editingContext,null,periode,null);
						
						if (regimeCotisation.estRAFP() || regimeCotisation.estSRE())
							preparerRubrique("Cotisations_RAFP",parametres,editingContext,null,periode,null);

						if (regimeCotisation.pregCodeCSGReduite() != null && regimeCotisation.pregCodeCSGReduite().equals("") == false)
							preparerRubrique("CSG_Specifique",parametres,editingContext,null,periode,null);


						// DADS V08R06
						if (contrat.statut().categorie().categorieLibelle().equals(ConstantesDads.CATEGORIE_STAGIAIRE_SANS_CHARGES)) {
							preparerRubrique("Autres_Sommes_Exonerees",parametres,editingContext,null,periode,null);
						}
						// DADS V08R06
						if (periode.aAIndemniteDepart())
							preparerRubrique("Indemnite_Rupture_Contrat",parametres,editingContext,null,periode,null);

//						// DADS V08R06
//						if (periode.montantHeuresSupExo() != null) {
//							preparerRubrique("Heures_Supp_Exonerees",parametres,editingContext,null,periode,null);
//							preparerRubrique("Heures_Supp_Exonerees_Complement",parametres,editingContext,null,periode,null);
//						}
						BigDecimal montantRetenues = periode.montantRetenues();
						// DADS V08R09 - on ne déclare plus de montant négatif à la CNRACL
						if (regimeCotisation.estCNRACL() && montantRetenues != null && montantRetenues.doubleValue() > 0) {
							preparerRubrique("Regime_Titulaire",parametres,editingContext,null,periode,null);
							preparerRubrique("Bonification", parametres, editingContext, null, periode, null);
						}
						if (regimeCotisation.estRegimeIntermittent()) {
							if (regimeCotisation.pregComplementaire().equals(Constantes.CAPRICAS)) 
								capricasSansSalarie = false;
							else if (regimeCotisation.pregComplementaire().equals(Constantes.CARCICAS)) {
								carcicasSansSalarie = false;
							}
							preparerRubrique("IRC",parametres,editingContext,null,periode,null);
						}


						// générer les périodes d'inactivité
						// 08/02/06	- on ne génère les périodes d'inactivité que pour le premier sous-groupe de périodes
						// qui comporte une période conflictuelle (i.e allant du 01/01 au 31/12 et avec 
						// chevauchement de périodes) ou bien pour le groupe si les périodes ne sont pas conflictuelles
						// Déplacer dans cette méthode le 13/12/07
						/*	if (!periodeInactiviteGeneree && (groupePeriodes.count() == 1 || 
								(sousGroupe.count() == 1 && 
										((PeriodeDadsActive)sousGroupe.objectAtIndex(0)).estConflictuelle()))) {*/
						// On ne génère les périodes d'activité qu'une seule fois et on les génère avec une des périodes
						// auxquelles elles correspondent
						if (!periodesInactivitesGenerees)
							periodesInactivitesGenerees = genererInformationsPeriodesInactivites(editingContext,agent,periode,periodes);

						//}

					} catch (Exception e1) {
						
						System.out
								.println("AutomateDads.genererInformationPeriodes() ERREUR !!!! " + e1.getMessage());
						
						signalerExceptionThread(e1);
						EnvironnementDads.sharedInstance().ajouterErreur(agent.identite() + "\n"+ e1.getMessage());
					}
				} else {
					String texte = "Veuillez definir le regime de cotisation du statut " + contrat.statut().pstaLibelle();
					EnvironnementDads.sharedInstance().ajouterErreur(texte);
					throw new Exception("Veuillez definir le regime de cotisation du statut " + contrat.statut().pstaLibelle());
				}
			}
		}
	}

	private boolean genererInformationsPeriodesInactivites(EOEditingContext editingContext,EOIndividu agent,PeriodeDadsActive periodeCourante,NSArray periodesActivites) {
		//System.out.println("nb periodesActivites " + periodesActivites.count());
		NSArray situationsParticulieres = EOSituationParticuliere.situationsParticulieresPourIndividu(editingContext,agent,anneeDads);
		if (situationsParticulieres.count() == 0) {
			return true;
		}
		// valider les codes motifs : un même code motif ne peut étre utilisé sur des périodes qui se chevauchent
		if (situationsParticulieres.count() > 1) {
			try {
				if (periodeCourante.contrat().statut().regimeCotisation().estIrcantec()) {
					verifierChevauchementSituations(situationsParticulieres);
				}
				verifierMotifsSituationsParticulieres(situationsParticulieres);
			} catch (Exception e) {
				e.printStackTrace();
				signalerExceptionThread(e);
				EnvironnementDads.sharedInstance().ajouterErreur(e.getMessage());
				return true;
			}
		}


		// Pour chaque situation particulière, rechercher les périodes possibles d'activité correspondant à 
		// cette période d'inactivité : elles doivent étre à cheval sur la situation particulière i.e
		// il ne faut pas que la situation particulière se termine avant le début de la période d'activité 
		// ou commence aprés la fin de celle-ci
		//System.out.println("situation debut " + DateCtrl.dateToString(situation.dateDebut()) + ", situation fin " + DateCtrl.dateToString(situation.dateFin()));
		//System.out.println("nb situations trouvees " + situationsParticulieres.count());
		for (Enumeration<EOSituationParticuliere> e = situationsParticulieres.objectEnumerator();e.hasMoreElements();) {
			EOSituationParticuliere situation = e.nextElement();


			NSMutableArray periodesPossibles = new NSMutableArray();
			for (Enumeration<PeriodeDadsActive> e1 = periodesActivites.objectEnumerator();e1.hasMoreElements();) {
				PeriodeDadsActive periode = e1.nextElement();

				if (DateCtrl.isBefore(situation.dateFin(), periode.dateDebut()) || 
						(periode.dateFin() != null && DateCtrl.isBefore(periode.dateFin(), situation.dateDebut()))) { 
					// Pas de chevauchement
				} else
					periodesPossibles.addObject(periode);

			}

			if (periodesPossibles.count() > 0) {

				// On ne genere pas de situation particulière avec cette période
				if (periodesPossibles.containsObject(periodeCourante) == false)
					return false;

				boolean situationValide = false;
				try {
					situationValide = situation.validePourPeriodes(periodesPossibles,anneeDads);
				} catch (Exception exc) {
					exc.printStackTrace();

					signalerExceptionThread(exc);
					EnvironnementDads.sharedInstance().ajouterErreur(exc.getMessage());
					return true;
				}
				if (situationValide) {
					// DADS V08R09 - Vérifier si toutes ces périodes correspondent à un seul contrat ou si il faut générer
					// une référence contrat, on prendra la première trouvée
					NSMutableArray contrats = new NSMutableArray();
					for (Enumeration<PeriodeDadsActive> e2 = periodesPossibles.objectEnumerator();e2.hasMoreElements();) {
						PeriodeDadsActive periode = (PeriodeDadsActive)e2.nextElement();
						if (contrats.containsObject(periode.contrat()) == false)
							contrats.addObject(periode.contrat());
					}

					String referenceContrat = null;

					for (Enumeration <EOPayeContrat> e2 = contrats.objectEnumerator();e2.hasMoreElements();) {
						EOPayeContrat contrat = e2.nextElement();
						if (contrat.referenceContrat() != null) {
							referenceContrat = contrat.referenceContrat();
							break;
						}
					} 

					if (referenceContrat == null)
						EnvironnementDads.sharedInstance().ajouterErreur("La période particuliere du" + DateCtrl.dateToString(situation.dateDebut()) + " au " + DateCtrl.dateToString(situation.dateFin()) + " correspond à plusieurs contrats. Au moins, un de ces contrats doit avoir une référence contrat");

					PeriodeDadsInactive periodeInactive = new PeriodeDadsInactive(situation,referenceContrat);
					NSMutableArray parametres = new NSMutableArray(situation);
					// DADS V08R09 - pour avoir la référence contrat
					parametres.addObject(periodeInactive);
					preparerRubrique("PeriodeParticuliere_Particuliere",parametres,editingContext,null,null,periodeInactive);
					if (situation.codeTypeMontant() == null) {	// le code est non nul pour l'ircantec et la CNRACL
						preparerRubrique("Situation_Particuliere_Autre",parametres,editingContext,null,null,periodeInactive);
					} else {
						preparerRubrique("Situation_Particuliere",parametres,editingContext,null,null,periodeInactive);
					}
					break;
				}
			} else {
				EnvironnementDads.sharedInstance().ajouterErreur("La période particulière du" + DateCtrl.dateToString(situation.dateDebut()) + " au " + DateCtrl.dateToString(situation.dateFin()) + " ne correspond à aucune période d'activité");
			}	
		}
		return true;
	}
	private void genererInformationsHonoraires(EOEditingContext editingContext,Integer annee) {

		int totalIndividus = listeIndividusPourHonoraires.nombreIndividus();
		NSArray individus = listeIndividusPourHonoraires.individusParOrdreAlphabetique();
		
		boolean messageAffiche = false;
		for (int i = 0; i < totalIndividus; i++) {
			nbEnregistrementsPourRubrique = 0;	// on recommence à zéro ce compteur pour chaque individu
			EOIndividu individu = (EOIndividu)individus.objectAtIndex(i);
			IndividuDads individuDADS = listeIndividusPourHonoraires.individuDADSPourIndividu(individu);
			if (EnvironnementDads.sharedInstance().individuValide(individu,individuDADS)) {
				if (!messageAffiche) {
					informerThread("Nombre d'honoraires a declarer :" + totalIndividus);
					informerThread("Preparation des honoraires");
					messageAffiche = true;
				}
				NSMutableArray parametres = new NSMutableArray(individu);
				EOAdresse adresse = individu.adresseCourante();
				if (adresse == null) {
					EnvironnementDads.sharedInstance().ajouterErreur("\tL'adresse de l'agent "+ individu.identite() + " n'est pas une adresse valide");
				} else {
					parametres.addObject(adresse);
					if (adresse.pays().estFrance())  {
						if (adresse.codePostal() != null) {		// commune en France
							EOCommune commune = EOCommune.rechercherCommune(editingContext,adresse.codePostal(),adresse.ville());
							if (commune == null)  {
								EnvironnementDads.sharedInstance().ajouterErreur("\tVeuillez modifier la commune de l'agent "+ individu.identite());
							}
						} else {
							EnvironnementDads.sharedInstance().ajouterErreur("\tVeuillez fournir le code postal dans l'adresse de l'agent "+ individu.identite());
						}
					} else {
						if (adresse.pays().codeIso() == null) {
							EnvironnementDads.sharedInstance().ajouterErreur("\tTable PAYS incomplete, pas de code iso pour le pays " + adresse.pays().llPays());
						}
					}

					parametres.addObject(adresse.pays());
				}
				// il n'y a forcément qu'une période
				PeriodeDadsActive periode = (PeriodeDadsActive)individuDADS.periodesActivites().objectAtIndex(0);
				parametres.addObject(periode);
				parametres.addObject(periode.contrat());
				parametres.addObject(periode.contrat().structure());
				// DADS V08R09 20/11/09 - Ajouter la structure si elle comporte un siren
				EOStructure structureAvecSiren = periode.contrat().structure().structureAvecSiren();
				if (structuresAvecSiren.containsObject(structureAvecSiren) == false) {
					structuresAvecSiren.addObject(structureAvecSiren);
				}
				try {
					informerThread(new Integer(i + 1).toString() + "\t" + individu.identite());
					preparerRubrique("EtablissementHonoraires",parametres,editingContext,null,periode,null);
					preparerRubrique("Honoraires",parametres,editingContext,null,periode,null);
					preparerRubrique("MontantHonoraires",parametres,editingContext,null,periode,null);
					//		editingContext.saveChanges();	// sauvegarder toutes les rubriques générées pour cet individu
				} catch (Exception e1) {
					signalerExceptionThread(e1);
				}
			}
		}

	}
	private void genererInformationStructures(EOEditingContext editingContext,Integer annee) {

		// rechercher les structures comportant un numéro de siret
		NSArray structures = EOStructure.rechercherStructuresAvecSiret(editingContext);

		date.setTime(new NSTimestamp());
		informerThread("Préparation Structures - " + getHour());

		nbEnregistrementsPourRubrique = 0;

		for (Enumeration<EOStructure> e = structures.objectEnumerator();e.hasMoreElements();) {

			EOStructure structure = e.nextElement();
			
			NSMutableArray parametres = new NSMutableArray(4);
			// DADS V08R09 - vérification de la cohérence des structures. Une structure qui n'apparaît pas en S41 ou S70
			// ne doit pas être déclarée			
			if (structuresAvecSiren.containsObject(structure)) {

				System.out.println("AutomateDads.genererInformationStructures() STRUCTURE : " + structure.llStructure());

				parametres.addObject(structure);
				EOAdresse adresse = structure.adresseStructure();

				if (adresse == null) {
					EnvironnementDads.sharedInstance().ajouterErreur("L'adresse de la structure "+ structure.lcStructure() + " n'est pas une adresse valide");
				} else {
					parametres.addObject(adresse);
					if (adresse.pays().estFrance())  {
						if (adresse.codePostal() != null) {		// commune en France
							EOCommune commune = EOCommune.rechercherCommune(editingContext,adresse.codePostal(),adresse.ville());
							if (commune == null)  {
								EnvironnementDads.sharedInstance().ajouterErreur("Veuillez modifier la commune de la structure " + structure.lcStructure());
							}
						} else {
							EnvironnementDads.sharedInstance().ajouterErreur("Veuillez fournir le code postal dans l'adresse de la structure " + structure.lcStructure());
						}
					} else {
						if (adresse.pays().codeIso() == null) {
							EnvironnementDads.sharedInstance().ajouterErreur("Table PAYS incomplete, pas de code iso pour le pays " + adresse.pays().llPays());
						}
					}
					parametres.addObject(adresse.pays());
				}

				try {

					EOEffectifStructure effectif = structure.effectifPourAnnee(annee);
					int nbAgents = 0;
					// 29/01/08 - si pas d'agents dans la structure, pas de génération de la rubrique
					if (effectif != null && effectif.nbAgents() != null) {
						nbAgents = effectif.nbAgents().intValue();
						if (nbAgents > 0) {
							parametres.addObject(effectif);
							System.out
									.println("AutomateDads.genererInformationStructures() PREPARER RUBRIQUE !");
							preparerRubrique("Structure",parametres,editingContext,structure,null,null);

							if (estDadsComplete) {
								if (capricasSansSalarie) {
									preparerRubrique("IRC_SansSalarie",parametres,editingContext,structure,null,null);
									capricasSansSalarie = false;	// on l'utilise pour dire que la rubrique est générée
								}
								if (carcicasSansSalarie) {
									preparerRubrique("IRC_SansSalarie",parametres,editingContext,structure,null,null);
								}
							}
//							// V08R06
//							preparerRubrique("Taxe_Apprentissage",parametres,editingContext,structure,null,null);
//							// V08R06
//							preparerRubrique("Formation_Professionnelle",parametres,editingContext,structure,null,null);
							// V08R07
							preparerRubrique("Assujetissements_fiscaux",parametres,editingContext,structure,null,null);
						} else if (structuresAvecSiren.containsObject(structure)) {
							// DADS V08R09 - vérification de la cohérence des structures. Une structure qui apparaît en S41 ou S70 doit
							// avoir des effectifs déclarés
							throw new Exception("La structure " + structure.llStructure() + " n'a pas d'effectif déclaré pour l'année " + annee);
						}
					} else if (structuresAvecSiren.containsObject(structure)) {
						// DADS V08R09 - vérification de la cohérence des structures. Une structure qui apparaît en S41 ou S70 doit
						// avoir des effectifs déclarés
						throw new Exception("La structure " + structure.llStructure() + " n'a pas d'effectif déclaré pour l'année " + annee);
					}
										
				} catch (Exception e1) {
					informerThread("Preparation des donnees de Structure - EXCEPTION !!!! ");
					e1.printStackTrace();
					signalerExceptionThread(e1);
				}
			}
		}
		//editingContext.saveChanges();	// sauvegarder toutes les rubriques générées s80
	}
	private void genererEnregistrementFinal(EOEditingContext editingContext) {

		informerThread("Préparation enregistrement final" + getHour());

		NSMutableArray parametres = new NSMutableArray(this);
		nbEnregistrementsPourRubrique = 0;
		try {
			preparerRubrique("Recapitulatif",parametres,editingContext,null,null,null);
			//editingContext.saveChanges();	// sauvegarder la rubrique générée S90
			System.out.println("AutomateDads.genererEnregistrementFinal() FIN");
		} catch (Exception e) {
			signalerExceptionThread(e);
			e.printStackTrace();
		}
	}
	// Gestion des périodes
	// On vérifie si il y a au moins un montant brut ou net imposable à déclarer ie montant positif ou pas deux montants qui s'annulent.
	// Dans le cas où on n'en trouve pas, on vérifie dans les historiques si il y en a un avec une base imposable
	private boolean verifierPeriodes(EOEditingContext editingContext,EOIndividu agent,IndividuDads agentDADS) {
		boolean periodesOK = false;
		for (Enumeration<PeriodeDadsActive> e = agentDADS.periodesActivites().objectEnumerator();e.hasMoreElements();) {
			PeriodeDadsActive periode = e.nextElement();
			// il y a au moins une somme brute pour cet agent
			if (periode.baseBrutImposable() != null && new BigDecimal(periode.baseBrutImposable()).doubleValue() != 0) {
				periodesOK = true;
				break;
			} else {
				if ((new BigDecimal(periode.montantNetImposable())).doubleValue() == 0)	{	// 29/01/08 cas des agents ayant deux BS qui s'annulent
					//	23/02/07 - cas des régulations a posteriori de cotisations 
					// Vérifier dans les historiques si il n'y a pas de base imposable non nulle
					for (Enumeration<EOPayeHisto> e1 = periode.historiquesPaye().objectEnumerator();e1.hasMoreElements();) {
						EOPayeHisto historique = e1.nextElement();
						if (historique.payeBimpmois() != null && historique.payeBimpmois().doubleValue() != 0) {
							periodesOK = true;
							break;
						}
					}
				}
				if (periodesOK)
					break;
			}
		}

		if (!periodesOK) {
			NSArray periodesInactivites = EOSituationParticuliere.situationsParticulieresPourIndividu(editingContext,agent,anneeDads);
			periodesOK = (periodesInactivites.count() > 0);
		}
		if (!periodesOK) // 11/02/08	- pour être sur que les bulletins contenant les rappels soient pris en compte
			periodesOK = EODadsAgents.dadsObligatoirePourIndividu(editingContext, agent,anneeDads);

		return periodesOK;
	}

	// Regroupe les périodes en créant un nouveau sous-groupe chaque fois qu'on rencontre une période conflictuelle
	private NSArray organiserPeriodes(IndividuDads agentDADS) {

		NSMutableArray groupePeriodes = new NSMutableArray();
		NSMutableArray sousGroupe = new NSMutableArray();
		for (Enumeration<PeriodeDadsActive> e = agentDADS.periodesActivites().objectEnumerator();e.hasMoreElements();) {
			PeriodeDadsActive periode = e.nextElement();
			if (periode.estConflictuelle()) {
				// si un sousGroupe est déjé constitué, l'ajouter au groupe de périodes
				if (sousGroupe.count() > 0) {
					groupePeriodes.addObject(sousGroupe);
					sousGroupe = new NSMutableArray();
				}
				sousGroupe.addObject(periode);	// on isole les périodes conflictuelles dans un sous-groupe
				groupePeriodes.addObject(sousGroupe);
				sousGroupe = new NSMutableArray();
			} else {
				sousGroupe.addObject(periode);
			}
		}
		// ajouter le dernier sousGroupe
		groupePeriodes.addObject(sousGroupe);
		return groupePeriodes;
	}
	private void verifierChevauchementSituations(NSArray situations) throws Exception {
		// Trier par ordre croissant
		situations = EOSortOrdering.sortedArrayUsingKeyOrderArray(situations, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
		EOSituationParticuliere situation1 = (EOSituationParticuliere)situations.objectAtIndex(0);	// il y en a plus d'une
		NSTimestamp dateDebut = situation1.dateDebut(), dateFin = situation1.dateFin();
		for (int i = 1;i < situations.count();i++) {
			EOSituationParticuliere situation2 = (EOSituationParticuliere)situations.objectAtIndex(i);
			// les situations sont classées par date de début croissant, on regarde donc si la situation2
			// se termine avant la situation1 ou commence aprés (si dateFin est nulle)
			if (dateFin != null) {
				if (DateCtrl.isBefore(situation2.dateDebut(),dateFin)) {
					throw new Exception("Erreur : deux situations particulières pour l'Ircantec se chevauchent");
				} else {
					dateDebut = situation2.dateDebut();
					dateFin = situation2.dateFin();
				}
			} else {
				if (DateCtrl.isBefore(dateDebut,situation2.dateDebut())) {
					throw new Exception("Erreur : deux situations particulières pour l'Ircantec se chevauchent");
				} else {
					dateDebut = situation2.dateDebut();
				}
			}
		}
	}
	private void verifierMotifsSituationsParticulieres(NSArray situations) throws Exception {
		if (situations.count() <= 1) {	// Pour gérer la récursivité
			return;
		}
		EOSituationParticuliere situation1 = (EOSituationParticuliere)situations.objectAtIndex(0);
		for (int i = 1;i < situations.count();i++) {
			EOSituationParticuliere situation2 = (EOSituationParticuliere)situations.objectAtIndex(i);
			if (DateCtrl.isBefore(situation2.dateDebut(),situation1.dateFin())) {
				// Chevauchement, vérifier les motifs
				if (situation1.codeMotif().equals(situation2.codeMotif())) {
					throw new Exception("Erreur : deux situations particulières qui se chevauchent ont le même motif");
				}
			} else {
				// Vérifier les situations suivantes en comparant à la première
				if (situations.count() > 2) {
					verifierMotifsSituationsParticulieres(situations.subarrayWithRange(new NSRange(1,situations.count())));
					break;
				} else {
					break;
				}
			}
		}
	}
	private boolean anterieurUnJour(NSTimestamp tempsFin,NSTimestamp tempsDebut) {
		GregorianCalendar dateFin = new GregorianCalendar();
		dateFin.setTime(tempsFin);
		GregorianCalendar dateDebut = new GregorianCalendar();
		dateDebut.setTime(tempsDebut);
		int anneeDebut = dateDebut.get(Calendar.YEAR);
		int jourDebut = dateDebut.get(Calendar.DAY_OF_YEAR);
		int anneeFin = dateFin.get(Calendar.YEAR);
		int jourFin = dateFin.get(Calendar.DAY_OF_YEAR);
		if (anneeDebut == anneeFin) {
			if ((jourDebut == jourFin) || (jourDebut == (jourFin + 1))) {
				return true;
			} else {
				return false;
			}
		} else if (anneeDebut == (anneeFin + 1)) {
			if (dateFin.get(Calendar.MONTH) == 11 && dateFin.get(Calendar.DAY_OF_MONTH) == 31 && jourDebut == 1) {
				// date fin : 31 décembre et date début : 1er janvier
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	// Calcule et écrit les rubriques dans la base et dans le fichier texte
	private void preparerRubrique(String nomRubrique,NSArray parametres,EOEditingContext editingContext,EOStructure structure,PeriodeDadsActive periodeActive,PeriodeDadsInactive periodeInactive)  {
		EOIndividu individu = null;
		if (periodeActive != null) {
			individu = periodeActive.contrat().individu();
		} else if (periodeInactive != null) {
			individu = periodeInactive.individu();
		}
		NSArray resultats = calculerRubrique(nomRubrique, parametres,individu);
		if (resultats != null && resultats.count() > 0) {
			RubriqueDads rubrique = (RubriqueDads)listeRubriques.rubriquePourNom(nomRubrique);
			if (rubrique.estSauvegardee()) {
				// Créer les éléments et préparer des records pour les éléments qui doivent étre sauvegardés dans la base
				for (Enumeration<SousRubriqueAvecValeur> e = resultats.objectEnumerator();e.hasMoreElements();) {
					SousRubriqueAvecValeur resultat = e.nextElement();
					nbEnregistrementsPourRubrique++;
					EODadsFichiers record = new EODadsFichiers();
					record.initAvec(editingContext,resultat,exerciceCourant,structure,periodeActive,periodeInactive);
					record.setTypeDads(typeDads());
					record.setDafiClassement(new Integer(nbEnregistrementsPourRubrique));
					valeursDads.addObject(record);
				}
			}
			nbEnregistrements += resultats.count();	// incrémenter le nombre d'enregistrements générés
			try {
				FichierDads.ecrireFichier(resultats);
			} catch (Exception exc) {
				exc.printStackTrace();
				EnvironnementDads.sharedInstance().ajouterErreur(exc.getMessage());
			}
		}
	} 
	private NSArray calculerRubrique(String nomRubrique,NSArray parametres,EOIndividu individu) {

		if (parametres == null)
			parametres = new NSArray();

		NSMutableArray classes = new NSMutableArray(parametres.count());
		for (Enumeration e = parametres.objectEnumerator();e.hasMoreElements();)
			classes.addObject(e.nextElement().getClass().getName());

		RubriqueDads rubrique = (RubriqueDads)listeRubriques.rubriquePourNom(nomRubrique);
		try {
			NSArray resultats = rubrique.preparer(parametres.objects(),classes.objects());
			if (rubrique.getExceptions().equals("") == false) {
				if (individu != null) {
					EnvironnementDads.sharedInstance().ajouterErreur("*************************\n"+individu.identite());
				}
				EnvironnementDads.sharedInstance().ajouterErreur("\tPreparation de la rubrique " + rubrique.cle() + rubrique.getExceptions());
			}
			return resultats;
		} catch (Exception e1) {
			EnvironnementDads.sharedInstance().ajouterErreur(e1.getMessage());
			return null;
		}
	}
	private int clePrimairePourInsert(EOEditingContext editingContext) {
		// Préparation de la clé primaire pour la table FichiersDADS
		NSArray results = EOUtilities.rawRowsForSQL(editingContext, "Papaye", "SELECT JEFY_PAYE.DADS_FICHIERS_SEQ.NEXTVAL FROM dual",null);
		NSDictionary dict = (NSDictionary)results.objectAtIndex(0);
		return ((Number)dict.objectForKey("NEXTVAL")).intValue();
	}

	// Génération des données dans la base
	private void enregistrerDonnees(String nomModele) {
		EOEditingContext editingContext = new EOEditingContext();
		int nbLignes = 0,totalLignes = valeursDads.count();
		try {
			editingContext.lock();
			informerThread("Enregistrement des donnees");
			for (Enumeration<EODadsFichiers> e = valeursDads.objectEnumerator();e.hasMoreElements();) {
				EODadsFichiers valeur = e.nextElement();
				int clePrimaire = clePrimairePourInsert(editingContext);
				EOUtilities.rawRowsForSQL(editingContext, nomModele, buildSQLString(valeur,clePrimaire), null);
				nbLignes++;
				if (nbLignes % 750 == 0) {
					EOUtilities.rawRowsForSQL(editingContext, nomModele,"COMMIT",null);
					informerThread("Enregistrement des donnnées : lignes " + nbLignes + "/" + totalLignes);
				}
			}
			informerThread("Enregistrement des donnnées : lignes " + nbLignes + "/" + totalLignes);
			EOUtilities.rawRowsForSQL(editingContext, nomModele,"COMMIT",null);
			informerThread("Fin Enregistrement des donnnées");
		} catch (Exception e1) {
			signalerExceptionThread(e1);
		} finally {
			editingContext.unlock();
		}
	}
	private String buildSQLString(EODadsFichiers dadsFichier,int clePrimaire) {
		String texte = "INSERT INTO JEFY_PAYE.DADS_FICHIERS " +
		"(DAFI_ID,EXE_ORDRE,NO_INDIVIDU,C_STRUCTURE,CODE_RUBRIQUE,DAFI_VALEUR_RUBRIQUE,DAFI_CLASSEMENT,DAFI_PERIODE_DEBUT," +
		"DAFI_PERIODE_FIN,TYPE_DADS) VALUES (";
		texte += new Integer(clePrimaire)  + "," + new Integer(dadsFichier.exeOrdre().intValue()) + ",";
		if (dadsFichier.noIndividu() != null) {
			texte += new Integer(dadsFichier.noIndividu().intValue());
		} else {
			texte += "NULL";
		}
		texte += "," + buildString(dadsFichier.cStructure()) + ",'" + dadsFichier.codeRubrique() + "'," + buildString(dadsFichier.dafiValeurRubrique()) + "," + 
		new Integer(dadsFichier.dafiClassement().intValue()) + "," + buildString(dadsFichier.dafiPeriodeDebut()) + "," + 
		buildString(dadsFichier.dafiPeriodeFin()) + ",'" + dadsFichier.typeDads() + "')";
		return texte;
	}
	private String buildString(String valeur) {
		if (valeur == null) {
			return "NULL";
		} else {
			if (valeur.indexOf("'") >= 0) {
				valeur = valeur.replaceAll("'","''");	// pour éviter des erreurs dans les INSERT
			}
			return "'" + valeur + "'";
		}
	}
	// Gestion des logs
	// envoie un message au thread courant
	private void informerThread(String message) {
		if (threadCourant != null && message != null) {
			threadCourant.setMessage(message);
			System.out.println(message);
		}
	}
	// signale une exception au thread courant */
	private void signalerExceptionThread(Exception e) {
		//e.printStackTrace();
		if (threadCourant != null && e.getMessage() != null) {
			threadCourant.setException(e.getMessage());
		}
	}
}
