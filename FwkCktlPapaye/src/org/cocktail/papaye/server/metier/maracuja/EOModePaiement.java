/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.papaye.server.metier.maracuja;


import org.cocktail.papaye.server.metier.jefy_admin.EOExercice;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParametres;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public class EOModePaiement extends _EOModePaiement {
	
	public static final String DEFAULT_MODE_PAIEMENT = "30";	// BANQUE 
	private static final String VIREMENT = "VIREMENT";


	public boolean estVirement() {

		if (modDom() == null)
			return false;

		return modDom().equals(VIREMENT);
	}
	/** Mode de paiement par defaut */
	public static EOModePaiement getDefaultModePaiementPourAnnee(EOEditingContext ec, Number anneeExercice) {

		try {
			
			EOPayeParametres parametreModePaiement = EOPayeParametres.rechercherParametre(ec, "DEFAULT_MODE_PAIEMENT");
			NSMutableArray qualifiers = new NSMutableArray();

			if (parametreModePaiement != null && parametreModePaiement.paramValue() != null)				
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_CODE_KEY + " = %@", new NSArray( parametreModePaiement.paramValue())));
			else
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_CODE_KEY + " = %@", new NSArray(DEFAULT_MODE_PAIEMENT)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY + " = %@", new NSArray(anneeExercice)));

			EOFetchSpecification fs = new EOFetchSpecification(EOModePaiement.ENTITY_NAME, new EOAndQualifier(qualifiers),null);

			return (EOModePaiement)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {
			e.printStackTrace(); return null;
		}
	}
	public static EOModePaiement getModePaiementForCodeEtAnnee(EOEditingContext ec,String code, Number anneeExercice) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_CODE_KEY + " = %@", new NSArray(DEFAULT_MODE_PAIEMENT)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY + " = %@", new NSArray(anneeExercice)));

			EOFetchSpecification fs = new EOFetchSpecification(EOModePaiement.ENTITY_NAME,new EOAndQualifier(qualifiers),null);
			return (EOModePaiement)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {return null;}
	}
	/** Mode de paiement par defaut (EOBanque) pour l'annee courante */
	public static EOModePaiement getDefaultModePaiement(EOEditingContext ec) {
		int anneeCourante = org.cocktail.papaye.server.common.DateCtrl.getYear(new NSTimestamp());
		return getDefaultModePaiementPourAnnee(ec,new Integer(anneeCourante));
	}
	/** Mode de paiement pour le code passe en parametre pour l'annee courante */
	public static EOModePaiement getModePaiementForCode(EOEditingContext ec,String code) {
		int anneeCourante = org.cocktail.papaye.server.common.DateCtrl.getYear(new NSTimestamp());
		return getModePaiementForCodeEtAnnee(ec,code,new Integer(anneeCourante));
	}
}
