// _EOStructure.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStructure.java instead.
package org.cocktail.papaye.server.metier.grhum;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOStructure extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Structure";
	public static final String ENTITY_TABLE_NAME = "GRHUM.structure_ulr";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cStructure";

	public static final String APE_CODE_KEY = "apeCode";
	public static final String C_NAF_KEY = "cNaf";
	public static final String CODE_URSSAF_KEY = "codeUrssaf";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String C_STRUCTURE_PERE_KEY = "cStructurePere";
	public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String GRP_ACCES_KEY = "grpAcces";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LL_STRUCTURE_KEY = "llStructure";
	public static final String NUM_ASSEDIC_KEY = "numAssedic";
	public static final String NUM_CNRACL_KEY = "numCNRACL";
	public static final String NUM_IRCANTEC_KEY = "numIrcantec";
	public static final String NUM_RAFP_KEY = "numRAFP";
	public static final String NUM_URSSAF_KEY = "numUrssaf";
	public static final String PERS_ID_KEY = "persId";
	public static final String RISQUE_ACC_TRAV_KEY = "risqueAccTrav";
	public static final String SIREN_KEY = "siren";
	public static final String SIRET_KEY = "siret";
	public static final String TAUX_ACC_TRAV_KEY = "tauxAccTrav";
	public static final String TAUX_EXONERATION_TVA_KEY = "tauxExonerationTVA";
	public static final String TAUX_IR_KEY = "tauxIR";
	public static final String TAUX_TRANSPORT_KEY = "tauxTransport";
	public static final String TEM_COTIS_ASSEDIC_KEY = "temCotisAssedic";
	public static final String TEM_DADS_KEY = "temDads";
	public static final String TEM_ETABLISSEMENT_PAYE_KEY = "temEtablissementPaye";
	public static final String TEM_PLAFOND_REDUIT_KEY = "temPlafondReduit";
	public static final String TEM_SECTORISE_KEY = "temSectorise";
	public static final String TEM_SOUMIS_TVA_KEY = "temSoumisTVA";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String C_RNE_KEY = "cRne";

//Colonnes dans la base de donnees
	public static final String APE_CODE_COLKEY = "GRP_APE_CODE";
	public static final String C_NAF_COLKEY = "C_NAF";
	public static final String CODE_URSSAF_COLKEY = "etab_codurssaf";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String C_STRUCTURE_PERE_COLKEY = "C_STRUCTURE_PERE";
	public static final String C_TYPE_STRUCTURE_COLKEY = "C_TYPE_STRUCTURE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String GRP_ACCES_COLKEY = "GRP_ACCES";
	public static final String LC_STRUCTURE_COLKEY = "LC_STRUCTURE";
	public static final String LL_STRUCTURE_COLKEY = "LL_STRUCTURE";
	public static final String NUM_ASSEDIC_COLKEY = "NUM_ASSEDIC";
	public static final String NUM_CNRACL_COLKEY = "num_cnracl";
	public static final String NUM_IRCANTEC_COLKEY = "NUM_IRCANTEC";
	public static final String NUM_RAFP_COLKEY = "num_rafp";
	public static final String NUM_URSSAF_COLKEY = "etab_numurssaf";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String RISQUE_ACC_TRAV_COLKEY = "risque_acc_trav";
	public static final String SIREN_COLKEY = "SIREN";
	public static final String SIRET_COLKEY = "SIRET";
	public static final String TAUX_ACC_TRAV_COLKEY = "taux_acc_trav";
	public static final String TAUX_EXONERATION_TVA_COLKEY = "taux_exoneration_tva";
	public static final String TAUX_IR_COLKEY = "taux_ir";
	public static final String TAUX_TRANSPORT_COLKEY = "taux_transport";
	public static final String TEM_COTIS_ASSEDIC_COLKEY = "TEM_COTIS_ASSEDIC";
	public static final String TEM_DADS_COLKEY = "TEM_DADS";
	public static final String TEM_ETABLISSEMENT_PAYE_COLKEY = "TEM_ETABLISSEMENT_PAYE";
	public static final String TEM_PLAFOND_REDUIT_COLKEY = "TEM_PLAFOND_REDUIT";
	public static final String TEM_SECTORISE_COLKEY = "TEM_SECTORISE";
	public static final String TEM_SOUMIS_TVA_COLKEY = "TEM_SOUMIS_TVA";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String C_RNE_COLKEY = "C_RNE";


	// Relationships
	public static final String APE_KEY = "ape";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String REPART_PERSONNE_ADRESSES_KEY = "repartPersonneAdresses";
	public static final String REPART_TYPE_GROUPES_KEY = "repartTypeGroupes";
	public static final String RNE_KEY = "rne";
	public static final String STRUCTURE_PARENT_KEY = "structureParent";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String apeCode() {
    return (String) storedValueForKey(APE_CODE_KEY);
  }

  public void setApeCode(String value) {
    takeStoredValueForKey(value, APE_CODE_KEY);
  }

  public String cNaf() {
    return (String) storedValueForKey(C_NAF_KEY);
  }

  public void setCNaf(String value) {
    takeStoredValueForKey(value, C_NAF_KEY);
  }

  public String codeUrssaf() {
    return (String) storedValueForKey(CODE_URSSAF_KEY);
  }

  public void setCodeUrssaf(String value) {
    takeStoredValueForKey(value, CODE_URSSAF_KEY);
  }

  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public String cStructurePere() {
    return (String) storedValueForKey(C_STRUCTURE_PERE_KEY);
  }

  public void setCStructurePere(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_PERE_KEY);
  }

  public String cTypeStructure() {
    return (String) storedValueForKey(C_TYPE_STRUCTURE_KEY);
  }

  public void setCTypeStructure(String value) {
    takeStoredValueForKey(value, C_TYPE_STRUCTURE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String grpAcces() {
    return (String) storedValueForKey(GRP_ACCES_KEY);
  }

  public void setGrpAcces(String value) {
    takeStoredValueForKey(value, GRP_ACCES_KEY);
  }

  public String lcStructure() {
    return (String) storedValueForKey(LC_STRUCTURE_KEY);
  }

  public void setLcStructure(String value) {
    takeStoredValueForKey(value, LC_STRUCTURE_KEY);
  }

  public String llStructure() {
    return (String) storedValueForKey(LL_STRUCTURE_KEY);
  }

  public void setLlStructure(String value) {
    takeStoredValueForKey(value, LL_STRUCTURE_KEY);
  }

  public String numAssedic() {
    return (String) storedValueForKey(NUM_ASSEDIC_KEY);
  }

  public void setNumAssedic(String value) {
    takeStoredValueForKey(value, NUM_ASSEDIC_KEY);
  }

  public String numCNRACL() {
    return (String) storedValueForKey(NUM_CNRACL_KEY);
  }

  public void setNumCNRACL(String value) {
    takeStoredValueForKey(value, NUM_CNRACL_KEY);
  }

  public String numIrcantec() {
    return (String) storedValueForKey(NUM_IRCANTEC_KEY);
  }

  public void setNumIrcantec(String value) {
    takeStoredValueForKey(value, NUM_IRCANTEC_KEY);
  }

  public String numRAFP() {
    return (String) storedValueForKey(NUM_RAFP_KEY);
  }

  public void setNumRAFP(String value) {
    takeStoredValueForKey(value, NUM_RAFP_KEY);
  }

  public String numUrssaf() {
    return (String) storedValueForKey(NUM_URSSAF_KEY);
  }

  public void setNumUrssaf(String value) {
    takeStoredValueForKey(value, NUM_URSSAF_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public String risqueAccTrav() {
    return (String) storedValueForKey(RISQUE_ACC_TRAV_KEY);
  }

  public void setRisqueAccTrav(String value) {
    takeStoredValueForKey(value, RISQUE_ACC_TRAV_KEY);
  }

  public String siren() {
    return (String) storedValueForKey(SIREN_KEY);
  }

  public void setSiren(String value) {
    takeStoredValueForKey(value, SIREN_KEY);
  }

  public String siret() {
    return (String) storedValueForKey(SIRET_KEY);
  }

  public void setSiret(String value) {
    takeStoredValueForKey(value, SIRET_KEY);
  }

  public String tauxAccTrav() {
    return (String) storedValueForKey(TAUX_ACC_TRAV_KEY);
  }

  public void setTauxAccTrav(String value) {
    takeStoredValueForKey(value, TAUX_ACC_TRAV_KEY);
  }

  public String tauxExonerationTVA() {
    return (String) storedValueForKey(TAUX_EXONERATION_TVA_KEY);
  }

  public void setTauxExonerationTVA(String value) {
    takeStoredValueForKey(value, TAUX_EXONERATION_TVA_KEY);
  }

  public String tauxIR() {
    return (String) storedValueForKey(TAUX_IR_KEY);
  }

  public void setTauxIR(String value) {
    takeStoredValueForKey(value, TAUX_IR_KEY);
  }

  public String tauxTransport() {
    return (String) storedValueForKey(TAUX_TRANSPORT_KEY);
  }

  public void setTauxTransport(String value) {
    takeStoredValueForKey(value, TAUX_TRANSPORT_KEY);
  }

  public String temCotisAssedic() {
    return (String) storedValueForKey(TEM_COTIS_ASSEDIC_KEY);
  }

  public void setTemCotisAssedic(String value) {
    takeStoredValueForKey(value, TEM_COTIS_ASSEDIC_KEY);
  }

  public String temDads() {
    return (String) storedValueForKey(TEM_DADS_KEY);
  }

  public void setTemDads(String value) {
    takeStoredValueForKey(value, TEM_DADS_KEY);
  }

  public String temEtablissementPaye() {
    return (String) storedValueForKey(TEM_ETABLISSEMENT_PAYE_KEY);
  }

  public void setTemEtablissementPaye(String value) {
    takeStoredValueForKey(value, TEM_ETABLISSEMENT_PAYE_KEY);
  }

  public String temPlafondReduit() {
    return (String) storedValueForKey(TEM_PLAFOND_REDUIT_KEY);
  }

  public void setTemPlafondReduit(String value) {
    takeStoredValueForKey(value, TEM_PLAFOND_REDUIT_KEY);
  }

  public String temSectorise() {
    return (String) storedValueForKey(TEM_SECTORISE_KEY);
  }

  public void setTemSectorise(String value) {
    takeStoredValueForKey(value, TEM_SECTORISE_KEY);
  }

  public String temSoumisTVA() {
    return (String) storedValueForKey(TEM_SOUMIS_TVA_KEY);
  }

  public void setTemSoumisTVA(String value) {
    takeStoredValueForKey(value, TEM_SOUMIS_TVA_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EOApe ape() {
    return (org.cocktail.papaye.server.metier.grhum.EOApe)storedValueForKey(APE_KEY);
  }

  public void setApeRelationship(org.cocktail.papaye.server.metier.grhum.EOApe value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOApe oldValue = ape();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, APE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, APE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EORne rne() {
    return (org.cocktail.papaye.server.metier.grhum.EORne)storedValueForKey(RNE_KEY);
  }

  public void setRneRelationship(org.cocktail.papaye.server.metier.grhum.EORne value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EORne oldValue = rne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RNE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOStructure structureParent() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_PARENT_KEY);
  }

  public void setStructureParentRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structureParent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_PARENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_PARENT_KEY);
    }
  }
  
  public NSArray fournisseur() {
    return (NSArray)storedValueForKey(FOURNISSEUR_KEY);
  }

  public NSArray fournisseur(EOQualifier qualifier) {
    return fournisseur(qualifier, null);
  }

  public NSArray fournisseur(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = fournisseur();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToFournisseurRelationship(org.cocktail.papaye.server.metier.grhum.EOFournis object) {
    addObjectToBothSidesOfRelationshipWithKey(object, FOURNISSEUR_KEY);
  }

  public void removeFromFournisseurRelationship(org.cocktail.papaye.server.metier.grhum.EOFournis object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNISSEUR_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EOFournis createFournisseurRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fournis");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, FOURNISSEUR_KEY);
    return (org.cocktail.papaye.server.metier.grhum.EOFournis) eo;
  }

  public void deleteFournisseurRelationship(org.cocktail.papaye.server.metier.grhum.EOFournis object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNISSEUR_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFournisseurRelationships() {
    Enumeration objects = fournisseur().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFournisseurRelationship((org.cocktail.papaye.server.metier.grhum.EOFournis)objects.nextElement());
    }
  }

  public NSArray repartPersonneAdresses() {
    return (NSArray)storedValueForKey(REPART_PERSONNE_ADRESSES_KEY);
  }

  public NSArray repartPersonneAdresses(EOQualifier qualifier) {
    return repartPersonneAdresses(qualifier, null);
  }

  public NSArray repartPersonneAdresses(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = repartPersonneAdresses();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToRepartPersonneAdressesRelationship(org.cocktail.papaye.server.metier.grhum.EORepartPersonneAdresse object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REPART_PERSONNE_ADRESSES_KEY);
  }

  public void removeFromRepartPersonneAdressesRelationship(org.cocktail.papaye.server.metier.grhum.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_PERSONNE_ADRESSES_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EORepartPersonneAdresse createRepartPersonneAdressesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartPersonneAdresse");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_PERSONNE_ADRESSES_KEY);
    return (org.cocktail.papaye.server.metier.grhum.EORepartPersonneAdresse) eo;
  }

  public void deleteRepartPersonneAdressesRelationship(org.cocktail.papaye.server.metier.grhum.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_PERSONNE_ADRESSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRepartPersonneAdressesRelationships() {
    Enumeration objects = repartPersonneAdresses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepartPersonneAdressesRelationship((org.cocktail.papaye.server.metier.grhum.EORepartPersonneAdresse)objects.nextElement());
    }
  }

  public NSArray repartTypeGroupes() {
    return (NSArray)storedValueForKey(REPART_TYPE_GROUPES_KEY);
  }

  public NSArray repartTypeGroupes(EOQualifier qualifier) {
    return repartTypeGroupes(qualifier, null, false);
  }

  public NSArray repartTypeGroupes(EOQualifier qualifier, boolean fetch) {
    return repartTypeGroupes(qualifier, null, fetch);
  }

  public NSArray repartTypeGroupes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.grhum.EORepartTypeGroupe.STRUCTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.grhum.EORepartTypeGroupe.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = repartTypeGroupes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRepartTypeGroupesRelationship(org.cocktail.papaye.server.metier.grhum.EORepartTypeGroupe object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REPART_TYPE_GROUPES_KEY);
  }

  public void removeFromRepartTypeGroupesRelationship(org.cocktail.papaye.server.metier.grhum.EORepartTypeGroupe object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_TYPE_GROUPES_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EORepartTypeGroupe createRepartTypeGroupesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartTypeGroupe");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_TYPE_GROUPES_KEY);
    return (org.cocktail.papaye.server.metier.grhum.EORepartTypeGroupe) eo;
  }

  public void deleteRepartTypeGroupesRelationship(org.cocktail.papaye.server.metier.grhum.EORepartTypeGroupe object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_TYPE_GROUPES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRepartTypeGroupesRelationships() {
    Enumeration objects = repartTypeGroupes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepartTypeGroupesRelationship((org.cocktail.papaye.server.metier.grhum.EORepartTypeGroupe)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOStructure avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOStructure createEOStructure(EOEditingContext editingContext, String cStructure
, String cTypeStructure
, String llStructure
, String temCotisAssedic
, String temDads
, String temSectorise
, String temSoumisTVA
, String temValide
			) {
    EOStructure eo = (EOStructure) createAndInsertInstance(editingContext, _EOStructure.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setCTypeStructure(cTypeStructure);
		eo.setLlStructure(llStructure);
		eo.setTemCotisAssedic(temCotisAssedic);
		eo.setTemDads(temDads);
		eo.setTemSectorise(temSectorise);
		eo.setTemSoumisTVA(temSoumisTVA);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOStructure localInstanceIn(EOEditingContext editingContext) {
	  		return (EOStructure)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOStructure creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOStructure creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOStructure object = (EOStructure)createAndInsertInstance(editingContext, _EOStructure.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOStructure localInstanceIn(EOEditingContext editingContext, EOStructure eo) {
    EOStructure localInstance = (eo == null) ? null : (EOStructure)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOStructure#localInstanceIn a la place.
   */
	public static EOStructure localInstanceOf(EOEditingContext editingContext, EOStructure eo) {
		return EOStructure.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOStructure fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStructure fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOStructure eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOStructure)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOStructure fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOStructure fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOStructure eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOStructure)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOStructure fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOStructure eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOStructure ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOStructure fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
