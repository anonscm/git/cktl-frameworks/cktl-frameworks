/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.papaye.server.metier.grhum;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;


public class EOCommune extends _EOCommune {

    public EOCommune() {
        super();
    }

    public static EOCommune rechercherCommune(EOEditingContext editingContext,String codePostal,String nom) {
   	
    		try {	// exception si pas de nom de commune pour l'agent

    	    	NSMutableArray mesQualifiers = new NSMutableArray();
    	    	NSMutableArray mesOrQualifiers = new NSMutableArray();

    			if (nom != null) {

        			// Preparation du libelle de la commune
        			String nomCommune = nom.toUpperCase();
        			int position = nomCommune.indexOf("CEDEX");
        			if (position > 0)
        				nomCommune = nomCommune.substring(0,position - 1);	
        			// supprimer aussi les espaces en début et en fin de chaîne
        			nomCommune = nomCommune.trim();

    				mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCommune.LL_COM_KEY + " = %@",
    						new NSArray(nomCommune)));

    				mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCommune.LC_COM_KEY + " = %@",
    						new NSArray(nomCommune)));

    				mesQualifiers.addObject(new EOOrQualifier(mesOrQualifiers));

    			}

    	    	if (codePostal != null)
    				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCommune.C_POSTAL_KEY + " = %@",
    										new NSArray(codePostal)));
    			    			        		
        		return fetchFirstByQualifier(editingContext, new EOAndQualifier(mesQualifiers));
        		        		
        } catch (Exception e) {
        		return null;
        }
    }
    
    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}
