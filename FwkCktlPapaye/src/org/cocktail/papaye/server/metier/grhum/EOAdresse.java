/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.papaye.server.metier.grhum;



import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EOAdresse extends _EOAdresse {

	public EOAdresse() {
		super();
	}

	// méthodes ajoutées
	public String localite() {
		if (adrAdresse2() != null && estCommune(adrAdresse2())) {
			return adrAdresse2().toUpperCase();
		} else if (ville() != null) {
			return ville().toUpperCase();
		} else {
			return null;
		}
	}
	public String bureauDistributeur() {
		if (ville() != null) {
			return ville().toUpperCase();
		} else {
			return null;
		}
	}
	public String hexaCle() {
		return null;
	}
	
	public String codeDistribEtranger() {
		
		if (!pays().cPays().equals(EOPays.CODE_PAYS_DEFAUT)) {    // FRANCE

			if (cpEtranger() != null)
				return cpEtranger();
			else
				if (codePostal() != null)
					return codePostal();				
			
			return "0";
		}
		
		return null;
		
	}

	
	/** retourne le code postal ou le code postal etranger selon les cas. Le code postal etranger peut comporter juste le code ISO pays */
	public String cPostal() {
		if (pays().cPays().equals(EOPays.CODE_PAYS_DEFAUT)) {    // FRANCE
			EOCommune commune = EOCommune.rechercherCommune(editingContext(),codePostal(), null);
			if (commune != null) {
				return commune.cPostalPrincipal();
			} else
				return null;
		}

		// ETRANGER
		return null;
//			if (codePostal() != null)
//				return codePostal();
//			else
//				if (cpEtranger() != null)
//					return cpEtranger();
//				else
//					return "0";
		
	}
	public String codeInseeCommune() {
		if (ville() == null) {
			return null;
		}
		EOCommune commune = EOCommune.rechercherCommune(editingContext(),codePostal(),ville());
		if (commune != null) {

			// Cas particulier de VARENNE SAINT HILAIRE pour lequel le code INSEE officiel n'est pas reconnu
			if (commune.cInsee().equals("94901"))
				return "94068";
			
			return commune.cInsee();
		} else {
			return null;
		}
	}
	/** retourne l'adresse a imprimer sur le bulletin de salaire */
	public String adresseBulletinSalaire() {
		return adrAdresse1() + "\n" + adrAdresse2();
	}
	public String toString() {
		return new String(getClass().getName() +
				"\nadresse1 : "+ adrAdresse1() +
				"\nadresse2 : "+ adrAdresse2() +
				"\nBP : "+ adrBp() +
				"\nville : "+ ville() +
				"\ncode postal : "+ codePostal() +
				"\ncp etranger : "+ cpEtranger() +
				"\npays : "+ pays() +
				"\nutilisee dans la paye : "+ temPayeUtil());
	} 
	/** initialisation d'une adresse */
	public void initAdresse(EOPays pays) {
		setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
		setTemPayeUtil("N");

		setPaysRelationship(pays);
	}
	// méthodes privées
	private boolean estCommune(String nom) {
		// rechercher la commune dans la table des communes
		NSArray values = new NSArray(nom.toUpperCase());
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
		("llCom = %@" ,values);
		EOFetchSpecification fs = new EOFetchSpecification("Commune",qualifier, null);
		fs.setFetchLimit(1);
		NSArray communes = editingContext().objectsWithFetchSpecification(fs);
		return communes.count() != 0;

	}
}
