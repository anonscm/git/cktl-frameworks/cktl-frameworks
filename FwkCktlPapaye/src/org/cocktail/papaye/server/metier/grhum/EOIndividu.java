/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.papaye.server.metier.grhum;

import java.util.Enumeration;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;


public class EOIndividu extends _EOIndividu {

	public static final String NO_INSEE_HOMME_INCONNU = "1999999999999";
	public static final String NO_INSEE_FEMME_INCONNU = "2999999999999";


	public EOIndividu() {
		super();
	}

	/** retourne une chaine de caracteres prenom nom */
	public String identite() {
		return prenom() + " " + nomUsuel();
	}

	public EOAdresse adresseCourante() {
		try {
			// déterminer la première adresse valide dans RepartAdressePersonne
			Enumeration e = repartPersonneAdresses().objectEnumerator();
			while (e.hasMoreElements()) {
				EORepartPersonneAdresse repart = (EORepartPersonneAdresse)e.nextElement();
				if (repart.rpaValide().equals(Constantes.VRAI) && repart.tadrCode().equals("PERSO")) {
					return repart.adresse();
				}
			}
			// si non trouvée
			return null;
		} catch (Exception e) { return null; }
	}


	public String lieuNaissanceDADS() {
		if (lieuNaissance() == null) {
			return null;
		}
		String lieu = lieuNaissance();
		// supprimer les / pour les remplacer par SUR
		int position = lieu.indexOf("/");
		if (position > 0) {
			lieu = lieu.substring(0,position) + "-SUR-" + lieu.substring(position+1);
		}
		// supprimer les noms de pays entre parenthèses
		position = lieu.indexOf("(");
		if (position >= 0) {
			int finParenthese = lieu.indexOf(")"); 
			if (finParenthese > 0 && finParenthese < lieu.length()) {
				lieu = lieu.substring(0,position) + lieu.substring(position + 1,finParenthese);
			} else {
				lieu = lieu.substring(0,position);
			}
		}
		return lieu;
	}

	public String nomIndividu() {
		if (nomPatronymique() != null && nomPatronymique().trim().equals("") == false)
			return nomPatronymique();
		else
			return nomUsuel();
	}

	public String noTelephoneProfessionnel() {

		try {

			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.PERS_ID_KEY + " = %@", 
					new NSArray(persId())));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TYPE_TEL_KEY + " = %@", 
					new NSArray("PRF")));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TYPE_NO_KEY + " = %@", 
					new NSArray("TEL")));

			return EOPersonneTelephone.fetchFirstByQualifier(editingContext(), new EOAndQualifier(mesQualifiers)).noTelephone();

		} catch (Exception e) {
			return null;
		}
	}
	/** retourne le premier numero de tel professionnel trouve ou null */
	public String noFaxProfessionnel() {

		try {

			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.PERS_ID_KEY + " = %@", 
					new NSArray(persId())));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TYPE_TEL_KEY + " = %@", 
					new NSArray("PRF")));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TYPE_NO_KEY + " = %@", 
					new NSArray("FAX")));

			return EOPersonneTelephone.fetchFirstByQualifier(editingContext(), new EOAndQualifier(mesQualifiers)).noTelephone();

		} catch (Exception e) {
			return null;
		}
	}

	public String paysNationalite() {
		try {
			return nationalite().llPays().toUpperCase();
		} catch(Exception e) {
			return null;
		}
	}

	/** retourne la civilite sous la forme voulue pour la DADS */
	public String civiliteDADS() {
		if (cCivilite().equals(Constantes.MONSIEUR)) {
			return "01";
		} 
		
		return "02";
	}

	
	public String paysNaissanceDADS() {
		
		try {
			if (departementNaissance().equals("99") == false)
				return "FRANCE";
			else {
				if (paysNaissance() != null && paysNaissance().llPays() != null && paysNaissance().llPays().toUpperCase().equals("FRANCE") == false)
					return paysNaissance().llPays().toUpperCase();
				else if (numeroInsee().equals(NO_INSEE_HOMME_INCONNU) == false && numeroInsee().equals(NO_INSEE_FEMME_INCONNU) == false) {
					String codePays = indNoInsee().substring(7,10);
					EOPays pays = EOPays.chercherPays(codePays,editingContext());
					return pays.llPays();
				} else
					return null;
			}
		} catch (Exception e) {	// pays = nul ou departement Naissance = nul
			return null;
		}
		
	}
	
	
	/** retourne le departement de naissance extrait du numero SS ou exception si numeroSS = null */
	public String departementNaissance() throws Exception {
		try {
			String departement = indNoInsee().substring(5,7);
			String annee = indNoInsee().substring(1,3);
			int anneeNaissance = new Integer(annee).intValue();
			if (departement.equals("96")) {	// département 96 (pays étranger sous protectorat = étranger)
				if (anneeNaissance >= 68) {	// ce numéro de département n'est valide que jusqu'en 1968
					departement = null;
				}
			} else if (departement.equals("20")) {
				if (anneeNaissance >= 76) {
					if (cDeptNaissance() != null)
						departement = cDeptNaissance().substring(1);
					else
						departement = null;
				}
			}
			return departement;
		} catch (Exception e) {
			throw new Exception("\tLe numero de departement de naissance est obligatoire. Veuillez fournir un numero Insee pour " + identite());
		}
	}

	public boolean paieSecu() {
		return personnel().temPaieSecu().equals(Constantes.VRAI);
	}

	public boolean estImposableEnFrance() {
		return personnel().temImposable().equals(Constantes.VRAI);
	}

	public EOGenericRecord fournis() {
		// id personne
		NSMutableArray values = new NSMutableArray(persId());
		// fournisseur valide
		values.addObject(Constantes.VRAI);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
		("persId = %@ AND fouValide = %@",values);
		EOFetchSpecification fs = new EOFetchSpecification(EOFournis.ENTITY_NAME,qualifier, null);
		NSArray fourniss = editingContext().objectsWithFetchSpecification(fs);
		try {
			return (EOGenericRecord)fourniss.objectAtIndex(0);
		} catch (Exception e) { return null; }
	}


	public EORibs ribCourant() throws Exception {
		EOGenericRecord fournis = this.fournis();
		if (fournis != null) {
			// id fournisseur
			NSMutableArray values = new NSMutableArray(fournis);
			NSArray sort = new NSArray(new EOSortOrdering(EORibs.TEM_PAYE_UTIL_KEY,EOSortOrdering.CompareDescending));

			// rib valide
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("fournisseur = %@ AND ribValide = 'O'",values);
			EOFetchSpecification fs = new EOFetchSpecification(EORibs.ENTITY_NAME,qualifier, sort);
			try {
				return (EORibs)editingContext().objectsWithFetchSpecification(fs).objectAtIndex(0);
			} catch (Exception e) { return null; }  // 4/12 pour autoriser le fait de ne pas avoir de rib courant
		}
		else  throw new Exception("Pas fournisseur pour " + identite());
	}


	public static NSArray chercherIndividusPourAnneeEtStructure(EOEditingContext editingContext, EOStructure structure, Number annee) {
		// périodes de l'année
		NSMutableArray values = new NSMutableArray(annee);
		// pour le mois générique
		values.addObject(new Integer(EOPayeMois.MOIS_GENERIQUE));
		// pour la structure
		values.addObject(structure);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(
				"(mois.moisAnnee = %@ OR mois.moisCode = %@) AND contrat.structure = %@",values);
		// trier les périodes sur les agents et les contrats, les périodes de mois de traitement et de mois ordre
		EOFetchSpecification fs = new EOFetchSpecification(EOPayePeriode.ENTITY_NAME,qualifier, null);
		NSArray prefetches = new NSArray("contrat.individu");
		fs.setPrefetchingRelationshipKeyPaths(prefetches);
		NSArray fetches = editingContext.objectsWithFetchSpecification(fs);
		NSMutableArray individus = new NSMutableArray();
		java.util.Enumeration e = fetches.objectEnumerator();
		while (e.hasMoreElements()) {
			EOIndividu individu = ((EOPayePeriode)e.nextElement()).contrat().individu();
			if (individus.containsObject(individu) == false) {
				individus.addObject(individu);
			}
		}
		NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey("nomUsuel",EOSortOrdering.CompareAscending));
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(individus,sorts);
	}



	public String numeroInsee() {
		if (indNoInsee() != null && indNoInsee().startsWith("7") == false && indNoInsee().startsWith("8") == false && indNoInsee().endsWith("000") == false) {
			String temp = indNoInsee().substring(5,7);
			if (temp.equals("20")) {	// Corse
				String annee = indNoInsee().substring(1,3);
				int anneeNaissance = new Integer(annee).intValue();
				if (anneeNaissance >= 76) {
					if (cDeptNaissance() != null) {
						String departement = cDeptNaissance().substring(1);
						return indNoInsee().substring(0,5) + departement + indNoInsee().substring(7);
					} else {
						return null;		// pour signaler qu'il n'y a pas de département de naissance pour un corse
					} 
				} else {
					return indNoInsee();
				}
			} else {
				return indNoInsee();
			}
		} else {
			if (cCivilite().equals(Constantes.MONSIEUR)) {
				return NO_INSEE_HOMME_INCONNU;
			} else {
				return NO_INSEE_FEMME_INCONNU;
			}
		}
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
