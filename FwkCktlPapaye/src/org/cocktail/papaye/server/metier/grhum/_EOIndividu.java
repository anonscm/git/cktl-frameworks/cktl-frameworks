// _EOIndividu.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOIndividu.java instead.
package org.cocktail.papaye.server.metier.grhum;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOIndividu extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Individu";
	public static final String ENTITY_TABLE_NAME = "GRHUM.individu_ulr";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noIndividu";

	public static final String C_CIVILITE_KEY = "cCivilite";
	public static final String C_DEPT_NAISSANCE_KEY = "cDeptNaissance";
	public static final String C_PAYS_NAISSANCE_KEY = "cPaysNaissance";
	public static final String C_PAYS_NATIONALITE_KEY = "cPaysNationalite";
	public static final String C_SITUATION_FAMILLE_KEY = "cSituationFamille";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String IND_ACTIVITE_KEY = "indActivite";
	public static final String IND_CLE_INSEE_KEY = "indCleInsee";
	public static final String IND_NO_INSEE_KEY = "indNoInsee";
	public static final String IND_QUALITE_KEY = "indQualite";
	public static final String LIEU_NAISSANCE_KEY = "lieuNaissance";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PERS_ID_KEY = "persId";
	public static final String PRENOM_KEY = "prenom";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_CIVILITE_COLKEY = "C_CIVILITE";
	public static final String C_DEPT_NAISSANCE_COLKEY = "C_DEPT_NAISSANCE";
	public static final String C_PAYS_NAISSANCE_COLKEY = "C_PAYS_NAISSANCE";
	public static final String C_PAYS_NATIONALITE_COLKEY = "C_PAYS_NATIONALITE";
	public static final String C_SITUATION_FAMILLE_COLKEY = "IND_C_SITUATION_FAMILLE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_NAISSANCE_COLKEY = "D_NAISSANCE";
	public static final String IND_ACTIVITE_COLKEY = "IND_ACTIVITE";
	public static final String IND_CLE_INSEE_COLKEY = "IND_CLE_INSEE";
	public static final String IND_NO_INSEE_COLKEY = "IND_NO_INSEE";
	public static final String IND_QUALITE_COLKEY = "IND_QUALITE";
	public static final String LIEU_NAISSANCE_COLKEY = "VILLE_DE_NAISSANCE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
	public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";



	// Relationships
	public static final String CIVILITE_KEY = "civilite";
	public static final String CONTRATS_KEY = "contrats";
	public static final String EMPLOYEURS_KEY = "employeurs";
	public static final String NATIONALITE_KEY = "nationalite";
	public static final String PAYS_NAISSANCE_KEY = "paysNaissance";
	public static final String PERSONNEL_KEY = "personnel";
	public static final String REPART_PERSONNE_ADRESSES_KEY = "repartPersonneAdresses";
	public static final String SITUATION_FAMILIALE_KEY = "situationFamiliale";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cCivilite() {
    return (String) storedValueForKey(C_CIVILITE_KEY);
  }

  public void setCCivilite(String value) {
    takeStoredValueForKey(value, C_CIVILITE_KEY);
  }

  public String cDeptNaissance() {
    return (String) storedValueForKey(C_DEPT_NAISSANCE_KEY);
  }

  public void setCDeptNaissance(String value) {
    takeStoredValueForKey(value, C_DEPT_NAISSANCE_KEY);
  }

  public String cPaysNaissance() {
    return (String) storedValueForKey(C_PAYS_NAISSANCE_KEY);
  }

  public void setCPaysNaissance(String value) {
    takeStoredValueForKey(value, C_PAYS_NAISSANCE_KEY);
  }

  public String cPaysNationalite() {
    return (String) storedValueForKey(C_PAYS_NATIONALITE_KEY);
  }

  public void setCPaysNationalite(String value) {
    takeStoredValueForKey(value, C_PAYS_NATIONALITE_KEY);
  }

  public String cSituationFamille() {
    return (String) storedValueForKey(C_SITUATION_FAMILLE_KEY);
  }

  public void setCSituationFamille(String value) {
    takeStoredValueForKey(value, C_SITUATION_FAMILLE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dNaissance() {
    return (NSTimestamp) storedValueForKey(D_NAISSANCE_KEY);
  }

  public void setDNaissance(NSTimestamp value) {
    takeStoredValueForKey(value, D_NAISSANCE_KEY);
  }

  public String indActivite() {
    return (String) storedValueForKey(IND_ACTIVITE_KEY);
  }

  public void setIndActivite(String value) {
    takeStoredValueForKey(value, IND_ACTIVITE_KEY);
  }

  public Integer indCleInsee() {
    return (Integer) storedValueForKey(IND_CLE_INSEE_KEY);
  }

  public void setIndCleInsee(Integer value) {
    takeStoredValueForKey(value, IND_CLE_INSEE_KEY);
  }

  public String indNoInsee() {
    return (String) storedValueForKey(IND_NO_INSEE_KEY);
  }

  public void setIndNoInsee(String value) {
    takeStoredValueForKey(value, IND_NO_INSEE_KEY);
  }

  public String indQualite() {
    return (String) storedValueForKey(IND_QUALITE_KEY);
  }

  public void setIndQualite(String value) {
    takeStoredValueForKey(value, IND_QUALITE_KEY);
  }

  public String lieuNaissance() {
    return (String) storedValueForKey(LIEU_NAISSANCE_KEY);
  }

  public void setLieuNaissance(String value) {
    takeStoredValueForKey(value, LIEU_NAISSANCE_KEY);
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public String nomPatronymique() {
    return (String) storedValueForKey(NOM_PATRONYMIQUE_KEY);
  }

  public void setNomPatronymique(String value) {
    takeStoredValueForKey(value, NOM_PATRONYMIQUE_KEY);
  }

  public String nomUsuel() {
    return (String) storedValueForKey(NOM_USUEL_KEY);
  }

  public void setNomUsuel(String value) {
    takeStoredValueForKey(value, NOM_USUEL_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(PRENOM_KEY);
  }

  public void setPrenom(String value) {
    takeStoredValueForKey(value, PRENOM_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EOCivilite civilite() {
    return (org.cocktail.papaye.server.metier.grhum.EOCivilite)storedValueForKey(CIVILITE_KEY);
  }

  public void setCiviliteRelationship(org.cocktail.papaye.server.metier.grhum.EOCivilite value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOCivilite oldValue = civilite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CIVILITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CIVILITE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOPays nationalite() {
    return (org.cocktail.papaye.server.metier.grhum.EOPays)storedValueForKey(NATIONALITE_KEY);
  }

  public void setNationaliteRelationship(org.cocktail.papaye.server.metier.grhum.EOPays value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOPays oldValue = nationalite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, NATIONALITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, NATIONALITE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOPays paysNaissance() {
    return (org.cocktail.papaye.server.metier.grhum.EOPays)storedValueForKey(PAYS_NAISSANCE_KEY);
  }

  public void setPaysNaissanceRelationship(org.cocktail.papaye.server.metier.grhum.EOPays value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOPays oldValue = paysNaissance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PAYS_NAISSANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PAYS_NAISSANCE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOPersonnel personnel() {
    return (org.cocktail.papaye.server.metier.grhum.EOPersonnel)storedValueForKey(PERSONNEL_KEY);
  }

  public void setPersonnelRelationship(org.cocktail.papaye.server.metier.grhum.EOPersonnel value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOPersonnel oldValue = personnel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNEL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNEL_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOSituationFamiliale situationFamiliale() {
    return (org.cocktail.papaye.server.metier.grhum.EOSituationFamiliale)storedValueForKey(SITUATION_FAMILIALE_KEY);
  }

  public void setSituationFamilialeRelationship(org.cocktail.papaye.server.metier.grhum.EOSituationFamiliale value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOSituationFamiliale oldValue = situationFamiliale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SITUATION_FAMILIALE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SITUATION_FAMILIALE_KEY);
    }
  }
  
  public NSArray contrats() {
    return (NSArray)storedValueForKey(CONTRATS_KEY);
  }

  public NSArray contrats(EOQualifier qualifier) {
    return contrats(qualifier, null, false);
  }

  public NSArray contrats(EOQualifier qualifier, boolean fetch) {
    return contrats(qualifier, null, fetch);
  }

  public NSArray contrats(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat.INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = contrats();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToContratsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CONTRATS_KEY);
  }

  public void removeFromContratsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONTRATS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat createContratsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeContrat");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CONTRATS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat) eo;
  }

  public void deleteContratsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONTRATS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllContratsRelationships() {
    Enumeration objects = contrats().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteContratsRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat)objects.nextElement());
    }
  }

  public NSArray employeurs() {
    return (NSArray)storedValueForKey(EMPLOYEURS_KEY);
  }

  public NSArray employeurs(EOQualifier qualifier) {
    return employeurs(qualifier, null, false);
  }

  public NSArray employeurs(EOQualifier qualifier, boolean fetch) {
    return employeurs(qualifier, null, fetch);
  }

  public NSArray employeurs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeEmployeur.AGENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeEmployeur.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = employeurs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToEmployeursRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeEmployeur object) {
    addObjectToBothSidesOfRelationshipWithKey(object, EMPLOYEURS_KEY);
  }

  public void removeFromEmployeursRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeEmployeur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EMPLOYEURS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeEmployeur createEmployeursRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeEmployeur");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EMPLOYEURS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeEmployeur) eo;
  }

  public void deleteEmployeursRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeEmployeur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EMPLOYEURS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEmployeursRelationships() {
    Enumeration objects = employeurs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEmployeursRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeEmployeur)objects.nextElement());
    }
  }

  public NSArray repartPersonneAdresses() {
    return (NSArray)storedValueForKey(REPART_PERSONNE_ADRESSES_KEY);
  }

  public NSArray repartPersonneAdresses(EOQualifier qualifier) {
    return repartPersonneAdresses(qualifier, null);
  }

  public NSArray repartPersonneAdresses(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = repartPersonneAdresses();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToRepartPersonneAdressesRelationship(org.cocktail.papaye.server.metier.grhum.EORepartPersonneAdresse object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REPART_PERSONNE_ADRESSES_KEY);
  }

  public void removeFromRepartPersonneAdressesRelationship(org.cocktail.papaye.server.metier.grhum.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_PERSONNE_ADRESSES_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EORepartPersonneAdresse createRepartPersonneAdressesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartPersonneAdresse");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_PERSONNE_ADRESSES_KEY);
    return (org.cocktail.papaye.server.metier.grhum.EORepartPersonneAdresse) eo;
  }

  public void deleteRepartPersonneAdressesRelationship(org.cocktail.papaye.server.metier.grhum.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_PERSONNE_ADRESSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRepartPersonneAdressesRelationships() {
    Enumeration objects = repartPersonneAdresses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepartPersonneAdressesRelationship((org.cocktail.papaye.server.metier.grhum.EORepartPersonneAdresse)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOIndividu avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOIndividu createEOIndividu(EOEditingContext editingContext, String cCivilite
, Integer noIndividu
, String temValide
			) {
    EOIndividu eo = (EOIndividu) createAndInsertInstance(editingContext, _EOIndividu.ENTITY_NAME);    
		eo.setCCivilite(cCivilite);
		eo.setNoIndividu(noIndividu);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOIndividu localInstanceIn(EOEditingContext editingContext) {
	  		return (EOIndividu)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndividu creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndividu creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOIndividu object = (EOIndividu)createAndInsertInstance(editingContext, _EOIndividu.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOIndividu localInstanceIn(EOEditingContext editingContext, EOIndividu eo) {
    EOIndividu localInstance = (eo == null) ? null : (EOIndividu)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOIndividu#localInstanceIn a la place.
   */
	public static EOIndividu localInstanceOf(EOEditingContext editingContext, EOIndividu eo) {
		return EOIndividu.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOIndividu fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOIndividu fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOIndividu eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOIndividu)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOIndividu fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOIndividu fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOIndividu eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOIndividu)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOIndividu fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOIndividu eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOIndividu ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOIndividu fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
