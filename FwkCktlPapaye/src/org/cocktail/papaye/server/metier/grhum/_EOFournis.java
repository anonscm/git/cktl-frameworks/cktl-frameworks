// _EOFournis.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFournis.java instead.
package org.cocktail.papaye.server.metier.grhum;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOFournis extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Fournis";
	public static final String ENTITY_TABLE_NAME = "GRHUM.fournis_ulr";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fouOrdre";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String FOU_CODE_KEY = "fouCode";
	public static final String FOU_DATE_KEY = "fouDate";
	public static final String FOU_MARCHE_KEY = "fouMarche";
	public static final String FOU_TYPE_KEY = "fouType";
	public static final String FOU_VALIDE_KEY = "fouValide";
	public static final String PERS_ID_KEY = "persId";

// Attributs non visibles
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String FOU_CODE_COLKEY = "FOU_CODE";
	public static final String FOU_DATE_COLKEY = "FOU_DATE";
	public static final String FOU_MARCHE_COLKEY = "FOU_MARCHE";
	public static final String FOU_TYPE_COLKEY = "FOU_TYPE";
	public static final String FOU_VALIDE_COLKEY = "FOU_VALIDE";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";


	// Relationships
	public static final String ADRESSE_KEY = "adresse";
	public static final String VALIDE_FOURNIS_KEY = "valideFournis";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String fouCode() {
    return (String) storedValueForKey(FOU_CODE_KEY);
  }

  public void setFouCode(String value) {
    takeStoredValueForKey(value, FOU_CODE_KEY);
  }

  public NSTimestamp fouDate() {
    return (NSTimestamp) storedValueForKey(FOU_DATE_KEY);
  }

  public void setFouDate(NSTimestamp value) {
    takeStoredValueForKey(value, FOU_DATE_KEY);
  }

  public String fouMarche() {
    return (String) storedValueForKey(FOU_MARCHE_KEY);
  }

  public void setFouMarche(String value) {
    takeStoredValueForKey(value, FOU_MARCHE_KEY);
  }

  public String fouType() {
    return (String) storedValueForKey(FOU_TYPE_KEY);
  }

  public void setFouType(String value) {
    takeStoredValueForKey(value, FOU_TYPE_KEY);
  }

  public String fouValide() {
    return (String) storedValueForKey(FOU_VALIDE_KEY);
  }

  public void setFouValide(String value) {
    takeStoredValueForKey(value, FOU_VALIDE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EOAdresse adresse() {
    return (org.cocktail.papaye.server.metier.grhum.EOAdresse)storedValueForKey(ADRESSE_KEY);
  }

  public void setAdresseRelationship(org.cocktail.papaye.server.metier.grhum.EOAdresse value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOAdresse oldValue = adresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ADRESSE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOValideFournis valideFournis() {
    return (org.cocktail.papaye.server.metier.grhum.EOValideFournis)storedValueForKey(VALIDE_FOURNIS_KEY);
  }

  public void setValideFournisRelationship(org.cocktail.papaye.server.metier.grhum.EOValideFournis value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOValideFournis oldValue = valideFournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, VALIDE_FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, VALIDE_FOURNIS_KEY);
    }
  }
  

/**
 * Créer une instance de EOFournis avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOFournis createEOFournis(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String fouMarche
, Integer persId
			) {
    EOFournis eo = (EOFournis) createAndInsertInstance(editingContext, _EOFournis.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setFouMarche(fouMarche);
		eo.setPersId(persId);
    return eo;
  }

  
	  public EOFournis localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFournis)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFournis creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFournis creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOFournis object = (EOFournis)createAndInsertInstance(editingContext, _EOFournis.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOFournis localInstanceIn(EOEditingContext editingContext, EOFournis eo) {
    EOFournis localInstance = (eo == null) ? null : (EOFournis)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOFournis#localInstanceIn a la place.
   */
	public static EOFournis localInstanceOf(EOEditingContext editingContext, EOFournis eo) {
		return EOFournis.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFournis fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFournis fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFournis eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFournis)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFournis fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFournis fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFournis eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFournis)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFournis fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFournis eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFournis ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFournis fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
