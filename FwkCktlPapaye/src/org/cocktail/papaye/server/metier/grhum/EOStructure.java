/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.papaye.server.metier.grhum;

import org.cocktail.papaye.server.common.Constantes;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;


public class EOStructure extends _EOStructure {

	private final static String ETABLISSEMENT = "E";

	public EOStructure() {
		super();
	}

	public static NSArray rechercherStructures(EOEditingContext editingContext) {

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOStructure.TEM_VALIDE_KEY + " = %@",
				new NSArray(Constantes.VRAI));

		return fetchAll(editingContext, qualifier);

	}

	public boolean estValide() {
		return temValide() != null && temValide().equals(Constantes.VRAI);
	}

	public boolean cotiseAuxAssedic() {
		return temCotisAssedic() != null && temCotisAssedic().equals(Constantes.VRAI);
	}

	public String codeTauxExonerationTVA() {
		if (tauxExonerationTVA() != null) {
			return tauxExonerationTVA();
		} else {
			if (structureParent() != null) {
				return structureParent().codeTauxExonerationTVA();
			}
			else
				return null;
		}
	}

	/** retourne la raison sociale de l'etablissement */
	public String raisonSociale() {
		if (structureParent() != null && cStructure().equals(cStructurePere()) == false) {
			return structureParent().raisonSociale();
		} else {
			if (rne() != null) {
				return rne().llRne();
			} else {
				return null;
			}
		}	
	}

	/** retourne le numero de nic de l'etablissement */
	public String nicEtablissement() {
		if (structureParent() != null && cStructure().equals(cStructurePere()) == false) {
			return structureParent().nicEtablissement();
		} else {
			return siret().substring(9);
		}
	}
	
	/** retourne le numero de siret de l'etablissement */
	public String siretEtablissement() {
		if (structureParent() != null && cStructure().equals(cStructurePere()) == false) {
			return structureParent().siretEtablissement();
		} else {
			return siret();
		}
	}
	
	public String sirenEtablissement() {
		if (structureParent() != null && cStructure().equals(cStructurePere()) == false) {
			return structureParent().siren();
		} else {
			return siren();
		}
	}



	
	public String codeNaf() {
		
		if (siret() != null) {
			if (cNaf() != null) {
				return cNaf();
			} else if (apeCode() != null) {
				return apeCode().substring(0,4);
			}
		}

		if (structureParent() != null) {
			return structureParent().codeNaf();
		} else {
			return null;
		}	
	}

    public String sectionDerogatoire() {
    	return null;
    }

	public String estSoumiseTaxeSurSalaire() {
		if (!estSoumiseTVA())
			return "01";
		else
			return "02";
	}

	/** retourne le premier numero nic d'une structure, non nul trouve */
	public String nicStructure() {

		if (estValide() && estEtablissementPaye() && siret() != null)
			return siret().substring(9);
		else if (structureParent() != null)
			return structureParent().nicStructure();

		return null;
	}

	public String sirenStructure() {
		EOStructure structure = structureAvecSiren();
		if (structure != null)
			return structure.siren();

		return null;
	}


	/** retourne le premier liebelle d'une structure, ayant un numero de siret */
	public String libelleStructure() {
		if (siret() != null) {
			return llStructure();
		} else if (structureParent() != null) {
			return structureParent().libelleStructure();
		} else {
			return llStructure();
		}	
	}

	public boolean estEtablissementPaye() {
		return temEtablissementPaye() != null && temEtablissementPaye().equals(Constantes.VRAI);
	}

	public String indemniteResidence() {
		if (tauxIR() != null) {
			return tauxIR();
		} else {
			if (structureParent() != null) {
				return structureParent().indemniteResidence();
			}
			else 
				return null;
		}
	}


	public String codeTauxTransport() {
		if (tauxTransport() != null) {
			return tauxTransport();
		} else {
			if (structureParent() != null) {
				return structureParent().codeTauxTransport();
			}
			else
				return null;
		}
	}

	public String siretStructure() {

		if (estValide() && estEtablissementPaye() && siret() != null) {
			return siret();
		} else if (structureParent() != null) {
			return structureParent().siretStructure();
		} 

		return null;

	}

	public static EOStructure rechercherEtablissement(EOEditingContext editingContext) throws Exception {
		// structures valides
		NSMutableArray values = new NSMutableArray(Constantes.VRAI);
		values.addObject(new String(ETABLISSEMENT));
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
		("temValide = %@ AND cTypeStructure = %@",values);
		EOFetchSpecification fs = new EOFetchSpecification(EOStructure.ENTITY_NAME,qualifier, null);
		NSArray structures = editingContext.objectsWithFetchSpecification(fs);
		if (structures.count() > 1) {
			throw new Exception("Il ne doit y avoir qu'un seul etablissement defini !");
		} else {
			try {
				return (EOStructure)structures.objectAtIndex(0);
			} catch (Exception exception) { throw new Exception("Il n'y a pas d'etablissement defini !");
			}
		}
	}

	public static NSArray rechercherStructuresAvecSiret(EOEditingContext editingContext) {

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.TEM_VALIDE_KEY + " = %@",
				new NSArray(Constantes.VRAI)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.SIRET_KEY + " != nil",null));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.TEM_ETABLISSEMENT_PAYE_KEY + " = %@",
				new NSArray(Constantes.VRAI)));

		return fetchAll(editingContext, new EOAndQualifier(mesQualifiers));

	}

	public boolean estSectorisee() {
		return temSectorise().equals(Constantes.VRAI);
	}


	public String numOrganismeComplementaire(String codeOrganisme) throws Exception {
		if (codeOrganisme.equals(Constantes.IRCANTEC)) {
			if (numIrcantec() != null) {
				return numIrcantec();
			} 
		} else if (codeOrganisme.equals(Constantes.CNRACL)) {
			if (numCNRACL() != null) {
				return numCNRACL();
			} 
		} else if (codeOrganisme.equals(Constantes.RAFP)) {
			if (numRAFP() != null) {
				return numRAFP();
			}
		}
		// si non trouvé, rechercher dans le parent
		if (structureParent() != null) {
			return structureParent().numOrganismeComplementaire(codeOrganisme);
		} else {
			return null;
		}
	}


	public boolean estSoumiseTVA() {
		return (temSoumisTVA().equals(Constantes.VRAI));
	}


	public String tauxAccidentTravail() {
		if (tauxAccTrav() != null) {
			return tauxAccTrav();
		} else {
			if (structureParent() != null) {
				return structureParent().tauxAccidentTravail();
			}
			else
				return null;
		}
	}


	public String risqueAccidentTravail() {
		if (risqueAccTrav() != null) {
			return risqueAccTrav();
		} else {
			if (structureParent() != null) {
				return structureParent().risqueAccidentTravail();
			}
			else
				return null;
		}
	}

	public static EOStructure rechercherStructureSiret(EOEditingContext ec, EOStructure structure) {
		try {
			if (structure.siret() != null && structure.estEtablissementPaye())
				return structure;
			if ( ("E".equals(structure.cTypeStructure())) || 
					("ES".equals(structure.cTypeStructure())) || 
					(structure.cStructure().equals(structure.cStructurePere())) || (structure.structureParent() == null))
				return rechercherEtablissement(ec);
			return rechercherStructureSiret(ec , structure.structureParent());       
		} catch (Exception e) {
			return null;
		}
	}

	public EOAdresse adresseCourante() {
		try {
			
		
			return ((EORepartPersonneAdresse)repartPersonneAdresses().objectAtIndex(0)).adresse();
		} catch (Exception e) { return null; }
	}


	public EOAdresse adresseStructure() {
		if (siret() != null && adresseCourante() != null) {
			return adresseCourante();
		} else if (structureParent() != null) {
			return structureParent().adresseStructure();
		} else {
			return null;
		}
	}


	public EOEffectifStructure effectifPourAnnee(Integer annee) throws Exception {
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEffectifStructure.C_STRUCTURE_KEY + " = %@", new NSArray(cStructure())));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEffectifStructure.CODE_ANNEE_KEY  + " = %@", new NSArray(annee.toString())));
				
		EOFetchSpecification fs = new EOFetchSpecification(EOEffectifStructure.ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		NSArray effectifs = editingContext().objectsWithFetchSpecification(fs);
		if (effectifs.count() == 0) {
			throw new Exception("Definir les effectifs pour la structure " + cStructure() + " et l'annee " + annee);
		} else {
			return (EOEffectifStructure)effectifs.objectAtIndex(0);
		}
	}


	public EOStructure structureAvecSiren() {
		if (estValide() && estEtablissementPaye() && siren() != null) {
			return this;
		} else if (structureParent() != null) {
			return structureParent().structureAvecSiren();
		} else {
			return null;
		}
	}


	public EOAdresse adresseEtablissement() {
		if (structureParent() != null && cStructure().equals(cStructurePere()) == false) {
			return structureParent().adresseEtablissement();
		} else {
			try {
				return ((EORepartPersonneAdresse)repartPersonneAdresses().objectAtIndex(0)).adresse();
			} catch (Exception e) { return null; }
		}
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
