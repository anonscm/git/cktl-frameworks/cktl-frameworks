// _EOPassageEchelon.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPassageEchelon.java instead.
package org.cocktail.papaye.server.metier.grhum;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPassageEchelon extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PassageEchelon";
	public static final String ENTITY_TABLE_NAME = "GRHUM.passage_echelon";



	// Attributes


	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String C_INDICE_BRUT_KEY = "cIndiceBrut";
	public static final String D_FERMETURE_KEY = "dFermeture";

// Attributs non visibles
	public static final String C_GRADE_KEY = "cGrade";

//Colonnes dans la base de donnees
	public static final String C_ECHELON_COLKEY = "C_ECHELON";
	public static final String C_INDICE_BRUT_COLKEY = "C_INDICE_BRUT";
	public static final String D_FERMETURE_COLKEY = "D_FERMETURE";

	public static final String C_GRADE_COLKEY = "C_GRADE";


	// Relationships
	public static final String GRADE_KEY = "grade";
	public static final String INDICE_KEY = "indice";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cEchelon() {
    return (String) storedValueForKey(C_ECHELON_KEY);
  }

  public void setCEchelon(String value) {
    takeStoredValueForKey(value, C_ECHELON_KEY);
  }

  public String cIndiceBrut() {
    return (String) storedValueForKey(C_INDICE_BRUT_KEY);
  }

  public void setCIndiceBrut(String value) {
    takeStoredValueForKey(value, C_INDICE_BRUT_KEY);
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey(D_FERMETURE_KEY);
  }

  public void setDFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, D_FERMETURE_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EOGrade grade() {
    return (org.cocktail.papaye.server.metier.grhum.EOGrade)storedValueForKey(GRADE_KEY);
  }

  public void setGradeRelationship(org.cocktail.papaye.server.metier.grhum.EOGrade value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOGrade oldValue = grade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GRADE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GRADE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOIndice indice() {
    return (org.cocktail.papaye.server.metier.grhum.EOIndice)storedValueForKey(INDICE_KEY);
  }

  public void setIndiceRelationship(org.cocktail.papaye.server.metier.grhum.EOIndice value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOIndice oldValue = indice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDICE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPassageEchelon avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPassageEchelon createEOPassageEchelon(EOEditingContext editingContext, String cEchelon
, NSTimestamp dFermeture
			) {
    EOPassageEchelon eo = (EOPassageEchelon) createAndInsertInstance(editingContext, _EOPassageEchelon.ENTITY_NAME);    
		eo.setCEchelon(cEchelon);
		eo.setDFermeture(dFermeture);
    return eo;
  }

  
	  public EOPassageEchelon localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPassageEchelon)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPassageEchelon creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPassageEchelon creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPassageEchelon object = (EOPassageEchelon)createAndInsertInstance(editingContext, _EOPassageEchelon.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPassageEchelon localInstanceIn(EOEditingContext editingContext, EOPassageEchelon eo) {
    EOPassageEchelon localInstance = (eo == null) ? null : (EOPassageEchelon)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPassageEchelon#localInstanceIn a la place.
   */
	public static EOPassageEchelon localInstanceOf(EOEditingContext editingContext, EOPassageEchelon eo) {
		return EOPassageEchelon.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPassageEchelon fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPassageEchelon fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPassageEchelon eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPassageEchelon)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPassageEchelon fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPassageEchelon fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPassageEchelon eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPassageEchelon)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPassageEchelon fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPassageEchelon eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPassageEchelon ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPassageEchelon fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
