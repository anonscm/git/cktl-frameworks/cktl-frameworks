// _EOConvention.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOConvention.java instead.
package org.cocktail.papaye.server.metier.convention;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOConvention extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Convention";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.v_convention";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "convOrdre";

	public static final String CONV_ANNEE_KEY = "convAnnee";
	public static final String CONV_INDEX_KEY = "convIndex";
	public static final String CONV_OBJET_KEY = "convObjet";
	public static final String CONV_PREMIER_EXERCICE_KEY = "convPremierExercice";
	public static final String CONV_REFERENCE_EXTERNE_KEY = "convReferenceExterne";
	public static final String GROUPE_KEY = "groupe";
	public static final String TEM_VALIDE_KEY = "temValide";

//Colonnes dans la base de donnees
	public static final String CONV_ANNEE_COLKEY = "EXERCICE";
	public static final String CONV_INDEX_COLKEY = "CONV_INDEX";
	public static final String CONV_OBJET_COLKEY = "LIBELLE";
	public static final String CONV_PREMIER_EXERCICE_COLKEY = "CONV_PREMIER_EXERCICE";
	public static final String CONV_REFERENCE_EXTERNE_COLKEY = "CONV_REFERENCE_EXTERNE";
	public static final String GROUPE_COLKEY = "GROUPE";
	public static final String TEM_VALIDE_COLKEY = "VALIDE";

// Relationships
	public static final String CONVENTION_LIMITATIVES_KEY = "conventionLimitatives";
	public static final String CONVENTION_NON_LIMITATIVES_KEY = "conventionNonLimitatives";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer convAnnee() {
    return (Integer) storedValueForKey(CONV_ANNEE_KEY);
  }

  public void setConvAnnee(Integer value) {
    takeStoredValueForKey(value, CONV_ANNEE_KEY);
  }

  public Integer convIndex() {
    return (Integer) storedValueForKey(CONV_INDEX_KEY);
  }

  public void setConvIndex(Integer value) {
    takeStoredValueForKey(value, CONV_INDEX_KEY);
  }

  public String convObjet() {
    return (String) storedValueForKey(CONV_OBJET_KEY);
  }

  public void setConvObjet(String value) {
    takeStoredValueForKey(value, CONV_OBJET_KEY);
  }

  public Integer convPremierExercice() {
    return (Integer) storedValueForKey(CONV_PREMIER_EXERCICE_KEY);
  }

  public void setConvPremierExercice(Integer value) {
    takeStoredValueForKey(value, CONV_PREMIER_EXERCICE_KEY);
  }

  public String convReferenceExterne() {
    return (String) storedValueForKey(CONV_REFERENCE_EXTERNE_KEY);
  }

  public void setConvReferenceExterne(String value) {
    takeStoredValueForKey(value, CONV_REFERENCE_EXTERNE_KEY);
  }

  public String groupe() {
    return (String) storedValueForKey(GROUPE_KEY);
  }

  public void setGroupe(String value) {
    takeStoredValueForKey(value, GROUPE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public NSArray conventionLimitatives() {
    return (NSArray)storedValueForKey(CONVENTION_LIMITATIVES_KEY);
  }

  public NSArray conventionLimitatives(EOQualifier qualifier) {
    return conventionLimitatives(qualifier, null);
  }

  public NSArray conventionLimitatives(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = conventionLimitatives();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToConventionLimitativesRelationship(org.cocktail.papaye.server.metier.convention.EOConventionLimitative object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CONVENTION_LIMITATIVES_KEY);
  }

  public void removeFromConventionLimitativesRelationship(org.cocktail.papaye.server.metier.convention.EOConventionLimitative object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_LIMITATIVES_KEY);
  }

  public org.cocktail.papaye.server.metier.convention.EOConventionLimitative createConventionLimitativesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ConventionLimitative");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CONVENTION_LIMITATIVES_KEY);
    return (org.cocktail.papaye.server.metier.convention.EOConventionLimitative) eo;
  }

  public void deleteConventionLimitativesRelationship(org.cocktail.papaye.server.metier.convention.EOConventionLimitative object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_LIMITATIVES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllConventionLimitativesRelationships() {
    Enumeration objects = conventionLimitatives().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteConventionLimitativesRelationship((org.cocktail.papaye.server.metier.convention.EOConventionLimitative)objects.nextElement());
    }
  }

  public NSArray conventionNonLimitatives() {
    return (NSArray)storedValueForKey(CONVENTION_NON_LIMITATIVES_KEY);
  }

  public NSArray conventionNonLimitatives(EOQualifier qualifier) {
    return conventionNonLimitatives(qualifier, null);
  }

  public NSArray conventionNonLimitatives(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = conventionNonLimitatives();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToConventionNonLimitativesRelationship(org.cocktail.papaye.server.metier.convention.EOConventionNonLimitative object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
  }

  public void removeFromConventionNonLimitativesRelationship(org.cocktail.papaye.server.metier.convention.EOConventionNonLimitative object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
  }

  public org.cocktail.papaye.server.metier.convention.EOConventionNonLimitative createConventionNonLimitativesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ConventionNonLimitative");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CONVENTION_NON_LIMITATIVES_KEY);
    return (org.cocktail.papaye.server.metier.convention.EOConventionNonLimitative) eo;
  }

  public void deleteConventionNonLimitativesRelationship(org.cocktail.papaye.server.metier.convention.EOConventionNonLimitative object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllConventionNonLimitativesRelationships() {
    Enumeration objects = conventionNonLimitatives().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteConventionNonLimitativesRelationship((org.cocktail.papaye.server.metier.convention.EOConventionNonLimitative)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOConvention avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOConvention createEOConvention(EOEditingContext editingContext, Integer convAnnee
, Integer convPremierExercice
			) {
    EOConvention eo = (EOConvention) createAndInsertInstance(editingContext, _EOConvention.ENTITY_NAME);    
		eo.setConvAnnee(convAnnee);
		eo.setConvPremierExercice(convPremierExercice);
    return eo;
  }

  
	  public EOConvention localInstanceIn(EOEditingContext editingContext) {
	  		return (EOConvention)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOConvention creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOConvention creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOConvention object = (EOConvention)createAndInsertInstance(editingContext, _EOConvention.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOConvention localInstanceIn(EOEditingContext editingContext, EOConvention eo) {
    EOConvention localInstance = (eo == null) ? null : (EOConvention)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOConvention#localInstanceIn a la place.
   */
	public static EOConvention localInstanceOf(EOEditingContext editingContext, EOConvention eo) {
		return EOConvention.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
