/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Tue Mar 18 15:02:51 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import java.util.Enumeration;

import org.cocktail.papaye.server.common.EOInfoBulletinSalaire;
import org.cocktail.papaye.server.n4ds.ConstantesDads;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOPayeHisto extends _EOPayeHisto {

    public EOPayeHisto() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPayeHisto(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
 
*/
	public NSArray histoLbuds() {
        return (NSArray)storedValueForKey("histoLbuds");
    }
	
    public void setHistoLbuds(NSArray value) {
        takeStoredValueForKey(value, "histoLbuds");
    }
	
    public void addToHistoLbuds(EOPayeHistoLbud object) {
        includeObjectIntoPropertyWithKey(object, "histoLbuds");
    }
	
    public void removeFromHistoLbuds(EOPayeHistoLbud object) {
        excludeObjectFromPropertyWithKey(object, "histoLbuds");
    }
	
    // méthodes de classe
    /** methode de classe permettant de trouver tous les historiques en fonction
    d'une operation de paye.
    @param editingContext editing context dans lequel effectuer le fetch
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return historiques trouvees
    */
    public static NSArray chercherHistoriques(EOEditingContext editingContext, NSArray prefetches) {
        return EOInfoBulletinSalaire.chercherInfos("PayeHisto", editingContext,prefetches);
    }
    /** methode de classe permettant de trouver les historiques en fonction d'un contrat
    au cours des n derniers mois
    @param editingContext editing context dans lequel effectuer le fetch
    @param mois nombre de mois pour lesquels effectuer la recherche
    @param contrat contrat concerne
    @return historiques trouves
    */
    public static NSArray chercherHistoriquesPourContratEtDuree(EOEditingContext editingContext,
                                                                EOPayeContrat contrat,
                                                                Number codeMoisDebut,
                                                                Number codeMoisFin) {
        // contrat
        NSMutableArray values = new NSMutableArray(contrat);
        // en commençant au mois
        values.addObject(codeMoisDebut);
        // en s'arrêtant au mois
        values.addObject(codeMoisFin);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(
                        "contrat = %@ AND mois.moisCode >= %@ AND mois.moisCode <= %@",values);
        // trier par ordre décroissant
        EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("mois.moisCode",
                                                                 EOSortOrdering.CompareDescending);
        EOFetchSpecification fs = new EOFetchSpecification(_EOPayeHisto.ENTITY_NAME,qualifier, new NSArray(sort));
        //fs.setPrefetchingRelationshipKeyPaths(new NSArray("elements"));

        return editingContext.objectsWithFetchSpecification(fs);
    }
    /** methode de classe permettant de trouver les historiques en fonction d'un contrat et d'une operation de paye.
    @param editingContext editing context dans lequel effectuer le fetch
    @param mois mois concerne
    @param contrat contrat concerne
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return historiques trouves
    */
    public static NSArray chercherHistoriquesPourMoisEtContrat(EOEditingContext editingContext,EOPayeMois mois, EOPayeContrat contrat,NSArray prefetches) {
        return EOInfoBulletinSalaire.chercherInfosPourMoisEtContrat(_EOPayeHisto.ENTITY_NAME,
                                                                          editingContext,
                                                                          mois, contrat,
                                                                          prefetches);
    }
    /** methode de classe permettant de trouver un historique en fonction d'un contrat et d'une operation de paye.
    @param editingContext editing context dans lequel effectuer le fetch
    @param mois mois concerne
    @param contrat contrat concerne
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return historiques trouves
    */
    public static EOPayeHisto chercherHistoriquePourMoisEtContrat(EOEditingContext editingContext,EOPayeMois mois, EOPayeContrat contrat,NSArray prefetches) {
        try {
            return (EOPayeHisto)chercherHistoriquesPourMoisEtContrat(editingContext,mois,contrat,
                                                                   prefetches).objectAtIndex(0);
        } catch (Exception e) {
            return null;
        }
    }
    /** methode de classe permettant de trouver tous les historiques en fonction
    d'une operation de paye et d'un secteur.
    @param editingContext editing context dans lequel effectuer le fetch
    @param operation operation de paye concernee
    @param secteur secteur pour lequel rechercher les preparations (peut &ecirc;tre nul)
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return historiques trouves
    */
    public static NSArray chercherHistoriquesPourOperationEtSecteur(EOEditingContext editingContext,
                                                          EOPayeOper operation,EOPayeSecteur secteur,
                                                          NSArray prefetches) {
        return EOInfoBulletinSalaire.chercherInfosPourOperationEtSecteur(_EOPayeHisto.ENTITY_NAME,
                                                            editingContext,
                                                            operation,secteur,prefetches);
    }
    /** methode de classe permettant de trouver tous les historiques en fonction
    d'un mois et d'un secteur.
    @param editingContext editing context dans lequel effectuer le fetch
    @param mois mois recherche
    @param secteur secteur pour lequel rechercher les preparations (peut &ecirc;tre nul)
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return historiques trouves
    */
    public static NSArray chercherHistoriquesPourMoisEtSecteur(EOEditingContext editingContext,
                                                                EOPayeMois mois,EOPayeSecteur secteur,
                                                                NSArray prefetches) {
        return EOInfoBulletinSalaire.chercherInfosPourMoisEtSecteur(_EOPayeHisto.ENTITY_NAME,
                                                                  editingContext,
                                                                  mois,secteur,prefetches);
    }
    
    
    public static String sqlQualifierHistoriquesPourAnnee(Integer annee) {
    	
    	String selectQualifier = " SELECT ph.paye_ordre PAYE_ORDRE, i.no_individu NO_INDIVIDU, ph.pctr_ordre PCTR_ORDRE";

    	String fromQualifier = " FROM jefy_paye.paye_histo ph, grhum.individu_ulr i, jefy_paye.dads_agents a, jefy_paye.paye_contrat c, jefy_paye.paye_mois m, jefy_paye.paye_statut ps ";

    	String whereQualifier = " WHERE ph.mois_ordre = m.mois_ordre and ph.pctr_ordre = c.pctr_ordre and ph.psta_ordre = ps.Psta_ordre and c.no_individu = i.no_individu and ph.exe_ordre = " + annee.intValue() + " and i.no_individu = a.no_individu and a.exe_ordre = " + annee.intValue()+ " ";

    	String orderQualifier = " ORDER BY nom_usuel, prenom, i.pers_id, c.d_deb_contrat_trav, c.d_fin_contrat_trav, m.mois_code, ps.psta_Cipdz ";

    	return selectQualifier + fromQualifier + whereQualifier + orderQualifier;
    	
    }
    
    public static EOPayeHisto histoForKey(EOEditingContext editingContext, Integer key) {

        NSMutableArray values = new NSMutableArray(key);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("payeOrdre = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification(_EOPayeHisto.ENTITY_NAME,qualifier, null);
        return  (EOPayeHisto)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
    }

    
    public static NSArray chercherHistoriquesPourAnnee(EOEditingContext editingContext,int annee,NSArray sorts) {

    	// année
        EOQualifier qualifier;
        NSMutableArray values = new NSMutableArray(new Integer(annee));
        qualifier = EOQualifier.qualifierWithQualifierFormat(EOPayeHisto.PAYE_ANNEE_KEY + " = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification(_EOPayeHisto.ENTITY_NAME,qualifier, sorts);
        NSMutableArray prefetches = new NSMutableArray(EOPayeHisto.CONTRAT_KEY);
        prefetches.addObject("contrat.individu");
        fs.setPrefetchingRelationshipKeyPaths(prefetches);
        return  editingContext.objectsWithFetchSpecification(fs);
    }
    public static NSArray chercherHistoriquesArtistesIntermittentsPourAnnee(EOEditingContext editingContext,int annee) {
		// année
        EOQualifier qualifier;
        NSMutableArray values = new NSMutableArray(new Integer(annee));
        values.addObject(ConstantesDads.CAPRICAS);
        values.addObject(ConstantesDads.CARCICAS);
        qualifier = EOQualifier.qualifierWithQualifierFormat("payeAnnee = %@ AND (statut.regimeCotisations.pregComplementaire = %@ OR statut.regimeCotisations.pregComplementaire = %@)",values);
        EOFetchSpecification fs = new EOFetchSpecification(EOPayeHisto.ENTITY_NAME,qualifier, null);
        return  editingContext.objectsWithFetchSpecification(fs);
    }
    /** methode de classe permettant de trouver tous les agents payes l'annee fournie en param&egrave;tre
    d'un mois et d'un secteur.
    @param editingContext editing context dans lequel effectuer le fetch
    @param annee annee recherchee
    @param secteur secteur pour lequel rechercher les preparations (peut &ecirc;tre nul)
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return historiques trouves
    */
    public static NSArray chercherAgentsPayesPourAnnee(EOEditingContext editingContext,Number annee) {
		// année
        EOQualifier qualifier;
        NSMutableArray values = new NSMutableArray(annee);
     
        qualifier = EOQualifier.qualifierWithQualifierFormat("payeAnnee = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification(_EOPayeHisto.ENTITY_NAME,qualifier, null);
        NSMutableArray prefetches = new NSMutableArray("contrat");
        prefetches.addObject("contrat.individu");
        fs.setPrefetchingRelationshipKeyPaths(prefetches);
        NSArray  resultats = editingContext.objectsWithFetchSpecification(fs);
        NSMutableArray agents = new NSMutableArray();
        Enumeration e = resultats.objectEnumerator();
        while (e.hasMoreElements()) {
        		EOPayeHisto historique = (EOPayeHisto)e.nextElement();
        		if (agents.containsObject(historique.contrat().individu()) == false) {
        			agents.addObject(historique.contrat().individu());
        		}
        }
        EOSortOrdering.sortArrayUsingKeyOrderArray(agents,new NSArray(EOSortOrdering.sortOrderingWithKey("nomUsuel",EOSortOrdering.CompareAscending)));
        return agents;
    }
}
