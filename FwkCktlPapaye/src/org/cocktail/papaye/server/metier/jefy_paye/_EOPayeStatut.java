// _EOPayeStatut.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeStatut.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeStatut extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeStatut";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_statut";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pstaOrdre";

	public static final String PSTA_ABREGE_KEY = "pstaAbrege";
	public static final String PSTA_CIPDZ_KEY = "pstaCipdz";
	public static final String PSTA_CODEMPLOI_KEY = "pstaCodemploi";
	public static final String PSTA_LIBELLE_KEY = "pstaLibelle";
	public static final String PSTA_MDEBUT_KEY = "pstaMdebut";
	public static final String PSTA_MFIN_KEY = "pstaMfin";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VALIDE_KEY = "temValide";

//Colonnes dans la base de donnees
	public static final String PSTA_ABREGE_COLKEY = "psta_abrege";
	public static final String PSTA_CIPDZ_COLKEY = "psta_cipdz";
	public static final String PSTA_CODEMPLOI_COLKEY = "psta_code_emploi";
	public static final String PSTA_LIBELLE_COLKEY = "psta_libelle";
	public static final String PSTA_MDEBUT_COLKEY = "psta_mdebut";
	public static final String PSTA_MFIN_COLKEY = "psta_mfin";
	public static final String TEM_TITULAIRE_COLKEY = "tem_titulaire";
	public static final String TEM_VALIDE_COLKEY = "tem_valide";

// Relationships
	public static final String CATEGORIE_KEY = "categorie";
	public static final String PAYE_CHAMPS_SAISIES_KEY = "payeChampsSaisies";
	public static final String REGIME_COTISATIONS_KEY = "regimeCotisations";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String pstaAbrege() {
    return (String) storedValueForKey(PSTA_ABREGE_KEY);
  }

  public void setPstaAbrege(String value) {
    takeStoredValueForKey(value, PSTA_ABREGE_KEY);
  }

  public String pstaCipdz() {
    return (String) storedValueForKey(PSTA_CIPDZ_KEY);
  }

  public void setPstaCipdz(String value) {
    takeStoredValueForKey(value, PSTA_CIPDZ_KEY);
  }

  public String pstaCodemploi() {
    return (String) storedValueForKey(PSTA_CODEMPLOI_KEY);
  }

  public void setPstaCodemploi(String value) {
    takeStoredValueForKey(value, PSTA_CODEMPLOI_KEY);
  }

  public String pstaLibelle() {
    return (String) storedValueForKey(PSTA_LIBELLE_KEY);
  }

  public void setPstaLibelle(String value) {
    takeStoredValueForKey(value, PSTA_LIBELLE_KEY);
  }

  public Integer pstaMdebut() {
    return (Integer) storedValueForKey(PSTA_MDEBUT_KEY);
  }

  public void setPstaMdebut(Integer value) {
    takeStoredValueForKey(value, PSTA_MDEBUT_KEY);
  }

  public Integer pstaMfin() {
    return (Integer) storedValueForKey(PSTA_MFIN_KEY);
  }

  public void setPstaMfin(Integer value) {
    takeStoredValueForKey(value, PSTA_MFIN_KEY);
  }

  public String temTitulaire() {
    return (String) storedValueForKey(TEM_TITULAIRE_KEY);
  }

  public void setTemTitulaire(String value) {
    takeStoredValueForKey(value, TEM_TITULAIRE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeCategorieStatut categorie() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeCategorieStatut)storedValueForKey(CATEGORIE_KEY);
  }

  public void setCategorieRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeCategorieStatut value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeCategorieStatut oldValue = categorie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CATEGORIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CATEGORIE_KEY);
    }
  }
  
  public NSArray payeChampsSaisies() {
    return (NSArray)storedValueForKey(PAYE_CHAMPS_SAISIES_KEY);
  }

  public NSArray payeChampsSaisies(EOQualifier qualifier) {
    return payeChampsSaisies(qualifier, null, false);
  }

  public NSArray payeChampsSaisies(EOQualifier qualifier, boolean fetch) {
    return payeChampsSaisies(qualifier, null, fetch);
  }

  public NSArray payeChampsSaisies(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeChampsSaisie.STATUT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeChampsSaisie.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = payeChampsSaisies();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPayeChampsSaisiesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeChampsSaisie object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PAYE_CHAMPS_SAISIES_KEY);
  }

  public void removeFromPayeChampsSaisiesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeChampsSaisie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PAYE_CHAMPS_SAISIES_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeChampsSaisie createPayeChampsSaisiesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeChampsSaisie");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PAYE_CHAMPS_SAISIES_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeChampsSaisie) eo;
  }

  public void deletePayeChampsSaisiesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeChampsSaisie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PAYE_CHAMPS_SAISIES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPayeChampsSaisiesRelationships() {
    Enumeration objects = payeChampsSaisies().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePayeChampsSaisiesRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeChampsSaisie)objects.nextElement());
    }
  }

  public NSArray regimeCotisations() {
    return (NSArray)storedValueForKey(REGIME_COTISATIONS_KEY);
  }

  public NSArray regimeCotisations(EOQualifier qualifier) {
    return regimeCotisations(qualifier, null, false);
  }

  public NSArray regimeCotisations(EOQualifier qualifier, boolean fetch) {
    return regimeCotisations(qualifier, null, fetch);
  }

  public NSArray regimeCotisations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EORegimeCotisation.STATUT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EORegimeCotisation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = regimeCotisations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRegimeCotisationsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EORegimeCotisation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REGIME_COTISATIONS_KEY);
  }

  public void removeFromRegimeCotisationsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EORegimeCotisation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REGIME_COTISATIONS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EORegimeCotisation createRegimeCotisationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RegimeCotisation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REGIME_COTISATIONS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EORegimeCotisation) eo;
  }

  public void deleteRegimeCotisationsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EORegimeCotisation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REGIME_COTISATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRegimeCotisationsRelationships() {
    Enumeration objects = regimeCotisations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRegimeCotisationsRelationship((org.cocktail.papaye.server.metier.jefy_paye.EORegimeCotisation)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPayeStatut avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeStatut createEOPayeStatut(EOEditingContext editingContext, String pstaAbrege
, String pstaLibelle
, Integer pstaMdebut
, Integer pstaMfin
, String temTitulaire
, String temValide
			) {
    EOPayeStatut eo = (EOPayeStatut) createAndInsertInstance(editingContext, _EOPayeStatut.ENTITY_NAME);    
		eo.setPstaAbrege(pstaAbrege);
		eo.setPstaLibelle(pstaLibelle);
		eo.setPstaMdebut(pstaMdebut);
		eo.setPstaMfin(pstaMfin);
		eo.setTemTitulaire(temTitulaire);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOPayeStatut localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeStatut)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeStatut creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeStatut creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeStatut object = (EOPayeStatut)createAndInsertInstance(editingContext, _EOPayeStatut.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeStatut localInstanceIn(EOEditingContext editingContext, EOPayeStatut eo) {
    EOPayeStatut localInstance = (eo == null) ? null : (EOPayeStatut)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeStatut#localInstanceIn a la place.
   */
	public static EOPayeStatut localInstanceOf(EOEditingContext editingContext, EOPayeStatut eo) {
		return EOPayeStatut.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeStatut fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeStatut fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeStatut eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeStatut)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeStatut fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeStatut fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeStatut eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeStatut)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeStatut fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeStatut eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeStatut ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeStatut fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
