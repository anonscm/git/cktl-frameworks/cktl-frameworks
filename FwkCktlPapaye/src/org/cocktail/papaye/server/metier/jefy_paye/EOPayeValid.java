/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Fri Mar 14 09:17:06 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import org.cocktail.papaye.server.common.EOInfoBulletinSalaire;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class EOPayeValid extends _EOPayeValid {

    public EOPayeValid() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPayeValid(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/
	public NSArray histoLbuds() {
        return (NSArray)storedValueForKey("histoLbuds");
    }
	
    public void setHistoLbuds(NSArray value) {
        takeStoredValueForKey(value, "histoLbuds");
    }
	
    public void addToHistoLbuds(EOPayeHistoLbud object) {
        includeObjectIntoPropertyWithKey(object, "histoLbuds");
    }
	
    public void removeFromHistoLbuds(EOPayeHistoLbud object) {
        excludeObjectFromPropertyWithKey(object, "histoLbuds");
    }
	
    // méthodes ajoutées
//    /** prepare historique en fonction des donnees de la validation */
//    public EOPayeHisto preparerHistorique() {
//        EOPayeHisto histo = new EOPayeHisto();
//        histo.initAvecObjet(this);
//        java.util.Enumeration e = elements().objectEnumerator();
//        while (e.hasMoreElements()) {
//            EOPayeElement element = (EOPayeElement) e.nextElement();
//            element.setHistoriqueRelationship(histo);
//        }
//		e = histoLbuds().objectEnumerator();
//        while (e.hasMoreElements()) {
//            EOPayeHistoLbud histoLbuds = (EOPayeHistoLbud) e.nextElement();
//            histoLbuds.setHistoriqueRelationship(histo);
//        }
//        e = commentaires().objectEnumerator();
//        while (e.hasMoreElements()) {
//            EOPayeCommentairesBs commentaire = (EOPayeCommentairesBs) e.nextElement();
//            commentaire.setHistoriqueRelationship(histo);
//        }
//        return histo;
//    }
    // Méthodes privées
    private NSArray commentaires() {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOPayeCommentairesBs.VALIDATION_KEY + " = %@", new NSArray(this));
		return editingContext().objectsWithFetchSpecification(new EOFetchSpecification(EOPayeCommentairesBs.ENTITY_NAME,qualifier,null));
    }
    // méthodes de classe
    /** methode de classe permettant de trouver toutes les validations
    @param editingContext editing context dans lequel effectuer le fetch
    @return validations trouvees
    */
    public static NSArray chercherValidations(EOEditingContext editingContext,NSArray prefetches) {
        return EOInfoBulletinSalaire.chercherInfos("PayeValid", editingContext,prefetches);
    }
    /** methode de classe permettant de trouver les validation en fonction d'un contrat
    et d'un mois
    @param editingContext editing context dans lequel effectuer le fetch
    @param mois mois concerne
    @param contrat contrat concerne
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return validations trouvees
    */
    public static NSArray chercherValidationsPourMoisEtContrat(EOEditingContext editingContext,EOPayeMois mois, EOPayeContrat contrat,NSArray prefetches) {
        return EOInfoBulletinSalaire.chercherInfosPourMoisEtContrat("PayeValid",
                                                               editingContext,
                                                               mois, contrat,
                                                               prefetches);
    }
    /** methode de classe permettant de trouver toutes les validations en fonction
    d'une operation de paye et d'un secteur.
    @param editingContext editing context dans lequel effectuer le fetch
    @param operation operation de paye concernee
    @param secteur secteur pour lequel rechercher les preparations (peut &ecirc;tre nul)
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return validations trouvees
    */
    public static NSArray chercherValidationsPourOperationEtSecteur(
                                                          EOEditingContext editingContext,
                                                          EOPayeOper operation,EOPayeSecteur secteur,
                                                          NSArray prefetches) {
        return EOInfoBulletinSalaire.chercherInfosPourOperationEtSecteur("PayeValid",
                                                            editingContext,
                                                            operation,secteur,prefetches);
    }
    /** methode de classe permettant de trouver toutes les validations en fonction
    d'un mois et d'un secteur.
    @param editingContext editing context dans lequel effectuer le fetch
    @param mois mois recherche
    @param secteur secteur pour lequel rechercher les preparations (peut &ecirc;tre nul)
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return validations trouvees
    */
    public static NSArray chercherValidationsPourMoisEtSecteur(EOEditingContext editingContext,
                                                                EOPayeMois mois,EOPayeSecteur secteur,
                                                                NSArray prefetches) {
        return EOInfoBulletinSalaire.chercherInfosPourMoisEtSecteur("PayeValid",
                                                                  editingContext,
                                                                  mois,secteur,prefetches);
    }

    
}
