/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Thu Apr 03 17:32:29 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.metier.grhum.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOPayeParamPerso extends _EOPayeParamPerso {

    public EOPayeParamPerso() {
        super();
    }


	/** retourne le parametre personnel valide associe a ce code et cette personnalisation  */
public static EOPayeParamPerso parametrePersoValide(EOEditingContext ec, EOPayeCode code, EOPayePerso personnalisation) {

	try {

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeParamPerso.CODE_KEY + " = %@", new NSArray(code)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeParamPerso.PERSONNALISATION_KEY + " = %@", new NSArray(personnalisation)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeParamPerso.TEM_VALIDE_KEY + " = %@", new NSArray(Constantes.VRAI)));

		return fetchFirstByQualifier(ec, new EOAndQualifier(mesQualifiers));
		
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}
}

    /** Initialisation d'un parametre personnel a partir d'une valeur, d'un
    libelle, d'un code.<BR>
    Les informations de dates de validite
    sont initialisees automatiquement. */
    public void initAvecLibelleValeurCode(String libelle,String valeur, EOPayeCode code,EOEditingContext editingContext) {
        setPparLibelle(libelle);
        setPparValeur(valeur);
        setPparMdebut(EOPayeMois.moisCourant(editingContext).moisCode());
        setPparMfin(new Integer(300000));	// valable jusqu'en 3000
        setTemValide(Constantes.VRAI);
        setCodeRelationship(code);
    }
    /** Initialisation d'un param&egrave;tre personnel a partir d'une valeur, d'un
    libelle, d'un code et d'un mois de paiement.<BR>
     */
    public void initAvecLibelleValeurCodeEtMois(String libelle,String valeur, EOPayeCode code,EOPayeMois mois,EOEditingContext editingContext) {
        setPparLibelle(libelle);
        setPparValeur(valeur);
        setPparMdebut(mois.moisCode());
        setPparMfin(new Integer(300000));	// valable jusqu'en 3000
        setTemValide(Constantes.VRAI);
        setCodeRelationship(code);
    }
    public String toString() {
        return new String(getClass().getName() +
                          "\nlibelle : " + pparLibelle() +
                          "\ndebut validite : " + pparMdebut() +
                          "\nfin validite : " + pparMfin() +
                          "\nvaleur : " + pparValeur() +
                          "\nvalide : " + temValide() +
                          "\ncode : " + code());
    }
    // Méthodes statiques
    public static NSArray rechercherParamPersoPourIndividuCodeEtPeriode(EOEditingContext editingContext,EOIndividu individu,String code,Number debutPeriode,Number finPeriode) {
    	  NSMutableArray values = new NSMutableArray(individu);
          values.addObject(code);
          values.addObject(debutPeriode);
          values.addObject(finPeriode);
          values.addObject(debutPeriode);
  		values.addObject(finPeriode);
          EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(
                        "personnalisation.contrat.individu = %@ AND code.pcodCode = %@ AND ((pparMfin >= %@ AND pparMfin <= %@) OR (pparMdebut >= %@ AND pparMdebut <= %@) OR pparMfin = 300000)", values);
          EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,qualifier,null);
          return editingContext.objectsWithFetchSpecification(fs);
    }
}
