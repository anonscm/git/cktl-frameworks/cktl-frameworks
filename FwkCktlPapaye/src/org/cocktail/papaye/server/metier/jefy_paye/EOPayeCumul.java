/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.papaye.server.metier.jefy_paye;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.metier.grhum.EOIndividu;
import org.cocktail.papaye.server.metier.grhum.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;


public class EOPayeCumul extends _EOPayeCumul {

    public static final String CUMUL_GLOBAL = "G";

    public EOPayeCumul() {
        super();
    }

    // méthodes ajoutées
    /** initialise un cumul<BR>
    Le type, le code de la rubrique de ce cumul, la structure et l'agent sont initialises.<BR>
    Tous les montants et taux sont initialises a zero
    @param annee annee de reference
    @param mois mois pour lequel le cumul est cree
    @param type type de cumul
    @param code de la rubrique
    @param rubrique rubrique associe a ce cumul (peut etre nulle)
    @param structure pour laquelle est effectue ce cumul
    @param agent pour lequel est effectue ce cumul
    */
    public void init(Integer annee,EOPayeMois mois,String type,EOPayeCode code,EOPayeRubrique rubrique,EOStructure structure,EOIndividu agent) {
        setPcumAnnee(annee);
        setPcumType(type);
        setPcumTaux(new BigDecimal(0));
        setPcumMontant(new BigDecimal(0));
        setPcumBase(new BigDecimal(0));
        setPcumRegul(new BigDecimal(0));
        addObjectToBothSidesOfRelationshipWithKey(code,"code");
        if (rubrique != null) {
            addObjectToBothSidesOfRelationshipWithKey(rubrique,"rubrique");
        }
        addObjectToBothSidesOfRelationshipWithKey(structure,"structure");
        addObjectToBothSidesOfRelationshipWithKey(agent,"agent");
        addObjectToBothSidesOfRelationshipWithKey(mois,"mois");
    }
    /** retourne true si un cumul est un cumul global */
    public boolean estGlobal() {
    		return pcumType().equals(CUMUL_GLOBAL);
    }
    /** retourne true si un cumul est un cumul de rubrique */
    public boolean estCumulRubrique() {
    		return !estGlobal();
    }
    /** met a jour le montant d'un cumul
    @param montant valeur du montant
    @param taux taux associe a ce montant
        */
    public void mettreAJourMontant(BigDecimal montant,BigDecimal taux,boolean estRappel) {
        if (estRappel) {
            setPcumRegul(pcumRegul().add(montant));
        } else {
            setPcumMontant(pcumMontant().add(montant));
        }
        setPcumTaux(taux);
    }
    /** met a jour l'assiette d'un cumul
        @param assiette du calcul
        @param remplacerCumul booleen qui indique si le montant du cumul doit &ecirc;tre
        remplace par une nouvelle valeur */
    public void mettreAJourAssiette(BigDecimal assiette,boolean remplacerCumul) {
        // mettre à jour le cumul courant
        if (remplacerCumul) {
            if (assiette.signum() == -1) {
                // cumul négatif
                setPcumBase(new BigDecimal(0));
            }
            else {
                setPcumBase(assiette);
            }
        } else {
            // un cumul ne pouvant être négatif, si il se trouve que le cumul devient négatif, on
            // rend égal à zéro
            BigDecimal total = pcumBase().add(assiette);
            if (total.signum() == -1) {
                // cumul négatif
                setPcumBase(new BigDecimal(0));
            }
            else {
                 setPcumBase(total);
            }
        }
    }
	/** methode permettant de trouver le cumul precedent ce cumul (ce n'est pas necessairement celui du mois 
		precedent)
	@return cumul trouve
	*/
    public EOPayeCumul rechercherCumulPrecedent() {
		// Rechercher sur l'agent, la structure et le mois, la rubrique et le code
        NSMutableArray values = new NSMutableArray(agent());
        values.addObject(structure());
		if (rubrique() != null) {
			values.addObject(rubrique());
		}
		values.addObject(code());
        values.addObject(mois().moisAnnee());
        values.addObject(mois().moisCode());
		EOQualifier qualifier;
		if (rubrique() != null) {
			qualifier = EOQualifier.qualifierWithQualifierFormat
            ("agent = %@ AND structure = %@ AND rubrique = %@ AND code = %@ AND pcumAnnee = %@ AND mois.moisCode < %@",values);
		} else {
			qualifier = EOQualifier.qualifierWithQualifierFormat
            ("agent = %@ AND structure = %@ AND code = %@ AND pcumAnnee = %@  AND mois.moisCode < %@",values);
		}
        EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("mois.moisCode",
                                                                 EOSortOrdering.CompareDescending);
		
        EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier,new NSArray(sort));
        NSArray cumuls = editingContext().objectsWithFetchSpecification(fs);
		try {
			return (EOPayeCumul)cumuls.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
    }
	/** methode  permettant de trouver le cumul qui a l'assiette la plus haute pour l'annee concernee et qui est un cumul anterieur
		qui n'est pas un cumul du mois
        @return cumul trouve
    */
    public EOPayeCumul rechercherCumulAnterieurAvecAssietteMax() {
        // Rechercher sur l'agent, la structure et le mois
        NSMutableArray values = new NSMutableArray(agent());
        values.addObject(pcumAnnee());
		values.addObject(mois().moisCode());
        values.addObject(code());
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
            ("agent = %@ AND pcumAnnee = %@ AND mois.moisCode < %@ AND code = %@",values);
        EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("pcumBase",
                                                                 EOSortOrdering.CompareDescending);
		
        EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier,new NSArray(sort));
        NSArray cumuls = editingContext().objectsWithFetchSpecification(fs);
		try {
			return (EOPayeCumul)cumuls.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
    }	
 
	public String toString() {
        return new String("code : "+ code().pcodCode() + " mois "+ mois().moisCode() + " assiette : "+ pcumBase() +  " rappels : "+ pcumRegul() +" montant : "+ pcumMontant() +
                          " taux :" + pcumTaux() +
                          " rubrique : "+ rubrique());
    }
	
	public String shortStringRubriqueAvecTaux() {
        return new String("\ncode : "+ code().pcodCode() +
						  " structure : "+ structure().llStructure() +
						  " agent : "+ agent().identite() +
						  " rubrique : "+ rubrique().prubLibelle() +
						  " taux calcul :" + pcumTaux() +
                          "\nassiette : "+ pcumBase() +
                          " rappels : "+ pcumRegul() +
                          " montant : "+ pcumMontant());
    }
	public String shortStringRubriqueSansTaux() {
        return new String("\ncode : "+ code().pcodCode() +
						  " agent : "+ agent().identite() +
						  " structure : "+ structure().llStructure() +
						  " rubrique : "+ rubrique().prubLibelle() +
                          "\nassiette : "+ pcumBase() +
                          " rappels : "+ pcumRegul() +
                          " montant : "+ pcumMontant());
    }
	public String shortStringSansRubrique() {
        return new String("\ncode : "+ code().pcodCode() +
						  " agent : "+ agent().identite() +
						  " structure : "+ structure().llStructure() +
                          "\nassiette : "+ pcumBase() +
                          " rappels : "+ pcumRegul() +
                          " montant : "+ pcumMontant());
    }
	
    // méthodes de recherche : méthodes de classe
    /** methode de classe permettant de trouver un cumul en fonction du mois, du code, de la rubrique et 
		de l'agent et de la structure concernee
		@param editingContext context dans lequel fetcher le cumul
		@param mois mois pour lequel rechercher ce cumul
		@param rubrique rubrique pour laquelle rechercher ce cumul
		@param code identifiant du cumul
		@param agent
		@param structure structure associe a ce cumul
		@return object cumul trouve
    */
    public static EOPayeCumul rechercherCumul(EOEditingContext editingContext,EOPayeMois mois, EOPayeRubrique rubrique,String code,
											EOIndividu agent, EOStructure structure) {
		// Rechercher sur l'agent, la structure et le mois, la rubrique et le code
        NSMutableArray values = new NSMutableArray(agent);
        values.addObject(structure);
		if (rubrique != null) {
			values.addObject(rubrique);
		}
		values.addObject(code);
        values.addObject(mois);
		EOQualifier qualifier;
		if (rubrique != null) {
			qualifier = EOQualifier.qualifierWithQualifierFormat
            ("agent = %@ AND structure = %@ AND rubrique = %@ AND code.pcodCode = %@ AND mois = %@",values);
		} else {
			qualifier = EOQualifier.qualifierWithQualifierFormat
            ("agent = %@ AND structure = %@ AND code.pcodCode = %@ AND mois = %@",values);
		}
        EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier, null);
        NSArray cumuls = editingContext.objectsWithFetchSpecification(fs);
		
        if (cumuls == null || cumuls.count() == 0) {	// pas de cumul trouvé
            return null;
        } else {
            return (EOPayeCumul)cumuls.objectAtIndex(0);
        }
    }
	
	/** methode de classe permettant de trouver le cumul precedent (ce n'est pas necessairement celui du mois 
		precedent pour un code, un agent et l'annee concernee)
		@param editingContext context dans lequel fetcher les cumuls
		@param mois mois courant
@param rubrique rubrique de ce cumul
		@param code code de ce cumul
		@param agent
		@return cumul trouve
	*/
    public static EOPayeCumul rechercherCumulPrecedent(EOEditingContext editingContext,EOPayeMois mois,EOPayeRubrique rubrique,String code, EOIndividu agent,EOStructure structure) {
        // Rechercher sur l'agent, la structure et l'année
        NSMutableArray values = new NSMutableArray(agent);
		values.addObject(structure);
		if (rubrique != null) {
			values.addObject(rubrique);
		}
        values.addObject(code);
		values.addObject(mois.moisAnnee());
		values.addObject(mois.moisCode());
		EOQualifier qualifier;
		if (rubrique != null) {
			qualifier = EOQualifier.qualifierWithQualifierFormat
            ("agent = %@ AND structure = %@ AND rubrique = %@ AND code.pcodCode = %@ AND pcumAnnee = %@ AND mois.moisCode < %@ ",values);
		} else {
			qualifier = EOQualifier.qualifierWithQualifierFormat
            ("agent = %@ AND structure = %@ AND code.pcodCode = %@ AND pcumAnnee = %@ AND mois.moisCode < %@",values);
		}
        EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("mois.moisCode",
                                                                 EOSortOrdering.CompareDescending);
		
        EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier,new NSArray(sort));
        NSArray cumuls = editingContext.objectsWithFetchSpecification(fs);
        try {
			return (EOPayeCumul)cumuls.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
    }
	/** methode de classe permettant de trouver tous les derniers cumuls pour un code cumul et pour des structures différentes
	 * A utiliser pour trouver des montants puisque les montant sont cumules par structure
	@param editingContext context dans lequel fetcher les cumuls
	@param mois mois courant
	@param code code de ce cumul
	@param agent
	@return cumuls trouves
	*/
	public static NSArray rechercherDerniersCumuls(EOEditingContext editingContext,EOPayeMois mois,String code, EOIndividu agent) {
	    // Rechercher sur l'agent, l'année et un mois inférieur ou égal au mois courant
	    NSMutableArray values = new NSMutableArray(agent);
	    values.addObject(code);
		values.addObject(mois.moisAnnee());
		values.addObject(mois.moisCode());
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("agent = %@  AND code.pcodCode = %@ AND pcumAnnee = %@ AND mois.moisCode <= %@",values);
	    EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("mois.moisCode",EOSortOrdering.CompareDescending);
		
	    EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier,new NSArray(sort));
	    NSArray cumuls = editingContext.objectsWithFetchSpecification(fs);
	    // on recherche tous les cumuls ayant le même code cumul et éventuellement des rubriques différentes
	    // pour cela on garde un tableau de rubriques pour ce code cumul dans un dictionnaire dont les clés sont les structures
	    NSMutableArray derniersCumuls = new NSMutableArray();
	    NSMutableDictionary dictRubriquesPourStructure = new NSMutableDictionary();
	    Enumeration e = cumuls.objectEnumerator();
	    while (e.hasMoreElements()) {
	    		EOPayeCumul cumul = (EOPayeCumul)e.nextElement();
	    		NSMutableArray rubriques = (NSMutableArray)dictRubriquesPourStructure.objectForKey(cumul.structure());
	    		if (rubriques == null) {
	    			// on n'a pas encore trouvé de cumul pour cette structure
	    			rubriques = new NSMutableArray(cumul.rubrique());
	    			dictRubriquesPourStructure.setObjectForKey(rubriques,cumul.structure());
	    			derniersCumuls.addObject(cumul);
	    		} else if (rubriques.containsObject(cumul.rubrique()) == false) {
	    			derniersCumuls.addObject(cumul);
	    			rubriques.addObject(cumul.rubrique());
	    		}
	    }
	    return derniersCumuls;
	}
	/** methode de classe permettant de trouver tous les derniers cumuls pour un code cumul et pour des structures différentes
	 * correspondant a un m&ecirc;me mois (cas des agents payes sur plusieurs structures)
	 * A utiliser pour trouver des montants puisque les montant sont cumules par structure
	@param editingContext context dans lequel fetcher les cumuls
	@param mois mois courant
	@param code code de ce cumul
	@param agent
	@return cumuls trouves
	*/
	public static NSArray rechercherDerniersCumulsPourMemeMois(EOEditingContext editingContext,EOPayeMois mois,String code, EOIndividu agent) {
	    NSArray cumuls = rechercherDerniersCumuls(editingContext,mois,code,agent);
	    if (cumuls.count() <= 1) {
	    		return cumuls;
	    }
	    EOPayeCumul premierCumul = (EOPayeCumul)cumuls.objectAtIndex(0);
	    long moisPremierCumul = premierCumul.mois().moisCode().longValue();
	    NSMutableArray derniersCumuls = new NSMutableArray(premierCumul);
	    for (int i = 1; i < cumuls.count();i++) {
	    		EOPayeCumul cumul = (EOPayeCumul)cumuls.objectAtIndex(i);
		    long moisCumul = cumul.mois().moisCode().longValue();
		    if (moisCumul == moisPremierCumul) {
		    	derniersCumuls.addObject(cumul);
		    } else {
		    	break;
		    }
	    }
	    return derniersCumuls;
	}
	/** methode de classe permettant de trouver le dernier cumul pour un code cumul. A utiliser pour trouver des assiettes puisqu'elles sont identiques
	 * dans tous les cumuls d'un mois pour un code cumul identique
	@param editingContext context dans lequel fetcher les cumuls
	@param mois mois courant
	@param code code de ce cumul
	@param agent
	@return cumul trouve
	*/
	public static EOPayeCumul rechercherDernierCumul(EOEditingContext editingContext,EOPayeMois mois,String code, EOIndividu agent) {
	    // Rechercher sur l'agent, la structure et l'année
	    NSMutableArray values = new NSMutableArray(agent);
	    values.addObject(code);
		values.addObject(mois.moisAnnee());
		values.addObject(mois.moisCode());
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("agent = %@  AND code.pcodCode = %@ AND pcumAnnee = %@ AND mois.moisCode <= %@",values);
	    EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("mois.moisCode",EOSortOrdering.CompareDescending);
		
	    EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier,new NSArray(sort));
	    NSArray cumuls = editingContext.objectsWithFetchSpecification(fs);
	    try {
			return (EOPayeCumul)cumuls.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}

    /** methode de classe permettant de trouver le dernier cumul lie a un code, pour
        un agent qui a l'assiette la plus haute
        pour l'annee concernee
        @param editingContext context dans lequel fetcher les cumuls
        @param annee annee pour lequel rechercher ce cumul
        @param agent
        @return cumul trouve
    */
    public static EOPayeCumul rechercherCumulAvecAssietteMax(EOEditingContext editingContext,Number annee,String code, EOIndividu agent) {
        // Rechercher sur l'agent l'année
        NSMutableArray values = new NSMutableArray(agent);
        values.addObject(annee);
        values.addObject(code);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
            ("agent = %@ AND pcumAnnee = %@ AND code.pcodCode = %@",values);
        EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("pcumBase",
                                                                 EOSortOrdering.CompareDescending);
		
        EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier,new NSArray(sort));
		NSArray cumuls = editingContext.objectsWithFetchSpecification(fs);
        try {
            return (EOPayeCumul)cumuls.objectAtIndex(0);
        } catch (Exception e) {
            return null;
        }
    }
    /** methode de classe permettant de trouver le dernier cumul lie a un code, pour
    un agent qui a l'assiette la plus haute pour l'annee concernee et la structure concernee
    @param editingContext context dans lequel fetcher les cumuls
    @param annee annee pour lequel rechercher ce cumul
    @param agent
    @param structure concernee
    @return cumul trouve
*/
public static EOPayeCumul rechercherCumulAvecAssietteMax(EOEditingContext editingContext,Number annee,String code, EOIndividu agent,EOStructure structure) {
    // Rechercher sur l'agent l'année
    NSMutableArray values = new NSMutableArray(agent);
    values.addObject(annee);
    values.addObject(code);
    values.addObject(structure);
    EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
        ("agent = %@ AND pcumAnnee = %@ AND code.pcodCode = %@ AND structure = %@",values);
    EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("pcumBase",
                                                             EOSortOrdering.CompareDescending);
	
    EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier,new NSArray(sort));
	NSArray cumuls = editingContext.objectsWithFetchSpecification(fs);
    try {
        return (EOPayeCumul)cumuls.objectAtIndex(0);
    } catch (Exception e) {
        return null;
    }
}
    /**  methode de classe permettant de trouver les cumuls pour un agent, un code et
        pour un mois
        @param editingContext context dans lequel fetcher les cumuls
        @param mois mois pour lequel rechercher ce cumul
        @param code
        @param agent
        @return cumuls trouves
    */
    public static NSArray rechercherCumuls(EOEditingContext editingContext,EOPayeMois mois,String code, EOIndividu agent) {
        // Rechercher sur l'agent, le code et le mois
        NSMutableArray values = new NSMutableArray(agent);
        values.addObject(code);
        values.addObject(mois);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
            ("agent = %@ AND code.pcodCode = %@ AND mois = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier, null);
        return editingContext.objectsWithFetchSpecification(fs);
    }
    /**  methode de classe permettant de trouver les cumuls pour un agent, une structure et
        pour un mois
        @param editingContext context dans lequel fetcher les cumuls
        @param mois mois pour lequel rechercher ce cumul
        @param agent
        @param structure
        @return cumuls trouves
        */
    public static NSArray rechercherCumuls(EOEditingContext editingContext,EOPayeMois mois, EOIndividu agent,EOStructure structure) {
        // Rechercher sur l'agent, la structure et le mois
        NSMutableArray values = new NSMutableArray(agent);
        values.addObject(structure);
        values.addObject(mois);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
            ("agent = %@ AND structure = %@ AND mois = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier, null);
        return editingContext.objectsWithFetchSpecification(fs);

    }
	/**  methode de classe permettant de trouver les cumuls pour un agent et
        pour un mois
        @param editingContext context dans lequel fetcher les cumuls
        @param mois mois pour lequel rechercher ce cumul
        @param agent
        @return cumuls trouves
        */
    public static NSArray rechercherCumuls(EOEditingContext editingContext,EOPayeMois mois, EOIndividu agent) {
        // Rechercher sur l'agent, la structure et le mois
        NSMutableArray values = new NSMutableArray(agent);
		values.addObject(mois);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("agent = %@  AND mois = %@",values);
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("pcumType",EOSortOrdering.CompareAscending);
		EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier, new NSArray(sort));
        return editingContext.objectsWithFetchSpecification(fs);
    }
	
    /** methode de classe permettant de trouver tous les cumuls rattaches a un m&ecirc;me numero de SIRET (de structure) pour un agent,  un mois, un code
    @param mois mois pour lequel rechercher ce cumul
    @param code identifiant du cumul
    @param agent
    @param siret
    @param editingContext context dans lequel fetcher le cumul
    @return : object cumul trouve
    */
    public static NSArray rechercherCumulsPourSiret(EOPayeMois mois, String code,EOIndividu agent, String siret,EOEditingContext editingContext) {
        // Rechercher sur l'agent, la structure et le mois
        NSMutableArray values = new NSMutableArray(agent);
        values.addObject(code);
        values.addObject(mois);
        values.addObject(siret);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
            ("agent = %@ AND code.pcodCode = %@ AND mois = %@ AND structure.siret = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier, null);
        return editingContext.objectsWithFetchSpecification(fs);
    }
	/**  methode de classe permettant de trouver tous les cumuls pour un agent, une structure jusqu'au mois courant
		@param editingContext context dans lequel fetcher les cumuls
        @param mois mois pour lequel rechercher ce cumul
        @param agent
        @param structure
        @return cumuls trouves
        */
    public static NSArray rechercherCumulsPourAnneeIndividuStructure(EOEditingContext editingContext,Number annee, EOIndividu agent,EOStructure structure,NSArray sorts) {
        // Rechercher sur l'agent, la structure et l'année
        NSMutableArray values = new NSMutableArray(agent);
        values.addObject(structure);
		values.addObject(annee);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
            ("agent = %@ AND structure = %@ AND pcumAnnee = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier, sorts);
        return editingContext.objectsWithFetchSpecification(fs);
    }
    /**  methode de classe permettant de trouver tous les cumuls pour un agent, une structure jusqu'au mois courant
	@param editingContext context dans lequel fetcher les cumuls
    @param annee annee pour laquelle rechercher ce cumul
    @param code code cumul
    @param agent
    @param structure
    @return cumul trouve
    */
	public static EOPayeCumul rechercherDernierCumulPourAnneeIndividuStructure(EOEditingContext editingContext,Number annee, String code,EOIndividu agent,EOStructure structure) {
	    // Rechercher sur l'agent, la structure et l'année
	    NSMutableArray values = new NSMutableArray(agent);
	    values.addObject(code);
	    values.addObject(structure);
		values.addObject(annee);
	    EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("agent = %@ AND code.pcodCode = %@ AND structure = %@ AND pcumAnnee = %@",values);
	    NSMutableArray sorts = new NSMutableArray();
	    EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("mois.moisCode",EOSortOrdering.CompareDescending);
	    sorts.addObject(sort);
	    sort = EOSortOrdering.sortOrderingWithKey("pcumBase",EOSortOrdering.CompareDescending);
	    sorts.addObject(sort);
	    EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier, sorts);
	    fs.setFetchLimit(1);
	    try {
	    		return (EOPayeCumul)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
	    } catch (Exception e) {
	    		return null;
	    }
	}
	 /**  methode de classe permettant de trouver tous les cumuls pour un agent, une structure jusqu'au mois courant
	@param editingContext context dans lequel fetcher les cumuls
    @param annee annee pour laquelle rechercher ce cumul
    @param code code cumul
    @param agent
    @return cumul trouve
    */
	public static EOPayeCumul rechercherDernierCumulPourAnneeIndividu(EOEditingContext editingContext,Number annee, String code,EOIndividu agent) {
	    // Rechercher sur l'agent, la structure et l'année
	    NSMutableArray values = new NSMutableArray(agent);
	    values.addObject(code);
		values.addObject(annee);
	    EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("agent = %@ AND code.pcodCode = %@ AND pcumAnnee = %@",values);
	    NSMutableArray sorts = new NSMutableArray();
	    EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("mois.moisCode",EOSortOrdering.CompareDescending);
	    sorts.addObject(sort);
	    sort = EOSortOrdering.sortOrderingWithKey("pcumBase",EOSortOrdering.CompareDescending);
	    sorts.addObject(sort);
	    EOFetchSpecification fs = new EOFetchSpecification("PayeCumul",qualifier, sorts);
	    fs.setFetchLimit(1);
	    try {
	    		return (EOPayeCumul)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
	    } catch (Exception e) {
	    		return null;
	    }
	}
	
	/** trouve la derni&egrave;re assiette d'une famille de cumuls designee par son code cumul
	 * @param editingContext dans lequel fetchter les objets
	 * @param mois avant lequel effectuer la recherche
	 * @param code cumul
	 * @param individu concerne
	 * @param tableau des cumuls courants
	 * @return montant de l'assiette
	 */
	 public static double trouverDerniereAssiette(EOEditingContext editingContext,EOPayeMois mois,String code,EOIndividu individu,NSArray cumulsCourants) {
        // tous les cumuls ayant la même assiette, on peut prendre n'importe lequel
        EOPayeCumul cumul = EOPayeCumul.rechercherDernierCumul(editingContext,mois,code,individu);
        if (cumul != null && cumulsCourants.containsObject(cumul) == false) { // ce n'est pas un cumul juste créé
            return cumul.pcumBase().doubleValue();
        } else {
            return 0;
        }
    }

	/** trouve la valeur du cumul de montants d'une famille de cumuls designee par son code cumul
	 * @param editingContext dans lequel fetchter les objets
	 * @param mois avant lequel effectuer la recherche
	 * @param code cumul
	 * @param individu concerne
	 * @param tableau des cumuls courants
	 * @return valeur du montant
	 */
	public static double trouverDernierMontant(EOEditingContext editingContext,EOPayeMois mois,String code,EOIndividu individu,NSArray cumulsCourants) {
		// les montants étant cumulés par structure, il faut rechercher tous les cumuls, les trier par structure et prendre les montants
        NSArray cumuls = EOPayeCumul.rechercherDerniersCumuls(editingContext,mois, code,individu);
        Enumeration e = cumuls.objectEnumerator();
        double montantTotal = 0;
        while (e.hasMoreElements()) {
        		EOPayeCumul cumul = (EOPayeCumul)e.nextElement();
        		 if (cumul != null && cumulsCourants.containsObject(cumul) == false) { // ce n'est pas un cumul juste créé
                    montantTotal +=  cumul.pcumMontant().doubleValue() + cumul.pcumRegul().doubleValue();
        		 }
        }
        return montantTotal;
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}
