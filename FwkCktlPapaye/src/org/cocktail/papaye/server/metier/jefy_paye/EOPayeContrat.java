/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Fri Mar 14 09:16:15 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.common.EOInfoBulletinSalaire;
import org.cocktail.papaye.server.metier.grhum.EOIndividu;
import org.cocktail.papaye.server.n4ds.ConstantesDads;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOPayeContrat extends _EOPayeContrat {
	// constantes
	/** Le mode de calcul de paie est direct */
	public static final String CALCUL_DIRECT = "D";
	/** Le mode de calcul de paie est invers&eacute (&agrave partir du net) */
	public static final String CALCUL_INVERSE = "I";
	/** Le mode de calcul de paie est forfaitaire */
	public static final String FORFAITAIRE = "I";
	private static final String INCONNU = "9999";
	private static final String SEDENTAIRE = "01"; 
	private static final String CLASSIFICATION_INCONNUE = "90";

	public EOPayeContrat() {
		super();
	}

	public String indiceMajore() {

		if (indiceContrat() != null)
			return indiceContrat();

		return "";
	}

	// méthodes ajoutées
	public boolean estUnCalculDirect() {
		return modeCalcul().equals(EOPayeContrat.CALCUL_DIRECT);
	}
	/** retourne true si l'agent peut avoir ses cotisations calculees avec un plafond reduit de securite sociale */
	public boolean peutAvoirPlafondReduit() {
		return (numQuotRecrutement() != null && numQuotRecrutement().doubleValue() >= 50 && numQuotRecrutement().doubleValue() <= 80);
	}
	/** retourne true si il faut calculer un plafond reduit de securite sociale */
	public boolean utiliserPlafondReduit() {
		if (temPlafondReduit() != null) {
			return temPlafondReduit().equals(Constantes.VRAI);
		} else {
			return false;
		}
	}
	/** retourne true si le contrat est a temps plein */
	public boolean travailATempsPlein() {
		if (temTempsPlein() != null) {
			return temTempsPlein().equals(Constantes.VRAI);
		} else {
			return true;
		}
	}
	/** retourne true si l'agent doit payer la cotisation solidarite
	 * retourne false par defaut */
	public boolean cotiseSolidarite() {
		if (temCotiseSolidarite() != null) { 
			return temCotiseSolidarite().equals(Constantes.VRAI);
		} else {
			return false;
		}
	}
	/** retourne la quotite travaillee sous la forme XXXX et null si temps plein (pour DADS) */
	public String quotite() {
		if (numQuotRecrutement() == null)
			return null;
		
		double quotite = numQuotRecrutement().doubleValue();
		if (quotite == 0.0 || quotite == 100.0)
			return null;
		else {
			double coefficient = quotite;
			switch ((int) quotite) {
			case 80:
				if (temTempsPlein() != null	&& temTempsPlein().equals(Constantes.VRAI)) 
					coefficient = (6.0 / 7.0) * 100;
				break;
			case 90:
				if (temTempsPlein() != null	&& temTempsPlein().equals(Constantes.VRAI)) 
					coefficient = (32.0 / 35.0) * 100;
				break;
			}
			
			int valeurEntiere = (int) (coefficient * 100);
			return new Integer(valeurEntiere).toString();
		}
	}
	/** Code situation salariee */
	public String codeSituationSalariee() {
		return null;
	}
	/** statut categoriel : pas de statut categoriel */
	public String codeStatutCategoriel() {
		return "90";
	}
	
	/** statut categoriel pour les artistes */
	public String codeStatutCategorielArtiste() {
		if (statut().regimeCotisation() != null && statut().regimeCotisation().pregComplementaire() != null) {
			if (statut().regimeCotisation().pregComplementaire().equals(ConstantesDads.CARCICAS)) {
				return ConstantesDads.CATEGORIE_ARTISTE_CADRE;	
			} else if (statut().regimeCotisation().pregComplementaire().equals(ConstantesDads.CAPRICAS)) {
				return ConstantesDads.CATEGORIE_ARTISTE_NON_CADRE;
			} 
		}
		
		return null;

	}
	/** categorie de l'agent : pas de categorie geree */
	public String categorieAgent() {
		if (grade() != null && grade().corps() != null && grade().corps().cCategorie() != null) {
			if (grade().corps().cCategorie().equals("A")) {
				return "01";
			} else if (grade().corps().cCategorie().equals("B")) {
				return "02";
			} else if (grade().corps().cCategorie().equals("C")) {
				return "03";
			} else {
				return "90";
			}
		} else
			return "90";
		
	}
	/** corps : pas de corps gere dans Papaye */
	public String corps() {
		if (grade() != null && grade().corps() != null) {
			String corps = grade().corps().cCorps();
			if (corps.length() < 4) {
				corps = corps + "0";
			}
			return corps;
		} else {
			return INCONNU;
		}
	}
	/** grade de l'agent */
	public String gradeAgent() {
		if (grade() != null) {
			return grade().cGrade();
		} else {
			return INCONNU;
		}
	}
	/** categorie emploi : non geree dans Papaye */
	public String categorieEmploi() {
		return INCONNU;
	}

	/** retourne la nature de l'emploi */
	public String populationEmploi() throws Exception {
		if (fonction() != null) {
			return fonction().foncLibelle();
		} else if (grade() != null) {
			return grade().llGrade();
		} else {
			throw new Exception("Vous devez fournir un grade ou une fonction");
		}
	}


	/** retourne la nature de l'emploi */
	public String natureEmploi() throws Exception {
		if (fonction() != null) {
			return fonction().foncLibelle();
		} else if (grade() != null) {
			return grade().llGrade();
		} else {
			throw new Exception("Vous devez fournir un grade ou une fonction");
		}
	}
	/** retourne la classification d'emploi **/
	public String classificationEmploi() {
		if (statut() != null && statut().regimeCotisation() != null && statut().regimeCotisation().estCNRACL()) {
			return SEDENTAIRE;
		} else {
			return CLASSIFICATION_INCONNUE;
		}
	}
	/** retourne un dictionnaire comportant :<BR>
        <UL>un tableau des rubriques standards et personnelles liees a ce contrat
        triees par code croissant.</UL>
        <UL>un tableau des personnalisations associées à ces rubriques</UL>
	 */
	public NSDictionary rubriques(EOPayeMois mois) throws Exception {
		NSMutableArray rubriques = new NSMutableArray();
		// commencer par identifier les profils actifs pour le statut lié à ce contrat
		// en faisant un fetch direct
		// statut
		NSMutableArray values = new NSMutableArray(statut());
		// profils valides à cette époque : profMdebut <= mois.moisCode < profMfin (si une rubrique
		// est invalidée un mois, elle ne doit pas être visible dans la paie de ce mois
		Number moisCode = mois.moisCode();
		values.addObject(moisCode);
		values.addObject(moisCode);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
		( "statut = %@ AND profMdebut <= %@ AND profMfin > %@ and temValide = 'O'" ,values);
		EOFetchSpecification fs = new EOFetchSpecification(EOPayeProfil.ENTITY_NAME,qualifier, null);
		// on prefetechera les rubriques
		fs.setPrefetchingRelationshipKeyPaths(new NSArray(EOPayeProfil.RUBRIQUE_KEY));
		NSArray profils = editingContext().objectsWithFetchSpecification(fs);
		try {
			Enumeration e = profils.objectEnumerator();
			while (e.hasMoreElements()) {
				// pour chaque profil ajouter la rubrique standard valide au tableau
				EOPayeProfil profil = (EOPayeProfil)e.nextElement();
				EOPayeRubrique rubrique = profil.rubrique();
				// rubrique valide à cette époque : prubMdebut <= mois.moisCode <= prubfMfin
				if (rubrique.prubMdebut().intValue() <= moisCode.intValue() &&
						rubrique.prubMfin().intValue() >= moisCode.intValue()) { // rubrique valide
					// vérifier si on est dans le cas d'un étranger non soumis à SS
					if (individu().paieSecu()) {
						rubriques.addObject(rubrique);
					} else {
						if (!rubrique.rentreDansCotisation() ||
								(rubrique.rentreDansCotisation() &&
										(rubrique.estContributionGeneralisee() || rubrique.estTaxeSurSalaire()))) {
							rubriques.addObject(rubrique);
						}
					}
				}
			}
		} catch (Exception e) {throw e;}
		// récupérer les rubriques personnelles
		try {
			Enumeration e = personnalisations().objectEnumerator();
			while (e.hasMoreElements()) {
				// pour chaque profil ajouter sa rubrique au tableau
				EOPayePerso personnalisation = (EOPayePerso)e.nextElement();
				if (personnalisation.temValide().equals(Constantes.VRAI)) {
					EOPayeRubrique rubrique = personnalisation.rubrique();
					if (rubrique.temValide().equals(Constantes.VRAI) &&
							rubrique.estAPayerCeMois(mois)) {
						// rubrique valide et payable ce mois
						rubriques.addObject(rubrique);
					}
				}
			}
		} catch (Exception e) {throw e; }

		// trier les rubriques sur le code de rubrique
		EOSortOrdering.sortArrayUsingKeyOrderArray(rubriques,new NSArray(EOSortOrdering.sortOrderingWithKey("prubClassement",EOSortOrdering.CompareAscending)));
		// préparer les personnalisations
		NSArray personnalisations = preparerPersonnalisations(rubriques,personnalisations());
		NSMutableDictionary dict = new NSMutableDictionary(2);
		dict.setObjectForKey(rubriques,"rubriques");
		dict.setObjectForKey(personnalisations,"personnalisations");
		return dict;
	}
	/** retourne un dictionnaire comportant :<BR>
        <UL>un tableau des rubriques standards et personnelles de remuneration
        liees a ce contrat et valides pour ce mois, 
        triees par code croissant.</UL>
        <UL>un tableau des personnalisations associées à ces rubriques</UL>
        @param mois
	 */
	public NSDictionary rubriquesRappelPourMois(EOPayeMois mois) throws Exception {
		NSMutableArray rubriques = new NSMutableArray();
		// commencer par identifier les profils valides à cet époque pour ce statut
		// en faisant un fetch direct
		// statut
		NSMutableArray values = new NSMutableArray(statut());
		// date début
		values.addObject(mois.moisCode());
		// date fin
		values.addObject(mois.moisCode());
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
		( "statut = %@ AND profMdebut <= %@ AND profMfin > %@",values);
		EOFetchSpecification fs = new EOFetchSpecification(EOPayeProfil.ENTITY_NAME,qualifier, null);
		// on prefetechera les rubriques
		fs.setPrefetchingRelationshipKeyPaths(new NSArray(EOPayeProfil.RUBRIQUE_KEY));
		NSArray profils = editingContext().objectsWithFetchSpecification(fs);
		try {
			Enumeration e = profils.objectEnumerator();
			while (e.hasMoreElements()) {
				// pour chaque profil ajouter sa rubrique au tableau
				EOPayeRubrique rubrique = ((EOPayeProfil)e.nextElement()).rubrique();
				//  if (rubrique.estAPayer() && rubrique.rentreDansAssiette()) { // pour les dom-tom, on l'a supprimé le 17/03/04
				if (rubrique.estAPayer()) {
					rubriques.addObject(rubrique);
				}
			}
		} catch (Exception e) { throw e; }

		// récupérer les rubriques personnelles permanentes
		try {
			Enumeration e = personnalisations().objectEnumerator();
			while (e.hasMoreElements()) {
				// pour chaque profil ajouter sa rubrique au tableau
				EOPayePerso personnalisation = (EOPayePerso)e.nextElement();
				if (personnalisation.temValide().equals(Constantes.VRAI) &&
						personnalisation.temPermanent().equals(Constantes.VRAI)) {
					EOPayeRubrique rubrique = personnalisation.rubrique();
					if (rubrique.temValide().equals(Constantes.VRAI) &&
							rubrique.estAPayerCeMois(mois)) {
						// rubrique valide et payable ce mois
						rubriques.addObject(rubrique);
					}
				}
			}
			// trier les rubriques sur le code de rubrique
			EOSortOrdering.sortArrayUsingKeyOrderArray(rubriques,new NSArray(EOSortOrdering.sortOrderingWithKey("prubClassement",EOSortOrdering.CompareAscending)));
			// préparer les personnalisations
			NSArray personnalisations = preparerPersonnalisations(rubriques,personnalisations());
			NSMutableDictionary dict = new NSMutableDictionary(2);
			dict.setObjectForKey(rubriques,"rubriques");
			dict.setObjectForKey(personnalisations,"personnalisations");
			return dict;
		} catch (Exception e) {throw e; }

	}    
	/** retourne true si le contrat correspond a une somme isolee (i.e contrat concernant une annee anterieure pour
	 * un salarie ayant quitte ou quittant l'entreprise l'annee de la DADS) 
	 * @param annee de la DADS
	 * @param editingContext dans lequel fetcher les objets */
	public boolean correspondSommeIsolee(int anneeDADS) {
		NSTimestamp debut = dDebContratTrav();
		GregorianCalendar date = new GregorianCalendar();
		date.setTime(debut);
		int annee = date.get(Calendar.YEAR);
		if  (annee == anneeDADS) {
			// le contrat démarre la même année que la DADS
			return false;
		}
		// début de contrat antérieur à l'année de la DADS
		NSTimestamp fin = dFinContratTrav();
		if (fin == null) {
			// CDI
			return false;
		} else {
			date.setTime(fin);
			annee = date.get(Calendar.YEAR);
			if (annee < anneeDADS) {
				// le contrat se termine avant la déclaration DADS, c'est forcément un rappel
				// Vérifier si il existe des contrats postérieurs à la date de fin du contrat
				NSArray contrats = EOPayeContrat.contratsPosterieursValidesPourIndividu(editingContext(), individu(), dFinContratTrav());
				return (contrats.count() == 0);
			} else  {	// 19/02/08	- correction les sommes isolées ne doivent pas être déclarées pour l'année en cours
				// le contrat ne se termine pas l'année de la DADS
				return false;
			} /*else {
				//	sinon rechercher dans l'historique si l'agent a déjà été payé pour ce contrat en décembre de l'année précédente
				int moisCode = (anneeDADS - 1) * 100 + 12;
				EOPayeMois moisDecembre = EOPayeMois.moisAvecCode(editingContext(), new Integer(moisCode));
				EOPayeHisto historique = EOPayeHisto.chercherHistoriquePourMoisEtContrat(editingContext(),moisDecembre,this,null);
				return (historique == null);	// l'agent n'a pas été payé pour ce contrat l'année précédente
			}*/
		}
	}
	public EOGlobalID idContrat() {
		return editingContext().globalIDForObject(this);
	}
	/** Initialisation d'un nouveau contrat */
	public void initContrat(EOIndividu individu)
	{
		setIndividuRelationship(individu);

		setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

		setTemPaiementPonctuel("N");
		setTemAnnulation("N");
		setTemPreContrat("N");
		setTemPayeLocale("O");
		setTemPayeUtile("N");
		setTemTempsPlein("N");
	}
	public String toString() {
		return new String(getClass().getName() +
				"\ndebut contrat : "+ dDebContratTrav() +
				"\nfin contrat : "+ dFinContratTrav() +
				"\nreference : "+ referenceContrat() +
				"\nechelon : "+ cEchelon() +
				"\nindice : "+ indiceContrat() +
				"\nmontant forfaitaire : "+ montantForfaitaire() +
				"\nmontant mensuel remuneration : "+ montantMensuelRemu() +
				"\nnombre heures contrat : "+ nbHeuresContrat() +
				"\taux horaire : "+ tauxHoraireContrat() +
				"\nquotite travaillee : "+ numQuotRecrutement() +
				"\nnb jours mensuels : " + nbJoursContrat() +
				"\nmode de calcule : "+ modeCalcul() +
				"\nstatut : "+ statut() +
				"\nstructure : "+ structure() +
				"\nsecteur : "+ secteur() +
				"\ntype contrat : "+ typeContratTravail());
	}
	/** retourne tous les contrats affectes a ce secteur */
	public static NSArray contratsCourantsPourSecteur(EOEditingContext editingContext,EOPayeSecteur secteur) {
		NSMutableArray values = new NSMutableArray(Constantes.FAUX);
		values.addObject(Constantes.VRAI);
		values.addObject(secteur);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
		("temAnnulation = %@ AND temPayeLocale = %@ AND secteur = %@",values);
		EOFetchSpecification fs = new EOFetchSpecification(EOPayeContrat.ENTITY_NAME,qualifier, null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	/** Retourne tous les contrats d'un individu posterieurs a la date passee en param&egrave;tre */
	public static NSArray contratsPosterieursValidesPourIndividu(EOEditingContext editingContext,EOIndividu individu,NSTimestamp date) {
		NSMutableArray values = new NSMutableArray(Constantes.FAUX);
		values.addObject(Constantes.VRAI);
		values.addObject(individu);
		values.addObject(date);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
		("temAnnulation = %@ AND temPayeLocale = %@ AND individu = %@ AND dDebContratTrav >= %@",values);
		EOFetchSpecification fs = new EOFetchSpecification(EOPayeContrat.ENTITY_NAME,qualifier, null);
		return editingContext.objectsWithFetchSpecification(fs);

	}
	/** retourne tous les contrats actifs lies a ce statut */
	public static NSArray contratsValidesPourStatut(EOEditingContext editingContext,EOPayeStatut statut) {
		NSMutableArray values = new NSMutableArray(Constantes.FAUX);
		values.addObject(Constantes.VRAI);
		values.addObject(statut);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
		("temAnnulation = %@ AND temPayeLocale = %@ AND statut = %@",values);
		EOFetchSpecification fs = new EOFetchSpecification(EOPayeContrat.ENTITY_NAME,qualifier, null);
		return editingContext.objectsWithFetchSpecification(fs);
	}

	/** rechercher le contrat en cours d'un individu pour un secteur 
		retourne null si pas de contrat trouve
	 */
	public static EOPayeContrat rechercherContratCourantPourIndividuEtSecteur (EOIndividu individu, EOPayeSecteur secteur,EOEditingContext ec) {
		NSMutableArray args = new NSMutableArray(individu);
		args.addObject(secteur);
		args.addObject(new NSTimestamp());
		args.addObject(new NSTimestamp());

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("individu = %@ AND secteur = %@ AND dDebContratTrav <=%@ and (dFinContratTrav >=%@ or dFinContratTrav = nil)",args);
		EOFetchSpecification fs = new EOFetchSpecification(EOPayeContrat.ENTITY_NAME,qual,null);
		try { 
			return (EOPayeContrat)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) { return null; }
	}
	/** rechercher dans les bulletins de salaire de l'annee les contrats dont le statut est non titulaire
	 * 	retourne un tableau contenant les contrats trouv&eacue;s
	 */
	public static NSArray rechercherContratsNonTitulairePourIndividuEtAnnee(EOEditingContext ec,EOIndividu individu,Number annee) {
		return rechercherContratsPourIndividuEtAnnee(ec,individu,annee,false);
	}
	/** rechercher dans les bulletins de salaire de l'annee les contrats dont le statut est titulaire
	 * 	retourne un tableau contenant les contrats trouv&eacue;s
	 */
	public static NSArray rechercherContratsTitulairePourIndividuEtAnnee(EOEditingContext ec,EOIndividu individu,Number annee) {
		return rechercherContratsPourIndividuEtAnnee(ec,individu,annee,true);
	}
	/** rechercher le dernier contrat d'un individu pour un secteur
		@param individu
		@param secteur (peut &ecirc;tre nul, en quel cas on retourne le dernier contrat de cet individu tout secteur confondu)
		retourne null si pas de contrat trouve
	 */
	public static EOPayeContrat rechercherDernierContratPourIndividuEtSecteur (EOIndividu individu, EOPayeSecteur secteur,EOEditingContext ec) {

		try { 

			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeContrat.INDIVIDU_KEY + " = %@",
					new NSArray(individu)));

			if (secteur != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeContrat.INDIVIDU_KEY + " = %@",
						new NSArray(individu)));

			return fetchFirstByQualifier(ec, new EOAndQualifier(mesQualifiers));

		} catch (Exception e) { return null; }
	}

	// methodes privees
	// prepare un tableau comportant toutes les personnalisations
	// Pour cela, on parcourt le tableau des rubriques
	// Si une rubrique est standard, on ajoute au tableau des personnalisations
	// une nouvelle personnalisation vide
	// Si c'est une rubrique personnelle : on recherche la prochaine personnalisation
	// associee à cette rubrique, non encore sélectionnee (cas où il y a plusieurs personnalisations
	// d'une même rubrique
	private NSArray preparerPersonnalisations(NSArray rubriques,NSArray personnalisations) {
		NSMutableArray persos = new NSMutableArray(rubriques.count());
		NSMutableArray persosDejaSelectionnees = new NSMutableArray();
		java.util.Enumeration e = rubriques.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeRubrique rubrique = (EOPayeRubrique)e.nextElement();
			if (rubrique.estRubriqueStatut()) {
				// c'est une rubrique standard, prendre son libellé
				EOPayePerso perso = new EOPayePerso();
				persos.addObject(perso);
			} else {
				// c'est une rubrique personnelle
				// elle n'a pas encore été vue
				Enumeration e1 = personnalisations.objectEnumerator();
				while (e1.hasMoreElements()) {
					EOPayePerso personnalisation = (EOPayePerso)e1.nextElement();
					if (personnalisation.rubrique() == rubrique) {
						if (personnalisation.temValide().equals(Constantes.VRAI) &&
								persosDejaSelectionnees.containsObject(personnalisation) == false) {
							persos.addObject(personnalisation);
							persosDejaSelectionnees.addObject(personnalisation);
							break;
						}
					}
				}
			}
		}
		return persos;
	}
	/** rechercher dans les bulletins de salaire de l'annee les contrats dont le statut est non titulaire
	 * 	retourne un tableau contenant les contrats trouv&eacue;s
	 */
	private static NSArray rechercherContratsPourIndividuEtAnnee(EOEditingContext ec,EOIndividu individu,Number annee,boolean estTitulaire) {
		NSArray infosBs = EOInfoBulletinSalaire.chercherInfosPourAnneeIndividu(ec,individu,annee,estTitulaire);

		NSMutableArray contratsTrouves = new NSMutableArray();
		java.util.Enumeration e = infosBs.objectEnumerator();
		while (e.hasMoreElements()) {
			EOInfoBulletinSalaire bs = (EOInfoBulletinSalaire)e.nextElement();
			if (contratsTrouves.containsObject(bs.contrat()) == false) {
				contratsTrouves.addObject(bs.contrat());
			}
		}
		return contratsTrouves;
	}

}
