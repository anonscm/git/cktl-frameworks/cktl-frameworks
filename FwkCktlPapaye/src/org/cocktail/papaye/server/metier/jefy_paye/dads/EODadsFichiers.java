/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.papaye.server.metier.jefy_paye.dads;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.metier.grhum.EOStructure;
import org.cocktail.papaye.server.metier.jefy_admin.EOExercice;
import org.cocktail.papaye.server.n4ds.PeriodeDadsActive;
import org.cocktail.papaye.server.n4ds.PeriodeDadsInactive;
import org.cocktail.papaye.server.n4ds.SousRubriqueAvecValeur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EODadsFichiers extends _EODadsFichiers {

    public EODadsFichiers() {
        super();
    }
    // méthodes ajoutées
    public void initAvec(EOEditingContext editingContext,SousRubriqueAvecValeur sousRubrique,EOExercice exercice,EOStructure structure,PeriodeDadsActive periodeActive,PeriodeDadsInactive periodeInactive) {
    	// on travaille sans les relations pour éviter une saturation mémoire
    	setDafiValeurRubrique(sousRubrique.valeur());
    	setCodeRubrique(sousRubrique.cle());
    	setExeOrdre(exercice.exeExercice());
    	if (structure != null) {
    		addObjectToBothSidesOfRelationshipWithKey(structure,"structure");
    		setCStructure(structure.cStructure());
    	}
    	if (periodeActive != null) {
    		setDafiPeriodeDebut(periodeActive.debutPeriode());
    		setDafiPeriodeFin(periodeActive.finPeriode());
    		setNoIndividu(periodeActive.contrat().individu().noIndividu());
    		addObjectToBothSidesOfRelationshipWithKey(periodeActive.contrat().individu(), "individu");
    		if (periodeActive.contrat().structure() != null) {
    			addObjectToBothSidesOfRelationshipWithKey(periodeActive.contrat().structure(), "structure");
    			setCStructure(periodeActive.contrat().structure().cStructure());
    		}
    	} else if (periodeInactive != null) {
    		setDafiPeriodeDebut(periodeInactive.debutPeriode());
    		setDafiPeriodeFin(periodeInactive.finPeriode());
    		setNoIndividu(periodeInactive.individu().noIndividu());
    		addObjectToBothSidesOfRelationshipWithKey(periodeInactive.individu(), "individu");
    	}
    }


    // Méthodes statiques
    /** retourne toutes les rubriques pour un exercice triees par code rubrique ascendant
     * @param exercice peut &ecirc;tre nul
     * @param estDadsNormale true si il s'agit des rubriques de la dads normale
     */
    public static NSArray rechercherRubriquesPourExercice(EOEditingContext editingContext, EOExercice exercice,boolean estDadsNormale) {
    	NSMutableArray args = new NSMutableArray();
    	String stringQualifier = "temDadsNormale = %@";
    	if (estDadsNormale) {
    		args.addObject(Constantes.VRAI);
    	} else {
    		args.addObject(Constantes.FAUX);
    	}
    	if (exercice != null) {
    		args.addObject(exercice);
    		stringQualifier = stringQualifier + " AND exercice = %@";
    	}
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
    	EOFetchSpecification fs = new EOFetchSpecification("DadsFichiers",qualifier,new NSArray(EOSortOrdering.sortOrderingWithKey("dadsCodage.codeRubrique", EOSortOrdering.CompareAscending)));
    	return editingContext.objectsWithFetchSpecification(fs);
    }
}
