// _EOPoemsBj.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPoemsBj.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPoemsBj extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PoemsBj";
	public static final String ENTITY_TABLE_NAME = "GRHUM.POEMS_BJ";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pbjKey";

	public static final String PBJ_CODE_APP_ETR_KEY = "pbjCodeAppEtr";
	public static final String PBJ_CODE_APP_PAY_KEY = "pbjCodeAppPay";
	public static final String PBJ_CODE_IMPUTATION_KEY = "pbjCodeImputation";
	public static final String PBJ_CODE_UTILISATEUR_KEY = "pbjCodeUtilisateur";
	public static final String PBJ_COMMENTAIRE_KEY = "pbjCommentaire";
	public static final String PBJ_CONSTATATION_CREDIT_KEY = "pbjConstatationCredit";
	public static final String PBJ_CONSTATATION_DEBIT_KEY = "pbjConstatationDebit";
	public static final String PBJ_D_MODIFICATION_KEY = "pbjDModification";
	public static final String PBJ_KEY_KEY = "pbjKey";
	public static final String PBJ_LIBELLE_KEY = "pbjLibelle";
	public static final String PBJ_REGLEMENT_CREDIT_KEY = "pbjReglementCredit";
	public static final String PBJ_REGLEMENT_DEBIT_KEY = "pbjReglementDebit";
	public static final String PBJ_TEM_VALIDE_KEY = "pbjTemValide";

//Colonnes dans la base de donnees
	public static final String PBJ_CODE_APP_ETR_COLKEY = "PBJ_CODE_APP_ETR";
	public static final String PBJ_CODE_APP_PAY_COLKEY = "PBJ_CODE_APP_PAY";
	public static final String PBJ_CODE_IMPUTATION_COLKEY = "PBJ_CODE_IMPUTATION";
	public static final String PBJ_CODE_UTILISATEUR_COLKEY = "PBJ_CODE_UTILISATEUR";
	public static final String PBJ_COMMENTAIRE_COLKEY = "PBJ_COMMENTAIRE";
	public static final String PBJ_CONSTATATION_CREDIT_COLKEY = "PBJ_CONSTATATION_CREDIT";
	public static final String PBJ_CONSTATATION_DEBIT_COLKEY = "PBJ_CONSTATATION_DEBIT";
	public static final String PBJ_D_MODIFICATION_COLKEY = "PBJ_D_MODIFICATION";
	public static final String PBJ_KEY_COLKEY = "PBJ_KEY";
	public static final String PBJ_LIBELLE_COLKEY = "PBJ_LIBELLE";
	public static final String PBJ_REGLEMENT_CREDIT_COLKEY = "PBJ_REGLEMENT_CREDIT";
	public static final String PBJ_REGLEMENT_DEBIT_COLKEY = "PBJ_REGLEMENT_DEBIT";
	public static final String PBJ_TEM_VALIDE_COLKEY = "PBJ_TEM_VALIDE";

// Relationships



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String pbjCodeAppEtr() {
    return (String) storedValueForKey(PBJ_CODE_APP_ETR_KEY);
  }

  public void setPbjCodeAppEtr(String value) {
    takeStoredValueForKey(value, PBJ_CODE_APP_ETR_KEY);
  }

  public String pbjCodeAppPay() {
    return (String) storedValueForKey(PBJ_CODE_APP_PAY_KEY);
  }

  public void setPbjCodeAppPay(String value) {
    takeStoredValueForKey(value, PBJ_CODE_APP_PAY_KEY);
  }

  public String pbjCodeImputation() {
    return (String) storedValueForKey(PBJ_CODE_IMPUTATION_KEY);
  }

  public void setPbjCodeImputation(String value) {
    takeStoredValueForKey(value, PBJ_CODE_IMPUTATION_KEY);
  }

  public String pbjCodeUtilisateur() {
    return (String) storedValueForKey(PBJ_CODE_UTILISATEUR_KEY);
  }

  public void setPbjCodeUtilisateur(String value) {
    takeStoredValueForKey(value, PBJ_CODE_UTILISATEUR_KEY);
  }

  public String pbjCommentaire() {
    return (String) storedValueForKey(PBJ_COMMENTAIRE_KEY);
  }

  public void setPbjCommentaire(String value) {
    takeStoredValueForKey(value, PBJ_COMMENTAIRE_KEY);
  }

  public String pbjConstatationCredit() {
    return (String) storedValueForKey(PBJ_CONSTATATION_CREDIT_KEY);
  }

  public void setPbjConstatationCredit(String value) {
    takeStoredValueForKey(value, PBJ_CONSTATATION_CREDIT_KEY);
  }

  public String pbjConstatationDebit() {
    return (String) storedValueForKey(PBJ_CONSTATATION_DEBIT_KEY);
  }

  public void setPbjConstatationDebit(String value) {
    takeStoredValueForKey(value, PBJ_CONSTATATION_DEBIT_KEY);
  }

  public String pbjDModification() {
    return (String) storedValueForKey(PBJ_D_MODIFICATION_KEY);
  }

  public void setPbjDModification(String value) {
    takeStoredValueForKey(value, PBJ_D_MODIFICATION_KEY);
  }

  public String pbjKey() {
    return (String) storedValueForKey(PBJ_KEY_KEY);
  }

  public void setPbjKey(String value) {
    takeStoredValueForKey(value, PBJ_KEY_KEY);
  }

  public String pbjLibelle() {
    return (String) storedValueForKey(PBJ_LIBELLE_KEY);
  }

  public void setPbjLibelle(String value) {
    takeStoredValueForKey(value, PBJ_LIBELLE_KEY);
  }

  public String pbjReglementCredit() {
    return (String) storedValueForKey(PBJ_REGLEMENT_CREDIT_KEY);
  }

  public void setPbjReglementCredit(String value) {
    takeStoredValueForKey(value, PBJ_REGLEMENT_CREDIT_KEY);
  }

  public String pbjReglementDebit() {
    return (String) storedValueForKey(PBJ_REGLEMENT_DEBIT_KEY);
  }

  public void setPbjReglementDebit(String value) {
    takeStoredValueForKey(value, PBJ_REGLEMENT_DEBIT_KEY);
  }

  public String pbjTemValide() {
    return (String) storedValueForKey(PBJ_TEM_VALIDE_KEY);
  }

  public void setPbjTemValide(String value) {
    takeStoredValueForKey(value, PBJ_TEM_VALIDE_KEY);
  }


/**
 * Créer une instance de EOPoemsBj avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPoemsBj createEOPoemsBj(EOEditingContext editingContext, String pbjKey
			) {
    EOPoemsBj eo = (EOPoemsBj) createAndInsertInstance(editingContext, _EOPoemsBj.ENTITY_NAME);    
		eo.setPbjKey(pbjKey);
    return eo;
  }

  
	  public EOPoemsBj localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPoemsBj)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPoemsBj creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPoemsBj creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPoemsBj object = (EOPoemsBj)createAndInsertInstance(editingContext, _EOPoemsBj.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPoemsBj localInstanceIn(EOEditingContext editingContext, EOPoemsBj eo) {
    EOPoemsBj localInstance = (eo == null) ? null : (EOPoemsBj)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPoemsBj#localInstanceIn a la place.
   */
	public static EOPoemsBj localInstanceOf(EOEditingContext editingContext, EOPoemsBj eo) {
		return EOPoemsBj.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPoemsBj fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPoemsBj fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPoemsBj eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPoemsBj)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPoemsBj fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPoemsBj fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPoemsBj eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPoemsBj)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPoemsBj fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPoemsBj eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPoemsBj ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPoemsBj fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
