// _EOPayeDisquette.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeDisquette.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeDisquette extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeDisquette";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_disquette";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "dskNumero";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_PRESENTATION_KEY = "dPresentation";
	public static final String DSK_CONTENU_KEY = "dskContenu";
	public static final String MONTANT_KEY = "montant";
	public static final String OPERATIONS_KEY = "operations";
	public static final String ORG_ETAB_KEY = "orgEtab";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "dsk_d_creation";
	public static final String D_PRESENTATION_COLKEY = "dsk_d_presentation";
	public static final String DSK_CONTENU_COLKEY = "dsk_contenu";
	public static final String MONTANT_COLKEY = "montant";
	public static final String OPERATIONS_COLKEY = "operations";
	public static final String ORG_ETAB_COLKEY = "org_etab";

// Relationships
	public static final String MOIS_KEY = "mois";
	public static final String SECTEUR_KEY = "secteur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dPresentation() {
    return (NSTimestamp) storedValueForKey(D_PRESENTATION_KEY);
  }

  public void setDPresentation(NSTimestamp value) {
    takeStoredValueForKey(value, D_PRESENTATION_KEY);
  }

  public String dskContenu() {
    return (String) storedValueForKey(DSK_CONTENU_KEY);
  }

  public void setDskContenu(String value) {
    takeStoredValueForKey(value, DSK_CONTENU_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_KEY);
  }

  public Integer operations() {
    return (Integer) storedValueForKey(OPERATIONS_KEY);
  }

  public void setOperations(Integer value) {
    takeStoredValueForKey(value, OPERATIONS_KEY);
  }

  public String orgEtab() {
    return (String) storedValueForKey(ORG_ETAB_KEY);
  }

  public void setOrgEtab(String value) {
    takeStoredValueForKey(value, ORG_ETAB_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur secteur() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur)storedValueForKey(SECTEUR_KEY);
  }

  public void setSecteurRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur oldValue = secteur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SECTEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SECTEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOPayeDisquette avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeDisquette createEOPayeDisquette(EOEditingContext editingContext, NSTimestamp dCreation
, java.math.BigDecimal montant
			) {
    EOPayeDisquette eo = (EOPayeDisquette) createAndInsertInstance(editingContext, _EOPayeDisquette.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setMontant(montant);
    return eo;
  }

  
	  public EOPayeDisquette localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeDisquette)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeDisquette creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeDisquette creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeDisquette object = (EOPayeDisquette)createAndInsertInstance(editingContext, _EOPayeDisquette.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeDisquette localInstanceIn(EOEditingContext editingContext, EOPayeDisquette eo) {
    EOPayeDisquette localInstance = (eo == null) ? null : (EOPayeDisquette)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeDisquette#localInstanceIn a la place.
   */
	public static EOPayeDisquette localInstanceOf(EOEditingContext editingContext, EOPayeDisquette eo) {
		return EOPayeDisquette.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeDisquette fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeDisquette fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeDisquette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeDisquette)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeDisquette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeDisquette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeDisquette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeDisquette)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeDisquette fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeDisquette eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeDisquette ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeDisquette fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
