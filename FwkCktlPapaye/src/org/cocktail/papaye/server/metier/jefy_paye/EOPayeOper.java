/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Fri Oct 17 16:46:59 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import java.util.Enumeration;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.metier.grhum.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOPayeOper extends _EOPayeOper {

    public EOPayeOper() {
        super();
    }


    // méthodes ajoutées
    public boolean estOuverte() {
        return temClose().equals(Constantes.FAUX);
    }
    public boolean estFermee() {
        return temClose().equals(Constantes.VRAI);
    }
    /** retourne true si toutes les EOPayePrepa ont ete generees
    pour cette operation de paye, tout secteur confondu */
    public boolean estPreparationPeriodeTerminee() {
        return estPreparationPeriodeTerminee(null);
    }
    /** retourne true si toutes les EOPayePrepa du secteur ont ete
    gen&eacuterees pour cette operation de paye
    @param secteur secteur pour lequel on recherche */
    public boolean estPreparationPeriodeTerminee(EOPayeSecteur secteur) {
        return EOPayePeriode.rechercherPeriodesCourantesNonTraitees(this.editingContext(),secteur).count() == 0;
    }
    /** initialise une operation de paye pour le mois courant.
    @param secteur secteur concerne. Peut &ecirc;tre nul
    @param datePayement date &agrave laquelle sera effectue le paiement. Peut &ecirc;tre nulle
    */
    public void initAvecSecteur(EOPayeSecteur secteur) {
        setTemClose(new String(Constantes.FAUX));
        // ajouter le mois courant
        addObjectToBothSidesOfRelationshipWithKey(EOPayeMois.moisCourant(editingContext()),"mois");
        // ajouter l'établissement
        try {
            addObjectToBothSidesOfRelationshipWithKey(
                                                      EOStructure.rechercherEtablissement(editingContext()),
                                                      "etablissement");
        } catch (Exception e) {}
        // ajouter le secteur
        addObjectToBothSidesOfRelationshipWithKey(secteur,"secteur");
    }
    /** ferme une operation de paie apr&egrave;s validation de la paye */
    public void fermer() {
        setTemClose(new String(Constantes.VRAI));
    }

    public String toString() {
        return new String(getClass().getName() +
                          "\nterminee : " + temClose() +
                          "\nmois : " + mois() +
                          "\netablissement : " + etablissement() +
                          "\nsecteur : "+ secteur());
    }

    // méthodes de classe
    /** methode de classe permettant de trouver toutes les operations de paye en cours
    dans l'etablissement.
    */
    public static NSArray rechercherOperationsCourantes(EOEditingContext editingContext) {
        // opération ouverte
        NSArray values = new NSArray(new String(Constantes.FAUX));
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("temClose = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification("PayeOper",qualifier, null);
        return editingContext.objectsWithFetchSpecification(fs);
    }
    /** methode de classe permettant de trouver l'operation de paye en cours pour un secteur
    @param editingContext editing context dans lequel effectuer le fetch
    @param secteur secteur concerne par cette operation (peut &ecirc;tre nul)
    */
    public static EOPayeOper rechercherOperationCourante(EOEditingContext editingContext,EOPayeSecteur secteur) {
        // opération ouverte
        NSMutableArray values = new NSMutableArray(new String(Constantes.FAUX));
        // pour le mois courant
        values.addObject(EOPayeMois.moisCourant(editingContext));
        EOQualifier qualifier;
        if (secteur != null) {
            values.addObject(secteur);
            qualifier = EOQualifier.qualifierWithQualifierFormat(
                                                                 "temClose = %@ AND mois = %@ AND secteur = %@",values);
        }
        else {
            qualifier = EOQualifier.qualifierWithQualifierFormat("temClose = %@ AND mois = %@",values);
        }
        EOFetchSpecification fs = new EOFetchSpecification("PayeOper",qualifier, null);
        NSArray opers = editingContext.objectsWithFetchSpecification(fs);
        try {
            return (EOPayeOper)opers.objectAtIndex(0);
        } catch (Exception e) {
            return null;
        }
    }
    /** methode de classe permettant de fermer toutes les operations de paye en cours
    dans l'etablissement.<BR>
    */
    public static void fermerOperationsCourantes(EOEditingContext editingContext) {
        NSArray operations = EOPayeOper.rechercherOperationsCourantes(editingContext);
        Enumeration e = operations.objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeOper oper = (EOPayeOper)e.nextElement();
            oper.setTemClose(new String(Constantes.VRAI));
        }
    }
    /** methode permettant de savoir le mois de la derniare operation de paye
    terminee
    @return moisCode code mois du dernier mois de Paie
    */
    public static Number dernierMoisPaie(EOEditingContext editingContext) {
        // opération fermée
        NSArray values = new NSArray(new String(Constantes.VRAI));
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("temClose = %@",values);
        EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("mois.moisCode",
                                                                 EOSortOrdering.CompareDescending);

        EOFetchSpecification fs = new EOFetchSpecification("PayeOper",qualifier, new NSArray(sort));
        NSArray opers = editingContext.objectsWithFetchSpecification(fs);
        try {
            return ((EOPayeOper)opers.objectAtIndex(0)).mois().moisCode();
        } catch (Exception e) {
            return null;
        }
    }
    
	/** Boolean indiquant si une operation de paye est en cours ou non */
	public static boolean operationEnCours(EOEditingContext ec, EOPayeMois mois, EOPayeSecteur secteur) {
		EOQualifier qual = null;
		NSMutableArray args = new NSMutableArray();			
		
		args.addObject(mois);
		
		if (secteur == null)
			qual = EOQualifier.qualifierWithQualifierFormat("mois = %@ and temClose = 'N'",args);
		else {
			args.addObject(secteur);
			qual = EOQualifier.qualifierWithQualifierFormat("mois = %@ and secteur = %@ and temClose = 'N'",args);
		}
		EOFetchSpecification fs = new EOFetchSpecification("PayeOper",qual,null);
		if (ec.objectsWithFetchSpecification(fs).count() > 0)
			return true;
		else
			return false;
	}
}
