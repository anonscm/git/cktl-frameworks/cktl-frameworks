/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Thu May 13 10:01:19 Europe/Paris 2004 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public class EOPayeElementLbud extends _EOPayeElementLbud {

    public EOPayeElementLbud() {
        super();
    }

    public static NSArray lbudsForElement(EOEditingContext editingContext,EOPayeElement element) {
        
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOPayeElementLbud.PAYE_ELEMENT_KEY + " = %@", new NSArray(element));

    	return fetchAll(editingContext, qualifier);
    	
    }

	// méthodes ajoutées
	/** initialisation d'une ligne budgetaire pour un element a partir des informations saisies dans une ligne budgetaire associee
	a une rubrique personnelle */
	public void initAvecPersoLbud(_EOPayePersoLbud lbud) {
		setPelQuotite(lbud.pbudQuotite());
		if (lbud.codeAnalytique() != null)
			addObjectToBothSidesOfRelationshipWithKey(lbud.codeAnalytique(),"codeAnalytique");
		if (lbud.convention() != null)
			addObjectToBothSidesOfRelationshipWithKey(lbud.convention(),"convention");
		if (lbud.exercice() != null)
			addObjectToBothSidesOfRelationshipWithKey(lbud.exercice(),"exercice");
		if (lbud.organ() != null)
			addObjectToBothSidesOfRelationshipWithKey(lbud.organ(),"organ");
		if (lbud.typeAction() != null)
			addObjectToBothSidesOfRelationshipWithKey(lbud.typeAction(),"typeAction");
		if (lbud.typeCredit() != null)
			addObjectToBothSidesOfRelationshipWithKey(lbud.typeCredit(),"typeCredit");
	}
}
