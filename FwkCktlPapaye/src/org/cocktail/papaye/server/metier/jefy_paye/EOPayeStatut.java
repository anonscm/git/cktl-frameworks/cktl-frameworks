/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Fri Mar 14 09:17:02 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import java.util.Enumeration;

import org.cocktail.papaye.server.common.Constantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOPayeStatut extends _EOPayeStatut {
	public static final String CATEGORIE_TEMPS_PLEIN = "1";
	public static final String CATEGORIE_VACATAIRE = "7";
	public static final String CATEGORIE_OCCASIONNEL = "8";
	
    public EOPayeStatut() {
        super();
    }

    public boolean estTitulaire() {
    	
    	return "O".equals(temTitulaire());
    	
    }
    
    public String codeEmploi() {
    	
    	if (pstaCodemploi().equals("9999") || pstaCodemploi().equals("999"))
    		return "99999";
    	
    	return pstaCodemploi(); 
    	
    }
    
    // méthodes ajoutées
	public EORegimeCotisation regimeCotisation() {
		try {
			return (EORegimeCotisation)regimeCotisations().objectAtIndex(0);
		} catch (Exception e) {
		//	e.printStackTrace();
			return null;
		}
    }
	/** Retourne le complement PCS de la categorie socio-professionnelle. Pas gere dans Papaye
	 * car les codes des categories socio-professionnelles n'ont pas de PCS */
	public String complementPCS() {
		return null;
	}
    public String toString() {
        return new String(getClass().getName() +
                          "\nabrege : " + pstaAbrege() +
                          "\nlibelle : " + pstaLibelle() +
                          "\ndebut validite : " + pstaMdebut() +
                          "\nfin validite : " + pstaMfin() +
                          "\ncipdz : " + pstaCipdz() +
                          "\ncode emploi : " + pstaCodemploi());
    }
    /** Retourne toutes les rubriques valides associees a ce statut
    triees par code croissant.
    */
    public NSArray rubriques() throws Exception {
        NSMutableArray rubriques = new NSMutableArray();
        // commencer par identifier les profils actifs pour le statut en faisant un fetch direct
        // statut
        NSMutableArray values = new NSMutableArray(this);
        // profils valides
        values.addObject(Constantes.VRAI);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
            ( "statut = %@ AND temValide = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification("PayeProfil",qualifier, null);
        // on prefetechera les rubriques
        fs.setPrefetchingRelationshipKeyPaths(new NSArray("rubrique"));
        NSArray profils = editingContext().objectsWithFetchSpecification(fs);
        try {
            Enumeration e = profils.objectEnumerator();
            while (e.hasMoreElements()) {
                // pour chaque profil ajouter sa rubrique au tableau
                EOPayeRubrique rubrique = ((EOPayeProfil)e.nextElement()).rubrique();
                if (rubrique.temValide().equals(Constantes.VRAI)) {
                    rubriques.addObject(rubrique);
                }
            }
            // trier les rubriques sur le code de rubrique
            EOSortOrdering.sortArrayUsingKeyOrderArray(rubriques,new NSArray(EOSortOrdering.sortOrderingWithKey("prubClassement",EOSortOrdering.CompareAscending)));
            return rubriques;
        } catch (Exception e) { throw e; }
    }
    /** Retourne toutes les rubriques valides qui font partie des cotisations salariales de ce statut
    triees par code croissant.
    */
    public NSArray rubriquesCotisationSalariale() throws Exception {
        NSMutableArray rubriquesCotisations = new NSMutableArray();
        Enumeration e = rubriques().objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeRubrique rubrique = (EOPayeRubrique)e.nextElement();
            if (rubrique.estADeduire()) {
                rubriquesCotisations.addObject(rubrique);
            }
        }
        return rubriquesCotisations;
    }
    /** Retourne un dictionnaire comportant le taux de charges pour un statut
     * @param montant du taux transport
     * @param taux accident du travail
     * @param doit-on prendre en compte la rafp
     <UL>cle : salarial, valeur : Number taux de charges salariales >/UL>
     <UL>cle : patronal, valeur : Number taux de charges patronales >/UL>*/
    public NSDictionary tauxCharges (Number tauxTransport,Number tauxAT,boolean prendreEnCompteRAFP) {
    		double tauxTotalPatronal = 0;
    		double tauxTotalSalarial = 0;
    		// pour ne pas prendre deux fois en compte un taux car il est associé à plusieurs rubriques
    		NSMutableDictionary codesPourTaux = new NSMutableDictionary();
    		try {
    			NSArray rubriquesTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(rubriques(),new NSArray(EOSortOrdering.sortOrderingWithKey("prubMode",EOSortOrdering.CompareAscending)));
			Enumeration e =  rubriquesTriees.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPayeRubrique rubrique = (EOPayeRubrique)e.nextElement();
				
				if (rubrique.estPartPatronale() || rubrique.estADeduire()) {
						double tauxPourRubrique = 0;
						Enumeration e1 = rubrique.codes().objectEnumerator();
						while (e1.hasMoreElements()) {
							EOPayeCode code = (EOPayeCode)e1.nextElement();
							if (rubrique.estPartPatronale()) {
								//System.out.println("code " + code.pcodCode() + " " + (code.pcodCode().startsWith("TX") || code.pcodCode().equals(CalculTaxeSurSalaire.TAUX1)));
								// prendre en compte la cotisation transport
								if (code.pcodCode().equals("COTTRANS") && codesPourTaux.objectForKey(code.pcodCode()) == null) {
									tauxPourRubrique = tauxTransport.doubleValue();
									codesPourTaux.setObjectForKey(code,code.pcodCode());
									break;
								}
								// prendre en compte la cotisation Accident du Travail
								if (code.pcodCode().equals("COTACTRA") && codesPourTaux.objectForKey(code.pcodCode()) == null) {
									tauxPourRubrique = tauxAT.doubleValue();
									codesPourTaux.setObjectForKey(code,code.pcodCode());
									break;
								}	
								//  ne prendre en compte que les taux et ne pas prendre en compte les paramètres qui ne correspondent pas à un taux
								if (code.estCodePourTauxChargePatronale() &&
									(code.pcodCode().indexOf("2") < 0 || code.pcodCode().endsWith("P") == false || 
									 pstaLibelle().indexOf("Tr. B") > 0)) {	// tranche 2 d'une cotisation plafonnée sauf pour les vacataires payant uniquement l'ircantec tranche B
									if (codesPourTaux.objectForKey(code.pcodCode()) == null && (code.pcodCode().indexOf("RAFP") < 0 || prendreEnCompteRAFP)) {
										// taux non déjà ajouté
										EOPayeParam parametre = EOPayeParam.parametreValide(code);
										tauxPourRubrique = parametre.pparTaux().doubleValue();
										codesPourTaux.setObjectForKey(code,code.pcodCode());
									}
									break;
								
								}
							} else {
								//System.out.println("code " + code.pcodCode() + " " + code.pcodCode().startsWith("TX"));
								// ne prendre en compte que les taux et ne pas prendre en compte
								if (((code.pcodCode().startsWith("TX"))) &&
									(code.pcodCode().indexOf("2") < 0 || code.pcodCode().endsWith("S") == false)) {	// tranche 2 d'une cotisation plafonnée
									if (codesPourTaux.objectForKey(code.pcodCode()) == null && (code.pcodCode().indexOf("RAFP") < 0 || prendreEnCompteRAFP)) {
										// taux non déjà ajouté
										EOPayeParam parametre = EOPayeParam.parametreValide(code);
										tauxPourRubrique = parametre.pparTaux().doubleValue();
										codesPourTaux.setObjectForKey(code,code.pcodCode());
									}
									break;
								}
									
							}
						}
						if (rubrique.estPartPatronale()) {
							tauxTotalPatronal += tauxPourRubrique;
						} else {
							tauxTotalSalarial += tauxPourRubrique;
						}
				}
			}
    		} catch (Exception e) {
			e.printStackTrace();
		}
    		NSMutableDictionary resultat = new NSMutableDictionary();
    		resultat.setObjectForKey(new Double(tauxTotalPatronal),"patronal");
    		resultat.setObjectForKey(new Double(tauxTotalSalarial),"salarial");
    		return resultat;
    }
    // méthodes de recherche : méthodes de classe
    /** methode de classe permettant de trouver tous les statuts valides<BR>
    Elle retourne un tableau contenant les statuts (EOPayeStatut), trie
    par ordre alphabetique.
    @param editingContext editingContext dans lequel fetcher les objets
    @return statuts statuts trouves classes par ordre alphabetique
    */
    public static NSArray rechercherStatuts(EOEditingContext editingContext) {
        // statuts valides
        NSArray values = new NSArray(Constantes.VRAI);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("temValide = %@",values);
        EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("pstaLibelle",
                                                                 EOSortOrdering.CompareAscending);
        EOFetchSpecification fs = new EOFetchSpecification("PayeStatut",qualifier, new NSArray(sort));
        fs.setPrefetchingRelationshipKeyPaths(new NSArray("regimeCotisations"));
        return editingContext.objectsWithFetchSpecification(fs);
    }
   
    /** retourne une string comportant les libelles des statuts et les libelle des rubriques de chaque statut */
    public static String rubriquesPourStatuts(EOEditingContext editingContext) {
    		NSArray statuts = rechercherStatuts(editingContext);
    		Enumeration e = statuts.objectEnumerator();
    		String result = "";
    		while (e.hasMoreElements()) {
    			EOPayeStatut statut = (EOPayeStatut)e.nextElement();
    			result = result + statut.pstaLibelle() + "\n";
    			try {
    				Enumeration e1 = statut.rubriques().objectEnumerator();
    				while (e1.hasMoreElements()) {
    					EOPayeRubrique rubrique = (EOPayeRubrique)e1.nextElement();
    					result = result + "\t" + rubrique.prubLibelle() + "\n";
    				}
    			} catch (Exception e2) {}	// pas de rubrique définie pour un statut
    		}
    		return result;
    }
}
