// _EOPayeParametresDads.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeParametresDads.java instead.
package org.cocktail.papaye.server.metier.jefy_paye.dads;

import java.util.NoSuchElementException;

import org.cocktail.application.serveur.eof.EOUtilisateur;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public abstract class _EOPayeParametresDads extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PayeParametresDads";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_parametres_dads";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "paramOrdre";

	public static final String CRITERE_TRI_POINT1_KEY = "critereTriPoint1";
	public static final String CRITERE_TRI_POINT2_KEY = "critereTriPoint2";
	public static final String DOSSIER_STOCKAGE_KEY = "dossierStockage";
	public static final String NOM_EDITEUR_KEY = "nomEditeur";
	public static final String NOM_LOGICIEL_KEY = "nomLogiciel";
	public static final String NUMERO_ENVOI_KEY = "numeroEnvoi";
	public static final String NUMERO_VERSION_KEY = "numeroVersion";
	public static final String TYPE_ENVOI_KEY = "typeEnvoi";
	public static final String TYPE_ENVOI_POINT_RETRAITE_KEY = "typeEnvoiPointRetraite";

// Attributs non visibles
	public static final String PARAM_ORDRE_KEY = "paramOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String CRITERE_TRI_POINT1_COLKEY = "CRITERE_TRI_POINT_1";
	public static final String CRITERE_TRI_POINT2_COLKEY = "CRITERE_TRI_POINT_2";
	public static final String DOSSIER_STOCKAGE_COLKEY = "DOSSIER_STOCKAGE";
	public static final String NOM_EDITEUR_COLKEY = "NOM_EDITEUR";
	public static final String NOM_LOGICIEL_COLKEY = "NOM_LOGICIEL";
	public static final String NUMERO_ENVOI_COLKEY = "NUMERO_ENVOI";
	public static final String NUMERO_VERSION_COLKEY = "NUMERO_VERSION";
	public static final String TYPE_ENVOI_COLKEY = "TYPE_ENVOI";
	public static final String TYPE_ENVOI_POINT_RETRAITE_COLKEY = "TYPE_ENVOI_POINT_RETRAITE";

	public static final String PARAM_ORDRE_COLKEY = "PARAM_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_Ordre";


	// Relationships
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String critereTriPoint1() {
    return (String) storedValueForKey(CRITERE_TRI_POINT1_KEY);
  }

  public void setCritereTriPoint1(String value) {
    takeStoredValueForKey(value, CRITERE_TRI_POINT1_KEY);
  }

  public String critereTriPoint2() {
    return (String) storedValueForKey(CRITERE_TRI_POINT2_KEY);
  }

  public void setCritereTriPoint2(String value) {
    takeStoredValueForKey(value, CRITERE_TRI_POINT2_KEY);
  }

  public String dossierStockage() {
    return (String) storedValueForKey(DOSSIER_STOCKAGE_KEY);
  }

  public void setDossierStockage(String value) {
    takeStoredValueForKey(value, DOSSIER_STOCKAGE_KEY);
  }

  public String nomEditeur() {
    return (String) storedValueForKey(NOM_EDITEUR_KEY);
  }

  public void setNomEditeur(String value) {
    takeStoredValueForKey(value, NOM_EDITEUR_KEY);
  }

  public String nomLogiciel() {
    return (String) storedValueForKey(NOM_LOGICIEL_KEY);
  }

  public void setNomLogiciel(String value) {
    takeStoredValueForKey(value, NOM_LOGICIEL_KEY);
  }

  public String numeroEnvoi() {
    return (String) storedValueForKey(NUMERO_ENVOI_KEY);
  }

  public void setNumeroEnvoi(String value) {
    takeStoredValueForKey(value, NUMERO_ENVOI_KEY);
  }

  public String numeroVersion() {
    return (String) storedValueForKey(NUMERO_VERSION_KEY);
  }

  public void setNumeroVersion(String value) {
    takeStoredValueForKey(value, NUMERO_VERSION_KEY);
  }

  public String typeEnvoi() {
    return (String) storedValueForKey(TYPE_ENVOI_KEY);
  }

  public void setTypeEnvoi(String value) {
    takeStoredValueForKey(value, TYPE_ENVOI_KEY);
  }

  public String typeEnvoiPointRetraite() {
    return (String) storedValueForKey(TYPE_ENVOI_POINT_RETRAITE_KEY);
  }

  public void setTypeEnvoiPointRetraite(String value) {
    takeStoredValueForKey(value, TYPE_ENVOI_POINT_RETRAITE_KEY);
  }

  public EOUtilisateur utilisateur() {
    return (EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(EOUtilisateur value) {
    if (value == null) {
    	EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOPayeParametresDads avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeParametresDads createEOPayeParametresDads(EOEditingContext editingContext, String critereTriPoint1
, String numeroEnvoi
, String typeEnvoi
, String typeEnvoiPointRetraite
			) {
    EOPayeParametresDads eo = (EOPayeParametresDads) createAndInsertInstance(editingContext, _EOPayeParametresDads.ENTITY_NAME);    
		eo.setCritereTriPoint1(critereTriPoint1);
		eo.setNumeroEnvoi(numeroEnvoi);
		eo.setTypeEnvoi(typeEnvoi);
		eo.setTypeEnvoiPointRetraite(typeEnvoiPointRetraite);
    return eo;
  }

  
	  public EOPayeParametresDads localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeParametresDads)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeParametresDads creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeParametresDads creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeParametresDads object = (EOPayeParametresDads)createAndInsertInstance(editingContext, _EOPayeParametresDads.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeParametresDads localInstanceIn(EOEditingContext editingContext, EOPayeParametresDads eo) {
    EOPayeParametresDads localInstance = (eo == null) ? null : (EOPayeParametresDads)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeParametresDads#localInstanceIn a la place.
   */
	public static EOPayeParametresDads localInstanceOf(EOEditingContext editingContext, EOPayeParametresDads eo) {
		return EOPayeParametresDads.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeParametresDads fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeParametresDads fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeParametresDads eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeParametresDads)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeParametresDads fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeParametresDads fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeParametresDads eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeParametresDads)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeParametresDads fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeParametresDads eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeParametresDads ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeParametresDads fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
