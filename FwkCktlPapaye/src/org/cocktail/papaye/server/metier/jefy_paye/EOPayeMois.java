/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Fri Mar 14 11:03:06 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import org.cocktail.papaye.server.common.Constantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOPayeMois extends _EOPayeMois {
    private static EOPayeMois moisCourant = null;
    /** code associe au mois generique pour les CDIs */
    public final static int MOIS_GENERIQUE = 999999;
    public EOPayeMois() {
        super();
    }

	public static NSArray findForExercice(EOEditingContext ec, int annee)		{

		try {
			
			NSArray mySort = new NSArray(new EOSortOrdering(EOPayeMois.MOIS_CODE_KEY, EOSortOrdering.CompareAscending));
			
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOPayeMois.MOIS_ANNEE_KEY + " = %@", new NSArray(new Integer(annee)));
			
			return fetchAll(ec, myQualifier, mySort);

		}
		catch (Exception e)	{return new NSArray();}
	}

    
    // méthodes ajoutées
    /** retourne le mois precedent en le fetchant dans la base. <BR>
    */
    public EOPayeMois moisPrecedent(EOEditingContext editingContext) {
        int moisCodePrecedent = moisCode().intValue() - 1;
        if ((moisCodePrecedent % 100) == 0) {
            // on est en janvier
			moisCodePrecedent = (((moisCode().intValue() / 100) - 1) * 100) + 12;
		}
		NSMutableArray values = new NSMutableArray(new Integer(moisCodePrecedent));
		// mois valide
		values.addObject(Constantes.VRAI);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("moisCode = %@ AND temValide = %@ ",values);
		EOFetchSpecification fs = new EOFetchSpecification("PayeMois",qualifier,null);
		try {
			return (EOPayeMois)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) { return null; }
	}
    /** retourne le numero du mois courant (entre 1 et 12) */
    public int numeroDuMois() {
        return (moisCode().intValue() - (moisAnnee().intValue() * 100));
    }
    // méthodes de recherche : méthodes de classe
    /** methode de classe permettant de trouver le mois courant.
    */
    public static EOPayeMois moisCourant(EOEditingContext editingContext) {
        if (EOPayeMois.moisCourant() == null) {
     /*       GregorianCalendar calendar = new GregorianCalendar();
            // Warning : avec Java Janvier = 0 => ajouter 1
            int moisCode = (calendar.get(Calendar.YEAR) * 100) + calendar.get(Calendar.MONTH) + 1;
            // code du mois
            NSMutableArray values = new NSMutableArray(new Integer(moisCode));
            // mois valide
            values.addObject(Constantes.VRAI);
            EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
                ("moisCode = %@ AND temValide = %@ ",values);
            EOFetchSpecification fs = new EOFetchSpecification("PayeMois",qualifier,null);
            try {
                EOPayeMois mois = (EOPayeMois)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
                EOPayeMois.setMoisCourant(mois);*/
        		NSArray operationsPaye = EOPayeOper.rechercherOperationsCourantes(editingContext);
        		try {
                EOPayeMois mois = ((EOPayeOper)operationsPaye.objectAtIndex(0)).mois();
                EOPayeMois.setMoisCourant(mois);
                return mois;
            } catch (Exception e) { return null; }
        } else {
            return moisCourant();
        }
    }
    public static EOPayeMois moisCourant() {
        return moisCourant;
    }
    public static void setMoisCourant(EOPayeMois mois) {
        moisCourant = mois;
    }
    /** methode de classe permettant de trouver un mois en fonction de son code
    */
    public static EOPayeMois moisAvecCode(EOEditingContext editingContext,Number moisCode) {
        NSMutableArray values = new NSMutableArray(moisCode);
        // mois valide
        values.addObject(Constantes.VRAI);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
            ("moisCode = %@ AND temValide = %@ ",values);
        EOFetchSpecification fs = new EOFetchSpecification("PayeMois",qualifier,null);
        try {
            EOPayeMois mois = (EOPayeMois)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
            return mois;
            } catch (Exception e) { return null; }
    }
	public static NSArray moisComprisEntre(EOEditingContext editingContext,Number moisDebut,Number moisFin) {
        NSMutableArray values = new NSMutableArray(moisDebut);
		values.addObject(moisFin);
        // mois valide
        values.addObject(Constantes.VRAI);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
            ("moisCode >= %@ AND moisCode <= %@ AND temValide = %@ ",values);
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("moisCode",EOSortOrdering.CompareAscending);
        EOFetchSpecification fs = new EOFetchSpecification("PayeMois",qualifier,new NSArray(sort));
            return editingContext.objectsWithFetchSpecification(fs);
    }
	
	/** Renvoie le mois suivant le mois passe en parametres */
	public static EOPayeMois moisSuivant(EOEditingContext ec, EOPayeMois mois)
	{
		int codeMoisSuivant = mois.moisCode().intValue();
		if ("12".equals((mois.moisCode().toString()).substring(4,6)))		// On teste les deux derniers chiffres du code pour savoir si on est en fin d'annee
			codeMoisSuivant += 89;		// Changement d'annee
		else
			codeMoisSuivant ++;
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("moisCode = " + codeMoisSuivant,null);
		EOFetchSpecification	fs = new EOFetchSpecification("PayeMois",myQualifier, null);
		try {return (EOPayeMois)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e) {return null;}			
	}
	
	
	public static EOPayeMois rechercherMoisGenerique(EOEditingContext ec)
	{
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("moisCode = " + MOIS_GENERIQUE,null);
		EOFetchSpecification	fs = new EOFetchSpecification("PayeMois",myQualifier, null);
		try {return (EOPayeMois)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e) {return null;}		
	}
    public String toString() {
        return new String(getClass().getName() +
                          "\nannee : " + moisAnnee() +
                          "\ncode : "+ moisCode() +
                          "\nnom : " + moisLibelle() +
                          "\nnom complet : " + moisComplet() +
                          "\ndebut mois : "+ moisDebut() +
                          "\nfin mois : "+ moisFin() +
                          "\ndebut mois : "+ moisDebut());
    }
    
}
