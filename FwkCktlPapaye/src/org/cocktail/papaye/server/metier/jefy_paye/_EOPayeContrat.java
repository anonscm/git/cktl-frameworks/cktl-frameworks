// _EOPayeContrat.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeContrat.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeContrat extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeContrat";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.PAYE_CONTRAT";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pctrOrdre";

	public static final String ABATTEMENT_KEY = "abattement";
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String CLASSIFICATION_EMPLOI_KEY = "classificationEmploi";
	public static final String CODE_STATUT_CATEGORIEL_KEY = "codeStatutCategoriel";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_CONTRAT_TRAV_KEY = "dDebContratTrav";
	public static final String D_FIN_CONTRAT_TRAV_KEY = "dFinContratTrav";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INDICE_CONTRAT_KEY = "indiceContrat";
	public static final String INDICE_ORIGINE_KEY = "indiceOrigine";
	public static final String INDIVIDU_NOM_USUEL_KEY = "individu_nomUsuel";
	public static final String INDIVIDU_PRENOM_KEY = "individu_prenom";
	public static final String MODE_CALCUL_KEY = "modeCalcul";
	public static final String MONTANT_FORFAITAIRE_KEY = "montantForfaitaire";
	public static final String MONTANT_MENSUEL_REMU_KEY = "montantMensuelRemu";
	public static final String MONTANT_PLAFOND_SECU_KEY = "montantPlafondSecu";
	public static final String NB_HEURES_CONTRAT_KEY = "nbHeuresContrat";
	public static final String NB_JOURS_CONTRAT_KEY = "nbJoursContrat";
	public static final String NO_CONTRAT_PRECEDENT_KEY = "noContratPrecedent";
	public static final String NUM_QUOT_RECRUTEMENT_KEY = "numQuotRecrutement";
	public static final String POURCENT_SMIC_KEY = "pourcentSmic";
	public static final String REFERENCE_CONTRAT_KEY = "referenceContrat";
	public static final String RISQUE_ACC_TRAV_KEY = "risqueAccTrav";
	public static final String TAUX_ACC_TRAV_KEY = "tauxAccTrav";
	public static final String TAUX_HORAIRE_CONTRAT_KEY = "tauxHoraireContrat";
	public static final String TAUX_PLAFOND_SECU_KEY = "tauxPlafondSecu";
	public static final String TEM_ANNULATION_KEY = "temAnnulation";
	public static final String TEM_COTISE_SOLIDARITE_KEY = "temCotiseSolidarite";
	public static final String TEM_PAIEMENT_PONCTUEL_KEY = "temPaiementPonctuel";
	public static final String TEM_PAYE_LOCALE_KEY = "temPayeLocale";
	public static final String TEM_PAYE_UTILE_KEY = "temPayeUtile";
	public static final String TEM_PLAFOND_REDUIT_KEY = "temPlafondReduit";
	public static final String TEM_PRE_CONTRAT_KEY = "temPreContrat";
	public static final String TEM_TEMPS_PLEIN_KEY = "temTempsPlein";

//Colonnes dans la base de donnees
	public static final String ABATTEMENT_COLKEY = "TAUX_ABATTEMENT";
	public static final String C_ECHELON_COLKEY = "C_ECHELON";
	public static final String CLASSIFICATION_EMPLOI_COLKEY = "CLASSIFICATION_EMPLOI";
	public static final String CODE_STATUT_CATEGORIEL_COLKEY = "CODE_STATUT_CATEGORIEL";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_CONTRAT_TRAV_COLKEY = "D_DEB_CONTRAT_TRAV";
	public static final String D_FIN_CONTRAT_TRAV_COLKEY = "D_FIN_CONTRAT_TRAV";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String INDICE_CONTRAT_COLKEY = "PCTR_INDICE";
	public static final String INDICE_ORIGINE_COLKEY = "PCTR_INDICE_ORIGINE";
	public static final String INDIVIDU_NOM_USUEL_COLKEY = "$attribute.columnName";
	public static final String INDIVIDU_PRENOM_COLKEY = "$attribute.columnName";
	public static final String MODE_CALCUL_COLKEY = "MODE_CALCUL";
	public static final String MONTANT_FORFAITAIRE_COLKEY = "MONTANT_FORFAITAIRE";
	public static final String MONTANT_MENSUEL_REMU_COLKEY = "MONTANT_MENSUEL_REMU";
	public static final String MONTANT_PLAFOND_SECU_COLKEY = "MONTANT_PLAFOND_SECU";
	public static final String NB_HEURES_CONTRAT_COLKEY = "PCTR_HEURES";
	public static final String NB_JOURS_CONTRAT_COLKEY = "PCTR_JOURS";
	public static final String NO_CONTRAT_PRECEDENT_COLKEY = "NO_CONTRAT_PRECEDENT";
	public static final String NUM_QUOT_RECRUTEMENT_COLKEY = "PCTR_QUOTITE";
	public static final String POURCENT_SMIC_COLKEY = "POURCENT_SMIC";
	public static final String REFERENCE_CONTRAT_COLKEY = "REFERENCE_CONTRAT";
	public static final String RISQUE_ACC_TRAV_COLKEY = "RISQUE_ACC_TRAV";
	public static final String TAUX_ACC_TRAV_COLKEY = "TAUX_ACC_TRAV";
	public static final String TAUX_HORAIRE_CONTRAT_COLKEY = "PCTR_TAUX_HORAIRE";
	public static final String TAUX_PLAFOND_SECU_COLKEY = "TAUX_PLAFOND_SECU";
	public static final String TEM_ANNULATION_COLKEY = "TEM_ANNULATION";
	public static final String TEM_COTISE_SOLIDARITE_COLKEY = "TEM_COTISE_SOLIDARITE";
	public static final String TEM_PAIEMENT_PONCTUEL_COLKEY = "TEM_PAIEMENT_PONCTUEL";
	public static final String TEM_PAYE_LOCALE_COLKEY = "TEM_PAYE_LOCALE";
	public static final String TEM_PAYE_UTILE_COLKEY = "TEM_PAYE_UTILE";
	public static final String TEM_PLAFOND_REDUIT_COLKEY = "TEM_PLAFOND_REDUIT";
	public static final String TEM_PRE_CONTRAT_COLKEY = "TEM_PRE_CONTRAT";
	public static final String TEM_TEMPS_PLEIN_COLKEY = "TEM_TEMPS_PLEIN";

// Relationships
	public static final String CONTRAT_LBUDS_KEY = "contratLbuds";
	public static final String CONTRAT_PRECEDENT_KEY = "contratPrecedent";
	public static final String FONCTION_KEY = "fonction";
	public static final String GRADE_KEY = "grade";
	public static final String HISTORIQUES_KEY = "historiques";
	public static final String INDIVIDU_KEY = "individu";
	public static final String MOTIF_DEBUT_KEY = "motifDebut";
	public static final String MOTIF_FIN_KEY = "motifFin";
	public static final String PERIODES_KEY = "periodes";
	public static final String PERSONNALISATIONS_KEY = "personnalisations";
	public static final String PREPARATIONS_KEY = "preparations";
	public static final String SECTEUR_KEY = "secteur";
	public static final String STATUT_KEY = "statut";
	public static final String STRUCTURE_KEY = "structure";
	public static final String STRUCTURE_SIRET_KEY = "structureSiret";
	public static final String TYPE_CONTRAT_TRAVAIL_KEY = "typeContratTravail";
	public static final String VALIDATIONS_KEY = "validations";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Double abattement() {
    return (Double) storedValueForKey(ABATTEMENT_KEY);
  }

  public void setAbattement(Double value) {
    takeStoredValueForKey(value, ABATTEMENT_KEY);
  }

  public String cEchelon() {
    return (String) storedValueForKey(C_ECHELON_KEY);
  }

  public void setCEchelon(String value) {
    takeStoredValueForKey(value, C_ECHELON_KEY);
  }

  public String classificationEmploi() {
    return (String) storedValueForKey(CLASSIFICATION_EMPLOI_KEY);
  }

  public void setClassificationEmploi(String value) {
    takeStoredValueForKey(value, CLASSIFICATION_EMPLOI_KEY);
  }

  public String codeStatutCategoriel() {
    return (String) storedValueForKey(CODE_STATUT_CATEGORIEL_KEY);
  }

  public void setCodeStatutCategoriel(String value) {
    takeStoredValueForKey(value, CODE_STATUT_CATEGORIEL_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDebContratTrav() {
    return (NSTimestamp) storedValueForKey(D_DEB_CONTRAT_TRAV_KEY);
  }

  public void setDDebContratTrav(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEB_CONTRAT_TRAV_KEY);
  }

  public NSTimestamp dFinContratTrav() {
    return (NSTimestamp) storedValueForKey(D_FIN_CONTRAT_TRAV_KEY);
  }

  public void setDFinContratTrav(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_CONTRAT_TRAV_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String indiceContrat() {
    return (String) storedValueForKey(INDICE_CONTRAT_KEY);
  }

  public void setIndiceContrat(String value) {
    takeStoredValueForKey(value, INDICE_CONTRAT_KEY);
  }

  public String indiceOrigine() {
    return (String) storedValueForKey(INDICE_ORIGINE_KEY);
  }

  public void setIndiceOrigine(String value) {
    takeStoredValueForKey(value, INDICE_ORIGINE_KEY);
  }

  public String individu_nomUsuel() {
    return (String) storedValueForKey(INDIVIDU_NOM_USUEL_KEY);
  }

  public void setIndividu_nomUsuel(String value) {
    takeStoredValueForKey(value, INDIVIDU_NOM_USUEL_KEY);
  }

  public String individu_prenom() {
    return (String) storedValueForKey(INDIVIDU_PRENOM_KEY);
  }

  public void setIndividu_prenom(String value) {
    takeStoredValueForKey(value, INDIVIDU_PRENOM_KEY);
  }

  public String modeCalcul() {
    return (String) storedValueForKey(MODE_CALCUL_KEY);
  }

  public void setModeCalcul(String value) {
    takeStoredValueForKey(value, MODE_CALCUL_KEY);
  }

  public java.math.BigDecimal montantForfaitaire() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_FORFAITAIRE_KEY);
  }

  public void setMontantForfaitaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_FORFAITAIRE_KEY);
  }

  public java.math.BigDecimal montantMensuelRemu() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_MENSUEL_REMU_KEY);
  }

  public void setMontantMensuelRemu(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_MENSUEL_REMU_KEY);
  }

  public java.math.BigDecimal montantPlafondSecu() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_PLAFOND_SECU_KEY);
  }

  public void setMontantPlafondSecu(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_PLAFOND_SECU_KEY);
  }

  public java.math.BigDecimal nbHeuresContrat() {
    return (java.math.BigDecimal) storedValueForKey(NB_HEURES_CONTRAT_KEY);
  }

  public void setNbHeuresContrat(java.math.BigDecimal value) {
    takeStoredValueForKey(value, NB_HEURES_CONTRAT_KEY);
  }

  public java.math.BigDecimal nbJoursContrat() {
    return (java.math.BigDecimal) storedValueForKey(NB_JOURS_CONTRAT_KEY);
  }

  public void setNbJoursContrat(java.math.BigDecimal value) {
    takeStoredValueForKey(value, NB_JOURS_CONTRAT_KEY);
  }

  public Integer noContratPrecedent() {
    return (Integer) storedValueForKey(NO_CONTRAT_PRECEDENT_KEY);
  }

  public void setNoContratPrecedent(Integer value) {
    takeStoredValueForKey(value, NO_CONTRAT_PRECEDENT_KEY);
  }

  public java.math.BigDecimal numQuotRecrutement() {
    return (java.math.BigDecimal) storedValueForKey(NUM_QUOT_RECRUTEMENT_KEY);
  }

  public void setNumQuotRecrutement(java.math.BigDecimal value) {
    takeStoredValueForKey(value, NUM_QUOT_RECRUTEMENT_KEY);
  }

  public Double pourcentSmic() {
    return (Double) storedValueForKey(POURCENT_SMIC_KEY);
  }

  public void setPourcentSmic(Double value) {
    takeStoredValueForKey(value, POURCENT_SMIC_KEY);
  }

  public String referenceContrat() {
    return (String) storedValueForKey(REFERENCE_CONTRAT_KEY);
  }

  public void setReferenceContrat(String value) {
    takeStoredValueForKey(value, REFERENCE_CONTRAT_KEY);
  }

  public String risqueAccTrav() {
    return (String) storedValueForKey(RISQUE_ACC_TRAV_KEY);
  }

  public void setRisqueAccTrav(String value) {
    takeStoredValueForKey(value, RISQUE_ACC_TRAV_KEY);
  }

  public String tauxAccTrav() {
    return (String) storedValueForKey(TAUX_ACC_TRAV_KEY);
  }

  public void setTauxAccTrav(String value) {
    takeStoredValueForKey(value, TAUX_ACC_TRAV_KEY);
  }

  public java.math.BigDecimal tauxHoraireContrat() {
    return (java.math.BigDecimal) storedValueForKey(TAUX_HORAIRE_CONTRAT_KEY);
  }

  public void setTauxHoraireContrat(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TAUX_HORAIRE_CONTRAT_KEY);
  }

  public java.math.BigDecimal tauxPlafondSecu() {
    return (java.math.BigDecimal) storedValueForKey(TAUX_PLAFOND_SECU_KEY);
  }

  public void setTauxPlafondSecu(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TAUX_PLAFOND_SECU_KEY);
  }

  public String temAnnulation() {
    return (String) storedValueForKey(TEM_ANNULATION_KEY);
  }

  public void setTemAnnulation(String value) {
    takeStoredValueForKey(value, TEM_ANNULATION_KEY);
  }

  public String temCotiseSolidarite() {
    return (String) storedValueForKey(TEM_COTISE_SOLIDARITE_KEY);
  }

  public void setTemCotiseSolidarite(String value) {
    takeStoredValueForKey(value, TEM_COTISE_SOLIDARITE_KEY);
  }

  public String temPaiementPonctuel() {
    return (String) storedValueForKey(TEM_PAIEMENT_PONCTUEL_KEY);
  }

  public void setTemPaiementPonctuel(String value) {
    takeStoredValueForKey(value, TEM_PAIEMENT_PONCTUEL_KEY);
  }

  public String temPayeLocale() {
    return (String) storedValueForKey(TEM_PAYE_LOCALE_KEY);
  }

  public void setTemPayeLocale(String value) {
    takeStoredValueForKey(value, TEM_PAYE_LOCALE_KEY);
  }

  public String temPayeUtile() {
    return (String) storedValueForKey(TEM_PAYE_UTILE_KEY);
  }

  public void setTemPayeUtile(String value) {
    takeStoredValueForKey(value, TEM_PAYE_UTILE_KEY);
  }

  public String temPlafondReduit() {
    return (String) storedValueForKey(TEM_PLAFOND_REDUIT_KEY);
  }

  public void setTemPlafondReduit(String value) {
    takeStoredValueForKey(value, TEM_PLAFOND_REDUIT_KEY);
  }

  public String temPreContrat() {
    return (String) storedValueForKey(TEM_PRE_CONTRAT_KEY);
  }

  public void setTemPreContrat(String value) {
    takeStoredValueForKey(value, TEM_PRE_CONTRAT_KEY);
  }

  public String temTempsPlein() {
    return (String) storedValueForKey(TEM_TEMPS_PLEIN_KEY);
  }

  public void setTemTempsPlein(String value) {
    takeStoredValueForKey(value, TEM_TEMPS_PLEIN_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat contratPrecedent() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat)storedValueForKey(CONTRAT_PRECEDENT_KEY);
  }

  public void setContratPrecedentRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat oldValue = contratPrecedent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTRAT_PRECEDENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONTRAT_PRECEDENT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeFonction fonction() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeFonction)storedValueForKey(FONCTION_KEY);
  }

  public void setFonctionRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeFonction value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeFonction oldValue = fonction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FONCTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FONCTION_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOGrade grade() {
    return (org.cocktail.papaye.server.metier.grhum.EOGrade)storedValueForKey(GRADE_KEY);
  }

  public void setGradeRelationship(org.cocktail.papaye.server.metier.grhum.EOGrade value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOGrade oldValue = grade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GRADE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GRADE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOIndividu individu() {
    return (org.cocktail.papaye.server.metier.grhum.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.papaye.server.metier.grhum.EOIndividu value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeParamDADS motifDebut() {
    return (org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeParamDADS)storedValueForKey(MOTIF_DEBUT_KEY);
  }

  public void setMotifDebutRelationship(org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeParamDADS value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeParamDADS oldValue = motifDebut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOTIF_DEBUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOTIF_DEBUT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeParamDADS motifFin() {
    return (org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeParamDADS)storedValueForKey(MOTIF_FIN_KEY);
  }

  public void setMotifFinRelationship(org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeParamDADS value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeParamDADS oldValue = motifFin();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOTIF_FIN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOTIF_FIN_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur secteur() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur)storedValueForKey(SECTEUR_KEY);
  }

  public void setSecteurRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur oldValue = secteur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SECTEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SECTEUR_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut statut() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut)storedValueForKey(STATUT_KEY);
  }

  public void setStatutRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut oldValue = statut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STATUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STATUT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOStructure structure() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_KEY);
  }

  public void setStructureRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOStructure structureSiret() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_SIRET_KEY);
  }

  public void setStructureSiretRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structureSiret();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_SIRET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_SIRET_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOTypeContratTravail typeContratTravail() {
    return (org.cocktail.papaye.server.metier.grhum.EOTypeContratTravail)storedValueForKey(TYPE_CONTRAT_TRAVAIL_KEY);
  }

  public void setTypeContratTravailRelationship(org.cocktail.papaye.server.metier.grhum.EOTypeContratTravail value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOTypeContratTravail oldValue = typeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CONTRAT_TRAVAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CONTRAT_TRAVAIL_KEY);
    }
  }
  
  public NSArray contratLbuds() {
    return (NSArray)storedValueForKey(CONTRAT_LBUDS_KEY);
  }

  public NSArray contratLbuds(EOQualifier qualifier) {
    return contratLbuds(qualifier, null, false);
  }

  public NSArray contratLbuds(EOQualifier qualifier, boolean fetch) {
    return contratLbuds(qualifier, null, fetch);
  }

  public NSArray contratLbuds(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeContratLbud.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeContratLbud.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = contratLbuds();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToContratLbudsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeContratLbud object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CONTRAT_LBUDS_KEY);
  }

  public void removeFromContratLbudsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeContratLbud object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONTRAT_LBUDS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeContratLbud createContratLbudsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeContratLbud");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CONTRAT_LBUDS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeContratLbud) eo;
  }

  public void deleteContratLbudsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeContratLbud object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONTRAT_LBUDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllContratLbudsRelationships() {
    Enumeration objects = contratLbuds().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteContratLbudsRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeContratLbud)objects.nextElement());
    }
  }

  public NSArray historiques() {
    return (NSArray)storedValueForKey(HISTORIQUES_KEY);
  }

  public NSArray historiques(EOQualifier qualifier) {
    return historiques(qualifier, null, false);
  }

  public NSArray historiques(EOQualifier qualifier, boolean fetch) {
    return historiques(qualifier, null, fetch);
  }

  public NSArray historiques(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = historiques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToHistoriquesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto object) {
    addObjectToBothSidesOfRelationshipWithKey(object, HISTORIQUES_KEY);
  }

  public void removeFromHistoriquesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, HISTORIQUES_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto createHistoriquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeHisto");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, HISTORIQUES_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto) eo;
  }

  public void deleteHistoriquesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, HISTORIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllHistoriquesRelationships() {
    Enumeration objects = historiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteHistoriquesRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto)objects.nextElement());
    }
  }

  public NSArray periodes() {
    return (NSArray)storedValueForKey(PERIODES_KEY);
  }

  public NSArray periodes(EOQualifier qualifier) {
    return periodes(qualifier, null, false);
  }

  public NSArray periodes(EOQualifier qualifier, boolean fetch) {
    return periodes(qualifier, null, fetch);
  }

  public NSArray periodes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = periodes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPeriodesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERIODES_KEY);
  }

  public void removeFromPeriodesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERIODES_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode createPeriodesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayePeriode");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERIODES_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode) eo;
  }

  public void deletePeriodesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERIODES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPeriodesRelationships() {
    Enumeration objects = periodes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePeriodesRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode)objects.nextElement());
    }
  }

  public NSArray personnalisations() {
    return (NSArray)storedValueForKey(PERSONNALISATIONS_KEY);
  }

  public NSArray personnalisations(EOQualifier qualifier) {
    return personnalisations(qualifier, null, false);
  }

  public NSArray personnalisations(EOQualifier qualifier, boolean fetch) {
    return personnalisations(qualifier, null, fetch);
  }

  public NSArray personnalisations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = personnalisations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPersonnalisationsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERSONNALISATIONS_KEY);
  }

  public void removeFromPersonnalisationsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNALISATIONS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso createPersonnalisationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayePerso");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERSONNALISATIONS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso) eo;
  }

  public void deletePersonnalisationsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNALISATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPersonnalisationsRelationships() {
    Enumeration objects = personnalisations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePersonnalisationsRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso)objects.nextElement());
    }
  }

  public NSArray preparations() {
    return (NSArray)storedValueForKey(PREPARATIONS_KEY);
  }

  public NSArray preparations(EOQualifier qualifier) {
    return preparations(qualifier, null, false);
  }

  public NSArray preparations(EOQualifier qualifier, boolean fetch) {
    return preparations(qualifier, null, fetch);
  }

  public NSArray preparations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = preparations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPreparationsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PREPARATIONS_KEY);
  }

  public void removeFromPreparationsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PREPARATIONS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa createPreparationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayePrepa");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PREPARATIONS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa) eo;
  }

  public void deletePreparationsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PREPARATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPreparationsRelationships() {
    Enumeration objects = preparations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePreparationsRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa)objects.nextElement());
    }
  }

  public NSArray validations() {
    return (NSArray)storedValueForKey(VALIDATIONS_KEY);
  }

  public NSArray validations(EOQualifier qualifier) {
    return validations(qualifier, null, false);
  }

  public NSArray validations(EOQualifier qualifier, boolean fetch) {
    return validations(qualifier, null, fetch);
  }

  public NSArray validations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = validations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToValidationsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid object) {
    addObjectToBothSidesOfRelationshipWithKey(object, VALIDATIONS_KEY);
  }

  public void removeFromValidationsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, VALIDATIONS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid createValidationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeValid");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, VALIDATIONS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid) eo;
  }

  public void deleteValidationsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, VALIDATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllValidationsRelationships() {
    Enumeration objects = validations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteValidationsRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPayeContrat avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeContrat createEOPayeContrat(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebContratTrav
, NSTimestamp dModification
, String temAnnulation
, String temPaiementPonctuel
, String temPayeLocale
, String temPayeUtile
, String temPreContrat
, String temTempsPlein
, org.cocktail.papaye.server.metier.grhum.EOIndividu individu			) {
    EOPayeContrat eo = (EOPayeContrat) createAndInsertInstance(editingContext, _EOPayeContrat.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebContratTrav(dDebContratTrav);
		eo.setDModification(dModification);
		eo.setTemAnnulation(temAnnulation);
		eo.setTemPaiementPonctuel(temPaiementPonctuel);
		eo.setTemPayeLocale(temPayeLocale);
		eo.setTemPayeUtile(temPayeUtile);
		eo.setTemPreContrat(temPreContrat);
		eo.setTemTempsPlein(temTempsPlein);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  
	  public EOPayeContrat localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeContrat)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeContrat creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeContrat creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeContrat object = (EOPayeContrat)createAndInsertInstance(editingContext, _EOPayeContrat.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeContrat localInstanceIn(EOEditingContext editingContext, EOPayeContrat eo) {
    EOPayeContrat localInstance = (eo == null) ? null : (EOPayeContrat)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeContrat#localInstanceIn a la place.
   */
	public static EOPayeContrat localInstanceOf(EOEditingContext editingContext, EOPayeContrat eo) {
		return EOPayeContrat.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeContrat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeContrat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeContrat)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeContrat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeContrat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeContrat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeContrat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeContrat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
