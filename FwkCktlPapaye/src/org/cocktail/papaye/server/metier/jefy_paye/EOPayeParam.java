/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Fri Mar 14 09:16:49 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import org.cocktail.papaye.server.common.Constantes;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public class EOPayeParam extends _EOPayeParam {
	/** Types du param&egrave;tre */
	public final static String PARAM_ENTIER = "E";
	public final static String PARAM_INDICE = "I";
	public final static String PARAM_MONTANT = "M";
	public final static String PARAM_TAUX = "T";

	public EOPayeParam() {
		super();
	}


	/** 
	 * retourne le parametre valide associe a un code donne
	 */
	public static EOPayeParam parametreValide(EOPayeCode code) {

		try {
			
			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeParam.CODE_KEY + " = %@", new NSArray(code)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeParam.TEM_VALIDE_KEY + " = %@", new NSArray(Constantes.VRAI)));

			return fetchFirstByQualifier(code.editingContext(), new EOAndQualifier(mesQualifiers));

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	
/**
 * Parametre pour un code et un mois donne
 * 
 * @param ec
 * @param code
 * @param moisCode
 * @return
 */
	public static EOPayeParam parametrePourMois(EOEditingContext ec, EOPayeCode code, Number moisCode) {
		
		
		try {
			
			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeParam.CODE_KEY + " = %@", new NSArray(code)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeParam.PPAR_MDEBUT_KEY + " <= %@", new NSArray(moisCode)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeParam.PPAR_MFIN_KEY + " >= %@", new NSArray(moisCode)));
			
			return fetchFirstByQualifier(ec, new EOAndQualifier(mesQualifiers));
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public String toString() {
		return new String(getClass().getName() +
				"\nentier : " + pparEntier() +
				"\ntype : " + pparType() +
				"\nlibelle : " + pparLibelle() +
				"\nindice : " + pparIndice() +
				"\nmontant : " + pparMontant() +
				"\ntaux : " + pparTaux() +
				"\ndebut : " + pparMdebut() +
				"\nfin : " + pparMfin());

	}

}
