// _EODiskParam.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODiskParam.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EODiskParam extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "DiskParam";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.v_disk_param";

// Attributes

	public static final String C1_KEY = "c1";
	public static final String C2_KEY = "c2";
	public static final String C3_KEY = "c3";
	public static final String C41_KEY = "c41";
	public static final String C42_KEY = "c42";
	public static final String C5_KEY = "c5";
	public static final String COMPTE_TPG_KEY = "compteTpg";
	public static final String D10_KEY = "d10";
	public static final String NOM_REMETTANT_KEY = "nomRemettant";
	public static final String NOM_TPG_KEY = "nomTpg";

//Colonnes dans la base de donnees
	public static final String C1_COLKEY = "C1";
	public static final String C2_COLKEY = "C2";
	public static final String C3_COLKEY = "C3";
	public static final String C41_COLKEY = "C41";
	public static final String C42_COLKEY = "C42";
	public static final String C5_COLKEY = "C5";
	public static final String COMPTE_TPG_COLKEY = "COMPTE_TPG";
	public static final String D10_COLKEY = "D10";
	public static final String NOM_REMETTANT_COLKEY = "NOM_REMETTANT";
	public static final String NOM_TPG_COLKEY = "NOM_TPG";

// Relationships



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String c1() {
    return (String) storedValueForKey(C1_KEY);
  }

  public void setC1(String value) {
    takeStoredValueForKey(value, C1_KEY);
  }

  public String c2() {
    return (String) storedValueForKey(C2_KEY);
  }

  public void setC2(String value) {
    takeStoredValueForKey(value, C2_KEY);
  }

  public String c3() {
    return (String) storedValueForKey(C3_KEY);
  }

  public void setC3(String value) {
    takeStoredValueForKey(value, C3_KEY);
  }

  public String c41() {
    return (String) storedValueForKey(C41_KEY);
  }

  public void setC41(String value) {
    takeStoredValueForKey(value, C41_KEY);
  }

  public String c42() {
    return (String) storedValueForKey(C42_KEY);
  }

  public void setC42(String value) {
    takeStoredValueForKey(value, C42_KEY);
  }

  public String c5() {
    return (String) storedValueForKey(C5_KEY);
  }

  public void setC5(String value) {
    takeStoredValueForKey(value, C5_KEY);
  }

  public String compteTpg() {
    return (String) storedValueForKey(COMPTE_TPG_KEY);
  }

  public void setCompteTpg(String value) {
    takeStoredValueForKey(value, COMPTE_TPG_KEY);
  }

  public String d10() {
    return (String) storedValueForKey(D10_KEY);
  }

  public void setD10(String value) {
    takeStoredValueForKey(value, D10_KEY);
  }

  public String nomRemettant() {
    return (String) storedValueForKey(NOM_REMETTANT_KEY);
  }

  public void setNomRemettant(String value) {
    takeStoredValueForKey(value, NOM_REMETTANT_KEY);
  }

  public String nomTpg() {
    return (String) storedValueForKey(NOM_TPG_KEY);
  }

  public void setNomTpg(String value) {
    takeStoredValueForKey(value, NOM_TPG_KEY);
  }


/**
 * Créer une instance de EODiskParam avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODiskParam createEODiskParam(EOEditingContext editingContext, String c1
, String c2
, String c3
, String c41
, String c42
, String c5
, String compteTpg
, String nomRemettant
, String nomTpg
			) {
    EODiskParam eo = (EODiskParam) createAndInsertInstance(editingContext, _EODiskParam.ENTITY_NAME);    
		eo.setC1(c1);
		eo.setC2(c2);
		eo.setC3(c3);
		eo.setC41(c41);
		eo.setC42(c42);
		eo.setC5(c5);
		eo.setCompteTpg(compteTpg);
		eo.setNomRemettant(nomRemettant);
		eo.setNomTpg(nomTpg);
    return eo;
  }

  
	  public EODiskParam localInstanceIn(EOEditingContext editingContext) {
	  		return (EODiskParam)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODiskParam creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODiskParam creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EODiskParam object = (EODiskParam)createAndInsertInstance(editingContext, _EODiskParam.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EODiskParam localInstanceIn(EOEditingContext editingContext, EODiskParam eo) {
    EODiskParam localInstance = (eo == null) ? null : (EODiskParam)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODiskParam#localInstanceIn a la place.
   */
	public static EODiskParam localInstanceOf(EOEditingContext editingContext, EODiskParam eo) {
		return EODiskParam.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODiskParam fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODiskParam fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODiskParam eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODiskParam)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODiskParam fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODiskParam fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODiskParam eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODiskParam)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODiskParam fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODiskParam eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODiskParam ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODiskParam fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
