// _EOPayeParamPerso.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeParamPerso.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeParamPerso extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeParamPerso";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_param_perso";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pparOrdre";

	public static final String PPAR_LIBELLE_KEY = "pparLibelle";
	public static final String PPAR_MDEBUT_KEY = "pparMdebut";
	public static final String PPAR_MFIN_KEY = "pparMfin";
	public static final String PPAR_VALEUR_KEY = "pparValeur";
	public static final String TEM_VALIDE_KEY = "temValide";

//Colonnes dans la base de donnees
	public static final String PPAR_LIBELLE_COLKEY = "ppar_libelle";
	public static final String PPAR_MDEBUT_COLKEY = "ppar_mdebut";
	public static final String PPAR_MFIN_COLKEY = "ppar_mfin";
	public static final String PPAR_VALEUR_COLKEY = "ppar_valeur";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

// Relationships
	public static final String CODE_KEY = "code";
	public static final String PERSONNALISATION_KEY = "personnalisation";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String pparLibelle() {
    return (String) storedValueForKey(PPAR_LIBELLE_KEY);
  }

  public void setPparLibelle(String value) {
    takeStoredValueForKey(value, PPAR_LIBELLE_KEY);
  }

  public Integer pparMdebut() {
    return (Integer) storedValueForKey(PPAR_MDEBUT_KEY);
  }

  public void setPparMdebut(Integer value) {
    takeStoredValueForKey(value, PPAR_MDEBUT_KEY);
  }

  public Integer pparMfin() {
    return (Integer) storedValueForKey(PPAR_MFIN_KEY);
  }

  public void setPparMfin(Integer value) {
    takeStoredValueForKey(value, PPAR_MFIN_KEY);
  }

  public String pparValeur() {
    return (String) storedValueForKey(PPAR_VALEUR_KEY);
  }

  public void setPparValeur(String value) {
    takeStoredValueForKey(value, PPAR_VALEUR_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode code() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode)storedValueForKey(CODE_KEY);
  }

  public void setCodeRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode oldValue = code();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso personnalisation() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso)storedValueForKey(PERSONNALISATION_KEY);
  }

  public void setPersonnalisationRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso oldValue = personnalisation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNALISATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNALISATION_KEY);
    }
  }
  

/**
 * Créer une instance de EOPayeParamPerso avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeParamPerso createEOPayeParamPerso(EOEditingContext editingContext, String pparLibelle
, Integer pparMdebut
, Integer pparMfin
, String pparValeur
, String temValide
, org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso personnalisation			) {
    EOPayeParamPerso eo = (EOPayeParamPerso) createAndInsertInstance(editingContext, _EOPayeParamPerso.ENTITY_NAME);    
		eo.setPparLibelle(pparLibelle);
		eo.setPparMdebut(pparMdebut);
		eo.setPparMfin(pparMfin);
		eo.setPparValeur(pparValeur);
		eo.setTemValide(temValide);
    eo.setPersonnalisationRelationship(personnalisation);
    return eo;
  }

  
	  public EOPayeParamPerso localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeParamPerso)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeParamPerso creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeParamPerso creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeParamPerso object = (EOPayeParamPerso)createAndInsertInstance(editingContext, _EOPayeParamPerso.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeParamPerso localInstanceIn(EOEditingContext editingContext, EOPayeParamPerso eo) {
    EOPayeParamPerso localInstance = (eo == null) ? null : (EOPayeParamPerso)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeParamPerso#localInstanceIn a la place.
   */
	public static EOPayeParamPerso localInstanceOf(EOEditingContext editingContext, EOPayeParamPerso eo) {
		return EOPayeParamPerso.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeParamPerso fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeParamPerso fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeParamPerso eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeParamPerso)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeParamPerso fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeParamPerso fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeParamPerso eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeParamPerso)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeParamPerso fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeParamPerso eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeParamPerso ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeParamPerso fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
