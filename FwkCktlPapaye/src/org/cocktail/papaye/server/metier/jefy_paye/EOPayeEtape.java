/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Tue Mar 18 14:59:20 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import java.util.Enumeration;

import org.cocktail.papaye.server.common.Constantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOPayeEtape extends _EOPayeEtape {
    /** libelle associe au demarrage d'une operation de paye */
    public final static String DEBUT_PAYE = "DEMARRAGE";
    /** libelle associe a la phase de verification de la paye */
    public final static String VERIFICATION_PAYE = "VERIFICATION";
    /** libelle associe a la phase de validation de la paye */
    public final static String VALIDATION_PAYE = "VALIDATION";
    /** libelle associe a la phase d'archivage de la paye */
   public final static String ARCHIVAGE_PAYE = "ARCHIVAGE";

    public EOPayeEtape() {
        super();
    }


    // méthodes ajoutées
    public String toString() {
        return new String(getClass().getName() +
                          "\nlibelle : " + petpLibelle() +
                          "\nclassement : " + petpClassement() +
                          "\nterminee : " + temClose() +
                          "\nsecteur : "+ secteur());
    }
    public boolean estOuverte() {
        return temClose().equals(Constantes.FAUX);
    }
    public boolean estFermee() {
        return temClose().equals(Constantes.VRAI);
    }
    public boolean estPhaseDemarrage() {
        return petpLibelle().equals(EOPayeEtape.DEBUT_PAYE);
    }
    public boolean estPhaseVerification() {
        return petpLibelle().equals(EOPayeEtape.VERIFICATION_PAYE);
    }
    public boolean estPhaseValidation() {
        return petpLibelle().equals(EOPayeEtape.VALIDATION_PAYE);
    }
    public boolean estPhaseArchivage() {
        return petpLibelle().equals(EOPayeEtape.ARCHIVAGE_PAYE);
    }
    /** ferme une etape */
    public void fermer() {
        setTemClose(Constantes.VRAI);
    }
    /** prepare l'etape suivante. Retourne null si
        l'etape en cours est l'etape d'historisation : cette derni&egrave;re est
        quand m&ecirc;me fermee */
    public EOPayeEtape preparerEtapeSuivante() {
        this.fermer();
        if (estPhaseArchivage()) {
            return null;
        } else {
            // fermer l'étape courante
            EOPayeEtape etape = new EOPayeEtape();
            etape.setPetpClassement(new Integer(petpClassement().intValue() + 1));
            etape.setTemClose(Constantes.FAUX);
            if (estPhaseDemarrage()) {
                etape.setPetpLibelle(EOPayeEtape.VERIFICATION_PAYE);
            } else if (estPhaseVerification()) {
                etape.setPetpLibelle(EOPayeEtape.VALIDATION_PAYE);
            } else if (estPhaseValidation()) {
                etape.setPetpLibelle(EOPayeEtape.ARCHIVAGE_PAYE);
            }
            if (secteur() != null) {
                etape.addObjectToBothSidesOfRelationshipWithKey(secteur(),"secteur");
            }
            return etape;
        }
    }
    /** initialise une etape de preparation pour un secteur donne
    lors d'une nouvelle operation de paye */
    public void initEtapeDemarrage(EOPayeSecteur secteur) {
        setPetpClassement(new Integer(1));
        setTemClose(Constantes.FAUX);
        setPetpLibelle(EOPayeEtape.DEBUT_PAYE);
        addObjectToBothSidesOfRelationshipWithKey(secteur,"secteur");
    }
    // méthodes de classe pour les recherches
    /** methode de classe permettant de trouver les etapes courantes
        @param editingContext editing context dans lequel fetcher les objets
        de sectorisation
        @return : object etape trouve ou null
      */
    public static NSArray etapesCourantes(EOEditingContext editingContext) {
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(
                                            "temClose = %@", new NSArray(new String(Constantes.FAUX)));
        EOFetchSpecification fs = new EOFetchSpecification("PayeEtape",qualifier,null);
        return editingContext.objectsWithFetchSpecification(fs);
    }
    /** methode de classe permettant de trouver l'etape courante pour le secteur
    @param editingContext editing context dans lequel fetcher les objets
    @param secteur secteur concerne par l'operation de paye ou null si pas
    de sectorisation
    @return : object etape trouve ou null
    */
    public static EOPayeEtape etapeCourante(EOEditingContext editingContext,EOPayeSecteur secteur) {
        EOQualifier qualifier = null;
        NSMutableArray values = new NSMutableArray(new String(Constantes.FAUX));
        if (secteur != null) {
            values.addObject(secteur);
            qualifier = EOQualifier.qualifierWithQualifierFormat(
                                                        "temClose = %@ AND secteur = %@", values);
        }
        else {
            qualifier = EOQualifier.qualifierWithQualifierFormat("temClose = %@",values);
        }
        EOFetchSpecification fs = new EOFetchSpecification("PayeEtape",qualifier,null);
        NSArray etapes = editingContext.objectsWithFetchSpecification(fs);
        try {
            return (EOPayeEtape)etapes.objectAtIndex(0);	// dernière étape ouverte pour ce secteur
        } catch (Exception e) {
            // aucune étape trouvée, démarrage
            return null;
        }
    }
    /** Supprime toutes les etapes de la base */
    public static void supprimerEtapes(EOEditingContext editingContext) {
        EOFetchSpecification fs = new EOFetchSpecification("PayeEtape",null,null);
        NSArray etapes = editingContext.objectsWithFetchSpecification(fs);
        Enumeration e = etapes.objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeEtape etape = (EOPayeEtape)e.nextElement();
            etape.removeObjectFromBothSidesOfRelationshipWithKey(etape.secteur(),"secteur");
            editingContext.deleteObject(etape);
        }
    }
    
}
