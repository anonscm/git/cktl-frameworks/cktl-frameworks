// _EOPayeRappel.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeRappel.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeRappel extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeRappel";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_rappel";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "prapOrdre";

	public static final String PRAP_APAYER_KEY = "prapApayer";
	public static final String PRAP_ASSIETTE_KEY = "prapAssiette";
	public static final String PRAP_LIBELLE_KEY = "prapLibelle";
	public static final String PRAP_NB_JOUR_KEY = "prapNbJour";
	public static final String PRUB_CLASSEMENT_KEY = "prubClassement";

//Colonnes dans la base de donnees
	public static final String PRAP_APAYER_COLKEY = "pelm_apayer";
	public static final String PRAP_ASSIETTE_COLKEY = "pelm_assiette";
	public static final String PRAP_LIBELLE_COLKEY = "pelm_libelle";
	public static final String PRAP_NB_JOUR_COLKEY = "prap_nbjour";
	public static final String PRUB_CLASSEMENT_COLKEY = "prub_classement";

// Relationships
	public static final String ELEMENT_KEY = "element";
	public static final String MOIS_KEY = "mois";
	public static final String RUBRIQUE_KEY = "rubrique";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal prapApayer() {
    return (java.math.BigDecimal) storedValueForKey(PRAP_APAYER_KEY);
  }

  public void setPrapApayer(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRAP_APAYER_KEY);
  }

  public java.math.BigDecimal prapAssiette() {
    return (java.math.BigDecimal) storedValueForKey(PRAP_ASSIETTE_KEY);
  }

  public void setPrapAssiette(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRAP_ASSIETTE_KEY);
  }

  public String prapLibelle() {
    return (String) storedValueForKey(PRAP_LIBELLE_KEY);
  }

  public void setPrapLibelle(String value) {
    takeStoredValueForKey(value, PRAP_LIBELLE_KEY);
  }

  public java.math.BigDecimal prapNbJour() {
    return (java.math.BigDecimal) storedValueForKey(PRAP_NB_JOUR_KEY);
  }

  public void setPrapNbJour(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRAP_NB_JOUR_KEY);
  }

  public String prubClassement() {
    return (String) storedValueForKey(PRUB_CLASSEMENT_KEY);
  }

  public void setPrubClassement(String value) {
    takeStoredValueForKey(value, PRUB_CLASSEMENT_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement element() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement)storedValueForKey(ELEMENT_KEY);
  }

  public void setElementRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement oldValue = element();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ELEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ELEMENT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique rubrique() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique)storedValueForKey(RUBRIQUE_KEY);
  }

  public void setRubriqueRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique oldValue = rubrique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RUBRIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RUBRIQUE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPayeRappel avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeRappel createEOPayeRappel(EOEditingContext editingContext, java.math.BigDecimal prapApayer
, java.math.BigDecimal prapAssiette
, String prapLibelle
, java.math.BigDecimal prapNbJour
, String prubClassement
, org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement element, org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois, org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique rubrique			) {
    EOPayeRappel eo = (EOPayeRappel) createAndInsertInstance(editingContext, _EOPayeRappel.ENTITY_NAME);    
		eo.setPrapApayer(prapApayer);
		eo.setPrapAssiette(prapAssiette);
		eo.setPrapLibelle(prapLibelle);
		eo.setPrapNbJour(prapNbJour);
		eo.setPrubClassement(prubClassement);
    eo.setElementRelationship(element);
    eo.setMoisRelationship(mois);
    eo.setRubriqueRelationship(rubrique);
    return eo;
  }

  
	  public EOPayeRappel localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeRappel)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeRappel creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeRappel creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeRappel object = (EOPayeRappel)createAndInsertInstance(editingContext, _EOPayeRappel.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeRappel localInstanceIn(EOEditingContext editingContext, EOPayeRappel eo) {
    EOPayeRappel localInstance = (eo == null) ? null : (EOPayeRappel)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeRappel#localInstanceIn a la place.
   */
	public static EOPayeRappel localInstanceOf(EOEditingContext editingContext, EOPayeRappel eo) {
		return EOPayeRappel.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeRappel fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeRappel fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeRappel eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeRappel)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeRappel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeRappel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeRappel eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeRappel)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeRappel fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeRappel eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeRappel ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeRappel fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
