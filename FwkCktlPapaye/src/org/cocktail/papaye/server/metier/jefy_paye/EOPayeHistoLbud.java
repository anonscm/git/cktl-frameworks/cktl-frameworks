/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Mon Feb 23 14:28:52 Europe/Paris 2004 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class EOPayeHistoLbud extends _EOPayeHistoLbud {

    public EOPayeHistoLbud() {
        super();
    }


	/** Initialise l'objet avec les donneacute;es de la ligne budgetaire passe en param&egrave;tre 
	@param contratLBuds ligne budgetaire 
	*/
	public void initAvecContratLBuds(EOPayeContratLbud contratLBuds) {
		if (contratLBuds != null) {
			double temp = contratLBuds.pclQuotite().doubleValue();
			setPhlQuotite(new BigDecimal(temp).setScale(2,BigDecimal.ROUND_HALF_UP));
			if (contratLBuds.codeAnalytique() != null) {
				addObjectToBothSidesOfRelationshipWithKey(contratLBuds.codeAnalytique(),"codeAnalytique");
			}
			if (contratLBuds.convention() != null) {
				addObjectToBothSidesOfRelationshipWithKey(contratLBuds.convention(),"convention");
			}
			if (contratLBuds.exercice() != null) {
				addObjectToBothSidesOfRelationshipWithKey(contratLBuds.exercice(),"exercice");
			}
			if (contratLBuds.organ() != null) {
				addObjectToBothSidesOfRelationshipWithKey(contratLBuds.organ(),"organ");
			}
			if (contratLBuds.typeAction() != null) {
				addObjectToBothSidesOfRelationshipWithKey(contratLBuds.typeAction(),"typeAction");
			}
			if (contratLBuds.typeCredit() != null) {
				addObjectToBothSidesOfRelationshipWithKey(contratLBuds.typeCredit(),"typeCredit");
			}
		
		}
	}
	// méthodes de classe
	/** Methode de classe : recherche toutes les lignes budgetaires historisees associees a une validation
	@param editingContext dans lequel faire le fetch
	@param validation validation concernee
	*/
	public static NSArray rechercherHistoriquesPourValidation(EOEditingContext editingContext,EOPayeValid validation) {
		NSArray values = new NSArray(validation);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("validation = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification("PayeHistoLbud",qualifier, null);
        return editingContext.objectsWithFetchSpecification(fs);
	}
}
