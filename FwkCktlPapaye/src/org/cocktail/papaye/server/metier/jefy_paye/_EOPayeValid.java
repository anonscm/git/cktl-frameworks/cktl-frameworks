// _EOPayeValid.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeValid.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import org.cocktail.papaye.server.common.EOInfoBulletinSalaire;


public abstract class _EOPayeValid extends  EOInfoBulletinSalaire {
	public static final String ENTITY_NAME = "PayeValid";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_valid";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "payeOrdre";

	public static final String PAYE_ADEDUIRE_KEY = "payeAdeduire";
	public static final String PAYE_ANNEE_KEY = "payeAnnee";
	public static final String PAYE_BIMPMOIS_KEY = "payeBimpmois";
	public static final String PAYE_BRUT_KEY = "payeBrut";
	public static final String PAYE_BRUT_TOTAL_KEY = "payeBrutTotal";
	public static final String PAYE_BSSMOIS_KEY = "payeBssmois";
	public static final String PAYE_COUT_KEY = "payeCout";
	public static final String PAYE_CUMUL_PLAFOND_KEY = "payeCumulPlafond";
	public static final String PAYE_INDICE_KEY = "payeIndice";
	public static final String PAYE_MODE_KEY = "payeMode";
	public static final String PAYE_NBHEURE_KEY = "payeNbheure";
	public static final String PAYE_NBJOUR_KEY = "payeNbjour";
	public static final String PAYE_NET_KEY = "payeNet";
	public static final String PAYE_PATRON_KEY = "payePatron";
	public static final String PAYE_PLAFOND_DU_MOIS_KEY = "payePlafondDuMois";
	public static final String PAYE_QUOTITE_KEY = "payeQuotite";
	public static final String PAYE_RETENUE_KEY = "payeRetenue";
	public static final String PAYE_TAUXHOR_KEY = "payeTauxhor";
	public static final String PSTA_ORDRE_KEY = "pstaOrdre";
	public static final String TEM_COMPTA_KEY = "temCompta";
	public static final String TEM_DISQUETTE_KEY = "temDisquette";
	public static final String TEM_RAPPEL_KEY = "temRappel";
	public static final String TEM_VERIFIE_KEY = "temVerifie";

//Colonnes dans la base de donnees
	public static final String PAYE_ADEDUIRE_COLKEY = "paye_adeduire";
	public static final String PAYE_ANNEE_COLKEY = "exe_ordre";
	public static final String PAYE_BIMPMOIS_COLKEY = "paye_bimp_mois";
	public static final String PAYE_BRUT_COLKEY = "paye_brut";
	public static final String PAYE_BRUT_TOTAL_COLKEY = "paye_brut_total";
	public static final String PAYE_BSSMOIS_COLKEY = "paye_bss_mois";
	public static final String PAYE_COUT_COLKEY = "paye_cout";
	public static final String PAYE_CUMUL_PLAFOND_COLKEY = "paye_cumul_plafond";
	public static final String PAYE_INDICE_COLKEY = "paye_indice";
	public static final String PAYE_MODE_COLKEY = "paye_mode";
	public static final String PAYE_NBHEURE_COLKEY = "paye_nb_heures";
	public static final String PAYE_NBJOUR_COLKEY = "paye_nb_jours";
	public static final String PAYE_NET_COLKEY = "paye_net";
	public static final String PAYE_PATRON_COLKEY = "paye_patron";
	public static final String PAYE_PLAFOND_DU_MOIS_COLKEY = "paye_plafond_du_mois";
	public static final String PAYE_QUOTITE_COLKEY = "paye_quotite";
	public static final String PAYE_RETENUE_COLKEY = "paye_retenue";
	public static final String PAYE_TAUXHOR_COLKEY = "paye_taux_horaire";
	public static final String PSTA_ORDRE_COLKEY = "psta_ordre";
	public static final String TEM_COMPTA_COLKEY = "tem_compta";
	public static final String TEM_DISQUETTE_COLKEY = "tem_disquette";
	public static final String TEM_RAPPEL_COLKEY = "tem_rappel";
	public static final String TEM_VERIFIE_COLKEY = "tem_verifie";

// Relationships
	public static final String ADRESSE_KEY = "adresse";
	public static final String CONTRAT_KEY = "contrat";
	public static final String HISTO_LBUDS_KEY = "histoLbuds";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String MOIS_KEY = "mois";
	public static final String RIB_KEY = "rib";
	public static final String SECTEUR_KEY = "secteur";
	public static final String STATUT_KEY = "statut";
	public static final String STRUCTURE_KEY = "structure";
	public static final String STRUCTURE_SIRET_KEY = "structureSiret";



}
