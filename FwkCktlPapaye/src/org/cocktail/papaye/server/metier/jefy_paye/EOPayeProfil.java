/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Wed Mar 19 12:24:18 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import org.cocktail.papaye.server.common.Constantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOPayeProfil extends _EOPayeProfil {

    public EOPayeProfil() {
        super();
    }


    // méthodes ajoutées
    public String toString() {
        return new String(getClass().getName() +
                          "\ndebut validite : " + profMdebut() +
                          "\nfin validite : " + profMfin() +
                          "\nrubrique : " + rubrique() +
                          "\nstatut :" + statut());
    }
    // méthodes de classe
    /* retourne tous les profils valides lies a une rubrique */
    public static NSArray profilsPourRubrique(EOEditingContext editingContext,EOPayeRubrique rubrique) {
        NSMutableArray values = new NSMutableArray(rubrique);
        values.addObject(Constantes.VRAI);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("rubrique = %@ AND temValide = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification("PayeProfil",qualifier,null);
        return editingContext.objectsWithFetchSpecification(fs);
    }
}
