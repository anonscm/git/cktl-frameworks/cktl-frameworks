/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Thu Apr 24 15:11:32 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class EOPayeRappel extends _EOPayeRappel {
    public EOPayeRappel() {
        super();
    }


    // méthodes ajoutées
    public String toString() {
        return new String( "Libelle : " + prapLibelle() +
                          "\nA payer : " + prapApayer() +
                          "\nAssiette : " + prapAssiette() +
                           "\nClassement rubrique : " + prubClassement() +
                           "\nprapNbJour : " + prapNbJour() +
                           "\nrubrique : " + rubrique() +
                           "\nmois : " + mois());
    }
    /** initialise un rappel
    @param rubrique rubrique associee a ce rappel
    @param periode periode au cours de laquelle le rappel est fait
    */
    public void initAvecRubriqueEtPeriode(EOPayeRubrique rubrique,EOPayePeriode periode) {
        setPrapLibelle(rubrique.prubLibelle());
        setPrubClassement(rubrique.prubClassement());
        setPrapNbJour(periode.pperNbJour());
        setRubriqueRelationship(rubrique);
        setMoisRelationship(periode.moisTraitement());
    }

    // méthodes de recherche : methodes de classe
    /** methode de classe permettant de trouver tous les rappels lies a un
    Elle retourne le premier objet trouve si plus d'un objet est trouve.
    @param editingContext editingContext dans lequel fetcher les objets
    @param element
    @return tableau des elements trouve
    */
    public static NSArray rappelsPourElement(EOEditingContext editingContext,EOPayeElement element) {
        
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOPayeRappel.ELEMENT_KEY + " = %@", new NSArray(element));

    	return fetchAll(editingContext, qualifier);
    	
    }
}
