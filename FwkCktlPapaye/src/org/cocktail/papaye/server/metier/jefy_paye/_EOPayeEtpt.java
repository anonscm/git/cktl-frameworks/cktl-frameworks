// _EOPayeEtpt.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeEtpt.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeEtpt extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeEtpt";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_etpt";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "etptId";

	public static final String ETPT_QUOTITE_KEY = "etptQuotite";
	public static final String ETPT_SEMAINES_KEY = "etptSemaines";
	public static final String ETPT_VALEUR_KEY = "etptValeur";

//Colonnes dans la base de donnees
	public static final String ETPT_QUOTITE_COLKEY = "ETPT_QUOTITE";
	public static final String ETPT_SEMAINES_COLKEY = "ETPT_SEMAINES";
	public static final String ETPT_VALEUR_COLKEY = "ETPT_VALEUR";

// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String INDIVIDU_KEY = "individu";
	public static final String MOIS_DEBUT_KEY = "moisDebut";
	public static final String MOIS_FIN_KEY = "moisFin";
	public static final String STATUT_KEY = "statut";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal etptQuotite() {
    return (java.math.BigDecimal) storedValueForKey(ETPT_QUOTITE_KEY);
  }

  public void setEtptQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ETPT_QUOTITE_KEY);
  }

  public java.math.BigDecimal etptSemaines() {
    return (java.math.BigDecimal) storedValueForKey(ETPT_SEMAINES_KEY);
  }

  public void setEtptSemaines(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ETPT_SEMAINES_KEY);
  }

  public java.math.BigDecimal etptValeur() {
    return (java.math.BigDecimal) storedValueForKey(ETPT_VALEUR_KEY);
  }

  public void setEtptValeur(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ETPT_VALEUR_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_admin.EOExercice exercice() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOExercice value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOIndividu individu() {
    return (org.cocktail.papaye.server.metier.grhum.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.papaye.server.metier.grhum.EOIndividu value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois moisDebut() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_DEBUT_KEY);
  }

  public void setMoisDebutRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = moisDebut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_DEBUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_DEBUT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois moisFin() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_FIN_KEY);
  }

  public void setMoisFinRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = moisFin();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_FIN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_FIN_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut statut() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut)storedValueForKey(STATUT_KEY);
  }

  public void setStatutRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut oldValue = statut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STATUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STATUT_KEY);
    }
  }
  

/**
 * Créer une instance de EOPayeEtpt avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeEtpt createEOPayeEtpt(EOEditingContext editingContext, java.math.BigDecimal etptQuotite
, java.math.BigDecimal etptSemaines
, java.math.BigDecimal etptValeur
			) {
    EOPayeEtpt eo = (EOPayeEtpt) createAndInsertInstance(editingContext, _EOPayeEtpt.ENTITY_NAME);    
		eo.setEtptQuotite(etptQuotite);
		eo.setEtptSemaines(etptSemaines);
		eo.setEtptValeur(etptValeur);
    return eo;
  }

  
	  public EOPayeEtpt localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeEtpt)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeEtpt creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeEtpt creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeEtpt object = (EOPayeEtpt)createAndInsertInstance(editingContext, _EOPayeEtpt.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeEtpt localInstanceIn(EOEditingContext editingContext, EOPayeEtpt eo) {
    EOPayeEtpt localInstance = (eo == null) ? null : (EOPayeEtpt)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeEtpt#localInstanceIn a la place.
   */
	public static EOPayeEtpt localInstanceOf(EOEditingContext editingContext, EOPayeEtpt eo) {
		return EOPayeEtpt.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeEtpt fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeEtpt fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeEtpt eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeEtpt)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeEtpt fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeEtpt fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeEtpt eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeEtpt)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeEtpt fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeEtpt eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeEtpt ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeEtpt fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
