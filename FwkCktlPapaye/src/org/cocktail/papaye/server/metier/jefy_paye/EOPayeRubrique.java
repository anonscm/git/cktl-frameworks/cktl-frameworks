/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Tue Mar 18 15:19:46 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.moteur.LanceurCalcul;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOPayeRubrique extends _EOPayeRubrique {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6515472875512104854L;
	private static LanceurCalcul lanceurCalcul;
	public static final String A_PAYER = "R";
	public static final String A_DEDUIRE = "S";
	public static final String PART_PATRONALE = "P";
	public static final String RUBRIQUE_STATUT = "S";
	public static final String RUBRIQUE_INDICIAIRE = "I";
	public static final String RUBRIQUE_PERSONNELLE = "P";
	public static final String RUBRIQUE_RAPPEL = "R";
	private static final String MENSUELLE = "M";
	private static final String TRIMESTRIELLE = "T";
	private static final String SEMESTRIELLE = "S";
	private static final String PRIME_ADMIN_TECH = "REMUNIAT";
	private static final String PRIME_IFTS = "REMUNIFT";
	// utilisé pour stocker la valeur d'un paramètre qui n'est pas à lire dans la base
	// et qui est fourni par l'application d'administration
	private EOPayeParam parametrePourRappel;

	public EOPayeRubrique() {
		super();
		parametrePourRappel = null;
	}


	// méthodes ajoutées
	/** choisit une des rubriques associ&eaecute;es a uen rubrique de rappel 
	 * null si ce n'est pas une rubrique de rappel */
	public EOPayeRubrique rubriquePourRubriqueRappel() {
		if (estRubriqueRappel()) {
			return null;
		} else {
			NSArray args = new NSArray(this);
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("rubriqueRappel = %@",args);
			EOFetchSpecification fs = new EOFetchSpecification(EOPayeRubrique.ENTITY_NAME,qualifier,null);
			NSArray results = editingContext().objectsWithFetchSpecification(fs);
			try {
				return (EOPayeRubrique)results.objectAtIndex(0);
			} catch (Exception e) {
				return null;
			}
		}
	}
	/** est une rubrique associe a un statut */
	public boolean estRubriqueStatut() {
		return prubType().equals(RUBRIQUE_STATUT);
	}
	/** est une rubrique personnelle uniquement pour les indiciaires */
	public boolean estRubriqueIndiciaire() {
		return prubType().equals(RUBRIQUE_INDICIAIRE);
	}
	/** est une rubrique personnelle accessible a tous les statuts */
	public boolean estRubriquePersonnelle() {
		return prubType().equals(RUBRIQUE_PERSONNELLE);
	}
	/** est une rubrique de rappel */
	public boolean estRubriqueRappel() {
		return prubType().equals(RUBRIQUE_RAPPEL);
	}
	/** retourne true si une rubrique est a deduire de la remuneration */
	public boolean estADeduire() {
		return (prubMode().equals(EOPayeRubrique.A_DEDUIRE));
	}
	/** retourne true si une rubrique est a ajouter a la remuneration */
	public boolean estAPayer() {
		return (prubMode().equals(EOPayeRubrique.A_PAYER));
	}
	/** retourne true si une rubrique est patronale */
	public boolean estPartPatronale() {
		return (prubMode().equals(EOPayeRubrique.PART_PATRONALE));
	}
	/** retourne true si une rubrique est imposable */
	public boolean estImposable() {
		return temImposable().equals(Constantes.VRAI);
	}
	/** retourne true si une rubrique de cotisation est deductible (i.e n'est pas imposable) */
	public boolean estDeductible() {
		return !estImposable();
	}
	/** retourne true si une rubrique est calculée */
	public boolean estCalculee() {
		return temEstCalcule().equals(Constantes.VRAI) ;
	}
	/** retourne true si une rubrique de remuneration rentre dans le calcul d'assiette */
	public boolean rentreDansAssiette() {
		return estAPayer() && temBaseAssiette().equals(Constantes.VRAI) ;
	}
	/** retourne true si une rubrique est une cotisation obligatoire */
	public boolean rentreDansCotisation() {
		return !estAPayer() && temBaseAssiette().equals(Constantes.VRAI) ;
	}
	/** retourne true si la rubrique doit &ecirc;tre inclus dans le calcul de la RAFP */
	public boolean rentreDansRAFP() {
		return temRafp() != null && temRafp().equals(Constantes.VRAI);
	}
	/** retourne true si une rubrique est une rubrique de retenue */
	public boolean estRetenue() {
		return estAPayer() && temRetenue().equals(Constantes.VRAI) ;
	}

	public boolean suitImputationBrut() {
		return temImputationBrut() != null && temImputationBrut().equals(Constantes.VRAI);
	}
	public boolean estAvantageEnNature() {
		return prubLibelle().startsWith("Avantages") == true || 
		prubLibelle().startsWith("Rappel Avantages") == true;
	}

	/** retourne true si une cotisation est la CSG ou la CRDS. La verification est
        effectue sur le premier code associe a cette rubrique */
	public boolean estContributionGeneralisee() {
		try {
			EOPayeCode code = (EOPayeCode)codes().objectAtIndex(0);
			return (code.pcodCode().endsWith("CRDS") || code.pcodCode().endsWith("CSG"));
		}
		catch (Exception e) {
			return false;
		}
	}
	/** retourne true si une cotisation est la la taxe sur salaire. */
	public boolean estTaxeSurSalaire() {
		try {
			Enumeration<EOPayeCode> e = codes().objectEnumerator();
			while (e.hasMoreElements()) {
				EOPayeCode code = e.nextElement();
				if (code.pcodCode().equals("COTTXSAL"))
					return true;

			}
			return false;
		}
		catch (Exception e) {
			return false;
		}
	}
	/** verifie si une rubrique est a payer ce mois
        @param mois EOPayeMois code du mois dans l'anne */
	public boolean estAPayerCeMois(EOPayeMois mois) {
		return estAPayerCeMois(mois.numeroDuMois());
	}
	/** utilise pour les rappels de param&egrave;tres retroactifs */
	public void setParametrePourRappel(EOPayeParam unParam) {
		if (codes().containsObject(unParam.code())) {
			parametrePourRappel = unParam;
		} else {
			parametrePourRappel = null;
		}
	}
	/** verifie si une rubrique est a payer ce mois
        @param index du mois dans l'anne */
	public boolean estAPayerCeMois(int moisCourant) {
		if (prubFrequence() == null || prubFrequence().equals(MENSUELLE)) {
			return true;
		} else {
			int baseFrequence = prubBaseFrequence().intValue();
			int frequence = 1;
			if (prubFrequence().equals(TRIMESTRIELLE)) {
				frequence = 3;
			} else if (prubFrequence().equals(SEMESTRIELLE)) {
				frequence = 6;
			}
			int resultat = moisCourant % frequence;
			return (resultat == 0 || resultat == baseFrequence) ? true : false;
		}
	}
	public boolean rentreDansCotisationMgen() {
		return temMgen() != null && temMgen().equals(Constantes.VRAI);
	}
	public void setRentreDansCotisationMgen(boolean aBool) {
		if (aBool) {
			setTemMgen(Constantes.VRAI);
		} else {
			setTemMgen(Constantes.FAUX);
		}
	}
	/** Retourne true si le code passe en param&egrave; est associe a la rubrique */
	public boolean  rubriqueACode(String codeRubrique) {
		java.util.Enumeration<EOPayeCode> e = codes().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeCode unCode = e.nextElement();
			if (unCode.pcodCode().equals(codeRubrique)) {
				return true;                
			}
		}
		return false;
	}
	/** calcul d'une rubrique :
        si la rubrique n'est pas calculee, on retourne un dictionnaire comportant le montant
        trouve (voir ci-dessous la description du dictionnaire). Sinon, la classe de calcul
        associee est instanciee, on lui demande d'effectuer le calcul. <BR>
        La valeur retournee est un dictionnaire comportant les resultats (voir la classe ModeleCalcul pour le descriptif du dictionnaire.
        @param periode periode pour le bulletin de salaire en cours
        @param prepa preparation associee a ce bulletin de salaire
		@param cumul des plafonds de S&acute;curite Sociale pour les rappels
        @param calculDirect le calcul est-il a effectuer en mode direct ou inverse
         (intermittent par exemple)
        @param elements tableauxdes elements (EOPayeElement) deja 
        obtenus pendant la generation du bulletin de salaire
        @param cumuls tableaux des cumuls (EOPayeCumul) deja evalues
        @param personnalisation personnalisation associee &agrave cette rubrique
            (nulle si rubrique standard ou rappel)
        @param periodesRappel tableau des periodes de rappel (on peut en avoir besoin pour calculer des cotisations)
        @param nbPeriodes nombre de periodes payées
        @param estRappel true si il s'agit du calcul d'un rappel
        @return dictionnaire contenant les resultats du calcul ou null si il s'est produit une exception ou que le resultat retourne n'est pas un dictionnaire
	 */
	public NSDictionary calculer(EOPayePeriode periode,EOPayePrepa prepa,BigDecimal cumulPlafondPourRappel,
			boolean calculDirect,NSArray elements,NSArray cumuls,
			EOPayePerso personnalisation,NSArray periodesRappel, Number nbPeriodes, boolean estUnRappel) 
	throws Exception {

		// RUBRIQUE NON CALCULEE
		if (!estCalculee()) {
			NSMutableDictionary resultat = new NSMutableDictionary(3);
			// il ne peut y avoir qu'un ou deux codes attaches à la rubrique
			// deux codes = parametre standard
			// un code = parametre perso
			// on prend de toute façon le premier
			int nbCodes = codes().count();
			EOPayeCode codeCumul = null;
			EOPayeCode codeParam = null;
			EOPayeCode codeMontant = null;
			EOPayeCode codeAssiette = null;

			switch(nbCodes) {
			case 0 : throw new Exception("la rubrique " + prubLibelle() + " n'a pas de code attache");
			case 1 :
				codeCumul = (EOPayeCode)codes().objectAtIndex(0);
				if (codeCumul.pcodCode().startsWith("REMUN") == false &&
						codeCumul.pcodCode().startsWith("COT") == false) {
					throw new Exception("Pas de code de cumul defini pour la rubrique " + prubLibelle());
				}
				break;
			case 2 :
				codeCumul = (EOPayeCode)codes().objectAtIndex(0);
				if (codeCumul.pcodCode().startsWith("REMUN") ||
						codeCumul.pcodCode().startsWith("COT")) {
					codeParam = (EOPayeCode)codes().objectAtIndex(1);
				} else {
					codeCumul = (EOPayeCode)codes().objectAtIndex(1);
					if (codeCumul.pcodCode().startsWith("REMUN") ||
							codeCumul.pcodCode().startsWith("COT")) {
						codeParam = (EOPayeCode)codes().objectAtIndex(0);
					} else {
						throw new Exception("Pas de code de cumul defini pour la rubrique " +  prubLibelle());
					}
				}
				break;
			default :
				if (estRubriqueRappel() && personnalisation != null) { // ajouté le 03/02/04 pour gérer les rappels manuels de rémunération et de cotisation
					Enumeration<EOPayeCode> e = codes().objectEnumerator();
					while (e.hasMoreElements()) {
						EOPayeCode codeCourant = e.nextElement();
						if (codeCourant.pcodCode().startsWith("REMUN") || codeCourant.pcodCode().startsWith("COT")) {
							codeCumul = codeCourant;
						} else if (codeCourant.pcodCode().startsWith("RAMON")) {
							codeMontant = codeCourant;
						} else if (codeCourant.pcodCode().startsWith("RAASS")) { 
							codeAssiette = codeCourant;
						}
					}
					if (codeCumul == null) {
						throw new Exception("Pas de code de cumul defini pour la rubrique " +  prubLibelle());
					}
					if (codeMontant == null) {
						throw new Exception("Pas de code montant defini pour la rubrique " +  prubLibelle());
					}
					if (codeAssiette == null) {
						throw new Exception("Pas de code assiette defini pour la rubrique " +  prubLibelle());
					}
				} else {
					throw new Exception("Trop de codes definis pour la rubrique " +
							prubLibelle());
				}
			}

			EOPayeParam param = null;
			if (parametrePourRappel != null && parametrePourRappel.code() == codeParam) {
				// cas des rappels rétroactifs suite à des changements de valeur
				// d'un paramètre
				param = parametrePourRappel;
			} else if (codeParam != null) {
				param = EOPayeParam.parametreValide(codeParam);
			}
			resultat.setObjectForKey(codeCumul,"CodeCumul");
			if (param != null) {
				// on a trouvé un paramètre standard
				resultat.setObjectForKey(param.pparMontant(),"Assiette");
				resultat.setObjectForKey(param.pparMontant(),"Montant");
				return new NSDictionary(resultat, codeParam);
			} else if (codeParam == null) { // on attend une personnalisation
				// si c'est une rubrique personnelle, récupérer son montant
				if (personnalisation != null) {
					// 21/12/06 - pour proratiser certaines primes
					float quotite = 1;
					if (codeCumul != null && (codeCumul.pcodCode().equals(PRIME_ADMIN_TECH) || codeCumul.pcodCode().equals(PRIME_IFTS)) && 
							periode.pperNbJour() != null && periode.pperNbJour().intValue() < 30) {
						quotite = periode.pperNbJour().floatValue() / 30;
					}
					if (codeAssiette != null) {
						EOPayeParamPerso paramPerso = EOPayeParamPerso.parametrePersoValide(editingContext(), codeAssiette, personnalisation);
						double assiette = new BigDecimal(paramPerso.pparValeur()).doubleValue() * quotite;
						resultat.setObjectForKey(new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_UP),"Assiette");
					} else {
						double assiette = personnalisation.persMontant().doubleValue() * quotite;
						resultat.setObjectForKey(new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_UP),"Assiette");
					}
					if (codeMontant != null) {
						EOPayeParamPerso paramPerso = EOPayeParamPerso.parametrePersoValide(editingContext(), codeMontant, personnalisation);
						double montant = new BigDecimal(paramPerso.pparValeur()).doubleValue() * quotite;
						resultat.setObjectForKey(new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP),"Montant");
					} else {
						double montant = personnalisation.persMontant().doubleValue() * quotite;
						resultat.setObjectForKey(new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP),"Montant");
					}
					return new NSDictionary(resultat, codeCumul);
				} else {
					// on attendait une rubrique personnelle
					throw new Exception("pas de personnalisation definie pour '" + prubLibelle() + " pour le contrat de " + periode.contrat().individu().identite());
				}
			} else
				throw new Exception("le parametre associe au code '" + codeParam.pcodCode() + "' n'est pas defini");

		} else {	// RUBRIQUE CALCULEE

			// instancier la classe de calcul qui doit être définie
			if (prubNomclasse() != null) {

				// instancier l'objet de calcul en utilisant le lanceur
				if (lanceurCalcul == null)
					lanceurCalcul = new LanceurCalcul();

				try {
					Object moteurCalcul = lanceurCalcul.instancierObjet(prubNomclasse());
					java.lang.reflect.Method method = moteurCalcul.getClass().getMethod ("calculer",new Class[] {NSDictionary.class});
					// remplir le dictionnaire avec les clés et valeurs
					NSMutableDictionary valeurs = new NSMutableDictionary(codes().count() + 11);
					// ajouter la période
					valeurs.setObjectForKey(periode,"Periode");
					// l'élément de préparation de paye
					valeurs.setObjectForKey(prepa,"PayePrepa");
					// ajouter le montant de cumul de plafonds SS pour les rappels
					valeurs.setObjectForKey(cumulPlafondPourRappel,"CumulPlafondPourRappel");
					// ajouter le fait que c'est un calcul direct ou non
					valeurs.setObjectForKey(new Boolean(calculDirect),"CalculDirect");
					// ajouter le fait qu'il s'agit d'un rappel ou non
					valeurs.setObjectForKey(new Boolean(estUnRappel),"Rappel");
					// ajouter les éléments
					valeurs.setObjectForKey(elements,"Elements");
					// ajouter les éléments
					valeurs.setObjectForKey(cumuls,"Cumuls");
					// ajouter les périodes de rappel
					if (periodesRappel != null)
						valeurs.setObjectForKey(periodesRappel,"PeriodesRappel");
					// ajouter le nombre de périodes de rappel
					valeurs.setObjectForKey(nbPeriodes,"NbPeriodes");
					// ajouter les données personnelles si c'est une rubrique perso
					if (personnalisation != null)
						valeurs.setObjectForKey(personnalisation,"Personnalisation");

					// ajouter tous les codes liés à cette rubrique :
					// la clé du dictionnaire est le code du code et l'objet est
					// le paramètre associé
					// ne pas ajouter les codes de cumul
					EOPayeCode codeCumul = null;
					Enumeration<EOPayeCode> e = codes().objectEnumerator();
					while (e.hasMoreElements()) {
						EOPayeCode code = e.nextElement();
						
						// regarder si il s'agit dun code de cumul
						if (code.pcodCode().startsWith("REMUN") == false &&
								code.pcodCode().startsWith("COT") == false) {
							// rechercher le paramètre associe
							EOPayeParam param;
							
							if (parametrePourRappel != null && parametrePourRappel.code() == code)
								// il s'agit d'un paramètre de rappel
								// cas des rappels retroactifs suite à des changements de valeur d'un paramètre
								param = parametrePourRappel;
							else {
								if (estUnRappel)
									param = EOPayeParam.parametrePourMois(editingContext(), code, periode.moisTraitement().moisCode());
								else
									param = EOPayeParam.parametreValide(code);
							}
							
							if (param == null) {	// pas de parametre standard trouve
								// si on attendait parametre standard, lancer une exception
								if (personnalisation == null)
									throw new Exception("le parametre associe au code '" + code.pcodCode() + "' n'est pas defini");
								else {
									// rechercher le parametre personnel valide pour ce code et cette personnalisation
									EOPayeParamPerso paramPerso = EOPayeParamPerso.parametrePersoValide(editingContext(), code, personnalisation);
									if (paramPerso == null)
										throw new Exception("Le parametre associe au code personnel '" + code.pcodCode() + "' et au contrat " + prepa.contrat() + " n'est pas defini !");
									else
										valeurs.setObjectForKey(paramPerso,code.pcodCode());
								}
							} else
								valeurs.setObjectForKey(param,code.pcodCode());

						} else {
							codeCumul = code;
							valeurs.setObjectForKey(codeCumul,"CodeCumul");
						}
					}

					if (codeCumul == null)
						throw new Exception("Pas de code de cumul defini pour la rubrique " + prubLibelle());


					// déclencher la méthode
					try {
						
						Object resultat = method.invoke(moteurCalcul,new Object [] {valeurs});

						// vérifier si la réponse retournée est bien un dictionnaire sinon retourner null
						if (resultat.getClass().getName().equals("com.webobjects.foundation.NSDictionary")
								|| resultat.getClass().getName().equals("com.webobjects.foundation.NSMutableDictionary"))
							return (NSDictionary)resultat;

						return null;

					} catch (Exception exception) {
						if (exception.getClass() ==
							java.lang.reflect.InvocationTargetException.class) {
							String message = ((java.lang.reflect.InvocationTargetException)exception).getTargetException().getMessage();
							if (message != null)
								throw new Exception(message);
							else
								throw exception;

						} else
							throw exception;

					}
				} catch (Exception e) { throw e; }
			} else {
				throw new Exception("Pas de classe associee pour la rubrique calculee " + prubLibelle());
			}
		}
	}

	/*   public String toString() {
       return new String(getClass().getName() +
                          "\nclassement : " + prubClassement() +
                          "\nlibelle imprimable : " + prubLibelleImp() +
                          "\nlibelle : " + prubLibelle() +
                          "\ndebut validite : " + prubMdebut() +
                          "\nfin validite : " + prubMfin() +
                          "\nmode : " + prubMode() +
                          "\nintervient dans l'assiette : " + temBaseAssiette() +
                          "\nest calculee : " + temEstCalcule() +
                          "\nest imposable : " + temImposable() +
                          "\nnom classe : " + prubNomclasse() +
                          "\nimputation budgetaire : " + prubImput());
    }*/
	public String toString() {
		return new String(prubLibelle());
	}

	// méthodes de recherche : méthodes de classe
	/** methode de classe permettant de trouver des rubriques en fonction d'un code. <BR>
        @param code identifiant pour retrouver la rubrique
        @param editingContext editingContext dans lequel fetcher les objets
        @return rubriques trouvees
	 */
	public static NSArray rechercherRubriques(String code,EOEditingContext editingContext) {
		// Rechercher un élément de classe EOPayeCode sur son code
		NSMutableArray values = new NSMutableArray(code);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("pcodCode = %@",values);
		EOFetchSpecification fs = new EOFetchSpecification("PayeCode",qualifier, null);
		fs.setPrefetchingRelationshipKeyPaths(new NSArray("rubriques"));
		NSArray codes = editingContext.objectsWithFetchSpecification(fs);
		if (codes == null || codes.count() == 0) {
			// pas de code trouvé, la valeur passée en paramètre était probablement invalide
			return null;
		} else {
			EOPayeCode objetCode = (EOPayeCode)codes.objectAtIndex(0);
			return objetCode.rubriques();
		}
	}
	/** methode de classe permettant de trouver une rubrique en fonction d'un code. Cette methode suppose que certaines rubriques ont et&eacute associees a un code unique qui n'est utilise que par elles.<BR>
        Elle retourne le premier objet trouve si plus d'un objet est trouve.
        @param code identifiant pour retrouver la rubrique
        @param editingContext editingContext dans lequel fetcher les objets
        @return rubrique trouvee
	 */
	public static EOPayeRubrique rechercherRubrique(String code,EOEditingContext editingContext) {
		NSArray rubriques = rechercherRubriques(code,editingContext);
		if (rubriques != null) {
			return (EOPayeRubrique)rubriques.objectAtIndex(0);
		} else {
			return null;
		}
		// Rechercher un élément de classe EOPayeCode sur son code
	}


}
