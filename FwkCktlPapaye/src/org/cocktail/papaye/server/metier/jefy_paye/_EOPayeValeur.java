// _EOPayeValeur.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeValeur.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeValeur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeValeur";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_valeur";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pvalOrdre";

	public static final String PVAL_LIBELLE_KEY = "pvalLibelle";
	public static final String PVAL_MDEBUT_KEY = "pvalMdebut";
	public static final String PVAL_MFIN_KEY = "pvalMfin";
	public static final String PVAL_VALEUR_KEY = "pvalValeur";
	public static final String TEM_VALIDE_KEY = "temValide";

//Colonnes dans la base de donnees
	public static final String PVAL_LIBELLE_COLKEY = "pval_libelle";
	public static final String PVAL_MDEBUT_COLKEY = "pval_mdebut";
	public static final String PVAL_MFIN_COLKEY = "pval_mfin";
	public static final String PVAL_VALEUR_COLKEY = "pval_valeur";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

// Relationships
	public static final String CODE_KEY = "code";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String pvalLibelle() {
    return (String) storedValueForKey(PVAL_LIBELLE_KEY);
  }

  public void setPvalLibelle(String value) {
    takeStoredValueForKey(value, PVAL_LIBELLE_KEY);
  }

  public Integer pvalMdebut() {
    return (Integer) storedValueForKey(PVAL_MDEBUT_KEY);
  }

  public void setPvalMdebut(Integer value) {
    takeStoredValueForKey(value, PVAL_MDEBUT_KEY);
  }

  public Integer pvalMfin() {
    return (Integer) storedValueForKey(PVAL_MFIN_KEY);
  }

  public void setPvalMfin(Integer value) {
    takeStoredValueForKey(value, PVAL_MFIN_KEY);
  }

  public String pvalValeur() {
    return (String) storedValueForKey(PVAL_VALEUR_KEY);
  }

  public void setPvalValeur(String value) {
    takeStoredValueForKey(value, PVAL_VALEUR_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode code() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode)storedValueForKey(CODE_KEY);
  }

  public void setCodeRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode oldValue = code();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPayeValeur avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeValeur createEOPayeValeur(EOEditingContext editingContext, String pvalLibelle
, Integer pvalMdebut
, Integer pvalMfin
, String pvalValeur
, String temValide
, org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode code			) {
    EOPayeValeur eo = (EOPayeValeur) createAndInsertInstance(editingContext, _EOPayeValeur.ENTITY_NAME);    
		eo.setPvalLibelle(pvalLibelle);
		eo.setPvalMdebut(pvalMdebut);
		eo.setPvalMfin(pvalMfin);
		eo.setPvalValeur(pvalValeur);
		eo.setTemValide(temValide);
    eo.setCodeRelationship(code);
    return eo;
  }

  
	  public EOPayeValeur localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeValeur)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeValeur creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeValeur creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeValeur object = (EOPayeValeur)createAndInsertInstance(editingContext, _EOPayeValeur.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeValeur localInstanceIn(EOEditingContext editingContext, EOPayeValeur eo) {
    EOPayeValeur localInstance = (eo == null) ? null : (EOPayeValeur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeValeur#localInstanceIn a la place.
   */
	public static EOPayeValeur localInstanceOf(EOEditingContext editingContext, EOPayeValeur eo) {
		return EOPayeValeur.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeValeur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeValeur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeValeur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeValeur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeValeur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeValeur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeValeur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeValeur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeValeur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeValeur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeValeur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeValeur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
