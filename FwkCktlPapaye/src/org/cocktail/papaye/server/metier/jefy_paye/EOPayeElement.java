/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Fri Mar 14 09:16:37 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.common.EOInfoBulletinSalaire;
import org.cocktail.papaye.server.metier.grhum.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOPayeElement extends _EOPayeElement {
	// constantes
	/** Type de l'element : remuneration */
	public final static String TYPE_REMUNERATION = "R";
	/** Type de l'element : deduction */
	public final static String TYPE_DEDUCTION = "D";
	/** Type de l'element : part patronale */
	public final static String TYPE_PART_PATRONALE = "P";
	/** Type de profil l'element : standard */
	public final static String PROFIL_TYPE = "P";
	/** Type de profil l'element : personnel */
	public final static String PROFIL_PERSONNEL = "V";
	private final static String TAXE_SUR_SALAIRE = "TAXSSAL";

	public EOPayeElement() {
		super();
	}



	// méthodes ajoutées
	/** l'element correspond a une remuneration
	 */
	public boolean estUneRemuneration() {
		return pelmType().equals(EOPayeElement.TYPE_REMUNERATION);
	}
	/** l'element correspond a une deduction
	 */
	public boolean estUneDeduction() {
		return pelmType().equals(EOPayeElement.TYPE_DEDUCTION);
	}
	/** l'element correspond a une charge patronale
	 */
	public boolean estUneChargePatronale() {
		return pelmType().equals(EOPayeElement.TYPE_PART_PATRONALE);
	}
	/** l'element est issu d'une rubrique personnelle */
	public boolean estRubriquePersonnelle() {
		return pelmMode().equals(EOPayeElement.PROFIL_PERSONNEL);
	}
	/** Initialise un element a partir des donnees d'une
        rubrique. Initialise l'ensemble des montants à zéro
        @param rubrique la rubrique a partir de laquelle faire l'initialisation
        @code code de cet element
	 */
	public void  initAvecRubriqueEtCode(EOPayeRubrique rubrique,EOPayeCode code) {
		setCodeRelationship(code);
		String s = code.pcodCode();
		// traitement particulier pour la taxe sur salaire où on peut avoir un taux différent
		if (s.startsWith(TAXE_SUR_SALAIRE)) {
			// incrémenter le rang de classement pour que les tranches soient dans l'ordre
			String numTranche = s.substring(s.length() - 1);
			int num = Integer.valueOf(rubrique.prubClassement()).intValue();
			num += new Integer(numTranche).intValue();
			setPrubClassement(Integer.toString(num));
			// modifier le libellé
			setPelmLibelle(rubrique.prubLibelleImp() + " " + numTranche);
		} else {
			setPrubClassement(rubrique.prubClassement());
			setPelmLibelle(rubrique.prubLibelleImp());
		}
		// imputation comptable
		setPelmTaux(new BigDecimal(0));
		setPelmAssiette(new BigDecimal(0));
		setPelmApayer(new BigDecimal(0));
		setPelmAdeduire(new BigDecimal(0));
		setPelmPatron(new BigDecimal(0));
		addObjectToBothSidesOfRelationshipWithKey(rubrique,"rubrique");
		// Connex
		setPelmConex("01");
	}
	/** Verifie si un element est associe avec une rubrique
        ayant le code passe en param&egrave;tre */
	public boolean estElementAvecCode(String code) {
		return rubrique().codes().containsObject(code);
	}
	public NSArray lbuds() {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOPayeElementLbud.PAYE_ELEMENT_KEY + " = %@", new NSArray(this));
		return editingContext().objectsWithFetchSpecification(new EOFetchSpecification(EOPayeElementLbud.ENTITY_NAME,qualifier,null));
	}
	public String toString() {
		return new String(getClass().getName() +
				"\nmode : "+ pelmMode() +
				"\ntype : "+ pelmType() +
				"\ncode classement : " + prubClassement() +
				"\nlibelle : "+ pelmLibelle() +
				"\na deduire : "+ pelmAdeduire() +
				"\na payer : "+ pelmApayer() +
				"\npart patronale : "+ pelmPatron() +
				"\nmontant : "+ pelmAssiette() +
				"\ntaux calcul :" + pelmTaux() +
				"\ncode : "+ code() +
				"\nrubrique : "+ rubrique());
	}
	// méthodes de classe
	/** methode de classe permettant de trouver dans l'historique un element
        pour un contrat, une rubrique et un mois
        @param editingContext editingContext dans lequel fetcher les objets
        @param rubrique pour laquelle on recherche cet element
        @param contrat contrat concerne
        @param mois
        @return element trouve
	 */
	public static EOPayeElement elementPourRubriqueContratEtMois(EOEditingContext editingContext,
			EOPayeRubrique rubrique,
			EOPayeContrat contrat,
			EOPayeMois mois) throws Exception {
		EOPayeHisto historique = EOPayeHisto.chercherHistoriquePourMoisEtContrat(editingContext,
				mois,
				contrat,
				null);
		try {
			NSArray elements = EOPayeElement.findForHistorique(editingContext, historique);
			Enumeration e = elements.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPayeElement element = (EOPayeElement)e.nextElement();
				if (element.rubrique() == rubrique) {
					return element;
				}
			}
			return null;
		} catch (Exception e) { throw e; }
	}
	/** methode de classe permettant de trouver dans l'historique un element
        pour un certain code
        @param editingContext editingContext dans lequel fetcher les objets
        @param historique historique pour lequel on recherche une element
        @param code EOPayeCode de l'element recherche
        @return element trouve
	 */
	public static EOPayeElement elementPourHistoriqueEtCode(EOEditingContext editingContext,
			EOPayeHisto historique,
			EOPayeCode code) {
		NSArray elements = EOPayeElement.findForHistorique(editingContext, historique);
		Enumeration e = elements.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeElement element = (EOPayeElement)e.nextElement();
			if (element.code() == code) {
				return element;
			}
		}
		return null;
	}
	/** m&acute;thode de classe evaluant si il y a des el&eacutements utilisant une
        rubrique
        @param editingContext editingContext dans lequel fetcher les objets
        @param rubrique rubrique pour laquelle on recherche une element
        @return boolean vrai si des el&eacutements existent
	 */
	public static boolean ExisteElementPourRubrique(EOEditingContext editingContext,EOPayeRubrique rubrique) {
		// Rechercher sur la rubrique
		NSArray values = new NSArray(rubrique);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
		("rubrique = %@",values);
		EOFetchSpecification fs = new EOFetchSpecification("PayeElement",qualifier, null);
		NSArray elements = editingContext.objectsWithFetchSpecification(fs);
		if (elements == null || elements.count() == 0) {	// pas d'élément trouvé
			return false;
		} else {
			return true;
		}
	}
	/** methode recherchant tous les elements pour cette rubrique entre
        la date passee en param&egrave;tre et la date courante. Elle retourne aussi les
        elements pour la rubrique de rappel qui lui est associee
        @param editingContext editingContext dans lequel fetcher les objets
        @param rubrique rubrique pour laquelle on recherche une element
        @param moisDebut code mois de depart de la recherche
        @param moisFin code mois de depart de la recherche
        @return elements elements trouves
	 */
	public static NSArray  trouverElements(EOEditingContext editingContext,
			EOPayeRubrique rubrique,
			Number moisDebut,Number moisFin) {
		EOQualifier qualifier;
		// Rechercher sur la rubrique
		NSMutableArray values = new NSMutableArray(rubrique);
		if (rubrique.rubriqueRappel() != null) {
			values.addObject(rubrique.rubriqueRappel());
		}
		// sur le mois de départ
		values.addObject(moisDebut);
		// sur le mois de fin
		values.addObject(moisFin);
		if (rubrique.rubriqueRappel() != null) {
			qualifier = EOQualifier.qualifierWithQualifierFormat
			("(rubrique = %@ OR rubrique = %@) AND pelmMoisCode >= %@ AND pelmMoisCode <= %@ ",values);
		} else {
			qualifier = EOQualifier.qualifierWithQualifierFormat
			("rubrique = %@ AND pelmMoisCode >= %@ AND pelmMoisCode <= %@ ",values);
		}
		EOFetchSpecification fs = new EOFetchSpecification(EOPayeElement.ENTITY_NAME,qualifier, null);
		NSArray elements = editingContext.objectsWithFetchSpecification(fs);
		return elements;
	}


	public static NSArray findForBulletin(EOEditingContext ec, EOInfoBulletinSalaire bulletin) {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (EOPayePrepa.ENTITY_NAME.equals(bulletin.getClass().getName()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeElement.PREPARATION_KEY+ " = %@", new NSArray(bulletin)));    		
		else    		
			if (EOPayeValid.ENTITY_NAME.equals(bulletin.getClass().getName()))
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeElement.VALIDATION_KEY+ " = %@", new NSArray(bulletin)));    		
			else
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeElement.HISTORIQUE_KEY+ " = %@", new NSArray(bulletin)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers), null);

	}

	public static NSArray findForCodeAndAnnee(EOEditingContext ec, String code, Integer annee) {

		try {

			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeElement.CODE_KEY+"."+EOPayeCode.PCOD_CODE_KEY + " = %@", new NSArray(code)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeElement.HISTORIQUE_KEY+ "."+EOPayeHisto.PAYE_ANNEE_KEY + " = %@", new NSArray(annee)));

			return fetchAll(ec, new EOAndQualifier(mesQualifiers));
		}
		catch (Exception ex) {
			return new NSArray();
		}

	}


	public static NSArray findForHistorique(EOEditingContext ec, EOPayeHisto historique) {


		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeElement.HISTORIQUE_KEY+ " = %@", new NSArray(historique)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers));

	}



	public static NSArray findForPreparation(EOEditingContext ec, EOPayePrepa preparation) {


		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeElement.PREPARATION_KEY+ " = %@", new NSArray(preparation)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers));

	}


	public static NSArray findForValidation(EOEditingContext ec, EOPayeValid validation) {

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayeElement.VALIDATION_KEY+ " = %@", new NSArray(validation)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers));

	}


	/** me recherchant tous les elements pour cette rubrique entre
        deux dates
        @param editingContext editingContext dans lequel fetcher les objets
        @param rubrique rubrique pour laquelle on recherche une element
        @param contrat contrat recherche
        @param moisDebut code mois de depart de recherche
        @param moisFin code mois de fin de recherche
	 */
	public static NSArray  trouverElements(EOEditingContext editingContext,EOPayeContrat contrat,
			EOPayeRubrique rubrique,
			Number moisDebut,Number moisFin) {
		NSArray elements = trouverElements(editingContext,rubrique,moisDebut,moisFin);
		NSMutableArray elementsContrat = new NSMutableArray();
		java.util.Enumeration e = elements.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeElement element = (EOPayeElement)e.nextElement();
			if (element.historique().contrat() == contrat) {
				elementsContrat.addObject(element);
			}
		}
		return elementsContrat;
	}
	/** me recherchant tous les elements pour cette rubrique entre
        deux dates
        @param editingContext editingContext dans lequel fetcher les objets
        @param individu individu pour laquelle on recherche une element
        @param mois mois recherche
	 */
	public static NSArray  trouverElements(EOEditingContext editingContext,EOIndividu individu,EOPayeMois mois) {
		NSMutableArray values = new NSMutableArray(2);
		// sur le mois
		values.addObject(mois.moisCode());
		// sur l'individu
		values.addObject(individu);
		NSMutableArray prefetches = new NSMutableArray(EOPayeElement.RUBRIQUE_KEY);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("pelmMoisCode = %@ AND preparation.contrat.individu = %@",
				values);
		EOFetchSpecification fs = new EOFetchSpecification(EOPayeElement.ENTITY_NAME,qualifier, null);
		fs.setPrefetchingRelationshipKeyPaths(prefetches);
		NSArray elements = editingContext.objectsWithFetchSpecification(fs);

		if (elements.count() == 0) {
			// rechercher dans les validations
			qualifier = EOQualifier.qualifierWithQualifierFormat("pelmMoisCode = %@ AND validation.contrat.individu = %@",
					values);
			fs = new EOFetchSpecification(EOPayeElement.ENTITY_NAME,qualifier, null);
			fs.setPrefetchingRelationshipKeyPaths(prefetches);
			elements = editingContext.objectsWithFetchSpecification(fs);
			if (elements.count() == 0) {
				// rechercher dans les historiques
				qualifier = EOQualifier.qualifierWithQualifierFormat("pelmMoisCode = %@ AND historique.contrat.individu = %@",
						values);
				fs = new EOFetchSpecification(EOPayeElement.ENTITY_NAME,qualifier, null);
				fs.setPrefetchingRelationshipKeyPaths(prefetches);
				elements = editingContext.objectsWithFetchSpecification(fs);
			}
		}
		return elements;
	}
	/** trouve toutes les elements d'un certain type (remuneration/charge salariale/charge patronale) pour l'annee */
	public static NSArray trouverElementsPourTypeContratEtAnnee(EOEditingContext editingContext,String typeElement,EOPayeContrat contrat, Number annee) {
		NSMutableArray result = new NSMutableArray();
		NSMutableArray values = new NSMutableArray(6);
		// on le fait 3 fois pour chercher dans les prépa, valides et historiques
		// sur l'année
		values.addObject(annee);
		// sur l'individu
		values.addObject(contrat);
		values.addObject(typeElement);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("historique.payeAnnee = %@ AND historique.contrat = %@ AND pelmType = %@",values);
		// trier par moisCode croissant
		EOFetchSpecification fs = new EOFetchSpecification(EOPayeElement.ENTITY_NAME,qualifier, new NSArray(EOSortOrdering.sortOrderingWithKey("pelmMoisCode",EOSortOrdering.CompareAscending)));
		NSMutableArray prefetches = new NSMutableArray(EOPayeElement.RUBRIQUE_KEY);
		fs.setPrefetchingRelationshipKeyPaths(prefetches);
		result.addObjectsFromArray(editingContext.objectsWithFetchSpecification(fs));
		fs.setQualifier(EOQualifier.qualifierWithQualifierFormat("validation.payeAnnee = %@ AND validation.contrat = %@ AND pelmType = %@",values));
		result.addObjectsFromArray(editingContext.objectsWithFetchSpecification(fs));
		fs.setQualifier(EOQualifier.qualifierWithQualifierFormat("preparation.payeAnnee = %@ AND preparation.contrat = %@ AND pelmType = %@",values));		fs.setPrefetchingRelationshipKeyPaths(prefetches);
		result.addObjectsFromArray(editingContext.objectsWithFetchSpecification(fs));
		return result;
	}
	/** me recherchant à partir des elements tous les contrats payes
        entre deux dates
        @param editingContext editingContext dans lequel fetcher les objets
        @param rubrique rubrique pour laquelle on recherche une element
        @param moisCode date de depart de la recherche
        @param moisCode date de fin de la recherche
        @param contrats contrats trouves
	 */
	public static NSArray trouverContratsPayes(EOEditingContext editingContext,
			EOPayeRubrique rubrique,
			Number moisDebut,Number moisFin) {
		NSArray elements = trouverElements(editingContext,rubrique,moisDebut,moisFin);
		NSMutableArray contrats = new NSMutableArray();
		java.util.Enumeration e = elements.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeElement element = (EOPayeElement)e.nextElement();
			EOPayeContrat contrat = element.historique().contrat();
			if (contrats.containsObject(contrat) == false) {
				contrats.addObject(element.historique().contrat());
			}
		}
		return contrats;
	}
}
