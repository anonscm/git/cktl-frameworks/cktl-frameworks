// _EOPayeCode.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeCode.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeCode extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeCode";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_code";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pcodOrdre";

	public static final String PCOD_CODE_KEY = "pcodCode";
	public static final String PCOD_LIBELLE_KEY = "pcodLibelle";

//Colonnes dans la base de donnees
	public static final String PCOD_CODE_COLKEY = "pcod_code";
	public static final String PCOD_LIBELLE_COLKEY = "pcod_libelle";

// Relationships
	public static final String PARAMETRES_KEY = "parametres";
	public static final String RUBCODES_KEY = "rubcodes";
	public static final String RUBRIQUES_KEY = "rubriques";
	public static final String VALEURS_KEY = "valeurs";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String pcodCode() {
    return (String) storedValueForKey(PCOD_CODE_KEY);
  }

  public void setPcodCode(String value) {
    takeStoredValueForKey(value, PCOD_CODE_KEY);
  }

  public String pcodLibelle() {
    return (String) storedValueForKey(PCOD_LIBELLE_KEY);
  }

  public void setPcodLibelle(String value) {
    takeStoredValueForKey(value, PCOD_LIBELLE_KEY);
  }

  public NSArray parametres() {
    return (NSArray)storedValueForKey(PARAMETRES_KEY);
  }

  public NSArray parametres(EOQualifier qualifier) {
    return parametres(qualifier, null, false);
  }

  public NSArray parametres(EOQualifier qualifier, boolean fetch) {
    return parametres(qualifier, null, fetch);
  }

  public NSArray parametres(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam.CODE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = parametres();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToParametresRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PARAMETRES_KEY);
  }

  public void removeFromParametresRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PARAMETRES_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam createParametresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeParam");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PARAMETRES_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam) eo;
  }

  public void deleteParametresRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PARAMETRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllParametresRelationships() {
    Enumeration objects = parametres().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteParametresRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam)objects.nextElement());
    }
  }

  public NSArray rubcodes() {
    return (NSArray)storedValueForKey(RUBCODES_KEY);
  }

  public NSArray rubcodes(EOQualifier qualifier) {
    return rubcodes(qualifier, null, false);
  }

  public NSArray rubcodes(EOQualifier qualifier, boolean fetch) {
    return rubcodes(qualifier, null, fetch);
  }

  public NSArray rubcodes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode.CODE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = rubcodes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRubcodesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RUBCODES_KEY);
  }

  public void removeFromRubcodesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RUBCODES_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode createRubcodesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeRubcode");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RUBCODES_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode) eo;
  }

  public void deleteRubcodesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RUBCODES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRubcodesRelationships() {
    Enumeration objects = rubcodes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRubcodesRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode)objects.nextElement());
    }
  }

  public NSArray rubriques() {
    return (NSArray)storedValueForKey(RUBRIQUES_KEY);
  }

  public NSArray rubriques(EOQualifier qualifier) {
    return rubriques(qualifier, null);
  }

  public NSArray rubriques(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = rubriques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToRubriquesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RUBRIQUES_KEY);
  }

  public void removeFromRubriquesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RUBRIQUES_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique createRubriquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeRubrique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RUBRIQUES_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique) eo;
  }

  public void deleteRubriquesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RUBRIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRubriquesRelationships() {
    Enumeration objects = rubriques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRubriquesRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique)objects.nextElement());
    }
  }

  public NSArray valeurs() {
    return (NSArray)storedValueForKey(VALEURS_KEY);
  }

  public NSArray valeurs(EOQualifier qualifier) {
    return valeurs(qualifier, null, false);
  }

  public NSArray valeurs(EOQualifier qualifier, boolean fetch) {
    return valeurs(qualifier, null, fetch);
  }

  public NSArray valeurs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeValeur.CODE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeValeur.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = valeurs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToValeursRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeValeur object) {
    addObjectToBothSidesOfRelationshipWithKey(object, VALEURS_KEY);
  }

  public void removeFromValeursRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeValeur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, VALEURS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeValeur createValeursRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeValeur");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, VALEURS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeValeur) eo;
  }

  public void deleteValeursRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeValeur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, VALEURS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllValeursRelationships() {
    Enumeration objects = valeurs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteValeursRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeValeur)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPayeCode avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeCode createEOPayeCode(EOEditingContext editingContext, String pcodCode
, String pcodLibelle
			) {
    EOPayeCode eo = (EOPayeCode) createAndInsertInstance(editingContext, _EOPayeCode.ENTITY_NAME);    
		eo.setPcodCode(pcodCode);
		eo.setPcodLibelle(pcodLibelle);
    return eo;
  }

  
	  public EOPayeCode localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeCode)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeCode creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeCode creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeCode object = (EOPayeCode)createAndInsertInstance(editingContext, _EOPayeCode.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeCode localInstanceIn(EOEditingContext editingContext, EOPayeCode eo) {
    EOPayeCode localInstance = (eo == null) ? null : (EOPayeCode)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeCode#localInstanceIn a la place.
   */
	public static EOPayeCode localInstanceOf(EOEditingContext editingContext, EOPayeCode eo) {
		return EOPayeCode.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeCode fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeCode fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeCode eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeCode)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeCode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeCode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeCode eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeCode)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeCode fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeCode eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeCode ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeCode fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
