// _EOPayeRubrique.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeRubrique.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeRubrique extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeRubrique";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_rubrique";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "prubOrdre";

	public static final String PRUB_BASE_FREQUENCE_KEY = "prubBaseFrequence";
	public static final String PRUB_CLASSEMENT_KEY = "prubClassement";
	public static final String PRUB_FREQUENCE_KEY = "prubFrequence";
	public static final String PRUB_KA_ELEMENT_KEY = "prubKaElement";
	public static final String PRUB_LIBELLE_KEY = "prubLibelle";
	public static final String PRUB_LIBELLE_IMP_KEY = "prubLibelleImp";
	public static final String PRUB_MDEBUT_KEY = "prubMdebut";
	public static final String PRUB_MFIN_KEY = "prubMfin";
	public static final String PRUB_MODE_KEY = "prubMode";
	public static final String PRUB_NOMCLASSE_KEY = "prubNomclasse";
	public static final String PRUB_TYPE_KEY = "prubType";
	public static final String TEM_BASE_ASSIETTE_KEY = "temBaseAssiette";
	public static final String TEM_EST_CALCULE_KEY = "temEstCalcule";
	public static final String TEM_IMPOSABLE_KEY = "temImposable";
	public static final String TEM_IMPUTATION_BRUT_KEY = "temImputationBrut";
	public static final String TEM_MGEN_KEY = "temMgen";
	public static final String TEM_PENSION_CIVILE_KEY = "temPensionCivile";
	public static final String TEM_RAFP_KEY = "temRafp";
	public static final String TEM_RETENUE_KEY = "temRetenue";
	public static final String TEM_VALIDE_KEY = "temValide";

//Colonnes dans la base de donnees
	public static final String PRUB_BASE_FREQUENCE_COLKEY = "prub_base_Frequence";
	public static final String PRUB_CLASSEMENT_COLKEY = "prub_classement";
	public static final String PRUB_FREQUENCE_COLKEY = "prub_frequence";
	public static final String PRUB_KA_ELEMENT_COLKEY = "prub_ka_element";
	public static final String PRUB_LIBELLE_COLKEY = "prub_libelle";
	public static final String PRUB_LIBELLE_IMP_COLKEY = "prub_libelle_Imp";
	public static final String PRUB_MDEBUT_COLKEY = "prub_mdebut";
	public static final String PRUB_MFIN_COLKEY = "prub_mfin";
	public static final String PRUB_MODE_COLKEY = "prub_mode";
	public static final String PRUB_NOMCLASSE_COLKEY = "prub_nom_classe";
	public static final String PRUB_TYPE_COLKEY = "prub_type";
	public static final String TEM_BASE_ASSIETTE_COLKEY = "tem_base_Assiette";
	public static final String TEM_EST_CALCULE_COLKEY = "tem_est_calcule";
	public static final String TEM_IMPOSABLE_COLKEY = "tem_imposable";
	public static final String TEM_IMPUTATION_BRUT_COLKEY = "TEM_IMPUTATION_BRUT";
	public static final String TEM_MGEN_COLKEY = "TEM_MGEN";
	public static final String TEM_PENSION_CIVILE_COLKEY = "tem_Pension_Civile";
	public static final String TEM_RAFP_COLKEY = "TEM_RAFP";
	public static final String TEM_RETENUE_COLKEY = "TEM_RETENUE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

// Relationships
	public static final String CATEGORIE_KEY = "categorie";
	public static final String CODES_KEY = "codes";
	public static final String PAYE_RUB_PLANCOS_KEY = "payeRubPlancos";
	public static final String RUBCODES_KEY = "rubcodes";
	public static final String RUBRIQUE_RAPPEL_KEY = "rubriqueRappel";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer prubBaseFrequence() {
    return (Integer) storedValueForKey(PRUB_BASE_FREQUENCE_KEY);
  }

  public void setPrubBaseFrequence(Integer value) {
    takeStoredValueForKey(value, PRUB_BASE_FREQUENCE_KEY);
  }

  public String prubClassement() {
    return (String) storedValueForKey(PRUB_CLASSEMENT_KEY);
  }

  public void setPrubClassement(String value) {
    takeStoredValueForKey(value, PRUB_CLASSEMENT_KEY);
  }

  public String prubFrequence() {
    return (String) storedValueForKey(PRUB_FREQUENCE_KEY);
  }

  public void setPrubFrequence(String value) {
    takeStoredValueForKey(value, PRUB_FREQUENCE_KEY);
  }

  public String prubKaElement() {
    return (String) storedValueForKey(PRUB_KA_ELEMENT_KEY);
  }

  public void setPrubKaElement(String value) {
    takeStoredValueForKey(value, PRUB_KA_ELEMENT_KEY);
  }

  public String prubLibelle() {
    return (String) storedValueForKey(PRUB_LIBELLE_KEY);
  }

  public void setPrubLibelle(String value) {
    takeStoredValueForKey(value, PRUB_LIBELLE_KEY);
  }

  public String prubLibelleImp() {
    return (String) storedValueForKey(PRUB_LIBELLE_IMP_KEY);
  }

  public void setPrubLibelleImp(String value) {
    takeStoredValueForKey(value, PRUB_LIBELLE_IMP_KEY);
  }

  public Integer prubMdebut() {
    return (Integer) storedValueForKey(PRUB_MDEBUT_KEY);
  }

  public void setPrubMdebut(Integer value) {
    takeStoredValueForKey(value, PRUB_MDEBUT_KEY);
  }

  public Integer prubMfin() {
    return (Integer) storedValueForKey(PRUB_MFIN_KEY);
  }

  public void setPrubMfin(Integer value) {
    takeStoredValueForKey(value, PRUB_MFIN_KEY);
  }

  public String prubMode() {
    return (String) storedValueForKey(PRUB_MODE_KEY);
  }

  public void setPrubMode(String value) {
    takeStoredValueForKey(value, PRUB_MODE_KEY);
  }

  public String prubNomclasse() {
    return (String) storedValueForKey(PRUB_NOMCLASSE_KEY);
  }

  public void setPrubNomclasse(String value) {
    takeStoredValueForKey(value, PRUB_NOMCLASSE_KEY);
  }

  public String prubType() {
    return (String) storedValueForKey(PRUB_TYPE_KEY);
  }

  public void setPrubType(String value) {
    takeStoredValueForKey(value, PRUB_TYPE_KEY);
  }

  public String temBaseAssiette() {
    return (String) storedValueForKey(TEM_BASE_ASSIETTE_KEY);
  }

  public void setTemBaseAssiette(String value) {
    takeStoredValueForKey(value, TEM_BASE_ASSIETTE_KEY);
  }

  public String temEstCalcule() {
    return (String) storedValueForKey(TEM_EST_CALCULE_KEY);
  }

  public void setTemEstCalcule(String value) {
    takeStoredValueForKey(value, TEM_EST_CALCULE_KEY);
  }

  public String temImposable() {
    return (String) storedValueForKey(TEM_IMPOSABLE_KEY);
  }

  public void setTemImposable(String value) {
    takeStoredValueForKey(value, TEM_IMPOSABLE_KEY);
  }

  public String temImputationBrut() {
    return (String) storedValueForKey(TEM_IMPUTATION_BRUT_KEY);
  }

  public void setTemImputationBrut(String value) {
    takeStoredValueForKey(value, TEM_IMPUTATION_BRUT_KEY);
  }

  public String temMgen() {
    return (String) storedValueForKey(TEM_MGEN_KEY);
  }

  public void setTemMgen(String value) {
    takeStoredValueForKey(value, TEM_MGEN_KEY);
  }

  public String temPensionCivile() {
    return (String) storedValueForKey(TEM_PENSION_CIVILE_KEY);
  }

  public void setTemPensionCivile(String value) {
    takeStoredValueForKey(value, TEM_PENSION_CIVILE_KEY);
  }

  public String temRafp() {
    return (String) storedValueForKey(TEM_RAFP_KEY);
  }

  public void setTemRafp(String value) {
    takeStoredValueForKey(value, TEM_RAFP_KEY);
  }

  public String temRetenue() {
    return (String) storedValueForKey(TEM_RETENUE_KEY);
  }

  public void setTemRetenue(String value) {
    takeStoredValueForKey(value, TEM_RETENUE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeCategorieRubrique categorie() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeCategorieRubrique)storedValueForKey(CATEGORIE_KEY);
  }

  public void setCategorieRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeCategorieRubrique value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeCategorieRubrique oldValue = categorie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CATEGORIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CATEGORIE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique rubriqueRappel() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique)storedValueForKey(RUBRIQUE_RAPPEL_KEY);
  }

  public void setRubriqueRappelRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique oldValue = rubriqueRappel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RUBRIQUE_RAPPEL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RUBRIQUE_RAPPEL_KEY);
    }
  }
  
  public NSArray codes() {
    return (NSArray)storedValueForKey(CODES_KEY);
  }

  public NSArray codes(EOQualifier qualifier) {
    return codes(qualifier, null);
  }

  public NSArray codes(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = codes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToCodesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CODES_KEY);
  }

  public void removeFromCodesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODES_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode createCodesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeCode");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CODES_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode) eo;
  }

  public void deleteCodesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCodesRelationships() {
    Enumeration objects = codes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCodesRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode)objects.nextElement());
    }
  }

  public NSArray payeRubPlancos() {
    return (NSArray)storedValueForKey(PAYE_RUB_PLANCOS_KEY);
  }

  public NSArray payeRubPlancos(EOQualifier qualifier) {
    return payeRubPlancos(qualifier, null, false);
  }

  public NSArray payeRubPlancos(EOQualifier qualifier, boolean fetch) {
    return payeRubPlancos(qualifier, null, fetch);
  }

  public NSArray payeRubPlancos(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubPlanco.RUBRIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubPlanco.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = payeRubPlancos();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPayeRubPlancosRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubPlanco object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PAYE_RUB_PLANCOS_KEY);
  }

  public void removeFromPayeRubPlancosRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PAYE_RUB_PLANCOS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubPlanco createPayeRubPlancosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeRubPlanco");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PAYE_RUB_PLANCOS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubPlanco) eo;
  }

  public void deletePayeRubPlancosRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PAYE_RUB_PLANCOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPayeRubPlancosRelationships() {
    Enumeration objects = payeRubPlancos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePayeRubPlancosRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubPlanco)objects.nextElement());
    }
  }

  public NSArray rubcodes() {
    return (NSArray)storedValueForKey(RUBCODES_KEY);
  }

  public NSArray rubcodes(EOQualifier qualifier) {
    return rubcodes(qualifier, null, false);
  }

  public NSArray rubcodes(EOQualifier qualifier, boolean fetch) {
    return rubcodes(qualifier, null, fetch);
  }

  public NSArray rubcodes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode.RUBRIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = rubcodes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRubcodesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RUBCODES_KEY);
  }

  public void removeFromRubcodesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RUBCODES_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode createRubcodesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeRubcode");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RUBCODES_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode) eo;
  }

  public void deleteRubcodesRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RUBCODES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRubcodesRelationships() {
    Enumeration objects = rubcodes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRubcodesRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubcode)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPayeRubrique avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeRubrique createEOPayeRubrique(EOEditingContext editingContext, String prubClassement
, String prubLibelle
, String prubLibelleImp
, Integer prubMdebut
, Integer prubMfin
, String prubType
, String temBaseAssiette
, String temEstCalcule
, String temImposable
, String temImputationBrut
, String temMgen
, String temPensionCivile
, String temRafp
, String temRetenue
, String temValide
			) {
    EOPayeRubrique eo = (EOPayeRubrique) createAndInsertInstance(editingContext, _EOPayeRubrique.ENTITY_NAME);    
		eo.setPrubClassement(prubClassement);
		eo.setPrubLibelle(prubLibelle);
		eo.setPrubLibelleImp(prubLibelleImp);
		eo.setPrubMdebut(prubMdebut);
		eo.setPrubMfin(prubMfin);
		eo.setPrubType(prubType);
		eo.setTemBaseAssiette(temBaseAssiette);
		eo.setTemEstCalcule(temEstCalcule);
		eo.setTemImposable(temImposable);
		eo.setTemImputationBrut(temImputationBrut);
		eo.setTemMgen(temMgen);
		eo.setTemPensionCivile(temPensionCivile);
		eo.setTemRafp(temRafp);
		eo.setTemRetenue(temRetenue);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOPayeRubrique localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeRubrique)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeRubrique creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeRubrique creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeRubrique object = (EOPayeRubrique)createAndInsertInstance(editingContext, _EOPayeRubrique.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeRubrique localInstanceIn(EOEditingContext editingContext, EOPayeRubrique eo) {
    EOPayeRubrique localInstance = (eo == null) ? null : (EOPayeRubrique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeRubrique#localInstanceIn a la place.
   */
	public static EOPayeRubrique localInstanceOf(EOEditingContext editingContext, EOPayeRubrique eo) {
		return EOPayeRubrique.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeRubrique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeRubrique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeRubrique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeRubrique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeRubrique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeRubrique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeRubrique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeRubrique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeRubrique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeRubrique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeRubrique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeRubrique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
