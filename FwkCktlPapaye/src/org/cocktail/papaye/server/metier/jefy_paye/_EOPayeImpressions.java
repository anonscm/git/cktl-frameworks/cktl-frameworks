// _EOPayeImpressions.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeImpressions.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeImpressions extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeImpressions";
	public static final String ENTITY_TABLE_NAME = "TABLE_LOCALE";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "cle";

	public static final String C_CIVILITE_KEY = "cCivilite";
	public static final String IND_CLE_INSEE_KEY = "indCleInsee";
	public static final String IND_NO_INSEE_KEY = "indNoInsee";
	public static final String MONTANT_KEY = "montant";
	public static final String MONTANT_ARRONDI_KEY = "montantArrondi";
	public static final String NO_MATRICULE_KEY = "noMatricule";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PAYE_ADEDUIRE_KEY = "payeAdeduire";
	public static final String PAYE_BRUT_KEY = "payeBrut";
	public static final String PAYE_BRUT_TOTAL_KEY = "payeBrutTotal";
	public static final String PAYE_COUT_KEY = "payeCout";
	public static final String PAYE_NBHEURE_KEY = "payeNbheure";
	public static final String PAYE_NBJOUR_KEY = "payeNbjour";
	public static final String PAYE_NET_KEY = "payeNet";
	public static final String PAYE_PATRON_KEY = "payePatron";
	public static final String PAYE_RETENUE_KEY = "payeRetenue";
	public static final String PCOD_CODE_KEY = "pcodCode";
	public static final String PELM_ADEDUIRE_KEY = "pelmAdeduire";
	public static final String PELM_APAYER_KEY = "pelmApayer";
	public static final String PELM_ASSIETTE_KEY = "pelmAssiette";
	public static final String PELM_ASSIETTE_ARRONDIE_KEY = "pelmAssietteArrondie";
	public static final String PELM_LIBELLE_KEY = "pelmLibelle";
	public static final String PELM_MOIS_CODE_KEY = "pelmMoisCode";
	public static final String PELM_PATRON_KEY = "pelmPatron";
	public static final String PELM_TAUX_KEY = "pelmTaux";
	public static final String PELM_TAUX_STRING_KEY = "pelmTauxString";
	public static final String PELM_TYPE_KEY = "pelmType";
	public static final String PEMP_ADRESSE1_KEY = "pempAdresse1";
	public static final String PEMP_ADRESSE2_KEY = "pempAdresse2";
	public static final String PEMP_CODE_POSTAL_KEY = "pempCodePostal";
	public static final String PEMP_NOM_KEY = "pempNom";
	public static final String PEMP_SIRET_KEY = "pempSiret";
	public static final String PEMP_VILLE_KEY = "pempVille";
	public static final String PRENOM_KEY = "prenom";
	public static final String PRUB_CLASSEMENT_KEY = "prubClassement";
	public static final String PRUB_LIBELLE_KEY = "prubLibelle";
	public static final String PSTA_LIBELLE_KEY = "pstaLibelle";
	public static final String RETENUES_KEY = "retenues";
	public static final String UFR_KEY = "ufr";

//Colonnes dans la base de donnees
	public static final String C_CIVILITE_COLKEY = "C_CIVILITE";
	public static final String IND_CLE_INSEE_COLKEY = "IND_CLE_INSEE";
	public static final String IND_NO_INSEE_COLKEY = "IND_NO_INSEE";
	public static final String MONTANT_COLKEY = "pelm_apayer";
	public static final String MONTANT_ARRONDI_COLKEY = "pelm_apayer";
	public static final String NO_MATRICULE_COLKEY = "NO_MATRICULE";
	public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
	public static final String PAYE_ADEDUIRE_COLKEY = "paye_adeduire";
	public static final String PAYE_BRUT_COLKEY = "paye_brut";
	public static final String PAYE_BRUT_TOTAL_COLKEY = "paye_brut_total";
	public static final String PAYE_COUT_COLKEY = "paye_cout";
	public static final String PAYE_NBHEURE_COLKEY = "paye_nbheure";
	public static final String PAYE_NBJOUR_COLKEY = "paye_nb_jours";
	public static final String PAYE_NET_COLKEY = "paye_net";
	public static final String PAYE_PATRON_COLKEY = "paye_patron";
	public static final String PAYE_RETENUE_COLKEY = "paye_retenue";
	public static final String PCOD_CODE_COLKEY = "pcod_code";
	public static final String PELM_ADEDUIRE_COLKEY = "pelm_adeduire";
	public static final String PELM_APAYER_COLKEY = "pelm_apayer";
	public static final String PELM_ASSIETTE_COLKEY = "pelm_assiette";
	public static final String PELM_ASSIETTE_ARRONDIE_COLKEY = "pelm_assiette";
	public static final String PELM_LIBELLE_COLKEY = "pelm_libelle";
	public static final String PELM_MOIS_CODE_COLKEY = "mois_code";
	public static final String PELM_PATRON_COLKEY = "pelm_patron";
	public static final String PELM_TAUX_COLKEY = "pelm_taux";
	public static final String PELM_TAUX_STRING_COLKEY = "pelm_taux_string";
	public static final String PELM_TYPE_COLKEY = "pelm_type";
	public static final String PEMP_ADRESSE1_COLKEY = "pemp_adresse1";
	public static final String PEMP_ADRESSE2_COLKEY = "pemp_adresse2";
	public static final String PEMP_CODE_POSTAL_COLKEY = "pemp_code_postal";
	public static final String PEMP_NOM_COLKEY = "pemp_nom";
	public static final String PEMP_SIRET_COLKEY = "pemp_siret";
	public static final String PEMP_VILLE_COLKEY = "pemp_ville";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String PRUB_CLASSEMENT_COLKEY = "prub_classement";
	public static final String PRUB_LIBELLE_COLKEY = "prub_libelle";
	public static final String PSTA_LIBELLE_COLKEY = "psta_libelle";
	public static final String RETENUES_COLKEY = "pelm_apayer";
	public static final String UFR_COLKEY = "ORG_COMP";

// Relationships



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cCivilite() {
    return (String) storedValueForKey(C_CIVILITE_KEY);
  }

  public void setCCivilite(String value) {
    takeStoredValueForKey(value, C_CIVILITE_KEY);
  }

  public Integer indCleInsee() {
    return (Integer) storedValueForKey(IND_CLE_INSEE_KEY);
  }

  public void setIndCleInsee(Integer value) {
    takeStoredValueForKey(value, IND_CLE_INSEE_KEY);
  }

  public String indNoInsee() {
    return (String) storedValueForKey(IND_NO_INSEE_KEY);
  }

  public void setIndNoInsee(String value) {
    takeStoredValueForKey(value, IND_NO_INSEE_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_KEY);
  }

  public java.math.BigDecimal montantArrondi() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_ARRONDI_KEY);
  }

  public void setMontantArrondi(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_ARRONDI_KEY);
  }

  public String noMatricule() {
    return (String) storedValueForKey(NO_MATRICULE_KEY);
  }

  public void setNoMatricule(String value) {
    takeStoredValueForKey(value, NO_MATRICULE_KEY);
  }

  public String nomUsuel() {
    return (String) storedValueForKey(NOM_USUEL_KEY);
  }

  public void setNomUsuel(String value) {
    takeStoredValueForKey(value, NOM_USUEL_KEY);
  }

  public java.math.BigDecimal payeAdeduire() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_ADEDUIRE_KEY);
  }

  public void setPayeAdeduire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_ADEDUIRE_KEY);
  }

  public java.math.BigDecimal payeBrut() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_BRUT_KEY);
  }

  public void setPayeBrut(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_BRUT_KEY);
  }

  public java.math.BigDecimal payeBrutTotal() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_BRUT_TOTAL_KEY);
  }

  public void setPayeBrutTotal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_BRUT_TOTAL_KEY);
  }

  public java.math.BigDecimal payeCout() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_COUT_KEY);
  }

  public void setPayeCout(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_COUT_KEY);
  }

  public Double payeNbheure() {
    return (Double) storedValueForKey(PAYE_NBHEURE_KEY);
  }

  public void setPayeNbheure(Double value) {
    takeStoredValueForKey(value, PAYE_NBHEURE_KEY);
  }

  public Double payeNbjour() {
    return (Double) storedValueForKey(PAYE_NBJOUR_KEY);
  }

  public void setPayeNbjour(Double value) {
    takeStoredValueForKey(value, PAYE_NBJOUR_KEY);
  }

  public java.math.BigDecimal payeNet() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_NET_KEY);
  }

  public void setPayeNet(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_NET_KEY);
  }

  public java.math.BigDecimal payePatron() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_PATRON_KEY);
  }

  public void setPayePatron(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_PATRON_KEY);
  }

  public Double payeRetenue() {
    return (Double) storedValueForKey(PAYE_RETENUE_KEY);
  }

  public void setPayeRetenue(Double value) {
    takeStoredValueForKey(value, PAYE_RETENUE_KEY);
  }

  public String pcodCode() {
    return (String) storedValueForKey(PCOD_CODE_KEY);
  }

  public void setPcodCode(String value) {
    takeStoredValueForKey(value, PCOD_CODE_KEY);
  }

  public java.math.BigDecimal pelmAdeduire() {
    return (java.math.BigDecimal) storedValueForKey(PELM_ADEDUIRE_KEY);
  }

  public void setPelmAdeduire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PELM_ADEDUIRE_KEY);
  }

  public java.math.BigDecimal pelmApayer() {
    return (java.math.BigDecimal) storedValueForKey(PELM_APAYER_KEY);
  }

  public void setPelmApayer(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PELM_APAYER_KEY);
  }

  public java.math.BigDecimal pelmAssiette() {
    return (java.math.BigDecimal) storedValueForKey(PELM_ASSIETTE_KEY);
  }

  public void setPelmAssiette(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PELM_ASSIETTE_KEY);
  }

  public java.math.BigDecimal pelmAssietteArrondie() {
    return (java.math.BigDecimal) storedValueForKey(PELM_ASSIETTE_ARRONDIE_KEY);
  }

  public void setPelmAssietteArrondie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PELM_ASSIETTE_ARRONDIE_KEY);
  }

  public String pelmLibelle() {
    return (String) storedValueForKey(PELM_LIBELLE_KEY);
  }

  public void setPelmLibelle(String value) {
    takeStoredValueForKey(value, PELM_LIBELLE_KEY);
  }

  public Integer pelmMoisCode() {
    return (Integer) storedValueForKey(PELM_MOIS_CODE_KEY);
  }

  public void setPelmMoisCode(Integer value) {
    takeStoredValueForKey(value, PELM_MOIS_CODE_KEY);
  }

  public java.math.BigDecimal pelmPatron() {
    return (java.math.BigDecimal) storedValueForKey(PELM_PATRON_KEY);
  }

  public void setPelmPatron(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PELM_PATRON_KEY);
  }

  public Double pelmTaux() {
    return (Double) storedValueForKey(PELM_TAUX_KEY);
  }

  public void setPelmTaux(Double value) {
    takeStoredValueForKey(value, PELM_TAUX_KEY);
  }

  public String pelmTauxString() {
    return (String) storedValueForKey(PELM_TAUX_STRING_KEY);
  }

  public void setPelmTauxString(String value) {
    takeStoredValueForKey(value, PELM_TAUX_STRING_KEY);
  }

  public String pelmType() {
    return (String) storedValueForKey(PELM_TYPE_KEY);
  }

  public void setPelmType(String value) {
    takeStoredValueForKey(value, PELM_TYPE_KEY);
  }

  public String pempAdresse1() {
    return (String) storedValueForKey(PEMP_ADRESSE1_KEY);
  }

  public void setPempAdresse1(String value) {
    takeStoredValueForKey(value, PEMP_ADRESSE1_KEY);
  }

  public String pempAdresse2() {
    return (String) storedValueForKey(PEMP_ADRESSE2_KEY);
  }

  public void setPempAdresse2(String value) {
    takeStoredValueForKey(value, PEMP_ADRESSE2_KEY);
  }

  public String pempCodePostal() {
    return (String) storedValueForKey(PEMP_CODE_POSTAL_KEY);
  }

  public void setPempCodePostal(String value) {
    takeStoredValueForKey(value, PEMP_CODE_POSTAL_KEY);
  }

  public String pempNom() {
    return (String) storedValueForKey(PEMP_NOM_KEY);
  }

  public void setPempNom(String value) {
    takeStoredValueForKey(value, PEMP_NOM_KEY);
  }

  public String pempSiret() {
    return (String) storedValueForKey(PEMP_SIRET_KEY);
  }

  public void setPempSiret(String value) {
    takeStoredValueForKey(value, PEMP_SIRET_KEY);
  }

  public String pempVille() {
    return (String) storedValueForKey(PEMP_VILLE_KEY);
  }

  public void setPempVille(String value) {
    takeStoredValueForKey(value, PEMP_VILLE_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(PRENOM_KEY);
  }

  public void setPrenom(String value) {
    takeStoredValueForKey(value, PRENOM_KEY);
  }

  public String prubClassement() {
    return (String) storedValueForKey(PRUB_CLASSEMENT_KEY);
  }

  public void setPrubClassement(String value) {
    takeStoredValueForKey(value, PRUB_CLASSEMENT_KEY);
  }

  public String prubLibelle() {
    return (String) storedValueForKey(PRUB_LIBELLE_KEY);
  }

  public void setPrubLibelle(String value) {
    takeStoredValueForKey(value, PRUB_LIBELLE_KEY);
  }

  public String pstaLibelle() {
    return (String) storedValueForKey(PSTA_LIBELLE_KEY);
  }

  public void setPstaLibelle(String value) {
    takeStoredValueForKey(value, PSTA_LIBELLE_KEY);
  }

  public java.math.BigDecimal retenues() {
    return (java.math.BigDecimal) storedValueForKey(RETENUES_KEY);
  }

  public void setRetenues(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RETENUES_KEY);
  }

  public String ufr() {
    return (String) storedValueForKey(UFR_KEY);
  }

  public void setUfr(String value) {
    takeStoredValueForKey(value, UFR_KEY);
  }


/**
 * Créer une instance de EOPayeImpressions avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeImpressions createEOPayeImpressions(EOEditingContext editingContext, String cCivilite
, java.math.BigDecimal montant
, java.math.BigDecimal montantArrondi
, String noMatricule
, java.math.BigDecimal payeAdeduire
, java.math.BigDecimal payeBrut
, java.math.BigDecimal payeBrutTotal
, java.math.BigDecimal payeCout
, Double payeNbheure
, Double payeNbjour
, java.math.BigDecimal payeNet
, java.math.BigDecimal payePatron
, String pcodCode
, java.math.BigDecimal pelmAdeduire
, java.math.BigDecimal pelmApayer
, java.math.BigDecimal pelmAssiette
, java.math.BigDecimal pelmAssietteArrondie
, String pelmLibelle
, Integer pelmMoisCode
, java.math.BigDecimal pelmPatron
, Double pelmTaux
, String pelmTauxString
, String pelmType
, String pempNom
, String prubClassement
, String prubLibelle
, String pstaLibelle
, java.math.BigDecimal retenues
			) {
    EOPayeImpressions eo = (EOPayeImpressions) createAndInsertInstance(editingContext, _EOPayeImpressions.ENTITY_NAME);    
		eo.setCCivilite(cCivilite);
		eo.setMontant(montant);
		eo.setMontantArrondi(montantArrondi);
		eo.setNoMatricule(noMatricule);
		eo.setPayeAdeduire(payeAdeduire);
		eo.setPayeBrut(payeBrut);
		eo.setPayeBrutTotal(payeBrutTotal);
		eo.setPayeCout(payeCout);
		eo.setPayeNbheure(payeNbheure);
		eo.setPayeNbjour(payeNbjour);
		eo.setPayeNet(payeNet);
		eo.setPayePatron(payePatron);
		eo.setPcodCode(pcodCode);
		eo.setPelmAdeduire(pelmAdeduire);
		eo.setPelmApayer(pelmApayer);
		eo.setPelmAssiette(pelmAssiette);
		eo.setPelmAssietteArrondie(pelmAssietteArrondie);
		eo.setPelmLibelle(pelmLibelle);
		eo.setPelmMoisCode(pelmMoisCode);
		eo.setPelmPatron(pelmPatron);
		eo.setPelmTaux(pelmTaux);
		eo.setPelmTauxString(pelmTauxString);
		eo.setPelmType(pelmType);
		eo.setPempNom(pempNom);
		eo.setPrubClassement(prubClassement);
		eo.setPrubLibelle(prubLibelle);
		eo.setPstaLibelle(pstaLibelle);
		eo.setRetenues(retenues);
    return eo;
  }

  
	  public EOPayeImpressions localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeImpressions)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeImpressions creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeImpressions creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeImpressions object = (EOPayeImpressions)createAndInsertInstance(editingContext, _EOPayeImpressions.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeImpressions localInstanceIn(EOEditingContext editingContext, EOPayeImpressions eo) {
    EOPayeImpressions localInstance = (eo == null) ? null : (EOPayeImpressions)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeImpressions#localInstanceIn a la place.
   */
	public static EOPayeImpressions localInstanceOf(EOEditingContext editingContext, EOPayeImpressions eo) {
		return EOPayeImpressions.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeImpressions fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeImpressions fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeImpressions eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeImpressions)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeImpressions fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeImpressions fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeImpressions eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeImpressions)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeImpressions fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeImpressions eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeImpressions ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeImpressions fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
