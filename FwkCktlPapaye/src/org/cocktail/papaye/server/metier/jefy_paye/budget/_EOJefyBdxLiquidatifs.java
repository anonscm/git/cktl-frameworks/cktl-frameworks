// _EOJefyBdxLiquidatifs.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJefyBdxLiquidatifs.java instead.
package org.cocktail.papaye.server.metier.jefy_paye.budget;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOJefyBdxLiquidatifs extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "JefyBdxLiquidatifs";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.JEFY_BDX_LIQUIDATIFS";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "blOrdre";

	public static final String BL_BRUT_KEY = "blBrut";
	public static final String BL_BRUT_TOTAL_KEY = "blBrutTotal";
	public static final String BL_COUT_KEY = "blCout";
	public static final String BL_DATE_DEBUT_KEY = "blDateDebut";
	public static final String BL_DATE_FIN_KEY = "blDateFin";
	public static final String BL_HEURES_KEY = "blHeures";
	public static final String BL_INDICE_KEY = "blIndice";
	public static final String BL_NET_KEY = "blNet";
	public static final String BL_OBSERVATIONS_KEY = "blObservations";
	public static final String BL_PATRONAL_KEY = "blPatronal";
	public static final String BL_QUOTITE_KEY = "blQuotite";
	public static final String BL_QUOTITE_PAIEMENT_KEY = "blQuotitePaiement";
	public static final String BL_RETENUE_KEY = "blRetenue";
	public static final String BL_SALARIAL_KEY = "blSalarial";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String PAYE_ORDRE_KEY = "payeOrdre";
	public static final String TEM_COMPTA_KEY = "temCompta";

//Colonnes dans la base de donnees
	public static final String BL_BRUT_COLKEY = "BL_BRUT";
	public static final String BL_BRUT_TOTAL_COLKEY = "BL_BRUT_TOTAL";
	public static final String BL_COUT_COLKEY = "BL_COUT";
	public static final String BL_DATE_DEBUT_COLKEY = "BL_DATE_DEBUT";
	public static final String BL_DATE_FIN_COLKEY = "BL_DATE_FIN";
	public static final String BL_HEURES_COLKEY = "BL_HEURES";
	public static final String BL_INDICE_COLKEY = "BL_INDICE";
	public static final String BL_NET_COLKEY = "BL_NET";
	public static final String BL_OBSERVATIONS_COLKEY = "BL_OBSERVATIONS";
	public static final String BL_PATRONAL_COLKEY = "BL_PATRONAL";
	public static final String BL_QUOTITE_COLKEY = "BL_QUOTITE";
	public static final String BL_QUOTITE_PAIEMENT_COLKEY = "BL_QUOTITE_PAIEMENT";
	public static final String BL_RETENUE_COLKEY = "BL_RETENUE";
	public static final String BL_SALARIAL_COLKEY = "BL_SALARIAL";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String PAYE_ORDRE_COLKEY = "paye_ordre";
	public static final String TEM_COMPTA_COLKEY = "tem_compta";

// Relationships
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String CONVENTION_KEY = "convention";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FONCTION_KEY = "fonction";
	public static final String INDIVIDU_KEY = "individu";
	public static final String MOIS_KEY = "mois";
	public static final String ORGAN_KEY = "organ";
	public static final String SECTEUR_KEY = "secteur";
	public static final String STATUT_KEY = "statut";
	public static final String STRUCTURE_KEY = "structure";
	public static final String STRUCTURE_SIRET_KEY = "structureSiret";
	public static final String TYPE_ACTION_KEY = "typeAction";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal blBrut() {
    return (java.math.BigDecimal) storedValueForKey(BL_BRUT_KEY);
  }

  public void setBlBrut(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_BRUT_KEY);
  }

  public java.math.BigDecimal blBrutTotal() {
    return (java.math.BigDecimal) storedValueForKey(BL_BRUT_TOTAL_KEY);
  }

  public void setBlBrutTotal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_BRUT_TOTAL_KEY);
  }

  public java.math.BigDecimal blCout() {
    return (java.math.BigDecimal) storedValueForKey(BL_COUT_KEY);
  }

  public void setBlCout(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_COUT_KEY);
  }

  public NSTimestamp blDateDebut() {
    return (NSTimestamp) storedValueForKey(BL_DATE_DEBUT_KEY);
  }

  public void setBlDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, BL_DATE_DEBUT_KEY);
  }

  public NSTimestamp blDateFin() {
    return (NSTimestamp) storedValueForKey(BL_DATE_FIN_KEY);
  }

  public void setBlDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, BL_DATE_FIN_KEY);
  }

  public Double blHeures() {
    return (Double) storedValueForKey(BL_HEURES_KEY);
  }

  public void setBlHeures(Double value) {
    takeStoredValueForKey(value, BL_HEURES_KEY);
  }

  public String blIndice() {
    return (String) storedValueForKey(BL_INDICE_KEY);
  }

  public void setBlIndice(String value) {
    takeStoredValueForKey(value, BL_INDICE_KEY);
  }

  public java.math.BigDecimal blNet() {
    return (java.math.BigDecimal) storedValueForKey(BL_NET_KEY);
  }

  public void setBlNet(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_NET_KEY);
  }

  public String blObservations() {
    return (String) storedValueForKey(BL_OBSERVATIONS_KEY);
  }

  public void setBlObservations(String value) {
    takeStoredValueForKey(value, BL_OBSERVATIONS_KEY);
  }

  public java.math.BigDecimal blPatronal() {
    return (java.math.BigDecimal) storedValueForKey(BL_PATRONAL_KEY);
  }

  public void setBlPatronal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_PATRONAL_KEY);
  }

  public java.math.BigDecimal blQuotite() {
    return (java.math.BigDecimal) storedValueForKey(BL_QUOTITE_KEY);
  }

  public void setBlQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_QUOTITE_KEY);
  }

  public Double blQuotitePaiement() {
    return (Double) storedValueForKey(BL_QUOTITE_PAIEMENT_KEY);
  }

  public void setBlQuotitePaiement(Double value) {
    takeStoredValueForKey(value, BL_QUOTITE_PAIEMENT_KEY);
  }

  public java.math.BigDecimal blRetenue() {
    return (java.math.BigDecimal) storedValueForKey(BL_RETENUE_KEY);
  }

  public void setBlRetenue(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_RETENUE_KEY);
  }

  public java.math.BigDecimal blSalarial() {
    return (java.math.BigDecimal) storedValueForKey(BL_SALARIAL_KEY);
  }

  public void setBlSalarial(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_SALARIAL_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public Integer payeOrdre() {
    return (Integer) storedValueForKey(PAYE_ORDRE_KEY);
  }

  public void setPayeOrdre(Integer value) {
    takeStoredValueForKey(value, PAYE_ORDRE_KEY);
  }

  public String temCompta() {
    return (String) storedValueForKey(TEM_COMPTA_KEY);
  }

  public void setTemCompta(String value) {
    takeStoredValueForKey(value, TEM_COMPTA_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytique oldValue = codeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.convention.EOConvention convention() {
    return (org.cocktail.papaye.server.metier.convention.EOConvention)storedValueForKey(CONVENTION_KEY);
  }

  public void setConventionRelationship(org.cocktail.papaye.server.metier.convention.EOConvention value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.convention.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOExercice exercice() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOExercice value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeFonction fonction() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeFonction)storedValueForKey(FONCTION_KEY);
  }

  public void setFonctionRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeFonction value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeFonction oldValue = fonction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FONCTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FONCTION_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOIndividu individu() {
    return (org.cocktail.papaye.server.metier.grhum.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.papaye.server.metier.grhum.EOIndividu value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOOrgan organ() {
    return (org.cocktail.application.serveur.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.serveur.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur secteur() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur)storedValueForKey(SECTEUR_KEY);
  }

  public void setSecteurRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur oldValue = secteur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SECTEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SECTEUR_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut statut() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut)storedValueForKey(STATUT_KEY);
  }

  public void setStatutRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut oldValue = statut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STATUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STATUT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOStructure structure() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_KEY);
  }

  public void setStructureRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOStructure structureSiret() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_SIRET_KEY);
  }

  public void setStructureSiretRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structureSiret();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_SIRET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_SIRET_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOTypeAction typeAction() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOTypeAction)storedValueForKey(TYPE_ACTION_KEY);
  }

  public void setTypeActionRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOTypeAction value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOTypeAction oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACTION_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOTypeCredit typeCredit() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  

/**
 * Créer une instance de EOJefyBdxLiquidatifs avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOJefyBdxLiquidatifs createEOJefyBdxLiquidatifs(EOEditingContext editingContext, java.math.BigDecimal blBrut
, java.math.BigDecimal blBrutTotal
, java.math.BigDecimal blCout
, java.math.BigDecimal blNet
, java.math.BigDecimal blPatronal
, java.math.BigDecimal blRetenue
, java.math.BigDecimal blSalarial
, Integer payeOrdre
			) {
    EOJefyBdxLiquidatifs eo = (EOJefyBdxLiquidatifs) createAndInsertInstance(editingContext, _EOJefyBdxLiquidatifs.ENTITY_NAME);    
		eo.setBlBrut(blBrut);
		eo.setBlBrutTotal(blBrutTotal);
		eo.setBlCout(blCout);
		eo.setBlNet(blNet);
		eo.setBlPatronal(blPatronal);
		eo.setBlRetenue(blRetenue);
		eo.setBlSalarial(blSalarial);
		eo.setPayeOrdre(payeOrdre);
    return eo;
  }

  
	  public EOJefyBdxLiquidatifs localInstanceIn(EOEditingContext editingContext) {
	  		return (EOJefyBdxLiquidatifs)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyBdxLiquidatifs creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyBdxLiquidatifs creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOJefyBdxLiquidatifs object = (EOJefyBdxLiquidatifs)createAndInsertInstance(editingContext, _EOJefyBdxLiquidatifs.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOJefyBdxLiquidatifs localInstanceIn(EOEditingContext editingContext, EOJefyBdxLiquidatifs eo) {
    EOJefyBdxLiquidatifs localInstance = (eo == null) ? null : (EOJefyBdxLiquidatifs)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOJefyBdxLiquidatifs#localInstanceIn a la place.
   */
	public static EOJefyBdxLiquidatifs localInstanceOf(EOEditingContext editingContext, EOJefyBdxLiquidatifs eo) {
		return EOJefyBdxLiquidatifs.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOJefyBdxLiquidatifs fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOJefyBdxLiquidatifs fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJefyBdxLiquidatifs eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJefyBdxLiquidatifs)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJefyBdxLiquidatifs fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJefyBdxLiquidatifs fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJefyBdxLiquidatifs eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJefyBdxLiquidatifs)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOJefyBdxLiquidatifs fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJefyBdxLiquidatifs eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJefyBdxLiquidatifs ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJefyBdxLiquidatifs fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
