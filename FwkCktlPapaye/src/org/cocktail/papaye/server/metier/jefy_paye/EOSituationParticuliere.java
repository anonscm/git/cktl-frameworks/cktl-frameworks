/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
//Created on Thu Dec 09 16:41:27 Europe/Paris 2004 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.common.DateCtrl;
import org.cocktail.papaye.server.metier.grhum.EOIndividu;
import org.cocktail.papaye.server.n4ds.ConstantesDads;
import org.cocktail.papaye.server.n4ds.PeriodeDadsActive;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;


public class EOSituationParticuliere extends _EOSituationParticuliere {
	private String codeTypeMontant;
	private int anneeDADS;
	private String uniteTempsArret;
	private NSTimestamp dateMaximum = null;
	private static String PERIODE_ANTERIEURE = "01"; // période d'inactivité antérieure à la période de référence de la déclaration
	private static String UNITE_HEURE = "01";
	private static String UNITE_JOUR = "03";
	public EOSituationParticuliere() {
		super();
	}


	// méthodes ajoutées
	public String codeMotif() {
		if (motifSituation() != null) {
			return motifSituation().pparValeur();
		} else {
			return null;
		}
	}
	public Number tauxRemuneration() throws Exception {
		if (codeTypeMontant() != null && codeTypeMontant().equals(ConstantesDads.SITUATION_PARTICULIERE_CNRACL)) {
			if (tauxRemunPourCotis() != null) {
				return new Integer((int)(tauxRemunPourCotis().floatValue() * 100));
			} else {
				if (montant2() != null) {		//  Verrue à supprimer pour gérer Papaye Actuel
					return new Integer((int)(montant2().floatValue() * 100));
				} else {
					throw new Exception("Pour la CNRACL, le taux de la situation particulière doit être fourni");
				}
			}
		} else {
			return null;
		}
	}
	/** retourne le montant de perte d'assiette pour des periodes de duree superieure a 30 jours. Dans le cas des conges
	 * parentaux, retourne 0 */
	public BigDecimal montantPerteAssiette() throws Exception {
		if (codeTypeMontant() != null && codeTypeMontant().equals(ConstantesDads.SITUATION_PARTICULIERE_IRCANTEC)) {
			if (DateCtrl.nbJoursEntre(dateDebut(),dateFin(),true) >= 30) {
				String codeMotif = codeMotif();
				if (codeMotif != null && (codeMotif.equals(ConstantesDads.TYPE_SITUATION_PARTICULIERE_CONGE_PARENTAL) || codeMotif.equals(ConstantesDads.TYPE_SITUATION_PARTICULIERE_PRESENCE_PARENTALE))) {
					return new BigDecimal(0.00);
				}
				if (montant1() == null) {
					throw new Exception("Pour l'Ircantec, le montant de la situation particulière doit être fourni");
				}
				return montant1();
			} else {
				return new BigDecimal(0.00);
			}
		} else {
			return null;
		}
	}
	public String uniteTempsArret() {
		return uniteTempsArret;
	}
	public String codeTypeMontant() {
		return codeTypeMontant;
	}
	public void setCodeTypeMontant(String unCode) {
		codeTypeMontant = unCode;
	}
	/** retourne le debut de periode de la situation particuli&egrave;re par rapport a l'annee de declaration de la DADS */
	public String debutPeriode() {
		return formaterDate(dateDebut());
	}
	/** retourne le debut de periode de la situation particuli&egrave;re par rapport a l'annee de declaration de la DADS */
	public String finPeriode() {
		if (dateMaximum != null && DateCtrl.isBefore(dateMaximum,dateFin())) {
			return formaterDate(dateMaximum);
		} else {
			return formaterDate(dateFin());
		}
	}
	/** retourne la duree ecoule entre le debut et la fin de la periode x 100 dans l'unite de temps voulue */
	public Number dureeArret() {
		if (uniteTempsArret() == null) {
			return null;
		} else {
			int duree = DateCtrl.nbJoursEntre(dateDebut(),dateFin(),true);
			if (uniteTempsArret().equals(UNITE_HEURE)) {
				duree = duree * 8;
			}
			duree = duree * 100;
			return new Integer(duree);
		}
	}
	public String codeSituationParticuliere() throws Exception {
		String codeSituationParticuliere = null;
		NSTimestamp debutAnnee = DateCtrl.stringToDate("01/01/" + anneeDADS);
		if (DateCtrl.isBefore(dateDebut(),debutAnnee)) {
			codeSituationParticuliere = PERIODE_ANTERIEURE;
		}
		return codeSituationParticuliere;
	}
	/** Une situation particuli&egrave;re est valide pour un ensemble de periodes si toute la situation est comprise dans 
	 * l'intervalle couvert pas les periodes et qu'il n'y a pas de discontinuite dans les periodes et que
	 * les regime de complementaires sont les m&ecirc;mes ou sont compatibles. 
	 * A ces conditions et si les motifs sont compatibles avec le regime de cotisation alors la situation est valide.
	 * @param periodes tableau de p&aeacute;riodes d'activite
	 * @return
	 */
	public boolean validePourPeriodes(NSArray periodes,int anneeDads) throws Exception {
		if (DateCtrl.isBefore(dateFin(),dateDebut())) {
			throw new Exception("La date de début d'une période de situation particulière ne peut être postérieure à la date de fin");
		}
		this.anneeDADS = anneeDads;
		// Déterminer la date de début et de fin des périodes, en vérifiant si elles sont contigües ou non
		PeriodeDadsActive periodeDebut = (PeriodeDadsActive)periodes.objectAtIndex(0); // Il y en a forcément une, le cas sans période est traité par l'appelant
		NSTimestamp dateDebutCourante = periodeDebut.dateDebut();	 
		NSTimestamp dateFinCourante =  periodeDebut.dateFin();
		String complementaire = periodeDebut.contrat().statut().regimeCotisation().pregComplementaire();
		if (complementaire == null) {
			throw new Exception("Pas de régime complémentaire pour le statut " + periodeDebut.contrat().statut().pstaLibelle());
		}
		for (int i = 1; i < periodes.count(); i++) {
			PeriodeDadsActive periodeCourante = (PeriodeDadsActive)periodes.objectAtIndex(i);
			// Si la date de fin courante est avant le début de la période et ne correspond pas au jour suivant => il y a un trou
			if (dateFinCourante != null && DateCtrl.isBefore(dateFinCourante, periodeCourante.dateDebut()) && 
				DateCtrl.dateToString(DateCtrl.jourSuivant(dateFinCourante)).equals(periodeCourante.dateDebut()) == false) {
				throw new Exception("Pas de continuité des périodes d'activité pour la situation particulière du " + 
						DateCtrl.dateToString(dateDebut()) + " au " + DateCtrl.dateToString(dateFin()));
			}
			if (dateFinCourante != null && periodeCourante.dateFin() != null && DateCtrl.isBefore(dateFinCourante, periodeCourante.dateFin())) {
				dateFinCourante = periodeCourante.dateFin();
			}
			// Vérifier que les régimes complémentaires sont compatibles
			String complementaireCourant = periodeCourante.contrat().statut().regimeCotisation().pregComplementaire();
			if (complementaireCourant == null) {
				throw new Exception("Pas de régime complémentaire pour le statut " + periodeCourante.contrat().statut().pstaLibelle());
			}
			if (complementaireCourant.equals(complementaire) || estCompatible(complementaireCourant, complementaire)) {
				complementaire = complementaireCourant;
			} else {
				throw new Exception("Les régimes complémentaires des périodes d'activité pour la situation particulière du " +
						DateCtrl.dateToString(dateDebut()) + " au " + DateCtrl.dateToString(dateFin()) + " ne sont pas compatibles. "+
						"Définissez 2 situations particulières correspondant aux dates de contrat");
			}
		}
		// Vérifier si la situation particulière est contenue sur toute la période en limitant à la fin
		// et en vérifiant qu'au début, c'est OK
		if (dateFinCourante != null && DateCtrl.isBefore(dateFinCourante,dateFin())) {
			// Limiter la date fin de la situation particulière à la fin de la période d'activité pour ne pas dépasser
			dateMaximum = dateFinCourante;
		}
		if (DateCtrl.isBefore(dateDebut(),dateDebutCourante)) {
			// Commence avant donc non incluse
			throw new Exception("La situation particulière commençant le " +
					DateCtrl.dateToString(dateDebut()) + " n'est pas compatible avec les dates des contrats. "+
					"Modifiez la date début de cette situation");
		}
		// vérifier le régime de cotisation
		if (complementaire.equals(Constantes.IRCANTEC)) {
			setCodeTypeMontant(ConstantesDads.SITUATION_PARTICULIERE_IRCANTEC);
		} else if (complementaire.equals(Constantes.CNRACL)) {
			setCodeTypeMontant(ConstantesDads.SITUATION_PARTICULIERE_CNRACL);
		} else if (complementaire.equals(Constantes.CAPRICAS) || complementaire.equals(Constantes.CARCICAS)) {
			if (periodeDebut.contrat().nbHeuresContrat() != null && periodeDebut.contrat().nbHeuresContrat().intValue() > 0) {
				uniteTempsArret = UNITE_HEURE;
			} else {
				uniteTempsArret = UNITE_JOUR;
			}
		} else {
			uniteTempsArret = UNITE_JOUR;	// Pour les autres complémentaires, on fournit la S46.G01.02
		}
		if (verifierMotif(complementaire)) {
			return true;
		} else {
			throw new Exception("Le motif " + codeMotif() + " n'est pas valable pour le régime de complémentaire de la période d'activité de la situation particulière du " +
					DateCtrl.dateToString(dateDebut()) + " au " + DateCtrl.dateToString(dateFin()));
		}
	}
	// méthodes statiques
	/** recherche les situations partirculières d'un individu 
	 * @param editingContext editing context dans lequel effectuer le fetch
	 * @parm individu concerne
	 * @param anne concernee
	 * */
	public static NSArray situationsParticulieresPourIndividu(EOEditingContext editingContext,EOIndividu individu,int annee) {
		NSMutableArray values = new NSMutableArray(individu);
		NSTimestamp dateDebut = new NSTimestamp(annee,1,1,0,0,0,NSTimeZone.timeZoneWithName("Europe/Paris", false));
		values.addObject(dateDebut);
		NSTimestamp dateFin = new NSTimestamp(annee,12,31,0,0,0,NSTimeZone.timeZoneWithName("Europe/Paris", false));
		values.addObject(dateFin);
		values.addObject(dateDebut);
		values.addObject(dateFin);
		NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareAscending));
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@ AND ((dateFin >= %@ AND dateFin <= %@) OR (dateDebut >= %@ AND dateDebut <= %@))",values);
		EOFetchSpecification fs = new EOFetchSpecification("SituationParticuliere",qualifier,sorts);
		return editingContext.objectsWithFetchSpecification(fs);
	}

	// méthodes privées
	private String formaterDate(NSTimestamp aDate) {
		GregorianCalendar date = new GregorianCalendar();
		date.setTime(aDate);
		int annee = date.get(Calendar.YEAR);
		String mois = new Integer(date.get(Calendar.MONTH) + 1).toString();	// les mois commencent à zéro en java
		String jour = new Integer(date.get(Calendar.DAY_OF_MONTH)).toString();
		if (mois.length() == 1) {
			mois = "0" + mois;
		}
		if (jour.length() == 1) {
			jour = "0" + jour;
		}
		return jour + mois + annee;
	}

	// vérifie que le motif est bien une des valeurs du tableau de chaînes ou se trouve entre les valeurs min et max
	// lance une exception si le motif ne correspond pas à un motif attendu
	private boolean verifierMotif(String regimeComplementaire) throws Exception {
		if (motifSituation() == null) {
			throw new Exception("Vous devez fournir un motif de situation particulière");
		}
		if (regimeComplementaire.equals(Constantes.RAFP) || regimeComplementaire.equals(Constantes.SANS_COMPLEMENTAIRE)) {
			return true;
		} else {
			// Vérifier si le motif est dans la table des motifs
			NSMutableArray args = new NSMutableArray(regimeComplementaire);
			args.addObject(codeMotif());
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("motComplementaire = %@ AND paramDads.pparValeur = %@ AND temValide = '" + Constantes.VRAI + "'", args);
			EOFetchSpecification fs = new EOFetchSpecification("MotifSituationPartComp",qualifier,null);
			return editingContext().objectsWithFetchSpecification(fs).count() > 0;
		}
	}
	private boolean estCompatible(String complementaire1,String complementaire2) {
		return ((complementaire1.equals(Constantes.CAPRICAS) || complementaire1.equals(Constantes.CARCICAS) || 
				complementaire1.equals(Constantes.RAFP) || complementaire1.equals(Constantes.SANS_COMPLEMENTAIRE)) && 
				(complementaire2.equals(Constantes.CAPRICAS) || complementaire2.equals(Constantes.CARCICAS) || 
						complementaire2.equals(Constantes.RAFP) || complementaire2.equals(Constantes.SANS_COMPLEMENTAIRE)));
	}
}
