// _EORegimeCotisation.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORegimeCotisation.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EORegimeCotisation extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "RegimeCotisation";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_statut_regime_n4ds";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pregOrdre";

	public static final String PREG_CODE_BASE_PLAFONNEE_KEY = "pregCodeBasePlafonnee";
	public static final String PREG_CODE_CRDS_KEY = "pregCodeCRDS";
	public static final String PREG_CODE_CSG_KEY = "pregCodeCSG";
	public static final String PREG_CODE_CSG_REDUITE_KEY = "pregCodeCSGReduite";
	public static final String PREG_CODE_IRCANTEC_TR_A_KEY = "pregCodeIrcantecTrA";
	public static final String PREG_CODE_SFT_KEY = "pregCodeSFT";
	public static final String PREG_COMPLEMENTAIRE_KEY = "pregComplementaire";
	public static final String PREG_EXO_URSSAF_KEY = "pregExoUrssaf";
	public static final String PREG_REGIME_AT_KEY = "pregRegimeAT";
	public static final String PREG_REGIME_MALADIE_KEY = "pregRegimeMaladie";
	public static final String PREG_REGIME_VIEILLESSE_KEY = "pregRegimeVieillesse";
	public static final String PREG_STATUT_PROF_KEY = "pregStatutProf";
	public static final String PREG_TAXE_SUR_SALAIRE_KEY = "pregTaxeSurSalaire";
	public static final String TEM_MONTANT_FORFAITAIRE_KEY = "temMontantForfaitaire";

//Colonnes dans la base de donnees
	public static final String PREG_CODE_BASE_PLAFONNEE_COLKEY = "PREG_BASE_PLAFONNEE";
	public static final String PREG_CODE_CRDS_COLKEY = "PREG_CODE_CRDS";
	public static final String PREG_CODE_CSG_COLKEY = "PREG_CODE_CSG";
	public static final String PREG_CODE_CSG_REDUITE_COLKEY = "PREG_CODE_CSG_REDUITE";
	public static final String PREG_CODE_IRCANTEC_TR_A_COLKEY = "PREG_CODE_IRCANTEC_TR_A";
	public static final String PREG_CODE_SFT_COLKEY = "PREG_CODE_SFT";
	public static final String PREG_COMPLEMENTAIRE_COLKEY = "PREG_COMPLEMENTAIRE";
	public static final String PREG_EXO_URSSAF_COLKEY = "PREG_EXO_URSSAF";
	public static final String PREG_REGIME_AT_COLKEY = "PREG_REGIME_AT";
	public static final String PREG_REGIME_MALADIE_COLKEY = "PREG_REGIME_MALADIE";
	public static final String PREG_REGIME_VIEILLESSE_COLKEY = "PREG_REGIME_VIEILLESSE";
	public static final String PREG_STATUT_PROF_COLKEY = "PREG_STATUT_PROF";
	public static final String PREG_TAXE_SUR_SALAIRE_COLKEY = "PREG_CODE_TAXE_SUR_SALAIRE";
	public static final String TEM_MONTANT_FORFAITAIRE_COLKEY = "TEM_MONTANT_FORFAITAIRE";

// Relationships
	public static final String STATUT_KEY = "statut";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String pregCodeBasePlafonnee() {
    return (String) storedValueForKey(PREG_CODE_BASE_PLAFONNEE_KEY);
  }

  public void setPregCodeBasePlafonnee(String value) {
    takeStoredValueForKey(value, PREG_CODE_BASE_PLAFONNEE_KEY);
  }

  public String pregCodeCRDS() {
    return (String) storedValueForKey(PREG_CODE_CRDS_KEY);
  }

  public void setPregCodeCRDS(String value) {
    takeStoredValueForKey(value, PREG_CODE_CRDS_KEY);
  }

  public String pregCodeCSG() {
    return (String) storedValueForKey(PREG_CODE_CSG_KEY);
  }

  public void setPregCodeCSG(String value) {
    takeStoredValueForKey(value, PREG_CODE_CSG_KEY);
  }

  public String pregCodeCSGReduite() {
    return (String) storedValueForKey(PREG_CODE_CSG_REDUITE_KEY);
  }

  public void setPregCodeCSGReduite(String value) {
    takeStoredValueForKey(value, PREG_CODE_CSG_REDUITE_KEY);
  }

  public String pregCodeIrcantecTrA() {
    return (String) storedValueForKey(PREG_CODE_IRCANTEC_TR_A_KEY);
  }

  public void setPregCodeIrcantecTrA(String value) {
    takeStoredValueForKey(value, PREG_CODE_IRCANTEC_TR_A_KEY);
  }

  public String pregCodeSFT() {
    return (String) storedValueForKey(PREG_CODE_SFT_KEY);
  }

  public void setPregCodeSFT(String value) {
    takeStoredValueForKey(value, PREG_CODE_SFT_KEY);
  }

  public String pregComplementaire() {
    return (String) storedValueForKey(PREG_COMPLEMENTAIRE_KEY);
  }

  public void setPregComplementaire(String value) {
    takeStoredValueForKey(value, PREG_COMPLEMENTAIRE_KEY);
  }

  public String pregExoUrssaf() {
    return (String) storedValueForKey(PREG_EXO_URSSAF_KEY);
  }

  public void setPregExoUrssaf(String value) {
    takeStoredValueForKey(value, PREG_EXO_URSSAF_KEY);
  }

  public String pregRegimeAT() {
    return (String) storedValueForKey(PREG_REGIME_AT_KEY);
  }

  public void setPregRegimeAT(String value) {
    takeStoredValueForKey(value, PREG_REGIME_AT_KEY);
  }

  public String pregRegimeMaladie() {
    return (String) storedValueForKey(PREG_REGIME_MALADIE_KEY);
  }

  public void setPregRegimeMaladie(String value) {
    takeStoredValueForKey(value, PREG_REGIME_MALADIE_KEY);
  }

  public String pregRegimeVieillesse() {
    return (String) storedValueForKey(PREG_REGIME_VIEILLESSE_KEY);
  }

  public void setPregRegimeVieillesse(String value) {
    takeStoredValueForKey(value, PREG_REGIME_VIEILLESSE_KEY);
  }

  public String pregStatutProf() {
    return (String) storedValueForKey(PREG_STATUT_PROF_KEY);
  }

  public void setPregStatutProf(String value) {
    takeStoredValueForKey(value, PREG_STATUT_PROF_KEY);
  }

  public String pregTaxeSurSalaire() {
    return (String) storedValueForKey(PREG_TAXE_SUR_SALAIRE_KEY);
  }

  public void setPregTaxeSurSalaire(String value) {
    takeStoredValueForKey(value, PREG_TAXE_SUR_SALAIRE_KEY);
  }

  public String temMontantForfaitaire() {
    return (String) storedValueForKey(TEM_MONTANT_FORFAITAIRE_KEY);
  }

  public void setTemMontantForfaitaire(String value) {
    takeStoredValueForKey(value, TEM_MONTANT_FORFAITAIRE_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut statut() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut)storedValueForKey(STATUT_KEY);
  }

  public void setStatutRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut oldValue = statut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STATUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STATUT_KEY);
    }
  }
  

/**
 * Créer une instance de EORegimeCotisation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORegimeCotisation createEORegimeCotisation(EOEditingContext editingContext			) {
    EORegimeCotisation eo = (EORegimeCotisation) createAndInsertInstance(editingContext, _EORegimeCotisation.ENTITY_NAME);    
    return eo;
  }

  
	  public EORegimeCotisation localInstanceIn(EOEditingContext editingContext) {
	  		return (EORegimeCotisation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORegimeCotisation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORegimeCotisation creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EORegimeCotisation object = (EORegimeCotisation)createAndInsertInstance(editingContext, _EORegimeCotisation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORegimeCotisation localInstanceIn(EOEditingContext editingContext, EORegimeCotisation eo) {
    EORegimeCotisation localInstance = (eo == null) ? null : (EORegimeCotisation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORegimeCotisation#localInstanceIn a la place.
   */
	public static EORegimeCotisation localInstanceOf(EOEditingContext editingContext, EORegimeCotisation eo) {
		return EORegimeCotisation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORegimeCotisation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORegimeCotisation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORegimeCotisation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORegimeCotisation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORegimeCotisation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORegimeCotisation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORegimeCotisation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORegimeCotisation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORegimeCotisation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORegimeCotisation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORegimeCotisation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORegimeCotisation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
