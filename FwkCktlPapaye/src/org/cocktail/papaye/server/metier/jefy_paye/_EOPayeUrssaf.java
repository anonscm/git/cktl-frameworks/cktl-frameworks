// _EOPayeUrssaf.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeUrssaf.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeUrssaf extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeUrssaf";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.Paye_Urssaf";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pursOrdre";

	public static final String PURS_MONTANT_KEY = "pursMontant";
	public static final String PURS_NB_AGENTS_KEY = "pursNbAgents";
	public static final String PURS_NB_BULLETINS_KEY = "pursNbBulletins";

//Colonnes dans la base de donnees
	public static final String PURS_MONTANT_COLKEY = "purs_montant";
	public static final String PURS_NB_AGENTS_COLKEY = "purs_nb_agents";
	public static final String PURS_NB_BULLETINS_COLKEY = "purs_nb_bulletins";

// Relationships
	public static final String DETAILS_KEY = "details";
	public static final String MOIS_DEBUT_KEY = "moisDebut";
	public static final String MOIS_FIN_KEY = "moisFin";
	public static final String SECTEUR_KEY = "secteur";
	public static final String STRUCTURE_KEY = "structure";
	public static final String STRUCTURE_SIRET_KEY = "structureSiret";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal pursMontant() {
    return (java.math.BigDecimal) storedValueForKey(PURS_MONTANT_KEY);
  }

  public void setPursMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PURS_MONTANT_KEY);
  }

  public Integer pursNbAgents() {
    return (Integer) storedValueForKey(PURS_NB_AGENTS_KEY);
  }

  public void setPursNbAgents(Integer value) {
    takeStoredValueForKey(value, PURS_NB_AGENTS_KEY);
  }

  public Integer pursNbBulletins() {
    return (Integer) storedValueForKey(PURS_NB_BULLETINS_KEY);
  }

  public void setPursNbBulletins(Integer value) {
    takeStoredValueForKey(value, PURS_NB_BULLETINS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois moisDebut() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_DEBUT_KEY);
  }

  public void setMoisDebutRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = moisDebut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_DEBUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_DEBUT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois moisFin() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_FIN_KEY);
  }

  public void setMoisFinRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = moisFin();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_FIN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_FIN_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur secteur() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur)storedValueForKey(SECTEUR_KEY);
  }

  public void setSecteurRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur oldValue = secteur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SECTEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SECTEUR_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOStructure structure() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_KEY);
  }

  public void setStructureRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOStructure structureSiret() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_SIRET_KEY);
  }

  public void setStructureSiretRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structureSiret();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_SIRET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_SIRET_KEY);
    }
  }
  
  public NSArray details() {
    return (NSArray)storedValueForKey(DETAILS_KEY);
  }

  public NSArray details(EOQualifier qualifier) {
    return details(qualifier, null, false);
  }

  public NSArray details(EOQualifier qualifier, boolean fetch) {
    return details(qualifier, null, fetch);
  }

  public NSArray details(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeUrssafDetail.PAYE_URSSAF_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeUrssafDetail.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = details();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDetailsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeUrssafDetail object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DETAILS_KEY);
  }

  public void removeFromDetailsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeUrssafDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DETAILS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeUrssafDetail createDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeUrssafDetail");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DETAILS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeUrssafDetail) eo;
  }

  public void deleteDetailsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeUrssafDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DETAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDetailsRelationships() {
    Enumeration objects = details().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDetailsRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeUrssafDetail)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPayeUrssaf avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeUrssaf createEOPayeUrssaf(EOEditingContext editingContext			) {
    EOPayeUrssaf eo = (EOPayeUrssaf) createAndInsertInstance(editingContext, _EOPayeUrssaf.ENTITY_NAME);    
    return eo;
  }

  
	  public EOPayeUrssaf localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeUrssaf)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeUrssaf creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeUrssaf creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeUrssaf object = (EOPayeUrssaf)createAndInsertInstance(editingContext, _EOPayeUrssaf.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeUrssaf localInstanceIn(EOEditingContext editingContext, EOPayeUrssaf eo) {
    EOPayeUrssaf localInstance = (eo == null) ? null : (EOPayeUrssaf)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeUrssaf#localInstanceIn a la place.
   */
	public static EOPayeUrssaf localInstanceOf(EOEditingContext editingContext, EOPayeUrssaf eo) {
		return EOPayeUrssaf.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeUrssaf fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeUrssaf fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeUrssaf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeUrssaf)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeUrssaf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeUrssaf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeUrssaf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeUrssaf)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeUrssaf fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeUrssaf eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeUrssaf ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeUrssaf fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
