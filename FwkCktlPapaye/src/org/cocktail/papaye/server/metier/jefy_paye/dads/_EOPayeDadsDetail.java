// _EOPayeDadsDetail.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeDadsDetail.java instead.
package org.cocktail.papaye.server.metier.jefy_paye.dads;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPayeDadsDetail extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PayeDadsDetail";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_dads_detail";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "detailOrdre";

	public static final String MNT_BRUT_FISCAL_KEY = "mntBrutFiscal";
	public static final String MNT_CRDS_KEY = "mntCrds";
	public static final String MNT_CSG_KEY = "mntCsg";
	public static final String MNT_DEPLAFONNE_KEY = "mntDeplafonne";
	public static final String MNT_IRCANTEC_A_KEY = "mntIrcantecA";
	public static final String MNT_IRCANTEC_B_KEY = "mntIrcantecB";
	public static final String MNT_NET_FISCAL_KEY = "mntNetFiscal";
	public static final String MNT_PLAFONNE_KEY = "mntPlafonne";
	public static final String MNT_RAFP_PAT_KEY = "mntRafpPat";
	public static final String MNT_RAFP_SAL_KEY = "mntRafpSal";
	public static final String MNT_TAXE_TR1_KEY = "mntTaxeTr1";
	public static final String MNT_TAXE_TR2_KEY = "mntTaxeTr2";
	public static final String MNT_TAXE_TR3_KEY = "mntTaxeTr3";
	public static final String MNT_TPG_KEY = "mntTpg";

// Attributs non visibles
	public static final String PSTA_ORDRE_KEY = "pstaOrdre";
	public static final String DADS_ORDRE_KEY = "dadsOrdre";
	public static final String DETAIL_ORDRE_KEY = "detailOrdre";
	public static final String NO_INDIVIDU_KEY = "noIndividu";

//Colonnes dans la base de donnees
	public static final String MNT_BRUT_FISCAL_COLKEY = "MNT_BRUT_FISCAL";
	public static final String MNT_CRDS_COLKEY = "MNT_CRDS";
	public static final String MNT_CSG_COLKEY = "MNT_CSG";
	public static final String MNT_DEPLAFONNE_COLKEY = "MNT_DEPLAFONNE";
	public static final String MNT_IRCANTEC_A_COLKEY = "MNT_IRCANTEC_A";
	public static final String MNT_IRCANTEC_B_COLKEY = "MNT_IRCANTEC_B";
	public static final String MNT_NET_FISCAL_COLKEY = "MNT_NET_FISCAL";
	public static final String MNT_PLAFONNE_COLKEY = "MNT_PLAFONNE";
	public static final String MNT_RAFP_PAT_COLKEY = "MNT_RAFP_PAT";
	public static final String MNT_RAFP_SAL_COLKEY = "MNT_RAFP_SAL";
	public static final String MNT_TAXE_TR1_COLKEY = "MNT_TAXE_TR1";
	public static final String MNT_TAXE_TR2_COLKEY = "MNT_TAXE_TR2";
	public static final String MNT_TAXE_TR3_COLKEY = "MNT_TAXE_TR3";
	public static final String MNT_TPG_COLKEY = "MNT_TPG";

	public static final String PSTA_ORDRE_COLKEY = "PSTA_ORDRE";
	public static final String DADS_ORDRE_COLKEY = "DADS_ORDRE";
	public static final String DETAIL_ORDRE_COLKEY = "DETAIL_ORDRE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";


	// Relationships
	public static final String DADS_KEY = "dads";
	public static final String INDIVIDU_KEY = "individu";
	public static final String STATUT_KEY = "statut";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal mntBrutFiscal() {
    return (java.math.BigDecimal) storedValueForKey(MNT_BRUT_FISCAL_KEY);
  }

  public void setMntBrutFiscal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_BRUT_FISCAL_KEY);
  }

  public java.math.BigDecimal mntCrds() {
    return (java.math.BigDecimal) storedValueForKey(MNT_CRDS_KEY);
  }

  public void setMntCrds(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_CRDS_KEY);
  }

  public java.math.BigDecimal mntCsg() {
    return (java.math.BigDecimal) storedValueForKey(MNT_CSG_KEY);
  }

  public void setMntCsg(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_CSG_KEY);
  }

  public java.math.BigDecimal mntDeplafonne() {
    return (java.math.BigDecimal) storedValueForKey(MNT_DEPLAFONNE_KEY);
  }

  public void setMntDeplafonne(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_DEPLAFONNE_KEY);
  }

  public java.math.BigDecimal mntIrcantecA() {
    return (java.math.BigDecimal) storedValueForKey(MNT_IRCANTEC_A_KEY);
  }

  public void setMntIrcantecA(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_IRCANTEC_A_KEY);
  }

  public java.math.BigDecimal mntIrcantecB() {
    return (java.math.BigDecimal) storedValueForKey(MNT_IRCANTEC_B_KEY);
  }

  public void setMntIrcantecB(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_IRCANTEC_B_KEY);
  }

  public java.math.BigDecimal mntNetFiscal() {
    return (java.math.BigDecimal) storedValueForKey(MNT_NET_FISCAL_KEY);
  }

  public void setMntNetFiscal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_NET_FISCAL_KEY);
  }

  public java.math.BigDecimal mntPlafonne() {
    return (java.math.BigDecimal) storedValueForKey(MNT_PLAFONNE_KEY);
  }

  public void setMntPlafonne(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_PLAFONNE_KEY);
  }

  public java.math.BigDecimal mntRafpPat() {
    return (java.math.BigDecimal) storedValueForKey(MNT_RAFP_PAT_KEY);
  }

  public void setMntRafpPat(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_RAFP_PAT_KEY);
  }

  public java.math.BigDecimal mntRafpSal() {
    return (java.math.BigDecimal) storedValueForKey(MNT_RAFP_SAL_KEY);
  }

  public void setMntRafpSal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_RAFP_SAL_KEY);
  }

  public java.math.BigDecimal mntTaxeTr1() {
    return (java.math.BigDecimal) storedValueForKey(MNT_TAXE_TR1_KEY);
  }

  public void setMntTaxeTr1(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_TAXE_TR1_KEY);
  }

  public java.math.BigDecimal mntTaxeTr2() {
    return (java.math.BigDecimal) storedValueForKey(MNT_TAXE_TR2_KEY);
  }

  public void setMntTaxeTr2(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_TAXE_TR2_KEY);
  }

  public java.math.BigDecimal mntTaxeTr3() {
    return (java.math.BigDecimal) storedValueForKey(MNT_TAXE_TR3_KEY);
  }

  public void setMntTaxeTr3(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_TAXE_TR3_KEY);
  }

  public java.math.BigDecimal mntTpg() {
    return (java.math.BigDecimal) storedValueForKey(MNT_TPG_KEY);
  }

  public void setMntTpg(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_TPG_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeDads dads() {
    return (org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeDads)storedValueForKey(DADS_KEY);
  }

  public void setDadsRelationship(org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeDads value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeDads oldValue = dads();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DADS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DADS_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOIndividu individu() {
    return (org.cocktail.papaye.server.metier.grhum.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.papaye.server.metier.grhum.EOIndividu value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut statut() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut)storedValueForKey(STATUT_KEY);
  }

  public void setStatutRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut oldValue = statut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STATUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STATUT_KEY);
    }
  }
  

/**
 * Créer une instance de EOPayeDadsDetail avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeDadsDetail createEOPayeDadsDetail(EOEditingContext editingContext			) {
    EOPayeDadsDetail eo = (EOPayeDadsDetail) createAndInsertInstance(editingContext, _EOPayeDadsDetail.ENTITY_NAME);    
    return eo;
  }

  
	  public EOPayeDadsDetail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeDadsDetail)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeDadsDetail creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeDadsDetail creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeDadsDetail object = (EOPayeDadsDetail)createAndInsertInstance(editingContext, _EOPayeDadsDetail.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeDadsDetail localInstanceIn(EOEditingContext editingContext, EOPayeDadsDetail eo) {
    EOPayeDadsDetail localInstance = (eo == null) ? null : (EOPayeDadsDetail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeDadsDetail#localInstanceIn a la place.
   */
	public static EOPayeDadsDetail localInstanceOf(EOEditingContext editingContext, EOPayeDadsDetail eo) {
		return EOPayeDadsDetail.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeDadsDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeDadsDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeDadsDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeDadsDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeDadsDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeDadsDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeDadsDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeDadsDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeDadsDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeDadsDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeDadsDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeDadsDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
