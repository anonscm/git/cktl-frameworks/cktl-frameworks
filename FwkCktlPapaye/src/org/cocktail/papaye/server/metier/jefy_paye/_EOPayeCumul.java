// _EOPayeCumul.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeCumul.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeCumul extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeCumul";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_cumul";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pcumOrdre";

	public static final String PCUM_ANNEE_KEY = "pcumAnnee";
	public static final String PCUM_BASE_KEY = "pcumBase";
	public static final String PCUM_MONTANT_KEY = "pcumMontant";
	public static final String PCUM_REGUL_KEY = "pcumRegul";
	public static final String PCUM_TAUX_KEY = "pcumTaux";
	public static final String PCUM_TYPE_KEY = "pcumType";

//Colonnes dans la base de donnees
	public static final String PCUM_ANNEE_COLKEY = "pcum_annee";
	public static final String PCUM_BASE_COLKEY = "pcum_base";
	public static final String PCUM_MONTANT_COLKEY = "pcum_montant";
	public static final String PCUM_REGUL_COLKEY = "pcum_regul";
	public static final String PCUM_TAUX_COLKEY = "pcum_taux";
	public static final String PCUM_TYPE_COLKEY = "pcum_type";

// Relationships
	public static final String AGENT_KEY = "agent";
	public static final String CODE_KEY = "code";
	public static final String MOIS_KEY = "mois";
	public static final String RUBRIQUE_KEY = "rubrique";
	public static final String STRUCTURE_KEY = "structure";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer pcumAnnee() {
    return (Integer) storedValueForKey(PCUM_ANNEE_KEY);
  }

  public void setPcumAnnee(Integer value) {
    takeStoredValueForKey(value, PCUM_ANNEE_KEY);
  }

  public java.math.BigDecimal pcumBase() {
    return (java.math.BigDecimal) storedValueForKey(PCUM_BASE_KEY);
  }

  public void setPcumBase(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PCUM_BASE_KEY);
  }

  public java.math.BigDecimal pcumMontant() {
    return (java.math.BigDecimal) storedValueForKey(PCUM_MONTANT_KEY);
  }

  public void setPcumMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PCUM_MONTANT_KEY);
  }

  public java.math.BigDecimal pcumRegul() {
    return (java.math.BigDecimal) storedValueForKey(PCUM_REGUL_KEY);
  }

  public void setPcumRegul(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PCUM_REGUL_KEY);
  }

  public java.math.BigDecimal pcumTaux() {
    return (java.math.BigDecimal) storedValueForKey(PCUM_TAUX_KEY);
  }

  public void setPcumTaux(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PCUM_TAUX_KEY);
  }

  public String pcumType() {
    return (String) storedValueForKey(PCUM_TYPE_KEY);
  }

  public void setPcumType(String value) {
    takeStoredValueForKey(value, PCUM_TYPE_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EOIndividu agent() {
    return (org.cocktail.papaye.server.metier.grhum.EOIndividu)storedValueForKey(AGENT_KEY);
  }

  public void setAgentRelationship(org.cocktail.papaye.server.metier.grhum.EOIndividu value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOIndividu oldValue = agent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, AGENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, AGENT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode code() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode)storedValueForKey(CODE_KEY);
  }

  public void setCodeRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode oldValue = code();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique rubrique() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique)storedValueForKey(RUBRIQUE_KEY);
  }

  public void setRubriqueRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique oldValue = rubrique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RUBRIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RUBRIQUE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOStructure structure() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_KEY);
  }

  public void setStructureRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPayeCumul avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeCumul createEOPayeCumul(EOEditingContext editingContext, Integer pcumAnnee
, java.math.BigDecimal pcumBase
, java.math.BigDecimal pcumMontant
, java.math.BigDecimal pcumRegul
, java.math.BigDecimal pcumTaux
, String pcumType
, org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode code, org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois, org.cocktail.papaye.server.metier.grhum.EOStructure structure			) {
    EOPayeCumul eo = (EOPayeCumul) createAndInsertInstance(editingContext, _EOPayeCumul.ENTITY_NAME);    
		eo.setPcumAnnee(pcumAnnee);
		eo.setPcumBase(pcumBase);
		eo.setPcumMontant(pcumMontant);
		eo.setPcumRegul(pcumRegul);
		eo.setPcumTaux(pcumTaux);
		eo.setPcumType(pcumType);
    eo.setCodeRelationship(code);
    eo.setMoisRelationship(mois);
    eo.setStructureRelationship(structure);
    return eo;
  }

  
	  public EOPayeCumul localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeCumul)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeCumul creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeCumul creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeCumul object = (EOPayeCumul)createAndInsertInstance(editingContext, _EOPayeCumul.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeCumul localInstanceIn(EOEditingContext editingContext, EOPayeCumul eo) {
    EOPayeCumul localInstance = (eo == null) ? null : (EOPayeCumul)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeCumul#localInstanceIn a la place.
   */
	public static EOPayeCumul localInstanceOf(EOEditingContext editingContext, EOPayeCumul eo) {
		return EOPayeCumul.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeCumul fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeCumul fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeCumul eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeCumul)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeCumul fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeCumul fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeCumul eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeCumul)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeCumul fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeCumul eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeCumul ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeCumul fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
