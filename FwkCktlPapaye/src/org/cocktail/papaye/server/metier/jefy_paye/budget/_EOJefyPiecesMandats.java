// _EOJefyPiecesMandats.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJefyPiecesMandats.java instead.
package org.cocktail.papaye.server.metier.jefy_paye.budget;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOJefyPiecesMandats extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "JefyPiecesMandats";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.JEFY_PIECES_MANDATS";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "jpmOrdre";

	public static final String GES_CODE_KEY = "gesCode";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PPJ_LIBELLE_KEY = "ppjLibelle";
	public static final String PPJ_MONTANT_KEY = "ppjMontant";
	public static final String PPJ_OBSERVATIONS_KEY = "ppjObservations";

//Colonnes dans la base de donnees
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PPJ_LIBELLE_COLKEY = "PPJ_LIBELLE";
	public static final String PPJ_MONTANT_COLKEY = "PPJ_MONTANT";
	public static final String PPJ_OBSERVATIONS_COLKEY = "PPJ_OBSERVATIONS";

// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String INDIVIDU_KEY = "individu";
	public static final String MOIS_KEY = "mois";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String RUBRIQUE_KEY = "rubrique";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public String ppjLibelle() {
    return (String) storedValueForKey(PPJ_LIBELLE_KEY);
  }

  public void setPpjLibelle(String value) {
    takeStoredValueForKey(value, PPJ_LIBELLE_KEY);
  }

  public java.math.BigDecimal ppjMontant() {
    return (java.math.BigDecimal) storedValueForKey(PPJ_MONTANT_KEY);
  }

  public void setPpjMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PPJ_MONTANT_KEY);
  }

  public String ppjObservations() {
    return (String) storedValueForKey(PPJ_OBSERVATIONS_KEY);
  }

  public void setPpjObservations(String value) {
    takeStoredValueForKey(value, PPJ_OBSERVATIONS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_admin.EOExercice exercice() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOExercice value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOIndividu individu() {
    return (org.cocktail.papaye.server.metier.grhum.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.papaye.server.metier.grhum.EOIndividu value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOOrgan organ() {
    return (org.cocktail.application.serveur.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.serveur.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique rubrique() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique)storedValueForKey(RUBRIQUE_KEY);
  }

  public void setRubriqueRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique oldValue = rubrique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RUBRIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RUBRIQUE_KEY);
    }
  }
  
  public NSArray planComptable() {
    return (NSArray)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public NSArray planComptable(EOQualifier qualifier) {
    return planComptable(qualifier, null);
  }

  public NSArray planComptable(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = planComptable();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToPlanComptableRelationship(org.cocktail.papaye.server.metier.maracuja.EOPlanComptableExer object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
  }

  public void removeFromPlanComptableRelationship(org.cocktail.papaye.server.metier.maracuja.EOPlanComptableExer object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
  }

  public org.cocktail.papaye.server.metier.maracuja.EOPlanComptableExer createPlanComptableRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PlanComptableExer");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PLAN_COMPTABLE_KEY);
    return (org.cocktail.papaye.server.metier.maracuja.EOPlanComptableExer) eo;
  }

  public void deletePlanComptableRelationship(org.cocktail.papaye.server.metier.maracuja.EOPlanComptableExer object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPlanComptableRelationships() {
    Enumeration objects = planComptable().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePlanComptableRelationship((org.cocktail.papaye.server.metier.maracuja.EOPlanComptableExer)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOJefyPiecesMandats avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOJefyPiecesMandats createEOJefyPiecesMandats(EOEditingContext editingContext, org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois			) {
    EOJefyPiecesMandats eo = (EOJefyPiecesMandats) createAndInsertInstance(editingContext, _EOJefyPiecesMandats.ENTITY_NAME);    
    eo.setMoisRelationship(mois);
    return eo;
  }

  
	  public EOJefyPiecesMandats localInstanceIn(EOEditingContext editingContext) {
	  		return (EOJefyPiecesMandats)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyPiecesMandats creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyPiecesMandats creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOJefyPiecesMandats object = (EOJefyPiecesMandats)createAndInsertInstance(editingContext, _EOJefyPiecesMandats.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOJefyPiecesMandats localInstanceIn(EOEditingContext editingContext, EOJefyPiecesMandats eo) {
    EOJefyPiecesMandats localInstance = (eo == null) ? null : (EOJefyPiecesMandats)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOJefyPiecesMandats#localInstanceIn a la place.
   */
	public static EOJefyPiecesMandats localInstanceOf(EOEditingContext editingContext, EOJefyPiecesMandats eo) {
		return EOJefyPiecesMandats.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOJefyPiecesMandats fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOJefyPiecesMandats fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJefyPiecesMandats eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJefyPiecesMandats)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJefyPiecesMandats fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJefyPiecesMandats fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJefyPiecesMandats eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJefyPiecesMandats)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOJefyPiecesMandats fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJefyPiecesMandats eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJefyPiecesMandats ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJefyPiecesMandats fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
