/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Tue Mar 18 15:10:10 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.metier.grhum.EOIndividu;
import org.cocktail.papaye.server.metier.grhum.EORibs;
import org.cocktail.papaye.server.metier.maracuja.EOModePaiement;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOPayePeriode extends _EOPayePeriode {
    /** Constante a utiliser dans moisCode pour le mois generique */
    public static int MOIS_GENERIQUE = 999999;
    public EOPayePeriode() {
        super();
    }


    // méthodes ajoutées
    public boolean estComplete() {
        return temComplet().equals(Constantes.VRAI);
    }
    public boolean premierePaye() {
        return temPremierePaye().equals(Constantes.VRAI);
    }
    public boolean salaireARetirer() {
        return temPositive().equals(Constantes.FAUX);
    }
	/** Initialisation d'une nouvelle periode en fonction d'un contrat et d'un secteur. 
		Boolean moisGenerique : Si true, on associe le mois generique a la periode (CDI), sinon on associe le mois passe en parametres */
	public void initPeriode(EOEditingContext ec, EOPayeContrat contrat, EOPayeMois mois, boolean moisGenerique)
	{
		setContratRelationship(contrat);
			
		if (moisGenerique) {
			EOPayeMois moisGen = EOPayeMois.rechercherMoisGenerique(ec);
			setMoisRelationship(moisGen);
			setMoisTraitementRelationship(moisGen);
		} else {
			setMoisRelationship(mois);
		}
		
		// Mode de paiement
		EORibs ribCourant = null;
		try {
			ribCourant = contrat.individu().ribCourant();
		} catch (Exception e) {}
		EOModePaiement modePaiement = null;
		// privilégier le mode de paiement du rib courant
		if (ribCourant != null) {
			modePaiement = EOModePaiement.getModePaiementForCodeEtAnnee(ec,ribCourant.modCode(),mois.moisAnnee());
		} else {
			if (moisGenerique) {
				modePaiement = EOModePaiement.getDefaultModePaiement(ec);
			} else {
				modePaiement = EOModePaiement.getDefaultModePaiementPourAnnee(ec,mois.moisAnnee());
			}
			if (modePaiement != null) {
				setModePaiementRelationship(modePaiement);
			}
		}
		
		setPperNbHeure(new BigDecimal(0.0));
		setPperNbJour(new BigDecimal(0));
		setTemPositive("O");
		setTemValide("O");
		setTemComplet("O");
		setTemPremierePaye("N");
	}
	
    /** supprime toutes les relations attachees a une periode */
    public void supprimerRelations() {
        setMoisRelationship(null);
        if (moisTraitement() != null) {
            setMoisTraitementRelationship(null);
        }
        setContratRelationship(null);
    }
    /** decale une periode a un autre mois */
    public void decalerPeriode(EOPayeMois mois) {
        setMoisRelationship(mois);
    }
    public String toString() {
        return new String(getClass().getName() +
                          "\ncomplete : " + temComplet() +
                          "\nnb heures : " + pperNbHeure() +
                          "\nnb jours : " + pperNbJour() +
                          "\nPremiere paye : " + temPremierePaye() +
                          "\nmois : " + mois() +

                          "\ncontrat : " + contrat());
    }
    // méthodes de classe
    /** methode de classe permettant de trouver toutes les periodes du mois courant pour un
        secteur triees par agent et contrat, pour le cas o&ugrave; un m&ecirc;me agent a plusieurs
        contrats et qu'il y a des rappels a effectuer.
        @param editingContext editing context dans lequel effectuer le fetch
        @param secteur secteur pour lequel rechercher les periodes (peut &ecirc;tre nul)
    */
    public static NSArray rechercherPeriodesCourantes(EOEditingContext editingContext,EOPayeSecteur secteur) {
        // périodes valides
        NSMutableArray values = new NSMutableArray(new String(Constantes.VRAI));
        // pour le mois générique
        values.addObject(new Integer(MOIS_GENERIQUE));
        // pour le mois courant
        values.addObject(EOPayeMois.moisCourant(editingContext));
        EOQualifier qualifier;
        if (secteur != null) {
            values.addObject(secteur);
            
            qualifier = EOQualifier.qualifierWithQualifierFormat(
                    "temValide = %@ AND (mois.moisCode = %@ OR mois = %@) AND secteur = %@",values);
        }
        else {
            qualifier = EOQualifier.qualifierWithQualifierFormat(
                                    "temValide = %@ AND (mois.moisCode = %@ OR mois = %@)",values);
        }
        // trier les périodes sur les agents et les contrats, les périodes de mois de traitement et de mois ordre
		EOFetchSpecification fs = new EOFetchSpecification(EOPayePeriode.ENTITY_NAME,qualifier, null);
        NSMutableArray prefetches = new NSMutableArray(EOPayePeriode.CONTRAT_KEY);
        prefetches.addObject(EOPayePeriode.CONTRAT_KEY+"."+EOPayeContrat.INDIVIDU_KEY);
        fs.setPrefetchingRelationshipKeyPaths(prefetches);
        NSArray resultats = editingContext.objectsWithFetchSpecification(fs);
		NSMutableArray sorts = new NSMutableArray();
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("contrat.individu.nomUsuel",
																 EOSortOrdering.CompareAscending);
        sorts.addObject(sort);
		sort = EOSortOrdering.sortOrderingWithKey("contrat.idContrat", EOSortOrdering.CompareAscending);
        sorts.addObject(sort);
		sort = EOSortOrdering.sortOrderingWithKey("moisTraitement.moisCode", EOSortOrdering.CompareAscending);
        sorts.addObject(sort);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(resultats,sorts);
    }
    /** methode de classe permettant de trouver toutes les periodes du mois
    correspondant a un contrat a duree indeterminee pour
    un secteur triees par agent (pour le cas o&ugrave; un m&ecirc;me agent a
                                        plusieurs contrats).
    @param editingContext editing context dans lequel effectuer le fetch
    @param secteur secteur pour lequel rechercher les periodes (peut &ecirc;tre nul)
    */
    public static NSArray rechercherPeriodesCourantesPermanentes(EOEditingContext editingContext,EOPayeSecteur secteur) {
        // périodes valides
        NSMutableArray values = new NSMutableArray(new String(Constantes.VRAI));
        // pour le mois générique
        values.addObject(new Integer(MOIS_GENERIQUE));
        // pour le mois courant
        values.addObject(EOPayeMois.moisCourant(editingContext));
        EOQualifier qualifier;
        if (secteur != null) {
            values.addObject(secteur);
            qualifier = EOQualifier.qualifierWithQualifierFormat(
                                "temValide = %@ AND mois.moisCode = %@ AND secteur = %@",values);
        }
        else {
            qualifier = EOQualifier.qualifierWithQualifierFormat(
                                "temValide = %@ AND mois.moisCode = %@",values);
        }
        // trier les périodes sur les agents en utilisant le numéro SS
        EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("contrat.individu.nomUsuel",
                                                                 EOSortOrdering.CompareAscending);
        EOFetchSpecification fs = new EOFetchSpecification(EOPayePeriode.ENTITY_NAME,qualifier, new NSArray(sort));
        NSMutableArray prefetches = new NSMutableArray(EOPayePeriode.CONTRAT_KEY);
        prefetches.addObject("contrat.individu");
        fs.setPrefetchingRelationshipKeyPaths(prefetches);
        return editingContext.objectsWithFetchSpecification(fs);
    }
    /** methode de classe permettant de rechercher toutes les periodes associees
        a des nouveaux contrats
        @param editingContext editing context dans lequel fetcher les objets
        @param secteur secteur pour lequel rechercher les periodes (peut &ecirc;tre nul)
    */
    public static NSArray rechercherPeriodesNouveauxContrats(EOEditingContext editingContext,EOPayeSecteur secteur) {
        // périodes valides
        NSMutableArray values = new NSMutableArray(new String(Constantes.VRAI));
        // pour le mois générique
        values.addObject(new Integer(MOIS_GENERIQUE));
        // pour le mois courant
        values.addObject(EOPayeMois.moisCourant(editingContext));
        values.addObject(new String(Constantes.FAUX));
        EOQualifier qualifier;
        if (secteur != null) {
            values.addObject(secteur);
            qualifier = EOQualifier.qualifierWithQualifierFormat(
            "temValide = %@ AND (mois.moisCode = %@ OR mois = %@) AND contrat.temPayeUtile = %@ AND secteur = %@",values);
        }
        else {
            qualifier = EOQualifier.qualifierWithQualifierFormat(
            "temValide = %@ AND (mois.moisCode = %@ OR mois = %@) AND contrat.temPayeUtile = %@ ",values);
        }
        EOFetchSpecification fs = new EOFetchSpecification(EOPayePeriode.ENTITY_NAME,qualifier,null);
        NSMutableArray prefetches = new NSMutableArray(EOPayePeriode.CONTRAT_KEY);
        prefetches.addObject("contrat.statut");
        fs.setPrefetchingRelationshipKeyPaths(prefetches);
        return editingContext.objectsWithFetchSpecification(fs);
    }
    /** methode de classe permettant de rechercher toutes les periodes du mois
    pour lesquelles une preparation n'a pas ete generee
    @param editingContext editing context dans lequel fetcher les objets
    @param secteur secteur pour lequel rechercher les periodes (peut &ecirc;tre nul)
    */
    public static NSArray rechercherPeriodesCourantesNonTraitees(EOEditingContext editingContext,EOPayeSecteur secteur) {
        // rechercher les périodes courantes
        NSArray periodesCourantes = rechercherPeriodesCourantes(editingContext,secteur);
        return preparationsNonTraitees(editingContext,secteur,periodesCourantes);
    }
    /** methode de classe permettant de rechercher toutes les periodes associees a des contrats a duree indeterminee
    pour lesquelles une preparation n'a pas ete generee
    @param editingContext editing context dans lequel fetcher les objets
    @param secteur secteur pour lequel rechercher les periodes (peut &ecirc;tre nul)
    */
    public static NSArray rechercherPeriodesPermanentesNonTraitees(EOEditingContext editingContext,EOPayeSecteur secteur) {
        // rechercher les périodes courantes
        NSArray periodesCourantes = rechercherPeriodesCourantesPermanentes(editingContext,secteur);
        return preparationsNonTraitees(editingContext,secteur,periodesCourantes);
    }
    /** methode de classe permettant de rechercher toutes les periodes associees a des nouveaux contrats a duree indeterminee
    pour lesquelles une preparation n'a pas ete generee
    @param editingContext editing context dans lequel fetcher les objets
    @param secteur secteur pour lequel rechercher les periodes (peut &ecirc;tre nul)
    */
    public static NSArray rechercherPeriodesNouveauxContratsNonTraitees(EOEditingContext editingContext,EOPayeSecteur secteur) {
        // rechercher les périodes courantes
        NSArray periodesCourantes = rechercherPeriodesNouveauxContrats(editingContext,secteur);
        return preparationsNonTraitees(editingContext,secteur,periodesCourantes);
    }
    /** methode de classe permettant de trouver toutes les periodes pour un mois,
    un statut et un secteur triees par agent (pour le cas o&ugrave; un m&ecirc;me agent a plusieurs contrats).
    @param editingContext editing context dans lequel effectuer le fetch
    @param mois mois pour lequel rechercher les periodes
    @param statut statut pour lequel rechercher les periodes
    @param secteur secteur pour lequel rechercher les periodes (peut &ecirc;tre nul)
    */
    public static NSArray rechercherPeriodesPourMoisStatutSecteur(EOEditingContext editingContext,EOPayeMois mois, EOPayeStatut statut,EOPayeSecteur secteur) {
        // périodes valides
        NSMutableArray values = new NSMutableArray(new String(Constantes.VRAI));
        // pour le mois
        values.addObject(mois);
        // pour le statut
        values.addObject(statut);
        // pour le secteur
        EOQualifier qualifier;
        if (secteur != null) {
            values.addObject(secteur);
            qualifier = EOQualifier.qualifierWithQualifierFormat(
                "temValide = %@ AND mois = %@ AND contrat.statut = %@ AND secteur = %@",values);
        }
        else {
            qualifier = EOQualifier.qualifierWithQualifierFormat(
                "temValide = %@ AND mois = %@ ND contrat.statut = %@",values);
        }
        // trier les périodes sur les agents en utilisant le numéro SS
        EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("contrat.individu.nomUsuel",
                                                                 EOSortOrdering.CompareAscending);
        EOFetchSpecification fs = new EOFetchSpecification(EOPayePeriode.ENTITY_NAME,qualifier, new NSArray(sort));
        NSMutableArray prefetches = new NSMutableArray(EOPayePeriode.CONTRAT_KEY);
        prefetches.addObject("contrat.individu");
        fs.setPrefetchingRelationshipKeyPaths(prefetches);
        return editingContext.objectsWithFetchSpecification(fs);
    }
    /** methode de classe permettant de trouver les periodes liees a un
    contrat et un mois.
    @param editingContext editing context dans lequel effectuer le fetch
    @param contrat contrat pour lequel rechercher la periode
    @param mois mois pour lequel rechercher les periodes
    @return periodes trouvees
    */
    public static NSArray rechercherPeriodesPourContratEtMois(EOEditingContext editingContext,
                                                                 EOPayeContrat contrat,
                                                                 EOPayeMois mois) {
        // périodes valides
        NSMutableArray values = new NSMutableArray(new String(Constantes.VRAI));
        // pour le contrat
        values.addObject(contrat);
        // pour le mois générique
        values.addObject(new Integer(MOIS_GENERIQUE));
        // pour le mois
        values.addObject(mois);
        // pour le mois traitement
        values.addObject(mois);
        // pour le statut
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(
    "temValide = %@ AND contrat = %@ AND (mois.moisCode = %@ OR mois = %@ OR moisTraitement = %@)",values);
        EOFetchSpecification fs = new EOFetchSpecification(EOPayePeriode.ENTITY_NAME,qualifier, null);
        return editingContext.objectsWithFetchSpecification(fs);
    }
    /** methode de classe permettant de trouver les periodes liees a un
    individu et un mois.
    @param editingContext editing context dans lequel effectuer le fetch
    @param individu contrat pour lequel rechercher les periodes
    @param mois mois pour lequel rechercher les periodes
    @return periodes trouvees
    */
    public static NSArray rechercherPeriodesPourIndividuEtMois(EOEditingContext editingContext,
                                                                 EOIndividu individu,
                                                                 EOPayeMois mois) {
        // périodes valides
        NSMutableArray values = new NSMutableArray(new String(Constantes.VRAI));
        // pour l'individu
        values.addObject(individu);
        // pour le mois générique
        values.addObject(new Integer(MOIS_GENERIQUE));
        // pour le mois
        values.addObject(mois);
        // pour le mois traitement
        values.addObject(mois);
        // pour le statut
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(
    "temValide = %@ AND contrat.individu = %@ AND (mois.moisCode = %@ OR mois = %@ OR moisTraitement = %@)",values);
        EOFetchSpecification fs = new EOFetchSpecification(EOPayePeriode.ENTITY_NAME,qualifier, null);
        return editingContext.objectsWithFetchSpecification(fs);
    }
	/** methode de classe permettant de trouver la periode courante liee a un
		contrat et un mois (moisTraitement == mois).
		@param editingContext editing context dans lequel effectuer le fetch
		@param contrat contrat pour lequel rechercher la periode
		@param mois mois pour lequel rechercher les periodes
		@return periode trouvee
		*/
	
	public static EOPayePeriode rechercherPeriodePourContratEtMois(EOEditingContext editingContext,EOPayeContrat contrat,EOPayeMois mois) throws Exception {
		NSArray periodes = rechercherPeriodesPourContratEtMois(editingContext,contrat,mois);
		Enumeration e = periodes.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayePeriode periode = (EOPayePeriode)e.nextElement();
			if (periode.mois().moisCode().intValue() == periode.moisTraitement().moisCode().intValue()) {
				return periode;
			}
		}
		throw new Exception("Pas de periode courante pour le contrat de " + contrat.individu().identite());
	}
    // méthode privée
    private static NSArray preparationsNonTraitees(EOEditingContext editingContext,
                                                   EOPayeSecteur secteur,NSArray periodes) {
        // rechercher toutes les préparations déjà réalisées pour ce mois
        // prefetcher les contrats
       NSArray prepas = EOPayePrepa.chercherPreparationsPourMoisEtSecteur(editingContext,
                                              EOPayeMois.moisCourant(editingContext),secteur,
                                              new NSArray(EOPayePeriode.CONTRAT_KEY));
        if ((prepas == null) || (prepas.count() == 0)) {
            // rien n'a été traité
            return periodes;
        }
        NSMutableArray periodesNonTraitees = new NSMutableArray();
        // ne garder que les périodes non traitées
        try {
            Enumeration e = periodes.objectEnumerator();
            while (e.hasMoreElements()) {
                EOPayePeriode periode = (EOPayePeriode)e.nextElement();
                Enumeration e1 = prepas.objectEnumerator();
                boolean found = false;
                while (e1.hasMoreElements()) {
                    EOPayePrepa prepa = (EOPayePrepa)e1.nextElement();
                    if (prepa.contrat() == periode.contrat()) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    periodesNonTraitees.addObject(periode);
                }
            }

            return periodesNonTraitees;
        } catch (Exception e) { return null; }
    }
}
