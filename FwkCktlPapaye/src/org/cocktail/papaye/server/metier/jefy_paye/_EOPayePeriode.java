// _EOPayePeriode.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayePeriode.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayePeriode extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayePeriode";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_periode";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pperOrdre";

	public static final String PPER_NB_HEURE_KEY = "pperNbHeure";
	public static final String PPER_NB_JOUR_KEY = "pperNbJour";
	public static final String TEM_COMPLET_KEY = "temComplet";
	public static final String TEM_POSITIVE_KEY = "temPositive";
	public static final String TEM_PREMIERE_PAYE_KEY = "temPremierePaye";
	public static final String TEM_VALIDE_KEY = "temValide";

//Colonnes dans la base de donnees
	public static final String PPER_NB_HEURE_COLKEY = "PPER_NB_HEURES";
	public static final String PPER_NB_JOUR_COLKEY = "PPER_NB_JOURS";
	public static final String TEM_COMPLET_COLKEY = "TEM_COMPLET";
	public static final String TEM_POSITIVE_COLKEY = "TEM_POSITIVE";
	public static final String TEM_PREMIERE_PAYE_COLKEY = "TEM_PREMIERE_PAYE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

// Relationships
	public static final String CONTRAT_KEY = "contrat";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String MOIS_KEY = "mois";
	public static final String MOIS_TRAITEMENT_KEY = "moisTraitement";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal pperNbHeure() {
    return (java.math.BigDecimal) storedValueForKey(PPER_NB_HEURE_KEY);
  }

  public void setPperNbHeure(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PPER_NB_HEURE_KEY);
  }

  public java.math.BigDecimal pperNbJour() {
    return (java.math.BigDecimal) storedValueForKey(PPER_NB_JOUR_KEY);
  }

  public void setPperNbJour(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PPER_NB_JOUR_KEY);
  }

  public String temComplet() {
    return (String) storedValueForKey(TEM_COMPLET_KEY);
  }

  public void setTemComplet(String value) {
    takeStoredValueForKey(value, TEM_COMPLET_KEY);
  }

  public String temPositive() {
    return (String) storedValueForKey(TEM_POSITIVE_KEY);
  }

  public void setTemPositive(String value) {
    takeStoredValueForKey(value, TEM_POSITIVE_KEY);
  }

  public String temPremierePaye() {
    return (String) storedValueForKey(TEM_PREMIERE_PAYE_KEY);
  }

  public void setTemPremierePaye(String value) {
    takeStoredValueForKey(value, TEM_PREMIERE_PAYE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat contrat() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat)storedValueForKey(CONTRAT_KEY);
  }

  public void setContratRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTRAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONTRAT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.maracuja.EOModePaiement modePaiement() {
    return (org.cocktail.papaye.server.metier.maracuja.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.papaye.server.metier.maracuja.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.maracuja.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois moisTraitement() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_TRAITEMENT_KEY);
  }

  public void setMoisTraitementRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = moisTraitement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_TRAITEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_TRAITEMENT_KEY);
    }
  }
  

/**
 * Créer une instance de EOPayePeriode avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayePeriode createEOPayePeriode(EOEditingContext editingContext, java.math.BigDecimal pperNbHeure
, java.math.BigDecimal pperNbJour
, String temComplet
, String temPositive
, String temPremierePaye
, String temValide
, org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois, org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois moisTraitement			) {
    EOPayePeriode eo = (EOPayePeriode) createAndInsertInstance(editingContext, _EOPayePeriode.ENTITY_NAME);    
		eo.setPperNbHeure(pperNbHeure);
		eo.setPperNbJour(pperNbJour);
		eo.setTemComplet(temComplet);
		eo.setTemPositive(temPositive);
		eo.setTemPremierePaye(temPremierePaye);
		eo.setTemValide(temValide);
    eo.setMoisRelationship(mois);
    eo.setMoisTraitementRelationship(moisTraitement);
    return eo;
  }

  
	  public EOPayePeriode localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayePeriode)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayePeriode creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayePeriode creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayePeriode object = (EOPayePeriode)createAndInsertInstance(editingContext, _EOPayePeriode.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayePeriode localInstanceIn(EOEditingContext editingContext, EOPayePeriode eo) {
    EOPayePeriode localInstance = (eo == null) ? null : (EOPayePeriode)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayePeriode#localInstanceIn a la place.
   */
	public static EOPayePeriode localInstanceOf(EOEditingContext editingContext, EOPayePeriode eo) {
		return EOPayePeriode.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayePeriode fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayePeriode fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayePeriode eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayePeriode)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayePeriode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayePeriode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayePeriode eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayePeriode)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayePeriode fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayePeriode eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayePeriode ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayePeriode fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
