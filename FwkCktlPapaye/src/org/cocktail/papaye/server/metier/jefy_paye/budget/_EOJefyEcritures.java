// _EOJefyEcritures.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJefyEcritures.java instead.
package org.cocktail.papaye.server.metier.jefy_paye.budget;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOJefyEcritures extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "JefyEcritures";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.JEFY_ECRITURES";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "ecrOrdre";

	public static final String ECR_COMP_KEY = "ecrComp";
	public static final String ECR_ETAT_KEY = "ecrEtat";
	public static final String ECR_MONT_KEY = "ecrMont";
	public static final String ECR_RETENUE_KEY = "ecrRetenue";
	public static final String ECR_SENS_KEY = "ecrSens";
	public static final String ECR_SOURCE_KEY = "ecrSource";
	public static final String ECR_TYPE_KEY = "ecrType";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String ECR_COMP_COLKEY = "ECR_COMP";
	public static final String ECR_ETAT_COLKEY = "ECR_ETAT";
	public static final String ECR_MONT_COLKEY = "ECR_MONT";
	public static final String ECR_RETENUE_COLKEY = "ECR_RETENUE";
	public static final String ECR_SENS_COLKEY = "ECR_SENS";
	public static final String ECR_SOURCE_COLKEY = "ECR_SOURCE";
	public static final String ECR_TYPE_COLKEY = "ECR_TYPE";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";

// Relationships
	public static final String CONVENTION_KEY = "convention";
	public static final String EXERCICE_KEY = "exercice";
	public static final String MOIS_KEY = "mois";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String ecrComp() {
    return (String) storedValueForKey(ECR_COMP_KEY);
  }

  public void setEcrComp(String value) {
    takeStoredValueForKey(value, ECR_COMP_KEY);
  }

  public String ecrEtat() {
    return (String) storedValueForKey(ECR_ETAT_KEY);
  }

  public void setEcrEtat(String value) {
    takeStoredValueForKey(value, ECR_ETAT_KEY);
  }

  public java.math.BigDecimal ecrMont() {
    return (java.math.BigDecimal) storedValueForKey(ECR_MONT_KEY);
  }

  public void setEcrMont(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECR_MONT_KEY);
  }

  public String ecrRetenue() {
    return (String) storedValueForKey(ECR_RETENUE_KEY);
  }

  public void setEcrRetenue(String value) {
    takeStoredValueForKey(value, ECR_RETENUE_KEY);
  }

  public String ecrSens() {
    return (String) storedValueForKey(ECR_SENS_KEY);
  }

  public void setEcrSens(String value) {
    takeStoredValueForKey(value, ECR_SENS_KEY);
  }

  public String ecrSource() {
    return (String) storedValueForKey(ECR_SOURCE_KEY);
  }

  public void setEcrSource(String value) {
    takeStoredValueForKey(value, ECR_SOURCE_KEY);
  }

  public String ecrType() {
    return (String) storedValueForKey(ECR_TYPE_KEY);
  }

  public void setEcrType(String value) {
    takeStoredValueForKey(value, ECR_TYPE_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public org.cocktail.papaye.server.metier.convention.EOConvention convention() {
    return (org.cocktail.papaye.server.metier.convention.EOConvention)storedValueForKey(CONVENTION_KEY);
  }

  public void setConventionRelationship(org.cocktail.papaye.server.metier.convention.EOConvention value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.convention.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOExercice exercice() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOExercice value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOOrgan organ() {
    return (org.cocktail.application.serveur.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.serveur.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public NSArray planComptable() {
    return (NSArray)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public NSArray planComptable(EOQualifier qualifier) {
    return planComptable(qualifier, null);
  }

  public NSArray planComptable(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = planComptable();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToPlanComptableRelationship(org.cocktail.papaye.server.metier.maracuja.EOPlanComptableExer object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
  }

  public void removeFromPlanComptableRelationship(org.cocktail.papaye.server.metier.maracuja.EOPlanComptableExer object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
  }

  public org.cocktail.papaye.server.metier.maracuja.EOPlanComptableExer createPlanComptableRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PlanComptableExer");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PLAN_COMPTABLE_KEY);
    return (org.cocktail.papaye.server.metier.maracuja.EOPlanComptableExer) eo;
  }

  public void deletePlanComptableRelationship(org.cocktail.papaye.server.metier.maracuja.EOPlanComptableExer object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPlanComptableRelationships() {
    Enumeration objects = planComptable().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePlanComptableRelationship((org.cocktail.papaye.server.metier.maracuja.EOPlanComptableExer)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOJefyEcritures avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOJefyEcritures createEOJefyEcritures(EOEditingContext editingContext, String pcoNum
			) {
    EOJefyEcritures eo = (EOJefyEcritures) createAndInsertInstance(editingContext, _EOJefyEcritures.ENTITY_NAME);    
		eo.setPcoNum(pcoNum);
    return eo;
  }

  
	  public EOJefyEcritures localInstanceIn(EOEditingContext editingContext) {
	  		return (EOJefyEcritures)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyEcritures creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyEcritures creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOJefyEcritures object = (EOJefyEcritures)createAndInsertInstance(editingContext, _EOJefyEcritures.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOJefyEcritures localInstanceIn(EOEditingContext editingContext, EOJefyEcritures eo) {
    EOJefyEcritures localInstance = (eo == null) ? null : (EOJefyEcritures)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOJefyEcritures#localInstanceIn a la place.
   */
	public static EOJefyEcritures localInstanceOf(EOEditingContext editingContext, EOJefyEcritures eo) {
		return EOJefyEcritures.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOJefyEcritures fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOJefyEcritures fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJefyEcritures eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJefyEcritures)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJefyEcritures fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJefyEcritures fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJefyEcritures eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJefyEcritures)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOJefyEcritures fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJefyEcritures eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJefyEcritures ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJefyEcritures fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
