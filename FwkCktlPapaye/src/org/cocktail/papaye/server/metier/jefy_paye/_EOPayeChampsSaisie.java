// _EOPayeChampsSaisie.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeChampsSaisie.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeChampsSaisie extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeChampsSaisie";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_statut_saisie";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pcsOrdre";

	public static final String AIDE_SAISIE_KEY = "aideSaisie";
	public static final String TEM_ABATTEMENT_KEY = "temAbattement";
	public static final String TEM_ABATTEMENT_OBLIG_KEY = "temAbattementOblig";
	public static final String TEM_COTISE_SOLIDARITE_KEY = "temCotiseSolidarite";
	public static final String TEM_DATE_FIN_KEY = "temDateFin";
	public static final String TEM_DATE_FIN_OBLIG_KEY = "temDateFinOblig";
	public static final String TEM_FREQUENCE_KEY = "temFrequence";
	public static final String TEM_FREQUENCE_OBLIG_KEY = "temFrequenceOblig";
	public static final String TEM_INDICE_KEY = "temIndice";
	public static final String TEM_INDICE_OBLIG_KEY = "temIndiceOblig";
	public static final String TEM_INDICE_ORIGINE_KEY = "temIndiceOrigine";
	public static final String TEM_INDICE_ORIGINE_OBLIG_KEY = "temIndiceOrigineOblig";
	public static final String TEM_MODE_CALCUL_KEY = "temModeCalcul";
	public static final String TEM_MONTANT_KEY = "temMontant";
	public static final String TEM_MONTANT_OBLIG_KEY = "temMontantOblig";
	public static final String TEM_NB_HEURES_KEY = "temNbHeures";
	public static final String TEM_NB_HEURES_OBLIG_KEY = "temNbHeuresOblig";
	public static final String TEM_NB_JOURS_KEY = "temNbJours";
	public static final String TEM_NB_JOURS_OBLIG_KEY = "temNbJoursOblig";
	public static final String TEM_POURCENT_PLAFOND_SECU_KEY = "temPourcentPlafondSecu";
	public static final String TEM_POURCENT_SMIC_KEY = "temPourcentSmic";
	public static final String TEM_POURCENT_SMIC_OBLIG_KEY = "temPourcentSmicOblig";
	public static final String TEM_QUOTITE_KEY = "temQuotite";
	public static final String TEM_QUOTITE_OBLIG_KEY = "temQuotiteOblig";
	public static final String TEM_REMUN_PERSO_KEY = "temRemunPerso";
	public static final String TEM_RUBRIQUES_PERSOS_KEY = "temRubriquesPersos";
	public static final String TEM_TAUX_HORAIRE_KEY = "temTauxHoraire";
	public static final String TEM_TAUX_HORAIRE_OBLIG_KEY = "temTauxHoraireOblig";
	public static final String TYPE_STATUT_KEY = "typeStatut";

//Colonnes dans la base de donnees
	public static final String AIDE_SAISIE_COLKEY = "aide_saisie";
	public static final String TEM_ABATTEMENT_COLKEY = "tem_abattement";
	public static final String TEM_ABATTEMENT_OBLIG_COLKEY = "tem_abattement_oblig";
	public static final String TEM_COTISE_SOLIDARITE_COLKEY = "TEM_COTISE_SOLIDARITE";
	public static final String TEM_DATE_FIN_COLKEY = "tem_date_fin";
	public static final String TEM_DATE_FIN_OBLIG_COLKEY = "tem_date_fin_oblig";
	public static final String TEM_FREQUENCE_COLKEY = "tem_frequence";
	public static final String TEM_FREQUENCE_OBLIG_COLKEY = "tem_frequence_oblig";
	public static final String TEM_INDICE_COLKEY = "tem_indice";
	public static final String TEM_INDICE_OBLIG_COLKEY = "tem_indice_oblig";
	public static final String TEM_INDICE_ORIGINE_COLKEY = "tem_indice_origine";
	public static final String TEM_INDICE_ORIGINE_OBLIG_COLKEY = "tem_indice_origine_oblig";
	public static final String TEM_MODE_CALCUL_COLKEY = "tem_mode_calcul";
	public static final String TEM_MONTANT_COLKEY = "tem_montant";
	public static final String TEM_MONTANT_OBLIG_COLKEY = "tem_montant_oblig";
	public static final String TEM_NB_HEURES_COLKEY = "tem_nb_heures";
	public static final String TEM_NB_HEURES_OBLIG_COLKEY = "tem_nb_heures_oblig";
	public static final String TEM_NB_JOURS_COLKEY = "tem_nb_jours";
	public static final String TEM_NB_JOURS_OBLIG_COLKEY = "tem_nb_jours_oblig";
	public static final String TEM_POURCENT_PLAFOND_SECU_COLKEY = "tem_pourcent_plafond_secu";
	public static final String TEM_POURCENT_SMIC_COLKEY = "tem_pourcent_smic";
	public static final String TEM_POURCENT_SMIC_OBLIG_COLKEY = "tem_pourcent_smic_oblig";
	public static final String TEM_QUOTITE_COLKEY = "tem_quotite";
	public static final String TEM_QUOTITE_OBLIG_COLKEY = "tem_quotite_oblig";
	public static final String TEM_REMUN_PERSO_COLKEY = "tem_remun_perso";
	public static final String TEM_RUBRIQUES_PERSOS_COLKEY = "tem_rubriques_perso";
	public static final String TEM_TAUX_HORAIRE_COLKEY = "tem_taux_horaire";
	public static final String TEM_TAUX_HORAIRE_OBLIG_COLKEY = "tem_taux_horaire_oblig";
	public static final String TYPE_STATUT_COLKEY = "TYPE_STATUT";

// Relationships
	public static final String STATUT_KEY = "statut";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String aideSaisie() {
    return (String) storedValueForKey(AIDE_SAISIE_KEY);
  }

  public void setAideSaisie(String value) {
    takeStoredValueForKey(value, AIDE_SAISIE_KEY);
  }

  public String temAbattement() {
    return (String) storedValueForKey(TEM_ABATTEMENT_KEY);
  }

  public void setTemAbattement(String value) {
    takeStoredValueForKey(value, TEM_ABATTEMENT_KEY);
  }

  public String temAbattementOblig() {
    return (String) storedValueForKey(TEM_ABATTEMENT_OBLIG_KEY);
  }

  public void setTemAbattementOblig(String value) {
    takeStoredValueForKey(value, TEM_ABATTEMENT_OBLIG_KEY);
  }

  public String temCotiseSolidarite() {
    return (String) storedValueForKey(TEM_COTISE_SOLIDARITE_KEY);
  }

  public void setTemCotiseSolidarite(String value) {
    takeStoredValueForKey(value, TEM_COTISE_SOLIDARITE_KEY);
  }

  public String temDateFin() {
    return (String) storedValueForKey(TEM_DATE_FIN_KEY);
  }

  public void setTemDateFin(String value) {
    takeStoredValueForKey(value, TEM_DATE_FIN_KEY);
  }

  public String temDateFinOblig() {
    return (String) storedValueForKey(TEM_DATE_FIN_OBLIG_KEY);
  }

  public void setTemDateFinOblig(String value) {
    takeStoredValueForKey(value, TEM_DATE_FIN_OBLIG_KEY);
  }

  public String temFrequence() {
    return (String) storedValueForKey(TEM_FREQUENCE_KEY);
  }

  public void setTemFrequence(String value) {
    takeStoredValueForKey(value, TEM_FREQUENCE_KEY);
  }

  public String temFrequenceOblig() {
    return (String) storedValueForKey(TEM_FREQUENCE_OBLIG_KEY);
  }

  public void setTemFrequenceOblig(String value) {
    takeStoredValueForKey(value, TEM_FREQUENCE_OBLIG_KEY);
  }

  public String temIndice() {
    return (String) storedValueForKey(TEM_INDICE_KEY);
  }

  public void setTemIndice(String value) {
    takeStoredValueForKey(value, TEM_INDICE_KEY);
  }

  public String temIndiceOblig() {
    return (String) storedValueForKey(TEM_INDICE_OBLIG_KEY);
  }

  public void setTemIndiceOblig(String value) {
    takeStoredValueForKey(value, TEM_INDICE_OBLIG_KEY);
  }

  public String temIndiceOrigine() {
    return (String) storedValueForKey(TEM_INDICE_ORIGINE_KEY);
  }

  public void setTemIndiceOrigine(String value) {
    takeStoredValueForKey(value, TEM_INDICE_ORIGINE_KEY);
  }

  public String temIndiceOrigineOblig() {
    return (String) storedValueForKey(TEM_INDICE_ORIGINE_OBLIG_KEY);
  }

  public void setTemIndiceOrigineOblig(String value) {
    takeStoredValueForKey(value, TEM_INDICE_ORIGINE_OBLIG_KEY);
  }

  public String temModeCalcul() {
    return (String) storedValueForKey(TEM_MODE_CALCUL_KEY);
  }

  public void setTemModeCalcul(String value) {
    takeStoredValueForKey(value, TEM_MODE_CALCUL_KEY);
  }

  public String temMontant() {
    return (String) storedValueForKey(TEM_MONTANT_KEY);
  }

  public void setTemMontant(String value) {
    takeStoredValueForKey(value, TEM_MONTANT_KEY);
  }

  public String temMontantOblig() {
    return (String) storedValueForKey(TEM_MONTANT_OBLIG_KEY);
  }

  public void setTemMontantOblig(String value) {
    takeStoredValueForKey(value, TEM_MONTANT_OBLIG_KEY);
  }

  public String temNbHeures() {
    return (String) storedValueForKey(TEM_NB_HEURES_KEY);
  }

  public void setTemNbHeures(String value) {
    takeStoredValueForKey(value, TEM_NB_HEURES_KEY);
  }

  public String temNbHeuresOblig() {
    return (String) storedValueForKey(TEM_NB_HEURES_OBLIG_KEY);
  }

  public void setTemNbHeuresOblig(String value) {
    takeStoredValueForKey(value, TEM_NB_HEURES_OBLIG_KEY);
  }

  public String temNbJours() {
    return (String) storedValueForKey(TEM_NB_JOURS_KEY);
  }

  public void setTemNbJours(String value) {
    takeStoredValueForKey(value, TEM_NB_JOURS_KEY);
  }

  public String temNbJoursOblig() {
    return (String) storedValueForKey(TEM_NB_JOURS_OBLIG_KEY);
  }

  public void setTemNbJoursOblig(String value) {
    takeStoredValueForKey(value, TEM_NB_JOURS_OBLIG_KEY);
  }

  public String temPourcentPlafondSecu() {
    return (String) storedValueForKey(TEM_POURCENT_PLAFOND_SECU_KEY);
  }

  public void setTemPourcentPlafondSecu(String value) {
    takeStoredValueForKey(value, TEM_POURCENT_PLAFOND_SECU_KEY);
  }

  public String temPourcentSmic() {
    return (String) storedValueForKey(TEM_POURCENT_SMIC_KEY);
  }

  public void setTemPourcentSmic(String value) {
    takeStoredValueForKey(value, TEM_POURCENT_SMIC_KEY);
  }

  public String temPourcentSmicOblig() {
    return (String) storedValueForKey(TEM_POURCENT_SMIC_OBLIG_KEY);
  }

  public void setTemPourcentSmicOblig(String value) {
    takeStoredValueForKey(value, TEM_POURCENT_SMIC_OBLIG_KEY);
  }

  public String temQuotite() {
    return (String) storedValueForKey(TEM_QUOTITE_KEY);
  }

  public void setTemQuotite(String value) {
    takeStoredValueForKey(value, TEM_QUOTITE_KEY);
  }

  public String temQuotiteOblig() {
    return (String) storedValueForKey(TEM_QUOTITE_OBLIG_KEY);
  }

  public void setTemQuotiteOblig(String value) {
    takeStoredValueForKey(value, TEM_QUOTITE_OBLIG_KEY);
  }

  public String temRemunPerso() {
    return (String) storedValueForKey(TEM_REMUN_PERSO_KEY);
  }

  public void setTemRemunPerso(String value) {
    takeStoredValueForKey(value, TEM_REMUN_PERSO_KEY);
  }

  public String temRubriquesPersos() {
    return (String) storedValueForKey(TEM_RUBRIQUES_PERSOS_KEY);
  }

  public void setTemRubriquesPersos(String value) {
    takeStoredValueForKey(value, TEM_RUBRIQUES_PERSOS_KEY);
  }

  public String temTauxHoraire() {
    return (String) storedValueForKey(TEM_TAUX_HORAIRE_KEY);
  }

  public void setTemTauxHoraire(String value) {
    takeStoredValueForKey(value, TEM_TAUX_HORAIRE_KEY);
  }

  public String temTauxHoraireOblig() {
    return (String) storedValueForKey(TEM_TAUX_HORAIRE_OBLIG_KEY);
  }

  public void setTemTauxHoraireOblig(String value) {
    takeStoredValueForKey(value, TEM_TAUX_HORAIRE_OBLIG_KEY);
  }

  public String typeStatut() {
    return (String) storedValueForKey(TYPE_STATUT_KEY);
  }

  public void setTypeStatut(String value) {
    takeStoredValueForKey(value, TYPE_STATUT_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut statut() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut)storedValueForKey(STATUT_KEY);
  }

  public void setStatutRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut oldValue = statut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STATUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STATUT_KEY);
    }
  }
  

/**
 * Créer une instance de EOPayeChampsSaisie avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeChampsSaisie createEOPayeChampsSaisie(EOEditingContext editingContext, String temAbattement
, String temAbattementOblig
, String temCotiseSolidarite
, String temDateFin
, String temDateFinOblig
, String temFrequence
, String temFrequenceOblig
, String temIndice
, String temIndiceOblig
, String temIndiceOrigine
, String temIndiceOrigineOblig
, String temModeCalcul
, String temMontant
, String temMontantOblig
, String temNbHeures
, String temNbHeuresOblig
, String temNbJours
, String temNbJoursOblig
, String temPourcentPlafondSecu
, String temPourcentSmic
, String temPourcentSmicOblig
, String temQuotite
, String temQuotiteOblig
, String temRemunPerso
, String temRubriquesPersos
, String temTauxHoraire
, String temTauxHoraireOblig
, String typeStatut
, org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut statut			) {
    EOPayeChampsSaisie eo = (EOPayeChampsSaisie) createAndInsertInstance(editingContext, _EOPayeChampsSaisie.ENTITY_NAME);    
		eo.setTemAbattement(temAbattement);
		eo.setTemAbattementOblig(temAbattementOblig);
		eo.setTemCotiseSolidarite(temCotiseSolidarite);
		eo.setTemDateFin(temDateFin);
		eo.setTemDateFinOblig(temDateFinOblig);
		eo.setTemFrequence(temFrequence);
		eo.setTemFrequenceOblig(temFrequenceOblig);
		eo.setTemIndice(temIndice);
		eo.setTemIndiceOblig(temIndiceOblig);
		eo.setTemIndiceOrigine(temIndiceOrigine);
		eo.setTemIndiceOrigineOblig(temIndiceOrigineOblig);
		eo.setTemModeCalcul(temModeCalcul);
		eo.setTemMontant(temMontant);
		eo.setTemMontantOblig(temMontantOblig);
		eo.setTemNbHeures(temNbHeures);
		eo.setTemNbHeuresOblig(temNbHeuresOblig);
		eo.setTemNbJours(temNbJours);
		eo.setTemNbJoursOblig(temNbJoursOblig);
		eo.setTemPourcentPlafondSecu(temPourcentPlafondSecu);
		eo.setTemPourcentSmic(temPourcentSmic);
		eo.setTemPourcentSmicOblig(temPourcentSmicOblig);
		eo.setTemQuotite(temQuotite);
		eo.setTemQuotiteOblig(temQuotiteOblig);
		eo.setTemRemunPerso(temRemunPerso);
		eo.setTemRubriquesPersos(temRubriquesPersos);
		eo.setTemTauxHoraire(temTauxHoraire);
		eo.setTemTauxHoraireOblig(temTauxHoraireOblig);
		eo.setTypeStatut(typeStatut);
    eo.setStatutRelationship(statut);
    return eo;
  }

  
	  public EOPayeChampsSaisie localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeChampsSaisie)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeChampsSaisie creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeChampsSaisie creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeChampsSaisie object = (EOPayeChampsSaisie)createAndInsertInstance(editingContext, _EOPayeChampsSaisie.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeChampsSaisie localInstanceIn(EOEditingContext editingContext, EOPayeChampsSaisie eo) {
    EOPayeChampsSaisie localInstance = (eo == null) ? null : (EOPayeChampsSaisie)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeChampsSaisie#localInstanceIn a la place.
   */
	public static EOPayeChampsSaisie localInstanceOf(EOEditingContext editingContext, EOPayeChampsSaisie eo) {
		return EOPayeChampsSaisie.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeChampsSaisie fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeChampsSaisie fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeChampsSaisie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeChampsSaisie)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeChampsSaisie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeChampsSaisie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeChampsSaisie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeChampsSaisie)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeChampsSaisie fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeChampsSaisie eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeChampsSaisie ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeChampsSaisie fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
