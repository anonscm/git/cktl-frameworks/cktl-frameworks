/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Fri Mar 14 09:16:38 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.metier.grhum.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOPayeEmployeur extends _EOPayeEmployeur {
    private final static String TYPE_ORIGINE = "ORIGINE";
    private final static String CODE_EMPLOYEUR_AUTRE="910";
    public EOPayeEmployeur() {
        super();
    }
	public String sirenEmployeur() {
		if (pempSiret() != null) {
			return pempSiret().substring(0,9);
		} else {
			return null;
		}
	}
	public String nicEmployeur() {
		if (pempSiret() != null) {
			return pempSiret().substring(9);
		} else {
			return null;
		}
	}
    public String codeEmployeur() {
    	if (pempCode() == null) {
    		return CODE_EMPLOYEUR_AUTRE;
    	} else {
    		return pempCode();
    	}
    }
    // méthodes de recherche : méthodes de classe
    /** methode de classe permettant de trouver les employeurs principaux d'un agent,
    classes par ordre alphabetique.
    */
    public static NSArray rechercherEmployeursDeType(EOIndividu agent,EOEditingContext editingContext,String type) {
        // agent
        NSMutableArray values = new NSMutableArray(agent);
        values.addObject(Constantes.VRAI);
        String stringQualifier = "agent = %@ AND temValide = %@";
        // employeur valide
        if (type != null) {
        	values.addObject(type);
        	stringQualifier += "  AND pempType = %@";
        }
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
            (stringQualifier,values);
        EOFetchSpecification fs = new EOFetchSpecification("PayeEmployeur",qualifier,null);
        return editingContext.objectsWithFetchSpecification(fs);
    }
    /** methode de classe permettant de trouver les employeurs principaux d'un agent,
    classes par ordre alphabetique.
    */
    public static NSArray rechercherEmployeurs(EOIndividu agent,EOEditingContext editingContext) {
     return rechercherEmployeursDeType(agent, editingContext, null);
    }
	/** methode de classe permettant de trouver l'employeur principal d'un agent
	retourne null si pas d'employeur principal
    */
	public static EOPayeEmployeur rechercherEmployeur(EOIndividu agent,EOEditingContext editingContext) {
		try { 
			return (EOPayeEmployeur)rechercherEmployeurs(agent,editingContext).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** methode de classe permettant de trouver l'employeur d'origine d'un agent d&acute;tache
	retourne null si pas d'employeur d'origine
    */
	public static EOPayeEmployeur rechercherEmployeurOrigine(EOIndividu agent,EOEditingContext editingContext) {
		try { 
			return (EOPayeEmployeur)rechercherEmployeursDeType(agent,editingContext,TYPE_ORIGINE).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
}
