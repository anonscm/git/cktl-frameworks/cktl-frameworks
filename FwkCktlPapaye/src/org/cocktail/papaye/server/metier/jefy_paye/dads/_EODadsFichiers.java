// _EODadsFichiers.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODadsFichiers.java instead.
package org.cocktail.papaye.server.metier.jefy_paye.dads;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EODadsFichiers extends  EOGenericRecord {
	public static final String ENTITY_NAME = "DadsFichiers";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.DADS_FICHIERS";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dafiId";

	public static final String CODE_RUBRIQUE_KEY = "codeRubrique";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String DAFI_CLASSEMENT_KEY = "dafiClassement";
	public static final String DAFI_PERIODE_DEBUT_KEY = "dafiPeriodeDebut";
	public static final String DAFI_PERIODE_FIN_KEY = "dafiPeriodeFin";
	public static final String DAFI_VALEUR_RUBRIQUE_KEY = "dafiValeurRubrique";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String TYPE_DADS_KEY = "typeDads";

// Attributs non visibles
	public static final String DAFI_ID_KEY = "dafiId";

//Colonnes dans la base de donnees
	public static final String CODE_RUBRIQUE_COLKEY = "CODE_RUBRIQUE";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String DAFI_CLASSEMENT_COLKEY = "DAFI_CLASSEMENT";
	public static final String DAFI_PERIODE_DEBUT_COLKEY = "DAFI_PERIODE_DEBUT";
	public static final String DAFI_PERIODE_FIN_COLKEY = "DAFI_PERIODE_FIN";
	public static final String DAFI_VALEUR_RUBRIQUE_COLKEY = "DAFI_VALEUR_RUBRIQUE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String TYPE_DADS_COLKEY = "TYPE_DADS";

	public static final String DAFI_ID_COLKEY = "DAFI_ID";


	// Relationships
	public static final String DADS_CODAGE_KEY = "dadsCodage";
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String codeRubrique() {
    return (String) storedValueForKey(CODE_RUBRIQUE_KEY);
  }

  public void setCodeRubrique(String value) {
    takeStoredValueForKey(value, CODE_RUBRIQUE_KEY);
  }

  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public Integer dafiClassement() {
    return (Integer) storedValueForKey(DAFI_CLASSEMENT_KEY);
  }

  public void setDafiClassement(Integer value) {
    takeStoredValueForKey(value, DAFI_CLASSEMENT_KEY);
  }

  public String dafiPeriodeDebut() {
    return (String) storedValueForKey(DAFI_PERIODE_DEBUT_KEY);
  }

  public void setDafiPeriodeDebut(String value) {
    takeStoredValueForKey(value, DAFI_PERIODE_DEBUT_KEY);
  }

  public String dafiPeriodeFin() {
    return (String) storedValueForKey(DAFI_PERIODE_FIN_KEY);
  }

  public void setDafiPeriodeFin(String value) {
    takeStoredValueForKey(value, DAFI_PERIODE_FIN_KEY);
  }

  public String dafiValeurRubrique() {
    return (String) storedValueForKey(DAFI_VALEUR_RUBRIQUE_KEY);
  }

  public void setDafiValeurRubrique(String value) {
    takeStoredValueForKey(value, DAFI_VALEUR_RUBRIQUE_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public String typeDads() {
    return (String) storedValueForKey(TYPE_DADS_KEY);
  }

  public void setTypeDads(String value) {
    takeStoredValueForKey(value, TYPE_DADS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeDadsCodage dadsCodage() {
    return (org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeDadsCodage)storedValueForKey(DADS_CODAGE_KEY);
  }

  public void setDadsCodageRelationship(org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeDadsCodage value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.dads.EOPayeDadsCodage oldValue = dadsCodage();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DADS_CODAGE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DADS_CODAGE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOIndividu individu() {
    return (org.cocktail.papaye.server.metier.grhum.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.papaye.server.metier.grhum.EOIndividu value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOStructure structure() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_KEY);
  }

  public void setStructureRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_KEY);
    }
  }
  

/**
 * Créer une instance de EODadsFichiers avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODadsFichiers createEODadsFichiers(EOEditingContext editingContext			) {
    EODadsFichiers eo = (EODadsFichiers) createAndInsertInstance(editingContext, _EODadsFichiers.ENTITY_NAME);    
    return eo;
  }

  
	  public EODadsFichiers localInstanceIn(EOEditingContext editingContext) {
	  		return (EODadsFichiers)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODadsFichiers creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODadsFichiers creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EODadsFichiers object = (EODadsFichiers)createAndInsertInstance(editingContext, _EODadsFichiers.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EODadsFichiers localInstanceIn(EOEditingContext editingContext, EODadsFichiers eo) {
    EODadsFichiers localInstance = (eo == null) ? null : (EODadsFichiers)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODadsFichiers#localInstanceIn a la place.
   */
	public static EODadsFichiers localInstanceOf(EOEditingContext editingContext, EODadsFichiers eo) {
		return EODadsFichiers.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODadsFichiers fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODadsFichiers fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODadsFichiers eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODadsFichiers)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODadsFichiers fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODadsFichiers fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODadsFichiers eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODadsFichiers)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODadsFichiers fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODadsFichiers eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODadsFichiers ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODadsFichiers fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
