// _EOKaElement.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOKaElement.java instead.
package org.cocktail.papaye.server.metier.jefy_paye.ka;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public abstract class _EOKaElement extends  EOGenericRecord {
	public static final String ENTITY_NAME = "KaElement";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.KA_ELEMENT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idelt";

	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_CATEGORIE_BUDGETAIRE_KEY = "cCategorieBudgetaire";
	public static final String C_NATURE_KEY = "cNature";
	public static final String C_PERIODICITE_KEY = "cPeriodicite";
	public static final String IDELT_KEY = "idelt";
	public static final String LC_ELEMENT_KEY = "lcElement";
	public static final String L_ELEMENT_KEY = "lElement";
	public static final String TEM_COT_RAFP_KEY = "temCotRafp";
	public static final String TEM_PRIME_INDEMNITE_KEY = "temPrimeIndemnite";
	public static final String TEM_RAFP_KEY = "temRafp";
	public static final String TEM_TIB_KEY = "temTib";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_CATEGORIE_COLKEY = "C_CATEGORIE";
	public static final String C_CATEGORIE_BUDGETAIRE_COLKEY = "C_CATEGORIE_BUDGETAIRE";
	public static final String C_NATURE_COLKEY = "C_NATURE";
	public static final String C_PERIODICITE_COLKEY = "C_PERIODICITE";
	public static final String IDELT_COLKEY = "IDELT";
	public static final String LC_ELEMENT_COLKEY = "LC_ELEMENT";
	public static final String L_ELEMENT_COLKEY = "L_ELEMENT";
	public static final String TEM_COT_RAFP_COLKEY = "TEM_COT_RAFP";
	public static final String TEM_PRIME_INDEMNITE_COLKEY = "TEM_PRIME_INDEMNITE";
	public static final String TEM_RAFP_COLKEY = "TEM_RAFP";
	public static final String TEM_TIB_COLKEY = "TEM_TIB";



	// Relationships



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cCategorie() {
    return (String) storedValueForKey(C_CATEGORIE_KEY);
  }

  public void setCCategorie(String value) {
    takeStoredValueForKey(value, C_CATEGORIE_KEY);
  }

  public String cCategorieBudgetaire() {
    return (String) storedValueForKey(C_CATEGORIE_BUDGETAIRE_KEY);
  }

  public void setCCategorieBudgetaire(String value) {
    takeStoredValueForKey(value, C_CATEGORIE_BUDGETAIRE_KEY);
  }

  public String cNature() {
    return (String) storedValueForKey(C_NATURE_KEY);
  }

  public void setCNature(String value) {
    takeStoredValueForKey(value, C_NATURE_KEY);
  }

  public Integer cPeriodicite() {
    return (Integer) storedValueForKey(C_PERIODICITE_KEY);
  }

  public void setCPeriodicite(Integer value) {
    takeStoredValueForKey(value, C_PERIODICITE_KEY);
  }

  public String idelt() {
    return (String) storedValueForKey(IDELT_KEY);
  }

  public void setIdelt(String value) {
    takeStoredValueForKey(value, IDELT_KEY);
  }

  public String lcElement() {
    return (String) storedValueForKey(LC_ELEMENT_KEY);
  }

  public void setLcElement(String value) {
    takeStoredValueForKey(value, LC_ELEMENT_KEY);
  }

  public String lElement() {
    return (String) storedValueForKey(L_ELEMENT_KEY);
  }

  public void setLElement(String value) {
    takeStoredValueForKey(value, L_ELEMENT_KEY);
  }

  public String temCotRafp() {
    return (String) storedValueForKey(TEM_COT_RAFP_KEY);
  }

  public void setTemCotRafp(String value) {
    takeStoredValueForKey(value, TEM_COT_RAFP_KEY);
  }

  public String temPrimeIndemnite() {
    return (String) storedValueForKey(TEM_PRIME_INDEMNITE_KEY);
  }

  public void setTemPrimeIndemnite(String value) {
    takeStoredValueForKey(value, TEM_PRIME_INDEMNITE_KEY);
  }

  public String temRafp() {
    return (String) storedValueForKey(TEM_RAFP_KEY);
  }

  public void setTemRafp(String value) {
    takeStoredValueForKey(value, TEM_RAFP_KEY);
  }

  public String temTib() {
    return (String) storedValueForKey(TEM_TIB_KEY);
  }

  public void setTemTib(String value) {
    takeStoredValueForKey(value, TEM_TIB_KEY);
  }


/**
 * Créer une instance de EOKaElement avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOKaElement createEOKaElement(EOEditingContext editingContext, Integer cPeriodicite
, String idelt
, String temCotRafp
, String temPrimeIndemnite
, String temRafp
, String temTib
			) {
    EOKaElement eo = (EOKaElement) createAndInsertInstance(editingContext, _EOKaElement.ENTITY_NAME);    
		eo.setCPeriodicite(cPeriodicite);
		eo.setIdelt(idelt);
		eo.setTemCotRafp(temCotRafp);
		eo.setTemPrimeIndemnite(temPrimeIndemnite);
		eo.setTemRafp(temRafp);
		eo.setTemTib(temTib);
    return eo;
  }

  
	  public EOKaElement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOKaElement)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOKaElement creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOKaElement creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOKaElement object = (EOKaElement)createAndInsertInstance(editingContext, _EOKaElement.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOKaElement localInstanceIn(EOEditingContext editingContext, EOKaElement eo) {
    EOKaElement localInstance = (eo == null) ? null : (EOKaElement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOKaElement#localInstanceIn a la place.
   */
	public static EOKaElement localInstanceOf(EOEditingContext editingContext, EOKaElement eo) {
		return EOKaElement.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOKaElement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOKaElement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOKaElement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOKaElement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOKaElement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOKaElement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOKaElement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOKaElement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOKaElement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOKaElement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOKaElement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOKaElement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
