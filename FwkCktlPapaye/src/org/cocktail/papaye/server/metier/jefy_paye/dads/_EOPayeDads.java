// _EOPayeDads.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeDads.java instead.
package org.cocktail.papaye.server.metier.jefy_paye.dads;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPayeDads extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PayeDads";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_dads";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dadsOrdre";

	public static final String ANNEE_EXERCICE_KEY = "anneeExercice";
	public static final String NB_AGENTS_KEY = "nbAgents";
	public static final String TOTAL_BRUT_FISCAL_KEY = "totalBrutFiscal";
	public static final String TOTAL_CRDS_KEY = "totalCrds";
	public static final String TOTAL_CSG_KEY = "totalCsg";
	public static final String TOTAL_DEPLAFONNE_KEY = "totalDeplafonne";
	public static final String TOTAL_IRCANTEC_A_KEY = "totalIrcantecA";
	public static final String TOTAL_IRCANTEC_B_KEY = "totalIrcantecB";
	public static final String TOTAL_NET_FISCAL_KEY = "totalNetFiscal";
	public static final String TOTAL_PLAFONNE_KEY = "totalPlafonne";
	public static final String TOTAL_RAFP_KEY = "totalRafp";
	public static final String TOTAL_TAXE_TR1_KEY = "totalTaxeTr1";
	public static final String TOTAL_TAXE_TR2_KEY = "totalTaxeTr2";
	public static final String TOTAL_TAXE_TR3_KEY = "totalTaxeTr3";
	public static final String TOTAL_TPG_KEY = "totalTpg";

// Attributs non visibles
	public static final String C_STRUCTURE_SIRET_KEY = "cStructureSiret";
	public static final String DADS_ORDRE_KEY = "dadsOrdre";

//Colonnes dans la base de donnees
	public static final String ANNEE_EXERCICE_COLKEY = "ANNEE_EXERCICE";
	public static final String NB_AGENTS_COLKEY = "NB_AGENTS";
	public static final String TOTAL_BRUT_FISCAL_COLKEY = "TOTAL_BRUT_FISCAL";
	public static final String TOTAL_CRDS_COLKEY = "TOTAL_CRDS";
	public static final String TOTAL_CSG_COLKEY = "TOTAL_CSG";
	public static final String TOTAL_DEPLAFONNE_COLKEY = "TOTAL_DEPLAFONNE";
	public static final String TOTAL_IRCANTEC_A_COLKEY = "TOTAL_IRCANTEC_A";
	public static final String TOTAL_IRCANTEC_B_COLKEY = "TOTAL_IRCANTEC_B";
	public static final String TOTAL_NET_FISCAL_COLKEY = "TOTAL_NET_FISCAL";
	public static final String TOTAL_PLAFONNE_COLKEY = "TOTAL_PLAFONNE";
	public static final String TOTAL_RAFP_COLKEY = "TOTAL_RAFP";
	public static final String TOTAL_TAXE_TR1_COLKEY = "TOTAL_TAXE_TR1";
	public static final String TOTAL_TAXE_TR2_COLKEY = "TOTAL_TAXE_TR2";
	public static final String TOTAL_TAXE_TR3_COLKEY = "TOTAL_TAXE_TR3";
	public static final String TOTAL_TPG_COLKEY = "TOTAL_TPG";

	public static final String C_STRUCTURE_SIRET_COLKEY = "C_STRUCTURE_SIRET";
	public static final String DADS_ORDRE_COLKEY = "DADS_ORDRE";


	// Relationships
	public static final String STRUCTURE_KEY = "structure";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer anneeExercice() {
    return (Integer) storedValueForKey(ANNEE_EXERCICE_KEY);
  }

  public void setAnneeExercice(Integer value) {
    takeStoredValueForKey(value, ANNEE_EXERCICE_KEY);
  }

  public Integer nbAgents() {
    return (Integer) storedValueForKey(NB_AGENTS_KEY);
  }

  public void setNbAgents(Integer value) {
    takeStoredValueForKey(value, NB_AGENTS_KEY);
  }

  public java.math.BigDecimal totalBrutFiscal() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_BRUT_FISCAL_KEY);
  }

  public void setTotalBrutFiscal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_BRUT_FISCAL_KEY);
  }

  public java.math.BigDecimal totalCrds() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_CRDS_KEY);
  }

  public void setTotalCrds(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_CRDS_KEY);
  }

  public java.math.BigDecimal totalCsg() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_CSG_KEY);
  }

  public void setTotalCsg(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_CSG_KEY);
  }

  public java.math.BigDecimal totalDeplafonne() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_DEPLAFONNE_KEY);
  }

  public void setTotalDeplafonne(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_DEPLAFONNE_KEY);
  }

  public java.math.BigDecimal totalIrcantecA() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_IRCANTEC_A_KEY);
  }

  public void setTotalIrcantecA(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_IRCANTEC_A_KEY);
  }

  public java.math.BigDecimal totalIrcantecB() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_IRCANTEC_B_KEY);
  }

  public void setTotalIrcantecB(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_IRCANTEC_B_KEY);
  }

  public java.math.BigDecimal totalNetFiscal() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_NET_FISCAL_KEY);
  }

  public void setTotalNetFiscal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_NET_FISCAL_KEY);
  }

  public java.math.BigDecimal totalPlafonne() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_PLAFONNE_KEY);
  }

  public void setTotalPlafonne(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_PLAFONNE_KEY);
  }

  public java.math.BigDecimal totalRafp() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_RAFP_KEY);
  }

  public void setTotalRafp(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_RAFP_KEY);
  }

  public java.math.BigDecimal totalTaxeTr1() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_TAXE_TR1_KEY);
  }

  public void setTotalTaxeTr1(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_TAXE_TR1_KEY);
  }

  public java.math.BigDecimal totalTaxeTr2() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_TAXE_TR2_KEY);
  }

  public void setTotalTaxeTr2(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_TAXE_TR2_KEY);
  }

  public java.math.BigDecimal totalTaxeTr3() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_TAXE_TR3_KEY);
  }

  public void setTotalTaxeTr3(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_TAXE_TR3_KEY);
  }

  public java.math.BigDecimal totalTpg() {
    return (java.math.BigDecimal) storedValueForKey(TOTAL_TPG_KEY);
  }

  public void setTotalTpg(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TOTAL_TPG_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EOStructure structure() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_KEY);
  }

  public void setStructureRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPayeDads avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeDads createEOPayeDads(EOEditingContext editingContext			) {
    EOPayeDads eo = (EOPayeDads) createAndInsertInstance(editingContext, _EOPayeDads.ENTITY_NAME);    
    return eo;
  }

  
	  public EOPayeDads localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeDads)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeDads creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeDads creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeDads object = (EOPayeDads)createAndInsertInstance(editingContext, _EOPayeDads.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeDads localInstanceIn(EOEditingContext editingContext, EOPayeDads eo) {
    EOPayeDads localInstance = (eo == null) ? null : (EOPayeDads)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeDads#localInstanceIn a la place.
   */
	public static EOPayeDads localInstanceOf(EOEditingContext editingContext, EOPayeDads eo) {
		return EOPayeDads.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeDads fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeDads fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeDads eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeDads)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeDads fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeDads fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeDads eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeDads)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeDads fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeDads eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeDads ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeDads fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
