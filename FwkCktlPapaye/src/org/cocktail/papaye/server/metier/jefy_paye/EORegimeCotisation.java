/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Fri Oct 15 12:51:23 Europe/Paris 2004 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import org.cocktail.papaye.server.common.Constantes;

import com.webobjects.foundation.NSKeyValueCoding;

public class EORegimeCotisation extends _EORegimeCotisation {
    private static final String TITULAIRE_FONCTION_PUBLIQUE_TERRITORIALE = "63";
    private static final String STAGIAIRE_FONCTION_PUBLIQUE_TERRITORIALE = "65";
    private static final String TITULAIRE_FONCTION_PUBLIQUE_HOSPITALIERE = "66";
    private static final String STAGIAIRE_FONCTION_PUBLIQUE_HOSPITALIERE = "68";
    private static final String FONCTIONNAIRE_PENSION_CIVILE = "122";
    private static final String  REGIME_GENERAL = "200";
    
   public EORegimeCotisation() {
        super();
    }

    // méthodes ajoutées
   /** retourne true si pas de regime complementaire
    */
   public boolean estSansComplementaire() {
   	 return (pregComplementaire() != null && pregComplementaire().equals(Constantes.SANS_COMPLEMENTAIRE));
   }
    /** retourne true si le regime complementaire du statut est l'Ircantec
     */
    public boolean estIrcantec() {
    	 return (pregComplementaire() != null && pregComplementaire().equals(Constantes.IRCANTEC));
    }
    /** retourne true si le regime complementaire du statut est celui des intermittents (Capricas ou Carcicas)
     */
    public boolean estRegimeIntermittent() {
    	 return pregComplementaire() != null && (pregComplementaire().equals(Constantes.CAPRICAS) || pregComplementaire().equals(Constantes.CARCICAS));
    }
    /** retourne true si le le regime complementaire du statut est la CNRACL */
    public boolean estCNRACL() {
   	 return (pregComplementaire() != null && pregComplementaire().equals(Constantes.CNRACL));
   }
    /** retourne true si le le regime complementaire du statut est la Pension Civile */
    public boolean estSRE() {
   	 return (pregComplementaire() != null && pregComplementaire().equals(Constantes.SRE));
   }
    /** retourne true si le le regime complementaire du statut est la RAFP */
    public boolean estRAFP() {
   	 return (pregComplementaire() != null && pregComplementaire().equals(Constantes.RAFP));
   }
    /** Retourne le regime complementaire de la RAFP */
    public String pregComplementaireRAFP() {
    	return Constantes.RAFP;
    }
    /** retourne le regime de base si il est identique pour les quatre autres cotisations sinon nul */
    // DADS
    /** retourne le regime maladie si le regime de base n'a pas ete renseigne sinon nul */
    public String regimeMaladie() {
    	return pregRegimeMaladie();
    }
    /** retourne le regime AT si le regime de base n'a pas ete renseigne sinon nul */
    public String regimeAT() {
		return pregRegimeAT();
    }
    	/** retourne le regime maladie si le regime de base n'a pas ete renseigne sinon nul */
    public String regimeVieillesse() {
    	
//    		if (memesRegimes())
//    			return null;
//    		else
    			return pregRegimeVieillesse();
    		
    }

    
    /** retourne true si le regime de cotisation est celui d'un fonctionnaire avec pension civile */
    public boolean cotisePensionCivile() {
       		return pregRegimeVieillesse() != null && (pregRegimeVieillesse().equals(FONCTIONNAIRE_PENSION_CIVILE));
    }
    
    
    /** retourne true si les codes (BasePlafonnee,SFT, CSG,CSGReduite, TaxeSurSalaire) sont egaux
     * et si les types de cotisation sont les m&ecirc;mes
     */
    public boolean regimeIdentique(EORegimeCotisation regimeCotisation) { 
    	
    		if (
    			egal(regimeCotisation,"pregCodeBasePlafonnee") == false) {
       			return false;
    		} else if (egal(regimeCotisation,"pregCodeCRDS") == false) {
       			return false;
    		} else if (egal(regimeCotisation,"pregCodeCSG") == false) {
       			return false;
    		} else if (egal(regimeCotisation,"pregCodeCSGReduite") == false) {
       			return false; 
    		} else if (egal(regimeCotisation,"pregTaxeSurSalaire") == false) {
       			return false;
    		} else if (egal(regimeCotisation,"pregComplementaire") == false) {
       			return false;
    		} else if (egal(regimeCotisation,"pregExoUrssaf") == false) {
       			return false;
    		} else if (egal(regimeCotisation,"pregRegimeMaladie") == false) {
       			return false;
    		} else if (egal(regimeCotisation,"pregRegimeAT") == false) {
       			return false;
    		} else if (egal(regimeCotisation,"pregRegimeVieillesse") == false)
       			return false;
    		
    		return true;
    }
    public boolean estFonctionPubliqueTerritoriale() {
    		return pregStatutProf() != null && (pregStatutProf().equals(TITULAIRE_FONCTION_PUBLIQUE_TERRITORIALE) || pregStatutProf().equals(STAGIAIRE_FONCTION_PUBLIQUE_TERRITORIALE));
    }
    public boolean estFonctionPubliqueHospitaliere() {
		return pregStatutProf() != null && (pregStatutProf().equals(TITULAIRE_FONCTION_PUBLIQUE_HOSPITALIERE) || pregStatutProf().equals(STAGIAIRE_FONCTION_PUBLIQUE_HOSPITALIERE));
    }
    public String extensionAlsaceLorraine() {
    	if ((regimeMaladie() != null && regimeMaladie().equals(REGIME_GENERAL)) ||
    			(regimeMaladie() != null && regimeMaladie().equals(REGIME_GENERAL))) {
    		EOPayeParametres param = EOPayeParametres.rechercherParametre(editingContext(), "REGIME_ALSACE_LORRAINE");
    		if (param != null && param.paramValue().equals(Constantes.VRAI)) {
    			return "01";
    		} else {
    			return null;
    		}
    	} else {
			return null;
		}
    }
    // méthodes privées
    private boolean memesRegimes() {
    	
    		if (pregRegimeAT() == null && pregRegimeMaladie() == null && pregRegimeVieillesse() == null )
    			return true;
    		
    		return pregRegimeAT().equals(pregRegimeMaladie()) && pregRegimeAT().equals(pregRegimeVieillesse());
    		
    }
    
    private boolean egal(EORegimeCotisation regimeCotisation,String cle) {
    	
    		String valeur = (String)NSKeyValueCoding.DefaultImplementation.valueForKey(regimeCotisation,cle);
    		String valeur1 = (String)NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
    		
    		boolean result = false;
    		if (valeur == null && valeur1 == null) {
    			result = true;
    		} else if (valeur != null && valeur1 != null) {
    			result = valeur.equals(valeur1);
    		}
 
    		return result;
    }
}
