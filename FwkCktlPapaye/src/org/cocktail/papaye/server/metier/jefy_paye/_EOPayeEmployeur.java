// _EOPayeEmployeur.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeEmployeur.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeEmployeur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeEmployeur";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_employeur";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pempOrdre";

	public static final String PEMP_ADRESSE1_KEY = "pempAdresse1";
	public static final String PEMP_ADRESSE2_KEY = "pempAdresse2";
	public static final String PEMP_CODE_KEY = "pempCode";
	public static final String PEMP_CODE_POSTAL_KEY = "pempCodePostal";
	public static final String PEMP_NOM_KEY = "pempNom";
	public static final String PEMP_SIRET_KEY = "pempSiret";
	public static final String PEMP_TYPE_KEY = "pempType";
	public static final String PEMP_VILLE_KEY = "pempVille";
	public static final String TEM_VALIDE_KEY = "temValide";

//Colonnes dans la base de donnees
	public static final String PEMP_ADRESSE1_COLKEY = "pemp_adresse1";
	public static final String PEMP_ADRESSE2_COLKEY = "pemp_adresse2";
	public static final String PEMP_CODE_COLKEY = "pemp_code";
	public static final String PEMP_CODE_POSTAL_COLKEY = "pemp_code_postal";
	public static final String PEMP_NOM_COLKEY = "pemp_nom";
	public static final String PEMP_SIRET_COLKEY = "pemp_siret";
	public static final String PEMP_TYPE_COLKEY = "pemp_type";
	public static final String PEMP_VILLE_COLKEY = "pemp_ville";
	public static final String TEM_VALIDE_COLKEY = "tem_Valide";

// Relationships
	public static final String AGENT_KEY = "agent";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String pempAdresse1() {
    return (String) storedValueForKey(PEMP_ADRESSE1_KEY);
  }

  public void setPempAdresse1(String value) {
    takeStoredValueForKey(value, PEMP_ADRESSE1_KEY);
  }

  public String pempAdresse2() {
    return (String) storedValueForKey(PEMP_ADRESSE2_KEY);
  }

  public void setPempAdresse2(String value) {
    takeStoredValueForKey(value, PEMP_ADRESSE2_KEY);
  }

  public String pempCode() {
    return (String) storedValueForKey(PEMP_CODE_KEY);
  }

  public void setPempCode(String value) {
    takeStoredValueForKey(value, PEMP_CODE_KEY);
  }

  public String pempCodePostal() {
    return (String) storedValueForKey(PEMP_CODE_POSTAL_KEY);
  }

  public void setPempCodePostal(String value) {
    takeStoredValueForKey(value, PEMP_CODE_POSTAL_KEY);
  }

  public String pempNom() {
    return (String) storedValueForKey(PEMP_NOM_KEY);
  }

  public void setPempNom(String value) {
    takeStoredValueForKey(value, PEMP_NOM_KEY);
  }

  public String pempSiret() {
    return (String) storedValueForKey(PEMP_SIRET_KEY);
  }

  public void setPempSiret(String value) {
    takeStoredValueForKey(value, PEMP_SIRET_KEY);
  }

  public String pempType() {
    return (String) storedValueForKey(PEMP_TYPE_KEY);
  }

  public void setPempType(String value) {
    takeStoredValueForKey(value, PEMP_TYPE_KEY);
  }

  public String pempVille() {
    return (String) storedValueForKey(PEMP_VILLE_KEY);
  }

  public void setPempVille(String value) {
    takeStoredValueForKey(value, PEMP_VILLE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EOIndividu agent() {
    return (org.cocktail.papaye.server.metier.grhum.EOIndividu)storedValueForKey(AGENT_KEY);
  }

  public void setAgentRelationship(org.cocktail.papaye.server.metier.grhum.EOIndividu value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOIndividu oldValue = agent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, AGENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, AGENT_KEY);
    }
  }
  

/**
 * Créer une instance de EOPayeEmployeur avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeEmployeur createEOPayeEmployeur(EOEditingContext editingContext, String pempNom
, String pempType
, String temValide
			) {
    EOPayeEmployeur eo = (EOPayeEmployeur) createAndInsertInstance(editingContext, _EOPayeEmployeur.ENTITY_NAME);    
		eo.setPempNom(pempNom);
		eo.setPempType(pempType);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOPayeEmployeur localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeEmployeur)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeEmployeur creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeEmployeur creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeEmployeur object = (EOPayeEmployeur)createAndInsertInstance(editingContext, _EOPayeEmployeur.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeEmployeur localInstanceIn(EOEditingContext editingContext, EOPayeEmployeur eo) {
    EOPayeEmployeur localInstance = (eo == null) ? null : (EOPayeEmployeur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeEmployeur#localInstanceIn a la place.
   */
	public static EOPayeEmployeur localInstanceOf(EOEditingContext editingContext, EOPayeEmployeur eo) {
		return EOPayeEmployeur.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeEmployeur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeEmployeur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeEmployeur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeEmployeur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeEmployeur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeEmployeur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeEmployeur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeEmployeur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeEmployeur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeEmployeur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeEmployeur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeEmployeur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
