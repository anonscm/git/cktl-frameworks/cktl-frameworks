// _EOJefyEcrituresReversement.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJefyEcrituresReversement.java instead.
package org.cocktail.papaye.server.metier.jefy_paye.budget;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOJefyEcrituresReversement extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "JefyEcrituresReversement";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.JEFY_ECRITURES_REVERSEMENT";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "ecrOrdre";

	public static final String ECR_COMP_KEY = "ecrComp";
	public static final String ECR_ETAT_KEY = "ecrEtat";
	public static final String ECR_MONT_KEY = "ecrMont";
	public static final String ECR_SENS_KEY = "ecrSens";
	public static final String ECR_SOURCE_KEY = "ecrSource";
	public static final String ECR_TYPE_KEY = "ecrType";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PCO_NUM_CONTREPARTIE_KEY = "pcoNumContrepartie";

//Colonnes dans la base de donnees
	public static final String ECR_COMP_COLKEY = "ECR_COMP";
	public static final String ECR_ETAT_COLKEY = "ECR_ETAT";
	public static final String ECR_MONT_COLKEY = "ECR_MONT";
	public static final String ECR_SENS_COLKEY = "ECR_SENS";
	public static final String ECR_SOURCE_COLKEY = "ECR_SOURCE";
	public static final String ECR_TYPE_COLKEY = "ECR_TYPE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PCO_NUM_CONTREPARTIE_COLKEY = "PCO_NUM_CONTREPARTIE";

// Relationships
	public static final String DEPENSE_REVERSEMENT_KEY = "depenseReversement";
	public static final String REVERSEMENT_KEY = "reversement";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String ecrComp() {
    return (String) storedValueForKey(ECR_COMP_KEY);
  }

  public void setEcrComp(String value) {
    takeStoredValueForKey(value, ECR_COMP_KEY);
  }

  public String ecrEtat() {
    return (String) storedValueForKey(ECR_ETAT_KEY);
  }

  public void setEcrEtat(String value) {
    takeStoredValueForKey(value, ECR_ETAT_KEY);
  }

  public java.math.BigDecimal ecrMont() {
    return (java.math.BigDecimal) storedValueForKey(ECR_MONT_KEY);
  }

  public void setEcrMont(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECR_MONT_KEY);
  }

  public String ecrSens() {
    return (String) storedValueForKey(ECR_SENS_KEY);
  }

  public void setEcrSens(String value) {
    takeStoredValueForKey(value, ECR_SENS_KEY);
  }

  public String ecrSource() {
    return (String) storedValueForKey(ECR_SOURCE_KEY);
  }

  public void setEcrSource(String value) {
    takeStoredValueForKey(value, ECR_SOURCE_KEY);
  }

  public String ecrType() {
    return (String) storedValueForKey(ECR_TYPE_KEY);
  }

  public void setEcrType(String value) {
    takeStoredValueForKey(value, ECR_TYPE_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public String pcoNumContrepartie() {
    return (String) storedValueForKey(PCO_NUM_CONTREPARTIE_KEY);
  }

  public void setPcoNumContrepartie(String value) {
    takeStoredValueForKey(value, PCO_NUM_CONTREPARTIE_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_depense.EODepenseBudget depenseReversement() {
    return (org.cocktail.papaye.server.metier.jefy_depense.EODepenseBudget)storedValueForKey(DEPENSE_REVERSEMENT_KEY);
  }

  public void setDepenseReversementRelationship(org.cocktail.papaye.server.metier.jefy_depense.EODepenseBudget value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_depense.EODepenseBudget oldValue = depenseReversement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_REVERSEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_REVERSEMENT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.budget.EOJefyReversements reversement() {
    return (org.cocktail.papaye.server.metier.jefy_paye.budget.EOJefyReversements)storedValueForKey(REVERSEMENT_KEY);
  }

  public void setReversementRelationship(org.cocktail.papaye.server.metier.jefy_paye.budget.EOJefyReversements value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.budget.EOJefyReversements oldValue = reversement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REVERSEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, REVERSEMENT_KEY);
    }
  }
  

/**
 * Créer une instance de EOJefyEcrituresReversement avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOJefyEcrituresReversement createEOJefyEcrituresReversement(EOEditingContext editingContext			) {
    EOJefyEcrituresReversement eo = (EOJefyEcrituresReversement) createAndInsertInstance(editingContext, _EOJefyEcrituresReversement.ENTITY_NAME);    
    return eo;
  }

  
	  public EOJefyEcrituresReversement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOJefyEcrituresReversement)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyEcrituresReversement creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyEcrituresReversement creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOJefyEcrituresReversement object = (EOJefyEcrituresReversement)createAndInsertInstance(editingContext, _EOJefyEcrituresReversement.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOJefyEcrituresReversement localInstanceIn(EOEditingContext editingContext, EOJefyEcrituresReversement eo) {
    EOJefyEcrituresReversement localInstance = (eo == null) ? null : (EOJefyEcrituresReversement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOJefyEcrituresReversement#localInstanceIn a la place.
   */
	public static EOJefyEcrituresReversement localInstanceOf(EOEditingContext editingContext, EOJefyEcrituresReversement eo) {
		return EOJefyEcrituresReversement.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOJefyEcrituresReversement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOJefyEcrituresReversement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJefyEcrituresReversement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJefyEcrituresReversement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJefyEcrituresReversement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJefyEcrituresReversement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJefyEcrituresReversement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJefyEcrituresReversement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOJefyEcrituresReversement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJefyEcrituresReversement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJefyEcrituresReversement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJefyEcrituresReversement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
