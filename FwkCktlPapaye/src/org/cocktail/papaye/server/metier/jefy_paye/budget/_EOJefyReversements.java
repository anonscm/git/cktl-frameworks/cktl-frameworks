// _EOJefyReversements.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJefyReversements.java instead.
package org.cocktail.papaye.server.metier.jefy_paye.budget;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOJefyReversements extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "JefyReversements";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.JEFY_REVERSEMENTS";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "jreOrdre";

	public static final String JRE_ETAT_KEY = "jreEtat";
	public static final String JRE_LIBELLE_KEY = "jreLibelle";
	public static final String JRE_OBSERVATIONS_KEY = "jreObservations";
	public static final String TITRE_INFOS_KEY = "titreInfos";

//Colonnes dans la base de donnees
	public static final String JRE_ETAT_COLKEY = "JRE_ETAT";
	public static final String JRE_LIBELLE_COLKEY = "JRE_LIBELLE";
	public static final String JRE_OBSERVATIONS_COLKEY = "JRE_OBSERVATIONS";
	public static final String TITRE_INFOS_COLKEY = "TITRE_INFOS";

// Relationships
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String CONVENTION_KEY = "convention";
	public static final String EXERCICE_KEY = "exercice";
	public static final String HISTORIQUE_KEY = "historique";
	public static final String JEFY_ECRITURES_REVERSEMENTS_KEY = "jefyEcrituresReversements";
	public static final String MOIS_KEY = "mois";
	public static final String ORGAN_KEY = "organ";
	public static final String TYPE_ACTION_KEY = "typeAction";
	public static final String TYPE_CREDIT_KEY = "typeCredit";
	public static final String VALIDATION_KEY = "validation";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String jreEtat() {
    return (String) storedValueForKey(JRE_ETAT_KEY);
  }

  public void setJreEtat(String value) {
    takeStoredValueForKey(value, JRE_ETAT_KEY);
  }

  public String jreLibelle() {
    return (String) storedValueForKey(JRE_LIBELLE_KEY);
  }

  public void setJreLibelle(String value) {
    takeStoredValueForKey(value, JRE_LIBELLE_KEY);
  }

  public String jreObservations() {
    return (String) storedValueForKey(JRE_OBSERVATIONS_KEY);
  }

  public void setJreObservations(String value) {
    takeStoredValueForKey(value, JRE_OBSERVATIONS_KEY);
  }

  public String titreInfos() {
    return (String) storedValueForKey(TITRE_INFOS_KEY);
  }

  public void setTitreInfos(String value) {
    takeStoredValueForKey(value, TITRE_INFOS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytique oldValue = codeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.convention.EOConvention convention() {
    return (org.cocktail.papaye.server.metier.convention.EOConvention)storedValueForKey(CONVENTION_KEY);
  }

  public void setConventionRelationship(org.cocktail.papaye.server.metier.convention.EOConvention value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.convention.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOExercice exercice() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOExercice value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto historique() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto)storedValueForKey(HISTORIQUE_KEY);
  }

  public void setHistoriqueRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto oldValue = historique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, HISTORIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, HISTORIQUE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOOrgan organ() {
    return (org.cocktail.application.serveur.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.serveur.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOTypeAction typeAction() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOTypeAction)storedValueForKey(TYPE_ACTION_KEY);
  }

  public void setTypeActionRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOTypeAction value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOTypeAction oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACTION_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOTypeCredit typeCredit() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid validation() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid)storedValueForKey(VALIDATION_KEY);
  }

  public void setValidationRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid oldValue = validation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, VALIDATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, VALIDATION_KEY);
    }
  }
  
  public NSArray jefyEcrituresReversements() {
    return (NSArray)storedValueForKey(JEFY_ECRITURES_REVERSEMENTS_KEY);
  }

  public NSArray jefyEcrituresReversements(EOQualifier qualifier) {
    return jefyEcrituresReversements(qualifier, null, false);
  }

  public NSArray jefyEcrituresReversements(EOQualifier qualifier, boolean fetch) {
    return jefyEcrituresReversements(qualifier, null, fetch);
  }

  public NSArray jefyEcrituresReversements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.budget.EOJefyEcrituresReversement.REVERSEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.budget.EOJefyEcrituresReversement.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = jefyEcrituresReversements();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToJefyEcrituresReversementsRelationship(org.cocktail.papaye.server.metier.jefy_paye.budget.EOJefyEcrituresReversement object) {
    addObjectToBothSidesOfRelationshipWithKey(object, JEFY_ECRITURES_REVERSEMENTS_KEY);
  }

  public void removeFromJefyEcrituresReversementsRelationship(org.cocktail.papaye.server.metier.jefy_paye.budget.EOJefyEcrituresReversement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, JEFY_ECRITURES_REVERSEMENTS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.budget.EOJefyEcrituresReversement createJefyEcrituresReversementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("JefyEcrituresReversement");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, JEFY_ECRITURES_REVERSEMENTS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.budget.EOJefyEcrituresReversement) eo;
  }

  public void deleteJefyEcrituresReversementsRelationship(org.cocktail.papaye.server.metier.jefy_paye.budget.EOJefyEcrituresReversement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, JEFY_ECRITURES_REVERSEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllJefyEcrituresReversementsRelationships() {
    Enumeration objects = jefyEcrituresReversements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteJefyEcrituresReversementsRelationship((org.cocktail.papaye.server.metier.jefy_paye.budget.EOJefyEcrituresReversement)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOJefyReversements avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOJefyReversements createEOJefyReversements(EOEditingContext editingContext, org.cocktail.papaye.server.metier.jefy_admin.EOExercice exercice, org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois			) {
    EOJefyReversements eo = (EOJefyReversements) createAndInsertInstance(editingContext, _EOJefyReversements.ENTITY_NAME);    
    eo.setExerciceRelationship(exercice);
    eo.setMoisRelationship(mois);
    return eo;
  }

  
	  public EOJefyReversements localInstanceIn(EOEditingContext editingContext) {
	  		return (EOJefyReversements)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyReversements creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyReversements creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOJefyReversements object = (EOJefyReversements)createAndInsertInstance(editingContext, _EOJefyReversements.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOJefyReversements localInstanceIn(EOEditingContext editingContext, EOJefyReversements eo) {
    EOJefyReversements localInstance = (eo == null) ? null : (EOJefyReversements)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOJefyReversements#localInstanceIn a la place.
   */
	public static EOJefyReversements localInstanceOf(EOEditingContext editingContext, EOJefyReversements eo) {
		return EOJefyReversements.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOJefyReversements fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOJefyReversements fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJefyReversements eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJefyReversements)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJefyReversements fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJefyReversements fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJefyReversements eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJefyReversements)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOJefyReversements fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJefyReversements eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJefyReversements ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJefyReversements fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
