/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.papaye.server.metier.jefy_paye;

import org.cocktail.papaye.server.calcul.cotisation.CalculTaxeSurSalaire;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;


public class EOPayeCode extends _EOPayeCode {

    /** Code rubrique utilise pour le cumul Base Securite Sociale */
    public final static String CODEBASESS = "BASESECU";
    /** Code rubrique utilise pour le cumul Net */
    public final static String NET = "CUMULNET";
    /** Code rubrique utilise pour le cumul Net Imposable */
    public final static String NETIMPOS = "NETIMPOS";
    /** Code rubrique utilise pour le cumul de la remuneration principale
        brute Mensuelle */
    public final static String REMUN_BRUT = "TRMTBASE";
    /** code global de l'ensemble des remunerations hors Avantages en Nature */
    public final static String REMUN_TOT = "REMUNTOT";
    /** code global de l'ensemble des remunerations y compris Avantages en Nature */
    public final static String REMUN_TOT_ET_AVANTAGES = "REMUNTOA";
    /** Code rubrique utilise pour le cumul C&ucirc;t total Patronale */
    public final static String COUTPATRONAL = "COUTTPAT";

    public EOPayeCode() {
        super();
    }

    
    public static EOPayeCode rechercherCode(EOEditingContext editingContext,String code) {
        // Rechercher sur le code
        NSArray values = new NSArray(code);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat
            ("pcodCode = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification(EOPayeCode.ENTITY_NAME,qualifier, null);
        NSArray codes = editingContext.objectsWithFetchSpecification(fs);
        if (codes == null || codes.count() == 0) {	// pas de code trouvé
            return null;
        } else {
            return (EOPayeCode)codes.objectAtIndex(0);
        }
    }

    public boolean estCodePourTauxChargePatronale() {
		String[] codesARejeter = {"TXVTRRGP","TXSMICST","TXVTRFOP","TXBRSMIC","TXVTRMRP","TXSMICEC","TXTRAPPP","TXREDUAS","TXVTCECP"};
		if (pcodCode().equals(CalculTaxeSurSalaire.TAUX1)) {
			return true;
		}
		if (pcodCode().startsWith("TX") == false) {
			return false;
		}
		for (int i = 0; i < codesARejeter.length;i++) {
			if (pcodCode().equals(codesARejeter[i])) {
				return false;
			}
		}
		return true;
}

    
    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}
