// _EOEffectifsActions.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEffectifsActions.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOEffectifsActions extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "EffectifsActions";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.V_EFFECTIFS_ACTIONS";

// Attributes

	public static final String ANNEE_KEY = "annee";
	public static final String C_STRUCTURE_SIRET_KEY = "cStructureSiret";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String ETPT_KEY = "etpt";
	public static final String QUOTITE_PAIEMENT_KEY = "quotitePaiement";
	public static final String QUOTITE_TRAVAIL_KEY = "quotiteTravail";

//Colonnes dans la base de donnees
	public static final String ANNEE_COLKEY = "ANNEE";
	public static final String C_STRUCTURE_SIRET_COLKEY = "C_STRUCTURE_SIRET";
	public static final String DATE_DEBUT_COLKEY = "DATE_DEBUT";
	public static final String DATE_FIN_COLKEY = "DATE_FIN";
	public static final String ETPT_COLKEY = "ETPT";
	public static final String QUOTITE_PAIEMENT_COLKEY = "QUOTITE_PAIEMENT";
	public static final String QUOTITE_TRAVAIL_COLKEY = "QUOTITE_TRAVAIL";

// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String MOIS_KEY = "mois";
	public static final String SECTEUR_KEY = "secteur";
	public static final String STATUT_KEY = "statut";
	public static final String STRUCTURE_SIRET_KEY = "structureSiret";
	public static final String TYPE_ACTION_KEY = "typeAction";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer annee() {
    return (Integer) storedValueForKey(ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    takeStoredValueForKey(value, ANNEE_KEY);
  }

  public String cStructureSiret() {
    return (String) storedValueForKey(C_STRUCTURE_SIRET_KEY);
  }

  public void setCStructureSiret(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_SIRET_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public Double etpt() {
    return (Double) storedValueForKey(ETPT_KEY);
  }

  public void setEtpt(Double value) {
    takeStoredValueForKey(value, ETPT_KEY);
  }

  public Double quotitePaiement() {
    return (Double) storedValueForKey(QUOTITE_PAIEMENT_KEY);
  }

  public void setQuotitePaiement(Double value) {
    takeStoredValueForKey(value, QUOTITE_PAIEMENT_KEY);
  }

  public Double quotiteTravail() {
    return (Double) storedValueForKey(QUOTITE_TRAVAIL_KEY);
  }

  public void setQuotiteTravail(Double value) {
    takeStoredValueForKey(value, QUOTITE_TRAVAIL_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EOIndividu individu() {
    return (org.cocktail.papaye.server.metier.grhum.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.papaye.server.metier.grhum.EOIndividu value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur secteur() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur)storedValueForKey(SECTEUR_KEY);
  }

  public void setSecteurRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur oldValue = secteur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SECTEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SECTEUR_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut statut() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut)storedValueForKey(STATUT_KEY);
  }

  public void setStatutRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut oldValue = statut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STATUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STATUT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOStructure structureSiret() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_SIRET_KEY);
  }

  public void setStructureSiretRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structureSiret();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_SIRET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_SIRET_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOTypeAction typeAction() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOTypeAction)storedValueForKey(TYPE_ACTION_KEY);
  }

  public void setTypeActionRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOTypeAction value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOTypeAction oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACTION_KEY);
    }
  }
  

/**
 * Créer une instance de EOEffectifsActions avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEffectifsActions createEOEffectifsActions(EOEditingContext editingContext, Integer annee
, Double etpt
, Double quotitePaiement
, Double quotiteTravail
			) {
    EOEffectifsActions eo = (EOEffectifsActions) createAndInsertInstance(editingContext, _EOEffectifsActions.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setEtpt(etpt);
		eo.setQuotitePaiement(quotitePaiement);
		eo.setQuotiteTravail(quotiteTravail);
    return eo;
  }

  
	  public EOEffectifsActions localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEffectifsActions)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEffectifsActions creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEffectifsActions creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOEffectifsActions object = (EOEffectifsActions)createAndInsertInstance(editingContext, _EOEffectifsActions.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOEffectifsActions localInstanceIn(EOEditingContext editingContext, EOEffectifsActions eo) {
    EOEffectifsActions localInstance = (eo == null) ? null : (EOEffectifsActions)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEffectifsActions#localInstanceIn a la place.
   */
	public static EOEffectifsActions localInstanceOf(EOEditingContext editingContext, EOEffectifsActions eo) {
		return EOEffectifsActions.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEffectifsActions fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEffectifsActions fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEffectifsActions eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEffectifsActions)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEffectifsActions fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEffectifsActions fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEffectifsActions eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEffectifsActions)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEffectifsActions fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEffectifsActions eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEffectifsActions ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEffectifsActions fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
