/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// Created on Wed Apr 23 10:39:37 Europe/Paris 2003 by Apple EOModeler Version 5.2

package org.cocktail.papaye.server.metier.jefy_paye;

import java.math.BigDecimal;

import org.cocktail.papaye.server.common.EOInfoBulletinSalaire;
import org.cocktail.papaye.server.metier.grhum.EOIndividu;
import org.cocktail.papaye.server.metier.grhum.EOStructure;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOPayePrepa extends _EOPayePrepa {

	public EOPayePrepa() {
		super();
	}

	/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPayePrepa(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
	 */

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}



	// méthodes ajoutées
	/** Initialise une preparation a partir d'un contrat.
	 *Les valeurs numeriques qui sont nulles dans le contrat sont initialisees a zero ou a "".
	 */
	public void initAvecPeriodeEtContrat(EOPayePeriode periode,EOPayeContrat contrat) {

		if (contrat.indiceContrat() != null)
			setPayeIndice(contrat.indiceContrat());
		else
			setPayeIndice("");

		if (contrat.tauxHoraireContrat() != null)
			setPayeTauxhor(contrat.tauxHoraireContrat().setScale(2, BigDecimal.ROUND_HALF_UP));
		else
			setPayeTauxhor(new BigDecimal(0.0));

		if (periode.pperNbHeure() != null) {			
			setPayeNbheure(periode.pperNbHeure());
		}
		else {
			setPayeNbheure(new BigDecimal(0));
		}

		if (contrat.numQuotRecrutement() != null)
			setPayeQuotite(contrat.numQuotRecrutement());
		else
			setPayeQuotite(new BigDecimal(100));

		// 27/02/09 - mettre en priorite les jours du contrat si ils sont saisis
		if (contrat.nbJoursContrat() != null)
			setPayeNbjour(contrat.nbJoursContrat());
		else {
			if (periode.pperNbJour() != null)
				setPayeNbjour(periode.pperNbJour());
			else
				setPayeNbjour(new BigDecimal(30));
		}
		if (contrat.secteur() != null)
			setSecteurRelationship(contrat.secteur());

		if (contrat.structure() != null) {
			setStructureRelationship(contrat.structure());
			EOStructure structureSiret = EOStructure.rechercherStructureSiret(editingContext(),contrat.structure());
			if (structureSiret != null)
				setStructureSiretRelationship(structureSiret);
		}

		setStatutRelationship(contrat.statut());

	}


	//    /** prepare une validation en fonction des donnees de la preparation */
	//    public EOPayeValid preparerValidation() {
	//        EOPayeValid valid = new EOPayeValid();
	//        valid.initAvecObjet(this);
	//        java.util.Enumeration e = elements().objectEnumerator();
	//        while (e.hasMoreElements()) {
	//            EOPayeElement element = (EOPayeElement) e.nextElement();
	//            element.setValidationRelationship(valid);
	//        }
	//        e = commentaires().objectEnumerator();
	//        while (e.hasMoreElements()) {
	//            EOPayeCommentairesBs commentaire = (EOPayeCommentairesBs) e.nextElement();
	//            commentaire.setValidationRelationship(valid);
	//        }
	//        return valid;
	//    }

	// Méthodes ajoutées
	// méthodes privées
	private NSArray commentaires() {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOPayeCommentairesBs.PREPARATION_KEY + " = %@", new NSArray(this));
		return editingContext().objectsWithFetchSpecification(new EOFetchSpecification(EOPayeCommentairesBs.ENTITY_NAME,qualifier,null));
	}
	// méthodes de classe
	/** methode de classe permettant de trouver toutes les preparations courantes.
    @param editingContext editing context dans lequel effectuer le fetch
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return preparations trouvees
	 */
	public static NSArray chercherPreparations(EOEditingContext editingContext,
			NSArray prefetches) {
		return EOInfoBulletinSalaire.chercherInfos("PayePrepa", editingContext,prefetches);
	}
	/** methode de classe permettant de trouver des preparations en fonction d'un contrat
    et d'un mois
    @param editingContext editing context dans lequel effectuer le fetch
    @param mois mois concernee (peut &circ;tre nul)
    @param contrat contrat concerne
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return preparations trouvees
	 */
	public static NSArray chercherPreparationsPourMoisEtContrat(EOEditingContext editingContext,EOPayeMois mois, EOPayeContrat contrat,NSArray prefetches) {
		return EOInfoBulletinSalaire.chercherInfosPourMoisEtContrat("PayePrepa",
				editingContext,
				mois, contrat,
				prefetches);
	}
	/** methode de classe permettant de trouver des preparations en fonction d'un individu
    et d'un mois
    @param editingContext editing context dans lequel effectuer le fetch
    @param mois mois concernee (peut &circ;tre nul)
    @param individu individu concerne
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return preparations trouvees
	 */
	public static NSArray chercherPreparationsPourMoisEtIndividu(EOEditingContext editingContext,EOPayeMois mois, EOIndividu individu,NSArray prefetches) {
		return EOInfoBulletinSalaire.chercherInfosPourMoisEtIndividu(_EOPayePrepa.ENTITY_NAME,
				editingContext,
				mois, individu,
				prefetches);
	}

	/** methode de classe permettant de trouver toutes les preparations en fonction
    d'une operation de paye et d'un secteur.
    @param editingContext editing context dans lequel effectuer le fetch
    @param operation operation de paye concernee
    @param secteur secteur pour lequel rechercher les preparations (peut &ecirc;tre nul)
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return preparations trouvees
	 */
	public static NSArray chercherPreparationsPourOperationEtSecteur(EOEditingContext editingContext,
			EOPayeOper operation,EOPayeSecteur secteur,
			NSArray prefetches) {
		return EOInfoBulletinSalaire.chercherInfosPourOperationEtSecteur(EOPayePrepa.ENTITY_NAME,
				editingContext,
				operation,secteur,prefetches);
	}
	/** methode de classe permettant de trouver toutes les preparations en fonction
    d'un mois et d'un secteur.
    @param editingContext editing context dans lequel effectuer le fetch
    @param mois mois recherche
    @param secteur secteur pour lequel rechercher les preparations (peut &ecirc;tre nul)
    @param prefetches tableau contenant le nom des relations a prefetcher
    @return preparations trouvees
	 */
	public static NSArray chercherPreparationsPourMoisEtSecteur(
			EOEditingContext editingContext,
			EOPayeMois mois,EOPayeSecteur secteur,
			NSArray prefetches) {
		return EOInfoBulletinSalaire.chercherInfosPourMoisEtSecteur(EOPayePrepa.ENTITY_NAME,
				editingContext,
				mois,secteur,prefetches);
	}
	/** methode de classe permettant de trouver si toutes les preparations
    d'un secteur et d'un mois sont terminees
    @param editingContext editing context dans lequel effectuer le fetch
    @param mois mois recherche
    @param secteur secteur pour lequel rechercher les preparations (peut &ecirc;tre nul)
	 */
	public static boolean verificationsTerminees(EOEditingContext ec,EOPayeMois mois,EOPayeSecteur secteur) { 
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayePrepa.MOIS_KEY + " = %@", new NSArray(mois)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayePrepa.TEM_VERIFIE_KEY + " = %@", new NSArray("N")));

		if (secteur != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPayePrepa.SECTEUR_KEY + " = %@", new NSArray(secteur)));

		EOFetchSpecification fs = new EOFetchSpecification(EOPayePrepa.ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		NSArray prepas = ec.objectsWithFetchSpecification(fs);
		
		return (prepas == null || prepas.count() == 0) ? true : false;
		
	}

	/** methode de classe permettant de trouver la preparation associe a un contrat
    @param editingContext editing context dans lequel effectuer le fetch
    @param contrat contrat recherche
    @return preparation trouvee ou null
	 */
	public static EOPayePrepa rechercherPreparation(EOEditingContext editingContext,EOPayeContrat contrat) {

		try {
			NSArray values = new NSArray(contrat);
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOPayePrepa.CONTRAT_KEY + " = %@",values);
			EOFetchSpecification fs = new EOFetchSpecification(EOPayePrepa.ENTITY_NAME,qualifier, null);

			return (EOPayePrepa)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) { return null; }
	}

}
