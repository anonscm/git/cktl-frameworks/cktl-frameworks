// _EOPayeElement.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPayeElement.java instead.
package org.cocktail.papaye.server.metier.jefy_paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOPayeElement extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "PayeElement";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAYE.paye_element";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "pelmOrdre";

	public static final String PELM_ADEDUIRE_KEY = "pelmAdeduire";
	public static final String PELM_APAYER_KEY = "pelmApayer";
	public static final String PELM_ASSIETTE_KEY = "pelmAssiette";
	public static final String PELM_CONEX_KEY = "pelmConex";
	public static final String PELM_LIBELLE_KEY = "pelmLibelle";
	public static final String PELM_MODE_KEY = "pelmMode";
	public static final String PELM_MOIS_CODE_KEY = "pelmMoisCode";
	public static final String PELM_PATRON_KEY = "pelmPatron";
	public static final String PELM_TAUX_KEY = "pelmTaux";
	public static final String PELM_TYPE_KEY = "pelmType";
	public static final String PRUB_CLASSEMENT_KEY = "prubClassement";

//Colonnes dans la base de donnees
	public static final String PELM_ADEDUIRE_COLKEY = "pelm_adeduire";
	public static final String PELM_APAYER_COLKEY = "pelm_apayer";
	public static final String PELM_ASSIETTE_COLKEY = "pelm_assiette";
	public static final String PELM_CONEX_COLKEY = "pelm_conex";
	public static final String PELM_LIBELLE_COLKEY = "pelm_libelle";
	public static final String PELM_MODE_COLKEY = "pelm_mode";
	public static final String PELM_MOIS_CODE_COLKEY = "pelm_mois_code";
	public static final String PELM_PATRON_COLKEY = "pelm_patron";
	public static final String PELM_TAUX_COLKEY = "pelm_taux";
	public static final String PELM_TYPE_COLKEY = "pelm_type";
	public static final String PRUB_CLASSEMENT_COLKEY = "prub_classement";

// Relationships
	public static final String CODE_KEY = "code";
	public static final String HISTORIQUE_KEY = "historique";
	public static final String LBUDS_KEY = "lbuds";
	public static final String PREPARATION_KEY = "preparation";
	public static final String RUBRIQUE_KEY = "rubrique";
	public static final String VALIDATION_KEY = "validation";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal pelmAdeduire() {
    return (java.math.BigDecimal) storedValueForKey(PELM_ADEDUIRE_KEY);
  }

  public void setPelmAdeduire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PELM_ADEDUIRE_KEY);
  }

  public java.math.BigDecimal pelmApayer() {
    return (java.math.BigDecimal) storedValueForKey(PELM_APAYER_KEY);
  }

  public void setPelmApayer(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PELM_APAYER_KEY);
  }

  public java.math.BigDecimal pelmAssiette() {
    return (java.math.BigDecimal) storedValueForKey(PELM_ASSIETTE_KEY);
  }

  public void setPelmAssiette(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PELM_ASSIETTE_KEY);
  }

  public String pelmConex() {
    return (String) storedValueForKey(PELM_CONEX_KEY);
  }

  public void setPelmConex(String value) {
    takeStoredValueForKey(value, PELM_CONEX_KEY);
  }

  public String pelmLibelle() {
    return (String) storedValueForKey(PELM_LIBELLE_KEY);
  }

  public void setPelmLibelle(String value) {
    takeStoredValueForKey(value, PELM_LIBELLE_KEY);
  }

  public String pelmMode() {
    return (String) storedValueForKey(PELM_MODE_KEY);
  }

  public void setPelmMode(String value) {
    takeStoredValueForKey(value, PELM_MODE_KEY);
  }

  public Integer pelmMoisCode() {
    return (Integer) storedValueForKey(PELM_MOIS_CODE_KEY);
  }

  public void setPelmMoisCode(Integer value) {
    takeStoredValueForKey(value, PELM_MOIS_CODE_KEY);
  }

  public java.math.BigDecimal pelmPatron() {
    return (java.math.BigDecimal) storedValueForKey(PELM_PATRON_KEY);
  }

  public void setPelmPatron(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PELM_PATRON_KEY);
  }

  public java.math.BigDecimal pelmTaux() {
    return (java.math.BigDecimal) storedValueForKey(PELM_TAUX_KEY);
  }

  public void setPelmTaux(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PELM_TAUX_KEY);
  }

  public String pelmType() {
    return (String) storedValueForKey(PELM_TYPE_KEY);
  }

  public void setPelmType(String value) {
    takeStoredValueForKey(value, PELM_TYPE_KEY);
  }

  public String prubClassement() {
    return (String) storedValueForKey(PRUB_CLASSEMENT_KEY);
  }

  public void setPrubClassement(String value) {
    takeStoredValueForKey(value, PRUB_CLASSEMENT_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode code() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode)storedValueForKey(CODE_KEY);
  }

  public void setCodeRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode oldValue = code();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto historique() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto)storedValueForKey(HISTORIQUE_KEY);
  }

  public void setHistoriqueRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto oldValue = historique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, HISTORIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, HISTORIQUE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa preparation() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa)storedValueForKey(PREPARATION_KEY);
  }

  public void setPreparationRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa oldValue = preparation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PREPARATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PREPARATION_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique rubrique() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique)storedValueForKey(RUBRIQUE_KEY);
  }

  public void setRubriqueRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique oldValue = rubrique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RUBRIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RUBRIQUE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid validation() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid)storedValueForKey(VALIDATION_KEY);
  }

  public void setValidationRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid oldValue = validation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, VALIDATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, VALIDATION_KEY);
    }
  }
  
  public NSArray lbuds() {
    return (NSArray)storedValueForKey(LBUDS_KEY);
  }

  public NSArray lbuds(EOQualifier qualifier) {
    return lbuds(qualifier, null, false);
  }

  public NSArray lbuds(EOQualifier qualifier, boolean fetch) {
    return lbuds(qualifier, null, fetch);
  }

  public NSArray lbuds(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeElementLbud.PAYE_ELEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeElementLbud.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = lbuds();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToLbudsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeElementLbud object) {
    addObjectToBothSidesOfRelationshipWithKey(object, LBUDS_KEY);
  }

  public void removeFromLbudsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeElementLbud object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LBUDS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeElementLbud createLbudsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeElementLbud");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, LBUDS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeElementLbud) eo;
  }

  public void deleteLbudsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeElementLbud object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LBUDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllLbudsRelationships() {
    Enumeration objects = lbuds().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteLbudsRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeElementLbud)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPayeElement avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeElement createEOPayeElement(EOEditingContext editingContext, java.math.BigDecimal pelmAdeduire
, java.math.BigDecimal pelmApayer
, java.math.BigDecimal pelmAssiette
, String pelmLibelle
, String pelmMode
, Integer pelmMoisCode
, java.math.BigDecimal pelmPatron
, java.math.BigDecimal pelmTaux
, String pelmType
, String prubClassement
, org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode code, org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique rubrique			) {
    EOPayeElement eo = (EOPayeElement) createAndInsertInstance(editingContext, _EOPayeElement.ENTITY_NAME);    
		eo.setPelmAdeduire(pelmAdeduire);
		eo.setPelmApayer(pelmApayer);
		eo.setPelmAssiette(pelmAssiette);
		eo.setPelmLibelle(pelmLibelle);
		eo.setPelmMode(pelmMode);
		eo.setPelmMoisCode(pelmMoisCode);
		eo.setPelmPatron(pelmPatron);
		eo.setPelmTaux(pelmTaux);
		eo.setPelmType(pelmType);
		eo.setPrubClassement(prubClassement);
    eo.setCodeRelationship(code);
    eo.setRubriqueRelationship(rubrique);
    return eo;
  }

  
	  public EOPayeElement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPayeElement)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeElement creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPayeElement creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPayeElement object = (EOPayeElement)createAndInsertInstance(editingContext, _EOPayeElement.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPayeElement localInstanceIn(EOEditingContext editingContext, EOPayeElement eo) {
    EOPayeElement localInstance = (eo == null) ? null : (EOPayeElement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeElement#localInstanceIn a la place.
   */
	public static EOPayeElement localInstanceOf(EOEditingContext editingContext, EOPayeElement eo) {
		return EOPayeElement.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeElement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeElement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeElement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeElement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeElement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeElement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeElement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeElement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeElement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeElement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeElement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeElement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
