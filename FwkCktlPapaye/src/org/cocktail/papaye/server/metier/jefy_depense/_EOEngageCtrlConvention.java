// _EOEngageCtrlConvention.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEngageCtrlConvention.java instead.
package org.cocktail.papaye.server.metier.jefy_depense;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOEngageCtrlConvention extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "EngageCtrlConvention";
	public static final String ENTITY_TABLE_NAME = "jefy_depense.ENGAGE_CTRL_CONVENTION";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "econId";

	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String ECON_DATE_SAISIE_KEY = "econDateSaisie";
	public static final String ECON_HT_SAISIE_KEY = "econHtSaisie";
	public static final String ECON_MONTANT_BUDGETAIRE_KEY = "econMontantBudgetaire";
	public static final String ECON_MONTANT_BUDGETAIRE_RESTE_KEY = "econMontantBudgetaireReste";
	public static final String ECON_TTC_SAISIE_KEY = "econTtcSaisie";
	public static final String ECON_TVA_SAISIE_KEY = "econTvaSaisie";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String ECON_DATE_SAISIE_COLKEY = "ECON_DATE_SAISIE";
	public static final String ECON_HT_SAISIE_COLKEY = "ECON_HT_SAISIE";
	public static final String ECON_MONTANT_BUDGETAIRE_COLKEY = "ECON_MONTANT_BUDGETAIRE";
	public static final String ECON_MONTANT_BUDGETAIRE_RESTE_COLKEY = "ECON_MONTANT_BUDGETAIRE_RESTE";
	public static final String ECON_TTC_SAISIE_COLKEY = "ECON_TTC_SAISIE";
	public static final String ECON_TVA_SAISIE_COLKEY = "ECON_TVA_SAISIE";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";

// Relationships



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer convOrdre() {
    return (Integer) storedValueForKey(CONV_ORDRE_KEY);
  }

  public void setConvOrdre(Integer value) {
    takeStoredValueForKey(value, CONV_ORDRE_KEY);
  }

  public NSTimestamp econDateSaisie() {
    return (NSTimestamp) storedValueForKey(ECON_DATE_SAISIE_KEY);
  }

  public void setEconDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, ECON_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal econHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ECON_HT_SAISIE_KEY);
  }

  public void setEconHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECON_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal econMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(ECON_MONTANT_BUDGETAIRE_KEY);
  }

  public void setEconMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECON_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal econMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(ECON_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setEconMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECON_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public java.math.BigDecimal econTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ECON_TTC_SAISIE_KEY);
  }

  public void setEconTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECON_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal econTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ECON_TVA_SAISIE_KEY);
  }

  public void setEconTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECON_TVA_SAISIE_KEY);
  }

  public Integer engId() {
    return (Integer) storedValueForKey(ENG_ID_KEY);
  }

  public void setEngId(Integer value) {
    takeStoredValueForKey(value, ENG_ID_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }


/**
 * Créer une instance de EOEngageCtrlConvention avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEngageCtrlConvention createEOEngageCtrlConvention(EOEditingContext editingContext, Integer convOrdre
, NSTimestamp econDateSaisie
, java.math.BigDecimal econHtSaisie
, java.math.BigDecimal econMontantBudgetaire
, java.math.BigDecimal econMontantBudgetaireReste
, java.math.BigDecimal econTtcSaisie
, java.math.BigDecimal econTvaSaisie
, Integer engId
, Integer exeOrdre
			) {
    EOEngageCtrlConvention eo = (EOEngageCtrlConvention) createAndInsertInstance(editingContext, _EOEngageCtrlConvention.ENTITY_NAME);    
		eo.setConvOrdre(convOrdre);
		eo.setEconDateSaisie(econDateSaisie);
		eo.setEconHtSaisie(econHtSaisie);
		eo.setEconMontantBudgetaire(econMontantBudgetaire);
		eo.setEconMontantBudgetaireReste(econMontantBudgetaireReste);
		eo.setEconTtcSaisie(econTtcSaisie);
		eo.setEconTvaSaisie(econTvaSaisie);
		eo.setEngId(engId);
		eo.setExeOrdre(exeOrdre);
    return eo;
  }

  
	  public EOEngageCtrlConvention localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEngageCtrlConvention)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEngageCtrlConvention creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEngageCtrlConvention creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOEngageCtrlConvention object = (EOEngageCtrlConvention)createAndInsertInstance(editingContext, _EOEngageCtrlConvention.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOEngageCtrlConvention localInstanceIn(EOEditingContext editingContext, EOEngageCtrlConvention eo) {
    EOEngageCtrlConvention localInstance = (eo == null) ? null : (EOEngageCtrlConvention)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEngageCtrlConvention#localInstanceIn a la place.
   */
	public static EOEngageCtrlConvention localInstanceOf(EOEditingContext editingContext, EOEngageCtrlConvention eo) {
		return EOEngageCtrlConvention.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEngageCtrlConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEngageCtrlConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngageCtrlConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngageCtrlConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngageCtrlConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngageCtrlConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngageCtrlConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngageCtrlConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEngageCtrlConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngageCtrlConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngageCtrlConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngageCtrlConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
