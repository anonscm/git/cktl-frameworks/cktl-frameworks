// _EOEngage.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEngage.java instead.
package org.cocktail.papaye.server.metier.jefy_depense;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOEngage extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Engage";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.ENGAGE_BUDGET";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "engId";

	public static final String ENG_DATE_SAISIE_KEY = "engDateSaisie";
	public static final String ENG_HT_SAISIE_KEY = "engHtSaisie";
	public static final String ENG_MONTANT_BUDGETAIRE_KEY = "engMontantBudgetaire";
	public static final String ENG_MONTANT_BUDGETAIRE_RESTE_KEY = "engMontantBudgetaireReste";
	public static final String ENG_NUMERO_KEY = "engNumero";
	public static final String ENG_TTC_SAISIE_KEY = "engTtcSaisie";
	public static final String ENG_TVA_SAISIE_KEY = "engTvaSaisie";
	public static final String TYAP_ID_KEY = "tyapId";

//Colonnes dans la base de donnees
	public static final String ENG_DATE_SAISIE_COLKEY = "ENG_DATE_SAISIE";
	public static final String ENG_HT_SAISIE_COLKEY = "ENG_HT_SAISIE";
	public static final String ENG_MONTANT_BUDGETAIRE_COLKEY = "ENG_MONTANT_BUDGETAIRE";
	public static final String ENG_MONTANT_BUDGETAIRE_RESTE_COLKEY = "ENG_MONTANT_BUDGETAIRE_RESTE";
	public static final String ENG_NUMERO_COLKEY = "ENG_NUMERO";
	public static final String ENG_TTC_SAISIE_COLKEY = "ENG_TTC_SAISIE";
	public static final String ENG_TVA_SAISIE_COLKEY = "ENG_TVA_SAISIE";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";

// Relationships
	public static final String ANALYTIQUES_KEY = "analytiques";
	public static final String CONVENTIONS_KEY = "conventions";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNIS_KEY = "fournis";
	public static final String ORGAN_KEY = "organ";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TYPE_CREDIT_KEY = "typeCredit";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp engDateSaisie() {
    return (NSTimestamp) storedValueForKey(ENG_DATE_SAISIE_KEY);
  }

  public void setEngDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, ENG_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal engHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_HT_SAISIE_KEY);
  }

  public void setEngHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal engMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(ENG_MONTANT_BUDGETAIRE_KEY);
  }

  public void setEngMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal engMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(ENG_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setEngMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public Integer engNumero() {
    return (Integer) storedValueForKey(ENG_NUMERO_KEY);
  }

  public void setEngNumero(Integer value) {
    takeStoredValueForKey(value, ENG_NUMERO_KEY);
  }

  public java.math.BigDecimal engTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_TTC_SAISIE_KEY);
  }

  public void setEngTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal engTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_TVA_SAISIE_KEY);
  }

  public void setEngTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_TVA_SAISIE_KEY);
  }

  public Integer tyapId() {
    return (Integer) storedValueForKey(TYAP_ID_KEY);
  }

  public void setTyapId(Integer value) {
    takeStoredValueForKey(value, TYAP_ID_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_admin.EOExercice exercice() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOExercice value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOFournis fournis() {
    return (org.cocktail.papaye.server.metier.grhum.EOFournis)storedValueForKey(FOURNIS_KEY);
  }

  public void setFournisRelationship(org.cocktail.papaye.server.metier.grhum.EOFournis value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOFournis oldValue = fournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOOrgan organ() {
    return (org.cocktail.application.serveur.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.serveur.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOTauxProrata tauxProrata() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
  }

  public void setTauxProrataRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOTauxProrata value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOTauxProrata oldValue = tauxProrata();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOTypeCredit typeCredit() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray analytiques() {
    return (NSArray)storedValueForKey(ANALYTIQUES_KEY);
  }

  public NSArray analytiques(EOQualifier qualifier) {
    return analytiques(qualifier, null);
  }

  public NSArray analytiques(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = analytiques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToAnalytiquesRelationship(org.cocktail.papaye.server.metier.jefy_depense.EOEngageCtrlAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ANALYTIQUES_KEY);
  }

  public void removeFromAnalytiquesRelationship(org.cocktail.papaye.server.metier.jefy_depense.EOEngageCtrlAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ANALYTIQUES_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_depense.EOEngageCtrlAnalytique createAnalytiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EngageCtrlAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ANALYTIQUES_KEY);
    return (org.cocktail.papaye.server.metier.jefy_depense.EOEngageCtrlAnalytique) eo;
  }

  public void deleteAnalytiquesRelationship(org.cocktail.papaye.server.metier.jefy_depense.EOEngageCtrlAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ANALYTIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAnalytiquesRelationships() {
    Enumeration objects = analytiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAnalytiquesRelationship((org.cocktail.papaye.server.metier.jefy_depense.EOEngageCtrlAnalytique)objects.nextElement());
    }
  }

  public NSArray conventions() {
    return (NSArray)storedValueForKey(CONVENTIONS_KEY);
  }

  public NSArray conventions(EOQualifier qualifier) {
    return conventions(qualifier, null);
  }

  public NSArray conventions(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = conventions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToConventionsRelationship(org.cocktail.papaye.server.metier.jefy_depense.EOEngageCtrlConvention object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CONVENTIONS_KEY);
  }

  public void removeFromConventionsRelationship(org.cocktail.papaye.server.metier.jefy_depense.EOEngageCtrlConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTIONS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_depense.EOEngageCtrlConvention createConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EngageCtrlConvention");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CONVENTIONS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_depense.EOEngageCtrlConvention) eo;
  }

  public void deleteConventionsRelationship(org.cocktail.papaye.server.metier.jefy_depense.EOEngageCtrlConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllConventionsRelationships() {
    Enumeration objects = conventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteConventionsRelationship((org.cocktail.papaye.server.metier.jefy_depense.EOEngageCtrlConvention)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOEngage avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEngage createEOEngage(EOEditingContext editingContext, NSTimestamp engDateSaisie
, java.math.BigDecimal engHtSaisie
, java.math.BigDecimal engMontantBudgetaire
, java.math.BigDecimal engMontantBudgetaireReste
, Integer engNumero
, java.math.BigDecimal engTtcSaisie
, java.math.BigDecimal engTvaSaisie
, Integer tyapId
			) {
    EOEngage eo = (EOEngage) createAndInsertInstance(editingContext, _EOEngage.ENTITY_NAME);    
		eo.setEngDateSaisie(engDateSaisie);
		eo.setEngHtSaisie(engHtSaisie);
		eo.setEngMontantBudgetaire(engMontantBudgetaire);
		eo.setEngMontantBudgetaireReste(engMontantBudgetaireReste);
		eo.setEngNumero(engNumero);
		eo.setEngTtcSaisie(engTtcSaisie);
		eo.setEngTvaSaisie(engTvaSaisie);
		eo.setTyapId(tyapId);
    return eo;
  }

  
	  public EOEngage localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEngage)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEngage creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEngage creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOEngage object = (EOEngage)createAndInsertInstance(editingContext, _EOEngage.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOEngage localInstanceIn(EOEditingContext editingContext, EOEngage eo) {
    EOEngage localInstance = (eo == null) ? null : (EOEngage)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEngage#localInstanceIn a la place.
   */
	public static EOEngage localInstanceOf(EOEditingContext editingContext, EOEngage eo) {
		return EOEngage.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEngage fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEngage fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngage eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngage)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngage eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngage)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEngage fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngage eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngage ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngage fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
