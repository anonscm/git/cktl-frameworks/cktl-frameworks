// _EODepensePapier.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODepensePapier.java instead.
package org.cocktail.papaye.server.metier.jefy_depense;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EODepensePapier extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "DepensePapier";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.DEPENSE_PAPIER";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "dppId";

	public static final String DPP_DATE_FACTURE_KEY = "dppDateFacture";
	public static final String DPP_DATE_RECEPTION_KEY = "dppDateReception";
	public static final String DPP_DATE_SAISIE_KEY = "dppDateSaisie";
	public static final String DPP_DATE_SERVICE_FAIT_KEY = "dppDateServiceFait";
	public static final String DPP_HT_INITIAL_KEY = "dppHtInitial";
	public static final String DPP_HT_SAISIE_KEY = "dppHtSaisie";
	public static final String DPP_ID_KEY = "dppId";
	public static final String DPP_ID_REVERSEMENT_KEY = "dppIdReversement";
	public static final String DPP_NB_PIECE_KEY = "dppNbPiece";
	public static final String DPP_NUMERO_FACTURE_KEY = "dppNumeroFacture";
	public static final String DPP_TTC_INITIAL_KEY = "dppTtcInitial";
	public static final String DPP_TTC_SAISIE_KEY = "dppTtcSaisie";
	public static final String DPP_TVA_INITIAL_KEY = "dppTvaInitial";
	public static final String DPP_TVA_SAISIE_KEY = "dppTvaSaisie";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String DPP_DATE_FACTURE_COLKEY = "DPP_DATE_FACTURE";
	public static final String DPP_DATE_RECEPTION_COLKEY = "DPP_DATE_RECEPTION";
	public static final String DPP_DATE_SAISIE_COLKEY = "DPP_DATE_SAISIE";
	public static final String DPP_DATE_SERVICE_FAIT_COLKEY = "DPP_DATE_SERVICE_FAIT";
	public static final String DPP_HT_INITIAL_COLKEY = "DPP_HT_INITIAL";
	public static final String DPP_HT_SAISIE_COLKEY = "DPP_HT_SAISIE";
	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String DPP_ID_REVERSEMENT_COLKEY = "DPP_ID_REVERSEMENT";
	public static final String DPP_NB_PIECE_COLKEY = "DPP_NB_PIECE";
	public static final String DPP_NUMERO_FACTURE_COLKEY = "DPP_NUMERO_FACTURE";
	public static final String DPP_TTC_INITIAL_COLKEY = "DPP_TTC_INITIAL";
	public static final String DPP_TTC_SAISIE_COLKEY = "DPP_TTC_SAISIE";
	public static final String DPP_TVA_INITIAL_COLKEY = "DPP_TVA_INITIAL";
	public static final String DPP_TVA_SAISIE_COLKEY = "DPP_TVA_SAISIE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";

// Relationships
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dppDateFacture() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_FACTURE_KEY);
  }

  public void setDppDateFacture(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_FACTURE_KEY);
  }

  public NSTimestamp dppDateReception() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_RECEPTION_KEY);
  }

  public void setDppDateReception(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_RECEPTION_KEY);
  }

  public NSTimestamp dppDateSaisie() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_SAISIE_KEY);
  }

  public void setDppDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_SAISIE_KEY);
  }

  public NSTimestamp dppDateServiceFait() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_SERVICE_FAIT_KEY);
  }

  public void setDppDateServiceFait(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_SERVICE_FAIT_KEY);
  }

  public java.math.BigDecimal dppHtInitial() {
    return (java.math.BigDecimal) storedValueForKey(DPP_HT_INITIAL_KEY);
  }

  public void setDppHtInitial(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_HT_INITIAL_KEY);
  }

  public java.math.BigDecimal dppHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPP_HT_SAISIE_KEY);
  }

  public void setDppHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_HT_SAISIE_KEY);
  }

  public Integer dppId() {
    return (Integer) storedValueForKey(DPP_ID_KEY);
  }

  public void setDppId(Integer value) {
    takeStoredValueForKey(value, DPP_ID_KEY);
  }

  public Integer dppIdReversement() {
    return (Integer) storedValueForKey(DPP_ID_REVERSEMENT_KEY);
  }

  public void setDppIdReversement(Integer value) {
    takeStoredValueForKey(value, DPP_ID_REVERSEMENT_KEY);
  }

  public Integer dppNbPiece() {
    return (Integer) storedValueForKey(DPP_NB_PIECE_KEY);
  }

  public void setDppNbPiece(Integer value) {
    takeStoredValueForKey(value, DPP_NB_PIECE_KEY);
  }

  public String dppNumeroFacture() {
    return (String) storedValueForKey(DPP_NUMERO_FACTURE_KEY);
  }

  public void setDppNumeroFacture(String value) {
    takeStoredValueForKey(value, DPP_NUMERO_FACTURE_KEY);
  }

  public java.math.BigDecimal dppTtcInitial() {
    return (java.math.BigDecimal) storedValueForKey(DPP_TTC_INITIAL_KEY);
  }

  public void setDppTtcInitial(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_TTC_INITIAL_KEY);
  }

  public java.math.BigDecimal dppTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPP_TTC_SAISIE_KEY);
  }

  public void setDppTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal dppTvaInitial() {
    return (java.math.BigDecimal) storedValueForKey(DPP_TVA_INITIAL_KEY);
  }

  public void setDppTvaInitial(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_TVA_INITIAL_KEY);
  }

  public java.math.BigDecimal dppTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPP_TVA_SAISIE_KEY);
  }

  public void setDppTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_TVA_SAISIE_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public Integer fouOrdre() {
    return (Integer) storedValueForKey(FOU_ORDRE_KEY);
  }

  public void setFouOrdre(Integer value) {
    takeStoredValueForKey(value, FOU_ORDRE_KEY);
  }

  public Integer modOrdre() {
    return (Integer) storedValueForKey(MOD_ORDRE_KEY);
  }

  public void setModOrdre(Integer value) {
    takeStoredValueForKey(value, MOD_ORDRE_KEY);
  }

  public Integer ribOrdre() {
    return (Integer) storedValueForKey(RIB_ORDRE_KEY);
  }

  public void setRibOrdre(Integer value) {
    takeStoredValueForKey(value, RIB_ORDRE_KEY);
  }

  public Integer utlOrdre() {
    return (Integer) storedValueForKey(UTL_ORDRE_KEY);
  }

  public void setUtlOrdre(Integer value) {
    takeStoredValueForKey(value, UTL_ORDRE_KEY);
  }

  public org.cocktail.papaye.server.metier.maracuja.EOModePaiement modePaiement() {
    return (org.cocktail.papaye.server.metier.maracuja.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.papaye.server.metier.maracuja.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.maracuja.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EODepensePapier avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODepensePapier createEODepensePapier(EOEditingContext editingContext, NSTimestamp dppDateFacture
, NSTimestamp dppDateReception
, NSTimestamp dppDateSaisie
, NSTimestamp dppDateServiceFait
, java.math.BigDecimal dppHtInitial
, java.math.BigDecimal dppHtSaisie
, Integer dppId
, Integer dppNbPiece
, String dppNumeroFacture
, java.math.BigDecimal dppTtcInitial
, java.math.BigDecimal dppTtcSaisie
, java.math.BigDecimal dppTvaInitial
, java.math.BigDecimal dppTvaSaisie
, Integer exeOrdre
, Integer fouOrdre
, Integer utlOrdre
			) {
    EODepensePapier eo = (EODepensePapier) createAndInsertInstance(editingContext, _EODepensePapier.ENTITY_NAME);    
		eo.setDppDateFacture(dppDateFacture);
		eo.setDppDateReception(dppDateReception);
		eo.setDppDateSaisie(dppDateSaisie);
		eo.setDppDateServiceFait(dppDateServiceFait);
		eo.setDppHtInitial(dppHtInitial);
		eo.setDppHtSaisie(dppHtSaisie);
		eo.setDppId(dppId);
		eo.setDppNbPiece(dppNbPiece);
		eo.setDppNumeroFacture(dppNumeroFacture);
		eo.setDppTtcInitial(dppTtcInitial);
		eo.setDppTtcSaisie(dppTtcSaisie);
		eo.setDppTvaInitial(dppTvaInitial);
		eo.setDppTvaSaisie(dppTvaSaisie);
		eo.setExeOrdre(exeOrdre);
		eo.setFouOrdre(fouOrdre);
		eo.setUtlOrdre(utlOrdre);
    return eo;
  }

  
	  public EODepensePapier localInstanceIn(EOEditingContext editingContext) {
	  		return (EODepensePapier)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODepensePapier creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODepensePapier creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EODepensePapier object = (EODepensePapier)createAndInsertInstance(editingContext, _EODepensePapier.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EODepensePapier localInstanceIn(EOEditingContext editingContext, EODepensePapier eo) {
    EODepensePapier localInstance = (eo == null) ? null : (EODepensePapier)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODepensePapier#localInstanceIn a la place.
   */
	public static EODepensePapier localInstanceOf(EOEditingContext editingContext, EODepensePapier eo) {
		return EODepensePapier.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODepensePapier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODepensePapier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODepensePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODepensePapier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODepensePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODepensePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODepensePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODepensePapier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODepensePapier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODepensePapier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODepensePapier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODepensePapier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
