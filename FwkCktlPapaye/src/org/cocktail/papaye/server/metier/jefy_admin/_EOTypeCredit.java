// _EOTypeCredit.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeCredit.java instead.
package org.cocktail.papaye.server.metier.jefy_admin;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOTypeCredit extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "TypeCredit";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.V_Type_Credit";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "tcdOrdre";

	public static final String TCD_ABREGE_KEY = "tcdAbrege";
	public static final String TCD_BUDGET_KEY = "tcdBudget";
	public static final String TCD_CODE_KEY = "tcdCode";
	public static final String TCD_LIBELLE_KEY = "tcdLibelle";
	public static final String TCD_SECT_KEY = "tcdSect";
	public static final String TCD_TYPE_KEY = "tcdType";

//Colonnes dans la base de donnees
	public static final String TCD_ABREGE_COLKEY = "TCD_ABREGE";
	public static final String TCD_BUDGET_COLKEY = "TCD_BUDGET";
	public static final String TCD_CODE_COLKEY = "TCD_CODE";
	public static final String TCD_LIBELLE_COLKEY = "TCD_LIBELLE";
	public static final String TCD_SECT_COLKEY = "TCD_SECT";
	public static final String TCD_TYPE_COLKEY = "TCD_TYPE";

// Relationships
	public static final String EXERCICE_KEY = "exercice";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String tcdAbrege() {
    return (String) storedValueForKey(TCD_ABREGE_KEY);
  }

  public void setTcdAbrege(String value) {
    takeStoredValueForKey(value, TCD_ABREGE_KEY);
  }

  public String tcdBudget() {
    return (String) storedValueForKey(TCD_BUDGET_KEY);
  }

  public void setTcdBudget(String value) {
    takeStoredValueForKey(value, TCD_BUDGET_KEY);
  }

  public String tcdCode() {
    return (String) storedValueForKey(TCD_CODE_KEY);
  }

  public void setTcdCode(String value) {
    takeStoredValueForKey(value, TCD_CODE_KEY);
  }

  public String tcdLibelle() {
    return (String) storedValueForKey(TCD_LIBELLE_KEY);
  }

  public void setTcdLibelle(String value) {
    takeStoredValueForKey(value, TCD_LIBELLE_KEY);
  }

  public String tcdSect() {
    return (String) storedValueForKey(TCD_SECT_KEY);
  }

  public void setTcdSect(String value) {
    takeStoredValueForKey(value, TCD_SECT_KEY);
  }

  public String tcdType() {
    return (String) storedValueForKey(TCD_TYPE_KEY);
  }

  public void setTcdType(String value) {
    takeStoredValueForKey(value, TCD_TYPE_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_admin.EOExercice exercice() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOExercice value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  

/**
 * Créer une instance de EOTypeCredit avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeCredit createEOTypeCredit(EOEditingContext editingContext, String tcdAbrege
, String tcdBudget
, String tcdCode
, String tcdLibelle
, String tcdSect
, String tcdType
			) {
    EOTypeCredit eo = (EOTypeCredit) createAndInsertInstance(editingContext, _EOTypeCredit.ENTITY_NAME);    
		eo.setTcdAbrege(tcdAbrege);
		eo.setTcdBudget(tcdBudget);
		eo.setTcdCode(tcdCode);
		eo.setTcdLibelle(tcdLibelle);
		eo.setTcdSect(tcdSect);
		eo.setTcdType(tcdType);
    return eo;
  }

  
	  public EOTypeCredit localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeCredit)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeCredit creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeCredit creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTypeCredit object = (EOTypeCredit)createAndInsertInstance(editingContext, _EOTypeCredit.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeCredit localInstanceIn(EOEditingContext editingContext, EOTypeCredit eo) {
    EOTypeCredit localInstance = (eo == null) ? null : (EOTypeCredit)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeCredit#localInstanceIn a la place.
   */
	public static EOTypeCredit localInstanceOf(EOEditingContext editingContext, EOTypeCredit eo) {
		return EOTypeCredit.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeCredit fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeCredit fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeCredit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeCredit)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeCredit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeCredit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeCredit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeCredit)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeCredit fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeCredit eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeCredit ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeCredit fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
