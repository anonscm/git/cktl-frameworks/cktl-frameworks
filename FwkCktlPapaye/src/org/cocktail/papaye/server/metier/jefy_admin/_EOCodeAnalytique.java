// _EOCodeAnalytique.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCodeAnalytique.java instead.
package org.cocktail.papaye.server.metier.jefy_admin;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;


public abstract class _EOCodeAnalytique extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "CodeAnalytique";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.V_CODE_ANALYTIQUE";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "canId";

	public static final String CAN_CODE_KEY = "canCode";
	public static final String CAN_FERMETURE_KEY = "canFermeture";
	public static final String CAN_LIBELLE_KEY = "canLibelle";
	public static final String CAN_NIVEAU_KEY = "canNiveau";
	public static final String CAN_OUVERTURE_KEY = "canOuverture";

//Colonnes dans la base de donnees
	public static final String CAN_CODE_COLKEY = "CAN_CODE";
	public static final String CAN_FERMETURE_COLKEY = "CAN_FERMETURE";
	public static final String CAN_LIBELLE_COLKEY = "CAN_LIBELLE";
	public static final String CAN_NIVEAU_COLKEY = "CAN_NIVEAU";
	public static final String CAN_OUVERTURE_COLKEY = "CAN_OUVERTURE";

// Relationships
	public static final String CODE_ANALYTIQUE_ORGAN_KEY = "codeAnalytiqueOrgan";
	public static final String CODE_ANALYTIQUE_PERE_KEY = "codeAnalytiquePere";
	public static final String EXERCICE_KEY = "exercice";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String TYPE_ETAT_PUBLIC_KEY = "typeEtatPublic";
	public static final String TYPE_ETAT_UTILISABLE_KEY = "typeEtatUtilisable";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String canCode() {
    return (String) storedValueForKey(CAN_CODE_KEY);
  }

  public void setCanCode(String value) {
    takeStoredValueForKey(value, CAN_CODE_KEY);
  }

  public NSTimestamp canFermeture() {
    return (NSTimestamp) storedValueForKey(CAN_FERMETURE_KEY);
  }

  public void setCanFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, CAN_FERMETURE_KEY);
  }

  public String canLibelle() {
    return (String) storedValueForKey(CAN_LIBELLE_KEY);
  }

  public void setCanLibelle(String value) {
    takeStoredValueForKey(value, CAN_LIBELLE_KEY);
  }

  public Integer canNiveau() {
    return (Integer) storedValueForKey(CAN_NIVEAU_KEY);
  }

  public void setCanNiveau(Integer value) {
    takeStoredValueForKey(value, CAN_NIVEAU_KEY);
  }

  public NSTimestamp canOuverture() {
    return (NSTimestamp) storedValueForKey(CAN_OUVERTURE_KEY);
  }

  public void setCanOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, CAN_OUVERTURE_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytique codeAnalytiquePere() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_PERE_KEY);
  }

  public void setCodeAnalytiquePereRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytique oldValue = codeAnalytiquePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_PERE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOExercice exercice() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOExercice value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat typeEtat() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat typeEtatPublic() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat)storedValueForKey(TYPE_ETAT_PUBLIC_KEY);
  }

  public void setTypeEtatPublicRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat oldValue = typeEtatPublic();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_PUBLIC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_PUBLIC_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat typeEtatUtilisable() {
    return (org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat)storedValueForKey(TYPE_ETAT_UTILISABLE_KEY);
  }

  public void setTypeEtatUtilisableRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat oldValue = typeEtatUtilisable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_UTILISABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_UTILISABLE_KEY);
    }
  }
  
  public NSArray codeAnalytiqueOrgan() {
    return (NSArray)storedValueForKey(CODE_ANALYTIQUE_ORGAN_KEY);
  }

  public NSArray codeAnalytiqueOrgan(EOQualifier qualifier) {
    return codeAnalytiqueOrgan(qualifier, null, false);
  }

  public NSArray codeAnalytiqueOrgan(EOQualifier qualifier, boolean fetch) {
    return codeAnalytiqueOrgan(qualifier, null, fetch);
  }

  public NSArray codeAnalytiqueOrgan(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytiqueOrgan.CODE_ANALYTIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytiqueOrgan.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = codeAnalytiqueOrgan();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCodeAnalytiqueOrganRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytiqueOrgan object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGAN_KEY);
  }

  public void removeFromCodeAnalytiqueOrganRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytiqueOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGAN_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytiqueOrgan createCodeAnalytiqueOrganRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("CodeAnalytiqueOrgan");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CODE_ANALYTIQUE_ORGAN_KEY);
    return (org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytiqueOrgan) eo;
  }

  public void deleteCodeAnalytiqueOrganRelationship(org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytiqueOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGAN_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCodeAnalytiqueOrganRelationships() {
    Enumeration objects = codeAnalytiqueOrgan().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCodeAnalytiqueOrganRelationship((org.cocktail.papaye.server.metier.jefy_admin.EOCodeAnalytiqueOrgan)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCodeAnalytique avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCodeAnalytique createEOCodeAnalytique(EOEditingContext editingContext, String canCode
, String canLibelle
, Integer canNiveau
, NSTimestamp canOuverture
, org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat typeEtat, org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat typeEtatPublic, org.cocktail.papaye.server.metier.jefy_admin.EOTypeEtat typeEtatUtilisable			) {
    EOCodeAnalytique eo = (EOCodeAnalytique) createAndInsertInstance(editingContext, _EOCodeAnalytique.ENTITY_NAME);    
		eo.setCanCode(canCode);
		eo.setCanLibelle(canLibelle);
		eo.setCanNiveau(canNiveau);
		eo.setCanOuverture(canOuverture);
    eo.setTypeEtatRelationship(typeEtat);
    eo.setTypeEtatPublicRelationship(typeEtatPublic);
    eo.setTypeEtatUtilisableRelationship(typeEtatUtilisable);
    return eo;
  }

  
	  public EOCodeAnalytique localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCodeAnalytique)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCodeAnalytique creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCodeAnalytique creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCodeAnalytique object = (EOCodeAnalytique)createAndInsertInstance(editingContext, _EOCodeAnalytique.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCodeAnalytique localInstanceIn(EOEditingContext editingContext, EOCodeAnalytique eo) {
    EOCodeAnalytique localInstance = (eo == null) ? null : (EOCodeAnalytique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCodeAnalytique#localInstanceIn a la place.
   */
	public static EOCodeAnalytique localInstanceOf(EOEditingContext editingContext, EOCodeAnalytique eo) {
		return EOCodeAnalytique.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCodeAnalytique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCodeAnalytique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCodeAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCodeAnalytique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCodeAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCodeAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCodeAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCodeAnalytique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCodeAnalytique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCodeAnalytique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCodeAnalytique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCodeAnalytique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
