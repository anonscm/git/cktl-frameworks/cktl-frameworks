/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.moteur;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.grhum.EOIndividu;
import org.cocktail.papaye.server.metier.grhum.EOStructure;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParamPerso;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/** Classe permettant de modeliser les classes de calcul definies comme plugins. Tous les plugins de calcul doivent heriter de cette classe.<BR>
La methode calculer attend un dictionnaire qui comporte :<BR>
<UL>la periode en cours (classe de la valeur : PayPeriode, cle : "Periode")</UL>
<UL>l'element de preparation de paye (classe de la valeur : EOPayePrepa, cle : "PayePrepa")</UL>
<UL>est-ce un calcul direct ou en mode inverse ? (classe de la valeur : Boolean, cle : "CalculDirect")</UL>
<UL>les elements de paye deja generes (classe de la valeur : NSArray, cle : "Elements"). Les objets dans le tableau sont de type EOPayeElement</UL>
<UL>le montant de la remuneration sur un temps plein (sans les heures complementaires)</UL>
<UL>le plafond serite sociale</UL>
<UL>les donnees personnelles dans le cas des calculs resultant de rubriques personnelles (classe de la valeur : EOPayePerso, cle : "Personnalisation")</UL>
<UL>les codes et les param&egrave;tres necessaires au calcul (classe de la valeur : EOPayeParam, cle : pcodCode issu de la table EOPayeCode)</UL>
Pour les sous-classes, adopter la philosophie generale suivante pour optimiser le traitement :<BR>
<UL>les param&egrave;tres de calcul standards ne sont verifies que lors de la premi&egrave;re execution de la methode calculer. Ils sont ensuite stockes dans des variables d'instance et utiliser directement.</UL>
 */
public class ModeleCalcul {
	private NSDictionary valeurs;
	private NSMutableDictionary resultats;
	/** lance le calcul lie a une rubrique. <BR>
    Tous les plugins de calcul doivent redefinir la methode calculer en n'oubliant pas de commencer par faire un appel a la methode de la super-classe.
    Un calcul peut retourner plusieurs valeurs liees a des taux differents, les resultats sont retournes sous la forme d'un dictionnaire.
    @param parametres dictionnaire comportant les donnees necessaires au calcul (voir description de la classe pour le format du dictionnaire)
        @return dictionnaire des resultats : une entree par resultat<BR>
objet : Dictionnaire de resultat cle: code (EOPayeCode) associe a ce resultat<BR>
    <UL>Dictionnaire de resultat
    <UL>objet : montant  du resultat (BigDecimal) cle: "Montant"</UL>
    <UL>objet : assiette de calcul (BigDecimal) cle: "Assiette"</UL>
        <UL></UL>
        <UL>Utiliser les methodes :
        <UL>ajouterCotisation() pour ajouter un resultat de calcul de cotisation</UL>
        <UL>ajouterRemuneration() pour ajourer un resultat de calcul de
        remuneration</UL>
        au dictionnaire des resultats au fur et a mesure d'un calcul.</UL>
    </UL>
	 */
	public NSDictionary calculer(NSDictionary parametres) throws Exception {
		try {

			valeurs = parametres;
			resultats = new NSMutableDictionary();
			return resultats;
		} catch (Exception e) { throw e; }

	}
	/** retourne le nom de la classe */
	public String nomClasse() {
		return getClass().getName();
	}
	/** accesseur sur la periode en cours */
	public EOPayePeriode periode() {
		return (EOPayePeriode) valeurs.objectForKey("Periode");
	}
	/** accesseur sur l'element de preparation de paye */
	public EOPayePrepa preparation() {
		return (EOPayePrepa)valeurs.objectForKey("PayePrepa");
	}
	/** accesseur sur les elements  */
	public NSArray elements() {
		return (NSArray)valeurs.objectForKey("Elements");
	}
	/** accesseur sur les cumuls  */
	public NSArray cumuls() {
		return (NSArray)valeurs.objectForKey("Cumuls");
	}
	/** accesseur sur le cumul de plafond de sécurité sociale pour les rappels */
	public BigDecimal cumulPlafondPourRappel() {
		return (BigDecimal)valeurs.objectForKey("CumulPlafondPourRappel");
	}
	/** accesseur sur le mois pendant lequel le bulletin de salaire est etabli */
	public EOPayeMois mois() {
		return preparation().mois();
	}
	/** accesseur sur les donnees personnelles dans le cas des calculs resultant de rubriques personnelles */
	public EOPayePerso personnalisation() {
		return (EOPayePerso)valeurs.objectForKey("Personnalisation");
	}
	/** accesseur sur les resultats generes  */
	public NSMutableDictionary resultats() {
		return resultats;
	}
	/** retourne true si le calcul est direct, false sinon */
	public boolean estUnCalculDirect() {
		return ((Boolean)valeurs.objectForKey("CalculDirect")).booleanValue();
	}
	/** retourne true si il s'agit du calcul d'un rappel, false sinon */
	public boolean estUnRappel() {
		return ((Boolean)valeurs.objectForKey("Rappel")).booleanValue();
	}
	/** periodes rappel : null si pas de rappel */
	public NSArray periodesRappel() {
		return (NSArray)valeurs.objectForKey("PeriodesRappel");
	}
	/** retourne le nombre de periodes payées, 1 si une seule période */
	public int nbPeriodes() {
		Number periodes = (Number)valeurs.objectForKey("NbPeriodes");
		if (periodes == null) {
			return 1;
		} else {
			return periodes.intValue();
		}
	}

	// accesseurs utilitaires
	/** retourne l'editing context dans lequel travailler. On prendra le m&ecirc;me que celui de la preparation courante. */
	public EOEditingContext editingContext() {
		if (preparation() != null && preparation().editingContext() != null) {
			return preparation().editingContext();
		} else {	// cas d'un précontrat
			return contrat().editingContext();
		}
	}
	/** retourne le contrat (EOPayeContrat) pour lequel on effectue ce calcul. */
	public EOPayeContrat contrat() {
		if (periode() != null) {
			return periode().contrat();
		} else {	// calcul de précontrat
			return (EOPayeContrat)valeurs.objectForKey("Contrat");
		}
	}
	/** retourne l'agent (EOIndividu) pour lequel on effectue ce calcul. */
	public EOIndividu agent() {
		return contrat().individu();
	}
	/** retourne la structure (EOStructure) associee au contrat pour lequel on
        effectue ce calcul. */
	public EOStructure structure() {
		return contrat().structure();
	}
	/** retourne le parametre associe a un code */
	public EOPayeParam parametrePourCode(String code) {
		return (EOPayeParam)valeurs.objectForKey(code);
	}
	/** retourne le param&egrave;tre personnel associe a un code */
	public EOPayeParamPerso parametrePersoPourCode(String code) {
		return (EOPayeParamPerso)valeurs.objectForKey(code);
	}
	/** retourne le code de cumul */
	public EOPayeCode codeCumul() {
		return (EOPayeCode)valeurs.objectForKey("CodeCumul");
	}
	/** ajoute une resultat de calcul de cotisation au dictionnaire
        retourne par la methode calculer()
        @param code code associe au taux utilise dans le calcul
        @param montant du calcul pour ce taux
        @param assiette : assiette de calcul utilisee
        @param cumul : cumul calcule auparavant
		@param codeCumul (EOPayeCode) : code cumul à modifier
	 */
	public void ajouterCotisation(EOPayeCode code,BigDecimal montant,BigDecimal assiette,BigDecimal cumul,EOPayeCode codeCumul) {
		NSMutableDictionary resultat = new NSMutableDictionary(4);
		resultat.setObjectForKey(montant,"Montant");
		if (assiette == null) {
			assiette = new BigDecimal(0);
		}
		resultat.setObjectForKey(assiette,"Assiette");
		if (cumul != null)
			resultat.setObjectForKey(cumul,"Cumul");

		resultat.setObjectForKey(codeCumul,"CodeCumul");

		resultats.setObjectForKey(resultat,code);
	}
	/** ajoute une resultat de calcul de cotisation au dictionnaire
        retourne par la methode calculer()
        @param code code associe au taux utilise dans le calcul
        @param montant du calcul pour ce taux
        @param assiette : assiette de calcul utilisee
        @param cumul : cumul calcule auparavant
	 */
	public void ajouterCotisation(EOPayeCode code,BigDecimal montant,BigDecimal assiette,BigDecimal cumul) {
		ajouterCotisation(code,montant,assiette,cumul,codeCumul());
	}
	/** ajoute une resultat de calcul de cotisation au dictionnaire
        retourne par la methode calculer()
        @param code code associe au taux utilise dans le calcul
        @param montant du calcul pour ce taux
        @param assiette : assiette de calcul utilisee
		@param codeCumul : codeCumul
	 */
	public void ajouterCotisation(EOPayeCode code,BigDecimal montant,BigDecimal assiette,EOPayeCode codeCumul) {
		ajouterCotisation(code,montant,assiette,null,codeCumul);
	}
	/** ajoute une resultat de calcul de cotisation au dictionnaire
        retourne par la methode calculer()
        @param code code associe au taux utilise dans le calcul
        @param montant du calcul pour ce taux
        @param assiette : assiette de calcul utilisee
	 */
	public void ajouterCotisation(EOPayeCode code,BigDecimal montant,BigDecimal assiette) {
		ajouterCotisation(code,montant,assiette,null,codeCumul());
	}


	/** ajoute un resultat de calcul de remunration sur base horaire au dictionnaire
    retourne par la methode calculer()
    @param code code assoce a cette remuneration
    @param montant du calcul 
    @param montant du calcul pour un temps plein
    @param nbHeures nombre d'heures payees
	@param tauxHoraire taux horaire
	 */        

	public void ajouterRemunerationHoraire(EOPayeCode code,BigDecimal montant,Number nbHeures,Number tauxHoraire) {

		ajouterRemunerationHoraire(code,montant,null,nbHeures,tauxHoraire);
	}

	public void ajouterRemunerationHoraire(EOPayeCode code,BigDecimal montant,BigDecimal montantTempsPlein,Number nbHeures,Number tauxHoraire) {

		NSMutableDictionary resultat = new NSMutableDictionary(6);
		resultat.setObjectForKey(montant,"Montant");

		if (montantTempsPlein != null)
			resultat.setObjectForKey(montantTempsPlein,"MontantTempsPlein");

		resultat.setObjectForKey(montant,"Assiette");
		resultat.setObjectForKey(codeCumul(),"CodeCumul");
		resultat.setObjectForKey(nbHeures,"NbHeures");
		resultat.setObjectForKey(tauxHoraire,"TauxHoraire");

		resultats.setObjectForKey(resultat,code);
		
	}


	
	public void ajouterRemunerationFO(EOPayeCode code,BigDecimal montant,BigDecimal montantTempsPlein,Number nbHeures,Number tauxHoraire) {

		NSMutableDictionary resultat = new NSMutableDictionary(6);
		resultat.setObjectForKey(montant,"Montant");

		if (montantTempsPlein != null)
			resultat.setObjectForKey(montantTempsPlein,"MontantTempsPlein");

		// Calcul de l'assiette en fonction du montant des vacations FO
		
		resultat.setObjectForKey(montant,"Assiette");
		
		resultat.setObjectForKey(codeCumul(),"CodeCumul");
		resultat.setObjectForKey(nbHeures,"NbHeures");
		resultat.setObjectForKey(tauxHoraire,"TauxHoraire");

		resultats.setObjectForKey(resultat,code);
		
	}

	
	
	

	/** ajoute une resultat de calcul de remuneration au dictionnaire retourne par la methode calculer()
        @param code code associe au taux utilise dans le calcul
        @param montant du calcul pour ce taux
        @param assiette : assiette de calcul utilisee
	 */        
	public void ajouterRemunerationMontant(EOPayeCode code,BigDecimal montant) {		
		ajouterRemunerationMontant(code,montant,null,montant);
	}
	public void ajouterRemunerationMontant(EOPayeCode code,BigDecimal montant,BigDecimal assiette) {
		ajouterRemunerationMontant(code,montant,null,assiette);
	}
	public void ajouterRemunerationMontant(EOPayeCode code,BigDecimal montant,BigDecimal montantTempsPlein,BigDecimal assiette) {
		NSMutableDictionary resultat = new NSMutableDictionary(4);
		resultat.setObjectForKey(montant,"Montant");
		if (montantTempsPlein != null) {
			resultat.setObjectForKey(montantTempsPlein,"MontantTempsPlein");
		}
		resultat.setObjectForKey(assiette,"Assiette");
		if (codeCumul() != null) {
			resultat.setObjectForKey(codeCumul(),"CodeCumul");
		}
		resultats.setObjectForKey(resultat,code);
	}

	/** met a jour le taux horaire et le nombre d'heures dans la preparation */
	public void mettreAJourPrepa(BigDecimal taux,BigDecimal nbHeures) {
		preparation().setPayeTauxhor(taux.setScale(2, BigDecimal.ROUND_HALF_UP));
		mettreAJourPrepa(nbHeures);
	}
	
	/** met a jour le nombre d'heures dans la preparation : cumule les heures */
	public void mettreAJourPrepa(BigDecimal nbHeures) {

		BigDecimal nbH = nbHeures;
		if (preparation().payeNbheure() == null)
			nbH = nbHeures;
		else 
			// i.e les heures sont fournies dans les rubriques personnelles
			if (periode().pperNbHeure() == null || periode().pperNbHeure().doubleValue() == 0)	
				nbH = (preparation().payeNbheure()).add(nbHeures);

		preparation().setPayeNbheure(nbH.setScale(2,BigDecimal.ROUND_HALF_UP));

	}

}
