/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.moteur;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/** Classe utilitaire comportant des methodes statiques pour calculer le montant des heures supplementaires
 * ou complementaires exonerees<BR>
 * @author christine
 *
 */
public class Tepa_Utilitaires {
	public final static String REMUN_HEURES_SUP_EXONEREES = "REMUNHSE";
	public final static String REMUN_HEURES_COMP_EXONEREES = "REMUNHCE";
	private final static String TAUX_POUR_CSG_CRDS = "TXTXSGRD";
	private final static String CATEGORIE_CSG_CRDS = "CSG CRDS";
	
	public static BigDecimal calculerMontantHeuresSupp(NSArray elements) {
		BigDecimal montantHeureSup = new BigDecimal(0);
		java.util.Enumeration e = elements.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeElement element = (EOPayeElement)e.nextElement();
			if (element.estUneRemuneration()) {
				// Vérifie si c'est une rubrique d'exonération des heures supp/complémentaires
				if (element.rubrique().rubriqueACode(REMUN_HEURES_SUP_EXONEREES) || element.rubrique().rubriqueACode(REMUN_HEURES_COMP_EXONEREES)) {
					montantHeureSup = montantHeureSup.add(element.pelmApayer());
				}
			}
		}
		return montantHeureSup;
	}
	/** Calcule le montant de la cotisation pour heures supplementaires de CRDS et CSG non d&eeacute;ductibles.
	 * 
	 */
	public static BigDecimal calculerMontantCSGCRDSPourMontantHeuresSup(NSArray elements) {
		double montantHeuresSup = calculerMontantHeuresSupp(elements).doubleValue();
		if (montantHeuresSup == 0) {
			return new BigDecimal(0);
		}
		double montant = 0.00;
		java.util.Enumeration e = elements.objectEnumerator();
		EOEditingContext editingContext = ((EOPayeElement)elements.objectAtIndex(0)).editingContext();
		EOPayeCode code = EOPayeCode.rechercherCode(editingContext, TAUX_POUR_CSG_CRDS);
		if (code == null) {
			return new BigDecimal(0);
		}
		EOPayeParam param = EOPayeParam.parametreValide(code);
		if (param == null) {
			return new BigDecimal(0);
		}
		double tauxCRDS = EOPayeParam.parametreValide(code).pparTaux().doubleValue();
		while (e.hasMoreElements()) {
			EOPayeElement element = (EOPayeElement)e.nextElement();
			if (element.rubrique().categorie().categorieLibelle().equals(CATEGORIE_CSG_CRDS) && element.rubrique().estDeductible() == false) {
				double tauxCotisation = element.pelmTaux().doubleValue();
				montant = montant + (montantHeuresSup * tauxCRDS /100 * tauxCotisation / 100);
			}
		}
		return new BigDecimal(montant);
	}
}
