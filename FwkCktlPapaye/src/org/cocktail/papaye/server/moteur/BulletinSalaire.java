
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.papaye.server.moteur;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.common.EOInfoBulletinSalaire;
import org.cocktail.papaye.server.metier.grhum.EOAdresse;
import org.cocktail.papaye.server.metier.grhum.EORibs;
import org.cocktail.papaye.server.metier.jefy_admin.EOAdminDevise;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCumul;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElementLbud;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParamPerso;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePersoLbud;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeRappel;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique;
import org.cocktail.papaye.server.metier.maracuja.EOModePaiement;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/** Cette classe g&egrave;re les informations globales d'un bulletin de salaire d'un bulletin de salaire.
 */
public class BulletinSalaire {
	private static final String PLAFOND_MENSUEL = "PLAFMSSS";
	private static final String NB_PLAFONDS_SS_SUP = "NBPLAFSS";
	private EOEditingContext editingContext;
	private EOPayePeriode periodeCourante;
	private EOPayePrepa preparation;
	private NSArray periodesRappel;
	private NSMutableArray elements;
	private NSMutableArray cumuls;
	private boolean estBulletinReel;
	private int nbPlafondsSupplementaires;		// 24/11/2009 - Ajouter pour les gens qui fournissent des rappels de trait
	private double remunTempsPlein;
	private BigDecimal cumulPlafondPourRappel;

	// utilisé pour stocker la valeur d'un paramètre qui n'est pas à lire dans la base
	// et qui est fourni par l'application d'administration
	private EOPayeParam parametrePourRappel;

	/** Constructeur
        @param edContext editingContext dans lequel inserer les objets a
       sauvegarder dansla base : il doit avoir une valeur non nulle
        @param periodes p&acute;riodes pour lesquelles generer le le bulletin de
        salaire (une dans le cas g&acute;neral, plusieurs dans le cas des rappels)
        @param operation operation de paie pour laquelle ce bulletin de salaire est
        genere
	 */
	public BulletinSalaire(EOEditingContext edContext,EOPayePeriode periode,NSArray autresPeriodes,boolean estBulletinReel) {
		periodeCourante = periode;
		if (autresPeriodes != null && autresPeriodes.count() > 0) {
			periodesRappel = autresPeriodes;
		} else {
			periodesRappel = null;
		}
		editingContext = edContext;
		this.estBulletinReel = estBulletinReel;
		elements = new NSMutableArray();
		cumuls = new NSMutableArray();
		cumulPlafondPourRappel = new BigDecimal(0);
	}

	// Accesseurs
	/** Accesseur : retourne le contrat de ce bulletin de salaire. */
	public EOPayeContrat contrat() {
		return periodeCourante().contrat();
	}

	/** Accesseur : retourne la preparation de ce bulletin de salaire. Elle est creee lors de la generation du bulletin de salaire
	 */
	public EOPayePrepa preparation() { return preparation; }
	/** Accesseur : retourne les elements generes
        pour ce bulletin de salaire.
        Le tableau contient des objets de type EOPayeElement. */
	public NSArray elements() {
		return elements;
	}
	/** Accesseur : retourne les cumuls generes
        pour ce bulletin de salaire.
        Le tableau contient des objets de type EOPayeCumul. */
	public NSArray cumuls() {
		return cumuls;
	}

	/** Lance la fabrication du bulletin de salaire avec un param&egrave;tre de rappel.
        Cette méthode est utilisée par l'application d'administration */
	public void preparerAvecParametrePourRappel(EOPayeParam parametre) throws Exception {
		parametrePourRappel = parametre;
		preparer();
	}
	/** Lance la fabrication du bulletin de salaire */
	public void preparer() throws Exception {
		editingContext().lock();
		try {
			
			NSLog.out.appendln(">>>>> PREPARATION BS de " + contrat().individu().identite() + " ( " + mois().moisComplet()+" )");

			// creer EOPayePrepa et initialiser les plafonds de sécurité sociale
			creerPreparation();

			// RAPPELS
			if (periodesRappel != null && periodesRappel.count() > 0)
				preparerRappels();

			nbPlafondsSupplementaires = -1;
			boolean plafondSecuCalcule = false;
			boolean modifierPlafondSS = false;
			// récupérer toutes les rubriques pour cette période de paye
			// elles sont stockées dans un dictionnaire ainsi que les personnalisations
			NSDictionary dict = contrat().rubriques(mois());
			NSArray rubriques = (NSArray)dict.objectForKey("rubriques");
			NSArray personnalisations = (NSArray)dict.objectForKey("personnalisations");
			
			for (int i = 0; i < rubriques.count();i++) {
				// effectuer le traitement de la rubrique courante
				EOPayeRubrique rubriqueCourante = (EOPayeRubrique)rubriques.objectAtIndex(i);
				EOPayePerso personnalisation = (EOPayePerso)personnalisations.objectAtIndex(i);

				// si le nombre de jours et le nombre d'heures sont égal à zéro et
				// qu'il s'agit d'une rémunération (cas des rappels pour contrat terminé),
				// et qu'il ne s'agit pas d'une rubrique personnelle ponctuelle
				// ou d'une rubrique de rappel utilisée comme rubrique personnelle
				// ne pas calculer cette rubrique
				// Dans tous les autres cas, la calculer
				if (periodeCourante().pperNbJour().intValue() == 0 &&
						periodeCourante().pperNbHeure().doubleValue() == 0 &&
						rubriqueCourante.estAPayer() &&
						((rubriqueCourante.estRubriquePersonnelle() == false &&
								rubriqueCourante.estRubriqueRappel() == false) ||
								(personnalisation.estPermanente()))) {
					;
				} 
				else {
					if (parametrePourRappel != null) {
						// paramètre fourni par l'application
						rubriqueCourante.setParametrePourRappel(parametrePourRappel);
					}
					int nbPeriodes;
					if (periodeCourante().pperNbJour().intValue() == 0 && periodeCourante().pperNbHeure().doubleValue() == 0) {
						// ne pas prendre en compte cette période dans les différents calcul de seuil d'exonération,…
						nbPeriodes = 0;
					} else
						nbPeriodes = 1;

					if (periodesRappel != null && periodesRappel.count() > 0) {
						nbPeriodes += periodesRappel.count();
					}
					if (rubriqueCourante.estRubriqueStatut()) {
						personnalisation = null;
					}

					System.out.println("      -- " + rubriqueCourante.prubLibelle());
					NSDictionary resultats = rubriqueCourante.calculer(periodeCourante(),
							preparation(),
							cumulPlafondPourRappel,
							contrat().estUnCalculDirect(),
							elements(),
							cumuls(),
							personnalisation,
							periodesRappel,
							new Integer(nbPeriodes),false);

					Enumeration<EOPayeCode> enum1 = resultats.keyEnumerator();
					// une rubrique peut renvoyer plusieurs lignes de bulletin de salaire					
					while (enum1.hasMoreElements()) {
						
						EOPayeCode code = enum1.nextElement();
						NSDictionary resultat = (NSDictionary)resultats.objectForKey(code);
						BigDecimal montant = lireDictionnaire(resultat,"Montant");

						BigDecimal assiette =  lireDictionnaire(resultat,"Assiette");
						BigDecimal taux =  lireDictionnaire(resultat,"Taux");

						EOPayeCode codeCumul = (EOPayeCode)resultat.objectForKey("CodeCumul");
						// 28/05/08 - Pour la Polynésie
						int nbDecimales = EOAdminDevise.nbDecimalesPourExercice(editingContext(), annee());
						if (nbDecimales != EOAdminDevise.NB_DECIMAL_DEFAUT) {
							montant = montant.setScale(nbDecimales, BigDecimal.ROUND_HALF_UP);
							assiette = assiette.setScale(nbDecimales, BigDecimal.ROUND_HALF_UP);
						}
						if (salaireARetirer()) {
							// trop perçu de salaire
							montant = montant.negate();
							assiette = assiette.negate();
						}
						if (rubriqueCourante.estAPayer()) {
							if (rubriqueCourante.rentreDansAssiette()) {
								// 22/09/05 - le plafond SS doit être augmenté car cette rubrique est soumise à cotisation sociale
								modifierPlafondSS = true;
								preparation().setPayeBssmois(preparation().payeBssmois().
										add(montant));
								double montantTempsPlein = lireDictionnaire(resultat,"MontantTempsPlein").doubleValue();
								if (montantTempsPlein > 0) {
									remunTempsPlein += montantTempsPlein;
								} else {
									remunTempsPlein += montant.doubleValue();
								}
							}
							if (rubriqueCourante.estImposable()) {
								preparation.setPayeBimpmois(preparation().payeBimpmois().
										add(montant));
							}
							// 24/11/09 - pour le cas des rubriques de rappel de traitement manuelles
							if (personnalisation != null && rubriqueCourante.estRubriqueRappel() && rubriqueCourante.prubClassement().equals("10001")) {
								EOPayeCode codePlafond = EOPayeCode.rechercherCode(editingContext(), NB_PLAFONDS_SS_SUP);
								if (codePlafond != null) {
									EOPayeParamPerso param = EOPayeParamPerso.parametrePersoValide(editingContext(), codePlafond, personnalisation);
									if (param != null && param.pparValeur() != null) {
										int nbPlafonds = new Integer(param.pparValeur()).intValue();
										if (nbPlafonds > 0) {
											nbPlafondsSupplementaires = nbPlafonds;	
										}
									}
								}
							}
							if (rubriqueCourante.estRetenue()) {
								// vérifier si la somme est cohérente avec le montant i.e montant négatif si paiement positif et réciproquement
								/*	if (!salaireARetirer() && montant.signum() > 0) {
									throw new Exception("Pour la rubrique " + rubriqueCourante.prubLibelle() + "\nle montant de la retenue doit etre negatif");
								}
								else */if  (salaireARetirer() && montant.signum() < 0) {	
									throw new Exception("Pour la rubrique " + rubriqueCourante.prubLibelle() + "\nle montant de la retenue doit etre positif, il s'agit d'un salaire a retirer");
								} else {
									preparation.setPayeRetenue(preparation().payeRetenue().add(montant));
								}
							} else if (rubriqueCourante.estAvantageEnNature() == false) {
								// 17/05/06 - pour la subrogation
								// toutes rémunérations en-dehors des retenues et des rubriques de valeur >= 95000
								try {
									int valeur = new Integer(rubriqueCourante.prubClassement()).intValue();
									if (valeur < 95000) {
										preparation.setPayeBrutTotal(preparation().payeBrutTotal().add(montant));
									}
								} catch (Exception exc) {}
							}
						} else {
							// 19/06/06	- si il ne s'agit pas d'un salaire à retirer mais que payeBssMois est négatif (cas des IJ), on
							// inverse le résultat trouvé car tous les calculs ont été faits en valeur absolue
							if (!salaireARetirer() && preparation().payeBssmois().signum() < 0) {
								montant = montant.negate();
								assiette = assiette.negate();
							}
							// à la première cotisation, évaluer le plafond de sécurité sociale
							if (plafondSecuCalcule == false) {
								// si il ne s'agit pas d'un rappel pour un contrat terminé, on ajoute le plafond SS mensuel
								// au cumul de plafond SS. On n'a pas à se préoccuper des autres contrats en cours pour
								// cet agent car dans tous les cas, on a pris le dernier plafond précédent auquel on
								// ajoute le plafond mensuel
								if (periodeCourante().pperNbJour().intValue() != 0 ||
										periodeCourante().pperNbHeure().doubleValue() != 0) {
									cumulerPlafondSS(-1);	// 24/11/09 - pour gérer les rappels de traitement manuels
								}
								if (nbPlafondsSupplementaires > 0) {
									cumulerPlafondSS(nbPlafondsSupplementaires);
								}
								plafondSecuCalcule = true;
							}
							if (rubriqueCourante.estPartPatronale()) {
								// rubrique patronale
								preparation().setPayePatron(preparation().payePatron().add(montant));
							}
						}

						// GENERATION DE L ELEMENT
						EOPayeElement element = ajouterElement(rubriqueCourante,code,montant, assiette, personnalisation);


						// Taux specifique (Peut etre fourni par un plugin)
						if (taux != null && taux.doubleValue() != 0) {	
							taux = taux.setScale(2, BigDecimal.ROUND_HALF_UP);
							element.setPelmTaux(taux);
						}

						// CUMUL
						BigDecimal cumul = lireDictionnaire(resultat,"Cumul");
						if (estBulletinReel()) {
							EOPayeRubrique rubriqueCumul = rubriqueCourante;
							boolean estRappel = rubriqueCourante.estRubriqueRappel();
							if (estRappel) {	// cas des rappels manuels traités dans le flot régulier des rubriques
								// chercher parmi toutes les rubriques la rubrique associée pour trouver le bon cumul
								rubriqueCumul = rubriqueAssocieeRappel(rubriqueCourante,rubriques);
								if (rubriqueCumul == null) {
									throw new Exception("Pas de rubrique du statut de ce contrat correspondant a la rubrique de rappel de cotisation");
								}
							}

							if (cumul.doubleValue() == 0) {
								mettreAJourCumul(rubriqueCumul,codeCumul,code,
										montant,assiette,false,estRappel);
							} else {	// il s'agit d'une cotisation plafonnée
								// remplacer le cumul par la nouvelle valeur fournie
								mettreAJourCumul(rubriqueCumul,codeCumul,code,
										montant,cumul,true,estRappel);
							}
						}

						// Vérifier si c'est une rubrique de type vacation horaire en regardant si une personnalisation existe et si les infos ont été stockées
						if (rubriqueCourante.estAPayer() && rubriqueCourante.estRubriquePersonnelle() && personnalisation != null) {

							Number nbHeures = (Number)resultat.objectForKey("NbHeures");

							// On ecrase l'assiette au profit de cette valeur
							if (nbHeures != null) {
								element.setPelmAssiette(new BigDecimal(nbHeures.doubleValue()).setScale(2,BigDecimal.ROUND_HALF_UP));
								Number tauxHoraire = (Number)resultat.objectForKey("TauxHoraire");
								if (tauxHoraire != null) {
									element.setPelmTaux((new BigDecimal(tauxHoraire.floatValue())).setScale(2, BigDecimal.ROUND_HALF_UP));
								}
							}

							if (estBulletinReel()) {
								// mettre à jour les lignes budgétaires des personnalisations si elles ont été définies
								java.util.Enumeration<EOPayePersoLbud> e = personnalisation.persoLbuds().objectEnumerator();
								while (e.hasMoreElements()) {
									EOPayePersoLbud persoLbud = e.nextElement();
									EOPayeElementLbud elementLbud = new EOPayeElementLbud();
									editingContext().insertObject(elementLbud);
									elementLbud.initAvecPersoLbud(persoLbud);
									elementLbud.setPayeElementRelationship(element);
								}
							}
						}
					}
				}


			}
			if (!modifierPlafondSS) {
				// remettre les cumuls de plafond sécurité sociale à la valeur qu'ils avaient avant le calcul de ce bulletin de salaire
				initialiserPlafondSecu();
			} else if (periodeCourante().salaireARetirer()) {		// 140205 - cas d'un rappel de salaire trop perçu, enlever la part de plafond SS calculée
				recalculerPlafondSecu();
			}

			terminerPreparation();


			// mettre à jour les flags interdisant une destruction possible de certains informations
			if (estBulletinReel()) {
				mettreAJourBooleens();
			}
		} catch (Exception exception) { throw exception; }
		finally { editingContext().unlock(); }
	}
	/** permet d'enregistrer les donnees d'un bulletin de salaire dans la base */
	public void enregistrer() throws Exception {
		editingContext().lock();
		try {    
			if ((preparation != null) && (elements.count() > 0)) {
				editingContext().saveChanges();
			} else {
				throw new Exception("Le bulletin de salaire de l'agent "+ contrat().individu().identite() + " est vide");
			}
		} catch (Exception e) { throw e; }
		finally { editingContext().unlock(); }

	}

	// méthodes privées
	// gestion de EOPayePrepa
	private void creerPreparation() throws Exception {

		preparation = new EOPayePrepa();
		if (estBulletinReel())
			editingContext().insertObject(preparation);

		preparation.initMontants(); 	// initialise tous les montants à 0
		preparation.setPayeAnnee(annee());
		preparation.setPayeMode(contrat().modeCalcul());
		
		if (periodeCourante.modePaiement() != null)
			preparation.setModePaiementRelationship(periodeCourante().modePaiement());
		else
			preparation.setModePaiementRelationship(EOModePaiement.getDefaultModePaiement(editingContext()));	
		
		if (preparation.modePaiement() != null && preparation.modePaiement().estVirement() && !salaireARetirer())
			preparation.setTemDisquette(Constantes.VRAI);
		else
			preparation.setTemDisquette(Constantes.FAUX);

		if (salaireARetirer())
			preparation.setTemCompta(Constantes.FAUX);
		else
			preparation.setTemCompta(Constantes.VRAI);

		preparation.signalerRappel(periodesRappel != null);
		preparation.setVerificationEffectuee(false);
		
		// mettre à jour le cumul de plafond SS avec la valeur du cumul de plafond précédent
		// pour cet agent
		initialiserPlafondSecu();
		// transférer toutes les informations contenues dans le contrat
		preparation.initAvecPeriodeEtContrat(periodeCourante(),contrat());

		// CONTRAT
		preparation.setContratRelationship(contrat());

		// ADRESSE
		EOAdresse adresseCourante = contrat().individu().adresseCourante();
		if (adresseCourante != null)
			preparation.setAdresseRelationship(adresseCourante);

		// RIB
		EORibs ribCourant = contrat().individu().ribCourant();
		if (ribCourant != null)
			preparation.setRibRelationship(ribCourant);
		

		preparation.setMoisRelationship(moisTraitement());
	}

	// met à jour EOPayePrepa et l'ensemble des cumuls associés aux montants
	// Calcule le montant Net = Somme des rémunérations - Somme des déductions
	// Calcule le montant Net imposable = Somme des rémunérations imposables - Somme des déductions
	// non imposables
	// Calcule la baseSS = somme des rémunérations rentrant dans la base sécu
	private void terminerPreparation() {
		BigDecimal cotisationsSalariales = new BigDecimal(0);
		BigDecimal cotisationsPatronales = new BigDecimal(0);	// 05/03/07 - elles ont pu être modifiées pour les assedic
		BigDecimal netImposable = new BigDecimal(0);
		BigDecimal montantNet = new BigDecimal(0);
		if (contrat().estUnCalculDirect()) {
		} else {	// pour les intermittents, le net est fourni
			if (contrat().montantMensuelRemu() != null &&
					contrat().montantMensuelRemu().doubleValue() != 0) {
				montantNet = new BigDecimal(contrat().montantMensuelRemu().doubleValue()).setScale(2,BigDecimal.ROUND_HALF_DOWN);
			} else {
				if (contrat().montantForfaitaire() != null) {
					montantNet = new BigDecimal(contrat().montantForfaitaire().doubleValue()).setScale(2,BigDecimal.ROUND_HALF_DOWN);
				}
			}
		}
		BigDecimal montantBrut = new BigDecimal(0);
		BigDecimal montantBrutAvecAvantage = new BigDecimal(0);
		BigDecimal coutEmployeur = new BigDecimal(0);

		Enumeration<EOPayeElement> e = elements().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeElement element = e.nextElement();
			if (element.estUneRemuneration()) {
				boolean	netUniquement = false;
				try {
					int valeur = new Integer(element.rubrique().prubClassement()).intValue();
					if (valeur >= 95000) {
						netUniquement = true;
					}
				} catch (Exception exc) {}
				// l'ajouter au Net imposable si c'est une rubrique imposable
				if (element.rubrique().estImposable()  && element.code().pcodCode().equals("REMUNITC") == false) {
					netImposable = netImposable.add(element.pelmApayer());
					montantBrutAvecAvantage = montantBrutAvecAvantage.add(element.pelmApayer());
				}
				// L'ajouter au montant brut
				if (element.rubrique().estAvantageEnNature() == false && netUniquement == false && element.rubrique().rentreDansAssiette()) {
					montantBrut = montantBrut.add(element.pelmApayer());
				}
				// ne pas prendre en compte les retenues dans les rubriques ou les rubriques de rappel
				if ((element.rubrique().estAvantageEnNature() == false && element.rubrique().estRetenue() == false && netUniquement == false) || 
						(element.rubrique().estRubriqueRappel() && element.rubrique().rubriquePourRubriqueRappel() != null && element.rubrique().rubriquePourRubriqueRappel().estRetenue() == false)) {
					coutEmployeur = coutEmployeur.add(element.pelmApayer());
				}
				if (contrat().estUnCalculDirect()) {
					if (element.rubrique().estAvantageEnNature() == false) {
						montantNet = montantNet.add(element.pelmApayer());	
					}
				}
			} else if (element.estUneDeduction()) {
				// le retirer du Net imposable si c'est une rubrique déductible
				if (element.rubrique().estDeductible()) {
					netImposable = netImposable.subtract(element.pelmAdeduire());
				}
				cotisationsSalariales = cotisationsSalariales.add(element.pelmAdeduire());
				if (contrat().estUnCalculDirect()) {
					montantNet = montantNet.subtract(element.pelmAdeduire());
				}
				// 10/06/08 - on teste sur le témoin titulaire du statut et non celui de personnel
				if (contrat().statut().temTitulaire().equals(Constantes.VRAI) && 
						(element.code().pcodCode().equals("TXREDHSF") || element.code().pcodCode().equals("TXREDHSU"))) {
					// Remboursement de charges pour heures exonérées, elles sont prises en compte dans
					// le coût total pour versement aux différentes caisses. Uniquement pour les titulaires
					//						coutEmployeur = coutEmployeur.add(element.pelmAdeduire().abs());
					coutEmployeur = coutEmployeur.add(element.pelmAdeduire().multiply(new BigDecimal(-1)));

				}
			} else if (element.estUneChargePatronale()) {
				cotisationsPatronales = cotisationsPatronales.add(element.pelmPatron());
				coutEmployeur = coutEmployeur.add(element.pelmPatron());
			}
		}
		// 16/07/08 - modification pour calculer le net imposable en cas d'heures supplémentaires exonérées
		// Il est égal au net imposable calculé normalement en ne prenant pas en compte toutes les rémunérations
		// non imposables et les déductions non déductibles
		// Il faut ensuite enlever le montant de la CSG non déductible/CRDS calculé sur les heures supplémentaires
		BigDecimal montantCSGCRDSPourHeuresSup = Tepa_Utilitaires.calculerMontantCSGCRDSPourMontantHeuresSup(elements());
		if (montantCSGCRDSPourHeuresSup.doubleValue() != 0) {
			if (netImposable.doubleValue() < 0 && !salaireARetirer()) {
				// On est dans le cas où la rémunération se limite aux heures exonérées
				netImposable = new BigDecimal(0);
			} else {
				montantCSGCRDSPourHeuresSup = montantCSGCRDSPourHeuresSup.setScale(2,BigDecimal.ROUND_HALF_UP);
				netImposable = netImposable.subtract(montantCSGCRDSPourHeuresSup);
			}
		}

		netImposable = arrondir(netImposable);
		cotisationsSalariales = arrondir(cotisationsSalariales);
		cotisationsPatronales = arrondir(cotisationsPatronales);
		montantNet = arrondir(montantNet);
		// mettre à jour l'entité prépa avec les différents éléments
		preparation().setPayeBrut(montantBrut.setScale(2,BigDecimal.ROUND_HALF_UP));
		preparation().setPayeNet(montantNet.setScale(2,BigDecimal.ROUND_HALF_UP));
		preparation().setPayeBimpmois(netImposable.setScale(2,BigDecimal.ROUND_HALF_UP));
		preparation().setPayeAdeduire(cotisationsSalariales.setScale(2,BigDecimal.ROUND_HALF_UP));
		preparation().setPayePatron(cotisationsPatronales.setScale(2,BigDecimal.ROUND_HALF_UP));
		preparation().setPayeCout(coutEmployeur.setScale(2,BigDecimal.ROUND_HALF_UP));
		// met à jour les cumuls de remuneration totale, base SS, net, net imposable,
		// coût total patronal
		try {
			mettreAJourCumulGlobal(EOPayeCode.REMUN_TOT,preparation().payeBrut());
			mettreAJourCumulGlobal(EOPayeCode.CODEBASESS,preparation().payeBssmois());
			mettreAJourCumulGlobal(EOPayeCode.NET,preparation().payeNet());
			mettreAJourCumulGlobal(EOPayeCode.NETIMPOS,preparation().payeBimpmois());
			mettreAJourCumulGlobal(EOPayeCode.COUTPATRONAL,preparation().payeCout());
			mettreAJourCumulGlobal(EOPayeCode.REMUN_TOT_ET_AVANTAGES,montantBrutAvecAvantage);
		} catch (Exception ee) {
			ee.printStackTrace();
		}
	}

	// initailise le plafond du mois et le cumul de plafond aux valeurs qu'ils avaient précédemment (soit pour une période précédente, soit pour
	// autre contrat si l'agent a plusieurs contrats
	private void initialiserPlafondSecu() throws Exception {
		NSDictionary plafonds = EOInfoBulletinSalaire.cumulPlafondsPrecedent(editingContext(),contrat().individu());
		preparation.setPayeCumulPlafond(lireDictionnaire(plafonds,"CumulPlafond"));
		preparation.setPayePlafondDuMois(lireDictionnaire(plafonds,"PlafondDuMois"));
		
		NSLog.out.appendln("Cumul plafond SS " + preparation.payeCumulPlafond());

	}

	// ajoute au plafond du mois et au cumul de plafond la valeur du plafond pour ce contrat
	private void cumulerPlafondSS(int nbPlafonds) {
		// 24/11/09 - modification pour forcer le calcul du plafond dans le cas de rappels manuels
		// on vérifie que le plafond du mois ne dépasse pas le plafond sécurité social mensuel
		// pour gérer le cas des multi-contrats :
		//		- sur un même mois qui n'ont pas été réalisés durant toute la période
		//		- ou avec plafonds réduits : lorsqu'il y a plusieurs contrats avec des plafonds réduits, on ne peut dépasser le plafond SS mensuel

		EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),PLAFOND_MENSUEL);
		EOPayeParam paramPlafond = EOPayeParam.parametreValide(code);
		double plafondMensuel = paramPlafond.pparMontant().doubleValue();
		double cumul = preparation().payeCumulPlafond().doubleValue();
		double plafondDuMois = preparation().payePlafondDuMois().doubleValue();
		double nouveauPlafond = calculerPlafondSS(periodeCourante()).doubleValue();

		if (nbPlafonds == -1) {
			if (plafondDuMois + nouveauPlafond > plafondMensuel) {
				// on est au dessus du plafond SS. Ramener au plafond SS
				cumul = cumul - plafondDuMois + plafondMensuel;
				preparation().setPayePlafondDuMois(paramPlafond.pparMontant());
			} else {
				// on est en dessous, ajouter le nouveau plafond
				cumul = cumul + nouveauPlafond;
				preparation().setPayePlafondDuMois(new BigDecimal(plafondDuMois + nouveauPlafond).setScale(2,BigDecimal.ROUND_HALF_UP));
			}
		} else {
			cumul = cumul + (nouveauPlafond * nbPlafonds);
			preparation().setPayePlafondDuMois(new BigDecimal(plafondDuMois + (nouveauPlafond * nbPlafonds)).setScale(2,BigDecimal.ROUND_HALF_UP));
		}
		preparation.setPayeCumulPlafond(new BigDecimal(cumul).setScale(2,BigDecimal.ROUND_HALF_UP));

	}
	// appelée lorsque le BS est négatif
	private void recalculerPlafondSecu() {
		try {
			double plafondDuMoisCourant = preparation().payePlafondDuMois().doubleValue();
			// 14/02/05 - pour effectuer les calculs de cotisation, on a pris les valeurs positives des plafonds
			// maintenant remettre les plafonds à leur valeur après déduction de manière à rediminuer les plafonds puisque 
			// le BS est payé en négatif
			initialiserPlafondSecu();
			// on laisse le plafond du mois à la valeur qu'il avait auparavant. Si pas d'autres BS => vaut zéro
			/*double ancienPlafondDuMois = preparation().payePlafondDuMois().doubleValue();
			double result = ancienPlafondDuMois - plafondDuMoisCourant;
			preparation.setPayePlafondDuMois(new BigDecimal(result).setScale(2,BigDecimal.ROUND_HALF_UP));*/
			// 23/09/05 on enlève le plafond du mois de l'ancien cumul de manière à prendre en compte
			// le salaire payé en négatif (i.e remboursement de charges). le cumul de plafond doit être inférieur au dernier cumul
			double ancienCumulDuMois = preparation().payeCumulPlafond().doubleValue();
			double result =  ancienCumulDuMois - plafondDuMoisCourant;
			if (result < 0) {
				result = 0;		// pas de cumul négatif
			}
			preparation.setPayeCumulPlafond(new BigDecimal(result).setScale(2,BigDecimal.ROUND_HALF_UP));


		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// Calcule le plafond SS standard pour la période.
	private BigDecimal calculerPlafondSS(EOPayePeriode periode) {
		double plafond = 0;
		// vérifier si un plafond manuel a été saisi
		if (contrat().montantPlafondSecu() != null) {
			plafond = contrat().montantPlafondSecu().doubleValue();
		} else {
			EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),PLAFOND_MENSUEL);
			EOPayeParam paramPlafond;
			if (periode == periodeCourante()) {
				paramPlafond = EOPayeParam.parametreValide(code);
			} else {
				paramPlafond = EOPayeParam.parametrePourMois(editingContext(), code, periode.mois().moisCode());
			}

			plafond = paramPlafond.pparMontant().doubleValue();
			if (contrat().tauxPlafondSecu() != null && contrat().tauxPlafondSecu().doubleValue() > 0) {
				plafond = (plafond * contrat().tauxPlafondSecu().doubleValue()) / 100;
			}
			// dans le cas où on n'a pas travaillé tout le mois, le plafond réel est proportionnel à
			// la quotité travaillée
			if (periode.temComplet().equals(Constantes.FAUX)) {
				plafond = (plafond * periode.pperNbJour().intValue()) / 30;
			}
			if (contrat().utiliserPlafondReduit() && remunTempsPlein > 0) {
				plafond = (plafond * preparation().payeBssmois().doubleValue()) / remunTempsPlein;
			}
		}
		return new BigDecimal(plafond).setScale(2,BigDecimal.ROUND_HALF_DOWN);
	}



	/**
	 * gestion des rappels
	 * prepare l'ensemble des rappels pour ce contrat
	 */
	private void preparerRappels() throws Exception {
		Enumeration<EOPayePeriode> e = periodesRappel.objectEnumerator();
		NSMutableArray resultats = new NSMutableArray();
		while (e.hasMoreElements()) {
			EOPayePeriode periode = e.nextElement();
			// Ajouter les jours de rappel dans PayePrepa
			if (periode.pperNbJour() != null) {
				int nbJoursTotal = preparation().payeNbjour().intValue() + periode.pperNbJour().intValue();
				preparation().setPayeNbjour(new BigDecimal(nbJoursTotal));
			}
			EOPayeContrat ancienContrat = periode.contrat().contratPrecedent();
			// Déterminer toutes les rubriques concernées
			// elles sont stockees dans un dictionnaire ainsi que les personnalisations
			// on ne récupère que les rubriques de rémunération
			NSDictionary dict = periode.contrat().rubriquesRappelPourMois(periode.moisTraitement());
			NSArray rubriques = (NSArray)dict.objectForKey("rubriques");
			NSArray personnalisations = (NSArray)dict.objectForKey("personnalisations");
			for (int i = 0; i < rubriques.count();i++) {
				EOPayeRubrique rubrique = (EOPayeRubrique)rubriques.objectAtIndex(i);
				EOPayePerso personnalisation = (EOPayePerso)personnalisations.objectAtIndex(i);
				if (rubrique.estRubriqueStatut()) {
					personnalisation = null;
				}
				if (parametrePourRappel != null) {
					// paramètre (EOPayeParam) fourni par l'extérieur
					rubrique.setParametrePourRappel(parametrePourRappel);
				}
				// calculer le montant pour cette rubrique
				NSLog.out.appendln("Rappel de " + rubrique.prubLibelle());

				// lors des calculs d'un rappel, ne pas passer le tableau des périodes de rappel car les rappels
				// ne sont calculés que pour les rémunérations et seule la période courante importe
				NSDictionary resultatsCalcul = rubrique.calculer(periode,preparation(),cumulPlafondPourRappel,
						contrat().estUnCalculDirect(),
						elements(),cumuls(),personnalisation,null,
						new Integer(0),
						true);
				if (resultatsCalcul.count() > 0) {

					// on attend un seul élément				
					EOPayeCode code = null; 
					for (Enumeration<EOPayeCode> e1 = resultatsCalcul.keyEnumerator();e1.hasMoreElements();)
						code = (EOPayeCode)e1.nextElement();

//					EOPayeCode code = (EOPayeCode) resultatsCalcul.keyEnumerator();
					NSDictionary resultatCalcul = (NSDictionary)resultatsCalcul.objectForKey(code);
					
					// préparer un dictionnaire contenant les informations à passer aux autres méthodes
					// montant, assiette, rappel, rubrique
					// on ne passe pas de cumul car dans le calcul des rémunérations, les cumuls
					// sont modifiés de façon linéaire
					NSMutableDictionary resultat = new NSMutableDictionary(resultatCalcul);
					resultat.setObjectForKey(code,"Code");
					BigDecimal montant = lireDictionnaire(resultat,"Montant");
					BigDecimal assiette = lireDictionnaire(resultat,"Assiette");
					if (ancienContrat != null) {	// rappel partiel
						// rechercher dans l'historique ce qui va été calculé à l'époque
						// pour ce contrat et cette rubrique
						EOPayeElement ancienElement = EOPayeElement.elementPourRubriqueContratEtMois(
								editingContext(),
								rubrique,
								ancienContrat,
								periode.moisTraitement());
						montant = montant.subtract(ancienElement.pelmApayer());
						assiette = assiette.subtract(ancienElement.pelmAssiette());
						resultat.setObjectForKey(montant,"Montant");
						resultat.setObjectForKey(assiette,"Assiette");
					}
					// Cas des trop-perçus
					if (periode.salaireARetirer()) {
						// trop perçu de salaire
						montant = montant.negate();
						assiette = assiette.negate();
						resultat.setObjectForKey(montant,"Montant");
						resultat.setObjectForKey(assiette,"Assiette");
					}
					if (montant.doubleValue() != 0) { // modification de contrat avec uniquement
						// changement du SFT par exemple, la rémunération principale ne change pas
						// => montant = 0
						// ajouter au brut et à la base sécu si nécessaire
						if (rubrique.estAPayer()) {
							if (rubrique.rentreDansAssiette()) {
								preparation().setPayeBssmois(preparation().payeBssmois().
										add(montant));
							}
							if (rubrique.estImposable()) {
								preparation.setPayeBimpmois(preparation().payeBimpmois().
										add(montant));
							}
							if (rubrique.estRetenue()) {
								// vérifier si la somme est cohérente avec le montant i.e montant négatif si paiement positif et réciproquement
								// 04/12/09 - il peut y avoir des retenues positives pour des remboursements
								/*if (!salaireARetirer() && montant.signum() > 0) {
									throw new Exception("Pour la rubrique " + rubrique.prubLibelle() + "\nle montant de la retenue doit etre negatif");
								}
								else */if  (salaireARetirer() && montant.signum() < 0) {	
									throw new Exception("Pour la rubrique " + rubrique.prubLibelle() + "\nle montant de la retenue doit etre positif, il s'agit d'un salaire a retirer");
								} else {
									preparation.setPayeRetenue(preparation().payeRetenue().add(montant));
								}
							} else {
								// 17/05/06 - pour la subrogation
								// toutes rémunérations en-dehors des retenues et des rubriques de valeur >= 95000 ne sont pas prises en compte
								try {
									int valeur = new Integer(rubrique.prubClassement()).intValue();
									if (valeur < 95000) {
										preparation.setPayeBrutTotal(preparation().payeBrutTotal().add(montant));
									}
								} catch (Exception exc) {}
							}
						}
						// créer le rappel en vue du stockage dans la base
						EOPayeRappel rappel = ajouterRappel(rubrique,montant,assiette,periode);
						resultat.setObjectForKey(rappel,"Rappel");
						resultat.setObjectForKey(rubrique,"Rubrique");
						resultats.addObject(resultat);
					}
				}
			}
			if (ancienContrat == null) {
				// rappel total - mettre à jour le cumul de plafond SS.
				BigDecimal plafondSS = calculerPlafondSS(periode);
				BigDecimal plafondTotal = preparation().payeCumulPlafond().add(plafondSS);
				preparation().setPayeCumulPlafond(plafondTotal);
				cumulPlafondPourRappel = cumulPlafondPourRappel.add(plafondSS);
			}  
		}
		// préparer les éléments
		ajouterElementsPourRappels(resultats);
	}
	// crée un nouveau rappel, l'initialise et l'insère dans l'editing context
	private EOPayeRappel ajouterRappel(EOPayeRubrique rubrique,BigDecimal montant,
			BigDecimal assiette,EOPayePeriode periode) {
		EOPayeRappel rappel =  new EOPayeRappel();
		// l'ajouter à l'editing context courant
		if (estBulletinReel()) {
			editingContext().insertObject(rappel);
		}
		rappel.initAvecRubriqueEtPeriode(rubrique.rubriqueRappel(),periode);
		rappel.setPrapApayer(montant);
		rappel.setPrapAssiette(assiette);
		return rappel;
	}
	private void ajouterElementsPourRappels(NSArray resultats) {
		
		// identifier les différentes rubriques à traiter via les codes
		
		NSMutableArray codes = new NSMutableArray();
		for (Enumeration<NSDictionary> e = resultats.objectEnumerator();e.hasMoreElements();) {
			NSDictionary resultat = e.nextElement();
			EOPayeCode code = (EOPayeCode)resultat.objectForKey("Code");
			if (codes.containsObject(code) == false) {
				codes.addObject(code);
			}
		}

		Enumeration<EOPayeCode> e1 = codes.objectEnumerator();
		while (e1.hasMoreElements()) {
			EOPayeCode code = e1.nextElement();
						
			BigDecimal montant = new BigDecimal(0);
			BigDecimal assiette = new BigDecimal(0);
			EOPayeRubrique rubriqueCourante = null;
			EOPayeCode codeCumul = null;
			for (Enumeration<NSDictionary> e2 = resultats.objectEnumerator();e2.hasMoreElements();) {
				NSDictionary resultat = e2.nextElement();
				if (code == (EOPayeCode)resultat.objectForKey("Code")) {
					rubriqueCourante = (EOPayeRubrique)resultat.objectForKey("Rubrique");
					montant = montant.add(lireDictionnaire(resultat,"Montant"));
					assiette = assiette.add(lireDictionnaire(resultat,"Assiette"));
					codeCumul = (EOPayeCode)resultat.objectForKey("CodeCumul");
				}

			}
			// Mettre à jour la relation entre les rappels et les éléments
			// générer l'élément associé
			EOPayeElement element = ajouterElement(rubriqueCourante.rubriqueRappel(),code,montant,assiette);
			for (Enumeration<NSDictionary> e3 = resultats.objectEnumerator();e3.hasMoreElements();) {
				NSDictionary resultat = e3.nextElement();
				if (code == (EOPayeCode)resultat.objectForKey("Code")) {
					EOPayeRappel rappel = (EOPayeRappel)resultat.objectForKey("Rappel");
					rappel.setElementRelationship(element);
				}
			}
			// mettre à jour le cumul de rubrique 
			mettreAJourCumul(rubriqueCourante,codeCumul,code,montant,assiette,false,true);
			
		}
	}
	
	
	private void mettreAJourBooleens() {
		// dans la période
		if (periodeCourante().temPremierePaye().equals(Constantes.VRAI)) {
			periodeCourante().setTemPremierePaye(new String(Constantes.FAUX));
		}
		// dans le contrat
		if (contrat().temPayeUtile().equals(Constantes.FAUX)) {
			contrat().setTemPayeUtile(new String(Constantes.VRAI));
		}
		// dans le RIB
		if (preparation().rib() != null) {
			if (preparation().rib().temPayeUtil().equals(Constantes.FAUX)) {
				preparation().rib().setTemPayeUtil(new String(Constantes.VRAI));
			}
		}
		// dans l'adresse
		if (preparation().adresse() != null && preparation().adresse().temPayeUtil().equals(Constantes.FAUX)) {
			preparation().adresse().setTemPayeUtil(new String(Constantes.VRAI));
		}
	}
	// gestion  des cumuls
	private EOPayeRubrique rubriqueAssocieeRappel(EOPayeRubrique rubrique,NSArray rubriques) {
		Enumeration<EOPayeRubrique> e = rubriques.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeRubrique rubriqueCourante = e.nextElement();
			if (rubriqueCourante.rubriqueRappel() == rubrique) {
				return rubriqueCourante;
			}
		}
		return null;
	}
	private EOPayeCumul preparerCumulDuMois(EOPayeRubrique rubrique,EOPayeCode code) {
		// rechercher si ce cumul est déjà défini pour ce mois, cet agent et cette structure, cette rubrique et ce code (cas des rubriques qui peuvent associer plusieurs codes)
		EOPayeCumul cumulCourant = EOPayeCumul.rechercherCumul(editingContext(),mois(),rubrique,code.pcodCode(),contrat().individu(),contrat().structure());
		if (cumulCourant == null) {
			// vérifier si il n'existe pas dans les cumuls juste créés suite à des rappels
			if (rubrique != null) {
				// vérifier si cumul existe dans les cumuls juste créés
				Enumeration<EOPayeCumul> e = cumuls.objectEnumerator();
				while (e.hasMoreElements())  {
					EOPayeCumul cumul = e.nextElement();
					if (cumul.rubrique().prubLibelle().equals(rubrique.prubLibelle()) && cumul.code().pcodCode().equals(code.pcodCode())) {
						cumulCourant = cumul;
						break;
					}
				}
			}
			if (cumulCourant == null) {
				cumulCourant = new EOPayeCumul();
				// initialiser le cumul
				String mode;
				if (rubrique != null) {
					mode = rubrique.prubMode();
				} else {
					// cas des cumuls globaux
					mode = EOPayeCumul.CUMUL_GLOBAL;
				}
				cumulCourant.init(annee(),mois(),mode,code,rubrique,contrat().structure(), contrat().individu());
				if (estBulletinReel()) {
					// si on n'est pas en janvier => remise des cumuls à zéro
					if ((mois().moisCode().intValue() % 100) != 1) {
						// Rechercher le dernier cumul pour le même agent, la même structure, cette rubrique et ce code (cas des rubriques qui peuvent associer plusieurs codes)
						EOPayeCumul cumulPrecedent = EOPayeCumul.rechercherCumulPrecedent(editingContext(),mois(),rubrique,code.pcodCode(),contrat().individu(),contrat().structure());
						if (cumulPrecedent != null) {
							BigDecimal montantTotal = cumulPrecedent.pcumMontant().add(cumulPrecedent.pcumRegul());		// au cas où le cumul précédent comportait un rappel
							cumulCourant.setPcumMontant(montantTotal);
						}
						// rechercher l'assiette du dernier cumul pour ce code cumul et cet agent
						cumulPrecedent = EOPayeCumul.rechercherDernierCumul(editingContext(),mois(),code.pcodCode(),contrat().individu());
						if (cumulPrecedent != null) {
							// on le trouve : initialiser l'assiette de cumul avec l'ancienne valeur
							cumulCourant.setPcumBase(cumulPrecedent.pcumBase());
						}
					}
					editingContext().insertObject(cumulCourant);
				}
			}
		}
		if (cumuls.containsObject(cumulCourant) == false) {
			cumuls.addObject(cumulCourant);
		}
		return cumulCourant;
	}

	// Identifie le cumul du mois et ajoute le montant calculé
	// paramètres : rubrique concernée, cumul précédent, montant à ajouter au cumul, code associé à cette rubrique
	private void mettreAJourCumul(EOPayeRubrique rubrique,EOPayeCode codeCumul,EOPayeCode code,BigDecimal montant,BigDecimal assiette,boolean remplacerCumul,boolean estRappel) {
		EOPayeCumul cumul = preparerCumulDuMois(rubrique,codeCumul);

		BigDecimal taux = new BigDecimal(0);

		if (rubrique != null && !rubrique.estAPayer() && rubrique.estRubriqueStatut()) {
			// cotisation : récupérer le taux
			if (rubrique.estRubriqueStatut() && EOPayeParam.parametreValide(code) != null) { // cas des rappels manuels de cotisation où le taux est nul
				taux = EOPayeParam.parametreValide(code).pparTaux();
			} 
		}
		// rémunération ou cumul global : mettre à jour le montant
		cumul.mettreAJourMontant(montant,taux,estRappel);

		// mettre à jour les assiettes
		mettreAJoursAssietteToutCumulDuMois(cumul,assiette,remplacerCumul);

	}
	// met à jour un cumul global qui n'est rattaché à aucune rubrique
	private void mettreAJourCumulGlobal(String stringCode,BigDecimal montant) {
		EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),stringCode);
		mettreAJourCumul(null,code,code,montant,montant,false,false);
	}
	
	// met à jour l'assiette de tous les cumuls du mois (de toutes les structures) pour cet agent et ce code (fournis par le cumul)
	private void mettreAJoursAssietteToutCumulDuMois(EOPayeCumul cumul,BigDecimal assiette,boolean remplacerCumul) {
		// mettre à jour le cumul courant
		cumul.mettreAJourAssiette(assiette,remplacerCumul);
		// mettre à jour tous les cumuls de la famille de cumuls
		NSArray cumuls = EOPayeCumul.rechercherCumuls(editingContext(),cumul.mois(),cumul.code().pcodCode(),cumul.agent());
		Enumeration<EOPayeCumul> e = cumuls.objectEnumerator();
		while (e.hasMoreElements()) {

			EOPayeCumul cumulCourant = e.nextElement();
			if (cumulCourant != cumul)
				cumulCourant.mettreAJourAssiette(assiette,remplacerCumul);

		}
	}
	
	
	// gestion des éléments
	// crée un nouvel élément, l'initialise et l'insère dans le tableau des éléments
	private EOPayeElement ajouterElement(EOPayeRubrique rubrique,EOPayeCode code,BigDecimal montant,BigDecimal assiette) {
		return ajouterElement(rubrique,code,montant,assiette,null);
	}
	private EOPayeElement ajouterElement(EOPayeRubrique rubrique,EOPayeCode code,BigDecimal montant,BigDecimal assiette,EOPayePerso personnalisation) {

		EOPayeElement element = new EOPayeElement();

		// l'ajouter à l'editing context courant
		if (estBulletinReel()) {
			editingContext().insertObject(element);
		}
		element.initAvecRubriqueEtCode(rubrique,code);

		element.setPelmMoisCode(mois().moisCode());

		if (rubrique.estAPayer()) {

			element.setPelmType(EOPayeElement.TYPE_REMUNERATION);
			element.setPelmApayer(montant);
			element.setPelmAssiette(assiette);

		} else if (rubrique.estADeduire()) {

			element.setPelmType(EOPayeElement.TYPE_DEDUCTION);
			element.setPelmAdeduire(montant);
			element.setPelmAssiette(assiette);

			if (EOPayeParam.parametreValide(code) != null && EOPayeParam.parametreValide(code).pparTaux() != null) {
				element.setPelmTaux((EOPayeParam.parametreValide(code).pparTaux()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
		} else { 	// part patronale
			element.setPelmType(EOPayeElement.TYPE_PART_PATRONALE);
			element.setPelmPatron(montant);
			element.setPelmAssiette(assiette);
			if (rubrique.estRubriqueStatut()) {
				element.setPelmTaux((EOPayeParam.parametreValide(code).pparTaux()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
		}
		if (personnalisation != null) {	// pour la personnalisation des rubriques personnelles
			element.setPelmMode(EOPayeElement.PROFIL_PERSONNEL);
			element.setPelmLibelle(personnalisation.persLibelle());
		} else {
			// dans les autres cas le libellé est initialisé avec le libellé de la rubrique
			// dans la méthode initAvecRubriqueEtCode
			element.setPelmMode(EOPayeElement.PROFIL_TYPE);
		}

		preparation().addObjectToBothSidesOfRelationshipWithKey(element,"elements");

		elements.addObject(element);
		return element;
	}
	private BigDecimal lireDictionnaire(NSDictionary dictionnaire, String cle) {
		BigDecimal valeur = (BigDecimal)dictionnaire.objectForKey(cle);
		return (valeur == null) ? new BigDecimal(0) : valeur;
	}

	// Accesseurs privés
	private EOEditingContext editingContext() { return editingContext; }
	private EOPayePeriode periodeCourante() { return periodeCourante; }
	private EOPayeMois mois() {
		if (periodeCourante().mois().moisCode().intValue() != EOPayePeriode.MOIS_GENERIQUE) {
			return periodeCourante().mois();
		} else {
			return EOPayeMois.moisCourant(editingContext());
		}

	}
	// retourne le mois à laquelle le traitement aurait dû être réellement effectué
	// à cause des rappels, cela peut ne pas être le mois courant
	private EOPayeMois moisTraitement() {
		if (periodeCourante().moisTraitement().moisCode().intValue() != EOPayePeriode.MOIS_GENERIQUE) {
			return periodeCourante().moisTraitement();
		} else {
			return EOPayeMois.moisCourant(editingContext());
		}
	}
	private boolean salaireARetirer() {
		return periodeCourante().salaireARetirer();
	}
	private Integer annee() {
		return mois().moisAnnee();
	}
	private boolean estBulletinReel() { return estBulletinReel; }

	// Si un montant vaut 0.01 ou -0.01, on retourne la valeur 0
	private BigDecimal arrondir(BigDecimal montant) {
		montant = montant.setScale(2,BigDecimal.ROUND_HALF_UP);
		double montantArrondi = montant.doubleValue();
		if (montantArrondi == 0.01 || montantArrondi == -0.01) {
			// On considère que c'est une valeur nulle
			return new BigDecimal(0);
		} else {
			return montant;
		}
	}
}
