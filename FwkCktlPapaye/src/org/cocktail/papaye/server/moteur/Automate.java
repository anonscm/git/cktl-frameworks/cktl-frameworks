/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.moteur;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import org.cocktail.papaye.server.calcul.remuneration.CalculTraitementGlobal;
import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.common.EOInfoBulletinSalaire;
import org.cocktail.papaye.server.metier.grhum.EOIndividu;
import org.cocktail.papaye.server.metier.grhum.EOStructure;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCumul;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElementLbud;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeEtape;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeOper;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePerso;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeRappel;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/** Cette classe comporte des methodes de classe utiles pour l'automatisation du traitement de la paye */
public class Automate {
	private static final String CODE_TAXE_SUR_SALAIRE = "COTTXSA";
	private static final String CODE_TRANCHE1 = "COTTXSAL";
	private static final String CODE_TRANCHE2 = "COTTXSA2";
	private static final String CODE_TRANCHE3 = "COTTXSA3";

	private static LanceurCalcul lanceurCalcul;
	/** methode de classe qui gen&egrave;re toutes les preparations
    pour cette operation de paye dans les secteurs non autonomes
    @param editingContext editing context dans lequel sauver les objets
    @return nombre de preparations realisees */
	public static int toutPreparer(EOEditingContext editingContext) throws Exception {
		EOStructure etablissement = EOStructure.rechercherEtablissement(editingContext);
		if (!etablissement.estSectorisee()) {
			return Automate.toutPreparerPourSecteur(editingContext,null);
		} else {
			NSArray secteurs = EOPayeSecteur.rechercherSecteursNonAutonomes(editingContext);
			Enumeration e = secteurs.objectEnumerator();
			int total = 0;
			while (e.hasMoreElements()) {
				EOPayeSecteur secteur = (EOPayeSecteur)e.nextElement();
				total += Automate.toutPreparerPourSecteur(editingContext,secteur);
			}
			return total;
		}
	}
	
    public static void finirOperation(EOEditingContext editingContext) throws Exception {
   		editingContext.lock();
    		try {   
           // fermer les opérations précédentes
            EOPayeOper.fermerOperationsCourantes(editingContext);
            // vider la table des étapes
            EOPayeEtape.supprimerEtapes(editingContext);
            EOPayeMois.setMoisCourant(null); 		// mois de la paie
            editingContext.saveChanges();
        } catch (Exception e) {
            throw e;
        }
        finally { editingContext.unlock(); }
    }

    
	/** methode de classe qui verifie d'un bloc tous les contrats
    sur des periodes indeterminees pour un secteur donne
    @param editingContext editing context dans lequel sauver les objets
    @param secteur secteur concerne
    @return nombre de verifications effectuees */
	public static int verifierTousContratsDureeIndetermineePourSecteur(EOEditingContext editingContext,
			EOPayeSecteur secteur) throws Exception {
		int count = 0;
		editingContext.lock();
		try {      
			EOPayeEtape etape = EOPayeEtape.etapeCourante(editingContext,secteur);
			if (etape == null || !etape.estPhaseVerification()) {
				throw new Exception("l'etape en cours n'est pas une etape de vérification");
			}
			EOPayeOper operation = EOPayeOper.rechercherOperationCourante(editingContext,secteur);
			NSArray prepas = EOPayePrepa.chercherPreparationsPourOperationEtSecteur(editingContext,operation,secteur,null);

			NSArray periodes = EOPayePeriode.rechercherPeriodesCourantesPermanentes(editingContext,secteur);
			Enumeration e = periodes.objectEnumerator();

			while (e.hasMoreElements()) {
				EOPayePeriode periode = (EOPayePeriode)e.nextElement();
				Enumeration e1 = prepas.objectEnumerator();
				while (e1.hasMoreElements()) {
					EOPayePrepa prepa = (EOPayePrepa)e1.nextElement();
					if (prepa.contrat() == periode.contrat() && prepa.mois() == operation.mois()) {
						prepa.setTemVerifie(new String(Constantes.VRAI));
						count++;
						break;
					}
				}
			}
			editingContext.saveChanges();  
		} catch (Exception e) { throw e; }
		finally {editingContext.unlock(); }
		return count;
	}


	/** methode de classe imprimant toutes les preparations pour cette
    operation de paye et ce secteur
    @param operation operation de paye concernee
    @param secteur secteur concerne (peut &ecirc;tre null)
	 */
	public static void imprimerPreparations(EOEditingContext editingContext, EOPayeOper operation,EOPayeSecteur secteur) throws Exception {
		NSArray prepas = EOPayePrepa.chercherPreparationsPourOperationEtSecteur(editingContext,
				operation,secteur,
				null);
		imprimer(prepas);
	}
	/** methode de classe imprimant toutes les validations pour cette
    operation de paye et ce secteur
    @param operation operation de paye concernee
    @param secteur secteur concerne (peut &ecirc;tre null)
	 */
	public static void imprimerValidations(EOEditingContext editingContext, EOPayeOper operation,EOPayeSecteur secteur) throws Exception {
		NSArray valids = EOPayeValid.chercherValidationsPourOperationEtSecteur(editingContext,
				operation,secteur,
				null);
		imprimer(valids);
	}

	/** methode de classe imprimant tous les historiques pour cette
    operation de paye et ce secteur
    @param operation operation de paye concernee
    @param secteur secteur concerne (peut &ecirc;tre null)
	 */
	public static void imprimerHistoriques(EOEditingContext editingContext, EOPayeOper operation,EOPayeSecteur secteur) throws Exception {
		NSArray histos = EOPayeHisto.chercherHistoriquesPourOperationEtSecteur(editingContext,
				operation,secteur,
				null);
		imprimer(histos);
	}
	/** methode de classe permettant de supprimer un bulletin de salaire de la paye: repepare tous les autres bulletins de salaire de l'individu
    @param editingContext editingContext dans lequel travailler
    @param contrat contrat pour lequel supprimer le bulletin de salaire
    @param operation operation de paye pour laquelle il faut repreparer un BS
    @return nouvelle preparation
	 */
	public static void rejeterBulletin(EOEditingContext editingContext,EOPayeContrat contrat,EOPayeOper operation) throws Exception {
		regenererBulletinsSalaire(editingContext,contrat,operation,false);
	}

	/** methode de classe permettant de repreparer un bulletin de paye pour un contrat : repepare tous les bulletins de salaire de l'individu
    @param editingContext editingContext dans lequel travailler
    @param contrat contrat a repreparer
    @param operation operation de paye pour laquelle il faut repreparer un BS
    @return nouvelle preparation
	 */
	public static EOPayePrepa repreparer(EOEditingContext editingContext,EOPayeContrat contrat,EOPayeOper operation) throws Exception {
		return regenererBulletinsSalaire(editingContext,contrat,operation,true);

	}

	public static String recalculerCumuls(EOEditingContext editingContext, EOIndividu individu, EOPayeMois mois,boolean modifierSiEerreur) throws Exception {
		String message = "";
		editingContext.lock();
		try {
			EOEditingContext nestedEditingContext = new EOEditingContext(editingContext);
			nestedEditingContext.lock();
			try { 
				EOGlobalID globalID = editingContext.globalIDForObject(individu);
				EOIndividu individuInNested = (EOIndividu)nestedEditingContext.faultForGlobalID(globalID,nestedEditingContext);
				EOPayeMois moisInNested = null;
				if (mois != null) {
					globalID = editingContext.globalIDForObject(mois);
					moisInNested = (EOPayeMois)nestedEditingContext.faultForGlobalID(globalID,nestedEditingContext);
				}
				message = recalculerCumulsPourMois(nestedEditingContext,individuInNested,moisInNested,modifierSiEerreur);
				if (!message.equals("")) {
					message = "Cumuls de rubriques erronnes pour l'individu " + individuInNested.identite() + " :\n" + message;
				}
				nestedEditingContext.saveChanges();
				// tout enregistrer dans l'editing context courant
				editingContext.saveChanges();	
			} catch (Exception e) { throw e; }
			finally { nestedEditingContext.unlock();}
		} catch (Exception e) { throw e; }
		finally {editingContext.unlock(); }
		return message;
	}
	/** Calcule le co&ucirc;t total d'une remuneration sur une peiode
    en prenant en compte le statut
    @param editing context dans lequel effectuer
    @param periode &acute; evaluer */
	public static BigDecimal calculerCoutPourPeriode(EOEditingContext editingContextCourant,EOPayePeriode periodeCourante) throws Exception {
		try {
			BulletinSalaire bulletin = new BulletinSalaire(editingContextCourant,periodeCourante,
					null,false);
			bulletin.preparer();
			BigDecimal resultat =  new BigDecimal(bulletin.preparation().payeCout().doubleValue()).setScale(2,BigDecimal.ROUND_HALF_UP);
			return resultat;
		} catch (Exception e) { throw e; }
	}
	/** Calcule le co&ucirc;t du SFT sur un mois
    @param indice indice pour lequel on calcule le SFT
    @param quotite quotite travaillee
    @param nbEnfants nombre d'enfants
    @param nbJours nombre de jours travailles pendant ce mois 
    @param estTempsPlein cet emploi est-il un temps plein ou partiel */
	public static BigDecimal calculerSFT(Integer indice,Integer quotite,Integer nbEnfants,Integer nbJours,Boolean estTempsPlein) throws Exception {
		return calculerPourSimulation("CalculSFT",new Class[] {Integer.class,Integer.class,Integer.class,Integer.class,Boolean.class},new Object [] {indice,quotite,nbEnfants,nbJours,estTempsPlein});
	}
	/** Calcule le co&ucirc;t de l'indemnite de residence sur un mois en fonction de l'indice pass&acute; en param&egrave;tre
    @param indice pour lequel on calcule l'IR
    @param quotite quotite travaillee
    @param nbJours nombre de jours travailles pendant ce mois 
    @param estTempsPlein cet emploi est-il un temps plein ou partiel 
    @param taux de l'indemnite de residence */
	public static BigDecimal calculerIR(Integer indice,Integer quotite,Integer nbJours,Boolean estTempsPlein,Number taux) throws Exception {
		return calculerPourSimulation("CalculIR",new Class[] {Integer.class,Integer.class,Integer.class,Boolean.class,Number.class},new Object [] {indice,quotite,nbJours,estTempsPlein,taux});
	}
	/** Calcule le co&ucirc;t de l'indemnite de residence sur un mois en fonction du montant brut passe en param&egrave;tre
    @param montant montant pour lequel on calcule l'IR
    @param nbJours nombre de jours travailles pendant ce mois 
    @param taux de l'indemnite de residence */
	public static BigDecimal calculerIR(BigDecimal montant,Integer nbJours,Number taux) throws Exception {
		return calculerPourSimulation("CalculIR",new Class[] {BigDecimal.class,Integer.class,Number.class},new Object [] {montant,nbJours,taux});
	}
	/** Lance le calcul d'une remuneration brute a partir d'un montant net */
	public static BigDecimal calculerBrutPourNet(EOPayeContrat contrat,BigDecimal montant) throws Exception {
		CalculTraitementGlobal calculateur = new CalculTraitementGlobal();
		// préparer le dictionnaire utile aux calculs
		NSMutableDictionary valeurs = new NSMutableDictionary(1);
		// ajouter le contrat
		valeurs.setObjectForKey(contrat,"Contrat");
		// ajouter la période
		//       valeurs.setObjectForKey(periode,"Periode");
		// l'élément de préparation de paye
		//    valeurs.setObjectForKey(prepa,"PayePrepa");
		// ajouter le fait que c'est un calcul direct ou non
		calculateur.calculer(valeurs);
		BigDecimal brut = calculateur.traitementBrutPourNet(montant.doubleValue());
		calculateur = null;
		return brut;
	}


	/** permet de creer toutes les preparations pour un secteur */
	public static int toutPreparerPourSecteur(EOEditingContext editingContext,EOPayeSecteur secteur) throws Exception {
		String messageErreur = "Papaye -";
		int count = 0;
		editingContext.lock();
		try {    
			EOPayeEtape etape = EOPayeEtape.etapeCourante(editingContext,secteur);
			if (etape == null || !etape.estPhaseDemarrage()) {
				throw new Exception("l'etape en cours n'est pas une etape de preparation");
			}
			NSArray periodes = EOPayePeriode.rechercherPeriodesCourantes(editingContext,secteur);

			// trier les périodes en mettant en tête celles qui correspondent à des salaires à retirer
			periodes = EOSortOrdering.sortedArrayUsingKeyOrderArray(periodes,new NSArray(EOSortOrdering.sortOrderingWithKey("temPositive",EOSortOrdering.CompareAscending)));
			for (int i = 0; i < periodes.count(); count++) {
				// pour les rappels
				NSDictionary periodesLiees = periodesLiees(periodes,i);
				EOPayePeriode periodeCourante = (EOPayePeriode)periodesLiees.objectForKey("PeriodeCourante");
				// vérifier si le contrat pour la période courante n'est pas un pré-contrat et est payé localement (normalement dans ces deux cas, les périodes ne devraient pas être générées
				NSArray autresPeriodes = (NSArray)periodesLiees.objectForKey("Autres");
				EOPayeContrat contrat = null;
				EOEditingContext nestedEditingContext = new EOEditingContext(editingContext);  
				nestedEditingContext.lock();
				try {	
					contrat = periodeCourante.contrat();
					if (contrat.temPreContrat().equals(Constantes.FAUX) &&
							contrat.temPayeLocale().equals(Constantes.VRAI)) {
						// mettre les objets dans le nested editing context pour qu'il n'y ait pas
						// de problème à la sauvegarde
						EOGlobalID globalID = editingContext.globalIDForObject(periodeCourante);
						EOPayePeriode periodeInNested  = (EOPayePeriode)nestedEditingContext.faultForGlobalID(globalID,nestedEditingContext);
						BulletinSalaire bulletin = new BulletinSalaire(nestedEditingContext,
								periodeInNested,
								autresPeriodes,true);
						bulletin.preparer();
						NSLog.out.appendln("termine");
						bulletin.enregistrer();
						editingContext.saveChanges();
					} 
				} catch (Exception e) {
					// on renverra une exception globale
					e.printStackTrace();
					String temp;
					if (contrat != null) {
						temp = "erreur pour le contrat de  " +
						contrat.individu().identite() + " message : " + e.getMessage();
					} else {
						temp = "Pas de periode courante pour un contrat\n";
					}
					messageErreur = messageErreur + temp;
				} finally {
					nestedEditingContext.unlock();
					nestedEditingContext = null;
				}
				i += autresPeriodes.count() + 1;
			}
			// on passe en mode préparation quoiqu'il arrive
			EOPayeEtape nouvelleEtape = etape.preparerEtapeSuivante();
			editingContext.insertObject(nouvelleEtape);
			editingContext.saveChanges();

			if (messageErreur.equals("Papaye -") == false) {	// il y a eu une exception
				throw new Exception(messageErreur);
			}
		} catch (Exception e) { throw e;}
		finally { editingContext.unlock(); }
		return count;
	}
	// permet de déterminer toutes les périodes liées à la période de paie courante. Cela peut être le cas lorsqu'il y a des rappels
	private static NSDictionary periodesLiees(NSArray periodes,int numPeriode) {
		EOPayePeriode periodeCourante = (EOPayePeriode)periodes.objectAtIndex(numPeriode);
		NSMutableArray periodesLiees = new NSMutableArray(periodeCourante);
		for (int j = numPeriode + 1; j < periodes.count();j++) {
			EOPayePeriode periode = (EOPayePeriode)periodes.objectAtIndex(j);
			if (periode.contrat() == periodeCourante.contrat()) {
				periodesLiees.addObject(periode);
			} else {
				break;
			}
		}
		// rechercher la période correspondant au mois courant (moisTraitement == mois)
		NSMutableDictionary resultats = new NSMutableDictionary(2);
		NSMutableArray autresPeriodes = new NSMutableArray();
		Enumeration e = periodesLiees.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayePeriode periode = (EOPayePeriode)e.nextElement();
			if (periode.moisTraitement().moisCode().intValue() == periode.mois().moisCode().intValue()) {
				resultats.setObjectForKey(periode,"PeriodeCourante");
			} else {
				autresPeriodes.addObject(periode);
			}
		}
		EOSortOrdering.sortArrayUsingKeyOrderArray(autresPeriodes,new NSArray(EOSortOrdering.sortOrderingWithKey("moisTraitement.moisCode",EOSortOrdering.CompareAscending)));
		resultats.setObjectForKey(autresPeriodes,"Autres");

		return resultats;
	}
	private static void mettreAJourPersonnalisations(EOPayeContrat contrat) {
		// dans les personnalisations exceptionnelles
		Enumeration e = contrat.personnalisations().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayePerso personnalisation = (EOPayePerso)e.nextElement();
			if (personnalisation.temPermanent().equals(Constantes.FAUX) &&
					personnalisation.temValide().equals(Constantes.VRAI)) {
				personnalisation.setTemValide(new String(Constantes.FAUX));
			}
		}
	}

	/*private static void verifierEtapes(NSArray etapes,String typeEtape) throws Exception {
        Enumeration e = etapes.objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeEtape etape = (EOPayeEtape)e.nextElement();
            if (!etape.petpLibelle().equals(typeEtape)) {
                throw new Exception("l'etape en cours n'est pas une " + typeEtape);
            }
        }
    }
    private static void preparerEtapesSuivantes(EOEditingContext editingContext,NSArray etapes) {
        Enumeration e = etapes.objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeEtape etape = (EOPayeEtape)e.nextElement();
            EOPayeEtape nouvelleEtape = etape.preparerEtapeSuivante();
            editingContext.insertObject(nouvelleEtape);
       }
    }*/

	private static void imprimer(NSArray array) {
		try {
			Enumeration e = array.objectEnumerator();
			// vérifier d'abord si il y a des vérifications manuelles à effectuer
			while (e.hasMoreElements()) {
				EOInfoBulletinSalaire info = (EOInfoBulletinSalaire)e.nextElement();
				info.imprimer();
			}
		} catch (Exception e) { /* tableau vide */ }
	}
	// Cette méthode vérifie tous contrats qui génèrent des rappels globaux.
	// Pour toutes les rubriques de rémunération, on vérifie si il y a une rubrique de rappel associée. Elle lance une exception si une rubrique de rappel n'est pas définie
	private static void verifierRubriquesRappels(EOEditingContext editingContext,EOPayeMois mois) throws Exception {
		EOPayeMois.setMoisCourant(mois);
		NSArray periodes = EOPayePeriode.rechercherPeriodesCourantes(editingContext,null);
		// vérifier si il existe des rappels globaux (ie moisTraitement != mois)
		java.util.Enumeration e = periodes.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayePeriode periode = (EOPayePeriode)e.nextElement();
			if (periode.mois() != periode.moisTraitement()) {	// c'est un rappel
				NSDictionary dict = periode.contrat().rubriquesRappelPourMois(mois);
				NSArray rubriques = (NSArray)dict.objectForKey("rubriques");
				java.util.Enumeration e1 = rubriques.objectEnumerator();
				while (e1.hasMoreElements()) {
					EOPayeRubrique rubrique = (EOPayeRubrique)e1.nextElement();
					if (rubrique.rubriqueRappel() == null) {
						throw new Exception ("La rubrique de rappel n'est pas definie pour la rubrique \""+ rubrique.prubLibelle() + "\"");
					}
				}
			}
		}
	}

	// 29/01/2004 - Gestion des cumuls revues
	// remet à jour les cumuls issus de la base de données et supprime la préparation
	// et les éléments
	private static void supprimerPreparation(EOEditingContext editingContext,EOPayePrepa preparationASupprimer) throws Exception {

		if (preparationASupprimer != null) {

			NSArray elements = EOPayeElement.findForPreparation(editingContext, preparationASupprimer);

			for (int i = 0; i < elements.count();i++) {

				EOPayeElement element = (EOPayeElement)elements.objectAtIndex(i);

				// Suppression lignes budgetaires
				NSArray lbuds = EOPayeElementLbud.lbudsForElement(editingContext,element);
				for (int l = 0;l<lbuds.count();l++)
					editingContext.deleteObject((EOPayeElementLbud)lbuds.objectAtIndex(l));

				// Suppression Rappels
				NSArray rappels = EOPayeRappel.rappelsPourElement(editingContext,element);
				for (int r = 0;r<rappels.count();r++)
					editingContext.deleteObject((EOPayeRappel)rappels.objectAtIndex(r));

				editingContext.deleteObject(element);

			}   
			
			editingContext.deleteObject(preparationASupprimer);
		}
	}
	
	private static void supprimerCumuls(EOEditingContext editingContext,EOIndividu individu,EOPayeMois mois) throws Exception {	
		NSArray cumuls = EOPayeCumul.rechercherCumuls(editingContext,mois,individu);
		Enumeration e = cumuls.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeCumul cumul = (EOPayeCumul)e.nextElement();
			editingContext.deleteObject(cumul);
		}
	}
	private static EOPayePrepa regenererBulletinsSalaire(EOEditingContext editingContext,EOPayeContrat contrat,EOPayeOper operation,boolean preparerTousBulletins) throws Exception {
		// le booléen à true signifie qu'on veut afficher l'exception. Essayer après problème de relancer mais en mettant false
		// pour voir ce qui se passe
		//EditingContextLogger logger = new EditingContextLogger(editingContext,"editing context principal",true);
		editingContext.lock();
		EOPayePrepa nouvellePrepa = null;
		try {
			EOEditingContext nestedEditingContext = new EOEditingContext(editingContext);
			//EditingContextLogger loggerNested = new EditingContextLogger(nestedEditingContext,"nested editing context",true);
			nestedEditingContext.lock();
			try {

				System.out.println("Automate.regenererBulletinsSalaire() 1");

				EOGlobalID globalID = editingContext.globalIDForObject(contrat);
				EOPayeContrat contratInNested = (EOPayeContrat)nestedEditingContext.faultForGlobalID(globalID,nestedEditingContext);
				globalID = editingContext.globalIDForObject(operation);
				EOPayeOper operationInNested = (EOPayeOper)nestedEditingContext.faultForGlobalID(globalID,nestedEditingContext);

				EOPayeMois mois = operationInNested.mois();

				EOPayeMois.setMoisCourant(null);

				EOIndividu individu = contratInNested.individu();

				//	NSMutableArray prefetches = new NSMutableArray(EOPayePrepa.ELEMENTS_KEY);
				//				prefetches.addObject(EOPayePrepa.CONTRAT_KEY);
				NSArray preparations = EOPayePrepa.chercherPreparationsPourMoisEtIndividu(nestedEditingContext, mois,individu, null);

				// trier les préparations dans l'ordre des nets inférieurs
				//	EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey("payeNet",EOSortOrdering.CompareAscending);
				// 16/10/2009 - trier les préparations dans l'ordre des nets supérieurs
				// pour le cas où il existe des heures supplémentaires exonérées qu'on calcule en tête le BS comportant
				// les charges principales
				EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey(EOPayePrepa.PAYE_NET_KEY,EOSortOrdering.CompareDescending);
				preparations = EOSortOrdering.sortedArrayUsingKeyOrderArray(preparations,new NSArray(sortOrdering));
				Enumeration e = preparations.objectEnumerator();
				// 16/10/2009	NSMutableArray contrats = new NSMutableArray(contratInNested);
				// 16/10/2009, on laisse le contrat se ranger dans la position où il devrait être
				NSMutableArray contrats = new NSMutableArray();
				while (e.hasMoreElements()) {

					EOPayePrepa preparation = (EOPayePrepa)e.nextElement();

					// garder la référence sur le contrat pour pouvoir recréer le bulletin de salaire
					if (contrats.containsObject(preparation.contrat()) == false)
						contrats.addObject(preparation.contrat());

					supprimerPreparation(nestedEditingContext,preparation);

				}

				// supprimer les cumuls
				supprimerCumuls(nestedEditingContext,individu,mois);

				nestedEditingContext.saveChanges();
				editingContext.saveChanges();

				//	loggerNested.loggerContenu();
				//	logger.loggerContenu();
				/*	NSLog.out.appendln("Affichage des contrats après suppression, nombre de contrats " + contrats.count());
				e = contrats.objectEnumerator();
				while (e.hasMoreElements()) {
					EOPayeContrat contratCourant = (EOPayeContrat)e.nextElement();
					if (contratCourant.preparations().count() > 0) {
						NSLog.out.appendln("Nombre de préparations pour le contrat " + contratCourant.preparations().count());
						EditingContextLogger.LogDetail(contratCourant.editingContext(),contratCourant.preparations());
						java.util.Enumeration e1 = contratCourant.preparations().objectEnumerator();
						while (e1.hasMoreElements()) {
							EOPayePrepa prepa = (EOPayePrepa)e1.nextElement();
							if (prepa.elements().count() > 0) {
								NSLog.out.appendln("Nombre d'éléments pour la préparation " + prepa.elements().count());
								EditingContextLogger.LogDetail(contratCourant.editingContext(),prepa.elements());
							}
						}
					}	
				}
				 */
				// 16/10/09 - au cas où il s'agit de la première préparation du contrat
				if (contrats.containsObject(contratInNested) == false) {
					contrats.addObject(contratInNested);
				}
				contrats = trierContratsSurSalaireARetirer(contrats,nestedEditingContext,mois);
				e = contrats.objectEnumerator();
				while (e.hasMoreElements()) {
					EOPayeContrat contratCourant = (EOPayeContrat)e.nextElement();
					// retrouver les périodes correspondant à ce mois (plusieurs si rappel)
					NSArray periodes = EOPayePeriode.rechercherPeriodesPourContratEtMois(nestedEditingContext, contratCourant,mois);
					NSMutableArray autresPeriodes = new NSMutableArray();
					EOPayePeriode periodeCourante = null;
					Enumeration e1 = periodes.objectEnumerator();
					while (e1.hasMoreElements()) {
						EOPayePeriode periode = (EOPayePeriode)e1.nextElement();
						if (periode.moisTraitement().moisCode().intValue() == periode.mois().moisCode().intValue()) {
							periodeCourante = periode;
						} else {
							autresPeriodes.addObject(periode);
						}
					}
					if (preparerTousBulletins || (contratCourant != contratInNested)) {
						BulletinSalaire bulletin = new BulletinSalaire(nestedEditingContext,periodeCourante,autresPeriodes,true);
						bulletin.preparer();
						bulletin.enregistrer();
						if (contratCourant == contratInNested) {
							nouvellePrepa = bulletin.preparation();
						}
					} else {
						// invalider les périodes
						e1 = periodes.objectEnumerator();
						while (e1.hasMoreElements()) {
							EOPayePeriode periode = (EOPayePeriode)e1.nextElement();
							periode.setTemValide(Constantes.FAUX);
						}
						nestedEditingContext.saveChanges();
					}
					// tout enregistrer dans l'editing context courant
					editingContext.saveChanges();
				}
				if (nouvellePrepa != null) {
					globalID = nestedEditingContext.globalIDForObject(nouvellePrepa);
					nouvellePrepa = (EOPayePrepa)editingContext.faultForGlobalID(globalID,editingContext);
				}	
			} catch (Exception e) { 
				e.printStackTrace();
				/*	loggerNested.loggerContenuDetaille();
				logger.loggerContenuDetaille();	
				if (nouvellePrepa != null) {
					NSLog.out.appendln("Affichage des éléments de la préparation");
					EditingContextLogger.LogDetail(nouvellePrepa.editingContext(),nouvellePrepa.elements());
				}*/
				throw e; 
			}
			finally { nestedEditingContext.unlock(); //loggerNested.terminer();
			}
		} catch (Exception e) { throw e; }
		finally { editingContext.unlock(); //logger.terminer();
		}
		return nouvellePrepa;
	}
	private static NSMutableArray trierContratsSurSalaireARetirer(NSArray contrats,EOEditingContext editingContext,EOPayeMois mois) {
		NSMutableArray contratsOrdonnes = new NSMutableArray();
		Enumeration e = contrats.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeContrat contratCourant = (EOPayeContrat)e.nextElement();
			// retrouver les périodes correspondant à ce mois (plusieurs si rappel)
			NSArray periodes = EOPayePeriode.rechercherPeriodesPourContratEtMois(editingContext,contratCourant,mois);
			java.util.Enumeration e1 = periodes.objectEnumerator();
			boolean estSalaireARetirer = false;
			while (e1.hasMoreElements()) {
				EOPayePeriode periode = (EOPayePeriode)e1.nextElement();
				if (periode.salaireARetirer()) {
					estSalaireARetirer = true;
					break;
				}
			}
			if (estSalaireARetirer) {
				contratsOrdonnes.insertObjectAtIndex(contratCourant,0);
			} else {
				contratsOrdonnes.addObject(contratCourant);
			}
		}
		return contratsOrdonnes;
	}
	// gestion des cumuls
	private static String recalculerCumulsPourMois(EOEditingContext editingContext,EOIndividu individu,EOPayeMois mois,boolean modifierSiEerreur) {
		String message = "";
		if (mois == null) {
			// prendre tous les mois de l'année jusqu'au mois courant, trié par ordre croissant
			NSArray moisAVerifier = rechercherMois(editingContext);
			if (moisAVerifier != null) {
				Enumeration e = moisAVerifier.objectEnumerator();
				while (e.hasMoreElements()) {
					EOPayeMois moisCourant = (EOPayeMois)e.nextElement();
					message = message + recalculerCumulsPourMois(editingContext,individu,moisCourant,modifierSiEerreur);
				}
			}
			return message;
		}
		NSArray cumuls = EOPayeCumul.rechercherCumuls(editingContext,mois,individu);
		if (cumuls.count() == 0) {
			return "";
		}
		// Pour rectifier le problème de la taxe sur salaire, erreur de taux, modifier le pcode de la taxe sur salaire dans les tranches 2 et 3
		if (mois.moisCode().equals("200401")) {
			// mettre à jour la taxe sur salaire
			corrigerTaxeSurSalaire(editingContext,cumuls);
		}
		NSArray elements = EOPayeElement.trouverElements(editingContext,individu,mois);
		NSArray infosBS = EOInfoBulletinSalaire.chercherInfosPourMoisEtIndividu("PayePrepa",editingContext,mois,individu,null);
		if (infosBS.count() == 0) {
			// chercher dans les validations
			infosBS = EOInfoBulletinSalaire.chercherInfosPourMoisEtIndividu("PayeValid",editingContext,mois,individu,null);
			if (infosBS.count() == 0) {
				// chercher dans les historiques
				infosBS = EOInfoBulletinSalaire.chercherInfosPourMoisEtIndividu("PayeHisto",editingContext,mois,individu,null);
			}
		}
		Enumeration e = cumuls.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeCumul cumul = (EOPayeCumul)e.nextElement();
			NSDictionary resultats;
			if (cumul.estGlobal()) {
				resultats = trouverMontantEtAssiette(cumul,infosBS);
			} else {
				resultats = trouverMontantRegulEtAssiette(cumul,elements);
			}
			BigDecimal montant = (BigDecimal)resultats.objectForKey("montant");
			BigDecimal regul = (BigDecimal)resultats.objectForKey("regularisation");
			if (regul == null) {
				regul = new BigDecimal(0);
			}
			BigDecimal assiette = (BigDecimal)resultats.objectForKey("assiette");
			// rechercher le cumul précédent(i.e d'un mois qui précède) pour ajouter montant et régul au montant
			EOPayeCumul cumulPrecedent = cumul.rechercherCumulPrecedent();
			if (cumulPrecedent != null) {
				montant = montant.add(cumulPrecedent.pcumMontant());
				montant = montant.add(cumulPrecedent.pcumRegul());
			}

			// vérifier les montants
			if ((cumul.pcumMontant().doubleValue()) != montant.doubleValue()) {
				// pour les rubriques de rappel mal calculées précédemment (supprimer le cumul si il comporte une rubrique de rappel)
				if (montant.doubleValue() == 0 && cumul.rubrique() != null && cumul.rubrique().estRubriqueRappel()) {
					editingContext.deleteObject(cumul);
				}
				if (cumul.pcumTaux() != null && cumul.pcumTaux().doubleValue() > 0) {
					message = message + "\nmontant faux pour cumul "+ cumul.shortStringRubriqueAvecTaux();
				} else {
					if (cumul.rubrique() != null) {
						message = message + "\nmontant faux pour cumul "+ cumul.shortStringRubriqueSansTaux();
					} else {
						message = message + "\nmontant faux pour cumul "+ cumul.shortStringSansRubrique();
					}
				}
				if (modifierSiEerreur) {
					cumul.setPcumMontant(montant);
				}
			}
			// vérifier les régularisations
			if (regul != null && regul.doubleValue() != 0) {
				if ((cumul.pcumRegul().doubleValue()) != regul.doubleValue()) {
					if (cumul.pcumTaux() != null && cumul.pcumTaux().doubleValue() > 0) {
						message = message + "\nregul fausse pour cumul "+ cumul.shortStringRubriqueAvecTaux();
					} else {
						if (cumul.rubrique() != null) {
							message = message + "\regul fausse pour cumul "+ cumul.shortStringRubriqueSansTaux();
						} else {
							message = message + "\nregul fausse pour cumul "+ cumul.shortStringSansRubrique();
						}
					}
					// changer la valeur dans le cumul
					if (modifierSiEerreur) {
						cumul.setPcumRegul(regul);
					}
				}
			}
			// vérifier les assiettes (sauf dans le cas des cotisations plafonnées et des rémunérations) 
			if (cumul.code().pcodCode().equals("COTVIPLF") == false && cumul.code().pcodCode().equals("COTVIPLS") == false &&  
					cumul.code().pcodCode().equals("COTIFNAL") == false && cumul.code().pcodCode().equals("COTIRTAP") == false && 
					cumul.code().pcodCode().equals("COTIRTAS") == false && cumul.code().pcodCode().equals("COTASCHP") == false  &&
					cumul.code().pcodCode().equals("COTASCHS") == false /*&& cumul.code().pcodCode().startsWith("REMUN") == false*/) {	// modif 30/03/06 pour prendre en compte les bases dans les cumuls de rémunération
				cumulPrecedent = cumul.rechercherCumulAnterieurAvecAssietteMax();
				if (cumulPrecedent != null) {

					// on le trouve : initialiser l'assiette de cumul avec l'ancienne valeur
					assiette = assiette.add(cumulPrecedent.pcumBase());
				}

				if (cumul.pcumBase().doubleValue() != assiette.doubleValue()) {
					if (cumul.pcumTaux() != null && cumul.pcumTaux().doubleValue() > 0) {
						message = message + "\nassiette fausse pour cumul "+ cumul.shortStringRubriqueAvecTaux();
					} else {
						if (cumul.rubrique() != null) {
							message = message + "\nassiette fausse pour cumul "+ cumul.shortStringRubriqueSansTaux();
						} else {
							message = message + "\nassiette fausse pour cumul "+ cumul.shortStringSansRubrique();
						}
					}
					// changer la valeur dans le cumul
					if (modifierSiEerreur) {
						cumul.setPcumBase(assiette);
					}
				}
			}
		}
		return message;
	}
	private static NSArray rechercherMois(EOEditingContext editingContext) {
		GregorianCalendar calendar = new GregorianCalendar();
		// On met la date du mois précédent la modif
		int mois = calendar.get(Calendar.MONTH);
		int moisDebut = (calendar.get(Calendar.YEAR) * 100) + 1;  // Janvier
		int moisFin = (calendar.get(Calendar.YEAR) * 100) + (mois + 1);  // Janvier = 0 en java
		return EOPayeMois.moisComprisEntre(editingContext,new Integer(moisDebut),new Integer(moisFin));
	}
	private static void corrigerTaxeSurSalaire(EOEditingContext editingContext,NSArray cumuls) {
		EOPayeCode codeTranche1 = EOPayeCode.rechercherCode(editingContext,CODE_TRANCHE1);
		EOPayeCode codeTranche2 = EOPayeCode.rechercherCode(editingContext,CODE_TRANCHE2);
		EOPayeCode codeTranche3 = EOPayeCode.rechercherCode(editingContext,CODE_TRANCHE3);
		Number taux2 = EOPayeParam.parametreValide(codeTranche2).pparTaux();
		Number taux3 = EOPayeParam.parametreValide(codeTranche3).pparTaux();

		Enumeration e = cumuls.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeCumul cumul = (EOPayeCumul)e.nextElement();
			if (cumul.code().pcodCode().equals(CODE_TRANCHE1)) {
				if (cumul.pcumTaux().doubleValue() == taux2.doubleValue()) {
					cumul.removeObjectFromBothSidesOfRelationshipWithKey(codeTranche1,"code");
					cumul.addObjectToBothSidesOfRelationshipWithKey(codeTranche2,"code");
				} else if (cumul.pcumTaux().doubleValue() == taux3.doubleValue()) {
					cumul.removeObjectFromBothSidesOfRelationshipWithKey(codeTranche1,"code");
					cumul.addObjectToBothSidesOfRelationshipWithKey(codeTranche3,"code");
				}
			}
		}
	}
	private static NSDictionary trouverMontantRegulEtAssiette(EOPayeCumul cumul,NSArray elements) {
		BigDecimal montant = new BigDecimal(0);
		BigDecimal regul = new BigDecimal(0);
		BigDecimal assiette = new BigDecimal(0);
		Enumeration e = elements.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeElement element = (EOPayeElement)e.nextElement();
			double taux = 0;
			double tauxCumul = 0;
			if (element.pelmTaux() != null && !element.rubrique().estAPayer()) {
				taux = element.pelmTaux().doubleValue();
			}
			if (cumul.pcumTaux() != null) {
				tauxCumul = cumul.pcumTaux().doubleValue();
			}
			// on teste sur les taux à cause de la taxe sur salaire ou les rubriques sont identiques pour les cumuls mais où il y a un cumul par taux
			if ((cumul.rubrique() == element.rubrique() && (taux == tauxCumul)) ||
					(cumul.rubrique().rubriqueRappel() == element.rubrique() && (taux == tauxCumul)) ||
					(cumul.code().pcodCode().startsWith(CODE_TAXE_SUR_SALAIRE) == false && element.rubrique().codes().containsObject(cumul.code()))) {		
				// chercher assiette
				if (element.estUneRemuneration() && (element.pelmTaux() == null || element.pelmTaux().doubleValue() == 0)) { // rémunération horaire : on utilise assiette et taux pour d'autres infos
					assiette = assiette.add(element.pelmApayer());
				} else {
					assiette = assiette.add(element.pelmAssiette());
				}
			}
			EOStructure structure = null;
			if (element.preparation() != null && element.preparation().payeNet() != null) {
				structure = element.preparation().structure();
			} else if (element.validation() != null && element.validation().payeNet() != null) {
				structure = element.validation().structure();
			} else if (element.historique() != null && element.historique().payeNet() != null) {
				structure = element.historique().structure();
			}
			if (cumul.structure() == structure && cumul.rubrique() == element.rubrique() && taux == tauxCumul) {
				if (element.estUneRemuneration()) {
					montant = montant.add(element.pelmApayer());
				} else if (element.estUneDeduction()) {
					montant = montant.add(element.pelmAdeduire());
				} else {
					montant = montant.add(element.pelmPatron());
				}
			}
			if (cumul.structure() == structure && cumul.rubrique().rubriqueRappel() == element.rubrique() && taux == tauxCumul) {
				if (element.estUneRemuneration()) {
					regul = regul.add(element.pelmApayer());
				} else if (element.estUneDeduction()) {
					regul = regul.add(element.pelmAdeduire());
				} else {
					regul = regul.add(element.pelmPatron());
				}
			}
		}
		NSMutableDictionary dict =  new NSMutableDictionary(3);
		dict.setObjectForKey(assiette,"assiette");
		dict.setObjectForKey(montant,"montant");
		dict.setObjectForKey(montant,"regul");
		return dict;
	}
	private static NSDictionary trouverMontantEtAssiette(EOPayeCumul cumul,NSArray items) {
		BigDecimal montant = new BigDecimal(0);
		BigDecimal assiette = new BigDecimal(0);
		Enumeration e = items.objectEnumerator();
		while (e.hasMoreElements()) {
			EOInfoBulletinSalaire info = (EOInfoBulletinSalaire)e.nextElement();
			if (cumul.code().pcodCode().equals(EOPayeCode.REMUN_TOT)) {
				assiette = assiette.add(info.payeBrut());
				if (cumul.structure() == info.structure()) {
					montant = montant.add(info.payeBrut());
				}
			} else if (cumul.code().pcodCode().equals(EOPayeCode.CODEBASESS)) {
				assiette = assiette.add(info.payeBssmois());
				if (cumul.structure() == info.structure()) {
					montant = montant.add(info.payeBssmois());
				}
			} else if (cumul.code().pcodCode().equals(EOPayeCode.NET)) {
				assiette = assiette.add(info.payeNet());
				if (cumul.structure() == info.structure()) {
					montant = montant.add(info.payeNet());
				}
			} else if (cumul.code().pcodCode().equals(EOPayeCode.NETIMPOS)) {
				assiette = assiette.add(info.payeBimpmois());
				if (cumul.structure() == info.structure()) {
					montant = montant.add(info.payeBimpmois());
				}
			} else if (cumul.code().pcodCode().equals(EOPayeCode.COUTPATRONAL)) {
				assiette = assiette.add(info.payeCout());
				if (cumul.structure() == info.structure()) {
					montant = montant.add(info.payeCout());
				}
			} 
		}
		NSMutableDictionary dict =  new NSMutableDictionary(2);
		dict.setObjectForKey(assiette,"assiette");
		dict.setObjectForKey(montant,"montant");
		return dict;
	}

	// méthodes pour la simulation
	private static BigDecimal calculerPourSimulation(String nomClasse,Class[] classes,Object[] objets) throws Exception {
		try {
			// instancier l'objet de calcul en utilisant le lanceur
			if (lanceurCalcul == null) {
				lanceurCalcul = new LanceurCalcul();
			}
			Object moteurCalcul = lanceurCalcul.instancierObjet(nomClasse);
			java.lang.reflect.Method method = moteurCalcul.getClass().getMethod ("calculerPourSimulation",classes);
			try {
				Object resultat = method.invoke(moteurCalcul,objets);
				// vérifier si la réponse retournée est bien un BigDecimal
				// sinon retourner null
				if (resultat instanceof BigDecimal) {
					return (BigDecimal)resultat;
				} else {
					return null;
				}
			} catch (Exception exception) {
				/* if (exception.getClass() ==
	                java.lang.reflect.InvocationTargetException.class) {
	                String message = ((java.lang.reflect.InvocationTargetException)exception).getTargetException().getMessage();
	                if (message != null) {
	                    throw new Exception(message);
	                } else {
	                    throw exception;
	                }
	            } else {*/
				throw exception;
				//}
			}
		} catch (Exception e) { throw e; }
	}
}
