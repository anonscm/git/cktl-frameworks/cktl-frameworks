/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.common;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.metier.grhum.EOIndividu;
import org.cocktail.papaye.server.metier.grhum.EOStructure;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCumul;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class UtilitairesCumul {
	// cette classe comporte tous les utilitaires de vérification des cumuls
	/** compare les cumuls de tous les agents payes pendant une annee aux bulletins de salaires de ceux-ci.
	 Dans la comparaison, on ne prend en compte que les montants payes
	@param editing context
	@param individu
	@param structure
	@param annee
	@return dictionnaire des erreurs (voir ci-dessous), null si pas d'erreur
	Retourne un dictionnaire organise par structure puis individu
	organisation du dictionnaire :
	<UL>cle : structure (EOStructure) </UL>
	<UL>dictionnaire des erreurs pour cette structure</UL>
	organisation du dictionnaire des erreurs de structure
	<UL>cle : individu (EOIndividu) </UL>
	<UL>dictionnaire des erreurs pour cet individu</UL>
	organisation du dictionnaire des erreurs pour un individu :
	<UL>cle : code cumul ou rubrique</UL>
	<UL>objet : dictionnaire<BR>
	<UL>cle : cumul (String), valeur : cumul (EOPayeCumul). Peut &ecirc;tre nul si ce cumul n'existe pas</UL>
	<UL>cle : rubrique (String), valeur : rubrique (EOPayeRubrique)Peut etre nul si le cumul calcule n'existe pas ou si cumul global</UL>
	<UL>cle : montant (String), valeur : montant (BigDecimal) si le cumul calcule n'existe pas</UL>
	<UL>cle : taux (String), valeur : taux (Number). Peut etre nul</UL>
	</UL>
	*/
	public static NSDictionary comparerCumuls(EOEditingContext editingContext,Number annee) {
		// rechercher les structures valides
		NSArray structures = EOStructure.rechercherStructures(editingContext);
		NSMutableDictionary resultatComparaison = new NSMutableDictionary();
		java.util.Enumeration e = structures.objectEnumerator();
		while (e.hasMoreElements()) {
			EOStructure structure = (EOStructure)e.nextElement();
			NSDictionary resultatStructure = comparerCumuls(editingContext,structure,annee);
			if (resultatStructure != null) {
				resultatComparaison.setObjectForKey(resultatStructure,structure);
			}
		}
		if (resultatComparaison.allKeys().count() > 0) {
			return resultatComparaison;
		} else {
			return null;
		}
	}
	
	/** compare les cumuls d'une structure pour une anne 
	@param editing context
	@param individu
	@param structure
	@param annee
	@return dictionnaire des erreurs (voir ci-dessous), null si pas d'erreur
	organisation du dictionnaire :
	<UL>cle : individu (EOIndividu) </UL>
	<UL>dictionnaire des erreurs pour cet individu</UL>
	organisation du dictionnaire des erreurs pour un individu :
	<UL>cle : code cumul ou rubrique</UL>
	<UL>objet : dictionnaire<BR>
	<UL>cle : cumul (String), valeur : cumul (EOPayeCumul). Peut &ecirc;tre nul si ce cumul n'existe pas</UL>
	<UL>cle : rubrique (String), valeur : rubrique (EOPayeRubrique)Peut &ecirc;tre nul si le cumul calcule n'existe pas ou si cumul global</UL>
	<UL>cle : montant (String), valeur : montant (BigDecimal) si le cumul calcule n'existe pas</UL>
	<UL>cle : taux (String), valeur : taux (Number). Peut &ecirc;tre nul</UL>
	</UL>
	*/	
	public static NSDictionary comparerCumuls(EOEditingContext editingContext,EOStructure structure,Number annee) {
		// rechercher les individus par le biais des périodes
		NSArray individus = EOIndividu.chercherIndividusPourAnneeEtStructure(editingContext,structure,annee);
		NSMutableDictionary resultatComparaison = new NSMutableDictionary();
		java.util.Enumeration e = individus.objectEnumerator();
		while (e.hasMoreElements()) {
			EOIndividu individu = (EOIndividu)e.nextElement();
			NSDictionary resultatIndividu = comparerCumuls(editingContext,individu,structure,annee);
			if (resultatIndividu != null) {
				resultatComparaison.setObjectForKey(resultatIndividu,individu);
			}
		}
		if (resultatComparaison.allKeys().count() > 0) {
			return resultatComparaison;
		} else {
			return null;
		}
	}
	
	/** compare les cumuls d'un individu pour une structure avec le cumul des elements et EOPayeHisto,…
		@param editing context
		@param individu
		@param structure
		@param annee
		@return dictionnaire des erreurs (voir ci-dessous), null si pas d'erreur
		organisation du dictionnaire :
		<UL>cle : code cumul ou rubrique</UL>
		<UL>dictionnaire pour ce code ou tableau de dictionnaires pour cette rubrique<BR>
		<UL>cle : cumul (String), valeur : cumul (EOPayeCumul). Peut &ecirc;tre nul si ce cumul n'existe pas</UL>
		<UL>cle : montant (String), valeur : montant (BigDecimal) si le cumul calcule n'existe pas</UL>
		</UL>
	*/
	public static NSDictionary comparerCumuls(EOEditingContext editingContext,EOIndividu individu,EOStructure structure,Number annee) {
		NSMutableDictionary resultatComparaison = new NSMutableDictionary();
		NSDictionary cumuls = derniersCumulsPourIndividuEtStructure(editingContext,individu,structure,annee);
		NSDictionary cumulsCalcules = cumulerElementsPourIndividuEtStructure(editingContext,individu,structure,annee);
		
		if (cumuls == null) {
			if (cumulsCalcules != null) {
				System.out.println("Pas de cumul pour " + individu.identite() + " et cumulsCalcules\n" + imprimerCumulsCalcules(cumulsCalcules));
				return cumulsCalcules;
			} else {
				return null;
			}
		}
		// comparer les cumuls globaux
		NSDictionary cumulsGlobaux = ((NSDictionary)cumuls.objectForKey("global"));
		java.util.Enumeration e = cumulsGlobaux.keyEnumerator();
		while (e.hasMoreElements()) {
			String code = (String)e.nextElement();
			EOPayeCumul cumul = (EOPayeCumul)cumulsGlobaux.objectForKey(code);
			NSDictionary cumulCalcule = (NSDictionary)cumulsCalcules.objectForKey(code);
			NSMutableDictionary differences = null;
			if (cumulCalcule == null) {
				differences = new NSMutableDictionary();
				differences.setObjectForKey(cumul,"cumul");
			} else {
				differences = comparerCumulGlobal(cumul,cumulCalcule);
			}
			if (differences != null) {
				resultatComparaison.setObjectForKey(differences,code);
			}
		}
		// comparer les cumuls de rubrique
		NSDictionary cumulsRubriques = ((NSDictionary)cumuls.objectForKey("rubrique"));
		e = cumulsRubriques.keyEnumerator();
		
		while (e.hasMoreElements()) {
			EOPayeRubrique rubrique = (EOPayeRubrique)e.nextElement();
			NSArray cumulsPourRubrique = (NSArray)cumulsRubriques.objectForKey(rubrique);
			NSArray cumulsCalculesPourRubrique = (NSArray)cumulsCalcules.objectForKey(rubrique);
			NSArray cumulsCalculesPourRappel = null;
			NSMutableArray differences = null;
			if (!rubrique.estRubriqueRappel() && rubrique.rubriqueRappel() != null) {
				cumulsCalculesPourRappel = (NSArray)cumulsCalcules.objectForKey(rubrique.rubriqueRappel());
			}
			if (cumulsCalculesPourRubrique == null && cumulsCalculesPourRappel == null) {
				differences = new NSMutableArray();
				java.util.Enumeration e1 = cumulsPourRubrique.objectEnumerator();
				while (e1.hasMoreElements()) {
					EOPayeCumul cumul = (EOPayeCumul)e1.nextElement();
					differences.addObject(new NSMutableDictionary(cumul,"cumul"));
				}	
			} else {
				differences = comparerCumulsRubrique(cumulsPourRubrique,cumulsCalculesPourRubrique,cumulsCalculesPourRappel);
			}
			if (differences != null) {
				resultatComparaison.setObjectForKey(differences,rubrique);
			}
		}

	
		if (resultatComparaison.allKeys().count() == 0) {
			return null;
		} else {
			System.out.println("c\nComparaison cumul de " + individu.identite());
			System.out.println(imprimerCumuls(cumuls));
			System.out.println(imprimerCumulsCalcules(cumulsCalcules));
			return resultatComparaison;
		}
	}
	
	/** retourne un dictionnaire comportant tous les derniers cumuls d'un individu pour une structure donnee. Ce ductionnaire comporte deux
	entr&ecaute;es : une pour les cumuls globaux, une pour les cumuls de rubrique. Chaque entree est un dictionnaire
	@param editing context
	@param individu
	@param structure
	@param annee
	<BR>organisation du dictionnaire :
		<UL>cle : "global","rubrique"</UL>
		<UL>dictionnaire des derniers cumuls globaux pour cet individu
		<UL>cle : code</UL>
		<UL>cumul pour ce code</UL>
		</UL>
		<UL>dictionnaire des dernires cumuls de rubrique pour cet individu
		<UL>cle : rubrique</UL>
		<UL>tableau de cumuls pour cette rubrique (pour les cumuls associes a une m&ecirc;me rubrique et ayant des codes differents)</UL>
		</UL></UL>
	*/
	public static NSDictionary derniersCumulsPourIndividuEtStructure(EOEditingContext editingContext,EOIndividu individu,EOStructure structure,Number annee) {
		// on veut que les cumuls soient triés sur le code cumul et sur l'ordre décroissant de mois afin d'être sûr que le premier cumul
		// trouvé pour un code et une rubrique (optionnelle) soit le dernier cumul
		NSMutableArray sorts = new NSMutableArray(2);
		sorts.addObject(new EOSortOrdering("code.pcodCode",EOSortOrdering.CompareAscending));
		sorts.addObject(new EOSortOrdering("mois.moisCode",EOSortOrdering.CompareDescending));
		NSArray cumuls = EOPayeCumul.rechercherCumulsPourAnneeIndividuStructure(editingContext,annee,individu,structure,sorts);
		if (cumuls.count() == 0) {
			return null;
		}
		// créer un dictionnaire dont les clés sont "global" et "rubrique"
		NSMutableDictionary cumulsGlobaux = new NSMutableDictionary();
		NSMutableDictionary cumulsRubriques = new NSMutableDictionary();
		
		java.util.Enumeration e = cumuls.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeCumul cumulCourant = (EOPayeCumul)e.nextElement();
			if (cumulCourant.estGlobal()) {
				// cumul global, vérifier si il est déjà dans le dictionnaire des cumuls globaux
				String codeCourant = cumulCourant.code().pcodCode();
				if (cumulsGlobaux.objectForKey(codeCourant) == null) {
					// ajouter au dictionnaires des cumuls globaux
					cumulsGlobaux.setObjectForKey(cumulCourant,codeCourant);
				}		
			} else {
				// cumul de rubrique, vérifier si le tableau contient déjà ce cumul. On stocke dans un tableau pour les cumuls associés
				// à différents codes cumuls et à une même rubrique (ex. taxe sur salaire)
				NSMutableArray cumulsRubrique = (NSMutableArray)cumulsRubriques.objectForKey(cumulCourant.rubrique());
				if (cumulsRubrique == null) {
					cumulsRubrique = new NSMutableArray(cumulCourant);
					cumulsRubriques.setObjectForKey(cumulsRubrique,cumulCourant.rubrique());
				} else {
					// vérifier si ce code cumul a déjà été ajouté
					java.util.Enumeration e1 = cumulsRubrique.objectEnumerator();
					boolean found = false;
					while (e1.hasMoreElements()) {
						EOPayeCumul cumul = (EOPayeCumul)e1.nextElement();
						if (cumul.code().pcodCode().equals(cumulCourant.code().pcodCode())) {
							found = true;
							break;
						}
					}
					if (!found) {
						cumulsRubrique.addObject(cumulCourant);
					}
				}
			}
		}
		NSMutableDictionary dictionnaireCumuls = new NSMutableDictionary(2);
		dictionnaireCumuls.setObjectForKey(cumulsGlobaux,"global");
		dictionnaireCumuls.setObjectForKey(cumulsRubriques,"rubrique");
		return dictionnaireCumuls;
	}
	
	/** retourne un dictionnaire comportant le cumul des elements des BS d'un individu pour une structure donnee. Les rappels sont
	 cumules dans les cumuls des rubriques qui leur sont associees<BR>
	organisation du dictionnaire :
	<UL>cle : code cumul global ou rubrique</UL>
		<UL>tableau de dictionnaire des cumuls de Bulletins de salaire (pour les rubriques associees a plusieurs codes (ex. Taxe sur salaire))
		<UL>code element
		<UL>cle : montant (String), valeur : montant (BigDecimal)</UL>
	</UL>
	Les rubriques et les rubriques de rappel sont stockées comme des entrées différentes du tableau de valeurs
	@param editing context
	@param individu
	@param structure
	@param annee
	*/
	public static NSDictionary cumulerElementsPourIndividuEtStructure(EOEditingContext editingContext,EOIndividu individu,EOStructure structure,Number annee) {
		// commencer par rechercher les préparations/validations/historiques
		// pour chaque code, on stockera dans un tableau toutes les rubriques différentes trouvées : une rubrique et une rubrique de rappel
		// sont considérées à ce niveau-là comme différentes
		NSArray infosBS = EOInfoBulletinSalaire.chercherInfosPourAnneeIndividuStructure(editingContext,individu,structure,annee);
		if (infosBS == null || infosBS.count() == 0) {
			return null;
		}
		NSMutableDictionary dictionnaireCumuls = new NSMutableDictionary();
		java.util.Enumeration e = infosBS.objectEnumerator();
		while (e.hasMoreElements()) {
			EOInfoBulletinSalaire bs = (EOInfoBulletinSalaire)e.nextElement();

			// traitement des rubriques
			NSArray elements = new NSArray();
			
			EOPayeElement.findForPreparation(bs.editingContext(), (EOPayePrepa)bs);
			java.util.Enumeration e1 = elements.objectEnumerator();//bs.elements().objectEnumerator();
			while (e1.hasMoreElements()) {
				EOPayeElement element = (EOPayeElement)e1.nextElement();
				NSMutableArray cumulsPourElement = (NSMutableArray)dictionnaireCumuls.objectForKey(element.rubrique());
				if (cumulsPourElement == null) {
					cumulsPourElement = new NSMutableArray();
				}
				java.util.Enumeration e2 = cumulsPourElement.objectEnumerator();
				NSMutableDictionary cumulElement = null;
				while (e2.hasMoreElements()) {
					NSMutableDictionary dict = (NSMutableDictionary)e2.nextElement();
					if (dict.objectForKey("code").equals(element.code().pcodCode())) {
						cumulElement = dict;
						break;
					}
				}
				if (cumulElement == null) {
					cumulElement = initCumul(element);
					cumulsPourElement.addObject(cumulElement);
					dictionnaireCumuls.setObjectForKey(cumulsPourElement,element.rubrique());
				} 
				calculerCumulElement(cumulElement,element);
				
			}
			// updater les cumuls globaux en fonction des données du bs
			calculerCumulsGlobaux(dictionnaireCumuls,bs);
		}
		return dictionnaireCumuls;
	}
	
	/** retourne une string qui est la liste des cumuls avec pour chaque cumul :le code cumul, le mois de cumul, la rubrique, le montant d'un cumul 
	@param cumuls tableau des cumuls & agrave; lister */
	public static String imprimerCumuls(NSDictionary dictionnaireCumuls) {
		String texte = "Valeur des cumuls\nCode\tRubrique\tMontant\tRegul\n";
		texte = texte + "Cumuls globaux\n";
		// lister les cumuls globaux
		NSDictionary dictionnaire = (NSDictionary)dictionnaireCumuls.objectForKey("global");
		java.util.Enumeration e = dictionnaire.keyEnumerator();
		while (e.hasMoreElements()) {
			String code = (String)e.nextElement();
			EOPayeCumul cumul = (EOPayeCumul)dictionnaire.objectForKey(code);
			texte = texte + code + "\t\t" + cumul.pcumMontant() + "\t" + cumul.pcumRegul() + "\n";
		}
	
		// lister les cumuls de rubrique
		texte = texte + "Cumuls Rubriques\n";
		dictionnaire = (NSDictionary)dictionnaireCumuls.objectForKey("rubrique");
		e = dictionnaire.keyEnumerator();
		while (e.hasMoreElements()) {
			EOPayeRubrique rubrique = (EOPayeRubrique)e.nextElement();
			NSArray cumulsRubrique = (NSArray)dictionnaire.objectForKey(rubrique);
			java.util.Enumeration e1 = cumulsRubrique.objectEnumerator();
			while (e1.hasMoreElements()) {
				EOPayeCumul cumul = (EOPayeCumul)e1.nextElement();
				texte = texte + cumul.code().pcodCode() + "\t" + cumul.rubrique().prubLibelle() + "\t" + cumul.pcumMontant() + "\t" + cumul.pcumRegul() + "\n";
			}
			
		}
		return texte;
	}
	/** retourne une string qui est la liste des comparaisons de cumuls et de cumuls d'éléments
	 *  avec pour chaque cumul :le code cumul, le montant d'un cumul 
	@param cumuls tableau des cumuls lister */
	public static String imprimerComparaisons(NSDictionary comparaisonCumul) {
		String texte = "Differences Trouvees\nCode Cumul\trubrique\tMontant\tRegul\tMontant Cumule\n";
		java.util.Enumeration e = comparaisonCumul.allKeys().objectEnumerator();
		NSArray differences = null;
		while (e.hasMoreElements()) {
			Object cle = e.nextElement();
			if (cle.getClass().getName().equals("java.lang.String")) {
				// cumul global
				NSDictionary comparaison = (NSDictionary)comparaisonCumul.objectForKey(cle);
				differences = new NSArray(comparaison);
			} else {
				// cumul de rubrique
				differences = (NSArray)comparaisonCumul.objectForKey(cle);
			}
			java.util.Enumeration e1 = differences.objectEnumerator();
			while (e1.hasMoreElements()) {
				NSDictionary comparaison = (NSDictionary)e1.nextElement();
				EOPayeCumul cumul = (EOPayeCumul)comparaison.objectForKey("cumul");
				if (cumul != null) {
					texte = texte + cumul.code().pcodCode() + "\t" ;
					if (cumul.rubrique() != null) {
						texte = texte + cumul.rubrique().prubLibelle() + "\t";
					}
					else {
						texte = texte + "\t";
					}
					texte = texte + cumul.pcumMontant() + "\t" + cumul.pcumRegul();
				} else {
					if (cle.getClass().getName().equals("org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique")) {
						texte = texte + "\t" + ((EOPayeRubrique)cle).prubLibelle() + "\t" + "\t\t";
					}
				}
				if (comparaison.objectForKey("montant calcule") != null) {
					texte = texte + "\t" + comparaison.objectForKey("montant calcule");
				}
				texte = texte + "\n";
			}
		}
		return texte;
	}
	
	// méthodes privées
	private static NSMutableDictionary comparerCumulGlobal(EOPayeCumul cumul,NSDictionary cumulCalcule) {
		NSMutableDictionary resultatComparaison = null;
		BigDecimal montant = (BigDecimal)cumulCalcule.objectForKey("montant");
		
		// comparer les montants
		if ((cumul.pcumMontant().add(cumul.pcumRegul()).compareTo(montant) != 0) ) {
				// montants différents, ajouter le cumul au dictionnaire cumulCalcule et le mettre dans les différences
				resultatComparaison = new NSMutableDictionary(2);
				resultatComparaison.setObjectForKey(cumulCalcule.objectForKey("montant"),"montant calcule");
				resultatComparaison.setObjectForKey(cumul,"cumul");
		}
		return resultatComparaison;
	}
	
	private static NSMutableArray comparerCumulsRubrique(NSArray cumuls,NSArray cumulsCalcules,NSArray cumulsRappel) {
		NSMutableArray resultatComparaison = null;
		
		java.util.Enumeration e = cumuls.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeCumul cumul = (EOPayeCumul)e.nextElement();
			NSMutableDictionary cumulCalcule = trouverCorrespondance(cumulsCalcules,cumul);
			NSMutableDictionary cumulPourRappel= null;
			if (cumulsRappel != null) {
				cumulPourRappel = trouverCorrespondance(cumulsRappel,cumul);
			}
			BigDecimal montant = new BigDecimal(0);
			// un même rappel peut être associé à plusieurs rubriques, ne l'ajouter que si les codes des éléments correspondent
			if (cumulPourRappel != null) {
				System.out.println(cumul.rubrique().prubLibelle());
				System.out.println("code rubrique " + cumulCalcule.objectForKey("code"));
				System.out.println("code rappel " + cumulCalcule.objectForKey("code"));
			}
			if (cumulPourRappel != null && cumulCalcule.objectForKey("code").equals(cumulPourRappel.objectForKey("code"))) {
				montant = montant.add((BigDecimal)cumulPourRappel.objectForKey("montant"));
			}
			montant = montant.add((BigDecimal)cumulCalcule.objectForKey("montant"));
			// comparer les montants
			if ((cumul.pcumMontant().add(cumul.pcumRegul()).compareTo(montant) != 0)) {
				// montants différents, ajouter le cumul au dictionnaire cumulCalcule et le mettre dans les différences
				NSMutableDictionary resultat = new NSMutableDictionary(3);
				resultat.setObjectForKey(montant,"montant calcule");
				resultat.setObjectForKey(cumul,"cumul");
				if (resultatComparaison == null) {
					resultatComparaison = new NSMutableArray();
				}
				resultatComparaison.addObject(resultat);
			}	
		}
		return resultatComparaison;
	}
	
	private static NSMutableDictionary trouverCorrespondance(NSArray cumulsCalcules,EOPayeCumul cumul) {
		if (cumulsCalcules.count() == 1) {
			return (NSMutableDictionary)cumulsCalcules.objectAtIndex(0);
		} else {
			// taxe sur salaire
			String code = cumul.code().pcodCode();
			String codeRubrique = null;
			// taxe sur salaire
			if (code.equals("COTTXSAL")) {
				codeRubrique = "TAXSSAL1";
			} else if (code.equals("COTTXSA2")) {
				codeRubrique = "TAXSSAL2";
			} else if (code.equals("COTTXSA3")) {
				codeRubrique = "TAXSSAL3";
			}
			if (codeRubrique == null) {
				return null;
			} else {
				Enumeration e = cumulsCalcules.objectEnumerator();
				while (e.hasMoreElements()) {
					NSMutableDictionary cumulCalcule = (NSMutableDictionary)e.nextElement();
					if (cumulCalcule.objectForKey("code").equals(codeRubrique)) {
						return cumulCalcule;
					}
				}
				return null;
			}
		}
	}
	
	private static NSMutableDictionary initCumul(EOPayeElement element) {
		NSMutableDictionary cumul = new NSMutableDictionary(3);
		if (element != null) {
			cumul.setObjectForKey(element.code().pcodCode(),"code");
		}
		cumul.setObjectForKey(new BigDecimal(0),"montant");
		return cumul;
	}
	
	private static void calculerCumulElement(NSMutableDictionary cumul,EOPayeElement element) {
		BigDecimal montant = (BigDecimal)cumul.objectForKey("montant");
		if (element.estUneRemuneration()) {
			cumul.setObjectForKey(montant.add(element.pelmApayer()),"montant");
		} else if (element.estUneDeduction()) {
			cumul.setObjectForKey(montant.add(element.pelmAdeduire()),"montant");
		} else {
			cumul.setObjectForKey(montant.add(element.pelmPatron()),"montant");
		}
	}
	
	private static void calculerCumulsGlobaux(NSMutableDictionary dictionnaireCumuls, EOInfoBulletinSalaire bs) {
		calculerCumulGlobal(dictionnaireCumuls,EOPayeCode.REMUN_TOT,bs.payeBrut());
		calculerCumulGlobal(dictionnaireCumuls,EOPayeCode.CODEBASESS,bs.payeBssmois());
		calculerCumulGlobal(dictionnaireCumuls,EOPayeCode.NET,bs.payeNet());
		calculerCumulGlobal(dictionnaireCumuls,EOPayeCode.NETIMPOS,bs.payeBimpmois());
		calculerCumulGlobal(dictionnaireCumuls,EOPayeCode.COUTPATRONAL,bs.payeCout());
	}
	
	private static void calculerCumulGlobal(NSMutableDictionary dictionnaireCumuls,String code,BigDecimal valeur) {
		NSMutableDictionary cumul = (NSMutableDictionary)dictionnaireCumuls.objectForKey(code);
			
		if (cumul == null) {
			// créer le cumul global
			cumul = initCumul(null);
			dictionnaireCumuls.setObjectForKey(cumul,code);
		}
		BigDecimal montant = (BigDecimal)cumul.objectForKey("montant");
		cumul.setObjectForKey(montant.add(valeur),"montant");
	}
	
	// Recherche les éléments dont les codes ne correspondent pas aux codes cumuls pour certains éléments
/*	private static NSArray rechercherCumulsCalculesPourCode(NSDictionary cumulsCalcules,String code) {
		// taxe sur salaire
		if (code.equals("COTTXSAL")) {
			return (NSArray)cumulsCalcules.objectForKey("TAXSSAL1");
		} else if (code.equals("COTTXSA2")) {
			return (NSArray)cumulsCalcules.objectForKey("TAXSSAL2");
		} else if (code.equals("COTTXSA3")) {
			return (NSArray)cumulsCalcules.objectForKey("TAXSSAL3");
		} else if (code.equals("COTCOSOL")) {
			// contribution solidarité
			return (NSArray)cumulsCalcules.objectForKey("TXCGCSOL");
		} else if (code.equals("COTICRDS")) {
			// ajouter tous les cumuls correspondant à la CRDS
			NSMutableArray nouveauxCumuls = new NSMutableArray();
			Enumeration e = cumulsCalcules.keyEnumerator();
			while (e.hasMoreElements()) {
				String codeRecherche = (String)e.nextElement();
				if (codeRecherche.endsWith("CRDS")) {
					nouveauxCumuls.addObjectsFromArray((NSArray)cumulsCalcules.objectForKey(codeRecherche));
				}
			}
			return nouveauxCumuls;
		} else if (code.equals("COTCSGDE")) {
			// ajouter tous les cumuls correspondant à la CSG Déductible
			NSMutableArray nouveauxCumuls = new NSMutableArray();
			Enumeration e = cumulsCalcules.keyEnumerator();
			while (e.hasMoreElements()) {
				String codeRecherche = (String)e.nextElement();
				if (codeRecherche.endsWith("DCSG") || codeRecherche.equals("TXDCGCSG")) {
					nouveauxCumuls.addObjectsFromArray((NSArray)cumulsCalcules.objectForKey(codeRecherche));
				}
			}
			return nouveauxCumuls;
		} else if (code.equals("COTCSGND")) {
			// ajouter tous les cumuls correspondant à la CSG Non Déductible
			NSMutableArray nouveauxCumuls = new NSMutableArray();
			Enumeration e = cumulsCalcules.keyEnumerator();
			while (e.hasMoreElements()) {
				String codeRecherche = (String)e.nextElement();
				if (codeRecherche.endsWith("NCSG") || codeRecherche.equals("TXAUTCSG") || codeRecherche.equals("TXNCHCSG") || codeRecherche.equals("TXNCGCSG")) {
					nouveauxCumuls.addObjectsFromArray((NSArray)cumulsCalcules.objectForKey(codeRecherche));
				}
			}
			return nouveauxCumuls;
		} 
		return null;
	}*/
	
	// méthodes privées

	private static String imprimerCumulsCalcules(NSDictionary cumulsCalcules) {
		String texte = "\nCumuls calcules\nCode\tRubrique\tMontant\n";
		// imprimer d'abord les cumuls globaux
		texte = texte + "Cumuls globaux\n";
		java.util.Enumeration e = cumulsCalcules.keyEnumerator();
		while (e.hasMoreElements()) {
			Object cle = e.nextElement();
			if (cle.getClass().getName().equals("java.lang.String")) {
				// cumul global
				NSDictionary cumulCalcule = (NSDictionary)cumulsCalcules.objectForKey(cle);
				texte = texte + cle + "\t\t" + cumulCalcule.objectForKey("montant") + "\n";
			}
		}
		// imprimer les cumuls de rubrique
		texte = texte + "Cumuls de rubrique\n";
		e = cumulsCalcules.keyEnumerator();
		while (e.hasMoreElements()) {
			Object cle = e.nextElement();
			if (cle.getClass().getName().equals("org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique")) {	
				// cumul de rubrique
				NSMutableArray cumulsRubrique = (NSMutableArray)cumulsCalcules.objectForKey(cle);
				java.util.Enumeration e1 = cumulsRubrique.objectEnumerator();
				while (e1.hasMoreElements()) {
					NSDictionary cumulCalcule = (NSDictionary)e1.nextElement();
					texte = texte + cumulCalcule.objectForKey("code") + "\t" + ((EOPayeRubrique)cle).prubLibelle() + "\t" + 
					cumulCalcule.objectForKey("montant") + "\n";
				}	
			}
		}
	return texte;
	}
}
