/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.common;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

public class DateCtrl {
  public static String dateToString(NSTimestamp gregorianDate, String dateFormat) {
    String dateString = "";
	
    NSTimestampFormatter formatter = new NSTimestampFormatter(dateFormat);
    try {
      dateString = formatter.format(gregorianDate);
    } catch(Exception ex) { }
    return dateString;
  }

  public static String dateToString(NSTimestamp gregorianDate) {
    return dateToString(gregorianDate, "%d/%m/%Y");
  }

  /*
  * Convertit la string en date
  */
  public static NSTimestamp stringToDate(String dateString, String dateFormat) {
    NSTimestamp date = null;
    NSTimestampFormatter formatter;
    if ((dateFormat == null) || (dateFormat.trim().length() == 0))
      return null;
    try {
      formatter = new NSTimestampFormatter(dateFormat);
      date = (NSTimestamp)formatter.parseObject(dateString);
      if (!dateString.equals(dateToString(date, dateFormat)))
        return null;
    } catch(Exception ex) { }

    //return date;
    return date.timestampByAddingGregorianUnits(0,0,0,0,0,0);
  }

  public static NSTimestamp stringToDate (String dateString ) {
    return stringToDate(dateString, "%d/%m/%Y");
  }
  public static NSTimestamp jourSuivant(NSTimestamp aDate) {
	  if (aDate == null) {
		  return null;
	  } else {
		  return aDate.timestampByAddingGregorianUnits(0,0,1,0,0,0);
	  }
  }
  /**
	 * Teste si la date <i>date1</i> precede strictement la date <i>date2</i>.
	 */
	public static boolean isBefore(NSTimestamp date1, NSTimestamp date2) {
		// WO4.5.x != WO5.x 
		 return (date1.getTime() < date2.getTime());
	}
	/**
	 * 
	 * @param date
	 * @return numero de l'annee
	 */
	public static int getYear(NSTimestamp date)	{
		java.util.Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return (cal.get(GregorianCalendar.YEAR));
	}
	/** retourne le nombre de jours ecoules entre deux dates 
	 * le r&acu;tesulat est negatif si la deuxi&egrave;me date est anterieure */
	public static int nbJoursEntre(NSTimestamp debut,NSTimestamp fin,boolean inclureBornes) {
		int nbJours = 0;
		boolean estPositif = true;
		NSTimestamp date1 = debut, date2 = fin;
		if (isBefore(fin,debut)) {
			estPositif = false;
			date1 = fin;
			date2 = debut;
		}
		while (dateToString(date1).equals(dateToString(date2)) == false) {
			date1 = date1.timestampByAddingGregorianUnits(0,0,1,0,0,0);
			nbJours++;
		}
		if (inclureBornes) {
			nbJours++;
		}
		if (estPositif) {
			return nbJours;
		} else {
			return -nbJours;
		}
	}
	public static int dernierJourDuMois(NSTimestamp aDate) {
		GregorianCalendar date = new GregorianCalendar();
		date.setTime(aDate);
		return date.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
}