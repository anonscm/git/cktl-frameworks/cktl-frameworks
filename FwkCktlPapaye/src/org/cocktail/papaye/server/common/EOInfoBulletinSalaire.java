/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.common;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.grhum.EOIndividu;
import org.cocktail.papaye.server.metier.grhum.EOStructure;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeHisto;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeOper;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePrepa;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid;
import org.cocktail.papaye.server.metier.jefy_paye._EOPayeHisto;
import org.cocktail.papaye.server.metier.jefy_paye._EOPayePrepa;
import org.cocktail.papaye.server.metier.jefy_paye._EOPayeValid;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/** Cette classe est la super-classe des classes EOPayePrepa, EOPayeHisto, EOPayeValid qui ont exactement la m&ecirc;me structure. Dans cette classe sont definies toutes les methodes communes aux trois sous-classes.
*/
public abstract class EOInfoBulletinSalaire extends InfoBulletinSalaire {

    public EOInfoBulletinSalaire() {
        super();
    }

  
    // méthodes ajoutées
    /** retourne true si la verification a ete effectuee */
    public boolean verificationEffectuee() {
        return temVerifie().equals(Constantes.VRAI);
    }
    /** change le statut de la verification */
    public void setVerificationEffectuee(boolean verifiee) {
        setTemVerifie((verifiee) ? Constantes.VRAI :Constantes.FAUX);
    }
    /** retourne true si un rappel a ete effectue */
    public boolean comporteRappel() {
        return temRappel().equals(Constantes.VRAI);
    }
    /** signale qu'un rappel est effectue pour ce contrat */
    public void signalerRappel(boolean aDesRappels) {
        setTemRappel((aDesRappels) ? Constantes.VRAI :Constantes.FAUX);
    }
    
    /** Initialise tous les montants (brut, net, rappel, patronale,adeduire,cout,bimpmois,bssmois) a zero */
    public void initMontants() {
        setPayeCout(new BigDecimal(0));
        setPayePatron(new BigDecimal(0));
        setPayeAdeduire(new BigDecimal(0));
        setPayeBssmois(new BigDecimal(0));
        setPayeBimpmois(new BigDecimal(0));
        setPayeBrut(new BigDecimal(0));
		setPayeBrutTotal(new BigDecimal(0));
        setPayeNet(new BigDecimal(0));
		setPayeRetenue(new BigDecimal(0));
        setPayeCumulPlafond(new BigDecimal(0));

    }
    public void initAvecObjet(EOInfoBulletinSalaire objet) {
        this.updateFromSnapshot(objet.snapshot());
        
    }
    /** imprime les donnees d'un bulletin de salaire */
    public void imprimer() {
        NSLog.out.appendln(this);
    }
    public String toString() {
        return new String(getClass().getName() +
                          "\nannee : "+ payeAnnee() +
                          "\nmois : "+ mois() +
                          "\nbrut : "+ payeBrut() +
                          "\nbase imposable : "+ payeBimpmois() +
                          "\nbase Secu : "+ payeBssmois() +
                          "\nnet : "+ payeNet() +
                          "\nrappel : "+ temRappel() +
                          "\na deduire : "+ payeAdeduire() +
                          "\na deduire : "+ payeAdeduire() +
                          "\npartPatronale : "+ payePatron() +
                          "\ncout total employeur : "+ payeCout() +
                          "\nindice : "+ payeIndice() +
                          "\ntaux horaire :" + payeTauxhor() +
                          "\nquotite : "+ payeQuotite() +
                          "\nnombre heures : "+ payeNbheure() +
                          "\nnombre jours : "+ payeNbjour() +
                          "\nmode : "+ payeMode() +
                          "\nVerification manuelle : "+ temVerifie() +
                          "\ncontrat : "+ contrat() +
                          "\nadresse : "+ adresse() +
                          "\nrib : "+ rib());
    }
    // méthodes de classe
    /* méthode de classe permettant de trouver toutes les infos bulletins de salaire */
    protected static NSArray chercherInfos(String nomEntite,
                                           EOEditingContext editingContext,
                                           NSArray prefetches) {
        EOFetchSpecification fs = new EOFetchSpecification(nomEntite,null, null);
        if (prefetches != null) {
            fs.setPrefetchingRelationshipKeyPaths(prefetches);
        }
        return editingContext.objectsWithFetchSpecification(fs);
    }
    protected static NSArray chercherInfosPourMoisEtContrat(String nomEntite,
                                                            EOEditingContext editingContext,
                                                            EOPayeMois mois,
                                                            EOPayeContrat contrat,
                                                            NSArray prefetches) {

        // mois
        EOQualifier qualifier;
        NSMutableArray values = new NSMutableArray(mois);
        // pour le contrat
        values.addObject(contrat);
        qualifier = EOQualifier.qualifierWithQualifierFormat("mois = %@ AND contrat = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier, null);
        if (prefetches != null) {
            fs.setPrefetchingRelationshipKeyPaths(prefetches);
        }
        return editingContext.objectsWithFetchSpecification(fs);
    }
	public static NSArray chercherInfosPourMoisEtIndividu(String nomEntite,
                                                            EOEditingContext editingContext,
                                                            EOPayeMois mois,
                                                            EOIndividu individu,
                                                            NSArray prefetches) {
		
        // mois
        EOQualifier qualifier;
        NSMutableArray values = new NSMutableArray(mois);
        // pour le contrat
        values.addObject(individu);
        qualifier = EOQualifier.qualifierWithQualifierFormat("mois = %@ AND contrat.individu = %@",values);
        EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier, null);
        if (prefetches != null) {
            fs.setPrefetchingRelationshipKeyPaths(prefetches);
        }
        fs.setRefreshesRefetchedObjects(true);
        return editingContext.objectsWithFetchSpecification(fs);
    }
	
    protected static NSArray chercherInfosPourOperationEtSecteur(String nomEntite,
                                                      EOEditingContext editingContext,
                                                      EOPayeOper operation,
                                                      EOPayeSecteur secteur,
                                                      NSArray prefetches) {

        // opération courante
        EOQualifier qualifier;
        NSMutableArray values = new NSMutableArray(operation);
        // pour le secteur
        if (secteur != null) {
            values.addObject(secteur);
            qualifier = EOQualifier.qualifierWithQualifierFormat(
                                                    "operation = %@ AND secteur = %@",values);
        } else {
            qualifier = EOQualifier.qualifierWithQualifierFormat(
                                                        "operation = %@",values);
        }
        EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier, null);
        if (prefetches != null) {
            fs.setPrefetchingRelationshipKeyPaths(prefetches);
        }
        return editingContext.objectsWithFetchSpecification(fs);
    }
    protected static NSArray chercherInfosPourMoisEtSecteur(String nomEntite,
                                                      EOEditingContext editingContext,
                                                      EOPayeMois mois,
                                                      EOPayeSecteur secteur,
                                                      NSArray prefetches) {

        EOQualifier qualifier;
        // mois courant
        NSMutableArray values = new NSMutableArray(mois);
        // pour le sectur
        if (secteur != null) {
            values.addObject(secteur);
            qualifier = EOQualifier.qualifierWithQualifierFormat(
                                            "mois = %@ AND secteur = %@",values);
        } else {
            qualifier = EOQualifier.qualifierWithQualifierFormat(
                                            "mois = %@",values);
        }
        EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier, null);
        if (prefetches != null) {
            fs.setPrefetchingRelationshipKeyPaths(prefetches);
        }
        return editingContext.objectsWithFetchSpecification(fs);
    }
	/** methode de classe permettant de trouver le dernier cumul de plafond SS
    pour cet agent :
    - les cumuls de plafond de Sécurité Sociale ne dépendant pas du contrat mais de l'agent,
    il faut retrouver le dernier cumul calcule pour cet agent dans les preparations ou l'historique
	Si l'agent a deja ete paye ce mois (i.e contrat dans EOPayePrepa ou EOPayeValid),
	il faut retirer le plafond du mois car il sera rajoute de nouveau lors de la realisation du Bulletin de salaire
    @param editingContext editing context dans lequel effectuer le fetch
    @param individu EOIndividu individu concerne
    @return montant du cumul plafond SS
    */
    public static double cumulPlafondSSPrecedent(EOEditingContext editingContext,EOIndividu individu) throws Exception {
		double plafond = 0;
		EOPayePrepa prepa = (EOPayePrepa)dernierBulletinSalaire(editingContext,_EOPayePrepa.ENTITY_NAME,individu);
		if (prepa != null) {
			plafond = prepa.payeCumulPlafond().doubleValue();
		} else {
			EOPayeValid valid = (EOPayeValid)dernierBulletinSalaire(editingContext,_EOPayeValid.ENTITY_NAME,individu);
			if (valid != null) {
				plafond = valid.payeCumulPlafond().doubleValue();
			} else {
				EOPayeHisto histo = (EOPayeHisto)dernierBulletinSalaire(editingContext,_EOPayeHisto.ENTITY_NAME,individu);
				if (histo != null) {
					plafond = histo.payeCumulPlafond().doubleValue();
				}
			}
		}
		return plafond;
	}	
	/** methode de classe permettant de trouver le dernier cumul de plafond SS
		pour cet agent ainsi que le plafond deja eventuellement calcule pour ce mois :
		- les cumuls de plafond de Sécurité Sociale ne dépendant pas du contrat mais de l'agent,
		il faut retrouver le dernier cumul calcule pour cet agent dans les preparations ou l'historique
		Si l'agent a deja ete paye ce mois (i.e contrat dans EOPayePrepa ou EOPayeValid),
		@param editingContext editing context dans lequel effectuer le fetch
		@param individu EOIndividu individu concerne
		@return dictionnaire comportant le cumul de Plafond SS et le plafond du mois ou zéro (si on trouve dans l'historique)
		*/
    public static NSDictionary cumulPlafondsPrecedent(EOEditingContext editingContext,EOIndividu individu) throws Exception {
		BigDecimal plafond =  new BigDecimal(0);
		BigDecimal plafondDuMois = new BigDecimal(0);
		EOPayePrepa prepa = (EOPayePrepa)dernierBulletinSalaire(editingContext,"PayePrepa",individu);
		if (prepa != null) {
			plafond = prepa.payeCumulPlafond();
			plafondDuMois = prepa.payePlafondDuMois();
		} else {
			EOPayeValid valid = (EOPayeValid)dernierBulletinSalaire(editingContext,"PayeValid",individu);
			if (valid != null) {
				plafond = valid.payeCumulPlafond();
				plafondDuMois = valid.payePlafondDuMois();
			} else {
				EOPayeHisto histo = (EOPayeHisto)dernierBulletinSalaire(editingContext,"PayeHisto",individu);
				if (histo != null) {
					plafond = histo.payeCumulPlafond();
				}
			}
		}
		NSMutableDictionary result = new NSMutableDictionary(2);
		result.setObjectForKey(plafond,"CumulPlafond");
		result.setObjectForKey(plafondDuMois,"PlafondDuMois");
		return result;
	}	
    
	/** methode de classe permettant de trouver le dernier cumul de plafond SS
    pour cet agent dans un type d'entit&eacut; donne :
    - les cumuls de plafond de Sécurité Sociale ne dépendant pas du contrat mais de l'agent,
    il faut retrouver le dernier cumul calcule pour cet agent dans les preparations ou l'historique
    @param editingContext editing context dans lequel effectuer le fetch
    @param individu EOIndividu individu concerne
    @param nomEntite nom entite concernee (EOPayePrepa, EOPayeHisto,EOPayeValid)
    @return montant du cumul plafond SS
    */
    public static EOInfoBulletinSalaire dernierBulletinSalaire(EOEditingContext editingContext,String nomEntite,EOIndividu individu) {
        
    	EOPayeMois mois = EOPayeMois.moisCourant(editingContext);
        int numMois = mois.numeroDuMois();
        
        if (numMois == 1 && nomEntite.equals("PayeHisto") == true) {
            // janvier, cumul remis à zéro, il n'y a donc pas d'historique
            return null;
        } else {
            // agent
            NSMutableArray values = new NSMutableArray(individu);
            // pour l'année
            values.addObject(mois.moisAnnee());
            EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("contrat.individu = %@ AND payeAnnee = %@",values);
            // trier par ordre décroissant de mois et de montant
			NSMutableArray sorts = new NSMutableArray(2);
			EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("mois.moisCode",EOSortOrdering.CompareDescending);
			sorts.addObject(sort);
			sort = EOSortOrdering.sortOrderingWithKey("payeCumulPlafond",EOSortOrdering.CompareDescending);
			sorts.addObject(sort);
             EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier, sorts);
             fs.setRefreshesRefetchedObjects(true);
			//fs.setFetchLimit(1);
            NSArray resultats = editingContext.objectsWithFetchSpecification(fs);
			try {
				// il y a déjà eu une paye pour cet agent. Les éléments du fetch étant classés
				// par ordre décroissant de mois et de plafond
				EOInfoBulletinSalaire bs = ((EOInfoBulletinSalaire)resultats.objectAtIndex(0));

				// vérifier si on est bien la même année
			/*	if (bs.mois().moisAnnee().intValue() == mois.moisAnnee().intValue()) {
					return bs;
				} else {
					return null;
				}*/
				return bs;
			} catch (Exception e) {
				/*if (nomEntite.equals("PayeHisto")) {
					e.printStackTrace();
				}*/
                // pas d'élément - cet agent n'a pas encore eu de contrat au cours de l'année
                return null;
            }
        }
    }        
    
	/** permet de rechercher tout l'historique + les bs de l'operation courante pour un individu, une structure, une annee
		les informations retournees sont classees par ordre croissant de mois
		@param editingContext editing context dans lequel effectuer le fetch
		@param individu EOIndividu individu concerne
		@param structure concernee 
		@param anneee (4 digits)
		*/
	public static NSArray chercherInfosPourAnneeIndividuStructure(EOEditingContext editingContext,EOIndividu individu,EOStructure structure,Number annee) {
		// chercher d'abord dans l'historique
		// année
        EOQualifier qualifier;
        NSMutableArray values = new NSMutableArray(annee);
        // pour l'individu
        values.addObject(individu);
		// pour la structure
		values.addObject(structure);
        qualifier = EOQualifier.qualifierWithQualifierFormat("payeAnnee = %@ AND contrat.individu = %@ AND structure = %@",values);
		NSArray sorts = new NSArray(new EOSortOrdering("mois.moisCode",EOSortOrdering.CompareAscending));
        EOFetchSpecification fs = new EOFetchSpecification(_EOPayeHisto.ENTITY_NAME,qualifier, sorts);
	//	fs.setPrefetchingRelationshipKeyPaths(new NSArray("elements"));
        NSArray resultFetch =  editingContext.objectsWithFetchSpecification(fs);
		NSMutableArray resultats = new NSMutableArray(resultFetch);
		// chercher dans la préparation
		fs.setEntityName("PayePrepa");
		resultFetch =  editingContext.objectsWithFetchSpecification(fs);
		if (resultFetch.count() > 0) {
			resultats.addObjectsFromArray(resultFetch);
		}
		// chercher dans la validation
		fs.setEntityName("PayeValid");
		resultFetch =  editingContext.objectsWithFetchSpecification(fs);
		if (resultFetch.count() > 0) {
			resultats.addObjectsFromArray(resultFetch);
		}
		return resultats;
	}
	/** permet de rechercher tout l'historique + les bs de l'operation courante pour un individu, une annee et un contrat de non titulaire
	les informations retournees sont classees par ordre decroissant de mois
	@param editingContext editing context dans lequel effectuer le fetch
	@param individu EOIndividu individu concerne
	@param anneee (4 digits)
	@param est titulaire si on cherche les informations concernant les contrats de titulaire
	*/
	
	public static NSArray chercherInfosPourAnneeIndividu(EOEditingContext editingContext,EOIndividu individu,Number annee,boolean estTitulaire) {
		// chercher d'abord dans l'historique
	    EOQualifier qualifier;
		// année
	    NSMutableArray values = new NSMutableArray(annee);
	    // pour l'individu
	    values.addObject(individu);
	    String typeComparaison = "=";
	    if (!estTitulaire) {
	    		typeComparaison = "<>";
	    }
	    qualifier = EOQualifier.qualifierWithQualifierFormat("payeAnnee = %@ AND contrat.individu = %@ AND contrat.statut.payeChampsSaisies.typeStatut " + typeComparaison + " 'T'",values);
		NSArray sorts = new NSArray(new EOSortOrdering("mois.moisCode",EOSortOrdering.CompareDescending));
	    EOFetchSpecification fs = new EOFetchSpecification("PayeHisto",qualifier, sorts);
	    NSArray resultFetch =  editingContext.objectsWithFetchSpecification(fs);
		NSMutableArray resultats = new NSMutableArray(resultFetch);
		// chercher dans la préparation
		fs.setEntityName("PayePrepa");
		resultFetch =  editingContext.objectsWithFetchSpecification(fs);
		if (resultFetch.count() > 0) {
			resultats.addObjectsFromArray(resultFetch);
		}
		// chercher dans la validation
		fs.setEntityName("PayeValid");
		resultFetch =  editingContext.objectsWithFetchSpecification(fs);
		if (resultFetch.count() > 0) {
			resultats.addObjectsFromArray(resultFetch);
		}
		return resultats;
	}

	/** methode de classe permettant de trouver le dernier mois de paiement pour un contrat
	 on recherche dans EOPayePrepa, EOPayeValid ou EOPayeHisto un el&eacute,ment quelconque en triant par mois decroissant
	@param editingContext editing context dans lequel effectuer le fetch
	@param contrat EOPayeContrat concerne
	@return null si non trouve
	*/
	public static EOPayeMois dernierMoisPaiementPourContratEtAnnee(EOEditingContext editingContext,EOPayeContrat contrat,Number annee) {
		EOPayeMois mois = dernierMoisPourEntiteContratEtAnnee(editingContext,"PayePrepa",contrat,annee);
		if (mois == null) {
			mois = dernierMoisPourEntiteContratEtAnnee(editingContext,"PayeValid",contrat,annee);
			if (mois == null) {
				mois = dernierMoisPourEntiteContratEtAnnee(editingContext,"PayeHisto",contrat,annee);
			}
		}
		return mois;
	}
	
	// méthodes privées
	private static EOPayeMois dernierMoisPourEntiteContratEtAnnee(EOEditingContext editingContext,String nomEntite,EOPayeContrat contrat,Number annee) {
		NSMutableArray values = new NSMutableArray(annee);
		// pour l'individu
		values.addObject(contrat);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("payeAnnee = %@ AND contrat = %@",values);
		NSArray sorts = new NSArray(new EOSortOrdering("mois.moisCode",EOSortOrdering.CompareDescending));
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier, sorts);
		fs.setRefreshesRefetchedObjects(true);
		NSArray resultFetch =  editingContext.objectsWithFetchSpecification(fs);
		try {
			return ((EOInfoBulletinSalaire)resultFetch.objectAtIndex(0)).mois();
		}
		catch (Exception e) {
			return null;
		}
	}
}
