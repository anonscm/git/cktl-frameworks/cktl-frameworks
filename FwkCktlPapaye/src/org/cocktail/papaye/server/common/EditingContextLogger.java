/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.common;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSLog;

/**
 * @author christine
 *
 * Pour Logger les problèmes d'editing context. Cette classe se pose comme délégué de l'editing context
 * 
 */
public class EditingContextLogger {
	private EOEditingContext editingContext;
	private String nomEditingContext;
	private boolean presentException;
	
	public EditingContextLogger(EOEditingContext editingContext,String nomEditingContext,boolean presentException) {
		this.editingContext = editingContext;
		this.nomEditingContext = nomEditingContext;
		this.presentException = presentException;
		editingContext.setDelegate(this);
		loggerAdresse();
	}
	public void terminer() {
		editingContext.setDelegate(null);
		editingContext = null;
	}
	// méthode de délégation
	public boolean editingContextShouldPresentException(EOEditingContext context,Throwable exception) {
		NSLog.out.appendln("editing context provoquant exception : ");
		if (context == editingContext) {
			NSLog.out.appendln(nomEditingContext);
		} else {
			NSLog.out.appendln("" + context + ", parent object store: " + context.parentObjectStore());
		}
		return presentException;
	}
	public void loggerAdresse() {
		NSLog.out.appendln(nomEditingContext + " : " + editingContext + " , parent object store " + editingContext.parentObjectStore());
	}

	public void loggerContenu() {
		NSLog.out.appendln(nomEditingContext + " :");
		NSLog.out.appendln("Objets supprimes " + editingContext.deletedObjects().count() + 
				", objets inseres " + editingContext.insertedObjects().count() + ", objets modifies " + editingContext.updatedObjects().count());
	}
	public void loggerContenuDetaille() {
		NSLog.out.appendln("Log detaille de " + nomEditingContext + " :");
		NSLog.out.appendln("Objets supprimes\n" + detailTable(editingContext.deletedObjects()));
		NSLog.out.appendln("Objets inseres\n" + detailTable(editingContext.insertedObjects()));
		NSLog.out.appendln("Objets modifies\n" + detailTable(editingContext.updatedObjects()));
	}
	public static void LogDetail(EOEditingContext editingContext,NSArray table) {
		NSLog.out.appendln("Log Detaille de l'editing context (appel statique):" + editingContext);
		String texte = "";
		java.util.Enumeration e = table.objectEnumerator();
		while (e.hasMoreElements()) {
			EOGenericRecord record = (EOGenericRecord)e.nextElement();
			texte = texte +  "classe objet : " + record.getClass() + ", cle primaire :" + EOUtilities.primaryKeyForObject(editingContext,record) + "\n";
		}
		NSLog.out.appendln(texte);
	}
	// méthode privée
	private String detailTable(NSArray table) {
		String texte = "";
		java.util.Enumeration e = table.objectEnumerator();
		while (e.hasMoreElements()) {
			EOGenericRecord record = (EOGenericRecord)e.nextElement();
			texte = texte +  "classe objet : " + record.getClass() + ", cle primaire :" + EOUtilities.primaryKeyForObject(editingContext,record) + "\n";
		}
		return texte;
	}

}
