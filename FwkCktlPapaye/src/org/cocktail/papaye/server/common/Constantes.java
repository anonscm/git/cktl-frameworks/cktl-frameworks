/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.common;

/** Definition des constantes generales */
public class Constantes {
    /** valeur vraie d'un champ de type booleen */
    public final static String VRAI = "O";
    /** valeur fausse d'un champ de type booleen */
    public final static String FAUX = "N";

    // CIVILITE
    public final static String MONSIEUR = "M.";
    public final static String MADAME = "MME";
    // modes de paiement
    /** Mode de paiement : versement via RIB */
    public static final String VERSEMENT = "VV";
    /** Mode de de paiement : versement par ch&egrave;que */
    public static final String CHEQUE = "CC";
    /** Mode de de paiement : versement en esp&egrave; */
    public static final String ESPECE = "EE";
    /** la rubrique doit &ecirc;tre invalidee */
    public static final int RUBRIQUE_INVALIDEE = 1;
    /** la rubrique doit &ecirc;tre detruite */
    public static final int RUBRIQUE_A_DETRUIRE = 2;
    // DADS
    // statut catétoriel
    /** pas de statut categoriel */
    public static final String PAS_DE_CATEGORIE = "90";
    /** statut categoriel : CADRE */
    public static final String CATEGORIE_CADRE = "01";
    /** statut categoriel : NON CADRE */
    public static final String CATEGORIE_NONCADRE = "02";
    // Classement Conventionnel
    /** sans classement conventionnel */
    public static final String SANS_CLASSEMENT_CONVENTIONNEL = "sans classement conventionnel";
    /** code bureau Accident du Travail */
    public static final String CODE_BUREAU_AT = "B";
    // numéros des organismes complémentaires
    /** numero organisme Ircantec */
    public static final String IRCANTEC = "I0001";
    /** numero organisme CNRACL */
    public static final String CNRACL = "CL001";
    /** numero organisme RAFP */
    public static final String RAFP = "R0001";
    /** numero organisme Pension Civile */
    public static final String SRE = "SRE";
    /** numero organisme CAPRICAS */
    public static final String CAPRICAS = "A190";
    /** numero organisme CARCICAS */
    public static final String CARCICAS = "C022";
    /** pas de cotisations complementaire */
    public static final String SANS_COMPLEMENTAIRE = "90000";
    // type de frais professionnels
    public static final String ALLOCATION_FORFAITAIRE = "F";
    // codes Exonération URSSAF
    /** code Exoneration de l'URSSAF pour les apprentis */
    public static final String CODE_EXONERATION_APPRENTI = "03";
    /** code Exoneration de l'URSSAF pour les CEC */
    public static final String CODE_EXONERATION_CEC = "15";
    /** code Exoneration de l'URSSAF pour les CES */
    public static final String CODE_EXONERATION_CES = "11";
    /** code Exoneration de l'URSSAF pour les CAE */
    public static final String CODE_EXONERATION_CAE = "34";
    /** code Exoneration de l'URSSAF pour les Contrats Avenir */
    public static final String CODE_EXONERATION_CA = "37";

}
