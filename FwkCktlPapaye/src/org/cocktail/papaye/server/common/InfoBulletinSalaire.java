/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.papaye.server.common;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeValid;
import org.cocktail.papaye.server.metier.jefy_paye._EOPayeHisto;
import org.cocktail.papaye.server.metier.jefy_paye._EOPayeValid;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/** Classe abstraite modelisant les parties communes des entites PayePrepa, PayeValid, PayeHisto */
public class InfoBulletinSalaire extends EOGenericRecord {



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "payeOrdre";

	public static final String PAYE_ADEDUIRE_KEY = "payeAdeduire";
	public static final String PAYE_ANNEE_KEY = "payeAnnee";
	public static final String PAYE_BIMPMOIS_KEY = "payeBimpmois";
	public static final String PAYE_BRUT_KEY = "payeBrut";
	public static final String PAYE_BRUT_TOTAL_KEY = "payeBrutTotal";
	public static final String PAYE_BSSMOIS_KEY = "payeBssmois";
	public static final String PAYE_COUT_KEY = "payeCout";
	public static final String PAYE_CUMUL_PLAFOND_KEY = "payeCumulPlafond";
	public static final String PAYE_INDICE_KEY = "payeIndice";
	public static final String PAYE_MODE_KEY = "payeMode";
	public static final String PAYE_NBHEURE_KEY = "payeNbheure";
	public static final String PAYE_NBJOUR_KEY = "payeNbjour";
	public static final String PAYE_NET_KEY = "payeNet";
	public static final String PAYE_PATRON_KEY = "payePatron";
	public static final String PAYE_PLAFOND_DU_MOIS_KEY = "payePlafondDuMois";
	public static final String PAYE_QUOTITE_KEY = "payeQuotite";
	public static final String PAYE_RETENUE_KEY = "payeRetenue";
	public static final String PAYE_TAUXHOR_KEY = "payeTauxhor";
	public static final String PSTA_ORDRE_KEY = "pstaOrdre";
	public static final String TEM_COMPTA_KEY = "temCompta";
	public static final String TEM_DISQUETTE_KEY = "temDisquette";
	public static final String TEM_RAPPEL_KEY = "temRappel";
	public static final String TEM_VERIFIE_KEY = "temVerifie";

// Attributs non visibles
	public static final String PAYE_ORDRE_KEY = "payeOrdre";
	public static final String MOIS_ORDRE_KEY = "moisOrdre";
	public static final String PADR_ORDRE_KEY = "padrOrdre";
	public static final String PCTR_ORDRE_KEY = "pctrOrdre";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String PRIB_ORDRE_KEY = "pribOrdre";
	public static final String PSEC_ORDRE_KEY = "psecOrdre";
	public static final String C_STRUCTURE_SIRET_KEY = "cStructureSiret";

//Colonnes dans la base de donnees
	public static final String PAYE_ADEDUIRE_COLKEY = "paye_adeduire";
	public static final String PAYE_ANNEE_COLKEY = "exe_ordre";
	public static final String PAYE_BIMPMOIS_COLKEY = "paye_bimp_mois";
	public static final String PAYE_BRUT_COLKEY = "paye_brut";
	public static final String PAYE_BRUT_TOTAL_COLKEY = "paye_brut_total";
	public static final String PAYE_BSSMOIS_COLKEY = "paye_bss_mois";
	public static final String PAYE_COUT_COLKEY = "paye_cout";
	public static final String PAYE_CUMUL_PLAFOND_COLKEY = "paye_cumul_plafond";
	public static final String PAYE_INDICE_COLKEY = "paye_indice";
	public static final String PAYE_MODE_COLKEY = "paye_mode";
	public static final String PAYE_NBHEURE_COLKEY = "paye_nb_heures";
	public static final String PAYE_NBJOUR_COLKEY = "paye_nb_jours";
	public static final String PAYE_NET_COLKEY = "paye_net";
	public static final String PAYE_PATRON_COLKEY = "paye_patron";
	public static final String PAYE_PLAFOND_DU_MOIS_COLKEY = "paye_plafond_du_mois";
	public static final String PAYE_QUOTITE_COLKEY = "paye_quotite";
	public static final String PAYE_RETENUE_COLKEY = "paye_retenue";
	public static final String PAYE_TAUXHOR_COLKEY = "paye_taux_horaire";
	public static final String PSTA_ORDRE_COLKEY = "psta_ordre";
	public static final String TEM_COMPTA_COLKEY = "tem_compta";
	public static final String TEM_DISQUETTE_COLKEY = "tem_disquette";
	public static final String TEM_RAPPEL_COLKEY = "tem_rappel";
	public static final String TEM_VERIFIE_COLKEY = "tem_verifie";

	public static final String PAYE_ORDRE_COLKEY = "paye_ordre";
	public static final String MOIS_ORDRE_COLKEY = "mois_ordre";
	public static final String PADR_ORDRE_COLKEY = "adr_ordre";
	public static final String PCTR_ORDRE_COLKEY = "pctr_ordre";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String MOD_ORDRE_COLKEY = "mod_Ordre";
	public static final String PRIB_ORDRE_COLKEY = "rib_ordre";
	public static final String PSEC_ORDRE_COLKEY = "psec_ordre";
	public static final String C_STRUCTURE_SIRET_COLKEY = "C_STRUCTURE_SIRET";


	// Relationships
	public static final String ADRESSE_KEY = "adresse";
	public static final String CONTRAT_KEY = "contrat";
	public static final String HISTO_LBUDS_KEY = "histoLbuds";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String MOIS_KEY = "mois";
	public static final String RIB_KEY = "rib";
	public static final String SECTEUR_KEY = "secteur";
	public static final String STATUT_KEY = "statut";
	public static final String STRUCTURE_KEY = "structure";
	public static final String STRUCTURE_SIRET_KEY = "structureSiret";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal payeAdeduire() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_ADEDUIRE_KEY);
  }

  public void setPayeAdeduire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_ADEDUIRE_KEY);
  }

  public Integer payeAnnee() {
    return (Integer) storedValueForKey(PAYE_ANNEE_KEY);
  }

  public void setPayeAnnee(Integer value) {
    takeStoredValueForKey(value, PAYE_ANNEE_KEY);
  }

  public java.math.BigDecimal payeBimpmois() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_BIMPMOIS_KEY);
  }

  public void setPayeBimpmois(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_BIMPMOIS_KEY);
  }

  public java.math.BigDecimal payeBrut() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_BRUT_KEY);
  }

  public void setPayeBrut(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_BRUT_KEY);
  }

  public java.math.BigDecimal payeBrutTotal() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_BRUT_TOTAL_KEY);
  }

  public void setPayeBrutTotal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_BRUT_TOTAL_KEY);
  }

  public java.math.BigDecimal payeBssmois() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_BSSMOIS_KEY);
  }

  public void setPayeBssmois(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_BSSMOIS_KEY);
  }

  public java.math.BigDecimal payeCout() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_COUT_KEY);
  }

  public void setPayeCout(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_COUT_KEY);
  }

  public java.math.BigDecimal payeCumulPlafond() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_CUMUL_PLAFOND_KEY);
  }

  public void setPayeCumulPlafond(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_CUMUL_PLAFOND_KEY);
  }

  public String payeIndice() {
    return (String) storedValueForKey(PAYE_INDICE_KEY);
  }

  public void setPayeIndice(String value) {
    takeStoredValueForKey(value, PAYE_INDICE_KEY);
  }

  public String payeMode() {
    return (String) storedValueForKey(PAYE_MODE_KEY);
  }

  public void setPayeMode(String value) {
    takeStoredValueForKey(value, PAYE_MODE_KEY);
  }

  public BigDecimal payeNbheure() {
    return (BigDecimal) storedValueForKey(PAYE_NBHEURE_KEY);
  }

  public void setPayeNbheure(BigDecimal value) {
    takeStoredValueForKey(value, PAYE_NBHEURE_KEY);
  }

  public BigDecimal payeNbjour() {
    return (BigDecimal) storedValueForKey(PAYE_NBJOUR_KEY);
  }

  public void setPayeNbjour(BigDecimal value) {
    takeStoredValueForKey(value, PAYE_NBJOUR_KEY);
  }

  public java.math.BigDecimal payeNet() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_NET_KEY);
  }

  public void setPayeNet(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_NET_KEY);
  }

  public java.math.BigDecimal payePatron() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_PATRON_KEY);
  }

  public void setPayePatron(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_PATRON_KEY);
  }

  public java.math.BigDecimal payePlafondDuMois() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_PLAFOND_DU_MOIS_KEY);
  }

  public void setPayePlafondDuMois(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_PLAFOND_DU_MOIS_KEY);
  }

  public BigDecimal payeQuotite() {
    return (BigDecimal) storedValueForKey(PAYE_QUOTITE_KEY);
  }

  public void setPayeQuotite(BigDecimal value) {
    takeStoredValueForKey(value, PAYE_QUOTITE_KEY);
  }

  public java.math.BigDecimal payeRetenue() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_RETENUE_KEY);
  }

  public void setPayeRetenue(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_RETENUE_KEY);
  }

  public BigDecimal payeTauxhor() {
    return (BigDecimal) storedValueForKey(PAYE_TAUXHOR_KEY);
  }

  public void setPayeTauxhor(BigDecimal value) {
    takeStoredValueForKey(value, PAYE_TAUXHOR_KEY);
  }

  public Integer pstaOrdre() {
    return (Integer) storedValueForKey(PSTA_ORDRE_KEY);
  }

  public void setPstaOrdre(Integer value) {
    takeStoredValueForKey(value, PSTA_ORDRE_KEY);
  }

  public String temCompta() {
    return (String) storedValueForKey(TEM_COMPTA_KEY);
  }

  public void setTemCompta(String value) {
    takeStoredValueForKey(value, TEM_COMPTA_KEY);
  }

  public String temDisquette() {
    return (String) storedValueForKey(TEM_DISQUETTE_KEY);
  }

  public void setTemDisquette(String value) {
    takeStoredValueForKey(value, TEM_DISQUETTE_KEY);
  }

  public String temRappel() {
    return (String) storedValueForKey(TEM_RAPPEL_KEY);
  }

  public void setTemRappel(String value) {
    takeStoredValueForKey(value, TEM_RAPPEL_KEY);
  }

  public String temVerifie() {
    return (String) storedValueForKey(TEM_VERIFIE_KEY);
  }

  public void setTemVerifie(String value) {
    takeStoredValueForKey(value, TEM_VERIFIE_KEY);
  }

  public org.cocktail.papaye.server.metier.grhum.EOAdresse adresse() {
    return (org.cocktail.papaye.server.metier.grhum.EOAdresse)storedValueForKey(ADRESSE_KEY);
  }

  public void setAdresseRelationship(org.cocktail.papaye.server.metier.grhum.EOAdresse value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOAdresse oldValue = adresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ADRESSE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat contrat() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat)storedValueForKey(CONTRAT_KEY);
  }

  public void setContratRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTRAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONTRAT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.maracuja.EOModePaiement modePaiement() {
    return (org.cocktail.papaye.server.metier.maracuja.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.papaye.server.metier.maracuja.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.maracuja.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EORibs rib() {
    return (org.cocktail.papaye.server.metier.grhum.EORibs)storedValueForKey(RIB_KEY);
  }

  public void setRibRelationship(org.cocktail.papaye.server.metier.grhum.EORibs value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EORibs oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIB_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur secteur() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur)storedValueForKey(SECTEUR_KEY);
  }

  public void setSecteurRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeSecteur oldValue = secteur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SECTEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SECTEUR_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut statut() {
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut)storedValueForKey(STATUT_KEY);
  }

  public void setStatutRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.jefy_paye.EOPayeStatut oldValue = statut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STATUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STATUT_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOStructure structure() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_KEY);
  }

  public void setStructureRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.papaye.server.metier.grhum.EOStructure structureSiret() {
    return (org.cocktail.papaye.server.metier.grhum.EOStructure)storedValueForKey(STRUCTURE_SIRET_KEY);
  }

  public void setStructureSiretRelationship(org.cocktail.papaye.server.metier.grhum.EOStructure value) {
    if (value == null) {
    	org.cocktail.papaye.server.metier.grhum.EOStructure oldValue = structureSiret();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_SIRET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_SIRET_KEY);
    }
  }
  
  public NSArray histoLbuds() {
    return (NSArray)storedValueForKey(HISTO_LBUDS_KEY);
  }

  public NSArray histoLbuds(EOQualifier qualifier) {
    return histoLbuds(qualifier, null, false);
  }

  public NSArray histoLbuds(EOQualifier qualifier, boolean fetch) {
    return histoLbuds(qualifier, null, fetch);
  }

  public NSArray histoLbuds(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.papaye.server.metier.jefy_paye.EOPayeHistoLbud.VALIDATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.papaye.server.metier.jefy_paye.EOPayeHistoLbud.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = histoLbuds();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToHistoLbudsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeHistoLbud object) {
    addObjectToBothSidesOfRelationshipWithKey(object, HISTO_LBUDS_KEY);
  }

  public void removeFromHistoLbudsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeHistoLbud object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, HISTO_LBUDS_KEY);
  }

  public org.cocktail.papaye.server.metier.jefy_paye.EOPayeHistoLbud createHistoLbudsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PayeHistoLbud");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, HISTO_LBUDS_KEY);
    return (org.cocktail.papaye.server.metier.jefy_paye.EOPayeHistoLbud) eo;
  }

  public void deleteHistoLbudsRelationship(org.cocktail.papaye.server.metier.jefy_paye.EOPayeHistoLbud object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, HISTO_LBUDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllHistoLbudsRelationships() {
    Enumeration objects = histoLbuds().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteHistoLbudsRelationship((org.cocktail.papaye.server.metier.jefy_paye.EOPayeHistoLbud)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPayeValid avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPayeValid createEOPayeValid(EOEditingContext editingContext, java.math.BigDecimal payeAdeduire
, Integer payeAnnee
, java.math.BigDecimal payeBimpmois
, java.math.BigDecimal payeBrut
, java.math.BigDecimal payeBrutTotal
, java.math.BigDecimal payeBssmois
, java.math.BigDecimal payeCout
, java.math.BigDecimal payeCumulPlafond
, BigDecimal payeNbheure
, BigDecimal payeNbjour
, java.math.BigDecimal payeNet
, java.math.BigDecimal payePatron
, BigDecimal payeQuotite
, BigDecimal payeTauxhor
, String temRappel
, String temVerifie
, org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois mois			) {
    EOPayeValid eo = (EOPayeValid) createAndInsertInstance(editingContext, _EOPayeValid.ENTITY_NAME);    
		eo.setPayeAdeduire(payeAdeduire);
		eo.setPayeAnnee(payeAnnee);
		eo.setPayeBimpmois(payeBimpmois);
		eo.setPayeBrut(payeBrut);
		eo.setPayeBrutTotal(payeBrutTotal);
		eo.setPayeBssmois(payeBssmois);
		eo.setPayeCout(payeCout);
		eo.setPayeCumulPlafond(payeCumulPlafond);
		eo.setPayeNbheure(payeNbheure);
		eo.setPayeNbjour(payeNbjour);
		eo.setPayeNet(payeNet);
		eo.setPayePatron(payePatron);
		eo.setPayeQuotite(payeQuotite);
		eo.setPayeTauxhor(payeTauxhor);
		eo.setTemRappel(temRappel);
		eo.setTemVerifie(temVerifie);
    eo.setMoisRelationship(mois);
    return eo;
  }




  
  public static EOPayeValid localInstanceIn(EOEditingContext editingContext, EOPayeValid eo) {
    EOPayeValid localInstance = (eo == null) ? null : (EOPayeValid)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPayeValid#localInstanceIn a la place.
   */
	public static EOPayeValid localInstanceOf(EOEditingContext editingContext, EOPayeValid eo) {
		return EOPayeValid.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPayeHisto.ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPayeValid fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPayeValid fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPayeValid eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPayeValid)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPayeValid fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPayeValid fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPayeValid eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPayeValid)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPayeValid fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPayeValid eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPayeValid ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPayeValid fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
