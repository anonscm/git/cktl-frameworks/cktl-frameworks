/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.utilitaires;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;


public class ServerThreadManager extends Thread {
	private NSMutableDictionary resultat;
	private NSMutableDictionary diagnostic;
	private String nomMethodeAInvoquer;
	private Class[] classeParametres;
	private Object[] parametres;
	private Object target;
	private Class classeTarget;

	/** Statuts du serveur */
	public static final String EN_COURS = "En cours";
	public static final String TERMINE = "Termine";
	/** constructeur
	 * @param objetCible objet dans lequel on invoque la methode
	 * @param classeCible classe de cet objet
	 * @param nomMethode methode &agrave invoquer
	 * @param classes classes des param&egraves;tres de la methode
	 * @param valeurs valeurs des param&egraves;tres 
	 */
	public ServerThreadManager(Object objetCible,Class classeCible,String nomMethode,Class[] classes,Object[]valeurs) {
		target = objetCible;
		classeTarget = classeCible;
		nomMethodeAInvoquer = nomMethode;
		preparerParametres(classes,valeurs);
		resultat = new NSMutableDictionary(2);
		diagnostic = new NSMutableDictionary(3);
		
	}
	
	/** constructeur
	 * @param objetCible objet dans lequel on invoque la methode
	 * @param nomMethode methode &agrave invoquer
	 * @param classes classes des param&egraves;tres de la methode
	 * @param valeurs valeurs des param&egraves;tres 
	 */
	public ServerThreadManager(Object objetCible,String nomMethode,Class[] classes,Object[]valeurs) {
		this(objetCible,objetCible.getClass(),nomMethode,classes,valeurs);
	}
	/** constructeur pour invocation d'une methode de classe
	 * @param classeCible classe
	 * @param nomMethode methode &agrave invoquer
	 * @param classes classes des param&egraves;tres de la methode
	 * @param valeurs valeurs des param&egraves;tres 
	 */
	public ServerThreadManager(Class classeCible,String nomMethode,Class[] classes,Object[]valeurs) {
		this(null,classeCible,nomMethode,classes,valeurs);
	}
	
	/** retourne le resultat de la methode invoquee sous la forme d'un dictionnaire 
	 * dont les cles sont la classe du resultat et le resultat lui-m&ecirc;me **/
	public NSDictionary resultat() {
		return resultat;
	}
	/** retourne le diagnostic de la methode invoquee sous la forme d'un dictionnaire **/
	public NSDictionary diagnostic() {
		return diagnostic;
	}
	
	/** retourne la classe du resultat */
	public String classeResultat() {
		return (String)resultat.objectForKey("classe");
	}
	
	/** retourne la valeur du resultat */
	public Object valeurResultat() {
		return resultat.objectForKey("valeur");
	}
	
	/** modifie la classe du resultat */
	public void setClasseResultat(String nomClasse) {
		resultat.setObjectForKey(nomClasse,"classe");
	}
	/** modifie la valeur du resultat */
	public void setValeurResultat(Object valeur) {
		resultat.setObjectForKey(valeur,"valeur");
	}
	/** retourne le status de l'operation en cours */
	public String status() {
		return (String)diagnostic.objectForKey("status");
	}
	/** retourne le message d'information de l'operation en cours. Peut contenir plusieurs messages separes par
	 * "\n" si l'operation a emis plusieurs messages qui n'ont pas ete transmis */
	public String message() {
		String texte = (String)diagnostic.objectForKey("message");
		diagnostic.removeObjectForKey("message");
		return texte;
	}
	/** retourne le message d'exception declenche pendant l'operation */
	public String exception() {
		return (String)diagnostic.objectForKey("exception");
	}
	/** modifie le status de l'operation en cours */
	public void setStatus(String unTexte) {
		diagnostic.setObjectForKey(unTexte,"status");
	}
	/** modifie le message d'information de l'operation en cours */
	public void setMessage(String unTexte) {
		// concaténer les messages si ils n'ont pas encore été lus
		if (diagnostic.objectForKey("message") != null) {
			diagnostic.setObjectForKey(diagnostic.objectForKey("message") + "\n" + unTexte,"message");
		} else {
			diagnostic.setObjectForKey(unTexte,"message");
		}
	}
	/** modifie le message d'exception de l'operation en cours */
	
	public void setException(String unTexte) {
		if (unTexte != null) {		
			diagnostic.setObjectForKey(unTexte,"exception");
		}
	}
	/** lance l'execution de la methode : a utiliser pour une application Java Client */
	public void run() {
		lancerOperation();
	}
	/** lance l'execution de la methode : a utiliser pour une application WO HTML */
	public void lancerOperation() {
		try {
			setStatus(EN_COURS);
			java.lang.reflect.Method methode = classeTarget.getMethod(nomMethodeAInvoquer,classeParametres);
			Object object = methode.invoke(target,parametres);
			if (object != null) {
				setClasseResultat(object.getClass().getName());
				setValeurResultat(object);
			}
			setStatus(TERMINE);

		} catch(Exception e) {
			System.out.println(e);
			setException(e.getMessage());
			setStatus(TERMINE);

		}
	}
	private void preparerParametres(Class[] classes,Object[] valeurs) {
		classeParametres = new Class[classes.length + 1];
		for (int i = 0; i < classes.length; i++) {
			classeParametres[i] = classes[i];
		}
		try {
			classeParametres[classes.length] = Class.forName("org.cocktail.papaye.server.utilitaires.ServerThreadManager");
		} catch (Exception e) {}
		parametres = new Object[valeurs.length + 1];
		for (int i = 0; i < valeurs.length; i++) {
			parametres[i] = valeurs[i];
		}
		parametres[valeurs.length] = this;
	}		
}
