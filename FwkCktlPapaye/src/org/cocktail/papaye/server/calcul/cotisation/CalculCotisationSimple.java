/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.moteur.ModeleCalcul;


/** Cette classe modelise tous les calculs de cotisations qui sont bases sur la r&egrave;gle de calcul suivante :<BR>
Calcul : taux x (baseAssiette x tauxAssiette) %
Elle lance une exception si un des param&egrave;tres utilises pour le calcul a une valeur nulle. Le message de l'exception indique la classe et la valeur responsables de l'exception
*/
public class CalculCotisationSimple extends ModeleCalcul {
    private EOPayeCode codeCotisation;
    private double taux;
    private double tauxAssiette;

    /** accesseur de lecture du code de la cotisation */
    public EOPayeCode codeCotisation() { return codeCotisation; }
    /** accesseur d'ecriture du code de la cotisation */
    public void setCodeCotisation(EOPayeCode aCode) { codeCotisation = aCode; }
    /** accesseur de lecture du taux de cotisation */
    public double taux() { return taux; }
    /** accesseur d'ecriture du taux cotisation */
    public void setTaux(double aNum) { taux = aNum; }
    /** accesseur de lecture du taux de l'assiette de cotisation */
    public double tauxAssiette() { return tauxAssiette; }
    /** accesseur d'ecriture du taux de l'assiette de cotisation */
    public void setTauxAssiette(double aNum) { tauxAssiette = aNum; }

    /** Effectue le calcul de la cotisation en appliquant la regle taux x (baseAssiette x tauxAssiette) % <BR>
    Ajoute le resultat obtenu au dictionnaire des resultats.
    @param codeTaux code associe au taux de la part de cotisation
    @param codeAssiette code associe au taux de l'assiette
    @param baseAssiette base sur laquelle est effectue le calcul
    @param cumul valeur du cumul determinee auparavant
    */
    public void effectuerCalcul(String codeTaux,String codeAssiette,BigDecimal baseAssiette,BigDecimal cumul) throws Exception {
		preparerParametres(codeTaux,codeAssiette);
        if (baseAssiette == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", la base de l'assiette" + codeAssiette + " n'est pas definie");
        }
                
        // prendre comme assiette la valeur absolue de l'assiette de calcul (cas des salaires trop payés)
        BigDecimal baseAssietteAbsolue = baseAssiette.abs();
        double assiette = baseAssietteAbsolue.doubleValue() * (tauxAssiette / 100);
        double result = assiette * (taux / 100);
        // arrondir les résultats au centième d'euro près et fournir deux chiffres
        // après la virgule
		if (result != 0) {
			ajouterCotisation(codeCotisation(),
                          new BigDecimal(result).setScale(2,BigDecimal.ROUND_HALF_UP),
                          new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_DOWN));
		}
    }
	/** Effectue le calcul de la cotisation en appliquant la r&egrave;gle taux x (baseAssiette x tauxAssiette) % <BR>
        Ajoute le resultat obtenu au dictionnaire des resultats.
        @param codeTaux code associe au taux de la part de cotisation
        @param codeAssiette code associe au taux de l'assiette
        @param baseAssiette base sur laquelle est effectue le calcul
        */
    public void effectuerCalcul(String codeTaux,String codeAssiette,BigDecimal baseAssiette,boolean assiettePeutEtreNegative) throws Exception {
		preparerParametres(codeTaux,codeAssiette);
        if (baseAssiette == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", la base de l'assiette" + codeAssiette + " n'est pas definie");
        }
        // prendre comme assiette la valeur absolue de l'assiette de calcul (cas des salaires trop payés)
		BigDecimal baseAssietteAbsolue = baseAssiette;
		if (!assiettePeutEtreNegative) {
	        baseAssietteAbsolue = baseAssiette.abs();
		}
        double assiette = baseAssietteAbsolue.doubleValue() * (tauxAssiette / 100);
        double result = assiette * (taux / 100);
        

        // arrondir les résultats au centième d'euro près et fournir deux chiffres
        // après la virgule
		if (result != 0) {
			ajouterCotisation(codeCotisation(),
							  new BigDecimal(result).setScale(2,BigDecimal.ROUND_HALF_UP),
							  new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_DOWN));
		}
    }
	
    /** Effectue le calcul de la cotisation en appliquant la regle taux x (baseAssiette x tauxAssiette) % <BR>
        Ajoute le resultat obtenu au dictionnaire des resultats.
        @param codeTaux code associe au taux de la part de cotisation
        @param codeAssiette code associe au taux de l'assiette
        @param baseAssiette base sur laquelle est effectue le calcul
        */
    public void effectuerCalcul(String codeTaux,String codeAssiette,BigDecimal baseAssiette) throws Exception {
		preparerParametres(codeTaux,codeAssiette);
        if (baseAssiette == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", la base de l'assiette" + codeAssiette + " n'est pas definie");
        }
        // prendre comme assiette la valeur absolue de l'assiette de calcul (cas des salaires trop payés)
        BigDecimal baseAssietteAbsolue = baseAssiette.abs();
        double assiette = baseAssietteAbsolue.doubleValue() * (tauxAssiette / 100);
        double result = assiette * (taux / 100);
        // arrondir les résultats au centième d'euro près et fournir deux chiffres
        // après la virgule

        if (result != 0) {
			ajouterCotisation(codeCotisation(),
                          new BigDecimal(result).setScale(2,BigDecimal.ROUND_HALF_UP),
                          new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_DOWN));
		}
    }
    /** prepare les parametres utiles au calcul */
    protected void preparerParametres(String codeTaux,String codeAssiette) throws Exception {
        EOPayeParam parametre1 = parametrePourCode(codeTaux);
        if (parametre1 == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le taux " + codeTaux + " n'est pas defini");
        }
        setCodeCotisation(parametre1.code());
        EOPayeParam parametre2 = parametrePourCode(codeAssiette);
        if (parametre2 == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le taux de l'assiette" + codeAssiette + " n'est pas defini");
        }
        setTaux(parametre1.pparTaux().doubleValue());
        setTauxAssiette(parametre2.pparTaux().doubleValue());
        if (taux() == 0 || tauxAssiette() == 0) {
            throw new Exception("Pour la classe : "+ nomClasse() +
                                ", un des parametres de taux a une valeur nulle");
        }
    }
}
