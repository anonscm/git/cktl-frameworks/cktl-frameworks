/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCumul;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.moteur.ModeleCalcul;



/** Cette classe modelise tous les calculs de taxe sur salaire. Selon l'assiette de calcul (fonction des cumuls), plusieurs cotisations de taxe sur salaire a des taux differents peuvent &ecirc;tre ajoutees aux resultats.
Elle lance une exception si un des param&egrave;tres utilise pour le calcul a une valeur nulle. Le message de l'exception indique la classe et la valeur responsables de l'exception
*/
public class CalculTaxeSurSalaire extends ModeleCalcul {
    public static  final String TAUX1 = "TAXSSAL1";
    private static final String TAUX2 = "TAXSSAL2";
    private static final String TAUX3 = "TAXSSAL3";
    private static  final String PLAFOND1 = "PLATAXS1";
    private static final String PLAFOND2 = "PLATAXS2";
	private static final String CODE_TRANCHE1 = "COTTXSAL";
	private static final String CODE_TRANCHE2 = "COTTXSA2";
	private static final String CODE_TRANCHE3 = "COTTXSA3";

	
    private double taux1;
    private double taux2;
    private double taux3;
    private double tranche1;
    private double tranche2;
    private EOPayeCode code1;
    private EOPayeCode code2;
    private EOPayeCode code3;
    
    public void effectuerCalcul(BigDecimal baseImposable) throws Exception {
		preparerParametres();
		EOPayeMois mois = preparation().mois();
        double assietteCumulPrecedent = 0;
		// rechercher le dernier cumul de taxe sur salaire de tranche 1
        EOPayeCumul cumul = EOPayeCumul.rechercherDernierCumul(editingContext(),mois,CODE_TRANCHE1, contrat().individu());
		if (cumul != null) {
			assietteCumulPrecedent = assietteCumulPrecedent + cumul.pcumBase().doubleValue();
		}
		// rechercher le dernier cumul de taxe sur salaire de tranche 2
		cumul = EOPayeCumul.rechercherDernierCumul(editingContext(),mois,CODE_TRANCHE2, contrat().individu());
		if (cumul != null) {
			assietteCumulPrecedent = assietteCumulPrecedent + cumul.pcumBase().doubleValue();
		}
		// rechercher le dernier cumul de taxe sur salaire de tranche 3
		cumul = EOPayeCumul.rechercherDernierCumul(editingContext(),mois,CODE_TRANCHE3, contrat().individu());
		if (cumul != null) {
			assietteCumulPrecedent = assietteCumulPrecedent + cumul.pcumBase().doubleValue();
		}
		// salaire positif
		if (baseImposable.doubleValue() > 0) {
	        double assietteTotale = assietteCumulPrecedent + baseImposable.doubleValue();
	        // calcul
	        if (assietteTotale <= tranche1) {
	            // on est en-dessous de la tranche 1
	            double calcul = baseImposable.doubleValue() * (taux1 / 100);
				EOPayeCode codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE1);
	            ajouterCotisation(code1,new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_UP),baseImposable,codeTranche);
	        } else if (assietteTotale < tranche2) {
	            // on est entre la tranche 1 et la tranche 2
	            // vérifier où se trouver le cumul précédent pour voir
	            // si il avait déjà franchi la tranche 1
	            if (assietteCumulPrecedent < tranche1) {
	                // ce n'est pas le cas
	                double difference = assietteTotale - tranche1;
	                double valeur1 = baseImposable.abs().doubleValue() - difference;
	                double calcul = valeur1 * (taux1 / 100);
					EOPayeCode codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE1);
	                ajouterCotisation(code1,
	                                  new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(valeur1).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
	                calcul = difference * (taux2 / 100);
					codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE2);
	                ajouterCotisation(code2,
	                                  new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(difference).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
	            } else  {
	                // c'est le cas
	                double calcul = baseImposable.doubleValue() * (taux2 / 100);
					EOPayeCode codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE2);
	                ajouterCotisation(code2,
	                                  new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  baseImposable,codeTranche);
	            }
	        } else { // on a dépassé la tranche 3
				// vérifier si le cumul précédent avait déjà franchi la tranche 1
	            if (assietteCumulPrecedent < tranche1) {
	                // ce n'est pas le cas
	                double valeur1 = tranche1 - assietteCumulPrecedent;
	                double calcul = valeur1 * (taux1 / 100);
					EOPayeCode codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE1);
	                ajouterCotisation(code1,
	                                  new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(valeur1).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
					calcul = (tranche2 - tranche1) * (taux2 / 100);
					codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE2);
					ajouterCotisation(code2,
	                                  new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(tranche2 - tranche1).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
	                double difference = assietteTotale - tranche2;
	                calcul = difference * (taux3 / 100);
					codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE3);
	                ajouterCotisation(code3,
	                                  new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(difference).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
				} else if (assietteCumulPrecedent < tranche2) {  // vérifier si le cumul précédent avait déjà franchi la tranche 2
	                // ce n'est pas le cas
	                double difference = assietteTotale - tranche2;
	                double valeur1 = baseImposable.doubleValue() - difference;
	                double calcul = valeur1 * (taux2 / 100);
					EOPayeCode codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE2);
	                ajouterCotisation(code2,
	                                  new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(valeur1).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
	                calcul = difference * (taux3 / 100);
					codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE3);
	                ajouterCotisation(code3,
	                                  new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(difference).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
	            } else {
	                // c'est le cas
	                double calcul = baseImposable.doubleValue() * (taux3 / 100);
					EOPayeCode codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE3);
	                ajouterCotisation(code3,
	                                  new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  baseImposable,codeTranche);
	            }
	        }
		} else {
			// salaire en négatif
	        double assietteTotale = assietteCumulPrecedent + baseImposable.doubleValue();
	        if (assietteCumulPrecedent <= tranche1) {
	            // on était en-dessous de la tranche 1, l'assiette du mois étant négative on reste en-dessous de la tranche 1
	            double montant = baseImposable.doubleValue() * (taux1 / 100);
				EOPayeCode codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE1);
	            ajouterCotisation(code1,new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP),baseImposable,codeTranche);
	        } else if (assietteCumulPrecedent <= tranche2) {
	            // vérifier si on passe en tranche 1 ou en tranche 2 avec la nouvelle assiette
	            if (assietteTotale <= tranche1) {
	            		// on repasse en tranche 1
	            		// on retire la partie tranche 2
	                double assietteTranche2 = tranche1 - assietteCumulPrecedent;
	                double assietteTranche1 = baseImposable.doubleValue() - assietteTranche2;		// doit donner une valeur négative
	                double montant = assietteTranche1 * (taux1 / 100);					// doit donner un montant négatif
	                EOPayeCode codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE1);
	                ajouterCotisation(code1,
	                                  new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(assietteTranche1).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
	                montant = assietteTranche2 * (taux2 / 100);	
	                codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE2);
	                ajouterCotisation(code2,
	                                  new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(assietteTranche2).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
	            } else  {
	                // on reste en tranche 2
	                double montant = baseImposable.doubleValue() * (taux2 / 100);
	                EOPayeCode codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE2);
	                ajouterCotisation(code2,
	                                  new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  baseImposable,codeTranche);
	            }
	        } else { // on était en tranche 3
	            if (assietteTotale <= tranche1) {
	            		// on repasse en tranche 1, il faut supprimer les tranches 2 et 3
	                double assietteTranche3 = tranche2 - assietteCumulPrecedent;		// devrait être négatif
	                double montant = assietteTranche3 * (taux3 / 100);
	                EOPayeCode codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE3);
	                ajouterCotisation(code3,
	                                  new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(assietteTranche3).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
					montant = -tranche2 * (taux2 / 100);
					codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE2);
					ajouterCotisation(code2,
	                                  new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(-tranche2).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
					double assietteTranche1 = baseImposable.doubleValue() + tranche2 - assietteTranche3;		// devrait être négatif
					montant = assietteTranche1 * (taux1 / 100);
					codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE1);
					ajouterCotisation(code1,
	                                  new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(assietteTranche1).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
	            
				} else if (assietteTotale <= tranche2) { 
					// on repasse en tranche 2, il faut supprimer la tranche 3
	                double assietteTranche3 = tranche2 - assietteCumulPrecedent;					// devrait être négatif
	                double assietteTranche2 =  baseImposable.doubleValue() - assietteTranche3;	// devrait être négatif
	                double montant = assietteTranche2 * (taux2 / 100);
	                EOPayeCode codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE2);
	                ajouterCotisation(code2,
	                                  new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(assietteTranche2).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
	                montant = assietteTranche3 * (taux3 / 100);
	                codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE3);
	                ajouterCotisation(code3,
	                                  new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  new BigDecimal(assietteTranche3).setScale(2,BigDecimal.ROUND_HALF_DOWN),codeTranche);
	            } else {
	                // on reste en tranche 3
	                double calcul = baseImposable.doubleValue() * (taux3 / 100);
					EOPayeCode codeTranche = EOPayeCode.rechercherCode(editingContext(),CODE_TRANCHE3);
	                ajouterCotisation(code3,
	                                  new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_UP),
	                                  baseImposable,codeTranche);
	            }
	        }
		
		}
    }
    private void preparerParametres() throws Exception {
        // vérifier tous les paramètres
        EOPayeParam parametre1 = parametrePourCode(TAUX1);
        if (parametre1 == null) {
            throw new Exception ("Dans la classe TaxeSurSalaire, le  taux 1 de la taxe sur salaire n'est pas défini");
        }
        code1 = parametre1.code();
        EOPayeParam parametre2 = parametrePourCode(TAUX2);
        if (parametre2 == null) {
            throw new Exception ("Dans la classe TaxeSurSalaire, le  taux 2 de la taxe sur salaire n'est pas défini");
        }
        code2 = parametre2.code();
        EOPayeParam parametre3 = parametrePourCode(TAUX3);
        if (parametre3 == null) {
            throw new Exception ("Dans la classe TaxeSurSalaire, le  taux 3 de la taxe sur salaire n'est pas défini");
        }
        code3 = parametre3.code();
        EOPayeParam parametre4 = parametrePourCode(PLAFOND1);
        if (parametre4 == null) {
            throw new Exception ("Dans la classe TaxeSurSalaire, le  plafond 1 de la taxe sur salaire n'est pas défini");
        }
        EOPayeParam parametre5 = parametrePourCode(PLAFOND2);
        if (parametre5 == null) {
            throw new Exception ("Dans la classe TaxeSurSalaire, le  plafond 2 de la taxe sur salaire n'est pas défini");
        }
        
        code3 = parametre3.code();
        taux1 = parametre1.pparTaux().doubleValue();
        taux2 = parametre2.pparTaux().doubleValue();
        taux3 = parametre3.pparTaux().doubleValue();
        tranche1 = parametre4.pparMontant().doubleValue();
        tranche2 = parametre5.pparMontant().doubleValue();
        if (taux1 == 0 || taux2 == 0 || taux3 == 0) {
            throw new Exception ("Dans la classe TaxeSurSalaire, la valeur d'un des taux de la taxe sur salaire n'est pas définie");
        } else if (tranche1 == 0 || tranche2 == 0) {
            throw new Exception ("Dans la classe TaxeSurSalaire, la valeur d'une des tranches de la taxe sur salaire n'est pas définie");
        }
    }
}
