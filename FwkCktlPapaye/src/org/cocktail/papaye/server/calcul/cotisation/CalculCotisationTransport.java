/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.moteur.ModeleCalcul;



/** Effectue le calcul de la part patronale de la cotisation Transport<BR>
Calcul : taux x (baseAssiette x tauxAssiette)% <BR>
Le taux est determine par la structure associee au contrat
*/
public class CalculCotisationTransport extends ModeleCalcul {
    private double tauxAssiette;
    
    /* Effectue le calcul de la cotisation Transport en appliquant la règle taux x (baseAssiette x tauxAssiette) % dans le cas général.
    Ajoute le résultat obtenu au dictionnaire des résultats. */
    protected void effectuerCalcul(String codeAssiette,BigDecimal baseAssiette,double pourcentageTaux) throws Exception {
        String codeTaux = contrat().structure().codeTauxTransport();
        if (codeTaux != null) {
            EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),codeTaux);
            if (code == null) {
                throw new Exception("Pour la classe " + nomClasse() + ", le code " + codeTaux + " n'est pas defini");
            }
            EOPayeParam parametre  = EOPayeParam.parametreValide(code);
            if (parametre == null) {
                throw new Exception("Pour la classe " + nomClasse() + ", le parametre associe au " + codeTaux + " n'est pas defini");
            }
            if (parametre.pparTaux() == null) {
                throw new Exception("Pour la classe " + nomClasse() + ", le taux associe au code "+ codeTaux + " a une valeur nulle");
            }
            double taux = parametre.pparTaux().doubleValue() ;
            taux = taux * (pourcentageTaux / 100);
            
            if (tauxAssiette == 0) {
                EOPayeParam parametre2 = parametrePourCode(codeAssiette);
                if (parametre2 == null) {
                    throw new Exception("Pour la classe " + nomClasse() + ", le taux de l'assiette" + codeAssiette + " n'est pas defini");
                }
                if (parametre2.pparTaux() == null) {
                    throw new Exception("Pour la classe " + nomClasse() + ", le taux de l'assiette a une valeur nulle");
                }
                tauxAssiette = parametre2.pparTaux().doubleValue();
            }

            if (baseAssiette == null) {
                throw new Exception("Pour la classe " + nomClasse() + ", la base de l'assiette" + codeAssiette + " n'est pas definie");
            }
            // au cas où la valeur est négative de l'assiette
            double assiette = baseAssiette.abs().doubleValue() * (tauxAssiette / 100);
            double result = assiette * (taux / 100);
            // arrondir les résultats au centième d'euro près et fournir deux chiffres
            // après la virgule
            if (result != 0) {	// cas où le taux transport est 0
                ajouterCotisation(parametre.code(),
                              new BigDecimal(result).setScale(2,BigDecimal.ROUND_HALF_DOWN),
                              new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_DOWN));
            }
        } else {
            throw new Exception ("Dans la classe " + nomClasse() + ", le code taux de Transport pour le contrat " + contrat() + " n'est pas defini dans la structure");
        }
    }
}
