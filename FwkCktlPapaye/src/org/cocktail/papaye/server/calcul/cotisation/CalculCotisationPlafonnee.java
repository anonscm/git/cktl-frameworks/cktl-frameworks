/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;

import org.cocktail.papaye.server.common.EOInfoBulletinSalaire;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCumul;

import com.webobjects.foundation.NSMutableDictionary;


/** Cette classe modelise tous les calculs de cotisations qui utilisent le plafond de
securite sociale. Elle apporte une methode de calcul pour evaluer le plafond reel en fonction de la quotite travaillee et du nombre de jours travailles dans le mois et une methode pour evaluer l'assiette reelle et le prochain cumul.
La progression des cumuls n'etant pas lineaire, on stocke les nouveaux cumuls dans les resultats.
*/

public class CalculCotisationPlafonnee extends CalculCotisationSimple {
    public static String PLAFOND_MENSUEL = "PLAFMSSS";
    private static String CUMUL_SFT = "REMUNSFT";
    private double plafondSS;
    private double cumulAssiettePlafonnee;
    
    /** accesseur de lecture. Utilise uniquement pour les artistes/intermittents */
    public double plafondSS() { return plafondSS; } 
    /** accesseur d'/&acute;criture;. Utilise uniquement pour les artistes/intermittents  */
    public void setPlafondSS(double aNum) { plafondSS = aNum; }

    /** Effectue le calcul du montant d'une cotisation plafonnee
        @param codeTaux code du taux de cotisation
        @param codeTauxAssiette code du taux de l'assiette de cotisation
        @param assiette montant de l'assiette utilise pour le calcul
        @param calculerAssiette l'assiette doit-elle &ecirc;tre recalculee ?
        */
    public void effectuerCalcul(String codeTaux,String codeAssiette,BigDecimal remuneration,boolean calculerAssiette) throws Exception {
       if (calculerAssiette) {
           this.effectuerCalcul(codeTaux,codeAssiette,remuneration);
       } else {
           super.effectuerCalcul(codeTaux,codeAssiette,remuneration);
       }
    }
    /** effectue le calcul du montant d'une cotisation plafonnee pour une
        remuneration mensuelle
        @param codeTaux code du taux de cotisation
        @param codeTauxAssiette code du taux de l'assiette de cotisation
        @param assiette montant de l'assiette utilise pour le calcul
        */
    public void effectuerCalcul(String codeTaux,String codeAssiette,BigDecimal remuneration) throws Exception {
        this.effectuerCalcul(codeTaux,codeAssiette,remuneration,null);
    }
    
    /** effectue le calcul du montant d'une cotisation plafonnee, la remuneration est
        mensuelle  avec prise en compte du montant du SFT
        @param codeTaux code du taux de cotisation
        @param codeTauxAssiette code du taux de l'assiette de cotisation
        @param assiette montant de l'assiette utilise pour le calcul
        @param montantSFT montant du SFT utilise pour le calcul
        */
    public void effectuerCalcul(String codeTaux,String codeAssiette,BigDecimal remuneration,BigDecimal montantSFT) throws Exception {
        // calculer l'assiette
        BigDecimal baseAssiette = calculerAssiette(remuneration,montantSFT);
        super.effectuerCalcul(codeTaux,codeAssiette,baseAssiette);
        ajouterCumulAuResultat();
    }
    
    // Méthodes protégées
    protected double evaluerCumulPrecedent() {
    	return EOPayeCumul.trouverDerniereAssiette(editingContext(),mois(),EOPayeCode.CODEBASESS,contrat().individu(),cumuls());
    }
    // Calcule l'assiette d'une cotisation plafonnée en fonction des cumuls SS des mois précédents
    private BigDecimal calculerAssiette(BigDecimal remunerationReelle,BigDecimal SFT) throws Exception {
		// vérifier d'abord si il n'y a pas un cumul de sécu dans les préparations sinon rechercher dans l'historique
		double cumulSSMensuelPrecedent = EOInfoBulletinSalaire.cumulPlafondSSPrecedent(editingContext(),contrat().individu());
        double cumulSSMensuel = preparation().payeCumulPlafond().doubleValue();
        // rechercher le dernier cumul pour cette rubrique avec l'assiette la plus haute
		// rechercher par le biais du code cumul associé à cette rubrique
        double cumulAssiettePlafonneePrecedente = EOPayeCumul.trouverDerniereAssiette(editingContext(),mois(),codeCumul().pcodCode(),contrat().individu(),cumuls());
		if (cumulAssiettePlafonneePrecedente == 0) {
			// il n'y a pas eu de charges plafonnées payées précédemment, ne pas prendre en compte les rémunérations précédentes
			double montantSFT = 0;
			if (SFT != null) {
				if (remunerationReelle.doubleValue() < 0) {
					// il s'agit d'une retenue globale du BS
					montantSFT = SFT.abs().doubleValue();
				} else {
					montantSFT = SFT.doubleValue();
				}
			} 
			double remuneration = remunerationReelle.abs().doubleValue();
			cumulAssiettePlafonnee = 0;
			double assiette = 0;
			//NSLog.out.appendln("remun " + remuneration);
			//NSLog.out.appendln("cumulSSMensuel "+ cumulSSMensuel);
			if ((remuneration - montantSFT) <= cumulSSMensuel) { // cumulSSMensuel - cumulSSPrecedent car on a accumulé dans tous les cas les plafonds
				// alors qu'il n'aurait fallu le faire que dans les cas de paiement de cotisation - A reprendre
				// on est en-dessous du plafond
				assiette = remuneration - montantSFT;
				cumulAssiettePlafonnee = remuneration - montantSFT;
			} else {	// on est au-dessus
				assiette = preparation().payePlafondDuMois().doubleValue() + cumulPlafondPourRappel().doubleValue();
				cumulAssiettePlafonnee = preparation().payePlafondDuMois().doubleValue() + cumulPlafondPourRappel().doubleValue();
			}
			return new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_DOWN);
		} else {  // l'agent a déjà payé une cotisation plafonnée
			// rechercher le dernier cumul de rémunération précédent
			double cumulRemunPrecedent = evaluerCumulPrecedent();
			double cumulSFTPrecedent = 0;
			double montantSFT = 0;
			if (SFT != null) {
				cumulSFTPrecedent = EOPayeCumul.trouverDernierMontant(editingContext(),mois(),CUMUL_SFT,contrat().individu(),cumuls());
				if (remunerationReelle.doubleValue() < 0) {
					// il s'agit d'une retenue globale du BS
					montantSFT = SFT.abs().doubleValue();
				} else {
					montantSFT = SFT.doubleValue();
				}
		   }

			double remuneration = remunerationReelle.abs().doubleValue();
			cumulAssiettePlafonnee = 0;
			double assiette = 0;
//			NSLog.out.appendln("remun " + remuneration);
//			NSLog.out.appendln("cumulRemunPrecedent " + cumulRemunPrecedent);
//			NSLog.out.appendln("cumulSSMensuelPrecedent " + cumulSSMensuelPrecedent);
//			NSLog.out.appendln("cumulSSMensuel "+ cumulSSMensuel);
//			NSLog.out.appendln("cumulSFTPrecedent "+ cumulSFTPrecedent);
//			NSLog.out.appendln("cumuleAssiettePlafonnee Precedente "+ cumulAssiettePlafonneePrecedente);
			if ((cumulRemunPrecedent - cumulSFTPrecedent) + (remuneration - montantSFT) <=
															cumulSSMensuel) {
				// on est en-dessous du plafond
				if ((cumulRemunPrecedent - cumulSFTPrecedent) <= cumulSSMensuelPrecedent) {
					// on était déjà au-dessous du plafond
					assiette = remuneration - montantSFT;
					cumulAssiettePlafonnee = cumulAssiettePlafonneePrecedente + (remuneration - montantSFT);
				} else {
					// on était au-dessus
					assiette = (cumulRemunPrecedent - cumulSFTPrecedent) + (remuneration  - montantSFT) - cumulAssiettePlafonneePrecedente;
					cumulAssiettePlafonnee = cumulRemunPrecedent + (remuneration - montantSFT);
				}
			}
			else {	// on est au-dessus
				   // on était au-dessous du plafond
				if ((cumulRemunPrecedent - cumulSFTPrecedent) <= cumulSSMensuelPrecedent) {
					assiette = cumulSSMensuel - (cumulRemunPrecedent - cumulSFTPrecedent);
					cumulAssiettePlafonnee = cumulSSMensuel;
				} else {
					// on était déjà au-dessus
					assiette = cumulSSMensuel - cumulSSMensuelPrecedent;
					cumulAssiettePlafonnee = cumulSSMensuel;
				}
			}
			return new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_DOWN);
		}
    }
    // stocke le nouveau cumul dans les résultats
    private void ajouterCumulAuResultat() {
        // ajouter aussi le cumul spécial aux résultats, un seul dictionnaire a été inséré dans les résultats
		if (resultats().allValues().count() > 0) {
			NSMutableDictionary resultat = (NSMutableDictionary)resultats().allValues().objectAtIndex(0);
			resultat.setObjectForKey(new BigDecimal(cumulAssiettePlafonnee).setScale(2,BigDecimal.ROUND_HALF_DOWN), "Cumul");
		}
    }
   
}
