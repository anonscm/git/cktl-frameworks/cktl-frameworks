
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;


/** Calcule une cotisation plafonnee en prenant en compte un seuil d'exoneration */
public abstract class CotisationPlafonneeExoneree extends CalculCotisationPlafonnee {
	private BigDecimal seuilExoneration;	  

	/** accesseur de lecture du montant &grave; partir duquel les charges sont calculees */
	public BigDecimal seuilExoneration() { return seuilExoneration; }

	public void effectuerCalcul(String codeTaux,String codeAssiette,BigDecimal remuneration) throws Exception {
		if (seuilExoneration() == null) {
			seuilExoneration = calculerSeuilExoneration();
		}
		// prendre en compte les périodes
		double exonerationDeBase = seuilExoneration().doubleValue();
		double exoneration = exonerationDeBase * (nbPeriodes());
		double remunPourCharge = remuneration.abs().doubleValue() - exoneration;
		if (remunPourCharge > 0) {
			BigDecimal assiette = new BigDecimal(remunPourCharge).setScale(2,BigDecimal.ROUND_HALF_DOWN);
			super.effectuerCalcul(codeTaux,codeAssiette,assiette);
			/*// 5/06/07 vérifier si le montant sécu est supérieur (il a été calculé au moment de l'ajout des rémunérations
			// et si c'est le cas, le mettre à la charge de cotisation
			double montantBss = preparation().payeBssmois().abs().doubleValue();
			if (montantBss > remunPourCharge) {
				preparation().setPayeBssmois(assiette);
			}*/
		}
	} 
	// méthodes protégées
	/** Calcule le seuil d'exoneration */
	protected abstract BigDecimal calculerSeuilExoneration() throws Exception;

}
