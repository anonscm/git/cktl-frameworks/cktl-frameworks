/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;


public class CalculCotisationCES extends CalculCotisationExoneree {
    private static String TAUX_HORAIRE_SMIC = "TXBRSMIC";
    private static String NB_HEURES_EXO_CES = "NBHEUCES";
    private double montantHoraireSmic;
    private double nbHeuresMini;

    /** Acesseurs */
    public double montantHoraireSmic() { return montantHoraireSmic; }
    public double tauxSmic() { return 1.0; }
    public double nbHeuresMini() { return nbHeuresMini; }

   
    // méthodes protégées
    /** Calcule le seuil d'exoneration pour un CES */
    protected BigDecimal calculerSeuilExoneration() throws Exception {
    		if (montantHoraireSmic == 0 && nbHeuresMini == 0) {
			preparerParametres();
		}
	    double tauxCalcule = montantHoraireSmic() * tauxSmic();
	    double calcul = tauxCalcule * nbHeuresMini();
	    return new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_DOWN);
	}
    /** prepare les param&egrave;tres necessaires au calcul */
    protected void preparerParametres()  throws Exception {
        EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),TAUX_HORAIRE_SMIC);
        if (code == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le code " + TAUX_HORAIRE_SMIC + " n'est pas defini");
        }
        EOPayeParam parametre = EOPayeParam.parametreValide(code);
        if (parametre == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le parametre associe au code "+ TAUX_HORAIRE_SMIC + " n'est pas defini");
        }
        montantHoraireSmic = parametre.pparMontant().doubleValue();
        if (montantHoraireSmic == 0) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le parametre associe au code "+ TAUX_HORAIRE_SMIC + " a une valeur nulle");
        }
        code = EOPayeCode.rechercherCode(editingContext(),NB_HEURES_EXO_CES);
        if (code == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le code " + NB_HEURES_EXO_CES + " n'est pas defini");
        }
        parametre = EOPayeParam.parametreValide(code);
        if (parametre == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le parametre associe au code "+ NB_HEURES_EXO_CES + " n'est pas defini");
        }
        nbHeuresMini = parametre.pparMontant().doubleValue();
        if (nbHeuresMini == 0) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le parametre associe au code "+ NB_HEURES_EXO_CES + " a une valeur nulle");
        }
    }
}
