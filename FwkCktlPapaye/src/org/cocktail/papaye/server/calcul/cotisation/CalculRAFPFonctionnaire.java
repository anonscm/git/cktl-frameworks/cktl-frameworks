/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.moteur.ModeleCalcul;

import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author christine
 *
 * Calcule la cotisation de retraite complementaire des fonctionnaires :<BR>
 * Calcule le traitement indiciaire cumule. Calcule le montant des primes cumule. L'assiette est egale a:<BR>
 * 		min(20% traitement brut cumule,primes cumulees)<BR>
 * Une prime correspond a une rubrique dont temRAFP = "O"
 */
public class CalculRAFPFonctionnaire extends ModeleCalcul {
	private final static String REMUNERATION = "REMUNBRU";
	private final String POURCENT_TRAITEMENT = "TXRAFPPC";
	private boolean LOG_INFO = true;
	private EOPayeCode codeCotisation;
	private double taux;
	private double tauxAssiette;
	private double tauxAbattementTraitement;

	/** accesseur de lecture du code de la cotisation */
	public EOPayeCode codeCotisation() { return codeCotisation; }
	/** accesseur d'ecriture du code de la cotisation */
	public void setCodeCotisation(EOPayeCode aCode) { codeCotisation = aCode; }
	/** accesseur de lecture du taux de cotisation */
	public double taux() { return taux; }
	/** accesseur d'ecriture du taux cotisation */
	public void setTaux(double aNum) { taux = aNum; }
	/** accesseur de lecture du taux de l'assiette de cotisation */
	public double tauxAssiette() { return tauxAssiette; }
	/** accesseur d'ecriture du taux de l'assiette de cotisation */
	public void setTauxAssiette(double aNum) { tauxAssiette = aNum; }

	/** Effectue le calcul de la cotisation  selon le r&egrave;glement <BR>
    Ajoute le resultat obtenu au dictionnaire des resultats.
    @param codeTaux code associe au taux de la part de cotisation
    @param codeAssiette code associe au taux de l'assiette
    @param baseAssiette base sur laquelle est effectue le calcul
	 */
	public void effectuerCalcul(String codeTaux,String codeAssiette,String codeCumulCotisation,String codeCumulRetraite) throws Exception {
		preparerParametres(codeTaux,codeAssiette);
		// prendre comme assiette la valeur absolue de l'assiette de calcul (cas des salaires trop payés)
		logInfo("calcul RAFP pour taux " + codeTaux);
		double baseAssiette = calculerAssiette(codeCumulRetraite);
		// maintenant calculer la cotisation
		double assiette = baseAssiette * (tauxAssiette / 100);
		logInfo("assiette de calcul " + assiette);
		double result = assiette * (taux / 100);
		// déduire du montant obtenu le cumul de cotisation précédent
		double cumulCotisation = 0, cumulAssiette = 0;
		NSArray elementsRAFP = rechercherElementsRAFP(codeTaux,codeCumulCotisation);
		logInfo("nb elements RAFP trouves " + elementsRAFP.count());
		java.util.Enumeration e = elementsRAFP.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeElement element = (EOPayeElement)e.nextElement();
			double value = 0;
			if (element.estUneDeduction()) {
				value = element.pelmAdeduire().doubleValue();
			} else if (element.estUneChargePatronale()) {
				value = element.pelmPatron().doubleValue();
			}
			cumulAssiette = cumulAssiette + element.pelmAssiette().doubleValue();
			logInfo("element du mois de "+ element.pelmMoisCode() + ", montant : " + value);
			cumulCotisation = cumulCotisation + value;
		}
		logInfo("cumul cotisation après application du taux "+ result + " cumul cotisation déduit " + cumulCotisation);
		logInfo("cumul assiette avant "+ cumulAssiette);
		result = result - cumulCotisation;
		// L'assiette affichée dans le BS n'est pas le cumul mais (montant * 100) / taux
		assiette = assiette - cumulAssiette;
		// arrondir les résultats au centième d'euro près et fournir deux chiffres
		// après la virgule
		if (result != 0) {
			ajouterCotisation(codeCotisation(),
					new BigDecimal(result).setScale(2,BigDecimal.ROUND_HALF_DOWN),
					new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_DOWN));
		}
	}

	/** prepare les param&egrave;tres utiles au calcul */
	protected void preparerParametres(String codeTaux,String codeAssiette) throws Exception {
		EOPayeParam parametre1 = parametrePourCode(codeTaux);
		if (parametre1 == null) {
			throw new Exception("Pour la classe : "+ nomClasse() + ", le taux " + codeTaux + " n'est pas defini");
		}
		setCodeCotisation(parametre1.code());
		EOPayeParam parametre2 = parametrePourCode(codeAssiette);
		if (parametre2 == null) {
			throw new Exception("Pour la classe : "+ nomClasse() + ", le taux de l'assiette" + codeAssiette + " n'est pas defini");
		}
		setTaux(parametre1.pparTaux().doubleValue());
		setTauxAssiette(parametre2.pparTaux().doubleValue());
		if (taux() == 0 || tauxAssiette() == 0) {
			throw new Exception("Pour la classe : "+ nomClasse() +
			", un des parametres de taux a une valeur nulle");
		}
		EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),POURCENT_TRAITEMENT);
		if (code == null) {
			throw new Exception("Pour la classe : "+ nomClasse() + ", le code " + POURCENT_TRAITEMENT + " n'est pas defini");
		}
		EOPayeParam parametre = EOPayeParam.parametreValide(code);
		if (parametre == null) {
			throw new Exception("Pour la classe : "+ nomClasse() + ", le parametre associe au code "+ POURCENT_TRAITEMENT + " n'est pas defini");
		}
		if (parametre.pparTaux() == null) {
			throw new Exception("Pour la classe : "+ nomClasse() + ", le taux associe au code "+ POURCENT_TRAITEMENT + " n'est pas defini");
		}
		tauxAbattementTraitement = parametre.pparTaux().doubleValue();
	}
	private double calculerAssiette(String codeCumulRetraite) {
		// pour calculer l'assiette, on calcule la rémunération brute et le montant total des primes soumises à RAFP pour l'année.
		// On prend comme assiette de cotisation le minimum entre le montant total des primes et un certain pourcentage de la rémunération brute.

		// calculer la rémunération brute et les primes sur les éléments courants
		BigDecimal remunBrute = new BigDecimal(0);
		BigDecimal remunPourRAFP = new BigDecimal(0);
		Enumeration e = elements().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPayeElement element = (EOPayeElement)e.nextElement();
			if (element.rubrique().estAPayer()) {
				if (element.code().pcodCode().equals(REMUNERATION) ||  element.code().pcodCode().equals(EOPayeCode.REMUN_BRUT)) {
					remunBrute = remunBrute.add(element.pelmApayer());
				}
				if (element.rubrique().rentreDansRAFP()) {
					remunPourRAFP = remunPourRAFP.add(element.pelmApayer());
				}
			}
		}
		logInfo("Remun brute avant cumul " + remunBrute);
		logInfo("Primes pour RAFP avant cumul " + remunPourRAFP); 
		// rechercher les contrats de titulaires pour ajouter à la rémun déductible, tous les éléments déductibles
		NSArray contratsTitulaires = EOPayeContrat.rechercherContratsTitulairePourIndividuEtAnnee(editingContext(),agent(),mois().moisAnnee());
		if (contratsTitulaires.count() > 0) {
			java.util.Enumeration e1 = contratsTitulaires.objectEnumerator();
			while (e1.hasMoreElements()) {
				EOPayeContrat contrat = (EOPayeContrat)e1.nextElement();
				NSArray elementsRemuneration = EOPayeElement.trouverElementsPourTypeContratEtAnnee(editingContext(),EOPayeElement.TYPE_REMUNERATION,contrat,mois().moisAnnee());
				logInfo("nb elements remuneration pour le contrat " + elementsRemuneration.count());
				java.util.Enumeration e2 = elementsRemuneration.objectEnumerator();
				while (e2.hasMoreElements()) {
					EOPayeElement element = (EOPayeElement)e2.nextElement();
					// Vérifier si cet élément n'a pas été pris en compte dans le calcul précédent (normalement non)
					if (elements().containsObject(element) == false) {
						if (element.code().pcodCode().equals(EOPayeCode.REMUN_BRUT) || element.code().pcodCode().equals(REMUNERATION)) {
							remunBrute = remunBrute.add(element.pelmApayer());
						}
						if (element.rubrique().rentreDansRAFP()) {
							remunPourRAFP = remunPourRAFP.add(element.pelmApayer());
						}
					}
				}
			}
		}
		logInfo("Remun Brute après cumul " + remunBrute);
		logInfo("Primes pour RAFP après cumul " + remunPourRAFP); 
		double plafond = (remunBrute.doubleValue() * tauxAbattementTraitement) / 100;
		logInfo("Plafond  " + plafond);
		// correction pour fonctionnaire en RA
		if (plafond == 0 || remunPourRAFP.doubleValue() <= plafond) {
			return remunPourRAFP.doubleValue();
		} else {
			return plafond;
		}
	}
	private NSArray rechercherElementsRAFP(String codeTaux,String codeCumulCotisation) {
		// On recherche tous les éléments associés à un taux pour un individu donné et une année donnée
		// le code taux est utilisé pour les rubriques normales, le code cumul pour les rappels
		NSMutableArray args = new NSMutableArray(codeTaux);
		args.addObject(codeCumulCotisation);
		Integer debutAnnee = new Integer(mois().moisAnnee().toString() + "01");
		Integer finAnnee = new Integer(mois().moisAnnee().toString() + "12");
		args.addObject(debutAnnee);
		args.addObject(finAnnee);
		args.addObject(contrat().individu());
		String stringQualifier = "(code.pcodCode = %@ OR code.pcodCode = %@) AND pelmMoisCode >= %@ AND pelmMoisCode <= %@";
		// Rechercher les éléments liés aux historiques puis validations puis préparations
		NSMutableArray result = new NSMutableArray();
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier + " AND historique.contrat.individu = %@", args);
		EOFetchSpecification fs = new EOFetchSpecification("PayeElement",qualifier,null);
		result.addObjectsFromArray(editingContext().objectsWithFetchSpecification(fs));
		qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier + " AND validation.contrat.individu = %@", args);
		fs = new EOFetchSpecification("PayeElement",qualifier,null);
		result.addObjectsFromArray(editingContext().objectsWithFetchSpecification(fs));
		qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier + " AND preparation.contrat.individu = %@", args);
		fs = new EOFetchSpecification("PayeElement",qualifier,null);
		result.addObjectsFromArray(editingContext().objectsWithFetchSpecification(fs));
		return result;
	}
	private void logInfo(String aString) {
		if (LOG_INFO) {
			System.out.println(aString);
		}
	}
}
