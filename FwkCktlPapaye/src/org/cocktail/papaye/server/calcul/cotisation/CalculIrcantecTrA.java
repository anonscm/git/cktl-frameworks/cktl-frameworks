/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;



/** Cette classe modelise tous les calculs Ircantec Tranche A qui sont bases sur la r&egrave;gle de calcul suivante :<BR>
Calcul : taux x Min((baseAssiette x tauxAssiette) %, plafond Mensuel SS)<BR>
Elle lance une exception si un des param&egrave;tres utilise pour le calcul a une valeur nulle. Le message de l'exception indique la classe et la valeur responsables de l'exception
*/

public class CalculIrcantecTrA extends CalculCotisationPlafonnee {
    /** Effectue le calcul de la cotisation en appliquant la r&egrave;gle taux x Min((baseAssiette x tauxAssiette) %, plafond Mensuel SS)<BR>
    Ajoute le resultat obtenu au dictionnaire des resultats.
    @param codeTaux code associe au taux de la part de cotisation
    @param codeAssiette code associe au taux de l'assiette
    @param baseAssiette base sur laquelle est effectue le calcul
    */
    public void effectuerCalcul(String codeTaux,String codeAssiette,BigDecimal baseAssiette,BigDecimal SFT) throws Exception {
        super.effectuerCalcul(codeTaux,codeAssiette,baseAssiette,SFT);
    }
}
