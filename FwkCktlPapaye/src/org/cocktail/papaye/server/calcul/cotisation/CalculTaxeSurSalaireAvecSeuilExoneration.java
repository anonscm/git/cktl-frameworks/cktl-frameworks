/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;

import org.cocktail.application.serveur.eof.EOOrgan;
import org.cocktail.application.serveur.eof.EOOrganProrata;
import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeContratLbud;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class CalculTaxeSurSalaireAvecSeuilExoneration extends CalculTaxeSurSalaire {
	private BigDecimal seuilExoneration;	  
	
	// méthodes statiques
	public static boolean organCotiseTVA(EOEditingContext editingContext,EOOrgan organ) {
		NSMutableArray args = new NSMutableArray(new Integer(0));
		args.addObject(organ);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("orpPriorite = %@ AND organ = %@",args);
		EOFetchSpecification fs = new EOFetchSpecification(EOOrganProrata.ENTITY_NAME,qualifier,null);
		fs.setPrefetchingRelationshipKeyPaths(new NSArray("tauxProrata"));
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		try {
			EOOrganProrata prorata = (EOOrganProrata)results.objectAtIndex(0);
			return prorata.tauxProrata().tapTaux().doubleValue() == 100.00;
		} catch (Exception e) {
			return false;
		}
	}
	/** accesseur de lecture du montant &grave; partir duquel les charges sont calculees */
	public BigDecimal seuilExoneration() { return seuilExoneration; }

	public void effectuerCalcul(BigDecimal remuneration) throws Exception {
        boolean soumisTVA = true;
        NSArray lbuds = contrat().contratLbuds();
        if (lbuds.count() > 0) {
            // vérifier si la ligne budgétaire est soumise à TVA, en quel cas pas de taxe sur salaire
            EOPayeContratLbud contratLbuds = (EOPayeContratLbud)contrat().contratLbuds().objectAtIndex(0);
            soumisTVA = organCotiseTVA(editingContext(),contratLbuds.organ());
        } else {
                // vérifier si la structure est soumise à TVA, en quel cas pas de taxe sur salaire
                if (contrat().structure().temSoumisTVA().equals(Constantes.FAUX)) {
                    soumisTVA = false;
                }
        }
        if (!soumisTVA) {
        		if (seuilExoneration() == null) {
        			seuilExoneration = calculerSeuilExoneration();
        		}
        		// prendre en compte les périodes
        		double exonerationDeBase = seuilExoneration().doubleValue();
    			double exoneration = exonerationDeBase * (nbPeriodes());
    			double remunPourCharge = remuneration.abs().doubleValue() - exoneration;
    			if (remunPourCharge > 0) {
    				BigDecimal assiette = new BigDecimal(remunPourCharge).setScale(2,BigDecimal.ROUND_HALF_DOWN);
    				super.effectuerCalcul(assiette);
    			}
        }
    }
    	protected abstract BigDecimal calculerSeuilExoneration() throws Exception;
}
