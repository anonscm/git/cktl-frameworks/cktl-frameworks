/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement;


/** Cette classe modelise tous les calculs de cotisations des fonctionnaires. Elle
apporte une methode de calcul de l'assiette de cotisation : Traitement Brut + NBI
*/
public class CalculCotisationFonctionnaire extends CalculCotisationSimple {
    private final static String REMUN_NBI = "REMUNNBI";
    private final static String REMUNERATION = "REMUNBRU";
    private final static String REMUN_GMR = "REMUNGMR";

    /** Calcule l'assiette de cotisation d'un fonctionnaire */
    public BigDecimal calculerAssiette() throws Exception {
        // parmi les éléments, sélectionner le traitement brut la NBI
        BigDecimal assiette = new BigDecimal(0.0);
        Enumeration e = elements().objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeElement element = (EOPayeElement)e.nextElement();
            if (element.code().pcodCode().equals(REMUNERATION) || element.code().pcodCode().equals(REMUN_NBI) ||
                element.code().pcodCode().equals(EOPayeCode.REMUN_BRUT) || element.code().pcodCode().equals(REMUN_GMR))  {
                // trouvé
                assiette = assiette.add(element.pelmApayer()); // on prend la valeur absolue au cas où il s'agisse d'un trop payé
            }
        }
        return assiette.abs();
    }
    
}
