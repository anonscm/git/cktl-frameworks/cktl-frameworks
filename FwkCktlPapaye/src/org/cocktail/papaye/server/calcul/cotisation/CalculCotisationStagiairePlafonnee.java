/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;


/** Cette classe modelise tous les calculs de cotisations des stagiaires avocat
qui sont basees sur un montant plafonne.*/
public class CalculCotisationStagiairePlafonnee extends CotisationPlafonneeExoneree {
	private static String PLAFOND_HORAIRE_SS = "PLAHOSS";
	private static String NB_HEURES_LEGALES = "NBHEULE1";
    private static String TAUX_ABATTTEMENT = "TXABASTA";

    protected BigDecimal calculerSeuilExoneration() throws Exception {
        EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),TAUX_ABATTTEMENT);
        if (code == null) {
            throw new Exception("Pour la classe " + nomClasse() + ", le code " + TAUX_ABATTTEMENT + " n'est pas defini");
        }
        EOPayeParam parametre  = EOPayeParam.parametreValide(code);
        if (parametre == null) {
            throw new Exception("Pour la classe " + nomClasse() + ", le parametre associe au " + TAUX_ABATTTEMENT + " n'est pas defini");
        }
        if (parametre.pparTaux() == null) {
            throw new Exception("Pour la classe " + nomClasse() + ", le taux  a une valeur nulle");
        }
        double taux = parametre.pparTaux().doubleValue() ;
        code = EOPayeCode.rechercherCode(editingContext(),PLAFOND_HORAIRE_SS);
        if (code == null) {
            throw new Exception("Pour la classe " + nomClasse() + ", le code " + PLAFOND_HORAIRE_SS + " n'est pas defini");
        }
        parametre  = EOPayeParam.parametreValide(code);
        if (parametre == null) {
            throw new Exception("Pour la classe " + nomClasse() + ", le parametre associe au " + PLAFOND_HORAIRE_SS + " n'est pas defini");
        }
        if (parametre.pparMontant() == null) {
            throw new Exception("Pour la classe " + nomClasse() + ", le montant  a une valeur nulle");
        }
        code = EOPayeCode.rechercherCode(editingContext(),NB_HEURES_LEGALES);
        if (code == null) {
            throw new Exception("Pour la classe " + nomClasse() + ", le code " + NB_HEURES_LEGALES + " n'est pas defini");
        }
        EOPayeParam parametre1  = EOPayeParam.parametreValide(code);
        if (parametre1 == null) {
            throw new Exception("Pour la classe " + nomClasse() + ", le parametre associe au " + NB_HEURES_LEGALES + " n'est pas defini");
        }
        if (parametre1.pparMontant() == null) {
            throw new Exception("Pour la classe " + nomClasse() + ", la valeur du parametre " +  NB_HEURES_LEGALES + " est nulle");
        }
        double calcul = (taux * parametre.pparMontant().doubleValue()) / 100;
        // appliquer le prorata du nombre d'heures réellement travaillées
        if (contrat().nbHeuresContrat() != null && contrat().nbHeuresContrat().doubleValue() > 0) {
        	double nbHeures = contrat().nbHeuresContrat().doubleValue();
        	calcul = calcul * nbHeures;
        } else {
        	calcul = calcul * parametre1.pparMontant().doubleValue();
        }
        return new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_DOWN);
    }
    
}
