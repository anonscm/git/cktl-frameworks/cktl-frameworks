/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;


import java.math.BigDecimal;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.metier.grhum.EOStructure;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;



/** Effectue le calcul de la cotisation Assedic pour les etablissements soumis aux assedic<BR>
Calcul : taux x (baseAssiette x tauxAssiette) %
 */
public class CalculCotisationAssedic extends CalculCotisationSimple {
	public static String PLAFOND_MENSUEL = "PLAFMSSS";
	private double plafond;

	public void effectuerCalcul(String codeTaux,String codeAssiette,BigDecimal baseAssiette) throws Exception {
		try {
			if (plafond == 0)
				evaluerPlafond();

			BigDecimal assiette = preparation().payeBssmois();
			// verifier si on est-au dessus de 4 fois le plafond SS
			double plafondReel = plafond;
			if (nbPeriodes() > 1)
				plafondReel = plafondReel * nbPeriodes();

			if (assiette.doubleValue() > plafondReel)
				assiette = new BigDecimal(plafondReel).setScale(2,BigDecimal.ROUND_HALF_UP);

			EOStructure etablissement = EOStructure.rechercherEtablissement(editingContext());
			if (etablissement != null && etablissement.cotiseAuxAssedic()) {
				if (agent().personnel().temTitulaire().equals(Constantes.FAUX))
					super.effectuerCalcul(codeTaux,codeAssiette,assiette) ;

			}
		} catch (Exception e) { throw e; }
	}

	private void evaluerPlafond() throws Exception {
		EOPayeParam parametre = parametrePourCode(PLAFOND_MENSUEL);
		if (parametre == null) {
			throw new Exception ("Dans la classe "+ nomClasse() + ", le plafond SS Mensuel  n'est pas defini");
		}
		if (parametre.pparMontant() == null) {
			throw new Exception("Dans la classe "+ nomClasse() + ", la valeur du plafond SS Mensuel n'est pas definie");
		}
		plafond = parametre.pparMontant().doubleValue() * 4;   
	}        

}
