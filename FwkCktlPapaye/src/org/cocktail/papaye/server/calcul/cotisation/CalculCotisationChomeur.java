/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayePeriode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique;
import org.cocktail.papaye.server.moteur.ModeleCalcul;

import com.webobjects.foundation.NSArray;

/** Cette classe modelise le calcul general d'une cotisation pour un ch&ocirc;meur. */
public class CalculCotisationChomeur extends ModeleCalcul {
    private static String MONTANT_POUR_SEUIL = "MTEXOCHO";
    private EOPayeCode codeCotisation;
    private double seuilExoneration;
    private double taux;
	private double tauxTotalCSG;
    private double tauxAssiette;
	private int nbJoursTravailles;

    /** accesseur de lecture du code de la cotisation */
    public EOPayeCode codeCotisation() { return codeCotisation; }
    /** accesseur d'ecriture du code de la cotisation */
    public void setCodeCotisation(EOPayeCode aCode) { codeCotisation = aCode; }
    /** accesseur de lecture du taux de cotisation */
    public double taux() { return taux; }
    /** accesseur d'ecriture du taux cotisation */
    public void setTaux(double aNum) { taux = aNum; }
    /** accesseur de lecture du taux de l'assiette de cotisation */
    public double tauxAssiette() { return tauxAssiette; }
    /** accesseur d'ecriture du taux de l'assiette de cotisation */
    public void setTauxAssiette(double aNum) { tauxAssiette = aNum; }
	/** accesseur de lecture du taux total de CSG */
    public double tauxTotalCSG() { return tauxTotalCSG; }
    /** accesseur de lecture du seuil d'exoneration */
    public double seuilExoneration() { return seuilExoneration; }
	/** accesseur de lecture du seuil d'exoneration */
    public int nbJoursTravailles() { return nbJoursTravailles; }

	/** Effectue le calcul de la cotisation en appliquant la r&egrave;gle taux x (baseAssiette x tauxAssiette) % <BR>
        Ce calcul est declenche lorsque la remuneration est au-dessus du seuil d'exoneration
        @param baseAssiette base sur laquelle est effectue le calcul
        Ajoute le resultat obtenu au dictionnaire des resultats.
	*/
	public void effectuerCalcul(BigDecimal baseAssiette) throws Exception {
		// prendre comme assiette la valeur absolue de l'assiette de calcul
        BigDecimal baseAssietteAbsolue = baseAssiette.abs();
        double assiette = baseAssietteAbsolue.doubleValue() * (tauxAssiette / 100);
        double result = assiette * (taux / 100);
		// arrondir les résultats au centième d'euro près et fournir deux chiffres après la virgule
		ajouterCotisation(codeCotisation(),
						  new BigDecimal(result).setScale(2,BigDecimal.ROUND_HALF_UP),
						  new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_DOWN));
		
	}
    /** Charge les param&egrave;tres et les verifie
        @param codeTaux code associe au taux de la part de cotisation
        @param codeAssiette code associe au taux de l'assiette
        @param baseAssiette base sur laquelle est effectue le calcul
        */
    public void preparerParametres(String codeTaux,String codeAssiette,BigDecimal baseAssiette) throws Exception {
		preparerParametres(codeTaux,codeAssiette);
		nbJoursTravailles = 0;
        if (baseAssiette == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", la base de l'assiette" + codeAssiette + " n'est pas definie");
        }
        // récupérer le nombre de jours de l'allocation chômage dans la période
		// période courante à rémunérer
		if (nbPeriodes() == 1 && periode().pperNbJour() != null && periode().pperNbJour().intValue() > 0) {
			Number jours = periode().pperNbJour();
			if (jours == null) {
				throw new Exception("Pour la classe : "+ nomClasse() + ", le nombre de jours d'allocation chomage n'est pas defini dans la periode");
			}
			nbJoursTravailles = jours.intValue();
			if (nbJoursTravailles == 0) {
				throw new Exception("Pour la classe : "+ nomClasse() + ", le nombre de jours d'allocation chomage a une valeur nulle dans la periode");
			}
		} else {
			NSArray periodesDuContrat = EOPayePeriode.rechercherPeriodesPourContratEtMois(editingContext(),contrat(),periode().mois());
			Enumeration e = periodesDuContrat.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPayePeriode periodeCourante = (EOPayePeriode)e.nextElement();
				Number jours = periodeCourante.pperNbJour();
				if (jours == null && periodeCourante != periode()) {
					throw new Exception("Pour la classe : "+ nomClasse() + ", le nombre de jours d'allocation chomage n'est pas defini dans une des periodes");
				}
				int nbJours = jours.intValue();
				if (nbJours == 0 && periodeCourante != periode()) {
					throw new Exception("Pour la classe : "+ nomClasse() + ", le nombre de jours d'allocation chomage a une valeur nulle dans une des periodes");
				} else {
					nbJoursTravailles += nbJours;
				}
			}
		}
    }
    /** prepare les param&egrave;tres utiles au calcul */
    public void preparerParametres(String codeTaux,String codeAssiette) throws Exception {
        // vérifier que le taux d'IR a été fourni en paramètre ainsi que l'indice minimum
        EOPayeParam parametre = parametrePourCode(MONTANT_POUR_SEUIL);
        if (parametre == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + " ,le parametre " +
                                MONTANT_POUR_SEUIL +
                                " n'est pas defini");
        }
        if (parametre.pparMontant() == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + " ,la valeur du parametre "+ MONTANT_POUR_SEUIL +" n'est pas definie");
        }
        seuilExoneration = parametre.pparMontant().doubleValue();
        
        parametre = parametrePourCode(codeTaux);
        codeCotisation = parametre.code();
        if (parametre == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + " ,le parametre " +
                                codeTaux +
                                " n'est pas defini");
        }
        if (parametre.pparTaux() == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + " ,la valeur du parametre "+ codeTaux +" n'est pas definie");
        }
        taux = parametre.pparTaux().doubleValue();
        parametre = parametrePourCode(codeAssiette);
        if (parametre == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + " ,le parametre " +
                                codeAssiette +
                                " n'est pas defini");
        }
        if (parametre.pparTaux() == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + " ,la valeur du parametre "+ codeAssiette +" n'est pas definie");
        }
        tauxAssiette = parametre.pparTaux().doubleValue();
    }

    // calcule le montant brut - la somme des cotisations déjà calculées
    public double calculerNet() {
        BigDecimal net = new BigDecimal(0);
        Enumeration e = elements().objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeElement element = (EOPayeElement)e.nextElement();
            if (element.estUneRemuneration()) {
                    net = net.add(element.pelmApayer());
            } else if (element.estUneDeduction() && element.rubrique().prubLibelle().indexOf("CSG") == -1) {
                net = net.subtract(element.pelmAdeduire());
            }
        }
        return net.abs().doubleValue(); // au cas où la valeur est négative (trop perçu de salaire)
    }

    public double calculerResultTotalCSG(double baseAssiette) throws Exception {
        NSArray rubriques = contrat().statut().rubriquesCotisationSalariale();
        java.util.Enumeration e = rubriques.objectEnumerator();
        double result = 0; tauxTotalCSG = 0;
        while (e.hasMoreElements()) {
            EOPayeRubrique rubrique = (EOPayeRubrique)e.nextElement();
            if (rubrique.prubLibelle().indexOf("CSG") >= 0) {
                // rubrique de CSG
                NSArray codes = rubrique.codes();
                // rechercher les paramètres associés à ces codes pour faire le calcul
                java.util.Enumeration e1 = codes.objectEnumerator();
                double taux = 0, tauxAssiette = 0;
                while (e1.hasMoreElements()) {
                    EOPayeCode code = (EOPayeCode)e1.nextElement();
                    if (code.pcodCode().startsWith("COT") == false) {
                        EOPayeParam parametre = EOPayeParam.parametreValide(code);
                        if (parametre == null) {
                            throw new Exception("Le parametre associe au code " + code.pcodCode() + " a une valeur nulle");
                        } else if (parametre.pparTaux() == null) {
                        throw new Exception("Le taux du parametre associe au code " + code.pcodCode() + " n'est pas defini");
                        }
                        if (code.pcodCode().startsWith("TX")) {
                        taux = parametre.pparTaux().doubleValue();
						tauxTotalCSG += taux;
                        } else if (code.pcodCode().startsWith("AS")) {
                            tauxAssiette = parametre.pparTaux().doubleValue();
                        }
                    }
                }
                result += (baseAssiette * (tauxAssiette / 100)) * (taux / 100);
            }
        }
		if (tauxTotalCSG == 0) {
			throw new Exception("Le taux total de la CSG a une valeur nulle");
		}
        return result;
    }
}

    