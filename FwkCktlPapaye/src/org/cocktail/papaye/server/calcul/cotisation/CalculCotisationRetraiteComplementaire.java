/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCumul;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.moteur.ModeleCalcul;


/** Cette classe modelise tous les calculs de cotisations de retraite complementaire
des artistes et intermittents qui sont basees sur un montant plafonne standard */

public class CalculCotisationRetraiteComplementaire extends ModeleCalcul {
    private static String PLAFOND_SS_ANNUEL = "PLAFANSS";
    private double taux1;
    private double tauxAssiette1;
    private double taux2;
    private double tauxAssiette2;
    private double plafondMin;
    private double plafondMax;
    private String codeCotisationTranche1;
    private String codeCotisationTranche2;
    
    /** Accesseur */
    public double tauxAbattement() { return contrat().abattement().doubleValue(); }
    /** Effectue un calcul de cotisation sur un salaire abattu
        @param remuneration montant de la r&acute;muneration */
    public void effectuerCalcul(BigDecimal remuneration) throws Exception {
        
        double assietteCotisation = remuneration.doubleValue() * (1 - (tauxAbattement() / 100));
        EOPayeCumul cumul = EOPayeCumul.rechercherDernierCumul(editingContext(),mois(),codeCumul().pcodCode(),agent());
        double assietteCumulPrecedent = 0;
        if (cumul != null) {
        		assietteCumulPrecedent = cumul.pcumBase().doubleValue();
        }
        if (assietteCotisation >= 0) {
        		// cas des bs positifs
	        if (assietteCumulPrecedent + assietteCotisation <= plafondMin) {		// on cotise en tranche 1
	            effectuerCalcul(codeCotisationTranche1,taux1, tauxAssiette1,assietteCotisation);
	        } else if (assietteCumulPrecedent + assietteCotisation <= plafondMax) {	// on cotise en tranche 2
				effectuerCalcul(codeCotisationTranche2,taux2, tauxAssiette2,assietteCotisation);
	        }
        } else {
        		// cas des bs négatifs (certainement rares pour les retraites complémentaires
        		if (assietteCumulPrecedent <= plafondMin) {
        			// on reste encore en-dessous du plafond minimum, on cotise toujours en tranche 1
        			effectuerCalcul(codeCotisationTranche1,taux1, tauxAssiette1,assietteCotisation);
        		} else {
        			// assiette du cumul précédent <= plafondMax
        			if (assietteCumulPrecedent + assietteCotisation <= plafondMin) {
        				// on repasse en-dessous du plafond minimum
        				double assietteTranche2 = plafondMin - assietteCumulPrecedent;			// doit donner une valeur négative
        				double assietteTranche1 = assietteCotisation - assietteTranche2;		// doit donner une valeur négative
        				effectuerCalcul(codeCotisationTranche2,taux2,tauxAssiette2,assietteTranche2);
        				effectuerCalcul(codeCotisationTranche1,taux1,tauxAssiette1,assietteTranche1);
        			} else if (assietteCumulPrecedent + assietteCotisation <= plafondMax) {
        				effectuerCalcul(codeCotisationTranche2,taux2,tauxAssiette2,assietteCotisation);
        			}
        		}
        }
    }
    /** prepare les param&egrave;tres necessaires au calcul
        @param codeTaux1 taux pour la tranche 1
        @param codeAssiette1 plafond superieur de la tranche 1
        @param codeTaux2 taux pour la tranche 2
        @param codeAssiette2 plafond superieur de la tranche 2
        @param coefficientMax coefficient multiplicateur pour calculer le plafond maximum de la tranche 2
    */
    public void preparerParametres(String codeTaux1,String codeAssiette1,String codeTaux2,String codeAssiette2, int coefficientMax)  throws Exception {
        EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),PLAFOND_SS_ANNUEL);
        if (code == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le code " + PLAFOND_SS_ANNUEL + " n'est pas defini");
        }
        EOPayeParam parametre = EOPayeParam.parametreValide(code);
        if (parametre == null) {
            throw new Exception("Pour la classe : "+ nomClasse() +
                                ", le parametre associe au code "+ PLAFOND_SS_ANNUEL + " n'est pas defini");
        }
        plafondMin = parametre.pparMontant().doubleValue();
        if (plafondMin == 0) {
            throw new Exception("Pour la classe : "+ nomClasse() +
                                ", le parametre associe au code "+ PLAFOND_SS_ANNUEL + " une valeur nulle");
        }
        plafondMax = plafondMin * coefficientMax;
        codeCotisationTranche1 = codeTaux1;
        codeCotisationTranche2 = codeTaux2;
        
        EOPayeParam parametre1 = parametrePourCode(codeTaux1);
        if (parametre1 == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le taux " + codeTaux1 + " n'est pas defini");
        }
        EOPayeParam parametre2 = parametrePourCode(codeAssiette1);
        if (parametre2 == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le taux de l'assiette" + codeAssiette1 + " n'est pas defini");
        }
        taux1 = parametre1.pparTaux().doubleValue();
        tauxAssiette1 = parametre2.pparTaux().doubleValue();

        if (taux1 == 0 || tauxAssiette1 == 0) {
            throw new Exception("Pour la classe : "+ nomClasse() +
                                ", un des parametres des taux 1 a une valeur nulle");
        }
        parametre1 = parametrePourCode(codeTaux2);
        if (parametre1 == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le taux " + codeTaux2 + " n'est pas defini");
        }
        parametre2 = parametrePourCode(codeAssiette2);
        if (parametre2 == null) {
            throw new Exception("Pour la classe : "+ nomClasse() + ", le taux de l'assiette" + codeAssiette2 + " n'est pas defini");
        }
        taux2 = parametre1.pparTaux().doubleValue();
        tauxAssiette2 = parametre2.pparTaux().doubleValue();
        if (taux2 == 0 || tauxAssiette2 == 0) {
            throw new Exception("Pour la classe : "+ nomClasse() +
                                ", un des parametres des taux 1 a une valeur nulle");
        }
    }
    private void effectuerCalcul(String codeCotisation,double taux,double tauxAssiette,double montant) {
        // prendre comme assiette la valeur absolue de l'assiette de calcul (cas des salaires trop payés)
        double assiette =0;
        if (montant > 0) {
            assiette = montant;
        } else {
            assiette = -montant ;
        }
        assiette = assiette * (tauxAssiette / 100);
        double result = assiette * (taux / 100);
        // arrondir les résultats au centième d'euro près et fournir deux chiffres
        // après la virgule
        EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),codeCotisation);
        ajouterCotisation(code,
                          new BigDecimal(result).setScale(2,BigDecimal.ROUND_HALF_UP),
                          new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_DOWN),code);
    }
    
}
