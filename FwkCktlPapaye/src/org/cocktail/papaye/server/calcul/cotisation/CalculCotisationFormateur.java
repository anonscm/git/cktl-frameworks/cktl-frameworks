/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;

import com.webobjects.foundation.NSMutableArray;

/** Cette classe modelise tous les calculs de cotisations des formateurs occasionnels
qui sont basees sur une assiette forfaitaire fonction du montant journalier paye.*/
public class CalculCotisationFormateur extends CalculCotisationSimple {
	private static String REMUN_LIMIT = "REMAXFO";
	private static String ASSIETTE_COTISATION = "ASCOTFO";
	private static int NB_REMUN = 8;
	private NSMutableArray tableauRemunerations;
	private NSMutableArray tableauAssiettes;

	/** Effectue un calcul de cotisation sur un assiette forfaitaire */
	public void effectuerCalcul(String taux,String tauxAssiette,BigDecimal assiette) throws Exception {

		if (tableauRemunerations == null && tableauAssiettes == null)
			chargerTableaux();

		super.effectuerCalcul(taux, tauxAssiette,calculerAssietteForfaitaire(assiette));

	}

	private void chargerTableaux() throws Exception {
		tableauRemunerations = new NSMutableArray(NB_REMUN);
		// vérifier tous les paramètres et stocker les montants limites dans un tableau
		for (int i = 0; i < NB_REMUN ; i++) {
			String code = new String(REMUN_LIMIT + (i + 1));
			EOPayeParam parametre = parametrePourCode(code);
			if (parametre == null) {
				throw new Exception ("Dans la classe CalculCotisationFormateur, le  parametre " + code + " n'est pas defini");
			} else {
				if (parametre.pparMontant() == null) {
					throw new Exception ("Dans la classe CalculCotisationFormateur, la valeur du taux  " + code + " n'est pas definie");
				} else {
					tableauRemunerations.addObject(parametre.pparMontant());
				}
			}
		}
		tableauAssiettes = new NSMutableArray(NB_REMUN+1);
		// vérifier tous les paramètres et stocker les montants des assiettes dans un tableau
		for (int i = 0; i < NB_REMUN ; i++) {
			String code = new String(ASSIETTE_COTISATION + (i + 1));
			EOPayeParam parametre = parametrePourCode(code);
			if (parametre == null) {
				throw new Exception ("Dans la classe CalculCotisationFormateur, le  parametre " + code + " n'est pas defini");
			} else {
				if (parametre.pparMontant() == null) {
					throw new Exception ("Dans la classe CalculCotisationFormateur, la valeur du montant de " + code + " n'est pas definie");
				} else {
					tableauAssiettes.addObject(parametre.pparMontant());
				}
			}
		}
	}
	private BigDecimal calculerAssietteForfaitaire(BigDecimal montant) throws Exception {
		BigDecimal assietteGlobale = new BigDecimal(0);
		if (contrat().nbJoursContrat() == null) {
			throw new Exception("Pour la classe : "+ nomClasse() + ", le nombre de jours travailles n'est pas defini dans le contrat de" + contrat().individu().identite());
		}
//		int nbJours = contrat().nbJoursContrat().intValue();
		for (Enumeration<EOPayeElement> e = elements().objectEnumerator();e.hasMoreElements();) {
			EOPayeElement element = e.nextElement();
			if (element.pelmType().equals(EOPayeElement.TYPE_REMUNERATION)) {				
				int i = 0;
				for ( i = 0;i < tableauRemunerations.count();i++) {
					if (element.pelmApayer().doubleValue() < ((BigDecimal)tableauRemunerations.objectAtIndex(i)).doubleValue())
						break;
				}
				// le tableau des assiettes contient une valeur de plus que celui des rémunérations
				if (i < tableauRemunerations.count()) {
					
					BigDecimal remun = (BigDecimal)tableauAssiettes.objectAtIndex(i);
					if (element.pelmApayer().floatValue() < remun.floatValue() && i == 0)
						assietteGlobale = assietteGlobale.add(element.pelmApayer());
					else
						assietteGlobale = assietteGlobale.add(remun);
				}
				else
					assietteGlobale = assietteGlobale.add(element.pelmApayer());
			}		    	
		}
		return assietteGlobale.setScale(2,BigDecimal.ROUND_HALF_DOWN);
	}
}
