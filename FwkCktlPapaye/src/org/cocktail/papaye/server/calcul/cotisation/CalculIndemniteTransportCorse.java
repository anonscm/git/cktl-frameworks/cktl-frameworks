/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.metier.grhum.EOEnfant;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCumul;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.moteur.ModeleCalcul;

import com.webobjects.foundation.NSArray;


/** Effectue le calcul de l'indemnite compensatoires pour frais de transport en Corse. Prend en compte le nombre d'enfants.<BR>
*/
public class CalculIndemniteTransportCorse extends ModeleCalcul {
    private final static String INDEMNITE = "REMUNITC";
    private final static String MONTANT_ENFANT = "MOINDTRE";
    private final static String NOMBRE_HEURES_LEGALES = "NBHEULE1";
    private double montantEnfant;
    private int nbEcheances = 2;    // nombre d'√©ch√©ances de paiement
    private double nbHeuresLegales;
    
    public void effectuerCalcul(double montantBase) throws Exception {
        try {
            // v√©rifier si on est en Mars ou en Octobre
            int numeroMois = mois().numeroDuMois();
            if (numeroMois == 3 || numeroMois == 10) {
                // d√©terminer le nombre d'enfants ouvrant droit √† prime
                int nbEnfants = nbEnfantsACharge();
                // Effectuer le calcul
                double calcul = (montantBase + (montantEnfant * nbEnfants)) / nbEcheances;
				BigDecimal remunTempsPlein = new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_DOWN);
                EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),INDEMNITE);
                // si travail √† temps partiel avec quotit√© < 50% alors recalculer la prime et v√©rifier que la
                // totalit√© pour les agents ne d√©passent pas le plafond
                if (contrat().temTempsPlein() != null &&  contrat().temTempsPlein().equals(Constantes.FAUX)) {
                    // v√©rifier si la quotit√© est connue, sinon v√©rifier si il y a un nombre d'heures connu
                    double quotite = 0;
                    double nbHeures = 0;
                    double calcul1 = 0;
                    if (preparation().payeNbheure() != null) {
                        nbHeures = preparation().payeNbheure().doubleValue();
                    }
                    if (contrat().numQuotRecrutement() != null) {
                        quotite = contrat().numQuotRecrutement().doubleValue();
                    } else if (nbHeures > 0) {
                        quotite = nbHeures / nbHeuresLegales;
                    }
                    if (quotite == 0) {
                        throw new Exception("Pour la classe  " + nomClasse() + ": impossible de determiner la quotite");
                    }
                    if (quotite < 50) { 
                        // si on ne travaille pas √† temps plein, calculer le montant en fonction du nombre d'heures ou de la quotite
                        if (nbHeures > 0) {
                            calcul1 = (calcul * nbHeures) / (nbHeuresLegales / 2);
                        } else {
                            calcul1 = (calcul * quotite) / 50;   // il faut prendre par rapport √† une quotit√© 50
                        }
                        // v√©rifier qu'on n'a pas d'autres contrats concern√©s (i.e v√©rifier le cumul du mois) et qu'on ne d√©passe pas le montant
                        // autoris√© qui est le montant max. de la prime
                        NSArray cumuls = EOPayeCumul.rechercherDerniersCumuls(editingContext(),mois(),code.pcodCode(), agent());
                        Enumeration e = cumuls.objectEnumerator();
                        while (e.hasMoreElements()) {
                        	EOPayeCumul cumul = (EOPayeCumul)e.nextElement();
	                        if (cumul !=  null && cumul.mois() == mois()) {
	                            double montant = cumul.pcumMontant().doubleValue() + cumul.pcumRegul().doubleValue();
	                            if (montant + calcul1 > calcul) {
	                                calcul1 = calcul - montant;
	                            }
	                        }
                        }
                        calcul = calcul1;
                    }
                }
                BigDecimal remun = new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_DOWN);
				if (contrat().utiliserPlafondReduit() && contrat().peutAvoirPlafondReduit()) {
					ajouterRemunerationMontant(code,remun,remunTempsPlein,new BigDecimal(0));
				} else {
					ajouterRemunerationMontant(code,remun,remun);
				}	
            }
        } catch (Exception e) { throw e; }
    }
    protected void preparerParametres() throws Exception {
        EOPayeParam parametre1 = parametrePourCode(MONTANT_ENFANT);
        if (parametre1 == null) {
            throw new Exception("Pour la classe " + nomClasse() + " le parametre montant n'est pas defini");
        }
        if (parametre1.pparMontant() == null) {
            throw new Exception("Pour la classe  " + nomClasse() + " la valeur du parametre montant n'est pas definie");
        }
        montantEnfant = parametre1.pparMontant().doubleValue();
        
        parametre1 = parametrePourCode(NOMBRE_HEURES_LEGALES);
        if (parametre1 == null) {
            throw new Exception("Pour la classe " + nomClasse() + " le parametre Nombre d'heures l√©gales n'est pas defini");
        }
        if (parametre1.pparMontant() == null) {
            throw new Exception("Pour la classe  " + nomClasse() + " la valeur du parametre Nombre d'heures l√©gales n'est pas definie");
        }
        nbHeuresLegales = parametre1.pparMontant().doubleValue();
    }
    
    // d√©termine le nombre d'enfants ayant droit au SFT au 1er janvier de l'ann√©e
    private int nbEnfantsACharge() {
        int moisCode = mois().moisAnnee().intValue() * 100 + 1;
        EOPayeMois mois = EOPayeMois.moisAvecCode(editingContext(),new Integer(moisCode));
        int nbEnfants = EOEnfant.nbEnfantsAyantDroitSFT(agent(),mois,editingContext());
        return nbEnfants;
    }
}
