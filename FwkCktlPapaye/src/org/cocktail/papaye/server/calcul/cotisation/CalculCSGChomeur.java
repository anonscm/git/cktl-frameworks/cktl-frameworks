/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;




/** Cette classe modelise le calcul de la CSG pour un ch&ocirc;meur */
public class CalculCSGChomeur extends CalculCotisationChomeur {
  
	public void effectuerCalcul(String codeTaux,String codeAssiette,BigDecimal baseAssiette) throws Exception {
        try {
			super.preparerParametres(codeTaux,codeAssiette,baseAssiette);
			// prendre comme assiette la valeur absolue de l'assiette de calcul
			double baseAssietteAbsolue = baseAssiette.abs().doubleValue();
			// calculer le net √† payer (ie Remun - cotisations d√©j√† pay√©es)
			double montantNet = calculerNet();
			// calculer √† part le montant de CSG total
			double montantCSG = calculerResultTotalCSG(baseAssietteAbsolue);
			// v√©rifier si la r√©mun√©ration apr√®s d√©duction de la CSG et de la CRDS est-en dessous du seuil d'exon√©ration
			double montantExoneration = seuilExoneration() * nbJoursTravailles();
			if (montantNet > montantExoneration) {
				// on paye de la CSG
				double assiette = 0, montantCotis = 0;
				// montantNet si en d√©duisant la CSG, on est encore au-dessus du seuil
				if (montantNet - montantCSG > montantExoneration) {
					// on paye la cotisation normale
					assiette = baseAssietteAbsolue * (tauxAssiette() / 100);
					montantCotis = assiette * (taux() / 100);
				} else {
					// on fait en sorte de ne pas d√©passer le seuil d'exon√©ration. 
					// Pour la CSG, on proratise entre les deux taux (d√©ductible et non d√©ductible)
					assiette = ((montantNet - montantExoneration) * 100) / tauxTotalCSG();
					montantCotis = assiette * (taux() / 100);
				}
				// arrondir les r√©sultats au centi√®me d'euro pr√®s et fournir deux chiffres
				// apr√®s la virgule
				ajouterCotisation(codeCotisation(),
								  new BigDecimal(montantCotis).setScale(2,BigDecimal.ROUND_HALF_UP),
								  new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_DOWN));
				
			}
        } catch (Exception e) { throw e; }
    }
}
