	/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.calcul.remuneration.CalculTraitement;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeElement;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.moteur.ModeleCalcul;

import com.webobjects.foundation.NSDictionary;

/** Calcul Mage
 * @author christine

 */
public class CalculMage extends ModeleCalcul {
    private static String INDICE_PLAFOND = "INMAGEMX";
    private static String INDICE_PLANCHER = "INMAGEMN";
	private final static String REMUN_NBI = "REMUNNBI";
	private final static String REMUNERATION = "REMUNBRU";
	private final static String REMUN_GMR = "REMUNGMR";
	private final String PRENDRE_EN_COMPTE_QUOTITE = "AQUOTITE";
	private final String CODE_INDEMNITE_SUJETION = "REMUNISU";
	private final static String REMUN_IR = "REMUNIRS";
    private int indicePlafond,indicePlancher;
    private double taux,montant,tauxAssiette,tauxIR;
    private EOPayeCode codeCotisation;
    private CalculTraitement calculateur;
    private boolean prendreEnCompteQuotite = true;
    
    /** Effectue le calcul de la cotisation en appliquant la r&egrave;gle taux x (baseAssiette x tauxAssiette) %, avec la baseAssiette
     * comprise entre un plafond minimum et un plafond maximum selon les valeurs des booleens fournis en param&egrave;tre<BR>
    Ajoute le resultat obtenu au dictionnaire des resultats.
    @param codeTaux code associe au taux de la part de cotisation
    @param codeMontant code du montant a ajouter eventuellement (peut &ecirc;tre nul)
    @param codeAssiette code associe au taux de l'assiette
    @param aValeurPlancher (true si il y a une valeur plancher)
    @param aValeurPlafond (true si il y a une valeur plafond)
    */
    public void effectuerCalcul(String codeTaux,String codeMontant,String codeAssiette,boolean aValeurPlancher,boolean aValeurPlafond) throws Exception {
    		try {
    			preparerParametres(codeTaux,codeMontant,codeAssiette);
	    		
	        BigDecimal baseAssiette = calculerAssiette();
	         double assiette = baseAssiette.doubleValue();
    			// pour les CPAs, les CFAs et les temps partiels, prendre l'assiette réelle sans plancher, ni plafonde
	         if (contrat().statut().pstaLibelle().indexOf(" CPA ") < 0 && contrat().statut().pstaLibelle().indexOf(" CFA ") < 0) {
	         	// 17/01/06 - on ne prend pas la quotité d'après ECP i.e on cotise au minimum à l'assiette plancher
    				// prendre comme assiette la valeur absolue de l'assiette de calcul (cas des salaires trop payés)
	             BigDecimal baseAssietteAbsolue = baseAssiette.abs();
		    		BigDecimal plancher = calculerValeurLimite(indicePlancher);
		    		BigDecimal plafond = calculerValeurLimite(indicePlafond);
	        		if (aValeurPlancher && baseAssietteAbsolue.doubleValue() < plancher.doubleValue()) {
	        			if (!prendreEnCompteQuotite) {
	        				assiette = plancher.doubleValue();
	        			}
	        		} else if (aValeurPlafond && baseAssietteAbsolue.doubleValue() > plafond.doubleValue()) {
	        			assiette = plafond.doubleValue();
	        		} 
	        		if (baseAssiette.doubleValue() < 0 && assiette > 0) {
	        			assiette = -assiette;
	        		}
    			}
	    		
	    		double montantAssiette = assiette * (tauxAssiette / 100);
	    		double result = montant +  (montantAssiette * (taux / 100));	// montant fixe + assiette
	        // arrondir les résultats au centième d'euro près et fournir deux chiffres
	        // après la virgule
			if (result != 0) {
				ajouterCotisation(codeCotisation,new BigDecimal(result).setScale(2,BigDecimal.ROUND_HALF_UP),new BigDecimal(assiette).setScale(2,BigDecimal.ROUND_HALF_DOWN));
			}
	        
        } catch (Exception e) { throw e; }
    }
    public NSDictionary calculer(NSDictionary parametres) throws Exception {
    		try {
	    		// pour initialiser le calculateur
	    		if (calculateur == null) {
	    			calculateur = new CalculTraitement();
				calculateur.calculer(parametres);   // pour préparer le calcul
	    		}
	    		return super.calculer(parametres);
    		} catch (Exception e) {
    			throw e;
    		}
    }
    
    private void preparerParametres(String codeTaux,String codeMontant,String codeAssiette) throws Exception {
    		EOPayeParam parametre1 = parametrePourCode(codeTaux);
	    if (parametre1 == null) {
	       throw new Exception("Pour la classe : "+ nomClasse() + ", le taux " + codeTaux + " n'est pas defini");
	    }
	    codeCotisation = parametre1.code();
	    taux = parametre1.pparTaux().doubleValue();
	    EOPayeParam parametre2 = parametrePourCode(codeAssiette);
	    if (codeMontant != null) {
	    		parametre1 = parametrePourCode(codeMontant);
		    if (parametre1 == null) {
		       throw new Exception("Pour la classe : "+ nomClasse() + ", le montant " + codeMontant + " n'est pas defini");
	    		} else {
	    			montant = parametre1.pparMontant().doubleValue();
	    		}
	    } else {
	    		montant = 0;
	    }
	    if (parametre2 == null) {
	       throw new Exception("Pour la classe : "+ nomClasse() + ", le taux de l'assiette" + codeAssiette + " n'est pas defini");
	    }
	    tauxAssiette = parametre2.pparTaux().doubleValue();
	    if ((montant == 0 && taux == 0) || tauxAssiette == 0) {
	       throw new Exception("Pour la classe : "+ nomClasse() + ", un des parametres de taux a une valeur nulle");
	    }
	    parametre1 = parametrePourCode(INDICE_PLAFOND);
	    if (parametre1 == null) {
		    throw new Exception ("Pour la classe "+ nomClasse() + ", le  parametre " + INDICE_PLAFOND + " n'est pas defini");
		} else {
		    if (parametre1.pparIndice() == null || parametre1.pparIndice().equals("0")) {
		throw new Exception ("Pour la classe "+ nomClasse() + ", la valeur du  parametre " + INDICE_PLAFOND + " n'est pas definie");
		    } else {
		        Integer valeur = new Integer(parametre1.pparIndice());
		        indicePlafond = valeur.intValue();
		    }
		}
	    parametre1 = parametrePourCode(INDICE_PLANCHER);
	    if (parametre1 == null) {
		    throw new Exception ("Pour la classe  "+ nomClasse() + ", le  parametre " + INDICE_PLANCHER + " n'est pas defini");
		} else {
		    if (parametre1.pparIndice() == null || parametre1.pparIndice().equals("0")) {
		throw new Exception ("Pour la classe " + nomClasse() + ", la valeur du  parametre " + INDICE_PLANCHER + " n'est pas definie");
		    } else {
		        Integer valeur = new Integer(parametre1.pparIndice());
		        indicePlancher = valeur.intValue();
		    }
		}
	    parametre1 = parametrePourCode(PRENDRE_EN_COMPTE_QUOTITE);
	    if (parametre1 != null && parametre1.pparEntier() != null) {
	    		prendreEnCompteQuotite = parametre1.pparEntier().intValue() == 1;
	    }
	    String codeIndemRes = contrat().structure().indemniteResidence();
        if (codeIndemRes!= null) {
            // déterminer le taux d'IR
            EOPayeCode code = EOPayeCode.rechercherCode(editingContext(), codeIndemRes);
            // rechercher le paramètre associé à ce code
            if (code == null) {
                throw new Exception("Pour la classe CalculIR le code " + codeIndemRes
                                    +  " n'est pas defini");
            }
            EOPayeParam parametre = EOPayeParam.parametreValide(code);
            if (parametre == null) {
                code = parametre.code();
                throw new Exception("Pour la classe "+ nomClasse() + ", le parametre " + codeIndemRes  +  " n'est pas defini");
            }
            if (parametre.pparTaux() == null) {
                throw new Exception("Pour la classe "+ nomClasse() + ", la valeur du taux IR n'est pas definie");
            }
            tauxIR = parametre.pparTaux().doubleValue();
        }
    }
    private BigDecimal calculerAssiette() {
	    	BigDecimal remunTotale = new BigDecimal(0);
	    	Enumeration e = elements().objectEnumerator();
	    while (e.hasMoreElements()) {
	        EOPayeElement element = (EOPayeElement)e.nextElement();
	        if (element.rubrique().estAPayer() && (element.code().pcodCode().equals(REMUNERATION) || element.code().pcodCode().equals(REMUN_NBI) ||
	             element.code().pcodCode().equals(EOPayeCode.REMUN_BRUT) || element.code().pcodCode().equals(CODE_INDEMNITE_SUJETION) ||
				element.code().pcodCode().equals(REMUN_GMR) || element.code().pcodCode().equals(REMUN_IR)))  {
	        		remunTotale = remunTotale.add(element.pelmApayer());
	        } else if (element.estUneChargePatronale() || element.estUneDeduction()) {
	        	// s'arrêter quand on rencontre les cotisations
	        		break;
	        }  
        }
	    return remunTotale;
    }
    private BigDecimal calculerValeurLimite(int valeurIndice) throws Exception {
    		try {
    			BigDecimal TB = calculateur.traitementBrutIndicielMensuelComplet(valeurIndice);
    			double calcul = TB.doubleValue() * (tauxIR / 100);
             BigDecimal IR = new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_DOWN);
             return TB.add(IR);
    		} catch (Exception e) {
    			throw e;
    		}
    }
}
