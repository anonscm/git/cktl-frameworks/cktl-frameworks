/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.papaye.server.calcul.cotisation;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;


/** Cette classe modelise tous les calculs de cotisations des artistes et intermittents
qui sont basees sur un montant plafonne.*/
public class CalculCotisationArtistePlafonnee extends CalculCotisationPlafonnee {
    private static String PLAFOND_HORAIRE = "PLAHOSS";
    private static String PLAFOND_JOURNALIER = "PLAJARTI";

    public double tauxAbattement() { return contrat().abattement().doubleValue(); }
    /** Effectue le calcul
        @param codeTaux code du taux de cotisation
        @param codeTauxAssiette code du taux de l'assiette de cotisation
        @param assiette montant de l'assiette utilise pour le calcul
        @param calculerAssiette l'assiette doit-elle &ecirc;tre recalculee ?
        @param faireAbattement doit-on faire un abattement sur le montant ?
    */
    public void effectuerCalcul(String codeTaux, String codeTauxAssiette,BigDecimal assiette,boolean calculerAssiette,boolean faireAbattement) throws Exception {
        double montant;
        if (faireAbattement) {	
            montant = assiette.doubleValue() * (1 - (tauxAbattement() / 100));
        } else {	// l'assiette sera calculée par la super classe
            montant = assiette.doubleValue();
        }
        super.effectuerCalcul(codeTaux, codeTauxAssiette,new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_DOWN),calculerAssiette);
    }
    /** calcule le montant du plafond SS en fonction du contrat */
    public BigDecimal calculerPlafondSS() throws Exception {
        // rechercher le nombre de jours du contrat
        int nbJours = 0;
        if (contrat().nbJoursContrat() != null) {
            nbJours = contrat().nbJoursContrat().intValue();
        }
        if (nbJours == 0) {
            if (periode().pperNbHeure() != null &&
                periode().pperNbHeure().floatValue() > 0) {
                double nbHeures = periode().pperNbHeure().doubleValue();
                setPlafondSS(rechercherPlafondSS(PLAFOND_HORAIRE) * nbHeures);
            } else if (contrat().montantMensuelRemu() != null && contrat().montantMensuelRemu().doubleValue() != 0) {
                setPlafondSS(rechercherPlafondSS(PLAFOND_MENSUEL));
            } else {
                throw new Exception("Le montant mensuel de remuneration de l'agent " +
                                    contrat().individu().identite() + " est nul");
            }
        } else if (nbJours < 5) {
            // calculer sur la période
            nbJours = periode().pperNbJour().intValue();
            setPlafondSS(rechercherPlafondSS(PLAFOND_HORAIRE) * 12 * nbJours);
        } else  {
            // calculer sur la période
            nbJours = periode().pperNbJour().intValue();
            setPlafondSS(rechercherPlafondSS(PLAFOND_JOURNALIER) * nbJours);
        }
        return new BigDecimal(plafondSS()).setScale(2,BigDecimal.ROUND_HALF_DOWN);
    }
    private double rechercherPlafondSS(String codeString) throws Exception {
        EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),codeString);
        EOPayeParam param = EOPayeParam.parametreValide(code);
        if (param == null) {
            throw new Exception("Dans la classe Remuneration_Artiste_NonCadre, le parametre "+  code + " n'est pas defini");
        }
        if (param.pparMontant().doubleValue() == 0) {
            throw new Exception("Dans la classe Remuneration_Artiste_NonCadre, la valeur du parametre "+  code + " n'est pas defini");
        }
        return param.pparMontant().doubleValue();
    }
    
}
