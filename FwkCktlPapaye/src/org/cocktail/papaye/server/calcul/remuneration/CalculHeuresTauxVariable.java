/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package  org.cocktail.papaye.server.calcul.remuneration;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParamPerso;

/** Effectue le calcul de la remuneration d'heures dont le taux est un parametre (EOPayeParam) et le nombre d'heures
un parametre personnel. Calcul : taux * nb heures */

public class CalculHeuresTauxVariable extends CalculTraitement {
    
    public void effectuerCalcul(String codeTaux, String codeHeures, String codeRemun) throws Exception {

    	try {
        
    		EOPayeParamPerso parametre1 = parametrePersoPourCode(codeHeures);
            if (parametre1 == null) {
                throw new Exception("Pour la classe " + nomClasse() + " le parametre " + codeHeures + " n'est pas defini");
            }
            String valeur = parametre1.pparValeur();
            if (valeur == null) {
                throw new Exception("Pour la classe  " + nomClasse() + " la valeur du nb d'heures montant n'est pas definie");
            }
            
            BigDecimal nbHeures = new BigDecimal(valeur);

            // recuperer dans les parametres, le montant associe au tarif horaire
			EOPayeParam parametre = parametrePourCode(codeTaux);
			if (parametre == null) {
				throw new Exception("Pour la classe " + nomClasse() + " le parametre associe au code " + codeTaux + " n'est pas defini");
			}
			BigDecimal montantHoraire = parametre.pparMontant();
			if (montantHoraire == null) {
				throw new Exception("Pour la classe " + nomClasse() + " la valeur du montant pour le code " + codeTaux + "n'est pas d√©finie");
			}
			
            BigDecimal montant = calculRemunerationSurBaseHoraire(nbHeures,montantHoraire);
            // ajouter au resultat.
            // rechercher le code associe au traitement brut : assiette non significative
            EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),codeRemun);
            ajouterRemunerationHoraire(code,montant,nbHeures,montantHoraire);
			mettreAJourPrepa(nbHeures);
			
        } catch (Exception e) { throw e; }
    }
}
