/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.papaye.server.calcul.remuneration;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeRubrique;
import org.cocktail.papaye.server.moteur.ModeleCalcul;

import com.webobjects.foundation.NSArray;

/** Fournit une methode utilitaire de calcul qui permet de determiner une
remuneration brute en fonction du net. Ne prend pas en compre les tranches 2 de retraite. Le calcul n'est precis que lorsqu'on n'est en-dessous des plafonds SS.
*/
public class CalculTraitementGlobal extends ModeleCalcul {
    private final static String PLAFOND_MENSUEL = "PLAFMSSS";
    public final static String TAUX_CONTRIBUTION_SOLIDARITE = "TXCGCSOL";
    private NSArray rubriquesCotisations;

    /** Accesseur */
    private double tauxAbattement() { return contrat().abattement().doubleValue(); }
    
    /** Effectue le calcul d'un montant brut a partir du net. Le plafond SS choisi est le plafond
        mensuel.
        @param montant net
        Ne pas utiliser cette methode pour le statut artiste/intermittent
    */
    public BigDecimal traitementBrutPourNet(double net) throws Exception {
        EOPayeParam parametre = EOPayeParam.parametreValide(EOPayeCode.rechercherCode(editingContext(),PLAFOND_MENSUEL));
        if (parametre == null) {
            throw new Exception("Pour la classe "+ nomClasse() + ", le plafond mensuel de la securite sociale n'est pas defini");
        }

        BigDecimal plafondSS = parametre.pparMontant();

        if (plafondSS.floatValue() == 0) {
            throw new Exception("Pour la classe "+ nomClasse() + ", la valeur du plafond mensuel de la securite sociale n'est pas definie");
        }
        return traitementBrutPourNet(net,plafondSS,false);
    }
    /** Effectue le calcul d'un montant brut a partir du net
        @param montant net
        @param montant du plafond SS
        @param estArtiste booleen, statut artiste/intermittent ou non
    */
    public BigDecimal traitementBrutPourNet(double net,BigDecimal plafondSS,boolean estArtiste) throws Exception {
        // récupérer toutes les rubriques du statut sont des charges salariales
        if (!estArtiste || rubriquesCotisations == null) {
            rubriquesCotisations = contrat().statut().rubriquesCotisationSalariale();
        }
        // déterminer la valeur du nette à partir de laquelle, on est au-dessus du plafond de SS
        double netPlafond;
        if (estArtiste) {
            netPlafond = traitementNetPourBrutArtiste(plafondSS.doubleValue());
        } else {
            netPlafond = traitementNetPourBrut(plafondSS.doubleValue());
        }
        double brut;
        if (net <= netPlafond) {
            if (estArtiste) {
                brut = calculerBrutPourNetArtiste(net);
            } else {
                brut = calculerBrutPourNet(net);
            }
        } else {
            if (estArtiste) {
                brut = calculerBrutPourNetArtiste(net,plafondSS.doubleValue());
            } else {
                brut = calculerBrutPourNet(net,plafondSS.doubleValue());
            }
        }
        return new BigDecimal(brut).setScale(2,BigDecimal.ROUND_HALF_DOWN);
    }
    
    private double traitementNetPourBrut(double brut) throws Exception {
        double montantCotis = 0;
        double tauxCotis = 0;
        boolean estCSG_CRDS;
        boolean estContribSolidarite = false;
        Enumeration e = rubriquesCotisations.objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeRubrique rubrique = (EOPayeRubrique)e.nextElement();
            Enumeration e1 = rubrique.codes().objectEnumerator();
            double taux = 0,tauxAssiette = 0, assiette = brut;
            estCSG_CRDS = false;
            while (e1.hasMoreElements()) {
                EOPayeCode code = (EOPayeCode)e1.nextElement();
                // rechercher le paramètre pour le contrat
                if (code.pcodCode().startsWith("COT") == false) {
                    EOPayeParam param = EOPayeParam.parametreValide(code);
                    if (param == null) {
                        throw new Exception("le parametre associe au code '" + code.pcodCode() + "' n'est pas defini");
                    }
                    if (code.pcodCode().startsWith("TX") &&
                        (code.pcodCode().indexOf("2") == -1)) {
                        // c'est un taux qui n'est pas de tranche 2
                        taux = param.pparTaux().doubleValue();
                        if (code.pcodCode().endsWith("CSG") == true ||
                            code.pcodCode().endsWith("RDS") == true) {
                            estCSG_CRDS = true;
                        }
                        if (code.pcodCode().equals(TAUX_CONTRIBUTION_SOLIDARITE)) {
                            estContribSolidarite = true;
                            tauxAssiette = 100.0;
                        }
                    } else if (code.pcodCode().startsWith("AS") &&
                               (code.pcodCode().indexOf("2") == -1)) {
                        // c'est un taux d'assiette qui n'est pas de tranche 2
                        tauxAssiette = param.pparTaux().doubleValue();
                    }
                }
            }
            if (estContribSolidarite) {
                assiette = brut * (1 - tauxCotis);
            } else {
                assiette = brut;
                if (estCSG_CRDS) {
                    tauxCotis += (tauxAssiette / 100) * (taux / 100);
                }
            }
            double cotis = assiette * (tauxAssiette / 100) * (taux / 100);
            montantCotis += cotis;
        }
        return (brut - montantCotis);
    }
    private double calculerBrutPourNet(double net) throws Exception {
        double tauxCotis = 0;
        double tauxCotisCSG_CRDS = 0;
        double tauxCotisSolidarite = 0;
        boolean estCSG_CRDS;
        boolean estContribSolidarite = false;
        Enumeration e = rubriquesCotisations.objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeRubrique rubrique = (EOPayeRubrique)e.nextElement();
            estCSG_CRDS = false;
            Enumeration e1 = rubrique.codes().objectEnumerator();
            double taux = 0,tauxAssiette = 0;
            while (e1.hasMoreElements()) {
                EOPayeCode code = (EOPayeCode)e1.nextElement();
                if (code.pcodCode().startsWith("COT") == false) {
                    // rechercher le paramètre pour le contrat
                    EOPayeParam param = EOPayeParam.parametreValide(code);
                    if (param == null) {
                        throw new Exception("le parametre associe au code '" + code.pcodCode() + "' n'est pas defini");
                    } else {
                        if (code.pcodCode().startsWith("TX") &&
                            (code.pcodCode().indexOf("2") == -1)) {
                            // c'est un taux qui n'est pas de tranche 2
                            taux = param.pparTaux().doubleValue();
                            if (code.pcodCode().endsWith("CSG") == true ||
                                code.pcodCode().endsWith("RDS") == true) {
                                estCSG_CRDS = true;
                            } else if (code.pcodCode().equals(TAUX_CONTRIBUTION_SOLIDARITE)) {
                                estContribSolidarite = true;
                                tauxAssiette = 100.0;
                            }
                        } else if (code.pcodCode().startsWith("AS") &&
                                   (code.pcodCode().indexOf("2") == -1)) {
                            // c'est un taux d'assiette qui n'est pas de tranche 2
                            tauxAssiette = param.pparTaux().doubleValue();
                        }
                    }
                }
            }
            if (estContribSolidarite) {
                tauxCotisSolidarite = (tauxAssiette / 100) * (taux / 100);
            } else if (estCSG_CRDS) {
                tauxCotisCSG_CRDS +=  (tauxAssiette / 100) * (taux / 100);
            } else {
                tauxCotis += (tauxAssiette / 100) * (taux / 100);
            }
        }
        double calcul = tauxCotis + tauxCotisSolidarite + tauxCotisCSG_CRDS - (tauxCotis * tauxCotisSolidarite);
        return net / (1 - calcul);
    }
    private double calculerBrutPourNet(double net,double plafondSS) throws Exception {
        double tauxCotis = 0;
        double tauxCotisAuPlafondSS = 0;
        double tauxCotisCSG_CRDS = 0;
        boolean estCotisTranche2;
        double tauxCotisSolidarite = 0;
        double tauxCotisTranche2 = 0;
        boolean estCSG_CRDS;
        boolean estContribSolidarite = false;
        Enumeration e = rubriquesCotisations.objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeRubrique rubrique = (EOPayeRubrique)e.nextElement();
            Enumeration e1 = rubrique.codes().objectEnumerator();
            double taux = 0,tauxAssiette = 0;
            boolean estAuPlafondSS = false;
            estCSG_CRDS = false;
            estCotisTranche2 = false;
            while (e1.hasMoreElements()) {
                EOPayeCode code = (EOPayeCode)e1.nextElement();
                if (code.pcodCode().startsWith("COT") == false) {
                    // rechercher le paramètre pour le contrat
                    EOPayeParam param = EOPayeParam.parametreValide(code);
                    if (param == null) {
                        throw new Exception("le parametre associe au code '" + code.pcodCode() + "' n'est pas defini");
                    }
                    // ne pas prendre en compte les assiettes et taux de tranche 2
                    if (code.pcodCode().startsWith("TX")) {
                        // c'est un taux
                        // c'est un taux qui n'est pas de tranche 2
                        taux = param.pparTaux().doubleValue();
                        if (code.pcodCode().endsWith("CSG") == true ||
                            code.pcodCode().endsWith("RDS") == true) {
                            estCSG_CRDS = true;
                        } else if (code.pcodCode().equals(TAUX_CONTRIBUTION_SOLIDARITE)) {
                            estContribSolidarite = true;
                            tauxAssiette = 100.0;
                        } else if (code.pcodCode().indexOf("2") == -1) {
                            estCotisTranche2 = true;
                        }
                    } else if (code.pcodCode().startsWith("AS")) {
                        // c'est une assiette
                        tauxAssiette = param.pparTaux().doubleValue();
                    } else if (code.pcodCode().startsWith("PLA")) {
                        estAuPlafondSS = true;
                    }
                }
            }
            if (estAuPlafondSS) {
                tauxCotisAuPlafondSS += (tauxAssiette / 100) * (taux / 100);
            } else if (estContribSolidarite) {
                tauxCotisSolidarite = (tauxAssiette / 100) * (taux / 100);
            } else if (estCSG_CRDS) {
                tauxCotisCSG_CRDS +=  (tauxAssiette / 100) * (taux / 100);
            } else if (estCotisTranche2) {
                tauxCotisTranche2 = (tauxAssiette / 100) * (taux / 100);
            }
            else {
                tauxCotis +=  (tauxAssiette / 100) * (taux / 100);
            }
        }
        tauxCotis += tauxCotisTranche2;
        tauxCotisAuPlafondSS -= tauxCotisTranche2;

        return (net  + (tauxCotisAuPlafondSS * (1 - tauxCotisSolidarite) * plafondSS)) / (1 - tauxCotis);
    }
    // artistes
    private double traitementNetPourBrutArtiste(double brut) throws Exception {
        double montantCotis = 0;
        boolean estCSG_CRDS;
        Enumeration e = rubriquesCotisations.objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeRubrique rubrique = (EOPayeRubrique)e.nextElement();
            Enumeration e1 = rubrique.codes().objectEnumerator();
            double taux = 0,tauxAssiette = 0, assiette = brut, abattement = 0;
            estCSG_CRDS = false;
            while (e1.hasMoreElements()) {
                EOPayeCode code = (EOPayeCode)e1.nextElement();
                if (code.pcodCode().startsWith("COT") == false) {
                // rechercher le paramètre pour le contrat
                    EOPayeParam param = EOPayeParam.parametreValide(code);
                    if (param == null) {
                        throw new Exception("le parametre associe au code '" + code.pcodCode() + "' n'est pas defini");
                    } else {
                        if (code.pcodCode().startsWith("TX") &&
                            (code.pcodCode().indexOf("2") == -1)) {
                            // c'est un taux qui n'est pas de tranche 2
                            taux = param.pparTaux().doubleValue();
                            if (code.pcodCode().endsWith("CSG") == true ||
                                code.pcodCode().endsWith("RDS") == true) {
                                estCSG_CRDS = true;
                            }
                        } else if (code.pcodCode().startsWith("AS") &&
                                   (code.pcodCode().indexOf("2") == -1)) {
                            // c'est un taux d'assiette qui n'est pas de tranche 2
                            tauxAssiette = param.pparTaux().doubleValue();
                        } else if (code.pcodCode().startsWith("ABAT")) {
                            // il faut faire un abattement
                            abattement = tauxAbattement();
                        }
                    }
                }
            }
            if (estCSG_CRDS) {
                assiette = brut;
            } else {
                assiette = brut * ((100 - abattement) / 100);
            }
            double cotis = assiette * (tauxAssiette / 100) * (taux / 100);
            montantCotis += cotis;
        }
        return (brut - montantCotis);
    }
    private double calculerBrutPourNetArtiste(double net) throws Exception {
        double tauxCotis = 0;
        boolean estCSG_CRDS;
        Enumeration e = rubriquesCotisations.objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeRubrique rubrique = (EOPayeRubrique)e.nextElement();
            estCSG_CRDS = false;
            Enumeration e1 = rubrique.codes().objectEnumerator();
            double taux = 0,tauxAssiette = 0,abattement = 0;
            while (e1.hasMoreElements()) {
                EOPayeCode code = (EOPayeCode)e1.nextElement();
                if (code.pcodCode().startsWith("COT") == false) {
                    // rechercher le paramètre pour le contrat
                    EOPayeParam param = EOPayeParam.parametreValide(code);
                    if (param == null) {
                        throw new Exception("le parametre associe au code '" + code.pcodCode() + "' n'est pas defini");
                    } else {
                        if (code.pcodCode().startsWith("TX") &&
                            (code.pcodCode().indexOf("2") == -1)) {
                            // c'est un taux qui n'est pas de tranche 2
                            taux = param.pparTaux().doubleValue();
                            if (code.pcodCode().endsWith("CSG") == true ||
                                code.pcodCode().endsWith("RDS") == true) {
                                estCSG_CRDS = true;
                            }
                        } else if (code.pcodCode().startsWith("AS") &&
                                   (code.pcodCode().indexOf("2") == -1)) {
                            // c'est un taux d'assiette qui n'est pas de tranche 2
                            tauxAssiette = param.pparTaux().doubleValue();
                        } else if (code.pcodCode().startsWith("ABAT")) {
                            // il faut faire un abattement
                            abattement = tauxAbattement();
                        }
                    }
                }
            }
            if (estCSG_CRDS) {
                tauxCotis +=  (tauxAssiette / 100) * (taux / 100);
            } else {
                double coefficient = 1 - (abattement / 100);
                tauxCotis += coefficient * (tauxAssiette / 100) * (taux / 100);
            }
        }
        return net / (1 - tauxCotis);
    }
    private double calculerBrutPourNetArtiste(double net,double plafondSS) throws Exception {
        double tauxCotis = 0;
        double tauxCotisAuPlafondSS = 0;
        boolean estCSG_CRDS;
        Enumeration e = rubriquesCotisations.objectEnumerator();
        while (e.hasMoreElements()) {
            EOPayeRubrique rubrique = (EOPayeRubrique)e.nextElement();
            Enumeration e1 = rubrique.codes().objectEnumerator();
            double taux = 0,tauxAssiette = 0,abattement = 0;
            boolean estAuPlafondSS = false;
            estCSG_CRDS = false;
            while (e1.hasMoreElements()) {
                EOPayeCode code = (EOPayeCode)e1.nextElement();
                if (code.pcodCode().startsWith("COT") == false) {
                    // rechercher le paramètre pour le contrat
                    EOPayeParam param = EOPayeParam.parametreValide(code);
                    if (param == null) {
                        throw new Exception("le parametre associe au code '" + code.pcodCode() + "' n'est pas defini");
                    }
                    // ne pas prendre en compte les assiettes et taux de tranche 2
                    if (code.pcodCode().startsWith("TX") &&
                        (code.pcodCode().indexOf("2") == -1)) {
                        // c'est un taux
                        // c'est un taux qui n'est pas de tranche 2
                        taux = param.pparTaux().doubleValue();
                        if (code.pcodCode().endsWith("CSG") == true ||
                            code.pcodCode().endsWith("RDS") == true) {
                            estCSG_CRDS = true;
                        }
                    } else if (code.pcodCode().startsWith("AS") &&
                               (code.pcodCode().indexOf("2") == -1)) {
                        // c'est une assiette
                        tauxAssiette = param.pparTaux().doubleValue();
                    } else if (code.pcodCode().startsWith("ABAT")) {
                        // il faut faire un abattement
                        abattement = tauxAbattement();
                    } else if (code.pcodCode().startsWith("PLA")) {
                        estAuPlafondSS = true;
                    }
                }
            }
            if (estAuPlafondSS) {
                tauxCotisAuPlafondSS += (tauxAssiette / 100) * (taux / 100);
            } else if (estCSG_CRDS) {
                tauxCotis +=  (tauxAssiette / 100) * (taux / 100);
            } else {
                double coefficient = 1 - (abattement / 100);
                tauxCotis += coefficient * (tauxAssiette / 100) * (taux / 100);
            }
        }
        return (net  + (tauxCotisAuPlafondSS * plafondSS)) / (1 - tauxCotis);
    }
}
