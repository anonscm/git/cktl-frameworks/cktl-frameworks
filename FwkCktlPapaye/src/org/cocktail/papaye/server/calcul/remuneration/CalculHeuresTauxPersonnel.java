/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.remuneration;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParamPerso;
import org.cocktail.papaye.server.moteur.ModeleCalcul;

import com.webobjects.foundation.NSDictionary;


/** Effectue le calcul d'une remuneration basee sur un taux horaire et un nombre d'heures variables
Calcul : montantHoraire * nbHeures
*/
public class CalculHeuresTauxPersonnel extends ModeleCalcul {
    private final static String MONTANT = "MONHOVAC";
    private final static String NB_HEURES = "NBHEUVAC";
    
	public NSDictionary effectuerCalcul(String codeRemuneration) throws Exception {
       return effectuerCalcul(codeRemuneration,false);	// ne pas prendre en compte le plafond reduit
	}
	public NSDictionary effectuerCalcul(String codeRemuneration,boolean prendreEnComptePlafondReduit) throws Exception {

		System.out.println(">>>>>>>>>>>>>>> CalculHeuresTauxPersonnel.effectuerCalcul() <<<<<<<<<<<<<<<<<<<<");
		
		try {
        	
            EOPayeParamPerso parametre1 = parametrePersoPourCode(MONTANT);
            if (parametre1 == null) {
                throw new Exception("Pour la classe " + nomClasse() + " le parametre montant n'est pas defini");
            }
            String valeur = parametre1.pparValeur();
            if (valeur == null) {
                throw new Exception("Pour la classe " + nomClasse() + " la valeur du parametre montant n'est pas definie");
            }
            BigDecimal montantHoraire;
            try {
                montantHoraire = new BigDecimal(valeur).setScale(2,BigDecimal.ROUND_HALF_DOWN);
            } catch (Exception e) {
                throw new Exception("le taux horaire saisi comme parametre personnel n'est pas une valeur numerique");
            }
            EOPayeParamPerso parametre2 = parametrePersoPourCode(NB_HEURES);
            if (parametre2 == null) {
                throw new Exception("Pour la classe " + nomClasse() + " le parametre nbHeures n'est pas defini");
            }
            valeur = parametre2.pparValeur();
            if (valeur == null) {
                throw new Exception("Pour la classe " + nomClasse() + " la valeur du parametre nbHeures n'est pas definie");
            }
			BigDecimal nbHeures;
			try {
				nbHeures = new BigDecimal(valeur);
			} catch (Exception e) {
				throw new Exception("Le nombre d'heures saisi comme parametre personnel n'est pas une valeur numerique");
			}
            // Effectuer le calcul
            BigDecimal calcul = montantHoraire.multiply(nbHeures);	
            		
            BigDecimal remun = calcul.setScale(2,BigDecimal.ROUND_HALF_DOWN);
            EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),codeRemuneration);
			if (prendreEnComptePlafondReduit && contrat().utiliserPlafondReduit() && contrat().peutAvoirPlafondReduit()) {
				BigDecimal montantTempsPlein = (calcul.multiply(new BigDecimal(100))).divide(contrat().numQuotRecrutement());
				BigDecimal remunTempsPlein = montantTempsPlein.setScale(2,BigDecimal.ROUND_HALF_DOWN);

				ajouterRemunerationHoraire(code,remun,remunTempsPlein,nbHeures,montantHoraire);
			} else {
				ajouterRemunerationHoraire(code,remun,nbHeures,montantHoraire);
			}

			mettreAJourPrepa(nbHeures);
			
            return resultats();
        } catch (Exception e) { throw e; }
    }
}

