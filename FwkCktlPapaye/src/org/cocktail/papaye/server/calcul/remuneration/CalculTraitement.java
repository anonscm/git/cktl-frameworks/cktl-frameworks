/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.remuneration;

import java.math.BigDecimal;

import org.cocktail.papaye.server.common.Constantes;
import org.cocktail.papaye.server.common.PayeFinder;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeContrat;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeMois;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;
import org.cocktail.papaye.server.moteur.ModeleCalcul;


/** Fournit des methodes utilitaires de calcul de base lies au traitement :
<UL>traitement indiciaire mensuel</UL>
<UL>traitement indiciaire annuel</UL>
<UL>traitement sur base horaire</UL>
<UL>calcul Indemnite de residence</UL>
*/
public class CalculTraitement extends ModeleCalcul {
    private final static String VAL_POINT_INDICE = "VALPTIND";
    private double valeurPoint;
    /** calcul la valeur annuelle d'un traitement en fonction de l'indice.
        @param indice indice pour lequel effectuer le calcul
        @param mois mois pour lequel il faut rechercher la valeur du point
        @return traitement brut pour une annee
        */
    public BigDecimal calculTraitementIndicielAnnuel(int indice,EOPayeMois mois) throws Exception {
        try {
			rechercherValeurPoint(mois);
			// calculer la rémunération
            double tba = valeurPoint * indice;
            BigDecimal tbAnnuel = new BigDecimal(tba).setScale(2,BigDecimal.ROUND_HALF_DOWN);
            return tbAnnuel;
        } catch (Exception e) { throw e; }
    }
    
    /** calcul la valeur annuelle d'un traitement en fonction de l'indice.
    @param indice indice pour lequel effectuer le calcul
    @return traitement brut pour une annee
    */
    public BigDecimal calculTraitementIndicielAnnuel(int indice) throws Exception {
        try {
			rechercherValeurPoint();
            // calculer la rémunération
            double tba = valeurPoint * indice;
            BigDecimal tbAnnuel = new BigDecimal(tba).setScale(2,BigDecimal.ROUND_HALF_DOWN);
            if (estUnRappel()) {
                valeurPoint = 0;	// remettre à zéro pour que ce soit évalué la prochaine fois
            }
            return tbAnnuel;
        } catch (Exception e) { throw e; }
    }

    /** calcule une quotite de  montant a partir d'un montant en appliquant la quotitefournie en param&egrave;etre
     * @param montant le montant sur lequel appliquer la quotite
     * @param la quotite de temps plein
     * @param estTempsPlein le contrat est-il considere comme un temps plein
     * @param nbJours le nombre de jours */
    public BigDecimal appliquerQuotite(BigDecimal montant,double quotite,boolean estTempsPlein,int nbJours) {
        // évaluer le traitement réel en fonction de la quotité travaillée et
        // du nombre de jours. On divise par 30 pour ramener au nombre de jours effectifs,
        // dans les cas standards.
        // Pour 80% quotite, la proportion est 6/7
        // Pour 90% quotite, la proportion est 32/35
        double coefficient = 0.0;
        switch ((int)quotite) {
            case 80 :
                if (estTempsPlein) {
                    coefficient = 6.0 / 7.0;
                } else {
                    coefficient = quotite / 100;
                }
                coefficient = coefficient * ((double)nbJours / 30);
                break;
            case 90 :
                if (estTempsPlein) {
                    coefficient = 32.0 / 35.0;
                } else {
                    coefficient = quotite / 100;
                }
				coefficient = coefficient * ((double)nbJours /30);
                break;
            default :
               coefficient = (quotite / 100) * ((double)nbJours /30);
        }
        double calcul = montant.doubleValue() * coefficient;
        return new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_DOWN);
    }     
    /** calcule un montant a partir d'un montant en appliquant la quotite fournie dans le contrat*/
    public BigDecimal appliquerQuotite(BigDecimal montant) {
    		boolean estTempsPlein = contrat().temTempsPlein() != null && contrat().temTempsPlein().equals(Constantes.VRAI);
    		return appliquerQuotite(montant,contrat().numQuotRecrutement().doubleValue(),estTempsPlein,periode().pperNbJour().intValue());
    }     
	/** calcule un nombre d'heures en fonction de la quotite travaillee */
    public BigDecimal appliquerQuotiteHoraire(BigDecimal nbHeures) {
   
    	BigDecimal taux = (contrat().numQuotRecrutement()).divide(new BigDecimal(100), BigDecimal.ROUND_HALF_UP);
    	
    	BigDecimal tauxMois = periode().pperNbJour().divide(new BigDecimal(30), 4, BigDecimal.ROUND_HALF_UP);
    	    	
    	BigDecimal coefficient = taux.multiply(tauxMois);

    	return nbHeures.multiply(coefficient);
    	
    }        
	
    /** calcul la valeur mensuelle d'un traitement en fonction de l'indice.
        @param indice indice pour lequel effectuer le calcul
        @return traitement brut pour un mois et un salaire complet
    */
    public BigDecimal traitementBrutIndicielMensuelComplet(int indice) throws Exception {
        try {
            BigDecimal tbAnnuel = calculTraitementIndicielAnnuel(indice);
            double calcul = tbAnnuel.doubleValue() / 12.0;
            return new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_DOWN);
        } catch (Exception e) { throw e; }
    }
    /** retourne la valeur de l'indice majore d'un contrat */
    public int indiceContrat() throws Exception {
        String indiceBrut = contrat().indiceContrat();
        if (indiceBrut == null) {
            throw new Exception("Dans la classe " + nomClasse() + " , l'indice du contrat " + contrat() + "est nul");
        }
        // effectuer une recherche dans la table Indice pour récupérer l'indice majoré
        Number indiceMajore = PayeFinder.indiceMajore(editingContext(),indiceBrut);
        int indice = indiceMajore.intValue();
        return indice;
    }
    /** calcul la valeur d'un traitement en fonction d'un taux horaire et
        d'un nombre d'heures effectuees.
        @param nbHeures nombre d'heures
        @param code code associe &agrave au montant horaire.
        @return resultat du calcul
        */
    public BigDecimal calculRemunerationSurBaseHoraire(Number nbHeures,String code) throws Exception {

        // récupérer dans les paramètres, le montant associé au tarif horaire
        EOPayeParam parametre = parametrePourCode(code);
        if (parametre == null) {
            throw new Exception("Pour la classe CalculTraitement(calculRemunerationSurBaseHoraire), le parametre associe au code " + code + " n'est pas defini");
        }
        Number montant = parametre.pparMontant();
        if (montant == null) {
            throw new Exception("Pour la classe classe CalculTraitement (calculRemunerationSurBaseHoraire), la valeur du montant pour le code " + code + "n'est pas définie");
        }
        double calcul = nbHeures.doubleValue()  * montant.doubleValue();
        return new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_DOWN);
    }
	/** calcul la valeur d'un traitement en fonction d'un taux horaire et
        d'un nombre d'heures effectuees.
        @param nbHeures nombre d'heures
        @param montant double montant horaire.
        @return resultat du calcul
        */
    public BigDecimal calculRemunerationSurBaseHoraire(Number nbHeures,Number montant) throws Exception {
        double calcul = nbHeures.doubleValue()  * montant.doubleValue();
        return new BigDecimal(calcul).setScale(2,BigDecimal.ROUND_HALF_DOWN);
    }
	
    /** retourne le contrat sur lequel le calcul va &ecirc;tre effectue */
    public EOPayeContrat contrat() {
        if (estUnRappel()) {
            try {
                EOPayeContrat contrat = super.contrat().contratPrecedent();
                if (contrat == null) { // rappel complet
                    return super.contrat();
                } else {	// rappel partiel
                    return contrat;
                }
            } catch (Exception e) { return null; } // ne peut pas arriver car sinon on ne traiterait pas un rappel
        } else {
            return super.contrat();
        }
    }
    // méthodes privées
    private void rechercherValeurPoint(EOPayeMois mois) throws Exception {
        EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),VAL_POINT_INDICE);
        if (code == null)
            throw new Exception ("Dans la classe CalculTraitement, le code pour le point indice n'est pas defini");
        EOPayeParam parametre;
        if (mois != null) {
            parametre = EOPayeParam.parametrePourMois(editingContext(),code, mois.moisCode());
        } else {
            parametre = EOPayeParam.parametreValide(code);
        }
        if (parametre == null)
            throw new Exception ("Dans la classe CalculTraitement, le parametre pour le point indice n'est pas defini");
        valeurPoint = parametre.pparMontant().doubleValue();
        if (valeurPoint == 0)
            throw new Exception ("Dans la classe CalculTraitement, la valeur du point indice n'est pas definie");
    }

    private void rechercherValeurPoint() throws Exception {
        EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),VAL_POINT_INDICE);
        if (code == null)
            throw new Exception ("Dans la classe CalculTraitement, le code pour le point indice n'est pas defini");
        EOPayeParam parametre;
        if (estUnRappel()) {
            parametre = EOPayeParam.parametrePourMois(editingContext(), code, periode().moisTraitement().moisCode());
        } else {
            parametre = EOPayeParam.parametreValide(code);
        }
        if (parametre == null)
            throw new Exception ("Dans la classe CalculTraitement, le parametre pour le point indice n'est pas defini");
        valeurPoint = parametre.pparMontant().doubleValue();
        if (valeurPoint == 0)
            throw new Exception ("Dans la classe CalculTraitement, la valeur du point indice n'est pas definie");
    }
    
}
