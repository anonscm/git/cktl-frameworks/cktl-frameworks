/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.papaye.server.calcul.remuneration;

import java.math.BigDecimal;

import org.cocktail.papaye.server.metier.jefy_paye.EOPayeCode;
import org.cocktail.papaye.server.metier.jefy_paye.EOPayeParam;



/** Fournit des methodes utilitaires de calcul de base lies au traitement d'un artiste qui permettent de determiner une remuneration brute en fonction du net:
<UL>traitementBrutPourNet</UL>
*/

public class CalculTraitementArtiste extends CalculTraitementGlobal {
    private static final String PLAFOND_JOURNALIER = "PLAJARTI";
    private static final String PLAFOND_HORAIRE = "PLAHOSS";
    private static final String PLAFOND_MENSUEL = "PLAFMSSS";

    /** Effectue le calcul du montant de la remuneration d'un artiste en fonction
        de son temps de travail (horaire, journalier, mensuel).
        Pour un artiste en remuneration mensuelle, le montant du contrat est le montant brut. */
    public void effectuerCalcul() throws Exception {
        // vérifier si le contrat est un cache isolé, groupé, mensuel
        BigDecimal montant;
        BigDecimal nbHeures = new BigDecimal(0);
        BigDecimal calcul = new BigDecimal(0);
        BigDecimal plafondSS = new BigDecimal(0);

        int nbJours = 0;

        if (contrat().montantForfaitaire() != null &&
            contrat().montantForfaitaire().doubleValue() > 0) {
            if (contrat().nbJoursContrat() != null &&
                contrat().nbJoursContrat().intValue() > 0) {
                nbJours = contrat().nbJoursContrat().intValue();
                calcul = contrat().montantForfaitaire().multiply(new BigDecimal(nbJours));
                if (nbJours < 5) {
                    plafondSS = (rechercherPlafondSS(PLAFOND_HORAIRE).multiply(new BigDecimal(12)).multiply(new BigDecimal(nbJours)));
                } else 
                    plafondSS = rechercherPlafondSS(PLAFOND_JOURNALIER).multiply(new BigDecimal(nbJours));
                
            } else if (contrat().nbHeuresContrat()  != null &&
                       contrat().nbHeuresContrat().doubleValue() > 0) {
                nbHeures = contrat().nbHeuresContrat();
                calcul = contrat().montantForfaitaire().multiply(nbHeures);
                plafondSS = rechercherPlafondSS(PLAFOND_HORAIRE).multiply(nbHeures);
            } else {
                calcul = contrat().montantForfaitaire();
                plafondSS = rechercherPlafondSS(PLAFOND_MENSUEL);
            }
        } else if (contrat().montantMensuelRemu() != null && contrat().montantMensuelRemu().doubleValue() != 0) {
            calcul = contrat().montantMensuelRemu();
            plafondSS = rechercherPlafondSS(PLAFOND_MENSUEL);

        } else {
            throw new Exception("Dans la classe "+ nomClasse() + "aucun montant n'est defini pour " + contrat().individu().identite());
        }
        if (contrat().estUnCalculDirect()) {
            montant = calcul.setScale(2,BigDecimal.ROUND_HALF_DOWN);
        } else {
            if (contrat().montantForfaitaire() != null &&
                contrat().montantForfaitaire().doubleValue() > 0) {
                montant = traitementBrutPourNet(contrat().montantForfaitaire().doubleValue(),
                                            plafondSS,true);
            } else {
                montant = traitementBrutPourNet(contrat().montantMensuelRemu().doubleValue(),
                                                plafondSS,true);
            }
        }
        // ajouter au résultat.
        // rechercher le code associé au traitement brut : assiette non significative
        EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),EOPayeCode.REMUN_BRUT);
        ajouterRemunerationMontant(code,montant);
        if (nbHeures.floatValue() > 0) {
            // stocker le nombre d'heures et le taux horaire
            mettreAJourPrepa(contrat().montantForfaitaire(),nbHeures);
        }
    }
    private BigDecimal rechercherPlafondSS(String codeString) throws Exception {
        EOPayeCode code = EOPayeCode.rechercherCode(editingContext(),codeString);
        EOPayeParam param = EOPayeParam.parametreValide(code);
        if (param == null) {
            throw new Exception("Dans la classe Remuneration_Artiste_NonCadre, le parametre "+  code + " n'est pas defini");
        }
        if (param.pparMontant().doubleValue() == 0) {
            throw new Exception("Dans la classe Remuneration_Artiste_NonCadre, la valeur du parametre "+  code + " n'est pas defini");
        }
        return param.pparMontant();
    }
    
}