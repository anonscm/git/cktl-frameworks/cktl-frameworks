/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcguiajax.server.controler;

import java.math.BigDecimal;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlgfcguiajax.server.component.CktlAjaxOrganSelect;
import org.cocktail.fwkcktlgfcguiajax.server.component.CktlAjaxOrganTreeView;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;

import er.ajax.AjaxTreeModel;

public class CktlAjaxOrganTreeViewCtrl extends CktlAjaxControler {

	private Object _delegate;
	private NSDictionary rootOrgan;
	private NSDictionary unOrgan;
	private static NSArray organs = null;

	private Integer lastExer;
	private String lastType;
	private Integer lastNiveauMax;

	public static final EOSortOrdering SORT_ORG_UNIV_ASC = EOSortOrdering.sortOrderingWithKey("ORG_UNIV", EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_ETAB_ASC = EOSortOrdering.sortOrderingWithKey("ORG_ETAB", EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_UB_ASC = EOSortOrdering.sortOrderingWithKey("ORG_UB", EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_CR_ASC = EOSortOrdering.sortOrderingWithKey("ORG_CR", EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_SOUSCR_ASC = EOSortOrdering.sortOrderingWithKey("ORG_SOUSCR", EOSortOrdering.CompareAscending);

	public static final NSArray SORT_ORGANS = new NSArray(new Object[] {
			SORT_ORG_UNIV_ASC, SORT_ORG_ETAB_ASC, SORT_ORG_UB_ASC, SORT_ORG_CR_ASC, SORT_ORG_SOUSCR_ASC
	});

	public CktlAjaxOrganTreeViewCtrl(CktlAjaxOrganTreeView component) {
		this(component, null);
	}

	public CktlAjaxOrganTreeViewCtrl(CktlAjaxOrganTreeView component, EOEditingContext edc) {
		super(component, edc);
		wocomponent = (CktlAjaxOrganTreeView) super.wocomponent;
	}

	public void chargerLesOrgans() {
		Integer exeOrdre = getExercice();
		lastExer = exeOrdre;
		Integer utlOrdre = (Integer) wocomponent.valueForBinding(CktlAjaxOrganSelect.UTL_ORDRE_BDG);
		Integer niveauMax = getNiveauMax();

		String type = getType();
		lastType = type;
		if (niveauMax == null) {
			niveauMax = Integer.valueOf(4);
		}
		lastNiveauMax = niveauMax;
		organs = FinderOrgan.getRawRowOrgan(edc, exeOrdre, utlOrdre, type, niveauMax);
		organs = EOSortOrdering.sortedArrayUsingKeyOrderArray(organs, EOOrgan.SORT_DEFAULT);
	}

	public WOActionResults onSelection() {
		NSDictionary organNode = unOrgan();
		if (organNode != null) {
			Number orgId = (Number) organNode.objectForKey("ORG_ID");
			EOOrgan organ = (EOOrgan) EOUtilities.objectWithPrimaryKey(edc, EOOrgan.ENTITY_NAME, new NSDictionary(orgId, EOOrgan.ORG_ID_KEY));
			((CktlAjaxOrganTreeView) wocomponent).setSelection(organ);
		}
		return null;
	}

	public NSDictionary rootOrgan() {
		if (rootOrgan == null || !getExercice().equals(lastExer) || !getType().equals(lastType) || (getNiveauMax() != null && !getNiveauMax().equals(lastNiveauMax))) {
			chargerLesOrgans();
			EOKeyValueQualifier qual = new EOKeyValueQualifier("ORG_NIV", EOKeyValueQualifier.QualifierOperatorEqual, BigDecimal.valueOf(0));
			NSArray array = EOQualifier.filteredArrayWithQualifier(organs, qual);
			if (array != null && array.count() == 1) {
				rootOrgan = (NSDictionary) array.lastObject();
			}
			else {
				rootOrgan = null;
			}
		}
		return rootOrgan;
	}

	public String onClickBefore() {
		return ((CktlAjaxOrganTreeView) wocomponent).stringValueForBinding(CktlAjaxOrganTreeView.BINDING_onClickBefore, CktlAjaxOrganTreeView.DEFAULT_onClickBefore);
		//return "confirm('Souhaitez-vous vraiment sélectionner cette ligne ? ')";
	}

	/**
	 * @param rootOrgan the rootOrgan to set
	 */
	public void setRootOrgan(NSDictionary rootOrgan) {
		this.rootOrgan = rootOrgan;
	}

	/*
	 * /**
	 * 
	 * @return the unOrgan
	 */
	public NSDictionary unOrgan() {
		return unOrgan;
	}

	/**
	 * @param unOrgan the unOrgan to set
	 */
	public void setUnOrgan(NSDictionary unOrgan) {
		this.unOrgan = unOrgan;
	}

	public String unOrganLibelle() {
		String unOrganLibelle = "";
		NSDictionary organ = unOrgan();
		if (organ != null) {
			Number niveau = (Number) organ.objectForKey("ORG_NIV");
			switch (niveau.intValue()) {
			case 4:
				unOrganLibelle = (String) organ.objectForKey("ORG_SOUSCR");
				break;
			case 3:
				unOrganLibelle = (String) organ.objectForKey("ORG_CR");
				break;
			case 2:
				unOrganLibelle = (String) organ.objectForKey("ORG_UB");
				break;
			case 1:
				unOrganLibelle = (String) organ.objectForKey("ORG_ETAB");
				break;

			default:
				unOrganLibelle = (String) organ.objectForKey("ORG_UNIV");
				break;
			}
		}

		if (((CktlAjaxOrganTreeView) wocomponent).afficherLibelles().booleanValue()) {
			if (!unOrganLibelle.equals(organ.objectForKey("ORG_LIB"))) {
				unOrganLibelle = unOrganLibelle + " (" + MyStringCtrl.compactString((String) organ.objectForKey("ORG_LIB"), 50, "...") + ")";
			}
		}

		return unOrganLibelle;
	}

	public Object delegate() {
		if (_delegate == null) {
			_delegate = new CktlAjaxOrganTreeViewCtrl.Delegate();
		}
		return _delegate;

	}

	public static class Delegate implements AjaxTreeModel.Delegate {

		public NSArray childrenTreeNodes(Object node) {
			NSArray childrenTreeNodes = null;
			NSDictionary treeNode = (NSDictionary) node;
			EOKeyValueQualifier qual = new EOKeyValueQualifier("ORG_PERE", EOKeyValueQualifier.QualifierOperatorEqual, treeNode.objectForKey("ORG_ID"));
			childrenTreeNodes = EOSortOrdering.sortedArrayUsingKeyOrderArray(EOQualifier.filteredArrayWithQualifier(organs, qual), SORT_ORGANS);

			return childrenTreeNodes;
		}

		public boolean isLeaf(Object node) {
			NSDictionary treeNode = (NSDictionary) node;
			Number niveau = (Number) treeNode.objectForKey("ORG_NIV");
			return niveau.intValue() == 4;
		}

		public Object parentTreeNode(Object node) {
			NSDictionary pere = null;
			NSDictionary treeNode = (NSDictionary) node;
			if (treeNode != null && treeNode.containsKey("ORG_PERE") && !treeNode.objectForKey("ORG_PERE").equals(NSKeyValueCoding.NullValue)) {
				Number orgIdPere = (Number) treeNode.objectForKey("ORG_PERE");
				EOKeyValueQualifier qual = new EOKeyValueQualifier("ORG_ID", EOKeyValueQualifier.QualifierOperatorEqual, orgIdPere);
				NSArray peres = EOQualifier.filteredArrayWithQualifier(organs, qual);
				if (peres != null && peres.count() == 1) {
					pere = (NSDictionary) peres.lastObject();
				}
			}

			return pere;
		}

	}

	public Integer getExercice() {
		return (Integer) wocomponent.valueForBinding(CktlAjaxOrganSelect.EXERCICE_BDG);
	}

	public Integer getNiveauMax() {
		return (Integer) wocomponent.valueForBinding(CktlAjaxOrganSelect.NIVEAU_MAX_BDG);
	}

	public String getType() {
		return (String) wocomponent.valueForBinding(CktlAjaxOrganSelect.TYPE_BDG);
	}
}
