/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcguiajax.server.controler;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.eocontrol.EOEditingContext;

public abstract class CktlAjaxControler {

	public WOComponent wocomponent;
	public EOEditingContext edc;

	public CktlAjaxControler() {
		this(null, null);
	}
	
	public CktlAjaxControler(WOComponent component) {
		this(component, null);
	}

	public CktlAjaxControler(WOComponent component, EOEditingContext ed) {
		wocomponent = component;
		edc = ed;
		
//		if (wocomponent != null) {
//			if (ed != null) {
//				edc = ed;
//			} else {
//				edc = wocomponent.session().defaultEditingContext();
//			}
//		}
	}

	public WOActionResults neFaitRien() {
		return null;
	}

	public WOComponent getWocomponent() {
		return wocomponent;
	}

	public void setWocomponent(WOComponent wocomponent) {
		this.wocomponent = wocomponent;
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

}
