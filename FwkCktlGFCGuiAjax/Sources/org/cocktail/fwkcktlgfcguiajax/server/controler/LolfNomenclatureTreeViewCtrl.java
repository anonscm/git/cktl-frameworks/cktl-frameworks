/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcguiajax.server.controler;

import java.util.Enumeration;
import java.util.Iterator;

import org.cocktail.fwkcktlgfcguiajax.server.component.lolf.LolfNomenclatureTreeView;
import org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureAbstract;
import org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense;
import org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureRecette;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxTreeModel;

public class LolfNomenclatureTreeViewCtrl {

	private LolfNomenclatureTreeView wocomponent;

	private Object _delegate;
	private LolfNomenclatureAbstractNode rootLolfNomenclatureAbstract;
	private LolfNomenclatureAbstractNode unNodeLolfNomenclatureAbstract;
	private LolfNomenclatureAbstractNode selectedNodeLolfNomenclatureAbstract;
	public AjaxTreeModel myTreeModel = new AjaxTreeModel();
	private EOQualifier qualifierForExercice;

	public LolfNomenclatureTreeViewCtrl() {

	}

	public LolfNomenclatureTreeViewCtrl(LolfNomenclatureTreeView component) {
		wocomponent = component;
	}

	private EOEditingContext edc() {
		return wocomponent.edc();
	}

	public LolfNomenclatureAbstractNode rootLolfNomenclatureAbstract() {
		if (rootLolfNomenclatureAbstract == null || (wocomponent.treeRootObject() != null && rootLolfNomenclatureAbstract.object() instanceof EmptyRootObject) || (wocomponent.treeRootObject() == null && !(rootLolfNomenclatureAbstract.object() instanceof EmptyRootObject))) {
			if (wocomponent.treeRootObject() != null) {
				rootLolfNomenclatureAbstract = new LolfNomenclatureAbstractNode(null, wocomponent.treeRootObject());
			}
			else {
				rootLolfNomenclatureAbstract = new LolfNomenclatureAbstractNode(null, new EmptyRootObject());
			}
		}
		return rootLolfNomenclatureAbstract;
	}

	public void setRootLolfNomenclatureAbstract(LolfNomenclatureAbstractNode rootLolfNomenclatureAbstract) {
		this.rootLolfNomenclatureAbstract = rootLolfNomenclatureAbstract;
		setSelectedNodeLolfNomenclatureAbstract(null);
	}

	public LolfNomenclatureAbstractNode unLolfNomenclatureAbstract() {
		return unNodeLolfNomenclatureAbstract;
	}

	public void setUnLolfNomenclatureAbstract(LolfNomenclatureAbstractNode unLolfNomenclatureAbstract) {
		this.unNodeLolfNomenclatureAbstract = unLolfNomenclatureAbstract;
	}

	public String unLolfNomenclatureAbstractLibelle() {
		NSKeyValueCoding obj = unLolfNomenclatureAbstract().object();
		return (String) obj.valueForKey(EOLolfNomenclatureAbstract.LOLF_CODE_KEY) + " (" + obj.valueForKey(EOLolfNomenclatureAbstract.LOLF_LIBELLE_KEY) + ")";
	}

	public Boolean isUnLolfNomenclatureAbstractSelectionnable() {
		return Boolean.TRUE;
	}

	public Object delegate() {
		if (_delegate == null) {
			_delegate = new LolfNomenclatureTreeViewCtrl.Delegate();
		}
		return _delegate;
	}

	public class Delegate implements AjaxTreeModel.Delegate {

		public NSArray childrenTreeNodes(Object node) {
			if (node != null) {
				return ((LolfNomenclatureAbstractNode) node).children();
			}
			return NSArray.EmptyArray;
		}

		public boolean isLeaf(Object node) {
			if (node != null) {
				return ((LolfNomenclatureAbstractNode) node).isLeaf();
			}
			return true;
		}

		public Object parentTreeNode(Object node) {
			if (node != null) {
				return ((LolfNomenclatureAbstractNode) node).parent();
			}
			return null;
		}
	}

	public class LolfNomenclatureAbstractNode {
		public NSArray children;
		public LolfNomenclatureAbstractNode parent;
		public NSKeyValueCoding object;

		public LolfNomenclatureAbstractNode(LolfNomenclatureAbstractNode parent, NSKeyValueCoding obj) {
			object = obj;
			this.parent = parent;
		}

		public NSArray children() {
			if (children == null) {
				NSArray res = NSArray.EmptyArray;
				if (object() instanceof EmptyRootObject) {
					if (EOLolfNomenclatureAbstract.MODE_RECETTE.equals(wocomponent.getMode())) {
						res = EOLolfNomenclatureRecette.getLolfNomenclaturesRacine(edc());
					}
					else {
						res = EOLolfNomenclatureDepense.getLolfNomenclaturesRacine(edc());
					}
				}
				else {
					EOLolfNomenclatureAbstract lolfNomenclature = ((EOLolfNomenclatureAbstract) object());
					res = EOSortOrdering.sortedArrayUsingKeyOrderArray(lolfNomenclature.lolfNomenclatureFils(getQualifierForExercice()), new NSArray(new Object[] {
							EOLolfNomenclatureAbstract.SORT_LOLF_CODE_ASC
					}));
					if (wocomponent.getQualifier() != null) {
						res = EOQualifier.filteredArrayWithQualifier(res, wocomponent.getQualifier());
					}

					//Balayer les resultats
					Iterator iter = res.iterator();
					NSMutableArray res2 = new NSMutableArray();
					while (iter.hasNext()) {
						EOLolfNomenclatureAbstract lolfNomenclatureFils = (EOLolfNomenclatureAbstract) iter.next();
						res2.addObject(lolfNomenclatureFils);
					}
					res = res2;
				}

				NSMutableArray res3 = new NSMutableArray();
				for (int i = 0; i < res.count(); i++) {
					//verifier si objet dans liste exclusions
					//if (wocomponent.getTreeExclusions()==null || wocomponent.getTreeExclusions().indexOf(res.objectAtIndex(i))==NSArray.NotFound) {

					LolfNomenclatureAbstractNode node = new LolfNomenclatureAbstractNode(this, (NSKeyValueCoding) res.objectAtIndex(i));
					res3.addObject(node);
					//}
				}
				children = res3.immutableClone();
			}
			return children;
		}

		public boolean isLeaf() {
			return (children().count() == 0);
		}

		public LolfNomenclatureAbstractNode parent() {
			return parent;
		}

		public void setParent(LolfNomenclatureAbstractNode node) {
			parent = node;
		}

		public NSKeyValueCoding object() {
			return object;
		}
	}

	public class EmptyRootObject implements NSKeyValueCoding {

		public EmptyRootObject() {
		}

		public void takeValueForKey(Object arg0, String arg1) {

		}

		public Object valueForKey(String arg0) {
			return "root";
		}

	}

	public LolfNomenclatureAbstractNode getSelectedNodeLolfNomenclatureAbstract() {
		return selectedNodeLolfNomenclatureAbstract;
	}

	public void setSelectedNodeLolfNomenclatureAbstract(LolfNomenclatureAbstractNode selectedNodeLolfNomenclatureAbstract) {
		this.selectedNodeLolfNomenclatureAbstract = selectedNodeLolfNomenclatureAbstract;
		if (selectedNodeLolfNomenclatureAbstract == null) {
			wocomponent.setSelection(null);
		}
		else {
			wocomponent.setSelection((EOLolfNomenclatureAbstract) getSelectedNodeLolfNomenclatureAbstract().object());
		}
	}

	/**
	 * Recherche le noeud correspondand à l'objet dans l'arbre et realise un expand sur les branches correspondantes.
	 * 
	 * @param node
	 * @param lolfNomenclature
	 */
	public LolfNomenclatureAbstractNode selectObjectInTree(LolfNomenclatureAbstractNode node, EOLolfNomenclatureAbstract lolfNomenclature) {
		LolfNomenclatureAbstractNode newSelectedNode = null;
		if (node.object().equals(lolfNomenclature)) {
			newSelectedNode = node;
		}
		else {
			Enumeration en = node.children().objectEnumerator();
			while (newSelectedNode == null && en.hasMoreElements()) {
				LolfNomenclatureAbstractNode tmpNode = (LolfNomenclatureAbstractNode) en.nextElement();
				newSelectedNode = selectObjectInTree(tmpNode, lolfNomenclature);
			}
		}
		if (newSelectedNode != null) {
			myTreeModel.setExpanded(node, true);
		}
		return newSelectedNode;
	}

	public LolfNomenclatureTreeView getWocomponent() {
		return wocomponent;
	}

	public void setWocomponent(LolfNomenclatureTreeView wocomponent) {
		this.wocomponent = wocomponent;
	}

	public void reset() {
		rootLolfNomenclatureAbstract().children = null;
	}

	public EOQualifier getQualifierForExercice() {
		if (qualifierForExercice == null) {
			setQualifierForExercice(EOLolfNomenclatureAbstract.getQualifierForExercice(Integer.valueOf(wocomponent.getCurrentExercice())));
		}
		return qualifierForExercice;
	}

	public void setQualifierForExercice(EOQualifier qualifierForExercice) {
		this.qualifierForExercice = qualifierForExercice;
	}

}
