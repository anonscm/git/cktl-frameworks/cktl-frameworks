/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcguiajax.server.controler;

// import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcguiajax.server.component.CktlAjaxOrganSelect;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;

import er.ajax.AjaxTreeModel;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @deprecated
 */
public class CktlAjaxOrganSelectCtrl extends CktlAjaxControler {

	private Object _delegate;
	private NSDictionary rootOrgan;
	private NSDictionary unOrgan;
	private Integer lastExer;
	private String lastType;
	private Integer lastNiveauMax;
	private static NSArray organs = null;

	public CktlAjaxOrganSelectCtrl(CktlAjaxOrganSelect component) {
		this(component, null);
	}

	public CktlAjaxOrganSelectCtrl(CktlAjaxOrganSelect component, EOEditingContext edc) {
		super(component, edc);
		wocomponent = super.wocomponent;
	}

	public WOActionResults chargerLesOrgans() {
		if (organs == null || !getExercice().equals(lastExer) || !getType().equals(lastType) || lastNiveauMax == null || !getNiveauMax().equals(lastNiveauMax)) {
			Integer exeOrdre = getExercice();
			lastExer = exeOrdre;
			Integer utlOrdre = (Integer) wocomponent.valueForBinding(CktlAjaxOrganSelect.UTL_ORDRE_BDG);
			Integer niveauMax = getNiveauMax();
			lastNiveauMax = niveauMax;
			String type = getType();
			lastType = type;
			if (niveauMax == null) {
				niveauMax = Integer.valueOf(4);
			}
			organs = FinderOrgan.getRawRowOrgan(edc, exeOrdre, utlOrdre, type, niveauMax);
			EOSortOrdering sortOrderingNiveau = EOSortOrdering.sortOrderingWithKey("ORG_NIV", EOSortOrdering.CompareAscending);
			EOSortOrdering sortOrderingUniv = EOSortOrdering.sortOrderingWithKey("ORG_UNIV", EOSortOrdering.CompareAscending);
			EOSortOrdering sortOrderingEtab = EOSortOrdering.sortOrderingWithKey("ORG_ETAB", EOSortOrdering.CompareAscending);
			EOSortOrdering sortOrderingUB = EOSortOrdering.sortOrderingWithKey("ORG_UB", EOSortOrdering.CompareAscending);
			EOSortOrdering sortOrderingCR = EOSortOrdering.sortOrderingWithKey("ORG_CR", EOSortOrdering.CompareAscending);
			EOSortOrdering sortOrderingSsCR = EOSortOrdering.sortOrderingWithKey("ORG_SOUSCR", EOSortOrdering.CompareAscending);
			NSArray<EOSortOrdering> orderings = new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					sortOrderingNiveau, sortOrderingUniv, sortOrderingEtab, sortOrderingUB, sortOrderingCR, sortOrderingSsCR
			});
			organs = EOSortOrdering.sortedArrayUsingKeyOrderArray(organs, orderings);
		}
		return null;
	}

	public WOActionResults afficherOrganSelectionne() {
		NSDictionary organNode = unOrgan();
		if (organNode != null) {
			Number orgId = (Number) organNode.objectForKey("ORG_ID");
			EOOrgan organ = (EOOrgan) EOUtilities.objectWithPrimaryKey(edc, EOOrgan.ENTITY_NAME, new NSDictionary(orgId, "orgId"));
			((CktlAjaxOrganSelect) wocomponent).setSelection(organ);
			// AjaxModalDialog.close(wocomponent.context());
		}
		return null;
	}

	public NSDictionary rootOrgan() {
		if (rootOrgan == null || !getExercice().equals(lastExer) || !getType().equals(lastType) || lastNiveauMax == null || !getNiveauMax().equals(lastNiveauMax)) {
			chargerLesOrgans();
			EOKeyValueQualifier qual = new EOKeyValueQualifier("ORG_NIV", EOKeyValueQualifier.QualifierOperatorEqual, Integer.valueOf(0));
			NSArray array = EOQualifier.filteredArrayWithQualifier(organs, qual);
			if (array != null && array.count() == 1) {
				rootOrgan = (NSDictionary) array.lastObject();
			}
			else {
				rootOrgan = null;
			}

		}
		return rootOrgan;
	}

	/**
	 * @param rootOrgan the rootOrgan to set
	 */
	public void setRootOrgan(NSDictionary rootOrgan) {
		this.rootOrgan = rootOrgan;
	}

	/*
	 * /**
	 * 
	 * @return the unOrgan
	 */
	public NSDictionary unOrgan() {
		return unOrgan;
	}

	/**
	 * @param unOrgan the unOrgan to set
	 */
	public void setUnOrgan(NSDictionary unOrgan) {
		this.unOrgan = unOrgan;
	}

	public String unOrganLibelle() {
		String unOrganLibelle = "";
		NSDictionary organ = unOrgan();
		if (organ != null) {
			Number niveau = (Number) organ.objectForKey("ORG_NIV");
			switch (niveau.intValue()) {
			case 4:
				unOrganLibelle = (String) organ.objectForKey("ORG_SOUSCR");
				break;
			case 3:
				unOrganLibelle = (String) organ.objectForKey("ORG_CR");
				break;
			case 2:
				unOrganLibelle = (String) organ.objectForKey("ORG_UB");
				break;
			case 1:
				unOrganLibelle = (String) organ.objectForKey("ORG_ETAB");
				break;

			default:
				unOrganLibelle = (String) organ.objectForKey("ORG_UNIV");
				break;
			}
		}
		return unOrganLibelle;
	}

	public Object delegate() {
		if (_delegate == null) {
			_delegate = new CktlAjaxOrganSelectCtrl.Delegate();
		}
		return _delegate;

	}

	public static class Delegate implements AjaxTreeModel.Delegate {

		public NSArray childrenTreeNodes(Object node) {
			NSArray childrenTreeNodes = null;
			NSDictionary treeNode = (NSDictionary) node;
			EOKeyValueQualifier qual = new EOKeyValueQualifier("ORG_PERE", EOKeyValueQualifier.QualifierOperatorEqual, treeNode.objectForKey("ORG_ID"));
			childrenTreeNodes = EOQualifier.filteredArrayWithQualifier(organs, qual);

			return childrenTreeNodes;
		}

		public boolean isLeaf(Object node) {
			NSDictionary treeNode = (NSDictionary) node;
			Number niveau = (Number) treeNode.objectForKey("ORG_NIV");
			return niveau.intValue() == 4;
		}

		public Object parentTreeNode(Object node) {
			NSDictionary pere = null;
			NSDictionary treeNode = (NSDictionary) node;
			if (treeNode != null && treeNode.containsKey("ORG_PERE") && !treeNode.objectForKey("ORG_PERE").equals(NSKeyValueCoding.NullValue)) {
				Number orgIdPere = (Number) treeNode.objectForKey("ORG_PERE");
				EOKeyValueQualifier qual = new EOKeyValueQualifier("ORG_ID", EOKeyValueQualifier.QualifierOperatorEqual, orgIdPere);
				NSArray peres = EOQualifier.filteredArrayWithQualifier(organs, qual);
				if (peres != null && peres.count() == 1) {
					pere = (NSDictionary) peres.lastObject();
				}
			}

			return pere;
		}
	}

	public Integer getExercice() {
		return (Integer) wocomponent.valueForBinding(CktlAjaxOrganSelect.EXERCICE_BDG);
	}

	public Integer getNiveauMax() {
		return (Integer) wocomponent.valueForBinding(CktlAjaxOrganSelect.NIVEAU_MAX_BDG);
	}

	public String getType() {
		return (String) wocomponent.valueForBinding(CktlAjaxOrganSelect.TYPE_BDG);
	}
	//FIXME ajouter la possibilite d'interdire la selection d'un noeud
}
