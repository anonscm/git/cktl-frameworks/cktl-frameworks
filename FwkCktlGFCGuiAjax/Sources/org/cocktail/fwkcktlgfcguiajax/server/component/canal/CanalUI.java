/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcguiajax.server.component.canal;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlgfcguiajax.server.component.ACktlAjaxGFCComponent;
import org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/**
 * Gère l'affichage et la création/modification des codes analytiques. Ce composant doit être intégré dans une FORM.
 * 
 * @binding selectedCodeAnalytique Obligatoire. L'objet {@link EOCodeAnalytique} qui sera sélectionné
 * @binding currentExercice
 * @binding editingContext
 * @binding utilisateurPersId
 * @binding isReadOnly
 * @binding isEditing
 * @binding selectedCodeAnalytique
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CanalUI extends ACktlAjaxGFCComponent {

	private static final long serialVersionUID = 1L;

	/** Obligatoire. L'objet {@link EOCodeAnalytique} sélectionné. */
	public static final String BINDING_selectedCodeAnalytique = "selectedCodeAnalytique";

	//public static final String BINDING_preselectedOrgan = "preselectedOrgan";

	private EOEditingContext _editingContext;
	private EOCodeAnalytique editingCodeAnalytique;
	private EOCodeAnalytique selectedCodeAnalytique;
	public EOCodeAnalytique unCodeAnalytique;

	public EOQualifier treeQualifier = null;

	private EOOrgan selectedOrgan;
	public EOOrgan unOrgan;

	private NSArray organsAutorisees = null;

	private Boolean isEditingNew = Boolean.FALSE;

	private Boolean hideCodesNonOuvertsSurExercice = Boolean.TRUE;

	private Boolean resetTree = Boolean.FALSE;

	public CanalUI(WOContext context) {
		super(context);
	}

	public EOEditingContext edc() {
		if (_editingContext == null) {
			if (valueForBinding(BINDING_editingContext) == null) {
				throw new RuntimeException("Le binding " + BINDING_editingContext + " est null.");
			}
			_editingContext = (EOEditingContext) valueForBinding(BINDING_editingContext);
		}
		return _editingContext;
	}

	public String canalViewContainerId() {
		return getComponentId() + "_CanalViewContainer";
	}

	public WOActionResults onCanalCreer() {
		EOEditingContext ed = edc();
		ed.revert();
		editingCodeAnalytique = EOCodeAnalytique.creerInstanceForParent(ed, getSelectedCodeAnalytique());
		editingCodeAnalytique.setUtilisateurRelationship(getAppUser().getUtilisateur());
		editingCodeAnalytique.setTypeEtatPublicRelationship(EOTypeEtat.getTypeEtat(ed, EOTypeEtat.ETAT_NON));
		editingCodeAnalytique.setTypeEtatDepassementRelationship(EOTypeEtat.getTypeEtat(ed, EOTypeEtat.ETAT_NE_RIEN_FAIRE));
		setIsEditing(Boolean.TRUE);
		isEditingNew = Boolean.TRUE;
		return null;
	}

	/**
	 * Suppression d'un code analytique. Fonctionne (normalement) mais pas implémenté. Si besoin de l'utiliser, vérifier que le code n'est pas
	 * affecté.
	 */
	public WOActionResults onCanalSupprimer() {
		EOEditingContext ed = edc();
		ed.revert();
		EOCodeAnalytique canalPere = getSelectedCodeAnalytique().codeAnalytiquePere();

		try {
			editingCodeAnalytique = getSelectedCodeAnalytique();
			EOCodeAnalytique.supprimer(ed, editingCodeAnalytique);
			ed.saveChanges();

			setSelectedCodeAnalytique(canalPere);
			setResetTree(Boolean.TRUE);

		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			ed.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}

		return null;
	}

	public WOActionResults onCanalModifier() {
		if (getSelectedCodeAnalytique() == null) {
			return null;
		}
		EOEditingContext ed = edc();
		ed.revert();
		editingCodeAnalytique = getSelectedCodeAnalytique();

		setIsEditing(Boolean.TRUE);
		isEditingNew = Boolean.FALSE;
		return null;
	}

	public EOCodeAnalytique getSelectedCodeAnalytique() {
		if (hasBinding(BINDING_selectedCodeAnalytique)) {
			EOCodeAnalytique rpa = (EOCodeAnalytique) valueForBinding(BINDING_selectedCodeAnalytique);
			if (rpa != null && !edc().equals(rpa.editingContext())) {
				rpa = rpa.localInstanceIn(edc());
			}
			selectedCodeAnalytique = rpa;
		}
		return selectedCodeAnalytique;
	}

	public Boolean isCanalCreerEnabled() {
		return Boolean.valueOf(getSelectedCodeAnalytique() != null && (getAppUser().hasDroitSaisieCodeAnalytiquesAll() || getAppUser().hasDroitSaisieCodeAnalytiquesPrives()));
	}

	public Boolean isCanalModifierEnabled() {
		return Boolean.valueOf(getSelectedCodeAnalytique() != null && !getSelectedCodeAnalytique().isPublic() && (getAppUser().hasDroitSaisieCodeAnalytiquesAll() || getAppUser().hasDroitSaisieCodeAnalytiquesPrives()));
	}

	public Boolean isCodeAnalytiqueSelected() {
		return Boolean.valueOf(getSelectedCodeAnalytique() != null);
	}

	public Boolean isCanalSupprimerEnabled() {
		return Boolean.valueOf(getSelectedCodeAnalytique() != null);
		//		return Boolean.TRUE;
	}

	//	public Boolean hasData() {
	//		NSArray res = getLesCodeAnalytique();
	//		return (res != null && res.count() > 0);
	//	}
	//
	//	public NSArray getLesCodeAnalytique() {
	//		EOOrgan organ = getSelectedOrgan();
	//		if (organ == null) {
	//			return NSArray.EmptyArray;
	//		}
	//		EOQualifier qual = new EOKeyValueQualifier(EOCodeAnalytique.CODE_ANALYTIQUE_ORGANS_KEY + "." + EOCodeAnalytiqueOrgan.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, organ);
	//		NSArray sort = new NSArray(new Object[] {
	//				EOCodeAnalytique.SORT_CAN_NIVEAU_ASC, EOCodeAnalytique.SORT_CAN_CODE_ASC
	//		});
	//		NSArray res = EOCodeAnalytique.fetchAll(edc(), new EOAndQualifier(new NSArray(new Object[] {
	//				qual, EOCodeAnalytique.QUAL_CODE_ANALYTIQUE_VALIDE
	//		})), sort);
	//		return res;
	//	}

	//	public String getCurrentExercice() {
	//		return (String) valueForBinding(BINDING_currentExercice);
	//	}

	//	public NSArray getLesOrgansAutorisees() {
	//		if (organsAutorisees == null) {
	//			organsAutorisees = getAppUser().getOrgansAutorisees(edc(), getCurrentExercice());
	//		}
	//		return organsAutorisees;
	//	}
	//
	//	public EOOrgan getSelectedOrgan() {
	//		if (selectedOrgan == null && hasBinding(BINDING_preselectedOrgan)) {
	//			selectedOrgan = (EOOrgan) valueForBinding(BINDING_preselectedOrgan);
	//		}
	//		return selectedOrgan;
	//	}

	//	public void setSelectedOrgan(EOOrgan selectedOrgan) {
	//		this.selectedOrgan = selectedOrgan;
	//	}

	public String getLibelleForUnCodeAnalytique() {
		return MyStringCtrl.compactString(unCodeAnalytique.canCode() + " (" + unCodeAnalytique.canLibelle(), 40, "...") + ")";
	}

	//
	//	public String getLibelleForUnOrgan() {
	//		return MyStringCtrl.compactString(unOrgan.getLongString(), 40, "...");
	//	}

	public WOActionResults onCanalFormAnnuler() {
		EOEditingContext ec = edc();
		setErreurSaisieMessage(null);
		ec.revert();
		if (editingCodeAnalytique.hasTemporaryGlobalID()) {
			setSelectedCodeAnalytique(null);
		}
		editingCodeAnalytique = null;
		//initialiseSelectedAdresse();
		setIsEditing(Boolean.FALSE);
		isEditingNew = Boolean.FALSE;
		return null;
	}

	public WOActionResults onCanalFormEnregistrer() {
		EOEditingContext ec = edc();
		setErreurSaisieMessage(null);
		try {
			if (ec != null && ec.hasChanges()) {
				editingCodeAnalytique.setUtilisateurRelationship(getAppUser().getUtilisateur());
				ec.saveChanges();
			}
			setSelectedCodeAnalytique(editingCodeAnalytique);
			if (isEditingNew().booleanValue()) {
				setResetTree(Boolean.TRUE);
			}
			setIsEditing(Boolean.FALSE);
			isEditingNew = Boolean.FALSE;
		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public void setSelectedCodeAnalytique(EOCodeAnalytique selectedCodeAnalytique) {
		this.selectedCodeAnalytique = selectedCodeAnalytique;
		if (hasBinding(BINDING_selectedCodeAnalytique) && canSetValueForBinding(BINDING_selectedCodeAnalytique)) {
			setValueForBinding(selectedCodeAnalytique, BINDING_selectedCodeAnalytique);
		}
	}

	public EOCodeAnalytique getRootCodeAnalytique() {
		return (EOCodeAnalytique) EOCodeAnalytique.getCodeAnalytiquesRacine(edc()).objectAtIndex(0);
	}

	public EOCodeAnalytique getEditingCodeAnalytique() {
		return editingCodeAnalytique;
	}

	public void setEditingCodeAnalytique(EOCodeAnalytique editingCodeAnalytique) {
		this.editingCodeAnalytique = editingCodeAnalytique;
	}

	public Boolean isEditingNew() {
		return isEditingNew;
	}

	public EOQualifier getTreeQualifier() {
		EOQualifier qual = null;
		if (hideCodesNonOuvertsSurExercice.booleanValue()) {
			qual = EOCodeAnalytique.qualCanalOuverts(EOExercice.getExercice(edc(), Integer.valueOf(getCurrentExercice())));
		}
		return qual;
	}

	public Boolean getHideCodesNonOuvertsSurExercice() {
		return hideCodesNonOuvertsSurExercice;
	}

	public void setHideCodesNonOuvertsSurExercice(Boolean hideCodesNonOuvertsSurExercice) {
		this.hideCodesNonOuvertsSurExercice = hideCodesNonOuvertsSurExercice;
		setResetTree(Boolean.TRUE);
		setSelectedCodeAnalytique(null);
	}

	public Boolean getResetTree() {
		return resetTree;
	}

	public void setResetTree(Boolean resetTree) {
		this.resetTree = resetTree;
	}

}
