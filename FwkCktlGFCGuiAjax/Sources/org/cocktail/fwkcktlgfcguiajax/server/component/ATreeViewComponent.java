/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfcguiajax.server.component;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class ATreeViewComponent extends ACktlAjaxGFCComponent {
	public static final String SELECTION_BDG = "selection";
	public static final String UPDATE_CONTAINER_ID_BDG = "updateContainerID";

	public static final String TREE_VIEW_TITLE_BDG = "treeViewTitle";
	public static final String TREE_VIEW_WIDTH_BDG = "treeViewWidth";
	public static final String TREE_VIEW_HEIGHT_BDG = "treeViewHeight";
	public static final String TREE_VIEW_CLASS_NAME_BDG = "treeViewClassName";

	/**
	 * Facultatif. Binding pour specifier le qualifier a appliquer pour recuperer les enfants d'un noeud. Ce qualifier doit être applicable aux objets
	 * EOStructure. Si non null, le qualifier se rajoute a l'eventuel filtre selectionne par l'utilisateur.
	 */
	public static final String TREE_QUALIFIER_BDG = "treeQualifier";

	/** Facultatif. Binding pour specifier la racine de l'arbre. */
	public static final String TREE_ROOT_OBJECT_BDG = "treeRootObject";

	/** Facultatif. Permet de specifier un NSArray d'objets a exclure de l'affichage de l'arbre. */
	public static final String TREE_EXCLUSIONS_BDG = "treeExclusions";

	public static final String TREE_COLLAPSED_IMG_BDG = "treeCollapsedImage";
	public static final String TREE_COLLAPSED_IMG_FWK_BDG = "treeCollapsedImageFramework";
	public static final String TREE_EXPANDED_IMG_BDG = "treeExpandedImage";
	public static final String TREE_EXPANDED_IMG_FWK_BDG = "treeExpandedImageFramework";
	public static final String TREE_LEAF_IMG_BDG = "treeLeafImage";
	public static final String TREE_LEAF_IMG_FWK_BDG = "treeLeafImageFramework";

	protected String collapsedImage, collapsedImageFramework;
	protected String expandedImage, expandedImageFramework;
	protected String leafImage, leafImageFramework;
	protected EOQualifier treeQualifier;

	public ATreeViewComponent(WOContext context) {
		super(context);
	}

	/**
	 * @return the collapsedImage
	 */
	public String getTreeCollapsedImage() {
		if (collapsedImage == null) {
			collapsedImage = (String) valueForBinding(TREE_COLLAPSED_IMG_BDG);
			if (collapsedImage == null) {
			collapsedImage = "images/ico_node_collapsed_gris_16.png";
				collapsedImageFramework = "FwkCktlAjaxWebExt.framework";
			}
		}

		return collapsedImage;
	}

	/**
	 * @param collapsedImage the collapsedImage to set
	 */
	public void setTreeCollapsedImage(String collapsedImage) {
		this.collapsedImage = collapsedImage;
	}

	/**
	 * @return the collapsedImageFramework
	 */
	public String getTreeCollapsedImageFramework() {
		if (collapsedImageFramework == null) {
			collapsedImageFramework = (String) valueForBinding(TREE_COLLAPSED_IMG_FWK_BDG);
		}

		return collapsedImageFramework;
	}

	/**
	 * @param collapsedImageFramework the collapsedImageFramework to set
	 */
	public void setTreeCollapsedImageFramework(String collapsedImageFramework) {
		this.collapsedImageFramework = collapsedImageFramework;
	}

	/**
	 * @return the expandedImage
	 */
	public String getTreeExpandedImage() {
		if (expandedImage == null) {
			expandedImage = (String) valueForBinding(TREE_EXPANDED_IMG_BDG);
			if (expandedImage == null) {
				expandedImage = "images/ico_node_expanded_gris_16.png";
				expandedImageFramework = "FwkCktlAjaxWebExt.framework";
			}
		}
		return expandedImage;
	}

	/**
	 * @param expandedImage the expandedImage to set
	 */
	public void setTreeExpandedImage(String expandedImage) {
		this.expandedImage = expandedImage;
	}

	/**
	 * @return the expandedImageFramework
	 */
	public String getTreeExpandedImageFramework() {
		if (expandedImageFramework == null) {
			expandedImageFramework = (String) valueForBinding(TREE_EXPANDED_IMG_FWK_BDG);
		}
		return expandedImageFramework;
	}

	/**
	 * @param expandedImageFramework the expandedImageFramework to set
	 */
	public void setTreeExpandedImageFramework(String expandedImageFramework) {
		this.expandedImageFramework = expandedImageFramework;
	}

	/**
	 * @return the leafImage
	 */
	public String getTreeLeafImage() {
		if (leafImage == null) {
			leafImage = (String) valueForBinding(TREE_LEAF_IMG_BDG);
			if (leafImage == null) {
				leafImage = "images/ico_node_leaf_gris_16.png";
				leafImageFramework = "FwkCktlAjaxWebExt.framework";
			}
		}
		return leafImage;
	}

	/**
	 * @param leafImage the leafImage to set
	 */
	public void setTreeLeafImage(String leafImage) {
		this.leafImage = leafImage;
	}

	/**
	 * @return the leafImageFramework
	 */
	public String getTreeLeafImageFramework() {
		if (leafImageFramework == null) {
			leafImageFramework = (String) valueForBinding(TREE_LEAF_IMG_FWK_BDG);
		}
		return leafImageFramework;
	}

	/**
	 * @param leafImageFramework the leafImageFramework to set
	 */
	public void setTreeLeafImageFramework(String leafImageFramework) {
		this.leafImageFramework = leafImageFramework;
	}

	public EOQualifier getTreeQualifier() {
		treeQualifier = (EOQualifier) valueForBinding(TREE_QUALIFIER_BDG);
		return treeQualifier;
	}

	public void setTreeViewQualifier(EOQualifier treeViewQualifier) {
		this.treeQualifier = treeViewQualifier;
	}

	public String updateContainerID() {
		return (String) valueForBinding(UPDATE_CONTAINER_ID_BDG);
	}

	public String getJsForUpdateContainerUpdate() {
		if (updateContainerID() != null) {
			return "function(){" + updateContainerID() + "Update();}";
		}
		return null;

	}
}
