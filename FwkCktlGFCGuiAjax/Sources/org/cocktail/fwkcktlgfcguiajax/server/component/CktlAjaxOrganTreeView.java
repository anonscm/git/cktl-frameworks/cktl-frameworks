/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software is governed by the CeCILL license under French law and abiding by the rules of distribution
 * of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL "http://www.cecill.info". As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license,
 * users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that
 * it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to
 * use and operate it in the same conditions as regards security. The fact that you are presently reading this means that you have had knowledge of the CeCILL
 * license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcguiajax.server.component;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcguiajax.server.controler.CktlAjaxOrganTreeViewCtrl;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;

import er.extensions.eof.ERXEOControlUtilities;

/**
 * @binding exercice Integer
 * @binding utilisateur Integer : utlOrdre
 * @binding niveauMax Integer (4 par defaut)
 * @binding onSuccessSelect
 * @binding onSelect
 * @binding onClickBefore
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlAjaxOrganTreeView extends ATreeViewComponent {
	// public static final String SELECTION_BDG = "selection";

	// public static final String EDC_BDG = "editingContext";

	public static final String EXERCICE_BDG = "exercice";
	public static final String UTL_ORDRE_BDG = "utilisateur";
	public static final String NIVEAU_MAX_BDG = "niveauMax";
	public static final String CLASS_BDG = "class";
	public static final String STYLE_BDG = "style";
	public static final String TYPE_BDG = "type";
	public static final String BINDING_allExpanded = "allExpanded";
	public static final String BINDING_onSuccessSelect = "onSuccessSelect";
	public static final String BINDING_onSelect = "onSelect";
	public static final String BINDING_onClickBefore = "onClickBefore";
	public static final String BINDING_ctrl = "ctrl";
	public static final String DEFAULT_onClickBefore = "confirm('Souhaitez-vous vraiment sélectionner cette ligne ? ')";

	/** Facultatif. Indique s'il faut afficher le libellé des branches de l'organigramme */
	public static final String BINDING_afficherLibelles = "afficherLibelles";

	private static final Boolean DEFAULT_afficherLibelles = Boolean.TRUE;

	private CktlAjaxOrganTreeViewCtrl ctrl = null;
	private EOOrgan selection;
	private Boolean allExpanded = Boolean.FALSE;
	private EOEditingContext ed;
	
	public CktlAjaxOrganTreeView(WOContext context) {
		super(context);
		ed = (EOEditingContext) valueForBinding(CktlAjaxOrganSelect.EDC_BDG);
		if (ed == null)
			ed = context.session().defaultEditingContext();

	}

	/**
	 * @return the ctrl
	 */
	public CktlAjaxOrganTreeViewCtrl ctrl() {
		if(ctrl == null) {
			if(hasBinding(BINDING_ctrl)) {
				ctrl = (CktlAjaxOrganTreeViewCtrl) valueForBinding(BINDING_ctrl);
			} else {
				ctrl = new CktlAjaxOrganTreeViewCtrl(this, ed);
			}
		}
		return ctrl;
	}

	/**
	 * @param ctrl
	 *            the ctrl to set
	 */
	public void setCtrl(CktlAjaxOrganTreeViewCtrl ctrl) {
		this.ctrl = ctrl;
	}

	/**
	 * @return the organSelectionne
	 */
	public EOOrgan selection() {
		return selection;
	}

	public void setSelection(EOOrgan selection) {
		this.selection = selection;
		setValueForBinding(selection, SELECTION_BDG);
		if (hasBinding(BINDING_onSelect)) {
			performParentAction(BINDING_onSelect);
		}
		if (selection != null) {
			NSMutableDictionary userInfo = new NSMutableDictionary();
			userInfo.setObjectForKey(session(), "session");
			NSNotificationCenter.defaultCenter().postNotification("organTreeViewChangeSelectionNotification", selection, userInfo);	
		}
	}

	public void onClose() {
		// ctrl().setRootOrgan(null);
		// String toto = pageOrganTreeView.organSelectionne();
	}

	public EOOrgan treeRootObject() {
		return (EOOrgan) valueForBinding(TREE_ROOT_OBJECT_BDG);
	}

	/*
	 * Pour test : ouverture d'une fenetre contenant une page et non plus une partie d'une page Commente pour eviter des pbs de compilation si
	 * CktlAjaxPageOrganTreeView non present
	 */
	public WOActionResults openWinOrgan() {
		return null;
	}

	public String containerAjaxTreeID() {
		return getComponentId() + "_containerAjaxTree";
	}

	public String unAjaxTreeID() {
		return getComponentId() + "_ajaxTree";
	}

	public String onSuccessSelect() {
		return (String) valueForBinding(BINDING_onSuccessSelect);
	}

	public Boolean afficherLibelles() {
		if (hasBinding(BINDING_afficherLibelles)) {
			return (Boolean) valueForBinding(BINDING_afficherLibelles);
		}
		return DEFAULT_afficherLibelles;
	}

	public Boolean isNoData() {
		return Boolean.valueOf(ctrl().rootOrgan() == null);
	}

	public String cssClassForSelectedLink() {
		String cssClass = "";
		if (selection() != null) {
			Integer orgIdSelection = (Integer) ERXEOControlUtilities.primaryKeyObjectForObject(selection());
			Integer orgIdCurrent = ((BigDecimal) ctrl().unOrgan().objectForKey("ORG_ID")).intValue();
			cssClass = orgIdSelection.equals(orgIdCurrent) ? "selected" : cssClass;
		}
		return cssClass;
	}

	/**
	 * @return the allExpanded
	 */
	public Boolean allExpanded() {
		return booleanValueForBinding(BINDING_allExpanded, Boolean.FALSE);
	}

	/**
	 * @param allExpanded
	 *            the allExpanded to set
	 */
	public void setAllExpanded(Boolean allExpanded) {
		this.allExpanded = allExpanded;
	}

	public String onClickLinkSelection() {
		String onClickLinkSelection = "";
		onClickLinkSelection += "var selections = $$('#" + unAjaxTreeID()
				+ " li a.selected'); if (selections.size()>0){selections.first().removeClassName('selected');};";
		onClickLinkSelection += "this.addClassName('selected');";

		return onClickLinkSelection;
	}

}
