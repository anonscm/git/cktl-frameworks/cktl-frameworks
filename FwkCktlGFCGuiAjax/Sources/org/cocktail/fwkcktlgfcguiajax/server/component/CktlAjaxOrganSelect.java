/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcguiajax.server.component;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxSelectWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * Affiche un champ avec possibilite de selectionner une ligne budgetaire dans un arbre.
 * 
 * @binding exercice Integer (exercice en cours)
 * @binding utilisateur Integer (utl_ordre de l'utilisateur)
 * @binding selection Will set. EOOrgan selectionné
 * @binding value KeyValueCoding pour afficher la selection
 * @binding editingContext
 * @binding id
 * @binding treeViewTitle
 * @binding type DEPENSE, RECETTE ou BUDGET
 * @binding btSrchType Le type du bouton de recherche, par défaut "find"
 * @binding updateContainerIDOnSupprimer
 * @binding updateContainerID
 * @binding showField Affiche ou non le champ qui affiche la selection
 * @binding displayDeleteButton Affiche ou non le bouton de suppression
 * @binding onClickBeforeSelection Indiquez "true" si vous ne voulez pas de message de confirmation, ou bien indiquez l'appel à une fonction JS genre
 *          confirm()
 * @see CktlAjaxOrganTreeView
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlAjaxOrganSelect extends CktlAjaxSelectWOComponent {

	private static final long serialVersionUID = 1L;
	public static final String SELECTION_BDG = "selection";
	public static final String VALUE_BDG = "value";
	public static final String EDC_BDG = "editingContext";

	public static final String EXERCICE_BDG = "exercice";
	public static final String UTL_ORDRE_BDG = "utilisateur";
	public static final String NIVEAU_MAX_BDG = "niveauMax";
	public static final String CLASS_BDG = "class";
	public static final String STYLE_BDG = "style";
	public static final String TYPE_BDG = "type";
	public static final String BINDING_BTSRCH_ICON_SIZE = "btSrchIconSize";
	public static final String BINDING_BTSRCH_TYPE = "btSrchType";

	/** Affiche le champ texte */
	public static final String BINDING_showField = "showField";

	//private CktlAjaxOrganSelectCtrl ctrl;
	private EOOrgan selection;

	public CktlAjaxOrganSelect(WOContext context) {
		super(context);
	}

	/**
	 * @return the organSelectionne
	 */
	public EOOrgan selection() {
		if (hasBinding(SELECTION_BDG)) {
			selection = (EOOrgan) valueForBinding(SELECTION_BDG);
		}
		return selection;
	}

	public void setSelection(EOOrgan selection) {
		this.selection = selection;
		setValueForBinding(selection, CktlAjaxOrganSelect.SELECTION_BDG);
	}

	public WOActionResults onSelect() {
		CktlAjaxWindow.close(context());
		return null;
	}

	public void onClose() {
		// ctrl().setRootOrgan(null);
	}

	public EOOrgan treeRootObject() {
		return (EOOrgan) valueForBinding(TREE_ROOT_OBJECT_BDG);
	}

	public String afterHide() {
		String afterHide = null;
		if (showField().booleanValue()) {
			afterHide = "function () {";
			afterHide += containerComponentId() + "Update();";
			afterHide += "}";
		}
		return afterHide;
	}

	public String modalOrganTreeViewID() {
		return getComponentId() + "_" + "modalOrganTreeViewID";
	}

	public Boolean showField() {
		if (hasBinding(BINDING_showField)) {
			return (Boolean) valueForBinding(BINDING_showField);
		}
		return Boolean.TRUE;
	}

	//	public String btSrchIconSize() {
	//		if (hasBinding(BINDING_BTSRCH_ICON_SIZE)) {
	//			return (String) valueForBinding(BINDING_BTSRCH_ICON_SIZE);
	//		}
	//		return CktlAjaxActionButton.ICON_SIZE_BIG;
	//	}

	public String btSrchType() {
	    return stringValueForBinding(BINDING_BTSRCH_TYPE, "find");
	}
	
	public String btSrchId() {
		return getComponentId() + "_btSrch";
	}

	public String onClick() {
		String onClick = "openCAW_";

		onClick += modalOrganTreeViewID() + "('Sélectionner une branche de l\\'organigramme');return false;";

		return onClick;
	}

}
