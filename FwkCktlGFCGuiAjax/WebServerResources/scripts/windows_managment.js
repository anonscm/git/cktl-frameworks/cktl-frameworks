
function openWinTreeView (id, title, closable, width, height, className)	{				  	
  var win = Windows.getWindow(id+'_win');
  if (closable==null) {
    closable=false;
  }
  if (width==null) {
    width=300;
  }
  if (height==null) {
    height=200;
  }
  if (typeof(win)=='undefined') {
	  if (className==null) {
		  className="greenlighting";
	  }
    win = new Window(id+'_win',{className: className, title: title, destroyOnClose:true, recenterAuto:true, resizable:true, closable:closable, minimizable:false, maximizable:false,  minWidth:width, minHeight:height, showEffectOptions: {duration:0.5}});    
		var editorOnClose = { onClose: function(eventName, win) {eval(id+'_containerOnCloseUpdate()'); } ,
			onDestroy: function(eventName, win) {
				Windows.removeObserver(editorOnClose);
			}
		};
		Windows.addObserver(editorOnClose);		
		win.setDestroyOnClose();
    win.setZIndex(99999);
    win.setContent(id,true);
    win.showCenter(true);
  } else {
    win.showCenter();
  }
}

function openWin (href, title, closable, width, height, className) {
  var win = Windows.getWindow(title+'_win');
  if (closable==null) {
    closable=false;
  }
  if (width==null) {
    width=300;
  }
  if (height==null) {
    height=200;
  }
  if (typeof(win)=='undefined') {
  	win = new Window(title+'_win',{className: className, title: title, url: href, destroyOnClose:true, recenterAuto:true, resizable:true, closable:closable, minimizable:false, maximizable:false,  minWidth:width, minHeight:height,  showEffectOptions: {duration:0.5}});    
  	win.setZIndex(99999);
  	win.showCenter(true);
  } else {
    win.showCenter();
  }
  
}

