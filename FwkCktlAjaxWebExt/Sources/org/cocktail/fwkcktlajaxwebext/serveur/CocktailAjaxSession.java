/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// package org.cocktail.fwkcktlajaxwebext.serveur;
package org.cocktail.fwkcktlajaxwebext.serveur;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.uimessages.CktlUIMessage;
import org.cocktail.fwkcktlajaxwebext.serveur.uimessages.CktlUIMessages;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxResponse;
import er.ajax.AjaxResponseAppender;
import er.ajax.AjaxUtils;

public class CocktailAjaxSession extends CktlWebSession {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(CocktailAjaxSession.class);

	private CktlUIMessages uiMessages = new CktlUIMessages();
    private NSMutableArray<String> registeredContainerIds = new NSMutableArray<String>();


	/**
	 * Nom de la classe qui sera appliquee a toutes les windows ouvertes via le CktlAjaxWindow ou CktlAjaxModalDialog
	 */
	private String windowsClassName;

	public String getWindowsClassName() {
		return windowsClassName;
	}

	/**
	 * Indique le nom de la classe a utiliser pour l'affichage des fenetres modales (gerees par {@link CktlAjaxWindow}). A initialiser par le
	 * developpeur de l'application, dans le constructeur de la session par exemple.
	 * 
	 * @param windowsClassName
	 */
	public void setWindowsClassName(String windowsClassName) {
		this.windowsClassName = windowsClassName;
	}

	public void initSession() {
		setWindowsClassName(CktlAjaxWindow.WINDOWS_CLASS_NAME_GREYLIGHTING);
		super.initSession();
	}

	/**
	 * @return La pile par defaut des messages à afficher à l'utilisateur
	 */
	public CktlUIMessages getUiMessages() {
		return uiMessages;
	}

	public void setUiMessages(CktlUIMessages uiMessages) {
		this.uiMessages = uiMessages;
	}

	public void addMessage(CktlUIMessage message) {
		getUiMessages().addObject(message);
	}

	/**
	 * 
	 * @param titre le titre de la notification
	 * @param details le détail de la notification
	 */
	public void addSimpleErrorMessage(String titre, String details) {
		getUiMessages().addSimpleErrorMessage(titre, details);
	}
	
	/**
	 * 
	 * @param details le détail de la notification
	 */
	public void addSimpleErrorMessage(String details) {
		addSimpleErrorMessage("", details);
	}

	
	/**
	 * Ajout d'un notification d'erreur avec les clés des messages à 
	 * aller chercher dans les fichiers de localisation
	 * @param titreKey la clé du titre de la notif
	 * @param detailsKey la clé du détail de la notif
	 */
	public void addSimpleLocalizedErrorMessage(String titreKey, String detailsKey) {
		addSimpleErrorMessage(
			localizer().localizedStringForKey(titreKey),
			localizer().localizedStringForKey(detailsKey)
		);
	}
	
	/**
	 * Ajout d'un notification d'erreur avec les clés des messages à 
	 * aller chercher dans les fichiers de localisation
	 * @param detailsKey la clé du détail de la notif
	 */
	public void addSimpleLocalizedErrorMessage(String detailsKey) {
		addSimpleErrorMessage(
			localizer().localizedStringForKey(detailsKey)
		);
	}

	/**
	 * Ajouter une notification d'erreur avec une exception pour le détail
	 * @param titre Le titre
	 * @param exception L'exception dans laquelle on va aller chercher le message
	 */
	public void addSimpleErrorMessage(String titre, Throwable exception) {
		getUiMessages().addSimpleErrorMessage(titre, exception);
	}

	/**
	 * Ajouter une notification d'erreur avec une exception pour le détail
	 * @param exception L'exception dans laquelle on va aller chercher le message
	 */
	public void addSimpleErrorMessage(Throwable exception) {
		addSimpleErrorMessage("", exception);
	}

	
	/**
	 * Ajouter un notification sans le titre
	 * @param details Le détail de la notification
	 */
	public void addSimpleInfoMessage(String details) {
		addSimpleInfoMessage("", details);
	}
	
	/**
	 * Ajouter un notification sans le titre avec la clé du message
	 * @param detailsKey la clé du détail de la notification
	 */
	public void addSimpleLocalizedInfoMessage(String detailsKey) {
		addSimpleInfoMessage("", localizer().localizedStringForKey(detailsKey));
	}
	
	/**
	 * 
	 * @param titre Le titre de la notification
	 * @param details Le détail de la notification
	 */
	public void addSimpleInfoMessage(String titre, String details) {
		getUiMessages().addSimpleInfoMessage(titre, details);
	}
	
	/**
	 * 
	 * @param titreKey La clé du titre de la notification
	 * @param detailsKey La clé du détail de la notification
	 */
	public void addSimpleLocalizedInfoMessage(String titreKey, String detailsKey) {
		addSimpleInfoMessage(
				localizer().localizedStringForKey(titreKey),
				localizer().localizedStringForKey(detailsKey)
			);
	}

	/* Messages success */
	
	/**
	 * 
	 * @param titreKey La clé du titre de la notification
	 * @param detailsKey La clé du détail de la notification
	 */
	public void addSimpleLocalizedSuccessMessage(String titreKey, String detailsKey) {
		addSimpleSuccessMessage(
				localizer().localizedStringForKey(titreKey),
				localizer().localizedStringForKey(detailsKey)
			);
	}
	
	/**
	 * Ajouter un notification sans le titre
	 * @param details Le détail de la notification
	 */
	public void addSimpleSuccessMessage(String details) {
		addSimpleSuccessMessage("", details);
	}
	
	/**
	 * Ajouter un notification sans le titre avec la clé du message
	 * @param detailsKey la clé du détail de la notification
	 */
	public void addSimpleLocalizedSuccessMessage(String detailsKey) {
		addSimpleSuccessMessage("", localizer().localizedStringForKey(detailsKey));
	}
	
	/**
	 * 
	 * @param titre Le titre de la notification
	 * @param details Le détail de la notification
	 */
	public void addSimpleSuccessMessage(String titre, String details) {
		getUiMessages().addSimpleSuccessMessage(titre, details);
	}
	
	/* Message help */

	/**
	 * 
	 * @param titreKey La clé du titre de la notification
	 * @param detailsKey La clé du détail de la notification
	 */
	public void addSimpleLocalizedHelpMessage(String titreKey, String detailsKey) {
		addSimpleHelpMessage(
				localizer().localizedStringForKey(titreKey),
				localizer().localizedStringForKey(detailsKey)
			);
	}
	
	/**
	 * Ajouter un notification sans le titre
	 * @param details Le détail de la notification
	 */
	public void addSimpleHelpMessage(String details) {
		addSimpleHelpMessage("", details);
	}
	
	/**
	 * Ajouter un notification sans le titre avec la clé du message
	 * @param detailsKey la clé du détail de la notification
	 */
	public void addSimpleLocalizedHelpMessage(String detailsKey) {
		addSimpleHelpMessage("", localizer().localizedStringForKey(detailsKey));
	}
	
	/**
	 * 
	 * @param titre Le titre de la notification
	 * @param details Le détail de la notification
	 */
	public void addSimpleHelpMessage(String titre, String details) {
		getUiMessages().addSimpleHelpMessage(titre, details);
	}
	
	/**
     * Enregistre un container comme devant être rafraichi à chaque appendToResponse Ajax. (Utile pour les msg par exemple). S'appuie sur les
     * AjaxResponseAppender
     * 
     * @param containerID
     */
	public void registerUpdateContainerForEachResponse(final String containerID) {
	    if (!StringCtrl.isEmpty(containerID)) {
	        if (!registeredContainerIds.contains(containerID)) {
	            registeredContainerIds.add(containerID);
	            AjaxResponse.addAjaxResponseAppender(new RefreshMessageAppender(containerID));
	        }
	    }
	}
	
	public static class RefreshMessageAppender extends AjaxResponseAppender {

	    private String containerID;

	    public RefreshMessageAppender(String containerID) {
	        this.containerID = containerID;
	    }

	    public void appendToResponse(WOResponse response, WOContext context) {
	        if (context.session() instanceof CocktailAjaxSession) {
	            CocktailAjaxSession cktlAjaxSession = (CocktailAjaxSession) context.session();
	            // Si la session a des messages...
	            if (!cktlAjaxSession.getUiMessages().isEmpty()) {
	                String script = "if ($('" + containerID + "') != null) {"
	                                + containerID + "Update();} else if ($('parent." + containerID + "') != null) {parent."
	                                + containerID + "Update();}";
	                LOG.debug(">>>> Insertion d'un script JS de maj d'un container de messages " + script);
	                AjaxUtils.appendScript(context, script);
	            }
	        }
	    }
	}
	
}
