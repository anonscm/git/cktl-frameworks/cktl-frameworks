package org.cocktail.fwkcktlajaxwebext.serveur;

import com.webobjects.jdbcadaptor.CktlOraclePlugin;
import com.webobjects.jdbcadaptor.JDBCPlugIn;

import er.extensions.ERXFrameworkPrincipal;
import er.extensions.foundation.ERXProperties;

public class FwkCktlAjaxWebExt extends ERXFrameworkPrincipal {

	static {
		setUpFrameworkPrincipalClass(FwkCktlAjaxWebExt.class);
	}

	@Override
	public void finishInitialization() {
		if (ERXProperties.booleanForKeyWithDefault("org.cocktail.fwkcktlajaxwebext.useCktlOraclePlugin", false)) {
			JDBCPlugIn.setPlugInNameForSubprotocol(CktlOraclePlugin.class.getName(), "oracle");
		}
	}

}
