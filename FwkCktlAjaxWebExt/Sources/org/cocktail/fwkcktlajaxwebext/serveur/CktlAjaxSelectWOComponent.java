/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

public class CktlAjaxSelectWOComponent extends CktlAjaxWOComponent {

	private static final long serialVersionUID = 1L;
	public static final String SELECTION_BDG = "selection";
	/** Facultatif. Binding pour specifier si le bouton supprimer doit etre affiche. Par defaut a true. */
	public static final String DISPLAY_DELETE_BUTTON_BDG = "displayDeleteButton";

	public static final String TREE_VIEW_TITLE_BDG = "treeViewTitle";
	public static final String TREE_VIEW_WIDTH_BDG = "treeViewWidth";
	public static final String TREE_VIEW_HEIGHT_BDG = "treeViewHeight";
	public static final String TREE_VIEW_CLASS_NAME_BDG = "treeViewClassName";

	/** Facultatif. Binding pour specifier la racine de l'arbre. */
	public static final String TREE_ROOT_OBJECT_BDG = "treeRootObject";
	/** Facultatif. Binding pour specifier la taille du champ (en colonnes). */
	public static final String TEXT_FIELD_SIZE_BDG = "textFieldSize";
	/** Facultatif. Binding pour specifier un container a mettre a jour qd la fenetre se ferme. */
	public static final String UPDATE_CONTAINER_ID_BDG = "updateContainerID";
	/** Facultatif. Binding pour specifier un container a mettre a jour qd on supprimer (efface) la selection. */
	public static final String UPDATE_CONTAINER_ID_ON_SUPPRIMER_BDG = "updateContainerIDOnSupprimer";
	/** Facultatif. Binding pour specifier si les boutons sont actifs ou non (actifs par defaut). */
	public static final String DISABLED_BDG = "disabled";

	public static final String TREE_COLLAPSED_IMG_BDG = "treeCollapsedImage";
	public static final String TREE_COLLAPSED_IMG_FWK_BDG = "treeCollapsedImageFramework";
	public static final String TREE_EXPANDED_IMG_BDG = "treeExpandedImage";
	public static final String TREE_EXPANDED_IMG_FWK_BDG = "treeExpandedImageFramework";
	public static final String TREE_LEAF_IMG_BDG = "treeLeafImage";
	public static final String TREE_LEAF_IMG_FWK_BDG = "treeLeafImageFramework";

	private String collapsedImage, collapsedImageFramework;
	private String expandedImage, expandedImageFramework;
	private String leafImage, leafImageFramework;

	private String containerComponentId;
	private String updateContainerIDOnSupprimer;

	// private Boolean isTreeViewOpened = Boolean.FALSE;

	public CktlAjaxSelectWOComponent(WOContext context) {
		super(context);
	}

	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}

	public String unAjaxTreeID() {
		return getComponentId() + "_ajaxTree";
	}

	public String containerAjaxTreeID() {
		return getComponentId() + "_containerAjaxTree";
	}

	/*
	 * public String onSuccessRechercher() { String onSuccessRechercher = null; onSuccessRechercher = "function () {openWinTreeView('";
	 * onSuccessRechercher += getComponentId()+"', '"; if (valueForBinding(TREE_VIEW_TITLE_BDG)!=null) { onSuccessRechercher +=
	 * valueForBinding(TREE_VIEW_TITLE_BDG)+"', true"; } else { onSuccessRechercher += "Choisir un element', true"; }
	 * 
	 * if (valueForBinding(TREE_VIEW_WIDTH_BDG)!=null) { onSuccessRechercher += ", "+valueForBinding(TREE_VIEW_WIDTH_BDG); } else {
	 * onSuccessRechercher += ", null"; } if (valueForBinding(TREE_VIEW_HEIGHT_BDG)!=null) { onSuccessRechercher +=
	 * ", "+valueForBinding(TREE_VIEW_HEIGHT_BDG); } else { onSuccessRechercher += ", null"; } if (valueForBinding(TREE_VIEW_CLASS_NAME_BDG)!=null) {
	 * onSuccessRechercher += ", '"+valueForBinding(TREE_VIEW_CLASS_NAME_BDG)+"'"; } else { onSuccessRechercher += ", null"; } if
	 * (valueForBinding(UPDATE_CONTAINER_ID_BDG)!=null) { onSuccessRechercher += ", '"+valueForBinding(UPDATE_CONTAINER_ID_BDG)+"'"; } else {
	 * onSuccessRechercher += ", null"; } onSuccessRechercher += ");}";
	 * 
	 * return onSuccessRechercher; } public String onSuccessSelect() { String onSuccessSelect = null; onSuccessSelect =
	 * "function () {Windows.close('"; onSuccessSelect += getComponentId()+"_win');}";
	 * 
	 * return onSuccessSelect; }
	 */
	public Boolean displayDeleteButton() {
		if (hasBinding(DISPLAY_DELETE_BUTTON_BDG)) {
			return (Boolean) valueForBinding(DISPLAY_DELETE_BUTTON_BDG);
		}
		return Boolean.TRUE;
	}

	public WOActionResults supprimerSelection() {
		setValueForBinding(null, SELECTION_BDG);
		return null;
	}

	public String containerOnCloseID() {
		return getComponentId() + "_containerOnClose";
	}

	public Integer textFieldSize() {
		if (hasBinding(TEXT_FIELD_SIZE_BDG)) {
			return (Integer) valueForBinding(TEXT_FIELD_SIZE_BDG);
		}
		return Integer.valueOf(50);
	}

	/**
	 * @return the collapsedImage
	 */
	public String getTreeCollapsedImage() {
		if (collapsedImage == null) {
			collapsedImage = (String) valueForBinding(TREE_COLLAPSED_IMG_BDG);
		}

		return collapsedImage;
	}

	/**
	 * @param collapsedImage the collapsedImage to set
	 */
	public void setTreeCollapsedImage(String collapsedImage) {
		this.collapsedImage = collapsedImage;
	}

	/**
	 * @return the collapsedImageFramework
	 */
	public String getTreeCollapsedImageFramework() {
		if (collapsedImageFramework == null) {
			collapsedImageFramework = (String) valueForBinding(TREE_COLLAPSED_IMG_FWK_BDG);
		}

		return collapsedImageFramework;
	}

	/**
	 * @param collapsedImageFramework the collapsedImageFramework to set
	 */
	public void setTreeCollapsedImageFramework(String collapsedImageFramework) {
		this.collapsedImageFramework = collapsedImageFramework;
	}

	/**
	 * @return the expandedImage
	 */
	public String getTreeExpandedImage() {
		if (expandedImage == null) {
			expandedImage = (String) valueForBinding(TREE_EXPANDED_IMG_BDG);
		}
		return expandedImage;
	}

	/**
	 * @param expandedImage the expandedImage to set
	 */
	public void setTreeExpandedImage(String expandedImage) {
		this.expandedImage = expandedImage;
	}

	/**
	 * @return the expandedImageFramework
	 */
	public String getTreeExpandedImageFramework() {
		if (expandedImageFramework == null) {
			expandedImageFramework = (String) valueForBinding(TREE_EXPANDED_IMG_FWK_BDG);
		}
		return expandedImageFramework;
	}

	/**
	 * @param expandedImageFramework the expandedImageFramework to set
	 */
	public void setTreeExpandedImageFramework(String expandedImageFramework) {
		this.expandedImageFramework = expandedImageFramework;
	}

	/**
	 * @return the leafImage
	 */
	public String getTreeLeafImage() {
		if (leafImage == null) {
			leafImage = (String) valueForBinding(TREE_LEAF_IMG_BDG);
		}
		return leafImage;
	}

	/**
	 * @param leafImage the leafImage to set
	 */
	public void setTreeLeafImage(String leafImage) {
		this.leafImage = leafImage;
	}

	/**
	 * @return the leafImageFramework
	 */
	public String getTreeLeafImageFramework() {
		if (leafImageFramework == null) {
			leafImageFramework = (String) valueForBinding(TREE_LEAF_IMG_FWK_BDG);
		}
		return leafImageFramework;
	}

	/**
	 * @param leafImageFramework the leafImageFramework to set
	 */
	public void setTreeLeafImageFramework(String leafImageFramework) {
		this.leafImageFramework = leafImageFramework;
	}

	/*
	 * public String onClickRechercher() { String onSuccessRechercher = null; onSuccessRechercher = "openWinPersonneSelect(this.href, '";
	 * onSuccessRechercher += getComponentId()+"', '"; if (valueForBinding(TREE_VIEW_TITLE_BDG)!=null) { onSuccessRechercher +=
	 * valueForBinding(TREE_VIEW_TITLE_BDG)+"', true"; } else { onSuccessRechercher += "Choisir un element', true"; }
	 * 
	 * if (valueForBinding(TREE_VIEW_WIDTH_BDG)!=null) { onSuccessRechercher += ", "+valueForBinding(TREE_VIEW_WIDTH_BDG); } else {
	 * onSuccessRechercher += ", null"; } if (valueForBinding(TREE_VIEW_HEIGHT_BDG)!=null) { onSuccessRechercher +=
	 * ", "+valueForBinding(TREE_VIEW_HEIGHT_BDG); } else { onSuccessRechercher += ", null"; } if (valueForBinding(TREE_VIEW_CLASS_NAME_BDG)!=null) {
	 * onSuccessRechercher += ", '"+valueForBinding(TREE_VIEW_CLASS_NAME_BDG)+"'"; } else { onSuccessRechercher += ", null"; } if
	 * (valueForBinding(UPDATE_CONTAINER_ID_BDG)!=null) { onSuccessRechercher += ", '"+valueForBinding(UPDATE_CONTAINER_ID_BDG)+"'"; } else {
	 * onSuccessRechercher += ", null"; } onSuccessRechercher += ");return false;";
	 * 
	 * return onSuccessRechercher; }
	 */
	/**
	 * @return the containerComponentId
	 */
	public String containerComponentId() {
		return "ContainerSelection" + getComponentId();
	}

	/**
	 * @param containerComponentId the containerComponentId to set
	 */
	public void setContainerComponentId(String containerComponentId) {
		this.containerComponentId = containerComponentId;
	}

	/**
	 * @return the updateContainerIDOnSupprimer
	 */
	public String updateContainerIDOnSupprimer() {
		updateContainerIDOnSupprimer = containerComponentId;
		if (hasBinding(UPDATE_CONTAINER_ID_ON_SUPPRIMER_BDG)) {
			updateContainerIDOnSupprimer = (String) valueForBinding(UPDATE_CONTAINER_ID_ON_SUPPRIMER_BDG);
		}
		return updateContainerIDOnSupprimer;
	}

	/**
	 * @param updateContainerIDOnSupprimer the updateContainerIDOnSupprimer to set
	 */
	public void setUpdateContainerIDOnSupprimer(String updateContainerIDOnSupprimer) {
		this.updateContainerIDOnSupprimer = updateContainerIDOnSupprimer;
	}

	/**
	 * @return the disabled
	 */
	public Boolean disabled() {
		return hasBinding(DISABLED_BDG) ? (Boolean) valueForBinding(DISABLED_BDG) : Boolean.FALSE;
	}
	/*
	*//**
	 * @return the isTreeViewOpened
	 */
	/*
	 * public Boolean isTreeViewOpened() { return isTreeViewOpened; }
	 *//**
	 * @param isTreeViewOpened the isTreeViewOpened to set
	 */
	/*
	 * public void setIsTreeViewOpened(Boolean isTreeViewOpened) { this.isTreeViewOpened = isTreeViewOpened; }
	 * 
	 * public WOActionResults openTreeView() { setIsTreeViewOpened(Boolean.TRUE); return null; }
	 */
}
