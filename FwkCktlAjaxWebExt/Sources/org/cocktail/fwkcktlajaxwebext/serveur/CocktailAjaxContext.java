/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlajaxwebext.serveur;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;

/**
 * CocktailAjaxContext provides the overrides necessary methods for partial form submits to work.
 * 
 * @author mschrag
 */
public class CocktailAjaxContext extends WOContext {
	public CocktailAjaxContext(WORequest request) {
		super(request);
	}

	// @Override no override for 5.4/5.3 compatibility
	public boolean wasFormSubmitted() {
		return _wasFormSubmitted();
	}

	@Override
	public boolean _wasFormSubmitted() {
		boolean wasFormSubmitted = super._wasFormSubmitted();
		if (wasFormSubmitted) {
			WORequest request = request();
			String partialSubmitSenderID = CocktailAjaxApplication.partialFormSenderID(request);
			if (partialSubmitSenderID != null) {
				// TODO When explicitly setting the "name" binding on an input, 
				// the following will fail in the takeValuesFromRequest phase.
				String elementID = elementID();
				if (!partialSubmitSenderID.equals(elementID)
						&& !partialSubmitSenderID.startsWith(elementID + ",")
						&& !partialSubmitSenderID.endsWith("," + elementID)
						&& !partialSubmitSenderID.contains("," + elementID + ",")) {
					String ajaxSubmitButtonID = CocktailAjaxApplication.ajaxSubmitButtonName(request);
					if (ajaxSubmitButtonID == null || !ajaxSubmitButtonID.equals(elementID)) {
						wasFormSubmitted = false;
					}
				}
			}
		}
		return wasFormSubmitted;
	}
}
