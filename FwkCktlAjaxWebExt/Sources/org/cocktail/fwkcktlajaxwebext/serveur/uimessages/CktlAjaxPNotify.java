/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.uimessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.ajax.AjaxDynamicElement;
import er.ajax.CktlAjaxUtils;

/**
 * Cet élément se base sur la présence d'une clef <code>uiMessages</code> dans la session correspondant à un objet <strong>CktlUIMessages</strong> de
 * Il se charge d'appeler l'affichage des messages contenus dans la pile. Cet élément doit être inclus dans un updateContainerpour déclencher
 * l'affichage des messages.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 * @author rprin
 * @todo Voir si on peut prendre en compte plus d'options via les bindings
 */
public class CktlAjaxPNotify extends AjaxDynamicElement {
	//private static PNotifyAjaxResponseAppender pNotifyAjaxResponseAppender;
	private NSMutableDictionary<String, String> options = new NSMutableDictionary<String, String>();

	public static String BDG_MESSAGES_KEY_PATH = "messagesKeyPath";

	private static final String DIRECTION_1_NOTIFICATION = "down";
	private static final String DIRECTION_2_NOTIFICATION = "left";
	private static final int HEIGHT_ENTRE_NOTIFICATIONS = 10;
	private static final int WIDTH_ENTRE_NOTIFICATIONS = 25;
	private static final int POSITION_INITIALE_WIDTH = 25;
	private static final int POSITION_INITIALE_HEIGHT = 25;

	//	private static PNotifyAjaxResponseAppender pNotifyAjaxResponseAppender;

	@SuppressWarnings("rawtypes")
	public CktlAjaxPNotify(String name, NSDictionary associations, WOElement children) {
		super(name, associations, children);
	}

	@Override
	protected void addRequiredWebResources(WOResponse response, WOContext context) {
		//		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "css/CktlAjaxSimpleMessageList.css");
		//		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "css/jquery/jquery-ui/jquery-ui.css");
		//		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "css/jquery/jquery.pnotify.default.css");
		//		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/jquery/jquery-1.4.2.js");
		//		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/jquery/jquery-ui-1.8.4.min.js");
		//		CktlAjaxUtils.addScriptCodeInHead(response, context, "jQuery.noConflict();");
		//		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/jquery/jquery.pnotify.js");
	}

	@Override
	public WOActionResults handleRequest(WORequest request, WOContext context) {
		return null;
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {

		super.appendToResponse(response, context);
		if (context != null) {
			String msgKeyPath = (String) valueForBinding(BDG_MESSAGES_KEY_PATH, "session.uiMessages", context.component());
			CktlUIMessages messages = (CktlUIMessages) context.valueForKeyPath(msgKeyPath);
			
			if (messages != null && !messages.isEmpty()) {
				
				// Verification de l'existance du stack
				StringBuffer sb = new StringBuffer("if (typeof stack_cocktail === \"undefined\") {");
				// Création d'un stack car celui par défaut gère mal le cas ou plusieurs notifications apparaissent en meme temps
				sb.append("stack_cocktail = {'dir1': '" + DIRECTION_1_NOTIFICATION + "'");
				sb.append(", 'dir2': '" + DIRECTION_2_NOTIFICATION + "'");
				sb.append(", 'push': 'bottom'");
				sb.append(", 'spacing1': " + HEIGHT_ENTRE_NOTIFICATIONS);
				sb.append(", 'spacing2': " + WIDTH_ENTRE_NOTIFICATIONS);
				sb.append(", 'context': jQuery('body')");
				sb.append(", 'firstpos1': " + POSITION_INITIALE_HEIGHT);
				sb.append(", 'firstpos2': " + POSITION_INITIALE_WIDTH + "}; ");
				sb.append("}");
				
				sb.append("jQuery(function(){");
				
				for (CktlUIMessage message : messages) {
					sb.append("new PNotify({");
					if ("".equals(message.titre())) {
						sb.append("icon: false");
					} else {
						sb.append("title: '" + CktlAjaxUtils.jsEncode(noNullString(message.titre())) + "'");
					}
					sb.append(", text: '" + CktlAjaxUtils.jsEncode(noNullString(message.details())) + "'");
					sb.append(", type: '" + message.type().toString().toLowerCase() + "'");
					sb.append(", history: {menu: true, labels: {redisplay: \"Réafficher\", all: \"Tous\", last: \"Dernier\"}}");
					sb.append(", stack : stack_cocktail");
					sb.append(", styling : 'bootstrap3'");
					sb.append("}); ");					
				}
				sb.append("});");
				response.appendContentString("<script>" + sb.toString() + "</script>");
				messages.removeAllObjects();
			}
		}
	}

	private String noNullString(String aString) {
		return aString == null ? String.valueOf("") : aString;
	}

	protected String _containerID(WOContext context) {
		return (String) valueForBinding("containerId", context.component());
	}

	//	public static PNotifyAjaxResponseAppender defaultPNotifyAjaxResponseAppender() {
	//		if (pNotifyAjaxResponseAppender == null)
	//			pNotifyAjaxResponseAppender = new PNotifyAjaxResponseAppender();
	//		return pNotifyAjaxResponseAppender;
	//	}
	//
	//	/**
	//	 * Response appender chargé d'exécuter les scripts js de notifications, si des messages sont présents en session.
	//	 */
	//	public static class PNotifyAjaxResponseAppender extends AjaxResponseAppender {
	//
	//		NSMutableDictionary<String, String> options = new NSMutableDictionary<String, String>();
	//		String messagesKeyPath = "messages";
	//
	//		public PNotifyAjaxResponseAppender() {
	//		}
	//
	//		public PNotifyAjaxResponseAppender(String messagesKeyPath) {
	//			if (messagesKeyPath != null)
	//				this.messagesKeyPath = messagesKeyPath;
	//		}
	//
	//		@SuppressWarnings("unchecked")
	//		@Override
	//		public void appendToResponse(WOResponse response, WOContext context) {
	//			if (context != null) {
	//				NSMutableArray<CktlUIMessage> messages =
	//						(NSMutableArray<CktlUIMessage>) context.session().valueForKeyPath(messagesKeyPath);
	//				if (messages != null && !messages.isEmpty()) {
	//					for (CktlUIMessage message : messages) {
	//						StringBuffer sb = new StringBuffer("jQuery.pnotify(");
	//						sb.append("{ pnotify_title: '").append(CktlAjaxUtils.jsEncode(noNullString(message.titre())))
	//								.append("',pnotify_text: '").append(CktlAjaxUtils.jsEncode(noNullString(message.details())))
	//								.append("',pnotify_type: '").append(noNullString(message.type().name().toLowerCase()))
	//								.append("',");
	//						for (String key : options.keySet()) {
	//							sb.append(key).append(":")
	//									.append("'").append(options.valueForKey(key)).append("',");
	//						}
	//						sb.append("})");
	//						AjaxUtils.appendScript(context, sb.toString());
	//					}
	//					messages.removeAllObjects();
	//				}
	//			}
	//		}
	//
	//		public void addOption(String key, String value) {
	//			options.setObjectForKey(value, key);
	//		}
	//
	//		private String noNullString(String aString) {
	//			return aString == null ? String.valueOf("") : aString;
	//		}
	//
	//	}

}
