package org.cocktail.fwkcktlajaxwebext.serveur.component;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.components.ERXStatelessComponent;

public class CktlDisplayGroupPaginator extends ERXStatelessComponent {
    public CktlDisplayGroupPaginator(WOContext context) {
        super(context);
    }
    
    private Integer numeroPage;
    private Integer numberOfObjectsPerBatch;
    private final static String BINDING_updateContainerId = "updateContainerId";
    
    private final static String BINDING_displayGroup = "displayGroup";


    public WOActionResults pageSuivante() {
      WODisplayGroup localDg = displayGroup();
      if (localDg != null) {
        NSArray selectedObjects = localDg.selectedObjects();
        localDg.displayNextBatch();
        localDg.selectObjectsIdenticalTo(selectedObjects);
        localDg.updateDisplayedObjects();
      }

      return null;
    }
    
    public WODisplayGroup displayGroup() {
      return (WODisplayGroup) valueForBinding(BINDING_displayGroup);
    }
    
    public String updateContainerId() {
      return stringValueForBinding(BINDING_updateContainerId);
    }
    

    public WOActionResults pagePrecedente() {
      WODisplayGroup localDg = displayGroup();
      if (localDg != null) {
        NSArray selectedObjects = localDg.selectedObjects();
        localDg.displayPreviousBatch();
        localDg.selectObjectsIdenticalTo(selectedObjects);
        localDg.updateDisplayedObjects();
      }

      return null;
    }
    
    public WOActionResults premierePage() {
      WODisplayGroup localDg = displayGroup();
      if (localDg != null) {
        NSArray selectedObjects = localDg.selectedObjects();
        localDg.setCurrentBatchIndex(1);
        localDg.selectObjectsIdenticalTo(selectedObjects);
        localDg.updateDisplayedObjects();
      }

      return null;
    }
    
    public WOActionResults dernierePage() {
      WODisplayGroup localDg = displayGroup();
      if (localDg != null) {
        NSArray selectedObjects = localDg.selectedObjects();
        localDg.setCurrentBatchIndex(localDg.batchCount());
        localDg.selectObjectsIdenticalTo(selectedObjects);
        localDg.updateDisplayedObjects();
      }

      return null;
    }
    
    public NSArray<Integer> getPagesList() {
      NSMutableArray<Integer> pagesList = new NSMutableArray<Integer>();
      if (displayGroup().batchCount() > 0) {        
        for (int i = 1; i <= displayGroup().batchCount(); i++)
          pagesList.addObject(i);
      }
      return pagesList;
    }

    public int currentBatchIndex() {
    	return displayGroup().currentBatchIndex();
    }
    
    public void setCurrentBatchIndex(Integer currentBatchIndex) {
    	if (currentBatchIndex != null) {
    		displayGroup().setCurrentBatchIndex(currentBatchIndex);
    	}
    }
    
    public Integer getNumeroPage() {
      return numeroPage;
    }

    public void setNumeroPage(Integer numeroPage) {
      this.numeroPage = numeroPage;
    }
    
    public WOActionResults changerNumeroPage() {
      WODisplayGroup localDg = displayGroup();
      NSArray selectedObjects = localDg.selectedObjects();
      localDg.selectObjectsIdenticalTo(selectedObjects);
      localDg.updateDisplayedObjects();
      return null;
    }
    
    public Integer numberOfObjectsPerBatch() {
      return Integer.valueOf(displayGroup().numberOfObjectsPerBatch());
    }

    public void setNumberOfObjectsPerBatch(Integer numberOfObjectsPerBatch) {
      if (numberOfObjectsPerBatch == null || numberOfObjectsPerBatch <= 0) {
        return;
      }
      this.numberOfObjectsPerBatch = numberOfObjectsPerBatch;
      WODisplayGroup localDg = displayGroup();
      NSArray selectedObjects = localDg.selectedObjects();
      if (localDg.allObjects().count() < numberOfObjectsPerBatch.intValue()) {
        numberOfObjectsPerBatch = Integer.valueOf(localDg.allObjects().count());
        this.numberOfObjectsPerBatch = numberOfObjectsPerBatch;
      }
      localDg.setNumberOfObjectsPerBatch(numberOfObjectsPerBatch.intValue());
      localDg.selectObjectsIdenticalTo(selectedObjects);
    }
    
    public WOActionResults changerNumberOfObjectsPerBatch() {
      displayGroup().updateDisplayedObjects();
      return null;
    }

    public Boolean isPremierePage() {
      return displayGroup().currentBatchIndex() == 1;
    }
    
    public Boolean isDernierePage() {
      return displayGroup().currentBatchIndex() == displayGroup().batchCount();
    }
    


}