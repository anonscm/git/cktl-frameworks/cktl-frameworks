package org.cocktail.fwkcktlajaxwebext.serveur.component;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eoaccess.EOSQLExpression;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.jdbc.ERXSQLHelper;

/**
 * Composant permettant d'effectuer des recherches sur une entité. La recherche n'est pas effectuée au lancement, mais obligatoirement sur
 * déclenchement.
 * 
 * @binding entityName Nom de l'entité sur laquel fetcher les résultats
 * @binding sortOrderings Tableau d'EOSortOrdering pour trier les résultats
 * @binding filtreButtonId
 * @binding editingContext
 * @binding helpTip s'affiche au dessus du champ de recherche (html possible)
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlAjaxFetchAndSelectComponent extends CktlAjaxSelectComponent {
	public final static Logger logger = Logger.getLogger(CktlAjaxFetchAndSelectComponent.class);

	private static final long serialVersionUID = 1L;
	public static String BINDING_ENTITY_NAME = "entityName";
	public static String BINDING_SORT_ORDERINGS = "sortOrderings";
	public static String BINDING_HELP_TIP = "helpTip";
	private static final String BINDING_FILTRE_BUTTON_ID = "filtreButtonId";
	public static final String BINDING_editingContext = "editingContext";
	private WODisplayGroup displayGroup;
	private Integer numberOfObjectsPerBatch = Integer.valueOf(50);
	private NSMutableArray<Integer> pagesList = new NSMutableArray<Integer>();
	private Integer numPage = Integer.valueOf(1);
	private Integer minPage = Integer.valueOf(1);
	protected Integer maxPage = Integer.valueOf(1);
	public Integer unNumPage;

	public CktlAjaxFetchAndSelectComponent(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse woresponse, WOContext wocontext) {
		refresh();
		super.appendToResponse(woresponse, wocontext);
	}

	public void refresh() {
		Boolean res = (Boolean) valueForBinding(BINDING_REFRESH_DATA);
		if (Boolean.TRUE.equals(res)) {
			if (canSetValueForBinding(BINDING_REFRESH_DATA)) {
				setValueForBinding(Boolean.FALSE, BINDING_REFRESH_DATA);
			}
			setFiltre(null);
			initDisplayGroup();
		}
	}

	private void initDisplayGroup() {
		displayGroup = new WODisplayGroup();
		displayGroup.setNumberOfObjectsPerBatch(numberOfObjectsPerBatch());

		setNumPage(Integer.valueOf(1));
		maxPage = Integer.valueOf(1);
		pagesList.removeAllObjects();
	}

	public WOActionResults filtrer() {
		setNumPage(Integer.valueOf(1));
		updateDisplayedObjects();

		return null;
	}

	public WODisplayGroup getDisplayGroup() {
		//		if (displayGroup == null || refreshData()) {
		//			displayGroup = new WODisplayGroup();
		//			displayGroup.setNumberOfObjectsPerBatch(numberOfObjectsPerBatch());
		//			setNumPage(1);
		//			maxPage = 0;
		//			pagesList.removeAllObjects();
		//		}
		return displayGroup;
	}

	public WODisplayGroup dg() {
		return getDisplayGroup();
	}

	public String getEntityName() {
		return (String) valueForBinding(BINDING_ENTITY_NAME);
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOSortOrdering> getSortOrderings() {
		return (NSArray<EOSortOrdering>) valueForBinding(BINDING_SORT_ORDERINGS);
	}

	public String getTbvId() {
		return getComponentId() + "_tbv";
	}

	public String filtreButtonId() {
		if (hasBinding(BINDING_FILTRE_BUTTON_ID)) {
			return (String) valueForBinding(BINDING_FILTRE_BUTTON_ID);
		}
		else {
			return "Bid_" + getContainerId();
		}
	}

	///////////

	public String divBarreDeNavigationID() {
		String divBarreDeNavigationID = getComponentId() + "_" + "DivBarreDeNavigation";
		return divBarreDeNavigationID;
	}

	public String divBarreDeStatutID() {
		String divBarreDeStatutID = getComponentId() + "_" + "DivBarreDeStatut";
		return divBarreDeStatutID;
	}

	public String divToolBarContentID() {
		String divToolBarContentID = getComponentId() + "_" + "DivToolBar";
		return divToolBarContentID;
	}

	public String formBarreNavID() {
		return getComponentId() + "_" + "FormBarreNav";
	}

	public String numeroPageID() {
		return getComponentId() + "_" + "NoPage";
	}

	public String nombreLigneParPageID() {
		return getComponentId() + "_" + "NbLignePage";
	}

	public WOActionResults afficherPremierePage() {
		setNumPage(Integer.valueOf(1));
		updateDisplayedObjects();
		return null;
	}

	public WOActionResults afficherDernierePage() {
		setNumPage(maxPage);
		updateDisplayedObjects();
		return null;
	}

	public WOActionResults afficherPagePrecedente() {
		setNumPage(numPage - Integer.valueOf(1));
		updateDisplayedObjects();
		return null;
	}

	public WOActionResults afficherPageSuivante() {
		setNumPage(numPage + Integer.valueOf(1));
		updateDisplayedObjects();
		return null;
	}

	public WOActionResults changerNumberOfObjectsPerBatch() {
		updateDisplayedObjects();
		return null;
	}

	public WOActionResults changerNumeroPage() {
		updateDisplayedObjects();
		return null;
	}

	public Integer numberOfObjectsPerBatch() {
		return numberOfObjectsPerBatch;
	}

	public void setNumberOfObjectsPerBatch(Integer numberOfObjectsPerBatch) {
		this.numberOfObjectsPerBatch = numberOfObjectsPerBatch;
	}

	public Integer numPage() {
		return numPage;
	}

	/**
	 * @param numPage the numPage to set
	 */
	public void setNumPage(Integer numPage) {
		this.numPage = numPage;
	}

	public NSArray<Integer> getPagesList() {
		if (pagesList.count() != maxPage) {
			pagesList.removeAllObjects();
			for (int i = 1; i <= maxPage; i++)
				pagesList.addObject(i);
		}
		return pagesList;
	}

	public NSArray<EOAttribute> getAttributes() {
		NSMutableArray<EOAttribute> res = new NSMutableArray<EOAttribute>();
		EOEntity entity = EOModelGroup.defaultGroup().entityNamed(getEntityName());

		NSArray<CktlAjaxTableViewColumn> cols = getColonnes();
		for (int i = 0; i < cols.count(); i++) {
			CktlAjaxTableViewColumn cktlAjaxTableViewColumn = (CktlAjaxTableViewColumn) cols.objectAtIndex(i);
			String key = cktlAjaxTableViewColumn.associations().getValue();
			int index = key.lastIndexOf(".");
			key = key.substring(index + 1);
			res.addObject(entity.attributeNamed(key));
		}

		return res.immutableClone();
	}

	/**
	 * Met à jour les objets à afficher en effectuant une requete sql.
	 */
	@SuppressWarnings({
			"rawtypes", "unchecked"
	})
	public void updateDisplayedObjects() {
		if (logger.isDebugEnabled()) {
			logger.debug("Recherche de banque avec qualifier =" + getQualifier());
		}
		EOFetchSpecification spec = new EOFetchSpecification(getEntityName(), getQualifier(), getSortOrderings());
		EOModel model = EOModelGroup.defaultGroup().entityNamed(getEntityName()).model();
		ERXSQLHelper sqlHelper = ERXSQLHelper.newSQLHelper(edc(), model.name());
		int start = (numPage().intValue() - 1) * numberOfObjectsPerBatch();
		int end = start + numberOfObjectsPerBatch();

		NSMutableArray<EOAttribute> attributes = new NSMutableArray<EOAttribute>();
		attributes.addObjectsFromArray(getAttributes());
		attributes.addObjectsFromArray(EOModelGroup.defaultGroup().entityNamed(getEntityName()).primaryKeyAttributes());

		//Recuperer le nombre total d'enregistrements
		int maxRows = ERXEOAccessUtilities.rowCountForFetchSpecification(edc(), spec);
		maxPage = BigDecimal.valueOf(maxRows).divide(BigDecimal.valueOf(numberOfObjectsPerBatch()), BigDecimal.ROUND_HALF_UP).intValue();
		if (maxPage.intValue() == 0 && maxRows > 0) {
			maxPage = Integer.valueOf(1);
		}

		//Recuperer les enregistrements
		EOSQLExpression sqlExpr = sqlHelper.sqlExpressionForFetchSpecification(edc(), spec, start, end, attributes);
		NSArray<NSDictionary> result = ERXEOAccessUtilities.rawRowsForSQLExpression(edc(), model, sqlExpr, attributes);

		dg().setObjectArray(result);
		if (!result.isEmpty()) {
			dg().setSelectedObject(result.get(0));
			selectionner();
		}
		dg().updateDisplayedObjects();
	}

	public String getContainerTbvId() {
		return getContainerId() + "tb";
	}

	public WOActionResults selectionner() {
		setValueForBinding(selection(), BINDING_SELECTION);
		return null;
	}

	public EOEnterpriseObject selection() {
		EOEnterpriseObject res = null;
		NSDictionary<String, ?> selection = (NSDictionary<String, ?>) dg().selectedObject();
		if (selection != null) {
			res = EOUtilities.objectFromRawRow(edc(), getEntityName(), selection);
		}
		return res;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public EOEditingContext edc() {
		if (hasBinding(BINDING_editingContext)) {
			return (EOEditingContext) valueForBinding(BINDING_editingContext);
		}
		return super.edc();
	}

	public Boolean isDernierePageDisabled() {
		return Boolean.valueOf(numPage.equals(maxPage));
	}

	public Boolean isPremierePageDisabled() {
		return Boolean.valueOf(numPage.equals(minPage));
	}

	public Boolean isPagePrecedenteDisabled() {
		return Boolean.valueOf(numPage.equals(minPage));
	}

	public Boolean isNumeroPageDisabled() {
		getPagesList();
		return Boolean.valueOf(pagesList.count() < 2);
	}

	public Boolean isPageSuivanteDisabled() {
		return Boolean.valueOf(numPage.equals(maxPage));
	}

	public String helpTip() {
		return (String) valueForBinding(BINDING_HELP_TIP);
	}

	public Boolean hasHelpTip() {
		return Boolean.valueOf(helpTip() != null);
	}
}