package org.cocktail.fwkcktlajaxwebext.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSKeyValueCoding;

/**
 * 
 * @author jlafourc
 *
 */
public class CktlAjaxFilterPopupButton extends CktlAjaxWOComponent {
    
	private static final long serialVersionUID = -662718765804079159L;

	private static final String BINDING_DISPLAY_STRING_KEY = "displayStringKey";
	private Object currentValue;
	
	/**
	 * 
	 * @param context le context
	 */
	public CktlAjaxFilterPopupButton(WOContext context) {
        super(context);
    }
      
    /**
     * Fait un valueForKey sur l'objet avec la valeur
     * récupérée en binding
     * @return la chaine à afficher
     */
    public String getDisplayString() {
    	if (getCurrentValue() instanceof NSKeyValueCoding) {
    		NSKeyValueCoding kvc = (NSKeyValueCoding) getCurrentValue();
    		return (String) kvc.valueForKey(getDisplayStringKey());
    	}
    	return ""; 
    }

	public Object getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(Object currentValue) {
		this.currentValue = currentValue;
	}
	
	public String getDisplayStringKey() {
		return stringValueForBinding(BINDING_DISPLAY_STRING_KEY, null);
	}
    
}