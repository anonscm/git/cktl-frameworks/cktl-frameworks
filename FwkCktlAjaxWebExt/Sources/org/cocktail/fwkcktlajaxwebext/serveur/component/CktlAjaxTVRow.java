package org.cocktail.fwkcktlajaxwebext.serveur.component;

import java.util.UUID;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;

import er.extensions.appserver.ERXRedirect;

public class CktlAjaxTVRow extends WOComponent {

	private String componentUniqueId = "cktl_" + UUID.randomUUID().toString().replaceAll("-", "_");

	public CktlAjaxTVRow(WOContext context) {
		super(context);
	}

	@Override
	public WOActionResults invokeAction(WORequest arg0, WOContext arg1) {
		return super.invokeAction(arg0, arg1);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public WOActionResults dblClickAction() {
		if (isAjax()) {
			return (WOActionResults) valueForBinding("dblClickAction");
		}

		else {

			ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
			WOComponent results = (WOComponent) valueForBinding("dblClickAction");
			redirect.setComponent(results);
			return redirect;
		}

	}

	public String rowClass() {
		if (hasBinding("rowClass")) {
			return (String) valueForBinding("rowClass");
		}
		return "";
	}

	public Boolean isAjax() {
		if (hasBinding("isAjax")) {
			return (Boolean) valueForBinding("isAjax");
		}
		else {
			return true;
		}
	}

	public Boolean hasDblClick() {
		return hasBinding("dblClickAction");
	}

	public String onClickFunctionName() {
		return componentUniqueId + "_onClickFunction";
	}

	public String onDblClickFunctionName() {
		return componentUniqueId + "_onDblClickFunction";
	}

	public String onClick() {
		return onClickFunctionName() + "();";
	}

	public String onDblClick() {
		return onDblClickFunctionName() + "();";
	}

}