/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column;

import java.text.Format;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

public class CktlAjaxTVTextFieldCell extends CktlAjaxTVCell {
	
	private Object value;
    private String dateformat;
    private String numberformat;
    private Format formatter;
    private Boolean disabled;
    private String classCss;
    private String styleCss;
    private String size;
    
    public CktlAjaxTVTextFieldCell(WOContext context) {
        super(context);
    }

    public void reset() {
    	super.reset();
    	value = null;
    	dateformat = null;
    	numberformat = null;
    	formatter = null;
       	disabled = null;
       	classCss = null;
       	styleCss = null;
       	size = null;
    }
	/**
	 * @return the value
	 */
	public Object value() {
		return valueForBinding("value");
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * @return the formatter
	 */
	public Format formatter() {
		return (Format)valueForBinding("formatter");
	}

	/**
	 * @param formatter the formatter to set
	 */
	public void setFormatter(Format formatter) {
		this.formatter = formatter;
	}

	/**
	 * @return the disabled
	 */
	public Boolean disabled() {
		return (Boolean)valueForBinding("disabled");
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * @return the classCss
	 */
	public String classCss() {
		return (String)valueForBinding("classCss");
	}

	/**
	 * @param classCss the classCss to set
	 */
	public void setClassCss(String classCss) {
		this.classCss = classCss;
	}

	/**
	 * @return the styleCss
	 */
	public String styleCss() {
		return (String)valueForBinding("styleCss");
	}

	/**
	 * @param styleCss the styleCss to set
	 */
	public void setStyleCss(String styleCss) {
		this.styleCss = styleCss;
	}

	/**
	 * @return the size
	 */
	public String size() {
		return (String)valueForBinding("size");
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}
	

	/**
	 * @return the dateformat
	 */
	public String dateformat() {
		return (String)valueForBinding("dateformat");
	}

	/**
	 * @param dateformat the dateformat to set
	 */
	public void setDateformat(String dateformat) {
		this.dateformat = dateformat;
	}

	/**
	 * @return the numberformat
	 */
	public String numberformat() {
		return (String)valueForBinding("numberformat");
	}

	/**
	 * @param numberformat the numberformat to set
	 */
	public void setNumberformat(String numberformat) {
		this.numberformat = numberformat;
	}
	
	public boolean isString() {
		return !(isDate() || isNumber());
	}

	public boolean isDate() {
		return hasBinding("dateformat") || (value() instanceof NSTimestamp && !hasBinding("formatter"));
	}
	public boolean isNumber() {
		return hasBinding("numberformat") || (value() instanceof Number && !hasBinding("formatter"));
	}


}
