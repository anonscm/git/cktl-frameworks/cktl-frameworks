/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component.tableview;

import java.util.Enumeration;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.ajax.AjaxUpdateContainer;
import er.extensions.batching.ERXBatchingDisplayGroup;
import er.extensions.foundation.ERXArrayUtilities;

public class CktlAjaxTableView extends CktlAjaxWOComponent {
	public static final String HEADER_CSS_STYLE = "headerCssStyle";
	public static final String HEADER_CSS_CLASS = "headerCssClass";

	public static final String BDG_ID = "id";
	public static final String BDG_DISPLAY_GROUP = "dg";
	private static final String BDG_ITEM = "item";
	private static final String BDG_COLONNES = "colonnes";
	public static final String BDG_SELECTION_ENABLED = "selectionEnabled";
	private static final String BDG_SELECTION_MULTIPLE_ENABLED = "selectionMultipleEnabled";
	private static final String BDG_FORCE_UNIQUE_SELECTION = "forceUniqueSelection";
	private static final String BDG_TRI_MULTICOLONNE_ENABLED = "triMultiColonneEnabled";
	public static final String BDG_UPDATE_CONTAINER_ID = "updateContainerID";
	public static final String BDG_UPDATE_CONTAINER_IDS = "updateContainerIDs";
	private static final String BDG_CALLBACK_ON_SELECTIONNER = "callbackOnSelectionner";
	private static final String BDG_CSS_STYLE = "cssStyle";
	private static final String BDG_CSS_CLASS = "cssClass";
	private static final String BDG_HEIGHT = "height";
	private static final String BDG_WIDTH = "width";

	private static final String BDG_ROW_CSS_CLASS = "rowCssClass";

	private static final String BDG_AFFICHER_BARRE_DE_NAVIGATION = "afficherBarreDeNavigation";
	private static final String BDG_WIDTH_BARRE_DE_NAVIGATION = "widthBarreDeNavigation";
	private static final String BDG_AFFICHER_BARRE_DE_STATUT = "afficherBarreDeStatut";
	private static final String BDG_WIDTH_BARRE_DE_STATUT = "widthBarreDeStatut";

	/** true|false. Affiche ou non l'en-tete de la tableView. Par defaut true. */
	public static final String BDG_SHOULD_DISPLAY_HEADER = "shouldDisplayHeader";

	private static final Boolean DEFAULT_SELECTION_ENABLED_KEY = Boolean.FALSE;
	private static final Boolean DEFAULT_SELECTION_MULTIPLE_ENABLED_KEY = Boolean.FALSE;
	private static final Boolean DEFAULT_TRI_MULTICOLONNE_ENABLED_KEY = Boolean.FALSE;
	private static final String DEFAULT_CSS_CLASS_KEY = "cktlajaxtableview ";
	private static final String DEFAULT_CSS_STYLE_KEY = "border:none; ";
	private static final Boolean DEFAULT_SHOULD_DISPLAY_HEADER = Boolean.TRUE;

	public String id;
	private WODisplayGroup dg;
	public NSKeyValueCoding item;
	public int indexLigne, indexColonne;
	private NSArray colonnes;
	private NSMutableDictionary uneColonne, uneColonne1;
	private Boolean isSelectionEnabled;
	private Boolean isSelectionMultipleEnabled;
	private Boolean triMultiColonneEnabled;
	private boolean afficherBarreDeNavigation;
	private boolean afficherBarreDeStatut;
	private Boolean isLigneSelectionnee;
	private String cssClass;
	private String cssStyle;
	private String height;
	private String width;
	private String updateContainerID;
	private String headerCssClass;
	private Integer numberOfObjectsPerBatch;
	private Integer numPage;
	private NSMutableArray<Integer> pagesList;

	public CktlAjaxTableView(WOContext context) {
		super(context);
		id = null;
		dg = null;
		colonnes = null;
		uneColonne = null;
		//Rod 21/07/2009 : ne pas initialiser sinon c'est toujours la valeur par defaut qui sera prise 
		//		isSelectionEnabled = DEFAULT_SELECTION_ENABLED_KEY;
		//		isSelectionMultipleEnabled = DEFAULT_SELECTION_MULTIPLE_ENABLED_KEY;
		//		triMultiColonneEnabled = DEFAULT_TRI_MULTICOLONNE_ENABLED_KEY;
		afficherBarreDeNavigation = false;
		afficherBarreDeStatut = false;
		isLigneSelectionnee = null;
		cssClass = null;
		cssStyle = null;
		pagesList = new NSMutableArray();
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}
	
	@Override
	public void reset() {
		super.reset();
		id = null;
		dg = null;
		colonnes = null;
		uneColonne = null;
		afficherBarreDeNavigation = false;
		afficherBarreDeStatut = false;
		isLigneSelectionnee = null;
		cssClass = null;
		cssStyle = null;
		pagesList = new NSMutableArray();
	}
	
	private void refreshUpdateContainerID() {
		String updateContainerID = (String) valueForBinding(BDG_UPDATE_CONTAINER_ID);
		if (updateContainerID != null)
			AjaxUpdateContainer.updateContainerWithID(updateContainerID, context());
	}

	public WOActionResults selectAll() {
		if (dg() != null) {
			dg().selectObjectsIdenticalTo(dg().allObjects());
		}
		refreshUpdateContainerID();
		if (hasBinding(BDG_CALLBACK_ON_SELECTIONNER))
			performParentAction((String) valueForBinding(BDG_CALLBACK_ON_SELECTIONNER));
		return null;
	}

	public WOActionResults selectNone() {
		if (dg() != null) {
			dg().selectObjectsIdenticalTo(NSArray.EmptyArray);
		}
		refreshUpdateContainerID();
		if (hasBinding(BDG_CALLBACK_ON_SELECTIONNER))
			performParentAction((String) valueForBinding(BDG_CALLBACK_ON_SELECTIONNER));
		return null;
	}

	@SuppressWarnings({
			"rawtypes", "unchecked"
	})
	public WOActionResults selectInverse() {
		if (dg() != null) {
			NSArray unSelectedObjects = ERXArrayUtilities.arrayMinusArray(dg().allObjects(), dg().selectedObjects());
			dg().selectObjectsIdenticalTo(NSArray.EmptyArray);
			dg().selectObjectsIdenticalTo(unSelectedObjects);
		}
		refreshUpdateContainerID();
		if (hasBinding(BDG_CALLBACK_ON_SELECTIONNER))
			performParentAction((String) valueForBinding(BDG_CALLBACK_ON_SELECTIONNER));
		return null;
	}

	public boolean isAfficherIndicateurDeTri() {
		boolean isAfficherIndicateurDeTri = false;
		NSMutableDictionary colonne = uneColonne();
		String tri = (String) colonne.objectForKey(CktlAjaxTableViewColumn.TRI_KEY);
		if (tri != null) {
			isAfficherIndicateurDeTri = true;
		}
		return isAfficherIndicateurDeTri;
	}

	public String indicateurTypeDeTri() {
		String indicateurTypeDeTri = "";
		NSMutableDictionary colonne = uneColonne();
		indicateurTypeDeTri = (new StringBuilder(String.valueOf(indicateurTypeDeTri))).append("images/").append(colonne.objectForKey(CktlAjaxTableViewColumn.TRI_KEY)).append(".gif").toString();
		return indicateurTypeDeTri;
	}

	public WOActionResults trierColonne() {
		NSMutableDictionary colonne = uneColonne();
		String keyPath = (String) colonne.objectForKey(CktlAjaxTableViewColumn.ORDER_KEY_PATH_KEY);
		if (keyPath != null) {
			EOSortOrdering colonneSortOrdering = EOSortOrdering.sortOrderingWithKey(keyPath, EOSortOrdering.CompareAscending);
			NSMutableArray sortOrderings = new NSMutableArray(dg().sortOrderings());
			EOKeyValueQualifier qualifier = new EOKeyValueQualifier("key", EOKeyValueQualifier.QualifierOperatorEqual, keyPath);
			NSArray array = EOQualifier.filteredArrayWithQualifier(sortOrderings, qualifier);
			if (array != null && array.count() > 0) {
				colonneSortOrdering = (EOSortOrdering) array.lastObject();
				if (colonneSortOrdering.selector().equals(EOSortOrdering.CompareAscending)) {
					int index = sortOrderings.indexOf(colonneSortOrdering);
					sortOrderings.add(index, EOSortOrdering.sortOrderingWithKey(keyPath, EOSortOrdering.CompareDescending));
					colonne.setObjectForKey("Descending", CktlAjaxTableViewColumn.TRI_KEY);
				}
				else {
					colonne.removeObjectForKey(CktlAjaxTableViewColumn.TRI_KEY);
				}
				sortOrderings.remove(colonneSortOrdering);
			}
			else {
				if (triMultiColonneEnabled() != null && !triMultiColonneEnabled().booleanValue()) {
					NSMutableDictionary uneColonne;
					for (Enumeration enumColonnes = colonnes().objectEnumerator(); enumColonnes.hasMoreElements(); uneColonne.removeObjectForKey(CktlAjaxTableViewColumn.TRI_KEY)) {
						uneColonne = (NSMutableDictionary) enumColonnes.nextElement();
					}

					sortOrderings = new NSMutableArray();
				}
				sortOrderings.add(colonneSortOrdering);
				colonne.setObjectForKey("Ascending", CktlAjaxTableViewColumn.TRI_KEY);
			}
			dg().setSortOrderings(sortOrderings);
			dg().updateDisplayedObjects();
		}
		return null;
	}

	public WODisplayGroup dg() {
		return (WODisplayGroup) valueForBinding(BDG_DISPLAY_GROUP);
	}

	public void setDg(WODisplayGroup dg) {
		this.dg = dg;
		setValueForBinding(dg, BDG_DISPLAY_GROUP);
	}

	public NSArray colonnes() {
		//if (colonnes == null) {
		colonnes = (NSArray) valueForBinding(BDG_COLONNES);
		//}
		return colonnes;
	}

	public void setColonnes(NSArray colonnes) {
		this.colonnes = colonnes;
	}

	public void setUneColonne(NSMutableDictionary uneColonne) {
		this.uneColonne = uneColonne;
	}

	public NSMutableDictionary uneColonne() {
		return uneColonne;
	}

	public void setUneColonne1(NSMutableDictionary uneColonne) {
		uneColonne1 = uneColonne;
	}

	public NSMutableDictionary uneColonne1() {
		return uneColonne1;
	}

	public String colonneComponentName() {
		NSMutableDictionary colonne = uneColonne1();
		String componentName = (String) colonne.valueForKey(CktlAjaxTableViewColumn.COMPONENT);
		return componentName == null ? "CktlAjaxTVStringCell" : componentName;
	}

	public boolean isTrue() {
		return true;
	}

	public boolean isFalse() {
		return false;
	}

	public CktlAjaxTableView thisComponent() {
		return this;
	}

	public NSDictionary associations() {
		return (NSDictionary) uneColonne1().objectForKey(CktlAjaxTableViewColumn.ASSOCIATIONS_KEY);
	}

	public int indexTri() {
		int indexTri = 0;
		NSMutableDictionary colonne = uneColonne();
		String keyPath = (String) colonne.objectForKey(CktlAjaxTableViewColumn.ORDER_KEY_PATH_KEY);
		EOSortOrdering colonneSortOrdering = null;
		NSMutableArray sortOrderings = new NSMutableArray(dg().sortOrderings());
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier("key", EOKeyValueQualifier.QualifierOperatorEqual, keyPath);
		NSArray array = EOQualifier.filteredArrayWithQualifier(sortOrderings, qualifier);
		if (array != null && array.count() > 0) {
			colonneSortOrdering = (EOSortOrdering) array.lastObject();
			indexTri = sortOrderings.indexOfObject(colonneSortOrdering);
		}
		return indexTri + 1;
	}

	public Boolean isLigneSelectionnee() {
		Boolean isLigneSelectionnee = Boolean.FALSE;
		if (dg() != null && item() != null) {
			NSArray selectedObjects = dg().selectedObjects();
			if (selectedObjects != null && selectedObjects.contains(item())) {
				isLigneSelectionnee = Boolean.TRUE;
			}
		}
		return isLigneSelectionnee;
	}

	public void setIsLigneSelectionnee(Boolean isLigneSelectionnee) {
		this.isLigneSelectionnee = isLigneSelectionnee;
		if (dg() != null && item != null) {
			NSArray selectedObjects = dg().selectedObjects();
			if (isLigneSelectionnee.booleanValue()) {
				selectedObjects.add(item);
			}
			else {
				selectedObjects.remove(item);
			}
			dg().selectObjectsIdenticalTo(selectedObjects);
		}
	}

	public Boolean isSelectionEnabled() {
		isSelectionEnabled = DEFAULT_SELECTION_ENABLED_KEY;
		//			isSelectionEnabled = Boolean.TRUE;
		if (hasBinding(BDG_SELECTION_ENABLED)) {
			isSelectionEnabled = (Boolean) valueForBinding(BDG_SELECTION_ENABLED);
		}
		NSMutableDictionary colonne = uneColonne1();
		if (colonne != null) {
			NSDictionary associations = (NSDictionary) colonne.objectForKey(CktlAjaxTableViewColumn.ASSOCIATIONS_KEY);
			String colonneSelectionEnabled = (String) associations.valueForKey("selectionEnabled");
			if (colonneSelectionEnabled != null && Boolean.valueOf(colonneSelectionEnabled).booleanValue() == false) {
				isSelectionEnabled = Boolean.FALSE;
			}
		}
		//		if (isSelectionEnabled == null) {
		//			isSelectionEnabled = DEFAULT_SELECTION_ENABLED_KEY;
		//			//			isSelectionEnabled = Boolean.TRUE;
		//			if (hasBinding(BDG_SELECTION_ENABLED)) {
		//				isSelectionEnabled = (Boolean) valueForBinding(BDG_SELECTION_ENABLED);
		//			}
		//			NSMutableDictionary colonne = uneColonne1();
		//			if (colonne != null) {
		//				NSDictionary associations = (NSDictionary)colonne.objectForKey(CktlAjaxTableViewColumn.ASSOCIATIONS_KEY);
		//				String colonneSelectionEnabled = (String)associations.valueForKey("selectionEnabled");
		//				if (colonneSelectionEnabled != null && Boolean.valueOf(colonneSelectionEnabled).booleanValue()==false) {
		//					isSelectionEnabled = Boolean.FALSE;
		//				}
		//			}
		//		}
		return isSelectionEnabled;
	}

	public void setIsSelectionEnabled(Boolean isSelectionEnabled) {
		this.isSelectionEnabled = isSelectionEnabled;
	}

	public Boolean isSelectionMultipleEnabled() {
		if (isSelectionMultipleEnabled == null) {
			isSelectionMultipleEnabled = Boolean.FALSE;
			if (hasBinding(BDG_SELECTION_MULTIPLE_ENABLED)) {
				isSelectionMultipleEnabled = (Boolean) valueForBinding(BDG_SELECTION_MULTIPLE_ENABLED);
			}
		}
		return isSelectionMultipleEnabled;
	}

	public void setIsSelectionMultipleEnabled(Boolean isSelectionMultipleEnabled) {
		this.isSelectionMultipleEnabled = isSelectionMultipleEnabled;
	}

	public boolean isForceUniqueSelectionEnabled() {
		return booleanValueForBinding(BDG_FORCE_UNIQUE_SELECTION, false);
	}

	public Boolean triMultiColonneEnabled() {
		triMultiColonneEnabled = DEFAULT_TRI_MULTICOLONNE_ENABLED_KEY;
		if (hasBinding(BDG_TRI_MULTICOLONNE_ENABLED)) {
			triMultiColonneEnabled = (Boolean) valueForBinding(BDG_TRI_MULTICOLONNE_ENABLED);
		}
		return triMultiColonneEnabled;
	}

	public void setTriMultiColonneEnabled(Boolean triMultiColonneEnabled) {
		this.triMultiColonneEnabled = triMultiColonneEnabled;
	}

	public WOActionResults selectionnerLigneWithCheckBox() {
		return selectionnerLigne(true);
	}

	public WOActionResults selectionnerLigne() {
		return selectionnerLigne(false);
	}

	private WOActionResults selectionnerLigne(boolean checkBox) {
		WODisplayGroup localDg = dg();
		if (localDg != null && item() != null) {
			// On peut sélectionner plusieurs lignes si :
			// - la selection multiple est activée
			// - ET (on sélectionne avec une checkbox OU on n'est pas interdit de le faire avec une sélection de ligne normale (sans checkbox))
			// TODO : prendre en compte le comportement de forceUniqueSelection côté client !
			if (isSelectionMultipleEnabled().booleanValue() && (checkBox || !isForceUniqueSelectionEnabled())) {
				NSArray selectedObjects = localDg.selectedObjects();
				if (selectedObjects != null && selectedObjects.count() > 0) {
					isLigneSelectionnee = Boolean.valueOf(!selectedObjects.remove(item()));
					if (isLigneSelectionnee.booleanValue()) {
						//						selectedObjects.add(item());
						selectedObjects = selectedObjects.arrayByAddingObject(item());
					}
				}
				else {
					isLigneSelectionnee = Boolean.TRUE;
					//					selectedObjects.add(item());
					if (selectedObjects != null)  {
						selectedObjects = selectedObjects.arrayByAddingObject(item());
					}
				}

				//localDg.setSelectedObjects(null);
				//Rodolphe : Dans le cas d'une nouvelle selection nulle il faut egalement 
				//informer les eventuels delegate, donc ne pas supprimer la selection 
				//initiale car le displaygroup informe ses delegate que lorsque la selection a changé
				//FIXME est-ce que localDg.setSelectedObjects(null) est nécessaire dans les autres cas ?
				if (selectedObjects != null && selectedObjects.count() > 0) {
					localDg.setSelectedObjects(null);
				}
				localDg.selectObjectsIdenticalTo(selectedObjects);

			}
			else {
				// EOCustomObject ligneSelectionnee = (EOCustomObject)localDg.selectedObject();
				//localDg.setSelectedObjects(null);
				localDg.selectObject(item());
				//Rod : garder ca sinon pb avec les ERXBatchingFisplayGroup
				if (localDg instanceof ERXBatchingDisplayGroup<?>) {
					localDg.setSelectedObjects(new NSArray(item()));
				}
			}
			performParentAction((String) valueForBinding(BDG_CALLBACK_ON_SELECTIONNER));
		}
		return null;
	}

	public String trLigneId() {
		String trLigneId = (String) valueForBinding(BDG_ID) + "_" + (new StringBuilder("TrLigne")).append(indexLigne).toString();
		return trLigneId;
	}

	public String onSuccessTdLigneSelection() {
		String onSuccessTdLigneSelection = "function () {";
		onSuccessTdLigneSelection = (new StringBuilder(String.valueOf(onSuccessTdLigneSelection))).append(trLigneId()).append("Update();" + (isSelectionMultipleEnabled().booleanValue() ? thToggleSelectAllContainerID() + "Update();" : "") + "}").toString();
		return onSuccessTdLigneSelection;
	}

	public String onCompleteValeurColonneObserver() {
		String onCompleteValeurColonneObserver = null;
		if (hasBinding(BDG_UPDATE_CONTAINER_ID) || hasBinding(BDG_UPDATE_CONTAINER_IDS)) {
		    if (onCompleteValeurColonneObserver == null) {
		    	onCompleteValeurColonneObserver = "function(){";
		    } else {
		    	onCompleteValeurColonneObserver = onCompleteValeurColonneObserver.substring(0, onCompleteValeurColonneObserver.length()-1);
		    }
	        if (hasBinding(BDG_UPDATE_CONTAINER_ID)) {
	        	onCompleteValeurColonneObserver += (String) valueForBinding(BDG_UPDATE_CONTAINER_ID) + "Update();";
	        }
	        if (hasBinding(BDG_UPDATE_CONTAINER_IDS)) {
	            String containers = (String) valueForBinding(BDG_UPDATE_CONTAINER_IDS);
	            NSArray containerIDs = NSArray.componentsSeparatedByString(containers, ",");
	            for (Object container : containerIDs) {
	            	onCompleteValeurColonneObserver += (String) container + "Update();";
	            }
	        }
	        onCompleteValeurColonneObserver += "}";
		}
		
		return onCompleteValeurColonneObserver;
	}

	public String cssClass() {
		cssClass = DEFAULT_CSS_CLASS_KEY;
		if (hasBinding(BDG_CSS_CLASS)) {
			cssClass += (String) valueForBinding(BDG_CSS_CLASS);
		}
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public String cssStyle() {
		cssStyle = DEFAULT_CSS_STYLE_KEY;
		if (hasBinding(BDG_CSS_STYLE)) {
			cssStyle += (String) valueForBinding(BDG_CSS_STYLE);
		}
		if (hasBinding(BDG_WIDTH)) {
			cssStyle += " width:" + (String) valueForBinding(BDG_WIDTH) + ";";
		}

		return cssStyle;
	}

	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}

	public String headerStyleCss() {
		String headerStyleCss = (String) uneColonne().objectForKey(CktlAjaxTableViewColumn.HEADER_CSS_STYLE_KEY);
		if (isAfficherIndicateurDeTri()) {
			String imagePath = "images/";
			NSMutableDictionary colonne = uneColonne();
			String tri = (String) colonne.objectForKey(CktlAjaxTableViewColumn.TRI_KEY);
			imagePath = (new StringBuilder(String.valueOf(imagePath))).append(tri).append(".gif").toString();
			if (headerStyleCss == null) {
				headerStyleCss = "";
			}
			headerStyleCss = (new StringBuilder("background: #FF0000 url(")).append(imagePath).append(") no-repeat scroll right top;").append(headerStyleCss).toString();
		}
		return headerStyleCss;
	}

	public String headerCssClass() {
		String headerCssClass = (String) uneColonne().objectForKey(CktlAjaxTableViewColumn.HEADER_CSS_CLASS_KEY);
		return headerCssClass;
	}

	public void setHeaderCssClass(String headerCssClass) {
		this.headerCssClass = headerCssClass;
	}

	public WOActionResults afficherPremierePage() {
		WODisplayGroup localDg = dg();
		if (localDg != null) {
			NSArray selectedObjects = localDg.selectedObjects();
			localDg.setCurrentBatchIndex(1);
			localDg.selectObjectsIdenticalTo(selectedObjects);
			localDg.updateDisplayedObjects();
		}
		return null;
	}

	public WOActionResults afficherDernierePage() {
		WODisplayGroup localDg = dg();
		if (localDg != null) {
			NSArray selectedObjects = localDg.selectedObjects();
			localDg.setCurrentBatchIndex(localDg.batchCount());
			localDg.selectObjectsIdenticalTo(selectedObjects);
			localDg.updateDisplayedObjects();
		}
		return null;
	}

	public WOActionResults afficherPagePrecedente() {
		WODisplayGroup localDg = dg();
		if (localDg != null) {
			NSArray selectedObjects = localDg.selectedObjects();
			localDg.displayPreviousBatch();
			localDg.selectObjectsIdenticalTo(selectedObjects);
			localDg.updateDisplayedObjects();
		}
		return null;
	}

	public WOActionResults afficherPageSuivante() {
		WODisplayGroup localDg = dg();
		if (localDg != null) {
			NSArray selectedObjects = localDg.selectedObjects();
			localDg.displayNextBatch();
			localDg.selectObjectsIdenticalTo(selectedObjects);
			localDg.updateDisplayedObjects();
		}
		return null;
	}

	public WOActionResults changerNumberOfObjectsPerBatch() {
		dg().updateDisplayedObjects();
		return null;
	}

	public WOActionResults changerNumeroPage() {
		WODisplayGroup localDg = dg();
		NSArray selectedObjects = localDg.selectedObjects();
		localDg.selectObjectsIdenticalTo(selectedObjects);
		localDg.updateDisplayedObjects();
		return null;
	}

	public Integer numberOfObjectsPerBatch() {
		return Integer.valueOf(dg().numberOfObjectsPerBatch());
	}

	public void setNumberOfObjectsPerBatch(Integer numberOfObjectsPerBatch) {
		if (numberOfObjectsPerBatch == null) {
			return;
		}
		this.numberOfObjectsPerBatch = numberOfObjectsPerBatch;
		WODisplayGroup localDg = dg();
		NSArray selectedObjects = localDg.selectedObjects();
		if (localDg.allObjects().count() < numberOfObjectsPerBatch.intValue()) {
			numberOfObjectsPerBatch = Integer.valueOf(localDg.allObjects().count());
			this.numberOfObjectsPerBatch = numberOfObjectsPerBatch;
		}
		localDg.setNumberOfObjectsPerBatch(numberOfObjectsPerBatch.intValue());
		localDg.selectObjectsIdenticalTo(selectedObjects);
	}

	public boolean isAfficherBarreDeNavigation() {
		return booleanValueForBinding(BDG_AFFICHER_BARRE_DE_NAVIGATION, Boolean.FALSE).booleanValue();
		//return ((Boolean)valueForBinding(BDG_AFFICHER_BARRE_DE_NAVIGATION)).booleanValue();
	}

	public void setAfficherBarreDeNavigation(boolean afficherBarreDeNavigation) {
		this.afficherBarreDeNavigation = afficherBarreDeNavigation;
	}

	public boolean isAfficherBarreDeStatut() {
		return booleanValueForBinding(BDG_AFFICHER_BARRE_DE_STATUT, Boolean.FALSE).booleanValue();
		//return ((Boolean)valueForBinding(BDG_AFFICHER_BARRE_DE_STATUT)).booleanValue();
	}

	public void setAfficherBarreDeStatut(boolean afficherBarreDeStatut) {
		this.afficherBarreDeStatut = afficherBarreDeStatut;
	}

	public String classLigne() {
		String classLigne = "odd";
		if (indexLigne % 2 == 0) {
			classLigne = "even";
		}
		if (isSelectionEnabled()) {
			classLigne += " selectionable";
		}
		if (dg() != null && item != null) {
			NSArray selectedObjects = dg().selectedObjects();
			if ((isSelectionEnabled().booleanValue() || isSelectionMultipleEnabled().booleanValue()) &&
					selectedObjects != null && selectedObjects.count() > 0 &&
					selectedObjects.contains(item)) {
				// if (selectedObjects != null && selectedObjects.count() > 0 && selectedObjects.contains(item)) {
				classLigne = (new StringBuilder("selected ")).append(classLigne).toString();
				//            NSArray updatedObjects = uneLigne.editingContext().updatedObjects();
				//            NSArray insertedObjects = uneLigne.editingContext().insertedObjects();
				//            if (updatedObjects != null && updatedObjects.count() > 0 && updatedObjects.contains(uneLigne) || insertedObjects != null && insertedObjects.count() > 0 && insertedObjects.contains(uneLigne))
				//                classLigne = (new StringBuilder("updated ")).append(classLigne).toString();
			}
		}
		return classLigne;
	}

	public String thSupClass() {
		String thSupClass = null;
		NSMutableDictionary colonne = uneColonne();
		String tri = (String) colonne.objectForKey(CktlAjaxTableViewColumn.TRI_KEY);
		if (tri != null) {
			if (thSupClass == null) {
				thSupClass = "";
			}
			thSupClass = (new StringBuilder(CktlAjaxTableViewColumn.TRI_KEY)).append(tri).toString();
		}
		return thSupClass;
	}

	public WOActionResults deleteSelectedItems() {
		WODisplayGroup localDg = dg();
		localDg.deleteSelection();
		localDg.selectObjectsIdenticalTo(null);
		localDg.updateDisplayedObjects();
		return null;
	}

	public boolean isDeleteSelectedItemsDisabled() {
		boolean isDeleteSelectedItemsDisabled = true;
		WODisplayGroup localDg = dg();
		if (localDg != null && localDg.selectedObjects() != null && localDg.selectedObjects().count() > 0) {
			isDeleteSelectedItemsDisabled = false;
		}
		return isDeleteSelectedItemsDisabled;
	}

	public int colspanTDBarreNavigation() {
		int colspanTDBarreNavigation = 1;
		colspanTDBarreNavigation = colonnes().count();
		if (isSelectionEnabled().booleanValue()) {
			colspanTDBarreNavigation++;
		}
		return colspanTDBarreNavigation;
	}

	public WOActionResults submit() {
		return null;
	}

	public String colonneComponentId() {
		String colonneComponentId = "";
		NSMutableDictionary colonne = uneColonne1();
		String id = (String) colonne.objectForKey(BDG_ID);
		if (id == null || id.equals(colonneComponentId)) {
			id = (String) colonne.objectForKey("libelle");
		}
		colonneComponentId = (new StringBuilder(String.valueOf(id))).append(indexLigne).toString();
		return colonneComponentId;
	}

	public String onRefreshComplete() {
		String onRefreshComplete = null;
		String updateContainerID = (String) valueForBinding(BDG_UPDATE_CONTAINER_ID);
		if (updateContainerID != null) {
			onRefreshComplete = updateContainerID + "Update();";
		}
		return onRefreshComplete;
	}

	/**
	 * @return the updateContainerID
	 */
	public String updateContainerID() {
		return updateContainerID;
	}

	/**
	 * @param updateContainerID the updateContainerID to set
	 */
	public void setUpdateContainerID(String updateContainerID) {
		this.updateContainerID = updateContainerID;
	}

	/**
	 * @return the item
	 */
	public NSKeyValueCoding item() {
		// return item;
		return (NSKeyValueCoding) valueForBinding(BDG_ITEM);
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(NSKeyValueCoding item) {
		this.item = item;
		setValueForBinding(item, BDG_ITEM);
	}

	public WOComponent pere() {
		return parent();
	}

	public String cssStyleForDivBarreDeNavigation() {
		String cssStyleForDivBarreDeNavigation = null;

		if (hasBinding(BDG_WIDTH_BARRE_DE_NAVIGATION)) {
			cssStyleForDivBarreDeNavigation = "width:" + (String) valueForBinding(BDG_WIDTH_BARRE_DE_NAVIGATION) + ";";
		}
		return cssStyleForDivBarreDeNavigation;
	}

	public String cssStyleForDivHeaderAndBody() {
		String cssStyleForDivHeaderAndBody = null;

		if (hasBinding(BDG_HEIGHT)) {
			cssStyleForDivHeaderAndBody = "height:" + (String) valueForBinding(BDG_HEIGHT) + ";";
		}
		return cssStyleForDivHeaderAndBody;
	}

	public String cssStyleForDivBarreDeStatut() {
		String cssStyleForDivBarreDeStatut = null;

		if (hasBinding(BDG_WIDTH_BARRE_DE_STATUT)) {
			cssStyleForDivBarreDeStatut = "width:" + (String) valueForBinding(BDG_WIDTH_BARRE_DE_STATUT) + ";";
		}
		return cssStyleForDivBarreDeStatut;
	}

	/**
	 * @return the height
	 */
	public String height() {
		return (String) valueForBinding(BDG_HEIGHT);
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(String height) {
		this.height = height;
	}

	/**
	 * @return the width
	 */
	public String width() {
		return (String) valueForBinding(BDG_WIDTH);
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(String width) {
		this.width = width;
	}

	public boolean isColonneTriable() {
		boolean isColonneTriable = false;
		NSDictionary laColonne = uneColonne();

		// TODO : Implementer la regle suivante si il est possible de trier a partir de la value contenue dans le dico d'associations
		//		NSDictionary associations = (NSDictionary)laColonne.objectForKey(CktlAjaxTableViewColumn.ASSOCIATIONS_KEY);
		//		if (laColonne != null && 
		//			( (!laColonne.containsKey(CktlAjaxTableViewColumn.TRI_ACTIF_KEY) ||
		//			  Boolean.valueOf((String)laColonne.valueForKey(CktlAjaxTableViewColumn.TRI_ACTIF_KEY)).equals(Boolean.TRUE)) &&
		//			  (laColonne.containsKey(CktlAjaxTableViewColumn.ORDER_KEY_PATH_KEY) ||
		//			  ( associations != null && associations.containsKey("value") )
		//			  )) 
		//		   ) {
		//			isColonneTriable = true;
		//		}

		// Pour l'instant, on considere qu'il est possible de trier une colonne qd il existe le binding "orderKeyPath" associe a la colonne
		if (laColonne != null && laColonne.containsKey(CktlAjaxTableViewColumn.ORDER_KEY_PATH_KEY)) {
			isColonneTriable = true;
		}
		return isColonneTriable;
	}

	private String onCompleteTdLigneSelectionHelper(boolean deselectOthers) {
		String onCompleteTdLigneSelection = "function(){";
		if (isSelectionMultipleEnabled() && !deselectOthers) {
			onCompleteTdLigneSelection += "if ($('" + trLigneId() + "').hasClassName('selected')) {";
			onCompleteTdLigneSelection += "$('" + trLigneId() + "').removeClassName('selected');$$('#" + trLigneId() + " td>input').first().checked = false;} else {";
			onCompleteTdLigneSelection += "$('" + trLigneId() + "').addClassName('selected');$$('#" + trLigneId() + " td>input').first().checked = true;};";
		}
		else if (isSelectionMultipleEnabled() && deselectOthers) {
			// Déselection de toutes les lignes
			onCompleteTdLigneSelection += "$$('#" + (String) valueForBinding(BDG_ID) + " table tr.selected').each(function(e) { " +
					"e.getElementsBySelector('input[type=checkbox]').first().checked = false;e.removeClassName('selected'); });";
			// Sélection de la ligne courante
			onCompleteTdLigneSelection += "$('" + trLigneId() + "').addClassName('selected');$$('#" + trLigneId() + " td>input').first().checked = true;";
		}
		else {
			onCompleteTdLigneSelection += "var selections = $$('#" + (String) valueForBinding(BDG_ID) + " table tr.selected'); if (selections.size()>0){selections.first().removeClassName('selected');};";
			onCompleteTdLigneSelection += "$('" + trLigneId() + "').addClassName('selected');";
		}

		if (hasBinding(BDG_UPDATE_CONTAINER_ID) && valueForBinding(BDG_UPDATE_CONTAINER_ID) != null) {
			onCompleteTdLigneSelection += (String) valueForBinding(BDG_UPDATE_CONTAINER_ID) + "Update();";
		}
		if (hasBinding(BDG_UPDATE_CONTAINER_IDS)) {
			String containers = (String) valueForBinding(BDG_UPDATE_CONTAINER_IDS);
			NSArray containerIDs = NSArray.componentsSeparatedByString(containers, ",");
			for (Object container : containerIDs) {
				onCompleteTdLigneSelection += (String) container + "Update();";
			}
		}
		if (isSelectionMultipleEnabled()) {
			onCompleteTdLigneSelection += thToggleSelectAllContainerID() + "Update();";
		}
		if (isAfficherBarreDeNavigation()) {
			onCompleteTdLigneSelection += divBarreDeNavigationID() + "Update();";
		}
		if (isAfficherBarreDeStatut()) {
			onCompleteTdLigneSelection += divBarreDeStatutID() + "Update();";
		}
		onCompleteTdLigneSelection += divToolBarContentID() + "Update();";

		onCompleteTdLigneSelection += "}";
		return onCompleteTdLigneSelection;
	}

	public String onCompleteTdLigneSelectionCheckBox() {
		return onCompleteTdLigneSelectionHelper(false);
	}

	public String onCompleteTdLigneSelection() {
		return onCompleteTdLigneSelectionHelper(isForceUniqueSelectionEnabled());
	}

	public Boolean shouldDispayHeader() {
		if (valueForBinding(BDG_SHOULD_DISPLAY_HEADER) == null) {
			return DEFAULT_SHOULD_DISPLAY_HEADER;
		}
		return (Boolean) valueForBinding(BDG_SHOULD_DISPLAY_HEADER);
	}

	public String getTdCssClass() {
		String TdCssClass = (String) uneColonne1().valueForKey(BDG_ROW_CSS_CLASS);
		if ((hasBinding(BDG_ROW_CSS_CLASS))
				&& !"".equals(valueForBinding(BDG_ROW_CSS_CLASS))) {
			TdCssClass = (new StringBuilder(valueForBinding(BDG_ROW_CSS_CLASS)
					+ " ")).append(TdCssClass).toString();
		}
		return TdCssClass;
	}

	/**
	 * @return the numPage
	 */
	public Integer numPage() {
		return numPage;
	}

	/**
	 * @param numPage the numPage to set
	 */
	public void setNumPage(Integer numPage) {
		this.numPage = numPage;
	}

	public NSArray<Integer> getPagesList() {
		if (pagesList.count() != dg().batchCount()) {
			pagesList.removeAllObjects();
			for (int i = 1; i <= dg().batchCount(); i++)
				pagesList.addObject(i);
		}
		return pagesList;
	}

	public String tdValeurColonneID() {
		String tdValeurColonneID = (String) valueForBinding(BDG_ID) + "_colonne_" + indexLigne + "_" + indexColonne;
		return tdValeurColonneID;
	}

	public String divHeaderAndBodyID() {
		String divHeaderAndBodyID = (String) valueForBinding(BDG_ID) + "_" + "DivHeaderAndBody";
		return divHeaderAndBodyID;
	}

	public String divBarreDeNavigationID() {
		String divBarreDeNavigationID = (String) valueForBinding(BDG_ID) + "_" + "DivBarreDeNavigation";
		return divBarreDeNavigationID;
	}

	public String divBarreDeStatutID() {
		String divBarreDeStatutID = (String) valueForBinding(BDG_ID) + "_" + "DivBarreDeStatut";
		return divBarreDeStatutID;
	}

	public String divToolBarContentID() {
		String divToolBarContentID = (String) valueForBinding(BDG_ID) + "_" + "DivToolBar";
		return divToolBarContentID;
	}

	public String formBarreNavID() {
		return (String) valueForBinding(BDG_ID) + "_" + "FormBarreNav";
	}

	public String numeroPageID() {
		return (String) valueForBinding(BDG_ID) + "_" + "NoPage";
	}

	public String nombreLigneParPageID() {
		return (String) valueForBinding(BDG_ID) + "_" + "NbLignePage";
	}

	public String thToggleSelectAllContainerID() {
		return getComponentId() + "_ThToggleSelectAllContainer";
	}

	public String cbLigneSelectionId() {
		return getComponentId() + "_cbSelection_" + indexLigne;
	}

	public boolean isSimpleMenuDisabled() {
		boolean isSimpleMenuDisabled = true;
		if (dg() != null && dg().displayedObjects().count() > 1) {
			isSimpleMenuDisabled = false;
		}
		return isSimpleMenuDisabled;
	}

	public NSArray displayedObjects() {
		NSArray res = dg().displayedObjects();
		return res;
	}

}
