/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.CktlAjaxTableView;

import com.webobjects.foundation.NSMutableDictionary;

/**
 * Represente la description d'un colonne pour le composant {@link CktlAjaxTableView}.<br/>
 * Exemple :<br/>
 * <code>
 * 	public static final NSMutableArray _colonnesForGroupesListe = new NSMutableArray();<br/>
 * 	static {<br/>
 * 		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();<br/>
 * 		col1.setLibelle("Groupes");<br/>
 * 		col1.setKeyPath(EOStructure.LIBELLE_FOR_GROUPE_KEY);<br/>
 * 		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(col1.getKeyPath(), " ");<br/>
 *      ass1.setFormatter("application.app2DicmalesFormatter");
 * 		col1.addToAssociations(ass1);<br/>
 * 		_colonnesForGroupesListe.addObject(col1);<br/>
 * 	}<br/>
 * </code>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class CktlAjaxTableViewColumn extends NSMutableDictionary {
	public static final String LIBELLE_KEY = "libelle";
	public static final String ASSOCIATIONS_KEY = "associations";
	public static final String ORDER_KEY_PATH_KEY = "orderKeyPath";
	public static final String HEADER_CSS_STYLE_KEY = "headerCssStyle";
	public static final String HEADER_CSS_CLASS_KEY = "headerCssClass";
	public static final String ROW_CSS_STYLE_KEY = "rowCssStyle";
	public static final String ROW_CSS_CLASS_KEY = "rowCssClass";
	public static final String COMPONENT = "component";
	public static final String UPDATE_CONTAINER = "updateContainer";
	//	public static final String COMPONENT_EDIT = "component_edit";

	public static final String TRI_ACTIF_KEY = "triActif";
	public static final String TRI_KEY = "tri";

	public CktlAjaxTableViewColumn() {
		super();
	}

	public String getLibelle() {
		return (String) valueForKey(LIBELLE_KEY);
	}

	public void setLibelle(String libelle) {
		takeValueForKey(libelle, LIBELLE_KEY);
	}

	public void setOrderKeyPath(String keyPath) {
		takeValueForKey(keyPath, ORDER_KEY_PATH_KEY);
	}

	public String getOrderKeyPath() {
		return (String) valueForKey(ORDER_KEY_PATH_KEY);
	}

	public void setHeaderCssStyle(String headerCssStyle) {
		takeValueForKey(headerCssStyle, HEADER_CSS_STYLE_KEY);
	}

	public String getHeadCssStyle() {
		return (String) valueForKey(HEADER_CSS_STYLE_KEY);
	}
	
	public void setHeaderCssClass(String headerCssClass) {
	    takeValueForKey(headerCssClass, HEADER_CSS_CLASS_KEY);
	}
	
	public String getHeadCssClass() {
	    return (String) valueForKey(HEADER_CSS_CLASS_KEY);
	}

	public void setRowCssStyle(String rowCssStyle) {
		takeValueForKey(rowCssStyle, ROW_CSS_STYLE_KEY);
	}

	public String getRowCssStyle() {
		return (String) valueForKey(ROW_CSS_STYLE_KEY);
	}

	public void setRowCssClass(String rowCssClass) {
		takeValueForKey(rowCssClass, ROW_CSS_CLASS_KEY);
	}

	public String getRowCssClass() {
		return (String) valueForKey(ROW_CSS_CLASS_KEY);
	}

	public void setAssociations(CktlAjaxTableViewColumnAssociation associations) {
		takeValueForKey(associations, ASSOCIATIONS_KEY);
	}

	public CktlAjaxTableViewColumnAssociation associations() {
		return (CktlAjaxTableViewColumnAssociation) valueForKey(ASSOCIATIONS_KEY);
	}

	//	public void setAssociations(NSMutableArray associations) {
	//		takeValueForKey(associations, ASSOCIATIONS_KEY);
	//	}
	//	
	//	public NSMutableArray associations() {
	//		return (NSMutableArray) valueForKey(ASSOCIATIONS_KEY);
	//	}
	//	public void addToAssociations(CktlAjaxTableViewColumnAssociation association) {
	//		if (associations() == null) {
	//			setAssociations(new NSMutableArray());
	//		}
	//		if (associations().indexOf(association) == NSArray.NotFound) {
	//			associations().addObject(association);
	//		}
	//		
	//	}

	public String getComponent() {
		return (String) valueForKey(COMPONENT);
	}

	public void setComponent(String component) {
		takeValueForKey(component, COMPONENT);
	}

	public String getUpdateContainer() {
		return (String) valueForKey(UPDATE_CONTAINER);
	}

	public void setUpdateContainer(String updateContainer) {
		takeValueForKey(updateContainer, UPDATE_CONTAINER);
	}

}
