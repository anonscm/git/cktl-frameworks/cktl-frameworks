/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component.tableview;

import java.lang.reflect.InvocationTargetException;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation._NSUtilities;

public class CktlAjaxEditableTableView extends CktlAjaxTableView {
    
	public static final String BDG_DISABLED = "disabled";

	private Boolean disabled = null;
    private Boolean editable = null;

	public CktlAjaxEditableTableView(WOContext context) {
        super(context);
    }

	public void reset() {
		disabled = null;
		editable = null;
	}
	
	public boolean isAutoSubmitEnabled() {
		Boolean isAutoSubmitEnabled = true;
		NSSelector sel = new NSSelector("isAutoSubmitEnabled");
		try {
			isAutoSubmitEnabled = (Boolean) sel.invoke(_NSUtilities.classWithName(colonneComponentName()));
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isAutoSubmitEnabled.booleanValue();
	}

//	public String onCompleteValeurColonneObserver() {
//		String onCompleteValeurColonneObserver = null;
//		if (hasBinding(BDG_UPDATE_CONTAINER_ID) || hasBinding(BDG_UPDATE_CONTAINER_IDS)) {
//		    onCompleteValeurColonneObserver = "function(){";
//	        if (hasBinding(BDG_UPDATE_CONTAINER_ID)) {
//	        	onCompleteValeurColonneObserver += (String) valueForBinding(BDG_UPDATE_CONTAINER_ID) + "Update();";
//	        }
//	        if (hasBinding(BDG_UPDATE_CONTAINER_IDS)) {
//	            String containers = (String) valueForBinding(BDG_UPDATE_CONTAINER_IDS);
//	            NSArray containerIDs = NSArray.componentsSeparatedByString(containers, ",");
//	            for (Object container : containerIDs) {
//	            	onCompleteValeurColonneObserver += (String) container + "Update();";
//	            }
//	        }
//	        onCompleteValeurColonneObserver += "}";
//		}
//        return onCompleteValeurColonneObserver;
//	}

	public String onCompleteValeurColonneObserver() {
		String onCompleteValeurColonneObserver = null;
		NSMutableDictionary colonne = uneColonne1();
		String updateContainer = (String) colonne.valueForKey(CktlAjaxTableViewColumn.UPDATE_CONTAINER);
		if (updateContainer != null) {
		    onCompleteValeurColonneObserver = "function(){";
			if (updateContainer.equalsIgnoreCase("TableView")) {
				onCompleteValeurColonneObserver += (String)valueForBinding(BINDING_id)+"Update();";
			} else if (updateContainer.equalsIgnoreCase("Ligne")) {
				onCompleteValeurColonneObserver += trLigneId()+"Update();";
			} else if (updateContainer.equalsIgnoreCase("Cellule")) {
				onCompleteValeurColonneObserver += tdValeurColonneID()+"Update();";
			}
	        onCompleteValeurColonneObserver += "}";
		}
		if (hasBinding(BDG_UPDATE_CONTAINER_ID) || hasBinding(BDG_UPDATE_CONTAINER_IDS)) {
		    if (onCompleteValeurColonneObserver == null) {
		    	onCompleteValeurColonneObserver = "function(){";
		    } else {
		    	onCompleteValeurColonneObserver = onCompleteValeurColonneObserver.substring(0, onCompleteValeurColonneObserver.length()-1);
		    }
	        if (hasBinding(BDG_UPDATE_CONTAINER_ID)) {
	        	onCompleteValeurColonneObserver += (String) valueForBinding(BDG_UPDATE_CONTAINER_ID) + "Update();";
	        }
	        if (hasBinding(BDG_UPDATE_CONTAINER_IDS)) {
	            String containers = (String) valueForBinding(BDG_UPDATE_CONTAINER_IDS);
	            NSArray containerIDs = NSArray.componentsSeparatedByString(containers, ",");
	            for (Object container : containerIDs) {
	            	onCompleteValeurColonneObserver += (String) container + "Update();";
	            }
	        }
	        onCompleteValeurColonneObserver += "}";
		}
		
		return onCompleteValeurColonneObserver;
	}

	/**
	 * @return the disabled
	 */
	public Boolean disabled() {
		if (disabled == null) {
			disabled = (Boolean)booleanValueForBinding(BDG_DISABLED, Boolean.FALSE);
		}
		return disabled;
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * @return the editable
	 */
	public Boolean editable() {
		if (editable == null) {
			editable = !disabled();
		}
		return editable;
	}

	/**
	 * @param editable the editable to set
	 */
	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

}
