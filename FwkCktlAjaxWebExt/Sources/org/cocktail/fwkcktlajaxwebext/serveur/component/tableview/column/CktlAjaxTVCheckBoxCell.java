/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column;

import com.webobjects.appserver.WOContext;

import er.extensions.components.ERXComponentUtilities;

public class CktlAjaxTVCheckBoxCell extends CktlAjaxTVCell {
	
	private Boolean checked;
	private Object value;
	private Object selection;
	private Boolean disabled;

	public CktlAjaxTVCheckBoxCell(WOContext context) {
        super(context);
    }

	/**
	 * @return the checked
	 */
	public Boolean checked() {
		return (Boolean)valueForBinding("checked");
	}

	/**
	 * @param checked the checked to set
	 */
	public void setChecked(Boolean checked) {
		this.checked = checked;
		setValueForBinding(checked, "checked");
	}

	/**
	 * @return the value
	 */
	public Object value() {
		return (Object)valueForBinding("value");
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * @return the selection
	 */
	public Object selection() {
		return (Object)valueForBinding("selection");
	}

	/**
	 * @param selection the selection to set
	 */
	public void setSelection(Object selection) {
		this.selection = selection;
		setValueForBinding(selection, "selection");
	}

	/**
	 * @return the disabled
	 */
	public Boolean disabled() {
		Boolean disabled = Boolean.TRUE;
		if (editable()) {
			disabled = ERXComponentUtilities.booleanValueForBinding(this, "disabled");
		}
		return disabled;
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isCheckedBindingPresent() {
		return hasBinding("checked");
	}

}
