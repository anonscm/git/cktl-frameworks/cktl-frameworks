/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOCustomObject;
import com.webobjects.foundation.NSDictionary;

public class CktlAjaxTVCell extends WOComponent {
	
	private static String BDG_LIGNE = "ligne";
	private static String BDG_COLONNE = "colonne";
	private static String BDG_EDITABLE = "editable";
	
	private String style;
	private EOCustomObject ligne;
	private Boolean editable;
	private NSDictionary colonne;
	private WOComponent component;
	
	public CktlAjaxTVCell(WOContext wocontext) {
		super(wocontext);
	}

    public boolean synchronizesVariablesWithBindings() {
    	return false;
    }
    public boolean isStateless() {
        return true;
    }

    public void reset() {
    	style = null;
    	ligne = null;
    	editable = null;
    	colonne = null;
    	component = null;
    }
	/**
	 * @return the ligne
	 */
	public EOCustomObject ligne() {
		return (EOCustomObject)valueForBinding(BDG_LIGNE);
	}
	/**
	 * @param ligne the ligne to set
	 */
	public void setLigne(EOCustomObject ligne) {
		this.ligne = ligne;
		setValueForBinding(ligne, BDG_LIGNE);
	}

	/**
	 * @return the colonne
	 */
	public NSDictionary colonne() {
		return (NSDictionary) valueForBinding(BDG_COLONNE);
	}

	/**
	 * @param colonne the colonne to set
	 */
	public void setColonne(NSDictionary colonne) {
		this.colonne = colonne;
	}

	/**
	 * @return the editable
	 */
	public Boolean editable() {
		if (hasBinding(BDG_EDITABLE)) {
			return (Boolean) valueForBinding(BDG_EDITABLE);
		}
		return Boolean.FALSE;
	}

	/**
	 * @param editable the editable to set
	 */
	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the style
	 */
	public String style() {
		return style;
	}

	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}

	static public Boolean isAutoSubmitEnabled() {
		return Boolean.TRUE;
	}
}
