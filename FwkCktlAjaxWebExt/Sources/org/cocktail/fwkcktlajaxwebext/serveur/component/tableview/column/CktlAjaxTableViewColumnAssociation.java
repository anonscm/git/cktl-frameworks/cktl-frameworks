/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column;

import com.webobjects.foundation.NSMutableDictionary;

/**
 * Represente un objet de type Association pour {@link CktlAjaxTableViewColumn}.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class CktlAjaxTableViewColumnAssociation extends NSMutableDictionary {
	public static final String VALUE_KEY = "value";
	public static final String VALUE_WHEN_EMPY_KEY = "valueWhenEmpty";
	public static final String NUMBER_FORMAT_KEY = "numberformat";
	public static final String DATE_FORMAT_KEY = "dateformat";
	public static final String FORMATTER_KEY = "formatter";
	public static final String ESCAPE_HTML_KEY = "escapeHTML";

	public CktlAjaxTableViewColumnAssociation() {
		super();
	}

	/**
	 * @param value
	 * @param valueWhenEmpty (valeur à afficher quand la valeur "normale" est vide). Sous forme statique. (Voir aussi
	 *            {@link CktlAjaxTableViewColumnAssociation#setValueWhenEmptyDynamique(String)})
	 */
	public CktlAjaxTableViewColumnAssociation(String value, String valueWhenEmpty) {
		this();
		setValue(value);
		setValueWhenEmpty(valueWhenEmpty);
	}

	public void setValue(String value) {
		takeValueForKey(value, VALUE_KEY);
	}

	public String getValue() {
		return (String) valueForKey(VALUE_KEY);
	}

	/**
	 * Voir aussi {@link CktlAjaxTableViewColumnAssociation#setValueWhenEmptyDynamique(String)}
	 * 
	 * @param valueWhenEmpty Permet d'indiquer la valeur à afficher dans le cas ou la valeur normale est nulle. valueWhenEmpty doit être indiqué sous
	 *            forme "statique"
	 */
	public void setValueWhenEmpty(String valueWhenEmpty) {
		if (valueWhenEmpty != null) {
			valueWhenEmpty = "\"" + valueWhenEmpty + "\"";
		}
		takeValueForKey(valueWhenEmpty, VALUE_WHEN_EMPY_KEY);
	}

	/**
	 * @param valueWhenEmpty Permet d'indiquer la valeur à afficher dans le cas ou la valeur normale est nulle. valueWhenEmpty doit être indiqué sous
	 *            forme dynamique (une méthode accessible via du keyValueCoding au niveau du composant contenant la CktlAjaxTableView)
	 */
	public void setValueWhenEmptyDynamique(String valueWhenEmpty) {
		takeValueForKey(valueWhenEmpty, VALUE_WHEN_EMPY_KEY);
	}

	public String getValueWhenEmpty() {
		return (String) valueForKey(VALUE_WHEN_EMPY_KEY);
	}

	public String getDateformat() {
		return (String) valueForKey(DATE_FORMAT_KEY);
	}

	/**
	 * Voir aussi {@link CktlAjaxTableViewColumnAssociation#setDateformatDynamique(String)}
	 * 
	 * @param dateFormat Permet d'indiquer un dateFormat sous forme statique (exemple : "dd/MM/yyyy")
	 */
	public void setDateformat(String dateFormat) {
		if (dateFormat != null) {
			dateFormat = "\"" + dateFormat + "\"";
		}
		takeValueForKey(dateFormat, DATE_FORMAT_KEY);
	}

	/**
	 * @param dateFormat Permet d'indiquer un dateFormat sous forme dynamique (une méthode accessible via du keyValueCoding au niveau du composant
	 *            contenant la CktlAjaxTableView)
	 */
	public void setDateformatDynamique(String dateFormat) {
		takeValueForKey(dateFormat, DATE_FORMAT_KEY);
	}

	public String getNumberformat() {
		return (String) valueForKey(NUMBER_FORMAT_KEY);
	}

	/**
	 * Voir aussi {@link CktlAjaxTableViewColumnAssociation#setNumberformatDynamique(String)}
	 * 
	 * @param numberFormat Permet d'indiquer un numberFormat sous forme statique (exemple : "#0.00")
	 */
	public void setNumberformat(String numberFormat) {
		if (numberFormat != null) {
			numberFormat = "\"" + numberFormat + "\"";
		}
		takeValueForKey(numberFormat, NUMBER_FORMAT_KEY);
	}

	/**
	 * @param numberFormat Permet d'indiquer un numberFormat sous formedynamique (une méthode accessible via du keyValueCoding au niveau du composant
	 *            contenant la CktlAjaxTableView)
	 */
	public void setNumberformatDynamique(String numberFormat) {
		takeValueForKey(numberFormat, NUMBER_FORMAT_KEY);
	}

	public String getEscapeHTML() {
		return (String) valueForKey(ESCAPE_HTML_KEY);
	}

	public void setEscapeHTML(String escapeHTML) {
		takeValueForKey(escapeHTML, ESCAPE_HTML_KEY);
	}

	/**
	 * @param formatter keyValueCoding pour acceder au formatter
	 */
	public void setFormatter(String formatter) {
		takeValueForKey(formatter, FORMATTER_KEY);
	}

	public String getFormatter() {
		return (String) valueForKey(FORMATTER_KEY);
	}

}
