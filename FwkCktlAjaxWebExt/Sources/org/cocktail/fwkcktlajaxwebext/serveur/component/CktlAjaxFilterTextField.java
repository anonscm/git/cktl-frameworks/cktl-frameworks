package org.cocktail.fwkcktlajaxwebext.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxUtils;


public class CktlAjaxFilterTextField extends CktlAjaxWOComponent {
  
	private static final long serialVersionUID = 5557670007751315934L;
	
	private static final String BINDING_ISFILTERAUTOMATIC = "isFilterAutomatic";
	private static final String BINDING_TIMEOUT = "timeout";
	
	private static final Integer DEFAULT_FILTER_FIELD_TIMEOUT = 1000;
	

    public CktlAjaxFilterTextField(WOContext context) {
        super(context);
    }
    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
        super.appendToResponse(response, context);
        AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlajaxfiltertextfield.js");
    }
    
    public String functionName() {
    	return getComponentId() + "_submitFunction";
    }

    public String getFilterFieldId() {
    	return getComponentId() + "_filterField"; 
    }
    
    public Boolean isFilterAutomatic() {
    	return valueForBooleanBinding(BINDING_ISFILTERAUTOMATIC, true);
    }
    
    public Integer getTimeout() {
    	return valueForIntegerBinding(BINDING_TIMEOUT, DEFAULT_FILTER_FIELD_TIMEOUT);
    }
    
    public Boolean hasTimeout() {
    	return hasBinding(BINDING_TIMEOUT);
    }
    
}
