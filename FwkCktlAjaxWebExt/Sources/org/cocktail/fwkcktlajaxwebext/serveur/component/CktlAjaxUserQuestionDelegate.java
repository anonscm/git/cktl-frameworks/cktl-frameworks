/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlajaxwebext.serveur.component;

import java.util.LinkedHashMap;

import com.webobjects.appserver.WOComponent;

import er.ajax.AjaxUtils;
import er.ajax.CktlAjaxUtils;

/**
 * Prend en charge la generation de questions a poser a l'utilisateur lors d'un traitement (à l'interieur d'un WOActionResults), typiquement sur une
 * action reliee a un AjaxUpdateLink (l'implementation actuelle ne fonctionne pas avec des AjaxSumitButton)<br/>
 * Cette classe prend en charge par defaut des questions affichees sous forme "confirm" avec reponses Oui/Annuler de javascript. <br/>
 * Creez votre propre classe {@link IClientQuestionGenerator} pour changer le comportement, regardez le code de la classe
 * {@link YesCancelJsClientQuestionGenerator} pour voir le fonctionnement.<br/>
 * <br/>
 * Des methodes de convenances {@link CktlAjaxUserQuestionDelegate#askUserAsBoolean(String, String, String)} ou
 * {@link CktlAjaxUserQuestionDelegate#askUserAsString(String, String, String)} sont disponibles suivant les cas. <br/>
 * <b><i>Important</i></b><br/>
 * Vous devez imperativement appeler la methode {@link CktlAjaxUserQuestionDelegate#clearAnswers()} une fois toutes les reponses traitees, sinon la
 * deuxieme fois que le traitement est execute, les reponses donnees la premiere fois seront utilisees et les questions non posees. <br/>
 * <br/>
 * <b><i>Exemple</i></b><br/>
 * 
 * <pre>
 * public MyComponent(WOContext context) {
 * 	super(context);
 * 	userQuestionDelegate = new CktlAjaxUserQuestionDelegate(this);
 * }
 * 
 * public WOActionResults faireTraitement() {
 * 	now = null;
 * 	boolean allConfirmed = false;
 * 	boolean cancel = false;
 * 
 * 	Boolean confirm1 = userQuestionDelegate.askUserAsBoolean(&quot;confirm1&quot;, &quot;Confirmation 1&quot;, getComponentId());
 * 	if (confirm1 == null) {
 * 		//La reponse n'a pas encore été donnée, on pose la question
 * 		return null;
 * 	}
 * 
 * 	if (confirm1.booleanValue()) {
 * 		Boolean confirm2 = userQuestionDelegate.askUserAsBoolean(&quot;confirm2&quot;, &quot;Confirmation 2 ?&quot;, getComponentId());
 * 		if (confirm2 == null) {
 * 			//La reponse n'a pas encore été donnée, on pose la question
 * 			return null;
 * 		}
 * 
 * 		if (confirm2.booleanValue()) {
 * 			allConfirmed = true;
 * 		}
 * 		else {
 * 			cancel = true;
 * 		}
 * 	}
 * 	else {
 * 		cancel = true;
 * 	}
 * 
 * 	//si tout est confirme
 * 	if (allConfirmed) {
 * 		//Faire traitement
 * 		System.out.println(&quot;Traitement effectue.&quot;);
 * 		now = new NSTimestamp();
 * 	}
 * 	if (allConfirmed || cancel) {
 * 		userQuestionDelegate.clearAnswers();
 * 	}
 * 	return null;
 * }
 * 
 * 
 * 
 * </pre>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class CktlAjaxUserQuestionDelegate {
	private WOComponent component;

	/**
	 * Map qui contient en cle l'identifiant de la question et en valeur la reponse donnee par l'utilisateur.
	 */
	private LinkedHashMap reponses = new LinkedHashMap();

	private IClientQuestionGenerator clientQuestionGenerator;

	public CktlAjaxUserQuestionDelegate(WOComponent comp) {
		this(comp, null);
	}

	public CktlAjaxUserQuestionDelegate(WOComponent comp, IClientQuestionGenerator aClientQuestionGenerator) {
		component = comp;
		if (aClientQuestionGenerator == null) {
			aClientQuestionGenerator = new YesCancelJsClientQuestionGenerator();
		}
		clientQuestionGenerator = aClientQuestionGenerator;
	}

	/**
	 * Pose une question a l'utilisateur.<br/>
	 * Techniquement, le code html necessaire à l'affichage de la question est ajoute a la reponse si celle-ci n'a pas encore ete posee et la methode
	 * renvoie alors null, sinon elle renvoie l'objet correspondant a la reponse.
	 * 
	 * @param key Identifiant de la question
	 * @param msg Message a afficher a l'utilisateur
	 * @param updateContainerID L'ID du container servant a interpreter la response html.
	 * @return null si on n'a pas la reponse a la question, sinon renvoie la reponse de l'utilisateur interpretee par IClientQuestionGenerator.
	 */
	public Object askUser(String key, String msg, String updateContainerID) {
		//Verifier si on a la cle dans les parametres de la requete
		String confirm = (String) component.context().request().formValueForKey(key);
		//Une reponse a ete retournee
		if (confirm != null) {
			reponses.put(key, confirm);
		}
		//pas de reponse retournee, on pose la question si celle-ci n'a pas ete posee
		else {
			if (reponses.get(key) == null) {
				addQuestionToResponse(key, updateContainerID, msg);
			}
		}
		return clientQuestionGenerator.parseReponse(reponses.get(key));
	}

	/**
	 * Methode de convenance a utiliser a la place de askUser dans le cas ou la methode askUser renvoie un objet de type Boolean (fonctionnement par
	 * defaut par exemple si vous n'avez pas specifie de clientQuestionGenerator).
	 * 
	 * @param key
	 * @param msg
	 * @param updateContainerID indispensable (logiquement l'id du composant)
	 * @return Null si la reponse n'est pas encore donnee, TRUE ou FALSE sinon.
	 */
	public Boolean askUserAsBoolean(String key, String msg, String updateContainerID) {
		return (Boolean) askUser(key, msg, updateContainerID);
	}

	public String askUserAsString(String key, String msg, String updateContainerID) {
		return (String) askUser(key, msg, updateContainerID);
	}

	/**
	 * Ajoute a la reponse html le code necessaire pour poser la question a l'utilisateur.
	 * 
	 * @param key L'identifiant de la cle a ajouter a la requete (pour la reponse a la question)
	 * @param updateContainerId
	 */
	public void addQuestionToResponse(String key, String updateContainerId, String msg) {
		clientQuestionGenerator.addQuestionToResponse(key, updateContainerId, msg);
	}

	/**
	 * Nettoie les reponses collectees.
	 */
	public void clearAnswers() {
		reponses.clear();
	}

	public interface IClientQuestionGenerator {
		public void addQuestionToResponse(String key, String updateContainerId, String msg);

		public Object parseReponse(Object reponse);
	}

	/**
	 * Generateur de question par defaut.
	 * 
	 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
	 */
	public class YesCancelJsClientQuestionGenerator implements IClientQuestionGenerator {
		public static final String REPONSE_YES = "1";
		public static final String REPONSE_CANCEL = "0";

		public void addQuestionToResponse(String key, String updateContainerId, String msg) {
			String actionUrl = AjaxUtils.ajaxComponentActionUrl(component.context());
			String actionUrlYes = "'" + actionUrl + "?" + key + "=" + REPONSE_YES + "'";
			String actionUrlCancel = "'" + actionUrl + "?" + key + "=" + REPONSE_CANCEL + "'";
			String ajaxOptions = "{asynchronous: true, evalScripts: true, method:'get'}";
			StringBuffer onYesBuffer = new StringBuffer();
			onYesBuffer.append("new Ajax.Updater('" + updateContainerId + "', " + actionUrlYes + ", " + ajaxOptions);
			onYesBuffer.append(");");

			StringBuffer onCancelBuffer = new StringBuffer();
			onCancelBuffer.append("new Ajax.Updater('" + updateContainerId + "', " + actionUrlCancel + ", " + ajaxOptions);
			onCancelBuffer.append(");");
			component.context().response().appendContentString("<script language=\"javascript\">if (confirm('" + CktlAjaxUtils.jsEncode(msg) + "')){ " + onYesBuffer + "  } else {" + onCancelBuffer + "};</script>");
		}

		public Object parseReponse(Object reponse) {
			if (reponse == null) {
				return null;
			}
			return Boolean.valueOf(REPONSE_YES.equals(reponse));
		}

	}

}
