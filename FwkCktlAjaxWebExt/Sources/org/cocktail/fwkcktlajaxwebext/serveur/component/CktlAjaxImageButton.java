package org.cocktail.fwkcktlajaxwebext.serveur.component;

import com.webobjects.appserver.WOContext;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

public class CktlAjaxImageButton extends CktlAjaxWOComponent {
    public CktlAjaxImageButton(WOContext context) {
        super(context);
    }
}