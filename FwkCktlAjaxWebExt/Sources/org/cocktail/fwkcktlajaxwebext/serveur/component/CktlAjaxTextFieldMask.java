package org.cocktail.fwkcktlajaxwebext.serveur.component;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.extensions.appserver.ERXResponseRewriter;
import er.extensions.components.ERXComponent;

/**
 * 
 * Composant facilitant l'utilisation du plugin jQuery Mask.
 * 
 * @binding idTextField l'id du textfield surlequel on veut appliquer le masque
 * @binding mask le masque 
 * 
 * @see http://igorescobar.github.io/jQuery-Mask-Plugin/
 * 
 * @author Alexis Tual
 *
 */
public class CktlAjaxTextFieldMask extends ERXComponent {
    
    private static final long serialVersionUID = 1L;
    
    public static final String BINDING_ID_TEXTFIELD = "idTextField";
    public static final String BINDING_MASK = "mask";
    
    /**
     * @param context le context
     */
    public CktlAjaxTextFieldMask(WOContext context) {
        super(context);
    }

    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
        ERXResponseRewriter.addScriptResourceInHead(response, context, "FwkCktlAjaxWebExt", "scripts/jquery.mask.min.js");
        super.appendToResponse(response, context);
    }
    
    /**
     * @return l'id du textfield
     */
    public String idTextField() {
        return stringValueForBinding(BINDING_ID_TEXTFIELD);
    }
    
    /**
     * @return le masque
     */
    public String mask() {
        return stringValueForBinding(BINDING_MASK);
    }
    
    /**
     * @return le js pour appliquer le masque
     */
    public String jsMask() {
        String  mask =  mask();
        String js = "";
        if (mask != null) {
            js =  "jQuery(document).ready(function(){ jQuery('#" + idTextField() + "').mask('" + mask + "'); });";
        }
        return js;
    }
    
    @Override
    public boolean isStateless() {
        return true;
    }
    
}