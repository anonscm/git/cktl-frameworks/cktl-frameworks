/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.ajax.AjaxComponent;
import er.ajax.AjaxOption;
import er.ajax.AjaxOptions;
import er.ajax.CktlAjaxUtils;
import er.extensions.appserver.ERXResponseRewriter;
import er.extensions.appserver.ERXWOContext;

/**
 * Composant permettant d'utiliser l'accordeon fourni par JQUery-UI Pour les détails, cf. <a
 * href="http://jqueryui.com/demos/accordion/">http://jqueryui.com/demos/accordion/</a>
 * 
 * @binding style permet d'indiquer un style à appliquer au container principal de l'accordeon (par exemple pour spécifier une hauteur.
 * @binding disabled [false] Disables (true) or enables (false) the accordion. Can be set when initialising (first creating) the accordion.
 * @binding active
 * @binding animated
 * @binding autoHeight [true] If set, the highest content part is used as height reference for all other parts. Provides more consistent animations.
 * @binding clearStyle [false] If set, clears height and overflow styles after finishing animations. This enables accordions to work with dynamic
 *          content. Won't work together with autoHeight.
 * @binding collapsible
 * @binding event
 * @binding fillSpace [false] If set, the accordion completely fills the height of the parent element. Overrides autoheight.
 * @binding header
 * @binding icons
 * @binding navigation
 * @binding navigationFilter
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlAjaxAccordionJQ extends AjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BDG_STYLE = "style";
	private String _accordionID;

	public CktlAjaxAccordionJQ(WOContext context) {
		super(context);
	}

	public boolean isStateless() {
		return true;
	}

	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public void appendToResponse(WOResponse response, WOContext context) {
		_accordionID = (String) valueForBinding("id", ERXWOContext.safeIdentifierName(context, true) + "Accordion");
		super.appendToResponse(response, context);
		CktlAjaxUtils.addScriptCodeInHead(response, context, "jQuery.noConflict();");
		StringBuffer str = new StringBuffer();
		str.append("<script type=\"text/javascript\">\n// <![CDATA[\n");
		str.append("jQuery('#" + accordionID() + "').accordion(");
		AjaxOptions.appendToBuffer(createAjaxOptions(), str, context);
		str.append(");");
		str.append("</script>");
		response.appendContentString(String.valueOf(str));
	}

	public String accordionID() {
		return _accordionID;
	}

	public NSDictionary createAjaxOptions() {
		NSMutableArray ajaxOptionsArray = new NSMutableArray();
		ajaxOptionsArray.addObject(new AjaxOption("disabled", AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("active", AjaxOption.STRING));
		ajaxOptionsArray.addObject(new AjaxOption("animated", AjaxOption.STRING));
		ajaxOptionsArray.addObject(new AjaxOption("autoHeight", AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("clearStyle", AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("collapsible", AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("event", AjaxOption.STRING));
		ajaxOptionsArray.addObject(new AjaxOption("fillSpace", AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("header", AjaxOption.STRING));
		ajaxOptionsArray.addObject(new AjaxOption("icons", AjaxOption.STRING));
		ajaxOptionsArray.addObject(new AjaxOption("navigation", AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("navigationFilter", AjaxOption.SCRIPT));
		NSMutableDictionary options = AjaxOption.createAjaxOptionsDictionary(ajaxOptionsArray, this);
		return options;
	}

	public WOActionResults handleRequest(WORequest request, WOContext context) {
		return null;
	}

	public String elementName() {
		String _elementName = (String) valueForBinding("elementName");
		return (_elementName != null) ? _elementName : "div";
	}

	@Override
	protected void addRequiredWebResources(WOResponse response) {
		ERXResponseRewriter.addScriptResourceInHead(response, context(), "FwkCktlAjaxWebExt", "scripts/jquery/jquery-1.7.1.min.js");
		ERXResponseRewriter.addScriptResourceInHead(response, context(), "FwkCktlAjaxWebExt", "scripts/jquery/jquery-ui-1.8.4.min.js");
		ERXResponseRewriter.addStylesheetResourceInHead(response, context(), "FwkCktlAjaxWebExt", "css/jquery/jquery-ui/jquery-ui.css");
		CktlAjaxUtils.addScriptCodeInHead(response, context(), "jQuery.noConflict();");
	}

	public String getStyle() {
		return (String) valueForBinding(BDG_STYLE);
	}
}