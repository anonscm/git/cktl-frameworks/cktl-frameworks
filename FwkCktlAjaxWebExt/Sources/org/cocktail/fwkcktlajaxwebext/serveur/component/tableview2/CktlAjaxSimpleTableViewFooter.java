package org.cocktail.fwkcktlajaxwebext.serveur.component.tableview2;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOElement;
import com.webobjects.foundation.NSDictionary;

import er.extensions.components.conditionals.ERXWOTemplate;

public class CktlAjaxSimpleTableViewFooter extends ERXWOTemplate {

    public CktlAjaxSimpleTableViewFooter(String s, NSDictionary associations, WOElement woelement) {
        super(s, associations, woelement);
    }

    @Override
    public String templateName(WOComponent component) {
        return "FooterContent";
    }
    
}
