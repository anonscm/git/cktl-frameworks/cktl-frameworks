package org.cocktail.fwkcktlajaxwebext.serveur.component.tableview2;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOElement;
import com.webobjects.foundation.NSDictionary;

import er.extensions.components.conditionals.ERXWOTemplate;

/**
 * 
 * @author isabelle
 *
 */
public class CktlAjaxSimpleTableViewHeader3 extends ERXWOTemplate{
	
	/**
	 *    
	 * @param s a string
	 * @param associations the association 
	 * @param woelement woelement
	 */
	public CktlAjaxSimpleTableViewHeader3(String s, NSDictionary associations, WOElement woelement) {
	        super(s, associations, woelement);
	    }

	    @Override
	    public String templateName(WOComponent component) {
	        return "HeaderContent3";
	    }
}
