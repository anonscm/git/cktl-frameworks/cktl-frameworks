package org.cocktail.fwkcktlajaxwebext.serveur.component.tableview2;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview2.CktlAjaxSimpleTableView.OrderState;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.extensions.appserver.ERXWOContext;
import er.extensions.components.ERXStatelessComponent;

public class CktlAjaxSimpleTableViewColumn extends ERXStatelessComponent {
    
    public static final String BINDING_LABEL = "label";
    public static final String BINDING_SORT_KEY_PATH = "sortKeypath";
    public static final String BINDING_WIDTH = "width";
    public static final String BINDING_COLSPAN = "colspan";
    public static final String BINDING_ROWSPAN = "rowspan";
    
    public CktlAjaxSimpleTableViewColumn(WOContext context) {
        super(context);
    }
    
    public String label() {
        return stringValueForBinding(BINDING_LABEL);
    }

    public String sortKeypath() {
        return stringValueForBinding(BINDING_SORT_KEY_PATH);
    }
    
    public CktlAjaxSimpleTableView currentSimpleTableView() {
        return (CktlAjaxSimpleTableView) ERXWOContext.contextDictionary().objectForKey(CktlAjaxSimpleTableView.CURRENT_STBV);
    }
    
    public WOActionResults reorder() {
        return currentSimpleTableView().reorder(sortKeypath());
    }
    
    public String containerIdForTable() {
        return currentSimpleTableView().id();
    }
    
    public String width() {
        return stringValueForBinding(BINDING_WIDTH);
    }
    
    public String colspan() {
        return stringValueForBinding(BINDING_COLSPAN);
    }
   
    public String rowspan() {
        return stringValueForBinding(BINDING_ROWSPAN);
    }
 
    public String cssForOrderIndication() {
        OrderState orderState = currentSimpleTableView().orderState(sortKeypath());
        String cssClass = ""; 
        if (orderState == OrderState.ORDER_ASC) {
            cssClass = "triAscending";
        } else if (orderState == OrderState.ORDER_DESC) {
            cssClass = "triDescending";
        }
        return cssClass;
    }
    
    public Boolean isOrdered() {
    	if(currentSimpleTableView() == null) {
    		return false;
    	}
    	return currentSimpleTableView().orderState(sortKeypath()) != OrderState.UNORDERED;
    }
    
    public Integer orderIndex() {
    	return currentSimpleTableView().orderIndex(sortKeypath());
    }
}