package org.cocktail.fwkcktlajaxwebext.serveur.component.tableview2;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.ajax.CktlAjaxUtils;
import er.extensions.appserver.ERXResponseRewriter;
import er.extensions.appserver.ERXWOContext;
import er.extensions.components.ERXStatelessComponent;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering;
import er.extensions.foundation.ERXStringUtilities;

/**
 * Composant générant une table simple
 */
public class CktlAjaxSimpleTableView extends ERXStatelessComponent {

    private static final long serialVersionUID = -7307521398921586204L;
    
	public static final String REORDER_ACTION = "reorder";
	public static final String ID_BINDING = "id";
	public static final String DISPLAY_GROUP_BINDING = "displayGroup";
	public static final String CSS_CLASS_FOR_TR_BINDING = "cssClassForTr";
	public static final String HEIGHT_BINDING = "height";
	public static final String MIN_HEIGHT_BINDING = "minHeight";
	public static final String WIDTH_BINDING = "width";
	public static final String ITEM_BINDING = "item";
	public static final String ON_SELECT_BINDING = "onSelect";
	public static final String UPDATE_TOOLBAR_ON_SELECT_BINDING = "updateToolbarOnSelect";
	public static final String TRI_MULTICOLONNE_ENABLED_BINDING = "triMultiColonneEnabled";
	public static final String SELECTION_MULTIPLE_ENABLED_BINDING = "selectionMultipleEnabled";
	public static final String USE_FIXED_HEADER_BINDING = "useFixedHeader";
	public static final String SHOW_FILTERS_BINDING = "showFilters";
	public static final String SHOW_TOOL_TIP_BINDING = "showToolTip";
	public static final String IS_ORANGE = "isOrange";

	public static final String CURRENT_STBV = "CurrentSTBV";
	public static final String PAGINATOR_HAS_FORM_BINDING = "paginatorHasForm";

	private int trIndex;

	/**
	 * Constructeur
	 * @param context contexte
	 */
	public CktlAjaxSimpleTableView(WOContext context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
    @Override
	public void appendToResponse(WOResponse response, WOContext context) {
		// On s'enregistre pour que les colonnes puissent nous contacter en tant que
		// reordering ou autre manipulations...
		// Rajout du javascript nécessaire
		ERXWOContext.contextDictionary().setObjectForKey(this, CURRENT_STBV);

		super.appendToResponse(response, context);

		ERXResponseRewriter.addScriptResourceInHead(response, context(), "Ajax", "prototype.js");
		ERXResponseRewriter.addScriptResourceInHead(response, context(), "FwkCktlAjaxWebExt.framework", "scripts/cktlajaxsimpletableview.js");

		if (useFixedHeader()) {
			ERXResponseRewriter.addScriptResourceInHead(response, context(), "FwkCktlAjaxWebExt.framework", "scripts/jquery/jquery-1.7.1.min.js");
			ERXResponseRewriter.addScriptCodeInHead(response, context(), "jQuery.noConflict();");

			ERXResponseRewriter.addScriptResourceInHead(response, context(), "FwkCktlAjaxWebExt.framework",
					"scripts/fixedheadertable/jquery.fixedheadertable.js");

			ERXResponseRewriter.addStylesheetResourceInHead(response, context(), "FwkCktlAjaxWebExt.framework",
					"themes/fixedheadertable/defaultTheme.css");

			applyFixedHeaderTableToResponse(response);

		}
		
		if (showToolTip()) {
			ERXResponseRewriter.addScriptResourceInHead(response, context(), "FwkCktlAjaxWebExt.framework", "scripts/cktlToolTip.js");
			response.appendContentString("<script>TOOL_TIP.desactiverSiTexteCourt();</script>");
		}
		
		if (isOrange()) {
			CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/cktlsimpletableview/CktlSimpleTableViewOrange.css");
		}
		
		response.appendContentString("<script>CASTBV.reapplyFocus();" + jsRegisterHandlerOnInputs() + "</script>");
	}

	/**
	 * Script lancé si on utilise un header fixe
	 * @param response response
	 */
	public void applyFixedHeaderTableToResponse(WOResponse response) {
		if (useFixedHeader()) {
			response.appendContentString("<script>" + scriptFixedHeader() + "</script>");
		}
	}
	
	/**
	 * @return l'appel du script pour utiliser le header fixe
	 */
	private String scriptFixedHeader() {
		return "jQuery('#" + tableId() + "').fixedHeaderTable({ footer: false, cloneHeadToFoot: false, autoShow: true });";
	}

	/**
	 * @return les classes pour le container de la table
	 */
	public String classForSimpleTableView() {
		String colorClass = "";
		if (isOrange()) {
			colorClass = " orange";
		}
		if (useFixedHeader()) {
			return "cktlajaxtableview cktlajaxsimpletableview" + colorClass;
		}
		return "cktlajaxtableview cktlajaxsimpletableviewnofixedheader" + colorClass;
	}

	/**
	 * @return la classe de la table
	 */
	public String classForTable() {
		if (isSelectionMultipleEnabled()) {
			return "selectionMultiple";
		}
		return "";
	}

	/**
	 * @return true si on doit afficher les filtres sous le header
	 */
	public Boolean showFilters() {
	  return booleanValueForBinding(SHOW_FILTERS_BINDING, false);
	}

	/**
	 * @return true si on doit afficher les tooltip si le texte est trop long à afficher dans une case de la table
	 */
	public Boolean showToolTip() {
	  return booleanValueForBinding(SHOW_TOOL_TIP_BINDING, false);
	}
	
	/**
	 * @return true si on doit afficher le tableau avec des couleurs orange
	 */
	public Boolean isOrange() {
	  return booleanValueForBinding(IS_ORANGE, false);
	}
	
	@SuppressWarnings("unchecked")
    @Override
	public WOActionResults invokeAction(WORequest request, WOContext context) {
		// On s'enregistre pour que les colonnes puissent nous contacter en tant que
		// reordering ou autre manipulations...
		ERXWOContext.contextDictionary().setObjectForKey(this, CURRENT_STBV);
		return super.invokeAction(request, context);
	}

	/**
	 * @return true si le tri multicolonne est activé
	 */
	public Boolean triMultiColonneEnabled() {
		return booleanValueForBinding(TRI_MULTICOLONNE_ENABLED_BINDING);
	}

	/**
	 * @return true si la selection multiple est activée
	 */
	public boolean isSelectionMultipleEnabled() {
		return booleanValueForBinding(SELECTION_MULTIPLE_ENABLED_BINDING);
	}

	/**
	 * @return true si on utilise un header fixe
	 */
	public boolean useFixedHeader() {
		return booleanValueForBinding(USE_FIXED_HEADER_BINDING, true);
	}

	/**
	 * Trie la table selon l'header cliqué
	 * @param keypath clé a trier
	 * @return null
	 */
	public WOActionResults reorder(String keypath) {
		WODisplayGroup displayGroup = displayGroup();
		NSMutableArray<EOSortOrdering> orderings;
		if (displayGroup.sortOrderings() != null) {
			orderings = displayGroup.sortOrderings().mutableClone();
		} else {
			orderings = new ERXSortOrdering.ERXSortOrderings();
		}

		EOSortOrdering sortOrdering = ERXQ.first(orderings, ERXQ.equals("key", keypath));
		if (sortOrdering != null) {
			if (sortOrdering.selector().equals(EOSortOrdering.CompareAscending)) {
				orderings.set(orderings.indexOf(sortOrdering), ERXS.desc(keypath));
			} else {
				if (triMultiColonneEnabled()) {
					orderings.remove(sortOrdering);
				} else {
					orderings.set(orderings.indexOf(sortOrdering), ERXS.asc(keypath));
				}
			}
		} else {
			if (!triMultiColonneEnabled()) {
				orderings.clear();
			}
			orderings.add(ERXS.asc(keypath));
		}

		displayGroup.setSortOrderings(orderings);
		displayGroup.updateDisplayedObjects();
		applyFixedHeaderTableToResponse(context().response());
		return null;
	}

	/**
	 * @return la selection
	 */
	public WOActionResults selectionner() {
		if (isSelectionMultipleEnabled()) {
			@SuppressWarnings("unchecked")
            NSArray<Object> selectedObjects = displayGroup().selectedObjects();
			if (!selectedObjects.remove(getItem())) {
				selectedObjects = selectedObjects.arrayByAddingObject(getItem());
			}
			displayGroup().setSelectedObjects(null);
			displayGroup().selectObjectsIdenticalTo(selectedObjects);
		} else {
			displayGroup().selectObject(getItem());
		}
		if (updateToobarOnSelect()) {
			AjaxUpdateContainer.updateContainerWithID(toolBarContainerId(), context());
		}

		return (WOActionResults) valueForBinding(ON_SELECT_BINDING);
	}

	/**
	 * @param keypath clé
	 * @return orderState
	 */
	public OrderState orderState(String keypath) {
		NSArray<EOSortOrdering> orderings = displayGroup().sortOrderings();
		EOSortOrdering sortOrdering = ERXQ.first(orderings, ERXQ.equals("key", keypath));
		OrderState orderState = null;
		if (sortOrdering == null) {
			orderState = OrderState.UNORDERED;
		} else
			if (sortOrdering.selector().equals(EOSortOrdering.CompareAscending)) {
				orderState = OrderState.ORDER_ASC;
			} else
				if (sortOrdering.selector().equals(EOSortOrdering.CompareDescending)) {
					orderState = OrderState.ORDER_DESC;
				}
		return orderState;
	}

	/**
	 * @param keypath clé
	 * @return integer
	 */
	public Integer orderIndex(String keypath) {
		if (!triMultiColonneEnabled()) {
			return null;
		}
		NSArray<EOSortOrdering> orderings = displayGroup().sortOrderings();
		if (orderings != null) {
			EOSortOrdering sortOrdering = ERXQ.first(orderings, ERXQ.equals("key", keypath));
			if (sortOrdering != null) {
				return orderings.indexOf(sortOrdering) + 1;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * @return classe pour le tr
	 */
	public String cssClassForTr() {
		String cssClass = stringValueForBinding(CSS_CLASS_FOR_TR_BINDING);
		if (ERXStringUtilities.stringIsNullOrEmpty(cssClass)) {
			if (displayGroup().selectedObjects().contains(getItem())) {
				cssClass = "selected";
			} else {
				if (trIndex % 2 == 0) {
					cssClass = "even";
				} else {
					cssClass = "odd";
				}
			}
		}
		return cssClass;
	}

	/**
	 * @return le display group
	 */
	public WODisplayGroup displayGroup() {
		return (WODisplayGroup) valueForBinding(DISPLAY_GROUP_BINDING);
	}
	
	/**
	 * @return l'identifiant passé en binding
	 */
	public String id() {
		String id = stringValueForBinding(ID_BINDING);
		if (id == null) {
			id = "STBV_" + ERXWOContext.safeIdentifierName(context(), false);
		}
		return id;
	}

	/**
	 * @return iedntifiant de la table
	 */
	public String tableId() {
		return id() + "_table";
	}

	/**
	 * @return l'identifiant du toolbar container
	 */
	public String toolBarContainerId() {
		return id() + "_toolBarContainer";
	}

	/**
	 * @return javascript
	 */
	public String jsRegisterHandlerOnInputs() {
		return "CASTBV.registerHandlerOnInputs($('" + id() + "'));";
	}

	public int getTrIndex() {
		return trIndex;
	}

	public void setTrIndex(int trIndex) {
		this.trIndex = trIndex;
	}

	public Object getItem() {
		return valueForBinding(ITEM_BINDING);
	}

	/**
	 * @param item item
	 */
	public void setItem(Object item) {
		setValueForBinding(item, ITEM_BINDING);
	}

	/**
	 * @return style du container
	 */
	public String styleForContainer() {
		String style = "";
		if (hasBinding(HEIGHT_BINDING)) {
			style += "height:" + stringValueForBinding(HEIGHT_BINDING) + ";";
		}
		if (hasBinding(MIN_HEIGHT_BINDING)) {
			style += "min-height:" + stringValueForBinding(MIN_HEIGHT_BINDING) + ";";
		}
		if (hasBinding(WIDTH_BINDING)) {
			style += "width:" + stringValueForBinding(WIDTH_BINDING) + ";";
		}
		return style;
	}

	@Override
	public void sleep() {
		super.sleep();
	}

	/**
	 * Enum des etats possibles
	 */
	public static enum OrderState {

		ORDER_DESC, ORDER_ASC, UNORDERED;

	}

	/**
	 * @return true si on update la toolbar 
	 */
	public Boolean updateToobarOnSelect() {
		return booleanValueForBinding(UPDATE_TOOLBAR_ON_SELECT_BINDING, false);
	}
	
	/**
	 * @return style pour la table
	 */
	public String styleForTable() {
		String style = "";
		
		if (hasBinding("style")) {
			style += valueForBinding("style");
		}
		if (showToolTip()) {
			style += " table-layout:fixed; width:100%";
		}
		
		return style;
	}

	/**
	 * @return indique si le paginator est dans un form
	 */
	public Boolean paginatorHasForm() {
		return booleanValueForBinding(PAGINATOR_HAS_FORM_BINDING, true);
	}
	
	/**
	 * @return Fonction javascript à lancer apres avoir utilisé un des boutons de navigation de la table
	 */
	public String onComplete() {
		
		String script = "function() {";
		
		if (showToolTip()) {
			script += "TOOL_TIP.desactiverSiTexteCourt();";
		}
		
		if (useFixedHeader()) {
			script += scriptFixedHeader();
		}
		
		script += "}";
		
		if (script.equals("function() {}")) {
			return null;
		} else {
			return script;
		}
		
	}
}