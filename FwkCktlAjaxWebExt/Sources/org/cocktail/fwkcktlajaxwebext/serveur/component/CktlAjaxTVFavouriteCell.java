package org.cocktail.fwkcktlajaxwebext.serveur.component;

import javax.swing.text.StyledEditorKit.BoldAction;

import com.webobjects.appserver.WOContext;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTVCell;

public class CktlAjaxTVFavouriteCell extends CktlAjaxTVCell {

	private Boolean favourite;
	
    public CktlAjaxTVFavouriteCell(WOContext context) {
        super(context);
    }

	public Boolean getFavourite() {
		return valueForBooleanBinding("favourite", false);
	}

	public void setFavourite(Boolean favourite) {
		this.favourite = favourite;
	}

	public boolean valueForBooleanBinding(String binding, boolean defaultValue)
	{
		boolean result = defaultValue;
		Object aValue = null;
		if (hasBinding(binding)) {
			aValue = valueForBinding(binding);
		}
		if (aValue != null) {
			if (aValue instanceof Number) {
				result = ((Number)aValue).intValue() != 0;
			} else if (aValue instanceof Boolean) {
				result = ((Boolean)aValue).booleanValue();
			} else if (aValue instanceof String) {
				String stringValue = (String)aValue;
				if ((stringValue.equalsIgnoreCase("false")) || (stringValue.equalsIgnoreCase("no")))
					result = false;
				else if ((stringValue.equalsIgnoreCase("true")) || (stringValue.equalsIgnoreCase("yes"))) {
					result = true;
				}
			}
		}
		return result;
	}
}