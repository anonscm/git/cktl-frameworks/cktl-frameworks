/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;

import er.extensions.appserver.ERXWOContext;

/**
 * Composant affichant une liste d'objet filtrable et permettant de sélectionner un objet.
 * 
 * @binding selection will set l'objet sélectionné dans la liste
 * @binding allObjects tous les objets à afficher dans la liste
 * @binding qualifier un qualifier pour filtrer la liste
 * @binding colonnes une liste de {@link CktlAjaxTableViewColumn}
 * @binding filtre la valeur du filtre
 * @binding nbObjectsParPage
 * @binding refreshData Boolean will set Permet d'indiquer qu'il faut reinitialiser les données du displayGroup. Si settable, la variable est remise à
 *          FALSE après refresh.
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class CktlAjaxSelectComponent extends CktlAjaxWOComponent {
	private static final long serialVersionUID = 4592817434048250435L;
	private WODisplayGroup displayGroup;
	protected static final String BINDING_SELECTION = "selection";
	private static final String BINDING_ALL_OBJECTS = "allObjects";
	private static final String BINDING_QUALIFIER = "qualifier";
	private static final String BINDING_COLONNES = "colonnes";
	private static final String BINDING_FILTRE = "filtre";
	private static final String BINDING_NB_OBJ_PAR_PAGE = "nbObjectsParPage";
	protected static final String BINDING_REFRESH_DATA = "refreshData";
	private static final int NB_OBJ_PAR_PAGE_DEFAUT = 20;

	public static final String CURRENT_OBJ_KEY = "currentObject";

	private String containerId;
	private NSKeyValueCoding currentObject;

	//public static final boolean isClickToOpenEnabled = Boolean.valueOf(System.getProperty("er.component.clickToOpen", "false"));

	public CktlAjaxSelectComponent(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse woresponse, WOContext wocontext) {
		super.appendToResponse(woresponse, wocontext);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public WODisplayGroup getDisplayGroup() {
		if (displayGroup == null || refreshData()) {
			displayGroup = new WODisplayGroup();
			displayGroup.setObjectArray(getAllObjects());
			displayGroup.setNumberOfObjectsPerBatch(nbObjParPage());
		}
		return displayGroup;
	}

	@SuppressWarnings("unchecked")
	private NSArray<NSKeyValueCoding> getAllObjects() {
		return (NSArray<NSKeyValueCoding>) valueForBinding(BINDING_ALL_OBJECTS);
	}

	@SuppressWarnings("unchecked")
	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		return (NSArray<CktlAjaxTableViewColumn>) valueForBinding(BINDING_COLONNES);
	}

	public WOActionResults filtrer() {
		getDisplayGroup().setQualifier(getQualifier());
		getDisplayGroup().updateDisplayedObjects();
		//RP : repositionne la liste de resultats en page 1
		getDisplayGroup().setCurrentBatchIndex(0);
		return null;
	}

	protected EOQualifier getQualifier() {
		return (EOQualifier) valueForBinding(BINDING_QUALIFIER);
	}

	public WOActionResults afficherTous() {
		setFiltre(null);
		getDisplayGroup().setQualifier(null);
		getDisplayGroup().updateDisplayedObjects();
		return null;
	}

	public WOActionResults selectionner() {
		setValueForBinding(getDisplayGroup().selectedObject(), BINDING_SELECTION);
		return null;
	}

	public String getContainerId() {
		if (containerId == null)
			containerId = ERXWOContext.safeIdentifierName(context(), true);
		return containerId;
	}

	public String getFiltre() {
		return (String) valueForBinding(BINDING_FILTRE);
	}

	public boolean refreshData() {
		Boolean res = (Boolean) valueForBinding(BINDING_REFRESH_DATA);
		if (Boolean.TRUE.equals(res)) {
			if (canSetValueForBinding(BINDING_REFRESH_DATA)) {
				setValueForBinding(Boolean.FALSE, BINDING_REFRESH_DATA);
			}
			setFiltre(null);
			return res.booleanValue();
		}
		return false;
	}

	public void setFiltre(String filtre) {
		setValueForBinding(filtre, BINDING_FILTRE);
	}

	protected Integer nbObjParPage() {
		if (hasBinding(BINDING_NB_OBJ_PAR_PAGE))
			return (Integer) valueForBinding(BINDING_NB_OBJ_PAR_PAGE);
		else
			return NB_OBJ_PAR_PAGE_DEFAUT;
	}

	public NSKeyValueCoding getCurrentObject() {
		return currentObject;
	}

	public void setCurrentObject(NSKeyValueCoding currentObject) {
		this.currentObject = currentObject;
	}

}