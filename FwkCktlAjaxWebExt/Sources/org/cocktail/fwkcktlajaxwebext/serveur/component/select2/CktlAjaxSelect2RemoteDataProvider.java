package org.cocktail.fwkcktlajaxwebext.serveur.component.select2;

import java.util.List;

/**
 * Permet de communiquer les résultats de recherche et
 * la sélection au composant {@link CktlAjaxSelect2Remote}
 * 
 * @author Alexis Tual
 *
 */
public interface CktlAjaxSelect2RemoteDataProvider {

    /**
     * @param searchTerm mot clef tappé par l'utilisateur
     * @return doit retourner les résultats de recherche en fonction
     *         du mot clef
     */
    List<Result> results(String searchTerm);
    
    /**
     * Callback appelé lorsque l'utilisateur sélectionne un item dans la liste
     * déroulante des résultats de recherche.
     * 
     * @param idSelection l'id de l'item sélectionné dans la liste déroulante
     * @return le résultat correspondant à l'id sélectionné
     */
    Result onSelect(String idSelection);
    
    /**
     * Est appelé au premier affichage du composant pour montrer le résultat
     * sélectionné initialement.
     * @return la sélection initiale sur le champ
     */
    Result selectionInitiale();
    
}
