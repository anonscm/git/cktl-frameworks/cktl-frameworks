package org.cocktail.fwkcktlajaxwebext.serveur.component.select2;

/**
 * 
 * Représente un résultat à passer en json au client jquery select2.
 * 
 * 
 * @author Alexis Tual
 *
 */
public class Result {

    private String id;
    private String text;
    
    /**
     * @param id l'id du résultat retournée par la recherche, doit être unique
     * @param text le texte affiché, n'est pas forcément unique
     */
    public Result(String id, String text) {
        super();
        this.id = id;
        this.text = text;
    }

    public String getId() {
        return id;
    }
    
    public void setId(String id) {
		this.id = id;
	}
    
    public String getText() {
        return text;
    }
    
}
