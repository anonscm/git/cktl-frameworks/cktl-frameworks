package org.cocktail.fwkcktlajaxwebext.serveur.component.select2;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxComponent;
import er.ajax.AjaxUpdateContainer;
import er.ajax.AjaxUtils;
import er.ajax.CktlAjaxUtils;

/**
 * Composant d'utilisation de la bibliothèque jquery select2.
 * 
 * La communication de la valeur sélectionné, des résultats de recherche se font
 * via le dataProvider implémenté par l'utilisateur du composant.
 * 
 * @see CktlAjaxSelect2RemoteDataProvider
 * 
 * @binding dataProvider une instance de {@link CktlAjaxSelect2RemoteDataProvider}
 * @binding cssClass la String correspondant à la classe css à appliquer au champ
 * @binding disabled true si le champ est désactivé, false sinon  
 * 
 * 
 * @author Alexis Tual
 *
 */
public class CktlAjaxSelect2Remote extends AjaxComponent {

	private static final String BINDING_MINIMUM_INPUT_LENGTH = "minimumInputLength";
	private static final String BINDING_STYLE = "style";
    private CktlAjaxSelect2RemoteController controller;
    
    private String actionUrl;
    private CktlAjaxSelect2RemoteDataProvider dataProvider;
    private String searchText;
    private String cssClass;
    private String elementId;
    private Boolean disabled;
    
    private String updateContainerID;
    
    /**
     * @param context le contexte
     */
    public CktlAjaxSelect2Remote(WOContext context) {
        super(context);
    }

    /**
     * @return l'id de l'élément html input hidden
     */
    public String getIdElement() {
        if (elementId == null) {
            elementId = safeElementID();
        }
        return elementId;
    }
    
    /**
     * @return l'url de callback vers ce composant
     * @see CktlAjaxSelect2Remote#handleRequest(WORequest, WOContext)
     */
    public String urlDataSource() {
        return actionUrl;
    }
    
    @Override
    public void appendToResponse(WOResponse res, WOContext ctx) {
        actionUrl = AjaxUtils.ajaxComponentActionUrl(context());
        super.appendToResponse(res, ctx);
    }
    
    @Override
    public boolean isStateless() {
        return true;
    }

    @Override
    public boolean synchronizesVariablesWithBindings() {
        return true;
    }
    
    @Override
    protected void addRequiredWebResources(WOResponse response) {
        CktlAjaxUtils.addScriptResourceInHead(context(), response, "FwkCktlAjaxWebExt.framework", "scripts/select2.js");
        CktlAjaxUtils.addScriptResourceInHead(context(), response, "FwkCktlAjaxWebExt.framework", "scripts/select2_locale_fr.js");
        CktlAjaxUtils.addStylesheetResourceInHead(context(), response, "FwkCktlAjaxWebExt.framework", "css/select2.css");        
    }

    @Override
    public WOActionResults handleRequest(WORequest request, WOContext context) {
        controller = new CktlAjaxSelect2RemoteController(dataProvider) {

			@Override
			protected void callBackOnReQueteSelection() {
		        if (getUpdateContainerID() != null) {
		        	AjaxUpdateContainer.updateContainerWithID(getUpdateContainerID(), context());
		        }
			}
        	
        };

        return controller.handle(request.formValues());
    }
    
    public CktlAjaxSelect2RemoteDataProvider getDataProvider() {
        return dataProvider;
    }
    
    public void setDataProvider(CktlAjaxSelect2RemoteDataProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

    public String getCssClass() {
        return cssClass;
    }
    
    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }
    
    public Boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public String getSearchText() {
        return searchText;
    }
    
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }
    
    @Override
    public void reset() {
        super.reset();
        dataProvider = null;
        cssClass = null;
        actionUrl = null;
        elementId = null;
        searchText = null;
        updateContainerID = null;
        controller = null;
        disabled = null;
    }
    
    public Integer getMinimumInputLength() {
    	return valueForIntegerBinding(BINDING_MINIMUM_INPUT_LENGTH, 1);
    }
    
    public void setMinimumInputLength(Integer minimumInputLength) {
    	
    }
    
    public String getStyle() {
    	return valueForStringBinding(BINDING_STYLE, "");
    }
    
    public void setStyle(String style) {
    	
    }
    
    public String getUpdateContainerID() {
    	return updateContainerID;
    }

	public void setUpdateContainerID(String updateContainerID) {
		this.updateContainerID = updateContainerID;
	}
	
	public String getJsForUpdateConainterID() {
		return "if ( $('" + getUpdateContainerID() + "') != null ) AUC.update('" + getUpdateContainerID() + "');";
	}
}