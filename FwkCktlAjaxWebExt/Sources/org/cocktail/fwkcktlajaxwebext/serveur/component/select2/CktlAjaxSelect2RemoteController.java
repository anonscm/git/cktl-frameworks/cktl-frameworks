package org.cocktail.fwkcktlajaxwebext.serveur.component.select2;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * 
 * Logique de réponse aux requêtes ajax lancées par le client jquery select2.
 * 
 * @author Alexis Tual
 *
 */
public abstract class CktlAjaxSelect2RemoteController {

    private CktlAjaxSelect2RemoteDataProvider dataProvider;
    private static Logger LOG = Logger.getLogger(CktlAjaxSelect2RemoteController.class);
    
    /**
     * @param dataProvider une instance de {@link CktlAjaxSelect2RemoteDataProvider}
     */
    public CktlAjaxSelect2RemoteController(CktlAjaxSelect2RemoteDataProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

    /**
     * @param params les paramètres de la requête
     * @return la réponse en fonction des paramètres
     */
    public WOResponse handle(NSDictionary<String, NSArray<Object>> params) {
        WOResponse response = new WOResponse();
        String searchTerm = (String) formValue(params, "q");
        String val = (String) formValue(params, "val");
        String initVal = (String) formValue(params, "initVal");
        try {
            if (searchTerm != null) {
                traiterRequeteRecherche(searchTerm, response);
            }
            if (val != null) {
                traiterRequeteSelection(val, response);
                callBackOnReQueteSelection();
            }
            if (initVal != null) {
                traiterRequeteSelectionInitiale(initVal, response);
            }
        } catch (Exception e) {
            traiterRequeteErreur(response, e);
        }
        return response;
    }

    private void traiterRequeteErreur(WOResponse response, Exception e) {
        Map<String, String> result = new HashMap<String, String>();
        result.put("error", e.getLocalizedMessage());
        try {
            constructJsonResponse(response, result);
        } catch (IOException e1) {
            LOG.warn(e1, e1);
        }
        response.setStatus(500);
        LOG.warn(e, e);
    }

    private Object formValue(NSDictionary<String, NSArray<Object>> params, String key) {
        Object value = null;
        NSArray<Object> formValues = params.objectForKey(key);
        if ((formValues != null) && (formValues.count() != 0)) {
          value = formValues.objectAtIndex(0);
        }
        return value;
    }
    
    private void traiterRequeteSelectionInitiale(String initVal, WOResponse response) throws IOException {
        Result result = dataProvider.selectionInitiale();
        constructJsonResponse(response, result);
    }

    private void traiterRequeteSelection(String val, WOResponse response) throws IOException {
        Result result = dataProvider.onSelect(val);
        constructJsonResponse(response, result);
    }

    private void traiterRequeteRecherche(String searchTerm, WOResponse response) throws IOException {
        List<Result> resultats = dataProvider.results(searchTerm);
        constructJsonResponse(response, resultats);
    }
    
    private void constructJsonResponse(WOResponse response, Object result) throws IOException {
        response.setContent(toJson(result));
        response.setStatus(200);
        response.setHeader("application/json", "Content-Type");
    }
    
    private String toJson(Object obj) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(obj);
        return json;
    }
    
    protected abstract void callBackOnReQueteSelection();
    
}
