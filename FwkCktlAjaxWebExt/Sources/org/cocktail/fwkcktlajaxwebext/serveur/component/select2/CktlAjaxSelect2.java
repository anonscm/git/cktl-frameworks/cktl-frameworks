package org.cocktail.fwkcktlajaxwebext.serveur.component.select2;

import com.webobjects.appserver.WOAssociation;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WOResponse;
import com.webobjects.appserver._private.WOPopUpButton;
import com.webobjects.foundation.NSDictionary;

import er.ajax.CktlAjaxUtils;

/**
 * Ce composant étend un popup button en y ajoutant une fonctionalité d'auto-complétion
 */
public class CktlAjaxSelect2 extends WOPopUpButton {

	/**
	 * @param aName : nom du composant
	 * @param someAssociations : association
	 * @param template : modele
	 */
	public CktlAjaxSelect2(String aName, NSDictionary<String, WOAssociation> someAssociations, WOElement template) {
	    super(aName, someAssociations, template);
    }

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/select2.js");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "css/select2.css");
	
		response.appendContentString("<script>jQuery(document).ready(function() { jQuery(" + idInContext(context) + ").select2(); });</script>");
	
	    // TODO Auto-generated method stub
	    super.appendToResponse(response, context);
	}
}
