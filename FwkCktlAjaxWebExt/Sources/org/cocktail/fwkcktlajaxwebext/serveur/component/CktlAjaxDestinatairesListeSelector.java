/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

/**
 * @binding destinataires Obligatoire. Objet DestinataireListe (sous-classe de NSMutableArray) contenant les destinataires (DestinataireListeElt).
 * @author rprin
 */
public class CktlAjaxDestinatairesListeSelector extends CktlAjaxWOComponent {

	private static final long serialVersionUID = 1338259919915943043L;
	public static final String BINDING_destinataires = "destinataires";
	public String rien;
	private String selectedDestOptionNew;

	//private String uneAdresse;

	//public static final String BINDING_ctrl = "ctrl";

	public CktlAjaxDestinatairesListeSelector(WOContext context) {
		super(context);
	}

	public WOActionResults onAdd() {

		DestinataireListeElt elt = destinataires().addObject(selectedDestOptionNew(), null, newEmail());
		return null;
	}

	public WOActionResults onDelete() {
		destinataires().removeObject(unDestinataire());
		return null;
	}

	public DestinataireListe destinataires() {
		return (DestinataireListe) valueForBinding(BINDING_destinataires);
	}

	public NSArray destinataireOptions() {
		return DestinataireListeElt.DESTINATAIRE_OPTIONS;
	}

	/**
	 * Objet représentant un destinataire (sous-classe de NSMutableDictionary)
	 * 
	 * @author rprin
	 */
	public static class DestinataireListeElt extends NSMutableDictionary {
		public static final String DESTINATAIRE_OPTION_TO = "Pour";
		public static final String DESTINATAIRE_OPTION_CC = "Copie à";
		public static final String DESTINATAIRE_OPTION_BCC = "Copie cachée";

		public static final NSArray DESTINATAIRE_OPTIONS = new NSArray(new String[] {
				DESTINATAIRE_OPTION_TO, DESTINATAIRE_OPTION_CC, DESTINATAIRE_OPTION_BCC
		});

		private final static String OPTION = "option";
		private final static String NOM = "nom";
		private final static String ADRESSE = "adresse";

		private boolean locked = false;

		private String tmpAdresse;

		/**
		 * @return true si le destinataire est bloqué (ile ne doit pas être supprimé/modifié)
		 */
		public boolean isLocked() {
			return locked;
		}

		public void setLocked(boolean locked) {
			this.locked = locked;
		}

		public DestinataireListeElt(String option, String nom, String adresse) {
			super();
			try {
				if (option == null) {
					throw new Exception("L'option de destinataire est obligatoire.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			setOption(option);
			setNom(nom);
			setAdresse(adresse);
		}

		public String getOption() {
			return (String) valueForKey(OPTION);
		}

		public void setOption(String option) {
			takeValueForKey(option, OPTION);
		}

		public String getNom() {
			return (String) valueForKey(NOM);
		}

		public void setNom(String nom) {
			takeValueForKey(nom, NOM);
		}

		public String getAdresse() {
			return (String) valueForKey(ADRESSE);
		}

		public void setAdresse(String adresse) {
			if (adresse == null) {
				remove(ADRESSE);
			}
			else {
				takeValueForKey(adresse, ADRESSE);
			}

		}

		public void takeValueForKey(Object value, String key) {
			if (key != null) {
				if (value == null) {
					super.removeObjectForKey(key);
				}
				else {
					super.takeValueForKey(value, key);
				}
			}
		}

		public Object valueForKey(String key) {
			return super.valueForKey(key);
		}

		@Override
		public String toString() {
			return getOption() + " : " + getNom() + " " + getAdresse();
		}

		public Boolean isAdresseValide() {
			if (StringCtrl.isEmpty(getAdresse())) {
				return Boolean.TRUE;
			}
			return Boolean.valueOf(isEmailValid(getAdresse()));
		}

		public void validateObject() throws NSValidation.ValidationException {
			if (!isAdresseValide().booleanValue()) {
				throw new NSValidation.ValidationException("L'adresse " + getAdresse() + " n'est pas une adresse email valide.");
			}
		}

		public String getTmpAdresse() {
			return tmpAdresse;
		}

		public void setTmpAdresse(String tmpAdresse) {
			this.tmpAdresse = tmpAdresse;
		}

	}

	/**
	 * @param s
	 * @return true si s est une adresse email construite correctement.
	 */
	public static boolean isEmailValid(String s) {
		//return StringCtrl.isEmailValid(s);
		//FIXME renvoyer vers StringCtrl.isEmailValid quand la pattern sera remontée.
		Pattern p = Pattern.compile("^[a-zA-Z0-9._-]{1,}+@[a-z0-9.-]{2,}[.][a-z]{2,}$");
		Matcher m = p.matcher(s);
		return (m.matches());
	}

	public static class DestinataireListe extends NSMutableArray<DestinataireListeElt> {
		private static final long serialVersionUID = 1L;

		@Override
		public boolean removeObject(Object object) {
			if (!((DestinataireListeElt) object).isLocked()) {
				return super.removeObject(object);
			}
			return false;
		}

		public DestinataireListeElt addObject(String option, String nom, String adresse) {
			DestinataireListeElt elt = new DestinataireListeElt(option, nom, adresse);
			super.addObject(elt);
			return elt;
		}

		public DestinataireListeElt addDestinataire(String option, String nom, String adresse) {
			return addObject(option, nom, adresse);
		}

		public void removeDestinataire(String option, String adresse) {
			EOQualifier qual = ERXQ.and(new EOKeyValueQualifier(DestinataireListeElt.ADRESSE, EOQualifier.QualifierOperatorEqual, adresse), (option != null ? new EOKeyValueQualifier(DestinataireListeElt.OPTION, EOQualifier.QualifierOperatorEqual, option) : null));
			NSArray res = EOQualifier.filteredArrayWithQualifier(this, qual);
			for (int i = res.count() - 1; i >= 0; i--) {
				remove(res.objectAtIndex(i));
			}
		}

		public void addObjectIfAdresseNotPresent(String option, String nom, String adresse) {
			EOKeyValueQualifier qual = new EOKeyValueQualifier(DestinataireListeElt.ADRESSE, EOQualifier.QualifierOperatorEqual, adresse);
			NSArray res = EOQualifier.filteredArrayWithQualifier(this, qual);
			if (res.count() == 0) {
				DestinataireListeElt elt = addObject(option, nom, adresse);
			}
		}

		public NSArray<String> getEmailsForOption(String option) {
			EOQualifier qual = new EOKeyValueQualifier(DestinataireListeElt.OPTION, EOQualifier.QualifierOperatorEqual, option);
			NSArray res1 = EOQualifier.filteredArrayWithQualifier(this, qual);
			NSArray res2 = (NSArray) res1.valueForKey(DestinataireListeElt.ADRESSE);
			return res2;
		}

		public boolean hasEmailsNonValides() {
			for (int i = 0; i < this.count(); i++) {
				DestinataireListeElt array_element = (DestinataireListeElt) this.objectAtIndex(i);
				if (!array_element.isAdresseValide().booleanValue()) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Nettoie la liste en supprimant les adresses vides
		 */
		public void cleanAdressesVides() {
			for (int i = this.count() - 1; i >= 0; i--) {
				if (StringCtrl.isEmpty(((DestinataireListeElt) objectAtIndex(i)).getAdresse())) {
					remove(this.objectAtIndex(i));
				}
			}
		}

		public String toString() {
			String res = "";
			Iterator<DestinataireListeElt> it = this.iterator();
			while (it.hasNext()) {
				CktlAjaxDestinatairesListeSelector.DestinataireListeElt destinataireListeElt = (CktlAjaxDestinatairesListeSelector.DestinataireListeElt) it.next();
				if (res.length() > 0) {
					res += ", ";
				}
				res = res + destinataireListeElt.toString();
			}
			return res;
		}

	}

	private String unDestOption;

	/**
	 * @return the unDestOption
	 */
	public String unDestOption() {
		return unDestOption;
	}

	/**
	 * @param unDestOption the unDestOption to set
	 */
	public void setUnDestOption(String unDestOption) {
		this.unDestOption = unDestOption;
	}

	private DestinataireListeElt unDestinataire;

	/**
	 * @return the unDestinataire
	 */
	public DestinataireListeElt unDestinataire() {
		return unDestinataire;
	}

	/**
	 * @param unDestinataire the unDestinataire to set
	 */
	public void setUnDestinataire(DestinataireListeElt unDestinataire) {
		this.unDestinataire = unDestinataire;
	}

	private String newEmail;

	/**
	 * @return the newEmail
	 */
	public String newEmail() {
		return newEmail;
	}

	/**
	 * @param newEmail the newEmail to set
	 */
	public void setNewEmail(String newEmail) {
		this.newEmail = newEmail;

		//		if (newEmail != null) {
		//			filtEmail = newEmail;
		//		}

		if (newEmail != null) {
			onAdd();
			this.newEmail = null;
			this.filtEmail = newEmail;
		}
	}

	private String unDestOptionNew;

	/**
	 * @return the unDestOptionNew
	 */
	public String unDestOptionNew() {
		return unDestOptionNew;
	}

	/**
	 * @param unDestOptionNew the unDestOptionNew to set
	 */
	public void setUnDestOptionNew(String unDestOptionNew) {
		this.unDestOptionNew = unDestOptionNew;
	}

	/**
	 * @return the selectedDestOptionNew
	 */
	public String selectedDestOptionNew() {
		if (selectedDestOptionNew == null) {
			selectedDestOptionNew = (String) destinataireOptions().objectAtIndex(0);
		}
		return selectedDestOptionNew;
	}

	/**
	 * @param selectedDestOptionNew the selectedDestOptionNew to set
	 */
	public void setSelectedDestOptionNew(String selectedDestOptionNew) {
		this.selectedDestOptionNew = selectedDestOptionNew;
	}

	public String positionneFocusJs() {
		return "function() {if (window.document.getElementById('" + fEmailId() + "')!=null) {window.document.getElementById('" + fEmailId() + "').focus();};}";
	}

	public String fEmailId() {
		return getComponentId() + "_newEmail";
	}

	public String fEmailIdField() {
		return fEmailId() + "_field";
	}

	public boolean isLocked() {
		return unDestinataire().isLocked();
	}

	public boolean isAdresseValide() {
		return unDestinataire().isAdresseValide();
	}

	public String filtEmail;

	public String getFiltEmail() {
		return filtEmail;
	}

	private String unEmail;

	/**
	 * @return the unEmail
	 */
	public String unEmail() {
		return unEmail;
	}

	/**
	 * @param unEmail the unEmail to set
	 */
	public void setUnEmail(String unEmail) {
		this.unEmail = unEmail;
	}

	public NSArray filteredEmails() {
		return new NSArray(new String[] {
				"toto@test.com", "truc@kjqhsdf.com", "taste@ljqdhsf.fr"
		});

	}

	public String afterUpdateElementJs() {
		return "function() {" + getMainContainerId() + "Update(); window.document.getElementById('" + fEmailIdField() + "').focus();}";
	}

	public String getUneAdresse() {
		return (unDestinataire() != null ? unDestinataire().getAdresse() : null);
	}

	public void setUneAdresse(String uneAdresse) {
		if (unDestinataire() != null) {
			unDestinataire().setAdresse(uneAdresse);
		}
	}

}