package org.cocktail.fwkcktlajaxwebext.serveur.component;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.CktlAjaxUtils;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;


public class CktlAjaxAccordeon extends CktlAjaxWOComponent {
	
	public static String BINDING_EDITING_CONTEXT = "editingContext";
	public static String BDG_STYLE = "style";

	
    public CktlAjaxAccordeon(WOContext context) {
        super(context);
    }

    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
    	super.appendToResponse(response, context);
    	
		CktlAjaxUtils.addStylesheetResourceInHead(context(), response, "FwkCktlAjaxWebExt", "css/Accordeon.css");
	}
    
    public String accordeonId() {
		return getComponentId() + "_accordeonId";
	}
    
    public String elementName() {
		String _elementName = (String) valueForBinding("elementName");
		return (_elementName != null) ? _elementName : "div";
	}

    public String style() {
		return (String) valueForBinding(BDG_STYLE);
	}

    
}