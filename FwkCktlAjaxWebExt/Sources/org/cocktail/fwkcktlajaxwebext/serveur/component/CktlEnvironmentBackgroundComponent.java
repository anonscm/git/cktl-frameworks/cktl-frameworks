package org.cocktail.fwkcktlajaxwebext.serveur.component;

import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOComponent;

/**
 * Composant qui rajouter l'image de fond en CSS sur le body
 */
public class CktlEnvironmentBackgroundComponent extends WOComponent {

	private static final long serialVersionUID = 1750602483423272343L;


	public CktlEnvironmentBackgroundComponent(WOContext context) {
        super(context);
    }
    

	public String getMainBackgroundImage() {
		return CktlWebApplication.application().getImageURL("HTML_MAIN_BG_IMAGE", "fond.jpg");
	}	
    
}