/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import java.util.Enumeration;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOAssociation;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODynamicElement;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.appserver._private.WOBindingNameAssociation;
import com.webobjects.appserver._private.WOConstantValueAssociation;
import com.webobjects.appserver._private.WODynamicElementCreationException;
import com.webobjects.appserver._private.WOShared;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation._NSStringUtilities;

public class MyWOSwitchComponent extends WODynamicElement {
	WOAssociation componentName, _associations;
	public NSMutableDictionary componentAttributes;
	NSMutableDictionary componentCache;
	NSArray _otherAttributesKeys;
	WOElement template;

	public MyWOSwitchComponent(String s, NSDictionary nsdictionary, WOElement woelement) {
		super(null, null, null);
		componentName = (WOAssociation) nsdictionary.objectForKey("WOComponentName");
		if (componentName == null) {
			componentName = (WOAssociation) nsdictionary.objectForKey("_componentName");
			if (componentName == null)
				throw new WODynamicElementCreationException("<" + getClass().getName() + "> : '" + "WOComponentName" + "' attribute missing.");
		}
		if (nsdictionary != null) {
			_associations = (WOAssociation) nsdictionary.objectForKey("associations");
			componentAttributes = nsdictionary.mutableClone();
			componentAttributes.removeObjectForKey("WOComponentName");
			componentAttributes.removeObjectForKey("_componentName");
			componentAttributes.removeObjectForKey("associations");
		}
		else {
			componentAttributes = new NSMutableDictionary();
		}

		componentCache = new NSMutableDictionary();
		template = woelement;
	}

	public String toString() {
		return "<WOSwitchComponent  componentName: " + (componentName == null ? "null" : componentName.toString()) + " componentAttributes: " + (componentAttributes == null ? "null" : componentAttributes.toString()) + " componentCache: "
				+ (componentCache == null ? "null" : componentCache.toString()) + " children: " + (template == null ? "null" : template.toString()) + ">";
	}

	public String _elementNameInContext(WOContext wocontext) {
		com.webobjects.appserver.WOComponent wocomponent = wocontext.component();
		String s = null;
		Object obj = componentName.valueInComponent(wocomponent);
		if (obj != null)
			s = obj.toString();
		if (s == null || s.length() == 0)
			throw new IllegalStateException("<" + getClass().getName() + "> : componentName not specified or componentName association evaluated to null.");
		else
			return s;
	}

	public WOElement _realComponentWithName(String s, WOContext wocontext) {
		WOElement woelement;
		synchronized (this) {
			woelement = (WOElement) componentCache.objectForKey(s);
			if (woelement == null) {
				if (_associations != null) {
					NSDictionary otherAttributes = (NSDictionary) _associations.valueInComponent(wocontext.component());
					if (_otherAttributesKeys != null) {
						componentAttributes.removeObjectsForKeys(_otherAttributesKeys);
					}
					if (otherAttributes != null) {
						_otherAttributesKeys = otherAttributes.allKeys();
						Enumeration enumOtherAttributes = otherAttributes.keyEnumerator();
						while (enumOtherAttributes.hasMoreElements()) {
							String key = (String) enumOtherAttributes.nextElement();
							String paramString = (String) otherAttributes.objectForKey(key);
							Object localObject = null;
							if (paramString != null && paramString.startsWith("\"")) {
								localObject = WOAssociation.associationWithValue(paramString.substring(1, paramString.length() - 1));
								//localObject = WOAssociation.associationWithValue(paramString);
							}
							else if (paramString != null && paramString.startsWith("^")) {
								localObject = new WOBindingNameAssociation(paramString);
							}
							else if (_NSStringUtilities.isNumber(paramString)) {
								Integer localInteger = WOShared.unsignedIntNumber(Integer.parseInt(paramString));
								localObject = WOAssociation.associationWithValue(localInteger);
							}
							else if (paramString != null && (paramString.equalsIgnoreCase("true") || paramString.equalsIgnoreCase("yes"))) {
								localObject = WOConstantValueAssociation.TRUE;
							}
							else if (paramString != null && (paramString.equalsIgnoreCase("false") || (paramString.equalsIgnoreCase("no")) || (paramString.equalsIgnoreCase("nil")) || (paramString.equalsIgnoreCase("null")))) {
								localObject = WOConstantValueAssociation.FALSE;
							}
							else {
								localObject = WOAssociation.associationWithKeyPath("parent." + paramString);
							}
							componentAttributes.setObjectForKey((WOAssociation) localObject, key);
							// if (key != null && ((String)(otherAttributes.objectForKey(key))).startsWith("^")) {
							//								WOBindingNameAssociation assoc = new WOBindingNameAssociation((String)otherAttributes.objectForKey(key));
							//								componentAttributes.setObjectForKey(assoc, key);						
							//							} else {
							//								WOKeyValueAssociation assoc = new WOKeyValueAssociation("parent."+(String)otherAttributes.objectForKey(key));
							//								try {
							//									assoc.valueInComponent(wocontext.component());
							//									componentAttributes.setObjectForKey(assoc, key);						
							//								} catch (UnknownKeyException e) {
							//									WOConstantValueAssociation assoc1 = new WOConstantValueAssociation((String)otherAttributes.objectForKey(key));
							//									componentAttributes.setObjectForKey(assoc1, key);						
							//								}
							//							}
						}
					}
				}
				woelement = WOApplication.application().dynamicElementWithName(s, componentAttributes, template, wocontext._languages());
				if (woelement == null)
					throw new WODynamicElementCreationException("<" + getClass().getName() + "> : cannot find component or dynamic element named " + s);
				// A desactiver sinon impossible d'avoir plusieurs composants de meme classe
				// componentCache.setObjectForKey(woelement, s);
			}
		}
		return woelement;
	}

	public void takeValuesFromRequest(WORequest worequest, WOContext wocontext) {
		String s = _elementNameInContext(wocontext);
		wocontext.appendElementIDComponent(s.replace('.', '_'));
		WOElement woelement = _realComponentWithName(s, wocontext);
		woelement.takeValuesFromRequest(worequest, wocontext);
		wocontext.deleteLastElementIDComponent();
	}

	public WOActionResults invokeAction(WORequest worequest, WOContext wocontext) {
		String s = _elementNameInContext(wocontext);
		wocontext.appendElementIDComponent(s.replace('.', '_'));
		WOElement woelement = _realComponentWithName(s, wocontext);
		WOActionResults woactionresults = woelement.invokeAction(worequest, wocontext);
		wocontext.deleteLastElementIDComponent();
		return woactionresults;
	}

	public void appendToResponse(WOResponse woresponse, WOContext wocontext) {
		String s = _elementNameInContext(wocontext);
		wocontext.appendElementIDComponent(s.replace('.', '_'));
		WOElement woelement = _realComponentWithName(s, wocontext);
		woelement.appendToResponse(woresponse, wocontext);
		wocontext.deleteLastElementIDComponent();
	}

}
