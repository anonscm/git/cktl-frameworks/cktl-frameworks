/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxUtils;

/**
 * Composant permettant d'afficher un tooltip pour un élément identifié via targetId. Le contenu du tooltip est défini par le "content" que vous
 * placez entre les balises.
 * 
 * @binding targetId Id de l'élément du dom sur lequel afficher le tooltip.
 * @binding title Titre du tooltip (facultatif)
 * @binding showIcon [true] Indique s'il faut afficher un icone "info"
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlAjaxTooltip extends CktlAjaxWOComponent {
	private static final long serialVersionUID = 1L;
	private static final String BDG_TARGET_ID = "targetId";
	private static final String BDG_TITLE = "title";
	private static final String BDG_SHOW_ICON = "showIcon";
	private static final String EVENT_FOR_SHOW = "mouseenter";
	private static final String EVENT_FOR_HIDE = "mouseleave";

	public CktlAjaxTooltip(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		AjaxUtils.appendScriptHeader(response);
		response.appendContentString(jsShowTooltipHandler() + " = function (e) { " + jsShowTooltip() + " };");
		response.appendContentString(jsHideTooltipHandler() + " = function (e) { " + jsHideTooltip() + " };");
		response.appendContentString(jsStartObservingMouseOverOnTarget());
		AjaxUtils.appendScriptFooter(response);
	}

	private String jsShowTooltipHandler() {
		return tooltipContentId() + "Show";
	}

	private String jsHideTooltipHandler() {
		return tooltipContentId() + "Hide";
	}

	private String jsShowTooltip() {
		return "Effect.Appear($('" + tooltipContentId() + "')); " + jsStopObservingMouseOverOnTarget() + jsStartObservingMouseoutOnContent();
	}

	private String jsHideTooltip() {
		return "Effect.Fade($('" + tooltipContentId() + "'));  " + jsStopObservingMouseoutOnContent() + jsStartObservingMouseOverOnTarget();
	}

	private String jsStartObservingMouseOverOnTarget() {
		return "Event.observe('" + getTargetId() + "', '" + EVENT_FOR_SHOW + "', " + jsShowTooltipHandler() + ");";
	}

	private String jsStopObservingMouseOverOnTarget() {
		return "Event.stopObserving('" + getTargetId() + "', '" + EVENT_FOR_SHOW + "', " + jsShowTooltipHandler() + ");";
	}

	private String jsStartObservingMouseoutOnContent() {
		return "Event.observe('" + tooltipContentId() + "', '" + EVENT_FOR_HIDE + "', " + jsHideTooltipHandler() + ");";
	}

	private String jsStopObservingMouseoutOnContent() {
		return "Event.stopObserving('" + tooltipContentId() + "', '" + EVENT_FOR_HIDE + "', " + jsHideTooltipHandler() + ");";
	}

	public String getTargetId() {
		return (String) valueForBinding(BDG_TARGET_ID);
	}

	public String tooltipContentId() {
		return getMainContainerId();
	}

	public boolean isStateless() {
		return true;
	}

	public String getTitle() {
		return (String) valueForBinding(BDG_TITLE);
	}

	public Boolean hasTitle() {
		return Boolean.valueOf(getTitle() != null);
	}

	public Boolean showIcon() {
		return booleanValueForBinding(BDG_SHOW_ICON, Boolean.TRUE);
	}
}