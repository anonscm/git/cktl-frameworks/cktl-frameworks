/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software is governed by the CeCILL license under French law and abiding by the rules of distribution
 * of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL "http://www.cecill.info". As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license,
 * users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that
 * it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to
 * use and operate it in the same conditions as regards security. The fact that you are presently reading this means that you have had knowledge of the CeCILL
 * license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import java.util.HashMap;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.ajax.AjaxTree;
import er.ajax.AjaxTreeModel;
import er.extensions.appserver.ERXWOContext;

public class CktlAjaxTree extends AjaxTree {
	private AjaxTreeModel _treeModel;
	public HashMap itemsIds = new HashMap();

	public CktlAjaxTree(WOContext context) {
		super(context);
	}

	public String treeNodeExpandLinkId() {
		return itemId(item()) + "_" + "expand";
	}

	public String treeNodeCollapseLinkId() {
		return itemId(item()) + "_" + "collapse";
	}

	public String itemId(Object item) {
		if (itemsIds.get(item) == null) {
			itemsIds.put(item, id() + "_" + ERXWOContext.safeIdentifierName(context(), true));
		}
		return (String) itemsIds.get(item);
	}

	public String lockTree() {
		return "lockTree = function() {$('" + id() + "').locked=true; return true;}";
	}

	public String unlockTree() {
		return "unlockTree = function () {$('" + id() + "').locked=false; return true;}";
	}

	public String canDoAction() {
		return "canDoAction = function() {if ($('" + id() + "').locked==true) {return false;} else {lockTree(); return true;}}";
	}

	public String expandActionOnBeforeClick() {
		return "canDoAction()";
	}

	public String collapseActionOnBeforeClick() {
		return "canDoAction()";
	}

	public WOActionResults expand() {
		return super.expand();
	}

	public String imageCollapsedLinkClass() {
		return "cktlajaxtree_collapsed";
	}

	public String imageExpandedLinkClass() {
		return "cktlajaxtree_expanded";
	}

	public void setTreeModel(AjaxTreeModel treeModel) {
		_treeModel = treeModel;
		super.setTreeModel(treeModel);
	}

	public AjaxTreeModel treeModel() {
		if (_treeModel == null || (canGetValueForBinding("treeModel") && valueForBinding("treeModel") != null && valueForBinding("treeModel") != _treeModel)) {
			if (canGetValueForBinding("treeModel") && valueForBinding("treeModel") != null) {
				_treeModel = (AjaxTreeModel) valueForBinding("treeModel");
			} else {
				_treeModel = new AjaxTreeModel();
				if (canSetValueForBinding("treeModel")) {
					setValueForBinding(_treeModel, "treeModel");
				}
			}
		}
		return _treeModel;
	}

}
