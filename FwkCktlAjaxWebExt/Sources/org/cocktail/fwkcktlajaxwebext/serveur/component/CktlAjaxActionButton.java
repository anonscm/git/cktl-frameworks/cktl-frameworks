/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import java.util.UUID;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

/**
 * Composant qui permet d'unifier la mise en place en liens, boutons etc.<br/>
 * Pour le binding woElement, si vous utilisez un woSubmitButton, veillez à ce qu'il soit place à l'interieur d'une FORM.
 */
public class CktlAjaxActionButton extends CktlAjaxAbstractButton {
	/**
	 * Type du bouton dans le cas d'un AjaxUpdateLink avec button=true. Si vous indiquez le type, l'image sera automatiquement recuperee pour un type
	 * predefini (edit, new, delete, find).
	 */
	public static final String BINDING_type = "type";

	public static final String BINDING_title = "title";
	public static final String BINDING_enabled = "enabled";
	public static final String BINDING_frameworkName = "frameworkName";
	public static final String BINDING_imageFileName = "imageFileName";

	/** Le texte a afficher */
	public static final String BINDING_text = "text";

	/** Indique s'il faut afficher le text */
	public static final String BINDING_showText = "showText";

	/** Indique s'il faut afficher l'image */
	public static final String BINDING_showImage = "showImage";

	/** Indique s'il l'hyperlien est associe a une action */
	public static final String BINDING_action = "action";

	/** Indique s'il l'hyperlien est associe a une url (href) */
	public static final String BINDING_href = "href";

	/** Indique s'il l'image est situee a droite du label */
	public static final String BINDING_isImagePositionIsRight = "isImagePositionIsRight";

	public static final String TYPE_EDIT = "edit";
	public static final String TYPE_NEW = "new";
	public static final String TYPE_DELETE = "delete";
	public static final String TYPE_FIND = "find";

	/** WOElement a utiliser (AjaxUpdateLink, WOHyperLink, WOSubmitButton) */
	public static final String BINDING_woElement = "woElement";

	/** Indique s'il faut afficher le bouton comme un submitButton (dans le cas d'un ajaxUpdateLink) */
	public static final String BINDING_button = "button";

	/**
	 * Indique La taille des icones à utiliser (small ou big). Small par défaut.
	 */
	public static final String BINDING_iconSize = "iconSize";

	private static String BT_PREFIX = "images/bt_";
	private static String BT_SUFFIX_20 = "_20.png";
	private static String BT_SUFFIX_31 = "_31.png";
	private static String NOIMAGE_PREFIX = "[";
	private static String NOIMAGE_SUFFIX = "]";

	public static final String WOHYPERLINK = "WOHyperLink";
	public static final String WOSUBMITBUTTON = "WOSubmitButton";
	public static final String AJAX_UPDATE_LINK = "AjaxUpdateLink";
	public static final String INACTIVE_IMAGE = "inactiveImage";

	public static final String ICON_SIZE_BIG = "big";
	public static final String ICON_SIZE_SMALL = "small";

	public static final Boolean DEFAULT_SHOW_TEXT = Boolean.FALSE;
	public static final Boolean DEFAULT_SHOW_IMAGE = Boolean.TRUE;
	private static final Boolean DEFAULT_BUTTON = Boolean.FALSE;
	private static final String DEFAULT_ICON_SIZE = ICON_SIZE_SMALL;

	private static String DEFAULT_WO_ELEMENT = AJAX_UPDATE_LINK;

	private String id;
	
	private WOActionResults action;

	public void setAction(WOActionResults action) {
		this.action = action;
	}

	public WOActionResults action() {
		return (WOActionResults)valueForBinding(BINDING_action);
	}

	public CktlAjaxActionButton(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}

	/**
	 * @return Si la valeur du binding BINDING_imageFileName est définie, renvoie cette valeur, sinon si la valeur du binding BINDING_type est
	 *         définie, renvoie BT_PREFIX + valeur du binding + BT_SUFFIX.
	 */
	public String imageFileName() {
		if (hasBinding(BINDING_imageFileName)) {
			return (String) valueForBinding(BINDING_imageFileName);
		}
		if (hasBinding(BINDING_type)) {
			return BT_PREFIX + valueForBinding(BINDING_type) + getBtSuffix();
		}
		return null;
	}

	public Boolean enabled() {
		if (hasBinding(BINDING_enabled)) {
			return (Boolean) valueForBinding(BINDING_enabled);
		}
		return Boolean.TRUE;

	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public String imageFrameworkName() {
		if (valueForBinding(BINDING_frameworkName) != null) {
			return (String) valueForBinding(BINDING_frameworkName);
		}
		return frameworkName();
	}

	public Boolean isImageAvailable() {
		// EGE 01/12/2009 Desactivation de ce test
		// qui a un effet catastrophique sur les performances
//		URL url = application().resourceManager().pathURLForResourceNamed(imageFileName(), imageFrameworkName(), null);
//		return Boolean.valueOf(url != null);
		return true;
	}

	public String getNoImageText() {
		if (getText() != null) {
			return getText();
		}
		return NOIMAGE_PREFIX + valueForBinding(BINDING_title) + NOIMAGE_SUFFIX;
	}

	public String getWoElement() {
		if (valueForBinding(BINDING_woElement) != null) {
			return (String) valueForBinding(BINDING_woElement);
		}
		return DEFAULT_WO_ELEMENT;
	}

	public Boolean isInactiveImage() {
		return INACTIVE_IMAGE.equals(getWoElement());
	}

	public Boolean isWOHyperLink() {
		return WOHYPERLINK.equals(getWoElement());
	}

	public Boolean isWOSubmitButton() {
		return WOSUBMITBUTTON.equals(getWoElement());
	}

	public Boolean isAjaxUpdateLink() {
		return AJAX_UPDATE_LINK.equals(getWoElement());
	}

	public boolean isWOHyperLinkAction() {
		return hasBinding(BINDING_action);
	}

	public boolean isWOHyperLinkHRef() {
		return hasBinding(BINDING_href);
	}

	public Boolean showText() {
		if (hasBinding(BINDING_showText)) {
			return (Boolean) valueForBinding(BINDING_showText);
		}
		return DEFAULT_SHOW_TEXT;
	}

	public Boolean showImage() {
		if (hasBinding(BINDING_showImage)) {
			return (Boolean) valueForBinding(BINDING_showImage);
		}
		return DEFAULT_SHOW_IMAGE;
	}

	public Boolean button() {
		if (hasBinding(BINDING_button)) {
			return (Boolean) valueForBinding(BINDING_button);
		}
		return DEFAULT_BUTTON;
	}

	public String getText() {
		if (hasBinding(BINDING_text)) {
			return (String) valueForBinding(BINDING_text);
		}
		return null;
	}

	public String getId() {
		if (id == null) {
			if (valueForBinding("id") != null) {
				id = (String) valueForBinding("id");
			}
			else {
				id = UUID.randomUUID().toString().replaceAll("-", "_");
			}
			setValueForBinding(id, "id");
		}
		return id;
	}
	public void setId(String id) {
		this.id = id;
		setValueForBinding(id, "id");
	}
	/**
	 * @return the buttonId
	 */
//	public String buttonId() {
//		return getId()+"_btn";
//	}

	public String jsOnAfterDisplayed() {
		if (enabled()) {
			return "document.getElementById('" + getId() + "').setOpacity(1);";
		}
		else {
			return "document.getElementById('" + getId() + "').setOpacity(0.5);document.getElementById('" + getId() + "').firstDescendant().addClassName('cktl_button_disabled');";
		}
	}

	public String iconSize() {
		if (hasBinding(BINDING_iconSize)) {
			return (String) valueForBinding(BINDING_iconSize);
		}
		return DEFAULT_ICON_SIZE;
	}

	public String getBtSuffix() {
		if (ICON_SIZE_BIG.equals(iconSize())) {
			return BT_SUFFIX_31;
		}
		else {
			return BT_SUFFIX_20;
		}
	}

	private String buttonId;

	/**
	 * @return the buttonId
	 */
	public String buttonId() {
		return getId()+"_btn";
	}

	/**
	 * @param buttonId the buttonId to set
	 */
	public void setButtonId(String buttonId) {
		this.buttonId = buttonId;
	}

	public String buttonBaseImageClass() {
		String buttonBaseImageClass = "cktl_action_button ";
		
		if (hasBinding(BINDING_type)) {
			buttonBaseImageClass += "cktl_action_" + (String)valueForBinding(BINDING_type) + " ";
		}
		buttonBaseImageClass += "cktl_button_icon cktl_inline_block";
		
		return buttonBaseImageClass;
	}

	public boolean isImagePositionIsRight() {
		return hasBinding(BINDING_isImagePositionIsRight) && ((Boolean)valueForBinding(BINDING_isImagePositionIsRight)).booleanValue();
	}

}
