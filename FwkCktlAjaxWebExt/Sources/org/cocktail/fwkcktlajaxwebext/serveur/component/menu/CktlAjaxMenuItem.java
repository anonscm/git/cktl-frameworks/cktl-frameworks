package org.cocktail.fwkcktlajaxwebext.serveur.component.menu;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.CktlAjaxUtils;
import er.extensions.appserver.ERXWOContext;

/**
 * item d'un menu.
 */
public class CktlAjaxMenuItem extends CktlAjaxMenuCommun {
	// Private fields
	private static final long serialVersionUID = 1L;
	private static final String BINDING_PAGE_NAME = "pageName";
	private static final String BINDING_IMG_URL = "imgUrl";
	private static final String BINDING_IMG_DISABLED_URL = "imgDisabledUrl";
	private static final String BINDING_ACTION = "action";
	private static final String BINDING_AVERTIR_MODIFS = "avertirModifs";
	private static final String BINDING_LABEL_AVERTISSEMENT = "labelAvertissement";
	public static final String PREFIXE_MENU_ITEM = "menuItem_";
	

	// Constructor
	/**
	 * @param context : contexte d'édition
	 */
	public CktlAjaxMenuItem(WOContext context) {
		super(context);
	}

	public WOActionResults action() {
		WOActionResults resultat = null;
		if (hasBinding(BINDING_PAGE_NAME)) {
			resultat = pageWithName(getPageName());
		} else {
			resultat = (WOActionResults)valueForBinding(BINDING_ACTION);
		}
		return resultat;
	}
	
	// Properties
	public String getMenuItemId() {
		return PREFIXE_MENU_ITEM + getComponentId();
	}
	
	public String getPageName() {
		return (String) valueForBinding(BINDING_PAGE_NAME);
	}

	public String getImgUrl() {
		return (String) valueForBinding(BINDING_IMG_URL);
	}

	public String getImgDisabledUrl() {
		return (String) valueForBinding(BINDING_IMG_DISABLED_URL);
	}
	
	public Boolean getAvertirModifs() {
		return valueForBooleanBinding(BINDING_AVERTIR_MODIFS, false);
	}
	
	public String getLabelAvertissement() {
		return valueForStringBinding(BINDING_LABEL_AVERTISSEMENT, "Des modifications ont été faites, êtes-vous sûr(e) de vouloir quitter la page ?");
	}

	/**
	 * @return class de l'élément
	 */
	public String getClassSelection() {
		if (isSelected()) {
			return CktlAjaxMenuConstants.getClassItemSelected();
		} else {
			return "";
		}
	}

	public String getClassDisabled() {
		return CktlAjaxMenuConstants.getClassItemDisabled();
	}

	/**
	 * @return true si l'élément est sélectionné
	 */
	public Boolean isSelected() {
		if (getCurrentAjaxMenuPanel().getSelectedItemId() == null) {
			return false;
		}
		return getCurrentAjaxMenuPanel().getSelectedItemId().equalsIgnoreCase(getId());
	}

	/**
	 * @return style de l'élément de menu
	 */
	public String getBackgroundStyle() {

		String style = "height : " + getImageSize().toString() + "px; width : " + getImageSize().toString() + "px; display: inline-block;";
		style += "background-image : url('" + context()._urlForResourceNamed(getImgUrl(), "app", false) + "');";

		return style;
	}
	
	/**
	 * @return style de l'élément de menu
	 */
	public String getBackgroundStyleDisabled() {

		String style = "height : " + getImageSize().toString() + "px; width : " + getImageSize().toString() + "px; display: inline-block;";
		style += "background-image : url('" + context()._urlForResourceNamed(getImgDisabledUrl(), "app", false) + "');";

		return style;
	}
	
	/**
	 * @return true si l'élément est disabled ou si son group est disabled.
	 */
	public Boolean isItemDisabled() {
		if (getCurrentAjaxMenuItemGroup() == null) {
			return isDisabled();
		} else {
			return getCurrentAjaxMenuItemGroup().isDisabled() || isDisabled();
		}
	}

	// Public Methods
	// Protected Methods
	// Private methods

	/**
	 * @return image size
	 */
	private Integer getImageSize() {
		if (getCurrentAjaxMenuPanel().getImageSize() == null) {
			return CktlAjaxMenuConstants.getDefaultImageSize();
		}

		return getCurrentAjaxMenuPanel().getImageSize();
	}

	public String getClassXCaption() {
		return CktlAjaxMenuConstants.getClassXCaption();

	}

	public String getClassXIcon() {
		return CktlAjaxMenuConstants.getClassXIcon();
	}

	public String getClassXIconDisabled() {
		return CktlAjaxMenuConstants.getClassXIconDisabled();
	}
	
	// Public methods
	@SuppressWarnings("unchecked")
	@Override
	public void appendToResponse(WOResponse aResponse, WOContext aContext) {
		ERXWOContext.contextDictionary().setObjectForKey(this, CURRENT_AJAX_MENU_ITEM);
		super.appendToResponse(aResponse, aContext);
		
		CktlAjaxUtils.addScriptResourceInHead(aContext, aResponse, "FwkCktlAjaxWebExt.framework", "scripts/watchHasChanged.js");
	}
		
}