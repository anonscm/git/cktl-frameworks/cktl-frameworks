package org.cocktail.fwkcktlajaxwebext.serveur.component.menu;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.extensions.appserver.ERXWOContext;

/**
 * Onglet d'un menu.
 */
public class CktlAjaxMenuPanelTab extends CktlAjaxMenuCommun {
	// Private fields
	private static final long serialVersionUID = 1L;

	// Constructor
	/**
	 * @param context : contexte d'édition
	 */
	public CktlAjaxMenuPanelTab(WOContext context) {
		super(context);
	}

	// Properties
	public String getTabId() {
		return getComponentId() + "_tabId";
	}

	/**
	 * @return code java script qui permet d'afficher le menu correspondnat lorsqu'on clique sur un item
	 */
	public String getDisplayInPlaceholder() {
		if (Boolean.TRUE.equals(isDisabled())) {
			return "";
		}
		
		return "onClick=displayInPlaceholder('" + getId() + "','" + getCurrentAjaxMenuPanel().getNomApplication() + "');setSelection('" + getTabId() + "','" + CktlAjaxMenuConstants.getClassTabUnselected() + "','"
		        + CktlAjaxMenuConstants.getClassTabSelected() + "');";
	}

	/**
	 * @return classe indiquant si l'élément est sélectionné
	 */
	public String getClassSelection() {
		if (isDisabled()) {
			return CktlAjaxMenuConstants.getClassTabDisabled();
		}

		if (isSelected()) {
			return CktlAjaxMenuConstants.getClassTabSelected();
		} else {
			return CktlAjaxMenuConstants.getClassTabUnselected();
		}
	}

	/**
	 * @return true si l'onglet courant est sélectionné
	 */
	public Boolean isSelected() {
		if (getCurrentAjaxMenuPanel().getSelectedTabId() == null) {
			return false;
		}
		return getCurrentAjaxMenuPanel().getSelectedTabId().equalsIgnoreCase(getId());
	}

	// Public Methods

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		ERXWOContext.contextDictionary().setObjectForKey(this, CURRENT_AJAX_MENU_PANELTAB);
		super.appendToResponse(response, context);
		if (isSelected()) {
			response.appendContentString("<script>jQuery(document).ready(function() { displayInPlaceholder('" + getId() + "','" + getCurrentAjaxMenuPanel().getNomApplication() + "'); });</script>");
		}
	}

	// Protected Methods
	// Private methods

}