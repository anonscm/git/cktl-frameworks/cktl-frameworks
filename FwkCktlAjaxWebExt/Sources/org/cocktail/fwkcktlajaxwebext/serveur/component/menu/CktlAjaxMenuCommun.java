package org.cocktail.fwkcktlajaxwebext.serveur.component.menu;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;

import er.ajax.CktlAjaxUtils;
import er.extensions.appserver.ERXWOContext;

/**
 * Encapsule les propriétés partagées par les toutes les classes qui composent un menu.
 */
public class CktlAjaxMenuCommun extends CktlAjaxWOComponent {

	// Private fields
	private static final long serialVersionUID = 1L;
	private static final String BINDING_CAPTION = "caption";
	private static final String BINDING_X_CAPTION = "x-caption";
	private static final String BINDING_VISIBLE = "visible";
	private static final String BINDING_DISABLED = "disabled";
	private static final String BINDING_ID = "id";

	protected static final String CURRENT_AJAX_MENU_COMMUN = "currentAjaxMenuCommun";
	protected static final String CURRENT_AJAX_MENU_PANEL = "currentAjaxMenuPanel";
	protected static final String CURRENT_AJAX_MENU_PANELTAB = "currentAjaxMenuPanelTab";
	protected static final String CURRENT_AJAX_MENU_ITEMGROUP = "currentAjaxMenuItemGroup";
	protected static final String CURRENT_AJAX_MENU_ITEM = "currentAjaxMenuItem";

	/**
	 * @param context : contexte d'édition
	 */
	public CktlAjaxMenuCommun(WOContext context) {
		super(context);
	}

	// Properties
	public String getCaption() {
		return valueForStringBinding(BINDING_CAPTION, "");
	}
	
	public String getXCaption() {
		return valueForStringBinding(BINDING_X_CAPTION, "");
	}

	public Boolean isVisible() {
		return booleanValueForBinding(BINDING_VISIBLE, true);
	}

	public String getId() {
		return valueForStringBinding(BINDING_ID, "");
	}

	public Boolean isDisabled() {
		return booleanValueForBinding(BINDING_DISABLED, false);
	}

	/**
	 * @return le menu principal de l'application
	 */
	public CktlAjaxMenuPanel getCurrentAjaxMenuPanel() {
		try {
			CktlAjaxMenuPanel camp = (CktlAjaxMenuPanel) ERXWOContext.contextDictionary().objectForKey(CURRENT_AJAX_MENU_PANEL);
			return camp;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	/**
	 * @return l'onglet du menu principal de l'application selectionne
	 */
	public CktlAjaxMenuPanel getCurrentAjaxMenuPanelTab() {
		try {
			CktlAjaxMenuPanel camp = (CktlAjaxMenuPanel) ERXWOContext.contextDictionary().objectForKey(CURRENT_AJAX_MENU_PANELTAB);
			return camp;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	/**
	 * @return le group courant
	 */
	public CktlAjaxMenuItemGroup getCurrentAjaxMenuItemGroup() {
		try {
			CktlAjaxMenuItemGroup itemGroup = (CktlAjaxMenuItemGroup) ERXWOContext.contextDictionary().objectForKey(CURRENT_AJAX_MENU_ITEMGROUP);
			return itemGroup;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	/**
	 * @return l'item courant
	 */
	public CktlAjaxMenuItem getCurrentAjaxMenuItem() {
		try {
			CktlAjaxMenuItem item = (CktlAjaxMenuItem) ERXWOContext.contextDictionary().objectForKey(CURRENT_AJAX_MENU_ITEM);
			return item;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	// Public Methods
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlAjaxMenu.js");
		ERXWOContext.contextDictionary().setObjectForKey(this, CURRENT_AJAX_MENU_COMMUN);
		super.appendToResponse(response, context);
	}
	// Protected Methods
	// Private methods

}
