package org.cocktail.fwkcktlajaxwebext.serveur.component.menu;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.extensions.appserver.ERXWOContext;

/**
 * Groupe d'items
 */
public class CktlAjaxMenuItemGroup extends CktlAjaxMenuCommun {

    private static final long serialVersionUID = 1L;

	/**
	 * @param context : contexte d'édition
	 */
	public CktlAjaxMenuItemGroup(WOContext context) {
        super(context);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void appendToResponse(WOResponse aResponse, WOContext aContext) {
		ERXWOContext.contextDictionary().setObjectForKey(this, CURRENT_AJAX_MENU_ITEMGROUP);
		super.appendToResponse(aResponse, aContext);
    }
}