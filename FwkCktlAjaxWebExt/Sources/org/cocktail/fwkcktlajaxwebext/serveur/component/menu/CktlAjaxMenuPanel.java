package org.cocktail.fwkcktlajaxwebext.serveur.component.menu;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.extensions.appserver.ERXWOContext;

/**
 * Contener principal du menu
 */
public class CktlAjaxMenuPanel extends CktlAjaxMenuCommun {
	// Private fields
	private static final long serialVersionUID = 1L;
	private static final String BINDING_SELECTED_TAB_ID = "selectedTabId";
	private static final String BINDING_SELECTED_ITEM_ID = "selectedItemId";
	private static final String BINDING_IMAGE_SIZE = "imageSize";
	private static final String BINDING_NOM_APPLICATION = "nomApplication";


	// Constructor
	/**
	 * @param context : contexte d'édition
	 */
	public CktlAjaxMenuPanel(WOContext context) {
		super(context);
	}

	// Properties
	public String getNomApplication() {
		return valueForStringBinding(BINDING_NOM_APPLICATION, "");
	}
	
	public String getSelectedTabId() {
		return (String) valueForBinding(BINDING_SELECTED_TAB_ID);
	}

	/**
	 * @param selectedTabId : l'onglet sélectionné
	 */
	public void setSelectedTabId(String selectedTabId) {
		setValueForBinding(selectedTabId, BINDING_SELECTED_TAB_ID);
	}

	public String getSelectedItemId() {
		return (String) valueForBinding(BINDING_SELECTED_ITEM_ID);
	}

	/**
	 * @param selectedItemId : l'élément de menu selectionné
	 */
	public void setSelectedItemId(String selectedItemId) {
		setValueForBinding(selectedItemId, BINDING_SELECTED_ITEM_ID);
	}

	public Integer getImageSize() {
		return valueForIntegerBinding(BINDING_IMAGE_SIZE, CktlAjaxMenuConstants.getDefaultImageSize());
	}

	// Public methods
	@SuppressWarnings("unchecked")
	@Override
	public void appendToResponse(WOResponse aResponse, WOContext aContext) {
		ERXWOContext.contextDictionary().setObjectForKey(this, CURRENT_AJAX_MENU_PANEL);
		super.appendToResponse(aResponse, aContext);
	}
}