package org.cocktail.fwkcktlajaxwebext.serveur.component.menu;

/**
 * Définition des constantes utilisées par les classes qui composent un menu.
 */
public final class CktlAjaxMenuConstants {
	private static final String CLASS_TAB_SELECTED = "ajaxTabbedPanelTab-selected";
	private static final String CLASS_TAB_UNSELECTED = "ajaxTabbedPanelTab-unselected";
	private static final String CLASS_TAB_DISABLED = "ajaxTabbedPanelTab-disabled";
	private static final String CLASS_ITEM_SELECTED = "selected";
	private static final String CLASS_ITEM_DISABLED = "disabled";
	private static final String CLASS_X_CAPTION = "x-caption";
	private static final String CLASS_X_ICON = "x-icon";
	private static final Integer DEFAULT_IMAGE_SIZE = 70;

	private CktlAjaxMenuConstants() {
	}

	public static String getClassTabSelected() {
		return CLASS_TAB_SELECTED;
	}

	public static String getClassTabUnselected() {
		return CLASS_TAB_UNSELECTED;
	}
	
	public static String getClassTabDisabled() {
	    return CLASS_TAB_DISABLED;
    }
	
	public static String getClassItemSelected() {
		return CLASS_ITEM_SELECTED;
	}

	public static String getClassItemDisabled() {
	    return CLASS_ITEM_DISABLED;
    }

	public static Integer getDefaultImageSize() {
	    return DEFAULT_IMAGE_SIZE;
    }

	public static String getClassXCaption() {
		return CLASS_X_CAPTION;
	}
	
	public static String getClassXIcon() {
		return CLASS_X_ICON;
	}
	
	public static String getClassXIconDisabled() {
		return CLASS_X_ICON + " " + CLASS_ITEM_DISABLED;
	}
	
}
