package org.cocktail.fwkcktlajaxwebext.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOContext;


public class CktlAjaxTextField extends CktlAjaxWOComponent {
	
	private static final long serialVersionUID = -422954917110326031L;

	public CktlAjaxTextField(WOContext context) {
        super(context);
    }
    
    public String functionName() {
		return getComponentId() + "_submitFunction";
	}

	public String getFilterFieldId() {
		return getComponentId() + "filterField";
	}

	public Boolean hasTimeout() {
		int timeout = valueForIntegerBinding("timeout", 0);
		return (timeout > 0);
	}

	public Integer getTimeout() {
		int timeout = valueForIntegerBinding("timeout", 0);
		return timeout;
	}
      
}