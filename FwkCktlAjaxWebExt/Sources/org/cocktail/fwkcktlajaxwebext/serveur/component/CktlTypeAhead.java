package org.cocktail.fwkcktlajaxwebext.serveur.component;

import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.CktlAjaxUtils;
import er.extensions.appserver.ERXResponseRewriter;

/**
 * 
 * @author jlafourc
 *
 */
public class CktlTypeAhead extends CktlAjaxWOComponent {
	
	public static final String BINDING_LISTE = "liste";

	

	private static Logger LOG = Logger.getLogger(CktlTypeAhead.class);
	
	private static final long serialVersionUID = -2304422845541605525L;

	/**
	 * @param context le context
	 */
	public CktlTypeAhead(WOContext context) {
        super(context);
    }

	public String getId() {
		return getComponentId() + "_id";
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		ERXResponseRewriter.addScriptResourceInHead(response, context(), "FwkCktlAjaxWebExt.framework", "scripts/jquery/jquery-1.10.2.js");
		ERXResponseRewriter.addScriptCodeInHead(response, context(), "jQuery.noConflict();");
        CktlAjaxUtils.addScriptResourceInHead(context(), response, "FwkCktlAjaxWebExt.framework", "scripts/typeahead.js");
        CktlAjaxUtils.addStylesheetResourceInHead(context(), response, "FwkCktlAjaxWebExt.framework", "css/typeahead.css");        
		super.appendToResponse(response, context);
	}
    
	/**
	 * @return la liste sérialisée en JSON
	 */
    public String getData() {
    	ObjectMapper mapper = new ObjectMapper();
    	String data = "[]";
    	try {
			data = mapper.writeValueAsString(getListe());
		} catch (Exception e) {
			LOG.error(e);
		}
    	return data;
    }
    
    /**
     * @return la liste des valeurs
     */
    public List<String> getListe() {
    	@SuppressWarnings("unchecked")
		List<String> liste = (List<String>) valueForBinding(BINDING_LISTE);
    	return liste;
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
    	return false;
    }

}