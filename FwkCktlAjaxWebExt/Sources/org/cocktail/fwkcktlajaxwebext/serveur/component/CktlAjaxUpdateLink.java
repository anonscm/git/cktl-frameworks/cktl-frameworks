/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.ajax.AjaxUpdateLink;
import er.ajax.AjaxUtils;
import er.extensions.appserver.ERXWOContext;
import er.extensions.components.ERXClickToOpenSupport;

public class CktlAjaxUpdateLink extends AjaxUpdateLink {

	/** Boolean. Si true, les multiples clics ne seront pas pris en compte sur le lien (evitant ainsi de generer plusieurs post vers le serveur). */
	public static final String BINDING_preventMultipleClick = "preventMultipleClick";

	public static final Boolean DEFAULT_preventMultipleClick = Boolean.TRUE;

	/** Indique l'id de la fenetre à ouvrir */
	public static final String BINDING_dialogIDForOpen = "dialogIDForOpen";

	/** Indique l'url de la page a afficher ds la fenetre à ouvrir */
	public static final String BINDING_href = "href";
	
	/** Ajout d'un binding afin de pouvoir customiser le link */
	public static final String BINDING_otherTagString = "otherTagString";

	public static final boolean isClickToOpenEnabled = Boolean.valueOf(System.getProperty("er.component.clickToOpen", "false"));

	public CktlAjaxUpdateLink(String name, NSDictionary associations, WOElement children) {
		super(name, associations, children);
	}

	public String getId(WOContext context) {
		String theId;
		if (hasBinding("id")) {
			theId = stringValueForBinding("id", context.component());
		}
		else {
			theId = "cktl_" + ERXWOContext.safeIdentifierName(context, false);
		}
		return theId;
	}

	@Override
	public String onClick(WOContext context, boolean generateFunctionWrapper) {
		String res = super.onClick(context, generateFunctionWrapper);
		if (preventMultipleClick(context.component())) {
			res = "var element=$('" + getId(context) + "');if (element.hasClassName('locked')){return false;} else {element.addClassName('locked');" + res + "}";
		}
		return res;
	}

	@Override
	protected NSMutableDictionary createAjaxOptions(WOComponent component) {
		NSMutableDictionary res = super.createAjaxOptions(component);

		if (preventMultipleClick(component)) {
			String onCompleteAddition = "var aul = $('" + getId(component.context()) + "');if (aul != null) { aul.removeClassName('locked'); };";
			// String onAddition = "$('"+getId(component)+"').removeClassName('locked');";

			String onComplete = (String) res.valueForKey("onComplete");
			if (onComplete != null) {
				int i = onComplete.indexOf("{", onComplete.indexOf("function("));
				if (i > -1) {
					onComplete = onComplete.substring(0, i + 1) + onCompleteAddition + onComplete.substring(i + 1);
				}
				else {
					//Erreur
					onComplete = null;
				}
			}
			if (onComplete == null) {
				onComplete = "function() {" + onCompleteAddition + "}";
			}
			res.takeValueForKey(onComplete, "onComplete");
		}

		if (hasBinding(BINDING_dialogIDForOpen)) {
			String dialogID = (String) valueForBinding(BINDING_dialogIDForOpen, component);
			String onSuccessAddition = "openCAW_" + dialogID + "($('" + getId(component.context()) + "').readAttribute('title')";

			if (hasBinding(BINDING_href)) {
				String href = (String) valueForBinding(BINDING_href, component);
				onSuccessAddition += ",'" + href + "'";
			}
			onSuccessAddition += ");return false;";
			String onSuccess = (String) res.valueForKey("onSuccess");
			if (onSuccess != null) {
				int i = onSuccess.indexOf("}");
				if (i > -1) {
					onSuccess = onSuccess.substring(0, i) + onSuccessAddition + onSuccess.substring(i);
				}
				else {
					//Erreur
					onSuccess = null;
				}
			}
			else {
				onSuccess = "function() {" + onSuccessAddition + "}";
			}
			res.takeValueForKey(onSuccess, "onSuccess");
		}

		return res;
	}

	protected boolean preventMultipleClick(WOComponent component) {
		if (valueForBinding("onClickBefore", component) != null) {
			return false;
		}
		Boolean preventMultipleClick = (Boolean) valueForBinding(BINDING_preventMultipleClick, component);
		if (preventMultipleClick == null) {
			preventMultipleClick = DEFAULT_preventMultipleClick;
		}
		return preventMultipleClick.booleanValue();
	}

	public void appendToResponse(WOResponse response, WOContext context) {
		ERXClickToOpenSupport.preProcessResponse(response, context, isClickToOpenEnabled);
		WOComponent component = context.component();

		boolean disabled = booleanValueForBinding("disabled", false, component);
		Object stringValue = valueForBinding("string", component);
		String functionName = (String) valueForBinding("functionName", component);
		if (functionName == null) {
			String elementName;
			boolean button = booleanValueForBinding("button", false, component);
			if (button) {
				elementName = "input";
			}
			else {
				elementName = (String) valueForBinding("elementName", "a", component);
			}
			boolean isATag = "a".equalsIgnoreCase(elementName);
			boolean renderTags = (!disabled || !isATag);
			if (renderTags) {
				response.appendContentString("<");
				response.appendContentString(elementName);
				response.appendContentString(" ");
				if (button) {
					appendTagAttributeToResponse(response, "type", "button");
				}
				if (isATag) {
					appendTagAttributeToResponse(response, "href", "javascript:void(0);");
				}
				appendTagAttributeToResponse(response, "onclick", onClick(context, false));
				appendTagAttributeToResponse(response, "title", valueForBinding("title", component));
				appendTagAttributeToResponse(response, "value", valueForBinding("value", component));
				appendTagAttributeToResponse(response, "class", valueForBinding("class", component));
				appendTagAttributeToResponse(response, "style", valueForBinding("style", component));
				appendTagAttributeToResponse(response, "id", getId(context));
				appendTagAttributeToResponse(response, "accesskey", valueForBinding("accesskey", component));
				appendTagAttributeToResponse(response, "tabindex", valueForBinding("tabindex", component));
				response.appendContentString((String) valueForBinding("otherTagString", "", component));
				if (button) {
					if (stringValue != null) {
						appendTagAttributeToResponse(response, "value", stringValue);
					}
					if (disabled) {
						response.appendContentString(" disabled");
					}
				}
				// appendTagAttributeToResponse(response, "onclick",
				// onClick(context));
				response.appendContentString(">");
			}
			if (stringValue != null && !button) {
				response.appendContentHTMLString(stringValue.toString());
			}
			appendChildrenToResponse(response, context);
			if (renderTags && !button) {
				response.appendContentString("</");
				response.appendContentString(elementName);
				response.appendContentString(">");
			}
		}
		else {
			AjaxUtils.appendScriptHeader(response);
			response.appendContentString(functionName);
			response.appendContentString(" = ");
			response.appendContentString(onClick(context, true));
			AjaxUtils.appendScriptFooter(response);
		}
		addRequiredWebResources(response, context);
		ERXClickToOpenSupport.postProcessResponse(getClass(), response, context, isClickToOpenEnabled);
	}
}
