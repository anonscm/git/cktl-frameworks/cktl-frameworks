/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import java.util.Calendar;
import java.util.Date;

import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.RandomKeyGenerator;
import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.ajax.CktlAjaxUtils;
import er.extensions.components.ERXClickToOpenSupport;
import er.extensions.foundation.ERXSimpleTemplateParser;
import er.extensions.validation.ERXValidationException;
import er.extensions.validation.ERXValidationFactory;

/**
 * Ce composant permet de selectionner une date, et de l'affecter a une variable. La valeur est passee dans un <code>WOTextField</code> cache, puis
 * renvoyee au serveur web en effectuant un submit sur un formulaire.<br>
 * <br>
 * <p align="center">
 * <table>
 * <tr>
 * <td align="center" valign="bottom"><img src="../../../../doc-files/CktlDatePicker-1.png" border="1"/></td>
 * <td>&nbsp;&nbsp;&nbsp;</td>
 * <td align="center"><img src="../../../../doc-files/CktlDatePicker-2.png" border="1"/></td>
 * </tr>
 * <tr>
 * <td><i>"Bouton" permettant d'ouvrir la fenetre du CktlDatePicker</i></td>
 * <td></td>
 * <td><i>Fenetre du CktlDatePicker (se ferme des que l'utilisateur choisit une date)</i></td>
 * </tr>
 * </table>
 * </p>
 * <br>
 * <h3>Bindings</h3>
 * <p>
 * <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 * <tr align="center" class="paramBoxHeader">
 * <td>Connecteur</td>
 * <td>Definition</td>
 * <td>Valeur</td>
 * <td>Description</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>dateValue</code></td>
 * <td>obligatoire</td>
 * <td><code>NSTimestamp</code><br>
 * [<i>null</i>]</td>
 * <td align="left">La date qui sera renseignee par la selection de l'utilisateur.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>showsTime</code></td>
 * <td>optionnel</td>
 * <td><code>boolean</code><br>
 * [<i>false</i>]</td>
 * <td align="left">Le composant gere les heures / minutes.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>disabled</code></td>
 * <td>optionnel</td>
 * <td><code>boolean</code><br>
 * [<i>false</i>]</td>
 * <td align="left">Le composant est-il accessible en modification.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>onChange</code></td>
 * <td>optionnel</td>
 * <td><code>action</code><br>
 * [<i>null</i>]</td>
 * <td align="left">Action exécutée lors de la sélection de la date ou après sortie du champ de la date</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>min</code></td>
 * <td>optionnel</td>
 * <td><code>NSTimestamp</code><br>
 * [<i>null</i>]</td>
 * <td align="left">La date minimum sélectionnable sur le calendrier</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>max</code></td>
 * <td>optionnel</td>
 * <td><code>NSTimestamp</code><br>
 * [<i>null</i>]</td>
 * <td align="left">La date maximum sélectionnable sur le calendrier</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>bottomBar</code></td>
 * <td>optionnel</td>
 * <td><code>String</code><br>
 * [<i>true</i>]</td>
 * <td align="left">La barre du bas du calendrier (Aujourd'hui) doit-elle être affichée ?</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author Arunas STOCKUS <arunas.stockus at univ-lr.fr>
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */
public class CktlAjaxDatePicker extends CktlWebComponent {

	private static final long serialVersionUID = -1L;
	// Les noms des connecteurs
	private static final String BND_DATE_VALUE = "dateValue";
	private static final String BND_DISABLED = "disabled";
	private static final String BND_SHOWS_TIME = "showsTime";
	private static final String BND_ID = "id";
	private static final String BND_ON_CHANGE = "onChange";
	private static final String BND_DATE_MIN = "min";
	private static final String BND_DATE_MAX = "max";
	private static final String BND_BOTTOM_BAR = "bottomBar";
	
	// Le format de la date pour JavaScript
	private static final String DEFAULT_DATE_FORMAT_JS = "%d/%m/%Y";
	private static final String DEFAULT_DATETIME_FORMAT_JS = "%d/%m/%Y %H:%M";
	private static final String DATE_FORMAT_YMD_JS = "%Y%m%d";
	
	// Le format de la date pour WebObjects
	private static final String DEFAULT_DATE_FORMAT_WO = "%d/%m/%Y";
	private static final String DEFAULT_DATETIME_FORMAT_WO = "%d/%m/%Y %H:%M";
	
	private static final String BND_VERIFIE_SIECLE_COURANT = "verifieSiecleCourant";
	
	private static final String DEFAULT_DATE = "00000000";
	
	private String fieldID;
	private String imageID;

	private NSTimestamp dateValue;

	// Dans cette implementation, les sources JavaScript se trouvent dans un
	// sous-repertoire
	private final static String JS_PARENT_FOLDER = "jscript/CktlDatePicker/";
	private final static String CSS_PARENT_FOLDER = "css/CktlDatePicker/";
	// Les noms des fichiers avec les fonctions JavaScript du composant.
	//	public final static String DATE_PICKER_STYLE = CSS_PARENT_FOLDER+"calendar-ulr.css";
	//	public final static String DATE_PICKER_CODE =  JS_PARENT_FOLDER+"calendar.js";
	//	public final static String DATE_PICKER_LANG =  JS_PARENT_FOLDER+"calendar-fr-ulr.js";
	//	public final static String DATE_PICKER_SETUP = JS_PARENT_FOLDER+"calendar-setup.js";
	public final static String DATE_PICKER_STYLE = CSS_PARENT_FOLDER + "jscal2.css";
	public final static String DATE_PICKER_STYLE1 = CSS_PARENT_FOLDER + "border-radius.css";
	public final static String DATE_PICKER_CODE = JS_PARENT_FOLDER + "jscal2.js";
	public final static String DATE_PICKER_LANG = JS_PARENT_FOLDER + "lang/fr.js";

	//FIXME Corriger le JS pour submit partiel de la selection (ne recuperer que lors de la selection et non sur le onUpdate)

	public static final boolean isClickToOpenEnabled = Boolean.valueOf(System.getProperty("er.component.clickToOpen", "false"));

	private static final int CENT = 100;
	
	public CktlAjaxDatePicker(WOContext context) {
		super(context);
	}

	@Override
	public void validationFailedWithException(Throwable e, Object obj, String keyPath) {
		interceptException(e);
		super.validationFailedWithException(e, obj, keyPath);
	}

	/**
	 * @param e
	 *            L'exception
	 */
	protected void interceptException(Throwable e) {
		if (e instanceof ERXValidationException) {
			ERXValidationException exception = (ERXValidationException) e;
			String template = ERXValidationFactory.defaultFactory().templateForException(exception);
			String message = ERXSimpleTemplateParser.sharedInstance().parseTemplateWithObject(template, null, this);
			((CocktailAjaxSession) session()).addSimpleErrorMessage(message, "");
		}
	}

	/*
	 * @see com.webobjects.appserver.WOComponent#synchronizesVariablesWithBindings()
	 */
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public boolean isStateless() {
		return true;
	}

	public void reset() {
		fieldID = null;
		imageID = null;
		dateValue = null;
	}

	public NSTimestamp dateValue() {
		if (dateValue == null) {
			dateValue = (NSTimestamp) valueForBinding(BND_DATE_VALUE);
		}
		return dateValue;
	}

	public boolean disabled() {
		if (valueForBinding(BND_DISABLED) != null) {
			return ((Boolean) valueForBinding(BND_DISABLED)).booleanValue();
		}
		return false;
	}

	public boolean showsTime() {
		if (valueForBinding(BND_SHOWS_TIME) != null) {
			return ((Boolean) valueForBinding(BND_SHOWS_TIME)).booleanValue();
		}
		return false;
	}

	/**
	 * Si le binding BND_VERIFIE_SIECLE_COURANT retourne true, on vérifie que l'année n'a pas seulement deux chiffres
	 * Dans tous les cas on affecte la nouvelle valeur
	 * @param dateValue la date a affecter
	 */
	public void setDateValue(NSTimestamp dateValue) {
		if (dateValue != null && verifieSiecleCourant()) {
			Calendar calendar = Calendar.getInstance();
			
			calendar.setTime(new Date());
	        int centaine = (calendar.get(Calendar.YEAR) / CENT);
	        
	        calendar.setTime(dateValue);
	        int year = calendar.get(Calendar.YEAR);
	        if (year < CENT) {
	        	calendar.set(centaine * CENT + year, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 
        				calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
	        }
	        
	        dateValue = new NSTimestamp(calendar.getTime());
		}
		this.dateValue = dateValue;
		setValueForBinding(dateValue, BND_DATE_VALUE);
	}

	public String dateFormatJS() {
		return !showsTime() ? DEFAULT_DATE_FORMAT_JS : DEFAULT_DATETIME_FORMAT_JS;
	}

	public String dateFormatWO() {
		return !showsTime() ? DEFAULT_DATE_FORMAT_WO : DEFAULT_DATETIME_FORMAT_WO;
	}

	public String fieldID() {
		if (fieldID == null) {
			if (hasBinding(BND_ID)) {
				fieldID = (String) valueForBinding(BND_ID);
			} else {
				fieldID = "f_date_" + RandomKeyGenerator.getNewKey(10);
			}
		}
		return fieldID;
	}

	public void setFieldID(String fieldID) {
		this.fieldID = fieldID;
	}

	public String imageID() {
		if (imageID == null) {
			imageID = "f_trigger_" + RandomKeyGenerator.getNewKey(10);
		}
		return imageID;
	}

	public int fieldSize() {
		if (showsTime()) {
			return 16;
		} else {
			return 10;
		}
	}

	/**
	 * Ajouter les CSS et js requis
	 */
	public void appendToResponse(WOResponse response, WOContext context) {
		ERXClickToOpenSupport.preProcessResponse(response, context, isClickToOpenEnabled);
		super.appendToResponse(response, context);
		//		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlWebApp.framework", DATE_PICKER_STYLE);
		//		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlWebApp.framework", "jscript/CktlDatePicker/cktlcalendar.js");
		//		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlWebApp.framework", DATE_PICKER_LANG);
		//		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlWebApp.framework", DATE_PICKER_SETUP);
		//		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlajaxdatepicker.js");
		//		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/dateformat.js");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlWebApp.framework", DATE_PICKER_STYLE);
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlWebApp.framework", DATE_PICKER_STYLE1);
		//CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlWebApp.framework", DATE_PICKER_CODE);
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktljscal2.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlWebApp.framework", DATE_PICKER_LANG);
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlajaxdatepicker.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/dateformat.js");
		//CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlWebApp.framework", "jscript/CktlDatePicker/cktljscal2.js");
		ERXClickToOpenSupport.postProcessResponse(getClass(), response, context, isClickToOpenEnabled);
	}

	public String containerID() {
		return fieldID() + "_container";
	}

	public WOActionResults submit() {
	    /*if (hasBinding(BND_ON_CHANGE)) {
	        return (WOActionResults) valueForBinding(BND_ON_CHANGE);
	    }*/

	    if (verifieSiecleCourant()) {
		    AjaxUpdateContainer.updateContainerWithID(containerID(), context());
	    }
	    
	    return null;
	}

	public String getMinDate() {
		if (valueForBinding(BND_DATE_MIN) != null) {
			return (String) DateCtrl.dateToString((NSTimestamp) valueForBinding(BND_DATE_MIN), DATE_FORMAT_YMD_JS);
		}
		return DEFAULT_DATE;
	}
	
	public String getMaxDate() {
		if (valueForBinding(BND_DATE_MAX) != null) {
			return (String) DateCtrl.dateToString((NSTimestamp) valueForBinding(BND_DATE_MAX), DATE_FORMAT_YMD_JS);
		} 
		return DEFAULT_DATE;	
	}
	
	public boolean bottomBar() {
		if (valueForBinding(BND_BOTTOM_BAR) != null) {
			return ((Boolean) valueForBinding(BND_BOTTOM_BAR)).booleanValue();
		}
		return true;
	}
	
	public boolean verifieSiecleCourant() {
		return valueForBooleanBinding(BND_VERIFIE_SIECLE_COURANT, false);
	}
	
	/*
	public String onChange() {
			String onChange = containerID()+"Update()";
			return onChange;
	}*/
	
	public String onChange() {
	    return (String) valueForBinding(BND_ON_CHANGE);
	}
}
