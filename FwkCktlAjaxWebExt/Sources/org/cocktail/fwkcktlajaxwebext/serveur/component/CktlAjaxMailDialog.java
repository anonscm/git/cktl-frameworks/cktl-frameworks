package org.cocktail.fwkcktlajaxwebext.serveur.component;


import java.io.File;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxMailMessage;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxDestinatairesListeSelector.DestinataireListe;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

import er.extensions.foundation.ERXFileUtilities;



public class CktlAjaxMailDialog extends CktlAjaxWOComponent{
	private static final long serialVersionUID = 1L;
	public static final String BINDING_windowId = "windowId";
	public static final String BINDING_callbackOnAnnulerMail = "callbackOnAnnulerMail";
	public static final String BINDING_callbackOnValiderMail = "callbackOnValiderMail";
	public static final String BINDING_CktlAjaxMailMessage = "cktlMailMessage";
	public static String BINDING_EDITING_CONTEXT = "editingContext";

	private String emailsTo = null;
	private String emailsToCc = null;
	private String emailsToBcc = null;
	private NSData foNewUploadData;
	private String foNewUploadFilePath;
	private String finalFilePath;

	private String callbackOnAnnulerMail;
	private String callbackOnValiderMail;

	
	public CktlAjaxMailDialog(WOContext context) {
        super(context);
    }
	   public EOEditingContext editingContext() {
	        return (EOEditingContext)valueForBinding(BINDING_EDITING_CONTEXT);
	    }
 

	public String modalWindowId() {
		return (String) valueForBinding(BINDING_windowId);
	}

	public String getContainerWindowId() {
		return getComponentId() + "_winContainer";
	}

	public String containerDestinataireId() {
		return getComponentId() + "_destContainer";
	}
	public String containerAttachementId() {
		return getComponentId() + "_attachement";
	}

	public String containerActionId() {
		return getComponentId() + "_actions";
	}

	public String containerSubjectId() {
		return getComponentId() + "_subject";
	}

	public String containerMessageId() {
		return getComponentId() + "_msg";
	}

	public String subjectId() {
		return getComponentId() + "_subjectId";
	}
	public String pourId() {
		return getComponentId() + "_to";
	}
	public String pourIdCc() {
		return getComponentId() + "_toCc";
	}
	public String pourIdBcc() {
		return getComponentId() + "_toBcc";
	}
	public NSData getFoNewUploadData() {
		return foNewUploadData;
	}

	public void setFoNewUploadData(NSData foNewUploadData) {
		this.foNewUploadData = foNewUploadData;
	}

	public String getFoNewUploadFilePath() {
		return foNewUploadFilePath;
	}

	public void setFoNewUploadFilePath(String foNewUploadFilePath) {
		this.foNewUploadFilePath = foNewUploadFilePath;
	}
	public String finalFilePath() {
		//fofinalFilePath = EOGrhumParametres.PARAM_GRHUM_HOME;
		return finalFilePath;
	}

	public void setFinalFilePath(String finalFilePath) {
		this.finalFilePath = finalFilePath;
	}

	public void cleanFileUploadMain() {
		foNewUploadData = null;
		foNewUploadFilePath = null;
		finalFilePath = null;
	}
	
	public String getOnFuFinishedJs() {
		return containerAttachementId() + "Update";
	}

	public void telechargementReussi() throws Exception {
		if ( !MyStringCtrl.isEmpty(finalFilePath())) {
			File tempfile = new File(finalFilePath);
			String directory = tempfile.getParent();
			StringBuffer filedest = new StringBuffer(directory).append(File.separator).append(foNewUploadFilePath);
			ERXFileUtilities.renameTo(tempfile, new File(filedest.toString()));
			setFinalFilePath(filedest.toString());
			//cktlAjaxMailMessage().addAttachment(foNewUploadFilePath);
			//finalFilePath = (String) valueForBinding("finalFilePath");
			cktlAjaxMailMessage().addAttachment(finalFilePath);
		}
	} 
	public CktlAjaxDestinatairesListeSelector.DestinataireListe getDestinatairesForMail() {
		return cktlAjaxMailMessage().getDestinataires();
	}
	public String getCallbackOnAnnulerMail() {
		if (callbackOnAnnulerMail == null) {
			callbackOnAnnulerMail = (String) valueForBinding(BINDING_callbackOnAnnulerMail);
		}
		return callbackOnAnnulerMail;
	}

	public String getCallbackOnValiderMail() {
		if (callbackOnValiderMail == null) {
			callbackOnValiderMail = (String) valueForBinding(BINDING_callbackOnValiderMail);
		}
		return callbackOnValiderMail;
	}


	public WOActionResults onSendMailAnnuler() {
		return performParentAction(getCallbackOnAnnulerMail());
	}

	public WOActionResults onSendMailValider() {
		return performParentAction(getCallbackOnValiderMail());
	}
	
	
	public WOActionResults annuler() {
		WOActionResults res = null;
		if (hasBinding(BINDING_callbackOnAnnulerMail)) {
			res = performParentAction((String) valueForBinding(BINDING_callbackOnAnnulerMail));
		} 
		CktlAjaxWindow.close(context());
		return res;
	}

	public WOActionResults valider() {
		WOActionResults res = null;
		try {
			validerEmail();
			CktlWebApplication app = ((CktlWebApplication) CktlWebApplication.application());

			cktlAjaxMailMessage().initAndSend(app.config().stringForKey(CktlConfig.CONFIG_GRHUM_HOST_MAIL_KEY));

		if (hasBinding(BINDING_callbackOnValiderMail)) {
				res = performParentAction((String) valueForBinding(BINDING_callbackOnValiderMail));
			}
		CktlAjaxWindow.close(context());
			
		} catch (Exception e) {
			if (!MyStringCtrl.isEmpty(e.getLocalizedMessage())) {
				mySession().addSimpleErrorMessage("Erreur", e.getLocalizedMessage());
			}
		} finally {
			setEmailsTo(null);
			setEmailsToBcc(null);
			setEmailsToCc(null);
		}
		

		return res;
	}

	public Boolean isValiderDisabled() {
		return Boolean.FALSE;
	}

	public CktlAjaxMailMessage cktlAjaxMailMessage() {
		return (CktlAjaxMailMessage) valueForBinding(BINDING_CktlAjaxMailMessage);
	}

	public void validerEmail() throws Exception {
		cktlAjaxMailMessage().prepare();
		cktlAjaxMailMessage().checkMailReady();
	}

	public String getJsValiderOnComplete() {
		return (updateContainerID() != null ? getJsForUpdateContainerUpdate() : null);
	}
	public String getEmailsTo() {
		NSArray<String>  destinataires = cktlAjaxMailMessage().getArrayTos();
		if (destinataires != null){
			emailsTo = destinataires.componentsJoinedByString(", ");
		} else {
			emailsTo = " ";
		}
		return emailsTo;
	}
	
	
	/**
	 * @param emailsToSaisis the emailsToSaisis to set
	 */
	public void setEmailsTo(String emailsTo) {
		NSArray<String> listEmail  = NSArray.componentsSeparatedByString(emailsTo, ",");
		cktlAjaxMailMessage().setDestinataires(new DestinataireListe() );
		for (int i =0; i < listEmail.size(); i++) {
			cktlAjaxMailMessage().addTo(null, (listEmail.get(i)).trim());
		}
		this.emailsTo = emailsTo;
	}
	/**
	 * @return the emailsToCc
	 */
	public String getEmailsToCc() {
		return emailsToCc;
	}
	/**
	 * @param emailsToCc the emailsToCc to set
	 */
	public void setEmailsToCc(String emailsToCc) {
		this.emailsToCc = emailsToCc;
		NSArray<String> listEmail  = NSArray.componentsSeparatedByString(emailsToCc, ",");
		for (int i =0; i < listEmail.size(); i++) {
			cktlAjaxMailMessage().addCc(null, listEmail.get(i).trim());
		}
	}
	/**
	 * @return the emailsToBcc
	 */
	public String getEmailsToBcc() {
		return emailsToBcc;
	}
	/**
	 * @param emailsToBcc the emailsToBcc to set
	 */
	public void setEmailsToBcc(String emailsToBcc) {
		this.emailsToBcc = emailsToBcc;
		NSArray<String> listEmail  = NSArray.componentsSeparatedByString(emailsToBcc, ",");
		for (int i =0; i < listEmail.size(); i++) {
			cktlAjaxMailMessage().addBcc(null, listEmail.get(i).trim());
		}
	}
	
	
}