package org.cocktail.fwkcktlajaxwebext.serveur.component.treetable;

import java.util.List;

public interface TreeTableNode {

    /**
     * @return tableau de nodes enfants
     */
    List<? extends TreeTableNode> getChildren();

    Boolean isVisible();

    void setVisible(Boolean visible);

    Boolean isSelectable();

    void setSelectable(Boolean selectable);

}