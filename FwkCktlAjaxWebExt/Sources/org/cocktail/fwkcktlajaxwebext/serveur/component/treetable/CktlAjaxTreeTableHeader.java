package org.cocktail.fwkcktlajaxwebext.serveur.component.treetable;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOElement;
import com.webobjects.foundation.NSDictionary;

import er.extensions.components.conditionals.ERXWOTemplate;

/**
 * Template pour la partie header d'un treeTable
 */
public class CktlAjaxTreeTableHeader extends ERXWOTemplate {

	/**
	 * @param s : nom
	 * @param associations : associations
	 * @param woelement : template
	 */
	@SuppressWarnings("rawtypes")
	public CktlAjaxTreeTableHeader(String s, NSDictionary associations, WOElement woelement) {
		super(s, associations, woelement);
	}

	@Override
	public String templateName(WOComponent component) {
		return "TreeTableHeaderContent";
	}
}
