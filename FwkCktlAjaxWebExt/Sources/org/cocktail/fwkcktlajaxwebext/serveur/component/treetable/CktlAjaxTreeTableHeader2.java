package org.cocktail.fwkcktlajaxwebext.serveur.component.treetable;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOElement;
import com.webobjects.foundation.NSDictionary;

import er.extensions.components.conditionals.ERXWOTemplate;

/**
 * Template pour la partie header d'un treeTable
 */
public class CktlAjaxTreeTableHeader2 extends ERXWOTemplate {

	/**
	 * @param s : nom
	 * @param associations : associations
	 * @param woelement : template
	 */
	@SuppressWarnings("rawtypes")
	public CktlAjaxTreeTableHeader2(String s, NSDictionary associations, WOElement woelement) {
		super(s, associations, woelement);
	}

	@Override
	public String templateName(WOComponent component) {
		return "TreeTableHeader2Content";
	}
}
