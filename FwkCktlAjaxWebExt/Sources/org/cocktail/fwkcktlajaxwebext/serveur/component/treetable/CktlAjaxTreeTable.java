package org.cocktail.fwkcktlajaxwebext.serveur.component.treetable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.TransformerUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.CktlAjaxUtils;
import er.extensions.appserver.ERXResponseRewriter;
import er.extensions.appserver.ERXWOContext;


/**
 * Composant de type table avec des elements repliables
 * @param <T> : type des elements contenu dans la table
 */
public class CktlAjaxTreeTable<T> extends CktlAjaxWOComponent {
	// Private fields
	private static final long serialVersionUID = 1L;
	private static final String BINDING_ROOT_NODE = "rootNode";
	private static final String BINDING_CURRENT_NODE = "currentNode";
	private static final String BINDING_SELECTED_NODE = "selectedNode";
	private static final String CST_NOT_SELECTABLE = "notSelectable";
	private static final String BINDING_USE_FIXED_HEADER = "useFixedHeader";
	private static final String BINDING_IS_EXPAND_COLLAPSE_ALL = "isExpandCollapseAll";
	private static final String BINDING_HEIGHT = "height";
	private static final String SHOW_TOOL_TIP_BINDING = "showToolTip";
	
	private static final String DEFAULT_HEIGHT_FIXED_TREE_TABLE = "300px";
	
	public static final String CURRENT_TREE_TABLE = "CurrentTreeTable";

	
	// Constructor
	/**
	 * @param context : contexte d'edition
	 */
	public CktlAjaxTreeTable(WOContext context) {
		super(context);
	}
	
	/**
	 * si un id est défini par le composant parent, on le recupère à chaque fois
	 * sinon on appelle la méthode standard pour éventuellement générer un id unique
	 * @return l'id du treeTable
	 */
	@Override
	public String getComponentId() {
		return stringValueForBinding(BINDING_id, super.getComponentId());
	}

	// Properties
	@SuppressWarnings("unchecked")
	public CktlTreeTableNode<T> getRootNode() {
		return (CktlTreeTableNode<T>) valueForBinding(BINDING_ROOT_NODE);
	}

	@SuppressWarnings("unchecked")
	public CktlTreeTableNode<T> getCurrentNode() {
		return (CktlTreeTableNode<T>) valueForBinding(BINDING_CURRENT_NODE);
	}

	/**
	 * @param currentNode : noeud a passer en binding
	 */
	public void setCurrentNode(CktlTreeTableNode<T> currentNode) {
		setValueForBinding(currentNode, BINDING_CURRENT_NODE);
	}

	@SuppressWarnings("unchecked")
	public CktlTreeTableNode<T> getSelectedNode() {
		return (CktlTreeTableNode<T>) valueForBinding(BINDING_SELECTED_NODE);
	}

	/**
	 * @param selectedNode : noeud a passer en binding
	 */
	public void setSelectedNode(CktlTreeTableNode<T> selectedNode) {
		setValueForBinding(selectedNode, BINDING_SELECTED_NODE);
	}
	
	/**
	 * Binding pour utiliser le header fixe
	 * @return Boolean 
	 */
	public Boolean useFixedHeader() {
		return booleanValueForBinding(BINDING_USE_FIXED_HEADER, false);
	}
	
	/**
	 * Binding pour indiquer si on affiche l'option pour Expand/Collapse All
	 * @return Boolean 
	 */
	public Boolean isExpandCollapse() {
		return booleanValueForBinding(BINDING_IS_EXPAND_COLLAPSE_ALL, false);
	}

	/**
	 * @return ensemble des elements du TreeTable
	 */
	public List<CktlTreeTableNode<T>> getNodes() {
		List<CktlTreeTableNode<T>> nodes = new ArrayList<CktlTreeTableNode<T>>();
		if (getRootNode() != null) {
			for (CktlTreeTableNode<T> composantNode2 : getRootNode().allDescendants()) {
				nodes.add((CktlTreeTableNode<T>) composantNode2);
			}
		}

		return nodes;
	}

	/**
	 * @return la class du noeud (index + statut selectable ou pas)
	 */
	/*public String getNodeClass() {
		String nodeclass = getCurrentNode().getNodeChildOfIndex();
		if (!getCurrentNode().isSelectable()) {
			nodeclass += " " + CST_NOT_SELECTABLE;
		} else {
		 	if (getCurrentNode() != null && getCurrentNode().equals(getSelectedNode())) {
		 		nodeclass += "  selected";
			}
		}
		
		if (getCurrentNode().isExpanded()) {
			nodeclass += " expanded";
		} else {
			nodeclass += " collapsed";
		}
		
		if (getCurrentNode().getParent() != null) {
			if (!getCurrentNode().getParent().isExpanded()) {
				nodeclass += " ui-helper-hidden";
			}
		}
		
		return nodeclass;
	}*/

	// Public Methods
	@SuppressWarnings("unchecked")
    @Override
	public void appendToResponse(WOResponse response, WOContext context) {
		// On s'enregistre pour que les colonnes puissent nous contacter en tant que
		// reordering ou autre manipulations...
		// Rajout du javascript necessaire
		ERXWOContext.contextDictionary().setObjectForKey(this, CURRENT_TREE_TABLE);
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "css/jquery.treetable-3.0.0.css");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/jquery.treetable-ajax-persist.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/jquery.treetable-3.0.0.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/persist-min.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/treeTable.js");

		boolean useFixedHeader = useFixedHeader();
		boolean showToolTip = showToolTip();
		
		if (useFixedHeader) {
		    CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "css/fixedHeaderTreeTable.css");
		    CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/fixedHeaderTreeTable.js");
		}

		if (showToolTip) {
		    ERXResponseRewriter.addScriptResourceInHead(response, context(), "FwkCktlAjaxWebExt.framework", "scripts/cktlToolTip.js");
		}
		// Pour des raisons de performance, la réponse générée par le treetable pouvant être énorme,
		// on a donc inséré les scripts avant de générer la reponse
		super.appendToResponse(response, context);
        response.appendContentString("<script>TREE_TABLE.init('" + getTreeTableId() + "', " + getNodesMaxId() + ");</script>");
        if (useFixedHeader) {
            response.appendContentString("<script>TREE_TABLE_HEADER.useFixedHeader();</script>");
        }
	}

	/**
	 * @return null (reste sur la page)
	 */
	public WOActionResults selectionner() {
		if (getCurrentNode() != null && getCurrentNode().isSelectable()) {
			setSelectedNode(getCurrentNode());
		}

		return doNothing();
	}
	
	public String getTreeTableId() {
		return "treeTable_" + getComponentId();
	}
	
	
	public Integer getNodesMaxId() {		
		Transformer transformer = TransformerUtils.invokerTransformer("getIndex");
		List<Integer> indexes = (List<Integer>) CollectionUtils.collect(getNodes(), transformer);
		if (indexes.size() >0 )
			return Collections.max(indexes);
		else 
			return null;
	}
	
	public WOActionResults expandAll() {
		return doNothing();
	}
	
	public WOActionResults collapseAll() {
		return doNothing();
	}
	
	public String styleTableContainer() {
		return "height:" + valueForStringBinding(BINDING_HEIGHT, DEFAULT_HEIGHT_FIXED_TREE_TABLE) + "; ";
	}
	
	/**
	 * @return true si on doit afficher les tooltip si le texte est trop long à afficher dans une case de la table
	 */
	public Boolean showToolTip() {
	  return booleanValueForBinding(SHOW_TOOL_TIP_BINDING, false);
	}
	
	/**
	 * @return la classe CSS
	 */
	public String getCurrentNodeClass() {
		if (getCurrentNode().isExpanded()) {
			return "expanded";
		} else {
			return "collapsed";
		}
	}

}