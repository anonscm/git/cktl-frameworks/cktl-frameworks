package org.cocktail.fwkcktlajaxwebext.serveur.component.treetable;

import java.util.ArrayList;
import java.util.List;


/**
 * Contenu d'une ligne d'un CktlAjaxTreeTable
 * @param <T> : type de composant représenté
 */
public abstract class CktlTreeTableNode<T> implements TreeTableNode {
	// Private fields
	private T data;
	private CktlTreeTableNode<T> parent;
	private int index;
	private Boolean visible;
	private Boolean selectable;
	private Boolean expanded = true;
	private Integer niveau;

	// Constructor
	/**
	 * @param data : donnée contenue dans le node
	 * @param parent : parent du node
	 * @param visible : true si ce node doit être affiché, false sinon
	 * @param expanded : true si la node est dépliée, false sinon
	 */
	public CktlTreeTableNode(T data, CktlTreeTableNode<T> parent, Boolean visible, Boolean expanded) {
		super();
		this.setData(data);
		this.setParent(parent);
		this.setVisible(visible);
		this.setExpanded(expanded);
		this.setSelectable(true);
	}
	
	/**
	 * @param data : donnée contenue dans le node
	 * @param parent : parent du node
	 * @param visible : true si ce node doit être affiché, false sinon
	 */
	public CktlTreeTableNode(T data, CktlTreeTableNode<T> parent, Boolean visible) {
		this(data, parent, visible, true);
	}

	// Properties
	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public CktlTreeTableNode<T> getParent() {
		return parent;
	}

	public void setParent(CktlTreeTableNode<T> parent) {
		this.parent = parent;
	}
	
	/**
	 * @return Boolean : Indique si le noeud a un parent
	 */
	public Boolean hasParent() {
		return (getParent()!=null);
	}

	/**
	 * @return tableau de nodes enfants
	 */
	public abstract List<CktlTreeTableNode<T>> getChildren();

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Boolean isVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return index du node
	 */
	/*public String getNodeIndex() {
		if (getFirstParentNodeVisible() == null) {
			if (getParent() == null) {
				setIndex("node-0");
			} else {
				setIndex("node-" + getParent().getChildren().indexOf(this));
			}
		} else {
			setIndex(getParent().index + "-" + getFirstParentNodeVisible().getChildren().indexOf(this));
		}

		return index;
	}*/
	
	public Integer getNiveau() {
		return this.niveau;
	}
	
	public void setNiveau(Integer niveau) {
		this.niveau = niveau;
	}
	
	public String getNodeValue() {
		if(hasParent() && this.parent.isVisible()) {
			return "data-tt-id="+this.getIndex()+" data-tt-parent-id="+this.getParent().getIndex();
		}
		else {
			return "data-tt-id="+this.getIndex();
		}
	}

	// Public Methods
	/**
	 * @return l'ensemble de la descendance d'un noeud donné
	 */
	public List<CktlTreeTableNode<T>> allDescendants() {
		List<CktlTreeTableNode<T>> nodes = new ArrayList<CktlTreeTableNode<T>>();
		setIndexBuildTree(0);
		this.setNiveau(0);
		addToNodes(nodes, this);

		return nodes;
	}

	/**
	 * @return index tag du node
	 */
	/*public String getNodeChildOfIndex() {
		if (getFirstParentNodeVisible() != null) {
			return "child-of-" + getFirstParentNodeVisible().getNodeIndex();
		}
		return "";
	}*/

	public Boolean isSelectable() {
		return selectable;
	}

	public void setSelectable(Boolean selectable) {
		this.selectable = selectable;
	}

	// Protected Methods

	// Private methods
	private CktlTreeTableNode<T> getFirstParentNodeVisible() {
		if (getParent() != null) {
			CktlTreeTableNode<T> composantParent = getParent();

			while (composantParent != null && !composantParent.isVisible()) {
				composantParent = composantParent.getParent();
			}

			return composantParent;
		}
		return null;

	}

	private int indexBuildTree;
	private int getIndexBuildTree() {
		return this.indexBuildTree;
	}
	private void setIndexBuildTree(int index) {
		this.indexBuildTree = index;
	}
	
	private void addToNodes(List<CktlTreeTableNode<T>> nodesArray, CktlTreeTableNode<T> aNode) {
		aNode.setIndex(getIndexBuildTree());
		nodesArray.add(aNode);
		int niveauParent = aNode.getNiveau();
		int niveauChildren = niveauParent + 1;
		if (aNode.getChildren() != null) {
			for (CktlTreeTableNode<T> childNode : aNode.getChildren()) {
				setIndexBuildTree(getIndexBuildTree() + 1);
				childNode.setIndex(getIndexBuildTree());
				childNode.setNiveau(niveauChildren);
				addToNodes(nodesArray, childNode);
			}
		}

		setIndexBuildTree(getIndexBuildTree() + 1);
	}

	/**
	 * @param node : noeud pour lequel on cherche un frere précédent
	 * @return : noeud précédent le noeud node.
	 */
	private CktlTreeTableNode<T> frerePrecedent() {
		CktlTreeTableNode<T> frereNode = null;
		if (this.getParent() != null) {
			int i = this.getParent().getChildren().indexOf(this);
			if (i > 0) {
				frereNode = this.getParent().getChildren().get(i - 1);
			}
		}
		return frereNode;
	}

	/**
	 * @param node : noeud pour lequel on cherche un frere suivant
	 * @return : noeud précédent le noeud node.
	 */
	private CktlTreeTableNode<T> frereSuivant() {
		CktlTreeTableNode<T> frereNode = null;
		if (this.getParent() != null) {
			int i = this.getParent().getChildren().indexOf(this);
			if (i < this.getParent().getChildren().size() - 1) {
				frereNode = this.getParent().getChildren().get(i + 1);
			}
		}
		return frereNode;
	}

	public Boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(Boolean expanded) {
		this.expanded = expanded;
	}
}
