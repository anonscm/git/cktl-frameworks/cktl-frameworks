package org.cocktail.fwkcktlajaxwebext.serveur.component.treetable;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

import er.extensions.appserver.ERXWOContext;

public class CktlAjaxTreeTableHeaderCell extends WOComponent {

	private static final long serialVersionUID = -7278292515264254773L;

	private static final String BINDING_CLASS = "class";
	private static final String BINDING_STYLE = "style";
	private static final String BINDING_WIDTH = "width";
	private static final String BINDING_TITLE = "title";
	private static final String BINDING_ROWSPAN = "rowspan";
	private static final String BINDING_COLSPAN = "colspan";

	public CktlAjaxTreeTableHeaderCell(WOContext context) {
		super(context);
	}

	public CktlAjaxTreeTable currentTreeTable() {
		return (CktlAjaxTreeTable) ERXWOContext.contextDictionary().objectForKey(CktlAjaxTreeTable.CURRENT_TREE_TABLE);
	}

	public String otherTagString() {
		String otherTagString = "";

		if (hasBinding(BINDING_CLASS)) {
			otherTagString += " class = '" + valueForStringBinding(BINDING_CLASS, "") + "'";
		}

		if (hasBinding(BINDING_TITLE)) {
			otherTagString += " title = '" + valueForStringBinding(BINDING_TITLE, "") + "'";
		}

		if (hasBinding(BINDING_STYLE) && hasBinding(BINDING_WIDTH)) {
			otherTagString += " style = '" + valueForStringBinding(BINDING_STYLE, "") + "; width:" + getWidth() + "'";
		} else if (hasBinding(BINDING_STYLE)) {
			otherTagString += " style = '" + valueForStringBinding(BINDING_STYLE, "") + "'";
		} else if (hasBinding(BINDING_WIDTH)) {
			otherTagString += " style = 'width:" + getWidth() + "'";
		}
		
		return otherTagString;
	}
	
	public String getWidth() {
		return valueForStringBinding(BINDING_WIDTH, "");
	}
	
	public void setWidth(String width) {
		setValueForBinding(width, BINDING_CLASS);
	}
	
	public String getRowspan() {
		return valueForStringBinding(BINDING_ROWSPAN, "");
	}
	
	public void setRowspan(String rowspan) {
		//setValueForBinding(rowspan, BINDING_ROWSPAN);
	}
	
	public String getColspan() {
		return valueForStringBinding(BINDING_COLSPAN, "");
	}
	
	public void setColspan(String colspan) {
		//setValueForBinding(colspan, BINDING_COLSPAN);
	}
	
	public String getStyle() {
		return valueForStringBinding(BINDING_STYLE, "");
	}
	
	public void setStyle(String style) {
	}
	
	public String _class() {
		return valueForStringBinding(BINDING_CLASS, "");
	}
	
	public void setClass(String classe) {
	}
	
	public String getTitle() {
		return valueForStringBinding(BINDING_TITLE, "");
	}
	
	public void setTitle(String title) {
	}
}