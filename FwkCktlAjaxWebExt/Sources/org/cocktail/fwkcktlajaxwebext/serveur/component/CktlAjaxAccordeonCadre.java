package org.cocktail.fwkcktlajaxwebext.serveur.component;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxUpdateContainer;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;


public class CktlAjaxAccordeonCadre extends CktlAjaxWOComponent {

	private static final long serialVersionUID = 1L;

	public static String BINDING_TITRE = "titre";
	public static String BINDING_IMAGE_TITRE = "imageTitre";
	public static String BINDING_TITRELIENOUVERTURECADRE = "titreLienOuvertureCadre";
	public static String BINDING_TITRELIENFERMETURECADRE = "titreLienFermetureCadre";
	public static String BINDING_EXPANDED = "expanded";
	public static String BINDING_DISABLED = "disabled";
	public static String BINDING_VISIBLE = "visible";
	
	private Boolean expanded = null;
	private Boolean disabled = null;
	private Boolean visible = null;
	

	public CktlAjaxAccordeonCadre(WOContext context) {
		super(context);
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		expanded = null;
		disabled = null;
		visible = null;
		
		super.appendToResponse(response, context);
	}

	public String ajaxAccordeonCadreContainerId() {
		return getComponentId() + "_ajaxAccordeonCadreContainerId";
	}

	public String contentId() {
		return getComponentId() + "_contentId";
	}

	public String titre() {
		return valueForStringBinding(BINDING_TITRE, "");
	}
	
	public String imageTitre() {
		return valueForStringBinding(BINDING_IMAGE_TITRE, null);
	}
	
	public String titreLienOuvertureCadre() {
		return valueForStringBinding(BINDING_TITRELIENOUVERTURECADRE, "");
	}
	
	public String titreLienFermetureCadre() {
		return valueForStringBinding(BINDING_TITRELIENFERMETURECADRE, "");
	}
	
	public Boolean isExpanded() {
		if(expanded==null) {  
			expanded = valueForBooleanBinding(BINDING_EXPANDED, true);  
		}  

		return expanded;
	}
	public void setExpanded(Boolean expanded) {
		this.expanded = expanded;
	}

	public Boolean isDisabled() {
		if(disabled==null) {  
			disabled = valueForBooleanBinding(BINDING_DISABLED, false);  
		}  

		return disabled; 
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public Boolean isVisible() {
		if(visible==null) {  
			visible = valueForBooleanBinding(BINDING_VISIBLE, true);  
		}
		
		return visible;  
	}
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	
	public WOActionResults ouvrirCadre() {
    	setExpanded(true);

    	AjaxUpdateContainer.updateContainerWithID(ajaxAccordeonCadreContainerId(), context());
    	
    	return null;
    }
    
    public WOActionResults fermerCadre() {
    	setExpanded(false);

    	AjaxUpdateContainer.updateContainerWithID(ajaxAccordeonCadreContainerId(), context());
    	
    	return null;
    }

}