/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.ajax.AjaxUtils;
import er.ajax.CktlAjaxUtils;

public class CktlAjaxButton extends CktlAjaxUpdateLink {

	/**
	 * AjaxUpdateLink sous forme d'un bouton au look google avec une image + un libelle. Beneficie du traitement contre les clics multiples
	 * (desactivable). Genere le code necessaire a l'ouverture de fenetre CktlAjaxWindow via l'id de cette derniere (dialogIDForOpen)
	 * 
	 * @binding action the action to call when the link executes
	 * @binding directActionName the direct action to call when link executes
	 * @binding onLoading JavaScript function to evaluate when the request begins
	 * @binding onComplete JavaScript function to evaluate when the request has finished.
	 * @binding onSuccess JavaScript function to evaluate when the request was successful.
	 * @binding onFailure JavaScript function to evaluate when the request has failed.
	 * @binding onException JavaScript function to evaluate when the request had errors.
	 * @binding evalScripts boolean defining if the container update is expected to be a script.
	 * @binding ignoreActionResponse boolean defining if the action's response should be thrown away (useful when the same action has both Ajax and
	 *          plain links)
	 * @binding onClickBefore if the given function returns true, the onClick is executed. This is to support confirm(..) dialogs.
	 * @binding onClick JS function, called after the click on the client
	 * @binding onClickServer JS returned from the server after the update
	 * @binding updateContainerID the id of the AjaxUpdateContainer to update after performing this action
	 * @binding type Type d'action realisee par le bouton (new, delete, update, ...). Genere une classe css cktl_action_xxxx decrite dans
	 *          CktlCommon.css ou autre afin d'associer une image a l'action
	 * @binding text Libelle du bouton
	 * @binding title title of the button
	 * @binding style css style of the link
	 * @binding class css class of the link
	 * @binding showBusyImage Indique si lors de la request, l'image associee au bouton est remplacee par un gif anime d'attente
	 * @binding isImagePositionIsRight Indique si l'image associee au bouton se situe a droite ou a gauche du libelle
	 * @binding dialogIDForOpen id de la fenetre a ouvrir (composant CktlAjaxWindow) lors du clic sur le bouton
	 * @binding id id of the link
	 * @binding disabled boolean defining if the link renders the tag
	 * @binding asynchronous boolean defining if the update request is sent asynchronously or synchronously, defaults to true
	 * @binding accesskey hot key that should trigger the link (optional) // PROTOTYPE EFFECTS
	 * @binding effect synonym of afterEffect except it always applies to updateContainerID
	 * @binding effectDuration the duration of the effect to apply before // PROTOTYPE EFFECTS
	 * @binding beforeEffect the Scriptaculous effect to apply onSuccess ("highlight", "slideIn", "blindDown", etc);
	 * @binding beforeEffectID the ID of the container to apply the "before" effect to (blank = try nearest container, then try updateContainerID)
	 * @binding beforeEffectDuration the duration of the effect to apply before // PROTOTYPE EFFECTS
	 * @binding afterEffect the Scriptaculous effect to apply onSuccess ("highlight", "slideIn", "blindDown", etc);
	 * @binding afterEffectID the ID of the container to apply the "after" effect to (blank = try nearest container, then try updateContainerID)
	 * @binding afterEffectDuration the duration of the effect to apply before // PROTOTYPE EFFECTS
	 * @binding insertion JavaScript function to evaluate when the update takes place (or effect shortcuts like "Effect.blind", or "Effect.BlindUp")
	 * @binding insertionDuration the duration of the before and after insertion animation (if using insertion)
	 * @binding beforeInsertionDuration the duration of the before insertion animation (if using insertion)
	 * @binding afterInsertionDuration the duration of the after insertion animation (if using insertion)
	 */
	public static final String BINDING_type = "type";
	public static final String BINDING_title = "title";
	public static final String BINDING_disabled = "disabled";
	public static final String BINDING_enabled = "enabled";

	/** Le texte a afficher */
	public static final String BINDING_text = "text";

	/** Indique s'il l'image est situee a droite du label */
	public static final String BINDING_isImagePositionIsRight = "isImagePositionIsRight";

	/** Indique s'il faut afficher l'image busy */
	public static final String BINDING_showBusyImage = "showBusyImage";

	/** Indique l'id de la fenetre à ouvrir */
	public static final String BINDING_dialogIDForOpen = "dialogIDForOpen";

	/** Indique l'url de la page a afficher ds la fenetre à ouvrir */
	public static final String BINDING_href = "href";

	public CktlAjaxButton(String name, NSDictionary associations, WOElement children) {
		super(name, associations, children);
	}

	public void appendToResponse(WOResponse response, WOContext context) {
		WOComponent component = context.component();

		boolean disabled = disabled(component);

		if (disabled) {
			response.appendContentString("<");
			response.appendContentString("span");
			response.appendContentString(" ");
			appendTagAttributeToResponse(response, "title", valueForBinding("title", component));
			appendTagAttributeToResponse(response, "id", getId(context));
			appendTagAttributeToResponse(response, "style", "cursor:pointer;margin:0px 2px;" + valueForBinding("style", component));
			response.appendContentString(">");

			appendButtonLookToResponse(response, component);

			response.appendContentString("</");
			response.appendContentString("span");
			response.appendContentString(">");
			AjaxUtils.appendScriptHeader(response);
			response.appendContentString(jsOnAfterDisplayed(component));
			AjaxUtils.appendScriptFooter(response);
		}
		else {
			response.appendContentString("<");
			response.appendContentString("a");
			response.appendContentString(" ");
			appendTagAttributeToResponse(response, "href", "javascript:void(0);");
			appendTagAttributeToResponse(response, "onclick", onClick(context, false));
			appendTagAttributeToResponse(response, "title", valueForBinding("title", component));
			appendTagAttributeToResponse(response, "value", valueForBinding("value", component));
			appendTagAttributeToResponse(response, "class", "cktl_button");
			appendTagAttributeToResponse(response, "style", valueForBinding("style", component));
			appendTagAttributeToResponse(response, "id", getId(context));
			appendTagAttributeToResponse(response, "accesskey", valueForBinding("accesskey", component));
			response.appendContentString(">");
			appendButtonLookToResponse(response, component);
			appendChildrenToResponse(response, context);
			response.appendContentString("</");
			response.appendContentString("a");
			response.appendContentString(">");
			AjaxUtils.appendScriptHeader(response);
			response.appendContentString(jsOnAfterDisplayed(component));
			AjaxUtils.appendScriptFooter(response);
		}

	}

	@Override
	public String onClick(WOContext context, boolean generateFunctionWrapper) {
		String res = "return false;";
		WOComponent component = context.component();
		boolean disabled = disabled(component);
		if (!disabled) {
			res = super.onClick(context, generateFunctionWrapper);
			Boolean showBusyImage = (Boolean) valueForBinding(BINDING_showBusyImage, Boolean.FALSE, context.component());

			if (showBusyImage.booleanValue()) {
				res = "$('" + getButtonBaseImageId(context.component()) + "').addClassName('cktl_action_busy');" + res;
			}

			if (hasBinding("onClickBefore") && showBusyImage.booleanValue()) {
				res += " else {" + "$('" + getButtonBaseImageId(context.component()) + "').removeClassName('cktl_action_busy');};";
			}
		}

		return res;
	}

	@Override
	protected NSMutableDictionary createAjaxOptions(WOComponent component) {
		NSMutableDictionary res = super.createAjaxOptions(component);

		//ROD : gestion du dialogIDForOpen remontée dans CktlAjaxUpdateLink
		//		if (hasBinding(BINDING_dialogIDForOpen)) {
		//			String dialogID = (String) valueForBinding(BINDING_dialogIDForOpen, component);
		//			String onSuccessAddition = "openCAW_" + dialogID + "($('" + getId(component.context()) + "').readAttribute('title')";
		//
		//			if (hasBinding(BINDING_href)) {
		//				String href = (String) valueForBinding(BINDING_href, component);
		//				onSuccessAddition += ",'" + href + "'";
		//			}
		//			onSuccessAddition += ");return false;";
		//			String onSuccess = (String) res.valueForKey("onSuccess");
		//			if (onSuccess != null) {
		//				int i = onSuccess.indexOf("}");
		//				if (i > -1) {
		//					onSuccess = onSuccess.substring(0, i) + onSuccessAddition + onSuccess.substring(i);
		//				}
		//				else {
		//					//Erreur
		//					onSuccess = null;
		//				}
		//			}
		//			else {
		//				onSuccess = "function() {" + onSuccessAddition + "}";
		//			}
		//			res.takeValueForKey(onSuccess, "onSuccess");
		//		}

		Boolean showBusyImage = (Boolean) valueForBinding(BINDING_showBusyImage, Boolean.FALSE, component);

		if (showBusyImage.booleanValue()) {
			String onCompleteAddition = "var btn = $('" + getButtonBaseImageId(component) + "');if (btn != null) { btn.removeClassName('cktl_action_busy'); };";
			String onComplete = (String) res.valueForKey("onComplete");
			if (onComplete != null) {
				int i = onComplete.indexOf("}");
				if (i > -1) {
					onComplete = onComplete.substring(0, i) + onCompleteAddition + onComplete.substring(i);
				}
				else {
					//Erreur
					onComplete = null;
				}
			}
			else {
				onComplete = "function() {" + onCompleteAddition + "}";
			}
			res.takeValueForKey(onComplete, "onComplete");
			//			String onFailureAddition = "$('"+getButtonBaseImageId(component)+"').removeClassName('cktl_action_busy');";
			//			String onFailure = (String) res.valueForKey("onFailure");
			//			if (onFailure != null) {
			//				int i = onFailure.indexOf("}"); 
			//				if ( i >-1 ) {
			//					onFailure = onFailure.substring(0, i) + onFailureAddition + onFailure.substring(i);
			//				}
			//				else {
			//					//Erreur
			//					onFailure = null;
			//				}
			//			} else {
			//				onFailure = "function() {"+onFailureAddition+"}";
			//			}
			//			res.takeValueForKey(onFailure, "onFailure");

		}

		return res;
	}

	private void appendButtonLookToResponse(WOResponse response, WOComponent component) {
		response.appendContentString("<div");
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_inline_block cktl_button cktl_button_base");
		response.appendContentString(">");

		response.appendContentString("<div");
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_inline_block cktl_button_base_outer_box");
		response.appendContentString(">");

		response.appendContentString("<div");
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_inline_block cktl_button_base_inner_box");
		response.appendContentString(">");

		response.appendContentString("<div");
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_button_base_pos");
		response.appendContentString(">");

		response.appendContentString("<div");
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_button_base_top_shadow");
		response.appendContentString(">");
		response.appendContentString("&nbsp;");
		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("<div");
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "id", getButtonId(component));
		appendTagAttributeToResponse(response, "class", "cktl_button_base_content");
		response.appendContentString(">");

		if (hasBinding(BINDING_type) || hasBinding(BINDING_showBusyImage)) {
			if (isImagePositionIsRight(component)) {
				response.appendContentString("<");
				response.appendContentString("span");
				response.appendContentString(" ");
				response.appendContentString(">");
				if (getText(component) != null) {
					response.appendContentString(getText(component) + "&nbsp;");
				}
				response.appendContentString("</");
				response.appendContentString("span");
				response.appendContentString(">");
				response.appendContentString("<span");
				response.appendContentString(" ");
				appendTagAttributeToResponse(response, "id", getButtonBaseImageId(component));
				appendTagAttributeToResponse(response, "class", getButtonBaseImageClass(component));
				response.appendContentString(">");
				response.appendContentString("</");
				response.appendContentString("span");
				response.appendContentString(">");
			}
			else {
				response.appendContentString("<span");
				response.appendContentString(" ");
				appendTagAttributeToResponse(response, "id", getButtonBaseImageId(component));
				appendTagAttributeToResponse(response, "class", getButtonBaseImageClass(component));
				response.appendContentString(">");
				response.appendContentString("</");
				response.appendContentString("span");
				response.appendContentString(">");
				response.appendContentString("<");
				response.appendContentString("span");
				response.appendContentString(" ");
				response.appendContentString(">");
				if (getText(component) != null) {
					response.appendContentString("&nbsp;" + getText(component));
				}
				response.appendContentString("</");
				response.appendContentString("span");
				response.appendContentString(">");
			}
		}
		else {
			response.appendContentString("<");
			response.appendContentString("span");
			response.appendContentString(" ");
			response.appendContentString(">");
			if (getText(component) != null) {
				response.appendContentString(getText(component));
			}
			response.appendContentString("</");
			response.appendContentString("span");
			response.appendContentString(">");
		}
		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

	}

	private boolean disabled(WOComponent component) {
		if (hasBinding(BINDING_disabled)) {
			return booleanValueForBinding(BINDING_disabled, false, component);
		}
		else if (hasBinding(BINDING_enabled)) {
			return !booleanValueForBinding(BINDING_enabled, true, component);
		}
		else {
			return false;
		}
	}

	private boolean isImagePositionIsRight(WOComponent component) {
		return hasBinding(BINDING_isImagePositionIsRight) && booleanValueForBinding(BINDING_isImagePositionIsRight, false, component);
	}

	public String getButtonId(WOComponent component) {
		return getId(component.context()) + "_btn";
	}

	public String getButtonBaseImageId(WOComponent component) {
		return getId(component.context()) + "_btn_base_image";
	}

	public String getButtonBaseImageClass(WOComponent component) {
		String buttonBaseImageClass = "cktl_action_button ";

		if (hasBinding(BINDING_type)) {
			buttonBaseImageClass += "cktl_action_" + stringValueForBinding(BINDING_type, component) + " ";
		}
		buttonBaseImageClass += "cktl_button_icon cktl_inline_block";

		return buttonBaseImageClass;
	}

	public String getText(WOComponent component) {
		if (hasBinding(BINDING_text)) {
			return (String) stringValueForBinding(BINDING_text, component);
		}
		return null;
	}

	public String jsOnAfterDisplayed(WOComponent component) {
		if (disabled(component)) {
			return "var btn = $('" + getId(component.context()) + "');if (btn != null) {btn.setOpacity(0.5);btn.firstDescendant().addClassName('cktl_button_disabled');}";
		}
		else {
			return "var btn = $('" + getId(component.context()) + "');if (btn != null) {btn.setOpacity(1);}";
		}
	}

	protected void addRequiredWebResources(WOResponse res, WOContext context) {
		CktlAjaxUtils.addScriptResourceInHead(context, res, "prototype.js");
		CktlAjaxUtils.addScriptResourceInHead(context, res, "effects.js");
		CktlAjaxUtils.addScriptResourceInHead(context, res, "wonder.js");
		CktlAjaxUtils.addStylesheetResourceInHead(context, res, "FwkCktlThemes", "css/CktlCommon.css");
	}

}
