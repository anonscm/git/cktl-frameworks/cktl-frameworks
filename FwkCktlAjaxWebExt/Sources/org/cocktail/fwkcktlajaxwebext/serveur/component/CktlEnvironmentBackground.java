package org.cocktail.fwkcktlajaxwebext.serveur.component;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOContext;
import com.woinject.Current;
import com.woinject.WORequestScoped;

import er.ajax.AjaxUtils;
import er.extensions.appserver.ERXResponseRewriter;
import er.extensions.appserver.ERXResponseRewriter.TagMissingBehavior;

/**
 * Permet d'ajouter les attributs CSS pour avoir un background
 */
@Deprecated
@WORequestScoped
public class CktlEnvironmentBackground {

	@Inject
	@Current
	private WOContext context;

	private Boolean added = false;

	private static final String CSS_GLOBAL_BG = "<style type=\"text/css\">\nbody{\nbackground-image:url($1);\n}</style>";

	public void setContext(WOContext context) {
		this.context = context;
	}

	/**
	 * Cette méthode ajoute le background pour la page
	 */
	public void add() {
		if (!added && !AjaxUtils.isAjaxRequest(context.request())) {

			added = true;

			ERXResponseRewriter.insertInResponseBeforeHead(context.response(), context, StringUtils.replace(CSS_GLOBAL_BG, "$1", getMainBackgroundImage()), TagMissingBehavior.Skip);

		}
	}

	public String getMainBackgroundImage() {
		return CktlWebApplication.application().getImageURL("HTML_MAIN_BG_IMAGE", "fond.jpg");
	}
}
