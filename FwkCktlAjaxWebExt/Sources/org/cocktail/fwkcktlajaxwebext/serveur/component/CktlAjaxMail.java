package org.cocktail.fwkcktlajaxwebext.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxMailMessage;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Composant permettant d'inserer un bouton ouvrant une fenetre pour rédiger et envoyer un mail
 * @binding emailFrom : email de la personne qui envoie le message
 * @binding emailTo : Un string contenant des emails separés par une virgule
 * @binding emailToListeRepartStructure : Une liste de repart structure à qui sera envoyé le mail
 */
public class CktlAjaxMail extends CktlAjaxWOComponent {
    
    private static final long serialVersionUID = 3879161074654907277L;
    private static final String BINDING_ENABLED = "enabled";
    private static final String BINDING_EDITING_CONTEXT = "editingContext";
    private static final String BINDING_FROM = "emailFrom";
    private static final String BINDING_TO = "emailTo";
    private static final String BINDING_TO_LISTE_REPART_STRUCTURE = "emailToListeRepartStructure";
    
    private CktlAjaxMailMessage cktlAjaxMailMessage;
	private CktlAjaxDestinatairesListeSelector.DestinataireListe destinatairesForMail = null;

	/**
	 * Contructeur
	 * @param context contexte
	 */
	public CktlAjaxMail(WOContext context) {
        super(context);
    }
	
	/**
	 * @return id de la fenetre
	 */
	public String mailDialogId() {
		return "MailDiag_" + getComponentId();
	}
	
	/**
	 * Action lancée à l'ouverture de la fenetre (initialisation des données)
	 * @return null
	 */
	public WOActionResults openMailDialogWindow() {
    	
		cktlAjaxMailMessage = new CktlAjaxMailMessage();
		cktlAjaxMailMessage.setDestinataires(getDestinatairesForMail());
		cktlAjaxMailMessage.addFroms(getEmailFrom());

		return null;
	}
	
	/**
	 * @return la liste des destinataires du mail
	 */
	public CktlAjaxDestinatairesListeSelector.DestinataireListe getDestinatairesForMail() {
		destinatairesForMail = null;
		if (destinatairesForMail == null && (hasBinding(BINDING_TO) || hasBinding(BINDING_TO_LISTE_REPART_STRUCTURE))) {
			destinatairesForMail = new CktlAjaxDestinatairesListeSelector.DestinataireListe();
			
			if (hasBinding(BINDING_TO)) {
				NSArray<String> listeMail = NSArray.componentsSeparatedByString(valueForStringBinding(BINDING_TO, ""), ",");
				for (String mail : listeMail) {
					destinatairesForMail.addObject(CktlAjaxDestinatairesListeSelector.DestinataireListeElt.DESTINATAIRE_OPTION_TO, "", mail.trim());
				}
			} else {
				@SuppressWarnings("unchecked")
                NSArray<EORepartStructure> membresSelectionnes = (NSArray<EORepartStructure>) valueForBinding(BINDING_TO_LISTE_REPART_STRUCTURE);
				for (EORepartStructure membre : membresSelectionnes) {
					IPersonne ipersonne = membre.toPersonneElt();
					ApplicationUser appUser = new ApplicationUser((EOEditingContext) valueForBinding(BINDING_EDITING_CONTEXT), ipersonne.persId());
					NSArray<String> emails = appUser.getEmails();
					if (emails.count() > 0) {
						destinatairesForMail.addObjectIfAdresseNotPresent(CktlAjaxDestinatairesListeSelector.DestinataireListeElt.DESTINATAIRE_OPTION_TO,
						        ipersonne.getNomCompletAffichage(), (String) emails.objectAtIndex(0));
					}
				}
			}
		}
		return destinatairesForMail;
	}
	
	public String getEmailFrom() {
		return valueForStringBinding(BINDING_FROM, "");
	}
    
	/**
	 * action lancée apres la fermeture de la fenetre par le bouton annuler
	 * @return null
	 */
    public WOActionResults onSendMailAnnuler() {
		destinatairesForMail = null;
		return null;
	}

    /**
	 * action lancée apres la fermeture de la fenetre par le bouton valider
	 * @return null
	 */
	public WOActionResults onSendMailValider() {
		destinatairesForMail = null;
		return null;
	}
    
    /**
	 * @return the cktlAjaxMailMessage
	 */
	public CktlAjaxMailMessage getCktlAjaxMailMessage() {
		if (cktlAjaxMailMessage == null) {
			cktlAjaxMailMessage = new CktlAjaxMailMessage();
		}

		return cktlAjaxMailMessage;
	}

	/**
	 * @param cktlAjaxMailMessage the cktlAjaxMailMessage to set
	 */
	public void setCktlAjaxMailMessage(CktlAjaxMailMessage cktlAjaxMailMessage) {
		this.cktlAjaxMailMessage = cktlAjaxMailMessage;
	}
	
	public Boolean getEnabled() {
		return valueForBooleanBinding(BINDING_ENABLED, true);
	}
}