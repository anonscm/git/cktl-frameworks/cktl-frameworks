package org.cocktail.fwkcktlajaxwebext.serveur.component;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxComponent;

/**
 * Composant affichant un chargement lors d'une requete ajax
 *
 * @binding onCreate (optional) the function to execute when the request starts
 * @binding onComplete (optional) the function to execute when the request ends
 * @binding watchContainerID (optional) if set, the other bindings will only apply when this container ID is being
 *          updated, which provides for per-element busy controls
 * @binding class (optional) if bound, you can provide a custom style for the generated busy image div 
 * @binding style (optional) if bound, you can provide a custom style for the generated busy image div 
 */
public class CktlAjaxLoader extends AjaxComponent {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructeur
	 * @param context contexte
	 */
	public CktlAjaxLoader(WOContext context) {
        super(context);
    }
	
	public boolean isStateless() {
		return true;
	}

	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	protected void addRequiredWebResources(WOResponse res) {
		addScriptResourceInHead(res, "prototype.js");
		addScriptResourceInHead(res, "effects.js");
		addScriptResourceInHead(res, "wonder.js");
	}

	public String style() {
		String style = "display:none; position:absolute; height: 100%; width: 100%; top: 0; text-align: center; background: rgba( 255, 255, 255, .8 )";
		style += (String) valueForBinding("style", "");
		return style;
	}

	public String onCreate() {
		return (String) valueForBinding("onCreate", "null");
	}

	public String onComplete() {
		return (String) valueForBinding("onComplete", "null");
	}

	public WOActionResults handleRequest(WORequest request, WOContext context) {
		return null;
	}
}