/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxComponent;
import er.extensions.appserver.ERXWOContext;

/**
 * 
 * Composant d'affichage d'un simple menu déroulant lors
 * d'un click sur le bouton.
 * Le contenu du panneau déroulant est à mettre dans le component content.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class CktlAjaxSimpleMenu extends AjaxComponent {

    private static final long serialVersionUID = -4046517933144079800L;
    private String dropDownId;
    private String dropDownButtonId;
	private boolean disabled;

    public CktlAjaxSimpleMenu(WOContext context) {
        super(context);
    }

    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }

    @Override
    public boolean isStateless() {
        return true;
    }

    @Override
    public void reset() {
        super.reset();
        dropDownButtonId = null;
        dropDownId = null;
        disabled = false;
    }
    
    public String dropDownId() {
        if (dropDownId == null) {
            dropDownId = "DD_" +ERXWOContext.safeIdentifierName(context(), false);
        }
        return dropDownId;
    }

    public String dropDownButtonId() {
        if (dropDownButtonId == null) {
            dropDownButtonId = "DDButton_" + ERXWOContext.safeIdentifierName(context(), false);
        }
        return dropDownButtonId;
    }

    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
        super.appendToResponse(response, context);
        if (!disabled) {
	        String scriptObs = "<script>var menu = new SimpleMenu('"+ dropDownButtonId() + "','" + dropDownId() + "');menu.addSimpleMenuObservers();</script>";
	        response.appendContentString(scriptObs);
        }
    }

    @Override
    protected void addRequiredWebResources(WOResponse response) {
        addScriptResourceInHead(response, "FwkCktlAjaxWebExt.framework", "scripts/cktlajaxsimplemenu.js");
    }

    @Override
    public WOActionResults handleRequest(WORequest request, WOContext context) {
        return null;
    }

	/**
	 * @return the disabled
	 */
	public boolean isDisabled() {
		disabled = booleanValueForBinding("disabled", false);
		return disabled;
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

}