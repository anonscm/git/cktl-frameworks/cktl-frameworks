/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;

/**
 * Composant pour afficher automatiquement une liste de selection suite à la saisie par l'utilisateur d'une value dans un champ. Alternative a
 * l'autoComplete. La proposition d'une liste se fait une fois que le champ perd le focus, et non pendant la saisie.
 * 
 * @binding noSelectionString
 * @binding selection Recuperation
 * @binding value Valeur a saisir dans le champ (par exemple adresse.ville)
 * @binding laListe Doit renvoyer une liste d'item filtres en fonction de value (par exemple listeCommunes())
 * @binding unItem Une variable pour que le composant puisse generer la liste (par exemple unCommune)
 * @binding fieldSize Taille du champ texte
 * @binding fieldClass Classe css du champ texte
 * @binding displayList Boolean qui doit renvoyer false si pour une raison quelconque il ne faut pas afficher la liste
 * @binding displayString Chaine de racatere a afficher pour reprsenter un element dans la liste (par exemple unCommune.lcCom)
 * @binding updateContainerID Container a mettre a jour apres post de la value ou de la selection (typiquement le container du formulaire)
 * @binding nextElementID ID de l'element sur lequel se postionner une fois la selection effectuee par l'utilisateur (pour la gestion des tabs)
 * @binding valueKeyPath Facultatif. Si specifie, le composant essaiera de mettre a jour lui-meme la value a la suite d'une selection dans la liste.
 *          (par exemple "lcComm", pour que la value soit automatiquement affectee a unCommune.lcComm)
 * @binding defaultValue Valeur du textfield a afficher quand le champ n'est pas rempli
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlAjaxAutoSelect extends CktlAjaxWOComponent {

	private static final long serialVersionUID = 1L;

	public static final String BINDING_noSelectionString = "noSelectionString";

	public static final String BINDING_selection = "selection";

	/** Valeur a saisir dans le champ (par exemple adresse.ville) */
	public static final String BINDING_value = "value";

	/** Doit renvoyer une liste d'item filtres en fonction de value (par exemple listeCommunes()) */
	public static final String BINDING_laListe = "laListe";

	/** Une variable pour que le composant puisse generer la liste (par exemple unCommune) */
	public static final String BINDING_unItem = "unItem";

	/** Taille du champ texte */
	public static final String BINDING_fieldSize = "fieldSize";

	/** Classe css du champ texte */
	public static final String BINDING_fieldClass = "fieldClass";

	/** Boolean qui doit renvoyer false si pour une raison quelconque il ne faut pas afficher la liste */
	public static final String BINDING_displayList = "displayList";

	/** Chaine de racatere a afficher pour reprsenter un element dans la liste (par exemple unCommune.lcCom) */
	public static final String BINDING_displayString = "displayString";

	/** Container a mettre a jour apres post de la value ou de la selection (typiquement le container du formulaire) */
	public static final String BINDING_updateContainerID = "updateContainerID";

	/** ID de l'element sur lequel se postionner une fois la selection effectuee par l'utilisateur (pour la gestion des tabs) */
	public static final String BINDING_nextElementID = "nextElementID";

	/**
	 * Facultatif. Si specifie, le composant essaiera de mettre a jour lui-meme la value a la suite d'une selection dans la liste. (par exemple
	 * "lcComm", pour que la value soit automatiquement affectee a unCommune.lcComm)
	 */
	public static final String BINDING_valueKeyPath = "valueKeyPath";

	/** Valeur du textfield a afficher quand le champ n'est pas rempli */
	public static final String BINDING_defaultValue = "defaultValue";

	/** Valeur du textfield a afficher quand le champ n'est pas rempli */
	public static final String BINDING_disabled = "disabled";

	
	private String listeId = null;
	private String textFieldId = null;
	private NSArray laListe = NSArray.EmptyArray;
	private Object cacheValue = null;
	private Boolean showList = Boolean.FALSE;

	public CktlAjaxAutoSelect(WOContext context) {
		super(context);
	}

	public String noSelectionString() {
		if (valueForBinding(BINDING_noSelectionString) == null) {
			return "Selectionnez une valeur";
		}
		return (String) valueForBinding(BINDING_noSelectionString);
	}

	public String getListeId() {
		if (listeId == null) {
			listeId = getComponentId() + "_list";
		}
		return listeId;
	}

	public String getFieldId() {
		if (textFieldId == null) {
			textFieldId = getComponentId() + "_field";
		}
		return textFieldId;
	}

	public NSArray getLaListe() {

		if (laListe.count() > 1) {
			context().response().appendContentString("<script>if (document.getElementById('" + getListeId() + "') != null) {document.getElementById('" + getListeId() + "').focus();}</script>");
		}
		//Si la valeur a changee, on met a jour la liste.
		//if (cacheValue != null) {
		//		laListe = (NSArray) valueForBinding(BINDING_laListe);
		//
		//		if (laListe == null) {
		//			laListe = NSArray.EmptyArray;
		//		}
		//
		//		if (laListe.count() == 1) {
		//			setSelection(laListe.objectAtIndex(0));
		//			showList = Boolean.FALSE;
		//		}
		//
		//		if (laListe.count() > 1) {
		//			context().response().appendContentString("<script>if (document.getElementById('" + getListeId() + "') != null) {document.getElementById('" + getListeId() + "').focus();}</script>");
		//		}
		//		//}

		return laListe;
		//		
		//		
		//		
		//		if (cacheValue == null || !cacheValue.equals(value())) {
		//			cacheValue = value();
		//			laListe = (NSArray) valueForBinding(BINDING_laListe);
		//
		//			if (laListe == null) {
		//				laListe = NSArray.EmptyArray;
		//			}
		//
		//			if (laListe.count() == 1) {
		//				setSelection(laListe.objectAtIndex(0));
		//			}
		//
		//			if (laListe.count() > 1) {
		//				context().response().appendContentString("<script>if (document.getElementById('" + getListeId() + "') != null) {document.getElementById('" + getListeId() + "').focus();}</script>");
		//			}
		//		}
		//		return laListe;
		//		//Si la valeur a changee, on met a jour la liste.
		//		if (cacheValue == null || !cacheValue.equals(value())) {
		//			cacheValue = value();
		//			laListe = (NSArray) valueForBinding(BINDING_laListe);
		//			
		//			if (laListe == null) {
		//				laListe = NSArray.EmptyArray;
		//			}
		//			
		//			if (laListe.count() == 1) {
		//				setSelection(laListe.objectAtIndex(0));
		//			}
		//			
		//			if (laListe.count() > 1) {
		//				context().response().appendContentString("<script>if (document.getElementById('" + getListeId() + "') != null) {document.getElementById('" + getListeId() + "').focus();}</script>");
		//			}
		//		}
		//		return laListe;
	}

	public Object getSelection() {
		return valueForBinding(BINDING_selection);
	}

	public void setSelection(Object selection) {
		setValueForBinding(selection, BINDING_selection);

		if (valueKeyPath() != null) {
			if (selection == null) {
				setValueForBinding(null, BINDING_value);
			}
			else {
				//cacheValue = ((NSKeyValueCoding) selection).valueForKey(valueKeyPath());
				setValueForBinding(((NSKeyValueCoding) selection).valueForKey(valueKeyPath()), BINDING_value);
			}
		}

		// la liste doit etre videe apres une selection
		laListe = NSArray.EmptyArray;
		cacheValue = null;

		//a la fin de la selection on se positionne sur le champ texte
		String nextId = (nextElementID() != null ? nextElementID() : getFieldId());
		context().response().appendContentString("<script>if (document.getElementById('" + nextId + "') != null) {document.getElementById('" + nextId + "').focus();}</script>");
	}

	public Object unItem() {
		return valueForBinding(BINDING_unItem);
	}

	public void setUnItem(Object item) {
		setValueForBinding(item, BINDING_unItem);
	}

	public Object value() {
		Object res = valueForBinding(BINDING_value);
		if (res == null) {
			if (getSelection() != null && valueKeyPath() != null) {
				res = ((NSKeyValueCoding) getSelection()).valueForKey(valueKeyPath());
			}
		}
		if (res == null) {
			res = valueForBinding(BINDING_defaultValue);
		}

		return res;
	}

	public void setValue(Object value) {
		cacheValue = value;
		setValueForBinding(null, BINDING_selection);

		if (value != null && value.equals(defaultValue())) {
			value = null;
		}
		setValueForBinding(value, BINDING_value);
		if (value == null) {
			setSelection(null);
		}

		//Si la valeur a changee, on met a jour la liste.
		//if (cacheValue != null) {
		laListe = (NSArray) valueForBinding(BINDING_laListe);

		showList = Boolean.TRUE;
		if (laListe == null) {
			laListe = NSArray.EmptyArray;
		}
		else if (laListe.count() == 1) {
			setSelection(laListe.objectAtIndex(0));
			showList = Boolean.FALSE;
		}

	}

	public Boolean displayList() {
		if (valueForBinding(BINDING_displayList) == null) {
			return Boolean.TRUE;
		}
		return (Boolean) valueForBinding(BINDING_displayList);
	}

	//	public Boolean showList() {
	//		return (displayList().booleanValue() && getLaListe().count() > 1);
	//	}

	public String updateContainerID() {
		return (String) valueForBinding(BINDING_updateContainerID);
	}

	public String nextElementID() {
		return (String) valueForBinding(BINDING_nextElementID);
	}

	public String valueKeyPath() {
		return (String) valueForBinding(BINDING_valueKeyPath);
	}

	public String defaultValue() {
		return (String) valueForBinding(BINDING_defaultValue);
	}
	
	public Boolean disabled() {
		if (valueForBinding(BINDING_disabled) == null) {
			return Boolean.FALSE;
		}
		return (Boolean) valueForBinding(BINDING_disabled);
	}

	public Boolean showList() {
		return showList && displayList() && !disabled();
	}

	public WOActionResults onTextChanged() {
		return null;
	}

	public WOActionResults onPopupChanged() {
		showList = Boolean.FALSE;
		return null;
	}

	public WOActionResults resetAll() {
		laListe = null;
		setValue(null);
		showList = Boolean.FALSE;
		return null;
	}

	public String onCompleteTF1() {
		String onCompleteTF1 = "function () {";
		onCompleteTF1 += updateContainerID();
		onCompleteTF1 += "Update();}";
		return onCompleteTF1;
	}
}
