/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.CktlAjaxUtils;

/**
 * Composant permettant de gérer l'affichage des messages à destination de l'utilisateur. Ce composant part du principe que la pile des messages à
 * affichée est stockée dans "session.uiMessages" par défaut. Vous pouvez spécifier un autre emplacement. Vous disposez des API addMessage,
 * addSimpleErrorMessage, addSimpleInfoMessage, etc. pour ajouter des messages à afficher. Ce composant s'enregistre automatiquement auprès de
 * CoktailAjaxApplication pour être rafraichi à chaque AjaxResponse.<br/>
 * Placez ce composant dans le wrapper de l'application.
 * 
 * @binding id Obligatoire
 * @binding messagesKeyPath Facultatif. Par défaut "session.uiMessages";
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlAjaxUiMessageContainer extends CktlAjaxWOComponent {

	private static final long serialVersionUID = 1L;
	public static String BDG_MESSAGES_KEY_PATH = "messagesKeyPath";

	public CktlAjaxUiMessageContainer(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/jquery/jquery-1.7.1.min.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/jquery/jquery-ui-1.8.4.min.js");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "css/pnotify.custom.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "css/CktlAjaxSimpleMessageList.css");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/jquery/pnotify.custom.js");
		CktlAjaxUtils.addScriptCodeInHead(response, context, "jQuery.noConflict();");
		((CocktailAjaxSession) session()).registerUpdateContainerForEachResponse(getComponentId());
	}

	public String messagesKeyPath() {
		return stringValueForBinding(BDG_MESSAGES_KEY_PATH, "session.uiMessages");
	}

}