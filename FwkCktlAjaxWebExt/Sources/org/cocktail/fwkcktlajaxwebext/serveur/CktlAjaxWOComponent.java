/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur;

import java.util.UUID;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;

import er.ajax.AjaxUtils;
import er.ajax.CktlAjaxUtils;
import er.extensions.components.ERXClickToOpenSupport;
import er.extensions.foundation.ERXProperties;

/**
 * @binding id
 * @binding cssFilename
 * @binding cssFwkName
 * @binding updateContainerID
 * @author rprin
 */
public abstract class CktlAjaxWOComponent extends CktlWebComponent {

	private static final long serialVersionUID = 1L;
	public final static String BINDING_id = "id";
	public static final String BINDING_cssFilename = "cssFilename";
	public static final String BINDING_cssFwkName = "cssFwkName";
	public static final String BINDING_updateContainerID = "updateContainerID";

	private String componentUniqueId = "cktl_" + UUID.randomUUID().toString().replaceAll("-", "_");
	private String componentId;
	private String mainContainerId;

	public static final boolean isClickToOpenEnabled = Boolean.valueOf(System.getProperty("er.component.clickToOpen", "false"));

	/**
	 * Constructeur obligatoire
	 * @param context le contexte
	 */
	public CktlAjaxWOComponent(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		ERXClickToOpenSupport.preProcessResponse(response, context, isClickToOpenEnabled);
		super.appendToResponse(response, context);
		AjaxUtils.addScriptResourceInHead(context, response, "prototype.js");
		AjaxUtils.addScriptResourceInHead(context, response, "effects.js");
		AjaxUtils.addScriptResourceInHead(context, response, "wonder.js");
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlThemes.framework", "scripts/window.js");
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlwonder.js");
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlajaxwebext.js");
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlwindowsopeners.js");

		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/default.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/alert.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/lighting.css");
		ERXClickToOpenSupport.postProcessResponse(getClass(), response, context, isClickToOpenEnabled);
		
		if (ERXProperties.booleanForKeyWithDefault("org.cocktail.global.formulaires.miseenformeautomatique", false)) {
			AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/formulaires.js");
			AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "css/formulaires.css");
			response.appendContentString("<script>FORM.aligner();</script>");
		}
		
	}

	public CocktailAjaxApplication myApp() {
		return (CocktailAjaxApplication) application();
	}

	public CocktailAjaxSession mySession() {
		return (CocktailAjaxSession) session();
	}

	public EOEditingContext edc() {
		return mySession().defaultEditingContext();
	}

	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public WOActionResults doNothing() {
		return null;
	}

	/**
	 * @return l'ID = le binding ID. Si celui-ci n'est pas defini, on en genere un unique.
	 */
	public String getComponentId() {
		if (componentId == null) {
			if (hasBinding(BINDING_id)) {
				componentId = (String) valueForBinding(BINDING_id);
			} else {
				componentId = componentUniqueId;
			}
		}
		return componentId;
	}

	/**
	 * @return Un identifiant pour le container principal du composant si besoin. (id du composant+_mainContainer).
	 */
	public String getMainContainerId() {
		if (mainContainerId == null) {
			mainContainerId = getComponentId() + "_mainContainer";
		}
		return mainContainerId;
	}

	/**
	 * @param s
	 * @return La chaine préparée pour du Javascript (les caracteres ' sont notamment remplacés par des \\').
	 */
	public String jsEncode(String s) {
		return CktlAjaxUtils.jsEncode(s);
	}

	/**
	 * Renvoie une chaine du type<br/>
	 * <code> function() {mainContainerIdUpdate();}</code> <br/>
	 * qui peut etre utilisee par exemple sur le onComplete d'un composant Ajax.
	 */
	public String getJsForMainContainerUpdate() {
		return "function(){" + getMainContainerId() + "Update();}";
	}

	/**
	 * Renvoie une chaine du type<br/>
	 * <code> function() {componentIdUpdate();}</code> <br/>
	 * qui peut etre utilisee par exemple sur le onComplete d'un composant Ajax.
	 */
	public String getJsForComponentUpdate() {
		return "function(){" + getComponentId() + "Update();}";
	}

	/**
	 * Renvoie une chaine du type<br/>
	 * <code> function() {componentIdUpdate();}</code> <br/>
	 * qui peut etre utilisee par exemple sur le onComplete d'un composant Ajax.
	 */
	public String getJsForUpdateContainerUpdate() {
		return "function(){" + updateContainerID() + "Update();}";
	}

	public String updateContainerID() {
		return (String) valueForBinding(BINDING_updateContainerID);
	}
}
