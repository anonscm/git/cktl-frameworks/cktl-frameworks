/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlajaxwebext.serveur;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;

import org.apache.log4j.Logger;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WORequestHandler;
import com.webobjects.appserver.WOResourceManager;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSBundle;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;

import er.extensions.foundation.ERXDictionaryUtilities;
import er.extensions.foundation.ERXProperties;

@Deprecated
/*
 *  Utiliser {@link org.cocktail.fwkcktlwebapp.server.CktlERXStaticResourceRequestHandler} 
 */

public class MyERXStaticResourceRequestHandler extends WORequestHandler {
	
	private static Logger log = Logger.getLogger(MyERXStaticResourceRequestHandler.class);
	private static WOApplication application = WOApplication.application();

	private String _documentRoot;

	private NSMutableDictionary<String, File>wsrPaths = new NSMutableDictionary<String, File>();
	
	public MyERXStaticResourceRequestHandler() {
		_documentRoot = null;
		NSBundle mainBundle = NSBundle.mainBundle();
		URL bundlePathURL = mainBundle.bundlePathURL();
		processAllFiles(new File(bundlePathURL.getPath()+"/Contents/WebServerResources"));
		NSArray frameworks = mainBundle.frameworkBundles();
		Enumeration<NSBundle> enumFrameworks = frameworks.objectEnumerator();
		
		while (enumFrameworks.hasMoreElements()) {
			NSBundle bundle = (NSBundle) enumFrameworks.nextElement();
			File wsrDirectory = new File(bundle.bundlePathURL().getPath()+"/WebServerResources");
			processAllFiles(wsrDirectory);			
		}
	}

	private void processAllFiles(File wsrDirectory) {
		NSArray <File>files = new NSArray<File>(wsrDirectory.listFiles());
		Enumeration<File>enumFiles = files.objectEnumerator();
		while (enumFiles.hasMoreElements()) {
			File file = (File) enumFiles.nextElement();
			if (file.isFile()) {
				if (!file.getName().endsWith(".class")) {
					wsrPaths.setObjectForKey(file, file.getName());
				}
			} else if (file.isDirectory()) {
				processAllFiles(file);
			}
		}
	}
	protected WOResponse _generateResponseForInputStream(InputStream is, int length, String type) {
		WOResponse response = application.createResponseInContext(null);
		if (is != null) {
			if (length != 0) {
				response.setContentStream(is, 50*1024, length);
			}
		} else {
			response.setStatus(404);
		}
		if (type != null) {
			response.setHeader(type, "content-type");
		}
		if(length != 0) {
			response.setHeader("" + length, "content-length");
		}
		return response;
	}

	private String documentRoot() {
		if (_documentRoot == null) {
			_documentRoot = ERXProperties.stringForKey("WODocumentRoot");
			if(_documentRoot == null) {
				NSBundle bundle = NSBundle.bundleForName("JavaWebObjects");
				NSDictionary dict = ERXDictionaryUtilities.dictionaryFromPropertyList("WebServerConfig", bundle);
				_documentRoot = (String) dict.objectForKey("DocumentRoot");
			}
		}
		return _documentRoot;
	}

	public WOResponse handleRequest(WORequest request) {
		WOResponse response = null;
		FileInputStream is = null;
		int length = 0;
		String contentType = null;
		String uri = request.uri();
		if (uri.charAt(0) == '/') {
			WOResourceManager rm = application.resourceManager();
			File file = null;
			String filename = (String)NSArray.componentsSeparatedByString(uri, "/").lastObject();
			file = wsrPaths.objectForKey(filename);
			if (file != null) {
				try {
					length = (int) file.length();
					is = new FileInputStream(file);
					
					contentType = rm.contentTypeForResourceNamed(file.getPath());
					log.debug("Reading file '" + file + "' for uri: " + uri);
				} catch (IOException ex) {
					if (!uri.toLowerCase().endsWith("/favicon.ico")) {
						log.info("Unable to get contents of file '" + file + "' for uri: " + uri);
					}
				}
			}
		} else {
			log.error("Can't fetch relative path: " + uri);
		}
		response = _generateResponseForInputStream(is, length, contentType);
		NSNotificationCenter.defaultCenter().postNotification(WORequestHandler.DidHandleRequestNotification, response);
		response._finalizeInContext(null);
		return response;
	}
}
