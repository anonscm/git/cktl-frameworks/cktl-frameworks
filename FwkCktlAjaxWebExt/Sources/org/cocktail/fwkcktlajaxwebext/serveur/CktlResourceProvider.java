package org.cocktail.fwkcktlajaxwebext.serveur;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

/**
 * 
 * En remplacement du mécanisme précédent d'injection automatique des ressources css dans une page de modal window
 * à partir des ressources du parent :
 * - les pages susceptibles d'accueillir des modal window mode iframe implémentent cette interface
 * - les pages incluses dans les modal window peuvent alors faire appel à injectResources dans leur appendToResponse :
 * 
 *      <pre>
 *      
 *      if (parentComponent != null && parentComponent instanceof CktlResourceProvider) {
 *          ((CktlResourceProvider)parentComponent).injectResources(response, context);
 *      }
 *      </pre>
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public interface CktlResourceProvider {
    
	/**
	 * injecte les resources dans la reponse courante
	 * @param response la reponse
	 * @param context le contexte
	 */
	void injectResources(WOResponse response, WOContext context);
	
}
