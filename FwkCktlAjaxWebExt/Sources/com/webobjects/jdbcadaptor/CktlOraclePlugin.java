package com.webobjects.jdbcadaptor;

public class CktlOraclePlugin extends EROraclePlugIn {

	public CktlOraclePlugin(JDBCAdaptor adaptor) {
		super(adaptor);
	}

	@Override
	public Class defaultExpressionClass() {
		return CktlOracleExpression.class;
	}
	
}
