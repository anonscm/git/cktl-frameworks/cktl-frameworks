package com.webobjects.jdbcadaptor;

import org.cocktail.fwkcktlwebapp.eof.CktlFetchSpecification;

import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOQualifierSQLGeneration;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSSelector;

import er.extensions.eof.ERXKey;

public class CktlOracleExpression extends EROracleExpression {

	private ERXKey<? extends Object> priorKey;
	private ERXKey<? extends Object> key;
	
	private EOQualifier startWithQualifier;
	private NSArray<EOSortOrdering> siblingsSortOrderings;

	private Integer maxLevel;
	
	public CktlOracleExpression(EOEntity eoentity) {
		super(eoentity);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void prepareSelectExpressionWithAttributes(NSArray<EOAttribute> attributes, boolean lock, EOFetchSpecification fetchSpec) {
		
		if (fetchSpec instanceof CktlFetchSpecification) {
			CktlFetchSpecification cktlFetchSpecification = (CktlFetchSpecification) fetchSpec;
			priorKey = cktlFetchSpecification.getPriorKey();
			key = cktlFetchSpecification.getKey();
			startWithQualifier = cktlFetchSpecification.getStartWithQualifier();
			siblingsSortOrderings = cktlFetchSpecification.getSiblingsSortOrderings();
			maxLevel = cktlFetchSpecification.getMaxLevel();
		}
		
		super.prepareSelectExpressionWithAttributes(attributes, lock, fetchSpec);

		
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public String assembleSelectStatementWithAttributes(NSArray attributes, boolean lock, EOQualifier qualifier, NSArray fetchOrder, String selectString, String columnList, String tableList,
			String whereClause, String joinClause, String orderByClause, String lockClause) {
		
		if (startWithQualifier != null && priorKey != null && key != null) {
			StringBuffer sb = new StringBuffer();
			
			sb.append(" START WITH ");
			sb.append(
				EOQualifierSQLGeneration.Support._sqlStringForSQLExpression(startWithQualifier, this)
			);
			
			sb.append(" CONNECT BY PRIOR ");

			EOAttribute priorAttribute = this.entity().anyAttributeNamed(priorKey.key());
			EOAttribute attribute = this.entity().anyAttributeNamed(key.key());

			String priorColumn = generateColumn(priorAttribute);
			String column = generateColumn(attribute);
			

			sb.append(priorColumn + " = " + column);
			sb.append(generateLevelClause(maxLevel));
			
			sb.append(generateOrderSiblingsClause(siblingsSortOrderings));
			
			whereClause += sb.toString();
		}
		
		return super.assembleSelectStatementWithAttributes(attributes, lock, qualifier, fetchOrder, selectString, columnList, tableList, whereClause, joinClause, orderByClause, lockClause);
	}

	String generateLevelClause(Integer maxLevel) {
		StringBuilder builder = new StringBuilder();
		if (maxLevel != null) {
			builder.append(" AND LEVEL <= ")
				   .append(maxLevel.toString());
		}
		return builder.toString();
	}

	private String generateColumn(EOAttribute priorAttribute) {
		return this.useAliases() ? "t0.".concat(sqlStringForSchemaObjectName(priorAttribute.columnName())) : sqlStringForSchemaObjectName(priorAttribute.columnName());
	}

	@SuppressWarnings("rawtypes")
	String generateOrderSiblingsClause(NSArray<EOSortOrdering> siblingsSortOrderings) {
		StringBuilder builder = new StringBuilder();
		if (siblingsSortOrderings != null && !siblingsSortOrderings.isEmpty()) {
			builder.append(" ORDER SIBLINGS BY ");
			NSArray<String> orderSiblingsByClauses = new NSMutableArray<String>();
			for (EOSortOrdering ordering : siblingsSortOrderings) {
				String orderingKey = ordering.key();
				NSSelector orderingSelector = ordering.selector();
				EOAttribute attribute = this.entity().anyAttributeNamed(orderingKey);
				String column = generateColumn(attribute);
				String clause = column + " " + ascOrDesc(orderingSelector);
				orderSiblingsByClauses.add(clause);
			}
			builder.append(orderSiblingsByClauses.componentsJoinedByString(","));
		}
		return builder.toString();
	}

	@SuppressWarnings("rawtypes")
	private String ascOrDesc(NSSelector orderingSelector) {
		String order = null;
		if (EOSortOrdering.CompareAscending == orderingSelector) {
			order = "ASC";
		} else if (EOSortOrdering.CompareDescending == orderingSelector) {
			order = "DESC";
		}
		return order;
	}
	
	
}
