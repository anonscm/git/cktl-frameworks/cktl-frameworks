/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package er.ajax;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.appserver.ERXWOContext;
import er.extensions.appserver.ajax.ERXAjaxApplication;

public class CktlAjaxObserveField extends AjaxDynamicElement {

	public CktlAjaxObserveField(String name, NSDictionary associations, WOElement children) {
		super(name, associations, children);
	}

	/**
	 * Adds all required resources.
	 */
	protected void addRequiredWebResources(WOResponse response, WOContext context) {
		AjaxUtils.addScriptResourceInHead(context, response, "prototype.js");
		AjaxUtils.addScriptResourceInHead(context, response, "wonder.js");
	}

	public NSMutableDictionary createAjaxOptions(WOComponent component) {
		// PROTOTYPE OPTIONS
		NSMutableArray ajaxOptionsArray = new NSMutableArray();
		ajaxOptionsArray.addObject(new AjaxOption("observeFieldFrequency", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("observeDelay", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("onCreate", AjaxOption.SCRIPT));
		ajaxOptionsArray.addObject(new AjaxOption("onLoading", AjaxOption.SCRIPT));
		ajaxOptionsArray.addObject(new AjaxOption("onComplete", AjaxOption.SCRIPT));
		ajaxOptionsArray.addObject(new AjaxOption("onBeforeSubmit", AjaxOption.SCRIPT));
		ajaxOptionsArray.addObject(new AjaxOption("onSuccess", AjaxOption.SCRIPT));
		ajaxOptionsArray.addObject(new AjaxOption("onFailure", AjaxOption.SCRIPT));
		ajaxOptionsArray.addObject(new AjaxOption("onException", AjaxOption.SCRIPT));
		ajaxOptionsArray.addObject(new AjaxOption("insertion", AjaxOption.SCRIPT));
		ajaxOptionsArray.addObject(new AjaxOption("evalScripts", AjaxOption.BOOLEAN));

		NSMutableDictionary options = AjaxOption.createAjaxOptionsDictionary(ajaxOptionsArray, component, associations());
		return options;
	}

	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);

		WOComponent component = context.component();
		String observeFieldID = (String) valueForBinding("observeFieldID", component);
		String formID = (String) valueForBinding("formID", component);
		String updateContainerID = AjaxUpdateContainer.updateContainerID(this, component);
		NSMutableDictionary options = createAjaxOptions(component);
		boolean fullSubmit = booleanValueForBinding("fullSubmit", false, component);
		boolean observeFieldDescendents;
		if (observeFieldID != null) {
			observeFieldDescendents = false;
		}
		else {
			observeFieldDescendents = true;
			observeFieldID = (String) valueForBinding("id", component);
			if (observeFieldID == null) {
				observeFieldID = ERXWOContext.safeIdentifierName(context, false);
			}
			String elementName = (String) valueForBinding("elementName", component);
			if (elementName == null) {
				elementName = "div";
			}
			response.appendContentString("<" + elementName + " id = \"" + observeFieldID + "\">");
			if (hasChildrenElements()) {
				appendChildrenToResponse(response, context);
			}
			response.appendContentString("</" + elementName + ">");
		}
		AjaxUtils.appendScriptHeader(response);
		CktlAjaxObserveField.appendToResponse(response, context, this, observeFieldID, formID, observeFieldDescendents, updateContainerID, fullSubmit, options);
		AjaxUtils.appendScriptFooter(response);
	}

	public static void appendToResponse(WOResponse response, WOContext context, AjaxDynamicElement element, String observeFieldID, String formID, boolean observeDescendentFields, String updateContainerID, boolean fullSubmit, NSMutableDictionary options) {
		WOComponent component = context.component();
		String submitButtonName = nameInContext(context, component, element);
		NSMutableDictionary observerOptions = new NSMutableDictionary();
		if (options != null) {
			observerOptions.addEntriesFromDictionary(options);
		}
		AjaxSubmitButton.fillInAjaxOptions(element, component, submitButtonName, observerOptions);

		if (formID != null) {
			//observerOptions.setObjectForKey(formID, "formID");
			//Rod 15/03/10 : imperatif de mettre le formID entre quote, sinon bug avec docType html 4 ou +
			observerOptions.setObjectForKey("'" + formID + "'", "formID");
		}
		Object observeFieldFrequency = null;
		if(options != null) {
			observeFieldFrequency = options.removeObjectForKey("observeFieldFrequency");
		}
		if (observeDescendentFields) {
			response.appendContentString("ASB.observeDescendentFields");
		}
		else {
			response.appendContentString("ASB.observeField");
		}

		Object observeDelay = null;
		if(options != null) {
			options.removeObjectForKey("observeDelay");
		}
		response.appendContentString("(" + AjaxUtils.quote(updateContainerID) + ", " + AjaxUtils.quote(observeFieldID) + ", " + observeFieldFrequency + ", " + (!fullSubmit) + ", " + observeDelay + ", ");
		AjaxOptions.appendToResponse(observerOptions, response, context);
		response.appendContentString(");");
	}

	public static String nameInContext(WOContext context, WOComponent component, AjaxDynamicElement element) {
		return (String) element.valueForBinding("name", context.elementID(), component);
	}

	public WOActionResults invokeAction(WORequest worequest, WOContext wocontext) {
		WOActionResults result = null;
		WOComponent wocomponent = wocontext.component();
		String nameInContext = nameInContext(wocontext, wocomponent, this);
		boolean shouldHandleRequest = !wocontext._wasActionInvoked() && wocontext._wasFormSubmitted() && nameInContext.equals(ERXAjaxApplication.ajaxSubmitButtonName(worequest));
		if (shouldHandleRequest) {
			String updateContainerID = AjaxUpdateContainer.updateContainerID(this, wocomponent);
			AjaxUpdateContainer.setUpdateContainerID(worequest, updateContainerID);
			wocontext._setActionInvoked(true);
			result = (WOActionResults) valueForBinding("action", wocomponent);
			if (result == null) {
				result = handleRequest(worequest, wocontext);
			}
			AjaxUtils.updateMutableUserInfoWithAjaxInfo(wocontext);
		}
		return result;
	}

	public WOActionResults handleRequest(WORequest request, WOContext context) {
		WOResponse response = AjaxUtils.createResponse(request, context);
		return response;
	}
}
