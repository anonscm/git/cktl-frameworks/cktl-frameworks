/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package er.ajax;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.appserver._private.WOForm;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSPathUtilities;

import er.extensions.appserver.ERXWOContext;
import er.extensions.appserver.ajax.ERXAjaxApplication;
import er.extensions.components._private.ERXWOForm;

/*
 * @deprecated Utiliser CktlAjaxWindow
 */

public class CktlAjaxModalDialog extends AjaxComponent {

	public static final String WINDOWS_CLASS_NAME_GREYLIGHTING = "greylighting";
	public static final String WINDOWS_CLASS_NAME_GREENLIGHTING = "greenlighting";
	public static final String WINDOWS_CLASS_NAME_BLUELIGHTING = "bluelighting";
	public static final String WINDOWS_CLASS_NAME_DARKBLUELIGHTING = "darkbluelighting";
	public static final String WINDOWS_CLASS_NAME_DIALOG = "dialog";

	/** JavaScript to execute on the client to close the modal dialog */
	public static final String Close = "CAMD.close();";

	/** Element ID suffix indicating an Open Dialog action. */
	public static final String Open_ElementID_Suffix = ".open";

	/** Element ID suffix indicating an C Dialog action. */
	public static final String Close_ElementID_Suffix = ".close";

	private boolean _open;
	private WOComponent _actionResults;
	private CktlAjaxModalDialog outerDialog;
	private boolean hasWarnedOnNesting = false;
	private WOComponent previousComponent;
	private String ajaxComponentActionUrl;

	public static final Logger logger = Logger.getLogger(CktlAjaxModalDialog.class);

	public CktlAjaxModalDialog(WOContext context) {
		super(context);
	}

	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	protected void addRequiredWebResources(WOResponse response) {
		AjaxUtils.addScriptResourceInHead(context(), response, "prototype.js");
		AjaxUtils.addScriptResourceInHead(context(), response, "wonder.js");
		AjaxUtils.addScriptResourceInHead(context(), response, "effects.js");
		AjaxUtils.addScriptResourceInHead(context(), response, "FwkCktlThemes.framework", "scripts/window.js");
		AjaxUtils.addScriptResourceInHead(context(), response, "FwkCktlAjaxWebExt.framework", "scripts/cktlwonder.js");
		AjaxUtils.addStylesheetResourceInHead(context(), response, "FwkCktlAjaxWebExt.framework", "themes/default.css");
		AjaxUtils.addStylesheetResourceInHead(context(), response, "FwkCktlAjaxWebExt.framework", "themes/lighting.css");
	}

	public boolean isOpen() {
		return _open;
	}

	public void setOpen(boolean open) {
		_open = open;
	}

	/**
	 * Call this method to have a JavaScript response returned that opens the modal dialog. The title of the dialog will be what it was when rendered.
	 * 
	 * @param context the current WOContext
	 * @param id the HTML ID of the CktlAjaxModalDialog to open
	 */
	public static void open(WOContext context, String id) {
		AjaxUtils.javascriptResponse(openDialogFunctionName(id) + "();", context);
	}

	/**
	 * Call this method to have a JavaScript response returned that opens the modal dialog. The title of the dialog will be the passed title. This is
	 * useful if the script to open this dialog was rendered without the title or with an incorrect title.
	 * 
	 * @param context the current WOContext
	 * @param id the HTML ID of the CktlAjaxModalDialog to open
	 * @param title the title for the CktlAjaxModalDialog
	 */
	public static void open(WOContext context, String id, String title) {
		AjaxUtils.javascriptResponse(openDialogFunctionName(id) + "(" + AjaxValue.javaScriptEscaped(title) + ");", context);
	}

	/**
	 * Returns the JavaScript function name for the function to open the CktlAjaxModalDialog with the specified ID.
	 * 
	 * @param id the HTML ID of the CktlAjaxModalDialog to open
	 * @return JavaScript function name for the function to open the CktlAjaxModalDialog
	 */
	public static String openDialogFunctionName(String id) {
		return "openCAMD_" + id;
	}

	/**
	 * Call this method to have a JavaScript response returned that closes the modal dialog.
	 * 
	 * @param context the current WOContext
	 */
	public static void close(WOContext context) {
		// If the page structure changes as a result of changes from the dialog, and the dialog is no longer
		// part of the page, the onClose action can't be triggered when the dialog closes.  To ensure that this
		// action is always called, it is invoked on the server side when the message is sent to the client to hide
		// the dialog.  In theory this should not be needed, in practice page state can get messy.
		currentDialog(context).closeDialog();
		AjaxUtils.javascriptResponse(CktlAjaxModalDialog.Close, context);
	}

	/**
	 * Call this method to have a JavaScript response returned that closes the modal dialog with id.
	 * 
	 * @param context the current WOContext
	 */
	public static void close(WOContext context, String id) {
		// If the page structure changes as a result of changes from the dialog, and the dialog is no longer
		// part of the page, the onClose action can't be triggered when the dialog closes.  To ensure that this
		// action is always called, it is invoked on the server side when the message is sent to the client to hide
		// the dialog.  In theory this should not be needed, in practice page state can get messy.
		currentDialog(context).closeDialog();
		if (id != null) {
			AjaxUtils.javascriptResponse("CAMD.close('" + id + "_win');", context);
		}
		else {
			AjaxUtils.javascriptResponse(CktlAjaxModalDialog.Close, context);
		}
	}

	/**
	 * @param context the current WOContext
	 * @return the CktlAjaxModalDialog currently being processed
	 * @throws RuntimeException if no CktlAjaxModalDialog is currently being processed
	 */
	public static CktlAjaxModalDialog currentDialog(WOContext context) {
		CktlAjaxModalDialog currentDialog = (CktlAjaxModalDialog) ERXWOContext.contextDictionary().objectForKey(CktlAjaxModalDialog.class.getName());
		if (currentDialog == null) {
			throw new RuntimeException("Attempted to get current CktlAjaxModalDialog when none active.  Check your page structure.");
		}
		return currentDialog;
	}

	/**
	 * @param context the current WOContext
	 * @return true if an CktlAjaxModalDialog currently being processed
	 */
	public static boolean isInDialog(WOContext context) {
		return ERXWOContext.contextDictionary().objectForKey(CktlAjaxModalDialog.class.getName()) != null;
	}

	/**
	 * Start of R-R loop. awakes the components from action if action is bound.
	 * 
	 * @see com.webobjects.appserver.WOComponent#awake()
	 */
	public void awake() {
		super.awake();
		if (_actionResults != null) {
			_actionResults._awakeInContext(context());
		}
	}

	/**
	 * Only handle this phase if the modal box is open. Also includes result returned by action binding if bound.
	 * 
	 * @see com.webobjects.appserver.WOComponent#takeValuesFromRequest(com.webobjects.appserver.WORequest, com.webobjects.appserver.WOContext)
	 */
	public void takeValuesFromRequest(WORequest request, WOContext context) {
		ajaxComponentActionUrl = AjaxUtils.ajaxComponentActionUrl(context());
		if (isOpen()) {
			try {
				pushDialog();
				if (_actionResults != null) {
					pushActionResultsIntoContext(context);
					try {
						_actionResults.takeValuesFromRequest(request, context);
					} finally {
						popActionResultsFromContext(context);
					}
				}
				else {
					super.takeValuesFromRequest(request, context);
				}
			} finally {
				popDialog();
			}
		}
	}

	/**
	 * Only handle this phase if the modal box is open or it is our action (opening the box). Overridden to include result returned by action binding
	 * if bound.
	 * 
	 * @see #close(WOContext)
	 * @see #update(WOContext)
	 * @see com.webobjects.appserver.WOComponent#takeValuesFromRequest(com.webobjects.appserver.WORequest, com.webobjects.appserver.WOContext)
	 */
	public WOActionResults invokeAction(WORequest request, WOContext context) {
		ajaxComponentActionUrl = AjaxUtils.ajaxComponentActionUrl(context());
		pushDialog();
		try {
			WOActionResults result = null;
			if (shouldHandleRequest(request, context)) {
				result = super.invokeAction(request, context);
			}
			else if (isOpen()) {
				if (_actionResults != null) {
					pushActionResultsIntoContext(context);
					try {
						result = _actionResults.invokeAction(request, context);
					} finally {
						popActionResultsFromContext(context);
					}
				}
				else {
					result = super.invokeAction(request, context);
				}
			}

			return result;
		} finally {
			popDialog();
		}
	}

	/**
	 * Removes Open_ElementID_Suffix or Close_ElementID_Suffix before evaluating senderID.
	 * 
	 * @see er.ajax.AjaxComponent#shouldHandleRequest(com.webobjects.appserver.WORequest, com.webobjects.appserver.WOContext)
	 * @return <code>true</code> if this request is for this component
	 */
	protected boolean shouldHandleRequest(WORequest request, WOContext context) {
		String elementID = context.elementID();
		String senderID = context.senderID();

		if (senderID.endsWith(Open_ElementID_Suffix) || senderID.endsWith(Close_ElementID_Suffix)) {
			senderID = NSPathUtilities.stringByDeletingPathExtension(senderID);
		}

		boolean shouldHandleRequest = elementID != null && (elementID.equals(senderID) ||
				elementID.equals(ERXAjaxApplication.ajaxSubmitButtonName(request)));
		return shouldHandleRequest;
	}

	/**
	 * Handles the open and close dialog actions.
	 * 
	 * @see er.ajax.AjaxComponent#handleRequest(com.webobjects.appserver.WORequest, com.webobjects.appserver.WOContext)
	 * @return null or dialog contents
	 */
	public WOActionResults handleRequest(WORequest request, WOContext context) {
		WOActionResults response = null;
		String modalBoxAction = NSPathUtilities.pathExtension(context.senderID());

		if ("close".equals(modalBoxAction)) {
			// This update can't be done in the closeDialog() method as that also gets called from close(WOContext) and
			// and Ajax update is not taking place.  If the page structure changes, this update will not take place,
			// but the correct container ID is on the URL and the update will still happen thanks to the magic in
			// AjaxResponse.AjaxResponseDelegate
			String closeUpdateContainerID = AjaxUpdateContainer.updateContainerID((String) valueForBinding("closeUpdateContainerID"));
			if (closeUpdateContainerID != null) {
				AjaxUpdateContainer.setUpdateContainerID(request, closeUpdateContainerID);
			}

			// This needs to happen AFTER setting up for an update so that AjaxUtils.appendScriptHeaderIfNecessary
			// knows if the script header is needed or not.  Doing this before and setting up a JS response in 
			// the onClose callback, resulted in plain text getting injected into the page.
			closeDialog();
		}
		else if ("open".equals(modalBoxAction) && !isOpen()) {
			openDialog();

			// If there is an action or pageName binding, we need to cache the result of calling that so that
			// the awake, takeValues, etc. messages can get passed onto it
			if (hasBinding("action")) {
				_actionResults = (WOComponent) valueForBinding("action");
				_actionResults._awakeInContext(context);
			}
			else if (hasBinding("pageName")) {
				_actionResults = pageWithName((String) valueForBinding("pageName"));
				_actionResults._awakeInContext(context);
			}

			// WOForm expects that it is inside a WODynamicGroup and relies on WODynamicGroup having setup the WOContext to correctly 
			// generate the URL.  It should not (IMO), but it does.  If you have anything before the WebObject tag for the form 
			// (a space, a carriage return, text, HTML tags, anything at all), then the WO parser creates a WODynamicGroup to hold that.  
			// If  the WebObject tag for the form is the very first thing in the template, then the WODynamicGroup is not created and 
			// invalid URLs are generated rendering the submit controls non-functional.  Throw an exception so the developer knows what is 
			// wrong and can correct it.
			if (_actionResults != null && (_actionResults.template() instanceof WOForm ||
					_actionResults.template() instanceof ERXWOForm)) {
				throw new RuntimeException(_actionResults.name() + " is used as contents of CktlAjaxModalDialog, but starts with WOForm tag.  " +
						"Action elements inside the dialog will not function.  Add a space at the start or end of " + _actionResults.name() + ".html");
			}

		}

		if (isOpen()) {
			response = AjaxUtils.createResponse(request, context);

			// Register the id of this component on the page in the request so that when 
			// it comes time to cache the context, it knows that this area is an Ajax updating area
			AjaxUtils.setPageReplacementCacheKey(context, _containerID(context));

			if (_actionResults != null) {
				pushActionResultsIntoContext(context);
				try {
					_actionResults.appendToResponse((WOResponse) response, context);
				} finally {
					popActionResultsFromContext(context);
				}
			}
			else {
				super.appendToResponse((WOResponse) response, context);
			}
		}

		return response;
	}

	/**
	 * This has two modes. One is to generate the link that opens the dialog. The other is to return the contents of the dialog (the result returned
	 * by action binding is handled in handleRequest, not here).
	 * 
	 * @see er.ajax.AjaxComponent#appendToResponse(com.webobjects.appserver.WOResponse, com.webobjects.appserver.WOContext)
	 */
	public void appendToResponse(WOResponse response, WOContext context) {
		ajaxComponentActionUrl = AjaxUtils.ajaxComponentActionUrl(context());
		if (context.isInForm()) {
			logger.warn("The CktlAjaxModalDialog should not be used inside of a WOForm (" + ERXWOForm.formName(context, "- not specified -") +
					") if it contains any form inputs or buttons.  Remove this AMD from this form, add a form of its own. Replace it with " +
					"an CktlAjaxModalDialogOpener with a dialogID that matches the ID of this dialog.");
			logger.warn("    page: " + context.page());
			logger.warn("    component: " + context.component());
		}

		if (!booleanValueForBinding("enabled", true)) {
			return;
		}

		// If this is not an Ajax request, the page has been reloaded.  Try to recover state
		if (isOpen() && !AjaxRequestHandler.AjaxRequestHandlerKey.equals(context().request().requestHandlerKey())) {
			closeDialog();
		}

		// If we are open, but the request is not for us, don't render the content.
		// This shouldHandleRequest prevents showing an open dialog in the page when 
		// an AUC refreshes
		if (isOpen() && shouldHandleRequest(context.request(), context)) {
			if (_actionResults != null) {
				throw new RuntimeException("Unexpected call to appendToResponse");
			}
			try {
				pushDialog();
				super.appendToResponse(response, context);
			} finally {
				popDialog();
			}
		}
		else {
			boolean showOpener = booleanValueForBinding("showOpener", true);

			if (showOpener) {
				response.appendContentString("<a href=\"javascript:void(0)\"");
				appendTagAttributeToResponse(response, "id", id());
				appendTagAttributeToResponse(response, "class", valueForBinding("class", null));
				appendTagAttributeToResponse(response, "style", valueForBinding("style", null));
				if (hasBinding("linkTitle")) {
					appendTagAttributeToResponse(response, "title", valueForBinding("linkTitle", null));
				}
				else {
					appendTagAttributeToResponse(response, "title", valueForBinding("title", null));
				}

				// onclick calls the script below
				response.appendContentString(" onclick=\"");
				response.appendContentString(openDialogFunctionName(id()));
				response.appendContentString("(); return false;\" >");

				if (hasBinding("label")) {
					response.appendContentString((String) valueForBinding("label"));
				}
				else {
					// This will append the contents of the ERXWOTemplate named "link"
					super.appendToResponse(response, context);
				}
				response.appendContentString("</a>");
			}
			else {
				response.appendContentString("<div style=\"display:none;\"");
				appendTagAttributeToResponse(response, "id", id());
				response.appendContentString(">");
				super.appendToResponse(response, context);
				response.appendContentString("</div>");
			}

			// This script can also be called directly by other code to show the modal dialog
			AjaxUtils.appendScriptHeader(response);
			response.appendContentString(openDialogFunctionName(id()));
			response.appendContentString(" = function(titleBarText) {\n");
			appendOpenModalDialogFunction(response, context);
			response.appendContentString("}\n");

			// Auto-open
			if (booleanValueForBinding("open", false)) {
				response.appendContentString(openDialogFunctionName(id()));
				response.appendContentString("();\n");
			}
			AjaxUtils.appendScriptFooter(response);

			// normally this would be done in super, but we're not always calling super here
			addRequiredWebResources(response);
		}
	}

	/**
	 * Appends function body to open the modal dialog window.
	 * 
	 * @see CktlAjaxModalDialog#openDialogFunctionName(String)
	 * @param response WOResponse to append to
	 * @param context WOContext of response
	 */
	protected void appendOpenModalDialogFunction(WOResponse response, WOContext context) {
		response.appendContentString("    options = ");
		NSMutableDictionary options = createModalBoxOptions();
		AjaxOptions.appendToResponse(options, response, context);
		response.appendContentString(";\n");
		response.appendContentString("    if (titleBarText) options.title = titleBarText;\n");
		response.appendContentString("    Window.keepMultiModalWindow=true;\n");
		response.appendContentString("    var win = Windows.getWindow('");
		response.appendContentString(id() + "_win');\n");
		response.appendContentString("    if (typeof(win)=='undefined') {\n");
		response.appendContentString("    	win = new Window('");
		response.appendContentString(id() + "_win");
		response.appendContentString("', options);\n");
		response.appendContentString("		var editorOnClose = {\n");
		response.appendContentString("			onBeforeShow: function(eventName, win) {\n");
		if (!options.containsKey("url")) {
			response.appendContentString("				AUL._update('ContainerAMD_" + id() + "','" + openDialogURL(context) + "', null, null, null);\n");
		}
		response.appendContentString("			}, \n");
		response.appendContentString("			onShow: function(eventName, win) {\n");
		response.appendContentString("			}, \n");
		response.appendContentString("			onClose: function(eventName, win) {\n");
		response.appendContentString(" 				AUL.request('" + closeDialogURL(context()) + "', null, null, null);");
		response.appendContentString("				options.afterHide.apply(this, null);\n");
		response.appendContentString("			}, \n");
		response.appendContentString("			onDestroy: function(eventName, win) {\n");
		response.appendContentString("				Windows.removeObserver(editorOnClose);\n");
		response.appendContentString("			} \n");
		if ((hasBinding("onMinimize"))&&(!"".equals(valueForBinding("onMinimize")))){
			response.appendContentString("			,onMinimize: function(eventName, win) {\n");
			response.appendContentString("				"+valueForBinding("onMinimize")+"\n");
			response.appendContentString("			} \n");
		}
		if ((hasBinding("onMaximize"))&&(!"".equals(valueForBinding("onMaximize")))){
			response.appendContentString("			,onMaximize: function(eventName, win) {\n");
			response.appendContentString("				"+valueForBinding("onMaximize")+"\n");
			response.appendContentString("			} \n");
		}
		response.appendContentString("		};\n");
		response.appendContentString("		Windows.addObserver(editorOnClose);\n");
		response.appendContentString("		win.setDestroyOnClose();\n");
		response.appendContentString("		win.setZIndex(99999);\n");
		if (!options.containsKey("url")) {
			response.appendContentString("		win.setContent(");
			//			response.appendContentString("containerId,true);\n");
			response.appendContentString("'ContainerAMD_" + id() + "',false);\n");
			//		} else {
			//			if (options.containsKey("width") || options.containsKey("height")) {
			//				response.appendContentString("setSize(options.width,options.height);\n");
			//			}
		}
		response.appendContentString("		win."+getShowWinFunction()+"("+((hasBinding("modal"))?(String) valueForBinding("modal"):"true")+");\n");
		response.appendContentString("	} else {\n");
		response.appendContentString("		win."+getShowWinFunction()+"();\n");
		response.appendContentString("		}\n");
	}
	
	protected String getShowWinFunction(){
		if ((hasBinding("center"))&&(!((Boolean) valueForBinding("center")))){
			return "show";			
		}
		return "showCenter";
	}

	/**
	 * End of R-R loop. Puts the components from action to sleep if action is bound.
	 * 
	 * @see com.webobjects.appserver.WOComponent#sleep()
	 */
	public void sleep() {
		if (_actionResults != null) {
			_actionResults._sleepInContext(context());
		}
		ajaxComponentActionUrl = null;
		super.sleep();
	}

	/**
	 * Calls the method bound to onOpen (if any), and marks the dialog state as open.
	 */
	public void openDialog() {
		if (hasBinding("onOpen")) {
			valueForBinding("onOpen");
		}

		setOpen(true);
	}

	/**
	 * If the dialog is open, calls the method bound to onClose (if any), and marks the dialog state as closed. This method can get called if the page
	 * gets reloaded so be careful modifying the response if
	 * <code>! AjaxRequestHandler.AjaxRequestHandlerKey.equals(context().request().requestHandlerKey())</code>
	 */
	public void closeDialog() {
		if (isOpen()) {
			if (hasBinding("onClose")) {
				valueForBinding("onClose");
			}

			setOpen(false);
			_actionResults = null;
		}
	}

	/**
	 * @see er.ajax.AjaxComponent#_containerID(com.webobjects.appserver.WOContext)
	 * @return id()
	 */
	protected String _containerID(WOContext context) {
		return id();
	}

	/**
	 * @return the value bound to id or an manufactured string if id is not bound
	 */
	public String id() {
		return hasBinding("id") ? (String) valueForBinding("id") : ERXWOContext.safeIdentifierName(context(), false);
	}

	protected NSMutableDictionary createModalBoxOptions() {
		NSMutableArray ajaxOptionsArray = new NSMutableArray();
		ajaxOptionsArray.addObject(new AjaxOption("id", AjaxOption.STRING));
		ajaxOptionsArray.addObject(new AjaxOption("title", AjaxOption.STRING));
		if (hasBinding("action") || hasBinding("pageName")) {
			ajaxOptionsArray.addObject(new AjaxOption("url", openDialogURL(context()), AjaxOption.STRING));
		}
		ajaxOptionsArray.addObject(new AjaxOption("parent", AjaxOption.STRING));
		ajaxOptionsArray.addObject(new AjaxOption("top", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("bottom", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("right", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("left", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("width", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("height", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("maxWidth", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("maxHeight", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("minWidth", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("minHeight", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("resizable", AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("closable", AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("minimizable", AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("maximizable", AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("draggable", AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("showEffect", "Effect.BlindDown", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("hideEffect", "Effect.Fold", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("showEffectOptions", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("hideEffectOptions", AjaxOption.STRING));
		ajaxOptionsArray.addObject(new AjaxOption("effectOptions", AjaxOption.STRING));

		ajaxOptionsArray.addObject(new AjaxOption("onload", AjaxOption.SCRIPT));
		ajaxOptionsArray.addObject(new AjaxOption("opacity", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("recenterAuto", AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("gridX", AjaxOption.NUMBER));
		ajaxOptionsArray.addObject(new AjaxOption("gridY", AjaxOption.NUMBER));

		ajaxOptionsArray.addObject(new AjaxOption("destroyOnClose", AjaxOption.BOOLEAN));

		if (hasBinding("className")) {
			ajaxOptionsArray.addObject(new AjaxOption("className", AjaxOption.STRING));
		}
		else if (session() instanceof CocktailAjaxSession) {
			//			ajaxOptionsArray.addObject(new AjaxOption("className", session().valueForKey("windowsClassName"), AjaxOption.STRING));
			ajaxOptionsArray.addObject(new AjaxOption("className", ((CocktailAjaxSession) session()).getWindowsClassName(), AjaxOption.STRING));
		}

		if (!hasBinding("minimizable")) {
			ajaxOptionsArray.addObject(new AjaxOption("minimizable", Boolean.FALSE, AjaxOption.BOOLEAN));
		}
		if (!hasBinding("maximizable")) {
			ajaxOptionsArray.addObject(new AjaxOption("maximizable", Boolean.FALSE, AjaxOption.BOOLEAN));
		}
		// JS to notify server when the dialog box is closed.  This needs to be added to anything
		// bound to afterHide
		String closeUpdateContainerID = AjaxUpdateContainer.updateContainerID((String) valueForBinding("closeUpdateContainerID"));
		String serverUpdate;
		if (closeUpdateContainerID == null) {
			serverUpdate = " AUL.request('" + closeDialogURL(context()) + "', null, null, null);";
		}
		else {
			serverUpdate = " AUL._update('" + closeUpdateContainerID + "', '" + closeDialogURL(context()) + "', null, null, null);";
		}

		if (hasBinding("afterHide") && valueForBinding("afterHide") != null) {
			String afterHide = (String) valueForBinding("afterHide");
			int openingBraceIndex = afterHide.indexOf('{');

			if (openingBraceIndex > -1) {
				serverUpdate = "function() {" + serverUpdate + " " + afterHide.substring(openingBraceIndex + 1);
			}
			else
				throw new RuntimeException("Don't know how to handle afterHide value '" + afterHide + "', did you forget to wrap it in function() { ...}?");
		}
		else {
			serverUpdate = "function(v) { " + serverUpdate + '}';
		}

		ajaxOptionsArray.addObject(new AjaxOption("afterHide", serverUpdate, AjaxOption.SCRIPT));

		NSMutableDictionary options = AjaxOption.createAjaxOptionsDictionary(ajaxOptionsArray, this);

		return options;
	}

	/**
	 * Stash this dialog instance in the context so we can access it from the static methods. If there is one AMD next in another (a rather dubious
	 * thing to do that we warn about but it may have its uses), we need to remember the outer one while processing this inner one
	 * 
	 * @see CktlAjaxModalDialog#popDialog()
	 */
	protected void pushDialog() {
		outerDialog = (CktlAjaxModalDialog) ERXWOContext.contextDictionary().objectForKey(CktlAjaxModalDialog.class.getName());
		ERXWOContext.contextDictionary().setObjectForKey(this, CktlAjaxModalDialog.class.getName());
		if (!hasWarnedOnNesting && outerDialog != null) {
			hasWarnedOnNesting = true;
			logger.warn("CktlAjaxModalDialog " + id() + " is nested inside of " + outerDialog.id() + ". Are you sure you want to do this?");
		}
	}

	/**
	 * Remove this dialog instance from the context, replacing the previous one if any.
	 * 
	 * @see #pushDialog()
	 */
	protected void popDialog() {
		if (outerDialog != null) {
			ERXWOContext.contextDictionary().setObjectForKey(outerDialog, CktlAjaxModalDialog.class.getName());
		}
		else {
			ERXWOContext.contextDictionary().removeObjectForKey(CktlAjaxModalDialog.class.getName());
		}
	}

	/**
	 * Make _actionResults (result of the action binding) the current component in context for WO processing. Remembers the current component so that
	 * it can be restored.
	 * 
	 * @param context WOContext to push _actionResults into
	 * @see #popActionResultsFromContext(WOContext)
	 */
	protected void pushActionResultsIntoContext(WOContext context) {
		previousComponent = context.component();
		context._setCurrentComponent(_actionResults);
	}

	/**
	 * Sets the current component in context to the one there before pushActionResultsIntoContext was called.
	 * 
	 * @param context WOContext to restore previous component in
	 * @see #pushActionResultsIntoContext(WOContext)
	 */
	protected void popActionResultsFromContext(WOContext context) {
		context._setCurrentComponent(previousComponent);
	}

	/**
	 * @param context WOContext to create URL in
	 * @return URL to invoke when the dialog is opened
	 */
	protected String openDialogURL(WOContext context) {
		return new StringBuffer(ajaxComponentActionUrl).append(Open_ElementID_Suffix).toString();
	}

	/**
	 * @param context WOContext to create URL in
	 * @return URL to invoke when the dialog is closed
	 */
	protected String closeDialogURL(WOContext context) {
		return new StringBuffer(ajaxComponentActionUrl).append(Close_ElementID_Suffix).toString();
	}

	public String containerAjaxModalDialogID() {
		return "ContainerAMD_" + id();
	}

}
