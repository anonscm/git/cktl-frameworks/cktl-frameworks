/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package er.ajax;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WOResponse;
import com.webobjects.appserver._private.WODynamicElementCreationException;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.appserver.ERXWOContext;
import er.extensions.components._private.ERXWOForm;

public class CktlAjaxSubmitButton extends AjaxSubmitButton {

	public static final String BINDING_type = "type";
	public static final String BINDING_title = "title";
	public static final String BINDING_enabled = "enabled";

	/** Le texte a afficher */
	public static final String BINDING_text = "text";

	/** Indique s'il faut afficher le text */
	public static final String BINDING_showText = "showText";

	/** Indique s'il faut afficher l'image */
	public static final String BINDING_showImage = "showImage";

	/** Indique s'il l'image est situee a droite du label */
	public static final String BINDING_isImagePositionIsRight = "isImagePositionIsRight";

	public static final String TYPE_EDIT = "edit";
	public static final String TYPE_NEW = "new";
	public static final String TYPE_DELETE = "delete";
	public static final String TYPE_FIND = "find";

	public static final String ICON_SIZE_BIG = "big";
	public static final String ICON_SIZE_SMALL = "small";
	
	public static final Boolean DEFAULT_SHOW_TEXT = Boolean.FALSE;
	public static final Boolean DEFAULT_SHOW_IMAGE = Boolean.TRUE;
	private static final Boolean DEFAULT_BUTTON = Boolean.FALSE;
	private static final String DEFAULT_ICON_SIZE = ICON_SIZE_SMALL;

	/** Boolean. Si true, les multiples clics ne seront pas pris en compte sur le lien (evitant ainsi de generer plusieurs post vers le serveur).*/
	public static final String BINDING_preventMultipleClick = "preventMultipleClick";
	
	public static final Boolean DEFAULT_preventMultipleClick = Boolean.TRUE;

	/** Indique s'il faut afficher l'image busy */
	public static final String BINDING_showBusyImage = "showBusyImage";

	public CktlAjaxSubmitButton(String name, NSDictionary associations, WOElement children) {
		super(name, associations, children);
	}

	public void appendToResponse(WOResponse response, WOContext context) {
		WOComponent component = context.component();

		String formName = (String)valueForBinding("formName", component);
		String formReference;
		if (formName == null) {
			formName = ERXWOForm.formName(context, null);
			if (formName == null) {
				throw new WODynamicElementCreationException("The containing form must have an explicit name.");
			}
		}
		if (formName == null) {
			formReference = "this.form";
		}
		else {
			formReference = "document." + formName;
		}

		StringBuffer onClickBuffer = new StringBuffer();

		if (preventMultipleClick(context.component())) {
			onClickBuffer.append("if ($('"+getButtonId(context)+"').hasClassName('locked')){return false;} else {$('"+getButtonId(context)+"').addClassName('locked');");
		}
		
		Boolean showBusyImage = (Boolean) valueForBinding(BINDING_showBusyImage, Boolean.FALSE, context.component());
		
		if (showBusyImage.booleanValue()) {
			onClickBuffer.append("$('"+getButtonBaseImageId(context)+"').addClassName('cktl_action_busy');");
		}
		String onClickBefore = (String)valueForBinding("onClickBefore", component);
		if (onClickBefore != null) {
			onClickBuffer.append("if (");
			onClickBuffer.append(onClickBefore);
			onClickBuffer.append(") {");
		}

		String updateContainerID = AjaxUpdateContainer.updateContainerID(this, component); 

		// PROTOTYPE EFFECTS
		String beforeEffect = (String) valueForBinding("beforeEffect", component);
		if (beforeEffect != null) {
			onClickBuffer.append("new ");
			onClickBuffer.append(AjaxUpdateLink.fullEffectName(beforeEffect));
			onClickBuffer.append("('");

			String beforeEffectID = (String) valueForBinding("beforeEffectID", component);
			if (beforeEffectID == null) {
				beforeEffectID = AjaxUpdateContainer.currentUpdateContainerID();
				if (beforeEffectID == null) {
					beforeEffectID = updateContainerID;
				}
			}
			onClickBuffer.append(beforeEffectID);

			onClickBuffer.append("', { ");

			String beforeEffectDuration = (String) valueForBinding("beforeEffectDuration", component);
			if (beforeEffectDuration != null) {
				onClickBuffer.append("duration: ");
				onClickBuffer.append(beforeEffectDuration);
				onClickBuffer.append(", ");
			}

			onClickBuffer.append("queue:'end', afterFinish: function() {");
		}

		if (updateContainerID != null) {
			onClickBuffer.append("ASB.update('" + updateContainerID + "',");
		}
		else {
			onClickBuffer.append("ASB.request(");
		}
		onClickBuffer.append(formReference);
		onClickBuffer.append(",null");
		onClickBuffer.append(",");

		NSMutableDictionary options = createAjaxOptions(component);

		AjaxUpdateLink.addEffect(options, (String) valueForBinding("effect", component), getId(context), (String) valueForBinding("effectDuration", component));
		String afterEffectID = (String) valueForBinding("afterEffectID", component);
		if (afterEffectID == null) {
			afterEffectID = AjaxUpdateContainer.currentUpdateContainerID();
			if (afterEffectID == null) {
				afterEffectID = getId(context);
			}
		}
		AjaxUpdateLink.addEffect(options, (String) valueForBinding("afterEffect", component), afterEffectID, (String) valueForBinding("afterEffectDuration", component));

		AjaxOptions.appendToBuffer(options, onClickBuffer, context);
		onClickBuffer.append(")");
		String onClick = (String) valueForBinding("onClick", component);
		if (onClick != null) {
			onClickBuffer.append(";");
			onClickBuffer.append(onClick);
		}

		if (beforeEffect != null) {
			onClickBuffer.append("}});");
		}

		if (onClickBefore != null) {
			onClickBuffer.append("}");
		}
		if (hasBinding("onClickBefore") && showBusyImage.booleanValue()) {
			onClickBuffer.append(" else {$('"+getButtonBaseImageId(context)+"').removeClassName('cktl_action_busy');};");
		}

		onClickBuffer.append(";return false;");

		if (preventMultipleClick(context.component())) {
			onClickBuffer.append("}");
		}
		
		boolean disabled = disabledInComponent(component);
		if (disabled) {

			response.appendContentString("<");
			response.appendContentString("span");
			response.appendContentString(" ");
			appendTagAttributeToResponse(response, "id", getId(context));
			appendTagAttributeToResponse(response, "style", "cursor:pointer;");
			response.appendContentString(">");

			appendButtonLookToResponse(response, component);

			response.appendContentString("</");
			response.appendContentString("span");
			response.appendContentString(">");				
			AjaxUtils.appendScriptHeader(response);
			response.appendContentString(jsOnAfterDisplayed(component));
			AjaxUtils.appendScriptFooter(response);
		} else {
			Object stringValue = valueForBinding("string", component);
			response.appendContentString("<");
			response.appendContentString("a");
			response.appendContentString(" ");
			appendTagAttributeToResponse(response, "href", "javascript:void(0);");
			appendTagAttributeToResponse(response, "title", valueForBinding("title", component));
			appendTagAttributeToResponse(response, "value", valueForBinding("value", component));
			appendTagAttributeToResponse(response, "class", "cktl_button");
			appendTagAttributeToResponse(response, "style", valueForBinding("style", component));
			appendTagAttributeToResponse(response, "id", getId(context));
			appendTagAttributeToResponse(response, "accesskey", valueForBinding("accesskey", component));
    		appendTagAttributeToResponse(response, "onclick", onClickBuffer.toString());
			if (stringValue != null) {
				appendTagAttributeToResponse(response, "value", stringValue);
			}
			response.appendContentString(">");
			appendButtonLookToResponse(response, component);
		}
		appendChildrenToResponse(response, context);
		response.appendContentString("</");
		response.appendContentString("a");
		response.appendContentString(">");
		AjaxUtils.appendScriptHeader(response);
		response.appendContentString(jsOnAfterDisplayed(component));
		AjaxUtils.appendScriptFooter(response);
	}

	protected boolean preventMultipleClick(WOComponent component) {
		if (  valueForBinding("onClickBefore", component)!=null ) {
			return false;
		}
		Boolean preventMultipleClick = (Boolean) valueForBinding(BINDING_preventMultipleClick, component);
		if (preventMultipleClick == null) {
			preventMultipleClick = DEFAULT_preventMultipleClick;
		}
		return preventMultipleClick.booleanValue();
	}

	private void appendButtonLookToResponse(WOResponse response, WOComponent component) {
		response.appendContentString("<div"); 
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_inline_block cktl_button cktl_button_base");
		response.appendContentString(">");

		response.appendContentString("<div"); 
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_inline_block cktl_button_base_outer_box");
		response.appendContentString(">");

		response.appendContentString("<div"); 
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_inline_block cktl_button_base_inner_box");
		response.appendContentString(">");

		response.appendContentString("<div"); 
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_button_base_pos");
		response.appendContentString(">");

		response.appendContentString("<div"); 
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_button_base_top_shadow");
		response.appendContentString(">");
		response.appendContentString("&nbsp;");
		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("<div"); 
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "id", getButtonId(component.context()));
		appendTagAttributeToResponse(response, "class", "cktl_button_base_content");
		response.appendContentString(">");

		if (hasBinding(BINDING_type) || hasBinding(BINDING_showBusyImage)) {
			if (isImagePositionIsRight(component)) {
				response.appendContentString("<");
				response.appendContentString("span");
				response.appendContentString(" ");
				response.appendContentString(">");
				if (getText(component) != null) {
					response.appendContentString(getText(component)+"&nbsp;");
				}
				response.appendContentString("</");
				response.appendContentString("span");
				response.appendContentString(">");
				response.appendContentString("<span"); 
				response.appendContentString(" ");
				appendTagAttributeToResponse(response, "id", getButtonBaseImageId(component.context()));
				appendTagAttributeToResponse(response, "class", getButtonBaseImageClass(component));
				response.appendContentString(">");
				response.appendContentString("</");
				response.appendContentString("span");
				response.appendContentString(">");
			} else {
				response.appendContentString("<span"); 
				response.appendContentString(" ");
				appendTagAttributeToResponse(response, "id", getButtonBaseImageId(component.context()));
				appendTagAttributeToResponse(response, "class", getButtonBaseImageClass(component));
				response.appendContentString(">");
				response.appendContentString("</");
				response.appendContentString("span");
				response.appendContentString(">");
				response.appendContentString("<");
				response.appendContentString("span");
				response.appendContentString(" ");
				response.appendContentString(">");
				if (getText(component) != null) {
					response.appendContentString("&nbsp;"+getText(component));
				}
				response.appendContentString("</");
				response.appendContentString("span");
				response.appendContentString(">");				
			}
		} else {
			response.appendContentString("<");
			response.appendContentString("span");
			response.appendContentString(" ");
			response.appendContentString(">");
			if (getText(component) != null) {
				response.appendContentString(getText(component));
			}
			response.appendContentString("</");
			response.appendContentString("span");
			response.appendContentString(">");				
		}
		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

	}

	@Override
	  public NSMutableDictionary createAjaxOptions(WOComponent component) {
		NSMutableDictionary res = super.createAjaxOptions(component);
		Boolean showBusyImage = (Boolean) valueForBinding(BINDING_showBusyImage, Boolean.FALSE, component);
		
		if (showBusyImage.booleanValue()) {
			String onCompleteAddition = "var btn = $('"+getButtonBaseImageId(component.context())+"');if (btn != null) { btn.removeClassName('cktl_action_busy'); };";
			String onComplete = (String) res.valueForKey("onComplete");
			if (onComplete != null) {
				int i = onComplete.lastIndexOf("}"); 
				if ( i >-1 ) {
					onComplete = onComplete.substring(0, i) + onCompleteAddition + onComplete.substring(i);
				}
				else {
					//Erreur
					onComplete = null;
				}
			} else {
				onComplete = "function() {"+onCompleteAddition+"}";
			}
			res.takeValueForKey(onComplete, "onComplete");
		}
				
		if (preventMultipleClick(component)) {
			String onCompleteAddition = "var aul = $('"+getButtonId(component.context())+"');if (aul != null) { aul.removeClassName('locked'); };";
			String onComplete = (String) res.valueForKey("onComplete");
			if (onComplete != null) {
				int i = onComplete.lastIndexOf("}"); 
				if ( i >-1 ) {
					onComplete = onComplete.substring(0, i) + onCompleteAddition + onComplete.substring(i);
				}
				else {
					//Erreur
					onComplete = null;
				}
			}
			if (onComplete == null) {
				onComplete = "function() {"+onCompleteAddition+"}";
			}
			res.takeValueForKey(onComplete, "onComplete");
		}

		return res;
	}

	private boolean disabled(WOComponent component) {
		return booleanValueForBinding("disabled", false, component);
	}
	private boolean isImagePositionIsRight(WOComponent component) {
		return hasBinding(BINDING_isImagePositionIsRight) && booleanValueForBinding(BINDING_isImagePositionIsRight, false, component);
	}

	public String getId(WOContext context) {
	    String theId;
	    if (hasBinding("id")) {
	        theId = stringValueForBinding("id", context.component());
	    }
	    else {
	        theId = "cktl_" + ERXWOContext.safeIdentifierName(context, false);
	    }
	    return theId;
	}

	public String getButtonBaseImageId(WOContext context) {
		return getId(context)+"_btn_base_image";
	}
	public String getButtonId(WOContext context) {
		return getId(context)+"_btn";
	}

	public String getButtonBaseImageClass(WOComponent component) {
		String buttonBaseImageClass = "cktl_action_button ";
		
		if (hasBinding(BINDING_type)) {
			buttonBaseImageClass += "cktl_action_" + stringValueForBinding(BINDING_type, component) + " ";
		}
		buttonBaseImageClass += "cktl_button_icon cktl_inline_block";
		
		return buttonBaseImageClass;
	}

	public String getText(WOComponent component) {
		if (hasBinding(BINDING_text)) {
			return (String) stringValueForBinding(BINDING_text, component);
		}
		return null;
	}

	public String jsOnAfterDisplayed(WOComponent component) {
		if (disabled(component)) {
			return "var btn = $('" + getId(component.context()) + "');if (btn != null) {btn.setOpacity(0.5);btn.firstDescendant().addClassName('cktl_button_disabled');}";
		}
		else {
			return "var btn = $('" + getId(component.context()) + "');if (btn != null) {btn.setOpacity(1);}";
		}
	}


	protected void addRequiredWebResources(WOResponse res, WOContext context) {
		CktlAjaxUtils.addScriptResourceInHead(context, res, "prototype.js");
		CktlAjaxUtils.addScriptResourceInHead(context, res, "effects.js");
		CktlAjaxUtils.addScriptResourceInHead(context, res, "wonder.js");
		CktlAjaxUtils.addStylesheetResourceInHead(context, res, "FwkCktlThemes", "css/CktlCommon.css");
	}
	
}
