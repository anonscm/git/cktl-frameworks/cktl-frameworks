/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */


package er.ajax;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.appserver.ERXWOContext;
import er.extensions.components.ERXClickToOpenSupport;

/*
 * @deprecated Utiliser CktlAjaxWindowOpener
 */
public class CktlAjaxModalDialogOpener extends AjaxComponent {

    public static final boolean isClickToOpenEnabled = Boolean.valueOf(System.getProperty("er.component.clickToOpen", "false"));

	public CktlAjaxModalDialogOpener(WOContext context) {
		super(context);
	}

    public boolean isStateless() {
    	return true;
    }
	/**
	 * Generate a link that opens the indicated dialog.
	 *
	 * @see er.ajax.AjaxComponent#appendToResponse(com.webobjects.appserver.WOResponse, com.webobjects.appserver.WOContext)
	 */
	public void appendToResponse(WOResponse response, WOContext context) {
        ERXClickToOpenSupport.preProcessResponse(response, context, isClickToOpenEnabled);
		if( ! booleanValueForBinding("enabled", true)) {
			response.appendContentString("<span style=\"opacity:0.5\">");
			if (hasBinding("label")) {
				response.appendContentString((String)valueForBinding("label", null));
			} else {
				// This will append the contents inside of the link
				super.appendToResponse(response, context);
			}
		} else {
		
			response.appendContentString("<a href=\"javascript:void(0)\"");
			appendTagAttributeToResponse(response, "id", id());
			appendTagAttributeToResponse(response, "class", valueForBinding("class", null));
			appendTagAttributeToResponse(response, "style", valueForBinding("style", null));
			appendTagAttributeToResponse(response, "title", valueForBinding("linkTitle", null));
			
			// onclick calls the script that opens the AjaxModalDialog
			response.appendContentString(" onclick=\"");
			if (!hasBinding("action")) {
				StringBuilder sb = new StringBuilder(500);
				sb.append(CktlAjaxModalDialog.openDialogFunctionName(modalDialogId()));
				sb.append("(");	
				
				// Override for dialog name
				if (hasBinding("title")) {	
					sb.append(AjaxValue.javaScriptEscaped(valueForBinding("title")));
				}		
				sb.append(");");
				response.appendContentString(sb+"");
			} else {
				response.appendContentString("new Ajax.Request('");
				response.appendContentString(AjaxUtils.ajaxComponentActionUrl(context));
				response.appendContentString("', ");
				AjaxOptions.appendToResponse(ajaxRequestOptions(), response, context);
				response.appendContentString("); ");
			}
			response.appendContentString("return false;\" >");	
	
			if (hasBinding("label")) {
				response.appendContentString((String) valueForBinding("label"));
			} else {
				// This will append the contents inside of the link
				super.appendToResponse(response, context);
			}
			response.appendContentString("</a>");
		}
        ERXClickToOpenSupport.postProcessResponse(getClass(), response, context, isClickToOpenEnabled);
	}

	/**
	 * @return the value bound to id or an manufactured string if id is not bound
	 */
	public String id() {
		return hasBinding("id") ? (String) valueForBinding("id") : ERXWOContext.safeIdentifierName(context(), false);
	}
	
	/**
	 * @return the value bound to dialogId
	 */
	public String modalDialogId() {
		return (String) valueForBinding("dialogId");
	}
	
	protected void addRequiredWebResources(WOResponse res) {
	}

	/**
	 * Runs action and returns success status if enabled, otherwise returns failed status.
	 */
	public WOActionResults handleRequest(WORequest request, WOContext context) {
		if( booleanValueForBinding("enabled", true)) {
			valueForBinding("action");
		}
		else {
			// Set the response status code to an error code so that the onSuccess callback is not executed
			// If there is an onFailure callback, it will get executed
			// Status 409 is "Conflict" which seemed like the best match for this
			AjaxUtils.createResponse(request, context).setStatus(409);
		}
		
		return null;
	}
	/**
	 * @return options for Ajax.Request that is made when the link is clicked
	 */
	protected NSMutableDictionary ajaxRequestOptions() {
		NSMutableArray ajaxOptionsArray = new NSMutableArray();
		ajaxOptionsArray.addObject(new AjaxOption("asynchronous", Boolean.FALSE, AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("evalScripts", Boolean.FALSE, AjaxOption.BOOLEAN));
		ajaxOptionsArray.addObject(new AjaxOption("onFailure", AjaxOption.SCRIPT));
		
//		ajaxOptionsArray.addObject(new AjaxOption("form", "this.form()", AjaxOption.SCRIPT));
		
		// onSuccess callback handler to open AMD
		StringBuilder sb = new StringBuilder(500);
		sb.append(CktlAjaxModalDialog.openDialogFunctionName(modalDialogId()));
		sb.append("(");	
		
		// Override for dialog name
		if (hasBinding("title")) {	
			sb.append(AjaxValue.javaScriptEscaped(valueForBinding("title")));
		}		
		sb.append(");");
		ajaxOptionsArray.addObject(new AjaxOption("onSuccess", "function(){"+sb.toString()+"}", AjaxOption.SCRIPT));

		return AjaxOption.createAjaxOptionsDictionary(ajaxOptionsArray, this);
	}

}
