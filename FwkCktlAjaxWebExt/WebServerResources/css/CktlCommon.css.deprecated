/**********************************************************/
/* Feuille de style commune des applications web Cocktail */
/**********************************************************/

/** 
* A charger en premier dans le header des pages de votre application 
* Si vous souhaitez changer le look des composants, créez une feuille 
* de style pour votre application dans laquelle vous redéfinissez 
*  tout ou partie des classes css ci-dessous.
* Les couleurs sont volontairement laissees en niveaux de gris.
* Chargez en plus CktlCommonVert.css, CktlCommonBleu.css etc. pour passer en couleur.

*/

/* --------------------------------------------------------------------------- */
/*Elements html de base*/
body {
  margin: 0;
  padding: 0;
  font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
}

form {
	margin: 0;
	padding: 0;
}

input, select, textarea {
 font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
 font-size: 11px;
 font-weight: bold;
 border: 1px solid;
 border-color: black;
  margin: 2px;
}


input[type=button], input[type=submit]  {
  cursor: default;
 /* background-color: #FFFFFF;*/
 background: #DDDDDD url(images/cktl_button_background_20.png) repeat-x scroll 0;
 height: 23px; 
 vertical-align: middle;
 border-color: lightgray;
}

input[type=radio], input[type=checkbox] {
	border-style: none;
}


input:hover[type=button],input:hover[type=submit]  {
  cursor: default;
  color : black;
  border-color : black; 
}

input[disabled=true] {
  cursor: default;
  background-color: lightgray;
}
input[disabled] {
  cursor: default;
  background-color: lightgray;
}
select[disabled] {
  cursor: default;
  background-color: lightgray;
}

td {
	 font-size: 12px;
}

th {
	 font-size: 12px;
	 font-weight: bold;
	 text-align: left;
}	

a {
	text-decoration: none;
	color: black;
}

a:hover {
	text-decoration: underline;
}

legend {
	font-weight: bold;
    padding-left: .5em;
    padding-right: .5em;
}

fieldset {
    padding : 1em;
    border : 1px solid;    
    margin : 1em 1em 2em 1em;
}

fieldset legend {
    padding : 0px 2em 0px 2em;
    font-weight : bold;  
    text-align: center;
    border : 1px solid;
}




/* --------------------------------------------------------------------------- */
/* Classes pour la mise en page globale  */
.alignToCenter {
	text-align: center;		
}
.alignToLeft {
	text-align: left;		
}
.alignToRight {
	text-align: right;		
}
.useMaxWidth {
	width: 100%;		
}
.useHalfWidth {
	width: 50%;		
}
.useMinWidth {
	width: 1%;		
}
.nowrap {
	white-space:nowrap;	
}
.wrap {
	width: 1%;	
}
.autoscroll {
	overflow: auto;	
}




/* --------------------------------------------------------------------------- */
/** Classes pour definir des zones de l'ecran */

.cadre {
	border: 1px #9c9c9c solid;
}

.box {
	background-color: #FFFFFF;
	border: 1px #9c9c9c solid;
}

.boxTitre {
	background-color: #9c9c9c;
	text-align: left;	
	font-weight: bold;	
	padding: 1px;
	padding-left: 4px;
}

.subBox {
	background-color: #FFFFFF;
	border: 1px #c0c0c0 solid;
}

.subBoxTitre {
	background-color: #c0c0c0;
	text-align: left;	
	font-weight: bold;	
	padding: 1px;
	padding-left: 4px;
}

/* --------------------------------------------------------------------------- */
/** Classes pour definir des types de message */

.blocInfo {
	background-color: #fbffc0;
	height: auto;
	width: auto;
	margin: 5px 5px 5px 5px;
	position: relative;
	text-align: center;		
}

.blocWarning {
	background-color: #ebe18d;
	height: auto;
	width: auto;
	margin: 5px 5px 5px 5px;
	position: relative;
	text-align: center;		
}

.blocInstruction {
	background-color: #ffd7bf;
	height: auto;
	width: auto;
	margin: 5px 5px 5px 5px;
	position: relative;
	text-align: center;		
}

.blocErreur {
	background-color: #FF0000;
	color: white;
	height: auto;
	width: auto;
	margin: 5px 5px 5px 5px;
	position: relative;
	text-align: center;		
}


.blocCentre {
	height: auto;
	width: auto;
	margin: 5px 5px 5px 5px;
	position: relative;
	text-align: center;		
	padding: 2px;
}

.cartouche {
	-moz-border-radius : 20px;
	padding: 2px 30px 2px 10px;	
	margin-right: -20px;
}


/* --------------------------------------------------------------------------- */
/* Barre d'outils */
.cktlAjaxToolbar {
  background: #c0c0c0; 
  vertical-align: middle;
  text-align: left;
  border-size: 0px;
  padding-top: 2px;
  padding-bottom: 2px;
  padding-left: 2px;
  padding-right: 2px;
}

.cktlAjaxToolbar img {
  vertical-align: middle;
}
/* --------------------------------------------------------------------------- */
/* Composant autocomplete */

.autocompletefield {
  background: #FFFFFF url(images/find.png) no-repeat scroll left top;
  padding-left: 20px;
  text-align: left;
}
.autocompletefield .disabled {
  background: #FFFFCC url(images/find.png) no-repeat scroll left top;
}
.autocompletefield_busy {
  background:#FFFFFF url(images/indicator_arrows.gif) no-repeat scroll left top;
  text-align: left;
  padding-left: 20px;
}

.auto_complete {
  background-color: transparent;
  font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
 font-size: 11px;
 font-weight: bold;
}
.auto_complete ul {
    border:1px solid #888;
    background-color: #fff;
    margin:0;
    padding:0;
    list-style-type: none;
    height: 150px;
    overflow: hidden;
    overflow-y: auto;
    display: block;
}
.auto_complete ul li {
    margin:0;
    padding:3px;
    display: block;
}
.auto_complete ul li.selected {
    background-color: #c0c0c0;
    color: #FF0000;
}
.auto_complete ul li.nullSelection {
    background-color: #FFFFFF;
    color: #FF0000;
    text-decoration: blink;
}
.auto_complete ul strong.highlight {
    color: #800;
    margin:0;
    padding:0;
}
/* --------------------------------------------------------------------------- */
/* Tableviews */
.cktlajaxtableview {
	overflow: hidden;
	overflow-x: hidden;
	padding: 5px;
	width: auto;
	border: 1px solid;
}

.cktlajaxtableview>div#CktlAjaxTableView_DivBarreDeNavigation {
  border: 1px solid ;
}
.cktlajaxtableview>div#CktlAjaxTableView_DivBarreDeNavigation table  {
  border: none;
  font-size: 8pt;
  width:100%;
}
.cktlajaxtableview>div#CktlAjaxTableView_DivBarreDeNavigation table tbody {
}
.cktlajaxtableview>div#CktlAjaxTableView_DivBarreDeNavigation table tbody tr td {
  border-left-width: 0;
  border-right-width: 0;
}
.cktlajaxtableview>div#CktlAjaxTableView_DivBarreDeNavigation table tr {
}
.cktlajaxtableview>div#CktlAjaxTableView_DivBarreDeNavigation table td {
  font-size: 8pt;
}

.cktlajaxtableview>div#CktlAjaxTableView_DivHeaderAndBody {
  border: 1px solid;
  overflow-y: scroll;
  overflow-x: auto;
}
.cktlajaxtableview>div#CktlAjaxTableView_DivHeaderAndBody table {
	width: 100%;
}

.cktlajaxtableview>div#CktlAjaxTableView_DivHeaderAndBody table tbody {
	overflow: hidden;
	overflow-x: hidden;
	overflow-y: hidden;	
}
.cktlajaxtableview>div#CktlAjaxTableView_DivHeaderAndBody table tbody tr.odd {
  background-color: #FFFFFF;
}
.cktlajaxtableview>div#CktlAjaxTableView_DivHeaderAndBody table tbody tr.even {
  background-color: #E0E0E0;
}

.cktlajaxtableview>div#CktlAjaxTableView_DivHeaderAndBody table tbody tr:hover {
	color: #FFFFFF;	
	background-color: #c0c0c0;
}

.cktlajaxtableview>div#CktlAjaxTableView_DivHeaderAndBody table tbody tr.selected {		
  background-color: #c0c0c0;
}

.cktlajaxtableview>div#CktlAjaxTableView_DivHeaderAndBody table thead tr th {
  border-top: 1px solid #555555;
  border-bottom: 1px solid #555555;
  border-left: 1px solid #DBDBDB;
  border-right:1px solid #A3A3A3;
  background-color: #EFEFEF;
  padding-left: 3px;
  padding-right: 3px;
}
.cktlajaxtableview>div#CktlAjaxTableView_DivHeaderAndBody table tbody tr td {
  border-left: 1px solid #FFFFFF;
  border-right: 1px solid #D9D9D9;
  padding-left: 3px;
  padding-right: 3px;
}

.cktlajaxtableview>div#CktlAjaxTableView_DivHeaderAndBody table tfoot tr th {
  border-top: 1px solid #555555;
  border-left: 1px solid #DBDBDB;
  border-right:1px solid #A3A3A3;
  background-color: #EFEFEF;
}

.cktlajaxtableview>div#CktlAjaxTableView_DivBarreDeStatut {
  border: 1px solid;
}
.cktlajaxtableview>div#CktlAjaxTableView_DivBarreDeStatut table  {
  border: none;
  font-size: 8pt;
  width:100%;
}
.cktlajaxtableview>div#CktlAjaxTableView_DivBarreDeStatut table tbody {
}
.cktlajaxtableview>div#CktlAjaxTableView_DivBarreDeStatut table tbody tr td {
  border-left-width: 0;
  border-right-width: 0;
}
.cktlajaxtableview>div#CktlAjaxTableView_DivBarreDeStatut table tr {
}
.cktlajaxtableview>div#CktlAjaxTableView_DivBarreDeStatut table td {
  font-size: 8pt;
}

.cktlajaxtableview .updated {
	text-shadow: rgb(203, 212, 75) 0em 0em 0.3em;
	color: rgb(188, 37, 45);
}

.cktlajaxtableview .triable {
	text-decoration: underline;
	cursor: pointer;
}

.cktlajaxtableview .triAscending {
  background: transparent url(images/Sort_Up_H.png) no-repeat scroll right center;
  padding: 0 7 0 0;
	cursor: pointer;
}
.cktlajaxtableview .triDescending {
  background: transparent url(images/Sort_Down_H.png) no-repeat scroll right center;
  padding: 0 7 0 0;
	cursor: pointer;
}
.cktlajaxtableview #PremierePage {
  background: transparent url(images/BeginningPressed.png) no-repeat scroll center center;
  border: none;
  width: 20px;
  height: 19px;
}
.cktlajaxtableview #PagePrecedente {
  background: transparent url(images/BackwardPressed.png) no-repeat scroll center center;
  border: none;
  width: 20px;
  height: 19px;
}
.cktlajaxtableview #PageSuivante {
  background: transparent url(images/ForwardPressed.png) no-repeat scroll center center;
  border: none;
  width: 20px;
  height: 19px;
}
.cktlajaxtableview #DernierePage {
  background: transparent url(images/EndPressed.png) no-repeat scroll center center;
  border: none;
  width: 20px;
  height: 19px;
}


/* --------------------------------------------------------------------------- */
/* Arbres */
ul.tree {
	margin: 0px;
	padding: 0px;
}
ul.tree ul {
	margin: 0px;
	padding: 0px 0px 0px 19px;
}
ul.tree li {
	list-style: none;
}
ul.tree li a {
	text-decoration: none;
	 font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
}
ul.tree img.nodeControl {
	border: none;
	vertical-align: middle;
}

.tree {
	background-color: #FFFFFF;
	border: 1px;
	border-style: solid;
	border-color: #c0c0c0; 
	overflow: hidden;
	overflow-y: auto;
	text-align: left;
	color: #868686;
}

.cktlajaxtree_collapsed {
	background: url(../images/ico_node_collapsed_gris_16.png) 0 0;
}
.cktlajaxtree_expanded {
	background: url(../images/ico_node_expanded_gris_16.png) 0 0;
}
.cktlajaxtree_leaf {
	background: url(../images/ico_node_leaf_gris_16.png) 0 0;
}


/* --------------------------------------------------------------------------- */
/* Boutons */

.cktl_inline_block {
	position: relative;
	display: inline-block;
}

a.cktl_button div:hover, a.cktl_button:hover {
	border-color: #666666;
	color: #000000;
	text-decoration: none;
}
.cktl_button {
	margin-left: 4px;
	color: #003366;
	cursor: pointer;
}
.cktl_button_disabled {
	cursor: default;
}

.cktl_button_base {
	-x-system-font: none;
	font-family: Arial,sans-serif;
	font-size: 1.3em;
	font-size-adjust: none;
	font-stretch: normal;
	font-style: normal;
	font-variant: normal;
	font-weight: normal;
	letter-spacing: normal;
	line-height: 1;
	margin: 0 1px;
	outline-color: -moz-use-text-color;
	outline-style: none;
	outline-width: medium;
	text-align: center;
	text-indent: 0;
	text-transform: none;
	vertical-align: baseline;
	white-space: nowrap;
	word-spacing: normal;
}

.cktl_button_base_outer_box {
	border-bottom:1px solid #AAAAAA;
	border-top:1px solid #BBBBBB;
}
.cktl_button_base_inner_box {
	-moz-background-clip:border;
	-moz-background-inline-policy:continuous;
	-moz-background-origin:padding;
	background:#E3E3E3 none repeat scroll 0 0;
	border-left:1px solid #BBBBBB;
	border-right:1px solid #AAAAAA;
	margin:0 -1px;
}
.cktl_button_base_pos {
	height:100%;
	position:relative;
}

.cktl_button_base_top_shadow {
	-moz-background-clip:border;
	-moz-background-inline-policy:continuous;
	-moz-background-origin:padding;
	background:#F9F9F9 none repeat scroll 0 0;
	border-bottom:0.23em solid #EEEEEE;
	height:0.692em;
	left:0;
	overflow:hidden;
	position:absolute;
	right:0;
	top:0;
}
.cktl_button_base_content {
	color:#333333;
	line-height:0.95em;
	padding:0.141em 0.461em;
	position:relative;
	text-align:center;
	font-weight: bold;
}

.cktl_button_icon {
background-repeat:no-repeat;
height:16px;
margin:0 1px 0 0;
vertical-align:middle;
}

/* --------------------------------------------------------------------------- */
/* Boutons d'actions */

.cktl_action_button {
	font-weight: bold;
	width: 16px;
}

/* --------------------------------------------------------------------------- */
/* Actions */

.cktl_action_new {
	background: transparent url(../images/16x16/001_01_16x16.png) no-repeat scroll 0 0;
}
.cktl_action_delete {
	background: transparent url(../images/16x16/001_49_16x16.png) no-repeat scroll 0 0;
}
.cktl_action_update {
	background: transparent url(../images/16x16/001_39_16x16.png) no-repeat scroll 0 0;
}
.cktl_action_find {
	background: transparent url(../images/16x16/001_38_16x16.png) no-repeat scroll 0 0;
}
.cktl_action_print {
	background: transparent url(../images/16x16/001_38_16x16.png) no-repeat scroll 0 0;
}
.cktl_action_edit {
	background: transparent url(../images/16x16/001_45_16x16.png) no-repeat scroll 0 0;
}
.cktl_action_previous {
	background: transparent url(../images/16x16/001_23_16x16.png) no-repeat scroll 0 0;
}
.cktl_action_next {
	background: transparent url(../images/16x16/001_21_16x16.png) no-repeat scroll 0 0;
}
.cktl_action_cancel {
	background: transparent url(../images/16x16/001_05_16x16.png) no-repeat scroll 0 0;
}
.cktl_action_etoile {
	background: transparent url(../images/16x16/001_15_16x16.png) no-repeat scroll 0 0;
}



/* --------------------------------------------------------------------------- */
/* Login */
.login {
  background-color: #FFFFFF;
  position: absolute;
  width: 400px;
  height: 170px;
  left: 50%;
  margin-left: -200px;
  top: 50%;
  margin-top: -85px;
  text-align: center;
  border: 2px solid #999999;
  font-size: 11px;
}
.login_label, .login_input, .login_error  {
  padding:10px;
  text-align:left;
  float:left; 
  width:150px;
  font-size:14px;
}

.login_input {
  width:200px;
}

.login_error {
  width:auto;
  color:#F00;
  text-decoration: blink;
  float: none;
}

.login_input input {
  width:auto;
}



