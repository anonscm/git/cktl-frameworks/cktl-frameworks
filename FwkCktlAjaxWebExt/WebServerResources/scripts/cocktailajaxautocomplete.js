Object.extend(Ajax.Autocompleter.prototype, {
	
  fixIEOverlapping: function() {
    Position.clone(this.update, this.iefix, {setLeft: true, setTop:(!this.update.style.height)});
    this.iefix.style.zIndex = 1;
    this.update.style.zIndex = 2;
    Element.show(this.iefix);
  },
  
  onBlur: function(event) {
    if (!Prototype.Browser.IE) {
	    // needed to make click events working
	    setTimeout(this.hide.bind(this), 250);
	    this.hasFocus = false;
	    this.active = false;   
	}  
  }, 

  onKeyPress: function(event) {
  	// console.log("onKeyPress: ",event," : ",this.element.value);
    if(this.active) {
      switch(event.keyCode) {
       case Event.KEY_TAB:
       case Event.KEY_RETURN:
         this.selectEntry();
         Event.stop(event);
       case Event.KEY_ESC:
         this.hide();
         this.active = false;
         Event.stop(event);
         return;
       case Event.KEY_LEFT:
       case Event.KEY_RIGHT:
         return;
       case Event.KEY_UP:
         this.markPrevious();
         this.render();
         Event.stop(event);
         return;
       case Event.KEY_DOWN:
         this.markNext();
         this.render();
         Event.stop(event);
         return;
      }
    } else {
      	if(event.keyCode==Event.KEY_DOWN && this.options.minChars-1<=0) {
     		this.getUpdatedChoices();
     	} else {
	    	if(event.keyCode==Event.KEY_TAB || event.keyCode==Event.KEY_RETURN || 
	         (Prototype.Browser.WebKit > 0 && event.keyCode == 0))  {
	  				// console.log("onKeyPress: KEY_TAB ou KEY_RETURN");
		       	if (this.element.value.length == 0) {
		         	this.setSelectionToNullValue();
					    if (this.options.afterUpdateElement)
					    	this.options.afterUpdateElement(this.element);
			         	// Event.stop(event);
		       	} else {
			         	// Event.stop(event);
			         	this.lastEvent = event;
		       	}
		       	return;
	     	} 
     	} 
    }
    this.changed = true;
    this.hasFocus = true;

    if(this.observer) clearTimeout(this.observer);
      this.observer = 
        setTimeout(this.onObserverEvent.bind(this), this.options.frequency*1000);
  },

  markPrevious: function() {
    if(this.index > 0) this.index--
      else this.index = this.entryCount-1;
    if (Prototype.Browser.IE) {
	    var li = this.getEntry(this.index);
	    var div = li.offsetParent;
	    var scroll = eval(li.scrollHeight*(this.index));
	    var divHeight = div.clientHeight;
	    if (div.scrollTop==0 && scroll>divHeight) {
	      div.scrollTop = eval(scroll-divHeight+li.scrollHeight);
	    } else if (scroll<div.scrollTop) {
	      if (eval(divHeight-scroll)<li.scrollHeight) {
	        div.scrollTop = scroll;
	      } else {
	        div.scrollTop = scroll;
	      }
	    }
    } else {
      this.getEntry(this.index).scrollIntoView(true);
    }
  },

  markNext: function() {
    if(this.index < this.entryCount-1) this.index++
      else this.index = 0;
    if (Prototype.Browser.IE) {
	    var li = this.getEntry(this.index);
	    var div = li.offsetParent;
	    var scroll = eval(li.scrollHeight*(this.index+1));
	    var divHeight = div.clientHeight;
	     if (scroll>divHeight) {
	      if (eval(scroll-divHeight)<li.scrollHeight) {
	        div.scrollTop = eval(scroll-divHeight);
	      } else {
	        div.scrollTop += li.scrollHeight;
	      }
	    } else {
	      div.scrollTop=0;
	    }
    } else {
      this.getEntry(this.index).scrollIntoView(false);
    }
  },

  selectEntry: function() {
  	// console.log("selectEntry Deb");
    this.active = false;
    var currentEntry = this.getCurrentEntry();
    this.updateElement(currentEntry);
    if (currentEntry.hasClassName('nullSelection')==false) {
    	this.getUpdatedIndex();
    }
  	// console.log("selectEntry Fin");
  },

	setSelectionToNullValue: function() {
    this.active = false;
    this.startIndicator();
    var entry = encodeURIComponent(this.options.paramName) + '=' + encodeURIComponent(this.element.value);

    this.options.parameters = 'selectedIndex=-1';
    if(this.options.defaultParams) 
      this.options.parameters += '&' + this.options.defaultParams;
    
/*    new Ajax.Request(this.url, this.options);					
    if (this.options.updateContainerId) {
			eval(this.options.updateContainerId+'Update()');
    }*/
        if (this.options.updateContainerId) {
			// eval(this.options.updateContainerId+'Update()');
	    var elementID = this.url.substring(this.url.lastIndexOf("/")+1,this.url.length);
	    var id = this.options.updateContainerId;
			// var actionUrl = $(id).getAttribute('updateUrl');
			// actionUrl = actionUrl.addQueryParameters('selectedIndex=-1');
			AUL._update(id, $(id).getAttribute('updateUrl'), this.options, elementID, null);
    } else {
    	//new Ajax.Request(this.url, this.options);
    	
        if (this.options.updateContainerID) {
				// eval(this.options.updateContainerId+'Update()');
		    var elementID = this.url.substring(this.url.lastIndexOf("/")+1,this.url.length);
		    var id = this.options.updateContainerID;
				// var actionUrl = $(id).getAttribute('updateUrl');
				// actionUrl = actionUrl.addQueryParameters('selectedIndex=-1');
				AUL._update(id, $(id).getAttribute('updateUrl'), this.options, elementID, null);
	    } else {
	    	new Ajax.Request(this.url, this.options);
	    }
 
    }
    
	},
		
  getUpdatedIndex: function(theIndex) {
  	// console.log("getUpdatedIndex Deb: ",this.index);
    var entry = encodeURIComponent(this.options.paramName) + '=' + encodeURIComponent(this.element.value);

    if (theIndex) {
    	this.options.parameters = 'selectedIndex=' + theIndex;
    } else {
    	this.options.parameters = 'selectedIndex=' + this.index;
    }
    if(this.options.defaultParams) 
      this.options.parameters += '&' + this.options.defaultParams;
    
    // new Ajax.Request(this.url, this.options);
    if (this.options.updateContainerId) {
			// eval(this.options.updateContainerId+'Update()');
	    var elementID = this.url.substring(this.url.lastIndexOf("/")+1,this.url.length);
	    var id = this.options.updateContainerId;
			var actionUrl = $(id).getAttribute('updateUrl');
			actionUrl = actionUrl.addQueryParameters('selectedIndex='+this.index);
			AUL._update(id, $(id).getAttribute('updateUrl'), this.options, elementID, null);
    } else {
    	//new Ajax.Request(this.url, this.options);
    	   if (this.options.updateContainerID) {
	   			// eval(this.options.updateContainerId+'Update()');
	   	    var elementID = this.url.substring(this.url.lastIndexOf("/")+1,this.url.length);
	   	    var id = this.options.updateContainerID;
	   			var actionUrl = $(id).getAttribute('updateUrl');
	   			actionUrl = actionUrl.addQueryParameters('selectedIndex='+this.index);
	   			AUL._update(id, $(id).getAttribute('updateUrl'), this.options, elementID, null);
	       } else {
	       	new Ajax.Request(this.url, this.options);
	       }
	    	
    }
  	// console.log("getUpdatedIndex Fin");
  },
  
  startIndicator: function() {
    if(this.options.classSearchIndicator) Element.addClassName(this.element,this.options.classSearchIndicator);
  },

  stopIndicator: function() {
    if(this.options.classSearchIndicator) Element.removeClassName(this.element,this.options.classSearchIndicator);
  },
  onComplete: function(request) {
  	// console.log("onComplete Deb: ",this.active);
		this.stopIndicator();
  	if (typeof(request.request.options.parameters.selectedIndex)=='undefined') {
  				// console.log("onComplete Deb1: ",request.responseText);
  				this.hasFocus = true;
    		this.updateChoices(request.responseText);
	  		if (this.active==false) {
  				// console.log("onComplete Deb2: ",this.update);
  				// console.log("onComplete Deb3: ",this.update.firstChild);
  				// console.log("onComplete Deb4: ",this.update.firstChild.childNodes[0]);
  				if (this.getEntry(0)) {
  					// La requete a renvoye des resultats mais n'a pas eu le temps d'afficher la liste
  					if (this.options.autoSelect==false || this.entryCount>1) {
  						// Nbre de selections possibles > 1 ou autoSelect non actif, on affiche la liste
  						this.activate();
  						this.active = true;
  						this.element.focus();
  					} else {
  						// Une seule selection possible, on selectionne l'element si autoSelect=true
  						this.getUpdatedIndex(0);
  					}
  				} else {
  					// Aucune selection possible, on push la valeur null
  					this.getUpdatedIndex(-1);
  				}
  			} else {
  						this.element.focus();
  			}
  	} else {
		  this.hide();
  	}
  	// console.log("onComplete Fin: ",this.active);
  }
  
})

