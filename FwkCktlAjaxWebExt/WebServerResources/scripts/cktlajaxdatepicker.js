function onSelectCktl(cal) {
	var p = cal.args;
	if (cal.inputField != null) {
		var form = cal.inputField.form;
	    if (form != null) {
	    	var updateContainerID = p.updateContainerID;
			if (updateContainerID == "undefined" || updateContainerID.length < 1) {
	      		updateContainerID = null;
	      	}
	        ASB.partial(updateContainerID, cal.inputField.id, null);
	    }      
		cal.hide();
	}
  }
