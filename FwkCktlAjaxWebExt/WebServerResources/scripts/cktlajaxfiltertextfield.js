var CktlAjaxFilterTextField = Class.create({
    initialize: function(filterField, submitFunction, isFilterAutomatic, timeout) {
        this.filterField = filterField;
        this.submitFunction = submitFunction;
        this.isFilterAutomatic = isFilterAutomatic;
        this.timeout = timeout;
        this.setHandlers();
    },

    setHandlers: function() {
        this.handlerKeypress();
        this.handlerKeyup();
        this.handlerPaste();
    },

    /**
     * le cas classique : les touches alphanumériques (c'est l'event keypress)
     */
    handlerKeypress: function() {
    	var that = this;
        jQuery('body').off('keypress', this.filterField).on('keypress', this.filterField, function(e) {
        	if (that.isFilterAutomatic === true) {
        		if (that.isNotToucheDirectionnelle(e)) {
        			that.submit();
        		}
        	}
        	else {
	        	if (that.isEnter(e)) {
	        		that.submit();
	        	}
        	}
        });
    },
        
    /**
     * les autres touches : on soumet uniquement sur le backspace
     */
    handlerKeyup: function() {
    	var that = this;
        jQuery('body').off('keyup', this.filterField).on('keyup', this.filterField, function(e) {
        	if (that.isFilterAutomatic === true) {
	            if (that.isBackspace(e)) {
	                that.submit();
	            }
	        }
        });
    },
    
    /**
     * enfin le cas du copy/past
     */
    handlerPaste: function() {
    	var that = this;
        jQuery('body').off('paste', this.filterField).on('paste', this.filterField, function(e) {
        	if (that.isFilterAutomatic === true) {
            	that.submit();
            }
        });
    },

    submit: function() {
        if (this.timeout !== undefined) {
            this.submitAfterTimeout();
        } else {
            this.submitFunction();
        }
    },
    
    submitAfterTimeout: function() {
        if ((window.timeoutObject !== undefined) && (window.timeoutObject != null)) { 
            window.clearTimeout(window.timeoutObject); 
        }
        timeoutObject = window.setTimeout(this.submitFunction, this.timeout);
    },
    
    /**
     * rajout nécessaire pour Firefox qui déclenche quand même
     * le keypress pour les touches directionnelles...
     */
    isNotToucheDirectionnelle: function(e) {
    	return !(e.keyCode == 37 || e.keyCode == 39);
    },
    
    isBackspace: function(e) {
    	return e.which == 8;
    },
    
    isEnter: function(e) {
    	return e.keyCode == 13;
    }

});