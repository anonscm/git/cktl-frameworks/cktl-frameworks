var SimpleMenu = Class.create({
	initialize: function(idButton, idDdown) {
		this.idButton = idButton;
		this.idDdown = idDdown;
		this.isMenuShown = false;
	},

	hideMenu: function(event) {
		var ddown = $(this.idDdown);
		if (ddown != null) {
			ddown.hide();
			this.isMenuShown = false;
		}
	},
	
	hideMenuOnEsc: function(event) {
        if (event.keyCode == 27) {
            this.hideMenu();
        }
	},

	showMenu: function(event) {
		var ddown = $(this.idDdown);
		if (ddown != null) {
			if (!this.isMenuShown) {
				ddown.show();
				event.stop();
				this.isMenuShown = true;
			} else {
				ddown.hide();
				this.isMenuShown = false;
			}
		}
	},

	addSimpleMenuObservers: function() {
		$$('body')[0].observe('click', this.hideMenu.bind(this));
		$$('body')[0].observe('keyup', this.hideMenuOnEsc.bind(this));
		$(this.idButton).observe('click', this.showMenu.bind(this));
		$(this.idDdown).observe('click', function(event) {
			event.stop();
		});
	}

});

function dummy(url) {
	// nuthing
}
