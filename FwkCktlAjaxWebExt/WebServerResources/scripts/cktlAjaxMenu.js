function displayInPlaceholder(id, nomApplication){
	if(nomApplication == "") {
		jQuery('.placeholder').html(jQuery('#'+id).html());
	} else {
		jQuery('.placeholder').html(jQuery('#'+id).html() + 
		"<div class='nomApplicationMenu'><p class='ombreInterieureTexte' data-title='" + nomApplication +"'><span class='premiereLettreNomApplicationMenu'>"+nomApplication[0]+"</span>"+nomApplication.substr(1)+"</p></div>");
	}
}

function setSelection(id, unselectedClass, selectedClass){
   
    jQuery("li."+ selectedClass).each(function(){
        jQuery(this).removeClass(selectedClass);
        jQuery(this).addClass(unselectedClass);
    });
    
    if (jQuery('#'+id).hasClass(unselectedClass)){
        jQuery('#'+id).removeClass(unselectedClass);
        jQuery('#'+id).addClass(selectedClass);
    }
}