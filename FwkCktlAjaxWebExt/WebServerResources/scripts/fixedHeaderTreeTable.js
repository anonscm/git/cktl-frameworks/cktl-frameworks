var TREE_TABLE_HEADER = {

	resetScroll : function() {
		jQuery('.fixed-table-container-inner').scrollTop(jQuery.treetable.scrollTop);
	},

	useFixedHeader: function() {

    	jQuery(document).ready(function() {
			resizeTreeTable();	
			if(jQuery.treetable == undefined) {
				jQuery.treetable = {}; 
			}
			jQuery('.fixed-table-container-inner').scroll(function() {
				var st =  jQuery(this).scrollTop();
				jQuery.treetable.scrollTop = st;
			});   
			
			jQuery(".treetable a").on("click", function() {
				resizeTreeTable();
			});	
	    });
	    
    	jQuery(window).resize(function() {
			resizeTreeTable();	    	
	    });
	    
	    
	    function resizeTreeTable() {
	    	var tableWidth = jQuery("#tree").width(); //taille de la table sans la scrollbar de droite

			/** Calcul de la largeur de chaque class th-inner **/
	        jQuery(".th-inner").each(function(){ // affectation da la taille du th au div pour pouvoir centrer le texte
	            var widthTh = jQuery(this).parent().width();
	            jQuery(this).css('width', widthTh);
	          });
	
	        /** Reinitialisation valeurs **/
	        var paddingSpan = 4;
	        var defaultHeightHeaderComplexTop = 25;
	        var defaultHeightHeaderComplexBottom = 0;
	        var defaultHeightHeaderDouble = 25;
	        jQuery(".complex-bottom .th-inner").css("top", defaultHeightHeaderComplexTop);
	        jQuery('.complex-bottom th div').css("height", defaultHeightHeaderComplexBottom);
	        jQuery('.complex-top th[colspan]').find("div").css("height", defaultHeightHeaderComplexTop);
	
	        /** header complexe top **/
	        var maxHeightHeaderComplexTop = defaultHeightHeaderComplexTop;
	        jQuery('.complex-top th[colspan] span').each(function() {
	          if (jQuery(this).height() + paddingSpan > maxHeightHeaderComplexTop) {
	            maxHeightHeaderComplexTop = jQuery(this).height() + paddingSpan;
	          }
	        });
	
	        /** header complexe bottom **/ 
	        var maxHeightHeaderComplexBottom = defaultHeightHeaderComplexBottom;
	        jQuery('.complex-bottom th span').each(function() {
	          if (jQuery(this).height() + paddingSpan > maxHeightHeaderComplexBottom) {
	            maxHeightHeaderComplexBottom = jQuery(this).height() + paddingSpan;
	          }
	        });
	
	        /** header double **/
	        var maxHeightHeaderDouble = defaultHeightHeaderDouble;
	        jQuery('.complex-top th').not('[colspan]').each(function() {
	          if (jQuery(this).find("span").height()+paddingSpan > maxHeightHeaderDouble) {
	            maxHeightHeaderDouble = jQuery(this).find("span").height()+paddingSpan;
	          }
	        });
	
	        /** recalcul de l'affichage du header **/
	        var maxHeightHeaderComplex = maxHeightHeaderComplexBottom + maxHeightHeaderComplexTop;
	        var heightHeader = Math.max(maxHeightHeaderComplex, maxHeightHeaderDouble);
	
	        jQuery(".header-background").css("height", heightHeader); // taille du header
	        jQuery(".complex.fixed-table-container").css("padding-top", heightHeader-5); // padding pour que la table commence sous le header
	
	        jQuery(".complex-top th").not("[colspan]").each(function() { // header Double
	          jQuery(this).find("div").css("height", heightHeader);
	        });
	
			
	        if (maxHeightHeaderComplexTop > defaultHeightHeaderComplexTop) { // header complexe top
	          jQuery('.complex-top th[colspan]').each(function() {
	            jQuery(this).find("div").css("height", maxHeightHeaderComplexTop);
	          });
	          jQuery(".complex-bottom .th-inner").css("top", maxHeightHeaderComplexTop); // abaisse les headers complexe bottom
	        } else if (maxHeightHeaderComplexBottom == 0) { // une seule ligne dans le header
	    		jQuery(".complex-top th div.th-inner").css("height", heightHeader);
	    	}
	
	        if (maxHeightHeaderComplexBottom > defaultHeightHeaderComplexBottom) { // header complexe bottom
	          jQuery('.complex-bottom th div').each(function() {
	            jQuery(this).css("height", maxHeightHeaderComplexBottom);
	          });
	        }
	
	        if (maxHeightHeaderDouble > maxHeightHeaderComplex) { // si le header double est plus grand qu'un header complexe alors on prolonge les headers complexes bottom'
	          var newHeight = maxHeightHeaderDouble - maxHeightHeaderComplexTop;
	        	jQuery('.complex-bottom th div').css("height", newHeight);    
	    	}
	    	
	    	/** Centrage vertical dans le header **/
	    	jQuery(".th-inner").each(function() {
	    		var heightDiv = jQuery(this).height();
	    		var heightSpan = jQuery(this).find("span").height();
	    		var positionTop = Math.floor((heightDiv - heightSpan)/2) -2;
	    		
	    		jQuery(this).find("span").css("position", "relative");
	    		jQuery(this).find("span").css("top", positionTop);
	    	});
	    }
	}
}