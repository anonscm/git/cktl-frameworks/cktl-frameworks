(function() {
  var module;

  module = angular.module('cktlButton', []);
	module.directive('cktlButton', function() {
      return {
        restrict: 'E',
        template: '<a class="cktl_button" ng-click="disabled ? null : action()" ng-class="{cktl_ngbutton_disabled : disabled}"><div class="cktl_inline_block cktl_button cktl_button_base"><div class="cktl_inline_block cktl_button_base_outer_box"><div class="cktl_inline_block cktl_button_base_inner_box"><div class="cktl_button_base_pos"><div class="cktl_button_base_top_shadow">&nbsp;</div><div class="cktl_button_base_content"><span class="cktl_action_button cktl_action_{{type}} cktl_button_icon cktl_inline_block"></span><span>&nbsp;{{label}}</span></div></div></div></div></div></a>',
        replace: true,
        scope: {
          label: '@',
          action:'&',
          type:'@',
          disabled :'='
        }
	  };
	});
}).call(this);