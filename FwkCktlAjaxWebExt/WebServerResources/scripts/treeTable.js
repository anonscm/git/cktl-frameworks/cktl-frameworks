var TREE_TABLE = {

	init : function(treeTableId, size) {
		jQuery(document).ready(function(){
			var persistStore = new Persist.Store(treeTableId);
			
			var initialized = false;
			
			if (persistStore.get("initialized") != null) {
				initialized = true;
			}
			
			if (initialized == false) {
	    		for (var i = 0; i < size; i++){
		    		if (jQuery("table#" + treeTableId + " tr[data-tt-id=" + i + "]").hasClass("expanded")) {
		    			persistStore.set(i, '1');   
		    		}
	    		}
	    		persistStore.set("initialized", '1');
	    	}
	    	
			jQuery("#" + treeTableId).agikiTreeTable({
				initialState: "expanded",
				persist: true,
				reinitPersist: true,
				persistStoreName: treeTableId
			});

		});
		
		jQuery("table#" + treeTableId + " tbody tr").mousedown(function() {    	    
    	    if (!jQuery(this).hasClass("notSelectable")) { 
      		    jQuery("table#" + treeTableId + " tr.selected").removeClass("selected"); // Deselect currently selected rows
      		    jQuery(this).addClass("selected");
      		}
    	});
    	
	},
	
	reinitPersist : function() {
		// r�initialisation de l'arbre
		// comme on ne peut pas supprimer le Persist.Store
		// on remet les noeuds � l'etat 1 pour pouvoir afficher l'arbre d�pli�
		// si on souhaite avoir l'arbre non depli� il faut remplacer par : persistStore.remove(i);
		// la taille du Persist a 10000 est compl�tement arbitroire car on ne sait pas combien de noeud il a enregistr� et combien il en contient
		// on s'est donc dit que 10000 �tait largement suffisant mais si une autre m�thode existe!!! pas de soucis
		var persistStore = new Persist.Store("orgcocktailtreetable");
	    for (var i=0; i< 10000; i++){
	        persistStore.set(i, '1');   
	    }
	},
    
}

function selectedRow(tr) {

	//alert("12");
	//oElement.css("background-color", "red");
	
	var table = Element.up(tr);
        //if (Element.up(table).hasClassName('selectionMultiple') == false ) {
	        table.getElementsBySelector('tr.selected').each(function(e) {
	            e.removeClassName('selected');
	        });
        //}
        // Sélection de la ligne courante
        Element.toggleClassName(tr, 'selected');

}