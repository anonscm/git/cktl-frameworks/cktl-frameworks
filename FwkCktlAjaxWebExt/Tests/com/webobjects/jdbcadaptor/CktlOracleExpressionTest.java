package com.webobjects.jdbcadaptor;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class CktlOracleExpressionTest {

	EOEntity entity;
	
	@Before
	public void setUp() {
		entity = Mockito.mock(EOEntity.class);
	}
	
	@Test
	public void testGenerateOrderSiblingsClauseAvecUnSeulSortOrdering() {
		mockAttribute(entity, "nom");
		EOSortOrdering sortOrdering = new EOSortOrdering("nom", EOSortOrdering.CompareAscending);
		
		CktlOracleExpression oracleExpression = new CktlOracleExpression(entity);
		
		String generateOrderSiblingsClause = oracleExpression.generateOrderSiblingsClause(new NSArray<EOSortOrdering>(sortOrdering));
		assertTrue(" ORDER SIBLINGS BY NOM ASC".equals(generateOrderSiblingsClause));
	}

	private void mockAttribute(EOEntity entity, String nomAttribut) {
		EOAttribute attribute = Mockito.mock(EOAttribute.class);
		Mockito.when(entity.anyAttributeNamed(nomAttribut)).thenReturn(attribute);
		Mockito.when(attribute.columnName()).thenReturn(nomAttribut.toUpperCase());
	}
	
	@Test
	public void testGenerateOrderSiblingsClauseAvecPlusieursSortOrdering() {
		mockAttribute(entity, "nom");
		mockAttribute(entity, "prenom");
		EOSortOrdering sortOrderingNom = new EOSortOrdering("nom", EOSortOrdering.CompareAscending);
		EOSortOrdering sortOrderingPrenom = new EOSortOrdering("prenom", EOSortOrdering.CompareDescending);
		
		CktlOracleExpression oracleExpression = new CktlOracleExpression(entity);
		
		String generateOrderSiblingsClause = oracleExpression.generateOrderSiblingsClause(new NSArray<EOSortOrdering>(sortOrderingNom, sortOrderingPrenom));
		assertTrue(" ORDER SIBLINGS BY NOM ASC,PRENOM DESC".equals(generateOrderSiblingsClause));
	}

	@Test
	public void testGenerateOrderSiblingsClauseEmpty() {
		CktlOracleExpression oracleExpression = new CktlOracleExpression(entity);
		
		String generateOrderSiblingsClause = oracleExpression.generateOrderSiblingsClause(null);
		
		assertTrue("".equals(generateOrderSiblingsClause));
	}
		
	@Test
	public void testGenerateLevelClause() {
		CktlOracleExpression oracleExpression = new CktlOracleExpression(entity);
		Integer maxLevel = Integer.valueOf(10);
		
		String generateLevelClause = oracleExpression.generateLevelClause(maxLevel);
		
		String attendu = " AND LEVEL <= " + maxLevel.toString();
		assertTrue(attendu.equals(generateLevelClause));
	}
	
	@Test
	public void testGenerateLevelClauseEmpty() {
		CktlOracleExpression oracleExpression = new CktlOracleExpression(entity);
		Integer maxLevel = null;
		
		String generateLevelClause = oracleExpression.generateLevelClause(maxLevel);
		
		assertTrue("".equals(generateLevelClause));
	}
	
}
