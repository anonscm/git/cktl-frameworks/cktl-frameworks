package org.cocktail.fwkcktlajaxwebext.serveur.component.select2;

import static org.fest.assertions.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class CktlAjaxSelect2RemoteControllerTest {

    private CktlAjaxSelect2RemoteController ajaxSelect2RemoteController;
    
    @Before
    public void setUp() {
        CktlAjaxSelect2RemoteDataProvider dataProvider = new CktlAjaxSelect2RemoteDummyDataProvider();
        ajaxSelect2RemoteController = new CktlAjaxSelect2RemoteController(dataProvider) {

			@Override
			protected void callBackOnReQueteSelection() {
				// RIEN DU TOUT
			}
        	
        };
    }
    
    @Test
    public void testHandleSearch() {
        NSDictionary<String, NSArray<Object>> params = constructParams("q", "Mot clef");
        WOResponse response = ajaxSelect2RemoteController.handle(params);
        assertThat(response.contentString()).isEqualTo("[{\"id\":\"1\",\"text\":\"Je contiens le Mot clef\"}]");
    }
    
    @Test
    public void testHandleSelectionInitiale() {
        NSDictionary<String, NSArray<Object>> params = constructParams("initVal", "2");
        WOResponse response = ajaxSelect2RemoteController.handle(params);
        assertThat(response.contentString()).isEqualTo("{\"id\":\"2\",\"text\":\"Un resultat\"}");
    }
    
    @Test
    public void testHandleSelection() {
        NSDictionary<String, NSArray<Object>> params = constructParams("val", "3");
        WOResponse response = ajaxSelect2RemoteController.handle(params);
        assertThat(response.contentString()).isEqualTo("{\"id\":\"3\",\"text\":\"Un autre resultat\"}");
    }

    @Test
    public void testRequeteInconnu() {
        NSDictionary<String, NSArray<Object>> params = constructParams("nimportequoi", "999");
        WOResponse response = ajaxSelect2RemoteController.handle(params);
        assertThat(response.contentString()).isEqualTo("");
    }
    
    @Test
    public void testRequeteLanceUneException() {
        NSDictionary<String, NSArray<Object>> params = constructParams("q", "zzz");
        WOResponse response = ajaxSelect2RemoteController.handle(params);
        assertThat(response.status()).isEqualTo(500);
        assertThat(response.contentString()).isEqualTo("{\"error\":\"Something bad happened !\"}");
    }
    
    private NSDictionary<String, NSArray<Object>> constructParams(String key, String value) {
        NSDictionary<String, NSArray<Object>> params = new NSDictionary<String, NSArray<Object>>(
                new NSArray<Object>(value), key);
        return params;
    }
    
    private static class CktlAjaxSelect2RemoteDummyDataProvider implements CktlAjaxSelect2RemoteDataProvider {
        
        public Result selectionInitiale() {
            return new Result("2", "Un resultat");
        }
        
        public List<Result> results(String searchTerm) {
            List<Result> result = new ArrayList<Result>();
            if (searchTerm.equals("Mot clef")) {
                result.add(new Result("1", "Je contiens le Mot clef"));
            } else if (searchTerm.equals("zzz")) {
                throw new RuntimeException("Something bad happened !");
            }
            return result;
        }
        
        public Result onSelect(String idSelection) {
            return new Result("3", "Un autre resultat");
        }
    }
    
}
