package org.cocktail.fwkcktldroitsperimetregenerique.components.controllers;

import org.cocktail.fwkcktldroitsperimetregenerique.components.PerimetreVueGenerique;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Controleur pour PerimetreVueGenerique 
 * @author jlafourc
 *
 */
public class PerimetreVueGeneriqueController {

	private PerimetreVueGenerique component;
	
    private ERXDisplayGroup<EOGdPerimetre> perimetresAssociesDisplayGroup;
    private EOArrayDataSource perimetresAssociesDataSource;

    private ERXDisplayGroup<EOGdPerimetre> perimetresApplicationDisplayGroup;
    private ERXDatabaseDataSource perimetresApplicationDataSource;
	
	/**
	 * @param component le composant à manipuler
	 */
	public PerimetreVueGeneriqueController(PerimetreVueGenerique component) {
		this.setComponent(component);
	}

	public PerimetreVueGenerique getComponent() {
		return component;
	}

	public void setComponent(PerimetreVueGenerique component) {
		this.component = component;
	}
	
	/**
	 * @return le displayGroup pour la table view
	 */
	public ERXDisplayGroup<EOGdPerimetre> getPerimetresAssociesDisplayGroup() {
		if (perimetresAssociesDisplayGroup == null) {
			perimetresAssociesDisplayGroup = new ERXDisplayGroup<EOGdPerimetre>();
			perimetresAssociesDisplayGroup.setDataSource(getPerimetresAssociesDataSource());
		}
		return perimetresAssociesDisplayGroup;
	}
	
	/**
	 * Rafaichis les donnees dans le displayGroup des perimetres associes
	 */
	public void refreshPerimetresAssociesDisplayGroup() {
		getPerimetresAssociesDataSource().setArray(getPerimetresAssocies());
		getPerimetresAssociesDisplayGroup().fetch();
	}
	
	/**
	 * Rafaichis les donnees dans le displayGroup des perimetres de l'application
	 */
	public void refreshPerimetresApplicationDisplayGroup() {
		getPerimetresApplicationDataSource().setFetchSpecification(getPerimetresApplicationFetchSpecification());
		getPerimetresApplicationDisplayGroup().fetch();
	}
	
	/**
	 * @return la datasource pour le displayGroup
	 */
	public EOArrayDataSource getPerimetresAssociesDataSource() {
		if (perimetresAssociesDataSource == null) {
			perimetresAssociesDataSource = 
					new EOArrayDataSource(
							EOClassDescription.classDescriptionForEntityName(EOGdPerimetre.ENTITY_NAME),
							component.edc()
						); 


			
			perimetresAssociesDataSource.setArray(getPerimetresAssocies());
		}
		return perimetresAssociesDataSource;
	}
	
	public NSArray<EOGdPerimetre> getPerimetresAssocies() {
		NSArray<EOGdPerimetre> perimetresAssocies = getPerimetresAssociesFetchSpecification().fetchObjects(component.edc());
		
		// Les perimetres arrivent en double (BUG EOF ?)
		perimetresAssocies = ERXArrayUtilities.arrayWithoutDuplicates(perimetresAssocies);
		
		return perimetresAssocies;
	}
	
	/**
	 * @return créé la fectchSpec pour la datasource
	 */
	public ERXFetchSpecification<EOGdPerimetre> getPerimetresAssociesFetchSpecification() {
		EOQualifier qualifier = getPerimetresAssociesQualifier();
		ERXSortOrderings sortOrderings = EOGdPerimetre.LIBELLE.ascInsensitives();
		
		ERXFetchSpecification<EOGdPerimetre> fetchSpecification = 
				new ERXFetchSpecification<EOGdPerimetre>(
						EOGdPerimetre.ENTITY_NAME,
						qualifier,
						sortOrderings
					);
		fetchSpecification.setIncludeEditingContextChanges(true);
		
		return fetchSpecification;
	}
	
	public EOQualifier getPerimetresAssociesQualifier() {
		return EOGdPerimetre.PROFILS.containsObject(component.getProfil()).and(EOGdPerimetre.APPLICATION.eq(component.getApplication()));
	}

	public ERXDisplayGroup<EOGdPerimetre> getPerimetresApplicationDisplayGroup() {
		if (perimetresApplicationDisplayGroup == null) {
			perimetresApplicationDisplayGroup = new ERXDisplayGroup<EOGdPerimetre>();
			perimetresApplicationDisplayGroup.setDataSource(getPerimetresApplicationDataSource());
		}
		return perimetresApplicationDisplayGroup;
	}

	public ERXDatabaseDataSource getPerimetresApplicationDataSource() {
		if (perimetresApplicationDataSource == null) {
			perimetresApplicationDataSource = new ERXDatabaseDataSource(component.edc(), EOGdPerimetre.ENTITY_NAME) {

				private static final long serialVersionUID = 5983465629284420550L;

				@SuppressWarnings({ "rawtypes", "unchecked" })
				@Override
				public NSArray fetchObjects() {
					return ERXArrayUtilities.arrayWithoutDuplicates(super.fetchObjects());
				}

			};
			perimetresApplicationDataSource.setFetchSpecification(getPerimetresApplicationFetchSpecification());
		}
		return perimetresApplicationDataSource;
	}

	private EOFetchSpecification getPerimetresApplicationFetchSpecification() {
		EOQualifier qualifier = getPerimetresApplicationQualifier();
		ERXSortOrderings sortOrderings = EOGdPerimetre.LIBELLE.ascInsensitives();

		ERXFetchSpecification<EOGdPerimetre> fetchSpecification = 
				new ERXFetchSpecification<EOGdPerimetre>(
						EOGdPerimetre.ENTITY_NAME,
						qualifier,
						sortOrderings
					);
		
		return fetchSpecification;
	}

	private EOQualifier getPerimetresApplicationQualifier() {
		return EOGdPerimetre.APPLICATION.eq(component.getApplication());
	}
	
	
	/**
	 * @throws Exception 
	 */
	public void validerAjoutPerimetre() throws Exception {
		EOGdPerimetre perimetreAAjouter = getPerimetresApplicationDisplayGroup().selectedObject();
		
		if (perimetreAAjouter == null) {
			throw new Exception("Vous devez sélectionner un périmètre");
		}
		
		if (perimetresAssociesDisplayGroup.allObjects().contains(perimetreAAjouter)) {
			throw new Exception("Le périmètre sélectionné est déjà ajouté au profil");
		}	
		
		component.getProfil().addToPerimetresRelationship(perimetreAAjouter);
		
		component.edc().saveChanges();
		
		getPerimetresAssociesDataSource().insertObject(perimetreAAjouter);
		
		getPerimetresAssociesDisplayGroup().fetch();
	}
	/**
	 * 
	 * @throws Exception En cas d'erreur à la suppression
	 */

	public void supprimerPerimetreSelectionne() throws Exception {
		EOGdPerimetre perimetreASupprimer = getPerimetresAssociesDisplayGroup().selectedObject();
		
		if (perimetreASupprimer == null) {
			throw new Exception("Vous devez sélectionner un périmètre");
		}	
		
		component.getProfil().removeFromPerimetresRelationship(perimetreASupprimer);
		
		component.edc().saveChanges();
		
		getPerimetresAssociesDataSource().deleteObject(perimetreASupprimer);
		
		getPerimetresAssociesDisplayGroup().fetch();
	}
	
}
