package org.cocktail.fwkcktldroitsperimetregenerique.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsperimetregenerique.components.controllers.PerimetreVueGeneriqueController;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import com.webobjects.appserver.WOActionResults;

/**
 * @author lilia
 * Vue générique des périmètres de données
 */
public class PerimetreVueGenerique extends CktlAjaxWOComponent {
    private static final long serialVersionUID = 1L;
    
    private static final String BINDING_gdApplication = "gdApplication";
    private static final String BINDING_editingContext = "editingContext";
    private static final String BINDING_profil = "profil";
    
    private EOGdPerimetre currentPerimetreAssocie;
    private EOGdPerimetre currentPerimetreApplication;
    
    private PerimetreVueGeneriqueController controller = new PerimetreVueGeneriqueController(this);

    
	/**
     * @param context : contexte d'édition
     */
    public PerimetreVueGenerique(WOContext context) {
        super(context);
    }
    
    
    public PerimetreVueGeneriqueController getController() {
		return controller;
	}

	public String getSelectionPerimetreWindowId() {
    	return getComponentId() + "_selectionPerimetreWindow";
    }
    
	public String getPerimetresApplicationTableViewId() {
		return getComponentId() + "_perimetresApplicationTableView";
	}

	public String getPerimetresAssociesTableViewId() {
		return getComponentId() + "_perimetresAssociesTableView";
	}
	

	public String getPerimetresAssociesContainerId() {
		return getComponentId() + "_perimetresAssociesContainer";
	}
	
	@Override
	public boolean synchronizesVariablesWithBindings() {
	    return false;
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
    	controller.refreshPerimetresAssociesDisplayGroup();
    	controller.refreshPerimetresApplicationDisplayGroup();
		super.appendToResponse(response, context);
	}
	
	
	@Override
	public EOEditingContext edc() {
		return (EOEditingContext) valueForBinding(BINDING_editingContext);
	}
	

	
	/**
	 * @return l'application
	 */
	public EOGdApplication getApplication() {
		return (EOGdApplication) valueForBinding(BINDING_gdApplication);
	}
	
	public EOGdProfil getProfil() {
		return (EOGdProfil) valueForBinding(BINDING_profil);
	}
	
	
	
	public EOGdPerimetre getCurrentPerimetreAssocie() {
		return currentPerimetreAssocie;
	}
	
	public void setCurrentPerimetreAssocie(EOGdPerimetre currentPerimetreAssocie) {
		this.currentPerimetreAssocie = currentPerimetreAssocie;
	}


	public EOGdPerimetre getCurrentPerimetreApplication() {
		return currentPerimetreApplication;
	}


	public void setCurrentPerimetreApplication(EOGdPerimetre currentPerimetreApplication) {
		this.currentPerimetreApplication = currentPerimetreApplication;
	}

	/**
	 * Action de validation d'ajout d'un périmètre
	 * @return Rien
	 */
	public WOActionResults validerAjoutPerimetre() {
		try {
			controller.validerAjoutPerimetre();
			CktlAjaxWindow.close(context(), getSelectionPerimetreWindowId());
		} catch (Exception e) {
			mySession().addSimpleErrorMessage("Erreur", e);
		}
		
		return doNothing();
	}


	/**
	 * Suppression d'un périmètre
	 * @return Rien !
	 */
	public WOActionResults supprimerPerimetre() {
		try {
			controller.supprimerPerimetreSelectionne();
		} catch(Exception e) {
			mySession().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}



	
	
}