package org.cocktail.fwkcktldroitsperimetrescolarite.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsperimetrescolarite.serveur.commun.ModeEdition;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;


/**
 * Ce composant affiche la liste des perimetres et permet de les gerer
 */
public class PerimetresSrch extends FwkCktlDroitsPerimetreScolariteFormComponent {
	// Private fields
	private static final long serialVersionUID = 1L;
	private static final String BINDING_SELECTED_PERIMETRE = "selectedPerimetre";
	private static final String BINDING_GD_APPLICATION = "gdApplication";
	
	private ERXDisplayGroup<EOGdPerimetreGirofle> displayGroupPerimetres;
	private EOArrayDataSource typePerimetreDatasource;
	private EOGdPerimetreGirofle currentPerimetre;
	private EOGdPerimetreGirofle selectedPerimetre;
	
	private EOGdPerimetreGirofle perimetre;
	

	// Constructor
	/**
	 * @param context : contexte d'edition
	 */
	public PerimetresSrch(WOContext context) {
		super(context);
	}

	// Properties
	public String getPerimetresContainerId() {
		return getComponentId() + "_perimetresContainerId";
	}

	public String getTableViewContainerId() {
		return getComponentId() + "_tableViewContainerId";
	}

	public String getPerimetresTableViewId() {
		return getComponentId() + "_perimetresTableViewId";
	}
	
	public String getBoutonsContainerId() {
		return getComponentId() + "_boutonsContainerId";
	}
	
	public String getPerimetreWindowId() {
		return getComponentId() + "_ajouterPerimetreWindowId";
	}
	
	public String getBoutonsPerimetreWindowContainerId() {
		return getComponentId() + "_boutonsPerimetreWindowContainerId";
	}
	
	public Boolean isResultatsRechercheVides() {
		return getDisplayGroupPerimetres().allObjects().count() == 0;
	}
	

	public EOGdPerimetreGirofle getPerimetre() {
		return perimetre;
	}

	public void setPerimetre(EOGdPerimetreGirofle perimetre) {
		this.perimetre = perimetre;
	}

	/**
	 * @return groupe des perimetres tries par libelles.
	 */
	public ERXDisplayGroup<EOGdPerimetreGirofle> getDisplayGroupPerimetres() {
		if (displayGroupPerimetres == null) {
			displayGroupPerimetres = new ERXDisplayGroup<EOGdPerimetreGirofle>();	
			
			displayGroupPerimetres.setDataSource(getPerimetresDatasource());
			displayGroupPerimetres.setDelegate(this);
			
			EOQualifier qualifier = ERXQ.isNull(EOGdPerimetreGirofle.PERIMETRE_PARENT_ID_KEY);
			displayGroupPerimetres.setQualifier(qualifier);
	
			displayGroupPerimetres.setSelectsFirstObjectAfterFetch(true);
			displayGroupPerimetres.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupPerimetres.fetch();					
		}

		return displayGroupPerimetres;
	}

	private NSArray<EOGdPerimetreGirofle> getPerimetres() {
		
        return EOGdPerimetreGirofle.fetchAllSco_GdPerimetreGirofles(edc());
    }

	public EOGdPerimetreGirofle getCurrentPerimetre() {
		return currentPerimetre;
	}

	public void setCurrentPerimetre(EOGdPerimetreGirofle currentPerimetre) {
		this.currentPerimetre = currentPerimetre;
	}

	public EOGdPerimetreGirofle getSelectedPerimetre() {
		return selectedPerimetre;
	}
	
	/**
	 * met a jour le perimetre et passe la valeur dans le binding
	 * @param selectedPerimetre : perimetre sélectionne
	 */
	public void setSelectedPerimetre(EOGdPerimetreGirofle selectedPerimetre) {
		this.selectedPerimetre = selectedPerimetre;
		setValueForBinding(selectedPerimetre, BINDING_SELECTED_PERIMETRE);
	}

	public boolean isSelectedPerimetre() {
		return (getSelectedPerimetre() == null);
	}
	
	public EOGdApplication getGdApplication() {
	    return (EOGdApplication) valueForBinding(BINDING_GD_APPLICATION);
    }
	
	/**
	 * @param gdApplication : application concerné par la gestion des droits
	 */
	public void setGdApplication(EOGdApplication gdApplication) {
	    setValueForBinding(gdApplication, BINDING_GD_APPLICATION);
    }
	
	/**
	 * Message de confirmation a la suppression d'un perimetre
	 * @return message
	 */
	public final String onClickBeforeSupprimerPerimetre() {
		String msgConfirm = localisation("PerimetresSrch.ConfirmerSuppression");
		return "confirm('" + msgConfirm + "')";
	}
	// Public Methods
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
	    displayGroupPerimetres = null;
	    super.appendToResponse(response, context);
	}
	/**
	 * rafraichissement de l'element selectionne.
	 * @param group : groupe d'element affiches
	 */
	public void displayGroupDidChangeSelectedObjects(WODisplayGroup group) {
		@SuppressWarnings("unchecked")
		ERXDisplayGroup<EOGdPerimetreGirofle> groupe = (ERXDisplayGroup<EOGdPerimetreGirofle>) group;
		if (groupe.selectedObject() != null) {
			setSelectedPerimetre(groupe.selectedObject());
		}
	}

	/**
	 * mise a jour des ecrans suite a la selection
	 * @return null (reste sur la page)
	 */
	public WOActionResults update() {		
		getDisplayGroupPerimetres().updateDisplayedObjects();
		setSelectedPerimetre(getDisplayGroupPerimetres().selectedObject());
		
		AjaxUpdateContainer.updateContainerWithID(getTableViewContainerId(), context());
		AjaxUpdateContainer.updateContainerWithID(getBoutonsContainerId(), context());

		return doNothing();
	}
	
	/**
	 * Retourne le titre dans la fenetre selon le modeEdition
	 * @return String
	 */
	public String titreWindow() {
		if (getModeEdition() != null) {
			switch (getModeEdition()) {
				case CREATION:
					return localisation("PerimetresSrch.WindowPerimetre.TitreAjouter");
				case MODIFICATION:
					return localisation("PerimetresSrch.WindowPerimetre.TitreModifier");
				default:
					return localisation("PerimetresSrch.WindowPerimetre.TitreUnknown");
			}
		}

		return localisation("TypeAEsSrch.titreUnknown");
	}

	/**
	 * ouverture du formulaire de definition des perimetres
	 * @return null (reste sur la page)
	 */
	public WOActionResults ajouterPerimetre() {
		try {
			EOGdPerimetreGirofle nouveauPerimetre = EOGdPerimetreGirofle.create(edc());
	    	nouveauPerimetre.setApplicationRelationship(getGdApplication());			
	    	nouveauPerimetre.setAppId(Integer.valueOf(getGdApplication().primaryKey()));
			nouveauPerimetre.setLibelle("");	      
	    	
	    	setPerimetre(nouveauPerimetre);

		} catch (ValidationException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		setModeEdition(ModeEdition.CREATION);
		CktlAjaxWindow.open(context(), getPerimetreWindowId(), titreWindow());

		return doNothing();
    }

	/**
	 * ouverture du formulaire de definition des perimetres
	 * @return null (reste sur la page)
	 */
	public WOActionResults modifierPerimetre() {
		setPerimetre(getSelectedPerimetre());
		
		setModeEdition(ModeEdition.MODIFICATION);
		CktlAjaxWindow.open(context(), getPerimetreWindowId(), titreWindow());
		
		return doNothing();	    
    }
	
	/**
	 * suppression du perimetre selectionne
	 * @return null (reste sur la page)
	 */
	public WOActionResults supprimerPerimetre() {
		try {
			getSelectedPerimetre().supprimer();
			edc().saveChanges();
			
			setSelectedPerimetre(null);
			
			update();
			
			// validation de la suppression
			session().addSimpleInfoMessage(localisation("application.OK"), localisation("PerimetresSrch.PerimetreSupprime"));

		} catch (Exception e) {
			edc().revert();
		} 
		
		return doNothing();
    }
	
	/**
	 * enregistre le perimetre
	 * @return boolean : indique si l'enregistrement s'est bien passe
	 */
	private boolean enregistrerPerimetre() throws ValidationException {
		try {			
			edc().saveChanges();						
			return true;
		} catch (ValidationException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Enregistre un nouveau perimetre
	 * @return WOActionResults
	 */
	public WOActionResults enregistrerEtCreerPerimetre() {

		if (!statutValidation()) {
			return null;
		}
		
		if (enregistrerPerimetre()) {
			getDisplayGroupPerimetres().insertObjectAtIndex(getPerimetre(), 0);
			update();

			CktlAjaxWindow.close(context(), getPerimetreWindowId());

			session().addSimpleInfoMessage(localisation("application.OK"), localisation("PerimetresSrch.PerimetreEnregistre"));

		}

		return doNothing();
	}
	
	/**
	 * Enregistre un nouveau perimetre
	 * @return WOActionResults
	 */
	public WOActionResults modifier() {
		if (!statutValidation()) {
			return null;
		}
		
		if (enregistrerPerimetre()) {
			update();

			CktlAjaxWindow.close(context(), getPerimetreWindowId());

			session().addSimpleInfoMessage(localisation("application.OK"), localisation("PerimetresSrch.PerimetreEnregistre"));

		}

		return doNothing();
	}

	/**
	 * annule l'action en cours et ferme la fenetre.
	 * @return null (reste sur la page).
	 */
	public WOActionResults annuler() {
		CktlAjaxWindow.close(context(), getPerimetreWindowId());

		return doNothing();
	}
	
	// Protected Methods
	
	// Private methods
	/**
	 * @return ERXDatabaseDataSource : data sources des perimetres
	 */	
	private EOArrayDataSource getPerimetresDatasource() {
		if (typePerimetreDatasource == null) {
			typePerimetreDatasource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(EOGdPerimetreGirofle.class), edc());
			typePerimetreDatasource.setArray(getPerimetres());
		}		
		return typePerimetreDatasource;
	}	
}