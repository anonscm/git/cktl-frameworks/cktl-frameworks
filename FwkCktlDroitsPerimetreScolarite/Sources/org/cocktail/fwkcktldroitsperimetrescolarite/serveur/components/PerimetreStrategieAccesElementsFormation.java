package org.cocktail.fwkcktldroitsperimetrescolarite.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpedagui.serveur.components.ComposantNode;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;


/**
 * Interface pour l'affichage des perimetres de type ACCES_ELEMENTS_FORMATION
 *
 */
public class PerimetreStrategieAccesElementsFormation extends FwkCktlDroitsPerimetreScolariteComponent {
	
	private static final long serialVersionUID = 1L;

	private static final String BINDING_PERIMETRE = "perimetre";
	private static final String BINDING_GD_APPLICATION = "gdApplication";
	
	private EODiplome diplome;
	private EODiplome selectedDiplome;

	private ComposantNode rootNode = null;
	private ComposantNode currentNode = null;
	private ComposantNode selectedNode = null;
	private NSArray<EOTypeComposant> typesComposantsInterdits;
	
	private NSArray<CheckboxNode> checkboxesNodes = new NSMutableArray<CheckboxNode>();
	
	
	/**
	 * Constructeur
	 * @param context : le contexte d'edition
	 */
    public PerimetreStrategieAccesElementsFormation(WOContext context) {
        super(context);
    }
    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
    	setCheckboxesNodes(new NSMutableArray<CheckboxNode>());
		setSelectedDiplome(null);

		if (getPerimetre().composant() != null) {
			setSelectedDiplome((EODiplome) getPerimetre().composant());
		}
		
    	super.appendToResponse(response, context);
    }
    
    public String getFormationId() {
    	return getComponentId() + "_formationId";                                                              
    }
    
    public String getListeDeroulanteDiplomesId() {
		return getComponentId() + "_listeDeroulanteDiplomesId";
	}
    
    public String getTreeTableContainerId() {
    	return getComponentId() + "_treeTableContainerId";                                                              
    }
    
	public String getBoutonsContainerId() {
		return getComponentId() + "_boutonsContainerId";
	}
    
    /**
     * @return EOPerimetreGirofle : le perimetre passe en binding
     */
    public EOGdPerimetreGirofle getPerimetre() {
		return ((EOGdPerimetreGirofle) valueForBinding(BINDING_PERIMETRE));
	}
    
    public EOGdApplication getGdApplication() {
	    return (EOGdApplication) valueForBinding(BINDING_GD_APPLICATION);
    }
    
    /**
	 * @param gdApplication : application concerné par la gestion des droits
	 */
	public void setGdApplication(EOGdApplication gdApplication) {
	    setValueForBinding(gdApplication, BINDING_GD_APPLICATION);
    }
    
    /**
	 * @return NSArray<EODiplome> : liste des diplomes
	 */
	public NSArray<EODiplome> getDiplomes() {
		NSArray<EODiplome> diplomes = new NSArray<EODiplome>();
		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EODiplome.CODE_KEY, EOSortOrdering.CompareAscending));
		diplomes = EODiplome.fetchAllSco_Diplomes(edc(), sortOrderings);

		return diplomes;
	}

	public EODiplome getDiplome() {
		return diplome;
	}

	public void setDiplome(EODiplome diplome) {
		this.diplome = diplome;
	}

	/**
	 * Si le perimetre un composant alors on est en mode modification et dans ce cas on set le selectedDiplome a celui corresp
	 * @return EODiplome : le diplome selectionne dans la liste deroulante
	 */
	public EODiplome getSelectedDiplome() {
		if (getPerimetre().composant() != null) {
			if (selectedDiplome == null) {
				selectedDiplome = (EODiplome) getPerimetre().composant();
			}
		}
		
		return selectedDiplome;
	}

	/**
	 * Set le diplome selectionne et met a jour l'interface utilisateur
	 * @param selectedDiplome : le diplome selectionne
	 */
	public void setSelectedDiplome(EODiplome selectedDiplome) {
		this.selectedDiplome = selectedDiplome;
		
		rootNode = null;
		if (selectedDiplome != null) {
			getPerimetre().setComposantRelationship(selectedDiplome);
		}
		
	}
	
	public Boolean isSelectedDiplome() {
		return (getSelectedDiplome() != null);
	}
	
	/**
	 * @return racine de la formation
	 */
	public ComposantNode getRootNode() {
		if (rootNode == null) {
			rootNode = new ComposantNode(getCurrentVersionDiplome(), null, true);
			rootNode.setTypeComposantsNonAffichables(getTypesComposantsInterdits());
		}
		
		return rootNode;
	}
	
	public void setRootNode(ComposantNode rootNode) {
		this.rootNode = rootNode;
	}
	
	/**
	 * @return EOVersionDiplome : la version du diplome courant
	 */
	public EOVersionDiplome getCurrentVersionDiplome() {
		EOVersionDiplome versionDiplome = null;
		if (getSelectedDiplome() != null) {
			//versionDiplome = getSelectedDiplome().versionDiplome(edc());
			versionDiplome = (EOVersionDiplome) getSelectedDiplome().getVersionDiplome(getAnneeExercice());
		}

		return versionDiplome;
	}
	
	public ComposantNode getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(ComposantNode currentNode) {
		this.currentNode = currentNode;
	}

	public ComposantNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(ComposantNode selectedNode) {
		this.selectedNode = selectedNode;
	}
	
	/**
	 * @return NSMutableArray<EOTypeComposant> : liste des types de compposants interdits affiches dans le tree table
	 */
	public NSArray<EOTypeComposant> getTypesComposantsInterdits() {
		if (typesComposantsInterdits == null) {
			typesComposantsInterdits = new NSMutableArray<EOTypeComposant>();

			typesComposantsInterdits.add(EOTypeComposant.typeAE(edc()));
			typesComposantsInterdits.add(EOTypeComposant.typeAP(edc()));
		}
		
		return typesComposantsInterdits;
	}

	public void setTypesComposantsInterdits(NSMutableArray<EOTypeComposant> typesComposantsInterdits) {
		this.typesComposantsInterdits = typesComposantsInterdits;
	}
	
	/**
	 * @return boolean : indique si la ligne a affichee dans le tree est de type Version Diplome
	 */
	public boolean affichageTypeVersionDiplome() {
		return EOTypeComposant.typeVersionDiplome(edc()).equals(getCurrentNode().getData().typeComposant());
	}
	
	public Boolean isRootNodeChecked() {
		return true;
	}
	
	public Boolean isRootNodeDisabled() {
		return true;
	}

	/**
	 * Construit le tableau des cases a cocher selectionnees en fonction des enregistrements des perimetres enfants du perimetre passe en binding
	 * @return NSArray<CheckboxNode> : le tableau des checkboxes nodes
	 */
	public NSArray<CheckboxNode> checkboxesNodes() {
		if (getRootNode() != null && getRootNode().getData() != null && checkboxesNodes.count() == 0) {
    		for (CktlTreeTableNode<IComposant> node : getRootNode().allDescendants()) {
    			ComposantNode tempNode = (ComposantNode) node;
    			
    			if (!tempNode.getData().typeComposant().equals(EOTypeComposant.typeVersionDiplome(edc()))) {
    				
    				if (tempNode.isVisible()) {
  
		    			Boolean checked = false;
		    			Boolean disabled = true;
		    			if (getPerimetre().perimetresEnfants().count() > 0) {
		    				// Verifier que le perimetre existe
		    				// Si oui il est coche
		    				// Si non le cocher si son parent existe
		    				NSArray<EOGdPerimetre> perimetresEnfantsTemp = getPerimetre().perimetresEnfants(ERXQ.equals(EOGdPerimetreGirofle.COMPOSANT_ID_KEY, tempNode.getData().id()));
		    				if (perimetresEnfantsTemp.count() == 2) {
		    					checked = true;
		    					disabled = false;
		    				}
		    				else {
		    					// verifier que le parent du tempNode est coche ou non
		    					ComposantNode parentTempNode = (ComposantNode)tempNode.getParent();
		    					for (CheckboxNode cn : checkboxesNodes) {
		    						if (cn.getComposantId() == parentTempNode.getData().id()) {
		    							checked = cn.isChecked();
		    							disabled = cn.isChecked();
		    						}
		    					}
		    					
		    				}
		    			}
		    			else {
		    				checked = true;
		    				
		    				if (tempNode.getParent().equals(getRootNode())) {
		    					disabled = false;
		    				}
		    				else {
		    					disabled = true;
		    				}
		    			}

    			
		    			checkboxesNodes.add(new CheckboxNode(checked, disabled, tempNode.getData().id()));
    				}
    			}
    		}
    	}
		
		return checkboxesNodes;
	}

	public void setCheckboxesNodes(NSArray<CheckboxNode> checkboxesNodes) {
		this.checkboxesNodes = checkboxesNodes;
	}

	/**
	 * Retrouve la checkboxnode en faisant la correspondance avec le noeud selectionne
	 * @return CheckboxNode : la checkboxnode correspondant au noeud selectionne
	 */
	public CheckboxNode getCurrentCheckboxNode() {
		// Parcourir le tableau a la recherche d'une checkboxnode dont l'id composant est egal a l'id du currentNode
		for (CheckboxNode cn : checkboxesNodes()) {
			if (cn.getComposantId() == getCurrentNode().getData().id()) {
				return cn;
			}
		}

		return null;
	}
	
	private boolean enregistrer() throws ValidationException {
		try {
			edc().saveChanges();

			return (true);
		} catch (ValidationException e) {
			session().addSimpleErrorMessage(localisation(" a p p l i c a t i o n . l a b e l E r r e u r "), e);

			return false;
		} catch (Exception e) {
			session().addSimpleErrorMessage(localisation(" a p p l i c a t i o n . l a b e l E r r e u r "), e);

			return false;
		}

	}

	/**
	 * 
	 * @return WOActionResults : doNothing reste sur la page
	 */
	public WOActionResults modifier() {
		// Supprimer tous les perimetres enfants du perimetre passe en parametre
		if (getPerimetre().perimetresEnfants().count() > 0) {
			for (EOGdPerimetre perimetre : getPerimetre().perimetresEnfants()) {
				perimetre.delete();
			}
		
			edc().saveChanges();
		}
		
		edc().saveChanges();
		
		// Enregistrer les perimetres en fonction des cases cocher
		// Il ne faut qu'enregistrer les cases cochees selectionnees et non disabled
		for (CheckboxNode cn : checkboxesNodes()) {
			if (cn.isChecked() && !cn.isDisabled()) {
				try {
					EOGdPerimetreGirofle nouveauPerimetre = EOGdPerimetreGirofle.create(edc());
			    	nouveauPerimetre.setApplicationRelationship(getGdApplication());			
			    	nouveauPerimetre.setAppId(Integer.valueOf(getGdApplication().primaryKey()));
			    	nouveauPerimetre.setLibelle(getPerimetre().libelle());   

			    	nouveauPerimetre.setStrategieRelationship(EOGdStrategie.strategieAccesElementsFormation(edc()));
			    	nouveauPerimetre.setStrategieId(Integer.valueOf(EOGdStrategie.strategieAccesElementsFormation(edc()).primaryKey()));
			    	nouveauPerimetre.setPerimetreParentRelationship(getPerimetre());
			    	nouveauPerimetre.setPerimetreParentId(Integer.valueOf(getPerimetre().primaryKey()));

			    	nouveauPerimetre.setComposantId(cn.getComposantId());

				} catch (ValidationException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		
		if (enregistrer()) {
			setSelectedNode(null);
			//setCheckboxesNodes(null);

			session().addSimpleInfoMessage(localisation("application.OK"), localisation("PerimetreStrategieAccesElementsFormation.AccesEnregistre"));
		}

		return doNothing();
	}
	
	/**
	 * Rafraichissement des checkboxes lorsqu'une checkbox est coche ou decoche
	 * @return WOActionResults : doNothing reste sur la page
	 */
	public WOActionResults updateCheckboxes() {
		for (CktlTreeTableNode<IComposant> node : getRootNode().allDescendants()) {
			ComposantNode tempNode = (ComposantNode) node;
			
			if (!tempNode.getData().typeComposant().equals(EOTypeComposant.typeVersionDiplome(edc()))) {
				if (tempNode.isVisible() && !tempNode.getParent().equals(getRootNode())) {
					if (!getCheckboxNodeForComposantNode((EOComposant) tempNode.getParent().getData()).isChecked()) {
						getCheckboxNodeForComposantNode(tempNode.getData()).setIsDisabled(false);
					}
					else {
						getCheckboxNodeForComposantNode(tempNode.getData()).setIsDisabled(true);
						getCheckboxNodeForComposantNode(tempNode.getData()).setIsChecked(true);
					}
				}
			}
		}
		
		AjaxUpdateContainer.updateContainerWithID(getTreeTableContainerId(), context());
		
		return doNothing();
	}
	
	private CheckboxNode getCheckboxNodeForComposantNode(IComposant composant) {
		for (CheckboxNode cn : checkboxesNodes()) {
			if (cn.getComposantId() == composant.id()) {
				return cn;
			}
		}
		
		return null;
	}

	 
	/**
	 * Classe permettant de creer des checkboxesnodes
	 * qui font la correspondance entre un noeud et ses caracteristiques (savoir s'il est coche ou non et savoir s'il est grise ou non)
	 *
	 */
	protected class CheckboxNode {
		private Boolean checked;
		private Boolean disabled;
		private Integer composantId;
		
		public CheckboxNode(Boolean checked, Boolean disabled, Integer composantId) {
			this.checked = checked;
			this.disabled = disabled;
			this.composantId = composantId;
		}
		
		public Boolean isChecked() {
			return checked;
		}
		public void setIsChecked(Boolean checked) {
			this.checked = checked;
		}
		
		public Boolean isDisabled() {
			return disabled;
		}
		public void setIsDisabled(Boolean disabled) {
			this.disabled = disabled;
		}
		
		public Integer getComposantId() {
			return composantId;
		}
		public void setComposantId(Integer composantId) {
			this.composantId = composantId;
		}
		
		@Override
		public String toString() {
			return "Checkboxnode : checked = " + isChecked() + " - disabled = " + isDisabled() + " - composantId = " + getComposantId();
		}

	}
	
}