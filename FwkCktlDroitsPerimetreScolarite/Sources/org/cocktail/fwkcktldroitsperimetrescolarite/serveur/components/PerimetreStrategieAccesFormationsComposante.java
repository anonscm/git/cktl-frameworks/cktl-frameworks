package org.cocktail.fwkcktldroitsperimetrescolarite.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.EOScolpedaCommonDefinitions;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;


/**
 * Interface pour l'affichage des perimetres de type ACCES_FORMATIONS_COMPOSANTE
 *
 */
public class PerimetreStrategieAccesFormationsComposante extends FwkCktlDroitsPerimetreScolariteComponent {
	
	private static final long serialVersionUID = 1L;

	private static final String BINDING_PERIMETRE = "perimetre";
	
	private EOStructure structure;
	private EOStructure selectedStructure;
	
	// Table de recherche des diplomes
	private ERXDisplayGroup<EODiplome> displayGroupDiplomes = null;
	private ERXDatabaseDataSource typeDiplomeDatasource = null;
	private EODiplome selectedDiplome = null;
	
	
	/**
	 * Constructeur
	 * @param context : le contexte d'edition
	 */
    public PerimetreStrategieAccesFormationsComposante(WOContext context) {
        super(context);
    }
    
    public String getComposanteId() {
    	return getComponentId() + "_composanteId";                                                              
    }
    
    public String getDiplomesContainerId() {
		return getComponentId() + "_diplomesContainerId";
	}
    
    /**
     * @return EOPerimetreGirofle : le perimetre passe en binding
     */
    public EOGdPerimetreGirofle perimetre() {
		return ((EOGdPerimetreGirofle) valueForBinding(BINDING_PERIMETRE));
	}

    /**
	 * @return NSArray<EOStructure> : tableau des structures (au sans composantes)
	 */
	public NSArray<EOStructure> getStructures() {
		return EOStructure.fetchAll(edc(), ERXQ.equals(EOStructure.C_TYPE_STRUCTURE_KEY, EOTypeStructure.TYPE_STRUCTURE_C), EOScolpedaCommonDefinitions.defaultsortOrderings(EOStructure.LL_STRUCTURE_KEY));
	}

	public EOStructure getStructure() {
		return structure;
	}

	public void setStructure(EOStructure structure) {
		this.structure = structure;
	}


	public EOStructure getSelectedStructure() {
		return selectedStructure;
	}

	public void setSelectedStructure(EOStructure selectedStructure) {
		this.selectedStructure = selectedStructure;
	}
	
	public EODiplome getSelectedDiplome() {
		return selectedDiplome;
	}

    /**
     * Set le diplome en parametre et le set aussi au perimetre passe en binding
     * @param selectedDiplome : le diplome selectionne
     */
	public void setSelectedDiplome(EODiplome selectedDiplome) {
		this.selectedDiplome = selectedDiplome;

		if (selectedDiplome != null) {
			perimetre().setComposant(selectedDiplome);
		}
	}

    /**
	 * @return ERXDisplayGroup<EODiplome> : display group des diplomes affiches
	 */
	public ERXDisplayGroup<EODiplome> displayGroupDiplomes() {
		displayGroupDiplomes = new ERXDisplayGroup<EODiplome>();
		displayGroupDiplomes.setDataSource(diplomesDatasource());
		
		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EODiplome.CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		displayGroupDiplomes.setSortOrderings(sortOrderings);
		
		EOQualifier rechercheQualifier = ERXQ.containsObject(EODiplome.STRUCTURES_KEY, getSelectedStructure());
		displayGroupDiplomes.setQualifier(rechercheQualifier);

		displayGroupDiplomes.setSelectsFirstObjectAfterFetch(true);
		displayGroupDiplomes.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
		displayGroupDiplomes.fetch();

		return displayGroupDiplomes;
	}
	
	/**
	 * @return ERXDatabaseDataSource : datasource des diplomes
	 */
	private ERXDatabaseDataSource diplomesDatasource() {
		if (typeDiplomeDatasource == null) {
			typeDiplomeDatasource = new ERXDatabaseDataSource(edc(), EODiplome.ENTITY_NAME);
		}
		
		return typeDiplomeDatasource;
	}
	
}