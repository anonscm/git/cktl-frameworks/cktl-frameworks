package org.cocktail.fwkcktldroitsperimetrescolarite.serveur.components;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;


/**
 * Interface pour l'affichage des perimetres de type ACCES_FORMATION
 *
 */
public class PerimetreStrategieAccesFormation extends FwkCktlDroitsPerimetreScolariteComponent {
	
	private static final long serialVersionUID = 1L;

	private static final String BINDING_PERIMETRE = "perimetre";
	
	// Table de recherche des diplomes
	private ERXDisplayGroup<EODiplome> displayGroupDiplomes = null;
	private ERXDatabaseDataSource typeDiplomeDatasource = null;
	private EODiplome selectedDiplome = null;
	
	
	/**
	 * Constructeur
	 * @param context : le contexte d'edition
	 */
    public PerimetreStrategieAccesFormation(WOContext context) {
        super(context);
    }
    
    @Override
	public void appendToResponse(WOResponse response, WOContext context) {
		setSelectedDiplome(null);
		displayGroupDiplomes = null;

		if (getPerimetre().composant() != null) {
			setSelectedDiplome((EODiplome) getPerimetre().composant());
		}

		super.appendToResponse(response, context);
	}
    
    /**
     * @return EOPerimetreGirofle : le perimetre passe en binding
     */
    public EOGdPerimetreGirofle getPerimetre() {
		return ((EOGdPerimetreGirofle) valueForBinding(BINDING_PERIMETRE));
	}
    
    public EODiplome getSelectedDiplome() {
		return selectedDiplome;
	}

    /**
     * Set le diplome en parametre et le set aussi au perimetre passe en binding
     * @param selectedDiplome : le diplome selectionne
     */
	public void setSelectedDiplome(EODiplome selectedDiplome) {
		this.selectedDiplome = selectedDiplome;

		if (selectedDiplome != null) {
			getPerimetre().setComposant(selectedDiplome);
		}
	}

    /**
	 * @return ERXDisplayGroup<EODiplome> : display group des diplomes affiches
	 */
	public ERXDisplayGroup<EODiplome> displayGroupDiplomes() {
		if (displayGroupDiplomes == null) {
			displayGroupDiplomes = new ERXDisplayGroup<EODiplome>();
			displayGroupDiplomes.setDataSource(diplomesDatasource());
			
			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EODiplome.CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
			displayGroupDiplomes.setSortOrderings(sortOrderings);
			

			displayGroupDiplomes.setSelectsFirstObjectAfterFetch(false);
			displayGroupDiplomes.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupDiplomes.fetch();
			
			if (getSelectedDiplome() != null) {
				displayGroupDiplomes.setSelectedObject(getSelectedDiplome());
			}
		}

		return displayGroupDiplomes;
	}
	
	/**
	 * @return ERXDatabaseDataSource : datasource des diplomes
	 */
	private ERXDatabaseDataSource diplomesDatasource() {
		if (typeDiplomeDatasource == null) {
			typeDiplomeDatasource = new ERXDatabaseDataSource(edc(), EODiplome.ENTITY_NAME);
		}
		
		return typeDiplomeDatasource;
	}
    
}