package org.cocktail.fwkcktldroitsperimetrescolarite.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsperimetrescolarite.serveur.commun.ModeEdition;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Vue des perimetres de donnees pour l'application Girofle (sphere scolarite)
 */
public class PerimetreVueScolariteGirofle extends FwkCktlDroitsPerimetreScolariteFormComponent {
	// Private fields
	private static final long serialVersionUID = 1L;
	private static final String BINDING_PROFIL = "profil";
	private static final String BINDING_GD_APPLICATION = "gdApplication";
	private ERXDisplayGroup<EOGdPerimetreGirofle> displayGroupPerimetres = null;

	private EOGdPerimetreGirofle editedPerimetre;
	private EOGdPerimetreGirofle selectedPerimetre;
	private EOArrayDataSource perimetreDatasource;

	// Constructor
	/**
	 * @param context : contexte d'edition
	 */
	public PerimetreVueScolariteGirofle(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		displayGroupPerimetres = null;
		setSelectedPerimetre(null);
		super.appendToResponse(response, context);
	}

	// Properties
	public String getPerimetresContainerId() {
		return getComponentId() + "_perimetresContainerId";
	}

	public String getBoutonsContainerId() {
		return getComponentId() + "_boutonsContainerId";
	}

	public String getPerimetreWindowId() {
		return getComponentId() + "_ajouterPerimetreWindowId";
	}

	public String getBoutonsPerimetreWindowContainerId() {
		return getComponentId() + "_boutonsPerimetreWindowContainerId";
	}

	public String getTableViewContainerId() {
		return getComponentId() + "_tableViewContainerId";
	}

	public EOGdProfil getProfil() {
		return (EOGdProfil) valueForBinding(BINDING_PROFIL);
	}

	public EOGdApplication getGdApplication() {
		return (EOGdApplication) valueForBinding(BINDING_GD_APPLICATION);
	}

	public EOGdPerimetreGirofle getEditedPerimetre() {
		return editedPerimetre;
	}

	public void setEditedPerimetre(EOGdPerimetreGirofle perimetreEdite) {
		this.editedPerimetre = perimetreEdite;
	}

	/**
	 * @return le display group des perimetres du profil pour l'application gdApplication
	 */
	public ERXDisplayGroup<EOGdPerimetreGirofle> getDisplayGroupPerimetres() {
		if (displayGroupPerimetres == null) {
			displayGroupPerimetres = new ERXDisplayGroup<EOGdPerimetreGirofle>();
			displayGroupPerimetres.setDataSource(getPerimetresDatasource());

			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOGdPerimetreGirofle.STRATEGIE_KEY, EOSortOrdering.CompareAscending));
			displayGroupPerimetres.setSortOrderings(sortOrderings);

			displayGroupPerimetres.setQualifier(EOGdPerimetreGirofle.APPLICATION.eq(getGdApplication()).and(
			        EOGdPerimetreGirofle.PROFILS.containsObject(getProfil())));

			displayGroupPerimetres.setSelectsFirstObjectAfterFetch(false);
			displayGroupPerimetres.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupPerimetres.fetch();
		}

		return displayGroupPerimetres;
	}

	/**
	 * Retourne le titre dans la fenetre selon le modeEdition
	 * @return String
	 */
	private String getTitreWindow() {
		if (getModeEdition() != null) {
			switch (getModeEdition()) {
			case CREATION:
				return localisation("PerimetresSrch.WindowPerimetre.TitreAjouter");
			case MODIFICATION:
				return localisation("PerimetresSrch.WindowPerimetre.TitreModifier");
			default:
				return localisation("PerimetresSrch.WindowPerimetre.TitreUnknown");
			}
		}

		return localisation("PerimetresSrch.WindowPerimetre.titreUnknown");
	}

	public EOGdPerimetreGirofle getSelectedPerimetre() {
		return selectedPerimetre;
	}

	public void setSelectedPerimetre(EOGdPerimetreGirofle selectedPerimetre) {
		this.selectedPerimetre = selectedPerimetre;
	}

	public boolean isNotSelectedPerimetre() {
		return (getSelectedPerimetre() == null);
	}

	// Public Methods
	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	/**
	 * Ouvre une fenetre qui permet de creer un perimetre et de l'ajouter
	 * @return null (reste sur la page)
	 */
	public WOActionResults ajouter() {
		try {
			EOGdPerimetreGirofle nouveauPerimetre = EOGdPerimetreGirofle.create(edc());
			nouveauPerimetre.setApplicationRelationship(getGdApplication());
			nouveauPerimetre.setAppId(Integer.valueOf(getGdApplication().primaryKey()));
			nouveauPerimetre.setLibelle("");

			setEditedPerimetre(nouveauPerimetre);

		} catch (ValidationException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		setModeEdition(ModeEdition.CREATION);
		CktlAjaxWindow.open(context(), getPerimetreWindowId(), getTitreWindow());

		return doNothing();
	}

	/**
	 * Ouvre une fenetre qui permet de modifier le perimetre selectionne
	 * @return null (reste sur la page)
	 */
	public WOActionResults modifier() {
		if (getSelectedPerimetre() != null) {

			setEditedPerimetre(getSelectedPerimetre());

			setModeEdition(ModeEdition.MODIFICATION);
			CktlAjaxWindow.open(context(), getPerimetreWindowId(), getTitreWindow());
		}
		return doNothing();
	}

	/**
	 * @return enregistre les modifications sur le perimetres
	 */
	public WOActionResults modifierPerimetre() {
		if (!statutValidation()) {
			return null;
		}

		if (enregistrerPerimetre()) {
			updateDisplaygroup();
			AjaxUpdateContainer.updateContainerID(getTableViewContainerId());
			update();

			CktlAjaxWindow.close(context(), getPerimetreWindowId());

			session().addSimpleInfoMessage(localisation("application.OK"), localisation("PerimetresSrch.PerimetreEnregistre"));

		}

		return doNothing();
	}

	/**
	 * @return message de confirmation avant la suppression du perimetre
	 */
	public String onClickBeforeSupprimer() {
		String msgConfirm = localisation("PerimetresSrch.ConfirmerSuppression");
		return "confirm('" + msgConfirm + "')";
	}

	/**
	 * Supprime le perimetre selectionne apres demande de confirmation
	 * @return null (reste sur la page)
	 */
	public WOActionResults supprimer() {
		if (getSelectedPerimetre() != null) {
			getProfil().removeFromPerimetresRelationship(getSelectedPerimetre());
			edc().saveChanges();
			session().addSimpleInfoMessage(localisation("application.OK"), localisation("PerimetreVueScolarite.PerimetreEnleveDuProfil"));

			if (getSelectedPerimetre().profils().count() == 0) {
				getSelectedPerimetre().delete();
				session().addSimpleInfoMessage(localisation("application.OK"), localisation("PerimetresSrch.PerimetreSupprime"));
			}

			edc().saveChanges();
			updateDisplaygroup();
		}
		return doNothing();
	}

	/**
	 * Enregistre un nouveau perimetre
	 * @return WOActionResults
	 */
	public WOActionResults enregistrerEtCreerPerimetre() {
		if (!statutValidation()) {
			return null;
		}
		if (enregistrerPerimetreProfil()) {
			update();
			CktlAjaxWindow.close(context(), getPerimetreWindowId());

			session().addSimpleInfoMessage(localisation("application.OK"), localisation("PerimetresSrch.PerimetreEnregistre"));
		}

		return doNothing();
	}

	/**
	 * annule l'edition en cours et ferme la fenếtre
	 * @return null (reste sur la page)
	 */
	public WOActionResults annuler() {
		CktlAjaxWindow.close(context(), getPerimetreWindowId());

		return doNothing();
	}

	// Private methods
	/**
	 * mise a jour des ecrans suite a la selection
	 * @return null (reste sur la page)
	 */
	public WOActionResults update() {
		getDisplayGroupPerimetres().updateDisplayedObjects();
		setSelectedPerimetre(getDisplayGroupPerimetres().selectedObject());

		AjaxUpdateContainer.updateContainerWithID(getTableViewContainerId(), context());
		AjaxUpdateContainer.updateContainerWithID(getBoutonsContainerId(), context());

		return doNothing();
	}

	public WOActionResults onSelect() {
		AjaxUpdateContainer.updateContainerWithID(getBoutonsContainerId(), context());
		return doNothing();
	}

	private void updateDisplaygroup() {
		getDisplayGroupPerimetres().setObjectArray(getPerimetres());
		getDisplayGroupPerimetres().updateDisplayedObjects();
	}

	/**
	 * enregistre l'association du perimetre pour le profil existant et cree le perimetre au besoin
	 * @return boolean : indique si l'enregistrement s'est bien passe
	 */
	private boolean enregistrerPerimetreProfil() throws ValidationException {
		try {
			cherchePerimetreSimilaireExistant();

			if (!getProfil().perimetres().contains(getEditedPerimetre())) {
				getEditedPerimetre().addToProfilsRelationship(getProfil());
				edc().saveChanges();
				updateDisplaygroup();
			} else {
				session().addSimpleInfoMessage(localisation("application.OK"), localisation("PerimetreVueScolariteGirofle.PerimetreDejaAssocie"));
				edc().revert();
			}
			return true;
		} catch (ValidationException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * enregistre le perimetre en cours d'edition
	 * @return boolean : indique si l'enregistrement s'est bien passe
	 */
	private boolean enregistrerPerimetre() throws ValidationException {
		try {
			edc().saveChanges();
			return true;
		} catch (ValidationException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void cherchePerimetreSimilaireExistant() {
		EOGdPerimetreGirofle perimetreSimilaire = perimetreSimilaire(getEditedPerimetre());
		if (perimetreSimilaire != null) {
			session().addSimpleInfoMessage(localisation("application.OK"), localisation("PerimetreVueScolariteGirofle.UtilisationPerimetreExistant"));
			edc().deleteObject(getEditedPerimetre());
			setEditedPerimetre(perimetreSimilaire);
		} else {
			getDisplayGroupPerimetres().insertObjectAtIndex(getEditedPerimetre(), 0);
		}
	}

	/**
	 * @return perimetre similaire au perimetre courant (même attributs)
	 */
	private EOGdPerimetreGirofle perimetreSimilaire(EOGdPerimetreGirofle perimetre) {
		EOGdPerimetreGirofle perimetreSimilaire = EOGdPerimetreGirofle.fetchSco_GdPerimetreGirofle(
		        edc(), EOGdPerimetreGirofle.STRATEGIE.eq(perimetre.strategie())
		                .and(EOGdPerimetreGirofle.ANNEE.eq(perimetre.annee()))
		                .and(EOGdPerimetreGirofle.STRUCTURE.eq(perimetre.structure()))
		                .and(EOGdPerimetreGirofle.COMPOSANT.eq(perimetre.composant()))
		                .and(EOGdPerimetreGirofle.APPLICATION.eq(perimetre.application()))
		                .and(EOGdPerimetreGirofle.GRADE_UNIVERSITAIRE.eq(perimetre.gradeUniversitaire())));

		//        .and(EOGdPerimetreGirofle.TYPE_DROIT_DONNEE.eq(perimetre.typeDroitDonnee())));

		return perimetreSimilaire;

	}

	/**
	 * @return ERXDatabaseDataSource : data sources des perimetres
	 */
	private EOArrayDataSource getPerimetresDatasource() {
		if (perimetreDatasource == null) {
			perimetreDatasource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(EOGdPerimetreGirofle.class), edc());
			perimetreDatasource.setArray(getPerimetres());
		}
		return perimetreDatasource;
	}

	private NSArray<EOGdPerimetreGirofle> getPerimetres() {
		return ERXArrayUtilities.arrayWithoutDuplicates(EOGdPerimetreGirofle.fetchAllSco_GdPerimetreGirofles(edc()));
	}

}