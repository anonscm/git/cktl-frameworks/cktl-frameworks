package org.cocktail.fwkcktldroitsperimetrescolarite.serveur.components;

import java.util.UUID;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktldroitsperimetrescolarite.serveur.FwkCktlDroitsPerimetreScolariteParamManager;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSNumberFormatter;

import er.ajax.AjaxUtils;
import er.ajax.CktlAjaxUtils;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

/**
 * @author lilia 
 * Classe parent des composant de gestion des droits
 *
 */
public class FwkCktlDroitsPerimetreScolariteComponent extends CktlAjaxWOComponent {


	// Private fields
	private static final long serialVersionUID = 1L;
	public static final String BINDING_EDITING_CONTEXT = "editingContext";
	public static final String BINDING_UPDATE_CONTAINER_ID = "updateContainerID";
	public static final String BINDING_ISEDITING = "isEditing";
	private static final String BINDING_NEED_REFRESH = "needRefresh";

	private static final String SESSION_KEY_NOTIFICATION_CONTAINER_ID = "notificationContainerId";

	private EOEditingContext edc = null;

	private NSNumberFormatter integerNumberFormatter;
	private String onloadJS;

	private NSNumberFormatter app2DecimalesFormatter; // Formatteur a deux decimales a utiliser pour les donnees numeriques non monetaires

	public static final boolean IS_CLICK_TO_OPEN_ENABLED = Boolean.valueOf(System.getProperty("er.component.clickToOpen", "false"));

	protected static final Integer NUMBER_OF_OBJECT_PER_BATCH = 15;
	
	private static final String ANNEE_UNIVERSITAIRE_EXERCICE = "anneeUniversitaireExercice";
	
	private FwkCktlDroitsPerimetreScolariteParamManager paramManager = null;
	

	//Constructor
	/**
	 * @param context : contexte d'édition
	 */
	public FwkCktlDroitsPerimetreScolariteComponent(WOContext context) {
		super(context);
	}
	
	// Properties
	@Override
	public CocktailAjaxSession session(){		
		 return (CocktailAjaxSession)super.session();
	}

	/**
	 * @return gestionnaire de parametre pour Girofle
	 */
	public FwkCktlDroitsPerimetreScolariteParamManager getParamManager() {
		if (paramManager == null) {
			paramManager = new FwkCktlDroitsPerimetreScolariteParamManager();
		}
		return paramManager;
	}
	
	public Boolean getNeedRefresh() {
		return valueForBooleanBinding(BINDING_NEED_REFRESH, false);
	}

	/**
	 * @param needRefresh : parametre a passer au binding
	 */
	public void setNeedRefresh(Boolean needRefresh) {
		setValueForBinding(needRefresh, BINDING_NEED_REFRESH);
	}

	public NSNumberFormatter getIntegerNumberFormatter() {
		return integerNumberFormatter;
	}

	public NSNumberFormatter getApp2DecimalesFormatter() {
		return app2DecimalesFormatter;
	}

	/**
	 * @return Le binding <i>editingContext</i> s'il est renseigne, sinon l'editingContext de la session. Cette methode peut etre surchargee pour renvoyer un
	 *         nestedEditingContext.
	 */
	@Override
	public EOEditingContext edc() {
		if (edc == null) {
			if (hasBinding(BINDING_EDITING_CONTEXT)) {
				edc = (EOEditingContext) valueForBinding(BINDING_EDITING_CONTEXT);
			} else {
				edc = ERXEC.newEditingContext();
			}
		}
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public String getUpdateContainerID() {
		return (String) valueForBinding(BINDING_UPDATE_CONTAINER_ID);
	}

	public Boolean isEditing() {
		return booleanValueForBinding(BINDING_ISEDITING, false);
	}

	/**
	 * @return objet contenant les messages de notification
	 */
	public String notificationContainerId() {
		if (session().objectForKey(SESSION_KEY_NOTIFICATION_CONTAINER_ID) == null) {
			String notificationContainerId = "cktl_" + UUID.randomUUID().toString().replaceAll("-", "_") + "_notificationContainer";
			session().setObjectForKey(notificationContainerId, SESSION_KEY_NOTIFICATION_CONTAINER_ID);
		}

		return (String) session().objectForKey(SESSION_KEY_NOTIFICATION_CONTAINER_ID);
	}

	/**
	 * @return the onloadJS
	 */
	public String onloadJS() {
		return onloadJS;
	}

	/**
	 * @param onloadJS the onloadJS to set
	 */
	public void setOnloadJS(String onloadJS) {
		this.onloadJS = onloadJS;
	}

	// Public Methods
	/**
	 * @param key :identifiant de la chaine à localiser
	 * @return chaine localisée
	 */
	public String localisation(String key) {
		return mySession().localizer().localizedStringForKey(key);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/default.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/alert.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/lighting.css");

		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommon.css");
		// TODO Choisir une css de couleur si necessaire
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCss3MenuCommon.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCss3Common.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/cktlAjaxMenuCommon.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "themes/fixedheadertable/defaultTheme.css");

		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommonBleu.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCss3MenuBleu.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCss3Bleu.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/cktlAjaxMenuBleu.css");

		//CktlAjaxUtils.addStylesheetResourceInHead(context, response, "app", "styles/scolpedaformation.css");

		CktlAjaxUtils.addScriptResourceInHead(context, response, "prototype.js");
//		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlThemes.framework", "scripts/window.js");
//		CktlAjaxUtils.addScriptResourceInHead(context, response, "app", "scripts/strings.js");
//		CktlAjaxUtils.addScriptResourceInHead(context, response, "app", "scripts/formatteurs.js");
//		CktlAjaxUtils.addScriptResourceInHead(context, response, "app", "scripts/kinetic-v4.0.1.js");
		
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlThemes.framework", "scripts/sectionCadre.js");
		response.appendContentString("<script>SECTION_CADRE.maxHeight();</script>");
	}

		/**
	 * Initialise les formatteurs
	 */
	public void initFormatters() {
		this.app2DecimalesFormatter = new NSNumberFormatter();
		this.app2DecimalesFormatter.setDecimalSeparator(",");
		this.app2DecimalesFormatter.setThousandSeparator(" ");

		this.app2DecimalesFormatter.setHasThousandSeparators(true);
		this.app2DecimalesFormatter.setPattern("#,##0.00;0,00");

	}

	// Protected Methods
	/**
	 * construit la condition sur la colonne.
	 * @param nomColonne : nom de la colonne sur la quelle on fait la recherche.
	 * @param filtre : element recherché.
	 * @return EOQualifier
	 */
	protected EOQualifier getContainsQualifier(String nomColonne, String filtre) {
		EOQualifier qualifier = null;
		if (filtre != null) {
			qualifier = ERXQ.contains(nomColonne, filtre);
		}
		return qualifier;
	}

	/**
	 * construit la condition sur la colonne.
	 * @param nomColonne : nom de la colonne sur la quelle on fait la recherche.
	 * @param filtre : element recherché.
	 * @return EOQualifier
	 */
	protected EOQualifier getEqualsQualifier(String nomColonne, Object filtre) {
		EOQualifier qualifier = null;
		if (filtre != null) {
			qualifier = ERXQ.equals(nomColonne, filtre);
		}
		return qualifier;
	}

	protected String convertToString(Integer number) {
		if (number == null) {
			return "";
		} else {
			return number.toString();
		}
	}
	
	/**
	 * @return annee de l'exercice: propriete d'instance partagee par l'application
	 */
	public Integer getAnneeExercice() {
		Integer annee = (Integer) session().objectForKey(ANNEE_UNIVERSITAIRE_EXERCICE);
		if (annee == null) {
			try {				
			    setAnneeExercice(Integer.parseInt(getParamManager().getParam(FwkCktlDroitsPerimetreScolariteParamManager.GRHUM_ANNEE_UNIVERSITAIRE)));   
            } catch (Exception e) {
	            e.printStackTrace();
	            setAnneeExercice(0);
            }
			
		}
		return (Integer) session().objectForKey(ANNEE_UNIVERSITAIRE_EXERCICE);
	}

	/**
	 * @param anneeExercice : annee de l'exercice 
	 */
	public void setAnneeExercice(Integer anneeExercice) {
		session().setObjectForKey(anneeExercice, ANNEE_UNIVERSITAIRE_EXERCICE);
	}

	/**
	 * @param response : reponse à laquelle on ajoute des ressources
	 * @param context : contexte d'édition
	 */
	public void injectResources(WOResponse response, WOContext context) {
		// TODO Auto-generated method stub
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/default.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/alert.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/lighting.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommon.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "Ajax", "default_ajaxupload.css");
	}

	// Private methodd

}
