package org.cocktail.fwkcktldroitsperimetrescolarite.serveur.components;

import org.cocktail.fwkcktldroitsperimetrescolarite.serveur.commun.ModeEdition;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;


/**
 * Formulaire d'affichage d'un perimetre de donnees
 *
 */
public class PerimetreForm extends FwkCktlDroitsPerimetreScolariteFormComponent {

private static final long serialVersionUID = -6362395292041546259L;
	
	private static final String BINDING_PERIMETRE = "perimetre";
	private static final String BINDING_GD_APPLICATION = "gdApplication";
	private static final String BINDING_MODE_EDITION = "modeEdition";
	
	private ModeEdition modeEdition;
	
	private EOGdStrategie currentTypeStrategie;
	private EOGdStrategie selectedTypeStrategie;
	
	
	/**
	 * Constructeur
	 * @param context : le contexte d'edition
	 */
    public PerimetreForm(WOContext context) {
        super(context);
    }
    
    @Override
	public void appendToResponse(WOResponse response, WOContext context) {
		setSelectedTypeStrategie(null);

		super.appendToResponse(response, context);
	}
    
    public String getLibelleId() {
    	return getComponentId() + "_libelleId";                                                              
    }
    
    public String getTypeStrategieId() {
    	return getComponentId() + "_typeStrategieId";                                                              
    }
    
    public String getPerimetreLibelleContainerId() {
		return getComponentId() + "_perimetreLibelleContainerId";
	}
    
    public String getTypeStrategieContainerId() {
		return getComponentId() + "_typeStrategieContainerId";
	}
    
    /**
     * @return EOPerimetreGirofle : le perimetre passe en binding
     */
    public EOGdPerimetreGirofle getPerimetre() {
		return ((EOGdPerimetreGirofle) valueForBinding(BINDING_PERIMETRE));
	}
    
    /**
	 * a remplacer par le modeEdition defini au niveau de AGirofleComponent
	 * @return mode d'edition
	 */
	public ModeEdition modeEdition() {
		if (hasBinding(BINDING_MODE_EDITION)) {
			setModeEdition((ModeEdition) valueForBinding(BINDING_MODE_EDITION));
		} else {
			setModeEdition(ModeEdition.UNKNOWN);
		}
		
		return modeEdition;
	}

	public void setModeEdition(ModeEdition modeEdition) {
		this.modeEdition = modeEdition;
	}
    
	public boolean isModeEditionCreation() {
		return (modeEdition().equals(ModeEdition.CREATION));
	}

	public boolean isModeEditionModification() {
		return (modeEdition().equals(ModeEdition.MODIFICATION));
	}

	public boolean isModeEditionConsultation() {
		return (modeEdition().equals(ModeEdition.CONSULTATION));
	}

	public boolean isModeEditionUnknown() {
		return (modeEdition().equals(ModeEdition.UNKNOWN));
	}
	
	public EOGdApplication getGdApplication() {
	    return (EOGdApplication) valueForBinding(BINDING_GD_APPLICATION);
    }
    
    /**
	 * @param gdApplication : application concerne par la gestion des droits
	 */
	public void setGdApplication(EOGdApplication gdApplication) {
	    setValueForBinding(gdApplication, BINDING_GD_APPLICATION);
    }
	
	/**
	 * @return NSArray<EOGdStrategie> : liste des types de strategies
	 */
	public NSArray<EOGdStrategie> typesStrategies() {
		return EOGdStrategie.fetchAllSco_GdStrategies(edc(), null);
	}

	public EOGdStrategie getCurrentTypeStrategie() {
		return currentTypeStrategie;
	}
	
	public void setCurrentTypeStrategie(EOGdStrategie currentTypeStrategie) {
		this.currentTypeStrategie = currentTypeStrategie;
	}

	/**
	 * Retourne le type de strategie
	 * Si en mode modification le type de strategie est sette a celui du perimetre passe en binding
	 * @return EOGdStrategie : le type de strategie selectionnee
	 */
	public EOGdStrategie getSelectedTypeStrategie() {
		if (isModeEditionModification() && getPerimetre().strategie() != null) {

			if (selectedTypeStrategie == null) {
				selectedTypeStrategie = getPerimetre().strategie();
			}
		}

		return selectedTypeStrategie;
	}
	
	/**
     * Set le type de strategie en parametre et le set aussi a la strategie du perimetre passe en binding
     * @param selectedTypeStrategie : le type de strategie selectionne
     */
	public void setSelectedTypeStrategie(EOGdStrategie selectedTypeStrategie) {
		this.selectedTypeStrategie = selectedTypeStrategie;
		
		if (selectedTypeStrategie != null) {
			getPerimetre().setStrategieRelationship(selectedTypeStrategie);
			getPerimetre().setLibelle(selectedTypeStrategie.libelle());
			getPerimetre().setComposant(null);
			getPerimetre().setTypeComposant(null);
			getPerimetre().setStructure(null);
			getPerimetre().setGradeUniversitaire(null);
			getPerimetre().setAnnee(null);
			
		}
	}
	
	public boolean isTypeStrategieAccesFormationSelected() {
		return EOGdStrategie.strategieAccesFormation(edc()).equals(getSelectedTypeStrategie());
	}

	public boolean isTypeStrategieAccesElementsFormationSelected() {
		return EOGdStrategie.strategieAccesElementsFormation(edc()).equals(getSelectedTypeStrategie());
	}
	
	public boolean isTypeStrategieAccesElementSelected() {
		return EOGdStrategie.strategieAccesElement(edc()).equals(getSelectedTypeStrategie());
	}
	
	public boolean isTypeStrategieAccesFormationsComposanteSelected() {
		return EOGdStrategie.strategieAccesFormationsComposante(edc()).equals(getSelectedTypeStrategie());
	}
	
	public boolean isTypeStrategieAccesFormationsComposanteUserSelected() {
		return EOGdStrategie.strategieAccesFormationsComposanteUser(edc()).equals(getSelectedTypeStrategie());
	}
	
	public boolean isTypeStrategieAccesElementsFormationUserResponsableSelected() {
		return EOGdStrategie.strategieAccesElementsFormationUserResponsable(edc()).equals(getSelectedTypeStrategie());
	}
	
	public boolean isTypeStrategieAccesGradeSelected() {
		return EOGdStrategie.strategieAccesGrade(edc()).equals(getSelectedTypeStrategie());
	}
	
	/**
	 * @return WOActionResults : update les containers et reste sur la page
	 */
	public WOActionResults update() {
		AjaxUpdateContainer.updateContainerWithID(getPerimetreLibelleContainerId(), context());
		
		AjaxUpdateContainer.updateContainerWithID(getTypeStrategieContainerId(), context());
		
		return doNothing();
	}
	
}
