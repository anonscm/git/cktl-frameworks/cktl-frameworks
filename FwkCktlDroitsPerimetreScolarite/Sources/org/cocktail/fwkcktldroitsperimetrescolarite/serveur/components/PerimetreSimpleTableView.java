package org.cocktail.fwkcktldroitsperimetrescolarite.serveur.components;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.fwkcktlscolpedagui.serveur.components.SimpleTableViewBase;

import com.webobjects.appserver.WOContext;

/**
 * @author lilia
 * Composant d'affichage de EOGdPerimetreGirofle dans une simple table view
 */
public class PerimetreSimpleTableView extends SimpleTableViewBase<EOGdPerimetreGirofle> {
    // Private fields
    private static final long serialVersionUID = 1L;

    // Constructor
	/**
     * @param context : contexte d'édition
     */
    public PerimetreSimpleTableView(WOContext context) {
        super(context);
    }
    
    // Properties
    // Public Methods
    // Protected Methods
    // Private methods

}