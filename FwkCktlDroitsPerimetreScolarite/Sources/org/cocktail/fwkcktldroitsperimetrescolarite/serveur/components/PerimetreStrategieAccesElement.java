package org.cocktail.fwkcktldroitsperimetrescolarite.serveur.components;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EORegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;


/**
 * Interface pour l'affichage des perimetres de type ACCES_ELEMENT
 *
 */
public class PerimetreStrategieAccesElement extends FwkCktlDroitsPerimetreScolariteComponent {
	
	private static final long serialVersionUID = 1L;

	private static final String BINDING_PERIMETRE = "perimetre";
	
	// Private fields
	private NSArray<EOTypeComposant> typesComposants = null;
	private EOTypeComposant currentTypeComposant = null;
	private EOTypeComposant selectedTypeComposant = null;
	
	private ERXDisplayGroup<EOEC> displayGroupECs = null;
	private ERXDatabaseDataSource typeECDatasource = null;
	private EOEC selectedEC = null;
	
	private ERXDisplayGroup<EOUE> displayGroupUEs = null;
	private ERXDatabaseDataSource typeUEDatasource = null;
	private EOUE selectedUE = null;
	
	private ERXDisplayGroup<EORegroupement> displayGroupRegroupements = null;
	private ERXDatabaseDataSource typeRegroupementDatasource = null;
	private EORegroupement selectedRegroupement = null;
	
	
	/**
	 * Constructeur
	 * @param context : le contexte d'edition
	 */
    public PerimetreStrategieAccesElement(WOContext context) {
        super(context);
    }
    
    @Override
	public void appendToResponse(WOResponse response, WOContext context) {
		setSelectedTypeComposant(null);
		setSelectedEC(null);
		setSelectedUE(null);
		setSelectedRegroupement(null);
		displayGroupECs = null;
		displayGroupUEs = null;
		displayGroupRegroupements = null;

		if (isTypeSelectedEC() && getPerimetre().composant() != null) {
			setSelectedEC((EOEC) getPerimetre().composant());
		}
		
		if (isTypeSelectedUE() && getPerimetre().composant() != null) {
			setSelectedUE((EOUE) getPerimetre().composant());
		}
		
		if (isTypeSelectedRegroupement() && getPerimetre().composant() != null) {
			setSelectedRegroupement((EORegroupement) getPerimetre().composant());
		}

		super.appendToResponse(response, context);
	}
    
    public String getTypesComposantsId() {
		return getComponentId() + "_typesComposantsId";
	}

	public String getComposantId() {
		return getComponentId() + "_composantId";
	}
	
	public String getListeComposantsContainerId() {
		return getComponentId() + "_listeComposantsContainerId";
	}
	
	public String getListeComposantsId() {
		return getComponentId() + "_listeComposantsId";
	}
    
    /**
     * @return EOPerimetreGirofle : le perimetre passe en binding
     */
    public EOGdPerimetreGirofle getPerimetre() {
		return ((EOGdPerimetreGirofle) valueForBinding(BINDING_PERIMETRE));
	}
    
    /**
	 * @return liste des types de composants supportés par le syllabus
	 */
	public NSArray<EOTypeComposant> getTypesComposants() {
		if (typesComposants == null) {
			typesComposants = new NSMutableArray<EOTypeComposant>();
			typesComposants.add(EOTypeComposant.typeRegroupement(edc()));
			typesComposants.add(EOTypeComposant.typeUE(edc()));
			typesComposants.add(EOTypeComposant.typeEC(edc()));
		}

		return typesComposants;
	}

	public EOTypeComposant getCurrentTypeComposant() {
		return currentTypeComposant;
	}

	public void setCurrentTypeComposant(EOTypeComposant currentTypeComposant) {
		this.currentTypeComposant = currentTypeComposant;
	}

	/**
	 * Si le perimetre un composant alors on est en mode modification et dans ce cas on set le selectedTypeComposant a celui correspondant au perimetre
	 * @return EOTypeComposant : le type de composant selectionne dans la liste deroulante
	 */
	public EOTypeComposant getSelectedTypeComposant() {
		if (getPerimetre().composant() != null) {
			if (selectedTypeComposant == null) {
				selectedTypeComposant = getPerimetre().composant().getTypeComposant();
			}
		}
		
		return selectedTypeComposant;
	}

	public void setSelectedTypeComposant(EOTypeComposant selectedTypeComposant) {
		this.selectedTypeComposant = selectedTypeComposant;
	}
	
	public boolean isTypeSelectedEC() {
		return EOTypeComposant.typeEC(edc()).equals(getSelectedTypeComposant());
	}

	public boolean isTypeSelectedUE() {
		return EOTypeComposant.typeUE(edc()).equals(getSelectedTypeComposant());
	}

	public boolean isTypeSelectedRegroupement() {
		return EOTypeComposant.typeRegroupement(edc()).equals(getSelectedTypeComposant());
	}
	
	// Table des ECs
	public EOEC getSelectedEC() {
		return selectedEC;
	}

    /**
     * Set l'EC en parametre et le set aussi au perimetre passe en binding
     * @param selectedEC : le'EC sélectionne
     */
	public void setSelectedEC(EOEC selectedEC) {
		this.selectedEC = selectedEC;

		if (selectedEC != null) {
			getPerimetre().setComposant(selectedEC);
		}
	}

    /**
	 * @return ERXDisplayGroup<EOEC> : display group des ECs affiches
	 */
	public ERXDisplayGroup<EOEC> getDisplayGroupECs() {
		if (displayGroupECs == null) {
			displayGroupECs = new ERXDisplayGroup<EOEC>();
			displayGroupECs.setDataSource(getECDatasource());

			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOEC.CODE_KEY, EOSortOrdering.CompareAscending));
			displayGroupECs.setSortOrderings(sortOrderings);

			displayGroupECs.setSelectsFirstObjectAfterFetch(false);
			displayGroupECs.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupECs.fetch();
			
			if (getSelectedEC() != null) {
				displayGroupECs.setSelectedObject(getSelectedEC());
			}
			
		}
		
		return displayGroupECs;
	}
	
	/**
	 * @return ERXDatabaseDataSource : datasource des ECs
	 */
	private ERXDatabaseDataSource getECDatasource() {
		if (typeECDatasource == null) {
			typeECDatasource = new ERXDatabaseDataSource(edc(), EOEC.ENTITY_NAME);
		}
		
		return typeECDatasource;
	}
	
	// Table des UEs
	public EOUE getSelectedUE() {
		return selectedUE;
	}

    /**
     * Set l'UE en parametre et le set aussi au perimetre passe en binding
     * @param selectedUE : l'UE sélectionnee
     */
	public void setSelectedUE(EOUE selectedUE) {
		this.selectedUE = selectedUE;

		if (selectedUE != null) {
			getPerimetre().setComposant(selectedUE);
		}
	}

    /**
	 * @return ERXDisplayGroup<EOUE> : display group des UEs affichees
	 */
	public ERXDisplayGroup<EOUE> getDisplayGroupUEs() {
		if (displayGroupUEs == null) {
			displayGroupUEs = new ERXDisplayGroup<EOUE>();
			displayGroupUEs.setDataSource(getUEDatasource());

			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOUE.CODE_KEY, EOSortOrdering.CompareAscending));
			displayGroupUEs.setSortOrderings(sortOrderings);

			displayGroupUEs.setSelectsFirstObjectAfterFetch(false);
			displayGroupUEs.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupUEs.fetch();
			
			if (getSelectedUE() != null) {
				displayGroupUEs.setSelectedObject(getSelectedUE());
			}
		}
		
		return displayGroupUEs;
	}
	
	/**
	 * @return ERXDatabaseDataSource : datasource des UEs
	 */
	private ERXDatabaseDataSource getUEDatasource() {
		if (typeUEDatasource == null) {
			typeUEDatasource = new ERXDatabaseDataSource(edc(), EOUE.ENTITY_NAME);
		}
		
		return typeUEDatasource;
	}
	
	// Table des Regroupements
	public EORegroupement getSelectedRegroupement() {
		return selectedRegroupement;
	}

	/**
     * Set le Regroupement en parametre et le set aussi au perimetre passe en binding
     * @param selectedRegroupement : le Regroupement selectionne
     */
	public void setSelectedRegroupement(EORegroupement selectedRegroupement) {
		this.selectedRegroupement = selectedRegroupement;

		if (selectedRegroupement != null) {
			getPerimetre().setComposant(selectedRegroupement);
		}
	}
	
	/**
	 * @return ERXDisplayGroup<EORegroupement> : display group des regroupements affiches
	 */
	public ERXDisplayGroup<EORegroupement> displayGroupRegroupements() {
		if (displayGroupRegroupements == null) {
			displayGroupRegroupements = new ERXDisplayGroup<EORegroupement>();
			displayGroupRegroupements.setDataSource(getRegroupementsDatasource());

			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EORegroupement.CODE_KEY, EOSortOrdering.CompareAscending));
			displayGroupRegroupements.setSortOrderings(sortOrderings);

			displayGroupRegroupements.setSelectsFirstObjectAfterFetch(false);
			displayGroupRegroupements.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupRegroupements.fetch();
			
			if (getSelectedRegroupement() != null) {
				displayGroupRegroupements.setSelectedObject(getSelectedRegroupement());
			}
			
		}

		return displayGroupRegroupements;
	}

	/**
	 * @return ERXDatabaseDataSource : data sources des regroupements
	 */
	private ERXDatabaseDataSource getRegroupementsDatasource() {
		if (typeRegroupementDatasource == null) {
			typeRegroupementDatasource = new ERXDatabaseDataSource(edc(), EORegroupement.ENTITY_NAME);
		}
		return typeRegroupementDatasource;
	}
	
}