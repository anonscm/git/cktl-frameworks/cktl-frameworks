package org.cocktail.fwkcktldroitsperimetrescolarite.serveur.components;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;


/**
 * Interface pour l'affichage des perimetres de type ACCES_GRADE
 *
 */
public class PerimetreStrategieAccesGrade extends FwkCktlDroitsPerimetreScolariteComponent {
	
	private static final long serialVersionUID = 1L;

	private static final String BINDING_PERIMETRE = "perimetre";
	
	private NSArray<EOGradeUniversitaire> gradesUniversitaires = new NSArray<EOGradeUniversitaire>();
	private EOGradeUniversitaire currentGrade;
	private EOGradeUniversitaire selectedGrade;
	
	
	/**
	 * Constructeur
	 * @param context : le contexte d'edition
	 */
    public PerimetreStrategieAccesGrade(WOContext context) {
        super(context);
    }
    
    @Override
	public void appendToResponse(WOResponse response, WOContext context) {
		setSelectedGrade(null);

		if (getPerimetre().gradeUniversitaire() != null) {
			setSelectedGrade(getPerimetre().gradeUniversitaire());
		}

		super.appendToResponse(response, context);
	}
    
    public String getGradeUniversitaireId() {
    	return getComponentId() + "_gradeUniversitaireId";                                                              
    }
    
    /**
     * @return EOPerimetreGirofle : le perimetre passe en binding
     */
    public EOGdPerimetreGirofle getPerimetre() {
		return ((EOGdPerimetreGirofle) valueForBinding(BINDING_PERIMETRE));
	}
    
    /**
     * @return NSArray<EOGradeUniversitaire> : la liste des grades universitaires
     */
    public NSArray<EOGradeUniversitaire> getGradesUniversitaires() {
		gradesUniversitaires = new NSArray<EOGradeUniversitaire>();
		gradesUniversitaires = EOGradeUniversitaire.fetchAllSco_GradeUniversitaires(edc(), null);
		
		return gradesUniversitaires;
	}

	public EOGradeUniversitaire getCurrentGrade() {
		return currentGrade;
	}
	public void setCurrentGrade(EOGradeUniversitaire currentGrade) {
		this.currentGrade = currentGrade;
	}

	public EOGradeUniversitaire getSelectedGrade() {
		return selectedGrade;
	}
	
	/**
     * Set le grade en parametre et le set aussi au perimetre passe en binding
     * @param selectedGrade : le grade selectionne
     */
	public void setSelectedGrade(EOGradeUniversitaire selectedGrade) {
		this.selectedGrade = selectedGrade;
		
		if (selectedGrade != null) {
			getPerimetre().setGradeUniversitaire(selectedGrade);
		}
	}
	
}