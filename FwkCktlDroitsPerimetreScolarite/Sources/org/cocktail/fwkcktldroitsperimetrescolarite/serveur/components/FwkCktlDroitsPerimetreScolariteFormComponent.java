package org.cocktail.fwkcktldroitsperimetrescolarite.serveur.components;

import org.cocktail.fwkcktldroitsperimetrescolarite.serveur.commun.ModeEdition;

import com.webobjects.appserver.WOContext;

public class FwkCktlDroitsPerimetreScolariteFormComponent extends FwkCktlDroitsPerimetreScolariteComponent {

	// Private fields
	private boolean validationDidFail = false;
	private ModeEdition modeEdition = ModeEdition.UNKNOWN;

	// Constructor
	/**
	 * @param context : contexte d'édition.
	 */
	public FwkCktlDroitsPerimetreScolariteFormComponent(WOContext context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	// Properties
	/**
	 * @return true if validation did fail
	 */
	public boolean isValidationDidFail() {
		return validationDidFail;
	}

	/**
	 * @param validationDidFail the validationDidFail to set
	 */
	public void setValidationDidFail(boolean validationDidFail) {
		this.validationDidFail = validationDidFail;
	}

	/**
	 * @return the modeEdition
	 */
	public ModeEdition getModeEdition() {
		return modeEdition;
	}

	/**
	 * @param modeEdition the modeEdition to set
	 */
	public void setModeEdition(ModeEdition modeEdition) {
		this.modeEdition = modeEdition;
	}

	// Public methods
	/**
	 * détermine si le formulaire a été correctement rempli.
	 * @return true si la validation du formulaire est correcte.
	 */
	public Boolean statutValidation() {
		Boolean statutOK = true;
		if (validationDidFail) {
			validationDidFail = false;
			statutOK = false;
		}

		return statutOK;
	}

	@Override
	public void validationFailedWithException(Throwable e, Object obj, String keyPath) {
		validationDidFail = true;
		session().addSimpleErrorMessage(localisation("application.labelErreur"), e.getMessage());
		super.validationFailedWithException(e, obj, keyPath);
	}

	public boolean isModeEditionCreation() {
		return (getModeEdition().equals(ModeEdition.CREATION));
	}

	public boolean isModeEditionDuplication() {
		return (getModeEdition().equals(ModeEdition.DUPLICATION));
	}

	public boolean isModeEditionModification() {
		return (getModeEdition().equals(ModeEdition.MODIFICATION));
	}

	public boolean isModeEditionConsultation() {
		return (getModeEdition().equals(ModeEdition.CONSULTATION));
	}

	public boolean isModeEditionUnknown() {
		return (getModeEdition().equals(ModeEdition.UNKNOWN));
	}

}
