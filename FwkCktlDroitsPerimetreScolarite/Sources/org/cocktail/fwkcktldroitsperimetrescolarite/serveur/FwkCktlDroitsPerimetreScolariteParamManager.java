package org.cocktail.fwkcktldroitsperimetrescolarite.serveur;

import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

/**
 * Gestion des parametres
 */
public class FwkCktlDroitsPerimetreScolariteParamManager extends CktlParamManager {
	public static final String GRHUM_ANNEE_UNIVERSITAIRE = "GRHUM_ANNEE_UNIVERSITAIRE";

	@Override
	public void createNewParam(String key, String value, String comment) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createNewParam(String key, String value, String comment, String type) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getParam(String key) {
		return getApplication().config().stringForKey(key);
	}

}
