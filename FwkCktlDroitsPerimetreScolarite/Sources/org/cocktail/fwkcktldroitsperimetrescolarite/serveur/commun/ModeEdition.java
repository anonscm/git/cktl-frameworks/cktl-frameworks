package org.cocktail.fwkcktldroitsperimetrescolarite.serveur.commun;

/**
 * Cet enum liste les différents modes d'éditions possibles.
 * @author lilia drouot 
 */
public enum ModeEdition {
    UNKNOWN, CREATION, MODIFICATION, DUPLICATION, CONSULTATION;
}
