--
-- Mise a jour des donnees de CKTL_GED du 10/06/2011 à executer depuis le user GRHUM
--
SET DEFINE OFF;
whenever sqlerror exit sql.sqlcode ;

INSERT INTO "CKTL_GED"."TYPE_DOCUMENT" (TD_ID, TD_STR_ID, TD_LIBELLE) VALUES ("CKTL_GED".TYPE_DOCUMENT_SEQ.nextval, 'org.cocktail.ged.typedocument.gfc.acte.autre', 'Autre type');
INSERT INTO "CKTL_GED"."TYPE_DOCUMENT" (TD_ID, TD_STR_ID, TD_LIBELLE) VALUES ("CKTL_GED".TYPE_DOCUMENT_SEQ.nextval, 'org.cocktail.ged.typedocument.gfc.acte.contrat', 'Contrat');
INSERT INTO "CKTL_GED"."TYPE_DOCUMENT" (TD_ID, TD_STR_ID, TD_LIBELLE) VALUES ("CKTL_GED".TYPE_DOCUMENT_SEQ.nextval, 'org.cocktail.ged.typedocument.gfc.acte.avenant', 'Avenant');
INSERT INTO "CKTL_GED"."TYPE_DOCUMENT" (TD_ID, TD_STR_ID, TD_LIBELLE) VALUES ("CKTL_GED".TYPE_DOCUMENT_SEQ.nextval, 'org.cocktail.ged.typedocument.gfc.acte.devis', 'Devis');
INSERT INTO "CKTL_GED"."TYPE_DOCUMENT" (TD_ID, TD_STR_ID, TD_LIBELLE) VALUES ("CKTL_GED".TYPE_DOCUMENT_SEQ.nextval, 'org.cocktail.ged.typedocument.gfc.acte.facture', 'Facture');

COMMIT;