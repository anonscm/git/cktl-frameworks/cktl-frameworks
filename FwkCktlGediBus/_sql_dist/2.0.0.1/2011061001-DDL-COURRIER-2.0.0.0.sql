--
-- Patch de COURRIER du 10/06/2011 à executer depuis le user GRHUM
--
SET DEFINE OFF;
whenever sqlerror exit sql.sqlcode ;

alter table COURRIER.REFERENCE_LIEN add DOCUMENT_ID NUMBER(38,0);
COMMENT ON COLUMN COURRIER.REFERENCE_LIEN.DOCUMENT_ID IS 'Reference au document CKTL_GED.DOCUMENT';

--------------------------------------------------------
--  DDL for Table DB_VERSION
--------------------------------------------------------

  CREATE TABLE "COURRIER"."DB_VERSION" 
   (	"DBV_ID" NUMBER(4,0), 
	"DBV_LIBELLE" VARCHAR2(15 BYTE), 
	"DBV_DATE" DATE, 
	"DBV_INSTALL" DATE, 
	"DBV_COMMENT" VARCHAR2(2000 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "COURRIER"."DB_VERSION"."DBV_ID" IS 'Identifiant de la version';
 
   COMMENT ON COLUMN "COURRIER"."DB_VERSION"."DBV_LIBELLE" IS 'Libelle de la version';
 
   COMMENT ON COLUMN "COURRIER"."DB_VERSION"."DBV_DATE" IS 'Date de release de la version';
 
   COMMENT ON COLUMN "COURRIER"."DB_VERSION"."DBV_INSTALL" IS 'Date d''installation de la version. Si non renseignee, la version n''est pas completement installee.';
 
   COMMENT ON COLUMN "COURRIER"."DB_VERSION"."DBV_COMMENT" IS 'Le commentaire : une courte description de cette version de la base de donnees.';
 
   COMMENT ON TABLE "COURRIER"."DB_VERSION"  IS 'Table contenant les versions des scripts pass?s';
--------------------------------------------------------
--  DDL for Index PK_DB_VERSION
--------------------------------------------------------

  CREATE UNIQUE INDEX "COURRIER"."PK_DB_VERSION" ON "COURRIER"."DB_VERSION" ("DBV_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
--------------------------------------------------------
--  Constraints for Table DB_VERSION
--------------------------------------------------------

  ALTER TABLE "COURRIER"."DB_VERSION" ADD CONSTRAINT "PK_DB_VERSION" PRIMARY KEY ("DBV_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH"  ENABLE;
 
  ALTER TABLE "COURRIER"."DB_VERSION" MODIFY ("DBV_ID" NOT NULL ENABLE);
 
  ALTER TABLE "COURRIER"."DB_VERSION" MODIFY ("DBV_LIBELLE" NOT NULL ENABLE);
 
  ALTER TABLE "COURRIER"."DB_VERSION" MODIFY ("DBV_DATE" NOT NULL ENABLE);
--------------------------------------------------------
--  DDL for Sequence DB_VERSION_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "COURRIER"."DB_VERSION_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 38 NOCACHE  NOORDER  NOCYCLE ;

-- Maj de  la version
Insert into COURRIER.DB_VERSION (DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) values (COURRIER.DB_VERSION_SEQ.nextval,'2.0.0.1',to_date('10/06/2011','DD/MM/YYYY'),sysdate,'Modifications pour intégration de la gestion des documents via ERAttachment');

COMMIT;