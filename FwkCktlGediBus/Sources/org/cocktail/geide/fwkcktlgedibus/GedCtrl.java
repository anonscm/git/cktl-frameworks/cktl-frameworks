/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.geide.fwkcktlgedibus;

import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.GEDDescription;
import org.cocktail.fwkcktlwebapp.server.CktlGedBus;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien;
import org.cocktail.geide.fwkcktlgedibus.tools.GedTool;
import org.cocktail.geide.fwkcktlgedibus.tools.GediBus;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSPathUtilities;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEC;

/**
 * Classe permetant une utilisation simplifier de la GED (methode de haut niveau d'abstraction)<br>
 * Dans le fichier de configuration de l'application, vous devez avoir ceci:<br>
 * <ul>
 * <li><strong>APP_USE_GED=YES</strong><br> 
 * <ul><li>Permet l'initialisation de la ged lors de l'instanciation de cette classe)</li></ul></li>
 * <li><strong>TYPE_ROOT_GED=26</strong><br> 
 * <ul><li>Permet de dire que les fichiers seront pour cette application deposer &agrave; partir du
 * 			repertoire No 26.<br>S'il y a un chemin dans le nom du fichier alors celui ci sera 
 * 			deposer dans la branche corespondante depuis le repertoire <strong>TYPE_ROOT_GED</strong>.<br>
 * 			Deplus, si la branche n'existe pas elle sera cr&eacute;&eacute;.</li></ul></li>
 * 
 * 
 * @author mparadot
 *
 */

public class GedCtrl {
    private static CktlWebApplication app = (CktlWebApplication)WOApplication.application();
	private GedTool ged = null;
	
	public GedCtrl() {
		// Initialisation des objets necessaires à l'utilisation de la Ged
		// dans un nouvel editingcontext independant
		ged = ged();
	}
	
	public GedTool ged() {
		if(ged==null)
		{
			try {
	        	if(((CktlWebApplication)WOApplication.application()).config().booleanForKey("APP_USE_GED"))
	        		// ged = new GedTool(new EOEditingContext());
        			ged = new GedTool(ERXEC.newEditingContext());
	        	else
	        	{
					if(!gedDisponible())
						System.out.println("-> le paramètre \"APP_USE_GED\" doit être positionné à TRUE");
	        	}
			} catch (Throwable e) {
				e.printStackTrace();
				System.out.println("Erreur d'initialiosation de la GED...");
				System.out.println("-> Vous devez utiliser le parametre \"APP_USE_GED\" dans le fichier de config");
			}
		}
		return ged;
	}

	/**
	 * Permet de r&eacute;cuperer la derniere erreur dans la ged
	 * @return retourne le dernier message d'erreur de la ged ou ""
	 */
    public String getLastErrorGed() {
    	if(ged()!=null && ged().getLastError()!=null)
    		return ged().getLastError().getMessage();
    	return "";
    }
    
    /**
     * Permet de savoir si la ged est disponible
     * @return true disponible, false sinon
     */
    public boolean gedDisponible() {
    	if(((CktlWebApplication)WOApplication.application()).config().valueForKey("APP_USE_GED")==null)
    		return false;
    	return ((CktlWebApplication)WOApplication.application()).config().booleanForKey("APP_USE_GED")?true:false;
    }

	/**
	 *     
	 * @param editingContext L'editingContext dans lequel les objets retournes devront-etre presents
	 * @param document Le document sous forme de NSData
	 * @param documentName Nom du document à déposer. ex:contrat2009_01564.pdf
	 * @param racineReference Racine de la reference qui sera associee au document: ex:Contrat2009_01564
	 * @param gedfsCategorie La categorie physique dans laquelle le document est déposé. Le document est enregistré dans l'arborescence des documents de cette catégorie.
	 * @param gediRoot La racine logique sous laquelle le document sera deposé. 
	 * 			Correspond au type_ordre dans la table Type_Courrier_New du user Courrier
	 * 			Si null, recuperation du root à travers la variable de configuration ROOT_GED_GROUPE_PARTENAIRE
	 * @param gediCategorie Le chemin logique à partir de la racine gediRoot du document sous forme d'un path. ex:CONTRAT 2009-0001/Documents principaux
	 * 			Si null, le document est directement déposé sous la racine
	 * @param utilisateur Instance de ApplicationUser qui permet de connaitre les differents attributs et droits de l'utilisateur realisant l'operation
	 * @return le document instance de Courrier si ok, null sinon
	 */
    public Courrier deposerUnDocument(EOEditingContext editingContext,
    		NSData document, String url, String documentName, String racineReference,
    		String objet, String commentaire, String motsCles, 
    		NSTimestamp dateDocument, NSTimestamp dateDerniereModification,
    		Integer visibilite,
    		String gedfsCategorie, 
    		Integer gediRoot, String gediCategorie,
    		ApplicationUser utilisateur) throws Exception {
    	Courrier documentGed = null;
    	
    	if (ged == null) {
            throw new Exception("GedCtrl doit-etre initialise avec un editingContext et un applicationUser");
    	}
		if (gedfsCategorie == null || ((document == null || documentName == null) && (url == null || url.endsWith("//"))) || editingContext == null || utilisateur == null) {
			// Ces arguments sont obligatoires et doivent-etre differents de null
            throw new Exception("Les arguments 'document', 'documentName' ou url, 'gedfsCategorie', 'editingContext' et 'utilisateur' doivent-etre renseignes");
		}

		// Initialisation de la racine Gedi
		if (gediRoot != null) {
			ged.setRootGed(String.valueOf(gediRoot));
		} else {
			String typeOrdre = EOGrhumParametres.parametrePourCle(editingContext, "ROOT_GED_GROUPE_PARTENAIRE");

			if (MyStringCtrl.isEmpty(typeOrdre)) {
				throw new Exception("Le parametre GRHUM " + "ROOT_GED_GROUPE_PARTENAIRE" + " n'est pas defini.");
			}
			gediRoot = Integer.valueOf(typeOrdre);
    		ged.setRootGed(typeOrdre);
		}

		GEDDescription documentGedfs = null;
		if (document != null && document.length()>0) {
			// Depot physique du document sur le serveur gedfs
			int numeroDocument = -1;
			CktlGedBus gedBus = app.gedBus(utilisateur.getNoIndividu().intValue());
			numeroDocument = gedBus.saveDocumentGED(document.bytes(), documentName, documentName, gedfsCategorie);
	        documentGedfs = gedBus.inspectDocumentGED(numeroDocument);
	
			if (numeroDocument == -1 || documentGedfs == null) {
	            throw new Exception("Erreur lors du depot du document sur gedfs: "+gedBus.gedMessage());			
			}
		}
		
		// Depot logique du document dans l'arborescence de gedi
		if (visibilite == null) {
			// La visibilite par defaut est PRIVEE
			visibilite = new Integer(Courrier.PRIVE);
		}
		if (dateDocument == null) {
			dateDocument = DateCtrl.now();
		}
		if (dateDerniereModification == null) {
			dateDerniereModification = DateCtrl.now();
		}
		documentGed = Courrier.creer(editingContext, documentName, gediRoot, gediCategorie, racineReference, visibilite, objet, commentaire, motsCles, dateDocument, dateDerniereModification, utilisateur);
		
		if (documentGed != null) {
			ReferenceLien rl = ReferenceLien.creer(editingContext, documentGed, documentGedfs, url, NSPathUtilities.stringByAppendingPathComponent(gediCategorie, documentName), null);
			rl.setCouNumero(documentGed.couNumero());
			documentGed.addToReferenceLiensRelationship(rl);
			ged.edtitingContext().saveChanges();
			documentGed = (Courrier)EOUtilities.localInstanceOfObject(editingContext, documentGed);
		}

		return documentGed;
    }
    
    public boolean supprimerUnDocument(Courrier document) {
    	boolean supprimerUnDocument = false;
    	// Suppression complete du document dans Gedi
    	// Le document gedfs ne devrait-etre eliminer que lors du saveChanges 
    	// et si il n'est plus reference par aucun autre Courrier
    	if (document != null) {
    		document.deleteAllReferenceLiensRelationships();
    		document.deleteAllGB_DestinatairesRelationships();
    		document.deleteAllGB_RepartCourAuteursRelationships();
    		document.deleteAllGB_TexteMultilinguesRelationships();
    		document.deleteAllSpecifWebRelationships();
    		document.deleteAllTypesAssociesRelationships();
    		document.editingContext().deleteObject(document);
    	}
    	
    	return supprimerUnDocument;
    }
    
    public Courrier archiverUnDocument(Courrier document) {
    	// TODO A coder
    	return null;
    }
    
    /**
     * Permet d'ajouter un fichier dans la ged, en echange, on r&eacute;cup&egrave;re un num&eacute;ro de courrier associ&eacute;
     * @param data le fichier
     * @param name le nom du fichier visible dans la ged. si le non de fichier contient de '/', alors le fichier sera place dans l'arboressance coresspondant depuis le repertoire TYPE_ROOT_GED de l'application
     * @param persId le persId de la personne qui depose le fichier
     * @return le num&eacute;ro du courrier associ&eacute; au fichier d&eacute;pos&eacute;, ou null si il y a un probl&egrave;me.
     */
   public Integer ajouterFicherDansGed(NSData data, String name, Number persId)
    {
        Integer couNum = null;
        try
        {
        	ged().edtitingContext().lock();
        	System.out.println("type root = "+((CktlWebApplication)WOApplication.application()).config().stringForKey("TYPE_ROOT_GED"));
        	if(ged().getRootGed()==null)
        		ged().setRootGed(((CktlWebApplication)WOApplication.application()).config().stringForKey("TYPE_ROOT_GED"));
            couNum = ged().saveFileIntoGed(data, name, new Integer(persId.intValue()));
        }
        catch(Throwable e)
        {
            e.printStackTrace();
        }
        finally{
        	ged().edtitingContext().unlock();
        }
        return couNum;
    }
    
   /**
    * Permet de choisir la visibilit&eacute; des fichiers a d&eacute;poser.
    * @param i un des type de visibilit&eacute; suivant: {@link GediBus#TYPE_INTERNET},{@link GediBus#TYPE_EXTRANET},{@link GediBus#TYPE_PRIVE}, {@link GediBus#TYPE_MASQUEE}
    */
    public void setVisibiliteFichierDansGedPourApplication(Integer i){
    	if(ged()!=null)
    		ged().setVisibiliteForFileToSave(i);
    }
    
    /**
     * Permet de savoir quel est la visibilit&eacute; en cours des fichiers a d&eacute;poser.
     * @return un des type de visibilit&eacute; suivant: {@link GediBus#TYPE_INTERNET},{@link GediBus#TYPE_EXTRANET},{@link GediBus#TYPE_PRIVE}, {@link GediBus#TYPE_MASQUEE}
     */
    public Integer getVisibiliteFichierDansGedPourApplication(){
    	if(ged()==null)
    		return null;
    	return ged().getVisibiliteForFileToSave();
    }

    /**
     * Permet de supprimer les fichiers de la ged
     * @param numero num&eacute;ro du courrier associ&eacute; au fichier
     * @param persId persId de la personne qui fait la suppression
     * @return true si la suppression est ok, false sinon (on peut avoir l'erreur avec {@link #getLastErrorGed()} )
     */
    public Boolean supprimerFicherDansGed(Integer numero, Number persId) {
        return new Boolean(ged().deleteFileIntoGed(numero, new Integer(persId.intValue())));
    }

    /**
     * Permet de recup&eacute;rer le(s) url du(des) fichier(s) associ&eacute;(s) a un courrier
     * @param numero num&eacute;ro du courrier 
     * @param persId persId de la personne qui consulte l'url
     * @return list de(des) l'url du(des) fichier(s)
     */
    public NSArray urlFichersDansGed(Integer numero, Number persId) {
        NSArray list = null;
        try {
            ged().edtitingContext().lock();
            list = ged().listUrlFileIntoGed(numero);
		} catch (Throwable e) {
			e.printStackTrace();
		}finally{
			ged().edtitingContext().unlock();
		}
        if (list == null) {
            list = new NSArray();
        }
        return list;
    }

    /**
     * Permet de recup&eacute;rer le(s) nom(s) du(des) fichier(s) associ&eacute;(s) a un courrier
     * @param numero num&eacute;ro du courrier 
     * @param persId persId de la personne qui consulte l'url
     * @return list du(des) nom(s) du(des) fichier(s)
     */
    public NSArray namesFichersDansGed(Integer numero, Number persId) {
        NSArray list = null;
        int noInd = noIndividuForPersId(persId);
        try {
            ged().edtitingContext().lock();
            list = ged().listNameFileIntoGed(numero,noInd);
		} catch (Throwable e) {
			e.printStackTrace();
		}finally{
			ged().edtitingContext().unlock();
		}
        if (list == null) {
            list = new NSArray();
        }
        return list;
    }

    private int noIndividuForPersId(Number persId) {
        return noIndividuForPersId(new Integer(persId.intValue()));
    }
    
    private int noIndividuForPersId(Integer persId) {
    	if(persId==null || persId.intValue()==0 || persId.intValue()==-1)
    		return -1;
        CktlUserInfoDB user = new CktlUserInfoDB(((CktlWebApplication)WOApplication.application()).dataBus());
        user.individuForPersId(persId, false);
        return user.noIndividu().intValue();
    }
    
    //Pour l'affichage de l'arboressence
    
    /**
     * Donne le nom du repertoire pour un numero de repertoire
     * @param numero le numero du reprtoire
     */
    public String nomRepertoireForNumero(int numero)
    {
    	return nomRepertoireForNumero(""+numero);
    }
    
    /**
     * Donne le nom du repertoire pour un numero de repertoire
     * @param numero le numero du reprtoire
     */
    public String nomRepertoireForNumero(String numero)
    {
    	return ged().nomRepertoireForNumero(numero);
    }
    
    /**
     * liste des numeros des repertoire fils
     * @param numero le numero du reprtoire pere
     * @return les numero des repertoire fils ou null si pas de fils
     */
    public NSArray listNumeroRepertoireFilsForNumeroPere(int numero){
    	return listNumeroRepertoireFilsForNumeroPere(""+numero);
    }
    
    
    /**
     * liste des numeros des repertoire fils
     * @param numero le numero du reprtoire pere
     * @return les numero des repertoire fils ou null si pas de fils
     */
    public NSArray listNumeroRepertoireFilsForNumeroPere(String numero){
    	return ged().listNumeroRepertoireFilsForNumero(numero);
    }
    
    /**
     * numero du pere pour un repertoire fils
     * @param numero le numero du reprtoire fils
     * @return le numero du reprtoire pere
     */
    public String numeroRepertoirPereForNumeroFils(int numero){
    	return numeroRepertoirPereForNumeroFils(""+numero);
    }
    
    
    /**
     * numero du pere pour un repertoire fils
     * @param numero le numero du reprtoire fils
     * @return le numero du reprtoire pere
     */
    public String numeroRepertoirPereForNumeroFils(String numero){
    	return ged().numeroRepertoirePereForNumeroFils(numero);
    }
    
    /**
     * Permet de changer la visibilit&eacute; d'un repertoire 
     * Attention ne repecte pas les droits de la ged !!!
     * @param numero le numero du reprtoire dont on veut modifier la visibilit&eacute;
     * @param visibilite la nouvelle visibilit&eacute; a applique : {@link GediBus#TYPE_INTERNET},{@link GediBus#TYPE_EXTRANET},{@link GediBus#TYPE_PRIVE}, {@link GediBus#TYPE_MASQUEE}
     */
    public void setVisibiliteForNumeroRepertoire(int numero,Integer visibilite){
    	setVisibiliteForNumeroRepertoire(""+numero, visibilite);
    }
    
    
    /**
     * Permet de changer la visibilit&eacute; d'un repertoire 
     * Attention ne repecte pas les droits de la ged !!!
     * @param numero le numero du reprtoire dont on veut modifier la visibilit&eacute;
     * @param visibilite la nouvelle visibilit&eacute; a applique : {@link GediBus#TYPE_INTERNET},{@link GediBus#TYPE_EXTRANET},{@link GediBus#TYPE_PRIVE}, {@link GediBus#TYPE_MASQUEE}
     */
    public void setVisibiliteForNumeroRepertoire(String numero,Integer visibilite){
    	ged().setVisibiliteForNumeroRepertoire(numero, visibilite);
    }
    
    /**
     * permet de connaitre la visibilit&eacute; d'un repertoire
     * @param numero le numero du reprtoire dont on veut connaitre la visibilit&eacute;
     * @return la visibilit&eacute; du repertoire
     */
    public Integer visibiliteForNumeroRepertoire(int numero){
    	return visibiliteForNumeroRepertoire(""+numero);
    }
    
    /**
     * permet de connaitre la visibilit&eacute; d'un repertoire
     * @param numero le numero du reprtoire dont on veut connaitre la visibilit&eacute;
     * @return la visibilit&eacute; du repertoire
     */
    public Integer visibiliteForNumeroRepertoire(String numero)
    {
    	return ged().getVisibiliteForNumeroRepertoire(numero);
    }
    
    /**
     * Liste des numero des fichiers pour un repertoire
     * @param numero le numero du repertoire
     * @return la liste des numeros de fichier
     */
    public NSArray listNumeroFichierForNumeroRepertoire(int numero){
    	return listNumeroFichierForNumeroRepertoire(""+numero);
    }
    
    /**
     * Liste des numero des fichiers pour un repertoire
     * @param numero le numero du repertoire
     * @return la liste des numeros de fichier
     */
    public NSArray listNumeroFichierForNumeroRepertoire(String numero){
    	return ged().listNumeroCourrierForNumeroRepertoire(numero);
    }
    
    /**
     * Liste des numeros des repertoires qui contiennent le fichier
     * @param numero le numero du fichier
     * @return liste des numeros des repertoires
     */
    public NSArray listNumeroRepertoireForNumeroCourrier(Integer numero){
    	if(numero!=null)
    		return ged().numerosRepertoiresForNumeroCourrier(numero.intValue());
    	return null;
    }
    
    /**
     * Numero du premier repertoires qui contient le fichier et qui est dans le sous-arbres du repertoire root actuel
     * Attention, utilise une methode recursive, peut etre tr&egrave;s long !!!!
     * @param numero le numero du fichier
     * @return Numeros du premier repertoire contenant le fichier et fils du root actuel
     */
    public String numeroRepertoireForNumeroCourrier(Integer numero){
    	if(numero!=null)
    		return ged().numeroRepertoireForNumeroCourrier(numero.intValue());
    	return null;
    }
    	
}
