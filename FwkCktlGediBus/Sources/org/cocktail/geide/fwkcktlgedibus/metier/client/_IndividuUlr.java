
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to IndividuUlr.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _IndividuUlr extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_IndividuUlr";

    public static final String ENTITY_TABLE_NAME = "GRHUM.INDIVIDU_ULR";

    public static final String NO_INDIVIDU_KEY = "noIndividu";
    public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
    public static final String NOM_USUEL_KEY = "nomUsuel";
    public static final String PERS_ID_KEY = "persId";
    public static final String PRENOM_KEY = "prenom";
    public static final String TEM_VALIDE_KEY = "temValide";

    public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
    public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
    public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
    public static final String PERS_ID_COLKEY = "PERS_ID";
    public static final String PRENOM_COLKEY = "PRENOM";
    public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";





	
    public _IndividuUlr() {
        super();
    }




    public Number noIndividu() {
        return (Number)storedValueForKey(NO_INDIVIDU_KEY);
    }
    public void setNoIndividu(Number aValue) {
        takeStoredValueForKey(aValue, NO_INDIVIDU_KEY);
    }

    public String nomPatronymique() {
        return (String)storedValueForKey(NOM_PATRONYMIQUE_KEY);
    }
    public void setNomPatronymique(String aValue) {
        takeStoredValueForKey(aValue, NOM_PATRONYMIQUE_KEY);
    }

    public String nomUsuel() {
        return (String)storedValueForKey(NOM_USUEL_KEY);
    }
    public void setNomUsuel(String aValue) {
        takeStoredValueForKey(aValue, NOM_USUEL_KEY);
    }

    public Number persId() {
        return (Number)storedValueForKey(PERS_ID_KEY);
    }
    public void setPersId(Number aValue) {
        takeStoredValueForKey(aValue, PERS_ID_KEY);
    }

    public String prenom() {
        return (String)storedValueForKey(PRENOM_KEY);
    }
    public void setPrenom(String aValue) {
        takeStoredValueForKey(aValue, PRENOM_KEY);
    }

    public String temValide() {
        return (String)storedValueForKey(TEM_VALIDE_KEY);
    }
    public void setTemValide(String aValue) {
        takeStoredValueForKey(aValue, TEM_VALIDE_KEY);
    }








}

