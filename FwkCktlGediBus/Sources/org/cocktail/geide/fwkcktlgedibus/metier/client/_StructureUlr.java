
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to StructureUlr.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _StructureUlr extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_StructureUlr";

    public static final String ENTITY_TABLE_NAME = "GRHUM.STRUCTURE_ULR";

    public static final String C_ACADEMIE_KEY = "cAcademie";
    public static final String C_NAF_KEY = "cNaf";
    public static final String C_NIC_KEY = "cNic";
    public static final String C_RNE_KEY = "cRne";
    public static final String C_STATUT_JURIDIQUE_KEY = "cStatutJuridique";
    public static final String C_STRUCTURE_KEY = "cStructure";
    public static final String C_STRUCTURE_PERE_KEY = "cStructurePere";
    public static final String C_TYPE_DECISION_STR_KEY = "cTypeDecisionStr";
    public static final String C_TYPE_ETABLISSEMEN_KEY = "cTypeEtablissemen";
    public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
    public static final String D_CREATION_KEY = "dCreation";
    public static final String D_MODIFICATION_KEY = "dModification";
    public static final String DATE_DECISION_KEY = "dateDecision";
    public static final String DATE_FERMETURE_KEY = "dateFermeture";
    public static final String DATE_OUVERTURE_KEY = "dateOuverture";
    public static final String ETAB_CODURSSAF_KEY = "etabCodurssaf";
    public static final String ETAB_NUMURSSAF_KEY = "etabNumurssaf";
    public static final String EXPORT_KEY = "export";
    public static final String GRP_ACCES_KEY = "grpAcces";
    public static final String GRP_ALIAS_KEY = "grpAlias";
    public static final String GRP_APE_CODE_KEY = "grpApeCode";
    public static final String GRP_APE_CODE_BIS_KEY = "grpApeCodeBis";
    public static final String GRP_APE_CODE_COMP_KEY = "grpApeCodeComp";
    public static final String GRP_CA_KEY = "grpCa";
    public static final String GRP_CAPITAL_KEY = "grpCapital";
    public static final String GRP_CENTRE_DECISION_KEY = "grpCentreDecision";
    public static final String GRP_EFFECTIFS_KEY = "grpEffectifs";
    public static final String GRP_FONCTION1_KEY = "grpFonction1";
    public static final String GRP_FONCTION2_KEY = "grpFonction2";
    public static final String GRP_FORME_JURIDIQUE_KEY = "grpFormeJuridique";
    public static final String GRP_MOTS_CLEFS_KEY = "grpMotsClefs";
    public static final String GRP_OWNER_KEY = "grpOwner";
    public static final String GRP_RESPONSABILITE_KEY = "grpResponsabilite";
    public static final String GRP_RESPONSABLE_KEY = "grpResponsable";
    public static final String GRP_TRADEMARQUE_KEY = "grpTrademarque";
    public static final String GRP_WEBMESTRE_KEY = "grpWebmestre";
    public static final String LC_STRUCTURE_KEY = "lcStructure";
    public static final String LL_STRUCTURE_KEY = "llStructure";
    public static final String PERS_ID_KEY = "persId";
    public static final String TEM_VALIDE_KEY = "temValide";

    public static final String C_ACADEMIE_COLKEY = "C_ACADEMIE";
    public static final String C_NAF_COLKEY = "C_NAF";
    public static final String C_NIC_COLKEY = "C_NIC";
    public static final String C_RNE_COLKEY = "C_RNE";
    public static final String C_STATUT_JURIDIQUE_COLKEY = "C_STATUT_JURIDIQUE";
    public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
    public static final String C_STRUCTURE_PERE_COLKEY = "C_STRUCTURE_PERE";
    public static final String C_TYPE_DECISION_STR_COLKEY = "C_TYPE_DECISION_STR";
    public static final String C_TYPE_ETABLISSEMEN_COLKEY = "C_TYPE_ETABLISSEMEN";
    public static final String C_TYPE_STRUCTURE_COLKEY = "C_TYPE_STRUCTURE";
    public static final String D_CREATION_COLKEY = "D_CREATION";
    public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
    public static final String DATE_DECISION_COLKEY = "DATE_DECISION";
    public static final String DATE_FERMETURE_COLKEY = "DATE_FERMETURE";
    public static final String DATE_OUVERTURE_COLKEY = "DATE_OUVERTURE";
    public static final String ETAB_CODURSSAF_COLKEY = "ETAB_CODURSSAF";
    public static final String ETAB_NUMURSSAF_COLKEY = "ETAB_NUMURSSAF";
    public static final String EXPORT_COLKEY = "EXPORT";
    public static final String GRP_ACCES_COLKEY = "GRP_ACCES";
    public static final String GRP_ALIAS_COLKEY = "GRP_ALIAS";
    public static final String GRP_APE_CODE_COLKEY = "GRP_APE_CODE";
    public static final String GRP_APE_CODE_BIS_COLKEY = "GRP_APE_CODE_BIS";
    public static final String GRP_APE_CODE_COMP_COLKEY = "GRP_APE_CODE_COMP";
    public static final String GRP_CA_COLKEY = "GRP_CA";
    public static final String GRP_CAPITAL_COLKEY = "GRP_CAPITAL";
    public static final String GRP_CENTRE_DECISION_COLKEY = "GRP_CENTRE_DECISION";
    public static final String GRP_EFFECTIFS_COLKEY = "GRP_EFFECTIFS";
    public static final String GRP_FONCTION1_COLKEY = "GRP_FONCTION1";
    public static final String GRP_FONCTION2_COLKEY = "GRP_FONCTION2";
    public static final String GRP_FORME_JURIDIQUE_COLKEY = "GRP_FORME_JURIDIQUE";
    public static final String GRP_MOTS_CLEFS_COLKEY = "GRP_MOTS_CLEFS";
    public static final String GRP_OWNER_COLKEY = "GRP_OWNER";
    public static final String GRP_RESPONSABILITE_COLKEY = "GRP_RESPONSABILITE";
    public static final String GRP_RESPONSABLE_COLKEY = "GRP_RESPONSABLE";
    public static final String GRP_TRADEMARQUE_COLKEY = "GRP_TRADEMARQUE";
    public static final String GRP_WEBMESTRE_COLKEY = "GRP_WEBMESTRE";
    public static final String LC_STRUCTURE_COLKEY = "LC_STRUCTURE";
    public static final String LL_STRUCTURE_COLKEY = "LL_STRUCTURE";
    public static final String PERS_ID_COLKEY = "PERS_ID";
    public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";





	
    public _StructureUlr() {
        super();
    }




    public String cAcademie() {
        return (String)storedValueForKey(C_ACADEMIE_KEY);
    }
    public void setCAcademie(String aValue) {
        takeStoredValueForKey(aValue, C_ACADEMIE_KEY);
    }

    public String cNaf() {
        return (String)storedValueForKey(C_NAF_KEY);
    }
    public void setCNaf(String aValue) {
        takeStoredValueForKey(aValue, C_NAF_KEY);
    }

    public String cNic() {
        return (String)storedValueForKey(C_NIC_KEY);
    }
    public void setCNic(String aValue) {
        takeStoredValueForKey(aValue, C_NIC_KEY);
    }

    public String cRne() {
        return (String)storedValueForKey(C_RNE_KEY);
    }
    public void setCRne(String aValue) {
        takeStoredValueForKey(aValue, C_RNE_KEY);
    }

    public String cStatutJuridique() {
        return (String)storedValueForKey(C_STATUT_JURIDIQUE_KEY);
    }
    public void setCStatutJuridique(String aValue) {
        takeStoredValueForKey(aValue, C_STATUT_JURIDIQUE_KEY);
    }

    public String cStructure() {
        return (String)storedValueForKey(C_STRUCTURE_KEY);
    }
    public void setCStructure(String aValue) {
        takeStoredValueForKey(aValue, C_STRUCTURE_KEY);
    }

    public String cStructurePere() {
        return (String)storedValueForKey(C_STRUCTURE_PERE_KEY);
    }
    public void setCStructurePere(String aValue) {
        takeStoredValueForKey(aValue, C_STRUCTURE_PERE_KEY);
    }

    public String cTypeDecisionStr() {
        return (String)storedValueForKey(C_TYPE_DECISION_STR_KEY);
    }
    public void setCTypeDecisionStr(String aValue) {
        takeStoredValueForKey(aValue, C_TYPE_DECISION_STR_KEY);
    }

    public String cTypeEtablissemen() {
        return (String)storedValueForKey(C_TYPE_ETABLISSEMEN_KEY);
    }
    public void setCTypeEtablissemen(String aValue) {
        takeStoredValueForKey(aValue, C_TYPE_ETABLISSEMEN_KEY);
    }

    public String cTypeStructure() {
        return (String)storedValueForKey(C_TYPE_STRUCTURE_KEY);
    }
    public void setCTypeStructure(String aValue) {
        takeStoredValueForKey(aValue, C_TYPE_STRUCTURE_KEY);
    }

    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey(D_CREATION_KEY);
    }
    public void setDCreation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_CREATION_KEY);
    }

    public NSTimestamp dModification() {
        return (NSTimestamp)storedValueForKey(D_MODIFICATION_KEY);
    }
    public void setDModification(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_MODIFICATION_KEY);
    }

    public NSTimestamp dateDecision() {
        return (NSTimestamp)storedValueForKey(DATE_DECISION_KEY);
    }
    public void setDateDecision(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, DATE_DECISION_KEY);
    }

    public NSTimestamp dateFermeture() {
        return (NSTimestamp)storedValueForKey(DATE_FERMETURE_KEY);
    }
    public void setDateFermeture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, DATE_FERMETURE_KEY);
    }

    public NSTimestamp dateOuverture() {
        return (NSTimestamp)storedValueForKey(DATE_OUVERTURE_KEY);
    }
    public void setDateOuverture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, DATE_OUVERTURE_KEY);
    }

    public String etabCodurssaf() {
        return (String)storedValueForKey(ETAB_CODURSSAF_KEY);
    }
    public void setEtabCodurssaf(String aValue) {
        takeStoredValueForKey(aValue, ETAB_CODURSSAF_KEY);
    }

    public String etabNumurssaf() {
        return (String)storedValueForKey(ETAB_NUMURSSAF_KEY);
    }
    public void setEtabNumurssaf(String aValue) {
        takeStoredValueForKey(aValue, ETAB_NUMURSSAF_KEY);
    }

    public Number export() {
        return (Number)storedValueForKey(EXPORT_KEY);
    }
    public void setExport(Number aValue) {
        takeStoredValueForKey(aValue, EXPORT_KEY);
    }

    public String grpAcces() {
        return (String)storedValueForKey(GRP_ACCES_KEY);
    }
    public void setGrpAcces(String aValue) {
        takeStoredValueForKey(aValue, GRP_ACCES_KEY);
    }

    public String grpAlias() {
        return (String)storedValueForKey(GRP_ALIAS_KEY);
    }
    public void setGrpAlias(String aValue) {
        takeStoredValueForKey(aValue, GRP_ALIAS_KEY);
    }

    public String grpApeCode() {
        return (String)storedValueForKey(GRP_APE_CODE_KEY);
    }
    public void setGrpApeCode(String aValue) {
        takeStoredValueForKey(aValue, GRP_APE_CODE_KEY);
    }

    public String grpApeCodeBis() {
        return (String)storedValueForKey(GRP_APE_CODE_BIS_KEY);
    }
    public void setGrpApeCodeBis(String aValue) {
        takeStoredValueForKey(aValue, GRP_APE_CODE_BIS_KEY);
    }

    public String grpApeCodeComp() {
        return (String)storedValueForKey(GRP_APE_CODE_COMP_KEY);
    }
    public void setGrpApeCodeComp(String aValue) {
        takeStoredValueForKey(aValue, GRP_APE_CODE_COMP_KEY);
    }

    public Number grpCa() {
        return (Number)storedValueForKey(GRP_CA_KEY);
    }
    public void setGrpCa(Number aValue) {
        takeStoredValueForKey(aValue, GRP_CA_KEY);
    }

    public Number grpCapital() {
        return (Number)storedValueForKey(GRP_CAPITAL_KEY);
    }
    public void setGrpCapital(Number aValue) {
        takeStoredValueForKey(aValue, GRP_CAPITAL_KEY);
    }

    public String grpCentreDecision() {
        return (String)storedValueForKey(GRP_CENTRE_DECISION_KEY);
    }
    public void setGrpCentreDecision(String aValue) {
        takeStoredValueForKey(aValue, GRP_CENTRE_DECISION_KEY);
    }

    public Number grpEffectifs() {
        return (Number)storedValueForKey(GRP_EFFECTIFS_KEY);
    }
    public void setGrpEffectifs(Number aValue) {
        takeStoredValueForKey(aValue, GRP_EFFECTIFS_KEY);
    }

    public String grpFonction1() {
        return (String)storedValueForKey(GRP_FONCTION1_KEY);
    }
    public void setGrpFonction1(String aValue) {
        takeStoredValueForKey(aValue, GRP_FONCTION1_KEY);
    }

    public String grpFonction2() {
        return (String)storedValueForKey(GRP_FONCTION2_KEY);
    }
    public void setGrpFonction2(String aValue) {
        takeStoredValueForKey(aValue, GRP_FONCTION2_KEY);
    }

    public String grpFormeJuridique() {
        return (String)storedValueForKey(GRP_FORME_JURIDIQUE_KEY);
    }
    public void setGrpFormeJuridique(String aValue) {
        takeStoredValueForKey(aValue, GRP_FORME_JURIDIQUE_KEY);
    }

    public String grpMotsClefs() {
        return (String)storedValueForKey(GRP_MOTS_CLEFS_KEY);
    }
    public void setGrpMotsClefs(String aValue) {
        takeStoredValueForKey(aValue, GRP_MOTS_CLEFS_KEY);
    }

    public Number grpOwner() {
        return (Number)storedValueForKey(GRP_OWNER_KEY);
    }
    public void setGrpOwner(Number aValue) {
        takeStoredValueForKey(aValue, GRP_OWNER_KEY);
    }

    public String grpResponsabilite() {
        return (String)storedValueForKey(GRP_RESPONSABILITE_KEY);
    }
    public void setGrpResponsabilite(String aValue) {
        takeStoredValueForKey(aValue, GRP_RESPONSABILITE_KEY);
    }

    public Number grpResponsable() {
        return (Number)storedValueForKey(GRP_RESPONSABLE_KEY);
    }
    public void setGrpResponsable(Number aValue) {
        takeStoredValueForKey(aValue, GRP_RESPONSABLE_KEY);
    }

    public String grpTrademarque() {
        return (String)storedValueForKey(GRP_TRADEMARQUE_KEY);
    }
    public void setGrpTrademarque(String aValue) {
        takeStoredValueForKey(aValue, GRP_TRADEMARQUE_KEY);
    }

    public String grpWebmestre() {
        return (String)storedValueForKey(GRP_WEBMESTRE_KEY);
    }
    public void setGrpWebmestre(String aValue) {
        takeStoredValueForKey(aValue, GRP_WEBMESTRE_KEY);
    }

    public String lcStructure() {
        return (String)storedValueForKey(LC_STRUCTURE_KEY);
    }
    public void setLcStructure(String aValue) {
        takeStoredValueForKey(aValue, LC_STRUCTURE_KEY);
    }

    public String llStructure() {
        return (String)storedValueForKey(LL_STRUCTURE_KEY);
    }
    public void setLlStructure(String aValue) {
        takeStoredValueForKey(aValue, LL_STRUCTURE_KEY);
    }

    public Number persId() {
        return (Number)storedValueForKey(PERS_ID_KEY);
    }
    public void setPersId(Number aValue) {
        takeStoredValueForKey(aValue, PERS_ID_KEY);
    }

    public String temValide() {
        return (String)storedValueForKey(TEM_VALIDE_KEY);
    }
    public void setTemValide(String aValue) {
        takeStoredValueForKey(aValue, TEM_VALIDE_KEY);
    }








}

