
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to Courrier.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _Courrier extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_Courrier";

    public static final String ENTITY_TABLE_NAME = "COURRIER";

    public static final String AGT_LOGIN_KEY = "agtLogin";
    public static final String C_STRUCTURE_KEY = "cStructure";
    public static final String COU_COMMENTAIRE_KEY = "couCommentaire";
    public static final String COU_DATE_COURRIER_KEY = "couDateCourrier";
    public static final String COU_DATE_ENREG_KEY = "couDateEnreg";
    public static final String COU_DATE_REPONSE_ATTENDUE_KEY = "couDateReponseAttendue";
    public static final String COU_DATE_REPONSE_EFFECTUEE_KEY = "couDateReponseEffectuee";
    public static final String COU_DEPART_ARRIVEE_KEY = "couDepartArrivee";
    public static final String COU_EXP_ETABLISSEMENT_KEY = "couExpEtablissement";
    public static final String COU_EXP_VIA_KEY = "couExpVia";
    public static final String COU_EXPEDITEUR_KEY = "couExpediteur";
    public static final String COU_IDENT_KEY = "couIdent";
    public static final String COU_LOCALISATION_KEY = "couLocalisation";
    public static final String COU_MOTS_CLEFS_KEY = "couMotsClefs";
    public static final String COU_NOMBRE_RELANCE_KEY = "couNombreRelance";
    public static final String COU_NUMERO_PERE_KEY = "couNumeroPere";
    public static final String COU_OBJET_KEY = "couObjet";
    public static final String COU_REFERENCE_KEY = "couReference";
    public static final String COU_TYPE_CONSULTATION_KEY = "couTypeConsultation";
    public static final String PERS_ID_KEY = "persId";

    public static final String AGT_LOGIN_COLKEY = "AGT_LOGIN";
    public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
    public static final String COU_COMMENTAIRE_COLKEY = "COU_COMMENTAIRE";
    public static final String COU_DATE_COURRIER_COLKEY = "COU_DATE_COURRIER";
    public static final String COU_DATE_ENREG_COLKEY = "COU_DATE_ENREG";
    public static final String COU_DATE_REPONSE_ATTENDUE_COLKEY = "COU_DATE_REPONSE_ATTENDUE";
    public static final String COU_DATE_REPONSE_EFFECTUEE_COLKEY = "COU_DATE_REPONSE_EFFECTUEE";
    public static final String COU_DEPART_ARRIVEE_COLKEY = "COU_DEPART_ARRIVEE";
    public static final String COU_EXP_ETABLISSEMENT_COLKEY = "COU_EXP_ETABLISSEMENT";
    public static final String COU_EXP_VIA_COLKEY = "COU_EXP_VIA";
    public static final String COU_EXPEDITEUR_COLKEY = "COU_EXPEDITEUR";
    public static final String COU_IDENT_COLKEY = "COU_IDENT";
    public static final String COU_LOCALISATION_COLKEY = "COU_LOCALISATION";
    public static final String COU_MOTS_CLEFS_COLKEY = "COU_MOTS_CLEFS";
    public static final String COU_NOMBRE_RELANCE_COLKEY = "COU_NOMBRE_RELANCE";
    public static final String COU_NUMERO_PERE_COLKEY = "COU_NUMERO_PERE";
    public static final String COU_OBJET_COLKEY = "COU_OBJET";
    public static final String COU_REFERENCE_COLKEY = "COU_REFERENCE";
    public static final String COU_TYPE_CONSULTATION_COLKEY = "COU_TYPE_CONSULTATION";
    public static final String PERS_ID_COLKEY = "PERS_ID";


    public static final String G_B__DESTINATAIRES_KEY = "gB_Destinataires";
    public static final String G_B__REPART_COUR_AUTEURS_KEY = "gB_RepartCourAuteurs";
    public static final String REFERENCE_LIENS_KEY = "referenceLiens";
    public static final String SPECIF_WEB_KEY = "specifWeb";
    public static final String TYPES_ASSOCIES_KEY = "typesAssocies";



	
    public _Courrier() {
        super();
    }




    public String agtLogin() {
        return (String)storedValueForKey(AGT_LOGIN_KEY);
    }
    public void setAgtLogin(String aValue) {
        takeStoredValueForKey(aValue, AGT_LOGIN_KEY);
    }

    public String cStructure() {
        return (String)storedValueForKey(C_STRUCTURE_KEY);
    }
    public void setCStructure(String aValue) {
        takeStoredValueForKey(aValue, C_STRUCTURE_KEY);
    }

    public String couCommentaire() {
        return (String)storedValueForKey(COU_COMMENTAIRE_KEY);
    }
    public void setCouCommentaire(String aValue) {
        takeStoredValueForKey(aValue, COU_COMMENTAIRE_KEY);
    }

    public NSTimestamp couDateCourrier() {
        return (NSTimestamp)storedValueForKey(COU_DATE_COURRIER_KEY);
    }
    public void setCouDateCourrier(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, COU_DATE_COURRIER_KEY);
    }

    public NSTimestamp couDateEnreg() {
        return (NSTimestamp)storedValueForKey(COU_DATE_ENREG_KEY);
    }
    public void setCouDateEnreg(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, COU_DATE_ENREG_KEY);
    }

    public NSTimestamp couDateReponseAttendue() {
        return (NSTimestamp)storedValueForKey(COU_DATE_REPONSE_ATTENDUE_KEY);
    }
    public void setCouDateReponseAttendue(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, COU_DATE_REPONSE_ATTENDUE_KEY);
    }

    public NSTimestamp couDateReponseEffectuee() {
        return (NSTimestamp)storedValueForKey(COU_DATE_REPONSE_EFFECTUEE_KEY);
    }
    public void setCouDateReponseEffectuee(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, COU_DATE_REPONSE_EFFECTUEE_KEY);
    }

    public String couDepartArrivee() {
        return (String)storedValueForKey(COU_DEPART_ARRIVEE_KEY);
    }
    public void setCouDepartArrivee(String aValue) {
        takeStoredValueForKey(aValue, COU_DEPART_ARRIVEE_KEY);
    }

    public String couExpEtablissement() {
        return (String)storedValueForKey(COU_EXP_ETABLISSEMENT_KEY);
    }
    public void setCouExpEtablissement(String aValue) {
        takeStoredValueForKey(aValue, COU_EXP_ETABLISSEMENT_KEY);
    }

    public String couExpVia() {
        return (String)storedValueForKey(COU_EXP_VIA_KEY);
    }
    public void setCouExpVia(String aValue) {
        takeStoredValueForKey(aValue, COU_EXP_VIA_KEY);
    }

    public String couExpediteur() {
        return (String)storedValueForKey(COU_EXPEDITEUR_KEY);
    }
    public void setCouExpediteur(String aValue) {
        takeStoredValueForKey(aValue, COU_EXPEDITEUR_KEY);
    }

    public Number couIdent() {
        return (Number)storedValueForKey(COU_IDENT_KEY);
    }
    public void setCouIdent(Number aValue) {
        takeStoredValueForKey(aValue, COU_IDENT_KEY);
    }

    public String couLocalisation() {
        return (String)storedValueForKey(COU_LOCALISATION_KEY);
    }
    public void setCouLocalisation(String aValue) {
        takeStoredValueForKey(aValue, COU_LOCALISATION_KEY);
    }

    public String couMotsClefs() {
        return (String)storedValueForKey(COU_MOTS_CLEFS_KEY);
    }
    public void setCouMotsClefs(String aValue) {
        takeStoredValueForKey(aValue, COU_MOTS_CLEFS_KEY);
    }

    public Number couNombreRelance() {
        return (Number)storedValueForKey(COU_NOMBRE_RELANCE_KEY);
    }
    public void setCouNombreRelance(Number aValue) {
        takeStoredValueForKey(aValue, COU_NOMBRE_RELANCE_KEY);
    }

    public Number couNumeroPere() {
        return (Number)storedValueForKey(COU_NUMERO_PERE_KEY);
    }
    public void setCouNumeroPere(Number aValue) {
        takeStoredValueForKey(aValue, COU_NUMERO_PERE_KEY);
    }

    public String couObjet() {
        return (String)storedValueForKey(COU_OBJET_KEY);
    }
    public void setCouObjet(String aValue) {
        takeStoredValueForKey(aValue, COU_OBJET_KEY);
    }

    public String couReference() {
        return (String)storedValueForKey(COU_REFERENCE_KEY);
    }
    public void setCouReference(String aValue) {
        takeStoredValueForKey(aValue, COU_REFERENCE_KEY);
    }

    public Number couTypeConsultation() {
        return (Number)storedValueForKey(COU_TYPE_CONSULTATION_KEY);
    }
    public void setCouTypeConsultation(Number aValue) {
        takeStoredValueForKey(aValue, COU_TYPE_CONSULTATION_KEY);
    }

    public Number persId() {
        return (Number)storedValueForKey(PERS_ID_KEY);
    }
    public void setPersId(Number aValue) {
        takeStoredValueForKey(aValue, PERS_ID_KEY);
    }







    public NSArray gB_Destinataires() {
        return (NSArray)storedValueForKey(G_B__DESTINATAIRES_KEY);
    }
    public void setGB_Destinataires(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, G_B__DESTINATAIRES_KEY);
    }
    public void addToGB_Destinataires(org.cocktail.geide.fwkcktlgedibus.metier.client.Destinataire object) {
        NSMutableArray array = (NSMutableArray)gB_Destinataires();
        willChange();
        array.addObject(object);
    }
    public void removeFromGB_Destinataires(org.cocktail.geide.fwkcktlgedibus.metier.client.Destinataire object) {
        NSMutableArray array = (NSMutableArray)gB_Destinataires();
        willChange();
        array.removeObject(object);
    }
	
    public void addToGB_DestinatairesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.Destinataire object) {
        addObjectToBothSidesOfRelationshipWithKey(object, G_B__DESTINATAIRES_KEY);
    }
    public void removeFromGB_DestinatairesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.Destinataire object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, G_B__DESTINATAIRES_KEY);
    }
	

    public NSArray gB_RepartCourAuteurs() {
        return (NSArray)storedValueForKey(G_B__REPART_COUR_AUTEURS_KEY);
    }
    public void setGB_RepartCourAuteurs(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, G_B__REPART_COUR_AUTEURS_KEY);
    }
    public void addToGB_RepartCourAuteurs(org.cocktail.geide.fwkcktlgedibus.metier.client.RepartCourAuteur object) {
        NSMutableArray array = (NSMutableArray)gB_RepartCourAuteurs();
        willChange();
        array.addObject(object);
    }
    public void removeFromGB_RepartCourAuteurs(org.cocktail.geide.fwkcktlgedibus.metier.client.RepartCourAuteur object) {
        NSMutableArray array = (NSMutableArray)gB_RepartCourAuteurs();
        willChange();
        array.removeObject(object);
    }
	
    public void addToGB_RepartCourAuteursRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.RepartCourAuteur object) {
        addObjectToBothSidesOfRelationshipWithKey(object, G_B__REPART_COUR_AUTEURS_KEY);
    }
    public void removeFromGB_RepartCourAuteursRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.RepartCourAuteur object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, G_B__REPART_COUR_AUTEURS_KEY);
    }
	

    public NSArray referenceLiens() {
        return (NSArray)storedValueForKey(REFERENCE_LIENS_KEY);
    }
    public void setReferenceLiens(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, REFERENCE_LIENS_KEY);
    }
    public void addToReferenceLiens(org.cocktail.geide.fwkcktlgedibus.metier.client.ReferenceLien object) {
        NSMutableArray array = (NSMutableArray)referenceLiens();
        willChange();
        array.addObject(object);
    }
    public void removeFromReferenceLiens(org.cocktail.geide.fwkcktlgedibus.metier.client.ReferenceLien object) {
        NSMutableArray array = (NSMutableArray)referenceLiens();
        willChange();
        array.removeObject(object);
    }
	
    public void addToReferenceLiensRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.ReferenceLien object) {
        addObjectToBothSidesOfRelationshipWithKey(object, REFERENCE_LIENS_KEY);
    }
    public void removeFromReferenceLiensRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.ReferenceLien object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, REFERENCE_LIENS_KEY);
    }
	

    public NSArray specifWeb() {
        return (NSArray)storedValueForKey(SPECIF_WEB_KEY);
    }
    public void setSpecifWeb(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, SPECIF_WEB_KEY);
    }
    public void addToSpecifWeb(org.cocktail.geide.fwkcktlgedibus.metier.client.SpecifWeb object) {
        NSMutableArray array = (NSMutableArray)specifWeb();
        willChange();
        array.addObject(object);
    }
    public void removeFromSpecifWeb(org.cocktail.geide.fwkcktlgedibus.metier.client.SpecifWeb object) {
        NSMutableArray array = (NSMutableArray)specifWeb();
        willChange();
        array.removeObject(object);
    }
	
    public void addToSpecifWebRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.SpecifWeb object) {
        addObjectToBothSidesOfRelationshipWithKey(object, SPECIF_WEB_KEY);
    }
    public void removeFromSpecifWebRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.SpecifWeb object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, SPECIF_WEB_KEY);
    }
	

    public NSArray typesAssocies() {
        return (NSArray)storedValueForKey(TYPES_ASSOCIES_KEY);
    }
    public void setTypesAssocies(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, TYPES_ASSOCIES_KEY);
    }
    public void addToTypesAssocies(org.cocktail.geide.fwkcktlgedibus.metier.client.TypesAssocies object) {
        NSMutableArray array = (NSMutableArray)typesAssocies();
        willChange();
        array.addObject(object);
    }
    public void removeFromTypesAssocies(org.cocktail.geide.fwkcktlgedibus.metier.client.TypesAssocies object) {
        NSMutableArray array = (NSMutableArray)typesAssocies();
        willChange();
        array.removeObject(object);
    }
	
    public void addToTypesAssociesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.TypesAssocies object) {
        addObjectToBothSidesOfRelationshipWithKey(object, TYPES_ASSOCIES_KEY);
    }
    public void removeFromTypesAssociesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.TypesAssocies object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, TYPES_ASSOCIES_KEY);
    }
	


}

