
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to ReferenceLien.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _ReferenceLien extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_ReferenceLien";

    public static final String ENTITY_TABLE_NAME = "REFERENCE_LIEN";

    public static final String COU_NUMERO_KEY = "couNumero";
    public static final String RFL_DOCUMENT_ID_KEY = "rflDocumentId";
    public static final String RFL_LIBELLE_KEY = "rflLibelle";
    public static final String RFL_LIEN_KEY = "rflLien";
    public static final String RFL_TYPE_KEY = "rflType";

    public static final String COU_NUMERO_COLKEY = "COU_NUMERO";
    public static final String RFL_DOCUMENT_ID_COLKEY = "RFL_DOCUMENT_ID";
    public static final String RFL_LIBELLE_COLKEY = "RFL_LIBELLE";
    public static final String RFL_LIEN_COLKEY = "RFL_LIEN";
    public static final String RFL_TYPE_COLKEY = "RFL_TYPE";





	
    public _ReferenceLien() {
        super();
    }




    public Number couNumero() {
        return (Number)storedValueForKey(COU_NUMERO_KEY);
    }
    public void setCouNumero(Number aValue) {
        takeStoredValueForKey(aValue, COU_NUMERO_KEY);
    }

    public Number rflDocumentId() {
        return (Number)storedValueForKey(RFL_DOCUMENT_ID_KEY);
    }
    public void setRflDocumentId(Number aValue) {
        takeStoredValueForKey(aValue, RFL_DOCUMENT_ID_KEY);
    }

    public String rflLibelle() {
        return (String)storedValueForKey(RFL_LIBELLE_KEY);
    }
    public void setRflLibelle(String aValue) {
        takeStoredValueForKey(aValue, RFL_LIBELLE_KEY);
    }

    public String rflLien() {
        return (String)storedValueForKey(RFL_LIEN_KEY);
    }
    public void setRflLien(String aValue) {
        takeStoredValueForKey(aValue, RFL_LIEN_KEY);
    }

    public String rflType() {
        return (String)storedValueForKey(RFL_TYPE_KEY);
    }
    public void setRflType(String aValue) {
        takeStoredValueForKey(aValue, RFL_TYPE_KEY);
    }








}

