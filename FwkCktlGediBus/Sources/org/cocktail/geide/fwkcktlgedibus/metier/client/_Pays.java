
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to Pays.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _Pays extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_Pays";

    public static final String ENTITY_TABLE_NAME = "GRHUM.PAYS";

    public static final String CODE_ISO_KEY = "codeIso";
    public static final String D_CREATION_KEY = "dCreation";
    public static final String D_DEB_VAL_KEY = "dDebVal";
    public static final String D_FIN_VAL_KEY = "dFinVal";
    public static final String D_MODIFICATION_KEY = "dModification";
    public static final String L_NATIONALITE_KEY = "lNationalite";
    public static final String LC_PAYS_KEY = "lcPays";
    public static final String LL_PAYS_KEY = "llPays";

    public static final String CODE_ISO_COLKEY = "CODE_ISO";
    public static final String D_CREATION_COLKEY = "D_CREATION";
    public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
    public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
    public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
    public static final String L_NATIONALITE_COLKEY = "L_NATIONALITE";
    public static final String LC_PAYS_COLKEY = "LC_PAYS";
    public static final String LL_PAYS_COLKEY = "LL_PAYS";


    public static final String LANGUES_KEY = "langues";
    public static final String P5_PAYS_DRAPEAUXS_KEY = "p5PaysDrapeauxs";



	
    public _Pays() {
        super();
    }




    public String codeIso() {
        return (String)storedValueForKey(CODE_ISO_KEY);
    }
    public void setCodeIso(String aValue) {
        takeStoredValueForKey(aValue, CODE_ISO_KEY);
    }

    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey(D_CREATION_KEY);
    }
    public void setDCreation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_CREATION_KEY);
    }

    public NSTimestamp dDebVal() {
        return (NSTimestamp)storedValueForKey(D_DEB_VAL_KEY);
    }
    public void setDDebVal(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_DEB_VAL_KEY);
    }

    public NSTimestamp dFinVal() {
        return (NSTimestamp)storedValueForKey(D_FIN_VAL_KEY);
    }
    public void setDFinVal(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_FIN_VAL_KEY);
    }

    public NSTimestamp dModification() {
        return (NSTimestamp)storedValueForKey(D_MODIFICATION_KEY);
    }
    public void setDModification(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_MODIFICATION_KEY);
    }

    public String lNationalite() {
        return (String)storedValueForKey(L_NATIONALITE_KEY);
    }
    public void setLNationalite(String aValue) {
        takeStoredValueForKey(aValue, L_NATIONALITE_KEY);
    }

    public String lcPays() {
        return (String)storedValueForKey(LC_PAYS_KEY);
    }
    public void setLcPays(String aValue) {
        takeStoredValueForKey(aValue, LC_PAYS_KEY);
    }

    public String llPays() {
        return (String)storedValueForKey(LL_PAYS_KEY);
    }
    public void setLlPays(String aValue) {
        takeStoredValueForKey(aValue, LL_PAYS_KEY);
    }







    public NSArray langues() {
        return (NSArray)storedValueForKey(LANGUES_KEY);
    }
    public void setLangues(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, LANGUES_KEY);
    }
    public void addToLangues(org.cocktail.geide.fwkcktlgedibus.metier.client.Langue object) {
        NSMutableArray array = (NSMutableArray)langues();
        willChange();
        array.addObject(object);
    }
    public void removeFromLangues(org.cocktail.geide.fwkcktlgedibus.metier.client.Langue object) {
        NSMutableArray array = (NSMutableArray)langues();
        willChange();
        array.removeObject(object);
    }
	
    public void addToLanguesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.Langue object) {
        addObjectToBothSidesOfRelationshipWithKey(object, LANGUES_KEY);
    }
    public void removeFromLanguesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.Langue object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, LANGUES_KEY);
    }
	

    public NSArray p5PaysDrapeauxs() {
        return (NSArray)storedValueForKey(P5_PAYS_DRAPEAUXS_KEY);
    }
    public void setP5PaysDrapeauxs(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, P5_PAYS_DRAPEAUXS_KEY);
    }
    public void addToP5PaysDrapeauxs(org.cocktail.geide.fwkcktlgedibus.metier.client.PaysDrapeaux object) {
        NSMutableArray array = (NSMutableArray)p5PaysDrapeauxs();
        willChange();
        array.addObject(object);
    }
    public void removeFromP5PaysDrapeauxs(org.cocktail.geide.fwkcktlgedibus.metier.client.PaysDrapeaux object) {
        NSMutableArray array = (NSMutableArray)p5PaysDrapeauxs();
        willChange();
        array.removeObject(object);
    }
	
    public void addToP5PaysDrapeauxsRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.PaysDrapeaux object) {
        addObjectToBothSidesOfRelationshipWithKey(object, P5_PAYS_DRAPEAUXS_KEY);
    }
    public void removeFromP5PaysDrapeauxsRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.PaysDrapeaux object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, P5_PAYS_DRAPEAUXS_KEY);
    }
	


}

