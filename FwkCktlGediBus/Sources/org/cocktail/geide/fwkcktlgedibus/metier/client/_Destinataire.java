
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to Destinataire.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _Destinataire extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_Destinataire";

    public static final String ENTITY_TABLE_NAME = "DESTINATAIRE";

    public static final String DEST_DATE_KEY = "destDate";
    public static final String DEST_DROIT_MODIF_KEY = "destDroitModif";
    public static final String DEST_LU_KEY = "destLu";
    public static final String DEST_MAIL_KEY = "destMail";
    public static final String DEST_RAISON_KEY = "destRaison";
    public static final String DEST_TYPE_KEY = "destType";
    public static final String PERS_ID_KEY = "persId";

    public static final String DEST_DATE_COLKEY = "DEST_DATE";
    public static final String DEST_DROIT_MODIF_COLKEY = "DEST_DROIT_MODIF";
    public static final String DEST_LU_COLKEY = "DEST_LU";
    public static final String DEST_MAIL_COLKEY = "DEST_MAIL";
    public static final String DEST_RAISON_COLKEY = "DEST_RAISON";
    public static final String DEST_TYPE_COLKEY = "DEST_TYPE";
    public static final String PERS_ID_COLKEY = "PERS_ID";





	
    public _Destinataire() {
        super();
    }




    public NSTimestamp destDate() {
        return (NSTimestamp)storedValueForKey(DEST_DATE_KEY);
    }
    public void setDestDate(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, DEST_DATE_KEY);
    }

    public String destDroitModif() {
        return (String)storedValueForKey(DEST_DROIT_MODIF_KEY);
    }
    public void setDestDroitModif(String aValue) {
        takeStoredValueForKey(aValue, DEST_DROIT_MODIF_KEY);
    }

    public String destLu() {
        return (String)storedValueForKey(DEST_LU_KEY);
    }
    public void setDestLu(String aValue) {
        takeStoredValueForKey(aValue, DEST_LU_KEY);
    }

    public String destMail() {
        return (String)storedValueForKey(DEST_MAIL_KEY);
    }
    public void setDestMail(String aValue) {
        takeStoredValueForKey(aValue, DEST_MAIL_KEY);
    }

    public String destRaison() {
        return (String)storedValueForKey(DEST_RAISON_KEY);
    }
    public void setDestRaison(String aValue) {
        takeStoredValueForKey(aValue, DEST_RAISON_KEY);
    }

    public String destType() {
        return (String)storedValueForKey(DEST_TYPE_KEY);
    }
    public void setDestType(String aValue) {
        takeStoredValueForKey(aValue, DEST_TYPE_KEY);
    }

    public Number persId() {
        return (Number)storedValueForKey(PERS_ID_KEY);
    }
    public void setPersId(Number aValue) {
        takeStoredValueForKey(aValue, PERS_ID_KEY);
    }








}

