
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to DroitTypeClient.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _DroitTypeClient extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_DroitTypeClient";

    public static final String ENTITY_TABLE_NAME = "DROIT_TYPE_CLIENT";

    public static final String AGT_TYPE_KEY = "agtType";
    public static final String CLIENT_TYPE_KEY = "clientType";
    public static final String D_LEC_KEY = "dLec";
    public static final String D_MOD_KEY = "dMod";
    public static final String D_SUP_KEY = "dSup";
    public static final String PERS_ID_KEY = "persId";

    public static final String AGT_TYPE_COLKEY = "AGT_TYPE";
    public static final String CLIENT_TYPE_COLKEY = "CLIENT_TYPE";
    public static final String D_LEC_COLKEY = "D_LEC";
    public static final String D_MOD_COLKEY = "D_MOD";
    public static final String D_SUP_COLKEY = "D_SUP";
    public static final String PERS_ID_COLKEY = "PERS_ID";





	
    public _DroitTypeClient() {
        super();
    }




    public String agtType() {
        return (String)storedValueForKey(AGT_TYPE_KEY);
    }
    public void setAgtType(String aValue) {
        takeStoredValueForKey(aValue, AGT_TYPE_KEY);
    }

    public String clientType() {
        return (String)storedValueForKey(CLIENT_TYPE_KEY);
    }
    public void setClientType(String aValue) {
        takeStoredValueForKey(aValue, CLIENT_TYPE_KEY);
    }

    public String dLec() {
        return (String)storedValueForKey(D_LEC_KEY);
    }
    public void setDLec(String aValue) {
        takeStoredValueForKey(aValue, D_LEC_KEY);
    }

    public String dMod() {
        return (String)storedValueForKey(D_MOD_KEY);
    }
    public void setDMod(String aValue) {
        takeStoredValueForKey(aValue, D_MOD_KEY);
    }

    public String dSup() {
        return (String)storedValueForKey(D_SUP_KEY);
    }
    public void setDSup(String aValue) {
        takeStoredValueForKey(aValue, D_SUP_KEY);
    }

    public Number persId() {
        return (Number)storedValueForKey(PERS_ID_KEY);
    }
    public void setPersId(Number aValue) {
        takeStoredValueForKey(aValue, PERS_ID_KEY);
    }








}

