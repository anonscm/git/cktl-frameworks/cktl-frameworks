
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to TypeCourrierNew.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _TypeCourrierNew extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_TypeCourrierNew";

    public static final String ENTITY_TABLE_NAME = "TYPE_COURRIER_NEW";

    public static final String CLE_DATES_KEY = "cleDates";
    public static final String REF_ORDRE_KEY = "refOrdre";
    public static final String TYPE_CODE_KEY = "typeCode";
    public static final String TYPE_DOCUMENT_ID_KEY = "typeDocumentId";
    public static final String TYPE_FAMILLE_KEY = "typeFamille";
    public static final String TYPE_LIBELLE_KEY = "typeLibelle";
    public static final String TYPE_ORDRE_CHAR_KEY = "typeOrdreChar";
    public static final String TYPE_PAGE_TITRE_KEY = "typePageTitre";
    public static final String TYPE_PAGE_URL_KEY = "typePageUrl";
    public static final String TYPE_TYPE_CONSULTATION_KEY = "typeTypeConsultation";
    public static final String TYPE_VISIBLE_APPLI_KEY = "typeVisibleAppli";

    public static final String CLE_DATES_COLKEY = "CLE_DATES";
    public static final String REF_ORDRE_COLKEY = "REF_ORDRE";
    public static final String TYPE_CODE_COLKEY = "TYPE_CODE";
    public static final String TYPE_DOCUMENT_ID_COLKEY = "TYPE_DOCUMENT_ID";
    public static final String TYPE_FAMILLE_COLKEY = "TYPE_FAMILLE";
    public static final String TYPE_LIBELLE_COLKEY = "TYPE_LIBELLE";
    public static final String TYPE_ORDRE_CHAR_COLKEY = "TYPE_ORDRE_CHAR";
    public static final String TYPE_PAGE_TITRE_COLKEY = "TYPE_PAGE_TITRE";
    public static final String TYPE_PAGE_URL_COLKEY = "TYPE_PAGE_URL";
    public static final String TYPE_TYPE_CONSULTATION_COLKEY = "TYPE_TYPE_CONSULTATION";
    public static final String TYPE_VISIBLE_APPLI_COLKEY = "TYPE_VISIBLE_APPLI";

    public static final String TYPE_LOGO_KEY = "typeLogo";




	
    public _TypeCourrierNew() {
        super();
    }




    public Number cleDates() {
        return (Number)storedValueForKey(CLE_DATES_KEY);
    }
    public void setCleDates(Number aValue) {
        takeStoredValueForKey(aValue, CLE_DATES_KEY);
    }

    public Number refOrdre() {
        return (Number)storedValueForKey(REF_ORDRE_KEY);
    }
    public void setRefOrdre(Number aValue) {
        takeStoredValueForKey(aValue, REF_ORDRE_KEY);
    }

    public String typeCode() {
        return (String)storedValueForKey(TYPE_CODE_KEY);
    }
    public void setTypeCode(String aValue) {
        takeStoredValueForKey(aValue, TYPE_CODE_KEY);
    }

    public Number typeDocumentId() {
        return (Number)storedValueForKey(TYPE_DOCUMENT_ID_KEY);
    }
    public void setTypeDocumentId(Number aValue) {
        takeStoredValueForKey(aValue, TYPE_DOCUMENT_ID_KEY);
    }

    public String typeFamille() {
        return (String)storedValueForKey(TYPE_FAMILLE_KEY);
    }
    public void setTypeFamille(String aValue) {
        takeStoredValueForKey(aValue, TYPE_FAMILLE_KEY);
    }

    public String typeLibelle() {
        return (String)storedValueForKey(TYPE_LIBELLE_KEY);
    }
    public void setTypeLibelle(String aValue) {
        takeStoredValueForKey(aValue, TYPE_LIBELLE_KEY);
    }

    public String typeOrdreChar() {
        return (String)storedValueForKey(TYPE_ORDRE_CHAR_KEY);
    }
    public void setTypeOrdreChar(String aValue) {
        takeStoredValueForKey(aValue, TYPE_ORDRE_CHAR_KEY);
    }

    public String typePageTitre() {
        return (String)storedValueForKey(TYPE_PAGE_TITRE_KEY);
    }
    public void setTypePageTitre(String aValue) {
        takeStoredValueForKey(aValue, TYPE_PAGE_TITRE_KEY);
    }

    public String typePageUrl() {
        return (String)storedValueForKey(TYPE_PAGE_URL_KEY);
    }
    public void setTypePageUrl(String aValue) {
        takeStoredValueForKey(aValue, TYPE_PAGE_URL_KEY);
    }

    public Number typeTypeConsultation() {
        return (Number)storedValueForKey(TYPE_TYPE_CONSULTATION_KEY);
    }
    public void setTypeTypeConsultation(Number aValue) {
        takeStoredValueForKey(aValue, TYPE_TYPE_CONSULTATION_KEY);
    }

    public String typeVisibleAppli() {
        return (String)storedValueForKey(TYPE_VISIBLE_APPLI_KEY);
    }
    public void setTypeVisibleAppli(String aValue) {
        takeStoredValueForKey(aValue, TYPE_VISIBLE_APPLI_KEY);
    }




    public org.cocktail.geide.fwkcktlgedibus.metier.client.TypeLogo typeLogo() {
        return (org.cocktail.geide.fwkcktlgedibus.metier.client.TypeLogo)storedValueForKey(TYPE_LOGO_KEY);
    }
    public void setTypeLogo(org.cocktail.geide.fwkcktlgedibus.metier.client.TypeLogo aValue) {
        takeStoredValueForKey(aValue, TYPE_LOGO_KEY);
    }
	
    public void setTypeLogoRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.TypeLogo value) {
        if (value == null) {
            org.cocktail.geide.fwkcktlgedibus.metier.client.TypeLogo object = typeLogo();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_LOGO_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_LOGO_KEY);
        }
    }





}

