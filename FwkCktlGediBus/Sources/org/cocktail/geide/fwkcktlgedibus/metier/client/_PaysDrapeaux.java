
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to PaysDrapeaux.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _PaysDrapeaux extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_PaysDrapeaux";

    public static final String ENTITY_TABLE_NAME = "GRHUM.P5_PAYS_DRAPEAUX";

    public static final String D_CREATION_KEY = "dCreation";
    public static final String D_MODIFICATION_KEY = "dModification";
    public static final String FILETYPE_KEY = "filetype";
    public static final String LIEN_FICHIER_KEY = "lienFichier";
    public static final String LIEN_REPERTOIRE_KEY = "lienRepertoire";
    public static final String LIEN_SERVEUR_KEY = "lienServeur";

    public static final String D_CREATION_COLKEY = "D_CREATION";
    public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
    public static final String FILETYPE_COLKEY = "FILETYPE";
    public static final String LIEN_FICHIER_COLKEY = "LIEN_FICHIER";
    public static final String LIEN_REPERTOIRE_COLKEY = "LIEN_REPERTOIRE";
    public static final String LIEN_SERVEUR_COLKEY = "LIEN_SERVEUR";

    public static final String PAYS_KEY = "pays";




	
    public _PaysDrapeaux() {
        super();
    }




    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey(D_CREATION_KEY);
    }
    public void setDCreation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_CREATION_KEY);
    }

    public NSTimestamp dModification() {
        return (NSTimestamp)storedValueForKey(D_MODIFICATION_KEY);
    }
    public void setDModification(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_MODIFICATION_KEY);
    }

    public String filetype() {
        return (String)storedValueForKey(FILETYPE_KEY);
    }
    public void setFiletype(String aValue) {
        takeStoredValueForKey(aValue, FILETYPE_KEY);
    }

    public String lienFichier() {
        return (String)storedValueForKey(LIEN_FICHIER_KEY);
    }
    public void setLienFichier(String aValue) {
        takeStoredValueForKey(aValue, LIEN_FICHIER_KEY);
    }

    public String lienRepertoire() {
        return (String)storedValueForKey(LIEN_REPERTOIRE_KEY);
    }
    public void setLienRepertoire(String aValue) {
        takeStoredValueForKey(aValue, LIEN_REPERTOIRE_KEY);
    }

    public String lienServeur() {
        return (String)storedValueForKey(LIEN_SERVEUR_KEY);
    }
    public void setLienServeur(String aValue) {
        takeStoredValueForKey(aValue, LIEN_SERVEUR_KEY);
    }




    public org.cocktail.geide.fwkcktlgedibus.metier.client.Pays pays() {
        return (org.cocktail.geide.fwkcktlgedibus.metier.client.Pays)storedValueForKey(PAYS_KEY);
    }
    public void setPays(org.cocktail.geide.fwkcktlgedibus.metier.client.Pays aValue) {
        takeStoredValueForKey(aValue, PAYS_KEY);
    }
	
    public void setPaysRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.Pays value) {
        if (value == null) {
            org.cocktail.geide.fwkcktlgedibus.metier.client.Pays object = pays();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PAYS_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PAYS_KEY);
        }
    }





}

