
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to TypesAssocies.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public abstract class _TypesAssocies extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_TypesAssocies";

    public static final String ENTITY_TABLE_NAME = "TYPES_ASSOCIES";

    public static final String COU_NUMERO_KEY = "couNumero";
    public static final String TYPE_RECORD_KEY = "typeRecord";

    public static final String COU_NUMERO_COLKEY = "COU_NUMERO";
    public static final String TYPE_RECORD_COLKEY = "TYPE_RECORD";

    public static final String COURRIER_KEY = "courrier";

    public static final String TYPE_COURRIER_NEW_KEY = "typeCourrierNew";



	
    public _TypesAssocies() {
        super();
    }




    public Number couNumero() {
        return (Number)storedValueForKey(COU_NUMERO_KEY);
    }
    public void setCouNumero(Number aValue) {
        takeStoredValueForKey(aValue, COU_NUMERO_KEY);
    }

    public String typeRecord() {
        return (String)storedValueForKey(TYPE_RECORD_KEY);
    }
    public void setTypeRecord(String aValue) {
        takeStoredValueForKey(aValue, TYPE_RECORD_KEY);
    }




    public org.cocktail.geide.fwkcktlgedibus.metier.client.Courrier courrier() {
        return (org.cocktail.geide.fwkcktlgedibus.metier.client.Courrier)storedValueForKey(COURRIER_KEY);
    }
    public void setCourrier(org.cocktail.geide.fwkcktlgedibus.metier.client.Courrier aValue) {
        takeStoredValueForKey(aValue, COURRIER_KEY);
    }
	
    public void setCourrierRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.Courrier value) {
        if (value == null) {
            org.cocktail.geide.fwkcktlgedibus.metier.client.Courrier object = courrier();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, COURRIER_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, COURRIER_KEY);
        }
    }




    public NSArray typeCourrierNew() {
        return (NSArray)storedValueForKey(TYPE_COURRIER_NEW_KEY);
    }
    public void setTypeCourrierNew(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, TYPE_COURRIER_NEW_KEY);
    }
    public void addToTypeCourrierNew(org.cocktail.geide.fwkcktlgedibus.metier.client.TypeCourrierNew object) {
        NSMutableArray array = (NSMutableArray)typeCourrierNew();
        willChange();
        array.addObject(object);
    }
    public void removeFromTypeCourrierNew(org.cocktail.geide.fwkcktlgedibus.metier.client.TypeCourrierNew object) {
        NSMutableArray array = (NSMutableArray)typeCourrierNew();
        willChange();
        array.removeObject(object);
    }
	
    public void addToTypeCourrierNewRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.TypeCourrierNew object) {
        addObjectToBothSidesOfRelationshipWithKey(object, TYPE_COURRIER_NEW_KEY);
    }
    public void removeFromTypeCourrierNewRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.TypeCourrierNew object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_COURRIER_NEW_KEY);
    }
	


}

