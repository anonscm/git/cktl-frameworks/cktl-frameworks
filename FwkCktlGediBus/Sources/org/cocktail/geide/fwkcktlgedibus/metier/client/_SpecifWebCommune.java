
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to SpecifWebCommune.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _SpecifWebCommune extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_SpecifWebCommune";

    public static final String ENTITY_TABLE_NAME = "SPECIF_WEB_COMMUNE";

    public static final String ACTION_KEY = "action";
    public static final String NO_INDIVIDU_KEY = "noIndividu";
    public static final String SPEC_CLE_KEY = "specCle";
    public static final String SPEC_VALEUR_KEY = "specValeur";

    public static final String ACTION_COLKEY = "ACTION";
    public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
    public static final String SPEC_CLE_COLKEY = "SPEC_CLE";
    public static final String SPEC_VALEUR_COLKEY = "SPEC_VALEUR";





	
    public _SpecifWebCommune() {
        super();
    }




    public String action() {
        return (String)storedValueForKey(ACTION_KEY);
    }
    public void setAction(String aValue) {
        takeStoredValueForKey(aValue, ACTION_KEY);
    }

    public Number noIndividu() {
        return (Number)storedValueForKey(NO_INDIVIDU_KEY);
    }
    public void setNoIndividu(Number aValue) {
        takeStoredValueForKey(aValue, NO_INDIVIDU_KEY);
    }

    public String specCle() {
        return (String)storedValueForKey(SPEC_CLE_KEY);
    }
    public void setSpecCle(String aValue) {
        takeStoredValueForKey(aValue, SPEC_CLE_KEY);
    }

    public String specValeur() {
        return (String)storedValueForKey(SPEC_VALEUR_KEY);
    }
    public void setSpecValeur(String aValue) {
        takeStoredValueForKey(aValue, SPEC_VALEUR_KEY);
    }








}

