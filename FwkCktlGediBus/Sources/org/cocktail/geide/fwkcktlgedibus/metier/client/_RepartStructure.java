
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to RepartStructure.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _RepartStructure extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_RepartStructure";

    public static final String ENTITY_TABLE_NAME = "GRHUM.REPART_STRUCTURE";

    public static final String C_STRUCTURE_KEY = "cStructure";
    public static final String D_CREATION_KEY = "dCreation";
    public static final String D_MODIFICATION_KEY = "dModification";
    public static final String PERS_ID_KEY = "persId";

    public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
    public static final String D_CREATION_COLKEY = "D_CREATION";
    public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
    public static final String PERS_ID_COLKEY = "PERS_ID";

    public static final String STRUCTURE_ULR_KEY = "structureUlr";




	
    public _RepartStructure() {
        super();
    }




    public String cStructure() {
        return (String)storedValueForKey(C_STRUCTURE_KEY);
    }
    public void setCStructure(String aValue) {
        takeStoredValueForKey(aValue, C_STRUCTURE_KEY);
    }

    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey(D_CREATION_KEY);
    }
    public void setDCreation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_CREATION_KEY);
    }

    public NSTimestamp dModification() {
        return (NSTimestamp)storedValueForKey(D_MODIFICATION_KEY);
    }
    public void setDModification(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_MODIFICATION_KEY);
    }

    public Number persId() {
        return (Number)storedValueForKey(PERS_ID_KEY);
    }
    public void setPersId(Number aValue) {
        takeStoredValueForKey(aValue, PERS_ID_KEY);
    }




    public org.cocktail.geide.fwkcktlgedibus.metier.client.StructureUlr structureUlr() {
        return (org.cocktail.geide.fwkcktlgedibus.metier.client.StructureUlr)storedValueForKey(STRUCTURE_ULR_KEY);
    }
    public void setStructureUlr(org.cocktail.geide.fwkcktlgedibus.metier.client.StructureUlr aValue) {
        takeStoredValueForKey(aValue, STRUCTURE_ULR_KEY);
    }
	
    public void setStructureUlrRelationship(org.cocktail.geide.fwkcktlgedibus.metier.client.StructureUlr value) {
        if (value == null) {
            org.cocktail.geide.fwkcktlgedibus.metier.client.StructureUlr object = structureUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, STRUCTURE_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_ULR_KEY);
        }
    }





}

