
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to TexteMultilingue.java instead.

package org.cocktail.geide.fwkcktlgedibus.metier.client;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _TexteMultilingue extends EOGenericRecord {

    public static final String ENTITY_NAME = "GB_TexteMultilingue";

    public static final String ENTITY_TABLE_NAME = "TEXTE_MULTILINGUE";

    public static final String C_LANGUE_KEY = "cLangue";
    public static final String C_PAYS_KEY = "cPays";
    public static final String TMU_BLOBS_KEY = "tmuBlobs";
    public static final String TMU_ORDRE_KEY = "tmuOrdre";
    public static final String TMU_TEXTE_KEY = "tmuTexte";

    public static final String C_LANGUE_COLKEY = "C_LANGUE";
    public static final String C_PAYS_COLKEY = "C_PAYS";
    public static final String TMU_BLOBS_COLKEY = "TMU_BLOBS";
    public static final String TMU_ORDRE_COLKEY = "TMU_ORDRE";
    public static final String TMU_TEXTE_COLKEY = "TMU_TEXTE";





	
    public _TexteMultilingue() {
        super();
    }




    public String cLangue() {
        return (String)storedValueForKey(C_LANGUE_KEY);
    }
    public void setCLangue(String aValue) {
        takeStoredValueForKey(aValue, C_LANGUE_KEY);
    }

    public String cPays() {
        return (String)storedValueForKey(C_PAYS_KEY);
    }
    public void setCPays(String aValue) {
        takeStoredValueForKey(aValue, C_PAYS_KEY);
    }

    public String tmuBlobs() {
        return (String)storedValueForKey(TMU_BLOBS_KEY);
    }
    public void setTmuBlobs(String aValue) {
        takeStoredValueForKey(aValue, TMU_BLOBS_KEY);
    }

    public Number tmuOrdre() {
        return (Number)storedValueForKey(TMU_ORDRE_KEY);
    }
    public void setTmuOrdre(Number aValue) {
        takeStoredValueForKey(aValue, TMU_ORDRE_KEY);
    }

    public String tmuTexte() {
        return (String)storedValueForKey(TMU_TEXTE_KEY);
    }
    public void setTmuTexte(String aValue) {
        takeStoredValueForKey(aValue, TMU_TEXTE_KEY);
    }








}

