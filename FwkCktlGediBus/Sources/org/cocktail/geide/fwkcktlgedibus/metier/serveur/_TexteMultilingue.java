// DO NOT EDIT.  Make changes to TexteMultilingue.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _TexteMultilingue extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_TexteMultilingue";

  // Attribute Keys
  public static final ERXKey<String> C_LANGUE = new ERXKey<String>("cLangue");
  public static final ERXKey<String> C_PAYS = new ERXKey<String>("cPays");
  public static final ERXKey<String> TMU_BLOBS = new ERXKey<String>("tmuBlobs");
  public static final ERXKey<Integer> TMU_ORDRE = new ERXKey<Integer>("tmuOrdre");
  public static final ERXKey<String> TMU_TEXTE = new ERXKey<String>("tmuTexte");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> LANGUE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue>("langue");

  // Attributes
  public static final String C_LANGUE_KEY = C_LANGUE.key();
  public static final String C_PAYS_KEY = C_PAYS.key();
  public static final String TMU_BLOBS_KEY = TMU_BLOBS.key();
  public static final String TMU_ORDRE_KEY = TMU_ORDRE.key();
  public static final String TMU_TEXTE_KEY = TMU_TEXTE.key();
  // Relationships
  public static final String LANGUE_KEY = LANGUE.key();

  private static Logger LOG = Logger.getLogger(_TexteMultilingue.class);

  public TexteMultilingue localInstanceIn(EOEditingContext editingContext) {
    TexteMultilingue localInstance = (TexteMultilingue)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cLangue() {
    return (String) storedValueForKey(_TexteMultilingue.C_LANGUE_KEY);
  }

  public void setCLangue(String value) {
    if (_TexteMultilingue.LOG.isDebugEnabled()) {
    	_TexteMultilingue.LOG.debug( "updating cLangue from " + cLangue() + " to " + value);
    }
    takeStoredValueForKey(value, _TexteMultilingue.C_LANGUE_KEY);
  }

  public String cPays() {
    return (String) storedValueForKey(_TexteMultilingue.C_PAYS_KEY);
  }

  public void setCPays(String value) {
    if (_TexteMultilingue.LOG.isDebugEnabled()) {
    	_TexteMultilingue.LOG.debug( "updating cPays from " + cPays() + " to " + value);
    }
    takeStoredValueForKey(value, _TexteMultilingue.C_PAYS_KEY);
  }

  public String tmuBlobs() {
    return (String) storedValueForKey(_TexteMultilingue.TMU_BLOBS_KEY);
  }

  public void setTmuBlobs(String value) {
    if (_TexteMultilingue.LOG.isDebugEnabled()) {
    	_TexteMultilingue.LOG.debug( "updating tmuBlobs from " + tmuBlobs() + " to " + value);
    }
    takeStoredValueForKey(value, _TexteMultilingue.TMU_BLOBS_KEY);
  }

  public Integer tmuOrdre() {
    return (Integer) storedValueForKey(_TexteMultilingue.TMU_ORDRE_KEY);
  }

  public void setTmuOrdre(Integer value) {
    if (_TexteMultilingue.LOG.isDebugEnabled()) {
    	_TexteMultilingue.LOG.debug( "updating tmuOrdre from " + tmuOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, _TexteMultilingue.TMU_ORDRE_KEY);
  }

  public String tmuTexte() {
    return (String) storedValueForKey(_TexteMultilingue.TMU_TEXTE_KEY);
  }

  public void setTmuTexte(String value) {
    if (_TexteMultilingue.LOG.isDebugEnabled()) {
    	_TexteMultilingue.LOG.debug( "updating tmuTexte from " + tmuTexte() + " to " + value);
    }
    takeStoredValueForKey(value, _TexteMultilingue.TMU_TEXTE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOLangue langue() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOLangue)storedValueForKey(_TexteMultilingue.LANGUE_KEY);
  }
  
  public void setLangue(org.cocktail.fwkcktlpersonne.common.metier.EOLangue value) {
    takeStoredValueForKey(value, _TexteMultilingue.LANGUE_KEY);
  }

  public void setLangueRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOLangue value) {
    if (_TexteMultilingue.LOG.isDebugEnabled()) {
      _TexteMultilingue.LOG.debug("updating langue from " + langue() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setLangue(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOLangue oldValue = langue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _TexteMultilingue.LANGUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _TexteMultilingue.LANGUE_KEY);
    }
  }
  

  public static TexteMultilingue createGB_TexteMultilingue(EOEditingContext editingContext, String cLangue
, String cPays
, Integer tmuOrdre
, org.cocktail.fwkcktlpersonne.common.metier.EOLangue langue) {
    TexteMultilingue eo = (TexteMultilingue) EOUtilities.createAndInsertInstance(editingContext, _TexteMultilingue.ENTITY_NAME);    
		eo.setCLangue(cLangue);
		eo.setCPays(cPays);
		eo.setTmuOrdre(tmuOrdre);
    eo.setLangueRelationship(langue);
    return eo;
  }

  public static ERXFetchSpecification<TexteMultilingue> fetchSpec() {
    return new ERXFetchSpecification<TexteMultilingue>(_TexteMultilingue.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TexteMultilingue> fetchAllGB_TexteMultilingues(EOEditingContext editingContext) {
    return _TexteMultilingue.fetchAllGB_TexteMultilingues(editingContext, null);
  }

  public static NSArray<TexteMultilingue> fetchAllGB_TexteMultilingues(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _TexteMultilingue.fetchGB_TexteMultilingues(editingContext, null, sortOrderings);
  }

  public static NSArray<TexteMultilingue> fetchGB_TexteMultilingues(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TexteMultilingue> fetchSpec = new ERXFetchSpecification<TexteMultilingue>(_TexteMultilingue.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TexteMultilingue> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TexteMultilingue fetchGB_TexteMultilingue(EOEditingContext editingContext, String keyName, Object value) {
    return _TexteMultilingue.fetchGB_TexteMultilingue(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TexteMultilingue fetchGB_TexteMultilingue(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TexteMultilingue> eoObjects = _TexteMultilingue.fetchGB_TexteMultilingues(editingContext, qualifier, null);
    TexteMultilingue eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_TexteMultilingue that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TexteMultilingue fetchRequiredGB_TexteMultilingue(EOEditingContext editingContext, String keyName, Object value) {
    return _TexteMultilingue.fetchRequiredGB_TexteMultilingue(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TexteMultilingue fetchRequiredGB_TexteMultilingue(EOEditingContext editingContext, EOQualifier qualifier) {
    TexteMultilingue eoObject = _TexteMultilingue.fetchGB_TexteMultilingue(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_TexteMultilingue that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TexteMultilingue localInstanceIn(EOEditingContext editingContext, TexteMultilingue eo) {
    TexteMultilingue localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
