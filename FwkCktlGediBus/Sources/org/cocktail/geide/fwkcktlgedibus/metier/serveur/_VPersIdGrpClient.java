// DO NOT EDIT.  Make changes to VPersIdGrpClient.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _VPersIdGrpClient extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "VPersIdGrpClient";

  // Attribute Keys
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  public static final ERXKey<Integer> PERS_ID_CLIENT = new ERXKey<Integer>("persIdClient");
  // Relationship Keys

  // Attributes
  public static final String PERS_ID_KEY = PERS_ID.key();
  public static final String PERS_ID_CLIENT_KEY = PERS_ID_CLIENT.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_VPersIdGrpClient.class);

  public VPersIdGrpClient localInstanceIn(EOEditingContext editingContext) {
    VPersIdGrpClient localInstance = (VPersIdGrpClient)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_VPersIdGrpClient.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_VPersIdGrpClient.LOG.isDebugEnabled()) {
    	_VPersIdGrpClient.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _VPersIdGrpClient.PERS_ID_KEY);
  }

  public Integer persIdClient() {
    return (Integer) storedValueForKey(_VPersIdGrpClient.PERS_ID_CLIENT_KEY);
  }

  public void setPersIdClient(Integer value) {
    if (_VPersIdGrpClient.LOG.isDebugEnabled()) {
    	_VPersIdGrpClient.LOG.debug( "updating persIdClient from " + persIdClient() + " to " + value);
    }
    takeStoredValueForKey(value, _VPersIdGrpClient.PERS_ID_CLIENT_KEY);
  }


  public static VPersIdGrpClient createVPersIdGrpClient(EOEditingContext editingContext, Integer persId
, Integer persIdClient
) {
    VPersIdGrpClient eo = (VPersIdGrpClient) EOUtilities.createAndInsertInstance(editingContext, _VPersIdGrpClient.ENTITY_NAME);    
		eo.setPersId(persId);
		eo.setPersIdClient(persIdClient);
    return eo;
  }

  public static ERXFetchSpecification<VPersIdGrpClient> fetchSpec() {
    return new ERXFetchSpecification<VPersIdGrpClient>(_VPersIdGrpClient.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<VPersIdGrpClient> fetchAllVPersIdGrpClients(EOEditingContext editingContext) {
    return _VPersIdGrpClient.fetchAllVPersIdGrpClients(editingContext, null);
  }

  public static NSArray<VPersIdGrpClient> fetchAllVPersIdGrpClients(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _VPersIdGrpClient.fetchVPersIdGrpClients(editingContext, null, sortOrderings);
  }

  public static NSArray<VPersIdGrpClient> fetchVPersIdGrpClients(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<VPersIdGrpClient> fetchSpec = new ERXFetchSpecification<VPersIdGrpClient>(_VPersIdGrpClient.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<VPersIdGrpClient> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static VPersIdGrpClient fetchVPersIdGrpClient(EOEditingContext editingContext, String keyName, Object value) {
    return _VPersIdGrpClient.fetchVPersIdGrpClient(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VPersIdGrpClient fetchVPersIdGrpClient(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<VPersIdGrpClient> eoObjects = _VPersIdGrpClient.fetchVPersIdGrpClients(editingContext, qualifier, null);
    VPersIdGrpClient eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one VPersIdGrpClient that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VPersIdGrpClient fetchRequiredVPersIdGrpClient(EOEditingContext editingContext, String keyName, Object value) {
    return _VPersIdGrpClient.fetchRequiredVPersIdGrpClient(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VPersIdGrpClient fetchRequiredVPersIdGrpClient(EOEditingContext editingContext, EOQualifier qualifier) {
    VPersIdGrpClient eoObject = _VPersIdGrpClient.fetchVPersIdGrpClient(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no VPersIdGrpClient that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VPersIdGrpClient localInstanceIn(EOEditingContext editingContext, VPersIdGrpClient eo) {
    VPersIdGrpClient localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
