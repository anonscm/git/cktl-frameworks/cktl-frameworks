

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.geide.fwkcktlgedibus.metier.serveur;


import org.cocktail.fwkcktlged.serveur.metier.EODocAsAttachment;
import org.cocktail.fwkcktlged.serveur.metier.EODocAsUrl;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlwebapp.common.util.GEDDescription;
import org.cocktail.geide.fwkcktlgedibus.exception.ReferenceLienPrimaryKeyNotFoundException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

import er.attachment.model.ERAttachment;
import er.attachment.model.ERFileAttachment;

public class ReferenceLien extends _ReferenceLien
{
    public static final String TYPE_REFERENCE_FICHIER = "F";
    public static final String TYPE_REFERENCE_LIEN = "U";

    public ReferenceLien() {
        super();
    }
    
    public Number rftOrdre() throws ReferenceLienPrimaryKeyNotFoundException{
        if(editingContext()==null)
            throw new ReferenceLienPrimaryKeyNotFoundException("l'editingContext de l'objet est null");
        return (Number)EOUtilities.primaryKeyForObject(editingContext(),this).valueForKey("rflOrdre");
    }
/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/



    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

	public static ReferenceLien creer(EOEditingContext edtitingContext, Courrier document, GEDDescription documentGedfs, String lien, String libelleLien, String typeLien) {
		ReferenceLien refLien = null;
		
		refLien = (ReferenceLien)EOUtilities.createAndInsertInstance(edtitingContext, ENTITY_NAME);
		
		if (documentGedfs != null) {
			refLien.setRflDocumentId(new Integer(documentGedfs.documentId));
			refLien.setRflLien(documentGedfs.reference);
			if (typeLien == null) {
				typeLien = TYPE_REFERENCE_FICHIER;
			}
		} else if (lien != null) {
			refLien.setRflLien(lien);
			if (typeLien == null) {
				typeLien = TYPE_REFERENCE_LIEN;
			}
		} else {
			
		}
		refLien.setRflLibelle(libelleLien);
		refLien.setRflType(typeLien);
		
		return refLien;
	}

	
	
	
	
	/**
	 * Creer.
	 *
	 * @param editingContext the editing context
	 * @param document the document
	 * @param cktlDocument the cktl document
	 * @return the reference lien
	 */
	public static ReferenceLien creer(EOEditingContext editingContext, Courrier document, EODocument cktlDocument) {
		ReferenceLien refLien = null;
		
		if (editingContext != null && document != null && cktlDocument != null) {
			refLien = (ReferenceLien)EOUtilities.createAndInsertInstance(editingContext, ENTITY_NAME);
			refLien.setCouNumero(document.couNumero());
			if (cktlDocument.isFile()) {
				EODocAsAttachment localCktlDocument = (EODocAsAttachment)cktlDocument;
				ERAttachment attachment = localCktlDocument.toErAttachment();
				refLien.setRflLien(((ERFileAttachment)attachment).filesystemPath());
				refLien.setRflType(TYPE_REFERENCE_FICHIER);
				refLien.setDocumentRelationship(localCktlDocument);
			} else if (cktlDocument.isUrl()) {
				EODocAsUrl localCktlDocument = (EODocAsUrl)cktlDocument;
				refLien.setRflLien(localCktlDocument.toStockageUrl().surlUrl());
				refLien.setRflType(TYPE_REFERENCE_LIEN);
				refLien.setDocumentRelationship(localCktlDocument);
			}
			refLien.setRflLibelle(document.couObjet());
		}
		
		return refLien;
	}

}
