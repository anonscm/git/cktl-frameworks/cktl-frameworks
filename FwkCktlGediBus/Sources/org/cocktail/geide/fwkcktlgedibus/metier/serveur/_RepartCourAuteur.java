// DO NOT EDIT.  Make changes to RepartCourAuteur.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _RepartCourAuteur extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_RepartCourAuteur";

  // Attribute Keys
  public static final ERXKey<Integer> COU_NUMERO = new ERXKey<Integer>("couNumero");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  // Relationship Keys

  // Attributes
  public static final String COU_NUMERO_KEY = COU_NUMERO.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String PERS_ID_KEY = PERS_ID.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_RepartCourAuteur.class);

  public RepartCourAuteur localInstanceIn(EOEditingContext editingContext) {
    RepartCourAuteur localInstance = (RepartCourAuteur)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer couNumero() {
    return (Integer) storedValueForKey(_RepartCourAuteur.COU_NUMERO_KEY);
  }

  public void setCouNumero(Integer value) {
    if (_RepartCourAuteur.LOG.isDebugEnabled()) {
    	_RepartCourAuteur.LOG.debug( "updating couNumero from " + couNumero() + " to " + value);
    }
    takeStoredValueForKey(value, _RepartCourAuteur.COU_NUMERO_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_RepartCourAuteur.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_RepartCourAuteur.LOG.isDebugEnabled()) {
    	_RepartCourAuteur.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _RepartCourAuteur.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_RepartCourAuteur.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_RepartCourAuteur.LOG.isDebugEnabled()) {
    	_RepartCourAuteur.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _RepartCourAuteur.D_MODIFICATION_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_RepartCourAuteur.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_RepartCourAuteur.LOG.isDebugEnabled()) {
    	_RepartCourAuteur.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _RepartCourAuteur.PERS_ID_KEY);
  }


  public static RepartCourAuteur createGB_RepartCourAuteur(EOEditingContext editingContext, Integer couNumero
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persId
) {
    RepartCourAuteur eo = (RepartCourAuteur) EOUtilities.createAndInsertInstance(editingContext, _RepartCourAuteur.ENTITY_NAME);    
		eo.setCouNumero(couNumero);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersId(persId);
    return eo;
  }

  public static ERXFetchSpecification<RepartCourAuteur> fetchSpec() {
    return new ERXFetchSpecification<RepartCourAuteur>(_RepartCourAuteur.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<RepartCourAuteur> fetchAllGB_RepartCourAuteurs(EOEditingContext editingContext) {
    return _RepartCourAuteur.fetchAllGB_RepartCourAuteurs(editingContext, null);
  }

  public static NSArray<RepartCourAuteur> fetchAllGB_RepartCourAuteurs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _RepartCourAuteur.fetchGB_RepartCourAuteurs(editingContext, null, sortOrderings);
  }

  public static NSArray<RepartCourAuteur> fetchGB_RepartCourAuteurs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<RepartCourAuteur> fetchSpec = new ERXFetchSpecification<RepartCourAuteur>(_RepartCourAuteur.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<RepartCourAuteur> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static RepartCourAuteur fetchGB_RepartCourAuteur(EOEditingContext editingContext, String keyName, Object value) {
    return _RepartCourAuteur.fetchGB_RepartCourAuteur(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartCourAuteur fetchGB_RepartCourAuteur(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<RepartCourAuteur> eoObjects = _RepartCourAuteur.fetchGB_RepartCourAuteurs(editingContext, qualifier, null);
    RepartCourAuteur eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_RepartCourAuteur that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartCourAuteur fetchRequiredGB_RepartCourAuteur(EOEditingContext editingContext, String keyName, Object value) {
    return _RepartCourAuteur.fetchRequiredGB_RepartCourAuteur(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartCourAuteur fetchRequiredGB_RepartCourAuteur(EOEditingContext editingContext, EOQualifier qualifier) {
    RepartCourAuteur eoObject = _RepartCourAuteur.fetchGB_RepartCourAuteur(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_RepartCourAuteur that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartCourAuteur localInstanceIn(EOEditingContext editingContext, RepartCourAuteur eo) {
    RepartCourAuteur localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
