

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.geide.fwkcktlgedibus.metier.serveur;


import org.cocktail.geide.fwkcktlgedibus.exception.ReferenceNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.UpdateSeqRefFailedException;

import com.webobjects.eoaccess.EOObjectNotAvailableException;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class Reference extends _Reference
{
    public Reference() {
        super();
    }

    
    /**
     * donne la référence à utiliser pour la création d'un nouveau courrier et incremente la sequence de la reference
     * 
     * @return libelle de la référence '/' la sequence de la référence
     * @throws UpdateSeqRefFailedException
     */
    public String refForNewCourrier() throws UpdateSeqRefFailedException {
        String str = (String) storedValueForKey("refLibelle");
        str += "/";
        str += ((Number) storedValueForKey("refSeq")).intValue();
        try {
            takeStoredValueForKey(new Integer(((Integer) storedValueForKey("refSeq")).intValue() + 1), "refSeq");
        } catch (Exception e) {
            throw new UpdateSeqRefFailedException(e.getMessage());
        }
        return str;
    }

    /**
     * Permet de récuperer la cle primaire de la table Reference
     * 
     * @return le refOrdre de la référence
     * @throws ReferenceNotFoundException
     */
    public Integer refOrdre() throws ReferenceNotFoundException {
        if (editingContext() == null)
            throw new ReferenceNotFoundException(null);
        NSDictionary dico = EOUtilities.primaryKeyForObject(editingContext(), this);
        return (Integer) dico.valueForKey("refOrdre");
    }
/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/



    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


	public static Reference referenceForLibelle(EOEditingContext ec, String libelle) {
		Reference reference = null;
		NSMutableDictionary values = new NSMutableDictionary();
		
		values.setObjectForKey(libelle, REF_LIBELLE_KEY);
		try {
			reference = (Reference)EOUtilities.objectMatchingValues(ec, ENTITY_NAME, values);
		} catch (EOObjectNotAvailableException e) {
			// La reference recherchee n'existe pas ou n'est pas unique !
			reference = null;
		}
		
		return reference;
	}

	public static Reference referenceForLibelleAndAgtLogin(EOEditingContext ec, String racineReference, String login) {
		Reference reference = null;
		NSMutableDictionary values = new NSMutableDictionary();
		
		values.setObjectForKey(racineReference, REF_LIBELLE_KEY);
		values.setObjectForKey(login, AGT_LOGIN_KEY);
		try {
			reference = (Reference)EOUtilities.objectMatchingValues(ec, ENTITY_NAME, values);
		} catch (EOObjectNotAvailableException e) {
			// La reference recherchee n'existe pas
			reference = null;
		}
		
		return reference;
	}


	public static Reference creer(EOEditingContext ec, String code, String libelle, String login) {
		Reference reference = null;
		
		reference = (Reference)EOUtilities.createAndInsertInstance(ec, ENTITY_NAME);
		reference.setAgtLogin(login);
		reference.setRefLibelle(libelle);
		reference.setRefCode(code);
		reference.setRefSeq(new Integer(0));
		
		return reference;
	}

}
