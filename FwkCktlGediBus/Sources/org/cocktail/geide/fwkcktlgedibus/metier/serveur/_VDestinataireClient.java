// DO NOT EDIT.  Make changes to VDestinataireClient.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _VDestinataireClient extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "VDestinataireClient";

  // Attribute Keys
  public static final ERXKey<Integer> COU_NUMERO = new ERXKey<Integer>("couNumero");
  public static final ERXKey<NSTimestamp> DEST_DATE = new ERXKey<NSTimestamp>("destDate");
  public static final ERXKey<String> DEST_DROIT_MODIF = new ERXKey<String>("destDroitModif");
  public static final ERXKey<String> DEST_LU = new ERXKey<String>("destLu");
  public static final ERXKey<String> DEST_MAIL = new ERXKey<String>("destMail");
  public static final ERXKey<String> DEST_RAISON = new ERXKey<String>("destRaison");
  public static final ERXKey<String> DEST_TYPE = new ERXKey<String>("destType");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  public static final ERXKey<Integer> PERS_ID_CLIENT = new ERXKey<Integer>("persIdClient");
  // Relationship Keys

  // Attributes
  public static final String COU_NUMERO_KEY = COU_NUMERO.key();
  public static final String DEST_DATE_KEY = DEST_DATE.key();
  public static final String DEST_DROIT_MODIF_KEY = DEST_DROIT_MODIF.key();
  public static final String DEST_LU_KEY = DEST_LU.key();
  public static final String DEST_MAIL_KEY = DEST_MAIL.key();
  public static final String DEST_RAISON_KEY = DEST_RAISON.key();
  public static final String DEST_TYPE_KEY = DEST_TYPE.key();
  public static final String PERS_ID_KEY = PERS_ID.key();
  public static final String PERS_ID_CLIENT_KEY = PERS_ID_CLIENT.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_VDestinataireClient.class);

  public VDestinataireClient localInstanceIn(EOEditingContext editingContext) {
    VDestinataireClient localInstance = (VDestinataireClient)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer couNumero() {
    return (Integer) storedValueForKey(_VDestinataireClient.COU_NUMERO_KEY);
  }

  public void setCouNumero(Integer value) {
    if (_VDestinataireClient.LOG.isDebugEnabled()) {
    	_VDestinataireClient.LOG.debug( "updating couNumero from " + couNumero() + " to " + value);
    }
    takeStoredValueForKey(value, _VDestinataireClient.COU_NUMERO_KEY);
  }

  public NSTimestamp destDate() {
    return (NSTimestamp) storedValueForKey(_VDestinataireClient.DEST_DATE_KEY);
  }

  public void setDestDate(NSTimestamp value) {
    if (_VDestinataireClient.LOG.isDebugEnabled()) {
    	_VDestinataireClient.LOG.debug( "updating destDate from " + destDate() + " to " + value);
    }
    takeStoredValueForKey(value, _VDestinataireClient.DEST_DATE_KEY);
  }

  public String destDroitModif() {
    return (String) storedValueForKey(_VDestinataireClient.DEST_DROIT_MODIF_KEY);
  }

  public void setDestDroitModif(String value) {
    if (_VDestinataireClient.LOG.isDebugEnabled()) {
    	_VDestinataireClient.LOG.debug( "updating destDroitModif from " + destDroitModif() + " to " + value);
    }
    takeStoredValueForKey(value, _VDestinataireClient.DEST_DROIT_MODIF_KEY);
  }

  public String destLu() {
    return (String) storedValueForKey(_VDestinataireClient.DEST_LU_KEY);
  }

  public void setDestLu(String value) {
    if (_VDestinataireClient.LOG.isDebugEnabled()) {
    	_VDestinataireClient.LOG.debug( "updating destLu from " + destLu() + " to " + value);
    }
    takeStoredValueForKey(value, _VDestinataireClient.DEST_LU_KEY);
  }

  public String destMail() {
    return (String) storedValueForKey(_VDestinataireClient.DEST_MAIL_KEY);
  }

  public void setDestMail(String value) {
    if (_VDestinataireClient.LOG.isDebugEnabled()) {
    	_VDestinataireClient.LOG.debug( "updating destMail from " + destMail() + " to " + value);
    }
    takeStoredValueForKey(value, _VDestinataireClient.DEST_MAIL_KEY);
  }

  public String destRaison() {
    return (String) storedValueForKey(_VDestinataireClient.DEST_RAISON_KEY);
  }

  public void setDestRaison(String value) {
    if (_VDestinataireClient.LOG.isDebugEnabled()) {
    	_VDestinataireClient.LOG.debug( "updating destRaison from " + destRaison() + " to " + value);
    }
    takeStoredValueForKey(value, _VDestinataireClient.DEST_RAISON_KEY);
  }

  public String destType() {
    return (String) storedValueForKey(_VDestinataireClient.DEST_TYPE_KEY);
  }

  public void setDestType(String value) {
    if (_VDestinataireClient.LOG.isDebugEnabled()) {
    	_VDestinataireClient.LOG.debug( "updating destType from " + destType() + " to " + value);
    }
    takeStoredValueForKey(value, _VDestinataireClient.DEST_TYPE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_VDestinataireClient.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_VDestinataireClient.LOG.isDebugEnabled()) {
    	_VDestinataireClient.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _VDestinataireClient.PERS_ID_KEY);
  }

  public Integer persIdClient() {
    return (Integer) storedValueForKey(_VDestinataireClient.PERS_ID_CLIENT_KEY);
  }

  public void setPersIdClient(Integer value) {
    if (_VDestinataireClient.LOG.isDebugEnabled()) {
    	_VDestinataireClient.LOG.debug( "updating persIdClient from " + persIdClient() + " to " + value);
    }
    takeStoredValueForKey(value, _VDestinataireClient.PERS_ID_CLIENT_KEY);
  }


  public static VDestinataireClient createVDestinataireClient(EOEditingContext editingContext, Integer couNumero
, String destDroitModif
, String destRaison
, String destType
, Integer persId
, Integer persIdClient
) {
    VDestinataireClient eo = (VDestinataireClient) EOUtilities.createAndInsertInstance(editingContext, _VDestinataireClient.ENTITY_NAME);    
		eo.setCouNumero(couNumero);
		eo.setDestDroitModif(destDroitModif);
		eo.setDestRaison(destRaison);
		eo.setDestType(destType);
		eo.setPersId(persId);
		eo.setPersIdClient(persIdClient);
    return eo;
  }

  public static ERXFetchSpecification<VDestinataireClient> fetchSpec() {
    return new ERXFetchSpecification<VDestinataireClient>(_VDestinataireClient.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<VDestinataireClient> fetchAllVDestinataireClients(EOEditingContext editingContext) {
    return _VDestinataireClient.fetchAllVDestinataireClients(editingContext, null);
  }

  public static NSArray<VDestinataireClient> fetchAllVDestinataireClients(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _VDestinataireClient.fetchVDestinataireClients(editingContext, null, sortOrderings);
  }

  public static NSArray<VDestinataireClient> fetchVDestinataireClients(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<VDestinataireClient> fetchSpec = new ERXFetchSpecification<VDestinataireClient>(_VDestinataireClient.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<VDestinataireClient> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static VDestinataireClient fetchVDestinataireClient(EOEditingContext editingContext, String keyName, Object value) {
    return _VDestinataireClient.fetchVDestinataireClient(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VDestinataireClient fetchVDestinataireClient(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<VDestinataireClient> eoObjects = _VDestinataireClient.fetchVDestinataireClients(editingContext, qualifier, null);
    VDestinataireClient eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one VDestinataireClient that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VDestinataireClient fetchRequiredVDestinataireClient(EOEditingContext editingContext, String keyName, Object value) {
    return _VDestinataireClient.fetchRequiredVDestinataireClient(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VDestinataireClient fetchRequiredVDestinataireClient(EOEditingContext editingContext, EOQualifier qualifier) {
    VDestinataireClient eoObject = _VDestinataireClient.fetchVDestinataireClient(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no VDestinataireClient that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VDestinataireClient localInstanceIn(EOEditingContext editingContext, VDestinataireClient eo) {
    VDestinataireClient localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
