// DO NOT EDIT.  Make changes to DroitTypeClient.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _DroitTypeClient extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_DroitTypeClient";

  // Attribute Keys
  public static final ERXKey<String> AGT_TYPE = new ERXKey<String>("agtType");
  public static final ERXKey<String> CLIENT_TYPE = new ERXKey<String>("clientType");
  public static final ERXKey<String> D_LEC = new ERXKey<String>("dLec");
  public static final ERXKey<String> D_MOD = new ERXKey<String>("dMod");
  public static final ERXKey<String> D_SUP = new ERXKey<String>("dSup");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew> TYPE_COURRIER_NEWS = new ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew>("typeCourrierNews");

  // Attributes
  public static final String AGT_TYPE_KEY = AGT_TYPE.key();
  public static final String CLIENT_TYPE_KEY = CLIENT_TYPE.key();
  public static final String D_LEC_KEY = D_LEC.key();
  public static final String D_MOD_KEY = D_MOD.key();
  public static final String D_SUP_KEY = D_SUP.key();
  public static final String PERS_ID_KEY = PERS_ID.key();
  // Relationships
  public static final String TYPE_COURRIER_NEWS_KEY = TYPE_COURRIER_NEWS.key();

  private static Logger LOG = Logger.getLogger(_DroitTypeClient.class);

  public DroitTypeClient localInstanceIn(EOEditingContext editingContext) {
    DroitTypeClient localInstance = (DroitTypeClient)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String agtType() {
    return (String) storedValueForKey(_DroitTypeClient.AGT_TYPE_KEY);
  }

  public void setAgtType(String value) {
    if (_DroitTypeClient.LOG.isDebugEnabled()) {
    	_DroitTypeClient.LOG.debug( "updating agtType from " + agtType() + " to " + value);
    }
    takeStoredValueForKey(value, _DroitTypeClient.AGT_TYPE_KEY);
  }

  public String clientType() {
    return (String) storedValueForKey(_DroitTypeClient.CLIENT_TYPE_KEY);
  }

  public void setClientType(String value) {
    if (_DroitTypeClient.LOG.isDebugEnabled()) {
    	_DroitTypeClient.LOG.debug( "updating clientType from " + clientType() + " to " + value);
    }
    takeStoredValueForKey(value, _DroitTypeClient.CLIENT_TYPE_KEY);
  }

  public String dLec() {
    return (String) storedValueForKey(_DroitTypeClient.D_LEC_KEY);
  }

  public void setDLec(String value) {
    if (_DroitTypeClient.LOG.isDebugEnabled()) {
    	_DroitTypeClient.LOG.debug( "updating dLec from " + dLec() + " to " + value);
    }
    takeStoredValueForKey(value, _DroitTypeClient.D_LEC_KEY);
  }

  public String dMod() {
    return (String) storedValueForKey(_DroitTypeClient.D_MOD_KEY);
  }

  public void setDMod(String value) {
    if (_DroitTypeClient.LOG.isDebugEnabled()) {
    	_DroitTypeClient.LOG.debug( "updating dMod from " + dMod() + " to " + value);
    }
    takeStoredValueForKey(value, _DroitTypeClient.D_MOD_KEY);
  }

  public String dSup() {
    return (String) storedValueForKey(_DroitTypeClient.D_SUP_KEY);
  }

  public void setDSup(String value) {
    if (_DroitTypeClient.LOG.isDebugEnabled()) {
    	_DroitTypeClient.LOG.debug( "updating dSup from " + dSup() + " to " + value);
    }
    takeStoredValueForKey(value, _DroitTypeClient.D_SUP_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_DroitTypeClient.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_DroitTypeClient.LOG.isDebugEnabled()) {
    	_DroitTypeClient.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _DroitTypeClient.PERS_ID_KEY);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew> typeCourrierNews() {
    return (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew>)storedValueForKey(_DroitTypeClient.TYPE_COURRIER_NEWS_KEY);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew> typeCourrierNews(EOQualifier qualifier) {
    return typeCourrierNews(qualifier, null);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew> typeCourrierNews(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew> results;
      results = typeCourrierNews();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTypeCourrierNews(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew object) {
    includeObjectIntoPropertyWithKey(object, _DroitTypeClient.TYPE_COURRIER_NEWS_KEY);
  }

  public void removeFromTypeCourrierNews(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew object) {
    excludeObjectFromPropertyWithKey(object, _DroitTypeClient.TYPE_COURRIER_NEWS_KEY);
  }

  public void addToTypeCourrierNewsRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew object) {
    if (_DroitTypeClient.LOG.isDebugEnabled()) {
      _DroitTypeClient.LOG.debug("adding " + object + " to typeCourrierNews relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToTypeCourrierNews(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _DroitTypeClient.TYPE_COURRIER_NEWS_KEY);
    }
  }

  public void removeFromTypeCourrierNewsRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew object) {
    if (_DroitTypeClient.LOG.isDebugEnabled()) {
      _DroitTypeClient.LOG.debug("removing " + object + " from typeCourrierNews relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromTypeCourrierNews(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _DroitTypeClient.TYPE_COURRIER_NEWS_KEY);
    }
  }

  public org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew createTypeCourrierNewsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _DroitTypeClient.TYPE_COURRIER_NEWS_KEY);
    return (org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew) eo;
  }

  public void deleteTypeCourrierNewsRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _DroitTypeClient.TYPE_COURRIER_NEWS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTypeCourrierNewsRelationships() {
    Enumeration<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew> objects = typeCourrierNews().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTypeCourrierNewsRelationship(objects.nextElement());
    }
  }


  public static DroitTypeClient createGB_DroitTypeClient(EOEditingContext editingContext, String agtType
, String clientType
, Integer persId
) {
    DroitTypeClient eo = (DroitTypeClient) EOUtilities.createAndInsertInstance(editingContext, _DroitTypeClient.ENTITY_NAME);    
		eo.setAgtType(agtType);
		eo.setClientType(clientType);
		eo.setPersId(persId);
    return eo;
  }

  public static ERXFetchSpecification<DroitTypeClient> fetchSpec() {
    return new ERXFetchSpecification<DroitTypeClient>(_DroitTypeClient.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<DroitTypeClient> fetchAllGB_DroitTypeClients(EOEditingContext editingContext) {
    return _DroitTypeClient.fetchAllGB_DroitTypeClients(editingContext, null);
  }

  public static NSArray<DroitTypeClient> fetchAllGB_DroitTypeClients(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _DroitTypeClient.fetchGB_DroitTypeClients(editingContext, null, sortOrderings);
  }

  public static NSArray<DroitTypeClient> fetchGB_DroitTypeClients(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<DroitTypeClient> fetchSpec = new ERXFetchSpecification<DroitTypeClient>(_DroitTypeClient.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<DroitTypeClient> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static DroitTypeClient fetchGB_DroitTypeClient(EOEditingContext editingContext, String keyName, Object value) {
    return _DroitTypeClient.fetchGB_DroitTypeClient(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static DroitTypeClient fetchGB_DroitTypeClient(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<DroitTypeClient> eoObjects = _DroitTypeClient.fetchGB_DroitTypeClients(editingContext, qualifier, null);
    DroitTypeClient eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_DroitTypeClient that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static DroitTypeClient fetchRequiredGB_DroitTypeClient(EOEditingContext editingContext, String keyName, Object value) {
    return _DroitTypeClient.fetchRequiredGB_DroitTypeClient(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static DroitTypeClient fetchRequiredGB_DroitTypeClient(EOEditingContext editingContext, EOQualifier qualifier) {
    DroitTypeClient eoObject = _DroitTypeClient.fetchGB_DroitTypeClient(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_DroitTypeClient that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static DroitTypeClient localInstanceIn(EOEditingContext editingContext, DroitTypeClient eo) {
    DroitTypeClient localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
