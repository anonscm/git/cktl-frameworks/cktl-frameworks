// DO NOT EDIT.  Make changes to MotsClefs.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _MotsClefs extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_MotsClefs";

  // Attribute Keys
  public static final ERXKey<String> MC_LIBELLE = new ERXKey<String>("mcLibelle");
  public static final ERXKey<String> MC_TYPE = new ERXKey<String>("mcType");
  // Relationship Keys

  // Attributes
  public static final String MC_LIBELLE_KEY = MC_LIBELLE.key();
  public static final String MC_TYPE_KEY = MC_TYPE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_MotsClefs.class);

  public MotsClefs localInstanceIn(EOEditingContext editingContext) {
    MotsClefs localInstance = (MotsClefs)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String mcLibelle() {
    return (String) storedValueForKey(_MotsClefs.MC_LIBELLE_KEY);
  }

  public void setMcLibelle(String value) {
    if (_MotsClefs.LOG.isDebugEnabled()) {
    	_MotsClefs.LOG.debug( "updating mcLibelle from " + mcLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, _MotsClefs.MC_LIBELLE_KEY);
  }

  public String mcType() {
    return (String) storedValueForKey(_MotsClefs.MC_TYPE_KEY);
  }

  public void setMcType(String value) {
    if (_MotsClefs.LOG.isDebugEnabled()) {
    	_MotsClefs.LOG.debug( "updating mcType from " + mcType() + " to " + value);
    }
    takeStoredValueForKey(value, _MotsClefs.MC_TYPE_KEY);
  }


  public static MotsClefs createGB_MotsClefs(EOEditingContext editingContext, String mcLibelle
, String mcType
) {
    MotsClefs eo = (MotsClefs) EOUtilities.createAndInsertInstance(editingContext, _MotsClefs.ENTITY_NAME);    
		eo.setMcLibelle(mcLibelle);
		eo.setMcType(mcType);
    return eo;
  }

  public static ERXFetchSpecification<MotsClefs> fetchSpec() {
    return new ERXFetchSpecification<MotsClefs>(_MotsClefs.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<MotsClefs> fetchAllGB_MotsClefses(EOEditingContext editingContext) {
    return _MotsClefs.fetchAllGB_MotsClefses(editingContext, null);
  }

  public static NSArray<MotsClefs> fetchAllGB_MotsClefses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _MotsClefs.fetchGB_MotsClefses(editingContext, null, sortOrderings);
  }

  public static NSArray<MotsClefs> fetchGB_MotsClefses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<MotsClefs> fetchSpec = new ERXFetchSpecification<MotsClefs>(_MotsClefs.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<MotsClefs> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static MotsClefs fetchGB_MotsClefs(EOEditingContext editingContext, String keyName, Object value) {
    return _MotsClefs.fetchGB_MotsClefs(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static MotsClefs fetchGB_MotsClefs(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<MotsClefs> eoObjects = _MotsClefs.fetchGB_MotsClefses(editingContext, qualifier, null);
    MotsClefs eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_MotsClefs that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static MotsClefs fetchRequiredGB_MotsClefs(EOEditingContext editingContext, String keyName, Object value) {
    return _MotsClefs.fetchRequiredGB_MotsClefs(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static MotsClefs fetchRequiredGB_MotsClefs(EOEditingContext editingContext, EOQualifier qualifier) {
    MotsClefs eoObject = _MotsClefs.fetchGB_MotsClefs(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_MotsClefs that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static MotsClefs localInstanceIn(EOEditingContext editingContext, MotsClefs eo) {
    MotsClefs localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
