// DO NOT EDIT.  Make changes to Destinataire.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _Destinataire extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_Destinataire";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DEST_DATE = new ERXKey<NSTimestamp>("destDate");
  public static final ERXKey<String> DEST_DROIT_MODIF = new ERXKey<String>("destDroitModif");
  public static final ERXKey<String> DEST_LU = new ERXKey<String>("destLu");
  public static final ERXKey<String> DEST_MAIL = new ERXKey<String>("destMail");
  public static final ERXKey<String> DEST_RAISON = new ERXKey<String>("destRaison");
  public static final ERXKey<String> DEST_TYPE = new ERXKey<String>("destType");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  // Relationship Keys

  // Attributes
  public static final String DEST_DATE_KEY = DEST_DATE.key();
  public static final String DEST_DROIT_MODIF_KEY = DEST_DROIT_MODIF.key();
  public static final String DEST_LU_KEY = DEST_LU.key();
  public static final String DEST_MAIL_KEY = DEST_MAIL.key();
  public static final String DEST_RAISON_KEY = DEST_RAISON.key();
  public static final String DEST_TYPE_KEY = DEST_TYPE.key();
  public static final String PERS_ID_KEY = PERS_ID.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_Destinataire.class);

  public Destinataire localInstanceIn(EOEditingContext editingContext) {
    Destinataire localInstance = (Destinataire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp destDate() {
    return (NSTimestamp) storedValueForKey(_Destinataire.DEST_DATE_KEY);
  }

  public void setDestDate(NSTimestamp value) {
    if (_Destinataire.LOG.isDebugEnabled()) {
    	_Destinataire.LOG.debug( "updating destDate from " + destDate() + " to " + value);
    }
    takeStoredValueForKey(value, _Destinataire.DEST_DATE_KEY);
  }

  public String destDroitModif() {
    return (String) storedValueForKey(_Destinataire.DEST_DROIT_MODIF_KEY);
  }

  public void setDestDroitModif(String value) {
    if (_Destinataire.LOG.isDebugEnabled()) {
    	_Destinataire.LOG.debug( "updating destDroitModif from " + destDroitModif() + " to " + value);
    }
    takeStoredValueForKey(value, _Destinataire.DEST_DROIT_MODIF_KEY);
  }

  public String destLu() {
    return (String) storedValueForKey(_Destinataire.DEST_LU_KEY);
  }

  public void setDestLu(String value) {
    if (_Destinataire.LOG.isDebugEnabled()) {
    	_Destinataire.LOG.debug( "updating destLu from " + destLu() + " to " + value);
    }
    takeStoredValueForKey(value, _Destinataire.DEST_LU_KEY);
  }

  public String destMail() {
    return (String) storedValueForKey(_Destinataire.DEST_MAIL_KEY);
  }

  public void setDestMail(String value) {
    if (_Destinataire.LOG.isDebugEnabled()) {
    	_Destinataire.LOG.debug( "updating destMail from " + destMail() + " to " + value);
    }
    takeStoredValueForKey(value, _Destinataire.DEST_MAIL_KEY);
  }

  public String destRaison() {
    return (String) storedValueForKey(_Destinataire.DEST_RAISON_KEY);
  }

  public void setDestRaison(String value) {
    if (_Destinataire.LOG.isDebugEnabled()) {
    	_Destinataire.LOG.debug( "updating destRaison from " + destRaison() + " to " + value);
    }
    takeStoredValueForKey(value, _Destinataire.DEST_RAISON_KEY);
  }

  public String destType() {
    return (String) storedValueForKey(_Destinataire.DEST_TYPE_KEY);
  }

  public void setDestType(String value) {
    if (_Destinataire.LOG.isDebugEnabled()) {
    	_Destinataire.LOG.debug( "updating destType from " + destType() + " to " + value);
    }
    takeStoredValueForKey(value, _Destinataire.DEST_TYPE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_Destinataire.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_Destinataire.LOG.isDebugEnabled()) {
    	_Destinataire.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _Destinataire.PERS_ID_KEY);
  }


  public static Destinataire createGB_Destinataire(EOEditingContext editingContext, String destDroitModif
, String destRaison
, String destType
, Integer persId
) {
    Destinataire eo = (Destinataire) EOUtilities.createAndInsertInstance(editingContext, _Destinataire.ENTITY_NAME);    
		eo.setDestDroitModif(destDroitModif);
		eo.setDestRaison(destRaison);
		eo.setDestType(destType);
		eo.setPersId(persId);
    return eo;
  }

  public static ERXFetchSpecification<Destinataire> fetchSpec() {
    return new ERXFetchSpecification<Destinataire>(_Destinataire.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Destinataire> fetchAllGB_Destinataires(EOEditingContext editingContext) {
    return _Destinataire.fetchAllGB_Destinataires(editingContext, null);
  }

  public static NSArray<Destinataire> fetchAllGB_Destinataires(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _Destinataire.fetchGB_Destinataires(editingContext, null, sortOrderings);
  }

  public static NSArray<Destinataire> fetchGB_Destinataires(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Destinataire> fetchSpec = new ERXFetchSpecification<Destinataire>(_Destinataire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Destinataire> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Destinataire fetchGB_Destinataire(EOEditingContext editingContext, String keyName, Object value) {
    return _Destinataire.fetchGB_Destinataire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Destinataire fetchGB_Destinataire(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Destinataire> eoObjects = _Destinataire.fetchGB_Destinataires(editingContext, qualifier, null);
    Destinataire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_Destinataire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Destinataire fetchRequiredGB_Destinataire(EOEditingContext editingContext, String keyName, Object value) {
    return _Destinataire.fetchRequiredGB_Destinataire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Destinataire fetchRequiredGB_Destinataire(EOEditingContext editingContext, EOQualifier qualifier) {
    Destinataire eoObject = _Destinataire.fetchGB_Destinataire(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_Destinataire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Destinataire localInstanceIn(EOEditingContext editingContext, Destinataire eo) {
    Destinataire localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
