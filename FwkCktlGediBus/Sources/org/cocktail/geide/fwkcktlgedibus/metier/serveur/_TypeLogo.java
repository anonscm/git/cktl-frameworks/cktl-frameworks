// DO NOT EDIT.  Make changes to TypeLogo.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _TypeLogo extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_TypeLogo";

  // Attribute Keys
  public static final ERXKey<NSData> TL_LOGO = new ERXKey<NSData>("tlLogo");
  // Relationship Keys

  // Attributes
  public static final String TL_LOGO_KEY = TL_LOGO.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_TypeLogo.class);

  public TypeLogo localInstanceIn(EOEditingContext editingContext) {
    TypeLogo localInstance = (TypeLogo)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSData tlLogo() {
    return (NSData) storedValueForKey(_TypeLogo.TL_LOGO_KEY);
  }

  public void setTlLogo(NSData value) {
    if (_TypeLogo.LOG.isDebugEnabled()) {
    	_TypeLogo.LOG.debug( "updating tlLogo from " + tlLogo() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeLogo.TL_LOGO_KEY);
  }


  public static TypeLogo createGB_TypeLogo(EOEditingContext editingContext) {
    TypeLogo eo = (TypeLogo) EOUtilities.createAndInsertInstance(editingContext, _TypeLogo.ENTITY_NAME);    
    return eo;
  }

  public static ERXFetchSpecification<TypeLogo> fetchSpec() {
    return new ERXFetchSpecification<TypeLogo>(_TypeLogo.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypeLogo> fetchAllGB_TypeLogos(EOEditingContext editingContext) {
    return _TypeLogo.fetchAllGB_TypeLogos(editingContext, null);
  }

  public static NSArray<TypeLogo> fetchAllGB_TypeLogos(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _TypeLogo.fetchGB_TypeLogos(editingContext, null, sortOrderings);
  }

  public static NSArray<TypeLogo> fetchGB_TypeLogos(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypeLogo> fetchSpec = new ERXFetchSpecification<TypeLogo>(_TypeLogo.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypeLogo> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypeLogo fetchGB_TypeLogo(EOEditingContext editingContext, String keyName, Object value) {
    return _TypeLogo.fetchGB_TypeLogo(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeLogo fetchGB_TypeLogo(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypeLogo> eoObjects = _TypeLogo.fetchGB_TypeLogos(editingContext, qualifier, null);
    TypeLogo eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_TypeLogo that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeLogo fetchRequiredGB_TypeLogo(EOEditingContext editingContext, String keyName, Object value) {
    return _TypeLogo.fetchRequiredGB_TypeLogo(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeLogo fetchRequiredGB_TypeLogo(EOEditingContext editingContext, EOQualifier qualifier) {
    TypeLogo eoObject = _TypeLogo.fetchGB_TypeLogo(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_TypeLogo that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeLogo localInstanceIn(EOEditingContext editingContext, TypeLogo eo) {
    TypeLogo localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
