

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.geide.fwkcktlgedibus.metier.serveur;


import java.util.Enumeration;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;


public class TypeCourrierNew extends _TypeCourrierNew
{
	public static final String SEQ_ENTITY_NAME = "GB_SeqTypeCourrierNew";

	public TypeCourrierNew() {
        super();
    }
    
	/**
	 * Racine de l'arborescence
	 */
	public static final String TYPE_CHAR_ROOT = "0";
	
    /**
     * 
     * @param ec
     * @param typeOdreChar
     * @return
     * 	Le type correspondant au typeOdreChar
     */
	public static org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew typeNewForTypeOrdreChar( EOEditingContext ec, String typeOdreChar) {
		org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew type = null;
		if ( (ec != null) && (typeOdreChar != null) && (typeOdreChar.length() > 0) ) {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("typeOrdreChar = %@", new NSArray(typeOdreChar));
			EOFetchSpecification spec = new EOFetchSpecification("GB_TypeCourrierNew",qualifier,null);
			NSArray array = ec.objectsWithFetchSpecification(spec);
			if ( (array != null) && (array.count() > 0))
				type = (org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew)array.objectAtIndex(0);
		}
		
		return type;
	}
	
	/**
	 * Forme un tableau de la hierarchie des dossiers en partant d'un type start jusqu'au type end.
	 * 
	 * @param typeCharStart
	 * @param typeCharEnd
	 * @return
	 */
	public static NSArray arrayHierarchy( EOEditingContext ec, String typeCharStart, String typeCharEnd) {

		NSMutableArray array = null;
		
		if ( (typeCharStart != null) && (typeCharStart.length() > 0) ) {
			org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew typeEnd = 	
				org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew.typeNewForTypeOrdreChar( ec, typeCharStart);
			
			boolean isContinue = true;
			String tcEnd = TYPE_CHAR_ROOT;
			if ( (typeCharEnd != null) && (typeCharEnd.length() > 0) )
				tcEnd = typeCharEnd;
			
			if (typeEnd != null) {
				array = new NSMutableArray();
				while ( isContinue ) {
					array.addObject(typeEnd.typeOrdreChar());
					typeEnd = org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew.typeNewForTypeOrdreChar( ec, typeEnd.typeCode());
					isContinue = (typeEnd != null) && (typeEnd.typeOrdreChar() != null) && ! (typeEnd.typeOrdreChar().equals(tcEnd));
				}
			}
		}
		return (NSArray)array;
	}

/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/



    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


	public static TypeCourrierNew categorieForTypeOrdre(EOEditingContext ec, Integer racine) {
		TypeCourrierNew categorie = null;
		
		try {
			categorie = (TypeCourrierNew)EOUtilities.objectWithPrimaryKeyValue(ec, ENTITY_NAME, racine);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return categorie;
	}


	public static TypeCourrierNew creerCategorie(EOEditingContext ec, Integer racine, String categorie) {
		TypeCourrierNew nouvelleCategorie = TypeCourrierNew.categorieForTypeOrdre(ec, racine);
		if (nouvelleCategorie != null) {
			NSArray categoriesLogiques = NSArray.componentsSeparatedByString(categorie, "/");
			Enumeration enumCategoriesLogiques = categoriesLogiques.objectEnumerator();
			while (enumCategoriesLogiques.hasMoreElements()) {
				String object = (String) enumCategoriesLogiques.nextElement();
				TypeCourrierNew categorieFille = TypeCourrierNew.categorieForRootWithLibelle(ec,nouvelleCategorie,object);
				if (categorieFille != null) {
					nouvelleCategorie = categorieFille;
				} else {
					nouvelleCategorie = TypeCourrierNew.creer(ec, nouvelleCategorie, object);
					break;
				}
			}
			while (enumCategoriesLogiques.hasMoreElements()) {
				String object = (String) enumCategoriesLogiques.nextElement();
				nouvelleCategorie = TypeCourrierNew.creer(ec, nouvelleCategorie, object);
			}
			
		}
		return nouvelleCategorie;
	}


	private static TypeCourrierNew creer(EOEditingContext ec, TypeCourrierNew categorieRacine, String libelle) {
		TypeCourrierNew type = null;
		
		type = (TypeCourrierNew)EOUtilities.createAndInsertInstance(ec, ENTITY_NAME);
        type.setTypeLibelle(libelle);
        type.setTypePageTitre(libelle);
        type.setTypeCode(categorieRacine.typeOrdre().toString());
        type.setCleDates(new Integer(4));
        type.setTypeFamille(categorieRacine.typeFamille());
        type.setTypeTypeConsultation(categorieRacine.typeTypeConsultation());
        type.setTypeVisibleAppli(categorieRacine.typeVisibleAppli());
        Integer cle = clePrimaire(ec);
        type.setTypeOrdre(cle);
        type.setTypeOrdreChar(String.valueOf(cle));
        
		return type;
	}


	private static TypeCourrierNew categorieForRootWithLibelle(EOEditingContext ec, Integer categorieRacine, String libelle) {
		TypeCourrierNew categorie = null;
		NSMutableDictionary values = new NSMutableDictionary();
		
		try {
			values.setObjectForKey(String.valueOf(categorieRacine.intValue()), TYPE_CODE_KEY);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		values.setObjectForKey(libelle, TYPE_LIBELLE_KEY);
		
		try {
			categorie = (TypeCourrierNew)EOUtilities.objectMatchingValues(ec, ENTITY_NAME, values);
		} catch (Exception e) {
			// La categorie recherchee n'existe pas
			categorie = null;
		}
		
		return categorie;
	}

	private static TypeCourrierNew categorieForRootWithLibelle(EOEditingContext ec, TypeCourrierNew categorieRacine, String libelle) {
		TypeCourrierNew categorie = null;
		NSMutableDictionary values = new NSMutableDictionary();
		
		try {
			values.setObjectForKey(String.valueOf(categorieRacine.typeOrdre()), TYPE_CODE_KEY);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		values.setObjectForKey(libelle, TYPE_LIBELLE_KEY);
		
		try {
			categorie = (TypeCourrierNew)EOUtilities.objectMatchingValues(ec, ENTITY_NAME, values);
		} catch (Exception e) {
			// La categorie recherchee n'existe pas
			categorie = null;
		}
		
		return categorie;
	}

	/** Evalue la cl&eacute; primaire pour une entit&eacute; en cherchant dans la table de s&eacute;quence pour une base Oracle
	 * @param editingContext
	 * @return valeur de la cl&eacute; primaire
	 */
	public static Integer clePrimaire(EOEditingContext editingContext) {
		Integer clePrimaire = null;
		EOFetchSpecification  myFetch = new EOFetchSpecification(SEQ_ENTITY_NAME,null,null);
		NSArray result = editingContext.objectsWithFetchSpecification(myFetch);
		try {
			Number numero = (Number)((NSArray) result.valueForKey("nextval")).objectAtIndex(0);
			clePrimaire = new Integer(numero.intValue());
		} catch (Exception e) {
		}
		return clePrimaire;
	}
	
}
