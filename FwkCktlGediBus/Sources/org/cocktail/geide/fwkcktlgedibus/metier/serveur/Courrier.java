

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.geide.fwkcktlgedibus.metier.serveur;


import java.util.Enumeration;

import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlged.serveur.metier.EOTypeDocument;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.geide.fwkcktlgedibus.tools.GediBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class Courrier extends _Courrier
{
	public static final String SEQ_ENTITY_NAME = "GB_SeqCourrier";
	/**
	 * Type de consultation de larticle: voir INTERNET, INTRANET etc ... 
	 */	 
	/**
	 * Visible aux Etudiants et Personnels
	 */
    public static final int INTRANET = 0;  	// Visible aux Etudiants et Personnels
    /**
     * visible a tous les internautes
     */
    public static final int INTERNET = 1;	// visible a tous les internautes
    /**
     * Toute personne authentifiee (sans destinataire choisi)
     */
    public static final int EXTRANET = 2;	// Toute personne authentifiee (sans destinataire choisi)
    /**
     * Visible a l'auteur et aux destinataires eventuels
     */
	public static final int PRIVE = 3;		// Visible a l'auteur et aux destinataires eventuels
	/**
	 * Doit devenir PRIVE
	 */
    public static final int MASQUE = 4;		// Doit devenir PRIVE
    /**
     * Invisible et inactif
     */
	public static final int CORBEILLE = 5;	// Invisible et inactif
	
    public Courrier() {
        super();
    }
    
	/**
	 * Appele lors de l'insertion de l'objet dans l'editingcontext.
	 * 
	 * @see com.webobjects.eocontrol.EOCustomObject#awakeFromInsertion(com.webobjects.eocontrol.EOEditingContext)
	 */
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);

		setCouNumero(clePrimaire(ec));

	}

	/** Evalue la cl&eacute; primaire pour une entit&eacute; en cherchant dans la table de s&eacute;quence pour une base Oracle
	 * @param editingContext
	 * @return valeur de la cl&eacute; primaire
	 */
	public static Integer clePrimaire(EOEditingContext editingContext) {
		Integer clePrimaire = null;
		EOFetchSpecification  myFetch = new EOFetchSpecification(SEQ_ENTITY_NAME,null,null);
		NSArray result = editingContext.objectsWithFetchSpecification(myFetch);
		try {
			Number numero = (Number)((NSArray) result.valueForKey("nextval")).objectAtIndex(0);
			clePrimaire = new Integer(numero.intValue());
		} catch (Exception e) {
		}
		return clePrimaire;
	}
	/**
     * Permet de recuperer la cle primaire de la table courrier
     * 
     * @return le couNumero du courrier
     * @throws Exception
     *             CourrierPrimaryKeyNotFoundException
     */
//    public Integer couNumero()throws CourrierPrimaryKeyNotFoundException{
//    	Integer counum = (Integer)storedValueForKey("couNumeroBis");
//    	if(counum!=null)
//    		return counum;
//        NSDictionary dico=null;
//        try {
//            dico = EOUtilities.primaryKeyForObject(this.editingContext(),this);   
//        } catch (Exception e) {
//            throw new CourrierPrimaryKeyNotFoundException(e.getMessage());
//        }
//        if(dico!=null)
//            return (Integer)dico.objectForKey("couNumero");
//        throw new CourrierPrimaryKeyNotFoundException(null);
//    }
    
	/**
	 * Type consultation du courrier
	 * @return Vrai si le type est verifie
	 * 
	 */
	public boolean isInternet() {
		return this.couTypeConsultation().intValue() == INTERNET;
	}

	public boolean isIntranet() {
		return this.couTypeConsultation().intValue() == INTRANET;
	}
	
	public boolean isExtranet() {
		return this.couTypeConsultation().intValue() == EXTRANET;
	}
	
	public boolean isPrive() {
		return this.couTypeConsultation().intValue() == PRIVE;
	}

	public boolean isMasque() {
		return this.couTypeConsultation().intValue() == MASQUE;
	}
	
	public boolean isCorbeille() {
		return this.couTypeConsultation().intValue() >= CORBEILLE;
	}
	

	/**
	 * Verifie si l'individu (donne par son persId) ou l'un de ses groupes ancetres est destinataire du couurier 
	 * avec le droit donne (lecture ou modification).
	 * @param individuPersId 
	 * 			persId de l'individu
	 * @param droitModif 
	 * 			valeur Null ou Boolean.false pour le droit de lecture <br
	 * 			valeur Boolean.true pour le droit de modification
	 * @return
	 * 			null si individuPersId est null
	 * 			Boolean.true si l'individu est destinataire de l'article avec le droit de modif
	 * 			Boolean.false si l'individu est destinataire de l'article avec le droit de lecteur
	 */
	public Boolean isIndividuDestinataireWithDroit( Number individuPersId, Boolean droitModif) {
		
		Boolean b = null;

		if ( (individuPersId != null) && (individuPersId.intValue() > 0) ) {
			// L'individu est destinataire directe ?
			b = isIndividuDirectDestinataireWithDroit( individuPersId, droitModif);
			
			// Uniquement dans le cas ou l'individu n'est pas direct destinataire
			// on cherche une structure destinataire dans l'arbre des structures mere dont l'individu est membre
			if ( b == null) {
				EOIndividu unIndividu = EOIndividu.individuWithPersId(editingContext(), (Integer)individuPersId);
				NSArray structures = GediBus.hierarchicalGroupesForIndividu(unIndividu);
				if ( (structures != null) && (structures.count() > 0) ) {
					Enumeration enumerator = structures.objectEnumerator();
					EOStructure aStructure = null;
					while( (b == null) && enumerator.hasMoreElements()) {
						aStructure = (EOStructure)enumerator.nextElement();
						// Recherche si la structure est destinataire direct
						b = isStructuredDirectDestinataireWithDroit( aStructure.persId(), droitModif);
					}
				}
			}
		}
		return b;
	}
	
	/**
	 * Verifie si l'individu (donne par son persId) est destinataire direct du courrier 
	 * avec le droit donne (lecture ou modification).
	 * @param individuPersId 
	 * 			persId de l'individu
	 * @param droitModif 
	 * 			valeur Null ou Boolean.false pour le droit de lecture <br
	 * 			valeur Boolean.true pour le droit de modification
	 * @return
	 * 			null si individuPersId est null
	 * 			Boolean.true si l'individu est destinataire de l'article avec le droit de modif
	 * 			Boolean.false si l'individu est destinataire de l'article avec le droit de lecteur
	 */
	public Boolean isIndividuDirectDestinataireWithDroit( Number individuPersId, Boolean droitModif) {
	
		Boolean b = null;

		if ( (individuPersId != null) && (individuPersId.intValue() > 0) ) {
			org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire destinataire = destinataireIndividuWithPersId( individuPersId);
			if ( destinataire != null) {
				try {
					EOIndividu individu = EOIndividu.individuWithPersId(editingContext(), (Integer)individuPersId);
					Number persId = individu.persId();
					
					// L'individu est-il destinataire ?
					if ( (persId != null) && (persId.intValue() > 0) && (persId.intValue() == individuPersId.intValue()))
						b = new Boolean (false); 		// L'individu est destinataire
					
					// L'individu destinataire a-t-il le droit ?
					if ( (b != null) && (droitModif != null) && droitModif.booleanValue()) {
						b = new Boolean ((destinataire.destDroitModif() != null) && destinataire.destDroitModif().equals("O"));
					}
				} catch (Exception e) {
					b = null;
				}
			}
		}
		return b;
	}
	
	/**
	 * Verifie si la structure (donne par son persId) est destinataire direct du couurier 
	 * avec le droit donne (lecture ou modification).
	 * @param individuPersId 
	 * 			persId de la structure
	 * @param droitModif 
	 * 			valeur Null ou Boolean.false pour le droit de lecture <br
	 * 			valeur Boolean.true pour le droit de modification
	 * @return
	 * 			null si individuPersId est null
	 * 			Boolean.true si l'individu est destinataire de l'article avec le droit de modif
	 * 			Boolean.false si l'individu est destinataire de l'article avec le droit de lecteur
	 */
	public Boolean isStructuredDirectDestinataireWithDroit( Number persId, Boolean isDroit) {
		Boolean b = null;
		Destinataire destinataire = destinataireGroupeWithPersId( persId);
		if ( (destinataire != null) && (destinataire.destType().startsWith("G")) ) {
			try {
				
				EOStructure structure = EOStructure.structureWithPersId(editingContext(), (Integer)persId);
				if ( structure.cStructure() != null) {
					b = new Boolean( false);		// La structure est destinataire
					if ( (isDroit != null) && isDroit.booleanValue()) {
						b = new Boolean((destinataire.destDroitModif() != null) && destinataire.destDroitModif().equals("O"));
					}
				}
			} catch (Exception e) {
				b = null;
			}
		}	
		return b;
	}
	
	/**
	 * Individu Destinataire du courrier, avec le persId donne.
	 * @param persId
	 * 			persId de l'individu
	 * @return
	 * 			L'objet Destinataire du courrier s'il existe
	 */
	public org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire destinataireIndividuWithPersId( Number persId) {
		org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire dest = null;
		
		if ( (persId != null) && (persId.intValue() > 0) ) {
			// gB_Destinataires
			NSArray destinataires = this.gB_Destinataires();
			if ( (destinataires != null) && (destinataires.count() > 0)) {
				NSMutableArray args = new NSMutableArray();
				args.addObject(EOQualifier.qualifierWithQualifierFormat("persId = %@",new NSArray(persId)));
				args.addObject(EOQualifier.qualifierWithQualifierFormat("destType <> %@",new NSArray("G")));
		 		NSArray array = EOQualifier.filteredArrayWithQualifier( destinataires, new EOAndQualifier(args));
		 		if ( (array != null) && (array.count() > 0) )
		 			dest = (org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire)array.objectAtIndex(0);
			}
		
		}
		return dest;
	}

	/**
	 * Le groupe Destinataire du courrier, avec le persId donne.
	 * @param persId
	 * 			persId de l'individu
	 * @return
	 * 			L'objet Destinataire du courrier s'il existe
	 */
	public Destinataire destinataireGroupeWithPersId( Number persId) {
		org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire dest = null;
		
		if ( (persId != null) && (persId.intValue() > 0) ) {
			// gB_Destinataires
			NSArray destinataires = this.gB_Destinataires();
			if ( (destinataires != null) && (destinataires.count() > 0)) {
				NSMutableArray args = new NSMutableArray();
				args.addObject(EOQualifier.qualifierWithQualifierFormat("persId = %@",new NSArray(persId)));
				args.addObject(EOQualifier.qualifierWithQualifierFormat("destType = %@",new NSArray("G")));
		 		NSArray array = EOQualifier.filteredArrayWithQualifier( destinataires, new EOAndQualifier(args));
		 		if ( (array != null) && (array.count() > 0) )
		 			dest = (org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire)array.objectAtIndex(0);
			}
		
		}
		return dest;
	}
    
	/**
	 * @param ec
	 * @param nomDocument
	 * @param racine
	 * @param categorie
	 * @param racineReference
	 * @param visibilite
	 * @param objet
	 * @param commentaire
	 * @param motsCles
	 * @param dateDocument
	 * @param dateDerniereModification
	 * @param utilisateur
	 * @return
	 */
	public static Courrier creer(EOEditingContext ec, 
			String nomDocument, 
			Integer racine, String categorie,
			String racineReference, 
			Integer visibilite, 
			String objet, String commentaire, String motsCles, 
			NSTimestamp dateDocument, NSTimestamp dateDerniereModification, 
			ApplicationUser utilisateur) {

		Courrier document = (Courrier)EOUtilities.createAndInsertInstance(ec, ENTITY_NAME);
		
		// TODO Utiliser une relation sur ApplicationUser pour memoriser les infos de l'utilisateur
		document.setAgtLogin(utilisateur.getLogin());
		document.setCouExpediteur(utilisateur.getNomAndPrenom());
		document.setPersId(utilisateur.getPersId());
		
		// Donnees directement liees au document
		document.setCouDepartArrivee("A");
		document.setCouObjet(objet);
		document.setCouCommentaire(commentaire);
		document.setCouDateCourrier(dateDocument);
		document.setCouDateEnreg(dateDerniereModification);
		document.setCouMotsClefs(motsCles);
		
		if (visibilite == null || visibilite.equals("")) {
			visibilite = new Integer(PRIVE);
		}
		document.setCouTypeConsultation(visibilite);
		
		// Calcul de la reference du document
		if (StringCtrl.isEmpty(racineReference)) {
			racineReference = "ABORT/"+utilisateur.getLogin(); 
		}
		Reference reference = Reference.referenceForLibelleAndAgtLogin(ec, racineReference, utilisateur.getLogin());
		if (reference == null) {
			reference = Reference.creer(ec, null, racineReference, utilisateur.getLogin());
		} else {
			reference.setRefSeq(new Integer(reference.refSeq().intValue()+1));
		}
		document.setCouReference(reference.refLibelle());
		document.setCouIdent(reference.refSeq());
		
		// Creation des types et sous-types (repertoires du chemin logique)
		TypeCourrierNew categorieMere = TypeCourrierNew.creerCategorie(ec, racine, categorie);
		if (categorieMere == null) {
			// Une erreur s'est produite lors de la creation des categories ou 
			// lors de l'ajout du document dans sa categorie ==> on annule tout
			ec.revert();
			document = null;
		} else {
			// On ajoute le document a la categorie
			TypesAssocies typeAssocie = null;
			typeAssocie = TypesAssocies.creer(ec, categorieMere, document);
			document.addToTypesAssociesRelationship(typeAssocie);
			// ec.saveChanges();
		}
		
		return document;
	}

    public void deleteReferenceLiensRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, REFERENCE_LIENS_KEY);
        editingContext().deleteObject(object);
      }
	public void deleteAllReferenceLiensRelationships() {
		Enumeration objects = referenceLiens().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteReferenceLiensRelationship((org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien)objects.nextElement());
		}
	}

    public void deleteGB_DestinatairesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, G_B__DESTINATAIRES_KEY);
        editingContext().deleteObject(object);
      }
	public void deleteAllGB_DestinatairesRelationships() {
		Enumeration objects = gB_Destinataires().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteGB_DestinatairesRelationship((Destinataire)objects.nextElement());
		}
	}

    public void deleteGB_RepartCourAuteursRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, G_B__REPART_COUR_AUTEURS_KEY);
        editingContext().deleteObject(object);
      }
	public void deleteAllGB_RepartCourAuteursRelationships() {
		Enumeration objects = gB_RepartCourAuteurs().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteGB_RepartCourAuteursRelationship((org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur)objects.nextElement());
		}
	}

    public void deleteTexteMultilinguesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, G_B__TEXTE_MULTILINGUES_KEY);
        editingContext().deleteObject(object);
      }
	public void deleteAllGB_TexteMultilinguesRelationships() {
		Enumeration objects = gB_TexteMultilingues().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteTexteMultilinguesRelationship((org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue)objects.nextElement());
		}
	}

    public void deleteSpecifWebRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, SPECIF_WEB_KEY);
        editingContext().deleteObject(object);
      }
	public void deleteAllSpecifWebRelationships() {
		Enumeration objects = specifWeb().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteSpecifWebRelationship((org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb)objects.nextElement());
		}
	}

    public void deleteTypesAssociesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, TYPES_ASSOCIES_KEY);
        editingContext().deleteObject(object);
      }
	public void deleteAllTypesAssociesRelationships() {
		Enumeration objects = typesAssocies().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteTypesAssociesRelationship((org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies)objects.nextElement());
		}
	}

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    
 
    public EOTypeDocument type() {
    	EOTypeDocument type = null;
    	NSArray<ReferenceLien>rfls = referenceLiens();
    	if (rfls != null && rfls.count()>0) {
    		ReferenceLien rfl = rfls.lastObject();
    		type = rfl.document().toTypeDocument();
    	}
    	return type;
    }
    
    
	/**
	 * Creer.
	 *
	 * @param ec the ec
	 * @param utilisateur the utilisateur
	 * @return the courrier
	 */
	public static Courrier creer(EOEditingContext ec,
			ApplicationUser utilisateur) {

		Courrier document = (Courrier)EOUtilities.createAndInsertInstance(ec, ENTITY_NAME);
		// TODO Utiliser une relation sur ApplicationUser pour memoriser les infos de l'utilisateur
		document.setAgtLogin(utilisateur.getLogin());
		document.setCouExpediteur(utilisateur.getNomAndPrenom());
		document.setPersId(utilisateur.getPersId());
		
		// Donnees directement liees au document
		document.setCouDepartArrivee("A");
		document.setCouTypeConsultation(new Integer(PRIVE));
		document.setCouDateCourrier(new NSTimestamp());

		return document;
	}
    
    
	/**
	 * Creer.
	 *
	 * @param ec the ec
	 * @param cktlDocument the cktl document
	 * @param racine the racine
	 * @param categorie the categorie
	 * @param racineReference the racine reference
	 * @param visibilite the visibilite
	 * @param objet the objet
	 * @param commentaire the commentaire
	 * @param motsCles the mots cles
	 * @param dateDocument the date document
	 * @param dateDerniereModification the date derniere modification
	 * @param utilisateur the utilisateur
	 * @return the courrier
	 */
	public static Courrier creer(EOEditingContext ec,
			EODocument cktlDocument, 
			Integer racine, String categorie,
			String racineReference, Integer visibilite, 
			String objet, String commentaire, String motsCles, 
			NSTimestamp dateDocument, NSTimestamp dateDerniereModification, 
			ApplicationUser utilisateur) {

		Courrier document = (Courrier)EOUtilities.createAndInsertInstance(ec, ENTITY_NAME);
		
		// TODO Utiliser une relation sur ApplicationUser pour memoriser les infos de l'utilisateur
		document.setAgtLogin(utilisateur.getLogin());
		document.setCouExpediteur(utilisateur.getNomAndPrenom());
		document.setPersId(utilisateur.getPersId());
		
		// Donnees directement liees au document
		document.setCouDepartArrivee("A");
		document.setCouObjet(objet);
		document.setCouCommentaire(commentaire);
		document.setCouDateCourrier(dateDocument);
		document.setCouDateEnreg(dateDerniereModification);
		document.setCouMotsClefs(motsCles);
		
		if (visibilite == null || visibilite.equals("")) {
			visibilite = new Integer(PRIVE);
		}
		document.setCouTypeConsultation(visibilite);
		
		// Calcul de la reference du document
		if (StringCtrl.isEmpty(racineReference)) {
			racineReference = "ABORT/"+utilisateur.getLogin(); 
		}
		Reference reference = Reference.referenceForLibelleAndAgtLogin(ec, racineReference, utilisateur.getLogin());
		if (reference == null) {
			reference = Reference.creer(ec, null, racineReference, utilisateur.getLogin());
		} else {
			reference.setRefSeq(new Integer(reference.refSeq().intValue()+1));
		}
		document.setCouReference(reference.refLibelle());
		document.setCouIdent(reference.refSeq());
		
		// Creation des types et sous-types (repertoires du chemin logique)
		TypeCourrierNew categorieMere = TypeCourrierNew.creerCategorie(ec, racine, categorie);
		if (categorieMere == null) {
			// Une erreur s'est produite lors de la creation des categories ou 
			// lors de l'ajout du document dans sa categorie ==> on annule tout
			ec.revert();
			document = null;
		} else {
			// On ajoute le document a la categorie
			TypesAssocies typeAssocie = null;
			typeAssocie = TypesAssocies.creer(ec, categorieMere, document);
			document.addToTypesAssociesRelationship(typeAssocie);
			// ec.saveChanges();
		}
		
		if (document != null && cktlDocument != null) {
			ReferenceLien rl = ReferenceLien.creer(ec, document, cktlDocument);
			rl.setCouNumero(document.couNumero());
			document.addToReferenceLiensRelationship(rl);
			
		}
		
		return document;
	}

	public String repertoire() {
		String repertoire = null;
		
		NSArray typesAssocies = typesAssocies();
		TypesAssocies unTypesAssocies = (TypesAssocies)typesAssocies.lastObject();
		TypeCourrierNew categorie = unTypesAssocies.typeCourrierNew();
//		repertoire = categorie.typeLibelle();
		while (categorie != null && !categorie.typeFamille().equalsIgnoreCase("W")) {
			String typeLibelle = categorie.typeLibelle();
			int index = typeLibelle.indexOf("#");
			if (index > -1) {
				typeLibelle = typeLibelle.substring(index+1);
			}
			if (repertoire != null) {
				
				repertoire = typeLibelle+" > "+repertoire;
			} else {
				repertoire = typeLibelle;
			}
			categorie = TypeCourrierNew.categorieForTypeOrdre(categorie.editingContext(), Integer.valueOf(categorie.typeCode()));
		}
		return repertoire;
	}
	
	public String referenceComplete() {
		return couReference()+"/"+couIdent();
	}
}
