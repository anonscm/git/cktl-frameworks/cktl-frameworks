// DO NOT EDIT.  Make changes to Reference.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _Reference extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_Reference";

  // Attribute Keys
  public static final ERXKey<String> AGT_LOGIN = new ERXKey<String>("agtLogin");
  public static final ERXKey<String> REF_CODE = new ERXKey<String>("refCode");
  public static final ERXKey<String> REF_LIBELLE = new ERXKey<String>("refLibelle");
  public static final ERXKey<Integer> REF_SEQ = new ERXKey<Integer>("refSeq");
  // Relationship Keys

  // Attributes
  public static final String AGT_LOGIN_KEY = AGT_LOGIN.key();
  public static final String REF_CODE_KEY = REF_CODE.key();
  public static final String REF_LIBELLE_KEY = REF_LIBELLE.key();
  public static final String REF_SEQ_KEY = REF_SEQ.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_Reference.class);

  public Reference localInstanceIn(EOEditingContext editingContext) {
    Reference localInstance = (Reference)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String agtLogin() {
    return (String) storedValueForKey(_Reference.AGT_LOGIN_KEY);
  }

  public void setAgtLogin(String value) {
    if (_Reference.LOG.isDebugEnabled()) {
    	_Reference.LOG.debug( "updating agtLogin from " + agtLogin() + " to " + value);
    }
    takeStoredValueForKey(value, _Reference.AGT_LOGIN_KEY);
  }

  public String refCode() {
    return (String) storedValueForKey(_Reference.REF_CODE_KEY);
  }

  public void setRefCode(String value) {
    if (_Reference.LOG.isDebugEnabled()) {
    	_Reference.LOG.debug( "updating refCode from " + refCode() + " to " + value);
    }
    takeStoredValueForKey(value, _Reference.REF_CODE_KEY);
  }

  public String refLibelle() {
    return (String) storedValueForKey(_Reference.REF_LIBELLE_KEY);
  }

  public void setRefLibelle(String value) {
    if (_Reference.LOG.isDebugEnabled()) {
    	_Reference.LOG.debug( "updating refLibelle from " + refLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, _Reference.REF_LIBELLE_KEY);
  }

  public Integer refSeq() {
    return (Integer) storedValueForKey(_Reference.REF_SEQ_KEY);
  }

  public void setRefSeq(Integer value) {
    if (_Reference.LOG.isDebugEnabled()) {
    	_Reference.LOG.debug( "updating refSeq from " + refSeq() + " to " + value);
    }
    takeStoredValueForKey(value, _Reference.REF_SEQ_KEY);
  }


  public static Reference createGB_Reference(EOEditingContext editingContext) {
    Reference eo = (Reference) EOUtilities.createAndInsertInstance(editingContext, _Reference.ENTITY_NAME);    
    return eo;
  }

  public static ERXFetchSpecification<Reference> fetchSpec() {
    return new ERXFetchSpecification<Reference>(_Reference.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Reference> fetchAllGB_References(EOEditingContext editingContext) {
    return _Reference.fetchAllGB_References(editingContext, null);
  }

  public static NSArray<Reference> fetchAllGB_References(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _Reference.fetchGB_References(editingContext, null, sortOrderings);
  }

  public static NSArray<Reference> fetchGB_References(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Reference> fetchSpec = new ERXFetchSpecification<Reference>(_Reference.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Reference> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Reference fetchGB_Reference(EOEditingContext editingContext, String keyName, Object value) {
    return _Reference.fetchGB_Reference(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Reference fetchGB_Reference(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Reference> eoObjects = _Reference.fetchGB_References(editingContext, qualifier, null);
    Reference eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_Reference that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Reference fetchRequiredGB_Reference(EOEditingContext editingContext, String keyName, Object value) {
    return _Reference.fetchRequiredGB_Reference(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Reference fetchRequiredGB_Reference(EOEditingContext editingContext, EOQualifier qualifier) {
    Reference eoObject = _Reference.fetchGB_Reference(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_Reference that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Reference localInstanceIn(EOEditingContext editingContext, Reference eo) {
    Reference localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
