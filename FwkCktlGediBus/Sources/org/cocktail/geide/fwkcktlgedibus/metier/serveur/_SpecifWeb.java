// DO NOT EDIT.  Make changes to SpecifWeb.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _SpecifWeb extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_SpecifWeb";

  // Attribute Keys
  public static final ERXKey<Integer> COU_NUMERO = new ERXKey<Integer>("couNumero");
  public static final ERXKey<Integer> SPWC_KEY = new ERXKey<Integer>("spwcKey");
  public static final ERXKey<Integer> TYPE_ORDRE = new ERXKey<Integer>("typeOrdre");
  // Relationship Keys
  public static final ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWebCommune> G_B__SPECIF_WEB_COMMUNE = new ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWebCommune>("gB_SpecifWebCommune");

  // Attributes
  public static final String COU_NUMERO_KEY = COU_NUMERO.key();
  public static final String SPWC_KEY_KEY = SPWC_KEY.key();
  public static final String TYPE_ORDRE_KEY = TYPE_ORDRE.key();
  // Relationships
  public static final String G_B__SPECIF_WEB_COMMUNE_KEY = G_B__SPECIF_WEB_COMMUNE.key();

  private static Logger LOG = Logger.getLogger(_SpecifWeb.class);

  public SpecifWeb localInstanceIn(EOEditingContext editingContext) {
    SpecifWeb localInstance = (SpecifWeb)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer couNumero() {
    return (Integer) storedValueForKey(_SpecifWeb.COU_NUMERO_KEY);
  }

  public void setCouNumero(Integer value) {
    if (_SpecifWeb.LOG.isDebugEnabled()) {
    	_SpecifWeb.LOG.debug( "updating couNumero from " + couNumero() + " to " + value);
    }
    takeStoredValueForKey(value, _SpecifWeb.COU_NUMERO_KEY);
  }

  public Integer spwcKey() {
    return (Integer) storedValueForKey(_SpecifWeb.SPWC_KEY_KEY);
  }

  public void setSpwcKey(Integer value) {
    if (_SpecifWeb.LOG.isDebugEnabled()) {
    	_SpecifWeb.LOG.debug( "updating spwcKey from " + spwcKey() + " to " + value);
    }
    takeStoredValueForKey(value, _SpecifWeb.SPWC_KEY_KEY);
  }

  public Integer typeOrdre() {
    return (Integer) storedValueForKey(_SpecifWeb.TYPE_ORDRE_KEY);
  }

  public void setTypeOrdre(Integer value) {
    if (_SpecifWeb.LOG.isDebugEnabled()) {
    	_SpecifWeb.LOG.debug( "updating typeOrdre from " + typeOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, _SpecifWeb.TYPE_ORDRE_KEY);
  }

  public org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWebCommune gB_SpecifWebCommune() {
    return (org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWebCommune)storedValueForKey(_SpecifWeb.G_B__SPECIF_WEB_COMMUNE_KEY);
  }
  
  public void setGB_SpecifWebCommune(org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWebCommune value) {
    takeStoredValueForKey(value, _SpecifWeb.G_B__SPECIF_WEB_COMMUNE_KEY);
  }

  public void setGB_SpecifWebCommuneRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWebCommune value) {
    if (_SpecifWeb.LOG.isDebugEnabled()) {
      _SpecifWeb.LOG.debug("updating gB_SpecifWebCommune from " + gB_SpecifWebCommune() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setGB_SpecifWebCommune(value);
    }
    else if (value == null) {
    	org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWebCommune oldValue = gB_SpecifWebCommune();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _SpecifWeb.G_B__SPECIF_WEB_COMMUNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _SpecifWeb.G_B__SPECIF_WEB_COMMUNE_KEY);
    }
  }
  

  public static SpecifWeb createGB_SpecifWeb(EOEditingContext editingContext, Integer spwcKey
) {
    SpecifWeb eo = (SpecifWeb) EOUtilities.createAndInsertInstance(editingContext, _SpecifWeb.ENTITY_NAME);    
		eo.setSpwcKey(spwcKey);
    return eo;
  }

  public static ERXFetchSpecification<SpecifWeb> fetchSpec() {
    return new ERXFetchSpecification<SpecifWeb>(_SpecifWeb.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<SpecifWeb> fetchAllGB_SpecifWebs(EOEditingContext editingContext) {
    return _SpecifWeb.fetchAllGB_SpecifWebs(editingContext, null);
  }

  public static NSArray<SpecifWeb> fetchAllGB_SpecifWebs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _SpecifWeb.fetchGB_SpecifWebs(editingContext, null, sortOrderings);
  }

  public static NSArray<SpecifWeb> fetchGB_SpecifWebs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<SpecifWeb> fetchSpec = new ERXFetchSpecification<SpecifWeb>(_SpecifWeb.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<SpecifWeb> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static SpecifWeb fetchGB_SpecifWeb(EOEditingContext editingContext, String keyName, Object value) {
    return _SpecifWeb.fetchGB_SpecifWeb(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static SpecifWeb fetchGB_SpecifWeb(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<SpecifWeb> eoObjects = _SpecifWeb.fetchGB_SpecifWebs(editingContext, qualifier, null);
    SpecifWeb eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_SpecifWeb that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static SpecifWeb fetchRequiredGB_SpecifWeb(EOEditingContext editingContext, String keyName, Object value) {
    return _SpecifWeb.fetchRequiredGB_SpecifWeb(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static SpecifWeb fetchRequiredGB_SpecifWeb(EOEditingContext editingContext, EOQualifier qualifier) {
    SpecifWeb eoObject = _SpecifWeb.fetchGB_SpecifWeb(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_SpecifWeb that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static SpecifWeb localInstanceIn(EOEditingContext editingContext, SpecifWeb eo) {
    SpecifWeb localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
