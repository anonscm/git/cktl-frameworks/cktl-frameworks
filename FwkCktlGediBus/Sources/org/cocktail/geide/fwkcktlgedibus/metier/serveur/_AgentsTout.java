// DO NOT EDIT.  Make changes to AgentsTout.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _AgentsTout extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_AgentsTout";

  // Attribute Keys
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  // Relationship Keys

  // Attributes
  public static final String PERS_ID_KEY = PERS_ID.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_AgentsTout.class);

  public AgentsTout localInstanceIn(EOEditingContext editingContext) {
    AgentsTout localInstance = (AgentsTout)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_AgentsTout.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_AgentsTout.LOG.isDebugEnabled()) {
    	_AgentsTout.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _AgentsTout.PERS_ID_KEY);
  }


  public static AgentsTout createGB_AgentsTout(EOEditingContext editingContext, Integer persId
) {
    AgentsTout eo = (AgentsTout) EOUtilities.createAndInsertInstance(editingContext, _AgentsTout.ENTITY_NAME);    
		eo.setPersId(persId);
    return eo;
  }

  public static ERXFetchSpecification<AgentsTout> fetchSpec() {
    return new ERXFetchSpecification<AgentsTout>(_AgentsTout.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<AgentsTout> fetchAllGB_AgentsTouts(EOEditingContext editingContext) {
    return _AgentsTout.fetchAllGB_AgentsTouts(editingContext, null);
  }

  public static NSArray<AgentsTout> fetchAllGB_AgentsTouts(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _AgentsTout.fetchGB_AgentsTouts(editingContext, null, sortOrderings);
  }

  public static NSArray<AgentsTout> fetchGB_AgentsTouts(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<AgentsTout> fetchSpec = new ERXFetchSpecification<AgentsTout>(_AgentsTout.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<AgentsTout> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static AgentsTout fetchGB_AgentsTout(EOEditingContext editingContext, String keyName, Object value) {
    return _AgentsTout.fetchGB_AgentsTout(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static AgentsTout fetchGB_AgentsTout(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<AgentsTout> eoObjects = _AgentsTout.fetchGB_AgentsTouts(editingContext, qualifier, null);
    AgentsTout eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_AgentsTout that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static AgentsTout fetchRequiredGB_AgentsTout(EOEditingContext editingContext, String keyName, Object value) {
    return _AgentsTout.fetchRequiredGB_AgentsTout(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static AgentsTout fetchRequiredGB_AgentsTout(EOEditingContext editingContext, EOQualifier qualifier) {
    AgentsTout eoObject = _AgentsTout.fetchGB_AgentsTout(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_AgentsTout that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static AgentsTout localInstanceIn(EOEditingContext editingContext, AgentsTout eo) {
    AgentsTout localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
