// DO NOT EDIT.  Make changes to ReferenceLien.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _ReferenceLien extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_ReferenceLien";

  // Attribute Keys
  public static final ERXKey<Integer> COU_NUMERO = new ERXKey<Integer>("couNumero");
  public static final ERXKey<Integer> RFL_DOCUMENT_ID = new ERXKey<Integer>("rflDocumentId");
  public static final ERXKey<String> RFL_LIBELLE = new ERXKey<String>("rflLibelle");
  public static final ERXKey<String> RFL_LIEN = new ERXKey<String>("rflLien");
  public static final ERXKey<String> RFL_TYPE = new ERXKey<String>("rflType");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument> DOCUMENT = new ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument>("document");

  // Attributes
  public static final String COU_NUMERO_KEY = COU_NUMERO.key();
  public static final String RFL_DOCUMENT_ID_KEY = RFL_DOCUMENT_ID.key();
  public static final String RFL_LIBELLE_KEY = RFL_LIBELLE.key();
  public static final String RFL_LIEN_KEY = RFL_LIEN.key();
  public static final String RFL_TYPE_KEY = RFL_TYPE.key();
  // Relationships
  public static final String DOCUMENT_KEY = DOCUMENT.key();

  private static Logger LOG = Logger.getLogger(_ReferenceLien.class);

  public ReferenceLien localInstanceIn(EOEditingContext editingContext) {
    ReferenceLien localInstance = (ReferenceLien)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer couNumero() {
    return (Integer) storedValueForKey(_ReferenceLien.COU_NUMERO_KEY);
  }

  public void setCouNumero(Integer value) {
    if (_ReferenceLien.LOG.isDebugEnabled()) {
    	_ReferenceLien.LOG.debug( "updating couNumero from " + couNumero() + " to " + value);
    }
    takeStoredValueForKey(value, _ReferenceLien.COU_NUMERO_KEY);
  }

  public Integer rflDocumentId() {
    return (Integer) storedValueForKey(_ReferenceLien.RFL_DOCUMENT_ID_KEY);
  }

  public void setRflDocumentId(Integer value) {
    if (_ReferenceLien.LOG.isDebugEnabled()) {
    	_ReferenceLien.LOG.debug( "updating rflDocumentId from " + rflDocumentId() + " to " + value);
    }
    takeStoredValueForKey(value, _ReferenceLien.RFL_DOCUMENT_ID_KEY);
  }

  public String rflLibelle() {
    return (String) storedValueForKey(_ReferenceLien.RFL_LIBELLE_KEY);
  }

  public void setRflLibelle(String value) {
    if (_ReferenceLien.LOG.isDebugEnabled()) {
    	_ReferenceLien.LOG.debug( "updating rflLibelle from " + rflLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, _ReferenceLien.RFL_LIBELLE_KEY);
  }

  public String rflLien() {
    return (String) storedValueForKey(_ReferenceLien.RFL_LIEN_KEY);
  }

  public void setRflLien(String value) {
    if (_ReferenceLien.LOG.isDebugEnabled()) {
    	_ReferenceLien.LOG.debug( "updating rflLien from " + rflLien() + " to " + value);
    }
    takeStoredValueForKey(value, _ReferenceLien.RFL_LIEN_KEY);
  }

  public String rflType() {
    return (String) storedValueForKey(_ReferenceLien.RFL_TYPE_KEY);
  }

  public void setRflType(String value) {
    if (_ReferenceLien.LOG.isDebugEnabled()) {
    	_ReferenceLien.LOG.debug( "updating rflType from " + rflType() + " to " + value);
    }
    takeStoredValueForKey(value, _ReferenceLien.RFL_TYPE_KEY);
  }

  public org.cocktail.fwkcktlged.serveur.metier.EODocument document() {
    return (org.cocktail.fwkcktlged.serveur.metier.EODocument)storedValueForKey(_ReferenceLien.DOCUMENT_KEY);
  }
  
  public void setDocument(org.cocktail.fwkcktlged.serveur.metier.EODocument value) {
    takeStoredValueForKey(value, _ReferenceLien.DOCUMENT_KEY);
  }

  public void setDocumentRelationship(org.cocktail.fwkcktlged.serveur.metier.EODocument value) {
    if (_ReferenceLien.LOG.isDebugEnabled()) {
      _ReferenceLien.LOG.debug("updating document from " + document() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setDocument(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlged.serveur.metier.EODocument oldValue = document();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _ReferenceLien.DOCUMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _ReferenceLien.DOCUMENT_KEY);
    }
  }
  

  public static ReferenceLien createGB_ReferenceLien(EOEditingContext editingContext, Integer couNumero
, Integer rflDocumentId
, String rflLien
, org.cocktail.fwkcktlged.serveur.metier.EODocument document) {
    ReferenceLien eo = (ReferenceLien) EOUtilities.createAndInsertInstance(editingContext, _ReferenceLien.ENTITY_NAME);    
		eo.setCouNumero(couNumero);
		eo.setRflDocumentId(rflDocumentId);
		eo.setRflLien(rflLien);
    eo.setDocumentRelationship(document);
    return eo;
  }

  public static ERXFetchSpecification<ReferenceLien> fetchSpec() {
    return new ERXFetchSpecification<ReferenceLien>(_ReferenceLien.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<ReferenceLien> fetchAllGB_ReferenceLiens(EOEditingContext editingContext) {
    return _ReferenceLien.fetchAllGB_ReferenceLiens(editingContext, null);
  }

  public static NSArray<ReferenceLien> fetchAllGB_ReferenceLiens(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _ReferenceLien.fetchGB_ReferenceLiens(editingContext, null, sortOrderings);
  }

  public static NSArray<ReferenceLien> fetchGB_ReferenceLiens(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<ReferenceLien> fetchSpec = new ERXFetchSpecification<ReferenceLien>(_ReferenceLien.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<ReferenceLien> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static ReferenceLien fetchGB_ReferenceLien(EOEditingContext editingContext, String keyName, Object value) {
    return _ReferenceLien.fetchGB_ReferenceLien(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ReferenceLien fetchGB_ReferenceLien(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<ReferenceLien> eoObjects = _ReferenceLien.fetchGB_ReferenceLiens(editingContext, qualifier, null);
    ReferenceLien eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_ReferenceLien that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ReferenceLien fetchRequiredGB_ReferenceLien(EOEditingContext editingContext, String keyName, Object value) {
    return _ReferenceLien.fetchRequiredGB_ReferenceLien(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ReferenceLien fetchRequiredGB_ReferenceLien(EOEditingContext editingContext, EOQualifier qualifier) {
    ReferenceLien eoObject = _ReferenceLien.fetchGB_ReferenceLien(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_ReferenceLien that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ReferenceLien localInstanceIn(EOEditingContext editingContext, ReferenceLien eo) {
    ReferenceLien localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
