// DO NOT EDIT.  Make changes to Courrier.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _Courrier extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_Courrier";

  // Attribute Keys
  public static final ERXKey<String> AGT_LOGIN = new ERXKey<String>("agtLogin");
  public static final ERXKey<String> COU_COMMENTAIRE = new ERXKey<String>("couCommentaire");
  public static final ERXKey<NSTimestamp> COU_DATE_COURRIER = new ERXKey<NSTimestamp>("couDateCourrier");
  public static final ERXKey<NSTimestamp> COU_DATE_ENREG = new ERXKey<NSTimestamp>("couDateEnreg");
  public static final ERXKey<NSTimestamp> COU_DATE_REPONSE_ATTENDUE = new ERXKey<NSTimestamp>("couDateReponseAttendue");
  public static final ERXKey<NSTimestamp> COU_DATE_REPONSE_EFFECTUEE = new ERXKey<NSTimestamp>("couDateReponseEffectuee");
  public static final ERXKey<String> COU_DEPART_ARRIVEE = new ERXKey<String>("couDepartArrivee");
  public static final ERXKey<String> COU_EXPEDITEUR = new ERXKey<String>("couExpediteur");
  public static final ERXKey<String> COU_EXP_ETABLISSEMENT = new ERXKey<String>("couExpEtablissement");
  public static final ERXKey<String> COU_EXP_VIA = new ERXKey<String>("couExpVia");
  public static final ERXKey<Integer> COU_IDENT = new ERXKey<Integer>("couIdent");
  public static final ERXKey<String> COU_LOCALISATION = new ERXKey<String>("couLocalisation");
  public static final ERXKey<String> COU_MOTS_CLEFS = new ERXKey<String>("couMotsClefs");
  public static final ERXKey<Integer> COU_NOMBRE_RELANCE = new ERXKey<Integer>("couNombreRelance");
  public static final ERXKey<Integer> COU_NUMERO = new ERXKey<Integer>("couNumero");
  public static final ERXKey<Integer> COU_NUMERO_PERE = new ERXKey<Integer>("couNumeroPere");
  public static final ERXKey<String> COU_OBJET = new ERXKey<String>("couObjet");
  public static final ERXKey<String> COU_REFERENCE = new ERXKey<String>("couReference");
  public static final ERXKey<Integer> COU_TYPE_CONSULTATION = new ERXKey<Integer>("couTypeConsultation");
  public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire> G_B__DESTINATAIRES = new ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire>("gB_Destinataires");
  public static final ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur> G_B__REPART_COUR_AUTEURS = new ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur>("gB_RepartCourAuteurs");
  public static final ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue> G_B__TEXTE_MULTILINGUES = new ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue>("gB_TexteMultilingues");
  public static final ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien> REFERENCE_LIENS = new ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien>("referenceLiens");
  public static final ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb> SPECIF_WEB = new ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb>("specifWeb");
  public static final ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies> TYPES_ASSOCIES = new ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies>("typesAssocies");

  // Attributes
  public static final String AGT_LOGIN_KEY = AGT_LOGIN.key();
  public static final String COU_COMMENTAIRE_KEY = COU_COMMENTAIRE.key();
  public static final String COU_DATE_COURRIER_KEY = COU_DATE_COURRIER.key();
  public static final String COU_DATE_ENREG_KEY = COU_DATE_ENREG.key();
  public static final String COU_DATE_REPONSE_ATTENDUE_KEY = COU_DATE_REPONSE_ATTENDUE.key();
  public static final String COU_DATE_REPONSE_EFFECTUEE_KEY = COU_DATE_REPONSE_EFFECTUEE.key();
  public static final String COU_DEPART_ARRIVEE_KEY = COU_DEPART_ARRIVEE.key();
  public static final String COU_EXPEDITEUR_KEY = COU_EXPEDITEUR.key();
  public static final String COU_EXP_ETABLISSEMENT_KEY = COU_EXP_ETABLISSEMENT.key();
  public static final String COU_EXP_VIA_KEY = COU_EXP_VIA.key();
  public static final String COU_IDENT_KEY = COU_IDENT.key();
  public static final String COU_LOCALISATION_KEY = COU_LOCALISATION.key();
  public static final String COU_MOTS_CLEFS_KEY = COU_MOTS_CLEFS.key();
  public static final String COU_NOMBRE_RELANCE_KEY = COU_NOMBRE_RELANCE.key();
  public static final String COU_NUMERO_KEY = COU_NUMERO.key();
  public static final String COU_NUMERO_PERE_KEY = COU_NUMERO_PERE.key();
  public static final String COU_OBJET_KEY = COU_OBJET.key();
  public static final String COU_REFERENCE_KEY = COU_REFERENCE.key();
  public static final String COU_TYPE_CONSULTATION_KEY = COU_TYPE_CONSULTATION.key();
  public static final String C_STRUCTURE_KEY = C_STRUCTURE.key();
  public static final String PERS_ID_KEY = PERS_ID.key();
  // Relationships
  public static final String G_B__DESTINATAIRES_KEY = G_B__DESTINATAIRES.key();
  public static final String G_B__REPART_COUR_AUTEURS_KEY = G_B__REPART_COUR_AUTEURS.key();
  public static final String G_B__TEXTE_MULTILINGUES_KEY = G_B__TEXTE_MULTILINGUES.key();
  public static final String REFERENCE_LIENS_KEY = REFERENCE_LIENS.key();
  public static final String SPECIF_WEB_KEY = SPECIF_WEB.key();
  public static final String TYPES_ASSOCIES_KEY = TYPES_ASSOCIES.key();

  private static Logger LOG = Logger.getLogger(_Courrier.class);

  public Courrier localInstanceIn(EOEditingContext editingContext) {
    Courrier localInstance = (Courrier)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String agtLogin() {
    return (String) storedValueForKey(_Courrier.AGT_LOGIN_KEY);
  }

  public void setAgtLogin(String value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating agtLogin from " + agtLogin() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.AGT_LOGIN_KEY);
  }

  public String couCommentaire() {
    return (String) storedValueForKey(_Courrier.COU_COMMENTAIRE_KEY);
  }

  public void setCouCommentaire(String value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couCommentaire from " + couCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_COMMENTAIRE_KEY);
  }

  public NSTimestamp couDateCourrier() {
    return (NSTimestamp) storedValueForKey(_Courrier.COU_DATE_COURRIER_KEY);
  }

  public void setCouDateCourrier(NSTimestamp value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couDateCourrier from " + couDateCourrier() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_DATE_COURRIER_KEY);
  }

  public NSTimestamp couDateEnreg() {
    return (NSTimestamp) storedValueForKey(_Courrier.COU_DATE_ENREG_KEY);
  }

  public void setCouDateEnreg(NSTimestamp value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couDateEnreg from " + couDateEnreg() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_DATE_ENREG_KEY);
  }

  public NSTimestamp couDateReponseAttendue() {
    return (NSTimestamp) storedValueForKey(_Courrier.COU_DATE_REPONSE_ATTENDUE_KEY);
  }

  public void setCouDateReponseAttendue(NSTimestamp value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couDateReponseAttendue from " + couDateReponseAttendue() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_DATE_REPONSE_ATTENDUE_KEY);
  }

  public NSTimestamp couDateReponseEffectuee() {
    return (NSTimestamp) storedValueForKey(_Courrier.COU_DATE_REPONSE_EFFECTUEE_KEY);
  }

  public void setCouDateReponseEffectuee(NSTimestamp value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couDateReponseEffectuee from " + couDateReponseEffectuee() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_DATE_REPONSE_EFFECTUEE_KEY);
  }

  public String couDepartArrivee() {
    return (String) storedValueForKey(_Courrier.COU_DEPART_ARRIVEE_KEY);
  }

  public void setCouDepartArrivee(String value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couDepartArrivee from " + couDepartArrivee() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_DEPART_ARRIVEE_KEY);
  }

  public String couExpediteur() {
    return (String) storedValueForKey(_Courrier.COU_EXPEDITEUR_KEY);
  }

  public void setCouExpediteur(String value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couExpediteur from " + couExpediteur() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_EXPEDITEUR_KEY);
  }

  public String couExpEtablissement() {
    return (String) storedValueForKey(_Courrier.COU_EXP_ETABLISSEMENT_KEY);
  }

  public void setCouExpEtablissement(String value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couExpEtablissement from " + couExpEtablissement() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_EXP_ETABLISSEMENT_KEY);
  }

  public String couExpVia() {
    return (String) storedValueForKey(_Courrier.COU_EXP_VIA_KEY);
  }

  public void setCouExpVia(String value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couExpVia from " + couExpVia() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_EXP_VIA_KEY);
  }

  public Integer couIdent() {
    return (Integer) storedValueForKey(_Courrier.COU_IDENT_KEY);
  }

  public void setCouIdent(Integer value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couIdent from " + couIdent() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_IDENT_KEY);
  }

  public String couLocalisation() {
    return (String) storedValueForKey(_Courrier.COU_LOCALISATION_KEY);
  }

  public void setCouLocalisation(String value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couLocalisation from " + couLocalisation() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_LOCALISATION_KEY);
  }

  public String couMotsClefs() {
    return (String) storedValueForKey(_Courrier.COU_MOTS_CLEFS_KEY);
  }

  public void setCouMotsClefs(String value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couMotsClefs from " + couMotsClefs() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_MOTS_CLEFS_KEY);
  }

  public Integer couNombreRelance() {
    return (Integer) storedValueForKey(_Courrier.COU_NOMBRE_RELANCE_KEY);
  }

  public void setCouNombreRelance(Integer value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couNombreRelance from " + couNombreRelance() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_NOMBRE_RELANCE_KEY);
  }

  public Integer couNumero() {
    return (Integer) storedValueForKey(_Courrier.COU_NUMERO_KEY);
  }

  public void setCouNumero(Integer value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couNumero from " + couNumero() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_NUMERO_KEY);
  }

  public Integer couNumeroPere() {
    return (Integer) storedValueForKey(_Courrier.COU_NUMERO_PERE_KEY);
  }

  public void setCouNumeroPere(Integer value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couNumeroPere from " + couNumeroPere() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_NUMERO_PERE_KEY);
  }

  public String couObjet() {
    return (String) storedValueForKey(_Courrier.COU_OBJET_KEY);
  }

  public void setCouObjet(String value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couObjet from " + couObjet() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_OBJET_KEY);
  }

  public String couReference() {
    return (String) storedValueForKey(_Courrier.COU_REFERENCE_KEY);
  }

  public void setCouReference(String value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couReference from " + couReference() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_REFERENCE_KEY);
  }

  public Integer couTypeConsultation() {
    return (Integer) storedValueForKey(_Courrier.COU_TYPE_CONSULTATION_KEY);
  }

  public void setCouTypeConsultation(Integer value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating couTypeConsultation from " + couTypeConsultation() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.COU_TYPE_CONSULTATION_KEY);
  }

  public String cStructure() {
    return (String) storedValueForKey(_Courrier.C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.C_STRUCTURE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_Courrier.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_Courrier.LOG.isDebugEnabled()) {
    	_Courrier.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _Courrier.PERS_ID_KEY);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire> gB_Destinataires() {
    return (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire>)storedValueForKey(_Courrier.G_B__DESTINATAIRES_KEY);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire> gB_Destinataires(EOQualifier qualifier) {
    return gB_Destinataires(qualifier, null);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire> gB_Destinataires(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire> results;
      results = gB_Destinataires();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToGB_Destinataires(org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire object) {
    includeObjectIntoPropertyWithKey(object, _Courrier.G_B__DESTINATAIRES_KEY);
  }

  public void removeFromGB_Destinataires(org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire object) {
    excludeObjectFromPropertyWithKey(object, _Courrier.G_B__DESTINATAIRES_KEY);
  }

  public void addToGB_DestinatairesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire object) {
    if (_Courrier.LOG.isDebugEnabled()) {
      _Courrier.LOG.debug("adding " + object + " to gB_Destinataires relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToGB_Destinataires(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _Courrier.G_B__DESTINATAIRES_KEY);
    }
  }

  public void removeFromGB_DestinatairesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire object) {
    if (_Courrier.LOG.isDebugEnabled()) {
      _Courrier.LOG.debug("removing " + object + " from gB_Destinataires relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromGB_Destinataires(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _Courrier.G_B__DESTINATAIRES_KEY);
    }
  }

  public org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire createGB_DestinatairesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _Courrier.G_B__DESTINATAIRES_KEY);
    return (org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire) eo;
  }

  public void deleteGB_DestinatairesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _Courrier.G_B__DESTINATAIRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllGB_DestinatairesRelationships() {
    Enumeration<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire> objects = gB_Destinataires().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteGB_DestinatairesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur> gB_RepartCourAuteurs() {
    return (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur>)storedValueForKey(_Courrier.G_B__REPART_COUR_AUTEURS_KEY);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur> gB_RepartCourAuteurs(EOQualifier qualifier) {
    return gB_RepartCourAuteurs(qualifier, null);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur> gB_RepartCourAuteurs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur> results;
      results = gB_RepartCourAuteurs();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToGB_RepartCourAuteurs(org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur object) {
    includeObjectIntoPropertyWithKey(object, _Courrier.G_B__REPART_COUR_AUTEURS_KEY);
  }

  public void removeFromGB_RepartCourAuteurs(org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur object) {
    excludeObjectFromPropertyWithKey(object, _Courrier.G_B__REPART_COUR_AUTEURS_KEY);
  }

  public void addToGB_RepartCourAuteursRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur object) {
    if (_Courrier.LOG.isDebugEnabled()) {
      _Courrier.LOG.debug("adding " + object + " to gB_RepartCourAuteurs relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToGB_RepartCourAuteurs(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _Courrier.G_B__REPART_COUR_AUTEURS_KEY);
    }
  }

  public void removeFromGB_RepartCourAuteursRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur object) {
    if (_Courrier.LOG.isDebugEnabled()) {
      _Courrier.LOG.debug("removing " + object + " from gB_RepartCourAuteurs relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromGB_RepartCourAuteurs(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _Courrier.G_B__REPART_COUR_AUTEURS_KEY);
    }
  }

  public org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur createGB_RepartCourAuteursRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _Courrier.G_B__REPART_COUR_AUTEURS_KEY);
    return (org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur) eo;
  }

  public void deleteGB_RepartCourAuteursRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _Courrier.G_B__REPART_COUR_AUTEURS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllGB_RepartCourAuteursRelationships() {
    Enumeration<org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur> objects = gB_RepartCourAuteurs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteGB_RepartCourAuteursRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue> gB_TexteMultilingues() {
    return (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue>)storedValueForKey(_Courrier.G_B__TEXTE_MULTILINGUES_KEY);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue> gB_TexteMultilingues(EOQualifier qualifier) {
    return gB_TexteMultilingues(qualifier, null);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue> gB_TexteMultilingues(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue> results;
      results = gB_TexteMultilingues();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToGB_TexteMultilingues(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue object) {
    includeObjectIntoPropertyWithKey(object, _Courrier.G_B__TEXTE_MULTILINGUES_KEY);
  }

  public void removeFromGB_TexteMultilingues(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue object) {
    excludeObjectFromPropertyWithKey(object, _Courrier.G_B__TEXTE_MULTILINGUES_KEY);
  }

  public void addToGB_TexteMultilinguesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue object) {
    if (_Courrier.LOG.isDebugEnabled()) {
      _Courrier.LOG.debug("adding " + object + " to gB_TexteMultilingues relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToGB_TexteMultilingues(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _Courrier.G_B__TEXTE_MULTILINGUES_KEY);
    }
  }

  public void removeFromGB_TexteMultilinguesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue object) {
    if (_Courrier.LOG.isDebugEnabled()) {
      _Courrier.LOG.debug("removing " + object + " from gB_TexteMultilingues relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromGB_TexteMultilingues(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _Courrier.G_B__TEXTE_MULTILINGUES_KEY);
    }
  }

  public org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue createGB_TexteMultilinguesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _Courrier.G_B__TEXTE_MULTILINGUES_KEY);
    return (org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue) eo;
  }

  public void deleteGB_TexteMultilinguesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _Courrier.G_B__TEXTE_MULTILINGUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllGB_TexteMultilinguesRelationships() {
    Enumeration<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue> objects = gB_TexteMultilingues().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteGB_TexteMultilinguesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien> referenceLiens() {
    return (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien>)storedValueForKey(_Courrier.REFERENCE_LIENS_KEY);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien> referenceLiens(EOQualifier qualifier) {
    return referenceLiens(qualifier, null);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien> referenceLiens(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien> results;
      results = referenceLiens();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToReferenceLiens(org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien object) {
    includeObjectIntoPropertyWithKey(object, _Courrier.REFERENCE_LIENS_KEY);
  }

  public void removeFromReferenceLiens(org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien object) {
    excludeObjectFromPropertyWithKey(object, _Courrier.REFERENCE_LIENS_KEY);
  }

  public void addToReferenceLiensRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien object) {
    if (_Courrier.LOG.isDebugEnabled()) {
      _Courrier.LOG.debug("adding " + object + " to referenceLiens relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToReferenceLiens(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _Courrier.REFERENCE_LIENS_KEY);
    }
  }

  public void removeFromReferenceLiensRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien object) {
    if (_Courrier.LOG.isDebugEnabled()) {
      _Courrier.LOG.debug("removing " + object + " from referenceLiens relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromReferenceLiens(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _Courrier.REFERENCE_LIENS_KEY);
    }
  }

  public org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien createReferenceLiensRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _Courrier.REFERENCE_LIENS_KEY);
    return (org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien) eo;
  }

  public void deleteReferenceLiensRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _Courrier.REFERENCE_LIENS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReferenceLiensRelationships() {
    Enumeration<org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien> objects = referenceLiens().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReferenceLiensRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb> specifWeb() {
    return (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb>)storedValueForKey(_Courrier.SPECIF_WEB_KEY);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb> specifWeb(EOQualifier qualifier) {
    return specifWeb(qualifier, null);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb> specifWeb(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb> results;
      results = specifWeb();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToSpecifWeb(org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb object) {
    includeObjectIntoPropertyWithKey(object, _Courrier.SPECIF_WEB_KEY);
  }

  public void removeFromSpecifWeb(org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb object) {
    excludeObjectFromPropertyWithKey(object, _Courrier.SPECIF_WEB_KEY);
  }

  public void addToSpecifWebRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb object) {
    if (_Courrier.LOG.isDebugEnabled()) {
      _Courrier.LOG.debug("adding " + object + " to specifWeb relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToSpecifWeb(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _Courrier.SPECIF_WEB_KEY);
    }
  }

  public void removeFromSpecifWebRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb object) {
    if (_Courrier.LOG.isDebugEnabled()) {
      _Courrier.LOG.debug("removing " + object + " from specifWeb relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromSpecifWeb(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _Courrier.SPECIF_WEB_KEY);
    }
  }

  public org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb createSpecifWebRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _Courrier.SPECIF_WEB_KEY);
    return (org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb) eo;
  }

  public void deleteSpecifWebRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _Courrier.SPECIF_WEB_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSpecifWebRelationships() {
    Enumeration<org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb> objects = specifWeb().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSpecifWebRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies> typesAssocies() {
    return (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies>)storedValueForKey(_Courrier.TYPES_ASSOCIES_KEY);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies> typesAssocies(EOQualifier qualifier) {
    return typesAssocies(qualifier, null, false);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies> typesAssocies(EOQualifier qualifier, boolean fetch) {
    return typesAssocies(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies> typesAssocies(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies.COURRIER_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies.fetchGB_TypesAssocieses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = typesAssocies();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTypesAssocies(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies object) {
    includeObjectIntoPropertyWithKey(object, _Courrier.TYPES_ASSOCIES_KEY);
  }

  public void removeFromTypesAssocies(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies object) {
    excludeObjectFromPropertyWithKey(object, _Courrier.TYPES_ASSOCIES_KEY);
  }

  public void addToTypesAssociesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies object) {
    if (_Courrier.LOG.isDebugEnabled()) {
      _Courrier.LOG.debug("adding " + object + " to typesAssocies relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToTypesAssocies(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _Courrier.TYPES_ASSOCIES_KEY);
    }
  }

  public void removeFromTypesAssociesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies object) {
    if (_Courrier.LOG.isDebugEnabled()) {
      _Courrier.LOG.debug("removing " + object + " from typesAssocies relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromTypesAssocies(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _Courrier.TYPES_ASSOCIES_KEY);
    }
  }

  public org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies createTypesAssociesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _Courrier.TYPES_ASSOCIES_KEY);
    return (org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies) eo;
  }

  public void deleteTypesAssociesRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _Courrier.TYPES_ASSOCIES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTypesAssociesRelationships() {
    Enumeration<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies> objects = typesAssocies().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTypesAssociesRelationship(objects.nextElement());
    }
  }


  public static Courrier createGB_Courrier(EOEditingContext editingContext, String agtLogin
, NSTimestamp couDateCourrier
, NSTimestamp couDateEnreg
, String couDepartArrivee
, String couExpediteur
, Integer couIdent
, Integer couNumero
, String couObjet
) {
    Courrier eo = (Courrier) EOUtilities.createAndInsertInstance(editingContext, _Courrier.ENTITY_NAME);    
		eo.setAgtLogin(agtLogin);
		eo.setCouDateCourrier(couDateCourrier);
		eo.setCouDateEnreg(couDateEnreg);
		eo.setCouDepartArrivee(couDepartArrivee);
		eo.setCouExpediteur(couExpediteur);
		eo.setCouIdent(couIdent);
		eo.setCouNumero(couNumero);
		eo.setCouObjet(couObjet);
    return eo;
  }

  public static ERXFetchSpecification<Courrier> fetchSpec() {
    return new ERXFetchSpecification<Courrier>(_Courrier.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Courrier> fetchAllGB_Courriers(EOEditingContext editingContext) {
    return _Courrier.fetchAllGB_Courriers(editingContext, null);
  }

  public static NSArray<Courrier> fetchAllGB_Courriers(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _Courrier.fetchGB_Courriers(editingContext, null, sortOrderings);
  }

  public static NSArray<Courrier> fetchGB_Courriers(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Courrier> fetchSpec = new ERXFetchSpecification<Courrier>(_Courrier.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Courrier> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Courrier fetchGB_Courrier(EOEditingContext editingContext, String keyName, Object value) {
    return _Courrier.fetchGB_Courrier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Courrier fetchGB_Courrier(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Courrier> eoObjects = _Courrier.fetchGB_Courriers(editingContext, qualifier, null);
    Courrier eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_Courrier that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Courrier fetchRequiredGB_Courrier(EOEditingContext editingContext, String keyName, Object value) {
    return _Courrier.fetchRequiredGB_Courrier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Courrier fetchRequiredGB_Courrier(EOEditingContext editingContext, EOQualifier qualifier) {
    Courrier eoObject = _Courrier.fetchGB_Courrier(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_Courrier that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Courrier localInstanceIn(EOEditingContext editingContext, Courrier eo) {
    Courrier localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
