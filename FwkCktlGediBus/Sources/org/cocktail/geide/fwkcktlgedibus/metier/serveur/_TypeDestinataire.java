// DO NOT EDIT.  Make changes to TypeDestinataire.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _TypeDestinataire extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_TypeDestinataire";

  // Attribute Keys
  public static final ERXKey<String> TYPE_DEST_LIBELLE = new ERXKey<String>("typeDestLibelle");
  public static final ERXKey<String> TYPE_DEST_TYPE = new ERXKey<String>("typeDestType");
  // Relationship Keys

  // Attributes
  public static final String TYPE_DEST_LIBELLE_KEY = TYPE_DEST_LIBELLE.key();
  public static final String TYPE_DEST_TYPE_KEY = TYPE_DEST_TYPE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_TypeDestinataire.class);

  public TypeDestinataire localInstanceIn(EOEditingContext editingContext) {
    TypeDestinataire localInstance = (TypeDestinataire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String typeDestLibelle() {
    return (String) storedValueForKey(_TypeDestinataire.TYPE_DEST_LIBELLE_KEY);
  }

  public void setTypeDestLibelle(String value) {
    if (_TypeDestinataire.LOG.isDebugEnabled()) {
    	_TypeDestinataire.LOG.debug( "updating typeDestLibelle from " + typeDestLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeDestinataire.TYPE_DEST_LIBELLE_KEY);
  }

  public String typeDestType() {
    return (String) storedValueForKey(_TypeDestinataire.TYPE_DEST_TYPE_KEY);
  }

  public void setTypeDestType(String value) {
    if (_TypeDestinataire.LOG.isDebugEnabled()) {
    	_TypeDestinataire.LOG.debug( "updating typeDestType from " + typeDestType() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeDestinataire.TYPE_DEST_TYPE_KEY);
  }


  public static TypeDestinataire createGB_TypeDestinataire(EOEditingContext editingContext) {
    TypeDestinataire eo = (TypeDestinataire) EOUtilities.createAndInsertInstance(editingContext, _TypeDestinataire.ENTITY_NAME);    
    return eo;
  }

  public static ERXFetchSpecification<TypeDestinataire> fetchSpec() {
    return new ERXFetchSpecification<TypeDestinataire>(_TypeDestinataire.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypeDestinataire> fetchAllGB_TypeDestinataires(EOEditingContext editingContext) {
    return _TypeDestinataire.fetchAllGB_TypeDestinataires(editingContext, null);
  }

  public static NSArray<TypeDestinataire> fetchAllGB_TypeDestinataires(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _TypeDestinataire.fetchGB_TypeDestinataires(editingContext, null, sortOrderings);
  }

  public static NSArray<TypeDestinataire> fetchGB_TypeDestinataires(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypeDestinataire> fetchSpec = new ERXFetchSpecification<TypeDestinataire>(_TypeDestinataire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypeDestinataire> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypeDestinataire fetchGB_TypeDestinataire(EOEditingContext editingContext, String keyName, Object value) {
    return _TypeDestinataire.fetchGB_TypeDestinataire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeDestinataire fetchGB_TypeDestinataire(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypeDestinataire> eoObjects = _TypeDestinataire.fetchGB_TypeDestinataires(editingContext, qualifier, null);
    TypeDestinataire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_TypeDestinataire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeDestinataire fetchRequiredGB_TypeDestinataire(EOEditingContext editingContext, String keyName, Object value) {
    return _TypeDestinataire.fetchRequiredGB_TypeDestinataire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeDestinataire fetchRequiredGB_TypeDestinataire(EOEditingContext editingContext, EOQualifier qualifier) {
    TypeDestinataire eoObject = _TypeDestinataire.fetchGB_TypeDestinataire(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_TypeDestinataire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeDestinataire localInstanceIn(EOEditingContext editingContext, TypeDestinataire eo) {
    TypeDestinataire localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
