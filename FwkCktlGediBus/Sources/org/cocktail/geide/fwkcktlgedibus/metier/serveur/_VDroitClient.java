// DO NOT EDIT.  Make changes to VDroitClient.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _VDroitClient extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "VDroitClient";

  // Attribute Keys
  public static final ERXKey<String> AGT_TYPE = new ERXKey<String>("agtType");
  public static final ERXKey<String> CLIENT_TYPE = new ERXKey<String>("clientType");
  public static final ERXKey<String> D_LEC = new ERXKey<String>("dLec");
  public static final ERXKey<String> D_MOD = new ERXKey<String>("dMod");
  public static final ERXKey<String> D_SUP = new ERXKey<String>("dSup");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  public static final ERXKey<Integer> PERS_ID_CLIENT = new ERXKey<Integer>("persIdClient");
  public static final ERXKey<String> TYPE_ORDRE_CHAR = new ERXKey<String>("typeOrdreChar");
  // Relationship Keys

  // Attributes
  public static final String AGT_TYPE_KEY = AGT_TYPE.key();
  public static final String CLIENT_TYPE_KEY = CLIENT_TYPE.key();
  public static final String D_LEC_KEY = D_LEC.key();
  public static final String D_MOD_KEY = D_MOD.key();
  public static final String D_SUP_KEY = D_SUP.key();
  public static final String PERS_ID_KEY = PERS_ID.key();
  public static final String PERS_ID_CLIENT_KEY = PERS_ID_CLIENT.key();
  public static final String TYPE_ORDRE_CHAR_KEY = TYPE_ORDRE_CHAR.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_VDroitClient.class);

  public VDroitClient localInstanceIn(EOEditingContext editingContext) {
    VDroitClient localInstance = (VDroitClient)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String agtType() {
    return (String) storedValueForKey(_VDroitClient.AGT_TYPE_KEY);
  }

  public void setAgtType(String value) {
    if (_VDroitClient.LOG.isDebugEnabled()) {
    	_VDroitClient.LOG.debug( "updating agtType from " + agtType() + " to " + value);
    }
    takeStoredValueForKey(value, _VDroitClient.AGT_TYPE_KEY);
  }

  public String clientType() {
    return (String) storedValueForKey(_VDroitClient.CLIENT_TYPE_KEY);
  }

  public void setClientType(String value) {
    if (_VDroitClient.LOG.isDebugEnabled()) {
    	_VDroitClient.LOG.debug( "updating clientType from " + clientType() + " to " + value);
    }
    takeStoredValueForKey(value, _VDroitClient.CLIENT_TYPE_KEY);
  }

  public String dLec() {
    return (String) storedValueForKey(_VDroitClient.D_LEC_KEY);
  }

  public void setDLec(String value) {
    if (_VDroitClient.LOG.isDebugEnabled()) {
    	_VDroitClient.LOG.debug( "updating dLec from " + dLec() + " to " + value);
    }
    takeStoredValueForKey(value, _VDroitClient.D_LEC_KEY);
  }

  public String dMod() {
    return (String) storedValueForKey(_VDroitClient.D_MOD_KEY);
  }

  public void setDMod(String value) {
    if (_VDroitClient.LOG.isDebugEnabled()) {
    	_VDroitClient.LOG.debug( "updating dMod from " + dMod() + " to " + value);
    }
    takeStoredValueForKey(value, _VDroitClient.D_MOD_KEY);
  }

  public String dSup() {
    return (String) storedValueForKey(_VDroitClient.D_SUP_KEY);
  }

  public void setDSup(String value) {
    if (_VDroitClient.LOG.isDebugEnabled()) {
    	_VDroitClient.LOG.debug( "updating dSup from " + dSup() + " to " + value);
    }
    takeStoredValueForKey(value, _VDroitClient.D_SUP_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_VDroitClient.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_VDroitClient.LOG.isDebugEnabled()) {
    	_VDroitClient.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _VDroitClient.PERS_ID_KEY);
  }

  public Integer persIdClient() {
    return (Integer) storedValueForKey(_VDroitClient.PERS_ID_CLIENT_KEY);
  }

  public void setPersIdClient(Integer value) {
    if (_VDroitClient.LOG.isDebugEnabled()) {
    	_VDroitClient.LOG.debug( "updating persIdClient from " + persIdClient() + " to " + value);
    }
    takeStoredValueForKey(value, _VDroitClient.PERS_ID_CLIENT_KEY);
  }

  public String typeOrdreChar() {
    return (String) storedValueForKey(_VDroitClient.TYPE_ORDRE_CHAR_KEY);
  }

  public void setTypeOrdreChar(String value) {
    if (_VDroitClient.LOG.isDebugEnabled()) {
    	_VDroitClient.LOG.debug( "updating typeOrdreChar from " + typeOrdreChar() + " to " + value);
    }
    takeStoredValueForKey(value, _VDroitClient.TYPE_ORDRE_CHAR_KEY);
  }


  public static VDroitClient createVDroitClient(EOEditingContext editingContext, Integer persIdClient
, String typeOrdreChar
) {
    VDroitClient eo = (VDroitClient) EOUtilities.createAndInsertInstance(editingContext, _VDroitClient.ENTITY_NAME);    
		eo.setPersIdClient(persIdClient);
		eo.setTypeOrdreChar(typeOrdreChar);
    return eo;
  }

  public static ERXFetchSpecification<VDroitClient> fetchSpec() {
    return new ERXFetchSpecification<VDroitClient>(_VDroitClient.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<VDroitClient> fetchAllVDroitClients(EOEditingContext editingContext) {
    return _VDroitClient.fetchAllVDroitClients(editingContext, null);
  }

  public static NSArray<VDroitClient> fetchAllVDroitClients(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _VDroitClient.fetchVDroitClients(editingContext, null, sortOrderings);
  }

  public static NSArray<VDroitClient> fetchVDroitClients(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<VDroitClient> fetchSpec = new ERXFetchSpecification<VDroitClient>(_VDroitClient.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<VDroitClient> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static VDroitClient fetchVDroitClient(EOEditingContext editingContext, String keyName, Object value) {
    return _VDroitClient.fetchVDroitClient(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VDroitClient fetchVDroitClient(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<VDroitClient> eoObjects = _VDroitClient.fetchVDroitClients(editingContext, qualifier, null);
    VDroitClient eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one VDroitClient that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VDroitClient fetchRequiredVDroitClient(EOEditingContext editingContext, String keyName, Object value) {
    return _VDroitClient.fetchRequiredVDroitClient(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VDroitClient fetchRequiredVDroitClient(EOEditingContext editingContext, EOQualifier qualifier) {
    VDroitClient eoObject = _VDroitClient.fetchVDroitClient(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no VDroitClient that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VDroitClient localInstanceIn(EOEditingContext editingContext, VDroitClient eo) {
    VDroitClient localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
