// DO NOT EDIT.  Make changes to Agents.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _Agents extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_Agents";

  // Attribute Keys
  public static final ERXKey<Integer> AGT_SEQ = new ERXKey<Integer>("agtSeq");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  // Relationship Keys

  // Attributes
  public static final String AGT_SEQ_KEY = AGT_SEQ.key();
  public static final String PERS_ID_KEY = PERS_ID.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_Agents.class);

  public Agents localInstanceIn(EOEditingContext editingContext) {
    Agents localInstance = (Agents)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer agtSeq() {
    return (Integer) storedValueForKey(_Agents.AGT_SEQ_KEY);
  }

  public void setAgtSeq(Integer value) {
    if (_Agents.LOG.isDebugEnabled()) {
    	_Agents.LOG.debug( "updating agtSeq from " + agtSeq() + " to " + value);
    }
    takeStoredValueForKey(value, _Agents.AGT_SEQ_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_Agents.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_Agents.LOG.isDebugEnabled()) {
    	_Agents.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _Agents.PERS_ID_KEY);
  }


  public static Agents createGB_Agents(EOEditingContext editingContext, Integer persId
) {
    Agents eo = (Agents) EOUtilities.createAndInsertInstance(editingContext, _Agents.ENTITY_NAME);    
		eo.setPersId(persId);
    return eo;
  }

  public static ERXFetchSpecification<Agents> fetchSpec() {
    return new ERXFetchSpecification<Agents>(_Agents.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Agents> fetchAllGB_Agentses(EOEditingContext editingContext) {
    return _Agents.fetchAllGB_Agentses(editingContext, null);
  }

  public static NSArray<Agents> fetchAllGB_Agentses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _Agents.fetchGB_Agentses(editingContext, null, sortOrderings);
  }

  public static NSArray<Agents> fetchGB_Agentses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Agents> fetchSpec = new ERXFetchSpecification<Agents>(_Agents.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Agents> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Agents fetchGB_Agents(EOEditingContext editingContext, String keyName, Object value) {
    return _Agents.fetchGB_Agents(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Agents fetchGB_Agents(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Agents> eoObjects = _Agents.fetchGB_Agentses(editingContext, qualifier, null);
    Agents eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_Agents that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Agents fetchRequiredGB_Agents(EOEditingContext editingContext, String keyName, Object value) {
    return _Agents.fetchRequiredGB_Agents(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Agents fetchRequiredGB_Agents(EOEditingContext editingContext, EOQualifier qualifier) {
    Agents eoObject = _Agents.fetchGB_Agents(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_Agents that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Agents localInstanceIn(EOEditingContext editingContext, Agents eo) {
    Agents localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
