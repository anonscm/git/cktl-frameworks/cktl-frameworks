

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.geide.fwkcktlgedibus.metier.serveur;


import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class DroitTypeClient extends _DroitTypeClient
{
    public DroitTypeClient() {
        super();
    }
    
    public static NSArray<DroitTypeClient> droitsTypeClient(IPersonne personne) {
        return fetchGB_DroitTypeClients(personne.editingContext(), PERS_ID.eq(personne.persId()), null);
    }
    
	/**
	 * Trnsforme le droit en entier selon dLec, dMod et dSup
	 * @param lecture = "O" ou "N"
	 * @param modification = "O" ou "N"
	 * @param suppression = "O" ou "N"
	 * @return
	 * 	l'entier dont l'hexa est XYZ tel que :
	 * 		Z = 1 si dLec() == "O" sinon 0
	 * 		Y = 1 si dMod() == "O" sinon 0
	 * 		X = 1 si dSup() == "O" sinon 0
	 * 
	 */
	public int droitInt() {
		int i = 0;
		
		if ( (dLec() != null) && (dLec().equals("O")) )
			i = 1;		// droit de lecture
		if ( (dMod() != null) && (dMod().equals("O")) )
			i += 2;		// droit de Modif
		if ( (dSup() != null) && (dSup().equals("O")) )
			i += 4;		// droit de Modif
    	
    	return i;
	}
	
	/**
	 * Transformation du to-many  typeCourrierNews() en to-one
	 * En effet, la relation est to-many car la cle de GB_TypeCourrierNew n'est plus typeOrdreChar.
	 * @return
	 */
	public org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew typeCourrierNew() {
		org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew type = null;
		NSArray array = typeCourrierNews();
		if( (array != null) && (array.count() > 0) )
			type = (org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew)array.objectAtIndex(0);
		return type;
	}
	
	

/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/



    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
    
}
