// DO NOT EDIT.  Make changes to ProprietaireType.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _ProprietaireType extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_ProprietaireType";

  // Attribute Keys
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  public static final ERXKey<String> TYPE_ORDRE_CHAR = new ERXKey<String>("typeOrdreChar");
  // Relationship Keys

  // Attributes
  public static final String PERS_ID_KEY = PERS_ID.key();
  public static final String TYPE_ORDRE_CHAR_KEY = TYPE_ORDRE_CHAR.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_ProprietaireType.class);

  public ProprietaireType localInstanceIn(EOEditingContext editingContext) {
    ProprietaireType localInstance = (ProprietaireType)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_ProprietaireType.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_ProprietaireType.LOG.isDebugEnabled()) {
    	_ProprietaireType.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _ProprietaireType.PERS_ID_KEY);
  }

  public String typeOrdreChar() {
    return (String) storedValueForKey(_ProprietaireType.TYPE_ORDRE_CHAR_KEY);
  }

  public void setTypeOrdreChar(String value) {
    if (_ProprietaireType.LOG.isDebugEnabled()) {
    	_ProprietaireType.LOG.debug( "updating typeOrdreChar from " + typeOrdreChar() + " to " + value);
    }
    takeStoredValueForKey(value, _ProprietaireType.TYPE_ORDRE_CHAR_KEY);
  }


  public static ProprietaireType createGB_ProprietaireType(EOEditingContext editingContext, Integer persId
, String typeOrdreChar
) {
    ProprietaireType eo = (ProprietaireType) EOUtilities.createAndInsertInstance(editingContext, _ProprietaireType.ENTITY_NAME);    
		eo.setPersId(persId);
		eo.setTypeOrdreChar(typeOrdreChar);
    return eo;
  }

  public static ERXFetchSpecification<ProprietaireType> fetchSpec() {
    return new ERXFetchSpecification<ProprietaireType>(_ProprietaireType.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<ProprietaireType> fetchAllGB_ProprietaireTypes(EOEditingContext editingContext) {
    return _ProprietaireType.fetchAllGB_ProprietaireTypes(editingContext, null);
  }

  public static NSArray<ProprietaireType> fetchAllGB_ProprietaireTypes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _ProprietaireType.fetchGB_ProprietaireTypes(editingContext, null, sortOrderings);
  }

  public static NSArray<ProprietaireType> fetchGB_ProprietaireTypes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<ProprietaireType> fetchSpec = new ERXFetchSpecification<ProprietaireType>(_ProprietaireType.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<ProprietaireType> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static ProprietaireType fetchGB_ProprietaireType(EOEditingContext editingContext, String keyName, Object value) {
    return _ProprietaireType.fetchGB_ProprietaireType(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ProprietaireType fetchGB_ProprietaireType(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<ProprietaireType> eoObjects = _ProprietaireType.fetchGB_ProprietaireTypes(editingContext, qualifier, null);
    ProprietaireType eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_ProprietaireType that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ProprietaireType fetchRequiredGB_ProprietaireType(EOEditingContext editingContext, String keyName, Object value) {
    return _ProprietaireType.fetchRequiredGB_ProprietaireType(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ProprietaireType fetchRequiredGB_ProprietaireType(EOEditingContext editingContext, EOQualifier qualifier) {
    ProprietaireType eoObject = _ProprietaireType.fetchGB_ProprietaireType(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_ProprietaireType that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ProprietaireType localInstanceIn(EOEditingContext editingContext, ProprietaireType eo) {
    ProprietaireType localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
