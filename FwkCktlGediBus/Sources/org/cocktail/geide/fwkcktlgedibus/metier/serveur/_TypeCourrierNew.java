// DO NOT EDIT.  Make changes to TypeCourrierNew.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _TypeCourrierNew extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_TypeCourrierNew";

  // Attribute Keys
  public static final ERXKey<Integer> CLE_DATES = new ERXKey<Integer>("cleDates");
  public static final ERXKey<Integer> REF_ORDRE = new ERXKey<Integer>("refOrdre");
  public static final ERXKey<String> TYPE_CODE = new ERXKey<String>("typeCode");
  public static final ERXKey<Integer> TYPE_DOCUMENT_ID = new ERXKey<Integer>("typeDocumentId");
  public static final ERXKey<String> TYPE_FAMILLE = new ERXKey<String>("typeFamille");
  public static final ERXKey<String> TYPE_LIBELLE = new ERXKey<String>("typeLibelle");
  public static final ERXKey<Integer> TYPE_ORDRE = new ERXKey<Integer>("typeOrdre");
  public static final ERXKey<String> TYPE_ORDRE_CHAR = new ERXKey<String>("typeOrdreChar");
  public static final ERXKey<String> TYPE_PAGE_TITRE = new ERXKey<String>("typePageTitre");
  public static final ERXKey<String> TYPE_PAGE_URL = new ERXKey<String>("typePageUrl");
  public static final ERXKey<Integer> TYPE_TYPE_CONSULTATION = new ERXKey<Integer>("typeTypeConsultation");
  public static final ERXKey<String> TYPE_VISIBLE_APPLI = new ERXKey<String>("typeVisibleAppli");
  // Relationship Keys

  // Attributes
  public static final String CLE_DATES_KEY = CLE_DATES.key();
  public static final String REF_ORDRE_KEY = REF_ORDRE.key();
  public static final String TYPE_CODE_KEY = TYPE_CODE.key();
  public static final String TYPE_DOCUMENT_ID_KEY = TYPE_DOCUMENT_ID.key();
  public static final String TYPE_FAMILLE_KEY = TYPE_FAMILLE.key();
  public static final String TYPE_LIBELLE_KEY = TYPE_LIBELLE.key();
  public static final String TYPE_ORDRE_KEY = TYPE_ORDRE.key();
  public static final String TYPE_ORDRE_CHAR_KEY = TYPE_ORDRE_CHAR.key();
  public static final String TYPE_PAGE_TITRE_KEY = TYPE_PAGE_TITRE.key();
  public static final String TYPE_PAGE_URL_KEY = TYPE_PAGE_URL.key();
  public static final String TYPE_TYPE_CONSULTATION_KEY = TYPE_TYPE_CONSULTATION.key();
  public static final String TYPE_VISIBLE_APPLI_KEY = TYPE_VISIBLE_APPLI.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_TypeCourrierNew.class);

  public TypeCourrierNew localInstanceIn(EOEditingContext editingContext) {
    TypeCourrierNew localInstance = (TypeCourrierNew)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer cleDates() {
    return (Integer) storedValueForKey(_TypeCourrierNew.CLE_DATES_KEY);
  }

  public void setCleDates(Integer value) {
    if (_TypeCourrierNew.LOG.isDebugEnabled()) {
    	_TypeCourrierNew.LOG.debug( "updating cleDates from " + cleDates() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeCourrierNew.CLE_DATES_KEY);
  }

  public Integer refOrdre() {
    return (Integer) storedValueForKey(_TypeCourrierNew.REF_ORDRE_KEY);
  }

  public void setRefOrdre(Integer value) {
    if (_TypeCourrierNew.LOG.isDebugEnabled()) {
    	_TypeCourrierNew.LOG.debug( "updating refOrdre from " + refOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeCourrierNew.REF_ORDRE_KEY);
  }

  public String typeCode() {
    return (String) storedValueForKey(_TypeCourrierNew.TYPE_CODE_KEY);
  }

  public void setTypeCode(String value) {
    if (_TypeCourrierNew.LOG.isDebugEnabled()) {
    	_TypeCourrierNew.LOG.debug( "updating typeCode from " + typeCode() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeCourrierNew.TYPE_CODE_KEY);
  }

  public Integer typeDocumentId() {
    return (Integer) storedValueForKey(_TypeCourrierNew.TYPE_DOCUMENT_ID_KEY);
  }

  public void setTypeDocumentId(Integer value) {
    if (_TypeCourrierNew.LOG.isDebugEnabled()) {
    	_TypeCourrierNew.LOG.debug( "updating typeDocumentId from " + typeDocumentId() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeCourrierNew.TYPE_DOCUMENT_ID_KEY);
  }

  public String typeFamille() {
    return (String) storedValueForKey(_TypeCourrierNew.TYPE_FAMILLE_KEY);
  }

  public void setTypeFamille(String value) {
    if (_TypeCourrierNew.LOG.isDebugEnabled()) {
    	_TypeCourrierNew.LOG.debug( "updating typeFamille from " + typeFamille() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeCourrierNew.TYPE_FAMILLE_KEY);
  }

  public String typeLibelle() {
    return (String) storedValueForKey(_TypeCourrierNew.TYPE_LIBELLE_KEY);
  }

  public void setTypeLibelle(String value) {
    if (_TypeCourrierNew.LOG.isDebugEnabled()) {
    	_TypeCourrierNew.LOG.debug( "updating typeLibelle from " + typeLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeCourrierNew.TYPE_LIBELLE_KEY);
  }

  public Integer typeOrdre() {
    return (Integer) storedValueForKey(_TypeCourrierNew.TYPE_ORDRE_KEY);
  }

  public void setTypeOrdre(Integer value) {
    if (_TypeCourrierNew.LOG.isDebugEnabled()) {
    	_TypeCourrierNew.LOG.debug( "updating typeOrdre from " + typeOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeCourrierNew.TYPE_ORDRE_KEY);
  }

  public String typeOrdreChar() {
    return (String) storedValueForKey(_TypeCourrierNew.TYPE_ORDRE_CHAR_KEY);
  }

  public void setTypeOrdreChar(String value) {
    if (_TypeCourrierNew.LOG.isDebugEnabled()) {
    	_TypeCourrierNew.LOG.debug( "updating typeOrdreChar from " + typeOrdreChar() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeCourrierNew.TYPE_ORDRE_CHAR_KEY);
  }

  public String typePageTitre() {
    return (String) storedValueForKey(_TypeCourrierNew.TYPE_PAGE_TITRE_KEY);
  }

  public void setTypePageTitre(String value) {
    if (_TypeCourrierNew.LOG.isDebugEnabled()) {
    	_TypeCourrierNew.LOG.debug( "updating typePageTitre from " + typePageTitre() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeCourrierNew.TYPE_PAGE_TITRE_KEY);
  }

  public String typePageUrl() {
    return (String) storedValueForKey(_TypeCourrierNew.TYPE_PAGE_URL_KEY);
  }

  public void setTypePageUrl(String value) {
    if (_TypeCourrierNew.LOG.isDebugEnabled()) {
    	_TypeCourrierNew.LOG.debug( "updating typePageUrl from " + typePageUrl() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeCourrierNew.TYPE_PAGE_URL_KEY);
  }

  public Integer typeTypeConsultation() {
    return (Integer) storedValueForKey(_TypeCourrierNew.TYPE_TYPE_CONSULTATION_KEY);
  }

  public void setTypeTypeConsultation(Integer value) {
    if (_TypeCourrierNew.LOG.isDebugEnabled()) {
    	_TypeCourrierNew.LOG.debug( "updating typeTypeConsultation from " + typeTypeConsultation() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeCourrierNew.TYPE_TYPE_CONSULTATION_KEY);
  }

  public String typeVisibleAppli() {
    return (String) storedValueForKey(_TypeCourrierNew.TYPE_VISIBLE_APPLI_KEY);
  }

  public void setTypeVisibleAppli(String value) {
    if (_TypeCourrierNew.LOG.isDebugEnabled()) {
    	_TypeCourrierNew.LOG.debug( "updating typeVisibleAppli from " + typeVisibleAppli() + " to " + value);
    }
    takeStoredValueForKey(value, _TypeCourrierNew.TYPE_VISIBLE_APPLI_KEY);
  }


  public static TypeCourrierNew createGB_TypeCourrierNew(EOEditingContext editingContext, Integer cleDates
, String typeCode
, String typeFamille
, Integer typeOrdre
, String typeVisibleAppli
) {
    TypeCourrierNew eo = (TypeCourrierNew) EOUtilities.createAndInsertInstance(editingContext, _TypeCourrierNew.ENTITY_NAME);    
		eo.setCleDates(cleDates);
		eo.setTypeCode(typeCode);
		eo.setTypeFamille(typeFamille);
		eo.setTypeOrdre(typeOrdre);
		eo.setTypeVisibleAppli(typeVisibleAppli);
    return eo;
  }

  public static ERXFetchSpecification<TypeCourrierNew> fetchSpec() {
    return new ERXFetchSpecification<TypeCourrierNew>(_TypeCourrierNew.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypeCourrierNew> fetchAllGB_TypeCourrierNews(EOEditingContext editingContext) {
    return _TypeCourrierNew.fetchAllGB_TypeCourrierNews(editingContext, null);
  }

  public static NSArray<TypeCourrierNew> fetchAllGB_TypeCourrierNews(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _TypeCourrierNew.fetchGB_TypeCourrierNews(editingContext, null, sortOrderings);
  }

  public static NSArray<TypeCourrierNew> fetchGB_TypeCourrierNews(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypeCourrierNew> fetchSpec = new ERXFetchSpecification<TypeCourrierNew>(_TypeCourrierNew.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypeCourrierNew> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypeCourrierNew fetchGB_TypeCourrierNew(EOEditingContext editingContext, String keyName, Object value) {
    return _TypeCourrierNew.fetchGB_TypeCourrierNew(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeCourrierNew fetchGB_TypeCourrierNew(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypeCourrierNew> eoObjects = _TypeCourrierNew.fetchGB_TypeCourrierNews(editingContext, qualifier, null);
    TypeCourrierNew eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_TypeCourrierNew that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeCourrierNew fetchRequiredGB_TypeCourrierNew(EOEditingContext editingContext, String keyName, Object value) {
    return _TypeCourrierNew.fetchRequiredGB_TypeCourrierNew(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeCourrierNew fetchRequiredGB_TypeCourrierNew(EOEditingContext editingContext, EOQualifier qualifier) {
    TypeCourrierNew eoObject = _TypeCourrierNew.fetchGB_TypeCourrierNew(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_TypeCourrierNew that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeCourrierNew localInstanceIn(EOEditingContext editingContext, TypeCourrierNew eo) {
    TypeCourrierNew localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
