// DO NOT EDIT.  Make changes to TypesAssocies.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _TypesAssocies extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_TypesAssocies";

  // Attribute Keys
  public static final ERXKey<Integer> COU_NUMERO = new ERXKey<Integer>("couNumero");
  public static final ERXKey<Integer> TYPE_RECORD = new ERXKey<Integer>("typeRecord");
  // Relationship Keys
  public static final ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier> COURRIER = new ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier>("courrier");
  public static final ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew> TYPE_COURRIER_NEW = new ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew>("typeCourrierNew");

  // Attributes
  public static final String COU_NUMERO_KEY = COU_NUMERO.key();
  public static final String TYPE_RECORD_KEY = TYPE_RECORD.key();
  // Relationships
  public static final String COURRIER_KEY = COURRIER.key();
  public static final String TYPE_COURRIER_NEW_KEY = TYPE_COURRIER_NEW.key();

  private static Logger LOG = Logger.getLogger(_TypesAssocies.class);

  public TypesAssocies localInstanceIn(EOEditingContext editingContext) {
    TypesAssocies localInstance = (TypesAssocies)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer couNumero() {
    return (Integer) storedValueForKey(_TypesAssocies.COU_NUMERO_KEY);
  }

  public void setCouNumero(Integer value) {
    if (_TypesAssocies.LOG.isDebugEnabled()) {
    	_TypesAssocies.LOG.debug( "updating couNumero from " + couNumero() + " to " + value);
    }
    takeStoredValueForKey(value, _TypesAssocies.COU_NUMERO_KEY);
  }

  public Integer typeRecord() {
    return (Integer) storedValueForKey(_TypesAssocies.TYPE_RECORD_KEY);
  }

  public void setTypeRecord(Integer value) {
    if (_TypesAssocies.LOG.isDebugEnabled()) {
    	_TypesAssocies.LOG.debug( "updating typeRecord from " + typeRecord() + " to " + value);
    }
    takeStoredValueForKey(value, _TypesAssocies.TYPE_RECORD_KEY);
  }

  public org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier courrier() {
    return (org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier)storedValueForKey(_TypesAssocies.COURRIER_KEY);
  }
  
  public void setCourrier(org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier value) {
    takeStoredValueForKey(value, _TypesAssocies.COURRIER_KEY);
  }

  public void setCourrierRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier value) {
    if (_TypesAssocies.LOG.isDebugEnabled()) {
      _TypesAssocies.LOG.debug("updating courrier from " + courrier() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setCourrier(value);
    }
    else if (value == null) {
    	org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier oldValue = courrier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _TypesAssocies.COURRIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _TypesAssocies.COURRIER_KEY);
    }
  }
  
  public org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew typeCourrierNew() {
    return (org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew)storedValueForKey(_TypesAssocies.TYPE_COURRIER_NEW_KEY);
  }
  
  public void setTypeCourrierNew(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew value) {
    takeStoredValueForKey(value, _TypesAssocies.TYPE_COURRIER_NEW_KEY);
  }

  public void setTypeCourrierNewRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew value) {
    if (_TypesAssocies.LOG.isDebugEnabled()) {
      _TypesAssocies.LOG.debug("updating typeCourrierNew from " + typeCourrierNew() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeCourrierNew(value);
    }
    else if (value == null) {
    	org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew oldValue = typeCourrierNew();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _TypesAssocies.TYPE_COURRIER_NEW_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _TypesAssocies.TYPE_COURRIER_NEW_KEY);
    }
  }
  

  public static TypesAssocies createGB_TypesAssocies(EOEditingContext editingContext, Integer couNumero
, Integer typeRecord
, org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier courrier, org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew typeCourrierNew) {
    TypesAssocies eo = (TypesAssocies) EOUtilities.createAndInsertInstance(editingContext, _TypesAssocies.ENTITY_NAME);    
		eo.setCouNumero(couNumero);
		eo.setTypeRecord(typeRecord);
    eo.setCourrierRelationship(courrier);
    eo.setTypeCourrierNewRelationship(typeCourrierNew);
    return eo;
  }

  public static ERXFetchSpecification<TypesAssocies> fetchSpec() {
    return new ERXFetchSpecification<TypesAssocies>(_TypesAssocies.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypesAssocies> fetchAllGB_TypesAssocieses(EOEditingContext editingContext) {
    return _TypesAssocies.fetchAllGB_TypesAssocieses(editingContext, null);
  }

  public static NSArray<TypesAssocies> fetchAllGB_TypesAssocieses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _TypesAssocies.fetchGB_TypesAssocieses(editingContext, null, sortOrderings);
  }

  public static NSArray<TypesAssocies> fetchGB_TypesAssocieses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypesAssocies> fetchSpec = new ERXFetchSpecification<TypesAssocies>(_TypesAssocies.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypesAssocies> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypesAssocies fetchGB_TypesAssocies(EOEditingContext editingContext, String keyName, Object value) {
    return _TypesAssocies.fetchGB_TypesAssocies(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypesAssocies fetchGB_TypesAssocies(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypesAssocies> eoObjects = _TypesAssocies.fetchGB_TypesAssocieses(editingContext, qualifier, null);
    TypesAssocies eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_TypesAssocies that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypesAssocies fetchRequiredGB_TypesAssocies(EOEditingContext editingContext, String keyName, Object value) {
    return _TypesAssocies.fetchRequiredGB_TypesAssocies(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypesAssocies fetchRequiredGB_TypesAssocies(EOEditingContext editingContext, EOQualifier qualifier) {
    TypesAssocies eoObject = _TypesAssocies.fetchGB_TypesAssocies(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_TypesAssocies that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypesAssocies localInstanceIn(EOEditingContext editingContext, TypesAssocies eo) {
    TypesAssocies localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
