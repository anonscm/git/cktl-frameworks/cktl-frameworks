// DO NOT EDIT.  Make changes to VDroitCreateType.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _VDroitCreateType extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "VDroitCreateType";

  // Attribute Keys
  public static final ERXKey<String> AGT_TYPE = new ERXKey<String>("agtType");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  public static final ERXKey<String> TYPE_ORDRE_CHAR = new ERXKey<String>("typeOrdreChar");
  // Relationship Keys

  // Attributes
  public static final String AGT_TYPE_KEY = AGT_TYPE.key();
  public static final String PERS_ID_KEY = PERS_ID.key();
  public static final String TYPE_ORDRE_CHAR_KEY = TYPE_ORDRE_CHAR.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_VDroitCreateType.class);

  public VDroitCreateType localInstanceIn(EOEditingContext editingContext) {
    VDroitCreateType localInstance = (VDroitCreateType)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String agtType() {
    return (String) storedValueForKey(_VDroitCreateType.AGT_TYPE_KEY);
  }

  public void setAgtType(String value) {
    if (_VDroitCreateType.LOG.isDebugEnabled()) {
    	_VDroitCreateType.LOG.debug( "updating agtType from " + agtType() + " to " + value);
    }
    takeStoredValueForKey(value, _VDroitCreateType.AGT_TYPE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_VDroitCreateType.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_VDroitCreateType.LOG.isDebugEnabled()) {
    	_VDroitCreateType.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _VDroitCreateType.PERS_ID_KEY);
  }

  public String typeOrdreChar() {
    return (String) storedValueForKey(_VDroitCreateType.TYPE_ORDRE_CHAR_KEY);
  }

  public void setTypeOrdreChar(String value) {
    if (_VDroitCreateType.LOG.isDebugEnabled()) {
    	_VDroitCreateType.LOG.debug( "updating typeOrdreChar from " + typeOrdreChar() + " to " + value);
    }
    takeStoredValueForKey(value, _VDroitCreateType.TYPE_ORDRE_CHAR_KEY);
  }


  public static VDroitCreateType createVDroitCreateType(EOEditingContext editingContext, String agtType
, Integer persId
, String typeOrdreChar
) {
    VDroitCreateType eo = (VDroitCreateType) EOUtilities.createAndInsertInstance(editingContext, _VDroitCreateType.ENTITY_NAME);    
		eo.setAgtType(agtType);
		eo.setPersId(persId);
		eo.setTypeOrdreChar(typeOrdreChar);
    return eo;
  }

  public static ERXFetchSpecification<VDroitCreateType> fetchSpec() {
    return new ERXFetchSpecification<VDroitCreateType>(_VDroitCreateType.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<VDroitCreateType> fetchAllVDroitCreateTypes(EOEditingContext editingContext) {
    return _VDroitCreateType.fetchAllVDroitCreateTypes(editingContext, null);
  }

  public static NSArray<VDroitCreateType> fetchAllVDroitCreateTypes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _VDroitCreateType.fetchVDroitCreateTypes(editingContext, null, sortOrderings);
  }

  public static NSArray<VDroitCreateType> fetchVDroitCreateTypes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<VDroitCreateType> fetchSpec = new ERXFetchSpecification<VDroitCreateType>(_VDroitCreateType.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<VDroitCreateType> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static VDroitCreateType fetchVDroitCreateType(EOEditingContext editingContext, String keyName, Object value) {
    return _VDroitCreateType.fetchVDroitCreateType(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VDroitCreateType fetchVDroitCreateType(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<VDroitCreateType> eoObjects = _VDroitCreateType.fetchVDroitCreateTypes(editingContext, qualifier, null);
    VDroitCreateType eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one VDroitCreateType that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VDroitCreateType fetchRequiredVDroitCreateType(EOEditingContext editingContext, String keyName, Object value) {
    return _VDroitCreateType.fetchRequiredVDroitCreateType(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VDroitCreateType fetchRequiredVDroitCreateType(EOEditingContext editingContext, EOQualifier qualifier) {
    VDroitCreateType eoObject = _VDroitCreateType.fetchVDroitCreateType(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no VDroitCreateType that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VDroitCreateType localInstanceIn(EOEditingContext editingContext, VDroitCreateType eo) {
    VDroitCreateType localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
