// DO NOT EDIT.  Make changes to SpecifWebCommune.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _SpecifWebCommune extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_SpecifWebCommune";

  // Attribute Keys
  public static final ERXKey<String> ACTION = new ERXKey<String>("action");
  public static final ERXKey<Double> NO_INDIVIDU = new ERXKey<Double>("noIndividu");
  public static final ERXKey<String> SPEC_CLE = new ERXKey<String>("specCle");
  public static final ERXKey<String> SPEC_VALEUR = new ERXKey<String>("specValeur");
  // Relationship Keys

  // Attributes
  public static final String ACTION_KEY = ACTION.key();
  public static final String NO_INDIVIDU_KEY = NO_INDIVIDU.key();
  public static final String SPEC_CLE_KEY = SPEC_CLE.key();
  public static final String SPEC_VALEUR_KEY = SPEC_VALEUR.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_SpecifWebCommune.class);

  public SpecifWebCommune localInstanceIn(EOEditingContext editingContext) {
    SpecifWebCommune localInstance = (SpecifWebCommune)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String action() {
    return (String) storedValueForKey(_SpecifWebCommune.ACTION_KEY);
  }

  public void setAction(String value) {
    if (_SpecifWebCommune.LOG.isDebugEnabled()) {
    	_SpecifWebCommune.LOG.debug( "updating action from " + action() + " to " + value);
    }
    takeStoredValueForKey(value, _SpecifWebCommune.ACTION_KEY);
  }

  public Double noIndividu() {
    return (Double) storedValueForKey(_SpecifWebCommune.NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Double value) {
    if (_SpecifWebCommune.LOG.isDebugEnabled()) {
    	_SpecifWebCommune.LOG.debug( "updating noIndividu from " + noIndividu() + " to " + value);
    }
    takeStoredValueForKey(value, _SpecifWebCommune.NO_INDIVIDU_KEY);
  }

  public String specCle() {
    return (String) storedValueForKey(_SpecifWebCommune.SPEC_CLE_KEY);
  }

  public void setSpecCle(String value) {
    if (_SpecifWebCommune.LOG.isDebugEnabled()) {
    	_SpecifWebCommune.LOG.debug( "updating specCle from " + specCle() + " to " + value);
    }
    takeStoredValueForKey(value, _SpecifWebCommune.SPEC_CLE_KEY);
  }

  public String specValeur() {
    return (String) storedValueForKey(_SpecifWebCommune.SPEC_VALEUR_KEY);
  }

  public void setSpecValeur(String value) {
    if (_SpecifWebCommune.LOG.isDebugEnabled()) {
    	_SpecifWebCommune.LOG.debug( "updating specValeur from " + specValeur() + " to " + value);
    }
    takeStoredValueForKey(value, _SpecifWebCommune.SPEC_VALEUR_KEY);
  }


  public static SpecifWebCommune createGB_SpecifWebCommune(EOEditingContext editingContext, Double noIndividu
, String specCle
, String specValeur
) {
    SpecifWebCommune eo = (SpecifWebCommune) EOUtilities.createAndInsertInstance(editingContext, _SpecifWebCommune.ENTITY_NAME);    
		eo.setNoIndividu(noIndividu);
		eo.setSpecCle(specCle);
		eo.setSpecValeur(specValeur);
    return eo;
  }

  public static ERXFetchSpecification<SpecifWebCommune> fetchSpec() {
    return new ERXFetchSpecification<SpecifWebCommune>(_SpecifWebCommune.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<SpecifWebCommune> fetchAllGB_SpecifWebCommunes(EOEditingContext editingContext) {
    return _SpecifWebCommune.fetchAllGB_SpecifWebCommunes(editingContext, null);
  }

  public static NSArray<SpecifWebCommune> fetchAllGB_SpecifWebCommunes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _SpecifWebCommune.fetchGB_SpecifWebCommunes(editingContext, null, sortOrderings);
  }

  public static NSArray<SpecifWebCommune> fetchGB_SpecifWebCommunes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<SpecifWebCommune> fetchSpec = new ERXFetchSpecification<SpecifWebCommune>(_SpecifWebCommune.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<SpecifWebCommune> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static SpecifWebCommune fetchGB_SpecifWebCommune(EOEditingContext editingContext, String keyName, Object value) {
    return _SpecifWebCommune.fetchGB_SpecifWebCommune(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static SpecifWebCommune fetchGB_SpecifWebCommune(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<SpecifWebCommune> eoObjects = _SpecifWebCommune.fetchGB_SpecifWebCommunes(editingContext, qualifier, null);
    SpecifWebCommune eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_SpecifWebCommune that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static SpecifWebCommune fetchRequiredGB_SpecifWebCommune(EOEditingContext editingContext, String keyName, Object value) {
    return _SpecifWebCommune.fetchRequiredGB_SpecifWebCommune(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static SpecifWebCommune fetchRequiredGB_SpecifWebCommune(EOEditingContext editingContext, EOQualifier qualifier) {
    SpecifWebCommune eoObject = _SpecifWebCommune.fetchGB_SpecifWebCommune(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_SpecifWebCommune that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static SpecifWebCommune localInstanceIn(EOEditingContext editingContext, SpecifWebCommune eo) {
    SpecifWebCommune localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
