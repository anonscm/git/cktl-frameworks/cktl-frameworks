// DO NOT EDIT.  Make changes to Superuser.java instead.
package org.cocktail.geide.fwkcktlgedibus.metier.serveur;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class _Superuser extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "GB_Superuser";

  // Attribute Keys
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  // Relationship Keys

  // Attributes
  public static final String PERS_ID_KEY = PERS_ID.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_Superuser.class);

  public Superuser localInstanceIn(EOEditingContext editingContext) {
    Superuser localInstance = (Superuser)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_Superuser.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_Superuser.LOG.isDebugEnabled()) {
    	_Superuser.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _Superuser.PERS_ID_KEY);
  }


  public static Superuser createGB_Superuser(EOEditingContext editingContext, Integer persId
) {
    Superuser eo = (Superuser) EOUtilities.createAndInsertInstance(editingContext, _Superuser.ENTITY_NAME);    
		eo.setPersId(persId);
    return eo;
  }

  public static ERXFetchSpecification<Superuser> fetchSpec() {
    return new ERXFetchSpecification<Superuser>(_Superuser.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Superuser> fetchAllGB_Superusers(EOEditingContext editingContext) {
    return _Superuser.fetchAllGB_Superusers(editingContext, null);
  }

  public static NSArray<Superuser> fetchAllGB_Superusers(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _Superuser.fetchGB_Superusers(editingContext, null, sortOrderings);
  }

  public static NSArray<Superuser> fetchGB_Superusers(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Superuser> fetchSpec = new ERXFetchSpecification<Superuser>(_Superuser.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Superuser> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Superuser fetchGB_Superuser(EOEditingContext editingContext, String keyName, Object value) {
    return _Superuser.fetchGB_Superuser(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Superuser fetchGB_Superuser(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Superuser> eoObjects = _Superuser.fetchGB_Superusers(editingContext, qualifier, null);
    Superuser eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GB_Superuser that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Superuser fetchRequiredGB_Superuser(EOEditingContext editingContext, String keyName, Object value) {
    return _Superuser.fetchRequiredGB_Superuser(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Superuser fetchRequiredGB_Superuser(EOEditingContext editingContext, EOQualifier qualifier) {
    Superuser eoObject = _Superuser.fetchGB_Superuser(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GB_Superuser that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Superuser localInstanceIn(EOEditingContext editingContext, Superuser eo) {
    Superuser localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
