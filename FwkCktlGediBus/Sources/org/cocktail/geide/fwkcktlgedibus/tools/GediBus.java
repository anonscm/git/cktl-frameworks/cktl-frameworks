/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.geide.fwkcktlgedibus.tools;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.geide.fwkcktlgedibus.exception.AgentNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.CourrierNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.CourrierPrimaryKeyNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.CreateAgentFailedException;
import org.cocktail.geide.fwkcktlgedibus.exception.CreateCourrierFaildException;
import org.cocktail.geide.fwkcktlgedibus.exception.CreateReferenceFailedException;
import org.cocktail.geide.fwkcktlgedibus.exception.CreateReferenceLienFailedException;
import org.cocktail.geide.fwkcktlgedibus.exception.CreateTypeAssociesFailedException;
import org.cocktail.geide.fwkcktlgedibus.exception.CreateTypeCourrierNewFailedException;
import org.cocktail.geide.fwkcktlgedibus.exception.DefaultReferenceNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.DeleteCourrierFailedException;
import org.cocktail.geide.fwkcktlgedibus.exception.DeleteReferenceLienFailedException;
import org.cocktail.geide.fwkcktlgedibus.exception.DeleteTypeAssociesFailedException;
import org.cocktail.geide.fwkcktlgedibus.exception.DeleteTypeCourrierNewException;
import org.cocktail.geide.fwkcktlgedibus.exception.LangueNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.ReferenceLienNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.ReferenceLienPrimaryKeyNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.ReferenceNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.ReplaceTypeAssociesFailedException;
import org.cocktail.geide.fwkcktlgedibus.exception.TypeCourrierNewNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.UpdateSeqAgentFailedException;
import org.cocktail.geide.fwkcktlgedibus.exception.UpdateSeqRefFailedException;
import org.cocktail.geide.fwkcktlgedibus.metier.client.Langue;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.Agents;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.AgentsTout;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.Destinataire;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.DroitTypeClient;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.Reference;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.RepartCourAuteur;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypesAssocies;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSSet;
import com.webobjects.foundation.NSTimestamp;

/**
 * Cette classe regroupe toutes les methodes utiles pour l'utilisation de la GEIDE
 * 
 * @author mparadot
 * @version 0.0.6 10/11/2006
 *  
 */
public class GediBus {
    public static final String VERSION = "1.2.0 08/12/2008";

    private static final String VIDE = "_";

    /**
     * visibilite de type intranet (i.e. en fonction de l'adresse IP)
     */
    public static final Integer TYPE_INTRANET = new Integer(0);

    /**
     * visibilite pour ceux qui se sont authentifies...
     */
    public static final Integer TYPE_EXTRANET = new Integer(2);

    /**
     * visible sur le web (sans condition)
     */
    public static final Integer TYPE_INTERNET = new Integer(1);

    /**
     * visible que par le createur et eventuellement les destinataires
     */
    public static final Integer TYPE_PRIVE = new Integer(3);

    /**
     * non visible depuis le web, seulement pour le createur
     */
    public static final Integer TYPE_MASQUEE = new Integer(4);

    /**
     * Courrier en arrivee
     */
    public static final String COURRIER_ARRIVEE = "A";

    /**
     * Courrier en depart
     */
    public static final String COURRIER_DEPART = "D";

    /**
     * Pas de type pere = nouvelle racine !!!!
     */
    public static final String TYPE_SANS_PERE = null;

    /**
     * Type visible depuis le web (en fonction de sa visibilite Internet/Intranet/Extranet/Prive)
     */
    public static final String TYPE_VISIBLE_VIA_WEB = "O";

    /**
     * Type non visible depuis le web (ie Masquee)
     */
    public static final String TYPE_NON_VISIBLE_VIA_WEB = "N";

    /**
     * Type de la famille 'Classique' (simple courrier)
     */
    public static final String TYPE_FAMILLE_CLASSIQUE = "C";

    /**
     * Type de la famille 'FlashInfo' (courrier avec date de début/fin de publication)
     */
    public static final String TYPE_FAMILLE_FLASH_INFO = "F";

    /**
     * Type de la famille 'WebHtml' (courrier dont le contenu est en HTML)
     */
    public static final String TYPE_FAMILLE_WEB_HTML = "W";

    /**
     * Variable à utiliser pour les droits : permet de savoir si la personne "a le droit"
     */
    public static final String DROIT = "O";

    /**
     * Variable à utiliser pour les droits : permet de savoir si la personne "n'a pas le droit"
     */
    public static final String PAS_DROIT = "N";

    public static final String TYPE_REFERENCE_FICHIER = "F";

    public static final String TYPE_REFERENCE_LIEN = "U";

    /**
     * editingContext utiliser par le framework
     */
    private EOEditingContext ec;

    /**
     * 
     * @param ec
     */
    private CktlWebApplication criApp;
    //= ((CktlWebApplication) CktlWebApplication.application());

    /**
     * Creation d'une instance de GediCourrier
     * 
     * @param ec
     *            editingContext a utiliser pour les acces a la base
     */
    public GediBus(EOEditingContext ec) {
        super();
        try {
            this.ec = ec;
            this.criApp = (CktlWebApplication) CktlWebApplication.application();
		} catch (Throwable e) {
			e.printStackTrace();
		}
    }

    /**
     * Creation d'une instance de GediCourrier
     * 
     * @param ec
     *            editingContext a utiliser pour les acces a la base
     * @param criApp
     *            si Gedibus est utilise depuis une autre application, permet les acces a la base
     */
    public GediBus(EOEditingContext ec, CktlWebApplication criApp) {
        super();
        this.ec = ec;
        this.criApp = criApp;
        CktlLog.trace("***GEDIBUS Version : " + VERSION);
    }

    public static NSArray<EOStructure> hierarchicalGroupesForIndividu(EOIndividu individu) {
        NSSet ancestorsArray = new NSSet();
        NSArray array = individu.toRepartStructures();
        if ((array != null) && (array.count() > 0)) {
            java.util.Enumeration en = array.objectEnumerator();
            // on prend chaque repartStructure de l individu et on rajoute tous
            // les ancetres
            // de sa structure Pere
            EORepartStructure myRecord = null;
            EOStructure structure = null;
            while (en.hasMoreElements()) {
                myRecord = (EORepartStructure) (en.nextElement());
                structure = myRecord.toStructureGroupe();
                // on ajoute le pere direct
                if (structure != null) {
                    // on ajoute le pere direct
                    ancestorsArray = ancestorsArray.setByUnioningSet(new NSSet(structure));
                    // on ajoute les ancetres du pere
                    ancestorsArray = ancestorsArray.setByUnioningSet(new NSSet(hierarchicalParentGroupesForStructure(structure, null)));
                }
            }
        }
        return ancestorsArray.allObjects();

    }
    
    public static NSArray<EOStructure> hierarchicalParentGroupesForStructure
                                                (EOStructure structure, NSMutableArray<EOStructure> treatedStructures) {
        if ( treatedStructures == null)
            treatedStructures = new NSMutableArray();

        NSSet parentGroupes = new NSSet();

        if ( treatedStructures.indexOfObject( structure) < 0) {
            treatedStructures.addObject( structure);

            NSArray tmp = structure.toRepartStructures();
            if ( (tmp != null) && (tmp.count() > 0) ) {
                java.util.Enumeration en = tmp.objectEnumerator();
                EORepartStructure myRecord = null;
                EOStructure currentStructure = null;
                while ( en.hasMoreElements()) {
                    myRecord = (EORepartStructure)en.nextElement();
                    currentStructure = myRecord.toStructureGroupe();
                    if ( (currentStructure != null) ) {
                        // on ajoute le pere direct
                        parentGroupes = parentGroupes.setByUnioningSet(new NSSet (currentStructure));
                        //                      System.out.println( "Structure.hierarchicalParentGroupes() : " + structure.llStructure());
                        // on ajoute les ancetres du pere
                        parentGroupes = parentGroupes.setByUnioningSet(new NSSet (hierarchicalParentGroupesForStructure(currentStructure, treatedStructures)));
                    }
                }
            }
        }
        return parentGroupes.allObjects();
    }
    
    /**
     * Calcul le droit effectif de l'individu et verifie le droit demande.
     * @param typeOdreChar
     * @param lecture
     * @param modification
     * @param suppression
     * @return
     */
    public static boolean isDroitHierarchicOnType(EOIndividu individu, String typeOdreChar, String lecture, String modification, String suppression) {
        boolean b = false;
        int i = droitHierarchicOnType(individu, typeOdreChar);
        if ( i > 0) {
            b = true;
            if ( (suppression != null) && suppression.equals("O")) 
                b = b && ((i >> 2) == 1);
//          System.out.println( "Suppression = " + suppression + " : (i >> 2) = " + (i >> 2) + " b = " + b);

            if ( (modification != null) && modification.equals("O")) 
                b =  b && ((i >> 1) % 2 == 1);
//          System.out.println( "modification = " + modification + " : (i >> 1) % 2= " + (i >> 1) + " b = " + b);

            if ( (lecture != null) && lecture.equals("O")) 
                b =  b && ((i % 2) == 1);
//          System.out.println( "lecture = " + lecture + " : (i % 2) = " + (i % 2) + " b = " + b);
        }
        return b;
    }
    

    /**
     * Calcule le droit eventuel de l'individu sur un dossier (TypeCourrierNew) donne par son typeOdreChar.
     * Le droit est calcule d'abord pour l'individu: on remonte la hierarchie des dossiers parents et on prend le droit trouve sur
     * le 1er dossier pere.
     * Si on ne trouvepas de droit pour l'individu, on calcule ceux des groupes hierachiques de l'individu en appliquant la meme regle.
     * @param typeOdreChar : cle du 
     * @param lecture
     * @param modification
     * @param suppression
     * @return
     *   -1 si la structure n'a droit sur aucun dossier
     *   0  si la structure a droit sur un dossier mais pas celui donne par typeOdreChar
     *   si la structure a droit sur un dossier donne par typeOdreChar, l'entier dont l'hexa est XYZ tel que :
     *      Z = 1 si dLec() == "O" sinon 0
     *      Y = 1 si dMod() == "O" sinon 0
     *      X = 1 si dSup() == "O" sinon 0
     * @see #droitOnType( String, String)
     * @see #droitHierarchicOnType( NSArray)
     */
    public static int droitHierarchicOnType(EOIndividu individu, String typeOdreChar) {
        // Defaut : -1 = l'individu n'a aucun droit sur aucun dossier
        int i = -1;
        if ( (typeOdreChar != null) && (typeOdreChar.length() > 0))
            i = droitHierarchicOnType(individu, typeOdreChar, TypeCourrierNew.TYPE_CHAR_ROOT);
        return i;
    }
    
    /**
     * Calcule le droit eventuel de l'individu sur une hierarchie de dossiers (TypeCourrierNew): on prend le droit
     * verifie sur le 1er dossier de la hierarchie. Il est donc preferable que le dossier typeCharEnd soit l'un des peres de typeCharStart.
     * Si ce n'est pas le cas on prend le pere racine "0".
     * Si on ne trouvepas de droit pour l'individu, on calcule ceux des groupes hierachiques de l'individu en appliquant la meme regle.
     * @param typeCharStart
     *      Dossier de depart
     * @param typeCharEnd
     *      Dossier pere dans la hierarchie des parents.
     * @param lecture
     * @param modification
     * @param suppression
     * @return
     *   -1 si la structure n'a droit sur aucun dossier
     *   0  si la structure a droit sur un dossier mais pas celui donne par typeOdreChar
     *   si la structure a droit sur un dossier donne par typeOdreChar, l'entier dont l'hexa est XYZ tel que :
     *      Z = 1 si dLec() == "O" sinon 0
     *      Y = 1 si dMod() == "O" sinon 0
     *      X = 1 si dSup() == "O" sinon 0
     */
    public static int droitHierarchicOnType(EOIndividu individu, String typeCharStart, String typeCharEnd) {
        int i = -1;
        
        if ( (typeCharStart != null) && (typeCharStart.length() > 0) ) {

            NSArray array = TypeCourrierNew.arrayHierarchy( individu.editingContext(), typeCharStart, typeCharEnd);
//          System.out.println("droitHierarchicOnType( String typeCharStart, String typeCharEnd) : TypeCourrierNew.arrayHierarchy = ");
//          System.out.println(array);

            i = droitHierarchicOnType(individu, array); 
//          System.out.println("droi : droitHierarchicOnType( array) = " + i);
        }
        
        return i;
    }
    
    
    /**
     * Calcule le droit eventuel de l'individu (sans calcule des droits des groupes hierarchique de l'individu) sur un dossier (TypeCourrierNew) du tableau de types: on prend le droit
     * verifie sur le 1er dossier du tableau. Il est donc preferable que le tableau contienne une hierachie d'un dossier.
     * 
     * @param typeOdreCharArray
     *      Tableau contenant les cles des dossiers dont il faut trouver un droit
     * @param lecture
     * @param modification
     * @param suppression
     * @return
     *   -1 si la structure n'a droit sur aucun dossier
     *   0  si la structure a droit sur un dossier mais pas celui donne par typeOdreChar
     *   si la structure a droit sur un dossier donne par typeOdreChar, l'entier dont l'hexa est XYZ tel que :
     *      Z = 1 si dLec() == "O" sinon 0
     *      Y = 1 si dMod() == "O" sinon 0
     *      X = 1 si dSup() == "O" sinon 0
     */
    public static int droitOnType(EOIndividu individu, NSArray typeOdreCharArray) {
        // Defaut : -1 = l'individu n'a aucun droit sur aucun dossier
        int i = -1;

        // Droit direct sur l'un des types
        if ( (typeOdreCharArray != null) && (typeOdreCharArray.count() > 0) ) {
            NSArray droitsArray = DroitTypeClient.droitsTypeClient(individu);

            if ( (droitsArray != null) && (droitsArray.count() > 0) ) {
                i = 0;      // pas le droit
                java.util.Enumeration en = droitsArray.objectEnumerator();
                org.cocktail.geide.fwkcktlgedibus.metier.serveur.DroitTypeClient droitType = null;
                while ( en.hasMoreElements() && (i == 0) )  {
                    droitType = (DroitTypeClient)(en.nextElement());
//                  System.out.println("droitType = " + droitType);
                    // Droit
                    if ( (droitType != null) && (droitType.clientType().equals("I")) && (typeOdreCharArray.indexOfObject(droitType.agtType()) >= 0) ) {
                        i = droitType.droitInt();
//                      System.out.println("Calcule du droit : droitInt = " + i);
                    }
                }
            }
        }
        return i;
    }
    
    /**
     * Calcule le droit eventuel de la structure sur un dossier (TypeCourrierNew) du tableau de types: on prend le droit
     * verifie sur le 1er dossier du tableau. Il est donc preferable que le tableau contienne une hierachie d'un dossier.
     * 
     * @param typeOdreCharArray
     *      Tableau contenant les cles des dossiers dont il faut trouver un droit
     * @param typeOdreChar
     * @param lecture
     * @param modification
     * @param suppression
     * @return
     *   -1 si la structure n'a droit sur aucun dossier
     *   0  si la structure a droit sur un dossier mais pas celui donne par typeOdreChar
     *   si la structure a droit sur un dossier donne par typeOdreChar, l'entier dont l'hexa est XYZ tel que :
     *      Z = 1 si dLec() == "O" sinon 0
     *      Y = 1 si dMod() == "O" sinon 0
     *      X = 1 si dSup() == "O" sinon 0
     */
    public static int droitOnType(EOStructure structure, NSArray typeOdreCharArray) {
        // Defaut : -1 = l'individu n'a aucun droit sur aucun dossier
        int i = -1;
        
        if ( (typeOdreCharArray != null) && (typeOdreCharArray.count() >= 0) ) {
            NSArray droitsArray = DroitTypeClient.droitsTypeClient(structure);

            if ( (droitsArray != null) && (droitsArray.count() > 0) ) {
                i = 0;      // pas le droit
                java.util.Enumeration en = droitsArray.objectEnumerator();
                DroitTypeClient droitType = null;
                while ( en.hasMoreElements() && (i == 0) )  {
                    droitType = (DroitTypeClient)(en.nextElement());
                    // Droit
                    if ( (droitType != null) && (typeOdreCharArray.indexOfObject(droitType.agtType()) >= 0) && (droitType.clientType().equals("G")) ) {
                        i = droitType.droitInt();
                    }
                }
            }
        }
        return i;
    }
    
    /**
     * Calcule le droit eventuel de l'individu sur un dossier ( TypeCourrierNew) donne par son typeOdreChar.
     * Le droit est calcule d'abord pour l'individu, si pas de droit, on calcule sur les groupes hierachiques de l'individu
     * @param typeOdreChar
     * @param lecture
     * @param modification
     * @param suppression
     * @return
     *   -1 si la structure n'a droit sur aucun dossier
     *   0  si la structure a droit sur un dossier mais pas celui donne par typeOdreChar
     *   si la structure a droit sur un dossier donne par typeOdreChar, l'entier dont l'hexa est XYZ tel que :
     *      Z = 1 si dLec() == "O" sinon 0
     *      Y = 1 si dMod() == "O" sinon 0
     *      X = 1 si dSup() == "O" sinon 0
     */
    public static int droitHierarchicOnType(EOIndividu individu, NSArray typeOdreCharArray) {
        // Defaut : -1 = l'individu n'a aucun droit sur aucun dossier
        int i = -1;
        
        if ( (typeOdreCharArray != null) && (typeOdreCharArray.count() > 0) ) {

            // Droit direct sur l'un des types
            i = droitOnType(individu, typeOdreCharArray);
//          System.out.println( "droitHierarchicOnType, Individu : droitOnType = " + i);
            
            // Si pas de droit, on cherche ceux des groupes hierarchiques
            if ( i <= 0) {
                NSArray arrayGroupes = hierarchicalGroupesForIndividu(individu);
                if ( (arrayGroupes != null) && (arrayGroupes.count() > 0) ) {
//                  System.out.println( "Individu.hierarchicalGroupes() = ");
//                  System.out.println( arrayGroupes.valueForKey("llStructure"));
                    
                    java.util.Enumeration en = arrayGroupes.objectEnumerator();
                    EOStructure structure = null;
                    while ( en.hasMoreElements() && (i <= 0) )  {
                        structure = (EOStructure)(en.nextElement());
                        if ( structure != null) {
                            i = droitOnType(structure, typeOdreCharArray);
//                          if ( i > 0)
//                          System.out.println( "droitHierarchicOnType, Structure = " + structure.llStructure() + " droitOnType = " + i);
                        }
                    }
                }
            }
        }
        
        return i;
    }
    
    /**
     * 
     * Cherche les droits sur les dossiers (DroitTypeClient) dont l'utilisateur a droit (entite DroitTypeClient)
     * @return
     */
    public static NSArray droitHierarchicAgentTypes(EOIndividu individu) {
//      NSSet set = new NSSet();
        NSMutableArray mArray = new NSMutableArray();
        
        // Droits dont l'utilisateur a droit directement
        NSArray array = DroitTypeClient.droitsTypeClient(individu);
        if( (array != null) && (array.count() > 0) ) {
//          set = set.setByUnioningSet(new NSSet (array));
            mArray.addObjectsFromArray(array);
//          CktlLog.trace( "Individu count = " + array.count()+ " - set.count() = " + set.count());
//          CktlLog.trace( "array = " + array);
        }
        
        // Droits herites des groupes hierarchiques de l'individu
        NSArray arrayGroupes = hierarchicalGroupesForIndividu(individu);
        if ( (arrayGroupes != null) && (arrayGroupes.count() > 0) ) {
            java.util.Enumeration en = arrayGroupes.objectEnumerator();
            EOStructure structure = null;
            while ( en.hasMoreElements())  {
                structure = (EOStructure)(en.nextElement());
                if ( structure != null) {
                    array = DroitTypeClient.droitsTypeClient(structure);
                    if( (array != null) && (array.count() > 0) ) {
//                      set = set.setByUnioningSet(new NSSet (array));
                        DroitTypeClient unDroit = null;
                        for (int i = 0; i < array.count(); i++) {
                            unDroit = (DroitTypeClient)array.objectAtIndex(i);
//                          CktlLog.trace( "unDroit = " + unDroit);
                            if ( mArray.indexOfObject(unDroit) == NSArray.NotFound) {
                                mArray.addObject(unDroit);
                            }
                        }
//                      CktlLog.trace( "StructureUlr " +  structure.llStructure() + " : count = " + array.count() + " - set.count() = " + set.count());
//                      CktlLog.trace( "array = " + array);
                    }
                }
            }
        }
//      array = set.allObjects();
        
//      CktlLog.trace( "Final array count = " + array.count());
//      CktlLog.trace( "array = " + array);
        
        if ( mArray.count() > 0 ) 
//          return (NSArray)array.valueForKey("typeCourrierNew");   
            return (NSArray)mArray; 
        else
            return null;
    }
    
    
    //---------------------------------------------La gestion des courriers----------------------------------------------

    /**
     * Permet la creation d'un courrier (avec un nb limite de parametres) en utilisant la reference par defaut de l'agent gedi correspondant au persId
     * 
     * @param objet
     *            Objet du courrier
     * @param commentaire
     *            Commentaire du courrier
     * @param persId
     *            PersId du createur du courrier
     * @param typeConsultation
     *            defini la visibilite sur le web
     * @param expediteur
     *            l'expediteur du courrier ( peut etre different du createur)
     * @param agtLogin
     *            le login du createur
     * @return le numero du courrier cree (couNumero)
     * @throws CourrierPrimaryKeyNotFoundException
     * @throws CreateCourrierFaildException
     * @throws DefaultReferenceNotFoundException
     * @throws ReferenceNotFoundException
     * @throws AgentNotFoundException
     * @throws UpdateSeqAgentFailedException
     * @throws CreateCourrierFaildException
     * @throws CourrierPrimaryKeyNotFoundException
     * @throws UpdateSeqRefFailedException
     */
    public Integer createCourrier(String objet, String commentaire, Number persId, Integer typeConsultation, String expediteur, String agtLogin) throws DefaultReferenceNotFoundException,
            ReferenceNotFoundException, UpdateSeqRefFailedException, AgentNotFoundException, UpdateSeqAgentFailedException, CreateCourrierFaildException, CourrierPrimaryKeyNotFoundException {
        GediReference ref = defaultReferenceForAgent(persId);
        return createCourrier(objet, commentaire, DateCtrl.now(), DateCtrl.now(), null, null, COURRIER_ARRIVEE, expediteur, null, null, null, null, null, null, ref.refLibelle(), persId,
                typeConsultation, null, agtLogin);
    }

    /**
     * Permet la creation d'un courrier avec un nombre limite de parametre
     * 
     * @param objet
     *            Objet du courrier
     * @param commentaire
     *            Commentaire du courrier
     * @param persId
     *            PersId du createur du courrier
     * @param ref
     *            Libelle de la reference utilisee par le createur
     * @param typeConsultation
     *            defini la visibilite sur le web
     * @param expediteur
     *            l'expediteur du courrier ( peut etre different du createur)
     * @param agtLogin
     *            le login du createur
     * @return le numero du courrier cree (couNumero)
     * @throws CourrierPrimaryKeyNotFoundException
     * @throws CreateCourrierFaildException
     * @throws UpdateSeqAgentFailedException
     * @throws AgentNotFoundException
     * @throws UpdateSeqRefFailedException
     * @throws ReferenceNotFoundException
     * @throws CreateCourrierFaildException
     *             si la creation du courrier a echoue
     *  
     */
    public Integer createCourrier(String objet, String commentaire, Number persId, String ref, Integer typeConsultation, String expediteur, String agtLogin) throws ReferenceNotFoundException,
            UpdateSeqRefFailedException, AgentNotFoundException, UpdateSeqAgentFailedException, CreateCourrierFaildException, CourrierPrimaryKeyNotFoundException {
        return createCourrier(objet, commentaire, DateCtrl.now(), DateCtrl.now(), null, null, COURRIER_ARRIVEE, expediteur, null, null, null, null, null, null, ref, persId, typeConsultation, null,
                agtLogin);
    }

    /**
     * Permet la creation d'un courrier (avec tous les parametres). Increment de la reference et de la sequence de l'agent 
     * Remarque: le courrier est toujours cree avec la langue par defaut (francais)
     * 
     * @param objet
     *            Objet du courrier
     * @param commentaire
     *            Commentaire du courrier
     * @param dateCourrier
     *            Date de creation du courrier
     * @param dateEnreg
     *            Date de modification du courrier
     * @param dateRespAttendue
     *            Date souhaitee pour la reponse / date de debut de visibilite sur le web
     * @param dateRepEffectuee
     *            Date reelle de la reponse / date de fin de visibilite sur le web
     * @param departArrivee
     *            Si il s'agit d'un depart ou d'une arrivee de courrier
     * @param expediteur
     *            l'expediteur du courrier ( peut etre different du createur)
     * @param expEtab
     *            l'etablisement expediteur du courrier
     * @param expVia
     *            envoi "sous couvert de" la hierarchie
     * @param localisation
     *            url de localisation d'un site liee au courrier
     * @param motsClefs
     *            mots clef du courrier, separes par des ';'
     * @param nbRelance
     *            nombre de relances au courrier
     * @param numeroPere
     *            numero du courrier pere (en cas de reponse a un courrier)
     * @param ref
     *            reference utilisee par le createur du courrier
     * @param persId
     *            persId du createur du courrier
     * @param typeConsultation
     *            defini la visibilite sur le web du courrier (et des docs assossiees)
     * @param cStructure
     *            code de la structure pour laquelle le createur a fait le courrier
     * @param agtLogin
     *            le login du createur....
     * @return le numero du courrier cree (couNumero)
     * @throws ReferenceNotFoundException
     * @throws CreateCourrierFaildException
     *             si la creation du courrier a echoue
     * @throws ReferenceNotFoundException
     *             si la reference est inconnue pour l'agent GEDI
     * @throws UpdateSeqRefFailedException
     * @throws AgentNotFoundException
     * @throws UpdateSeqAgentFailedException
     * @throws CreateCourrierFaildException
     * @throws CourrierPrimaryKeyNotFoundException
     * 
     * @see GediBus#COURRIER_ARRIVEE
     * @see GediBus#COURRIER_DEPART
     * @see GediBus#TYPE_EXTRANET
     * @see GediBus#TYPE_INTERNET
     * @see GediBus#TYPE_INTRANET
     * @see GediBus#TYPE_MASQUEE
     * @see GediBus#TYPE_PRIVE
     */
    public Integer createCourrier(String objet, String commentaire, NSTimestamp dateCourrier, NSTimestamp dateEnreg, NSTimestamp dateRespAttendue, NSTimestamp dateRepEffectuee, String departArrivee,
            String expediteur, String expEtab, String expVia, String localisation, String motsClefs, Integer nbRelance, Integer numeroPere, String ref, Number persId, Integer typeConsultation,
            String cStructure, String agtLogin) throws ReferenceNotFoundException, UpdateSeqRefFailedException, AgentNotFoundException, UpdateSeqAgentFailedException, CreateCourrierFaildException,
            CourrierPrimaryKeyNotFoundException {
        if (objet == null)
            throw new CreateCourrierFaildException("L'objet du courrier ne peut pas etre null !!!");
        Reference reference = referenceForLibelleAndAgtLogin(ref, agtLogin);
//        System.out.println("creation : ref = " + reference.refLibelle());
        Courrier cour = new Courrier();
        cour.setCouObjet(objet);
        //cour.setCouCommentaire(commentaire);
        cour.setCouDateCourrier(dateCourrier);
        cour.setCouDateEnreg(dateEnreg);
        cour.setCouDateReponseAttendue(dateRespAttendue);
        cour.setCouDateReponseEffectuee(dateRepEffectuee);
        cour.setCouDepartArrivee(departArrivee);
        cour.setCouExpediteur(expediteur);
        cour.setCouExpEtablissement(expEtab);
        cour.setCouExpVia(expVia);
        cour.setCouIdent(reference.refSeq());
        cour.setCouLocalisation(localisation);
        cour.setCouMotsClefs(motsClefs);
        cour.setCouNombreRelance(nbRelance);
        cour.setCouNumeroPere(numeroPere);
//        System.out.println("avant ref");
        String newRef = reference.refForNewCourrier();
//        System.out.println("creation :newref = " + newRef);
        cour.setCouReference(newRef);
        cour.setPersId((Integer)persId);
        cour.setCouTypeConsultation(typeConsultation);
        cour.setCStructure(cStructure);
        cour.setAgtLogin(agtLogin);
        Agents agt = agentForPersId(persId);
        agt.majAgtSeq();
        try {
        	System.out.println("av insert");
            ec.insertObject(cour);
            System.out.println("ap insert");
            ec.saveChanges();
            System.out.println("ap save1");
//            GediCourrier courr = new GediCourrier(cour);
//            System.out.println("av save2");
//            courr.setCouCommentaire(commentaire, langueForCode(GediLangue.CODE_LANGUE_FRANCE));
//            ec.saveChanges();
            System.out.println("ap save2");
        } catch (Exception e) {
            ec.revert();
            throw new CreateCourrierFaildException(e.getMessage());
        }
        return (Integer)cour.couNumero();
    }

    /**
     * Supprime le courrier dont le couNumero est passe en paramettre <BR>
     * <strong>ATTENTION: </strong> Lors de la suppression du courrier, tous les documents attaches sont supprimes. <br>
     * Les {@link TypesAssocies}, les {@link TexteMultilingue}, les {@link RepartCourAuteur}, les {@link Destinataire}et les {@link SpecifWeb}correspondant, sont eux aussi supprimes. <BR>
     * <font color="red">Les fichiers sur le serveur de fichiers GEDFS sont supprimes definitivement !!!! </font>
     * 
     * @param couNumero
     *            du courrier a supprimer
     * @throws DeleteCourrierFailedException
     *             {@link DeleteCourrierFailedException}
     * @deprecated voir {@link #removeCourrier(Integer, Number)}
     */
    public void removeCourrier(Integer couNumero) throws DeleteCourrierFailedException {
        removeCourrier(couNumero, new Integer(-1));
    }

    /**
     * Supprime le courrier dont le couNumero est passe en paramettre <BR>
     * <strong>ATTENTION: </strong> Lors de la suppression du courrier, tous les documents attaches sont supprimes. <br>
     * Les {@link TypesAssocies}, les {@link TexteMultilingue}, les {@link RepartCourAuteur}, les {@link Destinataire}et les {@link SpecifWeb}correspondant, sont eux aussi supprimés. <BR>
     * <font color="red">Les fichiers sur le serveur de fichiers GEDFS sont supprimes definitivement !!!! </font>
     * 
     * @param couNumero
     *            du courrier a supprimer
     * @param noIndividu
     *            de la personne qui supprime le courrier !!!!
     * @throws DeleteCourrierFailedException
     *             {@link DeleteCourrierFailedException}
     */
    public void removeCourrier(Integer couNumero, Number noIndividu) throws DeleteCourrierFailedException {
        Courrier courrier;
        try {
            courrier = courrierForCouNumero(couNumero).getCourrier();
        } catch (Exception e1) {
            e1.printStackTrace();
            throw new DeleteCourrierFailedException(e1.getMessage());
        }
        try {
            removeListReferenceLien(courrier.referenceLiens(), noIndividu);
            removeToMany(courrier.gB_Destinataires());
            removeToMany(courrier.gB_RepartCourAuteurs());
            removeToMany(courrier.gB_TexteMultilingues());
            removeToMany(courrier.specifWeb());
            removeToMany(courrier.typesAssocies());
            ec.saveChanges();
        } catch (Exception e) {
            ec.revert();
            e.printStackTrace();
            throw new DeleteCourrierFailedException(e.getMessage());
        }
        try {
            ec.deleteObject(courrier);
            ec.saveChanges();
        } catch (Exception e) {
            ec.revert();
            e.printStackTrace();
            throw new DeleteCourrierFailedException(e.getMessage());
        }
    }

    private void removeListReferenceLien(NSArray listRefLien, Number noIndividu) throws DeleteCourrierFailedException {
        for (int i = 0; i < listRefLien.count(); i++) {
            removeReferenceLien((ReferenceLien) listRefLien.objectAtIndex(i), noIndividu);
        }
    }

    private void removeReferenceLien(ReferenceLien ref, Number noIndividu) throws DeleteCourrierFailedException {
        if (TYPE_REFERENCE_LIEN.equals(ref.rflType())) {
            //si lien, simple suppression !!!
            ec.deleteObject(ref);
        } else {
            //sinon, c'est un fichier, est-il sur GEDFS ????
            if (ref.rflDocumentId() != null) {
                //c'est un fichier sur GEDFS, donc on supprime d'abord le fichier
                if (criApp.gedBus() == null)
                    throw new DeleteCourrierFailedException("Gedbus non initialise !!!\nVeuillez utiliser APP_USE_GEDFS=YES dans le fichier de config de l'application.");
                if (noIndividu == null)
                    noIndividu = new Integer(-1);
                boolean ok = criApp.gedBus(noIndividu.intValue()).deleteDocumentGED(ref.rflDocumentId().intValue());
                if (!ok) {
                    System.out.println(">>>>>GEDIBUS : probleme GEDF, impossible de supprimer le document numero " + ref.rflDocumentId().intValue());
                }
            }
            //on efface le referenceLien !!!
            ec.deleteObject(ref);
        }
    }

    private void removeToMany(NSArray list) {
        for (int i = 0; i < list.count(); i++) {
            ec.deleteObject((EOEnterpriseObject) list.objectAtIndex(i));
        }
    }

    /**
     * Renvoi un {@link GediCourrier}pour un couNumero donné
     * 
     * @param couNumero
     *            couNumero du courrier que l'on veut récupérer
     * @return un {@link GediCourrier}
     * @throws CourrierNotFoundException
     */
    public GediCourrier courrierForCouNumero(Integer couNumero) throws CourrierNotFoundException {
        if (couNumero == null)
            throw new CourrierNotFoundException(null);
        NSArray list = courrierForQualifierAndSort(EOQualifier.qualifierWithQualifierFormat("couNumero=%@", new NSArray(couNumero)), null);
        if (list == null || list.count() == 0)
            throw new CourrierNotFoundException("couNumero " + couNumero.intValue() + " inconnu");
        return (GediCourrier) list.lastObject();
    }

    /**
     * Renvoi une liste de {@link GediCourrier}pour le qualifier donné, et trié par sort
     * 
     * @param qual
     *            le qualifier
     * @param sort
     *            liste de {@link EOSortOrdering}
     * @return un {@link NSArray}
     * @throws CourrierNotFoundException
     */
    public NSArray courrierForQualifierAndSort(EOQualifier qual, NSArray sort) throws CourrierNotFoundException {
        return GediCourrier.initListCourrier(criApp.dataBus().fetchArray(ec, "GB_Courrier", qual, sort, false));
    }

    //-------------------------------------------Gestion des références-------------------------------------------------

    /**
     * Permet de créer une nouvelle référence
     * 
     * @param libelle
     *            libelle de la référence (celle qui utilisé dans le courrier)
     * @param code
     *            code interne de la référence
     * @param agtLogin
     *            login de la personne qui créé la référence. DOIT ÊTRE AGENT GEDI (cf {@link org.cocktail.geide.fwkcktlgedibus.metier.serveur.Agents})
     * @return Integer renvoi le refOrdre de la référence créée
     * @throws CreateReferenceFailedException
     * @throws ReferenceNotFoundException
     */
    public Integer createReference(String libelle, String code, String agtLogin) throws CreateReferenceFailedException, ReferenceNotFoundException {
        if (agtLogin == null || "".equals(agtLogin))
            throw new CreateReferenceFailedException("L'agtLogin est null ou inconnu");
        try {
            if (referenceForCode(code) != null)
                throw new CreateReferenceFailedException("Code deja utilise");
            if (referenceForLibelleAndAgtLogin(libelle, agtLogin) != null)
                throw new CreateReferenceFailedException("Libelle deja utilise");
        } catch (ReferenceNotFoundException e) {
        }

        Reference ref = new Reference();
        ref.setAgtLogin(agtLogin);
        ref.setRefCode(code);
        ref.setRefLibelle(libelle);
        ref.setRefSeq(new Integer(1));
        try {
            ec.insertObject(ref);
            ec.saveChanges();
        } catch (Exception e) {
            ec.revert();
            throw new CreateReferenceFailedException(e.getMessage());
        }
        return ref.refOrdre();
    }

    /**
     * Renvoi la liste des références d'un individu classé par ordre d'utilisation
     * 
     * @param persId
     *            persId de la personne
     * @return la liste des {@link GediReference}
     * @throws Exception
     *             {@link ReferenceNotFoundException}
     */
    public NSArray listReferenceForAgent(Number persId) throws ReferenceNotFoundException {
        CktlUserInfoDB user = new CktlUserInfoDB(((CktlWebApplication) CktlWebApplication.application()).dataBus());
        user.compteForPersId(persId, true);
        if (user.hasError())
            throw new ReferenceNotFoundException(user.errorMessage());
        return listReferenceForAgent(user.login());
    }

    /**
     * Renvoi la liste des références d'un individu classé par ordre d'utilisation
     * 
     * @param agtLogin
     *            le login de l'agent
     * @return la liste des {@link GediReference}
     */
    public NSArray listReferenceForAgent(String agtLogin) {
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("agtLogin = %@", new NSArray(agtLogin));
        NSMutableArray sort = new NSMutableArray();
        sort.addObject(EOSortOrdering.sortOrderingWithKey("refSeq", EOSortOrdering.CompareDescending));
        return GediReference.initListRef(criApp.dataBus().fetchArray(ec, "GB_Reference", qual, sort, true));
    }

    /**
     * La reference la plus utilisé de l'agent
     * 
     * @param agtLogin
     *            login de l'agent
     * @return la {@link GediReference}la plus utilisé de l'agent
     * @throws DefaultReferenceNotFoundException
     */
    public GediReference defaultReferenceForAgent(String agtLogin) throws DefaultReferenceNotFoundException {
        NSArray list = listReferenceForAgent(agtLogin);
        if (list == null || list.count() == 0)
            throw new DefaultReferenceNotFoundException(null);
        return (GediReference) list.objectAtIndex(0);
    }

    /**
     * La reference la plus utilisé de l'agent
     * 
     * @param persId
     *            persId de l'agent
     * @return la {@link GediReference}la plus utilisé de l'agent
     * @throws ReferenceNotFoundException
     * @throws DefaultReferenceNotFoundException
     */
    public GediReference defaultReferenceForAgent(Number persId) throws DefaultReferenceNotFoundException, ReferenceNotFoundException {
        if (persId == null)
            throw new DefaultReferenceNotFoundException("PersId est null");
        NSArray list = listReferenceForAgent(persId);
        if (list == null || list.count() == 0)
            throw new DefaultReferenceNotFoundException("L'agent n'a pas de reference");
        return (GediReference) list.objectAtIndex(0);
    }
    
    private Reference referenceForLibelleAndAgtLogin(String libelle, String agtLogin) throws ReferenceNotFoundException {
        NSMutableArray args = new NSMutableArray();
        args.addObject(EOQualifier.qualifierWithQualifierFormat("agtLogin =%@", new NSArray(agtLogin)));
        args.addObject(EOQualifier.qualifierWithQualifierFormat("refLibelle = %@", new NSArray(libelle)));
        Reference ref = (Reference) criApp.dataBus().fetchObject(ec, "GB_Reference", new EOAndQualifier(args));
        if (ref == null)
            throw new ReferenceNotFoundException("Aucun reference '" + libelle + "' pour l'agent '" + agtLogin + "'");
        return ref;
    }

    private Reference referenceForCode(String code) throws ReferenceNotFoundException {
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("refCode = %@", new NSArray(code));
        Reference ref = (Reference) criApp.dataBus().fetchObject(ec, "GB_Reference", qual);
        if (ref == null)
            throw new ReferenceNotFoundException("Aucun reference pour le code '" + code + "'");
        return ref;
    }

    /**
     * Renvoi la référence dont le refOrdre est passé en parametre
     * 
     * @param refOrdre
     *            refOrdre de la reference
     * @return la {@link GediReference}correspondante
     * @throws ReferenceNotFoundException
     */
    public GediReference referenceForRefOrdre(Integer refOrdre) throws ReferenceNotFoundException {
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("refOrdre = %@", new NSArray(refOrdre));
        Reference ref = (Reference) criApp.dataBus().fetchObject(ec, "GB_Reference", qual);
        if (ref == null)
            throw new ReferenceNotFoundException("Aucun reference pour le refOrdre '" + refOrdre + "'");
        GediReference gediRef = new GediReference(ref);
        return gediRef;
    }

    //--------------------------------------------------Gestion des Agents-----------------------------------------------

    private Agents agentForPersId(Number persId) throws AgentNotFoundException {
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(persId));
        Agents agt = (Agents) criApp.dataBus().fetchObject(ec, "GB_Agents", qual);
        CktlLog.trace("***GEDIBUS agt = " + agt);
        if (agt == null || agt.agtSeq() == null)
            throw new AgentNotFoundException(null);
        return agt;
    }

    /**
     * Création d'un nouvel agent
     * 
     * @param persId
     *            de l'agent
     * @param estAgentTout
     *            s'il doit être créer comme agent tout
     * @throws CreateAgentFailedException
     */
    public void createAgentForPersId(Integer persId, boolean estAgentTout) throws CreateAgentFailedException {
        try {
            agentForPersId(persId);
            throw new CreateAgentFailedException("l'agent existe deja");
        } catch (Exception e) {
            //CRéation de l'agent
        }
        Agents agt = new Agents();
        agt.setAgtSeq(new Integer(1));
        agt.setPersId(persId);
        try {
            ec.insertObject(agt);
            if (!estAgentTout)
                ec.saveChanges();
        } catch (Exception e) {
            ec.revert();
            throw new CreateAgentFailedException(e.getMessage());
        }
        if (estAgentTout) {
            AgentsTout agtTout = new AgentsTout();
            agtTout.setPersId(persId);
            try {
                ec.insertObject(agtTout);
                ec.saveChanges();
            } catch (Exception e) {
                ec.revert();
                throw new CreateAgentFailedException(e.getMessage());
            }
        }
    }

    //----------------------------------------------Gestion des TypeCourrierNew----------------------------------------
    /**
     * Création d'un type de courrier
     * 
     * @param libelle
     *            libellé du type
     * @param libelleHtml
     *            libellé html du type
     * @param pageUrl
     *            url à afficher à la place du contenu du type
     * @param typePere
     *            le type pere s'il existe (avec une verification), ou {@link GediBus#TYPE_SANS_PERE}
     * @param typeFamille
     * @param typeConsultation
     *            defini la visibilité sur le web du type (et des docs assossiées)
     * @param typeVisibleAppli
     *            defini si le type est visible sur le web
     * @param typeDocumentId
     *            identifiant des documents déposés sur le serveur web par GEDFS
     * @param refOrdre
     * @param cleDate
     * @return le typeOrdreChar du type créé
     * @throws CreateTypeCourrierNewFailedException
     * @throws TypeCourrierNewNotFoundException
     */
    public String createTypeCourrierNew(String libelle, String libelleHtml, String pageUrl, String typePere, String typeFamille, Integer typeConsultation, String typeVisibleAppli,
            Integer typeDocumentId, Integer refOrdre, Integer cleDate) throws TypeCourrierNewNotFoundException, CreateTypeCourrierNewFailedException {

        if (typePere != null && typeCourrierNewForTypeOrdreChar(typePere) == null)
            throw new CreateTypeCourrierNewFailedException("Le type pere " + typePere + " n'existe pas");
        return createRacineTypeCourrierNew(libelle, libelleHtml, pageUrl, typePere, typeFamille, typeConsultation, typeVisibleAppli, typeDocumentId, refOrdre, cleDate);
    }

    /**
     * Création d'un type de courrier comme Racine GEDI.
     * 
     * @param libelle
     *            libellé du type
     * @param libelleHtml
     *            libellé html du type
     * @param pageUrl
     *            url à afficher à la place du contenu du type
     * @param typePere
     *            le type pere s'il existe (avec une verification), ou {@link GediBus#TYPE_SANS_PERE}
     * @param typeFamille
     * @param typeConsultation
     *            defini la visibilité sur le web du type (et des docs assossiées)
     * @param typeVisibleAppli
     *            defini si le type est visible sur le web
     * @param typeDocumentId
     *            identifiant des documents déposés sur le serveur web par GEDFS
     * @param refOrdre
     * @param cleDate
     * @return le typeOrdreChar du type créé
     * @throws CreateTypeCourrierNewFailedException
     *             si la création du type de courrier est impossible
     */
    public String createRacineTypeCourrierNew(String libelle, String libelleHtml, String pageUrl, String typePere, String typeFamille, Integer typeConsultation, String typeVisibleAppli,
            Integer typeDocumentId, Integer refOrdre, Integer cleDate) throws CreateTypeCourrierNewFailedException {

        TypeCourrierNew type = new TypeCourrierNew();
        type.setCleDates(cleDate);
        type.setRefOrdre(refOrdre);
        type.setTypeCode(typePere);
        type.setTypeDocumentId(typeDocumentId);
        type.setTypeFamille(typeFamille);
        type.setTypeLibelle(libelle);
        type.setTypePageTitre(libelleHtml);
        type.setTypePageUrl(pageUrl);
        type.setTypeTypeConsultation(typeConsultation);
        type.setTypeVisibleAppli(typeVisibleAppli);
        type.setTypeOrdreChar(VIDE);

        try {
            ec.insertObject(type);
            ec.saveChanges();
        } catch (Exception e) {
            throw new CreateTypeCourrierNewFailedException(e.getMessage());
        }
        try {
            type.setTypeOrdreChar("" + type.typeOrdre().intValue());
            ec.saveChanges();
        } catch (Exception e) {
            throw new CreateTypeCourrierNewFailedException("Attention : le typeOrdreChar du Type n'est pas rempli !!!");
        }
        return type.typeOrdreChar();
    }

    /**
     * Permet d'obtenir un GediTypeCourrierNew pour un typeOrdreChar donné
     * 
     * @param typeOrdreChar
     *            le typeOrdreChar du type recherché
     * @return le {@link GediTypeCourrierNew}correspondant
     * @throws TypeCourrierNewNotFoundException
     */
    public GediTypeCourrierNew typeCourrierNewForTypeOrdreChar(String typeOrdreChar) throws TypeCourrierNewNotFoundException {
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("typeOrdreChar = %@", new NSArray(typeOrdreChar));
        NSArray list = typeCourrierNewForQualifierAndSort(qual, null);
        if (list == null || list.count() == 0)
            throw new TypeCourrierNewNotFoundException("aucun type trouvé pour le typeOrdreChar " + typeOrdreChar);
        return (GediTypeCourrierNew) list.lastObject();
    }

    /**
     * Permet d'obtenir une liste de {@link GediTypeCourrierNew}pour un qualifier et un tri donné
     * 
     * @param qual
     *            est le qualifier a utiliser pour la recherche
     * @param sort
     *            est un array de EOSortOrdering pour le tri des réponses à la recherche
     * @return NSArray de {@link GediTypeCourrierNew}
     * @throws TypeCourrierNewNotFoundException
     *             si il n'y a pas de type de courrier corespondant
     */
    public NSArray typeCourrierNewForQualifierAndSort(EOQualifier qual, NSArray sort) throws TypeCourrierNewNotFoundException {
        return GediTypeCourrierNew.initListType(criApp.dataBus().fetchArray(ec, "GB_TypeCourrierNew", qual, sort, false));
    }

    /**
     * Suppression du type de courrier corespondant. <br>
     * Pas de suppression si le type possède un sous-type ou un courrier.
     * 
     * @param typeOrdreChar
     *            du courrier à supprimer
     * @throws DeleteTypeCourrierNewException
     *             si la suppressin est impossible
     * @throws TypeCourrierNewNotFoundException
     */
    public void removeTypeCourrierNew(String typeOrdreChar) throws DeleteTypeCourrierNewException, TypeCourrierNewNotFoundException {
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("typeCode=%@", new NSArray(typeOrdreChar));
        NSArray list = null;
        try {
            list = typeCourrierNewForQualifierAndSort(qual, null);
        } catch (TypeCourrierNewNotFoundException e) {
        }
        if (list != null && list.count() > 0)
            throw new DeleteTypeCourrierNewException("ce type possède des sous-types");
        qual = EOQualifier.qualifierWithQualifierFormat("typesAssocies.typeCourrierNew.typeOrdreChar=%@", new NSArray(typeOrdreChar));
        try {
            list = courrierForQualifierAndSort(qual, null);
        } catch (CourrierNotFoundException e) {
        }
        if (list != null && list.count() > 0)
            throw new DeleteTypeCourrierNewException("ce type possède des courriers");
        TypeCourrierNew type = typeCourrierNewForTypeOrdreChar(typeOrdreChar).getTypeCourrierNew();

        try {
            ec.deleteObject(type);
            ec.saveChanges();
        } catch (Exception e) {
            ec.revert();
            throw new DeleteTypeCourrierNewException(e.getMessage());
        }
    }

    //------------------------------------------------Gestion des typesAssocies----------------------------------------------

    /**
     * Permet d'associer un courrier à un type de courrier
     * 
     * @param cou
     *            Gedicourrier a attacher au type
     * @param type
     *            GediTypeCourrierNew auquel on rattache le courrier
     * @throws CourrierPrimaryKeyNotFoundException
     * @throws CreateTypeAssociesFailedException
     *             si la création de l'association est impossible
     */
    public void createTypeAssocies(GediCourrier cou, GediTypeCourrierNew type) throws CourrierPrimaryKeyNotFoundException, CreateTypeAssociesFailedException {
        TypesAssocies asso = new TypesAssocies();
        try {
            asso.setCourrier(cou.getCourrier());
            asso.setCouNumero(cou.couNumero());
//            asso.setTypeCourrierNew(new NSMutableArray(type.getTypeCourrierNew()));
            asso.setTypeCourrierNew(type.getTypeCourrierNew());
            asso.setTypeRecord(Integer.valueOf(type.typeOrdreChar()));
		} catch (Throwable e) {
			e.printStackTrace();
		}
        try {
            ec.insertObject(asso);
            ec.saveChanges();
        } catch (Exception e) {
            ec.revert();
            throw new CreateTypeAssociesFailedException(e.getMessage());
        }
    }

    /**
     * Suppression de l'association entre un courrier et un type de courrier. <br>
     * La suppression ne peut pas ce faire, si il n'y a pas d'autre type d'associé au courrier
     * 
     * @param cou
     *            GediCourrier concerné
     * @param type
     *            GediTypeCourrierNew concerné
     * @throws DeleteTypeAssociesFailedException
     *             si la suppression est impossible
     * @throws CourrierPrimaryKeyNotFoundException
     */
    public void removeTypeAssocies(GediCourrier cou, GediTypeCourrierNew type) throws DeleteTypeAssociesFailedException, CourrierPrimaryKeyNotFoundException {
        //        System.out.println("1");
        if (cou == null || type == null)
            throw new DeleteTypeAssociesFailedException(null);
        NSArray list = listTypeAssociesForCouNumeroOrForTypeOrdreChar(cou.couNumero(), null);
        //        System.out.println("2 ="+list.count());
        if (list != null && list.count() <= 1)
            throw new DeleteTypeAssociesFailedException("il doit rester au moins 1 type associes à ce courrier");
        //        System.out.println("3");
        TypesAssocies asso = getTypeAssociesForCouNumeroAndTypeOrdreChar(cou.couNumero(), type.typeOrdreChar());
        //        System.out.println("4");
        try {
            ec.deleteObject(asso);
            ec.saveChanges();
        } catch (Exception e) {
            ec.revert();
            throw new DeleteTypeAssociesFailedException(e.getMessage());
        }
        //        System.out.println("5");
    }

    /**
     * Permet de 'deplacer' un courrier d'un type vers un autre.
     * 
     * @param cou
     *            GediCourrier à déplacer
     * @param oldType
     *            GediTypeCourrierNew de l'ancienne position
     * @param newType
     *            GediTypeCourrierNew de la destination
     * @throws ReplaceTypeAssociesFailedException
     *             si le déplacement est impossible
     * @throws CourrierPrimaryKeyNotFoundException
     * @throws CourrierPrimaryKeyNotFoundException
     * @throws DeleteTypeAssociesFailedException
     */
    public void replaceTypeAssocies(GediCourrier cou, GediTypeCourrierNew oldType, GediTypeCourrierNew newType) throws ReplaceTypeAssociesFailedException, DeleteTypeAssociesFailedException,
            CourrierPrimaryKeyNotFoundException {
        if (cou == null || oldType == null || newType == null)
            throw new ReplaceTypeAssociesFailedException(null);
        try {
            createTypeAssocies(cou, newType);
        } catch (Exception e) {
            throw new ReplaceTypeAssociesFailedException(e.getMessage());
        }
        try {
            removeTypeAssocies(cou, oldType);
        } catch (Exception e) {
            removeTypeAssocies(cou, newType);
            throw new ReplaceTypeAssociesFailedException(e.getMessage());
        }

    }

    private TypesAssocies getTypeAssociesForCouNumeroAndTypeOrdreChar(Number couNumero, String typeOrdreChar) {
        return (TypesAssocies) listTypeAssociesForCouNumeroOrForTypeOrdreChar(couNumero, typeOrdreChar).lastObject();
    }

    private NSArray listTypeAssociesForCouNumeroOrForTypeOrdreChar(Number couNumero, String typeOrdreChar) {
        NSMutableArray args = new NSMutableArray();
        if (couNumero != null)
            args.addObject(EOQualifier.qualifierWithQualifierFormat("couNumero=%@", new NSArray(couNumero)));
        if (typeOrdreChar != null)
            args.addObject(EOQualifier.qualifierWithQualifierFormat("typeRecord=%@", new NSArray(typeOrdreChar)));
        return criApp.dataBus().fetchArray(ec, "GB_TypesAssocies", new EOAndQualifier(args), null, false);
    }

    /**
     * Permet d'avoir la liste des types associes à un courrier
     * 
     * @param couNumero
     *            le courrier pour lequel on veut connaitre les types associes
     * @return NSArray de GediTypeCourrierNew
     * @throws TypeCourrierNewNotFoundException
     *             si le courrier n'est pas associà a un type.
     */
    public NSArray typeCourrierForCourrier(Number couNumero) throws TypeCourrierNewNotFoundException {
        return GediTypeCourrierNew.initListType((NSArray) listTypeAssociesForCouNumeroOrForTypeOrdreChar(couNumero, null).valueForKey("typeCourrierNew"));
    }

    /**
     * Permet d'avoir la liste des courriers associes à un type
     * 
     * @param typeOrdreChar
     *            le type pour lequel on veut connaitre les courriers associes
     * @return NSArray de GediCourrier
     * @throws CourrierNotFoundException
     */
    public NSArray courrierForTypeCourrier(String typeOrdreChar) throws CourrierNotFoundException {
        return GediCourrier.initListCourrier((NSArray) listTypeAssociesForCouNumeroOrForTypeOrdreChar(null, typeOrdreChar).valueForKey("courrier"));
    }

    //------------------------------------------------Gestion des droits----------------------------------------------

    /**
     * Permet de savoir si l'individu à le droit de lire/modifié le courrier,
     * 
     * @param couNumero
     *            numéro du courrier à tester
     * @param persId
     *            persId de l'individu
     * @param modification
     *            permet de chercher si la personne a le droit de modifier le courrier <br>
     *            Utiliser {@link #DROIT} pour tester s'il a le droit <br>
     *            Utiliser {@link #PAS_DROIT} pour tester s'il n'a pas le droit <br>
     *            Utiliser null pour tester seulement le droit de lecture
     * @return <strong>true </strong> si, pour la lecture :
     *         <ul>
     *         <li>soit l'individu est agent tout</li>
     *         <li>soit l'individu est superAdmin</li>
     *         <li>soit l'individu est l'auteur</li>
     *         <li>soit l'individu est un des destinataires du courrier</li>
     *         <li>soit le courrier est extranet, donc a destination de toute personne authentifiee</li>
     *         <li>soit le courrier est Intranet et l'individu appartient au groupe INTRANET, une structure explicitement creee</li>
     *         </ul>
     *         sinon <strong>false </strong> <br>
     *         Et pour la modif :
     *         <ul>
     *         <li>soit l'individu est l'auteur</li>
     *         <li>soit l'individu est agent tout</li>
     *         <li>soit l'individu est superAdmin</li>
     *         <li>soit l'individu est  destinataires du courrier, avec le tag <code>destDroitModif</code> = "O"</li>
     *         </ul>
     *         sinon <strong>false </strong> <br>
     *         <strong>Attention :<br>
     *         c'est à l'utilisateur du framework de gérer la priorité entre les droits sur les courriers et les droits sur les types. Pour l'instant les droits de groupes sur les droits de courriers
     *         ne sont pas implémenté !!! </strong>
     * 
     * @see #isAgentTout(Number)
     * @see #isSuperUser(Number)
     * @see #droitForTypeAndForPersId(Number, String, String, String, String)
     * @see #droitCreateTypeForType(Number, String)
     * @see #DROIT
     * @see #PAS_DROIT
     */
    public boolean droitForCourrier(Integer couNumero, Integer persId, String modification) {
        if (isAgentTout(persId) || isSuperUser(persId))
            return true;
        
        NSMutableArray args2 = new NSMutableArray();
        
        // recherche si le persId est l'auteur du courrier
        args2.addObject(EOQualifier.qualifierWithQualifierFormat("couNumero =%@", new NSArray(couNumero)));
//        args2.addObject(EOQualifier.qualifierWithQualifierFormat("persId =%@", new NSArray(persId)));
        NSArray array = criApp.dataBus().fetchArray(ec, "GB_Courrier", new EOAndQualifier(args2), null);
        org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier courrier = null;
        if ( (array != null) && (array.count() > 0)) {
        	courrier = (Courrier)array.objectAtIndex(0);
        	if ( (persId != null) && ((courrier.persId().intValue() == persId.intValue())) )
        		return true;
        }

        // Si on cherche uniquement le droit de lecture (pas de droit de modif : modification = null) :
        // droit de lecture Si : "le courrier est extranet" ou "le courrier Intranet et utilisateur Intranet"
        if ( (modification == null) && (courrier != null) ) {
        	if ( courrier.isExtranet())
        		return true;
        	// si courrier Intranet l'utilisateur doit faire partie du groupe Intranet
        	// Verifier d'abord si la structure 'INTRANET' existe dans le fichier de config 
        	if  ( courrier.isIntranet()) {
        		String cStructureIntranet = criApp.config().stringForKey("C_STRUCTURE_INTRANET");
        		if ( cStructureIntranet != null) {
        			EOStructure structureIntranet = EOStructure.structurePourCode(ec, cStructureIntranet);
        			if ( structureIntranet != null) {
        				// On verifie si l'utilisateur appartient a cette structure
        				EOIndividu individu = EOIndividu.individuWithPersId(ec, persId);
        				if ( individu != null) {
        					boolean b = (hierarchicalGroupesForIndividu(individu)).indexOfObject(structureIntranet) >= 0;
        					if ( b)
        						return true;
        				}
        			}
        		}
        	}
        }
        
        // Recherche dans la table des droits si l'individu a un droit (il est directement destinataire)
//        NSMutableArray args = new NSMutableArray();
//        NSMutableArray listDroit = new NSMutableArray();
//        if (modification != null)
//            args.addObject(EOQualifier.qualifierWithQualifierFormat("destDroitModif =%@", new NSArray(DROIT)));
//        args.addObject(EOQualifier.qualifierWithQualifierFormat("couNumero =%@", new NSArray(couNumero)));
//        args.addObject(EOQualifier.qualifierWithQualifierFormat("persIdClient =%@", new NSArray(persId)));
//        listDroit.addObjectsFromArray(criApp.dataBus().fetchArray(ec, "VDestinataireClient", new EOAndQualifier(args), null));
//        if ( listDroit.count() > 0)
//        	return true;

        // Rechrche si l'utilisateur est destinataire avec le droit donne
        if ( courrier != null) {
        	Boolean bDroit = null;
        	if ( modification != null) 
        		bDroit = new Boolean(true);
        	if ( courrier.isIndividuDestinataireWithDroit( persId, bDroit) != null) 
        		return true;
        }
        
        return false;
    }

    /**
     * Permet de savoir si une personne a les droits de :
     * <ul>
     * <li>Lecture</li>
     * <li>Modification</li>
     * <li>Suppression</li>
     * </ul>
     * sur les courriers du répertoire testé. Si la personne est superUser, elle a tout les droits. Si la personne est agentTout, elle a tout les droits, dans les répertoires où elle a droit de
     * lecture.
     * 
     * @param persId
     *            persId de l'individu à tester
     * @param typeOdreChar
     *            numero du type à tester
     * @param lecture
     *            utiliser {@link #DROIT}pour savoir si la personne a le droit de lecture, sinon {@link #PAS_DROIT}
     * @param modification
     *            utiliser {@link #DROIT}pour savoir si la personne a le droit de modification, sinon {@link #PAS_DROIT}
     * @param suppression
     *            utiliser {@link #DROIT}pour savoir si la personne a le droit de suppression, sinon {@link #PAS_DROIT}
     * @return <strong>true </strong> si la personne a le droit de lecture/modification/suppression sur les courriers du type dont le numéro est passé en paramètre... <strong>false </strong> sinon
     *         <br>
     *         <br>
     *         <strong>ATTENTION: les droits individuels et de groupes de la personne sont pris en compte. <br>
     *         Mais c'est à l'utilisateur du framework de gérer la priorité entre les droits sur les courriers et les droits sur les types </strong>
     * 
     * @see #isAgentTout(Number)
     * @see #isSuperUser(Number)
     * @see #droitForCourrier(Integer, Number, String)
     * @see #droitCreateTypeForType(Number, String)
     * @see #DROIT
     * @see #PAS_DROIT
     */
    public boolean droitForTypeAndForPersId(Integer persId, String typeOdreChar, String lecture, String modification, String suppression) {
        if (persId == null || typeOdreChar == null)
            return false;
        if (isSuperUser(persId))
            return true;
        boolean agtTout = isAgentTout(persId);
//        NSMutableArray args = new NSMutableArray();
//        args.addObject(EOQualifier.qualifierWithQualifierFormat("persIdClient = %@", new NSArray(persId)));
//        args.addObject(EOQualifier.qualifierWithQualifierFormat("typeOrdreChar = %@", new NSArray(typeOdreChar)));
//        if (lecture != null)
//            args.addObject(EOQualifier.qualifierWithQualifierFormat("dLec = %@", new NSArray(lecture)));
//        if (modification != null && !agtTout)
//            args.addObject(EOQualifier.qualifierWithQualifierFormat("dMod = %@", new NSArray(modification)));
//        if (suppression != null && !agtTout)
//            args.addObject(EOQualifier.qualifierWithQualifierFormat("dSup = %@", new NSArray(suppression)));
//        NSArray list = criApp.dataBus().fetchArray(ec, "VDroitClient", new EOAndQualifier(args), null);
//        if (list == null)
//            return false;
//        return list.count() > 0;
        
        boolean b = false;
		EOIndividu individu = EOIndividu.individuWithPersId(ec, persId);
		if ( individu != null) {
			if ( agtTout)
				b = isDroitHierarchicOnType(individu, typeOdreChar, lecture, null, null);
			else
				b = isDroitHierarchicOnType(individu, typeOdreChar, lecture, modification, suppression);
		}
		        
        return b;
    }

    /**
     * Permet de savoir si une personne a le droit de créer / modifier / supprimer un type de l'arboraisance de GEDI
     * 
     * @param persId
     *            persId de la personne à tester
     * @param typeOdreChar
     *            numéro du type père dans le cas d'une création, et numéro du type à modifier / supprimer
     * @return <strong>true </strong> s'il a le droit et <strong>false </strong> sinon <br>
     *         <br>
     *         <strong>Attention: pour la suppression du type, il faut passer par {@link #removeTypeCourrierNew(String)}car il se peut que le type ne puisse pas être supprimé !!! </strong>
     * 
     * @see #isAgentTout(Number)
     * @see #isSuperUser(Number)
     * @see #droitForCourrier(Integer, Number, String)
     * @see #droitForTypeAndForPersId(Number, String, String, String, String)
     * @see #DROIT
     * @see #PAS_DROIT
     */
    public boolean droitCreateTypeForType(Number persId, String typeOdreChar) {
        if (persId == null)
            return false;
        if (typeOdreChar == null)
            return isSuperUser(persId);
        NSMutableArray args = new NSMutableArray();
        args.addObject(EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(persId)));
        args.addObject(EOQualifier.qualifierWithQualifierFormat("typeOrdreChar = %@", new NSArray(typeOdreChar)));
        NSArray list = criApp.dataBus().fetchArray(ec, "VDroitCreateType", new EOAndQualifier(args), null);
        return list.count() > 0;
    }

    /**
     * Permet de savoir si une personne est agentTout
     * 
     * @param persId
     *            de la personne à tester
     * @return <strong>true </strong> si le personne est agent tout, <strong>false </strong> sinon.
     * @see #droitForTypeAndForPersId(Number, String, String, String, String)
     * @see #isSuperUser(Number)
     * @see #droitForCourrier(Integer, Number, String)
     * @see #droitCreateTypeForType(Number, String)
     */
    public boolean isAgentTout(Number persId) {
        if (persId == null)
            return false;
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(persId));
        NSArray listDroit = criApp.dataBus().fetchArray(ec, "GB_AgentsTout", qualifier, null);
        return listDroit.count() > 0;
    }

    /**
     * Permet de savoir si une personne est superUser
     * 
     * @param persId
     *            de la personne à tester
     * @return <strong>true </strong> si le personne est superUser, <strong>false </strong> sinon.
     * 
     * @see #isAgentTout(Number)
     * @see #droitForTypeAndForPersId(Number, String, String, String, String)
     * @see #droitForCourrier(Integer, Number, String)
     * @see #droitCreateTypeForType(Number, String)
     */
    public boolean isSuperUser(Number persId) {
        if (persId == null)
            return false;
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(persId));
        NSArray listDroit = criApp.dataBus().fetchArray(ec, "GB_Superuser", qualifier, null);
        return listDroit.count() > 0;
    }

    /**
     * Verifie si un individu par son persId appartient au groupe INTRANET. Si le groupe INTRANET n'existe pas
     * on verifie si l'utilisateur est un Personnel de l'etablissement (vlan = "P")
     * @param persId
     * @return true si la personne appartient au groupe INTRANET.
     */
    public boolean isUserIntranet( Integer persId) {
    	boolean b = false;
    	if  ( persId != null) {
    		String cStructureIntranet = criApp.config().stringForKey("C_STRUCTURE_INTRANET");
    		if ( cStructureIntranet != null) {
    			EOStructure structureIntranet = EOStructure.structurePourCode(ec, cStructureIntranet);
    			if ( structureIntranet != null) {
    				// On verifie si l'utilisateur appartient a cette structure
    				EOIndividu individu = EOIndividu.individuWithPersId(ec, persId);
    				if ( individu != null) {
    					b = (hierarchicalGroupesForIndividu(individu)).indexOfObject(structureIntranet) >= 0;
    				}
    			} else {
        			try {
            			CktlUserInfo userInfo = userInfoForPersId (persId);

            			if ( userInfo!= null) {
            				if ( userInfo.vLan() != null)
            					b = userInfo.vLan().equals("P");
            			}
            			
    				} catch (Exception e) {
    					b = false;
    				}
    			}
    		}
    	}
    	return b;
    }
    
    /**
     * Renvoie CktlUserInfo pour un persId
     * @param persId
     * @return {@link CktlUserInfo} de l'individu corespondant.
     * @throws ReferenceNotFoundException
     */
    public CktlUserInfo userInfoForPersId (Number persId) throws ReferenceNotFoundException  {

            CktlUserInfoDB user = new CktlUserInfoDB(((CktlWebApplication) CktlWebApplication.application()).dataBus());
            user.compteForPersId(persId, true);
            if (user.hasError())
                throw new ReferenceNotFoundException(user.errorMessage());
            
            return user;
    }
    
    /**
     * Renvoi la list des "agtType" des types pour lesquel, la personne a des droits (consultation)
     * 
     * @param persId
     *            de la personne
     * @return {@link NSArray}de String
     * 
     * @throws TypeCourrierNewNotFoundException
     *             si il n'y a pas de type pour cette personne
     */
    public NSArray typeForDroitConsultationAndPersId(Integer persId) throws TypeCourrierNewNotFoundException {
//        if (persId == null)
//            throw new TypeCourrierNewNotFoundException("le persId est null!!!");
//        NSMutableArray args = new NSMutableArray();
//        args.addObject(EOQualifier.qualifierWithQualifierFormat("persIdClient=%@", new NSArray(persId)));
//        NSMutableArray sort = new NSMutableArray();
//        sort.addObject(EOSortOrdering.sortOrderingWithKey("agtType", EOSortOrdering.CompareAscending));
//        NSArray list = criApp.dataBus().fetchArray(ec, "VDroitConsultClient", new EOAndQualifier(args), sort);
//        if (list == null || list.count() == 0)
//            throw new TypeCourrierNewNotFoundException("Aucun droit sur aucun type trouve pour le persId " + persId.intValue());
//        return (NSArray) list.valueForKey("agtType");

    	NSArray array = null;
    	if ( (persId != null) && (persId.intValue() > 0) ) {
    	    EOIndividu individu = EOIndividu.individuWithPersId(ec, persId);
    		if ( individu != null) {
    			array = droitHierarchicAgentTypes(individu);
    		}
    	}
    	
    	if ( (array != null) && (array.count() > 0) ) {
    		return (NSArray)array.valueForKey("agtType");	
//    		return array;
    	}
    	else
    		throw new TypeCourrierNewNotFoundException("Aucun droit sur aucun type trouve pour le persId " + persId.intValue());
    }

    //------------------------------------------------Pour la gestion du multilangue------------------------------------------------
    /**
     * Permet d'obtenir un objet {@link GediLangue}pour un code langue donné
     * 
     * @param code
     *            le code de la langue que l'on souhaite
     * @return le {@link GediLangue}corespondant, s'il existe
     * 
     * @throws LangueNotFoundException
     *             si la langue n'existe pas
     * @see GediLangue#CODE_LANGUE_FRANCE
     */
    public GediLangue langueForCode(String code) throws LangueNotFoundException {
        if (code == null)
            throw new LangueNotFoundException("Aucune langue trouvée pour le code " + code);
        EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("cLangue = %@", new NSArray(code));
        Langue langue = (Langue) criApp.dataBus().fetchArray(ec, "GB_Langue", qualifier, null, false, false).lastObject();
        if (langue == null)
            throw new LangueNotFoundException("Aucune langue trouvée pour le code " + code);
        return new GediLangue(langue);
    }

    //------------------------------------------------Pour la gestion des fichiers / urls------------------------------------------------

    /**
     * Permet d'obtenir les referenceliens associé au courrier
     * 
     * @param courrier
     *            le {@link GediCourrier}pour lequel on veut les referenceLien
     * @param type
     *            permet de filtrer le type de reference lien que l'on veut (cf ci-dessous)
     * @return {@link NSArray}de {@link GediReferenceLien}
     * 
     * @see GediReferenceLien#AUCUN_TYPE
     * @see GediReferenceLien#TYPE_FICHIER
     * @see GediReferenceLien#TYPE_LIEN
     * @throws ReferenceLienNotFoundException
     */
    public NSArray referenceLienForCourrier(GediCourrier courrier, String type) throws ReferenceLienNotFoundException {
        CktlLog.trace("***GEDIBUS consultation d'une liste de REFERENCE_LIEN");
        NSArray list = GediReferenceLien.initListRefLien(courrier.getCourrier().referenceLiens());
        if (type == GediReferenceLien.AUCUN_TYPE)
            return list;
        return EOQualifier.filteredArrayWithQualifier(list, EOQualifier.qualifierWithQualifierFormat("rflType=%@", new NSArray(type)));
    }

    /**
     * Permet d'obtenir le {@link GediReferenceLien}associé au rflOrdre
     * 
     * @param rflOrdre
     *            du refLien pour lequel on veut le {@link GediReferenceLien}
     * @return {@link GediReferenceLien}
     * 
     * @throws ReferenceLienNotFoundException
     */
    public GediReferenceLien referenceLienForRflOrdre(Number rflOrdre) throws ReferenceLienNotFoundException {
        CktlLog.trace("***GEDIBUS Consultation d'un REFERENCE_LIEN");
        if (rflOrdre == null)
            throw new ReferenceLienNotFoundException("le rflOrdre est null");
        NSMutableArray args = new NSMutableArray();
        args.addObject(EOQualifier.qualifierWithQualifierFormat("rflOrdre=%@", new NSArray(rflOrdre)));
        ReferenceLien refLien = (ReferenceLien) criApp.dataBus().fetchObject(ec, "GB_ReferenceLien", new EOAndQualifier(args));
        if (refLien == null)
            throw new ReferenceLienNotFoundException("Aucun reflien avec le rflOrdre = " + rflOrdre.intValue());
        return new GediReferenceLien(refLien);
    }

    /**
     * Permet de créer un referenceLien pour un courrier
     * 
     * @param url
     *            url d'accès au document ou url du lien
     * @param docId
     *            id du document dans GEDFS (si il y a)
     * @param type
     *            soit de type {@link GediReferenceLien#TYPE_FICHIER}, soit de type {@link GediReferenceLien#TYPE_LIEN}
     * @param libelle
     *            le libelle du lien ou du fichier
     * @param courrier
     *            le {@link GediCourrier}auquel est rataché le referenceLien
     * @return le rflOrdre du referenceLien
     * 
     * @throws CreateReferenceLienFailedException
     *             si echec de la création du referenceLien
     * @throws CourrierPrimaryKeyNotFoundException
     *             si le courrier n'a pas de clé primaire (ie pas sauvegardé dans la base)
     * @throws ReferenceLienPrimaryKeyNotFoundException
     *             si le refLien créer n'a pas de clé primaire (ie echec du saveChanges)
     */
    public Number createReferenceLienForCourrier(String url, Integer docId, String type, String libelle, GediCourrier courrier) throws CreateReferenceLienFailedException,
            CourrierPrimaryKeyNotFoundException, ReferenceLienPrimaryKeyNotFoundException {
        CktlLog.trace("***GEDIBUS Création d'un REFERENCE_LIEN");
        if (courrier == null)
            throw new CreateReferenceLienFailedException("le courrier est null");
        if (url == null && docId == null)
            throw new CreateReferenceLienFailedException("l'url et la reference sur le fichier ne peuvent pas être tout les deux null");
        if (type == null)
            throw new CreateReferenceLienFailedException("le type de lien ne peut être null");
        ReferenceLien refLien = new ReferenceLien();
        refLien.setCouNumero(courrier.couNumero());
        refLien.setRflDocumentId(docId);
        refLien.setRflLibelle(libelle);
        refLien.setRflLien(url);
        refLien.setRflType(type);
        try {
            ec.insertObject(refLien);
            ec.saveChanges();
            courrier.getCourrier().addObjectToBothSidesOfRelationshipWithKey(refLien, "referenceLiens");
            ec.saveChanges();
        } catch (Throwable e) {
            throw new CreateReferenceLienFailedException(e.getMessage());
        }
        return refLien.rftOrdre();
    }

    /**
     * Suppression d'un referenceLien
     * 
     * @param refLien
     *            le referenceLien à supprimer
     * @param courrier
     *            le courrier auquel est attaché le referenceLien
     * @param removeFile
     *            si de type fichier: si true, suppression du fichier sur GEDFS, sinon suppression simple du referenceLien
     * @param override
     *            si true, suppression du referenceLien même si la suppression GEDFS a échoué. <br>
     *            Sinon pas de suppression du referenceLien si GEDFS a échoué !!!
     * @param noIndividu
     *            no de l'individu qui fait la suppression
     * 
     * @throws DeleteReferenceLienFailedException
     *             si la suppression a échoué
     */
    public void removeReferenceLienForGediReferenceLien(GediReferenceLien refLien, GediCourrier courrier, boolean removeFile, boolean override, Number noIndividu)
            throws DeleteReferenceLienFailedException {
        CktlLog.trace("***GEDIBUS Suppression d'un REFERENCE_LIEN");
        if (refLien == null)
            throw new DeleteReferenceLienFailedException("le GediReferenceLien est null");
        if (removeFile && !GediReferenceLien.TYPE_LIEN.equals(refLien.rflLien()) && refLien.rflDocumentId() != null) {
            try {
                criApp.gedBus().setNoIndividu(noIndividu.intValue());
                criApp.gedBus().deleteDocumentGED(refLien.rflDocumentId().intValue());
            } catch (Throwable e) {
                if (!override)
                    throw new DeleteReferenceLienFailedException(e.getMessage());
            }
        }
        try {
            courrier.getCourrier().removeObjectFromBothSidesOfRelationshipWithKey(refLien.getReferenceLien(), "referenceLiens");
            ec.deleteObject(refLien.getReferenceLien());
            ec.saveChanges();
            return;
        } catch (Throwable e) {
            throw new DeleteReferenceLienFailedException(e.getMessage());
        }
    }
}
