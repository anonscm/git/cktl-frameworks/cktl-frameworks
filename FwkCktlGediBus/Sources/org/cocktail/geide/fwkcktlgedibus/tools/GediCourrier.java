/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.geide.fwkcktlgedibus.tools;



import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.geide.fwkcktlgedibus.exception.CourrierNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.CourrierPrimaryKeyNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.LangueNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.SpecifWebNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.TextMultilangueNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Class à utiliser à la place de {@link org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier}
 * 
 * @author mparadot
 */
public class GediCourrier extends GediObject {
    private Courrier _courrier;
    private static CktlWebApplication criApp = (CktlWebApplication)WOApplication.application(); 
    
    /**
     * Balise ouvrante du chapeau !!!!
     */
	public static String BALISE_CHAPEAU_DEB="<p class=\"chapo\">";
	
	/**
	 * Balise fermante du chapeau
	 */
	public static String BALISE_CHAPEAU_FIN="</p>";

    protected GediCourrier(Courrier aCourrier) {
        super();
        _courrier = aCourrier;
    }

    protected static NSArray initListCourrier(NSArray list) throws CourrierNotFoundException {
        if (list == null || list.count() == 0)
            throw new CourrierNotFoundException(null);
        NSMutableArray array = new NSMutableArray();
        for (int i = 0; i < list.count(); i++) {
            Courrier cou = (Courrier) list.objectAtIndex(i);
            array.addObject(new GediCourrier(cou));
        }
        return array;
    }

    protected Courrier getCourrier() {
        return (Courrier) getObjetMetier();
    }

    //Methode public

    public NSArray listTypeCourrierNewAssocies() throws Exception {
        return GediTypeCourrierNew.initListType((NSArray)_courrier.typesAssocies().valueForKey("typeCourrierNew"));
    }

    public Integer couNumero() throws CourrierPrimaryKeyNotFoundException {
//        System.out.println("counumero invok !!!");
        return _courrier.couNumero();
    }

    public String agtLogin() {
        return _courrier.agtLogin();
    }

    public void setAgtLogin(String value) {
        _courrier.setAgtLogin(value);
    }

    public String cStructure() {
        return _courrier.cStructure();
    }

    public void setCStructure(String value) {
        _courrier.setCStructure(value);
    }

    public Number couIdent() {
        return _courrier.couIdent();
    }

    public void setCouIdent(Integer value) {
        _courrier.setCouIdent(value);
    }

    public NSTimestamp couDateEnreg() {
        return _courrier.couDateEnreg();
    }

    public void setCouDateEnreg(NSTimestamp value) {
        _courrier.setCouDateEnreg(value);
    }

    public NSTimestamp couDateCourrier() {
        return _courrier.couDateCourrier();
    }

    public void setCouDateCourrier(NSTimestamp value) {
        _courrier.setCouDateCourrier(value);
    }

    public NSTimestamp couDateReponseAttendue() {
        return _courrier.couDateReponseAttendue();
    }

    public void setCouDateReponseAttendue(NSTimestamp value) {
        _courrier.setCouDateReponseAttendue(value);
    }

    public NSTimestamp couDateReponseEffectuee() {
        return _courrier.couDateReponseEffectuee();
    }

    public void setCouDateReponseEffectuee(NSTimestamp value) {
        _courrier.setCouDateReponseEffectuee(value);
    }

    public Number couNombreRelance() {
        return _courrier.couNombreRelance();
    }

    public void setCouNombreRelance(Integer value) {
        _courrier.setCouNombreRelance(value);
    }

    public String couDepartArrivee() {
        return _courrier.couDepartArrivee();
    }

    public void setCouDepartArrivee(String value) {
        _courrier.setCouDepartArrivee(value);
    }

    public String couReference() {
        return _courrier.couReference();
    }

    public void setCouReference(String value) {
        _courrier.setCouReference(value);
    }

    public String couExpediteur() {
        return _courrier.couExpediteur();
    }

    public void setCouExpediteur(String value) {
        _courrier.setCouExpediteur(value);
    }

    public String couExpVia() {
        return _courrier.couExpVia();
    }

    public void setCouExpVia(String value) {
        _courrier.setCouExpVia(value);
    }

    public String couExpEtablissement() {
        return _courrier.couExpEtablissement();
    }

    public void setCouExpEtablissement(String value) {
        _courrier.setCouExpEtablissement(value);
    }

    public String couObjet() {
        return _courrier.couObjet();
    }
    
    public String couObjet(GediLangue langue){
        if(langue==null || GediLangue.isLangueFr(langue))
            return _courrier.couObjet();
        GediTexteMultilingue text = null;
        try {
            text = GediTexteMultilingue.gediTextMultilangueForLangue(langue,this);
        } catch (TextMultilangueNotFoundException e) {
            return _courrier.couObjet();
        } catch (CourrierPrimaryKeyNotFoundException e) {
            return _courrier.couObjet();
        }
        return text.tmuTexte();
    }

    public void setCouObjet(String value) {
        if(value!=null)
            _courrier.setCouObjet(value);
    }
    
    public void setCouObjet(String value,GediLangue langue) throws TextMultilangueNotFoundException, CourrierPrimaryKeyNotFoundException {
        if(langue==null || GediLangue.isLangueFr(langue))
        {
            setCouObjet(value);
            return;
        }
        GediTexteMultilingue text = null;
        try {
            text = GediTexteMultilingue.gediTextMultilangueForLangue(langue,this);
        } catch (Exception e) {
            //si il n'existe pas, on le crée
            TexteMultilingue newText = new TexteMultilingue();
            newText.setCLangue(langue.cLangue());
            newText.setCPays(langue.cPays());
            newText.setTmuOrdre(couNumero());
            _courrier.editingContext().insertObject(newText);
            _courrier.editingContext().saveChanges();
            text = new GediTexteMultilingue(newText);
            _courrier.addObjectToBothSidesOfRelationshipWithKey(newText,"gB_TexteMultilingues");
            newText.addObjectToBothSidesOfRelationshipWithKey(langue.getLangue(),"gb_langue");
        }
        text.setTmuTexte(value);
        try {
            _courrier.editingContext().saveChanges();
            _courrier.editingContext().faultForGlobalID(_courrier.editingContext().globalIDForObject(text.getTextMultilangue()),_courrier.editingContext());
            _courrier.editingContext().faultForGlobalID(_courrier.editingContext().globalIDForObject(_courrier),_courrier.editingContext());
        } catch (Exception e) {
            throw new TextMultilangueNotFoundException("Impossible de créer le text multilangue : "+ e.getMessage());
        }
    }

    /**
     * @deprecated
     * utiliser {@link #couCommentaire(GediLangue)}
     * @return renvoi seulement maximum les 4000 premiers caractères du courrier.
     * <br><strong>Pour avoir la TOTALIT&#200; du courrier, vous devez utilisé {@link #couCommentaire(GediLangue)},
     * en précisant la langue ( voir {@link GediBus#langueForCode(String)} )</strong>
     */
    public String couCommentaire() {
        return _courrier.couCommentaire();
    }
    
    public String couCommentaire(GediLangue langue) {
        NSMutableArray args = new NSMutableArray();
        args.addObject(EOQualifier.qualifierWithQualifierFormat("cPays=%@", new NSArray(langue.cPays())));
        args.addObject(EOQualifier.qualifierWithQualifierFormat("cLangue=%@", new NSArray(langue.cLangue())));
        TexteMultilingue textmulti = (TexteMultilingue)(EOQualifier.filteredArrayWithQualifier(_courrier.gB_TexteMultilingues(),new EOAndQualifier(args))).lastObject();
        if(textmulti==null || textmulti.tmuBlobs()==null)
            return _courrier.couCommentaire();
        if(GediLangue.isLangueFr(langue))
        {
            StringBuffer str = new StringBuffer();
            str.append(_courrier.couCommentaire());
            str.append(textmulti.tmuBlobs());
            return str.toString();
        }
        return textmulti.tmuBlobs();
    }

    public void setCouCommentaire(String value) {
        _courrier.setCouCommentaire(value);
    }
    
    /**
     * Attention, cette methode fait un savechanges sur l'editingContext de l'objet
     * @param value texte à enregistrer
     * @param langue la langue a utiliser pour la sauvegarde <br>(si null, seul les 4000 premiers caratères de value seront sauvegardé !!!!)
     * @throws CourrierPrimaryKeyNotFoundException
     * @throws TextMultilangueNotFoundException
     */
    public void setCouCommentaire(String value,GediLangue langue) throws CourrierPrimaryKeyNotFoundException, TextMultilangueNotFoundException {
        if(langue==null)
        {
            _courrier.setCouCommentaire(value);
            return;
        }
        GediTexteMultilingue text = null;
        if(GediLangue.isLangueFr(langue))
        {
            try {
                text = GediTexteMultilingue.gediTextMultilangueForLangue(langue,this);
            } catch (Exception e) {
                if(value!=null && value.length()>=4000)
                {
	                TexteMultilingue newText = new TexteMultilingue();
	                newText.setTmuBlobs(value.substring(4000));
	                newText.setCLangue(langue.cLangue());
	                newText.setCPays(langue.cPays());
	                newText.setTmuOrdre(couNumero());
	                _courrier.editingContext().insertObject(newText);
	                _courrier.editingContext().saveChanges();
	                text = new GediTexteMultilingue(newText);
	                _courrier.addObjectToBothSidesOfRelationshipWithKey(newText,"gB_TexteMultilingues");
	                newText.addObjectToBothSidesOfRelationshipWithKey(langue.getLangue(),"gb_langue");
                }
            }
            if(value==null || value.length()<4000)
            {
                _courrier.setCouCommentaire(value);
                if(text!=null && text.getTextMultilangue()!=null)
                {
                    _courrier.editingContext().deleteObject(text.getTextMultilangue());
                    //_courrier.editingContext().saveChanges();
                   // _courrier.editingContext().faultForGlobalID(_courrier.editingContext().globalIDForObject(text.getTextMultilangue()),_courrier.editingContext());
                }
                _courrier.editingContext().saveChanges();
                _courrier.editingContext().faultForGlobalID(_courrier.editingContext().globalIDForObject(_courrier),_courrier.editingContext());
                return;
            }
            else
            {
                _courrier.setCouCommentaire(value.substring(0,4000));
                text.setTmuBlobs(value.substring(4000));
            }
        }
        else{
            try {
                text = GediTexteMultilingue.gediTextMultilangueForLangue(langue,this);
            } catch (Exception e) {
	            TexteMultilingue newText = new TexteMultilingue();
	            newText.setTmuBlobs(value);
	            newText.setCLangue(langue.cLangue());
	            newText.setCPays(langue.cPays());
	            newText.setTmuOrdre(couNumero());
	            _courrier.editingContext().insertObject(newText);
	            text = new GediTexteMultilingue(newText);
            }
            text.setTmuBlobs(value);
        }
        try {
            _courrier.editingContext().saveChanges();
            _courrier.editingContext().faultForGlobalID(_courrier.editingContext().globalIDForObject(text.getTextMultilangue()),_courrier.editingContext());
            _courrier.editingContext().faultForGlobalID(_courrier.editingContext().globalIDForObject(_courrier),_courrier.editingContext());
        } catch (Exception e) {
           throw new TextMultilangueNotFoundException("Erreur lors de la sauvegarde du courrier (pb multilangue) : "+e.getMessage());
        }
    }

    public String couMotsClefs() {
        return _courrier.couMotsClefs();
    }

    public void setCouMotsClefs(String value) {
        _courrier.setCouMotsClefs(value);
    }

    public Number couTypeConsultation() {
        return _courrier.couTypeConsultation();
    }

    public void setCouTypeConsultation(Integer value) {
        _courrier.setCouTypeConsultation(value);
    }

    public String couLocalisation() {
        return _courrier.couLocalisation();
    }

    public void setCouLocalisation(String value) {
        _courrier.setCouLocalisation(value);
    }

    public Number couNumeroPere() {
        return _courrier.couNumeroPere();
    }

    public void setCouNumeroPere(Integer value) {
        _courrier.setCouNumeroPere(value);
    }

    public Number persId() {
//        System.out.println("Dans persid de gedicourrier");
        return _courrier.persId();
    }

    public void setPersId(Integer value) {
        _courrier.setPersId(value);
    }

    /**
     * Affichage par defaut du courrier (objet : commantaire)
     */
    public String toString() {
        return couObjet() + " : " + couCommentaire();
    }
    
    /**
     * Liste des {@link GediTexteMultilingue} associé au courrier
     * @return {@link NSArray} de {@link GediTexteMultilingue}
     * @throws LangueNotFoundException
     */
    public NSArray listTextMultilangue(){
        try {
            return GediLangue.initListLangue((NSArray)_courrier.gB_TexteMultilingues().valueForKey("gb_langue"));
        } catch (LangueNotFoundException e) {
            return null;
        }
    }
    
//    public void setListTextMultilangue(NSArray list){
//        _courrier.setGb_TexteMultilingues(new NSMutableArray((NSArray)list.valueForKey("textMultilangue")));
//    }
    
    
    
//    public String chapeauHtml(){
//        return "";
//    }
//    
//    public void setChapeauHtml(){
//        
//    }
    /**
     * Le titre du courrier correspond
     * a l'objet sans le classement ( exmple: suppression de '01#' )
     * @return le titre sans la position
     */
	public String titre(GediLangue langue){
		String str = couObjet(langue);
		if(str==null)
			return null;
		int indexOf = str.indexOf("#")+1;
		if(str.indexOf("#")>0)
			return (indexOf!=str.length())?str.substring(indexOf):null;
		return str;
	}
	
	/**
	 * Renvoi le titre sans le classement et sans l'adresse de l'image!!!
	 * @return le titre
	 */
	public String titleTextOnly(GediLangue langue){
	    String str = titre(langue);
		int indexOf = str.indexOf("$");
		if(str.indexOf("$")>0)
		    return (indexOf!=0)?str.substring(0,indexOf):null;
		return str;
	}
	
	/**
	 * Remvoi l'adresse de l'image associée a l'objet
	 * @return l'url si existe, null sinon
	 */
	public String titleImgUrl(GediLangue langue){
	    String str = titre(langue);
		int indexOf = str.indexOf("$")+1;
		if(str.indexOf("$")>0)
		    return (indexOf!=str.length())?((("/".equals(str.subSequence(indexOf,indexOf+1)))?((CktlWebApplication)WOApplication.application()).config().stringForKey("MAIN_WEB_SITE_URL"):"")+ str.substring(indexOf)):null;
		return null;
	}
	
	/**
	 * Renvoi le classement du courrier pour la langue en cours
	 * @param langue la langue utilisée
	 * @return le classement
	 */
	public String classement(GediLangue langue){
		String str =  couObjet(langue);
		if(str==null || "".equals(str))
			return null;
		int indexOf = str.indexOf("#");
		if(str.indexOf("#")>=0)
			return str.substring(0,indexOf);
		return null;
	}
	
	/**
	 * Permet de changer le classement
	 * @param str le classement (exp: 1 , AA, 01, 0235 ...)
	 * @param langue la langue utilisée
	 * @throws TextMultilangueNotFoundException si la langue n'est pas utilisée !!!
	 * @throws CourrierPrimaryKeyNotFoundException si le courrier n'est pas trouvé !!!
	 */
	public void setClassement(String str,GediLangue langue) throws TextMultilangueNotFoundException, CourrierPrimaryKeyNotFoundException{
		if(str==null || "".equals(str))
			return;
		String tmp = titre(langue);
		setCouObjet(str==null?tmp:str+"#"+tmp,langue);
	}
	
	private int indexFinChapeau(String str){
		int debut = 0;
		int openTag=-1;
		int endTag=-1;
		if(str.toLowerCase().indexOf(BALISE_CHAPEAU_DEB,debut)<0)
			return -1;
		debut = str.toLowerCase().indexOf(BALISE_CHAPEAU_DEB,debut) + BALISE_CHAPEAU_DEB.length();
		int nbOpenP = 1;
//		System.out.println("nbOpenP = "+nbOpenP);
//		System.out.println("debut = "+debut);
		while(nbOpenP>0){
			openTag = str.toLowerCase().indexOf("<p>",debut);
			endTag = str.toLowerCase().indexOf("</p>",debut);
//			System.out.println("openTag = "+openTag+" endTag = "+endTag);
			if(endTag<0)
				return -1;
			if(openTag>0 && openTag<endTag){
				nbOpenP++;
				debut =  openTag + "<p>".length();
			}
			else
			{
				nbOpenP--;
				debut =  endTag + "</p>".length();
			}
//			System.out.println("nbOpenP = "+nbOpenP);
//			System.out.println("debut = "+debut);
		}
		return debut;
	}
	
	/**
	 * Permet d'afficher le chapeau si il existe
	 * @param langue la langue utilisée
	 * @return le chapeau si il existe, null sinon
	 */
	public String chapeau(GediLangue langue){
		String str = couCommentaire(langue);
		if(str==null || "".equals(str) || !hasChapeau())
			return null;
		//return StringCtrl.textBetweenTag(str,BALISE_CHAPEAU_NOM,false);
		int deb = str.indexOf(BALISE_CHAPEAU_DEB);//?0:str.indexOf(BALISE_CHAPEAU_DEB)+BALISE_CHAPEAU_DEB.length();
		if(deb<0)
		    return null;
		int fin = indexFinChapeau(str)-"</p>".length();
		fin = fin<0?deb:fin;
		return str.substring(deb,fin);
	}

	/**
	 * Attention : <strong>si vous devez utiliser setChapeau, il faut le faire APR&Egrave;S {@link #setCouCommentaire(String, GediLangue)}
	 * et pas avant. Sinon, le contenu du chapeau sera supprimé !!!! par le commentaire.
	 * @param str le chapeau
	 * @param langue la langue utilisée
	 */
	public void setChapeau(String str, GediLangue langue){
	    String old = chapeau(langue);
	    String commentaire = couCommentaire(langue);
	    if(str==null || "".equals(str))
	    {
	        if(old!=null && !"".equals(old))
	            setCouCommentaire(StringCtrl.replace(commentaire,old,str));
			return;
	    }
        if(old!=null && !"".equals(old))
            setCouCommentaire(StringCtrl.replace(commentaire,old,str));
        else
            setCouCommentaire(BALISE_CHAPEAU_DEB+str+BALISE_CHAPEAU_FIN+commentaire);
	}
	
	/**
	 * Renvoi toujour vrai !!!!
	 * @return true!!!
	 */
	public boolean hasChapeau(){
	    return true;
	}
	
	public NSArray specifWeb() throws SpecifWebNotFoundException{
	    return GediSpecifWeb.initListSpecifWeb(_courrier.specifWeb());
	}
	
 	/**
	 * 	Ajout d'un mot clef a la liste
	 */
    public void addMotClef(String value) {
        String newMotsClefs = couMotsClefs();
        // Si le mot clef n'existe pas dans la liste
        NSMutableArray listeMots = new NSMutableArray(NSArray.componentsSeparatedByString(newMotsClefs, ";"));
        listeMots.removeObject("");
        if(!listeMots.containsObject(value)) {
            listeMots.addObject(value);
            setCouMotsClefs(listeMots.componentsJoinedByString(";"));
        }
    }

 	/**
	 * 	Suppression d'un mot de la liste des mots clefs
	 */
    public void removeMotClef(String value) {
        String newMotsClefs = couMotsClefs();
        NSMutableArray listeMots = new NSMutableArray(NSArray.componentsSeparatedByString(newMotsClefs, ";"));
        listeMots.removeObject("");
        listeMots.removeObject(value);
        setCouMotsClefs(listeMots.componentsJoinedByString(";"));
    }


    protected EOGenericRecord getObjetMetier() {
        return _courrier;
    }
}
