/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.geide.fwkcktlgedibus.tools;

import org.cocktail.geide.fwkcktlgedibus.exception.TypeCourrierNewNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Class à utiliser à la place de {@link org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew}
 * 
 * @author mparadot
 */
public class GediTypeCourrierNew extends GediObject {
    private TypeCourrierNew _type;

    protected GediTypeCourrierNew(TypeCourrierNew type) {
        _type = type;
    }

    protected static NSArray initListType(NSArray list) throws TypeCourrierNewNotFoundException {
        if (list == null || list.count() == 0)
            throw new TypeCourrierNewNotFoundException(null);
        NSMutableArray array = new NSMutableArray();
        for (int i = 0; i < list.count(); i++) {
            TypeCourrierNew type = (TypeCourrierNew) list.objectAtIndex(i);
            array.addObject(new GediTypeCourrierNew(type));
        }
        return array;
    }

    protected TypeCourrierNew getTypeCourrierNew() {
        return _type;
    }

    public Integer typeOrdre() throws Exception {
        return (Integer) _type.typeOrdre();
    }

    public String typeLibelle() {
        return _type.typeLibelle();
    }

    public void setTypeLibelle(String value) {
        _type.setTypeLibelle(value);
    }

    public String typeCode() {
        return _type.typeCode();
    }

    public void setTypeCode(String value) {
        _type.setTypeCode(value);
    }

    public Number typeTypeConsultation() {
        return _type.typeTypeConsultation();
    }

    public void setTypeTypeConsultation(Integer value) {
        _type.setTypeTypeConsultation(value);
    }

    public Number refOrdre() {
        return _type.refOrdre();
    }

    public void setRefOrdre(Integer value) {
        _type.setRefOrdre(value);
    }

    public String typeVisibleAppli() {
        return _type.typeVisibleAppli();
    }

    public void setTypeVisibleAppli(String value) {
        _type.setTypeVisibleAppli(value);
    }

    public String typeFamille() {
        return _type.typeFamille();
    }

    public void setTypeFamille(String value) {
        _type.setTypeFamille(value);
    }

    public Number cleDates() {
        return _type.cleDates();
    }

    public void setCleDates(Integer value) {
        _type.setCleDates(value);
    }

    public String typePageUrl() {
        return _type.typePageUrl();
    }

    public void setTypePageUrl(String value) {
        _type.setTypePageUrl(value);
    }

    public String typePageTitre() {
        return _type.typePageTitre();
    }

    public void setTypePageTitre(String value) {
        _type.setTypePageTitre(value);
    }

    public String typeOrdreChar() {
        return _type.typeOrdreChar();
    }

    public void setTypeOrdreChar(String value) {
        _type.setTypeOrdreChar(value);
    }

    public Number typeDocumentId() {
        return _type.typeDocumentId();
    }

    public void setTypeDocumentId(Integer value) {
        _type.setTypeDocumentId(value);
    }

    public String toString() {
        return typeLibelle() + " " + typeOrdreChar();
    }
    

    protected EOGenericRecord getObjetMetier() {
        return _type;
    }
}
