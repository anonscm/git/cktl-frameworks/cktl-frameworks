/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.geide.fwkcktlgedibus.tools;

import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.geide.fwkcktlgedibus.exception.CourrierPrimaryKeyNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.TextMultilangueNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Class à utiliser à la place de {@link org.cocktail.geide.fwkcktlgedibus.metier.serveur.TexteMultilingue}
 * 
 * @author mparadot
 */
public class GediTexteMultilingue extends GediObject {
    private TexteMultilingue _texteMultilingue;
    private static CktlWebApplication criApp = (CktlWebApplication)WOApplication.application();

    protected GediTexteMultilingue(TexteMultilingue texteMultilingue) {
        super();
        _texteMultilingue=texteMultilingue;
    }
    
    protected static GediTexteMultilingue gediTextMultilangueForLangue(GediLangue langue, GediCourrier courrier)throws TextMultilangueNotFoundException,CourrierPrimaryKeyNotFoundException
    {
        if(langue==null)
            throw new TextMultilangueNotFoundException("Paramètre langue null !!!");
        if(courrier.getCourrier().editingContext()==null)
            throw new TextMultilangueNotFoundException("Pas d'editing context --> le courrier doit être enregistrer en base avant l'accès au multilangue !!!");
        NSMutableArray args = new NSMutableArray();
        args.addObject(EOQualifier.qualifierWithQualifierFormat("cLangue =%@", new NSArray(langue.cLangue())));
        args.addObject(EOQualifier.qualifierWithQualifierFormat("cPays =%@", new NSArray(langue.cPays())));
        args.addObject(EOQualifier.qualifierWithQualifierFormat("tmuOrdre =%@", new NSArray(courrier.couNumero())));
        TexteMultilingue text = (TexteMultilingue)criApp.dataBus().fetchArray(courrier.getCourrier().editingContext(),"GB_TexteMultilingue",new EOAndQualifier(args),null,false,false).lastObject();
        if(text==null)
            throw new TextMultilangueNotFoundException("pour la langue "+langue);
        return new GediTexteMultilingue(text);
    }
    
    protected TexteMultilingue getTextMultilangue(){
        return _texteMultilingue;
    }
    
    public String tmuTexte() {
        return (String)_texteMultilingue.storedValueForKey("tmuTexte");
    }

    public void setTmuTexte(String value) {
        _texteMultilingue.takeStoredValueForKey(value, "tmuTexte");
    }

    public String tmuBlobs() {
        return (String)_texteMultilingue.storedValueForKey("tmuBlobs");
    }

    public void setTmuBlobs(String value) {
        _texteMultilingue.takeStoredValueForKey(value, "tmuBlobs");
    }

    public String cPays() {
        return (String)_texteMultilingue.storedValueForKey("cPays");
    }

    public void setCPays(String value) {
        _texteMultilingue.takeStoredValueForKey(value, "cPays");
    }

    public String cLangue() {
        return (String)_texteMultilingue.storedValueForKey("cLangue");
    }

    public void setCLangue(String value) {
        _texteMultilingue.takeStoredValueForKey(value, "cLangue");
    }

    public Number tmuOrdre() {
        return (Number)_texteMultilingue.storedValueForKey("tmuOrdre");
    }

    public void setTmuOrdre(Number value) {
        _texteMultilingue.takeStoredValueForKey(value, "tmuOrdre");
    }

    protected EOGenericRecord getObjetMetier() {
        return _texteMultilingue;
    }

}
