/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.geide.fwkcktlgedibus.tools;

import org.cocktail.geide.fwkcktlgedibus.exception.LangueNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.metier.client.Langue;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Class à utiliser à la place de {@link org.cocktail.geide.fwkcktlgedibus.metier.serveur.Langue}
 * 
 * @author mparadot
 */
public class GediLangue extends GediObject {
    public static String CODE_PAYS_FRANCE="100";
    public static String CODE_LANGUE_FRANCE="fr";
    
    private Langue _langue;

    protected GediLangue() {
        super();
    }
    
    protected Langue getLangue(){
        return _langue;
    }
    
    protected static NSArray initListLangue(NSArray list) throws LangueNotFoundException {
        if (list == null || list.count() == 0)
            throw new LangueNotFoundException("Liste est null");
        NSMutableArray array = new NSMutableArray();
        for (int i = 0; i < list.count(); i++) {
            if(list.objectAtIndex(i)!=null && list.objectAtIndex(i)!=NSKeyValueCoding.NullValue)
            {
                Langue langue = (Langue) list.objectAtIndex(i);
                array.addObject(new GediLangue(langue));
            }
        }
        return array;
    }
    
    protected GediLangue(Langue langue) {
        super();
        _langue = langue;
    }
    
    public static boolean isLangueFr(GediLangue langue){
        return CODE_PAYS_FRANCE.equals(langue.cPays())&&CODE_LANGUE_FRANCE.equals(langue.cLangue());
    }
    
    public String cLangue(){
        if(_langue==null)
            return null;
        NSDictionary dico = EOUtilities.primaryKeyForObject(_langue.editingContext(),_langue);
        return (String)dico.valueForKey("cLangue");
    }
    
    public String cPays(){
        if(_langue==null)
            return null;
        NSDictionary dico = EOUtilities.primaryKeyForObject(_langue.editingContext(),_langue);
        return (String)dico.valueForKey("cPays");
    }
    

    public String llPays() {
        return (String)_langue.storedValueForKey("llPays");
    }

    public void setLlPays(String value) {
        _langue.takeStoredValueForKey(value, "llPays");
    }

    public String lcPays() {
        return (String)_langue.storedValueForKey("lcPays");
    }

    public void setLcPays(String value) {
        _langue.takeStoredValueForKey(value, "lcPays");
    }

    public String lNationalite() {
        return (String)_langue.storedValueForKey("lNationalite");
    }

    public void setLNationalite(String value) {
        _langue.takeStoredValueForKey(value, "lNationalite");
    }

    public NSTimestamp dDebVal() {
        return (NSTimestamp)_langue.storedValueForKey("dDebVal");
    }

    public void setDDebVal(NSTimestamp value) {
        _langue.takeStoredValueForKey(value, "dDebVal");
    }

    public NSTimestamp dFinVal() {
        return (NSTimestamp)_langue.storedValueForKey("dFinVal");
    }

    public void setDFinVal(NSTimestamp value) {
        _langue.takeStoredValueForKey(value, "dFinVal");
    }

    public NSTimestamp dCreation() {
        return (NSTimestamp)_langue.storedValueForKey("dCreation");
    }

    public void setDCreation(NSTimestamp value) {
        _langue.takeStoredValueForKey(value, "dCreation");
    }

    public NSTimestamp dModification() {
        return (NSTimestamp)_langue.storedValueForKey("dModification");
    }

    public void setDModification(NSTimestamp value) {
        _langue.takeStoredValueForKey(value, "dModification");
    }

    public String codeIso() {
        return (String)_langue.storedValueForKey("codeIso");
    }

    public void setCodeIso(String value) {
        _langue.takeStoredValueForKey(value, "codeIso");
    }

    public NSArray langues() {
        return (NSArray)_langue.storedValueForKey("langues");
    }

    public void setLangues(NSArray value) {
        _langue.takeStoredValueForKey(value, "langues");
    }

    /* (non-Javadoc)
     * @see com.webobjects.foundation.NSKeyValueCodingAdditions#valueForKeyPath(java.lang.String)
     */
    public Object valueForKeyPath(String arg0) {
        return _langue.valueForKeyPath(arg0);
    }

    /* (non-Javadoc)
     * @see com.webobjects.foundation.NSKeyValueCodingAdditions#takeValueForKeyPath(java.lang.Object, java.lang.String)
     */
    public void takeValueForKeyPath(Object arg0, String arg1) {
        _langue.takeValueForKeyPath(arg0, arg1);
        
    }

    /* (non-Javadoc)
     * @see com.webobjects.foundation.NSKeyValueCoding#valueForKey(java.lang.String)
     */
    public Object valueForKey(String arg0) {
        return _langue.valueForKey(arg0);
    }

    /* (non-Javadoc)
     * @see com.webobjects.foundation.NSKeyValueCoding#takeValueForKey(java.lang.Object, java.lang.String)
     */
    public void takeValueForKey(Object arg0, String arg1) {
        _langue.takeValueForKey(arg0, arg1);
    }
    
    public String toString() {
        return _langue.llLangue();
    }

    protected EOGenericRecord getObjetMetier() {
        return _langue;
    }

 
}
