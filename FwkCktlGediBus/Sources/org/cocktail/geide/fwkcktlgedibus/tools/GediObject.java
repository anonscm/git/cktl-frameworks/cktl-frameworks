/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.geide.fwkcktlgedibus.tools;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSKeyValueCodingAdditions;

/**
 * @author mparadot
 *
 *class abstraite qui permet les valueForKey avec parametre variable
 *
 *@see org.cocktail.geide.fwkcktlgedibus.tools.GediCourrier
 *@see org.cocktail.geide.fwkcktlgedibus.tools.GediLangue
 *@see org.cocktail.geide.fwkcktlgedibus.tools.GediPaysDrapeaux
 *@see org.cocktail.geide.fwkcktlgedibus.tools.GediReference
 *@see org.cocktail.geide.fwkcktlgedibus.tools.GediReferenceLien
 *@see org.cocktail.geide.fwkcktlgedibus.tools.GediSpecifWeb
 *@see org.cocktail.geide.fwkcktlgedibus.tools.GediSpecifWebCommune
 *@see org.cocktail.geide.fwkcktlgedibus.tools.GediTexteMultilingue
 *@see org.cocktail.geide.fwkcktlgedibus.tools.GediTypeCourrierNew
 */
public abstract class GediObject implements NSKeyValueCodingAdditions{
    
    protected abstract EOGenericRecord getObjetMetier();
    
    
    public Object valueForKey(String arg0,Object[] param) {
        Class[] listClass = null;
        if(param!=null)
        {
            for (int i = 0; i < param.length; i++) {
                listClass[i]=param[i].getClass();
            }
        }
        return valueForKey(arg0,param,listClass);
    }
    
    public Object valueForKey(String arg0,Object[] param,Class[] listClass) {
        try {
            Class c = this.getClass();
            Method method = c.getMethod(arg0,listClass);
            return method.invoke(this,param);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return getObjetMetier().valueForKey(arg0);
    }
    
    public void takeValueForKey(Object[] param, String arg1) {
        Class[] listClass = null;
        if(param!=null)
        {
            for (int i = 0; i < param.length; i++) {
                listClass[i]=param[i].getClass();
            }
        }
        takeValueForKey(arg1,param,listClass);
    }
    
    public void takeValueForKey(String arg1,Object[] param,Class[] listClass) {
        try {
            Class c = this.getClass();
            Method method = c.getMethod(arg1,listClass);
            method.invoke(this,param);
            return;
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        getObjetMetier().takeValueForKey(param[0], arg1);
    }

    public Object valueForKey(String arg0) {
        try {
            Class c = this.getClass();
            Method method = c.getMethod(arg0);
            return method.invoke(this);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return getObjetMetier().valueForKey(arg0);
    }
    
    public void takeValueForKey(Object arg0, String arg1) {
        System.out.println("arg0 = "+arg0);
        try{
            try {
                if ( arg0 != null) {
                    Class c = this.getClass();
                    Method method = c.getMethod(arg1,new Class[]{arg0.getClass()});
                    method.invoke(this,new Object[]{arg0});
                }
                return;
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }catch (Throwable e) {
        }
        getObjetMetier().takeValueForKey(arg0, arg1);
    }
    
    
    public Object valueForKeyPath(String arg0) {
        try {
            Class c = this.getClass();
            Method method = c.getMethod(arg0);
            return method.invoke(this);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return getObjetMetier().valueForKeyPath(arg0);
    }

    public void takeValueForKeyPath(Object arg0, String arg1) {
        System.out.println("arg0 = "+arg0);
        try{
            try {
                Class c =this.getClass();
                Method method = c.getMethod(arg1,new Class[]{Object.class});
                method.invoke(this,new Object[]{arg0});
                return;
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }catch (Throwable e) {
        }
        getObjetMetier().takeValueForKeyPath(arg0, arg1);
    }
    
}
