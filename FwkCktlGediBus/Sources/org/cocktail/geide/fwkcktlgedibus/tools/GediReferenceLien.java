/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.geide.fwkcktlgedibus.tools;

import org.cocktail.geide.fwkcktlgedibus.exception.ReferenceLienNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.ReferenceLienPrimaryKeyNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Class à utiliser à la place de {@link org.cocktail.geide.fwkcktlgedibus.metier.serveur.ReferenceLien}
 * 
 * @author mparadot
 */
public class GediReferenceLien extends GediObject{
    private ReferenceLien _refLien;
    public static final String TYPE_FICHIER="F";
    public static final String TYPE_LIEN="L";
    public static final String AUCUN_TYPE=null;
    
    private GediReferenceLien() {
        super();
    }
    
    protected GediReferenceLien(ReferenceLien refLien){
        super();
        _refLien=refLien;
    }
    
    protected ReferenceLien getReferenceLien(){
        return _refLien;
    }
    
    /**
     * renvoi une liste de {@link GediReferenceLien}
     * @param list de {@link ReferenceLien}
     * @return {@link NSArray}
     * @throws ReferenceLienNotFoundException si un referenceLien n'est pas trouvé
     */
    protected static NSArray initListRefLien(NSArray list) throws ReferenceLienNotFoundException{
        if(list==null)
            throw new ReferenceLienNotFoundException("la liste est null !!!");
        NSMutableArray listRef = new NSMutableArray();
        for (int i = 0; i < list.count(); i++) {
            ReferenceLien refLien =(ReferenceLien) list.objectAtIndex(i);
            if(refLien!=null)
            {
                listRef.addObject(new GediReferenceLien(refLien));
            }
        }
        return listRef;
    }
    
    public Number rftOrdre() throws ReferenceLienPrimaryKeyNotFoundException{
        if(_refLien==null || _refLien.editingContext()==null)
            return null;
        return _refLien.rftOrdre();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public ReferenceLien(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String rflLien() {
        return (String)_refLien.storedValueForKey("rflLien");
    }

    public void setRflLien(String value) {
        _refLien.takeStoredValueForKey(value, "rflLien");
    }

    public String rflType() {
        return (String)_refLien.storedValueForKey("rflType");
    }

    public void setRflType(String value) {
        _refLien.takeStoredValueForKey(value, "rflType");
    }

    public Number rflDocumentId() {
        return (Number)_refLien.storedValueForKey("rflDocumentId");
    }

    public void setRflDocumentId(Number value) {
        _refLien.takeStoredValueForKey(value, "rflDocumentId");
    }

    public String rflLibelle() {
        String libelle = (String)_refLien.storedValueForKey("rflLibelle");
        if(libelle!=null)
            return libelle;
        String url = rflLien();
        if(url==null)
            return null;
        if(url.indexOf("\\")>0 && url.indexOf("\\")<url.length())
            return url.substring(url.lastIndexOf("\\")+1);
        if(url.indexOf("/")>0 && url.indexOf("/")<url.length())
            return url.substring(url.lastIndexOf("/")+1);
        return url;
    }

    public void setRflLibelle(String value) {
        _refLien.takeStoredValueForKey(value, "rflLibelle");
    }

    public Number couNumero() {
        return (Number)_refLien.storedValueForKey("couNumero");
    }
    
    protected EOGenericRecord getObjetMetier() {
        return _refLien;
    }

//    public void setCouNumero(Number value) {
//        takeStoredValueForKey(value, "couNumero");
//    }
//
//    public org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier courrier() {
//        return (org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier)storedValueForKey("courrier");
//    }
//
//    public void setCourrier(org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier value) {
//        takeStoredValueForKey(value, "courrier");
//    }
}
