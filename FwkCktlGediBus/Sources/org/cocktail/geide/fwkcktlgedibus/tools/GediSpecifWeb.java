/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.geide.fwkcktlgedibus.tools;

import org.cocktail.geide.fwkcktlgedibus.exception.SpecifWebNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Class à utiliser à la place de {@link org.cocktail.geide.fwkcktlgedibus.metier.serveur.SpecifWeb}
 *  
 * @author mparadot
 */
public class GediSpecifWeb extends GediObject {
    private SpecifWeb _specifWeb;

    private GediSpecifWeb() {
        super();
    }
    
    protected GediSpecifWeb(SpecifWeb specifWeb){
        super();
        _specifWeb=specifWeb;
    }
    
    protected static NSArray initListSpecifWeb(NSArray list) throws SpecifWebNotFoundException {
        if (list == null || list.count() == 0)
            throw new SpecifWebNotFoundException("la list est vide");
        NSMutableArray array = new NSMutableArray();
        for (int i = 0; i < list.count(); i++) {
            SpecifWeb specif = (SpecifWeb) list.objectAtIndex(i);
            array.addObject(new GediSpecifWeb(specif));
        }
        return array;
    }
    
    public Number couNumero() {
        return (Number)_specifWeb.storedValueForKey("couNumero");
    }

    public void setCouNumero(Number value) {
        _specifWeb.takeStoredValueForKey(value, "couNumero");
    }

    public Number spwcKey() {
        return (Number)_specifWeb.storedValueForKey("spwcKey");
    }

    public void setSpwcKey(Number value) {
        _specifWeb.takeStoredValueForKey(value, "spwcKey");
    }

    public Number typeOrdre() {
        return (Number)_specifWeb.storedValueForKey("typeOrdre");
    }

    public void setTypeOrdre(Number value) {
        _specifWeb.takeStoredValueForKey(value, "typeOrdre");
    }
    
    public GediSpecifWebCommune specifWebCommun(){
        return new GediSpecifWebCommune(_specifWeb.gB_SpecifWebCommune());
    }

    protected EOGenericRecord getObjetMetier() {
        return _specifWeb;
    }

}
