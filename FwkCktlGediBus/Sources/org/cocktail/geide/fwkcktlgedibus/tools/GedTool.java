/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.geide.fwkcktlgedibus.tools;

/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 5 juin 07
 * author mparadot 
 */
//
//import org.cocktail.geide.fwkcktlgedibus.exception.CourrierNotFoundException;
//import org.cocktail.geide.fwkcktlgedibus.exception.CreateAgentFailedException;
//import org.cocktail.geide.fwkcktlgedibus.exception.DeleteCourrierFailedException;
//import org.cocktail.geide.fwkcktlgedibus.exception.ReferenceLienNotFoundException;
//import org.cocktail.geide.fwkcktlgedibus.tools.GediBus;
//import org.cocktail.geide.fwkcktlgedibus.tools.GediCourrier;
//import org.cocktail.geide.fwkcktlgedibus.tools.GediReferenceLien;

import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;
import org.cocktail.fwkcktlwebapp.common.util.GEDDescription;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.geide.fwkcktlgedibus.exception.CourrierNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.CreateAgentFailedException;
import org.cocktail.geide.fwkcktlgedibus.exception.DeleteCourrierFailedException;
import org.cocktail.geide.fwkcktlgedibus.exception.ReferenceLienNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.exception.TypeCourrierNewNotFoundException;
import org.cocktail.geide.fwkcktlgedibus.metier.client.Courrier;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.TypeCourrierNew;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;

public class GedTool{
	private GediBus gediBus;
    private static CktlWebApplication app = (CktlWebApplication)WOApplication.application();
    private Throwable _lastError;
    private EOEditingContext _ec;
    private String _rootGed;
    private Integer typeVisibilite;
    
    
    public GedTool(EOEditingContext ec) {
    	try {
            gediBus = new GediBus(ec);
            _ec = ec;
            setRootGed(app.config().stringForKey("TYPE_ROOT_GED"));
            typeVisibilite = GediBus.TYPE_PRIVE;
		} catch (Throwable e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Renvoi le type_ordre_char du root en cours
     */
    public String getRootGed() {
        return _rootGed;
    }

    /**
     * Permet de redefinir le type root dans la ged pour les depots
     * @param ged le type_ordre_char du type_courrier_new (repertoire)
     */
    public void setRootGed(String ged) {
        _rootGed = ged;
    }
    
    public Integer getVisibiliteForFileToSave(){
    	return typeVisibilite;
    }
    
	public void setVisibiliteForFileToSave(Integer visibilite) {
		this.typeVisibilite = visibilite;
	}
    
    /**
     * @deprecated
     * Utiliser {@link GedTool#getVisibiliteForFileToSave()}
     */
    public Integer getVisibilite() {
		return typeVisibilite;
	}

    /**
     * @deprecated
     * Utiliser {@link GedTool#setVisibiliteForFileToSave(Integer)}
     */
	public void setVisibilite(Integer visibilite) {
		this.typeVisibilite = visibilite;
	}

	public EOEditingContext edtitingContext(){
        return _ec;
    }
    
    public Integer saveFileIntoGed(NSData file, String name, Integer persId){
        _lastError = null;
        System.out.println("GedTool.saveFileIntoGed()");
        if(file==null)
        {
            _lastError = new Exception("Le fichier est vide ou n'existe pas !");
            return null;
        }
        if(getRootGed()==null)
        {
        	_lastError = new Exception("Vous n'avez pas le repertoire de depot de renseigné dans la config. (parametre TYPE_ROOT_GED)");
        	return null;
        }
        System.out.println("name = "+name);
        NSArray path = getPathForName(name);
        String realname = FileCtrl.getFileName(name);
        CktlUserInfoDB user = new CktlUserInfoDB(app.dataBus());
        user.compteForPersId(persId, true);
         //creation d'un courrier...
        Integer couNumero = null;
        GediCourrier cour = null;
         try {
             estAgent(user);
             System.out.println("Visibilité du fichier : "+getVisibilite());
             couNumero = gediBus.createCourrier(realname, "", persId, getVisibilite(), user.nomEtPrenom(), user.login());
             System.out.println("new counumero = "+couNumero);
             cour = gediBus.courrierForCouNumero(couNumero);
        } catch (Exception e) {
            e.printStackTrace();
            _lastError = e;
            return null;
        }
        System.out.println("GedTool.saveFileIntoGed(1)");
        app.gedBus().setNoIndividu(user.noIndividu().intValue());
        try {
            System.out.println("GedTool.saveFileIntoGed()->bindCourrierToType");
            bindCourrierToType(path,couNumero);
        } catch (Exception e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
            _lastError = new Exception("<<Erreur gedibus :+ "+e2.getMessage()+">>");
            System.out.println("GedTool.saveFileIntoGed()->Erreur creation path = -1 \n"+e2.getMessage());
            try {
                gediBus.removeCourrier(couNumero, user.noIndividu());
            } catch (DeleteCourrierFailedException e) {
                e.printStackTrace();
            }
            return null;
        }
        System.out.println("GedTool.saveFileIntoGed(2)");
        //app.gedBus(user.noIndividu().intValue());
        System.out.println(" use ged  : "+app.useGed());
        
        int numDoc = -1;
        try {
        	// numDoc = app.gedBus(user.noIndividu().intValue()).saveDocumentGED(file.bytes(), realname, realname, "COUR");
        	numDoc = app.gedBus(user.noIndividu().intValue()).saveDocumentGED(file.bytes(), realname, realname, "TEST");
		} catch (Throwable e) {
			e.printStackTrace();
			_lastError = new Exception("<<Erreur ged :+ "+e.getMessage()+">>");
			return null;
//			numDoc = app.gedBus().saveDocumentGED(file.bytes(), realname, realname, "COUR");
		}
        System.out.println("GedTool.saveFileIntoGed(2-1)");
        if(numDoc==-1)
        {
            _lastError = new Exception("<<Erreur ged :+ "+app.gedBus().gedMessage()+">>");
            System.out.println("GedTool.saveFileIntoGed()->numDoc = -1 \n"+app.gedBus().gedMessage());
            try {
                gediBus.removeCourrier(couNumero, user.noIndividu());
            } catch (DeleteCourrierFailedException e) {
                e.printStackTrace();
            }
            return null;
        }
        System.out.println("GedTool.saveFileIntoGed(3)");
        GEDDescription desc = app.gedBus().inspectDocumentGED(numDoc);
        System.out.println("GedTool.saveFileIntoGed(4)\n"+desc.toString());
        try {
            gediBus.createReferenceLienForCourrier(desc.reference,new Integer(numDoc), GediBus.TYPE_REFERENCE_FICHIER, name, cour);
        } catch (Throwable e) {
            e.printStackTrace();
            _lastError = e;
            app.gedBus().deleteDocumentGED(numDoc);
            try {
                gediBus.removeCourrier(couNumero, user.noIndividu());
            } catch (DeleteCourrierFailedException e1) {
                e.printStackTrace();
            }
            return null;
        }
        System.out.println("GedTool.saveFileIntoGed(5)");
        return couNumero;
    }
    
    private void bindCourrierToType(NSArray path, Integer couNumero) throws Exception{
        System.out.println("GedTool.bindCourrierToType()");
        GediTypeCourrierNew root=getGedTypeCourrierRoot();
        GediTypeCourrierNew enCours = root;
        System.out.println("le parh : "+path.count());
        for (int i = 0; i < path.count(); i++) {
            String temp = (String) path.objectAtIndex(i);
            System.out.println("path "+i+" "+temp);
            enCours = getNewChildForType(enCours,temp);
        }
        //enCours = le type a associer !!!
        GediCourrier cour = gediBus.courrierForCouNumero(couNumero);
        gediBus.createTypeAssocies(cour, enCours);
    }
    
    private GediTypeCourrierNew getNewChildForType(GediTypeCourrierNew root, String childName) throws Exception{
        System.out.println("GedTool.getChildForType()");
        System.out.println("root = "+root);
        System.out.println("childName = "+childName);
        NSMutableArray args = new NSMutableArray();
        args.addObject(EOQualifier.qualifierWithQualifierFormat("typeLibelle =%@", new NSArray(childName)));
        args.addObject(EOQualifier.qualifierWithQualifierFormat("typeCode =%@", new NSArray(root.typeOrdreChar())));
        NSArray list;
        try {
            list = gediBus.typeCourrierNewForQualifierAndSort(new EOAndQualifier(args),null);
            if(list!=null && list.count()!=0)
                return (GediTypeCourrierNew) list.lastObject();
        } catch (TypeCourrierNewNotFoundException e) {
        }
        //Creation du repertoire !!!
        System.out.println("creation de "+childName);
        String type = gediBus.createTypeCourrierNew(childName, childName, null, root.typeOrdreChar(), root.typeFamille(), new Integer(root.typeTypeConsultation().intValue()), root.typeVisibleAppli(), null, null, new Integer(4));
        return gediBus.typeCourrierNewForTypeOrdreChar(type);
    }
    
    private GediTypeCourrierNew getGedTypeCourrierRoot() throws Exception{
        System.out.println("GedTool.getGedTypeCourrierRoot()");
        if(getRootGed()==null)
            throw new Exception("Root ged de l'application non defini !!!");
        GediTypeCourrierNew root = null;
        try {
            root = gediBus.typeCourrierNewForTypeOrdreChar(getRootGed());
        } catch (TypeCourrierNewNotFoundException e) {
            //Attention le root n'existe pas !!!
            throw new Exception("Attention le root "+getRootGed()+" de l'application n'existe pas !!!");
        }
        return root;
    }
    
    private NSArray getPathForName(String name){
        if(name == null || name.indexOf(FileCtrl.SEPARATOR)==NSArray.NotFound)
            return new NSArray();
        return NSArray.componentsSeparatedByString(name.replaceAll(FileCtrl.SEPARATOR+FileCtrl.getFileName(name),""), FileCtrl.SEPARATOR);
    }
    
    public boolean deleteFileIntoGed(Integer numero, Integer persId){
        _lastError = null;
        CktlUserInfoDB user = new CktlUserInfoDB(app.dataBus());
        user.compteForPersId(persId, true);
        try {
            gediBus.removeCourrier(numero, user.noIndividu());
        } catch (DeleteCourrierFailedException e) {
            e.printStackTrace();
            _lastError = e;
            return false;
        }
        return true;
    }
    
    public NSArray listUrlFileIntoGed(Integer couNumero){
        NSArray list = null;
        try {
            list =gediBus.referenceLienForCourrier(gediBus.courrierForCouNumero(couNumero), GediBus.TYPE_REFERENCE_FICHIER);
        } catch (ReferenceLienNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            _lastError = e;
            return null;
        } catch (CourrierNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            _lastError = e;
            return null;
        }
        if(list==null || list.count()==0)
        {
            _lastError = new Exception("Aucun fichier joint !");
            return null;
        }
        //return (NSArray)list.valueForKey("rflLien");
        NSArray docs = (NSArray)list.valueForKey("rflDocumentId");
        NSMutableArray retour = new NSMutableArray();
        for (int i = 0; i < docs.count(); i++) {
            Number id = (Number) docs.objectAtIndex(i);
//            GEDDescription desc = app.gedBus(noIndividu.intValue()).inspectDocumentGED(id.intValue());
            if(app.config().stringForKey("URL_GED_DOWNLOAD")!=null)
                retour.addObject(app.config().stringForKey("URL_GED_DOWNLOAD")+id.intValue());
            else
                retour.addObject(((NSArray)list.valueForKey("rflLien")).objectAtIndex(i));
        }
        return retour;
    }
    
    public NSArray listNameFileIntoGed(Integer couNumero, int noIndividu){
        NSArray list = null;
        try {
            list =gediBus.referenceLienForCourrier(gediBus.courrierForCouNumero(couNumero), GediBus.TYPE_REFERENCE_FICHIER);
        } catch (ReferenceLienNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            _lastError = e;
            return null;
        } catch (CourrierNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            _lastError = e;
            return null;
        }
        if(list==null || list.count()==0)
        {
            _lastError = new Exception("Aucun fichier joint !");
            return null;
        }
        NSArray docs = (NSArray)list.valueForKey("rflDocumentId");
        NSMutableArray retour = new NSMutableArray();
        for (int i = 0; i < docs.count(); i++) {
            Number id = (Number) docs.objectAtIndex(i);
            GEDDescription desc = null;
            try {
				desc = app.gedBus(noIndividu).inspectDocumentGED(id.intValue());
			} catch (Throwable e) {
				desc = app.gedBus().inspectDocumentGED(id.intValue());
			}
            
            retour.addObject(desc.name);
        }
        return retour;
    }
    
    public Throwable getLastError(){
        return _lastError;
    }
    
    
    private void estAgent(CktlUserInfo user){
        try {
            gediBus.createAgentForPersId((Integer)user.persId(), false);
        } catch (CreateAgentFailedException e) {
            return;
        }
        try {
            gediBus.defaultReferenceForAgent(user.login());
        } catch (Throwable e) {
            try {
                gediBus.createReference("REF_"+user.nom(), user.login(), user.login());
            } catch (Throwable e1) {
            }
        }
    }
    
    private GediTypeCourrierNew repertoireForNumero(int typeOrdreChar){
    	return repertoireForNumero(""+typeOrdreChar);
    }
    
    private GediTypeCourrierNew repertoireForNumero(String typeOrdreChar)
    {
    	if(typeOrdreChar==null || typeOrdreChar.equals(""))
    	{
    		_lastError = new TypeCourrierNewNotFoundException("Aucun repertoire trouvé pour le numéro : "+typeOrdreChar);
    		return null;
    	}
    	try {
			return gediBus.typeCourrierNewForTypeOrdreChar(typeOrdreChar);
		} catch (TypeCourrierNewNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			_lastError = e;
		}
		return null;
    }
    
    public String nomRepertoireForNumero(String typeOrdreChar){
    	try {
        	return repertoireForNumero(typeOrdreChar).typeLibelle();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
    }
    
    public NSArray listNumeroRepertoireFilsForNumero(String typeOrdreChar){
    	if(typeOrdreChar==null || typeOrdreChar.equals(""))
    	{
    		_lastError = new TypeCourrierNewNotFoundException("Aucun repertoire trouvé pour le numéro : "+typeOrdreChar);
    		return null;
    	}
    	NSMutableArray args = new NSMutableArray();
		args.addObject(EOQualifier.qualifierWithQualifierFormat(TypeCourrierNew.TYPE_CODE_KEY+" = %@", new NSArray(typeOrdreChar)));
		NSMutableArray sort = new NSMutableArray();
		sort.addObject(EOSortOrdering.sortOrderingWithKey(TypeCourrierNew.TYPE_LIBELLE_KEY, EOSortOrdering.CompareAscending));
		NSArray list=null;
		try {
			list = gediBus.typeCourrierNewForQualifierAndSort(new EOAndQualifier(args), sort);
			return (NSArray)list.valueForKey(TypeCourrierNew.TYPE_ORDRE_CHAR_KEY);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			_lastError = e;
		}
		return null;
    }
    
    public String numeroRepertoirePereForNumeroFils(String typeOrdreChar){
    	GediTypeCourrierNew fils = repertoireForNumero(typeOrdreChar);
    	if(fils==null)
    		return null;
    	GediTypeCourrierNew pere = repertoireForNumero(fils.typeCode());
    	if(pere==null)
    	{
    		_lastError = new TypeCourrierNewNotFoundException("Aucun repertoire pere trouvé pour le numéro : "+typeOrdreChar);
    		return null;
    	}
    	return pere.typeOrdreChar();
    }
    
    public Integer getVisibiliteForNumeroRepertoire(String typeOrdreChar){
    	GediTypeCourrierNew rep = repertoireForNumero(typeOrdreChar);
    	if(rep!=null)
    	{
    		return new Integer(rep.typeTypeConsultation().intValue());
    	}
    	return null;
    }
    
    public void setVisibiliteForNumeroRepertoire(String typeOrdreChar,Integer visibilite){
    	GediTypeCourrierNew rep = repertoireForNumero(typeOrdreChar);
    	if(rep!=null)
    	{
    		TypeCourrierNew type =  rep.getTypeCourrierNew();
    		type.setTypeTypeConsultation(visibilite);
    		try {
				edtitingContext().lock();
				edtitingContext().saveChanges();
			} catch (Throwable e) {
				e.printStackTrace();
				_lastError = e;
			}
			finally{
				edtitingContext().unlock();
			}
    	}
    }
    
    public NSArray listNumeroCourrierForNumeroRepertoire(String typeOrdreChar){
		NSArray list = null;
    	try {
    		list = gediBus.courrierForTypeCourrier(typeOrdreChar);
		} catch (CourrierNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			_lastError = e;
			return null;
		}
		try {
			return (NSArray)list.valueForKey(Courrier.COU_NUMERO_KEY);
		} catch (Throwable e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public NSArray numerosRepertoiresForNumeroCourrier(int numero){
    	NSArray list = null;
    	try {
			 list = gediBus.typeCourrierForCourrier(new Integer(numero));
		} catch (TypeCourrierNewNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			_lastError = e;
		}
		if(list!=null)
		{
			return (NSArray)list.valueForKey(TypeCourrierNew.TYPE_ORDRE_CHAR_KEY);
		}
		return null;
    	
    }
    
    
    public String numeroRepertoireForNumeroCourrier(int numero){
    	NSArray list = numerosRepertoiresForNumeroCourrier(numero);
    	if(list==null)
    		return null;
    	for (int i = 0; i < list.count(); i++) {
			String num = (String)list.objectAtIndex(i);
			if(estUnFilsDeRoot(num))
				return num;
		}
    	return null;
    }
    
    private boolean estUnFilsDeRoot(String numero){
    	if(numero==null)
    		return false;
    	GediTypeCourrierNew rep = repertoireForNumero(numero);
    	if(rep==null)
    		return false;
    	if(numero.equals(getRootGed()))
    		return true;
    	if(rep.typeCode().equals(getRootGed()))
    		return true;
    	return estUnFilsDeRoot(rep.typeCode());
    }

	/**
	 * @return the gediBus
	 */
	public GediBus getGediBus() {
		return gediBus;
	}

	/**
	 * @param gediBus the gediBus to set
	 */
	public void setGediBus(GediBus gediBus) {
		this.gediBus = gediBus;
	}

}
