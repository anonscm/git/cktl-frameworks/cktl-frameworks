/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.modele;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlpersonne.common.metier.EOLangue;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class StagesModeleHelper {

	private static StagesModeleHelper instance = null;

	private static final String FILTER_EXT_ODT = "odt";
	private static final String FILTER_REGEX_DOT = "\\.";
	private static final String FILTER_REGEX_LOCALE = "_[a-z]{2}_[A-Z]{2}";

	private static final FilenameFilter FILE_NAME_FILTER = new FilenameFilter() {
		public boolean accept(File dir, String name) {
			return name.endsWith(FILTER_EXT_ODT)
					&& !name.matches(".*" + FILTER_REGEX_LOCALE + FILTER_REGEX_DOT + FILTER_EXT_ODT);
		}
	};

	/**
	 * @param newInstance
	 *            Une instance de {@link StagesModeleHelper}
	 */
	protected static void setInstance(StagesModeleHelper newInstance) {
		instance = newInstance;
	}

	/**
	 * @return L'instance de {@link StagesModeleHelper}
	 */
	public static StagesModeleHelper getInstance() {
		if (instance == null) {
			instance = new StagesModeleHelper();
		}
		return instance;
	}

	protected StagesModeleHelper() {
	}

	/**
	 * @param dirPath
	 *            Le répertoire où chercher les modèles
	 * @return La liste des modèles
	 */
	public List<String> getListeModeles(String dirPath) {
		return extractNames(dirPath, FILE_NAME_FILTER);
	}

	/**
	 * @param dirPath
	 *            Le répertoire où chercher les modèles
	 * @param modeleNom
	 *            Le nom du modele
	 * @return La liste des langues pour ce modele
	 */
	public List<String> getLanguesForModele(String dirPath, String modeleNom) {
		List<String> listeNames = extractNames(dirPath, getFilterByName(modeleNom));
		if (listeNames != null) {
			List<String> listeLangues = new ArrayList<String>();
			for (String name : listeNames) {
				String langue = StringUtils.substringAfter(name, modeleNom + "_");
				if (!StringCtrl.isEmpty(langue)) {
					listeLangues.add(langue);
				}
			}
			return listeLangues;
		}
		return null;
	}

	/**
	 * @param locale
	 *            La locale, du type en_US
	 * @return L'affichage de la langue et du pays associés
	 */
	public String localeToString(String locale) {
		if (StringCtrl.isEmpty(locale)) {
			return null;
		}
		String[] loc = locale.split("_");
		return getLlLangue(loc[0]) + "(" + getPays(loc[1]) + ")";
	}

	/**
	 * @param cLangue
	 *            Le code ISO 639-1 de la langue recherchée
	 * @return Le libellé en français
	 */
	public String getLlLangue(String cLangue) {
		if (StringCtrl.isEmpty(cLangue)) {
			return null;
		}
		EOEditingContext ec = ERXEC.newEditingContext();
		EOLangue langue = EOLangue.fetchByKeyValue(ec, EOLangue.C_LANGUE_KEY, cLangue.toLowerCase());
		if (langue != null) {
			return langue.llLangue();
		}
		return null;
	}

	/**
	 * @param cPays
	 *            Le code ISO 3166-1-alpha-2 du pays
	 * @return Le libellé du pays en français
	 */
	public String getPays(String cPays) {
		if (StringCtrl.isEmpty(cPays)) {
			return null;
		}
		EOEditingContext ec = ERXEC.newEditingContext();
		EOPays pays = EOPays.fetchByKeyValue(ec, EOPays.CODE_ISO_KEY, cPays.toLowerCase());
		if (pays != null) {
			return StringCtrl.capitalizeWords(pays.llPays());
		}
		return null;
	}

	/**
	 * @param dirPath
	 *            Le répertoire où chercher les modèles
	 * @param modeleNom
	 *            Le nom du modele
	 * @param langue
	 *            La langue à utiliser (du type "<code>en_US</code>")
	 * @return Le chemin vers le fichier
	 */
	public String getModelePath(String dirPath, String modeleNom, String langue) {
		String path = dirPath + File.separator + modeleNom;
		if (!StringCtrl.isEmpty(langue)) {
			path += "_" + langue;
		}
		path += "." + FILTER_EXT_ODT;
		return path;
	}

	private List<String> extractNames(String dirPath, FilenameFilter filter) {
		File directory = new File(dirPath);
		if (directory.exists()) {
			List<String> listeModeles = Arrays.asList(directory.list(filter));
			return removeExt(listeModeles);
		}
		return null;
	}

	/**
	 * @param listeModeles
	 *            Une liste de noms de fichiers
	 * @return La liste des fichiers sans leur extension
	 */
	private List<String> removeExt(List<String> listeModeles) {
		List<String> listeModelesNoms = new ArrayList<String>();
		for (String modele : listeModeles) {
			listeModelesNoms.add(StringUtils.substringBefore(modele, "." + FILTER_EXT_ODT));
		}
		return listeModelesNoms;
	}

	private FilenameFilter getFilterByName(final String modeleNom) {
		return new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.matches(modeleNom + "(" + FILTER_REGEX_LOCALE + ")?" + FILTER_REGEX_DOT + FILTER_EXT_ODT);
			}
		};
	}
}
