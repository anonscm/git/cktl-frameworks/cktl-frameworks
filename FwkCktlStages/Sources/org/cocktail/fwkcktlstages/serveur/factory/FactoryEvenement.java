/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.factory;

import javax.mail.MessagingException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgEtat;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgEvenementType;
import org.cocktail.fwkcktlstages.serveur.mail.FwkCktlStagesMailFactory;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

/**
 * Gère la création des évènements lors des actions des utilisateurs
 * 
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public final class FactoryEvenement extends AFactory {

	private static FactoryEvenement instance = null;
	private static Logger log = Logger.getLogger(FactoryEvenement.class);

	protected static void setInstance(FactoryEvenement newInstance) {
		instance = newInstance;
	}

	/**
	 * @return L'instance statique
	 */
	public static FactoryEvenement getInstance() {
		if (instance == null) {
			instance = new FactoryEvenement();
		}
		return instance;
	}

	@Override
	protected Logger log() {
		return log;
	}

	// -- -- -- -- --

	private EOStgEvenement creer(EOStgStage stage, EOStgEvenementType typeEvent, Integer persId, String lcEtatOld,
			String lcEtatNew) {
		return creer(stage, typeEvent, persId, null, lcEtatOld, lcEtatNew);
	}

	private EOStgEvenement creer(EOStgStage stage, EOStgEvenementType typeEvent, Integer persId, String msg,
			String lcEtatOld, String lcEtatNew) {
		EOStgEvenement newEvent = null;
		if (stage != null && typeEvent != null && persId != null) {
			newEvent = EOStgEvenement.creerInstance(stage.editingContext());
			newEvent.addToFwkpers_IndividusRelationship(EOIndividu.individuWithPersId(stage.editingContext(), persId));
			newEvent.setPersIdCreateur(persId);
			newEvent.setStgStageRelationship(stage);
			newEvent.setStgEvenementTypeRelationship(typeEvent);
			newEvent.setSevCommentaire(msg);

			newEvent.setToStgEtatOldRelationship(FinderStgEtat.getInstance().getStgEtat(stage.editingContext(),
					lcEtatOld));
			newEvent.setToStgEtatNewRelationship(FinderStgEtat.getInstance().getStgEtat(stage.editingContext(),
					lcEtatNew));

		}
		debug(newEvent);
		return newEvent;
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param createurPersId
	 *            Le <code>PERS_ID</code> du créateur de la demande
	 */
	public void eventCreationDemande(EOStgStage stage, Integer createurPersId) {
		// FIXME ajouter vérification d'unicité sur cet évènement
		creer(stage, FinderStgEvenementType.getEventCreation(stage.editingContext()), createurPersId,
				EOStgEtat.LC_INITIAL, EOStgEtat.LC_INITIAL);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventDemandeValidationPedagogique(EOStgStage stage, Integer utilisateurPersId)
			throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventDemandeValidationPeda(stage.editingContext()), utilisateurPersId,
				EOStgEtat.LC_INITIAL, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE);
		FwkCktlStagesMailFactory.getInstance().envoyerMailDemandeValidationPedagogique(stage, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param lcEtatSuivant
	 *            L'état suivant
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventAcceptationValidationPedagogique(EOStgStage stage, Integer utilisateurPersId, String lcEtatSuivant)
			throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventAcceptationValidationPedagogique(stage.editingContext()),
				utilisateurPersId, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE, lcEtatSuivant);
		FwkCktlStagesMailFactory.getInstance().envoyerMailValidationPedagogique(stage, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param raison
	 *            La raison du refus
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventRefusValidationPedagogique(EOStgStage stage, Integer utilisateurPersId, String raison)
			throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventRefusValidationPedagogique(stage.editingContext()),
				utilisateurPersId, raison, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE, EOStgEtat.LC_INITIAL);
		FwkCktlStagesMailFactory.getInstance().envoyerMailRefusPedagogique(stage, utilisateurPersId, raison);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventAcceptationValidationTuteur(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventAcceptationValidationTuteur(stage.editingContext()),
				utilisateurPersId, EOStgEtat.LC_ATTENTE_VALIDATION_TUTEUR,
				EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE);
		FwkCktlStagesMailFactory.getInstance().envoyerMailValidationTuteur(stage, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 */
	public void eventAnnulationValidationTuteur(EOStgStage stage, Integer utilisateurPersId) {
		creer(stage, FinderStgEvenementType.getEventModificationTuteur(stage.editingContext()), utilisateurPersId,
				EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE, EOStgEtat.LC_ATTENTE_VALIDATION_TUTEUR);
		// FwkCktlStagesMailFactory.getInstance().envoyerMailValidationTuteur(stage, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventRefusValidationTuteur(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventRefusValidationTuteur(stage.editingContext()), utilisateurPersId,
				EOStgEtat.LC_ATTENTE_VALIDATION_TUTEUR, EOStgEtat.LC_ATTENTE_DESIGNATION_TUTEUR);
		FwkCktlStagesMailFactory.getInstance().envoyerMailRefusTuteur(stage, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventAcceptationValidationAdministrative(EOStgStage stage, Integer utilisateurPersId)
			throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventAcceptationValidationAdministrative(stage.editingContext()),
				utilisateurPersId, EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE, EOStgEtat.LC_ATTENTE_EDITION);
		FwkCktlStagesMailFactory.getInstance().envoyerMailValidationAdministrative(stage, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 */
	public void eventAnnulationValidationAdministrative(EOStgStage stage, Integer utilisateurPersId) {
		creer(stage, FinderStgEvenementType.getEventAnnulationValidationAdministrative(stage.editingContext()),
				utilisateurPersId, EOStgEtat.LC_ATTENTE_EDITION, EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param lcEtatPrecedent
	 *            L'état précédent
	 */
	public void eventAnnulationValidationPedagogique(EOStgStage stage, Integer utilisateurPersId, String lcEtatPrecedent) {
		creer(stage, FinderStgEvenementType.getEventAnnulationValidationPedagogique(stage.editingContext()),
				utilisateurPersId, lcEtatPrecedent, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param lcEtat
	 *            L'état en cours
	 */
	public void eventModificationDemande(EOStgStage stage, Integer utilisateurPersId, String lcEtat) {
		creer(stage, FinderStgEvenementType.getEventModificationDemande(stage.editingContext()), utilisateurPersId,
				lcEtat, lcEtat);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param lcEtat
	 *            L'état courant
	 */
	public void eventModificationAdministrativeDemande(EOStgStage stage, Integer utilisateurPersId, String lcEtat) {
		creer(stage, FinderStgEvenementType.getEventModificationAdministrativeDemande(stage.editingContext()),
				utilisateurPersId, lcEtat, lcEtat);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventEditionConvention(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventEditionConvention(stage.editingContext()), utilisateurPersId,
				EOStgEtat.LC_ATTENTE_EDITION, EOStgEtat.LC_ATTENTE_SIGNATURES);
		FwkCktlStagesMailFactory.getInstance().envoyerMailEditionConvention(stage, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 */
	public void eventSuppressionConvention(EOStgStage stage, Integer utilisateurPersId) {
		creer(stage, FinderStgEvenementType.getEventSuppressionConvention(stage.editingContext()), utilisateurPersId,
				EOStgEtat.LC_ATTENTE_SIGNATURES, EOStgEtat.LC_ATTENTE_EDITION);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventSignaturesConvention(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventSignaturesConvention(stage.editingContext()), utilisateurPersId,
				EOStgEtat.LC_ATTENTE_SIGNATURES, EOStgEtat.LC_CONVENTION_SIGNEE);
		FwkCktlStagesMailFactory.getInstance().envoyerMailVerrouillageConvention(stage, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param lcEtatPrecedent
	 *            L'état précédent
	 * @param lcEtatSuivant
	 *            L'état suivant
	 */
	public void eventModificationTuteur(EOStgStage stage, int utilisateurPersId, String lcEtatPrecedent,
			String lcEtatSuivant) {
		String str = null;
		if (stage.toTuteur() == null) {
			str = "Suppression";
		} else {
			str = StringCtrl.capitalizeWords(stage.toTuteur().getNomPrenomAffichage());
		}
		creer(stage, FinderStgEvenementType.getEventModificationTuteur(stage.editingContext()), utilisateurPersId, str,
				lcEtatPrecedent, lcEtatSuivant);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 * @param lcEtatPrecedent
	 *            L'état précédent
	 */
	public void eventSuppressionTotaleConvention(EOStgStage stage, Integer utilisateurPersId, String lcEtatPrecedent)
			throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventSuppressionTotaleConvention(stage.editingContext()),
				utilisateurPersId, lcEtatPrecedent, EOStgEtat.LC_CONVENTION_SUPPRIMEE);
		FwkCktlStagesMailFactory.getInstance().envoyerMailSuppressionTotaleConvention(stage, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param lcEtatSuivant
	 *            L'état suivant
	 */
	public void eventAnnulationSuppressionTotaleConvention(EOStgStage stage, Integer utilisateurPersId,
			String lcEtatSuivant) {
		creer(stage, FinderStgEvenementType.getEventAnnulationSuppressionTotaleConvention(stage.editingContext()),
				utilisateurPersId, EOStgEtat.LC_CONVENTION_SUPPRIMEE, lcEtatSuivant);
		// FwkCktlStagesMailFactory.getInstance().envoyerMailSuppressionTotaleConvention(stage, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param raison
	 *            La raison de la résiliation
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventResilierConvention(EOStgStage stage, int utilisateurPersId, String raison)
			throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventResiliationConvention(stage.editingContext()), utilisateurPersId,
				raison, EOStgEtat.LC_CONVENTION_SIGNEE, EOStgEtat.LC_CONVENTION_RESILIEE);
		FwkCktlStagesMailFactory.getInstance().envoyerMailResiliationConvention(stage, utilisateurPersId, raison);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 */
	public void eventAnnulerResiliationConvention(EOStgStage stage, Integer utilisateurPersId) {
		creer(stage, FinderStgEvenementType.getEventAnnulationResiliationConvention(stage.editingContext()),
				utilisateurPersId, EOStgEtat.LC_CONVENTION_RESILIEE, EOStgEtat.LC_CONVENTION_SIGNEE);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventCreationAvenant(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventCreationAvenant(stage.editingContext()), utilisateurPersId,
				EOStgEtat.LC_CONVENTION_SIGNEE, EOStgEtat.LC_CONVENTION_SIGNEE);
		FwkCktlStagesMailFactory.getInstance().envoyerMailCreationAvenant(stage, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param avenant
	 *            L'avenant
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 */
	public void eventModificationAvenant(EOStgStage stage, EOStgAvenant avenant, Integer utilisateurPersId) {
		creer(stage, FinderStgEvenementType.getEventModificationAvenant(stage.editingContext()), utilisateurPersId,
				"Avenant #" + avenant.primaryKey(), EOStgEtat.LC_CONVENTION_SIGNEE, EOStgEtat.LC_CONVENTION_SIGNEE);
		// FwkCktlStagesMailFactory.getInstance().envoyerMailModificationAvenant(stage, avenant, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param avenant
	 *            L'avenant
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventValidationAvenant(EOStgStage stage, EOStgAvenant avenant, int utilisateurPersId)
			throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventValidationAvenant(stage.editingContext()), utilisateurPersId,
				"Avenant #" + avenant.primaryKey(), EOStgEtat.LC_CONVENTION_SIGNEE, EOStgEtat.LC_CONVENTION_SIGNEE);
		FwkCktlStagesMailFactory.getInstance().envoyerMailValidationAvenant(stage, avenant, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param avenant
	 *            L'avenant
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventRefusAvenant(EOStgStage stage, EOStgAvenant avenant, int utilisateurPersId)
			throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventRefusAvenant(stage.editingContext()), utilisateurPersId,
				"Avenant #" + avenant.primaryKey(), EOStgEtat.LC_CONVENTION_SIGNEE, EOStgEtat.LC_CONVENTION_SIGNEE);
		FwkCktlStagesMailFactory.getInstance().envoyerMailRefusAvenant(stage, avenant, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param avenant
	 *            L'avenant
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventSuppressionAvenant(EOStgStage stage, EOStgAvenant avenant, int utilisateurPersId)
			throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventSuppressionAvenant(stage.editingContext()), utilisateurPersId,
				"Avenant #" + avenant.primaryKey(), EOStgEtat.LC_CONVENTION_SIGNEE, EOStgEtat.LC_CONVENTION_SIGNEE);
		FwkCktlStagesMailFactory.getInstance().envoyerMailSuppressionAvenant(stage, avenant, utilisateurPersId);
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param avenant
	 *            L'avenant
	 * @param utilisateurPersId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException
	 *             Si erreur lors de l'envoi du mail
	 */
	public void eventVerrouillageAvenant(EOStgStage stage, EOStgAvenant avenant, int utilisateurPersId)
			throws MessagingException {
		creer(stage, FinderStgEvenementType.getEventVerrouillageAvenant(stage.editingContext()), utilisateurPersId,
				"Avenant #" + avenant.primaryKey(), EOStgEtat.LC_CONVENTION_SIGNEE, EOStgEtat.LC_CONVENTION_SIGNEE);
		FwkCktlStagesMailFactory.getInstance().envoyerMailVerrouillageAvenant(stage, avenant, utilisateurPersId);
	}
}
