/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.factory;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlged.serveur.metier.Document;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlged.serveur.metier.service.DocumentService;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgPieceJointe;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlwebapp.server.CktlGedBus;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FactoryStgPieceJointe extends AFactory {
	private static FactoryStgPieceJointe instance = null;
	private static Logger log = Logger.getLogger(FactoryStgPieceJointe.class);

	protected static void setInstance(FactoryStgPieceJointe newInstance) {
		instance = newInstance;
	}

	/**
	 * @return L'instance statique
	 */
	public static FactoryStgPieceJointe getInstance() {
		if (instance == null) {
			instance = new FactoryStgPieceJointe();
		}
		return instance;
	}

	@Override
	protected Logger log() {
		return log;
	}

	/**
	 * @param stage
	 *            Un {@link EOStgStage}
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param gedBus
	 *            Un {@link CktlGedBus}
	 * @param data
	 *            Les données du fichier
	 * @param titre
	 *            Le titre du fichier
	 * @param fileName
	 *            Le nom du fichier
	 * @param catCode
	 *            La catégorie
	 * @param type
	 *            Le type de pièce jointe
	 * @return Un nouveau {@link EOStgPieceJointeRepart}
	 * @throws Exception
	 *             Si une erreur lors du dépôt du fichier
	 */
	public EOStgPieceJointeRepart ajouterPieceJointe(EOStgStage stage, Integer persId, DocumentService documentService, byte[] data,
			String titre, String fileName, String catCode, EOStgPieceJointeType type) throws IOException {
		EOStgPieceJointeRepart newPj = null;
		if (stage != null && persId != null && type != null) {
		    Document doc = null;
			if (documentService != null) {
				// Dépôt sur la GED
			    doc = documentService.creerFileDocument("org.cocktail.ged.typedocument.gfc.acte.contrat", fileName, data, persId);
				if (doc == null) {
					throw new IOException("Erreur lors du dépôt de la convention");
				}
				doc.setObjet(titre);
				doc.setCommentaire(titre);
				doc.setMotsClefs(catCode);
				debug("Depot du fichier " + fileName + " sur la GED");
			}
			// Ajout du document dans la base
			newPj = EOStgPieceJointeRepart.creerInstance(stage.editingContext());
			newPj.setSpjrPersId(persId);
			newPj.setStgDocumentRelationship((EODocument)doc);
			newPj.setSpjrVersionPapier("O");
			newPj.setSpjrConventionOfficielle(false);
			newPj.setSpjrImpressionObligatoire(false);
			newPj.setStgPieceJointeTypeRelationship(type);
			newPj.setStgStageRelationship(stage);
		}
		debug(newPj);
		return newPj;
	}

	/**
	 * @param stage
	 *            Un {@link EOStgStage}
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param type
	 *            Le type de pièce jointe
	 * @throws Exception
	 *             Si une erreur lors du dépôt du fichier
	 */
	public void supprimerPieceJointe(EOStgStage stage, Integer persId, EOStgPieceJointeType type)
			throws Exception {
		if (stage == null || persId == null || type == null) {
			return;
		}

		EOStgPieceJointeRepart pieceJointe = FinderStgPieceJointe.getUnePieceJointe(stage, type);
		if (pieceJointe == null) {
			return;
		}

		if (pieceJointe.stgDocument() != null) {
		    pieceJointe.stgDocument().delete();
		}
		stage.deleteToPieceJointeRepartsRelationship(pieceJointe);
	}
	
	/**
	 * @param stage
	 *            Un {@link EOStgStage}
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param type
	 *            Le type de pièce jointe
	 * @throws Exception
	 *             Si une erreur lors du dépôt du fichier
	 */
	public void supprimerPieceJointe(EOStgStage stage, EOStgPieceJointeRepart pieceJointe, Integer persId)
	        throws Exception {
	    if (pieceJointe == null || persId == null) {
	        return;
	    }
	    if (pieceJointe.stgDocument() != null) {
	        pieceJointe.stgDocument().delete();
	    }
	    stage.deleteToPieceJointeRepartsRelationship(pieceJointe);
	}
	
	
}
