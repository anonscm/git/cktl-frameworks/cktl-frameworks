/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.factory;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgType;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FactoryStgType extends AFactory {
	private static FactoryStgType instance = null;
	private static Logger log = Logger.getLogger(FactoryStgType.class);

	protected static void setInstance(FactoryStgType newInstance) {
		instance = newInstance;
	}

	/**
	 * @return L'instance statique
	 */
	public static FactoryStgType getInstance() {
		if (instance == null) {
			instance = new FactoryStgType();
		}
		return instance;
	}

	@Override
	protected Logger log() {
		return log;
	}

	/**
	 * @param edc
	 *            Un {@link EOEditingContext}
	 * @return Un nouveau {@link EOStgType}
	 */
	public EOStgType creer(EOEditingContext edc) {
		EOStgType newType = null;
		if (edc != null) {
			newType = EOStgType.creerInstance(edc);
		}
		debug(newType);
		return newType;
	}
}