/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.factory;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryConvention;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderTypeContrat;
import org.cocktail.fwkcktldroitsutils.common.finders.EOUtilisateurFinder;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlstages.serveur.exception.FwkCktlStagesException;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgAcces;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgAnnee;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgConfig;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgEtat;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgAcces;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgType;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FactoryStgStage extends AFactory {
	private static FactoryStgStage instance = null;
	private static Logger log = Logger.getLogger(FactoryStgStage.class);

	private static final String TYPE_CONTRAT_ID = "CONV_STAGE";
	private static final long NB_JOURS_HEBDO = 5;

	protected static void setInstance(FactoryStgStage newInstance) {
		instance = newInstance;
	}

	/**
	 * @return L'instance statique
	 */
	public static FactoryStgStage getInstance() {
		if (instance == null) {
			instance = new FactoryStgStage();
		}
		return instance;
	}

	@Override
	protected Logger log() {
		return log;
	}

	private TypeContrat getTypeContratConventionDeStage(EOEditingContext editingcontext) throws ExceptionFinder {
		FinderTypeContrat tc = new FinderTypeContrat(editingcontext);
		return tc.findWithTyconIdInterne(TYPE_CONTRAT_ID);
	}

	/**
	 * Créé un nouveau stage, avec la convention associée dans ACCORD, pour l'année courrante
	 * 
	 * @param edc
	 *            L'EditingContext
	 * @param etudiant
	 *            L'étudiant
	 * @param mec
	 *            L'EC
	 * @param type
	 *            Le type de stage
	 * @param utilisateur_pers_id
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param serviceGestionnaire
	 *            La structure du service gestionnaire
	 * @return Un nouveau stage
	 * @throws Exception
	 *             Une exception
	 */
	public EOStgStage create(EOEditingContext edc, EOEtudiant etudiant, EC mec, EOStgType type,
			Integer utilisateur_pers_id, EOStructure serviceGestionnaire) throws Exception {
		return create(edc, etudiant, mec, type, utilisateur_pers_id, serviceGestionnaire, null);
	}

	/**
	 * Créé un nouveau stage, avec la convention associée dans ACCORD, pour l'année donnée
	 * 
	 * @param edc
	 *            L'EditingContext
	 * @param etudiant
	 *            L'étudiant
	 * @param mec
	 *            L'EC
	 * @param type
	 *            Le type de stage
	 * @param utilisateur_pers_id
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param serviceGestionnaire
	 *            La structure du service gestionnaire
	 * @param annee
	 *            L'année en cours
	 * @return Un nouveau stage
	 * @throws Exception
	 *             Une exception
	 */
	public EOStgStage create(EOEditingContext edc, EOEtudiant etudiant, EC mec, EOStgType type,
			Integer utilisateur_pers_id, EOStructure serviceGestionnaire, EOStgAnnee annee) throws Exception {
		EOStgStage newStage = null;

		if (edc != null && etudiant != null) {
			annee = initAnnee(edc, annee);
			etudiant = etudiant.localInstanceIn(edc);
			serviceGestionnaire = serviceGestionnaire.localInstanceIn(edc);

			EOStgEtat etat = FinderStgEtat.getInstance().getStgEtatInitial(edc);
			EOStgAcces acces = FinderStgAcces.getDefault(edc);
			EOStructure etablissement = getEtablissement(edc);
			EOUtilisateur utilisateur = getUtilisateur(edc, utilisateur_pers_id);
			
			// Convention
			FactoryConvention factoryConvention = new FactoryConvention(edc, false);
			Contrat conv = factoryConvention.creerConventionVierge(utilisateur, etablissement);
			conv.setTypeContratRelationship(getTypeContratConventionDeStage(edc));
			conv.setTypeClassificationContrat(TypeClassificationContrat.typeClassificationContratForCode(edc,
					TypeClassificationContrat.TYPE_CLASSIFICATION_CODE_CONV_STAGE));
			// TODO Type classification
			conv.setCentreResponsabilite(serviceGestionnaire);
			// FIXME
			conv.setConObjet("Convention de stage");
			conv.setDateDebut(null);
			conv.setDateFin(null);

			factoryConvention.ajouterPartenaireContractant(conv, etudiant.toIndividu(), Boolean.FALSE, BigDecimal.ZERO);

			newStage = EOStgStage.creerInstance(edc);
			newStage.setStgDCreation(DateCtrl.now());
			newStage.setStgLangue("fr");
			// FIXME
			newStage.setStgGratObligatoire("N");
			newStage.setStgNbJoursHebdo(NB_JOURS_HEBDO);
			newStage.setToAccesRelationship(acces);
			// Relations obligatoires : stgAccess, stgAnnee, stgEtat, StgType
			newStage.setToAnneeRelationship(annee);
			newStage.setToEtatRelationship(etat);

			type = initType(edc, type);
			newStage.setToTypeRelationship(type);

			newStage.setToEtudiantRelationship(etudiant);
			newStage.setToContratRelationship(conv);
			
			if (mec != null) {				
				newStage.setMecKey(mec.getId());
			}
			FactoryEvenement.getInstance().eventCreationDemande(newStage, utilisateur_pers_id);
			
		} else {
			throw new FwkCktlStagesException("Erreur d'initialisation : edc=" + edc + " ; etudiant=" + etudiant);
		}
		debug(newStage);
		return newStage;
	}

	private EOStgAnnee initAnnee(EOEditingContext edc, EOStgAnnee annee) {
		if (annee == null) {
			annee = FinderStgAnnee.getStgAnneeCourante(edc);
		} else {
			annee = annee.localInstanceIn(edc);
		}
		return annee;
	}

	private EOStgType initType(EOEditingContext edc, EOStgType type) {
		if (type == null) {
			type = EOStgType.fetchFirstByQualifier(edc, null);
		} else {
			type = type.localInstanceIn(edc);
		}
		if (type == null) {
			throw new FwkCktlStagesException("Erreur lors de l'initialisation du type de stage.");
		}
		return type;
	}

	private EOUtilisateur getUtilisateur(EOEditingContext edc, Integer utilisateur_pers_id) {
		EOUtilisateur utilisateur = EOUtilisateurFinder.fecthUtilisateurByPersId(edc, utilisateur_pers_id);
		if (utilisateur == null) {
			try {
				utilisateur = EOUtilisateurFinder.fecthUtilisateurByPersId(edc,
						new Integer(FinderStgConfig.getInstance().getDefaultUserPersId()));
				if (utilisateur == null) {
					throw new Exception();
				}
			} catch (Exception e) {
				throw new FwkCktlStagesException(
						"L'utilisateur par défaut (persId = "
								+ FinderStgConfig.getInstance().getDefaultUserPersId()
								+ ") n'a pas été trouvé ! Voir la valeur de la clé 'default_user_pers_id' dans la table STG_CONFIG");
			}
		}
		return utilisateur;
	}

	private EOStructure getEtablissement(EOEditingContext edc) {
		EOStructure etablissement = EOStructure.rechercherEtablissement(edc);
		if (etablissement == null) {
			throw new FwkCktlStagesException("Erreur : Aucun établissement trouvé !");
		}
		return etablissement;
	}
}