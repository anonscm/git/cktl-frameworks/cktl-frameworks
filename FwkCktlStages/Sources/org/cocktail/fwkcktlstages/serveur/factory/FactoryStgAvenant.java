/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.factory;

import org.apache.log4j.Logger;
import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.ModeGestion;
import org.cocktail.cocowork.server.metier.convention.TypeAvenant;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryAvenant;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderModeGestion;
import org.cocktail.fwkcktldroitsutils.common.finders.EOUtilisateurFinder;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlstages.serveur.exception.FwkCktlStagesException;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgConfig;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FactoryStgAvenant extends AFactory {
	private static Logger log = Logger.getLogger(FactoryStgAvenant.class);

	private static FactoryStgAvenant instance = null;

	protected static void setInstance(FactoryStgAvenant newInstance) {
		instance = newInstance;
	}

	/**
	 * @return L'instance statique
	 */
	public static FactoryStgAvenant getInstance() {
		if (instance == null) {
			instance = new FactoryStgAvenant();
		}
		return instance;
	}

	@Override
	protected Logger log() {
		return log;
	}

	/**
	 * @param unStage
	 *            Le stage
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @return Un nouvel avenant
	 * @throws ExceptionFinder
	 *             Exception lors de la recherche
	 * @throws ExceptionUtilisateur
	 *             Exception sur l'utilisateur
	 */
	public EOStgAvenant creer(EOStgStage unStage, Integer persId) throws ExceptionFinder, ExceptionUtilisateur {
		EOStgAvenant newAvenant = null;

		if (unStage != null) {
			newAvenant = EOStgAvenant.creerInstance(unStage.editingContext());
			newAvenant.setPersIdCreation(persId);
			newAvenant.setPersIdModification(persId);

			newAvenant.setToStageRelationship(unStage);

			FactoryAvenant FA = new FactoryAvenant(unStage.editingContext(), true);
			FinderModeGestion fmg = new FinderModeGestion(unStage.editingContext());
			ModeGestion modeGestion = (ModeGestion) fmg.findWithLibelleCourt(ModeGestion.MODE_GESTION_SIF).lastObject();
			TypeAvenant typeAvenant = TypeAvenant.TypeAvenantWithCode(unStage.editingContext(),
					TypeAvenant.CODE_TYPE_AVENANT_ADMINISTRATIF);

			EOUtilisateur utilisateur = EOUtilisateurFinder.fecthUtilisateurByPersId(unStage.editingContext(), persId);
			if (utilisateur == null) {
				try {
					utilisateur = EOUtilisateurFinder.fecthUtilisateurByPersId(unStage.editingContext(), new Integer(
							FinderStgConfig.getInstance().getDefaultUserPersId()));
					if (utilisateur == null) {
						throw new Exception();
					}
				} catch (Exception e) {
					throw new FwkCktlStagesException(
							"L'utilisateur par défaut (persId = "
									+ FinderStgConfig.getInstance().getDefaultUserPersId()
									+ ") n'a pas été trouvé ! Voir la valeur de la clé 'default_user_pers_id' dans la table STG_CONFIG");
				}
			}
			// FIXME Voir les information nécessaires pour l'avenant dans ACCORDS
			Avenant avenant = FA.creerAvenantVierge(unStage.toContrat(), null, typeAvenant, utilisateur);
			avenant.setModeGestionRelationship(modeGestion);
			avenant.setAvtObjet("Avenant à la convention de stage");
			
			newAvenant.setToAvenantRelationship(avenant);

		}
		return newAvenant;
	}
}