/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.factory;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FactoryStgPieceJointeType extends AFactory {
	private static FactoryStgPieceJointeType instance = null;
	private static Logger log = Logger.getLogger(FactoryStgPieceJointeType.class);

	protected static void setInstance(FactoryStgPieceJointeType newInstance) {
		instance = newInstance;
	}

	/**
	 * @return L'instance statique
	 */
	public static FactoryStgPieceJointeType getInstance() {
		if (instance == null) {
			instance = new FactoryStgPieceJointeType();
		}
		return instance;
	}

	@Override
	protected Logger log() {
		return log;
	}

	/**
	 * @param edc
	 *            Un {@link EOEditingContext}
	 * @param lc
	 *            Le libellé court
	 * @param ll
	 *            Le libellé long
	 * @return Un nouveau {@link EOStgPieceJointeType}
	 */
	public EOStgPieceJointeType creer(EOEditingContext edc, String lc, String ll) {
		EOStgPieceJointeType newType = null;
		if (edc != null) {
			newType = EOStgPieceJointeType.creerInstance(edc);
			newType.setSpjtLc(lc);
			newType.setSpjtLl(ll);
		}
		debug(newType);
		return newType;
	}
}
