/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.factory;

import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgConfig;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgConfig;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FactoryStgConfig extends AFactory {
	private static FactoryStgConfig instance = null;
	private static Logger log = Logger.getLogger(FactoryStgConfig.class);

	protected static void setInstance(FactoryStgConfig newInstance) {
		instance = newInstance;
	}

	/**
	 * @return L'instance statique
	 */
	public static FactoryStgConfig getInstance() {
		if (instance == null) {
			instance = new FactoryStgConfig();
		}
		return instance;
	}

	@Override
	protected Logger log() {
		return log;
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @param configKey La clé
	 * @param configValue La valeur
	 * @param mec Un {@link EOScolMaquetteEc}
	 * @return Un nouveau {@link EOStgConfig}
	 */
	public EOStgConfig creer(EOEditingContext edc, String configKey, String configValue, EC mec) {
		EOStgConfig conf = null;
		if (edc != null && !StringCtrl.isEmpty(configKey)) {
			if (exists(configKey)) {
				throw new IllegalStateException("La variable de configuration <" + configKey + "> est déjà créée.");
			}
			conf = EOStgConfig.creerInstance(edc);
			conf.setScfgLc(configKey);
			conf.setScfgValeur(configValue);
		}
		debug(conf);
		return conf;
	}

	private boolean exists(String configKey) {
		return (FinderStgConfig.getInstance().getConfigValueByLc(configKey, false) != null);
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @param liste Liste des persId Met à jour la liste des administrateurs de l'application
	 */
	public void setListeAdmin(EOEditingContext edc, List<Integer> liste) {
		setListeIndividus(edc, liste2String(liste), EOStgConfig.CONFIG_ADMINS_PERS_ID, true);
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @param liste Liste des persId Met à jour la liste des responsables administratifs de l'application
	 */
	public void setListeResponsablesAdministratifs(EOEditingContext edc, List<Integer> liste) {
		setListeIndividus(edc, liste2String(liste), EOStgConfig.CONFIG_RESP_ADMIN_PERS_ID, false);
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @param liste Liste des persId Met à jour la liste des responsables pédagogiques de l'application
	 */
	public void setListeResponsablesPedagogiques(EOEditingContext edc, List<Integer> liste) {
		setListeIndividus(edc, liste2String(liste), EOStgConfig.CONFIG_RESP_PEDA_PERS_ID, false);
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @param liste Liste des persId Met à jour la liste des responsables structures de l'application
	 */
	public void setListeResponsablesStructures(EOEditingContext edc, List<Integer> liste) {
		setListeIndividus(edc, liste2String(liste), EOStgConfig.CONFIG_RESP_STRUCT_PERS_ID, false);
	}

	private String liste2String(List<Integer> liste) {
		if (liste == null || liste.isEmpty()) {
			return "";
		}
		String listeString = liste.get(0).toString();
		for (int i = 1; i < liste.size(); i++) {
			listeString += "," + liste.get(i).toString();
		}
		return listeString;
	}

	private void setListeIndividus(EOEditingContext edc, String liste, String configKey, boolean nonVide) {
		if (nonVide && (liste == null || liste.trim().length() == 0)) {
			throw new NSValidation.ValidationException("Il doit y avoir au moins un individu");
		}
		EOStgConfig config = initConfig(edc, configKey);
		config.setScfgValeur(liste);
	}

	private EOStgConfig initConfig(EOEditingContext edc, String configKey) {
		EOStgConfig conf = FinderStgConfig.getInstance().getUniqueConfigByLc(edc, configKey);
		if (conf == null) {
			conf = creer(edc, configKey, null, null);
		}
		return conf;
	}
}
