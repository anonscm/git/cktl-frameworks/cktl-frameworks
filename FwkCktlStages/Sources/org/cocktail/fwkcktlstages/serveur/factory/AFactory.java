/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.factory;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlstages.serveur.exception.FwkCktlStagesException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXGenericRecord;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public abstract class AFactory {

	/**
	 * @return L'instance du logger
	 */
	protected abstract Logger log();

	/**
	 * Affiche un objet dans les logs. Si cet objet hérite d'un {@link ERXGenericRecord}, alors on affiche les valeurs
	 * 
	 * @param obj
	 *            Un objet à afficher dans les logs
	 */
	public void debug(Object obj) {
		if (log().isDebugEnabled()) {
			if (obj == null) {
				log().debug("null");
			} else if (obj instanceof ERXGenericRecord) {
				log().debug(((ERXGenericRecord) obj).toLongString());
			} else {
				log().debug(obj);
			}
		}
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @param entity
	 *            Le nom de l'entity
	 * @return Un {@link EOEnterpriseObject}
	 */
	public static EOEnterpriseObject instanceForEntity(EOEditingContext ec, String entity) {
		EOClassDescription description = EOClassDescription.classDescriptionForEntityName(entity);
		if (description == null) {
			throw new FwkCktlStagesException("Impossible de recuperer la description de l'entite  \"" + entity + "\" ");
		}
		EOEnterpriseObject object = description.createInstanceWithEditingContext(ec, null);
		return object;
	}

	/**
	 * @return Un gregorianCalendar initialise a la date du jour (sans les heures/minutes/etc). Utilisez
	 *         getToday().getTime() pour recuperer la date du jour nettoyee des secondes sous forme de Date.
	 */
	private GregorianCalendar getToday() {
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		return gc;
	}

	/**
	 * @return NSTimestamp
	 */
	public final NSTimestamp getDateJour() {
		return new NSTimestamp(getToday().getTime());
	}

	/**
	 * Methode qui <b>tente</b> de contourner le bug EOF qui se produit lors d'un saveChanges avec l'erreur "reentered
	 * responseToMessage()".<br>
	 * <b>Il faut appeler cette methode avant de creer un descendant d'EOCustomObject, donc bien avant le
	 * saveChanges()</b><br>
	 * 
	 * Le principe est d'appeler la methode EOClassDescription.classDescriptionForEntityName("A") pour chaque relation
	 * de l'objet qu'on va creer. Il faut appeler cette methode avant de creer un objet. Par exemple dans le cas d'un
	 * objet Facture qui a des objets Ligne, appeler EOClassDescription.classDescriptionForEntityName("Facture") avant
	 * de creer un objet Ligne. Repeter l'operation pour toutes les relations de l'objet.
	 * 
	 * @param list
	 *            Liste de String identifiant une entite du modele.
	 * @see "http://www.omnigroup.com/mailman/archive/webobjects-dev/2002-May/023698.html"
	 */
	public static final void fixWoBug_responseToMessage(final String[] list) {
		for (int i = 0; i < list.length; i++) {
			EOClassDescription.classDescriptionForEntityName(list[i]);
		}
	}
}
