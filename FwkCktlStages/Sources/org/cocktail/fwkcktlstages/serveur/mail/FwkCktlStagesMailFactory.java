/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.mail;

import javax.mail.MessagingException;

import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlstages.serveur.finder.FactoryFinderScolarite;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgConfig;
import org.cocktail.fwkcktlstages.serveur.finder.FinderElementPedagogique;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.foundation.ERXProperties;
import er.extensions.localization.ERXLocalizer;
import er.javamail.ERJavaMail;
import er.javamail.ERMailDeliveryPlainText;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FwkCktlStagesMailFactory {

	/**
	 * Type de responsabilité
	 */
	private static enum TYPE_RESP {
		RESPONSABLE, SECRETAIRE, TUTEUR, ETUDIANT, RESPONSABLE_STRUCTURES
	};

	private static final String MAIL_SUJET = "MailSujet";
	private static final String MAIL_SIGNATURE = "MailSignature";

	private static final String CORPS_INTRO = "CorpsIntro";

	private static final String TITRE_DEMANDE_VALIDATION_PEDAGOGIQUE_ETUDIANT = "TitreDemandeValidationPedagogiqueEtudiant";
	private static final String CORPS_DEMANDE_VALIDATION_PEDAGOGIQUE_ETUDIANT = "CorpsDemandeValidationPedagogiqueEtudiant";

	private static final String TITRE_DEMANDE_VALIDATION_PEDAGOGIQUE_ADMINISTRATIF = "TitreDemandeValidationPedagogiqueAdministratif";
	private static final String CORPS_DEMANDE_VALIDATION_PEDAGOGIQUE_ADMINISTRATIF = "CorpsDemandeValidationPedagogiqueAdministratif";

	private static final String TITRE_VALIDATION_PEDAGOGIQUE_TO_TUTEUR = "TitreValidationPedagogiqueToTuteur";
	private static final String CORPS_VALIDATION_PEDAGOGIQUE_TO_TUTEUR = "CorpsValidationPedagogiqueToTuteur";

	private static final String TITRE_VALIDATION_PEDAGOGIQUE_TO_SECR = "TitreValidationPedagogiqueToSecr";
	private static final String CORPS_VALIDATION_PEDAGOGIQUE_TO_SECR = "CorpsValidationPedagogiqueToSecr";

	private static final String TITRE_REFUS_PEDAGOGIQUE = "TitreRefusPedagogique";
	private static final String CORPS_REFUS_PEDAGOGIQUE = "CorpsRefusPedagogique";

	private static final String TITRE_VALIDATION_TUTEUR = "TitreValidationTuteur";
	private static final String CORPS_VALIDATION_TUTEUR = "CorpsValidationTuteur";

	private static final String TITRE_REFUS_TUTEUR = "TitreRefusTuteur";
	private static final String CORPS_REFUS_TUTEUR = "CorpsRefusTuteur";

	private static final String TITRE_VALIDATION_ADMINISTRATIVE = "TitreValidationAdministrative";
	private static final String CORPS_VALIDATION_ADMINISTRATIVE = "CorpsValidationAdministrative";

	private static final String TITRE_EDITION_CONVENTION = "TitreEditionConvention";
	private static final String CORPS_EDITION_CONVENTION = "CorpsEditionConvention";

	private static final String TITRE_VERROUILLAGE_CONVENTION = "TitreVerrouillageConvention";
	private static final String CORPS_VERROUILLAGE_CONVENTION = "CorpsVerrouillageConvention";

	private static final String TITRE_SUPPRESSION_TOTALE_CONVENTION = "TitreSuppressionTotaleConvention";
	private static final String CORPS_SUPPRESSION_TOTALE_CONVENTION = "CorpsSuppressionTotaleConvention";

	private static final String TITRE_RESILIATION_CONVENTION = "TitreResiliationConvention";
	private static final String CORPS_RESILIATION_CONVENTION = "CorpsResiliationConvention";

	private static final String TITRE_CREATION_AVENANT_ETUDIANT = "TitreCreationAvenantEtudiant";
	private static final String CORPS_CREATION_AVENANT_ETUDIANT = "CorpsCreationAvenantEtudiant";

	private static final String TITRE_CREATION_AVENANT_ADMINISTRATIF = "TitreCreationAvenantAdministratif";
	private static final String CORPS_CREATION_AVENANT_ADMINISTRATIF = "CorpsCreationAvenantAdministratif";

	private static final String TITRE_VALIDATION_AVENANT = "TitreValidationAvenant";
	private static final String CORPS_VALIDATION_AVENANT = "CorpsValidationAvenant";

	private static final String TITRE_REFUS_AVENANT = "TitreRefusAvenant";
	private static final String CORPS_REFUS_AVENANT = "CorpsRefusAvenant";

	private static final String TITRE_VERROUILLAGE_AVENANT = "TitreVerrouillageAvenant";
	private static final String CORPS_VERROUILLAGE_AVENANT = "CorpsVerrouillageAvenant";

	private static final String TITRE_SUPPRESSION_AVENANT = "TitreSuppressionAvenant";
	private static final String CORPS_SUPPRESSION_AVENANT = "CorpsSuppressionAvenant";

	private static final String TITRE_CREATION_STRUCTURE_ETUDIANT = "TitreCreationStructureEtudiant";
	private static final String CORPS_CREATION_STRUCTURE_ETUDIANT = "CorpsCreationStructureEtudiant";

	private static final String TITRE_CREATION_STRUCTURE_ADMINISTRATIF = "TitreCreationStructureAdministratif";
	private static final String CORPS_CREATION_STRUCTURE_ADMINISTRATIF = "CorpsCreationStructureAdministratif";

	private static final String VAR_MEC_CODE = "mecCode";
	private static final String VAR_TITRE = "titre";
	private static final String VAR_APPLI_NAME = "appliName";
	private static final String VAR_APPLI_URL = "appliUrl";
	private static final String VAR_UTILISATEUR = "utilisateur";
	private static final String VAR_ETUDIANT = "etudiant";
	private static final String VAR_CONVENTION_ID = "conventionId";
	private static final String VAR_CONVENTION_ULR = "conventionUrl";
	private static final String VAR_RAISON = "raison";
	private static final String VAR_AVENANT_ID = "avenantId";
	private static final String VAR_STRUCTURE_URL = "structureUrl";

	private static FwkCktlStagesMailFactory instance;

	private static String staticSignature;
	private static String staticCorpsIntro;
	private static final String APP_URL = ((CktlWebApplication) WOApplication.application()).getApplicationURL(null);
	private static final String APP_NAME = WOApplication.application().name();
	private static final String DA_CONSULTATION_CONVENTION = "/wa/consultationConvention?id=";
	private static final String DA_CONSULTATION_STRUCTURE = "/wa/consultationStructure?id=";

	/**
	 * Ne pas instancier directement la classe, utiliser {@link FwkCktlStagesMailFactory#getInstance()}
	 */
	protected FwkCktlStagesMailFactory() {
	}

	/**
	 * @return L'instance statique
	 */
	public static FwkCktlStagesMailFactory getInstance() {
		if (instance == null) {
			instance = new FwkCktlStagesMailFactory();
		}
		return instance;
	}

	/**
	 * @param stage Un stage
	 * @param utilisateurPersId L'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailDemandeValidationPedagogique(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.RESPONSABLE);
		NSArray<String> cc = getMails(stage, TYPE_RESP.ETUDIANT);

		String sujet;
		if (stage.isDemandeur(utilisateurPersId)) {
			sujet = getSujet(stage, TITRE_DEMANDE_VALIDATION_PEDAGOGIQUE_ETUDIANT);
		} else {
			sujet = getSujet(stage, TITRE_DEMANDE_VALIDATION_PEDAGOGIQUE_ADMINISTRATIF);
		}
		String corps = getCorpsDemandeValidationPedagogique(stage, utilisateurPersId);
		sendMail(sujet, corps, from, to, cc);
	}

	/**
	 * @param stage Un stage
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailValidationPedagogique(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		NSArray<String> to = null;
		String sujet = null;
		String corps = null;

		String from = getMail(stage, utilisateurPersId);
		NSArray<String> cc = getMails(stage, TYPE_RESP.ETUDIANT, TYPE_RESP.RESPONSABLE);

		if (FinderStgConfig.getInstance().isTuteurValidation() && stage.toTuteur() != null) {
			// On envoi au tuteur, CC la secrétaire
			sujet = getSujet(stage, TITRE_VALIDATION_PEDAGOGIQUE_TO_TUTEUR);
			corps = getCorpsValidationPedagogiqueToTuteur(stage, utilisateurPersId);
			to = getMails(stage, TYPE_RESP.TUTEUR);
			cc = cc.arrayByAddingObjectsFromArray(getMails(stage, TYPE_RESP.SECRETAIRE));
		} else {
			// On envoi à la secrétaire, CC le tuteur s'il existe
			sujet = getSujet(stage, TITRE_VALIDATION_PEDAGOGIQUE_TO_SECR);
			corps = getCorpsValidationPedagogiqueToSecr(stage, utilisateurPersId);
			to = getMails(stage, TYPE_RESP.SECRETAIRE);
			if (stage.toTuteur() != null) {
				cc = cc.arrayByAddingObjectsFromArray(getMails(stage, TYPE_RESP.TUTEUR));
			}
		}
		sendMail(sujet, corps, from, to, cc);
	}

	/**
	 * @param stage Un stage
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @param raison La raison du refus
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailRefusPedagogique(EOStgStage stage, Integer utilisateurPersId, String raison) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.ETUDIANT);

		String sujet = getSujet(stage, TITRE_REFUS_PEDAGOGIQUE);
		String corps = getCorpsRefusPedagogique(stage, utilisateurPersId, raison);
		sendMail(sujet, corps, from, to, null);
	}

	/**
	 * @param stage Un stage
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailValidationTuteur(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.SECRETAIRE);
		NSArray<String> cc = getMails(stage, TYPE_RESP.ETUDIANT, TYPE_RESP.RESPONSABLE);

		String sujet = getSujet(stage, TITRE_VALIDATION_TUTEUR);
		String corps = getCorpsValidationTuteur(stage, utilisateurPersId);
		sendMail(sujet, corps, from, to, cc);
	}

	/**
	 * @param stage Un stage
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailRefusTuteur(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.ETUDIANT);
		NSArray<String> cc = getMails(stage, TYPE_RESP.RESPONSABLE);

		String sujet = getSujet(stage, TITRE_REFUS_TUTEUR);
		String corps = getCorpsRefusTuteur(stage, utilisateurPersId);
		sendMail(sujet, corps, from, to, cc);
	}

	/**
	 * @param stage Un stage
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailValidationAdministrative(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.ETUDIANT);
		NSArray<String> cc = getMails(stage, TYPE_RESP.RESPONSABLE, TYPE_RESP.TUTEUR);

		String sujet = getSujet(stage, TITRE_VALIDATION_ADMINISTRATIVE);
		String corps = getCorpsValidationAdministrative(stage, utilisateurPersId);
		sendMail(sujet, corps, from, to, cc);
	}

	/**
	 * @param stage Un stage
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailEditionConvention(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.ETUDIANT);

		String sujet = getSujet(stage, TITRE_EDITION_CONVENTION);
		String corps = getCorpsEditionConvention(stage, utilisateurPersId);
		sendMail(sujet, corps, from, to, null);
	}

	/**
	 * @param stage Un stage
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailVerrouillageConvention(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.ETUDIANT, TYPE_RESP.RESPONSABLE, TYPE_RESP.TUTEUR);

		String sujet = getSujet(stage, TITRE_VERROUILLAGE_CONVENTION);
		String corps = getCorpsVerrouillageConvention(stage, utilisateurPersId);
		sendMail(sujet, corps, from, to, null);
	}

	/**
	 * @param stage Un stage
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailSuppressionTotaleConvention(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = null;
		NSArray<String> cc = null;
		if (stage.isDemandeur(utilisateurPersId)) {
			to = getMails(stage, TYPE_RESP.RESPONSABLE);
		} else {
			to = getMails(stage, TYPE_RESP.ETUDIANT);
			cc = getMails(stage, TYPE_RESP.ETUDIANT, TYPE_RESP.RESPONSABLE);
		}

		String sujet = getSujet(stage, TITRE_SUPPRESSION_TOTALE_CONVENTION);
		String corps = getCorpsSuppressionTotaleConvention(stage, utilisateurPersId);
		sendMail(sujet, corps, from, to, cc);
	}

	/**
	 * @param stage Un stage
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @param raison La raison de la résiliation
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailResiliationConvention(EOStgStage stage, int utilisateurPersId, String raison) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.ETUDIANT, TYPE_RESP.RESPONSABLE);
		NSArray<String> cc = getMails(stage, TYPE_RESP.TUTEUR);

		String sujet = getSujet(stage, TITRE_RESILIATION_CONVENTION);
		String corps = getCorpsResiliationConvention(stage, utilisateurPersId, raison);
		sendMail(sujet, corps, from, to, cc);
	}

	/**
	 * @param stage Un stage
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailCreationAvenant(EOStgStage stage, Integer utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = null;
		NSArray<String> cc = null;

		String sujet;
		if (stage.isDemandeur(utilisateurPersId)) {
			sujet = getSujet(stage, TITRE_CREATION_AVENANT_ETUDIANT);
			to = getMails(stage, TYPE_RESP.SECRETAIRE, TYPE_RESP.TUTEUR);
			cc = getMails(stage, TYPE_RESP.RESPONSABLE);
		} else {
			sujet = getSujet(stage, TITRE_CREATION_AVENANT_ADMINISTRATIF);
			to = getMails(stage, TYPE_RESP.ETUDIANT);
			cc = getMails(stage, TYPE_RESP.RESPONSABLE);
		}
		String corps = getCorpsCreationAvenant(stage, utilisateurPersId);
		sendMail(sujet, corps, from, to, cc);
	}

	/**
	 * @param stage Un stage
	 * @param avenant Un avenant
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailValidationAvenant(EOStgStage stage, EOStgAvenant avenant, int utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.ETUDIANT);
		NSArray<String> cc = getMails(stage, TYPE_RESP.RESPONSABLE);

		String sujet = getSujet(stage, TITRE_VALIDATION_AVENANT);
		String corps = getCorpsValidationAvenant(stage, avenant, utilisateurPersId);
		sendMail(sujet, corps, from, to, cc);
	}

	/**
	 * @param stage Un stage
	 * @param avenant Un avenant
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailRefusAvenant(EOStgStage stage, EOStgAvenant avenant, int utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.ETUDIANT);
		NSArray<String> cc = getMails(stage, TYPE_RESP.RESPONSABLE);

		String sujet = getSujet(stage, TITRE_REFUS_AVENANT);
		String corps = getCorpsRefusAvenant(stage, avenant, utilisateurPersId);
		sendMail(sujet, corps, from, to, cc);
	}

	/**
	 * @param stage Un stage
	 * @param avenant Un avenant
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailSuppressionAvenant(EOStgStage stage, EOStgAvenant avenant, int utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.ETUDIANT);
		NSArray<String> cc = getMails(stage, TYPE_RESP.RESPONSABLE);

		String sujet = getSujet(stage, TITRE_SUPPRESSION_AVENANT);
		String corps = getCorpsSuppressionAvenant(stage, avenant, utilisateurPersId);
		sendMail(sujet, corps, from, to, cc);
	}

	/**
	 * @param stage Un stage
	 * @param avenant Un avenant
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailVerrouillageAvenant(EOStgStage stage, EOStgAvenant avenant, int utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.ETUDIANT, TYPE_RESP.TUTEUR);
		NSArray<String> cc = getMails(stage, TYPE_RESP.RESPONSABLE);

		String sujet = getSujet(stage, TITRE_VERROUILLAGE_AVENANT);
		String corps = getCorpsVerrouillageAvenant(stage, avenant, utilisateurPersId);
		sendMail(sujet, corps, from, to, cc);
	}

	/**
	 * @param stage Un stage
	 * @param structure Une structure
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void envoyerMailCreationStructure(EOStgStage stage, EOStructure structure, int utilisateurPersId) throws MessagingException {
		String from = getMail(stage, utilisateurPersId);
		NSArray<String> to = getMails(stage, TYPE_RESP.RESPONSABLE_STRUCTURES);

		String sujet;
		if (stage.isDemandeur(utilisateurPersId)) {
			sujet = getSujet(stage, TITRE_CREATION_STRUCTURE_ETUDIANT);
		} else {
			sujet = getSujet(stage, TITRE_CREATION_STRUCTURE_ADMINISTRATIF);
		}
		String corps = getCorpsCreationStructure(stage, structure, utilisateurPersId);
		sendMail(sujet, corps, from, to, null);
	}

	private String getMailSujet(String mecCode, String titre) {
		NSMutableDictionary<String, String> strContainer = new NSMutableDictionary<String, String>();
		if (mecCode == null) {
			mecCode = "";
		}
		strContainer.put(VAR_MEC_CODE, mecCode);
		strContainer.put(VAR_TITRE, titre);
		return ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject(MAIL_SUJET, strContainer);
	}

	private String getMailSignature() {
		if (staticSignature == null) {
			NSMutableDictionary<String, String> strContainer = new NSMutableDictionary<String, String>();
			strContainer.put(VAR_APPLI_NAME, APP_NAME);
			strContainer.put(VAR_APPLI_URL, APP_URL);
			staticSignature = ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject(MAIL_SIGNATURE, strContainer);
		}
		return staticSignature;
	}

	private String getCorpsIntro() {
		if (staticCorpsIntro == null) {
			staticCorpsIntro = ERXLocalizer.defaultLocalizer().localizedStringForKey(CORPS_INTRO);
		}
		return staticCorpsIntro;
	}

	private String getSujet(EOStgStage stage, String titre) {

		String titreLocalized = ERXLocalizer.defaultLocalizer().localizedStringForKey(titre);
		if (stage.mecKey() != null) {
			FactoryFinderScolarite factoryFinderScolarite = new FactoryFinderScolarite();
			FinderElementPedagogique finderElementPedagogique = factoryFinderScolarite.getFinderElementPedagogique(stage.editingContext());
			EC ec = finderElementPedagogique.getEC(stage.mecKey());
			if (ec != null) {
				return getMailSujet(ec.getCode(), titreLocalized);
			}
		}
		return getMailSujet(null, titreLocalized);
	}

	private String getNomPrenomViaPersId(EOEditingContext edc, Integer persId) {
		return EOIndividu.individuWithPersId(edc, persId).getNomPrenomAffichage();
	}

	private String generateCorps(EOStgStage stage, EOStgAvenant avenant, EOStructure structure, String raison, Integer utilisateurPersId, String templateCorps) {
		String str;
		NSMutableDictionary<String, String> strContainer = new NSMutableDictionary<String, String>();
		strContainer.put(VAR_UTILISATEUR, getNomPrenomViaPersId(stage.editingContext(), utilisateurPersId));
		strContainer.put(VAR_ETUDIANT, stage.toEtudiant().toIndividu().getNomPrenomAffichage());
		strContainer.put(VAR_CONVENTION_ID, stage.primaryKey());
		strContainer.put(VAR_CONVENTION_ULR, APP_URL + DA_CONSULTATION_CONVENTION + stage.primaryKey());
		if (avenant != null) {
			strContainer.put(VAR_AVENANT_ID, avenant.primaryKey());
		}
		if (structure != null) {
			strContainer.put(VAR_STRUCTURE_URL, APP_URL + DA_CONSULTATION_STRUCTURE + structure.cStructure());
		}
		if (!StringCtrl.isEmpty(raison)) {
			strContainer.put(VAR_RAISON, raison);
		}
		// Création du corps en remplissant les variables
		str = ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject(templateCorps, strContainer);

		return getCorpsIntro() + str + getMailSignature();
	}

	private String getCorpsDemandeValidationPedagogique(EOStgStage stage, Integer utilisateurPersId) {
		String template;
		if (stage.isDemandeur(utilisateurPersId)) {
			template = CORPS_DEMANDE_VALIDATION_PEDAGOGIQUE_ETUDIANT;
		} else {
			template = CORPS_DEMANDE_VALIDATION_PEDAGOGIQUE_ADMINISTRATIF;
		}
		return generateCorps(stage, null, null, null, utilisateurPersId, template);
	}

	private String getCorpsValidationPedagogiqueToTuteur(EOStgStage stage, Integer utilisateurPersId) {
		return generateCorps(stage, null, null, null, utilisateurPersId, CORPS_VALIDATION_PEDAGOGIQUE_TO_TUTEUR);
	}

	private String getCorpsValidationPedagogiqueToSecr(EOStgStage stage, Integer utilisateurPersId) {
		return generateCorps(stage, null, null, null, utilisateurPersId, CORPS_VALIDATION_PEDAGOGIQUE_TO_SECR);
	}

	private String getCorpsRefusPedagogique(EOStgStage stage, Integer utilisateurPersId, String raison) {
		return generateCorps(stage, null, null, raison, utilisateurPersId, CORPS_REFUS_PEDAGOGIQUE);
	}

	private String getCorpsValidationTuteur(EOStgStage stage, Integer utilisateurPersId) {
		return generateCorps(stage, null, null, null, utilisateurPersId, CORPS_VALIDATION_TUTEUR);
	}

	private String getCorpsRefusTuteur(EOStgStage stage, Integer utilisateurPersId) {
		return generateCorps(stage, null, null, null, utilisateurPersId, CORPS_REFUS_TUTEUR);
	}

	private String getCorpsValidationAdministrative(EOStgStage stage, Integer utilisateurPersId) {
		return generateCorps(stage, null, null, null, utilisateurPersId, CORPS_VALIDATION_ADMINISTRATIVE);
	}

	private String getCorpsEditionConvention(EOStgStage stage, Integer utilisateurPersId) {
		return generateCorps(stage, null, null, null, utilisateurPersId, CORPS_EDITION_CONVENTION);
	}

	private String getCorpsVerrouillageConvention(EOStgStage stage, Integer utilisateurPersId) {
		return generateCorps(stage, null, null, null, utilisateurPersId, CORPS_VERROUILLAGE_CONVENTION);
	}

	private String getCorpsSuppressionTotaleConvention(EOStgStage stage, Integer utilisateurPersId) {
		return generateCorps(stage, null, null, null, utilisateurPersId, CORPS_SUPPRESSION_TOTALE_CONVENTION);
	}

	private String getCorpsResiliationConvention(EOStgStage stage, Integer utilisateurPersId, String raison) {
		return generateCorps(stage, null, null, raison, utilisateurPersId, CORPS_RESILIATION_CONVENTION);
	}

	private String getCorpsCreationAvenant(EOStgStage stage, Integer utilisateurPersId) {
		String template;
		if (stage.isDemandeur(utilisateurPersId)) {
			template = CORPS_CREATION_AVENANT_ETUDIANT;
		} else {
			template = CORPS_CREATION_AVENANT_ADMINISTRATIF;
		}
		return generateCorps(stage, null, null, null, utilisateurPersId, template);
	}

	private String getCorpsValidationAvenant(EOStgStage stage, EOStgAvenant avenant, Integer utilisateurPersId) {
		return generateCorps(stage, avenant, null, null, utilisateurPersId, CORPS_VALIDATION_AVENANT);
	}

	private String getCorpsRefusAvenant(EOStgStage stage, EOStgAvenant avenant, Integer utilisateurPersId) {
		return generateCorps(stage, avenant, null, null, utilisateurPersId, CORPS_REFUS_AVENANT);
	}

	private String getCorpsVerrouillageAvenant(EOStgStage stage, EOStgAvenant avenant, Integer utilisateurPersId) {
		return generateCorps(stage, avenant, null, null, utilisateurPersId, CORPS_VERROUILLAGE_AVENANT);
	}

	private String getCorpsSuppressionAvenant(EOStgStage stage, EOStgAvenant avenant, Integer utilisateurPersId) {
		return generateCorps(stage, avenant, null, null, utilisateurPersId, CORPS_SUPPRESSION_AVENANT);
	}

	private String getCorpsCreationStructure(EOStgStage stage, EOStructure structure, Integer utilisateurPersId) {
		String template;
		if (stage.isDemandeur(utilisateurPersId)) {
			template = CORPS_CREATION_STRUCTURE_ETUDIANT;
		} else {
			template = CORPS_CREATION_STRUCTURE_ADMINISTRATIF;
		}
		return generateCorps(stage, null, structure, null, utilisateurPersId, template);
	}

	private void sendMail(String sujet, String corps, String from, NSArray<String> to, NSArray<String> cc) throws MessagingException {
		ERMailDeliveryPlainText mail = new ERMailDeliveryPlainText();

		// FIXME Expéditeur des mails
		if (from == null) {
			from = ERXProperties.stringForKey("cktlmailfactory.from");
		}

		if (cc == null) {
			cc = new NSArray<String>();
		}
		if (cc.indexOfObject(from) == NSArray.NotFound) {
			cc = cc.arrayByAddingObject(from);
		}

		if (ERJavaMail.sharedInstance().centralize()) {
			corps += "\n\n-- DEBUG --\n";
			corps += "TO : " + to + "\n";
			corps += "CC : " + cc;
		}

		mail.newMail();
		mail.setFromAddress(from);
		mail.setToAddresses(to);
		mail.setCCAddresses(cc);
		mail.setSubject(sujet);
		mail.setTextContent(corps);
		mail.sendMail();
	}

	private NSArray<String> getMails(EOStgStage stage, TYPE_RESP... types) {
		NSMutableArray<String> str = new NSMutableArray<String>();
		for (TYPE_RESP type : types) {
			addMailByTypeResp(stage, str, type);
		}
		return str;
	}

	private void addMailByTypeResp(EOStgStage stage, NSMutableArray<String> str, TYPE_RESP type) {
		switch (type) {
		case RESPONSABLE:
			addMailResponsable(stage, str);
			break;
		case SECRETAIRE:
			addMailSecretaire(stage, str);
			break;
		case TUTEUR:
			addMailTuteur(stage, str);
			break;
		case ETUDIANT:
			addMailEtudiant(stage, str);
			break;
		case RESPONSABLE_STRUCTURES:
			addMailResponsableStructures(str);
			break;
		default:
			break;
		}
	}

	private void addMailResponsableStructures(NSMutableArray<String> str) {
		str.addObjectsFromArray(FwkCktlStagesMailDestinataireFactory.getInstance().getMailsResponsablesStructures());
	}

	private void addMailEtudiant(EOStgStage stage, NSMutableArray<String> str) {
		if (stage.toEtudiant() != null && stage.toEtudiant().toIndividu() != null
		    && stage.toEtudiant().toIndividu().getEmails(EOCompte.QUAL_CPT_VLAN_E).count() > 0) {
			str.addObject(stage.toEtudiant().toIndividu().getEmails(EOCompte.QUAL_CPT_VLAN_E).objectAtIndex(0));
		}
	}

	private void addMailTuteur(EOStgStage stage, NSMutableArray<String> str) {
		if (stage.toTuteur() != null && stage.toTuteur().getEmails(null).count() > 0) {
			str.addObject(stage.toTuteur().getEmails(null).objectAtIndex(0));
		}
	}

	private void addMailSecretaire(EOStgStage stage, NSMutableArray<String> str) {
		str.addObjectsFromArray(FwkCktlStagesMailDestinataireFactory.getInstance().getMailsResponsablesAdministratifs(stage));
	}

	private void addMailResponsable(EOStgStage stage, NSMutableArray<String> str) {
		str.addObjectsFromArray(FwkCktlStagesMailDestinataireFactory.getInstance().getMailsResponsablesPedagogiques(stage));
	}

	private String getMail(EOStgStage stage, Integer persId) {
		EOIndividu indiv = EOIndividu.individuWithPersId(stage.editingContext(), persId);
		EOQualifier qual = null;
		if (stage.isDemandeur(persId)) {
			qual = EOCompte.QUAL_CPT_VLAN_E;
		}
		NSArray<String> arrMails = indiv.getEmails(qual);
		if (arrMails.count() > 0) {
			return arrMails.objectAtIndex(0);
		}
		return null;
	}
}