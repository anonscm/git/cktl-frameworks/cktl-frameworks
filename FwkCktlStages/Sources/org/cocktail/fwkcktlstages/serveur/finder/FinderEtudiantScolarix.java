package org.cocktail.fwkcktlstages.serveur.finder;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.finder.FinderEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * * Implementation de la recherche des étudiants qui s'appuie sur l'ancienne scolarite (FwkCktlScol) {@inheritDoc}
 */
public class FinderEtudiantScolarix extends FinderEtudiantCommons {
	private EOEditingContext editingContext;
	private Integer anneeEnCours;

	/**
	 * @param editingContext contexte d'edition
	 * @param anneeEnCours année pour laquel on fait la recherche
	 */
	public FinderEtudiantScolarix(EOEditingContext editingContext, Integer anneeEnCours) {
		setEditingContext(editingContext);
		setAnneeEnCours(anneeEnCours);
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	public Integer getAnneeEnCours() {
		return anneeEnCours;
	}

	public void setAnneeEnCours(Integer anneeEnCours) {
		this.anneeEnCours = anneeEnCours;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IEtudiant> rechercheNomPrenomINE(Integer numeroEtudiant, String nom, String prenom, String INE) {
		if (!StringCtrl.isEmpty(nom) || !StringCtrl.isEmpty(INE) || numeroEtudiant != null) {
			NSArray<org.cocktail.scolarix.serveur.metier.eos.EOEtudiant> resultatsEtudiants = FinderEtudiant.getEtudiants(getEditingContext(), getAnneeEnCours(),
			    numeroEtudiant, INE, nom, prenom, null, null, null);
			return getEtudiants(resultatsEtudiants);

		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IEtudiant> rechercheNumeroIndividu(Integer etudiantNumeroIndividu) {
		org.cocktail.scolarix.serveur.interfaces.IEtudiant iEtu = FinderEtudiant.getEtudiantByNumeroIndividu(getEditingContext(), etudiantNumeroIndividu,
		    getAnneeEnCours());
		if (iEtu != null) {
			List<IEtudiant> etudiants = new ArrayList<IEtudiant>();
			etudiants.add(getEtudiant(iEtu.numero()));
			return etudiants;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IEtudiant> recherchePersId(Integer persId) {
		org.cocktail.scolarix.serveur.interfaces.IEtudiant iEtu = FinderEtudiant.getEtudiantByPersId(getEditingContext(), persId, getAnneeEnCours());
		if (iEtu != null) {
			List<IEtudiant> etudiants = new ArrayList<IEtudiant>();
			etudiants.add(getEtudiant(iEtu.numero()));
			return etudiants;
		}
		return null;
	}

	private List<IEtudiant> getEtudiants(NSArray<org.cocktail.scolarix.serveur.metier.eos.EOEtudiant> resultatsEtudiants) {
		List<IEtudiant> etudiants = new ArrayList<IEtudiant>();
		for (org.cocktail.scolarix.serveur.metier.eos.EOEtudiant etudiant : resultatsEtudiants) {
			etudiants.add(getEtudiant(etudiant.numero()));
		}
		return etudiants;
	}

	private IEtudiant getEtudiant(Integer numeroEtudiant) {
		return EOEtudiant.fetchByKeyValue(getEditingContext(), EOEtudiant.ETUD_NUMERO_KEY, numeroEtudiant);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isInscritPourAnne(IEtudiant etudiant, Integer annee) {
		FinderUtilsScolarix finderUtilsScolarix = new FinderUtilsScolarix(getEditingContext());
		org.cocktail.scolarix.serveur.metier.eos.EOEtudiant etudiantScolarix = finderUtilsScolarix.getEtudiantScolarix(etudiant.etudNumero());
		EOHistorique histo = etudiantScolarix.historique(annee);
		return (histo != null);
	}

}
