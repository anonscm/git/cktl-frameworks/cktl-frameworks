/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.finder;

import org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.ERXKeyValueQualifier;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FinderStgEtat extends AFinder {

	private static FinderStgEtat instance;

	protected FinderStgEtat() {

	}

	/**
	 * @param newInstance
	 *            Une instance de {@link FinderStgEtat}
	 */
	protected static void setInstance(FinderStgEtat newInstance) {
		instance = newInstance;
	}

	/**
	 * @return L'instance de {@link FinderStgEtat}
	 */
	public static FinderStgEtat getInstance() {
		if (instance == null) {
			instance = new FinderStgEtat();
		}
		return instance;
	}

	/**
	 * Recherche un état avec son libellé court
	 * 
	 * @param ec
	 *            L'EditingContext
	 * @param libelleCourt
	 *            Le libellé court de l'état
	 * @return {@link EOStgEtat}
	 */
	public EOStgEtat getStgEtat(EOEditingContext ec, String libelleCourt) {
		EOStgEtat stgEtat = null;

		if (ec != null && libelleCourt != null && libelleCourt.length() > 0) {
			EOQualifier qual = ERXQ.equals(EOStgEtat.SET_LC_KEY, libelleCourt);
			stgEtat = EOStgEtat.fetchByQualifier(ec, qual);
		}
		return stgEtat;
	}

	/**
	 * Recherche un état avec son ordre
	 * 
	 * @param ec
	 *            L'EditingContext
	 * @param ordre
	 *            Le numéro d'ordre de l'état
	 * @return {@link EOStgEtat}
	 */
	public EOStgEtat getStgEtat(EOEditingContext ec, Long ordre) {
		EOStgEtat stgEtat = null;

		if (ec != null && ordre != null) {
			EOQualifier qual = ERXQ.equals(EOStgEtat.SET_ORDRE_KEY, ordre);
			stgEtat = EOStgEtat.fetchByQualifier(ec, qual);
		}
		return stgEtat;
	}

	/**
	 * Recherche l'état initial
	 * 
	 * @param ec
	 *            L'EditingContext
	 * @return {@link EOStgEtat}
	 */
	public EOStgEtat getStgEtatInitial(EOEditingContext ec) {
		return getStgEtat(ec, new Long(0));
	}

	/**
	 * @param stage
	 *            Le stage
	 * @param lcEtat
	 *            Le libellé court de l'état à comparer
	 * @return TRUE si l'état courant du stage a pour libellé court <b>lcEtat</b>
	 */
	public boolean isEtatCourant(EOStgStage stage, String lcEtat) {
		if (stage != null && lcEtat != null) {
			return lcEtat.equals(stage.toEtat().setLc());
		} else {
			return false;
		}
	}

	/**
	 * Compare deux état, via leur ordre
	 * 
	 * @param etat1
	 *            Le premier état
	 * @param etat2
	 *            Le second état
	 * @return <code>0</code> si les deux états ont le même ordre, un nombre négatif si <code>etat1</code> est inférieur
	 *         à <code>etat2</code>, un nombre positif si <code>etat1</code> est supérieur à <code>etat2</code>
	 */
	public int compareEtats(EOStgEtat etat1, EOStgEtat etat2) {
		return etat1.setOrdre().compareTo(etat2.setOrdre());
	}

	/**
	 * Compare deux états, via leur ordre
	 * 
	 * @param lcEtat1
	 *            Le libellé du premier état
	 * @param lcEtat2
	 *            Le libellé du second état
	 * @return <code>0</code> si les deux états ont le même ordre, un nombre négatif si <code>etat1</code> est inférieur
	 *         à <code>etat2</code>, un nombre positif si <code>etat1</code> est supérieur à <code>etat2</code>
	 */
	public int compareEtats(String lcEtat1, String lcEtat2) {
		EOEditingContext ec = ERXEC.newEditingContext();
		EOStgEtat etat1 = getStgEtat(ec, lcEtat1);
		EOStgEtat etat2 = getStgEtat(ec, lcEtat2);
		return compareEtats(etat1, etat2);
	}

	/**
	 * @param lcEtat
	 *            Le linellé court d'un état
	 * @return Le {@link EOQualifier} pour filtrer des stages suivant l'état donné
	 */
	public ERXKeyValueQualifier getQualifierStageToEtat(String lcEtat) {
		return ERXQ.equals(EOStgStage.TO_ETAT_KEY + "." + EOStgEtat.SET_LC_KEY, lcEtat);
	}
}
