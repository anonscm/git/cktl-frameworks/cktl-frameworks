package org.cocktail.fwkcktlstages.serveur.finder;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * Utilitaires pour récupérer des élements scolpeda
 */
public class FinderUtilsScolPeda {

	private EOEditingContext editingContext;

	/**
	 * @param editingContext contexte d'édition
	 */
	public FinderUtilsScolPeda(EOEditingContext editingContext) {
		setEditingContext(editingContext);
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * @param responsableComposant responsable du composant
	 * @param typeResponsabilite type de responsabilite
	 * @return <code> true </code> si le responsable a un type de responsabilité donnée
	 */
	public boolean hasTypeResponsabilite(EOResponsableComposant responsableComposant, String typeResponsabilite) {
		for (EOResponsableComposantRole responsableComposantRole : responsableComposant.roles()) {
			if (typeResponsabilite.equals(responsableComposantRole.role().libelleCourt())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param annee annee pour laquelle on cherche l'inscription administrative
	 * @param numeroEtudiant numéro de l'etudiant inscrit
	 * @return un inscription administrative
	 */
	public IScoInscription getInscriptionPrincipale(Integer annee, Integer numeroEtudiant) {

		EOQualifier qualifierAnnee = ERXQ.and(EOInscription.ANNEE.eq(annee), EOInscription.TO_ETUDIANT.dot(EOEtudiant.ETUD_NUMERO).eq(numeroEtudiant),
		    EOInscription.TO_TYPE_INSCRIPTION_FORMATION.eq(EOTypeInscriptionFormation.typePrincipal(getEditingContext())));
		IScoInscription scoInscription = EOInscription.fetchSco_Inscription(getEditingContext(), qualifierAnnee);

		if (scoInscription == null) {
			List<EOInscription> inscriptions = EOInscription.fetchSco_Inscriptions(getEditingContext(),
			    ERXQ.and(EOInscription.ANNEE.eq(annee), EOInscription.TO_ETUDIANT.dot(EOEtudiant.ETUD_NUMERO).eq(numeroEtudiant)), null);
			if (inscriptions.size() > 0) {
				scoInscription = inscriptions.get(0);
			}
		}

		return scoInscription;
	}
}
