/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.finder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe abstraite dont doivent heriter les finders.
 * 
 */

public abstract class AFinder {

	/**
	 * Fetch un tableau d'objets depuis la base de donnees.
	 * 
	 * @param ec
	 *            Un {@link EOEditingContext}
	 * @param entityName
	 *            Le nom de l'entitée à fetcher
	 * @param conditionStr
	 *            Un qualifier sous forme de String
	 * @param params
	 *            Les paramètres du qualifier
	 * @param sortOrderings
	 *            Tableau d'objets EOSortOrdering
	 * @param refreshObjects
	 *            Indique si on souhaite que les objets soient récupérés de la base de données ou seulement depuis
	 *            l'editingContext.
	 * @param usesDistinct
	 *            Effectuer un distinct sur le resultat (double le temps de requete à peu pret...)
	 * @param isDeep
	 *            Dans le cas où l'entite a des sous_entites
	 * @param hints
	 *            Requête ?
	 * @return Les résultats du fetch
	 */
	public static NSArray fetchArray(final EOEditingContext ec, final String entityName, final String conditionStr,
			final NSArray params, final NSArray sortOrderings, boolean refreshObjects, boolean usesDistinct,
			boolean isDeep, NSDictionary hints) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(conditionStr, params);
		return fetchArray(ec, entityName, qual, sortOrderings, refreshObjects, usesDistinct, isDeep, hints);
	}

	/**
	 * Fetch un tableau d'objets depuis la base de données, sans distinct.
	 * 
	 * @param ec
	 *            Un {@link EOEditingContext}
	 * @param entityName
	 *            Le nom de l'entitée à fetcher
	 * @param qual
	 *            Le qualifier à utiliser
	 * @param sortOrderings
	 *            Tableau d'objets EOSortOrdering
	 * @param refreshObjects
	 *            Indique si on souhaite que les objets soient récupérés de la base de données ou seulement depuis
	 *            l'editingContext.
	 * @return Les résultats du fetch
	 */
	public static NSArray fetchArray(final EOEditingContext ec, final String entityName, final EOQualifier qual,
			final NSArray sortOrderings, boolean refreshObjects) {
		return fetchArray(ec, entityName, qual, sortOrderings, refreshObjects, false, false, null);
	}

	/**
	 * @param ec
	 *            Un {@link EOEditingContext}
	 * @param entityName
	 *            Le nom de l'entitée à fetcher
	 * @param qual
	 *            Le qualifier à utiliser
	 * @param sortOrderings
	 *            Tableau d'objets EOSortOrdering
	 * @param refreshObjects
	 *            Indique si on souhaite que les objets soient récupérés de la base de données ou seulement depuis
	 *            l'editingContext.
	 * @param usesDistinct
	 *            Effectuer un distinct sur le resultat (double le temps de requete à peu pret...)
	 * @param isDeep
	 *            Dans le cas où l'entite a des sous_entites
	 * @param hints
	 *            Requête ?
	 * @return Les résultats du fetch
	 */
	public static NSArray fetchArray(final EOEditingContext ec, final String entityName, final EOQualifier qual,
			final NSArray sortOrderings, boolean refreshObjects, boolean usesDistinct, boolean isDeep,
			NSDictionary hints) {
		final EOFetchSpecification spec = new EOFetchSpecification(entityName, qual, null, usesDistinct, isDeep, hints);
		spec.setSortOrderings(sortOrderings);
		spec.setRefreshesRefetchedObjects(refreshObjects);
		return ec.objectsWithFetchSpecification(spec);
	}

	/**
	 * Supprime les doublons dans un tableau
	 * 
	 * @param array
	 *            Un array
	 */
	public static final void removeDuplicatesInNSArray(final NSMutableArray array) {
		int i = array.count() - 1;
		while (i >= 0) {
			final Object obj = array.objectAtIndex(i);
			int found = array.indexOfObject(obj);
			if (found != NSArray.NotFound && found != i) {
				array.removeObjectAtIndex(i);
			}
			i--;
		}
	}

	/**
	 * @param globalID
	 *            Le global ID de l'objet
	 * @param editingContext
	 *            l'{@link EOEditingContext}
	 * 
	 * @return retourne le generic record pour la globalID pass&eacute;e en param&egrave;tre dans l'editing context
	 */
	public static EOGenericRecord objetForGlobalIDDansEditingContext(EOGlobalID globalID,
			EOEditingContext editingContext) {
		return (EOGenericRecord) editingContext.faultForGlobalID(globalID, editingContext);
	}

	// UtilitairesGraphiques de recherche
	/**
	 * Recherche toutes les entit&eacute;s d&eacute;finies par le qualifier fourni en param&egrave;tre
	 * 
	 * @param editingContext
	 *            l'{@link EOEditingContext}
	 * @param nomEntite
	 *            &agrave; rechercher
	 * @param qualifier
	 *            Un {@link EOQualifier}
	 * @param sansDoublon
	 *            true si supprimer doublons
	 * @param forceRefresh
	 *            true si forcer le refresh des objets
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecQualifier(EOEditingContext editingContext, String nomEntite,
			EOQualifier qualifier, boolean sansDoublon, boolean forceRefresh) {
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite, qualifier, null);
		if (!nomEntite.equals("PhotosEmployes") && !nomEntite.equals("EmploiType")
				&& !nomEntite.equals("PhotosStructuresGrhum")) {
			fs.setUsesDistinct(sansDoublon);
		}
		fs.setRefreshesRefetchedObjects(forceRefresh);
		return editingContext.objectsWithFetchSpecification(fs);
	}

	/**
	 * Recherche toutes les entit&eacute;s d&eacute;finies par le qualifier fourni en param&egrave;tre
	 * 
	 * @param editingContext
	 *            l'{@link EOEditingContext}
	 * @param nomEntite
	 *            &agrave; rechercher
	 * @param qualifier
	 *            Un {@link EOQualifier}
	 * @param sansDoublon
	 *            true si supprimer doublons
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecQualifier(EOEditingContext editingContext, String nomEntite,
			EOQualifier qualifier, boolean sansDoublon) {
		return rechercherAvecQualifier(editingContext, nomEntite, qualifier, sansDoublon, false);
	}

	/**
	 * Retourne le contenu complet d'une table sans qualification des entit&eacute;s
	 * 
	 * @param editingContext
	 *            l'{@link EOEditingContext}
	 * @param nomEntite
	 *            &agrave; rechercher
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherEntite(EOEditingContext editingContext, String nomEntite) {
		return rechercherAvecQualifier(editingContext, nomEntite, null, true);
	}

	/**
	 * Retourne le contenu complet d'une table sans qualification des entit&eacute;s et avec un refresh
	 * 
	 * @param editingContext
	 *            l'{@link EOEditingContext}
	 * @param nomEntite
	 *            &agrave; rechercher
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherEntiteAvecRefresh(EOEditingContext editingContext, String nomEntite) {
		return rechercherAvecQualifier(editingContext, nomEntite, null, true, true);
	}

	/**
	 * Recherche toutes les entit&eacute;s dont un champ a une certaine valeur
	 * 
	 * @param editingContext
	 *            l'{@link EOEditingContext}
	 * @param nomEntite
	 *            &agrave; rechercher
	 * @param nomLabel
	 *            champ sur lequel effectuer la recherche
	 * @param valeur
	 *            valeur du champ &agrave; recherch&eacute;
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecAttribut(EOEditingContext editingContext, String nomEntite, String nomLabel,
			String valeur) {
		NSArray values = new NSArray(valeur);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(nomLabel + " caseInsensitiveLike %@", values);
		return rechercherAvecQualifier(editingContext, nomEntite, qualifier, true);
	}

	/**
	 * Recherche toutes les entit&eacute;s dont un champ a une certaine valeur en faisant une &eacute;galit&eacute;
	 * 
	 * @param editingContext
	 *            l'{@link EOEditingContext}
	 * @param nomEntite
	 *            &agrave; rechercher
	 * @param nomLabel
	 *            champ sur lequel effectuer la recherche
	 * @param valeur
	 *            valeur du champ &agrave; recherch&eacute;
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecAttributEtValeurEgale(EOEditingContext editingContext, String nomEntite,
			String nomLabel, Object valeur) {
		NSArray values = new NSArray(valeur);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(nomLabel + " = %@", values);
		return rechercherAvecQualifier(editingContext, nomEntite, qualifier, true);
	}

	/**
	 * Recherche l'entit&eacute; dont un champ a une certaine valeur en faisant une &eacute;galit&eacute;
	 * 
	 * @param editingContext
	 *            l'{@link EOEditingContext}
	 * @param nomEntite
	 *            &agrave; rechercher
	 * @param nomLabel
	 *            champ sur lequel effectuer la recherche
	 * @param valeur
	 *            valeur du champ &agrave; recherch&eacute;
	 * @return objet trouv&eacute;
	 */
	public static EOGenericRecord rechercherObjetAvecAttributEtValeurEgale(EOEditingContext editingContext,
			String nomEntite, String nomLabel, String valeur) {
		NSArray resultats = rechercherAvecAttributEtValeurEgale(editingContext, nomEntite, nomLabel, valeur);
		try {
			return (EOGenericRecord) resultats.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Evalue la cl&eacute; primaire pour une entit&eacute; en cherchant dans la table de s&eacute;quence pour une base
	 * Oracle
	 * 
	 * @param editingContext
	 *            l'{@link EOEditingContext}
	 * @param nomEntite
	 *            Le nom de l'entité
	 * @param nomClePrimaire
	 *            Le nom de la pk
	 * @param nomEntiteSequenceOracle
	 *            null si Oracle g&egrave;re la s&eacute;quence
	 * @param estUnLong
	 *            <code>true</code> si c'est un long
	 * @return valeur de la cl&eacute; primaire
	 */
	public static Integer clePrimairePour(EOEditingContext editingContext, String nomEntite, String nomClePrimaire,
			String nomEntiteSequenceOracle, boolean estUnLong) {
		return numeroSequenceOracle(editingContext, nomEntiteSequenceOracle);
	}

	/**
	 * Recherche le num&eacute;ro de s&eacute;quence dans une table de s&eacute;quence Oracle (doit comporter un
	 * attribut nextval)
	 * 
	 * @param editingContext
	 *            l'{@link EOEditingContext}
	 * @param nomEntiteSequenceOracle
	 *            Le nom de la séquence
	 * @return valeur trouv&eacute;e ou null
	 */
	public static Integer numeroSequenceOracle(EOEditingContext editingContext, String nomEntiteSequenceOracle) {
		if (nomEntiteSequenceOracle == null || nomEntiteSequenceOracle.equals("")) {
			return null;
		}
		EOFetchSpecification myFetch = new EOFetchSpecification(nomEntiteSequenceOracle, null, null);
		NSArray result = editingContext.objectsWithFetchSpecification(myFetch);
		try {
			Number numero = (Number) ((NSArray) result.valueForKey("nextval")).objectAtIndex(0);
			return new Integer(numero.intValue());
		} catch (Exception e) {
			return null;
		}
	}
}
