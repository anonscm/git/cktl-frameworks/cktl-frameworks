package org.cocktail.fwkcktlstages.serveur.finder;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;

/**
 * @author lilia Recherche de responsable
 */
public interface FinderResponsable {

	/**
	 * @param etudiant etudiant
	 * @param meckey cle primaire de l'ec
	 * @param annee annee
	 * @param typeResponsabilite type de responsabilité
	 * @return liste de responsables pour un étudiant, un ec, une année et un type de responsabilité donné
	 */
	List<IPersonne> getResponsablesByEtudiantAndEc(IEtudiant etudiant, Integer meckey, Integer annee, String typeResponsabilite);

	/**
	 * @return le type de responsabilite "secrétaire"
	 */
	String getTypeSecretaire();

	/**
	 * @return le type de responsabilite "responsable"
	 */
	String getTypeResponsable();
	
	/**
	 * @return le type de responsabilite "responsable"
	 */
	String getTypeEnseignant();
}
