package org.cocktail.fwkcktlstages.serveur.finder;

import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Création des finders suivant le type de scolarite
 * 
 */
public class FactoryFinderScolarite {
	private ScolariteSystem scolariteSystem;
	private static final String CONFIG_MODELE_SCOLARITE = "MODELE_SCOLARITE";
	// private static final String CONFIG_MODELE_SCOLARITE_SCOLARIX = "scolarix";
	private static final String CONFIG_MODELE_SCOLARITE_SCOLPEDA = "scolpeda";

	/**
	 * système de scolarite
	 */
	private enum ScolariteSystem {
		SCOLARIX, SCOLPEDA
	}

	/**
	 * constructeur, initialisation du systeme de scolarite choisie
	 */
	public FactoryFinderScolarite() {
		this(CktlParamManager.getApplication().config().stringForKey(CONFIG_MODELE_SCOLARITE));
	}

	/**
	 * @param systeme systeme de scolarite
	 */
	public FactoryFinderScolarite(String systeme) {
		if (CONFIG_MODELE_SCOLARITE_SCOLPEDA.equals(systeme)) {
			scolariteSystem = ScolariteSystem.SCOLPEDA;
		} else {
			scolariteSystem = ScolariteSystem.SCOLARIX;
		}
	}

	/**
	 * @param editingContext contexte d'edition
	 * @param anneeEnCours annee
	 * @return implémentation de la classe de rechercher des etudiant
	 */
	public FinderEtudiant getFinderEtudiant(EOEditingContext editingContext, Integer anneeEnCours) {
		if (scolariteSystem == ScolariteSystem.SCOLARIX) {
			return new FinderEtudiantScolarix(editingContext, anneeEnCours);
		}

		if (scolariteSystem == ScolariteSystem.SCOLPEDA) {
			return new FinderEtudiantScolPeda(editingContext, anneeEnCours);
		}

		return null;
	}

	/**
	 * @param editingContext contexte d'edition
	 * @return implémentation de la classe de recherche des éléments pédagogiques
	 */
	public FinderElementPedagogique getFinderElementPedagogique(EOEditingContext editingContext) {
		if (scolariteSystem == ScolariteSystem.SCOLARIX) {
			return new FinderElementPedagogiqueScolarix(editingContext);
		}

		if (scolariteSystem == ScolariteSystem.SCOLPEDA) {
			return new FinderElementPedagogiqueScolPeda(editingContext);
		}

		return null;
	}

	/**
	 * @param editingContext contexte d'edition
	 * @return implémentation de la classe de recherche des responsables
	 */
	public FinderResponsable getFinderResponsable(EOEditingContext editingContext) {
		if (scolariteSystem == ScolariteSystem.SCOLARIX) {
			return new FinderResponsableScolarix(editingContext);
		}

		if (scolariteSystem == ScolariteSystem.SCOLPEDA) {
			return new FinderResponsableScolpeda(editingContext);
		}

		return null;
	}
}
