package org.cocktail.fwkcktlstages.serveur.finder;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.Diplome;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.InscriptionAdministrative;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.InscriptionEtudiant;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * {@inheritDoc}
 */
public class FinderElementPedagogiqueScolPeda implements FinderElementPedagogique {
	private EOEditingContext editingContext;
	private FinderUtilsScolPeda finderUtilsScolPeda;
	private static final String GRHUM_ANNEE_UNIVERSITAIRE = "GRHUM_ANNEE_UNIVERSITAIRE";

	/**
	 * @param editingContext contexte d'édition
	 */
	public FinderElementPedagogiqueScolPeda(EOEditingContext editingContext) {
		setEditingContext(editingContext);
		finderUtilsScolPeda = new FinderUtilsScolPeda(editingContext);
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * {@inheritDoc}
	 */
	public InscriptionAdministrative getInscriptionPrincipale(IEtudiant unEtudiant, Integer annee) {
		IInscription ip = finderUtilsScolPeda.getInscriptionPrincipale(annee, unEtudiant.etudNumero());
		return getInscriptionAdministrative(ip);
	}

	/**
	 * {@inheritDoc}
	 */
	public EC getEC(Integer id) {
		IEC iec = EOEC.fetchSco_EC(getEditingContext(), EOEC.ID.eq(id));
		return getEC(iec);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<EC> getListeECStage(Integer numeroEtudiant, Integer annee) {
		IScoInscription inscription = finderUtilsScolPeda.getInscriptionPrincipale(annee, numeroEtudiant);

		List<EC> ecs = new ArrayList<EC>();
		if (inscription != null) {
			for (IInscriptionPedagogique inscPeda : inscription.toInscriptionsPedagogiques()) {
				for (IInscriptionPedagogiqueElement elt : inscPeda.toInscriptionPedagogiqueElements()) {
					IComposant composant = elt.toLien().child();
					if (composant instanceof IEC && hasAPTypeStage(composant)) {
						ecs.add(getEC((IEC) composant));
					}
				}
			}
		}
		return ecs;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<EC> getListeECStageByResponsablite(Integer persId, Integer annee, String typeResponsabilite) {
		List<EOResponsableComposant> responsablesComposant = EOResponsableComposant.fetchSco_ResponsableComposants(getEditingContext(),
		    EOResponsableComposant.RESPONSABLE_PERS_ID.dot(EOIndividu.PERS_ID).eq(persId), null);

		List<EC> ecs = new ArrayList<EC>();
		for (EOResponsableComposant responsableComposant : responsablesComposant) {
			List<EOEC> iecs = EOEC.fetchSco_ECs(getEditingContext(), EOComposant.RESPONSABLES_COMPOSANT.containsObject(responsableComposant), null);
			for (IEC iec : iecs) {
				if (hasAPTypeStage(iec) && finderUtilsScolPeda.hasTypeResponsabilite(responsableComposant, typeResponsabilite)) {
					ecs.add(getEC((IEC) iec));
				}
			}
		}

		return ecs;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<InscriptionEtudiant> getInscriptionsEtudiantForEc(EC ec) {

		List<EOInscriptionPedagogiqueElement> inscriptionPedagogiqueElements = EOInscriptionPedagogiqueElement.fetchSco_InscriptionPedagogiqueElements(
		    getEditingContext(), EOInscriptionPedagogiqueElement.TO_LIEN.dot(EOLien.CHILD_ID_KEY).eq(ec.getId()), null);

		List<InscriptionEtudiant> inscriptionsEtudiant = new ArrayList<InscriptionEtudiant>();
		for (EOInscriptionPedagogiqueElement inscriptionPedagogiqueElement : inscriptionPedagogiqueElements) {

			try {
				inscriptionsEtudiant.add(getInscriptionEtudiant(inscriptionPedagogiqueElement.toInscriptionPedagogique().toInscription().toEtudiant()));
			} catch (Exception e) {
				System.err.println("problème de récupération de l'étudiant associé à l'EC: " + ec.getId());
			}

		}
		return inscriptionsEtudiant;
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer getAnneeUniversitaireCourante() {
		return CktlParamManager.getApplication().config().integerForKey(GRHUM_ANNEE_UNIVERSITAIRE);
	}

	private InscriptionEtudiant getInscriptionEtudiant(IEtudiant etudiant) {
		InscriptionEtudiant inscriptionEtudiant = new InscriptionEtudiant();
		inscriptionEtudiant.setPersId(etudiant.toIndividu().persId());
		inscriptionEtudiant.setEtudNumero(etudiant.etudNumero());
		inscriptionEtudiant.setAdrNom(etudiant.toIndividu().nomAffichage());
		inscriptionEtudiant.setAdrPrenom(etudiant.toIndividu().nomAffichage());
		return inscriptionEtudiant;
	}

	private InscriptionAdministrative getInscriptionAdministrative(IInscription inscriptionScolpeda) {
		if (inscriptionScolpeda == null) {
			return null;
		}
		InscriptionAdministrative inscription = new InscriptionAdministrative();
		inscription.setLibelle(inscriptionScolpeda.toString());
		inscription.setNiveau(inscriptionScolpeda.niveau());
		inscription.setDiplome(getDiplome(inscriptionScolpeda));
		if (inscriptionScolpeda.toDiplome() != null) {
			inscription.setLibelleDiplomeComplet(getLibelleComplet(inscriptionScolpeda));
		}

		return inscription;
	}

	private String getLibelleComplet(IInscription inscriptionScolpeda) {
		String libelleComplet = "";
		if (inscriptionScolpeda.toDiplome().gradeUniversitaire() != null) {
			libelleComplet = inscriptionScolpeda.toDiplome().gradeUniversitaire().libelle();
		}
		if (inscriptionScolpeda.annee() != null) {
			libelleComplet = libelleComplet + " " + inscriptionScolpeda.niveau();
		}

		libelleComplet = libelleComplet + " " + inscriptionScolpeda.toDiplome().libelle();
		if (inscriptionScolpeda.toParcoursDiplome() != null) {
			libelleComplet = libelleComplet + inscriptionScolpeda.toParcoursDiplome().libelle();
		}
		return libelleComplet;
	}

	private Diplome getDiplome(IInscription inscription) {
		Diplome diplome = new Diplome();
		if (inscription.toDiplome() != null) {
			diplome.setLibelle(inscription.toDiplome().libelle());
			if (inscription.toParcoursDiplome() != null) {
				diplome.setLibelleParcours(inscription.toParcoursDiplome().libelle());
			}
			if (inscription.toDiplome().gradeUniversitaire() != null) {
				diplome.setGradeAbreviation(inscription.toDiplome().gradeUniversitaire().type());
			}
		}
		return diplome;
	}

	private EC getEC(IEC iec) {
		if (iec == null) {
			return null;
		}
		EC ec = new EC();
		ec.setId(iec.id());
		ec.setCode(iec.code());
		ec.setLibelle(iec.libelle());
		if (iec.structures().size() > 0) {
			ec.setComposanteId(((EOStructure) iec.structures().get(0)).persId());
		}
		if (iec.creditable() != null) {
			ec.setCreditECTS(iec.creditable().creditECTS());
		}
		return ec;
	}

	private boolean hasAPTypeStage(IComposant composant) {
		for (IComposant child : composant.childs(EOTypeComposant.typeAP(getEditingContext()))) {
			if (EOTypeAP.typeStage(getEditingContext()).equals(((IAP) child).typeAP())) {
				return true;
			}
		}
		return false;
	}

}
