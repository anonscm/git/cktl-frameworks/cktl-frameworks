package org.cocktail.fwkcktlstages.serveur.finder;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.Diplome;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.InscriptionAdministrative;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.InscriptionEtudiant;
import org.cocktail.scolaritefwk.serveur.finder.FinderScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.finder.FinderScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.finder.FinderScolResponsabilites;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteBudget;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * {@inheritDoc}
 */
public class FinderElementPedagogiqueScolarix implements FinderElementPedagogique {
	private EOEditingContext editingContext;
	private FinderUtilsScolarix finderUtilsScolarix;

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * @param editingContext contexte d'édition
	 */
	public FinderElementPedagogiqueScolarix(EOEditingContext editingContext) {
		setEditingContext(editingContext);
		finderUtilsScolarix = new FinderUtilsScolarix(editingContext);
	}

	/**
	 * {@inheritDoc}
	 */
	public EC getEC(Integer id) {
		EOScolMaquetteEc maquetteEC = finderUtilsScolarix.getScolMaquetteEC(id);
		if (maquetteEC != null) {
			return getEC(maquetteEC);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public InscriptionAdministrative getInscriptionPrincipale(IEtudiant unEtudiant, Integer annee) {
		EOEtudiant etudiantScolarix = finderUtilsScolarix.getEtudiantScolarix(unEtudiant.etudNumero());
		EOHistorique histo = etudiantScolarix.historique(annee);
		EOInscDipl inscriptionScolarix = histo.inscriptionPrincipale();
		return getInscription(inscriptionScolarix);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<EC> getListeECStage(Integer numeroEtudiant, Integer annee) {
		NSArray<EOScolMaquetteEc> mecs = FinderScolMaquetteEc.getEcForEtudiant(getEditingContext(), numeroEtudiant,
		    FinderScolFormationAnnee.getScolFormationAnneeByKey(getEditingContext(), annee), FinderScolMaquetteEc.QUAL_EC_STAGE);

		List<EC> ecs = new ArrayList<EC>();
		for (EOScolMaquetteEc mec : mecs) {
			ecs.add(getEC(mec));
		}
		return ecs;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<InscriptionEtudiant> getInscriptionsEtudiantForEc(EC ec) {
		EOScolMaquetteEc mec = finderUtilsScolarix.getScolMaquetteEC(ec.getId());

		NSArray<EOScolInscriptionEtudiant> scolInscriptionsEtudiant = FinderScolMaquetteEc.getEtudiantsForEc(getEditingContext(), mec);
		List<InscriptionEtudiant> inscriptionsEtudiant = new ArrayList<InscriptionEtudiant>();

		for (EOScolInscriptionEtudiant scolInscriptionEtudiant : scolInscriptionsEtudiant) {
			inscriptionsEtudiant.add(getInscriptionEtudiant(scolInscriptionEtudiant));
		}
		return inscriptionsEtudiant;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<EC> getListeECStageByResponsablite(Integer persId, Integer annee, String typeResponsabilite) {
		NSArray<EOScolMaquetteEc> scolMaquetteECs = FinderScolResponsabilites.getInstance().getEcByResponsable(getEditingContext(), persId, annee,
		    typeResponsabilite);
		List<EC> ecs = new ArrayList<EC>();
		for (EOScolMaquetteEc maquetteEC : scolMaquetteECs) {
			ecs.add(getEC(maquetteEC));
		}
		return ecs;
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer getAnneeUniversitaireCourante() {
		return FinderScolFormationAnnee.getScolFormationAnneeCourante(getEditingContext()).fannKey();
	}

	// Private helpers
	private InscriptionEtudiant getInscriptionEtudiant(EOScolInscriptionEtudiant scolInscriptionEtudiant) {
		InscriptionEtudiant inscriptionEtudiant = new InscriptionEtudiant();
		inscriptionEtudiant.setPersId(scolInscriptionEtudiant.persId());
		inscriptionEtudiant.setEtudNumero(scolInscriptionEtudiant.etudNumero());
		inscriptionEtudiant.setAdrNom(scolInscriptionEtudiant.adrNom());
		inscriptionEtudiant.setAdrPrenom(scolInscriptionEtudiant.adrPrenom());
		return inscriptionEtudiant;
	}

	private InscriptionAdministrative getInscription(EOInscDipl inscriptionScolarix) {
		InscriptionAdministrative inscription = new InscriptionAdministrative();
		inscription.setLibelle(inscriptionScolarix.toStringLight());
		inscription.setNiveau(inscriptionScolarix.idiplAnneeSuivie());
		inscription.setDiplome(getDiplome(inscriptionScolarix));
		inscription.setLibelleDiplomeComplet(inscriptionScolarix.libelleDiplomeComplet());
		return inscription;
	}

	private EC getEC(EOScolMaquetteEc maquetteEC) {
		EC ec = new EC();
		ec.setId(Integer.parseInt(maquetteEC.primaryKey()));
		ec.setCode(maquetteEC.mecCode());
		ec.setLibelle(maquetteEC.mecLibelle());
		EOScolConstanteBudget scolConstanteBudget = maquetteEC.toFwkScolarite_ScolConstanteBudget();
		if (scolConstanteBudget != null && scolConstanteBudget.toFwkpers_Structure() != null) {
			ec.setComposanteId(scolConstanteBudget.toFwkpers_Structure().persId());
		}
		ec.setCreditECTS(maquetteEC.mecPoints());
		return ec;
	}

	private Diplome getDiplome(EOInscDipl inscriptionScolarix) {
		EOScolFormationSpecialisation spe = inscriptionScolarix.toFwkScolarite_ScolFormationSpecialisation();
		if (spe != null) {
			EOScolFormationDiplome dip = spe.toFwkScolarite_ScolFormationDiplome();
			Diplome diplome = new Diplome();
			diplome.setLibelle(dip.fdipLibelle());
			diplome.setLibelleParcours(spe.fspnLibelle());
			diplome.setGradeAbreviation(dip.filiere().abreviation());
			return diplome;
		}
		return null;
	}
}
