/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.finder;

import org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FinderStgPieceJointe extends AFinder {

	/**
	 * @param stage
	 *            Un {@link EOStgStage}
	 * @param pieceJointeTypeLc
	 *            Le libellé court du type de la pièce jointe
	 * @return Un {@link EOStgPieceJointeRepart}
	 */
	public static EOStgPieceJointeRepart getUnePieceJointe(EOStgStage stage, String pieceJointeTypeLc) {
		return getUnePieceJointe(stage,
				FinderStgPieceJointeType.getPieceJointeTypeByLc(stage.editingContext(), pieceJointeTypeLc));
	}

	/**
	 * @param stage
	 *            Un {@link EOStgStage}
	 * @param pieceJointeType
	 *            Un {@link EOStgPieceJointeType}
	 * @return Un {@link EOStgPieceJointeRepart}
	 */
	public static EOStgPieceJointeRepart getUnePieceJointe(EOStgStage stage, EOStgPieceJointeType pieceJointeType) {
		EOQualifier qualstage = ERXQ.equals(EOStgPieceJointeRepart.STG_STAGE_KEY, stage);
		EOQualifier qualType = ERXQ.equals(EOStgPieceJointeRepart.STG_PIECE_JOINTE_TYPE_KEY, pieceJointeType);
		return EOStgPieceJointeRepart.fetchByQualifier(stage.editingContext(), ERXQ.and(qualstage, qualType));
	}
}
