package org.cocktail.fwkcktlstages.serveur.finder;

import org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FinderStgAvenant extends AFinder {

	/**
	 * @param stage
	 *            Un {@link EOStgStage}
	 * @return La liste des avenants verrouillés
	 */
	public static NSArray<EOStgAvenant> getAvenantsVerrouilles(EOStgStage stage) {
		EOQualifier qual = ERXQ.isNotNull(EOStgAvenant.SAV_D_VERROUILLAGE_KEY);
		ERXSortOrderings sort = ERXS.desc(EOStgAvenant.SAV_D_VERROUILLAGE_KEY).array();
		return stage.toAvenants(qual, sort, false);
	}

	/**
	 * @param stage
	 *            Un {@link EOStgStage}
	 * @return Un avenant factice, regroupant toutes les modifications des avenants verrouillés
	 */
	public static EOStgAvenant getConcatAvenant(EOStgStage stage) {
		NSArray<EOStgAvenant> arr = getAvenantsVerrouilles(stage);
		// Avenant factice
		EOStgAvenant concatAvenant = new EOStgAvenant();
		// Parcours des avenants pour remplissage de l'avenant factice
		for (EOStgAvenant avenant : arr) {
			updateSujet(concatAvenant, avenant);
			updateInterruption(concatAvenant, avenant);
			updateStageDebut(concatAvenant, avenant);
			updateStageFin(concatAvenant, avenant);
			updateStageDuree(concatAvenant, avenant);
			updateStageGratification(concatAvenant, avenant);
			updateStageService(concatAvenant, avenant);
			updateLieu(concatAvenant, avenant);
		}
		return concatAvenant;
	}

	private static void updateStageService(EOStgAvenant concatAvenant, EOStgAvenant avenant) {
		if (concatAvenant.toService() == null && avenant.toService() != null) {
			concatAvenant.takeStoredValueForKey(avenant.toService(), EOStgAvenant.TO_SERVICE_KEY);
		}
		if (concatAvenant.toServiceAdresse() == null && avenant.toServiceAdresse() != null) {
			concatAvenant.takeStoredValueForKey(avenant.toServiceAdresse(), EOStgAvenant.TO_SERVICE_ADRESSE_KEY);
		}
	}

	private static void updateStageGratification(EOStgAvenant concatAvenant, EOStgAvenant avenant) {
		if (concatAvenant.savGratMontant() == null && avenant.savGratMontant() != null) {
			concatAvenant.setSavGratMontant(avenant.savGratMontant());
			concatAvenant.takeStoredValueForKey(avenant.toGratDevise(), EOStgAvenant.TO_GRAT_DEVISE_KEY);
		}
	}

	private static void updateStageDuree(EOStgAvenant concatAvenant, EOStgAvenant avenant) {
		if (concatAvenant.savDureeTotaleVal() == null && avenant.savDureeTotaleVal() != null) {
			concatAvenant.setSavDureeTotaleVal(avenant.savDureeTotaleVal());
			concatAvenant.setSavDureeTotaleUnite(avenant.savDureeTotaleUnite());
		}
	}

	private static void updateLieu(EOStgAvenant concatAvenant, EOStgAvenant avenant) {
		if (concatAvenant.toLieuAdresse() == null && avenant.toLieuAdresse() != null) {
			concatAvenant.takeStoredValueForKey(avenant.toLieuAdresse(), EOStgAvenant.TO_LIEU_ADRESSE_KEY);
		}
	}

	private static void updateStageFin(EOStgAvenant concatAvenant, EOStgAvenant avenant) {
		if (concatAvenant.savDStageFin() == null && avenant.savDStageFin() != null) {
			concatAvenant.setSavDStageFin(avenant.savDStageFin());
		}
	}

	private static void updateStageDebut(EOStgAvenant concatAvenant, EOStgAvenant avenant) {
		if (concatAvenant.savDStageDebut() == null && avenant.savDStageDebut() != null) {
			concatAvenant.setSavDStageDebut(avenant.savDStageDebut());
		}
	}

	private static void updateInterruption(EOStgAvenant concatAvenant, EOStgAvenant avenant) {
		if (concatAvenant.savDInterDebut() == null && avenant.savDInterDebut() != null) {
			concatAvenant.setSavDInterDebut(avenant.savDInterDebut());
			concatAvenant.setSavDInterFin(avenant.savDInterFin());
		}
	}

	private static void updateSujet(EOStgAvenant concatAvenant, EOStgAvenant avenant) {
		if (concatAvenant.savSujet() == null && !StringCtrl.isEmpty(avenant.savSujet())) {
			concatAvenant.setSavSujet(avenant.savSujet());
		}
	}
}
