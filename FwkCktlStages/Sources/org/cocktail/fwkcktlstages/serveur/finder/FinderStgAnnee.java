/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.finder;

import org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FinderStgAnnee extends AFinder {

	/**
	 * Recherche une annee precise
	 * 
	 * @param ec
	 *            L'EditingContext
	 * @param sanDebut
	 *            L'année de début
	 * 
	 * @return {@link EOStgAnnee}
	 */
	public static EOStgAnnee getStgAnnee(EOEditingContext ec, Integer sanDebut) {
		EOStgAnnee stgAnnee = null;
		if (ec != null && sanDebut != null) {
			EOQualifier qualifier = new EOKeyValueQualifier(EOStgAnnee.SAN_DEBUT_KEY,
					EOQualifier.QualifierOperatorEqual, sanDebut);
			stgAnnee = EOStgAnnee.fetchByQualifier(ec, qualifier);
		}
		return stgAnnee;
	}

	/**
	 * Recherche l'année courante
	 * 
	 * @param ec
	 *            L'EditingContext
	 * @return {@link EOStgAnnee}
	 */
	public static EOStgAnnee getStgAnneeCourante(EOEditingContext ec) {
		EOStgAnnee stgAnnee = null;
		if (ec != null) {
			EOQualifier qualifier = new EOKeyValueQualifier(EOStgAnnee.SAN_COURANTE_KEY,
					EOQualifier.QualifierOperatorEqual, EOStgAnnee.ANNEE_COURANTE);
			stgAnnee = EOStgAnnee.fetchByQualifier(ec, qualifier);
		}
		return stgAnnee;
	}

	/**
	 * @param edc
	 *            Un {@link EOEditingContext}
	 * @return La liste des années
	 */
	public static NSArray<EOStgAnnee> getAllStgAnnees(EOEditingContext edc) {
		ERXSortOrderings sorts = ERXS.asc(EOStgAnnee.SAN_ID_KEY).array();
		return EOStgAnnee.fetchAll(edc, sorts);
	}
}
