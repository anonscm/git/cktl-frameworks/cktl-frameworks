package org.cocktail.fwkcktlstages.serveur.finder;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Utilitaires pour récupérer des élements scolarix (ec, etudiants)
 */
public class FinderUtilsScolarix {
	private EOEditingContext editingContext;

	/**
	 * @param editingContext contexte d'édition
	 */
	public FinderUtilsScolarix(EOEditingContext editingContext) {
		setEditingContext(editingContext);
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * @param id clé primaire de l'ec
	 * @return ec (dans scolarix)
	 */
	protected EOScolMaquetteEc getScolMaquetteEC(Integer id) {
		return EOScolMaquetteEc.fetchByKeyValue(getEditingContext(), EOScolMaquetteEc.ENTITY_PRIMARY_KEY, id);
	}
	
	/**
	 * @param numeroEtudiant numero de l'etudiant
	 * @return
	 */
	protected EOEtudiant getEtudiantScolarix(Integer numeroEtudiant) {
		return EOEtudiant.fetchByKeyValue(getEditingContext(), EOEtudiant.ETUD_NUMERO_KEY, numeroEtudiant);
	}
}
