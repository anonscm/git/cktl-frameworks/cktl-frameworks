package org.cocktail.fwkcktlstages.serveur.finder;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.scolaritefwk.serveur.finder.FinderScolResponsabilites;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * {@inheritDoc}
 */
public class FinderResponsableScolarix implements FinderResponsable {
	private EOEditingContext editingContext;
	private FinderUtilsScolarix finderUtilsScolarix;

	/**
	 * @param editingContext contexte d'edition
	 */
	public FinderResponsableScolarix(EOEditingContext editingContext) {
		setEditingContext(editingContext);
		finderUtilsScolarix = new FinderUtilsScolarix(editingContext);
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IPersonne> getResponsablesByEtudiantAndEc(IEtudiant etudiant, Integer meckey, Integer annee, String typeResponsabilite) {
		return FinderScolResponsabilites.getInstance().getResponsablesByEtudiantAndEc(finderUtilsScolarix.getEtudiantScolarix(etudiant.etudNumero()),
		    finderUtilsScolarix.getScolMaquetteEC(meckey), annee, typeResponsabilite);		 
	}

	public String getTypeSecretaire() {
	  return FinderScolResponsabilites.TYPE_SECRETAIRE;
  }

	public String getTypeResponsable() {
	  return FinderScolResponsabilites.TYPE_RESPONSABLE;
  }

	public String getTypeEnseignant() {
	  return FinderScolResponsabilites.TYPE_ENSEIGNANT;
  }
}
