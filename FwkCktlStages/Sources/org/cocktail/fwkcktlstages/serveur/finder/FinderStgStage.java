/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.finder;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgType;
import org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCached;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Regroupe les méthodes de recherche des stages
 * 
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FinderStgStage extends AFinder {

	public static final EOQualifier IS_NOT_STAGE_DELETED = ERXQ.isNull(EOStgStage.STG_D_SUPPRESSION_KEY);
	public static final EOQualifier IS_NOT_CONVENTION_RESILIEE = FinderStgEtat.getInstance().getQualifierStageToEtat(EOStgEtat.LC_CONVENTION_RESILIEE).not();
	public static final EOQualifier IS_CONVENTION_VALIDE = IS_NOT_STAGE_DELETED; // ERXQ.and(IS_NOT_STAGE_DELETED,
	// IS_NOT_CONVENTION_RESILIEE);

	private static FinderStgStage instance;

	protected FinderStgStage() {

	}

	/**
	 * @param newInstance Une instance de {@link FinderStgEtat}
	 */
	public static void setInstance(FinderStgStage newInstance) {
		instance = newInstance;
	}

	/**
	 * @return L'instance de {@link FinderStgEtat}
	 */
	public static FinderStgStage getInstance() {
		if (instance == null) {
			instance = new FinderStgStage();
		}
		return instance;
	}

	/**
	 * @param ec L'EOEditingContext
	 * @param id L'ID du stage
	 * @return Le stage s'il a été trouvé, <code>null</code> sinon
	 */
	public EOStgStage getStageById(EOEditingContext ec, Integer id) {
		return EOStgStage.fetchByKeyValue(ec, EOStgStage.STG_ID_KEY, id);
	}

	/**
	 * @param edc L'EOEditingContext
	 * @param fannKey L'année
	 * @return La liste des stages pour l'année donnée
	 */
	public NSArray<EOStgStage> getLesStages(EOEditingContext edc, Integer fannKey) {
		EOQualifier qualifier = ERXQ.isNotNull(EOStgStage.MEC_KEY_KEY);
		if (fannKey != null) {
			qualifier = ERXQ.and(qualifier, ERXQ.equals(EOStgStage.TO_ANNEE_KEY + "." + EOStgAnnee.SAN_DEBUT_KEY, fannKey));
		}
		return EOStgStage.fetchAll(edc, qualifier);
	}

	/**
	 * Retourne les stages de l'étudiant pour l'année donnée
	 * 
	 * @param edc l'{@link EOEditingContext}
	 * @param etudNumero Le numéro étudiant
	 * @param fannKey L'année
	 * @return La liste des stages
	 */
	public NSArray<EOStgStage> getLesStages(EOEditingContext edc, Integer etudNumero, Integer fannKey) {
		return getLesStages(edc, etudNumero, fannKey, true);
	}

	/**
	 * Retourne les stages de l'étudiant pour l'année donnée
	 * 
	 * @param edc l'{@link EOEditingContext}
	 * @param etudNumero Le numéro étudiant
	 * @param fannKey L'année
	 * @param filtreValide <code>TRUE</code> si on ne doit retourner que les conventions valides (non résiliées, non supprimées)
	 * @return La liste des stages
	 */
	public NSArray<EOStgStage> getLesStages(EOEditingContext edc, Integer etudNumero, Integer fannKey, boolean filtreValide) {
		if (etudNumero == null) {
			return null;
		}
		EOQualifier qualifier = ERXQ.equals(EOStgStage.TO_ETUDIANT_KEY + "." + EOEtudiant.ETUD_NUMERO_KEY, etudNumero);
		if (filtreValide) {
			qualifier = ERXQ.and(qualifier, IS_CONVENTION_VALIDE);
		}

		if (fannKey != null) {
			qualifier = ERXQ.and(qualifier, ERXQ.equals(EOStgStage.TO_ANNEE_KEY + "." + EOStgAnnee.SAN_DEBUT_KEY, fannKey));
		}
		final NSArray<EOStgStage> result = EOStgStage.fetchAll(edc, qualifier);
		if (!result.isEmpty()) {
			return result;
		}
		return null;
	}

	/**
	 * Retourne le nombre de stages pour un type donné
	 * 
	 * @param type Le type de stage
	 * @return Le nombre de stages d'un type donné
	 */
	public int getNbStageFromType(EOStgType type) {
		int nb = 0;
		if (type != null) {
			nb = EOStgStage.fetchAll(type.editingContext(), ERXQ.equals(EOStgStage.TO_TYPE_KEY, type).and(IS_CONVENTION_VALIDE)).count();
		}
		return nb;
	}

	/**
	 * Les stages d'un tuteur pour l'année donnée
	 * 
	 * @param edc L'EOEditingContext
	 * @param persIdTuteur Le <code>PERS_ID</code> du tuteur
	 * @param fannKey L'année sélectionnée (peut être nulle)
	 * @return La liste des stages
	 */
	public NSArray<EOStgStage> getLesStagesByTuteur(EOEditingContext edc, Integer persIdTuteur, Integer fannKey) {
		if ((edc == null) || (persIdTuteur == null)) {
			return null;
		}
		EOQualifier qualifier = ERXQ.equals(EOStgStage.PERS_ID_TUTEUR_KEY, persIdTuteur).and(IS_NOT_STAGE_DELETED);
		if (fannKey != null) {
			qualifier = ERXQ.and(qualifier, ERXQ.equals(EOStgStage.TO_ANNEE_KEY + "." + EOStgAnnee.SAN_DEBUT_KEY, fannKey));
		}
		final NSArray<EOStgStage> result = EOStgStage.fetchAll(edc, qualifier);
		if (!result.isEmpty()) {
			return result;
		}
		return null;
	}

	/**
	 * @param edc L'EOEditingContext
	 * @param mec L'EC
	 * @return La liste des stages liés à un EC
	 */
	public NSArray<EOStgStage> getLesStagesByMec(EOEditingContext edc, EC mec) {
		return getLesStagesByMec(edc, mec, null);
	}

	/**
	 * @param edc L'EOEditingContext
	 * @param mec l'EC
	 * @param sort L'ordre de classement
	 * @return La liste des stages liés à un EC
	 */
	public NSArray<EOStgStage> getLesStagesByMec(EOEditingContext edc, EC mec, NSArray<EOSortOrdering> sort) {
		return getLesStagesByMec(edc, mec, sort, null);
	}

	/**
	 * @param edc L'EOEditingContext
	 * @param mec l'EC
	 * @param sort L'ordre de classement
	 * @param fannKey L'année
	 * @return La liste des stages liés à un EC
	 */
	public NSArray<EOStgStage> getLesStagesByMec(EOEditingContext edc, EC mec, NSArray<EOSortOrdering> sort, Integer fannKey) {
		if (mec != null) {
			EOQualifier qualifier = ERXQ.equals(EOStgStage.MEC_KEY_KEY, mec.getId()).and(IS_CONVENTION_VALIDE);
			if (fannKey != null) {
				qualifier = ERXQ.and(qualifier, EOStgStage.TO_ANNEE.dot(EOStgAnnee.SAN_DEBUT).eq(fannKey));
			}

			final NSArray<EOStgStage> result = EOStgStage.fetchAll(edc, qualifier);
			if (!result.isEmpty()) {
				return result;
			}
		}
		return null;
	}

	/**
	 * @param edc L'EOEditingContext
	 * @param listeMec La liste des EC
	 * @return La liste des stages liés à une liste d'EC
	 */
	public NSArray<EOStgStage> getLesStagesByListeMec(EOEditingContext edc, List<EC> listeMec) {
		final NSMutableArray<EOStgStage> listeStages = new NSMutableArray<EOStgStage>();
		for (final EC mec : listeMec) {
			listeStages.addObjectsFromArray(getLesStagesByMec(edc, mec));
		}
		removeDuplicatesInNSArray(listeStages);
		ERXArrayUtilities.sortArrayWithKey(listeStages, EOStgStage.STG_ID_KEY);
		return listeStages;
	}

	/**
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @param edc L'EOEditingContext
	 * @return La liste des stages liés à un EC pour lesquels un fonctionnel a des droits (responsable/secrétaire/enseignant)
	 */
	public NSArray<EOStgStage> getLesStagesPourUnFonctionnel(StagesDroitsCached sdhc, EOEditingContext edc) {
		NSMutableArray<EOStgStage> liste = new NSMutableArray<EOStgStage>();
		liste.addAll(sdhc.getLesStagesByResponsable(edc));
		liste.addAll(sdhc.getLesStagesBySecretaire(edc));
		liste.addAll(sdhc.getLesStagesByEnseignant(edc));
		return ERXArrayUtilities.arrayWithoutDuplicates(liste);
	}

	/**
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @param edc L'EOEditingContext
	 * @param fannKey L'année
	 * @return La liste des stages liés à un EC pour lesquels un fonctionnel a des droits (responsable/secrétaire/enseignant)
	 */
	public NSArray<EOStgStage> getLesStagesNonObligatoiresPourUnFonctionnel(StagesDroitsCached sdhc, EOEditingContext edc, Integer fannKey) {
		NSArray<EOStgStage> listeTousLesStagesNonObl = getLesStagesByMec(edc, null, null, fannKey);
		NSMutableArray<EOStgStage> listeStagesNonObl = new NSMutableArray<EOStgStage>();
		if (listeTousLesStagesNonObl != null) {
			for (EOStgStage unStage : listeTousLesStagesNonObl) {
				if (sdhc.isResponsableAdministratif(unStage) || sdhc.isResponsablePedagogique(unStage)) {
					listeStagesNonObl.addObject(unStage);
				}
			}
		}
		return listeStagesNonObl;
	}

	/**
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @param edc L'EOEditingContext
	 * @return La liste des stages lié à un EC pour lesquels l'utilisateur peut faire une action de validation
	 */
	public NSArray<EOStgStage> getLesStagesAValider(StagesDroitsCached sdhc, EOEditingContext edc) {
		NSMutableArray<EOStgStage> liste = new NSMutableArray<EOStgStage>();

		// Liste des stages à valider pédagogiquement
		NSArray<EOStgStage> stgResp = new NSArray<EOStgStage>(sdhc.getLesStagesByResponsable(edc));
		EOQualifier qualResp = FinderStgEtat.getInstance().getQualifierStageToEtat(EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE);
		liste.addObjectsFromArray(ERXArrayUtilities.filteredArrayWithQualifierEvaluation(stgResp, qualResp));

		// Liste des stages à valider par le tuteur
		NSArray<EOStgStage> stgTuteur = getLesStagesByTuteur(edc, sdhc.getPersId(), sdhc.getFannKey());
		EOQualifier qualTuteur = FinderStgEtat.getInstance().getQualifierStageToEtat(EOStgEtat.LC_ATTENTE_VALIDATION_TUTEUR);
		liste.addObjectsFromArray(ERXArrayUtilities.filteredArrayWithQualifierEvaluation(stgTuteur, qualTuteur));

		// Liste des conventions à valider/éditer/signer
		NSArray<EOStgStage> stgSecr = new NSArray<EOStgStage>(sdhc.getLesStagesBySecretaire(edc));
		EOQualifier qualSecr = FinderStgEtat.getInstance().getQualifierStageToEtat(EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE);
		// EOQualifier qualSecr_2 = FinderStgEtat.getQualifierStageToEtat(EOStgEtat.LC_ATTENTE_EDITION);
		// EOQualifier qualSecr_3 = FinderStgEtat.getQualifierStageToEtat(EOStgEtat.LC_ATTENTE_SIGNATURES);
		// EOQualifier qualSecr = qualSecr_1; // ERXQ.or(qualSecr_1, qualSecr_2, qualSecr_3);
		liste.addObjectsFromArray(ERXArrayUtilities.filteredArrayWithQualifierEvaluation(stgSecr, qualSecr));

		return liste;
	}

	/**
	 * @param struct Une structure représentant l'organisme d'accueil
	 * @return La liste des stages effectués dans cetorganisme d'accueil
	 */
	public NSArray<EOStgStage> getLesStagesByOrganismeAccueil(EOStructure struct) {
		final EOQualifier qualifier = ERXQ.equals(EOStgStage.TO_ORGANISME_ACCUEIL_KEY, struct);
		final NSArray<EOStgStage> result = EOStgStage.fetchAll(struct.editingContext(), qualifier);
		return result;
	}
}
