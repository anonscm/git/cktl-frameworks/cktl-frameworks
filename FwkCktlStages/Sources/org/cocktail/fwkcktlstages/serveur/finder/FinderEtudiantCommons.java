package org.cocktail.fwkcktlstages.serveur.finder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;

/**
 * methodes communes à toutes les implémentations de IFinderEtudiant
 */
public abstract class FinderEtudiantCommons implements FinderEtudiant {

	/**
	 * @param etudiants liste d'étudiants
	 * @param numeroIndividu numero d'individu
	 * @return liste des etudiants qui correspondent au numero d'individu
	 */
	public List<IEtudiant> filtrerSurNumeroIndividu(List<IEtudiant> etudiants, final Integer numeroIndividu) {
		Predicate predicate = new Predicate() {
			public boolean evaluate(Object object) {
				IEtudiant etudiant = ((IEtudiant) object);
				if (etudiant.toIndividu() != null) {
					return etudiant.toIndividu().noIndividu().equals(numeroIndividu);
				}
				return false;
			}
		};

		Collection filtered = CollectionUtils.select(etudiants, predicate);
		List<IEtudiant> etudiantsFiltres = new ArrayList<IEtudiant>(filtered);

		return etudiantsFiltres;

	}
	
	/**
	 * @param etudiants liste d'étudiants
	 * @param persId le persId correspondnat à l'idividu d'individu
	 * @return liste des etudiants qui correspondent au numero d'individu
	 */
	public List<IEtudiant> filtrerSurPersId(List<IEtudiant> etudiants, final Integer persId) {
		Predicate predicate = new Predicate() {
			public boolean evaluate(Object object) {
				IEtudiant etudiant = ((IEtudiant) object);
				if (etudiant.toIndividu() != null) {
					return etudiant.toIndividu().persId().equals(persId);
				}
				return false;
			}
		};

		Collection filtered = CollectionUtils.select(etudiants, predicate);
		List<IEtudiant> etudiantsFiltres = new ArrayList<IEtudiant>(filtered);

		return etudiantsFiltres;

	}
}
