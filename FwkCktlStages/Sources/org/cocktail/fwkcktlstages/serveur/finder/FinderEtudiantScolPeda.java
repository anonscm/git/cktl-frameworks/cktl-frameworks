package org.cocktail.fwkcktlstages.serveur.finder;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Implementation de la recherche des étudiants qui s'appuie sur la nouvelle scolarite (FwkCktlScolPeda) {@inheritDoc}
 */
public class FinderEtudiantScolPeda extends FinderEtudiantCommons {

	private EOEditingContext editingContext;
	private Integer anneeEnCours;

	/**
	 * @param editingContext contexte d'edition
	 * @param anneeEnCours année pour laquelle on recherche des étudiants
	 */
	public FinderEtudiantScolPeda(EOEditingContext editingContext, Integer anneeEnCours) {
		setEditingContext(editingContext);
		setAnneeEnCours(anneeEnCours);
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	public Integer getAnneeEnCours() {
		return anneeEnCours;
	}

	public void setAnneeEnCours(Integer anneeEnCours) {
		this.anneeEnCours = anneeEnCours;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IEtudiant> rechercheNomPrenomINE(Integer numeroEtudiant, String nom, String prenom, String INE) {

		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		if (getAnneeEnCours() != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiantAnnee.ANNEE_KEY, EOQualifier.QualifierOperatorEqual, getAnneeEnCours()));
		}
		if (numeroEtudiant != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiantAnnee.TO_ETUDIANT_KEY + "." + EOEtudiant.ETUD_NUMERO_KEY, EOQualifier.QualifierOperatorEqual,
			    numeroEtudiant));
		}
		if (INE != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiantAnnee.TO_ETUDIANT_KEY + "." + EOEtudiant.ETUD_CODE_INE_KEY,
			    EOQualifier.QualifierOperatorCaseInsensitiveLike, INE));
		}
		if (nom != null) {

			quals.addObject(new EOKeyValueQualifier(EOEtudiantAnnee.TO_ETUDIANT_KEY + "." + EOEtudiant.TO_INDIVIDU_KEY + "." + EOIndividu.NOM_PATRONYMIQUE_KEY,
			    EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + nom + "*"));
		}
		if (prenom != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiantAnnee.TO_ETUDIANT_KEY + "." + EOEtudiant.TO_INDIVIDU_KEY + "." + EOIndividu.PRENOM_KEY,
			    EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + prenom + "*"));
		}

		NSArray<EOEtudiantAnnee> etudiantsAnnees = EOEtudiantAnnee.fetchSco_EtudiantAnnees(getEditingContext(), new EOAndQualifier(quals), null);
		return getEtudiants(etudiantsAnnees);
	}

	private List<IEtudiant> getEtudiants(NSArray<EOEtudiantAnnee> etudiantsAnnees) {
		if (etudiantsAnnees != null) {
			List<IEtudiant> etudiants = new ArrayList<IEtudiant>();
			for (EOEtudiantAnnee etudiantAnnee : etudiantsAnnees) {
				etudiants.add(etudiantAnnee.toEtudiant());
			}
			return etudiants;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IEtudiant> rechercheNumeroIndividu(Integer etudiantNumeroIndividu) {
		if (etudiantNumeroIndividu == null) {
			return null;
		}

		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		if (getAnneeEnCours() != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiantAnnee.ANNEE_KEY, EOQualifier.QualifierOperatorEqual, getAnneeEnCours()));
		}
		quals.addObject(new EOKeyValueQualifier(EOEtudiantAnnee.TO_ETUDIANT_KEY + "." + EOEtudiant.TO_INDIVIDU_KEY + "." + EOIndividu.NO_INDIVIDU_KEY,
		    EOQualifier.QualifierOperatorEqual, etudiantNumeroIndividu));

		NSArray<EOEtudiantAnnee> etudiantsAnnees = EOEtudiantAnnee.fetchSco_EtudiantAnnees(getEditingContext(), new EOAndQualifier(quals), null);
		return getEtudiants(etudiantsAnnees);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IEtudiant> recherchePersId(Integer persId) {
		if (persId == null) {
			return null;
		}

		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		if (getAnneeEnCours() != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiantAnnee.ANNEE_KEY, EOQualifier.QualifierOperatorEqual, getAnneeEnCours()));
		}
		quals.addObject(new EOKeyValueQualifier(EOEtudiantAnnee.TO_ETUDIANT_KEY + "." + EOEtudiant.TO_INDIVIDU_KEY + "." + EOIndividu.PERS_ID_KEY,
		    EOQualifier.QualifierOperatorEqual, persId));

		NSArray<EOEtudiantAnnee> etudiantsAnnees = EOEtudiantAnnee.fetchSco_EtudiantAnnees(getEditingContext(), new EOAndQualifier(quals), null);
		return getEtudiants(etudiantsAnnees);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isInscritPourAnne(IEtudiant etudiant, Integer annee) {
		EOQualifier qualifier = EOEtudiantAnnee.ANNEE.eq(annee).and(EOEtudiantAnnee.TO_ETUDIANT.dot(EOEtudiant.ETUD_NUMERO).eq(etudiant.etudNumero()));
		return EOEtudiantAnnee.fetchSco_EtudiantAnnee(getEditingContext(), qualifier) != null;
	}

}
