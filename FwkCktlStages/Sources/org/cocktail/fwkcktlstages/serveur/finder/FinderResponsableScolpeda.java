package org.cocktail.fwkcktlstages.serveur.finder;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EORoleResponsable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IResponsableComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * {@inheritDoc}
 */
public class FinderResponsableScolpeda implements FinderResponsable {
	private EOEditingContext editingContext;
	private FinderUtilsScolPeda finderUtilsScolPeda;

	/**
	 * @param editingContext : contexte d'edition
	 */
	public FinderResponsableScolpeda(EOEditingContext editingContext) {
		setEditingContext(editingContext);
		finderUtilsScolPeda = new FinderUtilsScolPeda(editingContext);
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IPersonne> getResponsablesByEtudiantAndEc(IEtudiant etudiant, Integer meckey, Integer annee, String typeResponsabilite) {
		List<IPersonne> responsables = new ArrayList<IPersonne>();
		List<IScoInscription> inscriptions = getInscriptions(annee, etudiant.etudNumero());
		for (IScoInscription inscription : inscriptions) {
			for (IResponsableComposant responsableComposant : getResponsables(inscription.toDiplome(), typeResponsabilite)) {
				if (!responsables.contains(responsableComposant.responsable())) {
					responsables.add((IPersonne) responsableComposant.responsable());
				}
			}
			for (IResponsableComposant responsableComposant : getResponsables(inscription.toParcoursDiplome(), typeResponsabilite)) {
				if (!responsables.contains(responsableComposant.responsable())) {
					responsables.add((IPersonne) responsableComposant.responsable());
				}
			}
		}

		IEC iec = EOEC.fetchSco_EC(getEditingContext(), EOEC.ID.eq(meckey));
		if (iec != null) {
			for (IResponsableComposant responsableComposant : getResponsables(iec, typeResponsabilite)) {
				if (!responsables.contains(responsableComposant.responsable())) {
					responsables.add((IPersonne) responsableComposant.responsable());
				}
			}
		}
		return responsables;
	}

	private List<IResponsableComposant> getResponsables(IComposant composant, String typeResponsabilite) {
		List<IResponsableComposant> responsablesComposant = new ArrayList<IResponsableComposant>();
		if (composant != null) {
			for (IResponsableComposant responsableComposant : composant.responsablesComposant()) {
				if (finderUtilsScolPeda.hasTypeResponsabilite((EOResponsableComposant) responsableComposant, typeResponsabilite)) {
					responsablesComposant.add(responsableComposant);
				}
			}
		}
		return responsablesComposant;
	}

	private List<IScoInscription> getInscriptions(Integer annee, Integer numeroEtudiant) {
		EOQualifier qualifier = ERXQ.and(EOInscription.ANNEE.eq(annee), EOInscription.TO_ETUDIANT.dot(EOEtudiant.ETUD_NUMERO).eq(numeroEtudiant));
		return new ArrayList<IScoInscription>(EOInscription.fetchSco_Inscriptions(getEditingContext(), qualifier, null));
	}

	/**
	 * {@inheritDoc}
	 */
	public String getTypeSecretaire() {
		return EORoleResponsable.ROLE_SECRETAIRE_CODE;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getTypeResponsable() {
		return EORoleResponsable.ROLE_RESPONSABLE_CODE;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getTypeEnseignant() {
		return EORoleResponsable.ROLE_ENSEIGNANT_CODE;
	}

	/**
	 * @param annee annee pour laquelle on cherche l'inscription administrative
	 * @param numeroEtudiant numéro de l'etudiant inscrit
	 * @return un inscription administrative
	 */
	public IScoInscription getInscription(Integer annee, Integer numeroEtudiant) {
		EOQualifier qualifierAnnee = ERXQ.and(EOInscription.ANNEE.eq(annee), EOInscription.TO_ETUDIANT.dot(EOEtudiant.ETUD_NUMERO).eq(numeroEtudiant),
		    EOInscription.TO_TYPE_INSCRIPTION_FORMATION.eq(EOTypeInscriptionFormation.typePrincipal(getEditingContext())));
		return EOInscription.fetchSco_Inscription(getEditingContext(), qualifierAnnee);
	}
}
