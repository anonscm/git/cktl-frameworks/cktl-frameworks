package org.cocktail.fwkcktlstages.serveur.finder;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.InscriptionAdministrative;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.InscriptionEtudiant;

/**
 * Recherche d'un élement pédagogique (inscription, élément de la maquette de formation)
 */
public interface FinderElementPedagogique {

	/**
	 * @param unEtudiant un étudiants
	 * @param annee une année universitaire
	 * @return inscription principale de l'étudiant pour l'année donnée
	 */
	InscriptionAdministrative getInscriptionPrincipale(IEtudiant unEtudiant, Integer annee);

	/**
	 * @param id identifiant de l'EC
	 * @return un EC
	 */
	EC getEC(Integer id);

	/**
	 * @param numeroEtudiant etudiant pour lequel on recherche les ECs de type stage
	 * @param annee annee pour laquelle on recherche les EC
	 * @return liste des ECs de type stage
	 */
	List<EC> getListeECStage(Integer numeroEtudiant, Integer annee);

	/**
	 * @param persId identite de la personne pour laquelle on cherche les EC
	 * @param annee annee universitaire
	 * @param typeResponsabilite type de responsabilité sur les ECs
	 * @return liste d'EC contenant un stage dont la personne est responsable pour un type de responsabilité donné
	 */
	List<EC> getListeECStageByResponsablite(Integer persId, Integer annee, String typeResponsabilite);

	/**
	 * @param ec ec pour lequel on cherche les données de l'étudiant relatives aux inscriptions attachées à l'EC
	 * @return liste des données de l'étudiant relatives aux inscriptions attachées à l'EC
	 */
	List<InscriptionEtudiant> getInscriptionsEtudiantForEc(EC ec);
	
	/**
	 * @return l'année universitaire courante
	 */
	Integer getAnneeUniversitaireCourante();
}
