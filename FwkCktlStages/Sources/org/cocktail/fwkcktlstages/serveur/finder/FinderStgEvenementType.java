/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.finder;

import org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Méthodes pour retrouver les types d'évènement
 * 
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FinderStgEvenementType extends AFinder {

	/**
	 * @param edc
	 *            L'EditingContext
	 * @param lc
	 *            Libellé court de l'évènement
	 * @return L'évènement trouvé via le libellé
	 */
	public static final EOStgEvenementType getEvenementType(EOEditingContext edc, String lc) {
		return EOStgEvenementType.fetchRequiredByKeyValue(edc, EOStgEvenementType.SEVT_LC_KEY, lc);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement
	 */
	public static final EOStgEvenementType getEventCreation(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_CREATION_DEMANDE);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>MODIFICATION_DEMANDE</code>
	 */
	public static final EOStgEvenementType getEventModificationDemande(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_MODIFICATION_DEMANDE);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>MODIFICATION_ADMINISTRATIVE</code>
	 */
	public static final EOStgEvenementType getEventModificationAdministrativeDemande(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_MODIFICATION_ADMINISTRATIVE);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>DEMANDE_VALIDATION_PEDA</code>
	 */
	public static final EOStgEvenementType getEventDemandeValidationPeda(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_DEMANDE_VALIDATION_PEDA);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>ACCEPTATION_VALIDATION_PEDAGOGIQUE</code>
	 */
	public static final EOStgEvenementType getEventAcceptationValidationPedagogique(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_ACCEPTATION_VALIDATION_PEDAGOGIQUE);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>REFUS_VALIDATION_PEDAGOGIQUE</code>
	 */
	public static final EOStgEvenementType getEventRefusValidationPedagogique(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_REFUS_VALIDATION_PEDAGOGIQUE);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>ACCEPTATION_VALIDATION_TUTEUR</code>
	 */
	public static final EOStgEvenementType getEventAcceptationValidationTuteur(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_ACCEPTATION_VALIDATION_TUTEUR);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>REFUS_VALIDATION_TUTEUR</code>
	 */
	public static final EOStgEvenementType getEventRefusValidationTuteur(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_REFUS_VALIDATION_TUTEUR);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>ACCEPTATION_VALIDATION_ADMINISTRATIVE</code>
	 */
	public static final EOStgEvenementType getEventAcceptationValidationAdministrative(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_ACCEPTATION_VALIDATION_ADMINISTRATIVE);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>ANNULATION_VALIDATION_ADMINISTRATIVE</code>
	 */
	public static final EOStgEvenementType getEventAnnulationValidationAdministrative(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_ANNULATION_VALIDATION_ADMINISTRATIVE);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>ANNULATION_VALIDATION_PEDAGOGIQUE</code>
	 */
	public static EOStgEvenementType getEventAnnulationValidationPedagogique(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_ANNULATION_VALIDATION_PEDAGOGIQUE);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>EDITION_CONVENTION</code>
	 */
	public static final EOStgEvenementType getEventEditionConvention(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_EDITION_CONVENTION);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>SUPPRESSION_FICHIER_CONVENTION</code>
	 */
	public static final EOStgEvenementType getEventSuppressionConvention(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_SUPPRESSION_FICHIER_CONVENTION);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>RECEPTION_SIGNATURES</code>
	 */
	public static final EOStgEvenementType getEventSignaturesConvention(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_RECEPTION_SIGNATURES);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>DESIGNATION_TUTEUR</code>
	 */
	public static final EOStgEvenementType getEventModificationTuteur(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_DESIGNATION_TUTEUR);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>SUPPRESSION_TOTALE_CONVENTION</code>
	 */
	public static final EOStgEvenementType getEventSuppressionTotaleConvention(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_SUPPRESSION_TOTALE_CONVENTION);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>ANNULATION_SUPPRESSION_TOTALE_CONVENTION</code>
	 */
	public static EOStgEvenementType getEventAnnulationSuppressionTotaleConvention(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_ANNULATION_SUPPRESSION_TOTALE_CONVENTION);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>RESILIATION_CONVENTION</code>
	 */
	public static final EOStgEvenementType getEventResiliationConvention(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_RESILIATION_CONVENTION);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>ANNULATION_RESILIATION_CONVENTION</code>
	 */
	public static EOStgEvenementType getEventAnnulationResiliationConvention(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_ANNULATION_RESILIATION_CONVENTION);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>CREATION_AVENANT</code>
	 */
	public static final EOStgEvenementType getEventCreationAvenant(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_CREATION_AVENANT);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>MODIFICATION_AVENANT</code>
	 */
	public static EOStgEvenementType getEventModificationAvenant(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_MODIFICATION_AVENANT);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>VALIDATION_AVENANT</code>
	 */
	public static final EOStgEvenementType getEventValidationAvenant(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_VALIDATION_AVENANT);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>REFUS_AVENANT</code>
	 */
	public static EOStgEvenementType getEventRefusAvenant(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_REFUS_AVENANT);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>SUPPRESSION_AVENANT</code>
	 */
	public static final EOStgEvenementType getEventSuppressionAvenant(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_SUPPRESSION_AVENANT);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @return L'évènement <code>VERROUILLAGE_AVENANT</code>
	 */
	public static final EOStgEvenementType getEventVerrouillageAvenant(EOEditingContext edc) {
		return getEvenementType(edc, EOStgEvenementType.LC_VERROUILLAGE_AVENANT);
	}
}
