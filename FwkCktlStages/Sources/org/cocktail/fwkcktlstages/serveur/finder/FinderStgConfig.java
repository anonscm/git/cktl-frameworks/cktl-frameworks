/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.finder;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlstages.serveur.metier.EOStgConfig;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FinderStgConfig extends AFinder {

	private static FinderStgConfig instance;

	protected FinderStgConfig() {

	}

	/**
	 * @return L'instance statique de {@link FinderStgConfig}
	 */
	public static FinderStgConfig getInstance() {
		if (instance == null) {
			instance = new FinderStgConfig();
		}
		return instance;
	}

	/**
	 * @param newInstance Une instance de {@link FinderStgConfig}
	 */
	protected static void setInstance(FinderStgConfig newInstance) {
		instance = newInstance;
	}

	/**
	 * @param edc L'editingContext
	 * @param key La clé de la configuration
	 * @return Un objet {@link EOStgConfig} correspondant à la configuration demandée, ou <code>null</code> si aucune configuration n'a été trouvée
	 */
	public EOStgConfig getUniqueConfigByLc(EOEditingContext edc, String key) {
		return EOStgConfig.fetchByQualifier(edc, ERXQ.equals(EOStgConfig.SCFG_LC_KEY, key));
	}

	/**
	 * @param key La clé de la configuration
	 * @param required Indique si une configuration non nulle doit être renvoyée
	 * @return La valeur de la configuration au niveau de l'application
	 */
	public String getAppConfigValueByLc(String key, boolean required) {
		return getConfigValueByLc(key, required);
	}

	/**
	 * @param key La clé de la configuration
	 * @param required Indique si une configuration non nulle doit être renvoyée
	 * @return La valeur de la configuration
	 */
	public String getConfigValueByLc(String key, boolean required) {
		EOQualifier qual = ERXQ.equals(EOStgConfig.SCFG_LC_KEY, key);
		return getValueByQualifier(qual, required);
	}

	private String getValueByQualifier(EOQualifier qual, boolean required) {
		if (required) {
			return EOStgConfig.fetchFirstRequiredByQualifier(new ERXEC(), qual).scfgValeur();
		} else {
			EOStgConfig cfg = EOStgConfig.fetchFirstByQualifier(new ERXEC(), qual);
			if (cfg != null) {
				return cfg.scfgValeur();
			} else {
				return null;
			}
		}
	}

	/**
	 * @return Le <code>PERS_ID</code> de l'utilisateur par défaut pour la création des conventions
	 */
	public String getDefaultUserPersId() {
		return getAppConfigValueByLc(EOStgConfig.CONFIG_DEFAULT_USER_PERS_ID, true);
	}

	/**
	 * @return Le libellé court de l'accès par défaut
	 */
	public String getDefaultAccesLc() {
		return getAppConfigValueByLc(EOStgConfig.CONFIG_DEFAULT_ACCES_LC, true);
	}

	/**
	 * @return <code>TRUE</code> si l'étudiant peut choisir son tuteur
	 */
	public boolean isEtudiantPeutChoisirTuteur() {
		String result = getAppConfigValueByLc(EOStgConfig.CONFIG_ETUDIANT_CHOIX_TUTEUR, false);
		return "O".equals(result);
	}

	/**
	 * @return <code>TRUE</code> si le tuteur doit valider
	 */
	public boolean isTuteurValidation() {
		String result = getAppConfigValueByLc(EOStgConfig.CONFIG_TUTEUR_VALIDATION, false);
		return "O".equals(result);
	}

	/**
	 * @return la valeur du paramètre autorisation_antidatage issu de la table STG_CONFIG sous forme booléen
	 */

	public boolean isAutorisationAntidatage() {
		String result = getAppConfigValueByLc(EOStgConfig.CONFIG_ANTIDATAGE, false);
		return "O".equals(result);
	}

	/**
	 * @return le choix pour l'autorisation de'antidatage
	 */
	public String getAutorisationAntidatage() {
		String valString = getAppConfigValueByLc(EOStgConfig.CONFIG_ANTIDATAGE, false);
		if (valString == null) {
			return null;
		}
		return valString;
	}

	/**
	 * BBGS - 02092013
	 * @return la valeur du paramètre blocage_mails_alerte issu de la table STG_CONFIG sous forme booléen
	 */

	public boolean isBlocageMailsAlerte() {
		String result = getAppConfigValueByLc(EOStgConfig.CONFIG_BLOCAGE_MAILS_ALERTE, false);
		return "O".equals(result);
	}

	/**
	 * BBGS - 02092013
	 * @return le choix pour le paramètre blocage_mails_alerte
	 */
	public String getBlocageMailsAlerte() {
		String valString = getAppConfigValueByLc(EOStgConfig.CONFIG_BLOCAGE_MAILS_ALERTE, false);
		if (valString == null) {
			return null;
		}
		return valString;
	}

	/**
	 * BBGS - 02092013
	 * @return la valeur du paramètre blocage_mails_alerte_DDS issu de la table STG_CONFIG sous forme booléen
	 */

	public boolean isBlocageMailsAlerteDDS() {
		String result = getAppConfigValueByLc(EOStgConfig.CONFIG_BLOCAGE_MAILS_ALERTE_DDS, false);
		return "O".equals(result);
	}

	/**
	 * BBGS - 02092013
	 * @return le choix pour le paramètre blocage_mails_alerte_DDS
	 */
	public String getBlocageMailsAlerteDDS() {
		String valString = getAppConfigValueByLc(EOStgConfig.CONFIG_BLOCAGE_MAILS_ALERTE_DDS, false);
		if (valString == null) {
			return null;
		}
		return valString;
	}

	/**
	 * @return La liste des <code>PERS_ID</code> des administrateurs de l'application
	 */
	public List<Integer> getListeAdmin() {
		return getListePersIdFromConfig(EOStgConfig.CONFIG_ADMINS_PERS_ID);
	}

	public List<Integer> getListeResponsablesStructures() {
		return getListePersIdFromConfig(EOStgConfig.CONFIG_RESP_STRUCT_PERS_ID);
	}

	public List<Integer> getListeResponsablesAdministratifsModeCentralise() {
		return getListePersIdFromConfig(EOStgConfig.CONFIG_RESP_ADMIN_PERS_ID);
	}

	public List<Integer> getListeResponsablesPedagogiqueModeCentralise() {
		return getListePersIdFromConfig(EOStgConfig.CONFIG_RESP_PEDA_PERS_ID);
	}

	private List<Integer> getListePersIdFromConfig(String configKey) {
		String listToString = getConfigValueByLc(configKey, false);
		if (StringCtrl.isEmpty(listToString)) {
			return new ArrayList<Integer>();
		}
		List<Integer> liste = new ArrayList<Integer>();
		for (String persIdS : listToString.split(",")) {
			liste.add(Integer.valueOf(persIdS));
		}
		return liste;
	}

	/**
	 * @return La date de fin d'année si elle a été configurée (sous la forme jj/mm)
	 */
	public NSTimestamp getDateFinAnnee() {
		String dateToString = getAppConfigValueByLc(EOStgConfig.CONFIG_DATE_FIN_ANNEE, false);
		if (dateToString != null) {
			return DateCtrl.stringToDate(dateToString, EOStgConfig.FORMAT_DATE_FIN_ANNEE);
		}
		return null;
	}

	public double getTxPlafondHoraire() {
		return Double.valueOf(getAppConfigValueByLc(EOStgConfig.CONFIG_TX_PLAFOND_HORAIRE, true));
	}

	public double getPlafondHoraire() {
		return Double.valueOf(getAppConfigValueByLc(EOStgConfig.CONFIG_PLAFOND_HORAIRE, true));
	}

	public double getDureeMensuelle() {
		return Double.valueOf(getAppConfigValueByLc(EOStgConfig.CONFIG_DUREE_MENSUELLE, true));
	}

	public int getDureeMinGratificationObligatoire() {
		return Integer.valueOf(getAppConfigValueByLc(EOStgConfig.CONFIG_DUREE_MIN_GRAT_OBLIGATOIRE, true));
	}

	/**
	 * @return Le montant minimum de la gratification obligatoire
	 */
	public double getMontantGratificationObligatoire() {
		if (getTxPlafondHoraire() > 0 && getPlafondHoraire() > 0 && getDureeMensuelle() > 0) {
			final double _100 = 100.;
			double tmp = (getTxPlafondHoraire() / _100) * getPlafondHoraire() * getDureeMensuelle();
			return (Math.round(tmp * _100) / _100);
		} else {
			return 0;
		}
	}

	/**
	 * @return La valeur de la periode de carence (en pourcentage)
	 */
	public Integer getPeriodeCarencePourcentage() {
		String valString = getAppConfigValueByLc(EOStgConfig.CONFIG_POURCENTAGE_PERIODE_CARENCE, false);
		if (valString == null) {
			return null;
		}
		return Integer.valueOf(valString);
	}

	/**
	 * @return La durée maximale en mois du(des) stage(s) dans un organisme d'accueil au cours d'une année scolaire
	 */
	public Integer getDureeMaximaleDansOrganisme() {
		String valString = getAppConfigValueByLc(EOStgConfig.CONFIG_DUREE_MAXIMALE_DANS_ORGANISME_ACCUEIL, false);
		if (valString == null) {
			return null;
		}
		return Integer.valueOf(valString);
	}

	/**
	 * BBGS-21082013
	 * @return Le seuil d'alerte
	 */
	public Integer getSeuilAlerte() {
		String valString = getAppConfigValueByLc(EOStgConfig.CONFIG_SEUIL_ALERTE, false);

		if (valString == null) {
			return null;
		}
		return Integer.valueOf(valString);
	}

	/**
	 * @return la valeur de la responsabilité civile par défaut.
	 */

	public String getRespCivileDefaut() {
		return getAppConfigValueByLc(EOStgConfig.CONFIG_DEFAULT_RC, true);
	}

	/**
	 * @return <code> true </code> si l'application est basculée sur la nouvelle scolarité
	 */
	public Boolean getNouvelleScolarite() {
		String result = getAppConfigValueByLc(EOStgConfig.CONFIG_NOUVELLE_SCOlARITE, false);
		return "O".equalsIgnoreCase(result);
	}
}
