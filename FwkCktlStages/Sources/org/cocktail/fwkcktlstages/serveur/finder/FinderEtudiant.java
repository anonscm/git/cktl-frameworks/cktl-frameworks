package org.cocktail.fwkcktlstages.serveur.finder;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;

/**
 * Recherche d'un étudiant
 */
public interface FinderEtudiant {

	/**
	 * @param numeroEtudiant numéro étudiant
	 * @param nom nom de l'étudiant
	 * @param prenom prénom de l'étudiant
	 * @param INE code INE
	 * @return liste des étudiants qui correspondent à un des critères ci-dessus
	 */
	List<IEtudiant> rechercheNomPrenomINE(Integer numeroEtudiant, String nom, String prenom, String INE);

	/**
	 * @param etudiantNumeroIndividu numéro d'individu de l'étudiant
	 * @return liste des étudiants correspondants
	 */
	List<IEtudiant> rechercheNumeroIndividu(Integer etudiantNumeroIndividu);

	/**
	 * @param persId persid de l'étudiant
	 * @return liste des étudiants correspondants
	 */
	List<IEtudiant> recherchePersId(Integer persId);

	/**
	 * @param etudiants liste d'étudiants
	 * @param persId le persId correspondnat à l'idividu d'individu
	 * @return liste des etudiants qui correspondent au numero d'individu
	 */
	List<IEtudiant> filtrerSurPersId(List<IEtudiant> etudiants, final Integer persId);

	/**
	 * @param etudiants liste d'étudiants
	 * @param numeroIndividu numero d'individu
	 * @return liste des etudiants qui correspondent au numero d'individu
	 */
	List<IEtudiant> filtrerSurNumeroIndividu(List<IEtudiant> etudiants, final Integer numeroIndividu);
	
	/**
	 * @param etudiant une étudiant
	 * @param annee une année universitaire
	 * @return <code> true </code> si l'étudiant est bien inscrit pour l'année universitaire
	 */
	boolean isInscritPourAnne(IEtudiant etudiant, Integer annee);

}
