/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.exception;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation._NSStringUtilities;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FwkCktlStagesException extends RuntimeException {

	private String message;
	private boolean bloquant;

	/**
	 * 
	 */
	public FwkCktlStagesException() {
		super();
		bloquant = true;
	}

	/**
	 * @param s
	 *            Message
	 */
	public FwkCktlStagesException(String s) {
		super();
		this.setMessage(s);
		bloquant = true;
	}

	/**
	 * @param s
	 *            Message
	 * @param yn
	 *            Bloquant ?
	 */
	public FwkCktlStagesException(String s, boolean yn) {
		super();
		this.setMessage(s);
		bloquant = yn;
	}

	public boolean isBloquant() {
		return bloquant;
	}

	/**
	 * Indique si l'exception est une exception grave ou si il s'agit d'une exception d'information
	 * 
	 * @return <code>TRUE</code> si l'exception est informative
	 */
	public boolean isInformatif() {
		boolean isInformatif = false;
		String leMessage = getMessageFormatte();
		if (leMessage != null) {
			int index = leMessage.indexOf("ORA-");
			if (index > -1) {
				index = leMessage.indexOf("ORA-20001:");
				if (index > -1) {
					// Exception Oracle renvoyee par les procedures
					// Message informatif pour l'utilisateur
					isInformatif = true;
				} else {
					// Exception Oracle renvoyee par les procedures
					// Exception "non controlee" par les procedures
					isInformatif = false;
				}
			} else {
				// Erreur de validation
				// Message informatif pour l'utilisateur
				isInformatif = true;
			}
		}
		return isInformatif;
	}

	/**
	 * @return Un message reformatte si possible
	 */
	public String getMessageFormatte() {
		String leMessage = getMessage();
		if (leMessage == null) {
			return null;
		}
		leMessage = StringCtrl.replace(leMessage, "'", "\\'");
		leMessage = leMessage.replaceAll("\n", " ");
		leMessage = leMessage.replaceAll("\"", "''");

		int index = leMessage.indexOf("ORA-20001:");
		final int msgMaxSize = 10;
		if (index > -1) {
			leMessage = leMessage.substring(index + msgMaxSize);
			index = leMessage.indexOf("ORA-");
			if (index > -1) {
				leMessage = leMessage.substring(0, index);
			}
		} else {
			// Pour des raisons historiques, certains messages d'erreurs en
			// provenance de la base
			index = leMessage.indexOf("ORA-20016:");
			if (index > -1) {
				leMessage = leMessage.substring(index + msgMaxSize);
				index = leMessage.indexOf("ORA-");
				if (index > -1) {
					leMessage = leMessage.substring(0, index);
				}
			}
		}
		return leMessage;
	}

	/**
	 * @return Le message formaté pour du HTML
	 */
	public String getMessageHTML() {
		String leMessage = getMessage();
		if (leMessage != null) {
			leMessage = _NSStringUtilities.replaceAllInstancesOfString(leMessage, "é", "&eacute;");
			leMessage = _NSStringUtilities.replaceAllInstancesOfString(leMessage, "ê", "&ecirc;");
			leMessage = _NSStringUtilities.replaceAllInstancesOfString(leMessage, "è", "&egrave;");
			leMessage = _NSStringUtilities.replaceAllInstancesOfString(leMessage, "à", "&agrave;");
			leMessage = _NSStringUtilities.replaceAllInstancesOfString(leMessage, "\n", "<br/>");
		}
		return leMessage;
	}

	/**
	 * @return Le message formaté pour du JavaScript
	 */
	public String getMessageJS() {
		String leMessage = getMessage();
		if (leMessage != null) {
			leMessage = _NSStringUtilities.replaceAllInstancesOfString(leMessage, "'", "\\'");
		}
		return leMessage;
	}

	/**
	 * renvoie une cause message pour l exception
	 * 
	 * @return String
	 */
	@Override
	public String getMessage() {
		return message;
	}

	/**
	 * Affecte une cause message pour l exception
	 * 
	 * @param string
	 *            Le message
	 */
	public void setMessage(String string) {
		message = string;
	}
}
