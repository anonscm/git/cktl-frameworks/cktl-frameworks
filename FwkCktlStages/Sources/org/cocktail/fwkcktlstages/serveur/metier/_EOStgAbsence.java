/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStgAbsence.java instead.
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.util.Enumeration;
import java.util.NoSuchElementException;
// import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStgAbsence extends  AFwkCktlStagesRecord {
	//	private static Logger logger = Logger.getLogger(_EOStgAbsence.class);

	public static final String ENTITY_NAME = "FwkCktlStages_StgAbsence";
	public static final String ENTITY_TABLE_NAME = "STAGES.STG_ABSENCE";

	// Attribute Keys
	public static final ERXKey<Long> PERS_ID_VALIDE = new ERXKey<Long>("persIdValide");
	public static final ERXKey<String> SAB_COMMENTAIRE = new ERXKey<String>("sabCommentaire");
	public static final ERXKey<NSTimestamp> SAB_D_DEBUT = new ERXKey<NSTimestamp>("sabDDebut");
	public static final ERXKey<String> SAB_DEMIEJOURNEE_DEBUT = new ERXKey<String>("sabDemiejourneeDebut");
	public static final ERXKey<String> SAB_DEMIEJOURNEE_FIN = new ERXKey<String>("sabDemiejourneeFin");
	public static final ERXKey<NSTimestamp> SAB_D_FIN = new ERXKey<NSTimestamp>("sabDFin");
	public static final ERXKey<String> SAB_VALIDE = new ERXKey<String>("sabValide");

	// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> TO_STAGE = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>("toStage");

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "sabId";

	public static final String PERS_ID_VALIDE_KEY = "persIdValide";
	public static final String SAB_COMMENTAIRE_KEY = "sabCommentaire";
	public static final String SAB_D_DEBUT_KEY = "sabDDebut";
	public static final String SAB_DEMIEJOURNEE_DEBUT_KEY = "sabDemiejourneeDebut";
	public static final String SAB_DEMIEJOURNEE_FIN_KEY = "sabDemiejourneeFin";
	public static final String SAB_D_FIN_KEY = "sabDFin";
	public static final String SAB_VALIDE_KEY = "sabValide";

	// Attributs non visibles
	public static final String MEC_KEY_KEY = "mecKey";
	public static final String SAB_ID_KEY = "sabId";
	public static final String STG_ID_KEY = "stgId";

	// Colonnes dans la base de donnees
	public static final String PERS_ID_VALIDE_COLKEY = "PERS_ID_VALIDE";
	public static final String SAB_COMMENTAIRE_COLKEY = "SAB_COMMENTAIRE";
	public static final String SAB_D_DEBUT_COLKEY = "SAB_D_DEBUT";
	public static final String SAB_DEMIEJOURNEE_DEBUT_COLKEY = "SAB_DEMIEJOURNEE_DEBUT";
	public static final String SAB_DEMIEJOURNEE_FIN_COLKEY = "SAB_DEMIEJOURNEE_FIN";
	public static final String SAB_D_FIN_COLKEY = "SAB_D_FIN";
	public static final String SAB_VALIDE_COLKEY = "SAB_VALIDE";

	public static final String MEC_KEY_COLKEY = "MEC_KEY";
	public static final String SAB_ID_COLKEY = "SAB_ID";
	public static final String STG_ID_COLKEY = "STG_ID";

	// Relationships
	public static final String TO_STAGE_KEY = "toStage";


	// Accessors methods
	public Long persIdValide() {
		return (Long) storedValueForKey(PERS_ID_VALIDE_KEY);
	}

	public void setPersIdValide(Long value) {
		takeStoredValueForKey(value, PERS_ID_VALIDE_KEY);
	}

	public String sabCommentaire() {
		return (String) storedValueForKey(SAB_COMMENTAIRE_KEY);
	}

	public void setSabCommentaire(String value) {
		takeStoredValueForKey(value, SAB_COMMENTAIRE_KEY);
	}

	public NSTimestamp sabDDebut() {
		return (NSTimestamp) storedValueForKey(SAB_D_DEBUT_KEY);
	}

	public void setSabDDebut(NSTimestamp value) {
		takeStoredValueForKey(value, SAB_D_DEBUT_KEY);
	}

	public String sabDemiejourneeDebut() {
		return (String) storedValueForKey(SAB_DEMIEJOURNEE_DEBUT_KEY);
	}

	public void setSabDemiejourneeDebut(String value) {
		takeStoredValueForKey(value, SAB_DEMIEJOURNEE_DEBUT_KEY);
	}

	public String sabDemiejourneeFin() {
		return (String) storedValueForKey(SAB_DEMIEJOURNEE_FIN_KEY);
	}

	public void setSabDemiejourneeFin(String value) {
		takeStoredValueForKey(value, SAB_DEMIEJOURNEE_FIN_KEY);
	}

	public NSTimestamp sabDFin() {
		return (NSTimestamp) storedValueForKey(SAB_D_FIN_KEY);
	}

	public void setSabDFin(NSTimestamp value) {
		takeStoredValueForKey(value, SAB_D_FIN_KEY);
	}

	public String sabValide() {
		return (String) storedValueForKey(SAB_VALIDE_KEY);
	}

	public void setSabValide(String value) {
		takeStoredValueForKey(value, SAB_VALIDE_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgStage toStage() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgStage) storedValueForKey(TO_STAGE_KEY);
	}

	public void setToStageRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgStage oldValue = toStage();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STAGE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_STAGE_KEY);
		}
	}


	/**
	 * Créer une instance de EOStgAbsence avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOStgAbsence createEOStgAbsence(EOEditingContext editingContext, NSTimestamp sabDDebut
, String sabDemiejourneeDebut
, String sabDemiejourneeFin
, NSTimestamp sabDFin
, String sabValide
			) {
		EOStgAbsence eo = (EOStgAbsence) createAndInsertInstance(editingContext, _EOStgAbsence.ENTITY_NAME);
		eo.setSabDDebut(sabDDebut);
		eo.setSabDemiejourneeDebut(sabDemiejourneeDebut);
		eo.setSabDemiejourneeFin(sabDemiejourneeFin);
		eo.setSabDFin(sabDFin);
		eo.setSabValide(sabValide);
		return eo;
	}


	public EOStgAbsence localInstanceIn(EOEditingContext editingContext) {
		return (EOStgAbsence) localInstanceOfObject(editingContext, this);
	}
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOStgAbsence creerInstance(EOEditingContext editingContext) {
		EOStgAbsence object = (EOStgAbsence) createAndInsertInstance(editingContext, _EOStgAbsence.ENTITY_NAME);
		return object;
	}


	public static EOStgAbsence localInstanceIn(EOEditingContext editingContext, EOStgAbsence eo) {
		EOStgAbsence localInstance = (eo == null) ? null : (EOStgAbsence) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOStgAbsence#localInstanceIn a la place.
	 */
	public static EOStgAbsence localInstanceOf(EOEditingContext editingContext, EOStgAbsence eo) {
		return EOStgAbsence.localInstanceIn(editingContext, eo);
	}


	/* Finders */

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier) null);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		fetchSpec.setIsDeep(isDeep);
		fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> eoObjects = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Cree une fetchSpec sur l'entité avec les parametres.
	 * 
	 * @param qualifier
	 * @param sortOrderings
	 * @param distinct
	 * @return
	 */
	public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return fetchSpec;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOStgAbsence fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStgAbsence fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray<EOStgAbsence> eoObjects = fetchAll(editingContext, qualifier, null);
		EOStgAbsence eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else if (count == 1) {
			eoObject = (EOStgAbsence) eoObjects.objectAtIndex(0);
		} else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	
	public static EOStgAbsence fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOStgAbsence fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EOStgAbsence> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOStgAbsence eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else {
			eoObject = (EOStgAbsence) eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOStgAbsence fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOStgAbsence eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOStgAbsence ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	public static EOStgAbsence fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

}
