/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.controles;

import java.util.Date;

import org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;

import com.webobjects.foundation.NSValidation;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class ControleStageAbsences {

	private static final String AM_TXT = "AM";
	private static final String PM_TXT = "PM";
	private static ControleStageAbsences instance;

	protected ControleStageAbsences() {

	}

	/**
	 * @return L'instance de {@link ControleStageAbsences}
	 */
	public static ControleStageAbsences getInstance() {
		if (instance == null) {
			instance = new ControleStageAbsences();
		}
		return instance;
	}

	protected static void setInstance(ControleStageAbsences instance) {
		ControleStageAbsences.instance = instance;
	}

	/**
	 * Vérifie que les dates données sont cohérentes avec les dates données du stage
	 * 
	 * @param stgDebut
	 *            Début du stage
	 * @param stgFin
	 *            Fin du stage
	 * @param absDebut
	 *            Début de l'absence
	 * @param absDemiJourneeDebut
	 *            Demi journée du début de l'absence (AM ou PM)
	 * @param absFin
	 *            Fin de l'absence
	 * @param absDemiJourneeFin
	 *            Demi journée de la fin de l'absence (AM ou PM)
	 */
	public void controleDates(Date stgDebut, Date stgFin, Date absDebut,
			String absDemiJourneeDebut, Date absFin, String absDemiJourneeFin) {
		checkCoherenceDatesAbsence(absDebut, absFin);
		checkDemiesJournees(absDebut, absDemiJourneeDebut, absFin, absDemiJourneeFin);
		checkAvecDatesDuStage(stgDebut, stgFin, absDebut, absFin);
	}

	private void checkAvecDatesDuStage(Date stgDebut, Date stgFin, Date absDebut,
			Date absFin) {
		if (stgDebut == null || stgFin == null) {
			return;
		}
		if (absDebut.before(stgDebut) || absFin.after(stgFin)) {
			throwErreurValidation("L'absence est en dehors des dates du stage");
		}
	}

	private void checkDemiesJournees(Date absDebut, String absDemiJourneeDebut, Date absFin,
			String absDemiJourneeFin) {
		if (absDebut.equals(absFin) && absDemiJourneeDebut != null && absDemiJourneeFin != null
				&& PM_TXT.equals(absDemiJourneeDebut) && AM_TXT.equals(absDemiJourneeFin)) {
			throwErreurValidation("Les demi-journées de début et de fin de sont pas valides");

		}
	}

	private void checkCoherenceDatesAbsence(Date absDebut, Date absFin) {
		if (absDebut == null || absDebut == null) {
			throwErreurValidation("Les dates de début et de fin doivent être renseignées.");
		}
		if (absDebut.after(absFin)) {
			throwErreurValidation("La date de début est après la date de fin");
		}
	}

	private void throwErreurValidation(String msg) {
		String msgHead = "Erreur lors du controle des dates d'absence : ";
		throw new NSValidation.ValidationException(msgHead + msg);
	}

	/**
	 * Vérifie que les dates données sont cohérentes avec les dates du stage
	 * 
	 * @param stage
	 *            Le stage
	 * @param absDebut
	 *            Début de l'absence
	 * @param absDemiJourneeDebut
	 *            Demi journée du début de l'absence (AM ou PM)
	 * @param absFin
	 *            Fin de l'absence
	 * @param absDemiJourneeFin
	 *            Demi journée de la fin de l'absence (AM ou PM)
	 */
	public void controleDates(EOStgStage stage, Date absDebut, String absDemiJourneeDebut, Date absFin,
			String absDemiJourneeFin) {
		if (stage == null) {
			controleDates(null, null, absDebut, absDemiJourneeDebut, absFin, absDemiJourneeFin);
		} else {
			controleDates(stage.stgDDebut(), stage.stgDFin(), absDebut, absDemiJourneeDebut, absFin, absDemiJourneeFin);
		}
	}

	/**
	 * Vérifie que l'absence est cohérente avec les dates du stage auquel elle est attachée
	 * 
	 * @param absence
	 *            Une absence
	 */
	public void controleDates(EOStgAbsence absence) {
		if (absence == null) {
			return;
		}
		controleDates(absence.toStage(), absence.sabDDebut(), absence.sabDemiejourneeDebut(), absence.sabDFin(),
				absence.sabDemiejourneeFin());
	}
}
