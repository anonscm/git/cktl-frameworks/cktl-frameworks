/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStgStage.java instead.
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.util.Enumeration;
import java.util.NoSuchElementException;
// import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStgStage extends  AFwkCktlStagesRecord {
	//	private static Logger logger = Logger.getLogger(_EOStgStage.class);

	public static final String ENTITY_NAME = "FwkCktlStages_StgStage";
	public static final String ENTITY_TABLE_NAME = "STAGES.STG_STAGE";

	// Attribute Keys
	public static final ERXKey<String> CONV_SPECIFIQUE = new ERXKey<String>("convSpecifique");
	public static final ERXKey<Integer> MEC_KEY = new ERXKey<Integer>("mecKey");
	public static final ERXKey<Integer> NO_INDIVIDU_CONTACT_SERVICE = new ERXKey<Integer>("noIndividuContactService");
	public static final ERXKey<Integer> NO_INDIVIDU_CONTACT_STRUCT = new ERXKey<Integer>("noIndividuContactStruct");
	public static final ERXKey<String> NO_TELEPHONE = new ERXKey<String>("noTelephone");
	public static final ERXKey<String> OFFRE_ID = new ERXKey<String>("offreId");
	public static final ERXKey<Integer> PERS_ID_ENCADRANT = new ERXKey<Integer>("persIdEncadrant");
	public static final ERXKey<Integer> PERS_ID_TUTEUR = new ERXKey<Integer>("persIdTuteur");
	public static final ERXKey<String> STG_ACTIVITES = new ERXKey<String>("stgActivites");
	public static final ERXKey<NSTimestamp> STG_D_CREATION = new ERXKey<NSTimestamp>("stgDCreation");
	public static final ERXKey<NSTimestamp> STG_D_DEBUT = new ERXKey<NSTimestamp>("stgDDebut");
	public static final ERXKey<NSTimestamp> STG_D_FIN = new ERXKey<NSTimestamp>("stgDFin");
	public static final ERXKey<NSTimestamp> STG_D_MODIFICATION = new ERXKey<NSTimestamp>("stgDModification");
	public static final ERXKey<NSTimestamp> STG_D_SUPPRESSION = new ERXKey<NSTimestamp>("stgDSuppression");
	public static final ERXKey<Long> STG_DUREE_AM = new ERXKey<Long>("stgDureeAm");
	public static final ERXKey<Long> STG_DUREE_HEBDO = new ERXKey<Long>("stgDureeHebdo");
	public static final ERXKey<Long> STG_DUREE_PM = new ERXKey<Long>("stgDureePm");
	public static final ERXKey<Long> STG_DUREE_QUOTIDIENNE = new ERXKey<Long>("stgDureeQuotidienne");
	public static final ERXKey<String> STG_DUREE_TOTALE_COMMENTAIRE = new ERXKey<String>("stgDureeTotaleCommentaire");
	public static final ERXKey<String> STG_DUREE_TOTALE_UNITE = new ERXKey<String>("stgDureeTotaleUnite");
	public static final ERXKey<java.math.BigDecimal> STG_DUREE_TOTALE_VAL = new ERXKey<java.math.BigDecimal>("stgDureeTotaleVal");
	public static final ERXKey<String> STG_GRAT_AVANTAGES = new ERXKey<String>("stgGratAvantages");
	public static final ERXKey<String> STG_GRAT_MENSUELLE = new ERXKey<String>("stgGratMensuelle");
	public static final ERXKey<Double> STG_GRAT_MONTANT = new ERXKey<Double>("stgGratMontant");
	public static final ERXKey<String> STG_GRAT_OBLIGATOIRE = new ERXKey<String>("stgGratObligatoire");
	public static final ERXKey<String> STG_HORAIRES = new ERXKey<String>("stgHoraires");
	public static final ERXKey<String> STG_LANGUE = new ERXKey<String>("stgLangue");
	public static final ERXKey<Long> STG_NB_JOURS_HEBDO = new ERXKey<Long>("stgNbJoursHebdo");
	public static final ERXKey<String> STG_REF_RESP_CIVILE = new ERXKey<String>("stgRefRespCivile");
	public static final ERXKey<String> STG_SUJET = new ERXKey<String>("stgSujet");
	public static final ERXKey<String> STG_SUJET_PEDAGOGIQUE = new ERXKey<String>("stgSujetPedagogique");
	public static final ERXKey<String> STT_JOURS_NON_OUVRES = new ERXKey<String>("sttJoursNonOuvres");
	public static final ERXKey<String> TYPE_NO = new ERXKey<String>("typeNo");
	public static final ERXKey<String> TYPE_TEL = new ERXKey<String>("typeTel");

	// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> CONTACT_SERVICE_TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("contactServiceToIndividu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> CONTACT_STRUCT_TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("contactStructToIndividu");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine> STG_REPART_DOMAINES = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine>("stgRepartDomaines");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> TO_ABSENCES = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence>("toAbsences");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgAcces> TO_ACCES = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgAcces>("toAcces");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_ADRESSE_ETUDIANT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toAdresseEtudiant");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> TO_ADRESSES = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse>("toAdresses");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_ADRESSE_SERVICE_ACCUEIL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toAdresseServiceAccueil");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_ADRESSE_STRUCTURE_ACCUEIL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toAdresseStructureAccueil");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee> TO_ANNEE = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee>("toAnnee");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> TO_AVENANTS = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant>("toAvenants");
	public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat> TO_CONTRAT = new ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat>("toContrat");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EODevise> TO_DEVISE_GRATIFICATION = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EODevise>("toDeviseGratification");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat> TO_ETAT = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat>("toEtat");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> TO_ETUDIANT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant>("toEtudiant");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> TO_EVENEMENTS = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement>("toEvenements");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOModeVersement> TO_MODE_VERSEMENT_GRATIFICATION = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOModeVersement>("toModeVersementGratification");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_ORGANISME_ACCUEIL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toOrganismeAccueil");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> TO_PIECE_JOINTE_REPARTS = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart>("toPieceJointeReparts");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_SERVICE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toService");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgType> TO_TYPE = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgType>("toType");

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "stgId";

	public static final String CONV_SPECIFIQUE_KEY = "convSpecifique";
	public static final String MEC_KEY_KEY = "mecKey";
	public static final String NO_INDIVIDU_CONTACT_SERVICE_KEY = "noIndividuContactService";
	public static final String NO_INDIVIDU_CONTACT_STRUCT_KEY = "noIndividuContactStruct";
	public static final String NO_TELEPHONE_KEY = "noTelephone";
	public static final String OFFRE_ID_KEY = "offreId";
	public static final String PERS_ID_ENCADRANT_KEY = "persIdEncadrant";
	public static final String PERS_ID_TUTEUR_KEY = "persIdTuteur";
	public static final String STG_ACTIVITES_KEY = "stgActivites";
	public static final String STG_D_CREATION_KEY = "stgDCreation";
	public static final String STG_D_DEBUT_KEY = "stgDDebut";
	public static final String STG_D_FIN_KEY = "stgDFin";
	public static final String STG_D_MODIFICATION_KEY = "stgDModification";
	public static final String STG_D_SUPPRESSION_KEY = "stgDSuppression";
	public static final String STG_DUREE_AM_KEY = "stgDureeAm";
	public static final String STG_DUREE_HEBDO_KEY = "stgDureeHebdo";
	public static final String STG_DUREE_PM_KEY = "stgDureePm";
	public static final String STG_DUREE_QUOTIDIENNE_KEY = "stgDureeQuotidienne";
	public static final String STG_DUREE_TOTALE_COMMENTAIRE_KEY = "stgDureeTotaleCommentaire";
	public static final String STG_DUREE_TOTALE_UNITE_KEY = "stgDureeTotaleUnite";
	public static final String STG_DUREE_TOTALE_VAL_KEY = "stgDureeTotaleVal";
	public static final String STG_GRAT_AVANTAGES_KEY = "stgGratAvantages";
	public static final String STG_GRAT_MENSUELLE_KEY = "stgGratMensuelle";
	public static final String STG_GRAT_MONTANT_KEY = "stgGratMontant";
	public static final String STG_GRAT_OBLIGATOIRE_KEY = "stgGratObligatoire";
	public static final String STG_HORAIRES_KEY = "stgHoraires";
	public static final String STG_LANGUE_KEY = "stgLangue";
	public static final String STG_NB_JOURS_HEBDO_KEY = "stgNbJoursHebdo";
	public static final String STG_REF_RESP_CIVILE_KEY = "stgRefRespCivile";
	public static final String STG_SUJET_KEY = "stgSujet";
	public static final String STG_SUJET_PEDAGOGIQUE_KEY = "stgSujetPedagogique";
	public static final String STT_JOURS_NON_OUVRES_KEY = "sttJoursNonOuvres";
	public static final String TYPE_NO_KEY = "typeNo";
	public static final String TYPE_TEL_KEY = "typeTel";

	// Attributs non visibles
	public static final String ADR_ETU_ORDRE_KEY = "adrEtuOrdre";
	public static final String ADR_SERVICE_ACCUEIL_KEY = "adrServiceAccueil";
	public static final String ADR_STRUCTURE_ACCUEIL_KEY = "adrStructureAccueil";
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String C_STRUCTURE_ACCUEIL_KEY = "cStructureAccueil";
	public static final String C_STRUCTURE_SERVICE_KEY = "cStructureService";
	public static final String NUM_ETUDIANT_KEY = "numEtudiant";
	public static final String SAC_ID_KEY = "sacId";
	public static final String SAN_ID_KEY = "sanId";
	public static final String SET_ID_KEY = "setId";
	public static final String STG_GRAT_DEVISE_KEY = "stgGratDevise";
	public static final String STG_GRAT_MODE_VERSEMENT_KEY = "stgGratModeVersement";
	public static final String STG_ID_KEY = "stgId";
	public static final String STY_ID_KEY = "styId";

	// Colonnes dans la base de donnees
	public static final String CONV_SPECIFIQUE_COLKEY = "CONV_SPECIFIQUE";
	public static final String MEC_KEY_COLKEY = "MEC_KEY";
	public static final String NO_INDIVIDU_CONTACT_SERVICE_COLKEY = "NO_IND_CONTACT_SERVICE";
	public static final String NO_INDIVIDU_CONTACT_STRUCT_COLKEY = "NO_IND_CONTACT_STRUCT";
	public static final String NO_TELEPHONE_COLKEY = "NO_TELEPHONE";
	public static final String OFFRE_ID_COLKEY = "OFFRE_ID";
	public static final String PERS_ID_ENCADRANT_COLKEY = "PERS_ID_ENCADRANT";
	public static final String PERS_ID_TUTEUR_COLKEY = "PERS_ID_TUTEUR";
	public static final String STG_ACTIVITES_COLKEY = "STG_ACTIVITES";
	public static final String STG_D_CREATION_COLKEY = "STG_D_CREATION";
	public static final String STG_D_DEBUT_COLKEY = "STG_D_DEBUT";
	public static final String STG_D_FIN_COLKEY = "STG_D_FIN";
	public static final String STG_D_MODIFICATION_COLKEY = "STG_D_MODIFICATION";
	public static final String STG_D_SUPPRESSION_COLKEY = "STG_D_SUPPRESSION";
	public static final String STG_DUREE_AM_COLKEY = "STG_DUREE_AM";
	public static final String STG_DUREE_HEBDO_COLKEY = "STG_DUREE_HEBDO";
	public static final String STG_DUREE_PM_COLKEY = "STG_DUREE_PM";
	public static final String STG_DUREE_QUOTIDIENNE_COLKEY = "STG_DUREE_QUOTIDIENNE";
	public static final String STG_DUREE_TOTALE_COMMENTAIRE_COLKEY = "STG_DUREE_TOTALE_COMMENTAIRE";
	public static final String STG_DUREE_TOTALE_UNITE_COLKEY = "STG_DUREE_TOTALE_UNITE";
	public static final String STG_DUREE_TOTALE_VAL_COLKEY = "STG_DUREE_TOTALE_VAL";
	public static final String STG_GRAT_AVANTAGES_COLKEY = "STG_GRAT_AVANTAGES";
	public static final String STG_GRAT_MENSUELLE_COLKEY = "STG_GRAT_MENSUELLE";
	public static final String STG_GRAT_MONTANT_COLKEY = "STG_GRAT_MONTANT";
	public static final String STG_GRAT_OBLIGATOIRE_COLKEY = "STG_GRAT_OBLIGATOIRE";
	public static final String STG_HORAIRES_COLKEY = "STG_HORAIRES";
	public static final String STG_LANGUE_COLKEY = "STG_LANGUE";
	public static final String STG_NB_JOURS_HEBDO_COLKEY = "STG_NB_JOURS_HEBDO";
	public static final String STG_REF_RESP_CIVILE_COLKEY = "STG_REF_RESP_CIVILE";
	public static final String STG_SUJET_COLKEY = "STG_SUJET";
	public static final String STG_SUJET_PEDAGOGIQUE_COLKEY = "STG_SUJET_PEDAGOGIQUE";
	public static final String STT_JOURS_NON_OUVRES_COLKEY = "STT_JOURS_NON_OUVRES";
	public static final String TYPE_NO_COLKEY = "TYPE_NO";
	public static final String TYPE_TEL_COLKEY = "TYPE_TEL";

	public static final String ADR_ETU_ORDRE_COLKEY = "ADR_ETU_ORDRE";
	public static final String ADR_SERVICE_ACCUEIL_COLKEY = "ADR_SERVICE_ACCUEIL";
	public static final String ADR_STRUCTURE_ACCUEIL_COLKEY = "ADR_STRUCTURE_ACCUEIL";
	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String C_STRUCTURE_ACCUEIL_COLKEY = "C_STRUCTURE_ACCUEIL";
	public static final String C_STRUCTURE_SERVICE_COLKEY = "C_STRUCTURE_SERVICE";
	public static final String NUM_ETUDIANT_COLKEY = "NUM_ETUDIANT";
	public static final String SAC_ID_COLKEY = "SAC_ID";
	public static final String SAN_ID_COLKEY = "SAN_ID";
	public static final String SET_ID_COLKEY = "SET_ID";
	public static final String STG_GRAT_DEVISE_COLKEY = "STG_GRAT_DEVISE";
	public static final String STG_GRAT_MODE_VERSEMENT_COLKEY = "STG_GRAT_MODE_VERSEMENT";
	public static final String STG_ID_COLKEY = "STG_ID";
	public static final String STY_ID_COLKEY = "STY_ID";

	// Relationships
	public static final String CONTACT_SERVICE_TO_INDIVIDU_KEY = "contactServiceToIndividu";
	public static final String CONTACT_STRUCT_TO_INDIVIDU_KEY = "contactStructToIndividu";
	public static final String STG_REPART_DOMAINES_KEY = "stgRepartDomaines";
	public static final String TO_ABSENCES_KEY = "toAbsences";
	public static final String TO_ACCES_KEY = "toAcces";
	public static final String TO_ADRESSE_ETUDIANT_KEY = "toAdresseEtudiant";
	public static final String TO_ADRESSES_KEY = "toAdresses";
	public static final String TO_ADRESSE_SERVICE_ACCUEIL_KEY = "toAdresseServiceAccueil";
	public static final String TO_ADRESSE_STRUCTURE_ACCUEIL_KEY = "toAdresseStructureAccueil";
	public static final String TO_ANNEE_KEY = "toAnnee";
	public static final String TO_AVENANTS_KEY = "toAvenants";
	public static final String TO_CONTRAT_KEY = "toContrat";
	public static final String TO_DEVISE_GRATIFICATION_KEY = "toDeviseGratification";
	public static final String TO_ETAT_KEY = "toEtat";
	public static final String TO_ETUDIANT_KEY = "toEtudiant";
	public static final String TO_EVENEMENTS_KEY = "toEvenements";
	public static final String TO_MODE_VERSEMENT_GRATIFICATION_KEY = "toModeVersementGratification";
	public static final String TO_ORGANISME_ACCUEIL_KEY = "toOrganismeAccueil";
	public static final String TO_PIECE_JOINTE_REPARTS_KEY = "toPieceJointeReparts";
	public static final String TO_SERVICE_KEY = "toService";
	public static final String TO_TYPE_KEY = "toType";


	// Accessors methods
	public String convSpecifique() {
		return (String) storedValueForKey(CONV_SPECIFIQUE_KEY);
	}

	public void setConvSpecifique(String value) {
		takeStoredValueForKey(value, CONV_SPECIFIQUE_KEY);
	}

	public Integer mecKey() {
		return (Integer) storedValueForKey(MEC_KEY_KEY);
	}

	public void setMecKey(Integer value) {
		takeStoredValueForKey(value, MEC_KEY_KEY);
	}

	public Integer noIndividuContactService() {
		return (Integer) storedValueForKey(NO_INDIVIDU_CONTACT_SERVICE_KEY);
	}

	public void setNoIndividuContactService(Integer value) {
		takeStoredValueForKey(value, NO_INDIVIDU_CONTACT_SERVICE_KEY);
	}

	public Integer noIndividuContactStruct() {
		return (Integer) storedValueForKey(NO_INDIVIDU_CONTACT_STRUCT_KEY);
	}

	public void setNoIndividuContactStruct(Integer value) {
		takeStoredValueForKey(value, NO_INDIVIDU_CONTACT_STRUCT_KEY);
	}

	public String noTelephone() {
		return (String) storedValueForKey(NO_TELEPHONE_KEY);
	}

	public void setNoTelephone(String value) {
		takeStoredValueForKey(value, NO_TELEPHONE_KEY);
	}

	public String offreId() {
		return (String) storedValueForKey(OFFRE_ID_KEY);
	}

	public void setOffreId(String value) {
		takeStoredValueForKey(value, OFFRE_ID_KEY);
	}

	public Integer persIdEncadrant() {
		return (Integer) storedValueForKey(PERS_ID_ENCADRANT_KEY);
	}

	public void setPersIdEncadrant(Integer value) {
		takeStoredValueForKey(value, PERS_ID_ENCADRANT_KEY);
	}

	public Integer persIdTuteur() {
		return (Integer) storedValueForKey(PERS_ID_TUTEUR_KEY);
	}

	public void setPersIdTuteur(Integer value) {
		takeStoredValueForKey(value, PERS_ID_TUTEUR_KEY);
	}

	public String stgActivites() {
		return (String) storedValueForKey(STG_ACTIVITES_KEY);
	}

	public void setStgActivites(String value) {
		takeStoredValueForKey(value, STG_ACTIVITES_KEY);
	}

	public NSTimestamp stgDCreation() {
		return (NSTimestamp) storedValueForKey(STG_D_CREATION_KEY);
	}

	public void setStgDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, STG_D_CREATION_KEY);
	}

	public NSTimestamp stgDDebut() {
		return (NSTimestamp) storedValueForKey(STG_D_DEBUT_KEY);
	}

	public void setStgDDebut(NSTimestamp value) {
		takeStoredValueForKey(value, STG_D_DEBUT_KEY);
	}

	public NSTimestamp stgDFin() {
		return (NSTimestamp) storedValueForKey(STG_D_FIN_KEY);
	}

	public void setStgDFin(NSTimestamp value) {
		takeStoredValueForKey(value, STG_D_FIN_KEY);
	}

	public NSTimestamp stgDModification() {
		return (NSTimestamp) storedValueForKey(STG_D_MODIFICATION_KEY);
	}

	public void setStgDModification(NSTimestamp value) {
		takeStoredValueForKey(value, STG_D_MODIFICATION_KEY);
	}

	public NSTimestamp stgDSuppression() {
		return (NSTimestamp) storedValueForKey(STG_D_SUPPRESSION_KEY);
	}

	public void setStgDSuppression(NSTimestamp value) {
		takeStoredValueForKey(value, STG_D_SUPPRESSION_KEY);
	}

	public Long stgDureeAm() {
		return (Long) storedValueForKey(STG_DUREE_AM_KEY);
	}

	public void setStgDureeAm(Long value) {
		takeStoredValueForKey(value, STG_DUREE_AM_KEY);
	}

	public Long stgDureeHebdo() {
		return (Long) storedValueForKey(STG_DUREE_HEBDO_KEY);
	}

	public void setStgDureeHebdo(Long value) {
		takeStoredValueForKey(value, STG_DUREE_HEBDO_KEY);
	}

	public Long stgDureePm() {
		return (Long) storedValueForKey(STG_DUREE_PM_KEY);
	}

	public void setStgDureePm(Long value) {
		takeStoredValueForKey(value, STG_DUREE_PM_KEY);
	}

	public Long stgDureeQuotidienne() {
		return (Long) storedValueForKey(STG_DUREE_QUOTIDIENNE_KEY);
	}

	public void setStgDureeQuotidienne(Long value) {
		takeStoredValueForKey(value, STG_DUREE_QUOTIDIENNE_KEY);
	}

	public String stgDureeTotaleCommentaire() {
		return (String) storedValueForKey(STG_DUREE_TOTALE_COMMENTAIRE_KEY);
	}

	public void setStgDureeTotaleCommentaire(String value) {
		takeStoredValueForKey(value, STG_DUREE_TOTALE_COMMENTAIRE_KEY);
	}

	public String stgDureeTotaleUnite() {
		return (String) storedValueForKey(STG_DUREE_TOTALE_UNITE_KEY);
	}

	public void setStgDureeTotaleUnite(String value) {
		takeStoredValueForKey(value, STG_DUREE_TOTALE_UNITE_KEY);
	}

	public java.math.BigDecimal stgDureeTotaleVal() {
		return (java.math.BigDecimal) storedValueForKey(STG_DUREE_TOTALE_VAL_KEY);
	}

	public void setStgDureeTotaleVal(java.math.BigDecimal value) {
		takeStoredValueForKey(value, STG_DUREE_TOTALE_VAL_KEY);
	}

	public String stgGratAvantages() {
		return (String) storedValueForKey(STG_GRAT_AVANTAGES_KEY);
	}

	public void setStgGratAvantages(String value) {
		takeStoredValueForKey(value, STG_GRAT_AVANTAGES_KEY);
	}

	public String stgGratMensuelle() {
		return (String) storedValueForKey(STG_GRAT_MENSUELLE_KEY);
	}

	public void setStgGratMensuelle(String value) {
		takeStoredValueForKey(value, STG_GRAT_MENSUELLE_KEY);
	}

	public Double stgGratMontant() {
		return (Double) storedValueForKey(STG_GRAT_MONTANT_KEY);
	}

	public void setStgGratMontant(Double value) {
		takeStoredValueForKey(value, STG_GRAT_MONTANT_KEY);
	}

	public String stgGratObligatoire() {
		return (String) storedValueForKey(STG_GRAT_OBLIGATOIRE_KEY);
	}

	public void setStgGratObligatoire(String value) {
		takeStoredValueForKey(value, STG_GRAT_OBLIGATOIRE_KEY);
	}

	public String stgHoraires() {
		return (String) storedValueForKey(STG_HORAIRES_KEY);
	}

	public void setStgHoraires(String value) {
		takeStoredValueForKey(value, STG_HORAIRES_KEY);
	}

	public String stgLangue() {
		return (String) storedValueForKey(STG_LANGUE_KEY);
	}

	public void setStgLangue(String value) {
		takeStoredValueForKey(value, STG_LANGUE_KEY);
	}

	public Long stgNbJoursHebdo() {
		return (Long) storedValueForKey(STG_NB_JOURS_HEBDO_KEY);
	}

	public void setStgNbJoursHebdo(Long value) {
		takeStoredValueForKey(value, STG_NB_JOURS_HEBDO_KEY);
	}

	public String stgRefRespCivile() {
		return (String) storedValueForKey(STG_REF_RESP_CIVILE_KEY);
	}

	public void setStgRefRespCivile(String value) {
		takeStoredValueForKey(value, STG_REF_RESP_CIVILE_KEY);
	}

	public String stgSujet() {
		return (String) storedValueForKey(STG_SUJET_KEY);
	}

	public void setStgSujet(String value) {
		takeStoredValueForKey(value, STG_SUJET_KEY);
	}

	public String stgSujetPedagogique() {
		return (String) storedValueForKey(STG_SUJET_PEDAGOGIQUE_KEY);
	}

	public void setStgSujetPedagogique(String value) {
		takeStoredValueForKey(value, STG_SUJET_PEDAGOGIQUE_KEY);
	}

	public String sttJoursNonOuvres() {
		return (String) storedValueForKey(STT_JOURS_NON_OUVRES_KEY);
	}

	public void setSttJoursNonOuvres(String value) {
		takeStoredValueForKey(value, STT_JOURS_NON_OUVRES_KEY);
	}

	public String typeNo() {
		return (String) storedValueForKey(TYPE_NO_KEY);
	}

	public void setTypeNo(String value) {
		takeStoredValueForKey(value, TYPE_NO_KEY);
	}

	public String typeTel() {
		return (String) storedValueForKey(TYPE_TEL_KEY);
	}

	public void setTypeTel(String value) {
		takeStoredValueForKey(value, TYPE_TEL_KEY);
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu contactServiceToIndividu() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) storedValueForKey(CONTACT_SERVICE_TO_INDIVIDU_KEY);
	}

	public void setContactServiceToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = contactServiceToIndividu();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTACT_SERVICE_TO_INDIVIDU_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, CONTACT_SERVICE_TO_INDIVIDU_KEY);
		}
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu contactStructToIndividu() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) storedValueForKey(CONTACT_STRUCT_TO_INDIVIDU_KEY);
	}

	public void setContactStructToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = contactStructToIndividu();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTACT_STRUCT_TO_INDIVIDU_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, CONTACT_STRUCT_TO_INDIVIDU_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgAcces toAcces() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgAcces) storedValueForKey(TO_ACCES_KEY);
	}

	public void setToAccesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgAcces value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgAcces oldValue = toAcces();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ACCES_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_ACCES_KEY);
		}
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toAdresseEtudiant() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse) storedValueForKey(TO_ADRESSE_ETUDIANT_KEY);
	}

	public void setToAdresseEtudiantRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toAdresseEtudiant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ADRESSE_ETUDIANT_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_ADRESSE_ETUDIANT_KEY);
		}
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toAdresseServiceAccueil() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse) storedValueForKey(TO_ADRESSE_SERVICE_ACCUEIL_KEY);
	}

	public void setToAdresseServiceAccueilRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toAdresseServiceAccueil();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ADRESSE_SERVICE_ACCUEIL_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_ADRESSE_SERVICE_ACCUEIL_KEY);
		}
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toAdresseStructureAccueil() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse) storedValueForKey(TO_ADRESSE_STRUCTURE_ACCUEIL_KEY);
	}

	public void setToAdresseStructureAccueilRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toAdresseStructureAccueil();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ADRESSE_STRUCTURE_ACCUEIL_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_ADRESSE_STRUCTURE_ACCUEIL_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee toAnnee() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee) storedValueForKey(TO_ANNEE_KEY);
	}

	public void setToAnneeRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee oldValue = toAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ANNEE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_ANNEE_KEY);
		}
	}

	public org.cocktail.cocowork.server.metier.convention.Contrat toContrat() {
		return (org.cocktail.cocowork.server.metier.convention.Contrat) storedValueForKey(TO_CONTRAT_KEY);
	}

	public void setToContratRelationship(org.cocktail.cocowork.server.metier.convention.Contrat value) {
		if (value == null) {
			org.cocktail.cocowork.server.metier.convention.Contrat oldValue = toContrat();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CONTRAT_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_CONTRAT_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EODevise toDeviseGratification() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EODevise) storedValueForKey(TO_DEVISE_GRATIFICATION_KEY);
	}

	public void setToDeviseGratificationRelationship(org.cocktail.fwkcktlstages.serveur.metier.EODevise value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EODevise oldValue = toDeviseGratification();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEVISE_GRATIFICATION_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_DEVISE_GRATIFICATION_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat toEtat() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat) storedValueForKey(TO_ETAT_KEY);
	}

	public void setToEtatRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat oldValue = toEtat();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ETAT_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_ETAT_KEY);
		}
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiant() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant) storedValueForKey(TO_ETUDIANT_KEY);
	}

	public void setToEtudiantRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant oldValue = toEtudiant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ETUDIANT_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_ETUDIANT_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOModeVersement toModeVersementGratification() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOModeVersement) storedValueForKey(TO_MODE_VERSEMENT_GRATIFICATION_KEY);
	}

	public void setToModeVersementGratificationRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOModeVersement value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOModeVersement oldValue = toModeVersementGratification();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODE_VERSEMENT_GRATIFICATION_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_MODE_VERSEMENT_GRATIFICATION_KEY);
		}
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toOrganismeAccueil() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(TO_ORGANISME_ACCUEIL_KEY);
	}

	public void setToOrganismeAccueilRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toOrganismeAccueil();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORGANISME_ACCUEIL_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_ORGANISME_ACCUEIL_KEY);
		}
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toService() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(TO_SERVICE_KEY);
	}

	public void setToServiceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toService();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SERVICE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_SERVICE_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgType toType() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgType) storedValueForKey(TO_TYPE_KEY);
	}

	public void setToTypeRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgType value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgType oldValue = toType();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_KEY);
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine> stgRepartDomaines() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine>) storedValueForKey(STG_REPART_DOMAINES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine> stgRepartDomaines(EOQualifier qualifier) {
		return stgRepartDomaines(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine> stgRepartDomaines(EOQualifier qualifier, boolean fetch) {
		return stgRepartDomaines(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine> stgRepartDomaines(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine.STG_STAGE_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = stgRepartDomaines();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToStgRepartDomainesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine object) {
		addObjectToBothSidesOfRelationshipWithKey(object, STG_REPART_DOMAINES_KEY);
	}

	public void removeFromStgRepartDomainesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_REPART_DOMAINES_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine createStgRepartDomainesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgRepartDomaine");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, STG_REPART_DOMAINES_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine) eo;
	}

	public void deleteStgRepartDomainesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_REPART_DOMAINES_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllStgRepartDomainesRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgRepartDomaine> objects = stgRepartDomaines().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteStgRepartDomainesRelationship(objects.nextElement());
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> toAbsences() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence>) storedValueForKey(TO_ABSENCES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> toAbsences(EOQualifier qualifier) {
		return toAbsences(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> toAbsences(EOQualifier qualifier, boolean fetch) {
		return toAbsences(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> toAbsences(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence.TO_STAGE_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = toAbsences();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToToAbsencesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_ABSENCES_KEY);
	}

	public void removeFromToAbsencesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ABSENCES_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence createToAbsencesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgAbsence");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, TO_ABSENCES_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence) eo;
	}

	public void deleteToAbsencesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ABSENCES_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllToAbsencesRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence> objects = toAbsences().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToAbsencesRelationship(objects.nextElement());
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> toAdresses() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse>) storedValueForKey(TO_ADRESSES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> toAdresses(EOQualifier qualifier) {
		return toAdresses(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> toAdresses(EOQualifier qualifier, boolean fetch) {
		return toAdresses(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> toAdresses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse.TO_STAGE_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = toAdresses();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToToAdressesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_ADRESSES_KEY);
	}

	public void removeFromToAdressesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ADRESSES_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse createToAdressesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgAdresse");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, TO_ADRESSES_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse) eo;
	}

	public void deleteToAdressesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ADRESSES_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllToAdressesRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> objects = toAdresses().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToAdressesRelationship(objects.nextElement());
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> toAvenants() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant>) storedValueForKey(TO_AVENANTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> toAvenants(EOQualifier qualifier) {
		return toAvenants(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> toAvenants(EOQualifier qualifier, boolean fetch) {
		return toAvenants(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> toAvenants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant.TO_STAGE_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = toAvenants();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToToAvenantsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_AVENANTS_KEY);
	}

	public void removeFromToAvenantsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_AVENANTS_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant createToAvenantsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgAvenant");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, TO_AVENANTS_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant) eo;
	}

	public void deleteToAvenantsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_AVENANTS_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllToAvenantsRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> objects = toAvenants().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToAvenantsRelationship(objects.nextElement());
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> toEvenements() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement>) storedValueForKey(TO_EVENEMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> toEvenements(EOQualifier qualifier) {
		return toEvenements(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> toEvenements(EOQualifier qualifier, boolean fetch) {
		return toEvenements(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> toEvenements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement.STG_STAGE_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = toEvenements();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToToEvenementsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_EVENEMENTS_KEY);
	}

	public void removeFromToEvenementsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EVENEMENTS_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement createToEvenementsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgEvenement");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, TO_EVENEMENTS_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement) eo;
	}

	public void deleteToEvenementsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EVENEMENTS_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllToEvenementsRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> objects = toEvenements().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToEvenementsRelationship(objects.nextElement());
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> toPieceJointeReparts() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart>) storedValueForKey(TO_PIECE_JOINTE_REPARTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> toPieceJointeReparts(EOQualifier qualifier) {
		return toPieceJointeReparts(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> toPieceJointeReparts(EOQualifier qualifier, boolean fetch) {
		return toPieceJointeReparts(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> toPieceJointeReparts(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart.STG_STAGE_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = toPieceJointeReparts();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToToPieceJointeRepartsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_PIECE_JOINTE_REPARTS_KEY);
	}

	public void removeFromToPieceJointeRepartsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PIECE_JOINTE_REPARTS_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart createToPieceJointeRepartsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgPieceJointeRepart");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, TO_PIECE_JOINTE_REPARTS_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart) eo;
	}

	public void deleteToPieceJointeRepartsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PIECE_JOINTE_REPARTS_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllToPieceJointeRepartsRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> objects = toPieceJointeReparts().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToPieceJointeRepartsRelationship(objects.nextElement());
		}
	}


	/**
	 * Créer une instance de EOStgStage avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOStgStage createEOStgStage(EOEditingContext editingContext, String convSpecifique
, String noTelephone
, String stgActivites
, NSTimestamp stgDCreation
, NSTimestamp stgDDebut
, NSTimestamp stgDFin
, NSTimestamp stgDModification
, Long stgDureeHebdo
, String stgDureeTotaleUnite
, java.math.BigDecimal stgDureeTotaleVal
, String stgGratObligatoire
, Long stgNbJoursHebdo
, String stgRefRespCivile
, String stgSujet
, String typeNo
, String typeTel
, org.cocktail.fwkcktlstages.serveur.metier.EOStgAcces toAcces, org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toAdresseEtudiant, org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee toAnnee, org.cocktail.cocowork.server.metier.convention.Contrat toContrat, org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat toEtat, org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiant, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toOrganismeAccueil, org.cocktail.fwkcktlstages.serveur.metier.EOStgType toType			) {
		EOStgStage eo = (EOStgStage) createAndInsertInstance(editingContext, _EOStgStage.ENTITY_NAME);
		eo.setConvSpecifique(convSpecifique);
		eo.setNoTelephone(noTelephone);
		eo.setStgActivites(stgActivites);
		eo.setStgDCreation(stgDCreation);
		eo.setStgDDebut(stgDDebut);
		eo.setStgDFin(stgDFin);
		eo.setStgDModification(stgDModification);
		eo.setStgDureeHebdo(stgDureeHebdo);
		eo.setStgDureeTotaleUnite(stgDureeTotaleUnite);
		eo.setStgDureeTotaleVal(stgDureeTotaleVal);
		eo.setStgGratObligatoire(stgGratObligatoire);
		eo.setStgNbJoursHebdo(stgNbJoursHebdo);
		eo.setStgRefRespCivile(stgRefRespCivile);
		eo.setStgSujet(stgSujet);
		eo.setTypeNo(typeNo);
		eo.setTypeTel(typeTel);
		eo.setToAccesRelationship(toAcces);
		eo.setToAdresseEtudiantRelationship(toAdresseEtudiant);
		eo.setToAnneeRelationship(toAnnee);
		eo.setToContratRelationship(toContrat);
		eo.setToEtatRelationship(toEtat);
		eo.setToEtudiantRelationship(toEtudiant);
		eo.setToOrganismeAccueilRelationship(toOrganismeAccueil);
		eo.setToTypeRelationship(toType);
		return eo;
	}


	public EOStgStage localInstanceIn(EOEditingContext editingContext) {
		return (EOStgStage) localInstanceOfObject(editingContext, this);
	}
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOStgStage creerInstance(EOEditingContext editingContext) {
		EOStgStage object = (EOStgStage) createAndInsertInstance(editingContext, _EOStgStage.ENTITY_NAME);
		return object;
	}


	public static EOStgStage localInstanceIn(EOEditingContext editingContext, EOStgStage eo) {
		EOStgStage localInstance = (eo == null) ? null : (EOStgStage) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOStgStage#localInstanceIn a la place.
	 */
	public static EOStgStage localInstanceOf(EOEditingContext editingContext, EOStgStage eo) {
		return EOStgStage.localInstanceIn(editingContext, eo);
	}


	/* Finders */

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier) null);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		fetchSpec.setIsDeep(isDeep);
		fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> eoObjects = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Cree une fetchSpec sur l'entité avec les parametres.
	 * 
	 * @param qualifier
	 * @param sortOrderings
	 * @param distinct
	 * @return
	 */
	public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return fetchSpec;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOStgStage fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStgStage fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray<EOStgStage> eoObjects = fetchAll(editingContext, qualifier, null);
		EOStgStage eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else if (count == 1) {
			eoObject = (EOStgStage) eoObjects.objectAtIndex(0);
		} else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	
	public static EOStgStage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOStgStage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EOStgStage> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOStgStage eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else {
			eoObject = (EOStgStage) eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOStgStage fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOStgStage eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOStgStage ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	public static EOStgStage fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

}
