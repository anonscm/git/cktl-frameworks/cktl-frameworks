package org.cocktail.fwkcktlstages.serveur.metier.scolarite;

import java.math.BigDecimal;

/**
 * représentation d'un EC
 */
public class EC {
	private Integer id;
	private String code;
	private String libelle;
	private Integer annee;
	private Integer composanteId;
	private BigDecimal creditECTS;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return getCode() + "-" + getLibelle();
	}

	public Integer getAnnee() {
		return annee;
	}

	public void setAnnee(Integer annee) {
		this.annee = annee;
	}
	
	public Integer getComposanteId() {
	  return composanteId;
  }
	
	public void setComposanteId(Integer composanteId) {
	  this.composanteId = composanteId;
  }
	
	public BigDecimal getCreditECTS() {
	  return creditECTS;
  }
	
	public void setCreditECTS(BigDecimal creditECTS) {
	  this.creditECTS = creditECTS;
  }

	public String getCodeEtLibelle() {
		return getCode() + " - " + getLibelle();
	}
}
