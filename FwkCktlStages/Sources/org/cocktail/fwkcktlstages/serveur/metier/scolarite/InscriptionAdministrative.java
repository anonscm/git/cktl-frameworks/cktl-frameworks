package org.cocktail.fwkcktlstages.serveur.metier.scolarite;

/**
 * @author lilia
 * Une inscription administrative à un diplome
 */
public class InscriptionAdministrative {
	private String libelle;
	private Diplome diplome;
	private Integer niveau;
	private String libelleDiplomeComplet;

	public String getLibelle() {
	  return libelle;
  }


	public void setLibelle(String libelle) {
	  this.libelle = libelle;
  }


	public Diplome getDiplome() {
	  return diplome;
  }


	public void setDiplome(Diplome diplome) {
	  this.diplome = diplome;
  }


	public Integer getNiveau() {
	  return niveau;
  }


	public void setNiveau(Integer niveau) {
	  this.niveau = niveau;
  }


	public String getLibelleDiplomeComplet() {
	  return libelleDiplomeComplet;
  }


	public void setLibelleDiplomeComplet(String libelleDiplomeComplet) {
	  this.libelleDiplomeComplet = libelleDiplomeComplet;
  }	
	
}
