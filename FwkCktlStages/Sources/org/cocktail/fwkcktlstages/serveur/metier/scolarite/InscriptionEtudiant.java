package org.cocktail.fwkcktlstages.serveur.metier.scolarite;

/**
 *  données de l'étudiant relative à une inscription donnee
 */
public class InscriptionEtudiant {
	private Integer persId;
	private Integer etudNumero;
	private String adrNom;
	private String adrPrenom;

	public Integer getPersId() {
	  return persId;
  }

	public void setPersId(Integer persId) {
	  this.persId = persId;
  }

	public Integer getEtudNumero() {
	  return etudNumero;
  }

	public void setEtudNumero(Integer etudNumero) {
	  this.etudNumero = etudNumero;
  }

	public String getAdrNom() {
	  return adrNom;
  }

	public void setAdrNom(String adrNom) {
	  this.adrNom = adrNom;
  }

	public String getAdrPrenom() {
	  return adrPrenom;
  }

	public void setAdrPrenom(String adrPrenom) {
	  this.adrPrenom = adrPrenom;
  }

}
