package org.cocktail.fwkcktlstages.serveur.metier.scolarite;

/**
 * Représentation d'un diplome
 */
public class Diplome {
	private String libelle;
	private String libelleParcours;
	private String gradeAbreviation;

	public String getLibelle() {
	  return libelle;
  }

	public void setLibelle(String libelle) {
	  this.libelle = libelle;
  }

	public String getLibelleParcours() {
	  return libelleParcours;
  }

	public void setLibelleParcours(String libelleParcours) {
	  this.libelleParcours = libelleParcours;
  }

	public String getGradeAbreviation() {
	  return gradeAbreviation;
  }

	public void setGradeAbreviation(String gradeAbreviation) {
	  this.gradeAbreviation = gradeAbreviation;
  }
}
