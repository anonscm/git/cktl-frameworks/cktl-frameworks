/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStgType.java instead.
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.util.Enumeration;
import java.util.NoSuchElementException;
// import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStgType extends  AFwkCktlStagesRecord {
	//	private static Logger logger = Logger.getLogger(_EOStgType.class);

	public static final String ENTITY_NAME = "FwkCktlStages_StgType";
	public static final String ENTITY_TABLE_NAME = "STAGES.STG_TYPE";

	// Attribute Keys
	public static final ERXKey<String> STY_AFFICHAGE = new ERXKey<String>("styAffichage");
	public static final ERXKey<String> STY_EC_OBLIGATOIRE = new ERXKey<String>("styEcObligatoire");
	public static final ERXKey<String> STY_LC = new ERXKey<String>("styLc");
	public static final ERXKey<String> STY_LL = new ERXKey<String>("styLl");
	public static final ERXKey<String> STY_MODELE_CONV = new ERXKey<String>("styModeleConv");
	public static final ERXKey<String> STY_MODELE_LETTRE = new ERXKey<String>("styModeleLettre");

	// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> STG_PIECE_JOINTE_CONFIGS = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig>("stgPieceJointeConfigs");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> STG_STAGES = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>("stgStages");

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "styId";

	public static final String STY_AFFICHAGE_KEY = "styAffichage";
	public static final String STY_EC_OBLIGATOIRE_KEY = "styEcObligatoire";
	public static final String STY_LC_KEY = "styLc";
	public static final String STY_LL_KEY = "styLl";
	public static final String STY_MODELE_CONV_KEY = "styModeleConv";
	public static final String STY_MODELE_LETTRE_KEY = "styModeleLettre";

	// Attributs non visibles
	public static final String STY_ID_KEY = "styId";

	// Colonnes dans la base de donnees
	public static final String STY_AFFICHAGE_COLKEY = "STY_AFFICHAGE";
	public static final String STY_EC_OBLIGATOIRE_COLKEY = "STY_EC_OBLIGATOIRE";
	public static final String STY_LC_COLKEY = "STY_LC";
	public static final String STY_LL_COLKEY = "STY_LL";
	public static final String STY_MODELE_CONV_COLKEY = "STY_MODELE_CONV";
	public static final String STY_MODELE_LETTRE_COLKEY = "STY_MODELE_LETTRE";

	public static final String STY_ID_COLKEY = "STY_ID";

	// Relationships
	public static final String STG_PIECE_JOINTE_CONFIGS_KEY = "stgPieceJointeConfigs";
	public static final String STG_STAGES_KEY = "stgStages";


	// Accessors methods
	public String styAffichage() {
		return (String) storedValueForKey(STY_AFFICHAGE_KEY);
	}

	public void setStyAffichage(String value) {
		takeStoredValueForKey(value, STY_AFFICHAGE_KEY);
	}

	public String styEcObligatoire() {
		return (String) storedValueForKey(STY_EC_OBLIGATOIRE_KEY);
	}

	public void setStyEcObligatoire(String value) {
		takeStoredValueForKey(value, STY_EC_OBLIGATOIRE_KEY);
	}

	public String styLc() {
		return (String) storedValueForKey(STY_LC_KEY);
	}

	public void setStyLc(String value) {
		takeStoredValueForKey(value, STY_LC_KEY);
	}

	public String styLl() {
		return (String) storedValueForKey(STY_LL_KEY);
	}

	public void setStyLl(String value) {
		takeStoredValueForKey(value, STY_LL_KEY);
	}

	public String styModeleConv() {
		return (String) storedValueForKey(STY_MODELE_CONV_KEY);
	}

	public void setStyModeleConv(String value) {
		takeStoredValueForKey(value, STY_MODELE_CONV_KEY);
	}

	public String styModeleLettre() {
		return (String) storedValueForKey(STY_MODELE_LETTRE_KEY);
	}

	public void setStyModeleLettre(String value) {
		takeStoredValueForKey(value, STY_MODELE_LETTRE_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> stgPieceJointeConfigs() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig>) storedValueForKey(STG_PIECE_JOINTE_CONFIGS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> stgPieceJointeConfigs(EOQualifier qualifier) {
		return stgPieceJointeConfigs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> stgPieceJointeConfigs(EOQualifier qualifier, boolean fetch) {
		return stgPieceJointeConfigs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> stgPieceJointeConfigs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig.STG_TYPE_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = stgPieceJointeConfigs();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToStgPieceJointeConfigsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig object) {
		addObjectToBothSidesOfRelationshipWithKey(object, STG_PIECE_JOINTE_CONFIGS_KEY);
	}

	public void removeFromStgPieceJointeConfigsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_PIECE_JOINTE_CONFIGS_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig createStgPieceJointeConfigsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgPieceJointeConfig");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, STG_PIECE_JOINTE_CONFIGS_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig) eo;
	}

	public void deleteStgPieceJointeConfigsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_PIECE_JOINTE_CONFIGS_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllStgPieceJointeConfigsRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> objects = stgPieceJointeConfigs().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteStgPieceJointeConfigsRelationship(objects.nextElement());
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> stgStages() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>) storedValueForKey(STG_STAGES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> stgStages(EOQualifier qualifier) {
		return stgStages(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> stgStages(EOQualifier qualifier, boolean fetch) {
		return stgStages(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> stgStages(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage.TO_TYPE_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgStage.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = stgStages();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToStgStagesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage object) {
		addObjectToBothSidesOfRelationshipWithKey(object, STG_STAGES_KEY);
	}

	public void removeFromStgStagesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_STAGES_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgStage createStgStagesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgStage");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, STG_STAGES_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgStage) eo;
	}

	public void deleteStgStagesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_STAGES_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllStgStagesRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> objects = stgStages().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteStgStagesRelationship(objects.nextElement());
		}
	}


	/**
	 * Créer une instance de EOStgType avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOStgType createEOStgType(EOEditingContext editingContext, String styAffichage
, String styEcObligatoire
, String styLc
, String styLl
, String styModeleConv
			) {
		EOStgType eo = (EOStgType) createAndInsertInstance(editingContext, _EOStgType.ENTITY_NAME);
		eo.setStyAffichage(styAffichage);
		eo.setStyEcObligatoire(styEcObligatoire);
		eo.setStyLc(styLc);
		eo.setStyLl(styLl);
		eo.setStyModeleConv(styModeleConv);
		return eo;
	}


	public EOStgType localInstanceIn(EOEditingContext editingContext) {
		return (EOStgType) localInstanceOfObject(editingContext, this);
	}
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOStgType creerInstance(EOEditingContext editingContext) {
		EOStgType object = (EOStgType) createAndInsertInstance(editingContext, _EOStgType.ENTITY_NAME);
		return object;
	}


	public static EOStgType localInstanceIn(EOEditingContext editingContext, EOStgType eo) {
		EOStgType localInstance = (eo == null) ? null : (EOStgType) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOStgType#localInstanceIn a la place.
	 */
	public static EOStgType localInstanceOf(EOEditingContext editingContext, EOStgType eo) {
		return EOStgType.localInstanceIn(editingContext, eo);
	}


	/* Finders */

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgType> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier) null);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgType> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgType> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		fetchSpec.setIsDeep(isDeep);
		fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgType> eoObjects = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgType>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Cree une fetchSpec sur l'entité avec les parametres.
	 * 
	 * @param qualifier
	 * @param sortOrderings
	 * @param distinct
	 * @return
	 */
	public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return fetchSpec;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOStgType fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStgType fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray<EOStgType> eoObjects = fetchAll(editingContext, qualifier, null);
		EOStgType eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else if (count == 1) {
			eoObject = (EOStgType) eoObjects.objectAtIndex(0);
		} else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	
	public static EOStgType fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOStgType fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EOStgType> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOStgType eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else {
			eoObject = (EOStgType) eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOStgType fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOStgType eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOStgType ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	public static EOStgType fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

}
