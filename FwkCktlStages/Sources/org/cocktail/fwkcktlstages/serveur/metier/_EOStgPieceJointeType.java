/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStgPieceJointeType.java instead.
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.util.Enumeration;
import java.util.NoSuchElementException;
// import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStgPieceJointeType extends  AFwkCktlStagesRecord {
	//	private static Logger logger = Logger.getLogger(_EOStgPieceJointeType.class);

	public static final String ENTITY_NAME = "FwkCktlStages_StgPieceJointeType";
	public static final String ENTITY_TABLE_NAME = "STAGES.STG_PIECE_JOINTE_TYPE";

	// Attribute Keys
	public static final ERXKey<String> SPJT_LC = new ERXKey<String>("spjtLc");
	public static final ERXKey<String> SPJT_LL = new ERXKey<String>("spjtLl");

	// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> STG_PIECE_JOINTE_CONFIGS = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig>("stgPieceJointeConfigs");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> STG_PIECE_JOINTE_REPARTS = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart>("stgPieceJointeReparts");

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "spjtId";

	public static final String SPJT_LC_KEY = "spjtLc";
	public static final String SPJT_LL_KEY = "spjtLl";

	// Attributs non visibles
	public static final String SPJT_ID_KEY = "spjtId";

	// Colonnes dans la base de donnees
	public static final String SPJT_LC_COLKEY = "SPJT_LC";
	public static final String SPJT_LL_COLKEY = "SPJT_LL";

	public static final String SPJT_ID_COLKEY = "SPJT_ID";

	// Relationships
	public static final String STG_PIECE_JOINTE_CONFIGS_KEY = "stgPieceJointeConfigs";
	public static final String STG_PIECE_JOINTE_REPARTS_KEY = "stgPieceJointeReparts";


	// Accessors methods
	public String spjtLc() {
		return (String) storedValueForKey(SPJT_LC_KEY);
	}

	public void setSpjtLc(String value) {
		takeStoredValueForKey(value, SPJT_LC_KEY);
	}

	public String spjtLl() {
		return (String) storedValueForKey(SPJT_LL_KEY);
	}

	public void setSpjtLl(String value) {
		takeStoredValueForKey(value, SPJT_LL_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> stgPieceJointeConfigs() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig>) storedValueForKey(STG_PIECE_JOINTE_CONFIGS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> stgPieceJointeConfigs(EOQualifier qualifier) {
		return stgPieceJointeConfigs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> stgPieceJointeConfigs(EOQualifier qualifier, boolean fetch) {
		return stgPieceJointeConfigs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> stgPieceJointeConfigs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig.STG_PIECE_JOINTE_TYPE_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = stgPieceJointeConfigs();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToStgPieceJointeConfigsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig object) {
		addObjectToBothSidesOfRelationshipWithKey(object, STG_PIECE_JOINTE_CONFIGS_KEY);
	}

	public void removeFromStgPieceJointeConfigsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_PIECE_JOINTE_CONFIGS_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig createStgPieceJointeConfigsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgPieceJointeConfig");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, STG_PIECE_JOINTE_CONFIGS_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig) eo;
	}

	public void deleteStgPieceJointeConfigsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_PIECE_JOINTE_CONFIGS_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllStgPieceJointeConfigsRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeConfig> objects = stgPieceJointeConfigs().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteStgPieceJointeConfigsRelationship(objects.nextElement());
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> stgPieceJointeReparts() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart>) storedValueForKey(STG_PIECE_JOINTE_REPARTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> stgPieceJointeReparts(EOQualifier qualifier) {
		return stgPieceJointeReparts(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> stgPieceJointeReparts(EOQualifier qualifier, boolean fetch) {
		return stgPieceJointeReparts(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> stgPieceJointeReparts(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart.STG_PIECE_JOINTE_TYPE_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = stgPieceJointeReparts();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToStgPieceJointeRepartsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart object) {
		addObjectToBothSidesOfRelationshipWithKey(object, STG_PIECE_JOINTE_REPARTS_KEY);
	}

	public void removeFromStgPieceJointeRepartsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_PIECE_JOINTE_REPARTS_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart createStgPieceJointeRepartsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgPieceJointeRepart");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, STG_PIECE_JOINTE_REPARTS_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart) eo;
	}

	public void deleteStgPieceJointeRepartsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_PIECE_JOINTE_REPARTS_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllStgPieceJointeRepartsRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> objects = stgPieceJointeReparts().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteStgPieceJointeRepartsRelationship(objects.nextElement());
		}
	}


	/**
	 * Créer une instance de EOStgPieceJointeType avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOStgPieceJointeType createEOStgPieceJointeType(EOEditingContext editingContext, String spjtLc
, String spjtLl
			) {
		EOStgPieceJointeType eo = (EOStgPieceJointeType) createAndInsertInstance(editingContext, _EOStgPieceJointeType.ENTITY_NAME);
		eo.setSpjtLc(spjtLc);
		eo.setSpjtLl(spjtLl);
		return eo;
	}


	public EOStgPieceJointeType localInstanceIn(EOEditingContext editingContext) {
		return (EOStgPieceJointeType) localInstanceOfObject(editingContext, this);
	}
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOStgPieceJointeType creerInstance(EOEditingContext editingContext) {
		EOStgPieceJointeType object = (EOStgPieceJointeType) createAndInsertInstance(editingContext, _EOStgPieceJointeType.ENTITY_NAME);
		return object;
	}


	public static EOStgPieceJointeType localInstanceIn(EOEditingContext editingContext, EOStgPieceJointeType eo) {
		EOStgPieceJointeType localInstance = (eo == null) ? null : (EOStgPieceJointeType) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOStgPieceJointeType#localInstanceIn a la place.
	 */
	public static EOStgPieceJointeType localInstanceOf(EOEditingContext editingContext, EOStgPieceJointeType eo) {
		return EOStgPieceJointeType.localInstanceIn(editingContext, eo);
	}


	/* Finders */

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier) null);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		fetchSpec.setIsDeep(isDeep);
		fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType> eoObjects = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Cree une fetchSpec sur l'entité avec les parametres.
	 * 
	 * @param qualifier
	 * @param sortOrderings
	 * @param distinct
	 * @return
	 */
	public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return fetchSpec;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOStgPieceJointeType fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStgPieceJointeType fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray<EOStgPieceJointeType> eoObjects = fetchAll(editingContext, qualifier, null);
		EOStgPieceJointeType eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else if (count == 1) {
			eoObject = (EOStgPieceJointeType) eoObjects.objectAtIndex(0);
		} else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	
	public static EOStgPieceJointeType fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOStgPieceJointeType fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EOStgPieceJointeType> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOStgPieceJointeType eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else {
			eoObject = (EOStgPieceJointeType) eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOStgPieceJointeType fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOStgPieceJointeType eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOStgPieceJointeType ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	public static EOStgPieceJointeType fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

}
