/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStgAdresse.java instead.
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.util.Enumeration;
import java.util.NoSuchElementException;
// import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStgAdresse extends  AFwkCktlStagesRecord {
	//	private static Logger logger = Logger.getLogger(_EOStgAdresse.class);

	public static final String ENTITY_NAME = "FwkCktlStages_StgAdresse";
	public static final String ENTITY_TABLE_NAME = "STAGES.STG_ADRESSE";

	// Attribute Keys
	public static final ERXKey<String> SADR_COMMENTAIRE = new ERXKey<String>("sadrCommentaire");
	public static final ERXKey<String> SADR_TEM_DEPLACEMENT = new ERXKey<String>("sadrTemDeplacement");

	// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_FWKPERS__ADRESSE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toFwkpers_Adresse");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> TO_STAGE = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>("toStage");

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "sadrId";

	public static final String SADR_COMMENTAIRE_KEY = "sadrCommentaire";
	public static final String SADR_TEM_DEPLACEMENT_KEY = "sadrTemDeplacement";

	// Attributs non visibles
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String SADR_ID_KEY = "sadrId";
	public static final String STG_ID_KEY = "stgId";

	// Colonnes dans la base de donnees
	public static final String SADR_COMMENTAIRE_COLKEY = "SADR_COMMENTAIRE";
	public static final String SADR_TEM_DEPLACEMENT_COLKEY = "SADR_TEM_DEPLACEMENT";

	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String SADR_ID_COLKEY = "SADR_ID";
	public static final String STG_ID_COLKEY = "STG_ID";

	// Relationships
	public static final String TO_FWKPERS__ADRESSE_KEY = "toFwkpers_Adresse";
	public static final String TO_STAGE_KEY = "toStage";


	// Accessors methods
	public String sadrCommentaire() {
		return (String) storedValueForKey(SADR_COMMENTAIRE_KEY);
	}

	public void setSadrCommentaire(String value) {
		takeStoredValueForKey(value, SADR_COMMENTAIRE_KEY);
	}

	public String sadrTemDeplacement() {
		return (String) storedValueForKey(SADR_TEM_DEPLACEMENT_KEY);
	}

	public void setSadrTemDeplacement(String value) {
		takeStoredValueForKey(value, SADR_TEM_DEPLACEMENT_KEY);
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toFwkpers_Adresse() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse) storedValueForKey(TO_FWKPERS__ADRESSE_KEY);
	}

	public void setToFwkpers_AdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toFwkpers_Adresse();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FWKPERS__ADRESSE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_FWKPERS__ADRESSE_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgStage toStage() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgStage) storedValueForKey(TO_STAGE_KEY);
	}

	public void setToStageRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgStage oldValue = toStage();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STAGE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_STAGE_KEY);
		}
	}


	/**
	 * Créer une instance de EOStgAdresse avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOStgAdresse createEOStgAdresse(EOEditingContext editingContext, String sadrTemDeplacement
, org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toFwkpers_Adresse, org.cocktail.fwkcktlstages.serveur.metier.EOStgStage toStage			) {
		EOStgAdresse eo = (EOStgAdresse) createAndInsertInstance(editingContext, _EOStgAdresse.ENTITY_NAME);
		eo.setSadrTemDeplacement(sadrTemDeplacement);
		eo.setToFwkpers_AdresseRelationship(toFwkpers_Adresse);
		eo.setToStageRelationship(toStage);
		return eo;
	}


	public EOStgAdresse localInstanceIn(EOEditingContext editingContext) {
		return (EOStgAdresse) localInstanceOfObject(editingContext, this);
	}
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOStgAdresse creerInstance(EOEditingContext editingContext) {
		EOStgAdresse object = (EOStgAdresse) createAndInsertInstance(editingContext, _EOStgAdresse.ENTITY_NAME);
		return object;
	}


	public static EOStgAdresse localInstanceIn(EOEditingContext editingContext, EOStgAdresse eo) {
		EOStgAdresse localInstance = (eo == null) ? null : (EOStgAdresse) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOStgAdresse#localInstanceIn a la place.
	 */
	public static EOStgAdresse localInstanceOf(EOEditingContext editingContext, EOStgAdresse eo) {
		return EOStgAdresse.localInstanceIn(editingContext, eo);
	}


	/* Finders */

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier) null);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		fetchSpec.setIsDeep(isDeep);
		fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse> eoObjects = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Cree une fetchSpec sur l'entité avec les parametres.
	 * 
	 * @param qualifier
	 * @param sortOrderings
	 * @param distinct
	 * @return
	 */
	public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return fetchSpec;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOStgAdresse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStgAdresse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray<EOStgAdresse> eoObjects = fetchAll(editingContext, qualifier, null);
		EOStgAdresse eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else if (count == 1) {
			eoObject = (EOStgAdresse) eoObjects.objectAtIndex(0);
		} else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	
	public static EOStgAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOStgAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EOStgAdresse> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOStgAdresse eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else {
			eoObject = (EOStgAdresse) eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOStgAdresse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOStgAdresse eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOStgAdresse ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	public static EOStgAdresse fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

}
