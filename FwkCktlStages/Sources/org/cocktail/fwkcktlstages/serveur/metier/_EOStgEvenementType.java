/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStgEvenementType.java instead.
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.util.Enumeration;
import java.util.NoSuchElementException;
// import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStgEvenementType extends  AFwkCktlStagesRecord {
	//	private static Logger logger = Logger.getLogger(_EOStgEvenementType.class);

	public static final String ENTITY_NAME = "FwkCktlStages_StgEvenementType";
	public static final String ENTITY_TABLE_NAME = "STAGES.STG_EVENEMENT_TYPE";

	// Attribute Keys
	public static final ERXKey<String> SEVT_LC = new ERXKey<String>("sevtLc");
	public static final ERXKey<String> SEVT_LL = new ERXKey<String>("sevtLl");

	// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> STG_EVENEMENTS = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement>("stgEvenements");

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "sevtId";

	public static final String SEVT_LC_KEY = "sevtLc";
	public static final String SEVT_LL_KEY = "sevtLl";

	// Attributs non visibles
	public static final String SEVT_ID_KEY = "sevtId";

	// Colonnes dans la base de donnees
	public static final String SEVT_LC_COLKEY = "SEVT_LC";
	public static final String SEVT_LL_COLKEY = "SEVT_LL";

	public static final String SEVT_ID_COLKEY = "SEVT_ID";

	// Relationships
	public static final String STG_EVENEMENTS_KEY = "stgEvenements";


	// Accessors methods
	public String sevtLc() {
		return (String) storedValueForKey(SEVT_LC_KEY);
	}

	public void setSevtLc(String value) {
		takeStoredValueForKey(value, SEVT_LC_KEY);
	}

	public String sevtLl() {
		return (String) storedValueForKey(SEVT_LL_KEY);
	}

	public void setSevtLl(String value) {
		takeStoredValueForKey(value, SEVT_LL_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> stgEvenements() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement>) storedValueForKey(STG_EVENEMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> stgEvenements(EOQualifier qualifier) {
		return stgEvenements(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> stgEvenements(EOQualifier qualifier, boolean fetch) {
		return stgEvenements(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> stgEvenements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement.STG_EVENEMENT_TYPE_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = stgEvenements();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToStgEvenementsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement object) {
		addObjectToBothSidesOfRelationshipWithKey(object, STG_EVENEMENTS_KEY);
	}

	public void removeFromStgEvenementsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_EVENEMENTS_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement createStgEvenementsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgEvenement");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, STG_EVENEMENTS_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement) eo;
	}

	public void deleteStgEvenementsRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_EVENEMENTS_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllStgEvenementsRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> objects = stgEvenements().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteStgEvenementsRelationship(objects.nextElement());
		}
	}


	/**
	 * Créer une instance de EOStgEvenementType avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOStgEvenementType createEOStgEvenementType(EOEditingContext editingContext			) {
		EOStgEvenementType eo = (EOStgEvenementType) createAndInsertInstance(editingContext, _EOStgEvenementType.ENTITY_NAME);
		return eo;
	}


	public EOStgEvenementType localInstanceIn(EOEditingContext editingContext) {
		return (EOStgEvenementType) localInstanceOfObject(editingContext, this);
	}
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOStgEvenementType creerInstance(EOEditingContext editingContext) {
		EOStgEvenementType object = (EOStgEvenementType) createAndInsertInstance(editingContext, _EOStgEvenementType.ENTITY_NAME);
		return object;
	}


	public static EOStgEvenementType localInstanceIn(EOEditingContext editingContext, EOStgEvenementType eo) {
		EOStgEvenementType localInstance = (eo == null) ? null : (EOStgEvenementType) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOStgEvenementType#localInstanceIn a la place.
	 */
	public static EOStgEvenementType localInstanceOf(EOEditingContext editingContext, EOStgEvenementType eo) {
		return EOStgEvenementType.localInstanceIn(editingContext, eo);
	}


	/* Finders */

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier) null);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		fetchSpec.setIsDeep(isDeep);
		fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType> eoObjects = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Cree une fetchSpec sur l'entité avec les parametres.
	 * 
	 * @param qualifier
	 * @param sortOrderings
	 * @param distinct
	 * @return
	 */
	public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return fetchSpec;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOStgEvenementType fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStgEvenementType fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray<EOStgEvenementType> eoObjects = fetchAll(editingContext, qualifier, null);
		EOStgEvenementType eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else if (count == 1) {
			eoObject = (EOStgEvenementType) eoObjects.objectAtIndex(0);
		} else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	
	public static EOStgEvenementType fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOStgEvenementType fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EOStgEvenementType> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOStgEvenementType eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else {
			eoObject = (EOStgEvenementType) eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOStgEvenementType fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOStgEvenementType eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOStgEvenementType ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	public static EOStgEvenementType fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

}
