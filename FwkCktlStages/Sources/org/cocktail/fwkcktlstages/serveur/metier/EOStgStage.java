/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.cocktail.fwkcktlged.serveur.metier.service.DocumentService;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlstages.serveur.exception.DroitManquantException;
import org.cocktail.fwkcktlstages.serveur.exception.FwkCktlStagesException;
import org.cocktail.fwkcktlstages.serveur.factory.FactoryEvenement;
import org.cocktail.fwkcktlstages.serveur.factory.FactoryStgPieceJointe;
import org.cocktail.fwkcktlstages.serveur.finder.FactoryFinderScolarite;
import org.cocktail.fwkcktlstages.serveur.finder.FinderDevise;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgAcces;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgAnnee;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgConfig;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgEtat;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgPieceJointe;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgPieceJointeType;
import org.cocktail.fwkcktlstages.serveur.finder.FinderElementPedagogique;
import org.cocktail.fwkcktlstages.serveur.metier.beans.AvenantBean;
import org.cocktail.fwkcktlstages.serveur.metier.beans.ConventionBean;
import org.cocktail.fwkcktlstages.serveur.metier.controles.ControleStageAbsences;
import org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCached;
import org.cocktail.fwkcktlstages.serveur.metier.helper.EOStageHelper;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlGedBus;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;
import org.cocktail.reporting.server.xdocreport.ODTReporter;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * @author Julien BLANDINEAU <jblandin at univ-lr.fr>
 */
public class EOStgStage extends _EOStgStage {
	public static final String STG_ID_INT_KEY = "stgIdInt";
	public static final String STG_SUJET_KEY_COURT = "stgSujetCourt";

	public static final ERXKey<EOIndividu> TO_TUTEUR = new ERXKey<EOIndividu>("toTuteur");
	public static final ERXKey<String> TO_EC_CODE = new ERXKey<String>("toEcCode");

	public static final int SUJET_COURT_MAX_LENGTH = 40;

	private EOIndividu encadrant;
	private EC ec;
	private FinderElementPedagogique finderElementPedagogique;

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException Si l'objet n'est pas valide
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException Si l'objet n'est pas valide
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException Si l'objet n'est pas valide
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException Si l'objet n'est pas valide
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();

		checkContraintesLongueursMax();
		checkContraintesObligatoires();

		if (this.toOrganismeAccueil() == null) {
			throw new ValidationException("La structure d'accueil doit être renseignée");
		}

		if (this.toEncadrant() == null) {
			throw new ValidationException("L'encadrant au sein de la structure d'accueil doit être renseigné");
		}
		// checkRespCivile();
		checkDates();
		checkDuree();
		checkGratification();
		majDateConvention();
		checkAbsences();
	}

	private void majDateConvention() {
		if (changesFromCommittedSnapshot() != null
		    && (changesFromCommittedSnapshot().containsKey(STG_D_DEBUT_KEY) || changesFromCommittedSnapshot().containsKey(STG_D_FIN_KEY))) {
			toContrat().setDateDebut(stgDDebut());
			toContrat().setDateFin(stgDFin());
		}
	}

	private void checkAbsences() {
		for (EOStgAbsence abs : toAbsences()) {
			ControleStageAbsences.getInstance().controleDates(abs);
		}
	}

	@Override
	public Map displayNames() {
		Map<String, String> names = super.displayNames();
		names.put(STG_D_DEBUT_KEY, "Date de début");
		names.put(STG_D_FIN_KEY, "Date de fin");
		names.put(STG_DUREE_HEBDO_KEY, "Durée hebdomadaire");
		names.put(STG_DUREE_TOTALE_VAL_KEY, "Durée totale");
		names.put(STG_SUJET_KEY, "Sujet du stage");
		names.put(STG_ACTIVITES_KEY, "Activités pendant le stage");
		names.put(STG_REF_RESP_CIVILE_KEY, "Référence de la responsabilité civile");
		return names;
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException Si l'objet n'est pas valide
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		super.validateBeforeTransactionSave();
	}

	@Override
	public void awakeFromInsertion(EOEditingContext editingContext) {
		super.awakeFromInsertion(editingContext);

		EOGlobalID gid = editingContext.globalIDForObject(this);
		if (gid.isTemporary()) {
			initFields();
			initRelationships(editingContext);
		}
	}

	private void initRelationships(EOEditingContext editingContext) {
		if (toAnnee() == null) {
			setToAnneeRelationship(FinderStgAnnee.getStgAnneeCourante(editingContext));
		}
		if (toAcces() == null) {
			setToAccesRelationship(FinderStgAcces.getDefault(editingContext));
		}
		if (toDeviseGratification() == null) {
			setToDeviseGratificationRelationship(FinderDevise.getDefaultDevise(editingContext));
		}
	}

	private void initFields() {
		if (stgDCreation() == null) {
			setStgDCreation(now());
		}
		if (stgDModification() == null) {
			setStgDModification(now());
		}
		if (stgDureeTotaleUnite() == null) {
			setStgDureeTotaleUnite(EOStageHelper.UNITE_JOUR);
		}
		if (stgGratMontant() == null) {
			setStgGratMontant(0.0);
		}
		if (stgGratMensuelle() == null) {
			setStgGratMensuelle(OUI);
		}
		if (convSpecifique() == null) {
			setConvSpecifique(NON);
		}
		if (stgRefRespCivile() == null) {
			if (FinderStgConfig.getInstance().getRespCivileDefaut().equals("nd")) {
				setStgRefRespCivile("");
			} else {
				setStgRefRespCivile(FinderStgConfig.getInstance().getRespCivileDefaut());
			}
		}
	}

	// -- -- -- -- -- -- -- -- -- AFFICHAGE -- -- -- -- -- -- -- -- --

	/**
	 * @return L'affichage de l'unité de la durée du stage (jour, semaine, mois)
	 */
	public String stgDureeTotaleUniteAffichage() {
		if (super.stgDureeTotaleUnite() == null) {
			return "";
		}
		String strS = "";

		double dureeDouble = super.stgDureeTotaleVal().doubleValue();

		if (dureeDouble > 1) {
			strS = "s";
		}

		if (super.stgDureeTotaleUnite().equals(EOStageHelper.UNITE_JOUR)) {
			return "jour" + strS;
		} else if (super.stgDureeTotaleUnite().equals(EOStageHelper.UNITE_SEMAINE)) {
			return "semaine" + strS;
		} else if (super.stgDureeTotaleUnite().equals(EOStageHelper.UNITE_MOIS)) {
			return "mois";
		}
		return super.stgDureeTotaleUnite();
	}

	/**
	 * @return Les activités du stage, avec les sauts de lignes corrects
	 */
	public String stgActivitesToHtml() {
		return StringCtrl.replace(super.stgActivites(), "\n", "<br />");
	}

	/**
	 * @return Les avantages en nature et/ou espèce, avec les sauts de lignes corrects
	 */
	public String stgGratAvantagesToHtml() {
		return StringCtrl.replace(super.stgGratAvantages(), "\n", "<br />");
	}

	// -- -- -- -- -- -- -- -- -- ACCESSEURS -- -- -- -- -- -- -- -- --

	/**
	 * @return Le tuteur
	 */
	public EOIndividu toTuteur() {
		if (this.persIdTuteur() != null) {
			return EOIndividu.individuWithPersId(this.editingContext(), this.persIdTuteur());
		} else {
			return null;
		}
	}

	/**
	 * @param value Le tuteur
	 */
	public void setToTuteur(EOIndividu value) {
		if (value != null) {
			this.setPersIdTuteur(value.persId());
		} else {
			this.setPersIdTuteur(null);
		}
	}

	/**
	 * @return L'encadrant
	 */
	public EOIndividu toEncadrant() {
		if (encadrant != null) {
			return encadrant;
		}
		return EOIndividu.individuWithPersId(editingContext(), persIdEncadrant());
	}

	/**
	 * @param value L'encadrant
	 */
	public void setToEncadrant(EOIndividu value) {
		EOIndividu oldValue = toEncadrant();
		if (oldValue != null) {
			this.setPersIdEncadrant(null);
			encadrant = null;
		}
		if (value != null) {
			this.setPersIdEncadrant(value.persId());
			encadrant = value;
		}
	}

	/**
	 * @return L'ID du stage (String)
	 */
	public String stgId() {
		return this.primaryKey();
	}

	/**
	 * @return L'ID du stage (Interger)
	 */
	public Integer stgIdInt() {
		return Integer.valueOf(this.primaryKey());
	}

	protected void setEtatSuivant(String lcEtatSuivant) {
		EOStgEtat etatSuivant = FinderStgEtat.getInstance().getStgEtat(editingContext(), lcEtatSuivant);
		this.setToEtatRelationship(etatSuivant);
	}

	/**
	 * @return Le <code>PERS_ID</code> de l'étudiant
	 */
	public Integer persIdEtudiant() {
		return this.toEtudiant().toIndividu().persId();
	}

	/**
	 * @return Le {@link IEtudiant}
	 */
	public IEtudiant toIEtudiant() {
		return toEtudiant();
	}

	public boolean isGratificationMensuelle() {
		return this.stgGratMensuelle() != null && this.stgGratMensuelle().equals(OUI);
	}

	/**
	 * @param isGratificationMensuelle <code>TRUE</code> si la gratification est mensuelle
	 */
	public void setGratificationMensuelle(boolean isGratificationMensuelle) {
		if (isGratificationMensuelle) {
			this.setStgGratMensuelle(OUI);
		} else {
			this.setStgGratMensuelle(NON);
		}
	}

	public boolean isGratificationObligatoire() {
		return this.stgGratObligatoire() != null && this.stgGratObligatoire().equals(OUI);
	}

	/**
	 * @return La durée du stage, entre la date de début et la date de fin
	 */
	public String getDureeCalculee() {
		if (stgDDebut() != null && stgDFin() != null && !stgDDebut().after(stgDFin())) {
			NSNumberFormatter formatterDbl = new NSNumberFormatter("#0.##");
			double dureeSemainesDbl = EOStageHelper.getInstance().getDureeCalculeeSemaines(stgDDebut(), stgDFin());
			int dureeJours = EOStageHelper.getInstance().getDureeCalculeeJours(stgDDebut(), stgDFin());
			return dureeJours + " jour(s), soit " + formatterDbl.format(dureeSemainesDbl) + " semaine(s)";
		}
		return null;
	}

	/**
	 * 
	 * @return la durée du stage calculée en jours
	 */
	public Integer getDureeCalculeeJours() {
		if (stgDDebut() != null && stgDFin() != null && !stgDDebut().after(stgDFin())) {
			return EOStageHelper.getInstance().getDureeCalculeeJours(stgDDebut(), stgDFin());
		}
		return null;
	}

	public NSTimestamp getDateDebutStage() {
		return stgDDebut();
	}

	public String getStgRefRespCivile() {
		return stgRefRespCivile();
	}

	public NSTimestamp getDateFinStage() {
		return stgDFin();
	}

	public boolean isConventionSpecifique() {
		return convSpecifique().equals(OUI);
	}

	/**
	 * @param value <code>TRUE</code> si la convention est spécifique
	 */
	public void setConventionSpecifique(boolean value) {
		if (value) {
			setConvSpecifique(OUI);
		} else {
			setConvSpecifique(NON);
		}
	}

	@Override
	public void setStgDDebut(NSTimestamp value) {
		super.setStgDDebut(value);
		updateStgDureeTotaleVal();
		toContrat().setDateDebut(value);
	}

	private void updateStgDureeTotaleVal() {
		if (getDureeCalculeeJours() != null) {
			setStgDureeTotaleVal(new BigDecimal(getDureeCalculeeJours()));
			setStgDureeTotaleUnite(EOStageHelper.UNITE_JOUR);
		}
	}

	@Override
	public void setStgDFin(NSTimestamp value) {
		super.setStgDFin(value);
		updateStgDureeTotaleVal();
		toContrat().setDateFin(value);
	}

	/**
	 * @return Le {@link EOPersonneTelephone}
	 */
	public EOPersonneTelephone getPersonneTelephone() {
		if (!StringCtrl.isEmpty(noTelephone())) {
			EOQualifier qualifier = ERXQ.and(ERXQ.equals(EOPersonneTelephone.NO_TELEPHONE_KEY, noTelephone()),
			    ERXQ.equals(EOPersonneTelephone.TYPE_NO_KEY, typeNo()), ERXQ.equals(EOPersonneTelephone.TYPE_TEL_KEY, typeTel()));
			NSArray<EOPersonneTelephone> tels = toEtudiant().toIndividu().toPersonneTelephones(qualifier);
			if (tels.count() > 0) {
				return tels.objectAtIndex(0);
			}
		}
		return null;
	}

	/**
	 * @return L'affichage du téléphone
	 */
	public String getNumeroTelephoneAffichage() {
		EOPersonneTelephone tel = getPersonneTelephone();
		if (tel != null) {
			return tel.getTelephoneFormateAvecIndicatif();
		}
		return null;
	}

	// -- -- -- -- -- -- -- -- -- ACTIONS -- -- -- -- -- -- -- -- --

	/**
	 * La demande est envoyée pour validation pédagogique, si l'utilisateur a les droits pour le faire
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionEnvoyerDemandeValidationPedagogique(StagesDroitsCached sdhc) throws MessagingException {
		if (!sdhc.hasDroitsDemandeValidationPedagogique(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour demander la validation pédagogique de cette demande.", false);
		}

		// Changement d'état
		setEtatSuivant(EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventDemandeValidationPedagogique(this, sdhc.getPersId());
		// TODO envoi de mail
	}

	/**
	 * L'utilisateur accepte de valider pédagogiquement la demande, s'il a les droits
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionAccepterValidationPedagogique(StagesDroitsCached sdhc) throws MessagingException {
		if (!sdhc.hasDroitsValidationPedagogique(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour valider ou refuser pédagogiquement de cette demande.", false);
		}
		// FIXME Gestion de la validation du tuteur ou non
		// Changement d'état
		String lcEtatSuivant;
		if (FinderStgConfig.getInstance().isTuteurValidation()) {
			if (toTuteur() != null) {
				lcEtatSuivant = EOStgEtat.LC_ATTENTE_VALIDATION_TUTEUR;
			} else {
				lcEtatSuivant = EOStgEtat.LC_ATTENTE_DESIGNATION_TUTEUR;
			}
		} else {
			lcEtatSuivant = EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE;
		}
		setEtatSuivant(lcEtatSuivant);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventAcceptationValidationPedagogique(this, sdhc.getPersId(), lcEtatSuivant);
	}

	/**
	 * L'utilisateur refuse de valider la demande
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @param raison La raison du refus
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionRefuserValidationPedagogique(StagesDroitsCached sdhc, String raison) throws MessagingException {
		if (!sdhc.hasDroitsValidationPedagogique(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour valider ou refuser pédagogiquement de cette demande.", false);
		}
		if (StringCtrl.isEmpty(raison)) {
			throw new ValidationException("Vous devez renseigner une raison pour refuser pédagogiquement le stage.");
		}

		// Changement d'état
		setEtatSuivant(EOStgEtat.LC_INITIAL);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventRefusValidationPedagogique(this, sdhc.getPersId(), raison);
	}

	/**
	 * L'Utilisateur accepte de tutorer le stage
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionAccepterValidationTutorat(StagesDroitsCached sdhc) throws MessagingException {
		if (!sdhc.hasDroitsValidationTuteur(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour valider ou refuser d'être le tuteur sur cette convention.", false);
		}

		// Changement d'état
		setEtatSuivant(EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventAcceptationValidationTuteur(this, sdhc.getPersId());
	}

	/**
	 * L'Utilisateur annule la validation en tant que tuteur
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 */
	public void actionAnnulerValidationTutorat(StagesDroitsCached sdhc) {
		if (!sdhc.hasDroitsAnnulationValidationTuteur(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour annuler la validation de tutorat sur cette convention.", false);
		}

		// Changement d'état
		setEtatSuivant(EOStgEtat.LC_ATTENTE_VALIDATION_TUTEUR);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventAnnulationValidationTuteur(this, sdhc.getPersId());
	}

	/**
	 * L'Utilisateur refuse de tutorer le stage
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionRefusValidationTutorat(StagesDroitsCached sdhc) throws MessagingException {
		if (!sdhc.hasDroitsValidationTuteur(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour valider ou refuser d'être le tuteur sur cette convention.", false);
		}

		setToTuteur(null);
		// Changement d'état
		setEtatSuivant(EOStgEtat.LC_ATTENTE_DESIGNATION_TUTEUR);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventRefusValidationTuteur(this, sdhc.getPersId());
	}

	/**
	 * L'utilisateur modifie le tuteur
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 */
	public void actionModifierTuteur(StagesDroitsCached sdhc) {
		if (!sdhc.hasDroitsModificationTuteur(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour modifier le tuteur de cette convention.", false);
		}

		// Changement d'état
		String lcEtatPrecedent = toEtat().setLc();
		String lcEtatSuivant;
		if (FinderStgConfig.getInstance().isTuteurValidation() && isEtatCourantApresEtatDonne(EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE)) {
			if (toTuteur() == null) {
				lcEtatSuivant = EOStgEtat.LC_ATTENTE_DESIGNATION_TUTEUR;
			} else {
				lcEtatSuivant = EOStgEtat.LC_ATTENTE_VALIDATION_TUTEUR;
			}
		} else {
			lcEtatSuivant = lcEtatPrecedent;
		}
		setEtatSuivant(lcEtatSuivant);

		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventModificationTuteur(this, sdhc.getPersId(), lcEtatPrecedent, lcEtatSuivant);
	}

	/**
	 * L'utilisateur accepte de valider administrativement la demande
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionAccepterValidationAdministrative(StagesDroitsCached sdhc) throws MessagingException {
		if (!sdhc.hasDroitsValidationAdministrative(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour valider ou refuser administrativement cette demande", false);
		}
		if (this.toTuteur() == null) {
			throw new ValidationException("Le tuteur doit être renseigné avant de valider");
		}
		// Changement d'état
		setEtatSuivant(EOStgEtat.LC_ATTENTE_EDITION);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventAcceptationValidationAdministrative(this, sdhc.getPersId());
	}

	/**
	 * L'utilisateur annule la validation administrative de la demande
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 */
	public void actionAnnulerValidationAdministrative(StagesDroitsCached sdhc) {
		if (!sdhc.hasDroitsDevalidationAdministrative(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour annuler la validation administrative de cette demande", false);
		}
		// Changement d'état
		setEtatSuivant(EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventAnnulationValidationAdministrative(this, sdhc.getPersId());
	}

	/**
	 * L'utilisateur annule la validation pédagogique de la demande
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 */
	public void actionAnnulerValidationPedagogique(StagesDroitsCached sdhc) {
		if (!sdhc.hasDroitsDevalidationPedagogique(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour annuler la validation pédagogique de cette demande", false);
		}
		String lcEtatPrecedent = toEtat().setLc();
		// Changement d'état
		setEtatSuivant(EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventAnnulationValidationPedagogique(this, sdhc.getPersId(), lcEtatPrecedent);
	}

	/**
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @param filePath Le chemin vers le modèle
	 * @param reporter Un {@link ODTReporter}
	 * @param gedBus Un {@link CktlGedBus}
	 * @throws Throwable Si droits manquants ou erreur lors de la génération
	 */
	public void actionEditionConvention(StagesDroitsCached sdhc, String filePath, ODTReporter reporter, DocumentService documentService) throws Throwable {
		if (!sdhc.hasDroitsEditionConvention(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour éditer cette convention", false);
		}
		NSData data = new NSData();
		if (filePath != null) {
			data = generateConvention(sdhc, filePath, reporter);
		}
		EOStgPieceJointeType pjt = FinderStgPieceJointeType.getTypeConvention(editingContext());
		String titre = "Convention " + toEtudiant().toIndividu().getNomPrenomAffichage();
		String nomfichier = titre + ".pdf";
		EOStgPieceJointeRepart spjr = FactoryStgPieceJointe.getInstance().ajouterPieceJointe(this, sdhc.getPersId(), documentService, data.bytes(), titre,
		    nomfichier, CktlParamManager.getApplication().config().stringForKey("GED_CONVENTIONS"), pjt);
		if (filePath != null) {
			spjr.setSpjrModele(FileCtrl.getFileName(filePath, true));
		}
		// Changement d'état
		setEtatSuivant(EOStgEtat.LC_ATTENTE_SIGNATURES);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventEditionConvention(this, sdhc.getPersId());
	}

	/**
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @param filePath Le chemin vers le modèle
	 * @param reporter Un {@link ODTReporter}
	 * @return Le fichier généré
	 * @throws Throwable Si droits manquants ou erreur lors de la génération
	 */
	public NSData actionEditionConventionALaDemande(StagesDroitsCached sdhc, String filePath, ODTReporter reporter) throws Throwable {
		if (!sdhc.hasDroitsConsultationConvention(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour éditer cette convention", false);
		}
		return generateConvention(sdhc, filePath, reporter);
	}

	/**
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @param filePath Le chemin vers le modèle
	 * @param reporter Un {@link ODTReporter}
	 * @return Le fichier généré
	 * @throws Throwable Si erreurs lors de la génération
	 */
	private NSData generateConvention(StagesDroitsCached sdhc, String filePath, ODTReporter reporter) throws Throwable {
		// Liste des beans
		Map<String, Object> params = new HashMap<String, Object>();
		ConventionBean convBean = new ConventionBean(this);
		convBean.initFieldsMap(params);
		// Liste des champs qui peuvent être iterables dans un tableau dans le
		// document
		List<String> fieldsAsList = new ArrayList<String>();
		ConventionBean.initFieldsAsList(fieldsAsList);
		return reporter.printNow("Convention", filePath, params, fieldsAsList, 0, null);
	}

	/**
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @param filePath Le chemin vers le modèle
	 * @param reporter Un {@link ODTReporter}
	 * @return Le fichier généré
	 * @throws Throwable Si erreurs lors de la génération
	 */
	private NSData generateAvenant(EOStgAvenant avenant, StagesDroitsCached sdhc, String filePath, ODTReporter reporter) throws Throwable {
		// Liste des beans
		Map<String, Object> params = new HashMap<String, Object>();
		AvenantBean avBean = new AvenantBean(avenant);
		avBean.initFieldsMap(params);
		return reporter.printNow("Avenant", filePath, params, null, 0, null);
	}

	/**
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @param gedBus Un {@link CktlGedBus}
	 * @throws Exception Si droit manquant ou erreur lors de la suppression
	 */
	public void actionSupprimerConvention(StagesDroitsCached sdhc) throws Exception {
		if (!sdhc.hasDroitsSuppressionConvention(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour supprimer cette convention.", false);
		}
		// TODO Supprimer le fichier
		EOStgPieceJointeType pjt = FinderStgPieceJointeType.getTypeConvention(editingContext());
		FactoryStgPieceJointe.getInstance().supprimerPieceJointe(this, sdhc.getPersId(), pjt);

		setEtatSuivant(EOStgEtat.LC_ATTENTE_EDITION);

		FactoryEvenement.getInstance().eventSuppressionConvention(this, sdhc.getPersId());

	}

	/**
	 * Action à effectuer lors de la réception des conventions signées. Cela verrouille la convention.
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionReceptionSignatures(StagesDroitsCached sdhc) throws MessagingException {
		if (!sdhc.hasDroitsSignatures(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour signer cette convention.", false);
		}
		// Changement d'état
		setEtatSuivant(EOStgEtat.LC_CONVENTION_SIGNEE);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventSignaturesConvention(this, sdhc.getPersId());
	}

	/**
	 * Passe la convention dans l'état supprimé.
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionSupprimerTotalementConvention(StagesDroitsCached sdhc) throws MessagingException {
		if (!sdhc.hasDroitsSuppressionTotaleConvention(this)) {
			throw new DroitManquantException("Vos n'avez pas les droits pour supprimer cette convention", false);
		}
		// FIXME Ajouter raison
		setStgDSuppression(now());
		// Changement d'état
		String lcEtatPrecedent = toEtat().setLc();
		setEtatSuivant(EOStgEtat.LC_CONVENTION_SUPPRIMEE);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventSuppressionTotaleConvention(this, sdhc.getPersId(), lcEtatPrecedent);
	}

	/**
	 * @param sdhc Le {@link StagesDroitsCached}
	 */
	public void actionAnnulerSuppressionTotaleConvention(StagesDroitsCached sdhc) {
		if (!sdhc.hasDroitsAnnulationSuppressionTotaleConvention(this)) {
			throw new DroitManquantException("Vos n'avez pas les droits pour annuler la suppression de cette convention", false);
		}
		setStgDSuppression(null);

		EOQualifier qual = ERXQ.equals(EOStgEvenement.STG_EVENEMENT_TYPE.dot(EOStgEvenementType.SEVT_LC_KEY).key(),
		    EOStgEvenementType.LC_SUPPRESSION_TOTALE_CONVENTION);
		ERXSortOrderings sorts = ERXS.desc(EOStgEvenement.SEV_D_CREATION_KEY).array();
		NSArray<EOStgEvenement> eve = toEvenements(qual, sorts, true);
		if (eve == null || eve.count() == 0) {
			throw new FwkCktlStagesException("Impossible de trouver l'évènement de suppression...");
		}
		EOStgEvenement evenement = eve.objectAtIndex(0);
		setEtatSuivant(evenement.toStgEtatOld().setLc());
		FactoryEvenement.getInstance().eventAnnulationSuppressionTotaleConvention(this, sdhc.getPersId(), evenement.toStgEtatOld().setLc());
	}

	/**
	 * Passe la convention dans l'état résilié
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @param raison La raison de la résilisation
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionResilierConvention(StagesDroitsCached sdhc, String raison) throws MessagingException {
		if (!sdhc.hasDroitsResiliationConvention(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour résilier cette convention.", false);
		}
		if (StringCtrl.isEmpty(raison)) {
			throw new ValidationException("Vous devez renseigner une raison pour résilier la convention.");
		}
		// Changement d'état
		setEtatSuivant(EOStgEtat.LC_CONVENTION_RESILIEE);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventResilierConvention(this, sdhc.getPersId(), raison);
	}

	/**
	 * Passe la convention dans l'état signé
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 */
	public void actionAnnulerResiliationConvention(StagesDroitsCached sdhc) {
		if (!sdhc.hasDroitsAnnulationResiliationConvention(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour annuler la résiliation de cette convention.", false);
		}
		// Changement d'état
		setEtatSuivant(EOStgEtat.LC_CONVENTION_SIGNEE);
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventAnnulerResiliationConvention(this, sdhc.getPersId());
	}

	/**
	 * Création d'un avenant
	 * 
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionAjouterAvenant(StagesDroitsCached sdhc) throws MessagingException {
		if (!sdhc.hasDroitsCreationAvenant(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour créer un avenant.", false);
		}
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventCreationAvenant(this, sdhc.getPersId());
	}

	/**
	 * Modification d'un avenant
	 * 
	 * @param avenant L'{@link EOStgAvenant} à valider
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionModifierAvenant(EOStgAvenant avenant, StagesDroitsCached sdhc) throws MessagingException {
		if (!sdhc.hasDroitsModificationAvenant(avenant)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour modifier un avenant.", false);
		}
		// Ajout d'un évènement
		FactoryEvenement.getInstance().eventModificationAvenant(this, avenant, sdhc.getPersId());
	}

	/**
	 * Valide un avenant
	 * 
	 * @param avenant L'{@link EOStgAvenant} à valider
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionValiderAvenant(EOStgAvenant avenant, StagesDroitsCached sdhc) throws MessagingException {
		actionValiderAvenant(avenant, sdhc, true);
	}

	/**
	 * Valide un avenant
	 * 
	 * @param avenant L'{@link EOStgAvenant} à valider
	 * @param sdhcint i=0; Le {@link StagesDroitsCached}
	 * @param addEvent <code>true</code> si l'évènement doit être enregistré
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionValiderAvenant(EOStgAvenant avenant, StagesDroitsCached sdhc, boolean addEvent) throws MessagingException {
		if (!sdhc.hasDroitsValidationAvenant(avenant)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour valider cet avenant.", false);
		}
		// Changement d'état de l'avenant
		avenant.setPersIdValidation(sdhc.getPersId());
		avenant.setSavDValidation(now());
		avenant.setSavValide(OUI);
		// Ajou d'un évènement
		if (addEvent) {
			FactoryEvenement.getInstance().eventValidationAvenant(this, avenant, sdhc.getPersId());
		}
	}

	/**
	 * Refuse un avenant
	 * 
	 * @param avenant L'{@link EOStgAvenant} à valider
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionRefuserAvenant(EOStgAvenant avenant, StagesDroitsCached sdhc) throws MessagingException {
		if (!sdhc.hasDroitsRefusAvenant(avenant)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour valider/refuser cet avenant.", false);
		}
		// Changement d'état de l'avenant
		// FIXME
		avenant.setPersIdValidation(sdhc.getPersId());
		avenant.setSavDValidation(now());
		avenant.setSavValide(NON);
		// Ajou d'un évènement
		FactoryEvenement.getInstance().eventRefusAvenant(this, avenant, sdhc.getPersId());
	}

	/**
	 * Supprime un avenant
	 * 
	 * @param avenant L'{@link EOStgAvenant} à supprimer
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionSuppressionAvenant(EOStgAvenant avenant, StagesDroitsCached sdhc) throws MessagingException {
		if (!sdhc.hasDroitsSuppressionAvenant(avenant)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour supprimer cet avenant.", false);
		}
		// Changement d'état de l'avenant
		avenant.setPersIdSuppression(sdhc.getPersId());
		avenant.setSavDSuppression(now());
		// Ajou d'un évènement
		FactoryEvenement.getInstance().eventSuppressionAvenant(this, avenant, sdhc.getPersId());
	}

	/**
	 * Verrouille un avenant
	 * 
	 * @param avenant L'{@link EOStgAvenant} à supprimer
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @throws MessagingException Si erreur lors de l'envoi du mail
	 */
	public void actionVerrouillageAvenant(EOStgAvenant avenant, StagesDroitsCached sdhc) throws MessagingException {
		if (!sdhc.hasDroitsVerrouillageAvenant(avenant)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour verrouiller cet avenant.", false);
		}
		// Changement d'état de l'avenant
		avenant.setPersIdVerrouillage(sdhc.getPersId());
		avenant.setSavDVerrouillage(now());
		// Ajou d'un évènement
		FactoryEvenement.getInstance().eventVerrouillageAvenant(this, avenant, sdhc.getPersId());
	}

	/**
	 * @param unStgAvenant
	 * @param sdhc
	 * @param filePath
	 * @param reporter
	 * @throws Throwable
	 */
	public NSData actionEditionAvenant(EOStgAvenant unStgAvenant, StagesDroitsCached sdhc, String filePath, ODTReporter reporter) throws Throwable {
		if (!sdhc.hasDroitsEditionAvenant(unStgAvenant)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour éditer cet avenant.", false);
		}
		return generateAvenant(unStgAvenant, sdhc, filePath, reporter);
	}

	// -- -- -- -- -- -- -- -- -- VERIFICATION ETAT -- -- -- -- -- -- -- -- --

	/**
	 * @param lcEtat Le libellé court
	 * @return L'état correspondant au libellé court donné
	 */
	public boolean isEtatCourantApresEtatDonne(String lcEtat) {
		return FinderStgEtat.getInstance().compareEtats(this.toEtat().setLc(), lcEtat) > 0;
	}

	/**
	 * @return <code>TRUE</code> si l'état courant est l'état initial
	 */
	public boolean isEtatInitial() {
		return FinderStgEtat.getInstance().isEtatCourant(this, EOStgEtat.LC_INITIAL);
	}

	/**
	 * @return <code>TRUE</code> si la demande est en attente de validation pédagogique
	 */
	public boolean isEtatAttenteValidationPedagogique() {
		return FinderStgEtat.getInstance().isEtatCourant(this, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE);
	}

	public boolean isEtatAttenteValidationAdministrative() {
		return FinderStgEtat.getInstance().isEtatCourant(this, EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE);
	}

	public boolean isEtatAttenteEditionConvention() {
		return FinderStgEtat.getInstance().isEtatCourant(this, EOStgEtat.LC_ATTENTE_EDITION);
	}

	public boolean isEtatAttenteSignatures() {
		return FinderStgEtat.getInstance().isEtatCourant(this, EOStgEtat.LC_ATTENTE_SIGNATURES);
	}

	public boolean isEtatAttenteValidationTuteur() {
		return FinderStgEtat.getInstance().isEtatCourant(this, EOStgEtat.LC_ATTENTE_VALIDATION_TUTEUR);
	}

	public boolean isEtatAttenteDesignationTuteur() {
		return FinderStgEtat.getInstance().isEtatCourant(this, EOStgEtat.LC_ATTENTE_DESIGNATION_TUTEUR);
	}

	public boolean isEtatConventionSignee() {
		return FinderStgEtat.getInstance().isEtatCourant(this, EOStgEtat.LC_CONVENTION_SIGNEE);
	}

	public boolean isEtatConventionResiliee() {
		return FinderStgEtat.getInstance().isEtatCourant(this, EOStgEtat.LC_CONVENTION_RESILIEE);
	}

	public boolean isEtatConventionSupprimee() {
		return FinderStgEtat.getInstance().isEtatCourant(this, EOStgEtat.LC_CONVENTION_SUPPRIMEE);
	}

	// -- -- -- -- -- -- -- -- -- VERIFICATION ROLE -- -- -- -- -- -- -- -- --
	/**
	 * @param persId Le <code>PERS_ID</code> de l'utilisateur
	 * @return <code>TRUE</code> si le persId est celui de l'étudiant
	 */
	int i = 0;

	public boolean isDemandeur(Integer persId) {
		return persIdEtudiant().equals(persId);
	}

	/**
	 * @param persId Le <code>PERS_ID</code> de l'utilisateur
	 * @return <code>TRUE</code> si le tuteur est défini et que le persId lui appartient
	 */
	public boolean isTuteur(Integer persId) {
		if (persId != null && this.toTuteur() != null) {
			return this.toTuteur().persId().equals(persId);
		} else {
			return false;
		}
	}

	/**
	 * @param sdhc Le {@link StagesDroitsCached}
	 * @param gedBus Un {@link CktlGedBus}
	 * @return L'URL de la convention
	 */
	public String getConventionUrl(StagesDroitsCached sdhc, DocumentService documentService, WOContext context) {
		if (!sdhc.hasDroitsConsultationConvention(this)) {
			throw new DroitManquantException("Vous n'avez pas les droits pour consulter cette convention.", false);
		}

		EOStgPieceJointeRepart conventionPj = FinderStgPieceJointe.getUnePieceJointe(this, FinderStgPieceJointeType.TYPE_CONVENTION);

		if (conventionPj != null && conventionPj.stgDocument() != null) {
			return documentService.urlDocument(conventionPj.stgDocument(), context);
		}
		throw new FwkCktlStagesException("Erreur lors de la récupération du fichier.");

	}

	public String getStgSujetCourt() {
		return StringCtrl.compactString(super.stgSujet(), SUJET_COURT_MAX_LENGTH, " ...");
	}

	/**
	 * Vérifie la cohérence des dates
	 */
	public void checkDates() {
		EOStageHelper.getInstance().checkDates(stgDDebut(), stgDFin(), toAnnee().sanFin());
	}

	/**
	 * Vérifie que la durée maximale de stage dans un même organisme d'accueil est respectée
	 */
	public void checkDuree() {
		EOStageHelper.getInstance().checkDureeMaximale(stgDDebut(), stgDFin());
		EOStageHelper.getInstance().checkDureeMaximaleCumulee(this);
		EOStageHelper.getInstance().checkPeriodeDeCarence(this);
	}

	/**
	 * Vérifie la gratification de la convention
	 */
	public void checkGratification() {
		if (EOStageHelper.getInstance().isGratificationObligatoire(this)) {
			setStgGratObligatoire(OUI);
		} else {
			setStgGratObligatoire(NON);
		}
		EOStageHelper.getInstance().checkGratification(this);
	}

	/**
	 * @return La liste des avenants non supprimés
	 */
	public NSArray<EOStgAvenant> toAvenantsNonSuppr() {
		return toAvenants(ERXQ.isNull(EOStgAvenant.SAV_D_SUPPRESSION_KEY));
	}

	/**
	 * Vérifie si la convention est verrouillée
	 */
	public boolean isEtatVerrouille() {

		if ((isEtatInitial()) || (isEtatAttenteValidationPedagogique()) || (isEtatAttenteValidationTuteur()) || (isEtatAttenteDesignationTuteur())
		    || (isEtatAttenteValidationAdministrative()) || (isEtatAttenteEditionConvention()) || (isEtatAttenteSignatures())) {
			return false;
		} else {
			return true;
		}

	}

	/**
	 * @return Indique si la convention est en alerte càd non verrouillée et telle que la date du jour<date de début du stage-délai d'alerte
	 */
	public boolean isEnAlerte() {

		int seuilAlerteEnJours = FinderStgConfig.getInstance().getSeuilAlerte();
		long dateDebutStageConversion = getDateDebutStage().getTime();
		// long dateFinStageConversion=getDateFinStage().getTime();
		long dateJourConversion = new Date().getTime();

		// conversion du seuilAlerte en jours en millisecondes afin de soustraire à la date de fin du stage
		long dateSeuilAlerte = dateDebutStageConversion - (seuilAlerteEnJours * 24 * 3600 * 1000);

		// on regarde ensuite si la date du jour est >= à la dateSeuilAlerte
		// dans ce cas, la convention si elle n'est pas verrouillée est en alerte
		long difference = dateSeuilAlerte - dateJourConversion;

		if ((!isEtatVerrouille()) && (difference <= 0)) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * @return ec associé au stage
	 */
	public EC getEc() {
		if (ec == null) {
			ec = getFinderElementPedagogique().getEC(mecKey());
		}
		return ec;
	}

	/**
	 * @param ec ec associé au stage
	 */
	public void setEc(EC ec) {
		this.ec = ec;
		setMecKey(ec.getId());
	}

	/**
	 * @return le code de l'EC utilisé pour le tri
	 */
	public String toEcCode() {
		if (getEc() == null) {
			return null;
		}
		return getEc().getCode();
	}

	/**
	 * @return utilitaire de recherche des elements pedagogique
	 */
	public FinderElementPedagogique getFinderElementPedagogique() {
		if (finderElementPedagogique == null) {
			FactoryFinderScolarite factoryFinderScolarite = new FactoryFinderScolarite();
			finderElementPedagogique = factoryFinderScolarite.getFinderElementPedagogique(editingContext());
		}
		return finderElementPedagogique;
	}

}
