/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStgPieceJointeRepart.java instead.
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.util.Enumeration;
import java.util.NoSuchElementException;
// import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStgPieceJointeRepart extends  AFwkCktlStagesRecord {
	//	private static Logger logger = Logger.getLogger(_EOStgPieceJointeRepart.class);

	public static final String ENTITY_NAME = "FwkCktlStages_StgPieceJointeRepart";
	public static final String ENTITY_TABLE_NAME = "STAGES.STG_PIECE_JOINTE_REPART";

	// Attribute Keys
	public static final ERXKey<Boolean> SPJR_CONVENTION_OFFICIELLE = new ERXKey<Boolean>("spjrConventionOfficielle");
	public static final ERXKey<NSTimestamp> SPJR_D_CREATION = new ERXKey<NSTimestamp>("spjrDCreation");
	public static final ERXKey<Boolean> SPJR_IMPRESSION_OBLIGATOIRE = new ERXKey<Boolean>("spjrImpressionObligatoire");
	public static final ERXKey<String> SPJR_MODELE = new ERXKey<String>("spjrModele");
	public static final ERXKey<Integer> SPJR_PERS_ID = new ERXKey<Integer>("spjrPersId");
	public static final ERXKey<String> SPJR_VERSION_PAPIER = new ERXKey<String>("spjrVersionPapier");

	// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument> STG_DOCUMENT = new ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument>("stgDocument");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType> STG_PIECE_JOINTE_TYPE = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType>("stgPieceJointeType");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> STG_STAGE = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>("stgStage");

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "spjrId";

	public static final String SPJR_CONVENTION_OFFICIELLE_KEY = "spjrConventionOfficielle";
	public static final String SPJR_D_CREATION_KEY = "spjrDCreation";
	public static final String SPJR_IMPRESSION_OBLIGATOIRE_KEY = "spjrImpressionObligatoire";
	public static final String SPJR_MODELE_KEY = "spjrModele";
	public static final String SPJR_PERS_ID_KEY = "spjrPersId";
	public static final String SPJR_VERSION_PAPIER_KEY = "spjrVersionPapier";

	// Attributs non visibles
	public static final String SPJR_DOC_ID_KEY = "spjrDocId";
	public static final String SPJR_ID_KEY = "spjrId";
	public static final String SPJT_ID_KEY = "spjtId";
	public static final String STG_ID_KEY = "stgId";

	// Colonnes dans la base de donnees
	public static final String SPJR_CONVENTION_OFFICIELLE_COLKEY = "SPJR_CONVENTION_OFFICIELLE";
	public static final String SPJR_D_CREATION_COLKEY = "SPJR_D_CREATION";
	public static final String SPJR_IMPRESSION_OBLIGATOIRE_COLKEY = "SPJR_IMPRESSION_OBLIGATOIRE";
	public static final String SPJR_MODELE_COLKEY = "SPJR_MODELE";
	public static final String SPJR_PERS_ID_COLKEY = "SPJR_PERS_ID";
	public static final String SPJR_VERSION_PAPIER_COLKEY = "SPJR_VERSION_PAPIER";

	public static final String SPJR_DOC_ID_COLKEY = "SPJR_DOCUMENT_ID";
	public static final String SPJR_ID_COLKEY = "SPJR_ID";
	public static final String SPJT_ID_COLKEY = "SPJT_ID";
	public static final String STG_ID_COLKEY = "STG_ID";

	// Relationships
	public static final String STG_DOCUMENT_KEY = "stgDocument";
	public static final String STG_PIECE_JOINTE_TYPE_KEY = "stgPieceJointeType";
	public static final String STG_STAGE_KEY = "stgStage";


	// Accessors methods
	public Boolean spjrConventionOfficielle() {
		return (Boolean) storedValueForKey(SPJR_CONVENTION_OFFICIELLE_KEY);
	}

	public void setSpjrConventionOfficielle(Boolean value) {
		takeStoredValueForKey(value, SPJR_CONVENTION_OFFICIELLE_KEY);
	}

	public NSTimestamp spjrDCreation() {
		return (NSTimestamp) storedValueForKey(SPJR_D_CREATION_KEY);
	}

	public void setSpjrDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, SPJR_D_CREATION_KEY);
	}

	public Boolean spjrImpressionObligatoire() {
		return (Boolean) storedValueForKey(SPJR_IMPRESSION_OBLIGATOIRE_KEY);
	}

	public void setSpjrImpressionObligatoire(Boolean value) {
		takeStoredValueForKey(value, SPJR_IMPRESSION_OBLIGATOIRE_KEY);
	}

	public String spjrModele() {
		return (String) storedValueForKey(SPJR_MODELE_KEY);
	}

	public void setSpjrModele(String value) {
		takeStoredValueForKey(value, SPJR_MODELE_KEY);
	}

	public Integer spjrPersId() {
		return (Integer) storedValueForKey(SPJR_PERS_ID_KEY);
	}

	public void setSpjrPersId(Integer value) {
		takeStoredValueForKey(value, SPJR_PERS_ID_KEY);
	}

	public String spjrVersionPapier() {
		return (String) storedValueForKey(SPJR_VERSION_PAPIER_KEY);
	}

	public void setSpjrVersionPapier(String value) {
		takeStoredValueForKey(value, SPJR_VERSION_PAPIER_KEY);
	}

	public org.cocktail.fwkcktlged.serveur.metier.EODocument stgDocument() {
		return (org.cocktail.fwkcktlged.serveur.metier.EODocument) storedValueForKey(STG_DOCUMENT_KEY);
	}

	public void setStgDocumentRelationship(org.cocktail.fwkcktlged.serveur.metier.EODocument value) {
		if (value == null) {
			org.cocktail.fwkcktlged.serveur.metier.EODocument oldValue = stgDocument();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STG_DOCUMENT_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, STG_DOCUMENT_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType stgPieceJointeType() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType) storedValueForKey(STG_PIECE_JOINTE_TYPE_KEY);
	}

	public void setStgPieceJointeTypeRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType oldValue = stgPieceJointeType();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STG_PIECE_JOINTE_TYPE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, STG_PIECE_JOINTE_TYPE_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgStage stgStage() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgStage) storedValueForKey(STG_STAGE_KEY);
	}

	public void setStgStageRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgStage oldValue = stgStage();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STG_STAGE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, STG_STAGE_KEY);
		}
	}


	/**
	 * Créer une instance de EOStgPieceJointeRepart avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOStgPieceJointeRepart createEOStgPieceJointeRepart(EOEditingContext editingContext, Boolean spjrConventionOfficielle
, NSTimestamp spjrDCreation
, Boolean spjrImpressionObligatoire
, Integer spjrPersId
, String spjrVersionPapier
, org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeType stgPieceJointeType, org.cocktail.fwkcktlstages.serveur.metier.EOStgStage stgStage			) {
		EOStgPieceJointeRepart eo = (EOStgPieceJointeRepart) createAndInsertInstance(editingContext, _EOStgPieceJointeRepart.ENTITY_NAME);
		eo.setSpjrConventionOfficielle(spjrConventionOfficielle);
		eo.setSpjrDCreation(spjrDCreation);
		eo.setSpjrImpressionObligatoire(spjrImpressionObligatoire);
		eo.setSpjrPersId(spjrPersId);
		eo.setSpjrVersionPapier(spjrVersionPapier);
		eo.setStgPieceJointeTypeRelationship(stgPieceJointeType);
		eo.setStgStageRelationship(stgStage);
		return eo;
	}


	public EOStgPieceJointeRepart localInstanceIn(EOEditingContext editingContext) {
		return (EOStgPieceJointeRepart) localInstanceOfObject(editingContext, this);
	}
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOStgPieceJointeRepart creerInstance(EOEditingContext editingContext) {
		EOStgPieceJointeRepart object = (EOStgPieceJointeRepart) createAndInsertInstance(editingContext, _EOStgPieceJointeRepart.ENTITY_NAME);
		return object;
	}


	public static EOStgPieceJointeRepart localInstanceIn(EOEditingContext editingContext, EOStgPieceJointeRepart eo) {
		EOStgPieceJointeRepart localInstance = (eo == null) ? null : (EOStgPieceJointeRepart) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOStgPieceJointeRepart#localInstanceIn a la place.
	 */
	public static EOStgPieceJointeRepart localInstanceOf(EOEditingContext editingContext, EOStgPieceJointeRepart eo) {
		return EOStgPieceJointeRepart.localInstanceIn(editingContext, eo);
	}


	/* Finders */

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier) null);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		fetchSpec.setIsDeep(isDeep);
		fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart> eoObjects = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Cree une fetchSpec sur l'entité avec les parametres.
	 * 
	 * @param qualifier
	 * @param sortOrderings
	 * @param distinct
	 * @return
	 */
	public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return fetchSpec;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOStgPieceJointeRepart fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStgPieceJointeRepart fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray<EOStgPieceJointeRepart> eoObjects = fetchAll(editingContext, qualifier, null);
		EOStgPieceJointeRepart eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else if (count == 1) {
			eoObject = (EOStgPieceJointeRepart) eoObjects.objectAtIndex(0);
		} else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	
	public static EOStgPieceJointeRepart fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOStgPieceJointeRepart fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EOStgPieceJointeRepart> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOStgPieceJointeRepart eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else {
			eoObject = (EOStgPieceJointeRepart) eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOStgPieceJointeRepart fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOStgPieceJointeRepart eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOStgPieceJointeRepart ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	public static EOStgPieceJointeRepart fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

}
