/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODbVersion.java instead.
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.util.Enumeration;
import java.util.NoSuchElementException;
// import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODbVersion extends  AFwkCktlStagesRecord {
	//	private static Logger logger = Logger.getLogger(_EODbVersion.class);

	public static final String ENTITY_NAME = "FwkCktlStages_DbVersion";
	public static final String ENTITY_TABLE_NAME = "STAGES.DB_VERSION";

	// Attribute Keys
	public static final ERXKey<String> DBV_COMMENT = new ERXKey<String>("dbvComment");
	public static final ERXKey<NSTimestamp> DBV_DATE = new ERXKey<NSTimestamp>("dbvDate");
	public static final ERXKey<NSTimestamp> DBV_INSTALL = new ERXKey<NSTimestamp>("dbvInstall");
	public static final ERXKey<String> DBV_LIBELLE = new ERXKey<String>("dbvLibelle");

	// Relationship Keys

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "dbvId";

	public static final String DBV_COMMENT_KEY = "dbvComment";
	public static final String DBV_DATE_KEY = "dbvDate";
	public static final String DBV_INSTALL_KEY = "dbvInstall";
	public static final String DBV_LIBELLE_KEY = "dbvLibelle";

	// Attributs non visibles
	public static final String DBV_ID_KEY = "dbvId";

	// Colonnes dans la base de donnees
	public static final String DBV_COMMENT_COLKEY = "DBV_COMMENT";
	public static final String DBV_DATE_COLKEY = "DBV_DATE";
	public static final String DBV_INSTALL_COLKEY = "DBV_INSTALL";
	public static final String DBV_LIBELLE_COLKEY = "DBV_LIBELLE";

	public static final String DBV_ID_COLKEY = "DBV_ID";

	// Relationships


	// Accessors methods
	public String dbvComment() {
		return (String) storedValueForKey(DBV_COMMENT_KEY);
	}

	public void setDbvComment(String value) {
		takeStoredValueForKey(value, DBV_COMMENT_KEY);
	}

	public NSTimestamp dbvDate() {
		return (NSTimestamp) storedValueForKey(DBV_DATE_KEY);
	}

	public void setDbvDate(NSTimestamp value) {
		takeStoredValueForKey(value, DBV_DATE_KEY);
	}

	public NSTimestamp dbvInstall() {
		return (NSTimestamp) storedValueForKey(DBV_INSTALL_KEY);
	}

	public void setDbvInstall(NSTimestamp value) {
		takeStoredValueForKey(value, DBV_INSTALL_KEY);
	}

	public String dbvLibelle() {
		return (String) storedValueForKey(DBV_LIBELLE_KEY);
	}

	public void setDbvLibelle(String value) {
		takeStoredValueForKey(value, DBV_LIBELLE_KEY);
	}


	/**
	 * Créer une instance de EODbVersion avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EODbVersion createEODbVersion(EOEditingContext editingContext, NSTimestamp dbvDate
, String dbvLibelle
			) {
		EODbVersion eo = (EODbVersion) createAndInsertInstance(editingContext, _EODbVersion.ENTITY_NAME);
		eo.setDbvDate(dbvDate);
		eo.setDbvLibelle(dbvLibelle);
		return eo;
	}


	public EODbVersion localInstanceIn(EOEditingContext editingContext) {
		return (EODbVersion) localInstanceOfObject(editingContext, this);
	}
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EODbVersion creerInstance(EOEditingContext editingContext) {
		EODbVersion object = (EODbVersion) createAndInsertInstance(editingContext, _EODbVersion.ENTITY_NAME);
		return object;
	}


	public static EODbVersion localInstanceIn(EOEditingContext editingContext, EODbVersion eo) {
		EODbVersion localInstance = (eo == null) ? null : (EODbVersion) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EODbVersion#localInstanceIn a la place.
	 */
	public static EODbVersion localInstanceOf(EOEditingContext editingContext, EODbVersion eo) {
		return EODbVersion.localInstanceIn(editingContext, eo);
	}


	/* Finders */

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EODbVersion> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier) null);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EODbVersion> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EODbVersion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EODbVersion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EODbVersion> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EODbVersion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EODbVersion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		fetchSpec.setIsDeep(isDeep);
		fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EODbVersion> eoObjects = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EODbVersion>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Cree une fetchSpec sur l'entité avec les parametres.
	 * 
	 * @param qualifier
	 * @param sortOrderings
	 * @param distinct
	 * @return
	 */
	public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return fetchSpec;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EODbVersion fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODbVersion fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray<EODbVersion> eoObjects = fetchAll(editingContext, qualifier, null);
		EODbVersion eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else if (count == 1) {
			eoObject = (EODbVersion) eoObjects.objectAtIndex(0);
		} else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	
	public static EODbVersion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EODbVersion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EODbVersion> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EODbVersion eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else {
			eoObject = (EODbVersion) eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EODbVersion fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EODbVersion eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EODbVersion ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	public static EODbVersion fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

}
