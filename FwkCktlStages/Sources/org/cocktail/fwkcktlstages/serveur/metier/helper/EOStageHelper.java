package org.cocktail.fwkcktlstages.serveur.metier.helper;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgConfig;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class EOStageHelper {
	private static EOStageHelper instance;

	public static final double JOURS_PAR_MOIS = 30.5;
	public static final double MOYENNE_SEMAINES_PAR_MOIS = 4.34;
	public static final double JOURS_PAR_SEMAINE = 7.0;
	public static final int MILLI_TO_DAYS = 86400000;

	public static final String UNITE_MOIS = "M";
	public static final String UNITE_SEMAINE = "S";
	public static final String UNITE_JOUR = "J";

	public static final NSArray<String> DUREE_UNITES = new NSArray<String>("J", "S", "M");

	protected EOStageHelper() {
	}

	/**
	 * @return L'instance de {@link EOStageHelper}
	 */
	public static EOStageHelper getInstance() {
		if (instance == null) {
			instance = new EOStageHelper();
		}
		return instance;
	}

	protected static void setInstance(EOStageHelper sH) {
		instance = sH;
	}

	/**
	 * @param valeur
	 *            La valeur de la durée
	 * @param unite
	 *            L'unite (J, S, M)
	 * @return La durée totale en semaines
	 * 
	 */
	public double getDureeTotaleEnSemaines(BigDecimal valeur1, String unite) {
		double dureeStageSemaines = 0;
		double valeur=valeur1.doubleValue();
		if (EOStageHelper.UNITE_JOUR.equals(unite)) {
			dureeStageSemaines = valeur / EOStageHelper.JOURS_PAR_SEMAINE;
		} else if (EOStageHelper.UNITE_SEMAINE.equals(unite)) {
			dureeStageSemaines = valeur;
		} else if (EOStageHelper.UNITE_MOIS.equals(unite)) {
			dureeStageSemaines = valeur * EOStageHelper.MOYENNE_SEMAINES_PAR_MOIS;
		} else {
			throw new IllegalArgumentException("L'unité pour la durée du stage n'est pas reconnue.");
		}
		return dureeStageSemaines;
	}

	public double getDureeMinEnSemainesGratObligatoire() {
		return FinderStgConfig.getInstance().getDureeMinGratificationObligatoire() * MOYENNE_SEMAINES_PAR_MOIS;
	}

	public double getMontantGratificationObligatoire() {
		return FinderStgConfig.getInstance().getMontantGratificationObligatoire();
	}

	/**
	 * @param stage
	 *            Le stage
	 * @return <code>true</code> si la gratification est obligatoire
	 */
	public boolean isGratificationObligatoire(EOStgStage stage) {
		return isGratificationObligatoire(
				EOStructureHelper.getInstance().isOrganismeEtranger(stage.toOrganismeAccueil()),
				stage.stgDureeTotaleVal(), stage.stgDureeTotaleUnite());
	}

	/**
	 * @param isOrganismeEtranger
	 *            <code>true</code> si l'organisme est à l'étranger
	 * @param dureeVal
	 *            Valeur de la durée du stage
	 * @param dureeUnite
	 *            Unite de la durée du stage
	 * @return <code>true</code> si la gratification est obligatoire
	 */
	public boolean isGratificationObligatoire(boolean isOrganismeEtranger, BigDecimal dureeVal, String dureeUnite) {
		// 1ere vérification : la structure d'accueil est basée en France, la durée peut être calculée
		if (!isOrganismeEtranger && dureeVal != null) {
			// 2nde vérification : si la durée totale du stage est supérieure à duree_min_grat_obligatoire
			double minSemainesGratObligatoire = getDureeMinEnSemainesGratObligatoire();
			double dureeStageSemaines = getDureeTotaleEnSemaines(dureeVal, dureeUnite);
			
			if (dureeStageSemaines > minSemainesGratObligatoire) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Vérifie la gratification<br>
	 * <br>
	 * Pour que la gratification soit obligatoire, l'organisme d'accueil doit être en France et la <u>durée cumulée dans
	 * l'organisme d'accueil</u> dépasse une durée définie (2 mois actuellement) <br>
	 * Le montant de la gratification est défini suivant un pourcentage du plafond horaire de la Sécurité Sociale. <br>
	 * <br>
	 * Actuellement, 12,5% du plafond horaire (de 23€) de la SS, c'est à dire que la gratification doit être au minimum
	 * de :
	 * 
	 * <pre>
	 * 0.125 * 23 * 151.67 = 436.05€
	 * </pre>
	 * 
	 * @param stage
	 *            Un {@link EOStgStage}
	 */
	public void checkGratification(EOStgStage stage) {
		checkGratification(stage.toOrganismeAccueil(), stage.stgDureeTotaleVal(), stage.stgDureeTotaleUnite(),
				stage.stgGratMontant());
	}

	/**
	 * Vérifie la gratification<br>
	 * <br>
	 * Pour que la gratification soit obligatoire, l'organisme d'accueil doit être en France et la <u>durée cumulée dans
	 * l'organisme d'accueil</u> dépasse une durée définie (2 mois actuellement) <br>
	 * Le montant de la gratification est défini suivant un pourcentage du plafond horaire de la Sécurité Sociale. <br>
	 * <br>
	 * Actuellement, 12,5% du plafond horaire (de 23€) de la SS, c'est à dire que la gratification doit être au minimum
	 * de :
	 * 
	 * <pre>
	 * 0.125 * 23 * 151.67 = 436.05€
	 * </pre>
	 * 
	 * @param organisme
	 *            L'organisme d'accueil
	 * @param dureeVal
	 *            La durée du stage
	 * @param dureeUnite
	 *            L'unité de la durée du stage ("J"ours, "S"emaine, "M"ois)
	 * @param gratMontant
	 *            La montant de la gratification
	 */
	public void checkGratification(EOStructure organisme, BigDecimal dureeVal, String dureeUnite, Double gratMontant) {
		if (EOStructureHelper.getInstance().isOrganismeEtranger(organisme) || dureeVal == null || gratMontant == null) {
			return;
		}
		if (isGratificationObligatoire(EOStructureHelper.getInstance().isOrganismeEtranger(organisme), dureeVal,
				dureeUnite) && gratMontant < getMontantGratificationObligatoire()) {
			throw new NSValidation.ValidationException(
					"Le montant minimum de la gratification obligatoire n'est pas respecté.");
		}

	}

	/**
	 * @param debut
	 *            Date de début du stage
	 * @param fin
	 *            Date de fin du stage
	 * 
	 *            Vérifie la durée du stage
	 */
	public void checkDureeMaximale(Date debut, Date fin) {
		if (FinderStgConfig.getInstance().getDureeMaximaleDansOrganisme() == null) {
			// Pas de durée maximale
			return;
		}
		int dureeMaxMois = FinderStgConfig.getInstance().getDureeMaximaleDansOrganisme();
		// On vérifie que le stage ne dépasse pas la durée maximale
		Calendar myCalendarDebut = new GregorianCalendar();
		Calendar myCalendarFin = new GregorianCalendar();

		myCalendarDebut.setTime(debut);
		myCalendarFin.setTime(fin);

		myCalendarDebut.setFirstDayOfWeek(Calendar.MONDAY);
		myCalendarFin.setFirstDayOfWeek(Calendar.MONDAY);

		Calendar testDureeMax = myCalendarDebut;
		testDureeMax.add(Calendar.MONTH, dureeMaxMois);
		final int deltaJour = 3; // On laisse une marge de 3 jours
		testDureeMax.add(Calendar.DAY_OF_YEAR, deltaJour);
		if (testDureeMax.before(myCalendarFin)) {
			throw new NSValidation.ValidationException("la durée du stage que vous venez de saisir dépasse les "
					+ dureeMaxMois + " mois.");
		}
	}

	/**
	 * Vérifie la durée cumulée dans le même organisme d'accueil
	 * 
	 * @param stage
	 *            Le {@link EOStgStage}
	 */
	public void checkDureeMaximaleCumulee(EOStgStage stage) {
		final double deltaJours = 3;
		Integer dureeMaxMois = FinderStgConfig.getInstance().getDureeMaximaleDansOrganisme();
		if (dureeMaxMois == null || stage.toOrganismeAccueil() == null) {
			// Pas de durée maximale
			return;
		}
		// Recherche des autres stages dans le même organisme
		NSArray<EOStgStage> lesStages = getStagesDansMemeOrganisme(stage.toOrganismeAccueil(), stage.toEtudiant(),
				stage.toAnnee().sanDebut());
		if (lesStages == null) {
			// Pas d'autres stages
			return;
		}
		if (lesStages.indexOfObject(stage) == NSArray.NotFound) {
			lesStages = lesStages.arrayByAddingObject(stage);
		}
		int dureeCumuleeEnJours = 0;
		for (EOStgStage unStage : lesStages) {
			dureeCumuleeEnJours += getDureeCalculeeJours(unStage.stgDDebut(), unStage.stgDFin());
		}
		int dureeMaxJours = (int) (dureeMaxMois * JOURS_PAR_MOIS + deltaJours);
		if (dureeCumuleeEnJours > dureeMaxJours) {
			throw new NSValidation.ValidationException(
					"la durée cumulée des stages dans cet organisme d'accueil dépasse les " + dureeMaxMois + " mois.");
		}
	}

	/**
	 * @param debut
	 *            Date de début
	 * @param fin
	 *            Date de fin
	 * @return La durée du stage en jours.
	 */
	public int getDureeCalculeeJours(Date debut, Date fin) {
		return (int) ((fin.getTime() - debut.getTime()) / MILLI_TO_DAYS) + 1;
	}

	/**
	 * @param debut
	 *            Date de début
	 * @param fin
	 *            Date de fin
	 * @return La durée du stage en semaines
	 */
	public double getDureeCalculeeSemaines(Date debut, Date fin) {
		int dureeJours = getDureeCalculeeJours(debut, fin);
		return dureeJours / JOURS_PAR_SEMAINE;
	}

	/**
	 * @return La liste des stages dans le même organisme, la même année. Retourne <code>null</code> si aucun stage
	 *         n'est trouvé
	 */
	private NSArray<EOStgStage> getStagesDansMemeOrganisme(EOStructure organisme, EOEtudiant etudiant, Integer fannKey) {
		if (organisme == null) {
			return null;
		}
		NSMutableArray<EOStgStage> liste = (NSMutableArray<EOStgStage>) FinderStgStage.getInstance().getLesStages(
				etudiant.editingContext(), etudiant.etudNumero(), fannKey, true);
		if (liste == null) {
			return null;
		}
		EOQualifier qual = ERXQ.equals(EOStgStage.TO_ORGANISME_ACCUEIL_KEY, organisme);
		liste = EOQualifier.filteredArrayWithQualifier(liste, qual).mutableClone();

		if (liste.count() > 0) {
			return liste.immutableClone();
		} else {
			return null;
		}
	}

	/**
	 * Vérifie que la période de carence entre deux stages dans le même organisme d'accueil est bien respectée
	 * 
	 * @param leStage
	 *            Le {@link EOStgStage}
	 */
	public void checkPeriodeDeCarence(EOStgStage leStage) {
		Integer periodeCarencePourCent = FinderStgConfig.getInstance().getPeriodeCarencePourcentage();
		if (leStage.toOrganismeAccueil() == null || periodeCarencePourCent == null) {
			// Pas d'organisme ou de période de carence
			return;
		}
		NSArray<EOStgStage> lesStages = getStagesDansMemeOrganisme(leStage.toOrganismeAccueil(), leStage.toEtudiant(),
				leStage.toAnnee().sanDebut());
		if (lesStages == null) {
			// Pas d'autres stages
			return;
		}

		// On a des stages dans le même organisme d'accueil, pour la même année
		final double centPourCent = 100.0;
		double periodeCarencePourcentage = periodeCarencePourCent / centPourCent;
		for (EOStgStage unAutreStage : lesStages) {
			EOStgStage premierStage, secondStage;
			if (DateCtrl.isAfter(leStage.stgDDebut(), unAutreStage.stgDFin())) {
				// UnAutreStage avant leStage
				premierStage = unAutreStage;
				secondStage = leStage;
			} else if (DateCtrl.isAfter(unAutreStage.stgDDebut(), leStage.stgDFin())) {
				// UnAutreStage après leStage
				premierStage = leStage;
				secondStage = unAutreStage;
			} else {
				// !! Chevauchement !!
				throw new NSValidation.ValidationException(
						"Chevauchement de dates : vous ne pouvez pas avoir deux stages en même temps.");
			}
			Long delaisJours = (secondStage.stgDDebut().getTime() - premierStage.stgDFin().getTime())
					/ EOStageHelper.MILLI_TO_DAYS;
			Long dureePremierStageJours = (long) getDureeCalculeeJours(premierStage.stgDDebut(), premierStage.stgDFin());
			// (premierStage.stgDFin().getTime() - premierStage.stgDDebut().getTime()) / MILLI_TO_DAYS;
			Long dureeCarenceMin = (long) (dureePremierStageJours * periodeCarencePourcentage);
			if (delaisJours < dureeCarenceMin) {
				// La durée de carence n'est pas respectée
				throw new NSValidation.ValidationException(
						"La durée de carence entre deux stages dans le même organisme d'accueil n'est pas respectée.<br>"
								+ " Elle doit être d'au moins " + periodeCarencePourCent
								+ "% de la durée du premier stage (soit ici de " + dureeCarenceMin + " jours)");
			}
		}
	}

	/**
	 * Vérifie la cohérence des dates
	 * 
	 * @param debut
	 *            Début du stage
	 * @param fin
	 *            Fin du stage
	 * @param finAnnee
	 *            Fin de l'année universitaire
	 */
	public void checkDates(Date debut, Date fin, Integer finAnnee) {
		Date now = new Date();
		if (debut != null && fin != null) {
			if (debut.after(fin)) {
				throw new NSValidation.ValidationException("La date de début ne peut pas être après la date de fin.");
			}
			if (!FinderStgConfig.getInstance().isAutorisationAntidatage() && (debut.before(now)))
			{	
				throw new NSValidation.ValidationException("La date de début ne peut pas débuter avant la date du jour.");
			}
		if (FinderStgConfig.getInstance().getDateFinAnnee() != null) {
				Date dateFinAnnee = getDateFinAnnee(finAnnee);
				if (fin.after(dateFinAnnee)) {
					throw new NSValidation.ValidationException(
							"Le stage ne peut pas dépasser la date de fin d'année scolaire ("
									+ DateCtrl.dateToString(new NSTimestamp(dateFinAnnee), "%d/%m/%Y") + ")");
				}
			}
		}
	}


	private Date getDateFinAnnee(Integer anneeFin) {
		NSTimestamp dateFinAnnee = FinderStgConfig.getInstance().getDateFinAnnee();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFinAnnee);
		cal.set(Calendar.YEAR, anneeFin);
		return cal.getTime();
	}
	
}