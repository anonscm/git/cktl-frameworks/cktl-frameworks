package org.cocktail.fwkcktlstages.serveur.metier.helper;

import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgEtat;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class EOStructureHelper {

	private static EOStructureHelper instance;

	public static final EOQualifier QUAL_STRUCT_A_VALIDER = ERXQ.equals(EOStructure.TO_STRUCTURE_PERE_KEY + "."
			+ EOStructure.LC_STRUCTURE_KEY, "ORG_ACC-A_VALIDER");
	public static final EOQualifier QUAL_STRUCT_ANNULEES = ERXQ.equals(EOStructure.TO_STRUCTURE_PERE_KEY + "."
			+ EOStructure.LC_STRUCTURE_KEY, "ORG_ACC-ANNULES");
	public static final EOQualifier QUAL_STRUCT_VALIDES = ERXQ.equals(EOStructure.TO_STRUCTURE_PERE_KEY + "."
			+ EOStructure.LC_STRUCTURE_KEY, "ORG_ACC-VALIDES");

	public static final EOQualifier QUAL_GRP_STRUCT_VALIDES = ERXQ.equals(EOStructure.LC_STRUCTURE_KEY,
			"ORG_ACC-VALIDES");
	public static final EOQualifier QUAL_GRP_STRUCT_A_VALIDER = ERXQ.equals(EOStructure.LC_STRUCTURE_KEY,
			"ORG_ACC-A_VALIDER");
	public static final EOQualifier QUAL_GRP_STRUCT_ANNULEES = ERXQ.equals(EOStructure.LC_STRUCTURE_KEY,
			"ORG_ACC-ANNULES");

	protected EOStructureHelper() {
	}

	/**
	 * @return L'instance de {@link EOStructureHelper}
	 */
	public static EOStructureHelper getInstance() {
		if (instance == null) {
			instance = new EOStructureHelper();
		}
		return instance;
	}

	protected static void setInstance(EOStructureHelper anInstance) {
		instance = anInstance;
	}

	/**
	 * @param organisme
	 *            Un organisme
	 * @return <code>true</code> si l'organisme se trouve à l'étranger
	 */
	public boolean isOrganismeEtranger(EOStructure organisme) {
		// FIXME Refaire cette méthode en utilisant des techniques plus fiables...
		// FIXME Voir problèmes éventuels avec les TOM => gratification
		// non-obligatoire
		// # 976xx Mayotte
		// # 986xx Wallis-et-Futuna
		// # 987xx Polynésie Française
		// # 988xx Nouvelle-Calédonie

		if (organisme == null) {
			return false;
		}
		// On regarde toutes les adresses
		boolean toutesAdressesEtranger = true;
		boolean toutesAdressesPaysDefaut = true;
		NSArray<EORepartPersonneAdresse> repartAdresses = organisme
				.toRepartPersonneAdresses(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE_SANS_PERSO);
		for (EORepartPersonneAdresse eoRepartPersonneAdresse : repartAdresses) {
			if (eoRepartPersonneAdresse.toAdresse().toPays().isPaysDefaut()) {
				toutesAdressesEtranger = false;
				continue;
			} else {
				toutesAdressesPaysDefaut = false;
			}
		}
		// Si toutes les adresses sont à l'étranger
		if (toutesAdressesEtranger) {
			return true;
		}
		// Si toutes les adresses sont en France
		if (toutesAdressesPaysDefaut) {
			return false;
		}
		// Si des adresses à l'étranger et en France
		if (organisme.toFournis() == null) {
			if (organisme.toFournis().isEtranger()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param organisme
	 *            Un {@link EOStructure}
	 * @return <code>true</code> si l'organisme est valide
	 */
	public boolean isOrganismeValide(EOStructure organisme) {
		if (organisme == null) {
			return false;
		}
		EOQualifier qual = ERXQ.not(QUAL_STRUCT_ANNULEES).and(ERXQ.not(QUAL_STRUCT_A_VALIDER));
		NSArray<EOStructure> liste = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(new NSArray<EOStructure>(
				organisme), qual);
		return liste.count() == 1;
	}

	/**
	 * @param organisme
	 *            Un {@link EOStructure}
	 * @return <code>true</code> si l'organisme est utilisé dans des conventions déjà éditées
	 */
	public boolean hasConventionEditee(EOStructure organisme) {
		if (organisme == null) {
			return false;
		}
		NSArray<EOStgStage> listeStg = FinderStgStage.getInstance().getLesStagesByOrganismeAccueil(organisme);
		if (listeStg != null) {
			EOQualifier qual1 = FinderStgEtat.getInstance().getQualifierStageToEtat(EOStgEtat.LC_ATTENTE_SIGNATURES);
			EOQualifier qual2 = FinderStgEtat.getInstance().getQualifierStageToEtat(EOStgEtat.LC_CONVENTION_SIGNEE);
			listeStg = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(listeStg, ERXQ.or(qual1, qual2));
			if (listeStg.count() > 0) {
				return true;
			}
		}
		return false;
	}
}
