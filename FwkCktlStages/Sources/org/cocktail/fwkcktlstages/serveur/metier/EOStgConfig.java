/*
 * Copyright Cocktail, 2001-2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.NSValidation;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class EOStgConfig extends _EOStgConfig {

	public static final String CONFIG_DEFAULT_USER_PERS_ID = "default_user_pers_id";
	public static final String CONFIG_DEFAULT_ACCES_LC = "default_acces_lc";
	public static final String CONFIG_ETUDIANT_CHOIX_TUTEUR = "etudiant_choix_tuteur";
	public static final String CONFIG_TUTEUR_VALIDATION = "tuteur_validation";
	public static final String CONFIG_ADMINS_PERS_ID = "admins_pers_id";
	public static final String CONFIG_RESP_STRUCT_PERS_ID = "resp_struct_pers_id";
	public static final String CONFIG_RESP_PEDA_PERS_ID = "resp_peda_pers_id";
	public static final String CONFIG_RESP_ADMIN_PERS_ID = "resp_admin_pers_id";	
	
	public static final String CONFIG_DATE_FIN_ANNEE = "date_fin_annee";
	public static final String FORMAT_DATE_FIN_ANNEE = "%d/%m";
	public static final String CONFIG_TX_PLAFOND_HORAIRE = "tx_plafond_horaire";
	public static final String CONFIG_PLAFOND_HORAIRE = "plafond_horaire";
	public static final String CONFIG_DUREE_MENSUELLE = "duree_mensuelle";
	public static final String CONFIG_DUREE_MIN_GRAT_OBLIGATOIRE = "duree_min_grat_obligatoire";
	
	public static final String CONFIG_POURCENTAGE_PERIODE_CARENCE = "pourcentage_periode_carence";
	public static final String CONFIG_DUREE_MAXIMALE_DANS_ORGANISME_ACCUEIL = "duree_max_dans_orga_accueil";;
	
	public static final String CONFIG_ANTIDATAGE = "autorisation_antidatage";
	
	public static final String CONFIG_SEUIL_ALERTE = "seuil_alerte";
	
	public static final String CONFIG_BLOCAGE_MAILS_ALERTE="blocage_mails_alerte";
	
	public static final String CONFIG_BLOCAGE_MAILS_ALERTE_DDS="blocage_mails_alerte_DDS";
	
	public static final String CONFIG_DEFAULT_RC="RC_default";
	
	public static final String CONFIG_NOUVELLE_SCOlARITE ="nouvelle_scolarite";
	
	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException 
	 */
	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException 
	 */
	@Override
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException 
	 */
	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException 
	 */
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de
	 * l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException 
	 */
	@Override
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		super.validateBeforeTransactionSave();
	}
}
