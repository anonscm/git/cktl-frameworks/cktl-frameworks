/*
 * Copyright Cocktail, 2001-2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlstages.serveur.metier.helper.EOStageHelper;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSValidation;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class EOStgAvenant extends _EOStgAvenant {

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 *             Si erreur lors de la validation
	 */
	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 *             Si erreur lors de la validation
	 */
	@Override
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();

		EOGlobalID gid = editingContext().globalIDForObject(this);
		if (!gid.isTemporary()) {
			setSavDModification(now());
		}

		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 *             Si erreur lors de la validation
	 */
	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 *             Si erreur lors de la validation
	 */
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de
	 * l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 *             Si erreur lors de la validation
	 */
	@Override
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		super.validateBeforeTransactionSave();
	}

	@Override
	public void awakeFromInsertion(EOEditingContext editingContext) {
		super.awakeFromInsertion(editingContext);

		EOGlobalID gid = editingContext.globalIDForObject(this);
		if (gid.isTemporary()) {
			if (savDCreation() == null) {
				setSavDCreation(now());
			}
			if (savDModification() == null) {
				setSavDModification(savDCreation());
			}
		}
	}

	public EOIndividu getCreateur() {
		return EOIndividu.individuWithPersId(editingContext(), persIdCreation());
	}

	public EOIndividu getModificateur() {
		return EOIndividu.individuWithPersId(editingContext(), persIdModification());
	}

	public EOIndividu getValidateur() {
		return EOIndividu.individuWithPersId(editingContext(), persIdValidation());
	}

	public EOIndividu getSupprimeur() {
		return EOIndividu.individuWithPersId(editingContext(), persIdSuppression());
	}

	public String getId() {
		return primaryKey();
	}

	private String savDureeTotaleUniteAffichage() {
		if (super.savDureeTotaleUnite() == null) {
			return "";
		}
		String strS = "";
		if (super.savDureeTotaleVal() > 1) {
			strS = "s";
		}

		if (super.savDureeTotaleUnite().equals(EOStageHelper.UNITE_JOUR)) {
			return "jour" + strS;
		} else if (super.savDureeTotaleUnite().equals(EOStageHelper.UNITE_SEMAINE)) {
			return "semaine" + strS;
		} else if (super.savDureeTotaleUnite().equals(EOStageHelper.UNITE_MOIS)) {
			return "mois";
		}
		return super.savDureeTotaleUnite();
	}

	/**
	 * @return L'affichage de la durée
	 */
	public String savDureeTotaleAffichage() {
		if (savDureeTotaleVal() != null) {
			return savDureeTotaleVal() + " " + savDureeTotaleUniteAffichage();
		} else {
			return null;
		}
	}

	/**
	 * @return L'affichage de la gratification
	 */
	public String savGratificationMontantAffichage() {
		if (savGratMontant() != null) {
			return savGratMontant() + " " + toGratDevise().llDevise();
		} else {
			return null;
		}

	}

	/**
	 * @return <code>true</code> s'il y a eu des modifications
	 */
	public boolean hasModifications() {
		return !savDCreation().equals(savDModification());
	}

	public boolean isVerrouille() {
		return savDVerrouillage() != null;
	}

	public boolean isSupprime() {
		return savDSuppression() != null;
	}

	public boolean isValide() {
		return !isSupprime() && "O".equals(savValide());
	}

	public boolean isRefuse() {
		return !isSupprime() && "N".equals(savValide());
	}

	public boolean isAttenteValidation() {
		return !isSupprime() && StringCtrl.isEmpty(savValide()) && !isVerrouille();
	}

	public boolean isModificationSujet() {
		return !StringCtrl.isEmpty(savSujet());
	}

	public boolean isModificationDates() {
		return savDStageDebut() != null || savDStageFin() != null;
	}

	public boolean isInterruption() {
		return savDInterDebut() != null && savDInterFin() != null;
	}

	public boolean isAjoutLieu() {
		return toLieuAdresse() != null;
	}

	public boolean isModificationGratification() {
		return savGratMontant() != null;
	}
	
	public boolean isModificationService() {
		return toService() != null; 
	}
}
