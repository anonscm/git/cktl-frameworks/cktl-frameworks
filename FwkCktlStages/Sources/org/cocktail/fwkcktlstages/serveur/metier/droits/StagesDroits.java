/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.droits;

//import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Interface de Gestion des droits<br />
 * Deux implémentations disponibles :
 * <ul>
 * <li>Décentralisé <i>(par défaut)</i></li>
 * <li>Centralisé</li>
 * </ul>
 * 
 * @see StagesDroitsDecentralise
 * @see StagesDroitsCentralise
 * 
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public interface StagesDroits {

	/**
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @return <code>true</code> si l'utilisateur est administrateur
	 */
	boolean isAdmin(Integer persId);

	/**
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @param fannKey
	 *            L'année
	 * @return <code>true</code> si l'utilisateur est étudiant pour l'année donnée
	 */
	boolean isEtudiant(Integer persId, Integer fannKey);

	/**
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @return <code>true</code> si l'utilisateur est responsable des structures
	 */
	boolean isResponsableStructures(Integer persId);

	/**
	 * @param stage
	 *            Le stage
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @return <code>TRUE</code> si le persId appartient à un responsable administratif
	 */
	boolean isResponsableAdministratif(EOStgStage stage, Integer persId);

	/**
	 * @param mec
	 *            L'EC
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @return <code>TRUE</code> si le persId appartient à un responsable administratif
	 */
	boolean isResponsableAdministratif(EC mec, Integer persId);

	/**
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'individu
	 * @param fannKey
	 *            L'année
	 * @return <code>true</code> si l'individu est un responsable administratif
	 */
	boolean isResponsableAdministratif(Integer persId, Integer fannKey);

	/**
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'individu
	 * @param fannKey
	 *            L'année
	 * @return <code>true</code> si l'individu est un responsable pédagogique
	 */
	boolean isResponsablePedagogique(Integer persId, Integer fannKey);

	/**
	 * @param etudiant
	 *            L'Etudiant
	 * @param fannKey
	 *            L'année
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @return <code>TRUE</code> si le persId appartient à un responsable administratif
	 */
	boolean isResponsableAdministratif(IEtudiant etudiant, Integer fannKey, Integer persId);

	/**
	 * @param stage
	 *            Le stage
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @return <code>TRUE</code> si le persId appartient à un responsable pédagogique
	 */
	boolean isResponsablePedagogique(EOStgStage stage, Integer persId);

	/**
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @return L'{@link EOEtudiant} de l'utilisateur, s'il existe
	 */
	IEtudiant getConnectedUserEtudiant(Integer persId);

	/**
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'utilisateur
	 * @return L'{@link EOIndividu} de l'utilisateur
	 */
	IIndividu getConnectedUserIndividu(Integer persId);

	/**
	 * @param edc
	 *            L'EOEditingContext
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'individu
	 * @param fannKey
	 *            L'année
	 * @return La liste des stages liés à un EC pour lequel l'individu est un responsable, ou une liste vide.
	 */
	NSArray<EOStgStage> getLesStagesByResponsable(EOEditingContext edc, Integer persId, Integer fannKey);

	/**
	 * @param edc
	 *            L'EOEditingContext
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'individu
	 * @param fannKey
	 *            L'année
	 * @return La liste des stages liés à un EC pour lequel l'individu est un secrétaire, ou une liste vide.
	 */
	NSArray<EOStgStage> getLesStagesBySecretaire(EOEditingContext edc, Integer persId, Integer fannKey);

	/**
	 * @param edc
	 *            L'EOEditingContext
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'individu
	 * @param fannKey
	 *            L'année
	 * @return La liste des stages liés à un EC pour lequel l'individu est un enseignant, ou une liste vide.
	 */
	NSArray<EOStgStage> getLesStagesByEnseignant(EOEditingContext edc, Integer persId, Integer fannKey);

	/**
	 * @param edc
	 *            L'EOEditingContext
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'individu
	 * @param fannKey
	 *            L'année
	 * @return La liste des ECs pour lesquels l'individu est responsable
	 */
	List<EC> getLesEcByResponsable(EOEditingContext edc, Integer persId, Integer fannKey);

	/**
	 * @param edc
	 *            L'EOEditingContext
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'individu
	 * @param fannKey
	 *            L'année
	 * @return La liste des ECs pour lesquels l'individu est secrétaire
	 */
	List<EC> getLesEcBySecretaire(EOEditingContext edc, Integer persId, Integer fannKey);

	/**
	 * @param edc
	 *            L'EOEditingContext
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'individu
	 * @param fannKey
	 *            L'année
	 * @return La liste des ECs pour lesquels l'individu est enseignant
	 */
	List<EC> getLesEcByEnseignant(EOEditingContext edc, Integer persId, Integer fannKey);
	
	
	/**
	 * 
	 * @return l'information sur la centralisation de la gestion des droits
	 */
	Boolean isGestionDroitsCentralisee();
		
}