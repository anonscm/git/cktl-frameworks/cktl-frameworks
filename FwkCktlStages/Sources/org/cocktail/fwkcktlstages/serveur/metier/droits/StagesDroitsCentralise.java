/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.droits;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgConfig;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;

import com.google.inject.Singleton;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Mode de gestion <b>centralisé</b> pour la gestion des droits.<br />
 * Les droits sont déterminés suivant une appartenance à un groupe de l'annuaire.
 * 
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
@Singleton
public class StagesDroitsCentralise extends BaseStagesDroits {

	/** {@inheritDoc} */
	public boolean isResponsableAdministratif(Integer persId, Integer fannKey) {
		List<Integer> listeRespAdm = getListeResponsablesAdministratifs();
		return listeRespAdm.contains(persId);
	}

	protected List<Integer> getListeResponsablesAdministratifs() {
		return FinderStgConfig.getInstance().getListeResponsablesAdministratifsModeCentralise();
	}

	/** {@inheritDoc} */
	public boolean isResponsableAdministratif(EOStgStage stage, Integer persId) {
		return isResponsableAdministratif(persId, stage.toAnnee().sanDebut());
	}

	/** {@inheritDoc} */
	public boolean isResponsableAdministratif(EC mec, Integer persId) {
		return isResponsableAdministratif(persId, mec.getAnnee());
	}

	/** {@inheritDoc} */
	public boolean isResponsableAdministratif(IEtudiant etudiant, Integer fannKey, Integer persId) {
		return isResponsableAdministratif(persId, fannKey);
	}

	/** {@inheritDoc} */
	public boolean isResponsablePedagogique(Integer persId, Integer fannKey) {
		List<Integer> listeRespPeda = getListeResponsablesPedagogiques();
		return listeRespPeda.contains(persId);
	}

	protected List<Integer> getListeResponsablesPedagogiques() {
		return FinderStgConfig.getInstance().getListeResponsablesPedagogiqueModeCentralise();
	}

	/** {@inheritDoc} */
	public boolean isResponsablePedagogique(EOStgStage stage, Integer persId) {
		return isResponsablePedagogique(persId, stage.toAnnee().sanDebut());
	}

	/** {@inheritDoc} */
	public NSArray<EOStgStage> getLesStagesByResponsable(EOEditingContext edc, Integer persId, Integer fannKey) {
		if (isResponsablePedagogique(persId, fannKey)) {
			return getStages(edc, fannKey);
		}
		return NSArray.EmptyArray;
	}

	/** {@inheritDoc} */
	public NSArray<EOStgStage> getLesStagesBySecretaire(EOEditingContext edc, Integer persId, Integer fannKey) {
		if (isResponsableAdministratif(persId, fannKey)) {
			return getStages(edc, fannKey);
		}
		return NSArray.EmptyArray;
	}
	
	protected NSArray<EOStgStage> getStages(EOEditingContext edc, Integer fannKey) {
		return FinderStgStage.getInstance().getLesStages(edc, fannKey);
	}

	/** {@inheritDoc} */
	public NSArray<EOStgStage> getLesStagesByEnseignant(EOEditingContext edc, Integer persId, Integer fannKey) {
		return NSArray.EmptyArray;
	}


	/** {@inheritDoc} */
	public Boolean isGestionDroitsCentralisee() {
		return true;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public List<EC> getLesEcByResponsable(EOEditingContext edc, Integer persId, Integer fannKey) {
	  return new ArrayList<EC>();
  }

	/**
	 * {@inheritDoc}
	 */
	public List<EC> getLesEcBySecretaire(EOEditingContext edc, Integer persId, Integer fannKey) {
		 return new ArrayList<EC>();
  }

	/**
	 * {@inheritDoc}
	 */
	public List<EC> getLesEcByEnseignant(EOEditingContext edc, Integer persId, Integer fannKey) {
		 return new ArrayList<EC>();
  }
}
