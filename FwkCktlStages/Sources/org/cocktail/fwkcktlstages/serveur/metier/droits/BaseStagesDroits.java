/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.droits;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlstages.serveur.finder.FactoryFinderScolarite;
import org.cocktail.fwkcktlstages.serveur.finder.FinderEtudiant;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgConfig;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public abstract class BaseStagesDroits implements StagesDroits {

	/** {@inheritDoc} */
	public final boolean isAdmin(Integer persId) {
		List<Integer> listeAdmin = FinderStgConfig.getInstance().getListeAdmin();
		return listeAdmin.contains(persId);
	}

	/** {@inheritDoc} */
	public boolean isResponsableStructures(Integer persId) {
		List<Integer> liste = FinderStgConfig.getInstance().getListeResponsablesStructures();
		return liste.contains(persId);
	}

	/** {@inheritDoc} */
	public boolean isEtudiant(Integer persId, Integer fannKey) {
		boolean isEtudiant = false;
		EOEtudiant etudUser = getConnectedUserEtudiant(persId);
		if (etudUser != null) {
			FactoryFinderScolarite factoryFinder = new FactoryFinderScolarite();
			FinderEtudiant finderEtudiant = factoryFinder.getFinderEtudiant(etudUser.editingContext(), fannKey);			
		  isEtudiant = finderEtudiant.isInscritPourAnne(etudUser, fannKey); 
		}
		return isEtudiant;
	}

	/** {@inheritDoc} */
	public EOEtudiant getConnectedUserEtudiant(Integer persId) {
		EOIndividu individu = getConnectedUserIndividu(persId);
		EOEtudiant etudiant = null;
		if (individu != null) {
			EOEditingContext edc = individu.editingContext();
			etudiant = EOEtudiant.etudiantForIndividu(edc, individu);
		}
		return etudiant;
	}

	/** {@inheritDoc} */
	public EOIndividu getConnectedUserIndividu(Integer persId) {
		EOEditingContext edc = ERXEC.newEditingContext();
		return EOIndividu.individuWithPersId(edc, persId);
	}
}