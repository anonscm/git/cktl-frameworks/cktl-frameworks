/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.droits;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlstages.serveur.finder.FactoryFinderScolarite;
import org.cocktail.fwkcktlstages.serveur.finder.FinderElementPedagogique;
import org.cocktail.fwkcktlstages.serveur.finder.FinderResponsable;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;

import com.google.inject.Singleton;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEC;

/**
 * Mode de gestion <b>décentralisé</b> pour la gestion des droits. Ceci est le mode par défaut.<br />
 * Les droits sont issus des responsabilités présentes dans la scolarité, à plusieurs niveaux :
 * <ul>
 * <li>EC</li>
 * <li>UE</li>
 * <li>Parcours</li>
 * <li>Diplôme</li>
 * </ul>
 * 
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
@Singleton
public class StagesDroitsDecentralise extends BaseStagesDroits {
	private FactoryFinderScolarite factoryFinder;
	
	public FactoryFinderScolarite getFactoryFinder() {		
	  return factoryFinder;
  }
	
	public void setFactoryFinder(FactoryFinderScolarite factoryFinder) {
	  this.factoryFinder = factoryFinder;
  }

	/** {@inheritDoc} */
	public boolean isResponsableAdministratif(EOStgStage stage, Integer persId) {
		if (persId == null) {
			return false;
		}
		List<Integer> listeResp = getPersIdResponsablesAdministratifs(stage);
		if (listeResp != null) {
			return listeResp.contains(persId);
		}
		return false;
	}

	/** {@inheritDoc} */
	public boolean isResponsableAdministratif(EC mec, Integer persId) {
		if (persId == null) {
			return false;
		}
		List<Integer> listeResp = getPersIdResponsablesAdministratifs(mec);
		if (listeResp != null) {
			return listeResp.contains(persId);
		}
		return false;
	}

	/** {@inheritDoc} */
	public boolean isResponsableAdministratif(Integer persId, Integer fannKey) {
		EOEditingContext edc = ERXEC.newEditingContext();
		FinderResponsable finderResponsable = getFactoryFinder().getFinderResponsable(edc);
		return hasResponsabilite(persId, fannKey, edc, finderResponsable.getTypeSecretaire());
	}

	/** {@inheritDoc} */
	public boolean isResponsablePedagogique(Integer persId, Integer fannKey) {
		EOEditingContext edc = ERXEC.newEditingContext();
		FinderResponsable finderResponsable = getFactoryFinder().getFinderResponsable(edc);
		return hasResponsabilite(persId, fannKey, edc, finderResponsable.getTypeResponsable());
	}

	private boolean hasResponsabilite(Integer persId, Integer fannKey, EOEditingContext edc, String typeResponsabilite) {
		return !getEcByResponsabilite(persId, fannKey, edc, typeResponsabilite).isEmpty();
	}

	protected NSArray<EOStgStage> getLesStagesByResponsabilite(Integer persId, Integer fannKey, EOEditingContext edc, String typeResponsabilite) {
		return	FinderStgStage.getInstance().getLesStagesByListeMec(edc, getEcByResponsabilite(persId, fannKey, edc, typeResponsabilite));
	}

	protected List<EC> getEcByResponsabilite(Integer persId, Integer fannKey, EOEditingContext edc, String typeResponsabilite) {
		FinderElementPedagogique finderElementPedagogique = getFactoryFinder().getFinderElementPedagogique(edc);
		return finderElementPedagogique.getListeECStageByResponsablite(persId, fannKey, typeResponsabilite);
	}

	/** {@inheritDoc} */
	public boolean isResponsableAdministratif(IEtudiant etudiant, Integer fannKey, Integer persId) {
		if (persId == null) {
			return false;
		}
		List<Integer> listeResp = getPersIdResponsablesAdministratifs(etudiant, fannKey);
		if (listeResp != null) {
			return listeResp.contains(persId);
		}
		return false;
	}

	/** {@inheritDoc} */
	public boolean isResponsablePedagogique(EOStgStage stage, Integer persId) {
		if (persId == null) {
			return false;
		}
		List<Integer> listeResp = getPersIdResponsablesPedagogiques(stage);
		if (listeResp != null) {
			return listeResp.contains(persId);
		}
		return false;
	}

	/** {@inheritDoc} */
	public NSArray<EOStgStage> getLesStagesByEnseignant(EOEditingContext edc, Integer persId, Integer fannKey) {
		FinderResponsable finderResponsable = getFactoryFinder().getFinderResponsable(edc);
		return getLesStagesByResponsabilite(persId, fannKey, edc, finderResponsable.getTypeEnseignant());
	}

	/** {@inheritDoc} */
	public NSArray<EOStgStage> getLesStagesBySecretaire(EOEditingContext edc, Integer persId, Integer fannKey) {
		FinderResponsable finderResponsable = getFactoryFinder().getFinderResponsable(edc);
		return getLesStagesByResponsabilite(persId, fannKey, edc, finderResponsable.getTypeSecretaire());
	}

	/** {@inheritDoc} */
	public NSArray<EOStgStage> getLesStagesByResponsable(EOEditingContext edc, Integer persId, Integer fannKey) {
		FinderResponsable finderResponsable = getFactoryFinder().getFinderResponsable(edc);
		return getLesStagesByResponsabilite(persId, fannKey, edc, finderResponsable.getTypeResponsable());
	}

	/**
	 * @param stage Le stage
	 * @return La liste des <code>PERS_ID</code> des responsables pédagogiques
	 */
	protected List<Integer> getPersIdResponsablesPedagogiques(EOStgStage stage) {
		if (stage == null) {
			return null;
		}
		FinderResponsable finderResponsable = getFactoryFinder().getFinderResponsable(stage.editingContext());
		List<IPersonne> listeResp = finderResponsable.getResponsablesByEtudiantAndEc(stage.toIEtudiant(), stage.mecKey(), stage.toAnnee().sanDebut(),
		    finderResponsable.getTypeResponsable());
		return getPersIds(listeResp);
	}

	/**
	 * @param stage Le stage
	 * @return La liste des <code>PERS_ID</code> des responsables administratifs
	 */
	protected List<Integer> getPersIdResponsablesAdministratifs(EOStgStage stage) {
		if (stage == null) {
			return null;
		}
		FinderResponsable finderResponsable = getFactoryFinder().getFinderResponsable(stage.editingContext());
		List<IPersonne> listeResp = finderResponsable.getResponsablesByEtudiantAndEc(stage.toIEtudiant(), stage.mecKey(), stage.toAnnee().sanDebut(),
		    finderResponsable.getTypeResponsable());
		return getPersIds(listeResp);
	}

	/**
	 * @param mec L'EC
	 * @return La liste des <code>PERS_ID</code> des responsables administratifs
	 */
	protected List<Integer> getPersIdResponsablesAdministratifs(EC mec) {
		FinderResponsable finderResponsable = getFactoryFinder().getFinderResponsable(ERXEC.newEditingContext());
		List<IPersonne> listeResp = finderResponsable.getResponsablesByEtudiantAndEc(null, mec.getId(), mec.getAnnee(), finderResponsable.getTypeSecretaire());
		return getPersIds(listeResp);
	}

	/**
	 * @param etudiant L'Etudiant
	 * @param fannKey L'année
	 * @return La liste des <code>PERS_ID</code> des responsables administratifs
	 */
	protected List<Integer> getPersIdResponsablesAdministratifs(IEtudiant etudiant, Integer fannKey) {
		FinderResponsable finderResponsable = getFactoryFinder().getFinderResponsable(((EOEtudiant) etudiant).editingContext());
		List<IPersonne> listeResp = finderResponsable.getResponsablesByEtudiantAndEc(etudiant, null, fannKey, finderResponsable.getTypeSecretaire());
		return getPersIds(listeResp);
	}

	/**
	 * @param listeResp
	 * @return
	 */
	private List<Integer> getPersIds(List<IPersonne> listeResp) {
		List<Integer> listeRespPersId = new ArrayList<Integer>();
		for (Iterator<IPersonne> iterator = listeResp.iterator(); iterator.hasNext();) {
			listeRespPersId.add(iterator.next().persId());
		}
		return listeRespPersId;
	}

	/** {@inheritDoc} */
	public List<EC> getLesEcByResponsable(EOEditingContext edc, Integer persId, Integer fannKey) {
		FinderResponsable finderResponsable = getFactoryFinder().getFinderResponsable(edc);
		return getEcByResponsabilite(persId, fannKey, edc, finderResponsable.getTypeResponsable());
	}

	/** {@inheritDoc} */
	public List<EC> getLesEcBySecretaire(EOEditingContext edc, Integer persId, Integer fannKey) {
		FinderResponsable finderResponsable = getFactoryFinder().getFinderResponsable(edc);
		return getEcByResponsabilite(persId, fannKey, edc, finderResponsable.getTypeSecretaire());
	}

	/** {@inheritDoc} */
	public List<EC> getLesEcByEnseignant(EOEditingContext edc, Integer persId, Integer fannKey) {
		FinderResponsable finderResponsable = getFactoryFinder().getFinderResponsable(edc);
		return getEcByResponsabilite(persId, fannKey, edc, finderResponsable.getTypeEnseignant());
	}

	/** {@inheritDoc} */
	public Boolean isGestionDroitsCentralisee() {
		return false;
	}
}
