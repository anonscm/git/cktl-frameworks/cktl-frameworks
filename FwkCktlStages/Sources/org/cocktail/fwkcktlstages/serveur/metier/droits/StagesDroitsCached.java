/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.droits;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlstages.serveur.finder.FactoryFinderScolarite;
import org.cocktail.fwkcktlstages.serveur.finder.FinderElementPedagogique;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgConfig;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgPieceJointe;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgPieceJointeType;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgPieceJointeRepart;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class StagesDroitsCached {

	private final Integer persId;
	private final Integer etudNumero;
	private final Integer fannKey;

	private Boolean isAdmin;
	private Boolean isEtudiant;
	private Boolean isResponsableAdministratif;
	private Boolean isResponsablePedagogique;
	private Boolean isResponsableStructures;

	private final Map<Integer, Boolean> mapIsResponsableAdministratifStage = new HashMap<Integer, Boolean>();
	private final Map<Integer, Boolean> mapIsResponsableAdministratifEC = new HashMap<Integer, Boolean>();
	private final Map<Integer, Boolean> mapIsResponsableAdministratifEtudiant = new HashMap<Integer, Boolean>();
	private final Map<Integer, Boolean> mapIsResponsablePedagogiqueStage = new HashMap<Integer, Boolean>();
	private final Map<Integer, Boolean> mapIsResponsablePedagogiqueEC = new HashMap<Integer, Boolean>();
	private final Map<Integer, Boolean> mapIsResponsablePedagogiqueEtudiant = new HashMap<Integer, Boolean>();

	private final List<EOStgStage> listeStagesEnseignant = new ArrayList<EOStgStage>();
	private final List<EOStgStage> listeStagesSecretaire = new ArrayList<EOStgStage>();
	private final List<EOStgStage> listeStagesResponsable = new ArrayList<EOStgStage>();

	private final List<EC> listeEcEnseignant = new ArrayList<EC>();
	private final List<EC> listeEcSecretaire = new ArrayList<EC>();
	private final List<EC> listeEcResponsable = new ArrayList<EC>();

	private StagesDroits sdh;

	/**
	 * @param utilisateurPersId Le <code>PERS_ID</code> de l'utilisateur
	 * @param annee L'année
	 * @param aSdh Un {@link StagesDroits}
	 * @throws Exception Si erreur lors de l'initialisation du SDH
	 */
	public StagesDroitsCached(Integer utilisateurPersId, Integer annee, StagesDroits aSdh) throws Exception {
		persId = utilisateurPersId;
		fannKey = annee;
		sdh = aSdh;
		etudNumero = initEtudiant();
		initListesStages();
		initListesEc();
	}

	/**
	 * reset des listes de stages
	 */
	public void resetCacheStage() {
		listeStagesEnseignant.clear();
		listeStagesSecretaire.clear();
		listeStagesResponsable.clear();
		initListesStages();
	}

	protected Integer initEtudiant() {
		if (isEtudiant()) {
			return sdh.getConnectedUserEtudiant(persId).etudNumero();
		} else {
			return null;
		}
	}

	protected void initListesEc() {
		EOEditingContext edc = ERXEC.newEditingContext();
		for (EC ec : sdh.getLesEcByEnseignant(edc, persId, fannKey)) {
			listeEcEnseignant.add(ec);
		}
		for (EC ec : sdh.getLesEcBySecretaire(edc, persId, fannKey)) {
			listeEcSecretaire.add(ec);
		}
		for (EC ec : sdh.getLesEcByResponsable(edc, persId, fannKey)) {
			listeEcResponsable.add(ec);
		}
	}

	protected void initListesStages() {
		EOEditingContext edc = ERXEC.newEditingContext();
		for (EOStgStage stage : sdh.getLesStagesByEnseignant(edc, persId, fannKey)) {
			listeStagesEnseignant.add(stage);
		}
		for (EOStgStage stage : sdh.getLesStagesBySecretaire(edc, persId, fannKey)) {
			listeStagesSecretaire.add(stage);
		}
		for (EOStgStage stage : sdh.getLesStagesByResponsable(edc, persId, fannKey)) {
			listeStagesResponsable.add(stage);
		}
	}

	/**
	 * @return Le <code>PERS_ID</code> de l'utilisateur
	 */
	public Integer getPersId() {
		return persId;
	}

	/**
	 * @return <code>true</code> si l'utilisateur est administrateur
	 */
	public boolean isAdmin() {
		if (isAdmin == null) {
			isAdmin = sdh.isAdmin(persId);
		}
		return isAdmin;
	}

	/**
	 * @return <code>true</code> si l'utilisateur est un étudiant
	 */
	public boolean isEtudiant() {
		if (isEtudiant == null) {
			isEtudiant = sdh.isEtudiant(persId, fannKey);
		}
		return isEtudiant;
	}

	/**
	 * @return Le numéro étudiant de l'utilisateur, <code>null</code> si l'utilisateur n'est pas étudiant
	 */
	public Integer getEtudNumero() {
		return etudNumero;
	}

	/**
	 * @return L'année courante
	 */
	public Integer getFannKey() {
		return fannKey;
	}

	/**
	 * @return <code>true</code> si l'utilisateur est un responsable administratif/pédagogique/de structure ou un admin
	 */
	public boolean isResponsable() {
		return isAdmin() || isResponsableStructures() || isResponsableAdministratif() || isResponsablePedagogique();
	}

	/**
	 * @return <code>true</code> si l'utilisateur est responsable administratif
	 */
	public boolean isResponsableAdministratif() {
		if (isResponsableAdministratif == null) {
			isResponsableAdministratif = sdh.isResponsableAdministratif(persId, fannKey);
		}
		return isResponsableAdministratif;
	}

	/**
	 * @return <code>true</code> si l'utilisateur est responsable pédagogique
	 */
	public boolean isResponsablePedagogique() {
		if (isResponsablePedagogique == null) {
			isResponsablePedagogique = sdh.isResponsablePedagogique(persId, fannKey);
		}
		return isResponsablePedagogique;
	}

	/**
	 * @return <code>true</code> si l'utilisateur est responsable structures
	 */
	public boolean isResponsableStructures() {
		if (isResponsableStructures == null) {
			isResponsableStructures = sdh.isResponsableStructures(persId);
		}
		return isResponsableStructures;
	}

	/**
	 * @param stage Un stage
	 * @return <code>true</code> si l'utilisateur est responsable administratif du stage
	 */
	public boolean isResponsableAdministratif(EOStgStage stage) {
		if (stage == null || stage.primaryKey() == null) {
			return false;
		}
		Integer primaryKey = Integer.parseInt(stage.primaryKey());
		Boolean isResp = mapIsResponsableAdministratifStage.get(primaryKey);
		if (isResp == null) {
			isResp = sdh.isResponsableAdministratif(stage, persId);
			mapIsResponsableAdministratifStage.put(primaryKey, isResp);
		}
		return isResp;
	}

	/**
	 * @param mec Un Ec
	 * @return <code>true</code> si l'utilisateur est responsable administratif de l'EC
	 */
	public boolean isResponsableAdministratif(EC mec) {
		if (mec == null || mec.getId() == null) {
			return false;
		}
		Boolean isResp = mapIsResponsableAdministratifEC.get(mec.getId());
		if (isResp == null) {
			isResp = sdh.isResponsableAdministratif(mec, persId);
			mapIsResponsableAdministratifEC.put(mec.getId(), isResp);
		}
		return isResp;
	}

	/**
	 * @param etudiant Un étudiant
	 * @return <code>true</code> si l'utilisateur est responsable administratif de l'étudiant
	 */
	public boolean isResponsableAdministratif(IEtudiant etudiant) {
		if (etudiant == null || etudiant.etudNumero() == null || ((EOEtudiant) etudiant).editingContext() == null) {
			return false;
		}
		Boolean isResp = mapIsResponsableAdministratifEtudiant.get(etudiant.etudNumero());
		if (isResp == null) {
			isResp = sdh.isResponsableAdministratif(etudiant, fannKey, persId);
			mapIsResponsableAdministratifEtudiant.put(etudiant.etudNumero(), isResp);
		}
		return isResp;
	}

	/**
	 * @param stage Un stage
	 * @return <code>true</code> si l'utilisateur est responsable pédagogique du stage
	 */
	public boolean isResponsablePedagogique(EOStgStage stage) {
		if (stage == null || stage.primaryKey() == null) {
			return false;
		}
		Integer primaryKey = Integer.parseInt(stage.primaryKey());
		if (primaryKey == null) {
			return false;
		}

		Boolean isResp = mapIsResponsablePedagogiqueStage.get(primaryKey);
		if (isResp == null) {
			isResp = sdh.isResponsablePedagogique(stage, persId);
			mapIsResponsablePedagogiqueStage.put(primaryKey, isResp);
		}
		return isResp;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de modifier la demande
	 */
	public boolean hasDroitsModificationDemande(EOStgStage stage) {
		// Si l'utilisateur est l'étudiant et que la demande est toujours à l'état initial
		if (stage.isDemandeur(persId) && stage.isEtatInitial()) {
			return true;
		}
		// Si l'utilisateur est un responsable administratif
		if (isResponsableAdministratif(stage) && !stage.isEtatCourantApresEtatDonne(EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE)) {
			return true;
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de modifier le sujet pédagogique
	 */
	public boolean hasDroitsModificationSujetPedagogique(EOStgStage stage) {
		// Si l'utilisateur est un responsable pédagogique et que la demande n'est pas éditée
		if (isResponsablePedagogique(stage) && !stage.isEtatCourantApresEtatDonne(EOStgEtat.LC_ATTENTE_EDITION)) {
			return true;
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de demander la validation pédagogique
	 */
	public boolean hasDroitsDemandeValidationPedagogique(EOStgStage stage) {
		// La demande doit être dans l'état initial
		if (stage.isEtatInitial() && (stage.isDemandeur(persId) || isResponsableAdministratif(stage))) {
			return true;
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de valider/refuser pédagogiquement
	 */
	public boolean hasDroitsValidationPedagogique(EOStgStage stage) {
		// La demande de validation pédagogique doit être en attente
		if (stage.isEtatAttenteValidationPedagogique()) {
			return isResponsablePedagogique(stage);
		}

		// TODO gestion centralisée ou non ?
		// return isResponsablePedagogique(stage, persId);
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de dévalider la validation pédagogique
	 */
	public boolean hasDroitsDevalidationPedagogique(EOStgStage stage) {
		// La demande de validation pédagogique doit être en attente de tuteur ou de validation administative
		if (stage.isEtatAttenteDesignationTuteur() || stage.isEtatAttenteValidationAdministrative()) {
			return isResponsablePedagogique(stage);
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de valider/refuser administrativement
	 */
	public boolean hasDroitsValidationAdministrative(EOStgStage stage) {
		if (stage.isEtatAttenteValidationAdministrative()) {
			return isResponsableAdministratif(stage);
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit d'annuler la validation administrative
	 */
	public boolean hasDroitsDevalidationAdministrative(EOStgStage stage) {
		if (stage.isEtatAttenteEditionConvention()) {
			return isResponsableAdministratif(stage);
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit d'éditer la convention
	 */
	public boolean hasDroitsEditionConvention(EOStgStage stage) {
		if (stage.isEtatAttenteEditionConvention()) {
			return isResponsableAdministratif(stage);
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de consulter le fichier de la convention
	 */
	public boolean hasDroitsConsultationConvention(EOStgStage stage) {
		EOStgPieceJointeRepart pj = FinderStgPieceJointe.getUnePieceJointe(stage, FinderStgPieceJointeType.TYPE_CONVENTION);
		return pj != null && isResponsableAdministratif(stage);
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de consulter la convention
	 */
	public boolean hasDroitsConsultationStage(EOStgStage stage) {
		if (stage.isDemandeur(persId) || isResponsableAdministratif(stage) || isResponsablePedagogique(stage) || stage.isTuteur(persId) || isAdmin()) {
			return true;
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de verrouiller la convention après signature
	 */
	public boolean hasDroitsSignatures(EOStgStage stage) {
		if (stage.isEtatAttenteSignatures()) {
			return isResponsableAdministratif(stage);
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de supprimer le fichier de la convention
	 */
	public boolean hasDroitsSuppressionConvention(EOStgStage stage) {
		if (stage.isEtatAttenteSignatures()) {
			return isResponsableAdministratif(stage);
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de valider/refuser en tant que tuteur
	 */
	public boolean hasDroitsValidationTuteur(EOStgStage stage) {
		if (FinderStgConfig.getInstance().isTuteurValidation() && stage.isEtatAttenteValidationTuteur()) {
			return stage.isTuteur(persId);
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit d'annuler la validation en tant que tuteur
	 */
	public boolean hasDroitsAnnulationValidationTuteur(EOStgStage stage) {
		if (FinderStgConfig.getInstance().isTuteurValidation() && stage.isEtatAttenteValidationAdministrative()) {
			return stage.isTuteur(persId);
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de modifier le tuteur de la convention
	 */
	public boolean hasDroitsModificationTuteur(EOStgStage stage) {
		if (!stage.isEtatCourantApresEtatDonne(EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE)) {
			return isResponsablePedagogique(stage) || isResponsableAdministratif(stage) || stage.isDemandeur(persId);
		}
		return false;
	}

	/**
	 * @return <code>true</code> si l'utilisateur peut créer une convention
	 */
	public boolean hasDroitsCreationConvention() {
		EOEditingContext edc = ERXEC.newEditingContext();
		FactoryFinderScolarite factoryFinderScolarite = new FactoryFinderScolarite();
		FinderElementPedagogique finderElementPedagogique = factoryFinderScolarite.getFinderElementPedagogique(edc);
		Integer fannCourante = finderElementPedagogique.getAnneeUniversitaireCourante();
		return fannKey.equals(fannCourante) && (isEtudiant() || isResponsableAdministratif());
	}

	/**
	 * @param mec L'EC
	 * @return <code>true</code> si l'utilisateur a le droit de créer une convention dans l'EC
	 */
	public boolean hasDroitsCreationConventionForEc(EC mec) {
		return hasDroitsCreationConvention() && isResponsableAdministratif(mec);
	}

	/**
	 * @param etudiant L'Etudiant
	 * @return <code>true</code> si l'utilisateur a le droit de créer une convention dans l'EC
	 */
	public boolean hasDroitsCreationConventionForEtudiant(IEtudiant etudiant) {
		if (!hasDroitsCreationConvention()) {
			return false;
		}

		FactoryFinderScolarite factoryFinderScolarite = new FactoryFinderScolarite();
		FinderElementPedagogique finderElementPedagogique = factoryFinderScolarite.getFinderElementPedagogique(((EOEtudiant) etudiant).editingContext());

		List<EC> ecs = finderElementPedagogique.getListeECStage(etudiant.etudNumero(), fannKey);
		for (EC ec : ecs) {
			if (isResponsableAdministratif(ec)) {
				return true;
			}
		}
		return isResponsableAdministratif(etudiant);
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de supprimer complètement une convention
	 */
	public boolean hasDroitsSuppressionTotaleConvention(EOStgStage stage) {
		if (stage.isDemandeur(persId)) {
			return !stage.isEtatCourantApresEtatDonne(EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE);
		} else if (!stage.isEtatCourantApresEtatDonne(EOStgEtat.LC_ATTENTE_EDITION)) {
			return isResponsableAdministratif(stage);
		}
		return false;
	}

	/**
	 * @param stage La stage
	 * @return <code>true</code> si l'utilisateur a le droit d'annuler la suppression totale
	 */
	public boolean hasDroitsAnnulationSuppressionTotaleConvention(EOStgStage stage) {
		if (stage.isEtatConventionSupprimee()) {
			return isAdmin();
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de résilier la convention
	 */
	public boolean hasDroitsResiliationConvention(EOStgStage stage) {
		if (stage.isEtatConventionSignee()) {
			return isResponsableAdministratif(stage);
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit d'annuler la résiliation de la convention
	 */
	public boolean hasDroitsAnnulationResiliationConvention(EOStgStage stage) {
		if (stage.isEtatConventionResiliee()) {
			return isAdmin();
		}
		return false;
	}

	/**
	 * @param stage Le stage
	 * @return <code>true</code> si l'utilisateur a le droit de créer un avenant
	 */
	public boolean hasDroitsCreationAvenant(EOStgStage stage) {
		if (stage.isEtatConventionSignee()) {
			return isResponsableAdministratif(stage) || stage.isDemandeur(persId);
		}
		return false;
	}

	/**
	 * @param avenant L'avenant
	 * @return <code>true</code> si l'utilisateur a le droit de modifier l'avenant
	 */
	public boolean hasDroitsModificationAvenant(EOStgAvenant avenant) {
		if (!avenant.isSupprime() && !avenant.isVerrouille() && avenant.toStage().isEtatConventionSignee()) {
			if (avenant.toStage().isDemandeur(persId)) {
				avenant.persIdCreation().equals(persId);
			} else {
				return isResponsableAdministratif(avenant.toStage());
			}
		}
		return false;
	}

	/**
	 * @param avenant L'avenant
	 * @return <code>true</code> si l'utilisateur a le droit de valider l'avenant
	 */
	public boolean hasDroitsValidationAvenant(EOStgAvenant avenant) {
		if ((!avenant.isSupprime() && !avenant.isValide() && !avenant.isVerrouille()) && avenant.toStage().isEtatConventionSignee()) {
			return isResponsableAdministratif(avenant.toStage());
		}
		return false;
	}

	/**
	 * @param avenant L'avenant
	 * @return <code>true</code> si l'utilisateur a le droit de refuser l'avenant
	 */
	public boolean hasDroitsRefusAvenant(EOStgAvenant avenant) {
		if ((!avenant.isSupprime() && !avenant.isRefuse() && !avenant.isVerrouille()) && avenant.toStage().isEtatConventionSignee()) {
			return isResponsableAdministratif(avenant.toStage());
		}
		return false;
	}

	/**
	 * @param avenant L'avenant
	 * @return <code>true</code> si l'utilisateur a le droit de supprimer l'avenant
	 */
	public boolean hasDroitsSuppressionAvenant(EOStgAvenant avenant) {
		if (!avenant.isSupprime() && !avenant.isVerrouille() && avenant.toStage().isEtatConventionSignee()) {
			return isResponsableAdministratif(avenant.toStage());
		}
		return false;
	}

	/**
	 * @param avenant L'avenant
	 * @return <code>true</code> si l'utilisateur a le droit de verrouiller l'avenant
	 */
	public boolean hasDroitsVerrouillageAvenant(EOStgAvenant avenant) {
		if (!avenant.isSupprime() && avenant.isValide() && !avenant.isVerrouille() && avenant.toStage().isEtatConventionSignee()) {
			return isResponsableAdministratif(avenant.toStage());
		}
		return false;
	}

	/**
	 * @param avenant L'avenant
	 * @return <code>true</code> si l'utilisateur a le droit d'éditer l'avenant
	 */
	public boolean hasDroitsEditionAvenant(EOStgAvenant avenant) {
		if (!avenant.isSupprime() && avenant.isValide() && avenant.toStage().isEtatConventionSignee()) {
			return isResponsableAdministratif(avenant.toStage());
		}
		return false;
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @return La liste des {@link EOStgStage} pour lesquels l'utilisateur est enseignant
	 */
	public List<EOStgStage> getLesStagesByEnseignant(EOEditingContext edc) {
		return getLesStagesInEdc(edc, listeStagesEnseignant);
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @return La liste des {@link EOStgStage} pour lesquels l'utilisateur est secrétaire
	 */
	public List<EOStgStage> getLesStagesBySecretaire(EOEditingContext edc) {
		return getLesStagesInEdc(edc, listeStagesSecretaire);
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @return La liste des {@link EOStgStage} pour lesquels l'utilisateur est enseignant
	 */
	public List<EOStgStage> getLesStagesByResponsable(EOEditingContext edc) {
		return getLesStagesInEdc(edc, listeStagesResponsable);
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @return La liste des {@link EOScolMaquetteEc} pour lesquels l'utilisateur est enseignant
	 */
	public List<EC> getLesEcByEnseignant(EOEditingContext edc) {
		return listeEcEnseignant;
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @return La liste des {@link EOScolMaquetteEc} pour lesquels l'utilisateur est secrétaire
	 */
	public List<EC> getLesEcBySecretaire(EOEditingContext edc) {
		return listeEcSecretaire;
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @return La liste des {@link EOScolMaquetteEc} pour lesquels l'utilisateur est enseignant
	 */
	public List<EC> getLesEcByResponsable(EOEditingContext edc) {
		return listeEcResponsable;
	}

	public void setSdh(StagesDroits aSdh) {
		sdh = aSdh;
	}

	private List<EOStgStage> getLesStagesInEdc(EOEditingContext edc, List<EOStgStage> listeStagesCached) {
		List<EOStgStage> stages = new ArrayList<EOStgStage>();
		for (EOStgStage stage : listeStagesCached) {
			stages.add(stage.localInstanceIn(edc));
		}
		return stages;
	}

}
