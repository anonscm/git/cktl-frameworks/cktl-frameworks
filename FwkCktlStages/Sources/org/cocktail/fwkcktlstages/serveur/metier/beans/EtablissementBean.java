/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.beans;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class EtablissementBean extends StructureBean {

	/**
	 * Initialise le bean pour l'établissement
	 */
	public EtablissementBean() {
		super(EOStructure.rechercherEtablissement(new ERXEC()), getAdresseEtab());
	}

	/**
	 * @return Le libellé du bean
	 */
	public static String libelle() {
		return "etablissement";
	}

	/**
	 * @param uneEOStructure
	 *            La structure
	 * @param uneEOAdresse
	 *            L'adresse de la structure
	 */
	private EtablissementBean(EOStructure uneEOStructure, EOAdresse uneEOAdresse) {
		super(uneEOStructure, uneEOAdresse);
	}

	private static EOAdresse getAdresseEtab() {
		EOStructure etab = EOStructure.rechercherEtablissement(new ERXEC());
		if (etab.adresseProfessionnelle() != null) {
			return etab.adresseProfessionnelle().toAdresse();
		}

		EOQualifier qualType = ERXQ.or(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_FACT,
				EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_LIVR);
		EOQualifier qual = ERXQ.and(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE, qualType);

		NSArray res = etab.toRepartPersonneAdresses(qual, false);
		if (res.count() > 0) {
			return ((EORepartPersonneAdresse) res.objectAtIndex(0)).toAdresse();
		} else {
			throw new NSValidation.ValidationException("L'établissement n'a pas d'adresse (PRO, FACT, LIVR) valide !");
		}
	}
}
