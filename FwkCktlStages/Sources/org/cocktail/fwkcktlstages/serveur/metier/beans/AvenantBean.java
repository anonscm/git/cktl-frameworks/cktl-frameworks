/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.beans;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class AvenantBean extends ABean {

	private static Logger log = Logger.getLogger(AvenantBean.class);
	private final String stageDebut;
	private final String stageFin;
	private final String stageDuree;
	private final String interruptionDebut;
	private final String interruptionFin;
	private final String sujet;
	private final String gratification;

	// Adresse
	private final AdresseAvenantBean adresseAvenant;
	// Service
	private final ServiceAccueilAvenantBean serviceAvenant;
	// Etudiant
	private final EtudiantAvenantBean etudiantAvenant;
	// Etablissement
	private final EtablissementAvenantBean etablissementAvenant;
	// Organisme d'accueil
	private final OrganismeAccueilAvenantBean organismeAccueilAvenant;
	// Adresse etudiant
	private final AdresseEtudiantAvenantBean adresseEtudiantAvenant;

	/**
	 * @param avenant
	 *            Un {@link EOStgAvenant}
	 */
	public AvenantBean(EOStgAvenant avenant) {
		log.debug("AvenantBean :: Début de la contruction de l'avenant");

		stageDebut = DateCtrl.dateToString(avenant.savDStageDebut());
		stageFin = DateCtrl.dateToString(avenant.savDStageFin());
		stageDuree = StringCtrl.normalize(avenant.savDureeTotaleAffichage());

		interruptionDebut = DateCtrl.dateToString(avenant.savDInterDebut());
		interruptionFin = DateCtrl.dateToString(avenant.savDInterFin());

		sujet = StringCtrl.normalize(avenant.savSujet());
		gratification = StringCtrl.normalize(avenant.savGratificationMontantAffichage());

		adresseAvenant = new AdresseAvenantBean(avenant.toLieuAdresse());
		serviceAvenant = new ServiceAccueilAvenantBean(avenant.toService(), avenant.toServiceAdresse());
		etudiantAvenant = new EtudiantAvenantBean(avenant.toStage().toEtudiant(), avenant.toStage()
				.getNumeroTelephoneAffichage(), avenant.toStage().stgRefRespCivile());
		adresseEtudiantAvenant = new AdresseEtudiantAvenantBean(avenant.toStage().toAdresseEtudiant());
		etablissementAvenant = new EtablissementAvenantBean();
		organismeAccueilAvenant = new OrganismeAccueilAvenantBean(avenant.toStage().toOrganismeAccueil(), avenant
				.toStage().toAdresseStructureAccueil());

		log.debug("AvenantBean :: Fin de la contruction de l'avenant");
	}

	/**
	 * @return Le bean de l'adresse de l'avenant
	 */
	public AdresseAvenantBean toAdresseAvenant() {
		return adresseAvenant;
	}

	/**
	 * @return Le bean de l'étudiant
	 */
	public EtudiantAvenantBean toEtudiantAvenant() {
		return etudiantAvenant;
	}

	/**
	 * @return Le bean de l'établissement
	 */
	public EtablissementAvenantBean toEtablissementAvenant() {
		return etablissementAvenant;
	}

	/**
	 * @return Le bean de l'organisme d'accueil
	 */
	public OrganismeAccueilAvenantBean toOrganismeAccueilAvenant() {
		return organismeAccueilAvenant;
	}

	/**
	 * @return Le bean de l'adresse de l'étudiant
	 */
	public AdresseEtudiantAvenantBean toAdresseEtudiantAvenant() {
		return adresseEtudiantAvenant;
	}

	/**
	 * @return Le bean du service
	 */
	public ServiceAccueilAvenantBean toServiceAvenant() {
		return serviceAvenant;
	}

	public String getStageDebut() {
		return stageDebut;
	}

	public String getStageFin() {
		return stageFin;
	}

	public String getStageDuree() {
		return stageDuree;
	}

	public String getInterruptionDebut() {
		return interruptionDebut;
	}

	public String getInterruptionFin() {
		return interruptionFin;
	}

	public String getSujet() {
		return sujet;
	}

	public String getGratification() {
		return gratification;
	}

	/**
	 * @return Le libellé du bean
	 */
	public static String libelle() {
		return "avenant";
	}

	public Boolean getIsNouveauSujet() {
		return !StringCtrl.isEmpty(sujet);
	}

	public Boolean getIsNouvellesDates() {
		return !StringCtrl.isEmpty(stageDebut) || !StringCtrl.isEmpty(stageFin);
	}

	public Boolean getIsInterruption() {
		return !StringCtrl.isEmpty(interruptionDebut);
	}

	public Boolean getIsNouvelleGratification() {
		return !StringCtrl.isEmpty(gratification);
	}

	public Boolean getIsNouvelleAdresse() {
		return !StringCtrl.isEmpty(adresseAvenant.getAdresse1());
	}

	public Boolean getIsNouveauService() {
		return !StringCtrl.isEmpty(serviceAvenant.getNom());
	}

	/**
	 * Initialise une liste avec les champs utilisés, sous forme d'un tableau de String, avec des triplets <nom, classe,
	 * liste?><br>
	 * Utilisé lors de la génération du fichier XML project.fields.xml
	 * 
	 * @param liste
	 *            Une liste
	 */
	public static void initFields(List<String> liste) {
		liste.add(AvenantBean.libelle());
		liste.add(AvenantBean.class.getName());
		liste.add("false");

		liste.add(AdresseAvenantBean.libelle());
		liste.add(AdresseAvenantBean.class.getName());
		liste.add("false");

		liste.add(ServiceAccueilAvenantBean.libelle());
		liste.add(ServiceAccueilAvenantBean.class.getName());
		liste.add("false");

		liste.add(EtudiantAvenantBean.libelle());
		liste.add(EtudiantAvenantBean.class.getName());
		liste.add("false");

		liste.add(OrganismeAccueilAvenantBean.libelle());
		liste.add(OrganismeAccueilAvenantBean.class.getName());
		liste.add("false");

		liste.add(EtablissementAvenantBean.libelle());
		liste.add(EtablissementAvenantBean.class.getName());
		liste.add("false");

		liste.add(AdresseEtudiantAvenantBean.libelle());
		liste.add(AdresseEtudiantAvenantBean.class.getName());
		liste.add("false");
	}

	/**
	 * Initialise la liste des bean utilisés, avec leur libellé utilisé dans le document
	 * 
	 * @param params
	 *            Une Map
	 */
	public void initFieldsMap(Map params) {
		params.put(AvenantBean.libelle(), this);
		params.put(AdresseAvenantBean.libelle(), toAdresseAvenant());
		params.put(ServiceAccueilAvenantBean.libelle(), toServiceAvenant());
		params.put(EtudiantAvenantBean.libelle(), toEtudiantAvenant());
		params.put(OrganismeAccueilAvenantBean.libelle(), toOrganismeAccueilAvenant());
		params.put(EtablissementAvenantBean.libelle(), toEtablissementAvenant());
		params.put(AdresseEtudiantAvenantBean.libelle(), toAdresseEtudiantAvenant());
	}
}
