/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.beans;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgAdresse;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import er.extensions.eof.ERXEC;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class ConventionBean extends ABean {

	private static Logger log = Logger.getLogger(ConventionBean.class);

	private final String sujet;
	private final String activites;
	private final String debut;
	private final String fin;
	private final String dureeEffective;
	private final String dureeCommentaires;
	private final String dureeHebdomadaire;
	private final String gratification;
	private final String gratificationModalites;
	private final String gratificationAvantages;
	private final String isGratificationObligatoire;
	private final String anneeUniversitaire;
	private final String composanteUFR;
	private final String creditsECTS;

	// Etudiant
	private final EtudiantBean etudiant;
	// Inscription de l'étudiant
	private final EtudiantInscriptionBean etudiantInscription;
	// Tuteur
	private final TuteurBean tuteur;
	// Encadrant
	private final EncadrantBean encadrant;
	// Adresse etudiant
	private final AdresseEtudiantBean adresseEtudiant;
	// Liste d'absences
	private final List<AbsenceBean> stageAbsences;
	// FIXME Gérer la liste d'horaires
	private final String stageHoraires = "";
	// Adresses du lieu du stage
	private final List<AdresseStageBean> adressesStage;
	// Etablissement
	private final EtablissementBean etablissement;
	// Organisme d'accueil
	private final OrganismeAccueilBean organismeAccueil;
	// Service d'accueil
	private final ServiceAccueilBean serviceAccueil;
	// Adresse de la composante/UFR attachée à l'EC
	private final AdresseComposanteUFR adresseComposanteUFR;

	private final String type;

	/**
	 * @param aStage Un {@link EOStgStage}
	 * @throws Exception
	 */
	public ConventionBean(EOStgStage aStage) {
		log.debug("ConventionBean :: Debut de construction de la convention");
		// L'étudiant
		etudiant = new EtudiantBean(aStage.toEtudiant(), aStage.getNumeroTelephoneAffichage(), aStage.stgRefRespCivile());
		log.debug("ConventionBean :: EtudiantBean ok");
		// Inscription
		etudiantInscription = new EtudiantInscriptionBean(aStage.toEtudiant(), aStage.toAnnee().sanDebut(), aStage.mecKey());
		log.debug("ConventionBean :: EtudiantInscriptionBean ok");
		// Adresse étudiant
		adresseEtudiant = new AdresseEtudiantBean(aStage.toAdresseEtudiant());
		log.debug("ConventionBean :: AdresseEtudiantBean ok");
		// Le tuteur
		tuteur = new TuteurBean(aStage.toTuteur());
		log.debug("ConventionBean :: TuteurBean ok");
		// L'encadrant
		encadrant = new EncadrantBean(aStage.toEncadrant());
		log.debug("ConventionBean :: EncadrantBean ok");
		// Les absences
		stageAbsences = new ArrayList<AbsenceBean>();
		for (EOStgAbsence uneAbsence : aStage.toAbsences()) {
			stageAbsences.add(new AbsenceBean(uneAbsence));
		}
		log.debug("ConventionBean :: AbsenceBean ok");
		// Les horaires
		// TODO Les horaires

		// Les adresses du lieu du stage
		adressesStage = new ArrayList<AdresseStageBean>();
		for (EOStgAdresse uneStgAdresse : aStage.toAdresses()) {
			adressesStage.add(new AdresseStageBean(uneStgAdresse.toFwkpers_Adresse()));
		}
		log.debug("ConventionBean :: AdresseStageBean ok");
		// Etablissement
		etablissement = new EtablissementBean();
		log.debug("ConventionBean :: EtablissementBean ok");
		// L'organisme d'accueil
		organismeAccueil = new OrganismeAccueilBean(aStage.toOrganismeAccueil(), aStage.toAdresseStructureAccueil());
		log.debug("ConventionBean :: OrganismeAccueilBean ok");
		// Le service d'accueil
		serviceAccueil = new ServiceAccueilBean(aStage.toService(), aStage.toAdresseServiceAccueil());
		log.debug("ConventionBean :: ServiceAccueilBean ok");

		sujet = aStage.stgSujet();
		activites = aStage.stgActivites();

		debut = DateCtrl.dateToString(aStage.stgDDebut());
		fin = DateCtrl.dateToString(aStage.stgDFin());
		dureeHebdomadaire = aStage.stgDureeHebdo().toString();
		dureeEffective = aStage.stgDureeTotaleVal().toString() + " " + aStage.stgDureeTotaleUniteAffichage();
		dureeCommentaires = StringCtrl.normalize(aStage.stgDureeTotaleCommentaire());

		DecimalFormat df = new DecimalFormat("0.00");

		gratification = df.format(aStage.stgGratMontant()).toString() + " " + aStage.toDeviseGratification().llDevise();

		gratificationModalites = aStage.toModeVersementGratification().modLibelle();
		gratificationAvantages = StringCtrl.normalize(aStage.stgGratAvantages());
		isGratificationObligatoire = aStage.stgGratObligatoire();

		type = aStage.toType().styLl();

		anneeUniversitaire = aStage.toAnnee().affichage();

		// les informations relatives à l'EC
		EOStructure composanteEC = getComposanteUFR(aStage.getEc());
		composanteUFR = getLibelleComposanteUFR(composanteEC);
		adresseComposanteUFR = new AdresseComposanteUFR(getAdresseComposante(composanteEC));
		creditsECTS = getCreditsECTS(aStage.getEc());

		log.debug("ConventionBean :: Fin de construction de la convention");
	}

	private String getCreditsECTS(EC ec) {
		if (ec == null || ec.getCreditECTS() == null) {
			return "";
		}
		DecimalFormat decimalFormat = new DecimalFormat();
		return decimalFormat.format(ec.getCreditECTS());
	}

	private EOAdresse getAdresseComposante(EOStructure composante) {
		if (composante == null) {
			return null;
		}

		if (composante.toRepartPersonneAdresses().size() > 0) {
			EORepartPersonneAdresse repartPersonneAdresse = composante.toRepartPersonneAdresses().get(0);
			return repartPersonneAdresse.toAdresse();
		}

		return null;
	}

	private String getLibelleComposanteUFR(EOStructure composante) {
		if (composante == null) {
			return "";
		}
		return composante.libelle();
	}

	private EOStructure getComposanteUFR(EC ec) {
		if (ec == null || ec.getComposanteId() == null) {
			return null;
		}
		return EOStructure.fetchByKeyValue(new ERXEC(), EOStructure.PERS_ID_KEY, ec.getComposanteId());
	}

	/**
	 * @return Le libellé du bean
	 */
	public static String libelle() {
		return "stage";
	}

	// ************* //
	// *** BEANS *** //
	// ************* //

	/**
	 * @return Le bean contenant les données de l'étudiant
	 */
	public EtudiantBean toEtudiant() {
		return etudiant;
	}

	/**
	 * @return Le bean contenant les données sur l'inscription de l'étudiant
	 */
	public EtudiantInscriptionBean toEtudiantInscription() {
		return etudiantInscription;
	}

	/**
	 * @return Le bean contenant les données du tuteur
	 */
	public TuteurBean toTuteur() {
		return tuteur;
	}

	/**
	 * @return Le bean contenant les données de l'encadrant
	 */
	public EncadrantBean toEncadrant() {
		return encadrant;
	}

	/**
	 * @return Le bean contenant l'adresse de l'étudiant(e)
	 */
	public AdresseEtudiantBean toAdresseEtudiant() {
		return adresseEtudiant;
	}

	/**
	 * @return La liste des beans contenant les données des absences
	 */
	public List<AbsenceBean> toAbsencesBeans() {
		return stageAbsences;
	}

	/**
	 * @return La liste des beans contenant les adresses du stage
	 */
	public List<AdresseStageBean> toAdressesStage() {
		return adressesStage;
	}

	/**
	 * @return Le bean contenant l'établissement d'origine de l'étudiant
	 */
	public EtablissementBean toEtablissement() {
		return etablissement;
	}

	/**
	 * @return Le bean contenant l'organisme d'accueil de l'étudiant
	 */
	public OrganismeAccueilBean toOrganismeAccueil() {
		return organismeAccueil;
	}

	/**
	 * @return Le bean contenant le service d'accueil de l'étudiant
	 */
	public ServiceAccueilBean toServiceAccueil() {
		return serviceAccueil;
	}

	/**
	 * @return Le bean contenant l'adresse de l'étudiant(e)
	 */
	public AdresseComposanteUFR toAdresseAdresseComposanteUFR() {
		return adresseComposanteUFR;
	}

	// *************** //
	// *** GETTERS *** //
	// *************** //

	/**
	 * @return the stageSujet
	 */
	public String getSujet() {
		return sujet;
	}

	/**
	 * @return the stageActivites
	 */
	public String getActivites() {
		return activites;
	}

	/**
	 * @return the stageDebut
	 */
	public String getDebut() {
		return debut;
	}

	/**
	 * @return the stageFin
	 */
	public String getFin() {
		return fin;
	}

	/**
	 * @return the stageDureeEffective
	 */
	public String getDureeEffective() {
		return dureeEffective;
	}

	/**
	 * @return the stageDureeCommentaires
	 */
	public String getDureeCommentaires() {
		return dureeCommentaires;
	}

	/**
	 * @return the stageHoraires
	 */
	public String getHoraires() {
		return stageHoraires;
	}

	/**
	 * @return the stageGratification
	 */
	public String getGratification() {
		return gratification;
	}

	/**
	 * @return the stageGratificationModalites
	 */
	public String getGratificationModalites() {
		return gratificationModalites;
	}

	/**
	 * @return the stageGratificationAvantages
	 */
	public String getGratificationAvantages() {
		return gratificationAvantages;
	}

	/**
	 * @return the gratificationObligatoire
	 */
	public String getIsGratificationObligatoire() {
		return isGratificationObligatoire;
	}

	/**
	 * @return the stageType
	 */
	public String getType() {
		return type;
	}

	public String getAnneeUniversitaire() {
		return anneeUniversitaire;
	}

	public String getComposanteUFR() {
		return composanteUFR;
	}

	public String getDureeHebdomadaire() {
		return dureeHebdomadaire;
	}

	public String getCreditsECTS() {
		return creditsECTS;
	}

	// *************** //
	// *** HELPERS *** //
	// *************** //
	/**
	 * Initialise une liste avec les champs utilisés, sous forme d'un tableau de String, avec des triplets <nom, classe, liste?><br>
	 * Utilisé lors de la génération du fichier XML project.fields.xml
	 * 
	 * @param liste Une liste
	 */
	public static void initFields(List<String> liste) {
		liste.add(ConventionBean.libelle());
		liste.add(ConventionBean.class.getName());
		liste.add("false");

		liste.add(EtudiantBean.libelle());
		liste.add(EtudiantBean.class.getName());
		liste.add("false");

		liste.add(EtudiantInscriptionBean.libelle());
		liste.add(EtudiantInscriptionBean.class.getName());
		liste.add("false");

		liste.add(AdresseEtudiantBean.libelle());
		liste.add(AdresseEtudiantBean.class.getName());
		liste.add("false");

		liste.add(TuteurBean.libelle());
		liste.add(TuteurBean.class.getName());
		liste.add("false");

		liste.add(EncadrantBean.libelle());
		liste.add(EncadrantBean.class.getName());
		liste.add("false");

		liste.add(AbsenceBean.libelle());
		liste.add(AbsenceBean.class.getName());
		liste.add("true");

		liste.add(AdresseStageBean.libelle());
		liste.add(AdresseStageBean.class.getName());
		liste.add("true");

		liste.add(EtablissementBean.libelle());
		liste.add(EtablissementBean.class.getName());
		liste.add("false");

		liste.add(OrganismeAccueilBean.libelle());
		liste.add(OrganismeAccueilBean.class.getName());
		liste.add("false");

		liste.add(ServiceAccueilBean.libelle());
		liste.add(ServiceAccueilBean.class.getName());
		liste.add("false");

		liste.add(AdresseComposanteUFR.libelle());
		liste.add(AdresseComposanteUFR.class.getName());
		liste.add("false");
	}

	/**
	 * Initilise une liste avec les champs qui peuvent être iterables dans le document
	 * 
	 * @param liste Une liste
	 */
	public static void initFieldsAsList(List<String> liste) {
		liste.addAll(ABean.retrieveFields("absences.", AbsenceBean.class));
		liste.addAll(ABean.retrieveFields("adressesStage.", AdresseBean.class));
	}

	/**
	 * Initialise la liste des bean utilisés, avec leur libellé utilisé dans le document
	 * 
	 * @param params Une Map
	 */
	public void initFieldsMap(Map params) {
		params.put(ConventionBean.libelle(), this);
		params.put(EtudiantBean.libelle(), toEtudiant());
		params.put(EtudiantInscriptionBean.libelle(), toEtudiantInscription());
		params.put(AdresseEtudiantBean.libelle(), toAdresseEtudiant());
		params.put(TuteurBean.libelle(), toTuteur());
		params.put(EncadrantBean.libelle(), toEncadrant());
		params.put(AbsenceBean.libelle(), toAbsencesBeans());
		params.put(AdresseStageBean.libelle(), toAdressesStage());
		params.put(EtablissementBean.libelle(), toEtablissement());
		params.put(OrganismeAccueilBean.libelle(), toOrganismeAccueil());
		params.put(ServiceAccueilBean.libelle(), toServiceAccueil());
		params.put(AdresseComposanteUFR.libelle(), toAdresseAdresseComposanteUFR());
	}
}