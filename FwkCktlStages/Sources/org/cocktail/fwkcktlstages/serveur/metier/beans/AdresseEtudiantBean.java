package org.cocktail.fwkcktlstages.serveur.metier.beans;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class AdresseEtudiantBean extends AdresseBean {
	/**
	 * @param adresse
	 *            L'adresse de l'étudiant
	 */
	public AdresseEtudiantBean(EOAdresse adresse) {
		super(adresse);
	}

	/**
	 * @return Le libellé du bean
	 */
	public static String libelle() {
		return "adresseEtudiant";
	}
}
