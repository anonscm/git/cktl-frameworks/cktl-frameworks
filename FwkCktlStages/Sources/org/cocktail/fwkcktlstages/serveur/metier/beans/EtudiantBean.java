/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.beans;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class EtudiantBean extends IndividuBean {
	private final String numero;
//	private final String emailPersonnel = "";
	private final String telephone;
	private final String dateDeNaissance;

	private final String referenceContratRespCivile;

	/**
	 * @param etudiant
	 *            Un {@link EOEtudiant}
	 * @param unTelephone
	 *            Le téléphone
	 * @param refRespCivile
	 *            La référence au contrat de responsabilité civile
	 */
	public EtudiantBean(EOEtudiant etudiant, String unTelephone, String refRespCivile) {
		super(etudiant.toIndividu(), EOVlans.VLAN_E);

		numero = etudiant.etudNumero().toString();
		dateDeNaissance = DateCtrl.dateToString(etudiant.toIndividu().dNaissance());
		telephone = unTelephone;
		referenceContratRespCivile = StringCtrl.normalize(refRespCivile);
	}

	/**
	 * @return Le libellé du bean
	 */
	public static String libelle() {
		return "etudiant";
	}

	// *************** //
	// *** GETTERS *** //
	// *************** //

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

//	/**
//	 * @return the emailPersonnel
//	 */
//	public String getEmailPersonnel() {
//		return emailPersonnel;
//	}

	/**
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @return the dateDeNaissance
	 */
	public String getDateDeNaissance() {
		return dateDeNaissance;
	}

	/**
	 * @return the referenceContratRespCivile
	 */
	public String getReferenceContratRespCivile() {
		return referenceContratRespCivile;
	}
}
