package org.cocktail.fwkcktlstages.serveur.metier.beans;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;

/**
 * adresse de la composante (UFR) attachée à l'EC stage
 *
 */
public class AdresseComposanteUFR extends AdresseBean {

	/**
	 * @param adresse
	 *            L'adresse de la composante/UFR
	 */
	public AdresseComposanteUFR(EOAdresse adresse) {
	  super(adresse);
  }
	
	/**
	 * @return Le libellé du bean
	 */
	public static String libelle() {
		return "adresseComposanteUFR";
	}

}
