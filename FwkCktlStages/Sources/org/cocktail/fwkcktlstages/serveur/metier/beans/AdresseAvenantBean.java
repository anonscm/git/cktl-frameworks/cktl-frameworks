package org.cocktail.fwkcktlstages.serveur.metier.beans;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class AdresseAvenantBean extends AdresseBean {

	/**
	 * @param adresse
	 *            L'adresse présente sur l'avenant
	 */
	public AdresseAvenantBean(EOAdresse adresse) {
		super(adresse);
	}

	/**
	 * @return Le libellé du bean
	 */
	public static String libelle() {
		return "adresseAvenant";
	}
}
