/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.beans;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

/**
 * Classe abstraite permettant d'initialiser un bean Adresse. <br>
 * Vu que l'on ne peut pas utiliser deux fois la même classe pour deux varibles différentes lors de la génération du
 * project.fields.xml, on la dérive alors à chaque fois que l'on en a besoin (adresse étudiant, adresse du stage, ...)
 * 
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public abstract class AdresseBean extends ABean {
	private final String adresse1;
	private final String adresse2;
	private final String boitePostale;
	private final String codePostal;
	private final String ville;
	private final String pays;

	/**
	 * @param adresse
	 *            Une {@link EOAdresse}
	 */
	public AdresseBean(EOAdresse adresse) {
		if (adresse == null) {
			adresse1 = "";
			adresse2 = "";
			boitePostale = "";
			codePostal = "";
			ville = "";
			pays = "";
			return;
		}
		adresse1 = adresse.adrAdresse1();
		adresse2 = StringCtrl.normalize(adresse.adrAdresse2());
		boitePostale = StringCtrl.normalize(adresse.adrBp());
		codePostal = StringCtrl.normalize(adresse.getCPCache());
		ville = adresse.ville();
		pays = adresse.toPays().llPays();
	}

	// *************** //
	// *** GETTERS *** //
	// *************** //

	/**
	 * @return the adresse1
	 */
	public String getAdresse1() {
		return adresse1;
	}

	/**
	 * @return the adresse2
	 */
	public String getAdresse2() {
		return adresse2;
	}

	/**
	 * @return the boitePostale
	 */
	public String getBoitePostale() {
		return boitePostale;
	}

	/**
	 * @return the codePostal
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * @return the pays
	 */
	public String getPays() {
		return pays;
	}
}
