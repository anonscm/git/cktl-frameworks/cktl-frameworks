/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.beans;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.webobjects.foundation.NSArray;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public abstract class ABean {

	/**
	 * @return Le libellé du bean
	 */
	public static String libelle() {
		throw new RuntimeException("La classe doit implémenter la méthode statique libelle()");
	}

	/**
	 * Méthode nécessaire lorsque l'on utilise des listes de beans ( {@link ConventionBean#toAdressesStage()} par
	 * exemple)
	 * 
	 * @param pre
	 *            Une chaine à ajouter au début de chaque champs
	 * @param T
	 *            La classe bean à utiliser
	 * @return La liste des getters (sans le "get" ...) pour renseigner le fieldsAsList
	 * @see fr.opensagres.xdocreport.template.formatter.FieldsMetadata
	 */
	public static List<String> retrieveFields(String pre, Class T) {
		final String get = "get";
		List<String> fieldsList = new ArrayList<String>();
		NSArray<Method> methodes = new NSArray<Method>(T.getDeclaredMethods());
		for (Method method : methodes) {
			if (method.getName().startsWith(get)) {
				fieldsList.add(pre + method.getName().substring(get.length()));
			}
		}
		return fieldsList;
	}
}
