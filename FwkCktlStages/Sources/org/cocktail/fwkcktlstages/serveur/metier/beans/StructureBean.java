/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.beans;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public abstract class StructureBean extends ABean {

	private final String nom;
	private final String representant;
	private final String representantQualite;
	private final String adresse;
	private final String codePostal;
	private final String boitePostale;
	private final String ville;
	private final String pays;
	private final String telephone;
	private final String fax;
	private final String email;

	/**
	 * Initialise un bean pour la eoStructure passée en paramètre
	 * 
	 * @param uneEOStructure
	 *            La eoStructure
	 * @param uneEOAdresse
	 *            L'adresse de la eoStructure
	 */
	public StructureBean(EOStructure uneEOStructure, EOAdresse uneEOAdresse) {

		nom = initNom(uneEOStructure, uneEOAdresse);
		representant = initRepresentant(uneEOStructure, uneEOAdresse);
		representantQualite = initRepresentantQualite(uneEOStructure, uneEOAdresse);
		telephone = initTelephone(uneEOStructure, uneEOAdresse);
		fax = initFax(uneEOStructure, uneEOAdresse);

		email = initEmail(uneEOStructure, uneEOAdresse);
		adresse = initAdresse(uneEOStructure, uneEOAdresse);
		codePostal = initCodePostal(uneEOStructure, uneEOAdresse);
		boitePostale = initBoitePostale(uneEOStructure, uneEOAdresse);
		ville = initVille(uneEOStructure, uneEOAdresse);
		pays = initPays(uneEOStructure, uneEOAdresse);

	}

	private String initPays(EOStructure eoStructure, EOAdresse eoAdresse) {
		if (eoStructure == null || eoAdresse == null) {
			return "";
		}
		return eoAdresse.toPays().llPays();
	}

	private String initVille(EOStructure eoStructure, EOAdresse eoAdresse) {
		if (eoStructure == null || eoAdresse == null) {
			return "";
		}
		return eoAdresse.ville();
	}

	private String initFax(EOStructure eoStructure, EOAdresse eoAdresse) {
		if (eoStructure == null) {
			return "";
		}
		List<EOPersonneTelephone> faxs = eoStructure.toPersonneTelephones(ERXQ.and(
				EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_PRF_OK, EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_FAX),
				new ERXSortOrderings(EOPersonneTelephone.SORT_TEL_PRINCIPAL_DESC), true);
		if (!faxs.isEmpty()) {
			return faxs.get(0).getTelephoneFormateAvecIndicatif();
		} else {
			return "";
		}
	}

	private String initTelephone(EOStructure eoStructure, EOAdresse eoAdresse) {
		if (eoStructure == null) {
			return "";
		}
		List<EOPersonneTelephone> tels = eoStructure.toPersonneTelephones(ERXQ
				.and(EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_PRF_OK,
						EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_NON_FAX), new ERXSortOrderings(
				EOPersonneTelephone.SORT_TEL_PRINCIPAL_DESC), true);
		if (!tels.isEmpty()) {
			return tels.get(0).getTelephoneFormateAvecIndicatif();
		} else {
			return "";
		}
	}

	private String initBoitePostale(EOStructure eoStructure, EOAdresse eoAdresse) {
		if (eoStructure == null || eoAdresse == null || StringCtrl.isEmpty(eoAdresse.adrBp())) {
			return "";
		} else {
			return "BP: " + eoAdresse.adrBp();
		}
	}

	private String initCodePostal(EOStructure eoStructure, EOAdresse eoAdresse) {
		if (eoStructure == null || eoAdresse == null || StringCtrl.isEmpty(eoAdresse.codePostal())) {
			return "";
		} else {
			return eoAdresse.codePostal();
		}
	}

	private String initAdresse(EOStructure eoStructure, EOAdresse eoAdresse) {
		if (eoStructure == null || eoAdresse == null) {
			return "";
		}
		if (eoAdresse.adrAdresse2() != null) {
			return eoAdresse.adrAdresse1() + "\n" + eoAdresse.adrAdresse2();
		} else {
			return eoAdresse.adrAdresse1();
		}
	}

	private String initEmail(EOStructure eoStructure, EOAdresse eoAdresse) {
		if (eoStructure == null || eoAdresse == null) {
			return "";
		}
		EORepartPersonneAdresse repartadresse = getRepartPersonneAdresse(eoStructure, eoAdresse);
		if (repartadresse != null) {
			return StringCtrl.normalize(repartadresse.eMail());
		} else {
			return "";
		}
	}

	private EORepartPersonneAdresse getRepartPersonneAdresse(EOStructure eoStructure, EOAdresse eoAdresse) {
		EOQualifier qual = ERXQ.equals(EORepartPersonneAdresse.ADR_ORDRE_KEY, eoAdresse.adrOrdre());
		NSArray<EORepartPersonneAdresse> reps = eoStructure.toRepartPersonneAdresses(qual);
		if (reps.count() == 0) {
			throw new NSValidation.ValidationException(
					"L'adresse donnée ne correspond pas à une adresse associée à la eoStructure " + nom);
		}
		EORepartPersonneAdresse repartadresse = eoStructure.toRepartPersonneAdresses(qual).objectAtIndex(0);
		return repartadresse;
	}

	private String initNom(EOStructure eoStructure, EOAdresse eoAdresse) {
		if (eoStructure == null) {
			return "";
		}
		if (isStructureEstEtablissement(eoStructure, eoAdresse)) {
			return getConfigValue("GRHUM_ETAB");
		}
		return eoStructure.libelle();
	}

	private String initRepresentant(EOStructure eoStructure, EOAdresse eoAdresse) {
		if (eoStructure == null) {
			return "";
		}
		if (isStructureEstEtablissement(eoStructure, eoAdresse)) {
			return StringCtrl.capitalizeWords(eoStructure.toResponsable().getNomCompletAffichage());
		} else if (eoStructure.toResponsable() != null) {
			return StringCtrl.capitalizeWords(eoStructure.toResponsable().getNomCompletAffichage());
		}
		return "";
	}

	private String initRepresentantQualite(EOStructure eoStructure, EOAdresse eoAdresse) {
		if (eoStructure == null) {
			return "";
		}
		if (isStructureEstEtablissement(eoStructure, eoAdresse)) {
			return StringCtrl.initCap(getConfigValue("GRHUM_PRESIDENT"));
		}

		if (eoStructure.toResponsable() != null) {
			List<EORepartAssociation> rassList = eoStructure.toRepartAssociations(ERXQ.equals(
					EORepartAssociation.TO_INDIVIDUS_ASSOCIES_KEY, eoStructure.toResponsable()));
			if (rassList != null && rassList.size() > 0) {
				EORepartAssociation rass = rassList.get(0);
				return rass.toAssociation().assLibelle();
			}
			if (eoStructure.toResponsable().indQualite()!=null){
				return eoStructure.toResponsable().indQualite();
			}
		}
		return "";
	}

	private boolean isStructureEstEtablissement(EOStructure eoStructure, EOAdresse eoAdresse) {
		return EOStructure.rechercherEtablissement(eoStructure.editingContext()).equals(eoStructure);
	}

	private static String getConfigValue(String key) {
		return CktlParamManager.getApplication().config().stringForKey(key);
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @return the representant
	 */
	public String getRepresentant() {
		return representant;
	}

	/**
	 * @return the representantQualite
	 */
	public String getRepresentantQualite() {
		return representantQualite;
	}

	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * @return the codePostal
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * @return the boitePostale
	 */
	public String getBoitePostale() {
		return boitePostale;
	}

	/**
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * @return the pays
	 */
	public String getPays() {
		return pays;
	}

	/**
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
}
