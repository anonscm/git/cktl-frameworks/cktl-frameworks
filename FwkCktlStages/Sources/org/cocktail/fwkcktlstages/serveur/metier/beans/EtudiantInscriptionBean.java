/*
 * Copyright Cocktail, 2001-2012
 *  
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.beans;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlstages.serveur.finder.FactoryFinderScolarite;
import org.cocktail.fwkcktlstages.serveur.finder.FinderElementPedagogique;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.Diplome;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.InscriptionAdministrative;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class EtudiantInscriptionBean extends ABean {

	private final String anneeFormation;
	private final String diplome;
	private final String ec;
	private final String filiere;
	private final String parcours = "";
	private final String specialisation;
	private final String ue = "";
	private final String libelleDiplomeComplet;

	/**
	 * @param etudiant Un {@link EOEtudiant}
	 * @param annee Une année
	 * @param mecKey Un {@link EOScolMaquetteEc}
	 */
	public EtudiantInscriptionBean(EOEtudiant etudiant, Integer annee, Integer mecKey) {
		FactoryFinderScolarite factoryFinderScolarite = new FactoryFinderScolarite();
		FinderElementPedagogique finderElementPedagogique = factoryFinderScolarite.getFinderElementPedagogique(etudiant.editingContext());

		InscriptionAdministrative inscPrincipale = finderElementPedagogique.getInscriptionPrincipale(etudiant, annee);
		libelleDiplomeComplet = inscPrincipale.getLibelleDiplomeComplet();
		anneeFormation = inscPrincipale.getNiveau().toString();
		Diplome inscdiplome = inscPrincipale.getDiplome();
		diplome = inscdiplome.getLibelle();

		specialisation = StringCtrl.normalize(inscdiplome.getLibelleParcours());
		filiere = inscdiplome.getGradeAbreviation();

		if (mecKey != null) {
			EC mec = finderElementPedagogique.getEC(mecKey);
			ec = mec.toString();
		} else {
			ec = "";
		}
	}

	/**
	 * @return Le libellé du bean
	 */
	public static String libelle() {
		return "etudiantInscription";
	}

	/**
	 * @return the filiere
	 */
	public String getFiliere() {
		return filiere;
	}

	/**
	 * @return the anneeFormation
	 */
	public String getAnneeFormation() {
		return anneeFormation;
	}

	/**
	 * @return the diplome
	 */
	public String getDiplome() {
		return diplome;
	}

	/**
	 * @return the ec
	 */
	public String getEc() {
		return ec;
	}

	/**
	 * @return the parcours
	 */
	public String getParcours() {
		return parcours;
	}

	/**
	 * @return the specialisation
	 */
	public String getSpecialisation() {
		return specialisation;
	}

	/**
	 * @return the ue
	 */
	public String getUe() {
		return ue;
	}

	public String getLibelleDiplomeComplet() {
		return libelleDiplomeComplet;
	}
}
