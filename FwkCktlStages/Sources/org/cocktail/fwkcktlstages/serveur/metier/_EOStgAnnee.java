/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStgAnnee.java instead.
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.util.Enumeration;
import java.util.NoSuchElementException;
// import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStgAnnee extends  AFwkCktlStagesRecord {
	//	private static Logger logger = Logger.getLogger(_EOStgAnnee.class);

	public static final String ENTITY_NAME = "FwkCktlStages_StgAnnee";
	public static final String ENTITY_TABLE_NAME = "STAGES.STG_ANNEE";

	// Attribute Keys
	public static final ERXKey<String> SAN_COURANTE = new ERXKey<String>("sanCourante");
	public static final ERXKey<Integer> SAN_DEBUT = new ERXKey<Integer>("sanDebut");
	public static final ERXKey<Integer> SAN_FIN = new ERXKey<Integer>("sanFin");

	// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> TO_STAGES = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>("toStages");

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "sanId";

	public static final String SAN_COURANTE_KEY = "sanCourante";
	public static final String SAN_DEBUT_KEY = "sanDebut";
	public static final String SAN_FIN_KEY = "sanFin";

	// Attributs non visibles
	public static final String SAN_ID_KEY = "sanId";

	// Colonnes dans la base de donnees
	public static final String SAN_COURANTE_COLKEY = "SAN_COURANTE";
	public static final String SAN_DEBUT_COLKEY = "SAN_DEBUT";
	public static final String SAN_FIN_COLKEY = "SAN_FIN";

	public static final String SAN_ID_COLKEY = "SAN_ID";

	// Relationships
	public static final String TO_STAGES_KEY = "toStages";


	// Accessors methods
	public String sanCourante() {
		return (String) storedValueForKey(SAN_COURANTE_KEY);
	}

	public void setSanCourante(String value) {
		takeStoredValueForKey(value, SAN_COURANTE_KEY);
	}

	public Integer sanDebut() {
		return (Integer) storedValueForKey(SAN_DEBUT_KEY);
	}

	public void setSanDebut(Integer value) {
		takeStoredValueForKey(value, SAN_DEBUT_KEY);
	}

	public Integer sanFin() {
		return (Integer) storedValueForKey(SAN_FIN_KEY);
	}

	public void setSanFin(Integer value) {
		takeStoredValueForKey(value, SAN_FIN_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> toStages() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>) storedValueForKey(TO_STAGES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> toStages(EOQualifier qualifier) {
		return toStages(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> toStages(EOQualifier qualifier, boolean fetch) {
		return toStages(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> toStages(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage.TO_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgStage.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = toStages();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToToStagesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_STAGES_KEY);
	}

	public void removeFromToStagesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_STAGES_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgStage createToStagesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgStage");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, TO_STAGES_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgStage) eo;
	}

	public void deleteToStagesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_STAGES_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllToStagesRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> objects = toStages().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToStagesRelationship(objects.nextElement());
		}
	}


	/**
	 * Créer une instance de EOStgAnnee avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOStgAnnee createEOStgAnnee(EOEditingContext editingContext, String sanCourante
, Integer sanDebut
, Integer sanFin
			) {
		EOStgAnnee eo = (EOStgAnnee) createAndInsertInstance(editingContext, _EOStgAnnee.ENTITY_NAME);
		eo.setSanCourante(sanCourante);
		eo.setSanDebut(sanDebut);
		eo.setSanFin(sanFin);
		return eo;
	}


	public EOStgAnnee localInstanceIn(EOEditingContext editingContext) {
		return (EOStgAnnee) localInstanceOfObject(editingContext, this);
	}
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOStgAnnee creerInstance(EOEditingContext editingContext) {
		EOStgAnnee object = (EOStgAnnee) createAndInsertInstance(editingContext, _EOStgAnnee.ENTITY_NAME);
		return object;
	}


	public static EOStgAnnee localInstanceIn(EOEditingContext editingContext, EOStgAnnee eo) {
		EOStgAnnee localInstance = (eo == null) ? null : (EOStgAnnee) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOStgAnnee#localInstanceIn a la place.
	 */
	public static EOStgAnnee localInstanceOf(EOEditingContext editingContext, EOStgAnnee eo) {
		return EOStgAnnee.localInstanceIn(editingContext, eo);
	}


	/* Finders */

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier) null);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		fetchSpec.setIsDeep(isDeep);
		fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee> eoObjects = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Cree une fetchSpec sur l'entité avec les parametres.
	 * 
	 * @param qualifier
	 * @param sortOrderings
	 * @param distinct
	 * @return
	 */
	public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return fetchSpec;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOStgAnnee fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStgAnnee fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray<EOStgAnnee> eoObjects = fetchAll(editingContext, qualifier, null);
		EOStgAnnee eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else if (count == 1) {
			eoObject = (EOStgAnnee) eoObjects.objectAtIndex(0);
		} else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	
	public static EOStgAnnee fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOStgAnnee fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EOStgAnnee> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOStgAnnee eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else {
			eoObject = (EOStgAnnee) eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOStgAnnee fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOStgAnnee eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOStgAnnee ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	public static EOStgAnnee fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

}
