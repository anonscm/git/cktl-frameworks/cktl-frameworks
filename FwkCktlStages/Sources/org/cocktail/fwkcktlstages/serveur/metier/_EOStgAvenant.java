/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStgAvenant.java instead.
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.util.Enumeration;
import java.util.NoSuchElementException;
// import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStgAvenant extends  AFwkCktlStagesRecord {
	//	private static Logger logger = Logger.getLogger(_EOStgAvenant.class);

	public static final String ENTITY_NAME = "FwkCktlStages_StgAvenant";
	public static final String ENTITY_TABLE_NAME = "STAGES.STG_AVENANT";

	// Attribute Keys
	public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
	public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
	public static final ERXKey<Integer> PERS_ID_SUPPRESSION = new ERXKey<Integer>("persIdSuppression");
	public static final ERXKey<Integer> PERS_ID_VALIDATION = new ERXKey<Integer>("persIdValidation");
	public static final ERXKey<Integer> PERS_ID_VERROUILLAGE = new ERXKey<Integer>("persIdVerrouillage");
	public static final ERXKey<NSTimestamp> SAV_D_CREATION = new ERXKey<NSTimestamp>("savDCreation");
	public static final ERXKey<NSTimestamp> SAV_D_DEPLAC_DEBUT = new ERXKey<NSTimestamp>("savDDeplacDebut");
	public static final ERXKey<NSTimestamp> SAV_D_DEPLAC_FIN = new ERXKey<NSTimestamp>("savDDeplacFin");
	public static final ERXKey<NSTimestamp> SAV_D_INTER_DEBUT = new ERXKey<NSTimestamp>("savDInterDebut");
	public static final ERXKey<NSTimestamp> SAV_D_INTER_FIN = new ERXKey<NSTimestamp>("savDInterFin");
	public static final ERXKey<NSTimestamp> SAV_D_MODIFICATION = new ERXKey<NSTimestamp>("savDModification");
	public static final ERXKey<NSTimestamp> SAV_D_STAGE_DEBUT = new ERXKey<NSTimestamp>("savDStageDebut");
	public static final ERXKey<NSTimestamp> SAV_D_STAGE_FIN = new ERXKey<NSTimestamp>("savDStageFin");
	public static final ERXKey<NSTimestamp> SAV_D_SUPPRESSION = new ERXKey<NSTimestamp>("savDSuppression");
	public static final ERXKey<String> SAV_DUREE_TOTALE_UNITE = new ERXKey<String>("savDureeTotaleUnite");
	public static final ERXKey<Long> SAV_DUREE_TOTALE_VAL = new ERXKey<Long>("savDureeTotaleVal");
	public static final ERXKey<NSTimestamp> SAV_D_VALIDATION = new ERXKey<NSTimestamp>("savDValidation");
	public static final ERXKey<NSTimestamp> SAV_D_VERROUILLAGE = new ERXKey<NSTimestamp>("savDVerrouillage");
	public static final ERXKey<Double> SAV_GRAT_MONTANT = new ERXKey<Double>("savGratMontant");
	public static final ERXKey<String> SAV_SUJET = new ERXKey<String>("savSujet");
	public static final ERXKey<String> SAV_VALIDE = new ERXKey<String>("savValide");

	// Relationship Keys
	public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Avenant> TO_AVENANT = new ERXKey<org.cocktail.cocowork.server.metier.convention.Avenant>("toAvenant");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EODevise> TO_GRAT_DEVISE = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EODevise>("toGratDevise");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_LIEU_ADRESSE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toLieuAdresse");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_SERVICE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toService");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_SERVICE_ADRESSE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toServiceAdresse");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> TO_STAGE = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>("toStage");

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "savId";

	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String PERS_ID_SUPPRESSION_KEY = "persIdSuppression";
	public static final String PERS_ID_VALIDATION_KEY = "persIdValidation";
	public static final String PERS_ID_VERROUILLAGE_KEY = "persIdVerrouillage";
	public static final String SAV_D_CREATION_KEY = "savDCreation";
	public static final String SAV_D_DEPLAC_DEBUT_KEY = "savDDeplacDebut";
	public static final String SAV_D_DEPLAC_FIN_KEY = "savDDeplacFin";
	public static final String SAV_D_INTER_DEBUT_KEY = "savDInterDebut";
	public static final String SAV_D_INTER_FIN_KEY = "savDInterFin";
	public static final String SAV_D_MODIFICATION_KEY = "savDModification";
	public static final String SAV_D_STAGE_DEBUT_KEY = "savDStageDebut";
	public static final String SAV_D_STAGE_FIN_KEY = "savDStageFin";
	public static final String SAV_D_SUPPRESSION_KEY = "savDSuppression";
	public static final String SAV_DUREE_TOTALE_UNITE_KEY = "savDureeTotaleUnite";
	public static final String SAV_DUREE_TOTALE_VAL_KEY = "savDureeTotaleVal";
	public static final String SAV_D_VALIDATION_KEY = "savDValidation";
	public static final String SAV_D_VERROUILLAGE_KEY = "savDVerrouillage";
	public static final String SAV_GRAT_MONTANT_KEY = "savGratMontant";
	public static final String SAV_SUJET_KEY = "savSujet";
	public static final String SAV_VALIDE_KEY = "savValide";

	// Attributs non visibles
	public static final String ADR_SERVICE_ACCUEIL_KEY = "adrServiceAccueil";
	public static final String AVT_ORDRE_KEY = "avtOrdre";
	public static final String C_STRUCTURE_SERVICE_KEY = "cStructureService";
	public static final String SAV_GRAT_DEVISE_KEY = "savGratDevise";
	public static final String SAV_ID_KEY = "savId";
	public static final String SAV_LIEU_ADR_ORDRE_KEY = "savLieuAdrOrdre";
	public static final String STG_ID_KEY = "stgId";

	// Colonnes dans la base de donnees
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String PERS_ID_SUPPRESSION_COLKEY = "PERS_ID_SUPPRESSION";
	public static final String PERS_ID_VALIDATION_COLKEY = "PERS_ID_VALIDATION";
	public static final String PERS_ID_VERROUILLAGE_COLKEY = "PERS_ID_VERROUILLAGE";
	public static final String SAV_D_CREATION_COLKEY = "SAV_D_CREATION";
	public static final String SAV_D_DEPLAC_DEBUT_COLKEY = "SAV_D_DEPLAC_DEBUT";
	public static final String SAV_D_DEPLAC_FIN_COLKEY = "SAV_D_DEPLAC_FIN";
	public static final String SAV_D_INTER_DEBUT_COLKEY = "SAV_D_INTER_DEBUT";
	public static final String SAV_D_INTER_FIN_COLKEY = "SAV_D_INTER_FIN";
	public static final String SAV_D_MODIFICATION_COLKEY = "SAV_D_MODIFICATION";
	public static final String SAV_D_STAGE_DEBUT_COLKEY = "SAV_D_STAGE_DEBUT";
	public static final String SAV_D_STAGE_FIN_COLKEY = "SAV_D_STAGE_FIN";
	public static final String SAV_D_SUPPRESSION_COLKEY = "SAV_D_SUPPRESSION";
	public static final String SAV_DUREE_TOTALE_UNITE_COLKEY = "SAV_DUREE_TOTALE_UNITE";
	public static final String SAV_DUREE_TOTALE_VAL_COLKEY = "SAV_DUREE_TOTALE_VAL";
	public static final String SAV_D_VALIDATION_COLKEY = "SAV_D_VALIDATION";
	public static final String SAV_D_VERROUILLAGE_COLKEY = "SAV_D_VERROUILLAGE";
	public static final String SAV_GRAT_MONTANT_COLKEY = "SAV_GRAT_MONTANT";
	public static final String SAV_SUJET_COLKEY = "SAV_SUJET";
	public static final String SAV_VALIDE_COLKEY = "SAV_VALIDE";

	public static final String ADR_SERVICE_ACCUEIL_COLKEY = "ADR_SERVICE_ACCUEIL";
	public static final String AVT_ORDRE_COLKEY = "AVT_ORDRE";
	public static final String C_STRUCTURE_SERVICE_COLKEY = "C_STRUCTURE_SERVICE";
	public static final String SAV_GRAT_DEVISE_COLKEY = "SAV_GRAT_DEVISE";
	public static final String SAV_ID_COLKEY = "SAV_ID";
	public static final String SAV_LIEU_ADR_ORDRE_COLKEY = "SAV_LIEU_ADR_ORDRE";
	public static final String STG_ID_COLKEY = "STG_ID";

	// Relationships
	public static final String TO_AVENANT_KEY = "toAvenant";
	public static final String TO_GRAT_DEVISE_KEY = "toGratDevise";
	public static final String TO_LIEU_ADRESSE_KEY = "toLieuAdresse";
	public static final String TO_SERVICE_KEY = "toService";
	public static final String TO_SERVICE_ADRESSE_KEY = "toServiceAdresse";
	public static final String TO_STAGE_KEY = "toStage";


	// Accessors methods
	public Integer persIdCreation() {
		return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(Integer value) {
		takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}

	public Integer persIdModification() {
		return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(Integer value) {
		takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}

	public Integer persIdSuppression() {
		return (Integer) storedValueForKey(PERS_ID_SUPPRESSION_KEY);
	}

	public void setPersIdSuppression(Integer value) {
		takeStoredValueForKey(value, PERS_ID_SUPPRESSION_KEY);
	}

	public Integer persIdValidation() {
		return (Integer) storedValueForKey(PERS_ID_VALIDATION_KEY);
	}

	public void setPersIdValidation(Integer value) {
		takeStoredValueForKey(value, PERS_ID_VALIDATION_KEY);
	}

	public Integer persIdVerrouillage() {
		return (Integer) storedValueForKey(PERS_ID_VERROUILLAGE_KEY);
	}

	public void setPersIdVerrouillage(Integer value) {
		takeStoredValueForKey(value, PERS_ID_VERROUILLAGE_KEY);
	}

	public NSTimestamp savDCreation() {
		return (NSTimestamp) storedValueForKey(SAV_D_CREATION_KEY);
	}

	public void setSavDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, SAV_D_CREATION_KEY);
	}

	public NSTimestamp savDDeplacDebut() {
		return (NSTimestamp) storedValueForKey(SAV_D_DEPLAC_DEBUT_KEY);
	}

	public void setSavDDeplacDebut(NSTimestamp value) {
		takeStoredValueForKey(value, SAV_D_DEPLAC_DEBUT_KEY);
	}

	public NSTimestamp savDDeplacFin() {
		return (NSTimestamp) storedValueForKey(SAV_D_DEPLAC_FIN_KEY);
	}

	public void setSavDDeplacFin(NSTimestamp value) {
		takeStoredValueForKey(value, SAV_D_DEPLAC_FIN_KEY);
	}

	public NSTimestamp savDInterDebut() {
		return (NSTimestamp) storedValueForKey(SAV_D_INTER_DEBUT_KEY);
	}

	public void setSavDInterDebut(NSTimestamp value) {
		takeStoredValueForKey(value, SAV_D_INTER_DEBUT_KEY);
	}

	public NSTimestamp savDInterFin() {
		return (NSTimestamp) storedValueForKey(SAV_D_INTER_FIN_KEY);
	}

	public void setSavDInterFin(NSTimestamp value) {
		takeStoredValueForKey(value, SAV_D_INTER_FIN_KEY);
	}

	public NSTimestamp savDModification() {
		return (NSTimestamp) storedValueForKey(SAV_D_MODIFICATION_KEY);
	}

	public void setSavDModification(NSTimestamp value) {
		takeStoredValueForKey(value, SAV_D_MODIFICATION_KEY);
	}

	public NSTimestamp savDStageDebut() {
		return (NSTimestamp) storedValueForKey(SAV_D_STAGE_DEBUT_KEY);
	}

	public void setSavDStageDebut(NSTimestamp value) {
		takeStoredValueForKey(value, SAV_D_STAGE_DEBUT_KEY);
	}

	public NSTimestamp savDStageFin() {
		return (NSTimestamp) storedValueForKey(SAV_D_STAGE_FIN_KEY);
	}

	public void setSavDStageFin(NSTimestamp value) {
		takeStoredValueForKey(value, SAV_D_STAGE_FIN_KEY);
	}

	public NSTimestamp savDSuppression() {
		return (NSTimestamp) storedValueForKey(SAV_D_SUPPRESSION_KEY);
	}

	public void setSavDSuppression(NSTimestamp value) {
		takeStoredValueForKey(value, SAV_D_SUPPRESSION_KEY);
	}

	public String savDureeTotaleUnite() {
		return (String) storedValueForKey(SAV_DUREE_TOTALE_UNITE_KEY);
	}

	public void setSavDureeTotaleUnite(String value) {
		takeStoredValueForKey(value, SAV_DUREE_TOTALE_UNITE_KEY);
	}

	public Long savDureeTotaleVal() {
		return (Long) storedValueForKey(SAV_DUREE_TOTALE_VAL_KEY);
	}

	public void setSavDureeTotaleVal(Long value) {
		takeStoredValueForKey(value, SAV_DUREE_TOTALE_VAL_KEY);
	}

	public NSTimestamp savDValidation() {
		return (NSTimestamp) storedValueForKey(SAV_D_VALIDATION_KEY);
	}

	public void setSavDValidation(NSTimestamp value) {
		takeStoredValueForKey(value, SAV_D_VALIDATION_KEY);
	}

	public NSTimestamp savDVerrouillage() {
		return (NSTimestamp) storedValueForKey(SAV_D_VERROUILLAGE_KEY);
	}

	public void setSavDVerrouillage(NSTimestamp value) {
		takeStoredValueForKey(value, SAV_D_VERROUILLAGE_KEY);
	}

	public Double savGratMontant() {
		return (Double) storedValueForKey(SAV_GRAT_MONTANT_KEY);
	}

	public void setSavGratMontant(Double value) {
		takeStoredValueForKey(value, SAV_GRAT_MONTANT_KEY);
	}

	public String savSujet() {
		return (String) storedValueForKey(SAV_SUJET_KEY);
	}

	public void setSavSujet(String value) {
		takeStoredValueForKey(value, SAV_SUJET_KEY);
	}

	public String savValide() {
		return (String) storedValueForKey(SAV_VALIDE_KEY);
	}

	public void setSavValide(String value) {
		takeStoredValueForKey(value, SAV_VALIDE_KEY);
	}

	public org.cocktail.cocowork.server.metier.convention.Avenant toAvenant() {
		return (org.cocktail.cocowork.server.metier.convention.Avenant) storedValueForKey(TO_AVENANT_KEY);
	}

	public void setToAvenantRelationship(org.cocktail.cocowork.server.metier.convention.Avenant value) {
		if (value == null) {
			org.cocktail.cocowork.server.metier.convention.Avenant oldValue = toAvenant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_AVENANT_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_AVENANT_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EODevise toGratDevise() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EODevise) storedValueForKey(TO_GRAT_DEVISE_KEY);
	}

	public void setToGratDeviseRelationship(org.cocktail.fwkcktlstages.serveur.metier.EODevise value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EODevise oldValue = toGratDevise();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GRAT_DEVISE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_GRAT_DEVISE_KEY);
		}
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toLieuAdresse() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse) storedValueForKey(TO_LIEU_ADRESSE_KEY);
	}

	public void setToLieuAdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toLieuAdresse();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_LIEU_ADRESSE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_LIEU_ADRESSE_KEY);
		}
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toService() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(TO_SERVICE_KEY);
	}

	public void setToServiceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toService();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SERVICE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_SERVICE_KEY);
		}
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toServiceAdresse() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse) storedValueForKey(TO_SERVICE_ADRESSE_KEY);
	}

	public void setToServiceAdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toServiceAdresse();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SERVICE_ADRESSE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_SERVICE_ADRESSE_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgStage toStage() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgStage) storedValueForKey(TO_STAGE_KEY);
	}

	public void setToStageRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgStage oldValue = toStage();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STAGE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_STAGE_KEY);
		}
	}


	/**
	 * Créer une instance de EOStgAvenant avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOStgAvenant createEOStgAvenant(EOEditingContext editingContext, Integer persIdCreation
, Integer persIdModification
, NSTimestamp savDCreation
, NSTimestamp savDModification
, org.cocktail.cocowork.server.metier.convention.Avenant toAvenant, org.cocktail.fwkcktlstages.serveur.metier.EOStgStage toStage			) {
		EOStgAvenant eo = (EOStgAvenant) createAndInsertInstance(editingContext, _EOStgAvenant.ENTITY_NAME);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setSavDCreation(savDCreation);
		eo.setSavDModification(savDModification);
		eo.setToAvenantRelationship(toAvenant);
		eo.setToStageRelationship(toStage);
		return eo;
	}


	public EOStgAvenant localInstanceIn(EOEditingContext editingContext) {
		return (EOStgAvenant) localInstanceOfObject(editingContext, this);
	}
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOStgAvenant creerInstance(EOEditingContext editingContext) {
		EOStgAvenant object = (EOStgAvenant) createAndInsertInstance(editingContext, _EOStgAvenant.ENTITY_NAME);
		return object;
	}


	public static EOStgAvenant localInstanceIn(EOEditingContext editingContext, EOStgAvenant eo) {
		EOStgAvenant localInstance = (eo == null) ? null : (EOStgAvenant) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOStgAvenant#localInstanceIn a la place.
	 */
	public static EOStgAvenant localInstanceOf(EOEditingContext editingContext, EOStgAvenant eo) {
		return EOStgAvenant.localInstanceIn(editingContext, eo);
	}


	/* Finders */

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier) null);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		fetchSpec.setIsDeep(isDeep);
		fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant> eoObjects = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgAvenant>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Cree une fetchSpec sur l'entité avec les parametres.
	 * 
	 * @param qualifier
	 * @param sortOrderings
	 * @param distinct
	 * @return
	 */
	public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return fetchSpec;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOStgAvenant fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStgAvenant fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray<EOStgAvenant> eoObjects = fetchAll(editingContext, qualifier, null);
		EOStgAvenant eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else if (count == 1) {
			eoObject = (EOStgAvenant) eoObjects.objectAtIndex(0);
		} else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	
	public static EOStgAvenant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOStgAvenant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EOStgAvenant> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOStgAvenant eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else {
			eoObject = (EOStgAvenant) eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOStgAvenant fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOStgAvenant eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOStgAvenant ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	public static EOStgAvenant fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

}
