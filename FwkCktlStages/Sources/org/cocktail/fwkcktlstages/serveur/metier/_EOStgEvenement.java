/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStgEvenement.java instead.
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.util.Enumeration;
import java.util.NoSuchElementException;
// import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStgEvenement extends  AFwkCktlStagesRecord {
	//	private static Logger logger = Logger.getLogger(_EOStgEvenement.class);

	public static final String ENTITY_NAME = "FwkCktlStages_StgEvenement";
	public static final String ENTITY_TABLE_NAME = "STAGES.STG_EVENEMENT";

	// Attribute Keys
	public static final ERXKey<Integer> PERS_ID_CREATEUR = new ERXKey<Integer>("persIdCreateur");
	public static final ERXKey<String> SEV_COMMENTAIRE = new ERXKey<String>("sevCommentaire");
	public static final ERXKey<NSTimestamp> SEV_D_CREATION = new ERXKey<NSTimestamp>("sevDCreation");
	public static final ERXKey<NSTimestamp> SEV_D_DEBUT = new ERXKey<NSTimestamp>("sevDDebut");
	public static final ERXKey<NSTimestamp> SEV_D_FIN = new ERXKey<NSTimestamp>("sevDFin");

	// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> FWKPERS__INDIVIDUS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("fwkpers_Individus");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType> STG_EVENEMENT_TYPE = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType>("stgEvenementType");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> STG_STAGE = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>("stgStage");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat> TO_STG_ETAT_NEW = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat>("toStgEtatNew");
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat> TO_STG_ETAT_OLD = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat>("toStgEtatOld");

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "sevId";

	public static final String PERS_ID_CREATEUR_KEY = "persIdCreateur";
	public static final String SEV_COMMENTAIRE_KEY = "sevCommentaire";
	public static final String SEV_D_CREATION_KEY = "sevDCreation";
	public static final String SEV_D_DEBUT_KEY = "sevDDebut";
	public static final String SEV_D_FIN_KEY = "sevDFin";

	// Attributs non visibles
	public static final String SET_ID_NEW_KEY = "setIdNew";
	public static final String SET_ID_OLD_KEY = "setIdOld";
	public static final String SEV_ID_KEY = "sevId";
	public static final String SEVT_ID_KEY = "sevtId";
	public static final String STG_ID_KEY = "stgId";

	// Colonnes dans la base de donnees
	public static final String PERS_ID_CREATEUR_COLKEY = "PERS_ID_CREATEUR";
	public static final String SEV_COMMENTAIRE_COLKEY = "SEV_COMMENTAIRE";
	public static final String SEV_D_CREATION_COLKEY = "SEV_D_CREATION";
	public static final String SEV_D_DEBUT_COLKEY = "SEV_D_DEBUT";
	public static final String SEV_D_FIN_COLKEY = "SEV_D_FIN";

	public static final String SET_ID_NEW_COLKEY = "SET_ID_NEW";
	public static final String SET_ID_OLD_COLKEY = "SET_ID_OLD";
	public static final String SEV_ID_COLKEY = "SEV_ID";
	public static final String SEVT_ID_COLKEY = "SEVT_ID";
	public static final String STG_ID_COLKEY = "STG_ID";

	// Relationships
	public static final String FWKPERS__INDIVIDUS_KEY = "fwkpers_Individus";
	public static final String STG_EVENEMENT_TYPE_KEY = "stgEvenementType";
	public static final String STG_STAGE_KEY = "stgStage";
	public static final String TO_STG_ETAT_NEW_KEY = "toStgEtatNew";
	public static final String TO_STG_ETAT_OLD_KEY = "toStgEtatOld";


	// Accessors methods
	public Integer persIdCreateur() {
		return (Integer) storedValueForKey(PERS_ID_CREATEUR_KEY);
	}

	public void setPersIdCreateur(Integer value) {
		takeStoredValueForKey(value, PERS_ID_CREATEUR_KEY);
	}

	public String sevCommentaire() {
		return (String) storedValueForKey(SEV_COMMENTAIRE_KEY);
	}

	public void setSevCommentaire(String value) {
		takeStoredValueForKey(value, SEV_COMMENTAIRE_KEY);
	}

	public NSTimestamp sevDCreation() {
		return (NSTimestamp) storedValueForKey(SEV_D_CREATION_KEY);
	}

	public void setSevDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, SEV_D_CREATION_KEY);
	}

	public NSTimestamp sevDDebut() {
		return (NSTimestamp) storedValueForKey(SEV_D_DEBUT_KEY);
	}

	public void setSevDDebut(NSTimestamp value) {
		takeStoredValueForKey(value, SEV_D_DEBUT_KEY);
	}

	public NSTimestamp sevDFin() {
		return (NSTimestamp) storedValueForKey(SEV_D_FIN_KEY);
	}

	public void setSevDFin(NSTimestamp value) {
		takeStoredValueForKey(value, SEV_D_FIN_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType stgEvenementType() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType) storedValueForKey(STG_EVENEMENT_TYPE_KEY);
	}

	public void setStgEvenementTypeRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType oldValue = stgEvenementType();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STG_EVENEMENT_TYPE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, STG_EVENEMENT_TYPE_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgStage stgStage() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgStage) storedValueForKey(STG_STAGE_KEY);
	}

	public void setStgStageRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgStage oldValue = stgStage();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STG_STAGE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, STG_STAGE_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat toStgEtatNew() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat) storedValueForKey(TO_STG_ETAT_NEW_KEY);
	}

	public void setToStgEtatNewRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat oldValue = toStgEtatNew();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STG_ETAT_NEW_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_STG_ETAT_NEW_KEY);
		}
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat toStgEtatOld() {
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat) storedValueForKey(TO_STG_ETAT_OLD_KEY);
	}

	public void setToStgEtatOldRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat value) {
		if (value == null) {
			org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat oldValue = toStgEtatOld();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STG_ETAT_OLD_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_STG_ETAT_OLD_KEY);
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> fwkpers_Individus() {
		return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>) storedValueForKey(FWKPERS__INDIVIDUS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> fwkpers_Individus(EOQualifier qualifier) {
		return fwkpers_Individus(qualifier, null);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> fwkpers_Individus(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> results;
			results = fwkpers_Individus();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		return results;
	}

	public void addToFwkpers_IndividusRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
		addObjectToBothSidesOfRelationshipWithKey(object, FWKPERS__INDIVIDUS_KEY);
	}

	public void removeFromFwkpers_IndividusRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, FWKPERS__INDIVIDUS_KEY);
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createFwkpers_IndividusRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, FWKPERS__INDIVIDUS_KEY);
		return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
	}

	public void deleteFwkpers_IndividusRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, FWKPERS__INDIVIDUS_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllFwkpers_IndividusRelationships() {
		Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> objects = fwkpers_Individus().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteFwkpers_IndividusRelationship(objects.nextElement());
		}
	}


	/**
	 * Créer une instance de EOStgEvenement avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOStgEvenement createEOStgEvenement(EOEditingContext editingContext, Integer persIdCreateur
, NSTimestamp sevDCreation
, org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenementType stgEvenementType, org.cocktail.fwkcktlstages.serveur.metier.EOStgStage stgStage, org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat toStgEtatNew, org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat toStgEtatOld			) {
		EOStgEvenement eo = (EOStgEvenement) createAndInsertInstance(editingContext, _EOStgEvenement.ENTITY_NAME);
		eo.setPersIdCreateur(persIdCreateur);
		eo.setSevDCreation(sevDCreation);
		eo.setStgEvenementTypeRelationship(stgEvenementType);
		eo.setStgStageRelationship(stgStage);
		eo.setToStgEtatNewRelationship(toStgEtatNew);
		eo.setToStgEtatOldRelationship(toStgEtatOld);
		return eo;
	}


	public EOStgEvenement localInstanceIn(EOEditingContext editingContext) {
		return (EOStgEvenement) localInstanceOfObject(editingContext, this);
	}
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOStgEvenement creerInstance(EOEditingContext editingContext) {
		EOStgEvenement object = (EOStgEvenement) createAndInsertInstance(editingContext, _EOStgEvenement.ENTITY_NAME);
		return object;
	}


	public static EOStgEvenement localInstanceIn(EOEditingContext editingContext, EOStgEvenement eo) {
		EOStgEvenement localInstance = (eo == null) ? null : (EOStgEvenement) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOStgEvenement#localInstanceIn a la place.
	 */
	public static EOStgEvenement localInstanceOf(EOEditingContext editingContext, EOStgEvenement eo) {
		return EOStgEvenement.localInstanceIn(editingContext, eo);
	}


	/* Finders */

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier) null);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		fetchSpec.setIsDeep(isDeep);
		fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement> eoObjects = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEvenement>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Cree une fetchSpec sur l'entité avec les parametres.
	 * 
	 * @param qualifier
	 * @param sortOrderings
	 * @param distinct
	 * @return
	 */
	public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return fetchSpec;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOStgEvenement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStgEvenement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray<EOStgEvenement> eoObjects = fetchAll(editingContext, qualifier, null);
		EOStgEvenement eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else if (count == 1) {
			eoObject = (EOStgEvenement) eoObjects.objectAtIndex(0);
		} else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	
	public static EOStgEvenement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOStgEvenement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EOStgEvenement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOStgEvenement eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else {
			eoObject = (EOStgEvenement) eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOStgEvenement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOStgEvenement eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOStgEvenement ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	public static EOStgEvenement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

}
