/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStgEtat.java instead.
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.util.Enumeration;
import java.util.NoSuchElementException;
// import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStgEtat extends  AFwkCktlStagesRecord {
	//	private static Logger logger = Logger.getLogger(_EOStgEtat.class);

	public static final String ENTITY_NAME = "FwkCktlStages_StgEtat";
	public static final String ENTITY_TABLE_NAME = "STAGES.STG_ETAT";

	// Attribute Keys
	public static final ERXKey<String> SET_LC = new ERXKey<String>("setLc");
	public static final ERXKey<String> SET_LL = new ERXKey<String>("setLl");
	public static final ERXKey<Long> SET_ORDRE = new ERXKey<Long>("setOrdre");

	// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> STG_STAGES = new ERXKey<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>("stgStages");

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "setId";

	public static final String SET_LC_KEY = "setLc";
	public static final String SET_LL_KEY = "setLl";
	public static final String SET_ORDRE_KEY = "setOrdre";

	// Attributs non visibles
	public static final String SET_ID_KEY = "setId";

	// Colonnes dans la base de donnees
	public static final String SET_LC_COLKEY = "SET_LC";
	public static final String SET_LL_COLKEY = "SET_LL";
	public static final String SET_ORDRE_COLKEY = "SET_ORDRE";

	public static final String SET_ID_COLKEY = "SET_ID";

	// Relationships
	public static final String STG_STAGES_KEY = "stgStages";


	// Accessors methods
	public String setLc() {
		return (String) storedValueForKey(SET_LC_KEY);
	}

	public void setSetLc(String value) {
		takeStoredValueForKey(value, SET_LC_KEY);
	}

	public String setLl() {
		return (String) storedValueForKey(SET_LL_KEY);
	}

	public void setSetLl(String value) {
		takeStoredValueForKey(value, SET_LL_KEY);
	}

	public Long setOrdre() {
		return (Long) storedValueForKey(SET_ORDRE_KEY);
	}

	public void setSetOrdre(Long value) {
		takeStoredValueForKey(value, SET_ORDRE_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> stgStages() {
		return (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>) storedValueForKey(STG_STAGES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> stgStages(EOQualifier qualifier) {
		return stgStages(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> stgStages(EOQualifier qualifier, boolean fetch) {
		return stgStages(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> stgStages(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> results;
		if (fetch) {
			EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage.TO_ETAT_KEY, EOQualifier.QualifierOperatorEqual, this);

			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

			results = org.cocktail.fwkcktlstages.serveur.metier.EOStgStage.fetchAll(editingContext(), fullQualifier, sortOrderings);
		} else {
			results = stgStages();
			if (qualifier != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>) EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage>) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		}
		return results;
	}

	public void addToStgStagesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage object) {
		addObjectToBothSidesOfRelationshipWithKey(object, STG_STAGES_KEY);
	}

	public void removeFromStgStagesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_STAGES_KEY);
	}

	public org.cocktail.fwkcktlstages.serveur.metier.EOStgStage createStgStagesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlStages_StgStage");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, STG_STAGES_KEY);
		return (org.cocktail.fwkcktlstages.serveur.metier.EOStgStage) eo;
	}

	public void deleteStgStagesRelationship(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, STG_STAGES_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllStgStagesRelationships() {
		Enumeration<org.cocktail.fwkcktlstages.serveur.metier.EOStgStage> objects = stgStages().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteStgStagesRelationship(objects.nextElement());
		}
	}


	/**
	 * Créer une instance de EOStgEtat avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOStgEtat createEOStgEtat(EOEditingContext editingContext, String setLc
, String setLl
, Long setOrdre
			) {
		EOStgEtat eo = (EOStgEtat) createAndInsertInstance(editingContext, _EOStgEtat.ENTITY_NAME);
		eo.setSetLc(setLc);
		eo.setSetLl(setLl);
		eo.setSetOrdre(setOrdre);
		return eo;
	}


	public EOStgEtat localInstanceIn(EOEditingContext editingContext) {
		return (EOStgEtat) localInstanceOfObject(editingContext, this);
	}
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOStgEtat creerInstance(EOEditingContext editingContext) {
		EOStgEtat object = (EOStgEtat) createAndInsertInstance(editingContext, _EOStgEtat.ENTITY_NAME);
		return object;
	}


	public static EOStgEtat localInstanceIn(EOEditingContext editingContext, EOStgEtat eo) {
		EOStgEtat localInstance = (eo == null) ? null : (EOStgEtat) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOStgEtat#localInstanceIn a la place.
	 */
	public static EOStgEtat localInstanceOf(EOEditingContext editingContext, EOStgEtat eo) {
		return EOStgEtat.localInstanceIn(editingContext, eo);
	}


	/* Finders */

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier) null);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	}

	public static NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		fetchSpec.setIsDeep(isDeep);
		fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat> eoObjects = (NSArray<org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Cree une fetchSpec sur l'entité avec les parametres.
	 * 
	 * @param qualifier
	 * @param sortOrderings
	 * @param distinct
	 * @return
	 */
	public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return fetchSpec;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOStgEtat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStgEtat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray<EOStgEtat> eoObjects = fetchAll(editingContext, qualifier, null);
		EOStgEtat eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else if (count == 1) {
			eoObject = (EOStgEtat) eoObjects.objectAtIndex(0);
		} else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	
	public static EOStgEtat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOStgEtat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EOStgEtat> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOStgEtat eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		} else {
			eoObject = (EOStgEtat) eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOStgEtat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOStgEtat eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOStgEtat ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}


	public static EOStgEtat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

}
