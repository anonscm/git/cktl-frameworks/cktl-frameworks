/*
 * Copyright Cocktail, 2001-2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier;

import com.webobjects.foundation.NSValidation;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class EOStgEvenementType extends _EOStgEvenementType {

	public static final String LC_CREATION_DEMANDE = "INIT";
	public static final String LC_MODIFICATION_DEMANDE = "MODIF";
	public static final String LC_DEMANDE_VALIDATION_PEDA = "DVALP";
	public static final String LC_ACCEPTATION_VALIDATION_PEDAGOGIQUE = "VALP";
	public static final String LC_REFUS_VALIDATION_PEDAGOGIQUE = "RVALP";

	public static final String LC_ACCEPTATION_VALIDATION_TUTEUR = "VALT";
	public static final String LC_REFUS_VALIDATION_TUTEUR = "RVALT";
	public static final String LC_DESIGNATION_TUTEUR = "NEWTU";
	public static final String LC_MODIFICATION_ADMINISTRATIVE = "MODAD";
	public static final String LC_ACCEPTATION_VALIDATION_ADMINISTRATIVE = "VALAD";
	public static final String LC_ANNULATION_VALIDATION_ADMINISTRATIVE = "ANNAD";
	public static final String LC_ANNULATION_VALIDATION_PEDAGOGIQUE = "ANNPE";
	public static final String LC_EDITION_CONVENTION = "EDIT";
	public static final String LC_SUPPRESSION_FICHIER_CONVENTION = "SUPPF";
	public static final String LC_SUPPRESSION_TOTALE_CONVENTION = "SUPPC";
	public static final String LC_ANNULATION_SUPPRESSION_TOTALE_CONVENTION = "ANNSU";
	public static final String LC_RECEPTION_SIGNATURES = "SIGNA";
	public static final String LC_RESILIATION_CONVENTION = "RESIL";
	public static final String LC_ANNULATION_RESILIATION_CONVENTION = "ANNRE";
	public static final String LC_CREATION_AVENANT = "NEWAV";
	public static final String LC_MODIFICATION_AVENANT = "MODAV";
	public static final String LC_VALIDATION_AVENANT = "VALAV";
	public static final String LC_SUPPRESSION_AVENANT = "SUPAV";
	public static final String LC_REFUS_AVENANT = "REFAV";
	public static final String LC_VERROUILLAGE_AVENANT = "VERAV";

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException 
	 */
	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException 
	 */
	@Override
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException 
	 */
	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException 
	 */
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de
	 * l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException 
	 */
	@Override
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		super.validateBeforeTransactionSave();
	}
}
