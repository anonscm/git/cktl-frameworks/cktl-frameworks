package org.cocktail.pistache.serveur.components.assistants;

import java.text.SimpleDateFormat;
import java.util.Date;



public class Annexe {
	
	private String cheminAnnexe;
	private String nomAnnexe;
	private String typeAnnexe;
	private Integer persId;
	private boolean impOblig;
	private boolean convOfficielle;
	private Date dateCreation;
	

	public void Annexe(String cheminAnnexe,String nomAnnexe,String typeAnnexe,Integer persId,boolean impOblig,
			boolean convOfficielle,
			Date dateCreation){
		this.cheminAnnexe=cheminAnnexe;
		this.nomAnnexe=nomAnnexe;
		
		this.typeAnnexe=typeAnnexe;
		this.persId=persId;
		this.impOblig=impOblig;
		this.convOfficielle=convOfficielle;
		this.dateCreation=now();
		
		
	}
	
	public Date now(){
		Date date1 = new Date();
		String date;
		
		SimpleDateFormat  simpleFormat = new SimpleDateFormat("dd/MM/yyyy");
		date = simpleFormat.format(date1);
		return date1;
		
	}
	public void setCheminAnnexe(String chemin){
		this.cheminAnnexe=chemin;	
		
	}
	
	public void setNomAnnexe(String nom){
		this.nomAnnexe=nom;
	}

	public String getNomAnnexe(){
		return nomAnnexe;
		
	}
	
	public String getCheminAnnexe(){
		
		return cheminAnnexe;
	}
}
