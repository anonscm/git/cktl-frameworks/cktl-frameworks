package org.cocktail.fwkcktlstages.serveur.finder;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.junit.Test;

public class FinderEtudiantCommonsTests {

	
	@Test
	public void filtrerSurNumeroEtudiant() {
		// Arrange
		IEtudiant etudiant1 = getEtudiant(1, null);
		IEtudiant etudiant2 = getEtudiant(2, null);
		IEtudiant etudiant3 = getEtudiant(3, null);
		List<IEtudiant> etudiants = Arrays.asList(etudiant1, etudiant2, etudiant3);
		FinderEtudiantCommons finderEtudiantCommons = new FinderEtudiantCommonsImplemented();

		// Act
		List<IEtudiant> etudiantFiltres = finderEtudiantCommons.filtrerSurNumeroIndividu(etudiants, 2);

		// Assert
		assertThat(etudiantFiltres.size()).isEqualTo(1);
		assertThat(etudiantFiltres.get(0)).isEqualTo(etudiant2);

	}

	@Test
	public void filtrerSurPersId() {
		// Arrange
		IEtudiant etudiant1 = getEtudiant(null, 1);
		IEtudiant etudiant2 = getEtudiant(null, 2);
		IEtudiant etudiant3 = getEtudiant(null, 3);
		List<IEtudiant> etudiants = Arrays.asList(etudiant1, etudiant2, etudiant3);
		FinderEtudiantCommons finderEtudiantCommons = new FinderEtudiantCommonsImplemented();

		// Act
		List<IEtudiant> etudiantFiltres = finderEtudiantCommons.filtrerSurPersId(etudiants, 3);

		// Assert
		assertThat(etudiantFiltres.size()).isEqualTo(1);
		assertThat(etudiantFiltres.get(0)).isEqualTo(etudiant3);

	}

	private class FinderEtudiantCommonsImplemented extends FinderEtudiantCommons{

		public List<IEtudiant> rechercheNomPrenomINE(Integer numeroEtudiant, String nom, String prenom, String INE) {
	    return null;
    }

		public List<IEtudiant> rechercheNumeroIndividu(Integer etudiantNumeroIndividu) {
	    return null;
    }

		public List<IEtudiant> recherchePersId(Integer persId) {
	    return null;
    }

		public boolean isInscritPourAnne(IEtudiant etudiant, Integer annee) {
	    return false;
    }
		
	}
	
	private IEtudiant getEtudiant(Integer numeroIndividu, Integer persId) {
		IEtudiant etudiant = mock(IEtudiant.class);
		IIndividu individu = mock(IIndividu.class);
		when(individu.noIndividu()).thenReturn(numeroIndividu);
		when(individu.persId()).thenReturn(persId);
		when(etudiant.toIndividu()).thenReturn(individu);
		return etudiant;
	}
}
