package org.cocktail.fwkcktlstages.serveur.metier.controles;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import junit.framework.TestCase;

import org.cocktail.fwkcktlstages.serveur.metier.EOStgAbsence;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.rules.MockEditingContext;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class ControleStageAbsencesTest {

	private ControleStageAbsences csa;
	private static SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");

	@Before
	public void setUp() throws Exception {
		ControleStageAbsences.setInstance(new ControleStageAbsences());
		csa = ControleStageAbsences.getInstance();
	}

	@Test
	public void testControleDatesRenseignees() throws ParseException {
		assertTrue("Pas d'erreur sur une absence null.", isControleOk(null));

		EOStgAbsence abs = stubAbsence(null, null);
		assertFalse("Les dates de début et de fin doivent être renseignées.", isControleOk(abs));

		abs = stubAbsence("01/01/2013", null);
		assertFalse("La date de fin doit être renseignée.", isControleOk(abs));

		abs = stubAbsence(null, "01/01/2013");
		assertFalse("La date de début doit être renseignée.", isControleOk(abs));
	}

	@Test
	public void testControleDatesValides() throws ParseException {
		EOStgAbsence abs = stubAbsence("01/02/2013", "01/01/2013");
		assertFalse("La date de début doit être avant la date de fin", isControleOk(abs));

		abs = stubAbsence("01/01/2013", "01/02/2013");
		assertTrue("La date de début doit être avant la date de fin", isControleOk(abs));

		abs = stubAbsence("01/01/2013", "01/01/2013", "PM", "AM");
		assertFalse("Les demies journees doivent être cohérentes quand les dates de début et de fin sont identiques",
				isControleOk(abs));
		abs = stubAbsence("01/01/2013", "01/01/2013", "AM", "AM");
		assertTrue("Les demies journees doivent être cohérentes quand les dates de début et de fin sont identiques",
				isControleOk(abs));
		abs = stubAbsence("01/01/2013", "01/01/2013", "AM", "PM");
		assertTrue("Les demies journees doivent être cohérentes quand les dates de début et de fin sont identiques",
				isControleOk(abs));
		abs = stubAbsence("01/01/2013", "01/01/2013", "PM", "PM");
		assertTrue("Les demies journees doivent être cohérentes quand les dates de début et de fin sont identiques",
				isControleOk(abs));
		abs = stubAbsence("01/01/2013", "02/01/2013", "PM", "AM");
		assertTrue("Les demies journees doivent être cohérentes quand les dates de début et de fin sont identiques",
				isControleOk(abs));
	}

	@Test
	public void testControlesDatesCoherentesAvecStage() throws ParseException {
		EOStgAbsence abs = stubAbsenceEtStage("10/01/2013", "11/01/2013", "01/01/2013", "31/02/2013");
		assertTrue("Un absence définie dans les limites du stage doit être valide", isControleOk(abs));

		abs = stubAbsenceEtStage("01/01/2013", "11/01/2013", "10/01/2013", "31/01/2013");
		assertFalse("Une absence qui débute avant le stage est invalide", isControleOk(abs));
		abs = stubAbsenceEtStage("11/01/2013", "01/02/2013", "10/01/2013", "31/01/2013");
		assertFalse("Une absence qui fini après le stage est invalide", isControleOk(abs));
		abs = stubAbsenceEtStage("01/02/2013", "02/02/2013", "10/01/2013", "31/01/2013");
		assertFalse("Une absence qui débute après la fin du stage est invalide", isControleOk(abs));
		abs = stubAbsenceEtStage("01/01/2013", "02/02/2013", "10/01/2013", "31/01/2013");
		assertFalse("Une absence qui débute avant et fini après le stage est invalide", isControleOk(abs));

	}

	/** PRIVATES **/

	private boolean isControleOk(EOStgAbsence absence) {
		try {
			csa.controleDates(absence);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private EOStgAbsence stubAbsence(final String debut, final String fin) throws ParseException {
		EOStgAbsence abs = mock(EOStgAbsence.class);
		if (debut != null) {
			when(abs.sabDDebut()).thenReturn(new NSTimestamp(SDF.parse(debut)));
		}
		if (fin != null) {
			when(abs.sabDFin()).thenReturn(new NSTimestamp(SDF.parse(fin)));
		}
		when(abs.toStage()).thenReturn(null);
		return abs;
	}

	private EOStgAbsence stubAbsence(String debut, String fin, String demieJourneeDebut, String demieJourneeFin)
			throws ParseException {
		EOStgAbsence abs = stubAbsence(debut, fin);
		if (demieJourneeDebut != null) {
			when(abs.sabDemiejourneeDebut()).thenReturn(demieJourneeDebut);
		}
		if (demieJourneeFin != null) {
			when(abs.sabDemiejourneeFin()).thenReturn(demieJourneeFin);
		}
		when(abs.toStage()).thenReturn(null);
		return abs;
	}

	private EOStgStage stubStage(String debut, String fin) throws ParseException {
		EOStgStage stg = mock(EOStgStage.class);
		if (debut != null) {
			when(stg.stgDDebut()).thenReturn(new NSTimestamp(SDF.parse(debut)));
		}
		if (fin != null) {
			when(stg.stgDFin()).thenReturn(new NSTimestamp(SDF.parse(fin)));
		}
		return stg;
	}

	private EOStgAbsence stubAbsenceEtStage(String debutAbs, String finAbs, String debutStg, String finStg)
			throws ParseException {
		EOStgAbsence abs = stubAbsence(debutAbs, finAbs);
		EOStgStage stg = stubStage(debutStg, finStg);

		when(abs.toStage()).thenReturn(stg);
		return abs;
	}
}
