/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlstages.serveur.metier.droits;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import org.cocktail.fwkcktlstages.serveur.metier.EOStgEtat;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Julien BLANDINEAU <jblandin@univ-lr.fr> © 2012 Cocktail
 */
public class StagesDroitsCachedTest extends TestCase {
	private static final int ANNEE = 2013;

	private StagesDroitsCached sdc;
	private StagesDroits sd = new StagesDroitsCentralise();

	private EOStgStage stage = mock(EOStgStage.class);
	private EOStgEtat etat = mock(EOStgEtat.class);

	/**
	 * Le rôle de l'utilisateur
	 */
	private enum ROLE {
		DEMANDEUR,
		RESPONSABLE_ADMINISTRATIF,
		RESPONSABLE_PEDAGOGIQUE,
		AUCUN
	};

	@Override
	@Before
	public void setUp() throws Exception {
		System.setProperty(StagesDroits.class.getSimpleName(), StagesDroitsCentralise.class.getName());
		sdc = new StagesDroitsCached(0, ANNEE, sd) {
			@Override
			protected Integer initEtudiant() {
				return null;
			}

			@Override
			protected void initListesStages() {
			}

			@Override
			protected void initListesEc() {
			}
		};
	}

	private void stubIsResp(final Boolean isRespAdm, final Boolean isRespPeda) {
		try {
			sdc = new StagesDroitsCached(0, ANNEE, sd) {
				@Override
				protected void initListesStages() {
				}

				@Override
				protected Integer initEtudiant() {
					return null;
				}

				@Override
				protected void initListesEc() {
				}

				@Override
				public boolean isResponsableAdministratif(EOStgStage stage) {
					if (isRespAdm != null) {
						return isRespAdm;
					} else {
						return super.isResponsableAdministratif(stage);
					}
				};

				@Override
				public boolean isResponsableAdministratif(EC mec) {
					if (isRespAdm != null) {
						return isRespAdm;
					} else {
						return super.isResponsableAdministratif(mec);
					}
				}

				@Override
				public boolean isResponsablePedagogique(EOStgStage stage) {
					if (isRespPeda != null) {
						return isRespPeda;
					} else {
						return super.isResponsablePedagogique(stage);
					}
				};
			};
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void stubEtat(String lcEtat) {
		if (lcEtat != null) {
			when(stage.toEtat()).thenReturn(etat);
			when(stage.isEtatInitial()).thenReturn(EOStgEtat.LC_INITIAL.equals(lcEtat));
			when(stage.isEtatAttenteEditionConvention()).thenReturn(EOStgEtat.LC_ATTENTE_EDITION.equals(lcEtat));
			when(stage.isEtatAttenteSignatures()).thenReturn(EOStgEtat.LC_ATTENTE_SIGNATURES.equals(lcEtat));
			when(stage.isEtatAttenteValidationAdministrative()).thenReturn(EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE.equals(lcEtat));
			when(stage.isEtatAttenteValidationPedagogique()).thenReturn(EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE.equals(lcEtat));
			when(stage.isEtatAttenteValidationTuteur()).thenReturn(EOStgEtat.LC_ATTENTE_VALIDATION_TUTEUR.equals(lcEtat));
			when(stage.isEtatConventionSignee()).thenReturn(EOStgEtat.LC_CONVENTION_SIGNEE.equals(lcEtat));
			List<String> apresValAdm = Arrays.asList(EOStgEtat.LC_ATTENTE_EDITION,   
													 EOStgEtat.LC_ATTENTE_SIGNATURES,
													 EOStgEtat.LC_CONVENTION_SIGNEE, 
													 EOStgEtat.LC_CONVENTION_RESILIEE,
													 EOStgEtat.LC_CONVENTION_SUPPRIMEE);
			if (apresValAdm.contains(lcEtat)) {
				when(stage.isEtatCourantApresEtatDonne(EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE)).thenReturn(Boolean.TRUE);
			} else {
				when(stage.isEtatCourantApresEtatDonne(EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE)).thenReturn(Boolean.FALSE);
			}

		}
	}

	private void stubAllValues(ROLE role, final String lcEtat) {
		switch (role) {
		case DEMANDEUR:
			when(stage.isDemandeur(0)).thenReturn(Boolean.TRUE);
			stubIsResp(Boolean.FALSE, Boolean.FALSE);
			break;
		case RESPONSABLE_ADMINISTRATIF:
			when(stage.isDemandeur(0)).thenReturn(Boolean.FALSE);
			stubIsResp(Boolean.TRUE, Boolean.FALSE);
			break;
		case RESPONSABLE_PEDAGOGIQUE:
			when(stage.isDemandeur(0)).thenReturn(Boolean.FALSE);
			stubIsResp(Boolean.FALSE, Boolean.TRUE);
			break;
		case AUCUN:
		default:
			when(stage.isDemandeur(0)).thenReturn(Boolean.FALSE);
			stubIsResp(Boolean.FALSE, Boolean.FALSE);
			break;
		}
		stubEtat(lcEtat);
	}

	private boolean subHasDroitModificationDemande(ROLE role, String etat) {
		stubAllValues(role, etat);
		return sdc.hasDroitsModificationDemande(stage);
	}

	private boolean subHasDroitsDemandeValidationPedagogique(ROLE role, String etat) {
		stubAllValues(role, etat);
		return sdc.hasDroitsDemandeValidationPedagogique(stage);
	}

	private boolean subHasDroitsValidationAdministrative(ROLE role, String etat) {
		stubAllValues(role, etat);
		return sdc.hasDroitsValidationAdministrative(stage);
	}

	private boolean subHasDroitsValidationPedagogique(ROLE role, String etat) {
		stubAllValues(role, etat);
		return sdc.hasDroitsValidationPedagogique(stage);
	}

	// Tests

	/**
	 * Test method for {@link StagesDroitsCached#hasDroitsModificationDemande(EOStgStage, Integer)}
	 * 
	 */
	@Test
	public void testHasDroitModificationDemandeLambda() {
		assertFalse(subHasDroitModificationDemande(ROLE.AUCUN, EOStgEtat.LC_INITIAL));
		assertFalse(subHasDroitModificationDemande(ROLE.RESPONSABLE_PEDAGOGIQUE, EOStgEtat.LC_INITIAL));

		assertFalse(subHasDroitModificationDemande(ROLE.AUCUN, EOStgEtat.LC_ATTENTE_EDITION));
		assertFalse(subHasDroitModificationDemande(ROLE.RESPONSABLE_PEDAGOGIQUE, EOStgEtat.LC_ATTENTE_EDITION));

		assertFalse(subHasDroitModificationDemande(ROLE.AUCUN, EOStgEtat.LC_CONVENTION_SIGNEE));
		assertFalse(subHasDroitModificationDemande(ROLE.RESPONSABLE_PEDAGOGIQUE, EOStgEtat.LC_CONVENTION_SIGNEE));

		assertFalse(subHasDroitModificationDemande(ROLE.AUCUN, EOStgEtat.LC_CONVENTION_SUPPRIMEE));
		assertFalse(subHasDroitModificationDemande(ROLE.RESPONSABLE_PEDAGOGIQUE, EOStgEtat.LC_CONVENTION_SUPPRIMEE));

		assertFalse(subHasDroitModificationDemande(ROLE.AUCUN, EOStgEtat.LC_CONVENTION_RESILIEE));
		assertFalse(subHasDroitModificationDemande(ROLE.RESPONSABLE_PEDAGOGIQUE, EOStgEtat.LC_CONVENTION_RESILIEE));
	}

	/**
	 * Test method for {@link StagesDroitsCached#hasDroitsModificationDemande(EOStgStage, Integer)}
	 * 
	 */
	@Test
	public void testHasDroitModificationDemandeDemandeur() {
		assertTrue(subHasDroitModificationDemande(ROLE.DEMANDEUR, EOStgEtat.LC_INITIAL));
		assertFalse(subHasDroitModificationDemande(ROLE.DEMANDEUR, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE));
		assertFalse(subHasDroitModificationDemande(ROLE.DEMANDEUR, EOStgEtat.LC_ATTENTE_DESIGNATION_TUTEUR));
		assertFalse(subHasDroitModificationDemande(ROLE.DEMANDEUR, EOStgEtat.LC_ATTENTE_SIGNATURES));
		assertFalse(subHasDroitModificationDemande(ROLE.DEMANDEUR, EOStgEtat.LC_ATTENTE_EDITION));
		assertFalse(subHasDroitModificationDemande(ROLE.DEMANDEUR, EOStgEtat.LC_CONVENTION_SIGNEE));
		assertFalse(subHasDroitModificationDemande(ROLE.DEMANDEUR, EOStgEtat.LC_CONVENTION_SUPPRIMEE));
		assertFalse(subHasDroitModificationDemande(ROLE.DEMANDEUR, EOStgEtat.LC_CONVENTION_RESILIEE));
	}

	/**
	 * Test method for {@link StagesDroitsCached#hasDroitsModificationDemande(EOStgStage, Integer)}
	 * 
	 */
	@Test
	public void testHasDroitModificationDemandeResponsableAdministratif() {
		assertTrue(subHasDroitModificationDemande(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_INITIAL));
		assertTrue(subHasDroitModificationDemande(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE));
		assertTrue(subHasDroitModificationDemande(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_ATTENTE_VALIDATION_TUTEUR));
		assertTrue(subHasDroitModificationDemande(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE));
		assertTrue(subHasDroitModificationDemande(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_ATTENTE_DESIGNATION_TUTEUR));

		assertFalse(subHasDroitModificationDemande(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_ATTENTE_EDITION));
		assertFalse(subHasDroitModificationDemande(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_ATTENTE_SIGNATURES));
		assertFalse(subHasDroitModificationDemande(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_ATTENTE_SIGNATURES));
		assertFalse(subHasDroitModificationDemande(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_CONVENTION_RESILIEE));
		assertFalse(subHasDroitModificationDemande(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_CONVENTION_SUPPRIMEE));
	}

	/**
	 * Test method for {@link StagesDroitsCached#hasDroitsDemandeValidationPedagogique(EOStgStage, Integer)}
	 * 
	 */
	@Test
	public void testHasDroitDemandeValidationPedagogiqueLamba() {
		assertFalse(subHasDroitsDemandeValidationPedagogique(ROLE.AUCUN, EOStgEtat.LC_INITIAL));
		assertFalse(subHasDroitsDemandeValidationPedagogique(ROLE.AUCUN, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE));
		assertFalse(subHasDroitsDemandeValidationPedagogique(ROLE.RESPONSABLE_PEDAGOGIQUE, EOStgEtat.LC_INITIAL));
		assertFalse(subHasDroitsDemandeValidationPedagogique(ROLE.RESPONSABLE_PEDAGOGIQUE, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE));
	}

	/**
	 * Test method for {@link StagesDroitsCached#hasDroitsDemandeValidationPedagogique(EOStgStage, Integer)}
	 * 
	 */
	@Test
	public void testHasDroitDemandeValidationPedagogiqueDemandeur() {
		assertFalse(subHasDroitsDemandeValidationPedagogique(ROLE.DEMANDEUR, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE));
		assertTrue(subHasDroitsDemandeValidationPedagogique(ROLE.DEMANDEUR, EOStgEtat.LC_INITIAL));
	}

	/**
	 * Test method for {@link StagesDroitsCached#hasDroitsDemandeValidationPedagogique(EOStgStage, Integer)}
	 * 
	 */
	@Test
	public void testHasDroitDemandeValidationPedagogiqueResponsableAdministratif() {
		assertFalse(subHasDroitsDemandeValidationPedagogique(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE));
		assertTrue(subHasDroitsDemandeValidationPedagogique(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_INITIAL));
	}

	/**
	 * Test method for {@link StagesDroitsCached#hasDroitsValidationPedagogique(EOStgStage, Integer)}
	 * 
	 */
	@Test
	public void testHasDroitValidationPedagogique() {
		assertFalse(subHasDroitsValidationPedagogique(ROLE.AUCUN, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE));
		assertFalse(subHasDroitsValidationPedagogique(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE));
		assertFalse(subHasDroitsValidationPedagogique(ROLE.DEMANDEUR, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE));

		assertFalse(subHasDroitsValidationPedagogique(ROLE.AUCUN, EOStgEtat.LC_INITIAL));

		assertFalse(subHasDroitsValidationPedagogique(ROLE.RESPONSABLE_PEDAGOGIQUE, EOStgEtat.LC_INITIAL));
		assertFalse(subHasDroitsValidationPedagogique(ROLE.RESPONSABLE_PEDAGOGIQUE, EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE));

		assertTrue(subHasDroitsValidationPedagogique(ROLE.RESPONSABLE_PEDAGOGIQUE, EOStgEtat.LC_ATTENTE_VALIDATION_PEDAGOGIQUE));
	}

	/**
	 * Test method for {@link StagesDroitsCached#hasDroitsValidationAdministrative(EOStgStage, Integer)}
	 * 
	 */
	@Test
	public void testHasDroitValidationAdministrative() {
		assertFalse(subHasDroitsValidationAdministrative(ROLE.AUCUN, EOStgEtat.LC_INITIAL));
		assertFalse(subHasDroitsValidationAdministrative(ROLE.DEMANDEUR, EOStgEtat.LC_INITIAL));
		assertFalse(subHasDroitsValidationAdministrative(ROLE.RESPONSABLE_PEDAGOGIQUE, EOStgEtat.LC_INITIAL));
		assertFalse(subHasDroitsValidationAdministrative(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_INITIAL));

		assertFalse(subHasDroitsValidationAdministrative(ROLE.AUCUN, EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE));
		assertFalse(subHasDroitsValidationAdministrative(ROLE.DEMANDEUR, EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE));
		assertFalse(subHasDroitsValidationAdministrative(ROLE.RESPONSABLE_PEDAGOGIQUE, EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE));

		assertFalse(subHasDroitsValidationAdministrative(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_ATTENTE_EDITION));

		assertTrue(subHasDroitsValidationAdministrative(ROLE.RESPONSABLE_ADMINISTRATIF, EOStgEtat.LC_ATTENTE_VALIDATION_ADMINISTRATIVE));
	}
}
