package org.cocktail.fwkcktlstages.serveur.metier.droits;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlstages.serveur.finder.FactoryFinderScolarite;
import org.cocktail.fwkcktlstages.serveur.finder.FinderResponsable;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class StagesDroitsDecentraliseTest {

	private static final int RESP_ADM_PERS_ID = 1;
	private static final int RESP_PEDA_PERS_ID = 5;
	private static final int ENS_PERS_ID = 9;

	private static final int ANNEE_1 = 2012;
	private static final int ANNEE_2 = 2013;
	private static final List<Integer> respAdmPersId = Arrays.asList(RESP_ADM_PERS_ID, 2, 3, 4);
	private static final List<Integer> respPedaPersId = Arrays.asList(RESP_PEDA_PERS_ID, 6, 7, 8);
	private static final List<Integer> ensPersId = Arrays.asList(ENS_PERS_ID, 10, 11, 12);

	private EC unEc_1 = mock(EC.class);
	private EC unEc_2 = mock(EC.class);
	private EC unEc_3 = mock(EC.class);
	private EC unEc_4 = mock(EC.class);
	private EC unEc_5 = mock(EC.class);
	private EC unEc_6 = mock(EC.class);

	private EOStgStage unstage_1 = mock(EOStgStage.class);
	private EOStgStage unstage_2 = mock(EOStgStage.class);
	private EOStgStage unstage_3 = mock(EOStgStage.class);
	private EOStgStage unstage_4 = mock(EOStgStage.class);
	private EOStgStage unstage_5 = mock(EOStgStage.class);
	private EOStgStage unstage_6 = mock(EOStgStage.class);

	private StagesDroitsDecentralise sdd;

	@Before
	public void setUp() throws Exception {
		sdd = new StagesDroitsDecentralise() {
			
			private FactoryFinderScolarite factoryFinder = new FactoryFinderScolarite("");

			@Override
			public FactoryFinderScolarite getFactoryFinder() {
				return factoryFinder;
			};
			
			@Override
			public void setFactoryFinder(FactoryFinderScolarite factoryFinder) {
				this.factoryFinder = factoryFinder;
			};
			
			
			@Override
			protected List<EC> getEcByResponsabilite(Integer persId, Integer fannKey, EOEditingContext edc, String typeResponsabilite) {

				FinderResponsable finderResponsable = factoryFinder.getFinderResponsable(edc);

				if (finderResponsable.getTypeResponsable().equals(typeResponsabilite) && respPedaPersId.contains(persId)) {
					if (ANNEE_1 == fannKey) {
						return Arrays.asList(unEc_1, unEc_2, unEc_3);
					} else {
						return new ArrayList<EC>();
					}
				} else if (finderResponsable.getTypeSecretaire().equals(typeResponsabilite) && respAdmPersId.contains(persId)) {
					if (ANNEE_1 == fannKey) {
						return Arrays.asList(unEc_4, unEc_5);
					} else {
						return new ArrayList<EC>();
					}
				} else if (finderResponsable.getTypeEnseignant().equals(typeResponsabilite) && ensPersId.contains(persId)) {
					if (ANNEE_1 == fannKey) {
						return Arrays.asList(unEc_6);
					} else {
						return new ArrayList<EC>();
					}
				}
				return new ArrayList<EC>();
			}

			@Override
			protected NSArray<EOStgStage> getLesStagesByResponsabilite(Integer persId, Integer fannKey, EOEditingContext edc, String typeResponsabilite) {
				FinderResponsable finderResponsable = factoryFinder.getFinderResponsable(edc);

				if (ANNEE_2 == fannKey) {
					return NSArray.EmptyArray;
				}
				if (finderResponsable.getTypeResponsable().equals(typeResponsabilite) && respPedaPersId.contains(persId)) {
					return new NSArray<EOStgStage>(unstage_1, unstage_2, unstage_3);
				} else if (finderResponsable.getTypeSecretaire().equals(typeResponsabilite) && respAdmPersId.contains(persId)) {
					return new NSArray<EOStgStage>(unstage_4, unstage_5);
				} else if (finderResponsable.getTypeEnseignant().equals(typeResponsabilite) && ensPersId.contains(persId)) {
					return new NSArray<EOStgStage>(unstage_6);
				}
				return NSArray.EmptyArray;
			}

			@Override
			protected List<Integer> getPersIdResponsablesPedagogiques(EOStgStage stage) {
				if (stage.equals(unstage_1) || stage.equals(unstage_2) || stage.equals(unstage_3)) {
					return respPedaPersId;
				}
				return new ArrayList<Integer>();
			}

			@Override
			protected List<Integer> getPersIdResponsablesAdministratifs(EOStgStage stage) {
				if (stage.equals(unstage_4) || stage.equals(unstage_5)) {
					return respAdmPersId;
				}
				return new ArrayList<Integer>();
			}

			@Override
			protected List<Integer> getPersIdResponsablesAdministratifs(EC mec) {
				if (mec.equals(unEc_4) || mec.equals(unEc_5)) {
					return respAdmPersId;
				}
				return new ArrayList<Integer>();
			}

			@Override
			protected List<Integer> getPersIdResponsablesAdministratifs(IEtudiant etudiant, Integer fannKey) {
				if (ANNEE_1 == fannKey) {
					return respAdmPersId;
				}
				return new ArrayList<Integer>();
			}
		};

	}

	@Test
	public void testIsResponsableAdministratif_Stage_PersId() {
		assertFalse(sdd.isResponsableAdministratif(unstage_1, RESP_ADM_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_2, RESP_ADM_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_3, RESP_ADM_PERS_ID));
		assertTrue(sdd.isResponsableAdministratif(unstage_4, RESP_ADM_PERS_ID));
		assertTrue(sdd.isResponsableAdministratif(unstage_5, RESP_ADM_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_6, RESP_ADM_PERS_ID));

		assertFalse(sdd.isResponsableAdministratif(unstage_1, RESP_PEDA_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_2, RESP_PEDA_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_3, RESP_PEDA_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_4, RESP_PEDA_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_5, RESP_PEDA_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_6, RESP_PEDA_PERS_ID));

		assertFalse(sdd.isResponsableAdministratif(unstage_1, ENS_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_2, ENS_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_3, ENS_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_4, ENS_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_5, ENS_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unstage_6, ENS_PERS_ID));
	}

	@Test
	public void testIsResponsableAdministratif_UnEc_PersId() {

		assertFalse(sdd.isResponsableAdministratif(unEc_1, RESP_ADM_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unEc_2, RESP_ADM_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unEc_3, RESP_ADM_PERS_ID));
		assertTrue(sdd.isResponsableAdministratif(unEc_4, RESP_ADM_PERS_ID));
		assertTrue(sdd.isResponsableAdministratif(unEc_5, RESP_ADM_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unEc_6, RESP_ADM_PERS_ID));

		assertFalse(sdd.isResponsableAdministratif(unEc_4, RESP_PEDA_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unEc_5, RESP_PEDA_PERS_ID));

		assertFalse(sdd.isResponsableAdministratif(unEc_4, ENS_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(unEc_5, ENS_PERS_ID));
	}

	@Test
	public void testIsResponsableAdministratif_PersId_FannKey() {
		assertTrue(sdd.isResponsableAdministratif(RESP_ADM_PERS_ID, ANNEE_1));
		assertFalse(sdd.isResponsableAdministratif(RESP_ADM_PERS_ID, ANNEE_2));

		assertFalse(sdd.isResponsableAdministratif(RESP_PEDA_PERS_ID, ANNEE_1));
		assertFalse(sdd.isResponsableAdministratif(RESP_PEDA_PERS_ID, ANNEE_2));

		assertFalse(sdd.isResponsableAdministratif(ENS_PERS_ID, ANNEE_1));
		assertFalse(sdd.isResponsableAdministratif(ENS_PERS_ID, ANNEE_2));
	}

	@Test
	public void testIsResponsablePedagogique_PersId_FannKey() {
		assertTrue(sdd.isResponsablePedagogique(RESP_PEDA_PERS_ID, ANNEE_1));
		assertFalse(sdd.isResponsablePedagogique(RESP_PEDA_PERS_ID, ANNEE_2));

		assertFalse(sdd.isResponsablePedagogique(RESP_ADM_PERS_ID, ANNEE_1));
		assertFalse(sdd.isResponsablePedagogique(RESP_ADM_PERS_ID, ANNEE_2));

		assertFalse(sdd.isResponsablePedagogique(ENS_PERS_ID, ANNEE_1));
		assertFalse(sdd.isResponsablePedagogique(ENS_PERS_ID, ANNEE_2));
	}

	@Test
	public void testIsResponsableAdministratif_Etudiant_FannKey_PersId() {
		assertTrue(sdd.isResponsableAdministratif(null, ANNEE_1, RESP_ADM_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(null, ANNEE_2, RESP_ADM_PERS_ID));

		assertFalse(sdd.isResponsableAdministratif(null, ANNEE_1, RESP_PEDA_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(null, ANNEE_2, RESP_PEDA_PERS_ID));

		assertFalse(sdd.isResponsableAdministratif(null, ANNEE_1, ENS_PERS_ID));
		assertFalse(sdd.isResponsableAdministratif(null, ANNEE_2, ENS_PERS_ID));
	}

	@Test
	public void testIsResponsablePedagogique_Stage_PersId() {
		assertTrue(sdd.isResponsablePedagogique(unstage_1, RESP_PEDA_PERS_ID));
		assertTrue(sdd.isResponsablePedagogique(unstage_2, RESP_PEDA_PERS_ID));
		assertTrue(sdd.isResponsablePedagogique(unstage_3, RESP_PEDA_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_4, RESP_PEDA_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_5, RESP_PEDA_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_6, RESP_PEDA_PERS_ID));

		assertFalse(sdd.isResponsablePedagogique(unstage_1, RESP_ADM_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_2, RESP_ADM_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_3, RESP_ADM_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_4, RESP_ADM_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_5, RESP_ADM_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_6, RESP_ADM_PERS_ID));

		assertFalse(sdd.isResponsablePedagogique(unstage_1, ENS_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_2, ENS_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_3, ENS_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_4, ENS_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_5, ENS_PERS_ID));
		assertFalse(sdd.isResponsablePedagogique(unstage_6, ENS_PERS_ID));
	}

//	@Test
//	public void testGetLesStagesByEnseignant() {
//		assertTrue(sdd.getLesStagesByEnseignant(ec, ENS_PERS_ID, ANNEE_1).containsObject(unstage_6));
//		assertTrue(sdd.getLesStagesByEnseignant(ec, ENS_PERS_ID, ANNEE_2).isEmpty());
//
//		assertTrue(sdd.getLesStagesByEnseignant(ec, RESP_ADM_PERS_ID, ANNEE_1).isEmpty());
//		assertTrue(sdd.getLesStagesByEnseignant(ec, RESP_ADM_PERS_ID, ANNEE_2).isEmpty());
//		assertTrue(sdd.getLesStagesByEnseignant(ec, RESP_PEDA_PERS_ID, ANNEE_1).isEmpty());
//		assertTrue(sdd.getLesStagesByEnseignant(ec, RESP_PEDA_PERS_ID, ANNEE_2).isEmpty());
//
//	}
//
//	@Test
//	public void testGetLesStagesBySecretaire() {
//		assertTrue(sdd.getLesStagesBySecretaire(ec, RESP_ADM_PERS_ID, ANNEE_1).containsObject(unstage_4));
//		assertTrue(sdd.getLesStagesBySecretaire(ec, RESP_ADM_PERS_ID, ANNEE_1).containsObject(unstage_5));
//		assertTrue(sdd.getLesStagesBySecretaire(ec, RESP_ADM_PERS_ID, ANNEE_2).isEmpty());
//
//		assertTrue(sdd.getLesStagesBySecretaire(ec, RESP_PEDA_PERS_ID, ANNEE_1).isEmpty());
//		assertTrue(sdd.getLesStagesBySecretaire(ec, RESP_PEDA_PERS_ID, ANNEE_2).isEmpty());
//		assertTrue(sdd.getLesStagesBySecretaire(ec, ENS_PERS_ID, ANNEE_1).isEmpty());
//		assertTrue(sdd.getLesStagesBySecretaire(ec, ENS_PERS_ID, ANNEE_2).isEmpty());
//	}
//
//	@Test
//	public void testGetLesStagesByResponsable() {
//		assertTrue(sdd.getLesStagesByResponsable(ec, RESP_PEDA_PERS_ID, ANNEE_1).containsObject(unstage_1));
//		assertTrue(sdd.getLesStagesByResponsable(ec, RESP_PEDA_PERS_ID, ANNEE_1).containsObject(unstage_2));
//		assertTrue(sdd.getLesStagesByResponsable(ec, RESP_PEDA_PERS_ID, ANNEE_1).containsObject(unstage_3));
//		assertTrue(sdd.getLesStagesByResponsable(ec, RESP_PEDA_PERS_ID, ANNEE_2).isEmpty());
//
//		assertTrue(sdd.getLesStagesByResponsable(ec, RESP_ADM_PERS_ID, ANNEE_1).isEmpty());
//		assertTrue(sdd.getLesStagesByResponsable(ec, RESP_ADM_PERS_ID, ANNEE_2).isEmpty());
//		assertTrue(sdd.getLesStagesByResponsable(ec, ENS_PERS_ID, ANNEE_1).isEmpty());
//		assertTrue(sdd.getLesStagesByResponsable(ec, ENS_PERS_ID, ANNEE_2).isEmpty());
//	}
//
//	@Test
//	public void testGetLesEcByResponsable() {
//		assertTrue(sdd.getLesEcByResponsable(ec, RESP_PEDA_PERS_ID, ANNEE_1).containsObject(unEc_1));
//		assertTrue(sdd.getLesEcByResponsable(ec, RESP_PEDA_PERS_ID, ANNEE_1).containsObject(unEc_2));
//		assertTrue(sdd.getLesEcByResponsable(ec, RESP_PEDA_PERS_ID, ANNEE_1).containsObject(unEc_3));
//		assertTrue(sdd.getLesEcByResponsable(ec, RESP_PEDA_PERS_ID, ANNEE_2).isEmpty());
//
//		assertTrue(sdd.getLesEcByResponsable(ec, RESP_ADM_PERS_ID, ANNEE_1).isEmpty());
//		assertTrue(sdd.getLesEcByResponsable(ec, RESP_ADM_PERS_ID, ANNEE_2).isEmpty());
//		assertTrue(sdd.getLesEcByResponsable(ec, ENS_PERS_ID, ANNEE_1).isEmpty());
//		assertTrue(sdd.getLesEcByResponsable(ec, ENS_PERS_ID, ANNEE_2).isEmpty());
//	}
//
//	@Test
//	public void testGetLesEcBySecretaire() {
//		assertTrue(sdd.getLesEcBySecretaire(ec, RESP_ADM_PERS_ID, ANNEE_1).containsObject(unEc_4));
//		assertTrue(sdd.getLesEcBySecretaire(ec, RESP_ADM_PERS_ID, ANNEE_1).containsObject(unEc_5));
//		assertTrue(sdd.getLesEcBySecretaire(ec, RESP_ADM_PERS_ID, ANNEE_2).isEmpty());
//
//		assertTrue(sdd.getLesEcBySecretaire(ec, RESP_PEDA_PERS_ID, ANNEE_1).isEmpty());
//		assertTrue(sdd.getLesEcBySecretaire(ec, RESP_PEDA_PERS_ID, ANNEE_2).isEmpty());
//		assertTrue(sdd.getLesEcBySecretaire(ec, ENS_PERS_ID, ANNEE_1).isEmpty());
//		assertTrue(sdd.getLesEcBySecretaire(ec, ENS_PERS_ID, ANNEE_2).isEmpty());
//	}
//
//	@Test
//	public void testGetLesEcByEnseignant() {
//		assertTrue(sdd.getLesEcByEnseignant(ec, ENS_PERS_ID, ANNEE_1).containsObject(unEc_6));
//		assertTrue(sdd.getLesEcByEnseignant(ec, ENS_PERS_ID, ANNEE_2).isEmpty());
//
//		assertTrue(sdd.getLesEcByEnseignant(ec, RESP_ADM_PERS_ID, ANNEE_1).isEmpty());
//		assertTrue(sdd.getLesEcByEnseignant(ec, RESP_ADM_PERS_ID, ANNEE_2).isEmpty());
//		assertTrue(sdd.getLesEcByEnseignant(ec, RESP_PEDA_PERS_ID, ANNEE_1).isEmpty());
//		assertTrue(sdd.getLesEcByEnseignant(ec, RESP_PEDA_PERS_ID, ANNEE_2).isEmpty());
//	}
}
