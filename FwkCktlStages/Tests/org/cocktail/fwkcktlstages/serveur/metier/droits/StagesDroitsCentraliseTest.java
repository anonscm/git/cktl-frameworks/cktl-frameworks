package org.cocktail.fwkcktlstages.serveur.metier.droits;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlstages.serveur.finder.FinderStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgAnnee;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.cocktail.fwkcktlstages.serveur.metier.scolarite.EC;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class StagesDroitsCentraliseTest{

	private static final int ANNEE_1 = 2012;
	private static final int ANNEE_2 = 2013;
	private static final List<Integer> respAdmPersId = Arrays.asList(1, 2, 3, 4);
	private static final List<Integer> respPedaPersId = Arrays.asList(5, 6, 7, 8);

	private StagesDroitsCentralise sdc;

	private EOStgStage unStage_1 = mock(EOStgStage.class);
	private EOStgAnnee uneAnnee_1 = mock(EOStgAnnee.class);
	private EOStgStage unStage_2 = mock(EOStgStage.class);
	private EOStgAnnee uneAnnee_2 = mock(EOStgAnnee.class);
	private EC unEc_1 = mock(EC.class);
	private EC unEc_2 = mock(EC.class);

	private EOStgStage unstage_1 = mock(EOStgStage.class);
	private EOStgStage unstage_2 = mock(EOStgStage.class);
	private EOStgStage unstage_3 = mock(EOStgStage.class);
	
	private FinderStgStage finderStageInstance;
	
	/** {@inheritDoc} */
	@Before
	//@Override
	public void setUp() throws Exception {
		sdc = new StagesDroitsCentralise() {
			@Override
			protected List<Integer> getListeResponsablesAdministratifs() {
				return respAdmPersId;
			}

			@Override
			protected List<Integer> getListeResponsablesPedagogiques() {
				return respPedaPersId;
			}

			@Override
			protected NSArray<EOStgStage> getStages(EOEditingContext edc, Integer fannKey) {
				if (ANNEE_1 == fannKey) {
					return new NSArray<EOStgStage>(unstage_1);
				} else {
					return new NSArray<EOStgStage>(unstage_2, unstage_3);
				}
			}
		};
		when(unStage_1.toAnnee()).thenReturn(uneAnnee_1);
		when(unStage_2.toAnnee()).thenReturn(uneAnnee_2);
		when(uneAnnee_1.sanDebut()).thenReturn(ANNEE_1);
		when(uneAnnee_2.sanDebut()).thenReturn(ANNEE_2);

		when(unEc_1.getAnnee()).thenReturn(ANNEE_1);
		when(unEc_2.getAnnee()).thenReturn(ANNEE_2);
		
		finderStageInstance = mock(FinderStgStage.class);
		FinderStgStage.setInstance(finderStageInstance);
	}

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCentralise#isResponsableAdministratif(java.lang.Integer, java.lang.Integer)}
	 * .
	 */
	@Test
	public void testIsResponsableAdministratif_PersId_FannKey() {
		assertTrue(sdc.isResponsableAdministratif(1, ANNEE_1));
		assertTrue(sdc.isResponsableAdministratif(1, ANNEE_2));
		assertFalse(sdc.isResponsableAdministratif(5, ANNEE_1));
		assertFalse(sdc.isResponsableAdministratif(5, ANNEE_2));
	}

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCentralise#isResponsableAdministratif(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage, java.lang.Integer)}
	 * .
	 */
	@Test
	public void testIsResponsableAdministratif_Stage_PersId() {
		assertTrue(sdc.isResponsableAdministratif(unStage_1, 1));
		assertTrue(sdc.isResponsableAdministratif(unStage_2, 1));
		assertFalse(sdc.isResponsableAdministratif(unStage_1, 5));
		assertFalse(sdc.isResponsableAdministratif(unStage_2, 5));
	}

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCentralise#isResponsableAdministratif(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc, java.lang.Integer)}
	 * .
	 */
	@Test
	public void testIsResponsableAdministratif_Mec_PersId() {
		assertTrue(sdc.isResponsableAdministratif(unEc_1, 1));
		assertTrue(sdc.isResponsableAdministratif(unEc_2, 1));
		assertFalse(sdc.isResponsableAdministratif(unEc_1, 5));
		assertFalse(sdc.isResponsableAdministratif(unEc_2, 5));
	}

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCentralise#isResponsableAdministratif(org.cocktail.scolarix.serveur.interfaces.IEtudiant, java.lang.Integer, java.lang.Integer)}
	 * .
	 */
	@Test
	public void testIsResponsableAdministratif_Etudiant_FannKey_PersId() {
		assertTrue(sdc.isResponsableAdministratif(null, ANNEE_1, 1));
		assertTrue(sdc.isResponsableAdministratif(null, ANNEE_2, 1));
		assertFalse(sdc.isResponsableAdministratif(null, ANNEE_1, 5));
		assertFalse(sdc.isResponsableAdministratif(null, ANNEE_2, 5));
	}

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCentralise#isResponsablePedagogique(java.lang.Integer, java.lang.Integer)}
	 * .
	 */
	@Test
	public void testIsResponsablePedagogique_PersId_FannKey() {
		assertTrue(sdc.isResponsablePedagogique(5, ANNEE_1));
		assertTrue(sdc.isResponsablePedagogique(5, ANNEE_2));
		assertFalse(sdc.isResponsablePedagogique(1, ANNEE_1));
		assertFalse(sdc.isResponsablePedagogique(1, ANNEE_2));
	}

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCentralise#isResponsablePedagogique(org.cocktail.fwkcktlstages.serveur.metier.EOStgStage, java.lang.Integer)}
	 * .
	 */
	@Test
	public void testIsResponsablePedagogique_Stage_PersId() {
		assertTrue(sdc.isResponsablePedagogique(unStage_1, 5));
		assertTrue(sdc.isResponsablePedagogique(unStage_2, 5));
		assertFalse(sdc.isResponsablePedagogique(unStage_1, 1));
		assertFalse(sdc.isResponsablePedagogique(unStage_2, 1));
	}

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCentralise#getLesStagesByResponsable(com.webobjects.eocontrol.EOEditingContext, java.lang.Integer, java.lang.Integer)}
	 * .
	 */
	@Test
	public void testGetLesStagesByResponsable() {
		assertFalse(sdc.getLesStagesByResponsable(null, 5, ANNEE_1).isEmpty());
		assertFalse(sdc.getLesStagesByResponsable(null, 5, ANNEE_2).isEmpty());

		assertTrue(sdc.getLesStagesByResponsable(null, 5, ANNEE_1).count() == 1);
		assertTrue(sdc.getLesStagesByResponsable(null, 5, ANNEE_2).count() == 2);

		assertTrue(sdc.getLesStagesByResponsable(null, 1, ANNEE_1).isEmpty());
		assertTrue(sdc.getLesStagesByResponsable(null, 1, ANNEE_2).isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCentralise#getLesStagesBySecretaire(com.webobjects.eocontrol.EOEditingContext, java.lang.Integer, java.lang.Integer)}
	 * .
	 */
	@Test
	public void testGetLesStagesBySecretaire() {
		assertFalse(sdc.getLesStagesBySecretaire(null, 1, ANNEE_1).isEmpty());
		assertFalse(sdc.getLesStagesBySecretaire(null, 1, ANNEE_2).isEmpty());

		assertTrue(sdc.getLesStagesBySecretaire(null, 1, ANNEE_1).count() == 1);
		assertTrue(sdc.getLesStagesBySecretaire(null, 1, ANNEE_2).count() == 2);

		assertTrue(sdc.getLesStagesBySecretaire(null, 5, ANNEE_1).isEmpty());
		assertTrue(sdc.getLesStagesBySecretaire(null, 5, ANNEE_2).isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCentralise#getLesStagesByEnseignant(com.webobjects.eocontrol.EOEditingContext, java.lang.Integer, java.lang.Integer)}
	 * .
	 */
	@Test
	public void testGetLesStagesByEnseignant() {
		assertTrue(sdc.getLesStagesByEnseignant(null, 1, ANNEE_1).isEmpty());
		assertTrue(sdc.getLesStagesByEnseignant(null, 1, ANNEE_2).isEmpty());
		assertTrue(sdc.getLesStagesByEnseignant(null, 5, ANNEE_1).isEmpty());
		assertTrue(sdc.getLesStagesByEnseignant(null, 5, ANNEE_2).isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCentralise#getLesEcByResponsable(com.webobjects.eocontrol.EOEditingContext, java.lang.Integer, java.lang.Integer)}
	 * .
	 */
	@Test
	public void testGetLesEcByResponsable() {
		assertTrue(sdc.getLesStagesByEnseignant(null, 1, ANNEE_1).isEmpty());
		assertTrue(sdc.getLesStagesByEnseignant(null, 1, ANNEE_2).isEmpty());
		assertTrue(sdc.getLesStagesByEnseignant(null, 5, ANNEE_1).isEmpty());
		assertTrue(sdc.getLesStagesByEnseignant(null, 5, ANNEE_2).isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCentralise#getLesEcBySecretaire(com.webobjects.eocontrol.EOEditingContext, java.lang.Integer, java.lang.Integer)}
	 * .
	 */
	@Test
	public void testGetLesEcBySecretaire() {
		assertTrue(sdc.getLesStagesByEnseignant(null, 1, ANNEE_1).isEmpty());
		assertTrue(sdc.getLesStagesByEnseignant(null, 1, ANNEE_2).isEmpty());
		assertTrue(sdc.getLesStagesByEnseignant(null, 5, ANNEE_1).isEmpty());
		assertTrue(sdc.getLesStagesByEnseignant(null, 5, ANNEE_2).isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlstages.serveur.metier.droits.StagesDroitsCentralise#getLesEcByEnseignant(com.webobjects.eocontrol.EOEditingContext, java.lang.Integer, java.lang.Integer)}
	 * .
	 */
	@Test
	public void testGetLesEcByEnseignant() {
		assertTrue(sdc.getLesStagesByEnseignant(null, 1, ANNEE_1).isEmpty());
		assertTrue(sdc.getLesStagesByEnseignant(null, 1, ANNEE_2).isEmpty());
		assertTrue(sdc.getLesStagesByEnseignant(null, 5, ANNEE_1).isEmpty());
		assertTrue(sdc.getLesStagesByEnseignant(null, 5, ANNEE_2).isEmpty());
	}
}
