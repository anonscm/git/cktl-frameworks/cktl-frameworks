package org.cocktail.fwkcktlstages.serveur.metier.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlstages.serveur.finder.FinderStgConfig;
import org.cocktail.fwkcktlstages.serveur.metier.EOStgStage;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class EOStageHelperTest {

	@Before
	public void setUp() throws Exception {
	}

	private EOStageHelper sH() {
		return EOStageHelper.getInstance();
	}

	@Test
	public void testIsGratificationValideComportementParDefaut() {
		EOStgStage stage = mock(EOStgStage.class);
		sH().checkGratification(stage);
	}

	@Test
	public void testIsGratificationNonValide() {
		EOStructure organismeAccueil = mock(EOStructure.class);

		EOStgStage stage = getStubStage(BigDecimal.valueOf(3), 2, 100, 1000, EOStageHelper.UNITE_MOIS);
		when(stage.toOrganismeAccueil()).thenReturn(organismeAccueil);

		EOStructureHelper structureHelper = mock(EOStructureHelper.class);
		when(structureHelper.isOrganismeEtranger(organismeAccueil)).thenReturn(false);
		EOStructureHelper.setInstance(structureHelper);

		FinderStgConfig stageConfig = spy(FinderStgConfig.getInstance());
		doReturn(23.0).when(stageConfig).getPlafondHoraire();
		doReturn(12.5).when(stageConfig).getTxPlafondHoraire();
		doReturn(151.67).when(stageConfig).getDureeMensuelle();

		EOStageHelper stageHelper = spy(new EOStageHelper());
		doReturn(EOStageHelper.MOYENNE_SEMAINES_PAR_MOIS).when(stageHelper).getDureeMinEnSemainesGratObligatoire();
		doReturn(200.0).when(stageHelper).getMontantGratificationObligatoire();
		EOStageHelper.setInstance(stageHelper);

		try {
			stageHelper.checkGratification(stage);
			fail("Exception attendue");
		} catch (ValidationException e) {
		} catch (Exception e) {
			fail("Exception non attendue " + e.getMessage());
		}
	}

	@Test
	public void testIsGratificationValide() {
		EOStgStage stage = getStubStage(BigDecimal.valueOf(3), 2, 5000, 1000, EOStageHelper.UNITE_MOIS);
		sH().checkGratification(stage);
	}

	@Test
	public void testIsGratificationValideOrganismeEtranger() {
		EOStgStage stage = getStubStage(BigDecimal.valueOf(3), 2, 100, 1000, EOStageHelper.UNITE_MOIS);
		sH().checkGratification(stage);
	}

	@Test
	public void testIsGratificationValideDureeInferieureADureeMinimale() {
		EOStgStage stage = getStubStage(BigDecimal.valueOf(2), 2, 100, 1000, EOStageHelper.UNITE_MOIS);
		sH().checkGratification(stage);
	}

	/**
	 * @return
	 */
	protected EOStgStage getStubStage(final BigDecimal dureeTotaleVal, final int dureeMinGratificationObligatoire, final double montantGratification,
	    final double montantGratificationObligatoire, final String uniteDuree) {

		EOStgStage stubStage = mock(EOStgStage.class);
		when(stubStage.toOrganismeAccueil()).thenReturn(null);
		when(stubStage.stgDureeTotaleVal()).thenReturn(dureeTotaleVal);
		when(stubStage.stgGratMontant()).thenReturn((double) montantGratification);
		when(stubStage.stgDureeTotaleUnite()).thenReturn(uniteDuree);

		EOStageHelper stageHelper = mock(EOStageHelper.class);
		when(stageHelper.getDureeMinEnSemainesGratObligatoire()).thenReturn(dureeMinGratificationObligatoire * EOStageHelper.MOYENNE_SEMAINES_PAR_MOIS);
		when(stageHelper.getMontantGratificationObligatoire()).thenReturn(montantGratificationObligatoire);
		EOStageHelper.setInstance(stageHelper);

		return stubStage;
	}

	@Test
	public void testCalculDureeTotaleEnSemaines() {
		final double delta = 0.01;
		EOStageHelper.setInstance(new EOStageHelper());
		assertEquals(0.00, sH().getDureeTotaleEnSemaines(BigDecimal.valueOf(0), EOStageHelper.UNITE_JOUR), delta);
		assertEquals(0.14, sH().getDureeTotaleEnSemaines(BigDecimal.valueOf(1), EOStageHelper.UNITE_JOUR), delta);
		assertEquals(0.85, sH().getDureeTotaleEnSemaines(BigDecimal.valueOf(6), EOStageHelper.UNITE_JOUR), delta);
		assertEquals(1.00, sH().getDureeTotaleEnSemaines(BigDecimal.valueOf(7), EOStageHelper.UNITE_JOUR), delta);
		assertEquals(1.14, sH().getDureeTotaleEnSemaines(BigDecimal.valueOf(8), EOStageHelper.UNITE_JOUR), delta);

		assertEquals(0, sH().getDureeTotaleEnSemaines(BigDecimal.valueOf(0), EOStageHelper.UNITE_SEMAINE), delta);
		assertEquals(1, sH().getDureeTotaleEnSemaines(BigDecimal.valueOf(1), EOStageHelper.UNITE_SEMAINE), delta);

		assertEquals(0.00, sH().getDureeTotaleEnSemaines(BigDecimal.valueOf(0), EOStageHelper.UNITE_MOIS), delta);
		assertEquals(1.00 * EOStageHelper.MOYENNE_SEMAINES_PAR_MOIS, sH().getDureeTotaleEnSemaines(BigDecimal.valueOf(1), EOStageHelper.UNITE_MOIS), delta);
	}

	@Test
	public void testCalculDureeTotaleEnSemainesUniteInconnue() {
		try {
			EOStageHelper.setInstance(new EOStageHelper());
			sH().getDureeTotaleEnSemaines(BigDecimal.valueOf(0), "code inconnu");
			fail("Exception attendue");
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			fail("Exception non attendue " + e.getMessage());
		}
	}
}
