SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°1/1
-- Type : DML
-- Schéma modifié :  STAGES
-- Schéma d'execution du script : STAGES
-- Numéro de version :  0.1.1.0
-- Date de publication : 25/06/2012
-- Licence : CeCILL version 2
--
--

/**
 * Ajout du type "Convention de stage" pour les pièces jointes  
 */

whenever sqlerror exit sql.sqlcode ;

--
-- DB_VERSION
--
INSERT INTO STAGES.DB_VERSION VALUES(STAGES.DB_VERSION_SEQ.NEXTVAL, '0.1.1.0', TO_DATE('26/06/2012', 'DD/MM/YYYY'),NULL,'Type convention');

--
-- Type convention
--
INSERT INTO STAGES.STG_PIECE_JOINTE_TYPE (SPJT_ID, SPJT_LC, SPJT_LL) VALUES (STAGES.STG_PIECE_JOINTE_TYPE_SEQ.NEXTVAL,'CONV', 'Convention de stage');

--
-- DB_VERSION
--
UPDATE STAGES.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='0.1.1.0';