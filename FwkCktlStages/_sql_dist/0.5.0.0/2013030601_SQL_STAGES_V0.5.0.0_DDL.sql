SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié : STAGES
-- Schéma d'execution du script : STAGES
-- Numéro de version :  0.5.0.0
-- Date de publication : 06/03/2013
-- Licence : CeCILL version 2
-- 
--

whenever sqlerror exit sql.sqlcode ;

--
-- DB_VERSION
--
INSERT INTO STAGES.DB_VERSION VALUES(STAGES.DB_VERSION_SEQ.NEXTVAL, '0.5.0.0', TO_DATE('06/03/2013', 'DD/MM/YYYY'),NULL,'Avenants');

--
-- Ajout de champs dans la table STG_AVENANT
--
ALTER TABLE STAGES.STG_AVENANT 
ADD (
	SAV_SUJET				VARCHAR2(2000 BYTE), 
	SAV_D_SUPPRESSION		DATE,
	PERS_ID_SUPPRESSION		NUMBER(12, 0),
	SAV_VALIDE				VARCHAR2(1 BYTE),
	PERS_ID_VERROUILLAGE	NUMBER(12, 0),
	SAV_D_VERROUILLAGE		DATE 
);
COMMENT ON COLUMN STAGES.STG_AVENANT.SAV_SUJET IS 'Un nouveau sujet de stage';
COMMENT ON COLUMN STAGES.STG_AVENANT.SAV_D_SUPPRESSION IS 'Date de suppression';
COMMENT ON COLUMN STAGES.STG_AVENANT.PERS_ID_SUPPRESSION IS 'Pers_id de la personne qui a supprimé l''avenant';
COMMENT ON COLUMN STAGES.STG_AVENANT.SAV_VALIDE IS 'Indique si l''avenant est valide (''O'', ''N'')';
COMMENT ON COLUMN STAGES.STG_AVENANT.PERS_ID_VERROUILLAGE IS 'Pers_id de la personne qui a verrouillé l''avenant';
COMMENT ON COLUMN STAGES.STG_AVENANT.SAV_D_VERROUILLAGE IS 'Date de verrouillage';


