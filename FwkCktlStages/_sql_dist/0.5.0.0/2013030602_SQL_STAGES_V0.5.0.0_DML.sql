SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°2/2
-- Type : DDL
-- Schéma modifié : STAGES
-- Schéma d'execution du script : STAGES
-- Numéro de version :  0.5.0.0
-- Date de publication : 06/03/2013
-- Licence : CeCILL version 2
-- 
--

whenever sqlerror exit sql.sqlcode ;

--
-- Ajout d'évenements
--
INSERT INTO STAGES.STG_EVENEMENT_TYPE (SEVT_ID, SEVT_LC, SEVT_LL) 
	VALUES	(STAGES.STG_EVENEMENT_TYPE_SEQ.NEXTVAL, 'NEWAV', 'Création d''un avenant');

INSERT INTO STAGES.STG_EVENEMENT_TYPE (SEVT_ID, SEVT_LC, SEVT_LL) 
  VALUES (STAGES.STG_EVENEMENT_TYPE_SEQ.NEXTVAL, 'VALAV', 'Validation d''un avenant');
  
INSERT INTO STAGES.STG_EVENEMENT_TYPE (SEVT_ID, SEVT_LC, SEVT_LL) 
  VALUES (STAGES.STG_EVENEMENT_TYPE_SEQ.NEXTVAL, 'SUPAV', 'Suppression d''un avenant');

INSERT INTO STAGES.STG_EVENEMENT_TYPE (SEVT_ID, SEVT_LC, SEVT_LL) 
  VALUES (STAGES.STG_EVENEMENT_TYPE_SEQ.NEXTVAL, 'REFAV', 'Refus d''un avenant');
  
INSERT INTO STAGES.STG_EVENEMENT_TYPE (SEVT_ID, SEVT_LC, SEVT_LL) 
  VALUES (STAGES.STG_EVENEMENT_TYPE_SEQ.NEXTVAL, 'VERAV', 'Verrouillage d''un avenant');

--
-- DB_VERSION
--
UPDATE STAGES.DB_VERSION SET DBV_INSTALL = SYSDATE WHERE DBV_LIBELLE = '0.5.0.0';
