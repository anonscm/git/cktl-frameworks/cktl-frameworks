SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°2/3
-- Type : DDL
-- Schéma modifié :  STAGES
-- Schéma d'execution du script : STAGES
-- Numéro de version :  0.4.0.0
-- Date de publication : 09/01/2013
-- Licence : CeCILL version 2
--
--

whenever sqlerror exit sql.sqlcode ;

/**
 * Adresse de la structure d'accueil
 */
ALTER TABLE STG_STAGE 
	ADD (ADR_STRUCTURE_ACCUEIL NUMBER);
COMMENT ON COLUMN STG_STAGE.ADR_STRUCTURE_ACCUEIL IS 'Identifiant de l''adresse de la structure d''accueil apparaissant sur la convention';

/**
 * Adresse du service de la sctructure d'accueil
 */
ALTER TABLE STG_STAGE 
	ADD (ADR_SERVICE_ACCUEIL NUMBER);
COMMENT ON COLUMN STG_STAGE.ADR_SERVICE_ACCUEIL IS 'Identifiant de l''adresse du service de la structure d''accueil apparaissant sur la convention';

