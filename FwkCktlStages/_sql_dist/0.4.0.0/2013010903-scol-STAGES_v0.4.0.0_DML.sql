SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°3/3
-- Type : DDL
-- Schéma modifié :  STAGES
-- Schéma d'execution du script : STAGES
-- Numéro de version :  0.4.0.0
-- Date de publication : 09/01/2013
-- Licence : CeCILL version 2
--
--

whenever sqlerror exit sql.sqlcode ;


-- Indicateur de convention spécifique
ALTER TABLE STAGES.STG_STAGE 
	ADD (CONV_SPECIFIQUE VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL);
COMMENT ON COLUMN STAGES.STG_STAGE.CONV_SPECIFIQUE IS 'Indique si on est dans le cadre d''une convention specifique';

-- Ajout d'évenements
INSERT INTO STAGES.STG_EVENEMENT_TYPE (SEVT_ID, SEVT_LC, SEVT_LL) 
	VALUES (STAGES.STG_EVENEMENT_TYPE_SEQ.NEXTVAL, 'ANNAD', 'Annulation validation administrative');
INSERT INTO STAGES.STG_EVENEMENT_TYPE (SEVT_ID, SEVT_LC, SEVT_LL) 
	VALUES (STAGES.STG_EVENEMENT_TYPE_SEQ.NEXTVAL, 'RESIL', 'Résilisation de la convention');

-- Ajout de l'état convention résiliée
INSERT INTO STAGES.STG_ETAT (SET_ID, SET_ORDRE, SET_LC, SET_LL) 
	VALUES (STAGES.STG_ETAT_SEQ.NEXTVAL, '98', 'RESIL', 'Convention résiliée');
