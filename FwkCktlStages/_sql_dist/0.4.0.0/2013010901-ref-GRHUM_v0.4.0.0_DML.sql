SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°1/3
-- Type : DML
-- Schéma modifié : GRHUM
-- Schéma d'execution du script : GRHUM
-- Date de publication : 09/01/2013
-- Licence : CeCILL version 2
--
--

whenever sqlerror exit sql.sqlcode ;

/**
 * Structures et contrats
 */

--
-- Création des groupes "Organisme d'accueil" annulés/à valider/valides
--
declare

    localCStructureEntreprise   	GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE;	-- c_structure du groupe Entreprise
    
    localPersIdStructOrgaAnnule		GRHUM.STRUCTURE_ULR.PERS_ID%TYPE;		-- pers_id du groupe Organismes d'accueil annulés
    localPersIdStructOrgaAValider	GRHUM.STRUCTURE_ULR.PERS_ID%TYPE;		-- pers_id du groupe Organismes d'accueil à valider
    localPersIdStructOrgaValide		GRHUM.STRUCTURE_ULR.PERS_ID%TYPE;		-- pers_id du groupe Organismes d'accueil valides
    
    localCStructOrgaAnnule    		GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE;	-- c_structure du groupe Organismes d'accueil annulés
    localCStructOrgaAValider    	GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE;	-- c_structure du groupe Organismes d'accueil à valider
    localCStructOrgaValide    		GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE;	-- c_structure du groupe Organismes d'accueil valides
    
    paramValue                  	GRHUM.GRHUM_PARAMETRES.PARAM_VALUE%TYPE;
    typeIdCStructure				      GRHUM.GRHUM_PARAMETRES_TYPE.TYPE_ID%TYPE;
    noIndividuCreateur          	GRHUM.INDIVIDU_ULR.NO_INDIVIDU%TYPE;
    persIdCreateur          		  GRHUM.INDIVIDU_ULR.PERS_ID%TYPE;
    v_count                       NUMBER;
    

begin

	/**
	 * Structures
	 */

    -- Groupe Entreprise
    SELECT PARAM_VALUE into localCStructureEntreprise FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_ENTREPRISE';

    -- grpOwner et grpResponsable
    select no_individu into noIndividuCreateur from grhum.individu_ulr where pers_id =
     (SELECT PERS_ID FROM COMPTE WHERE CPT_LOGIN =
        (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE =
                (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR')));
                
 	select pers_id into persIdCreateur from grhum.individu_ulr where pers_id =
     (SELECT PERS_ID FROM COMPTE WHERE CPT_LOGIN =
        (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE =
                (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR')));
 
 
  SELECT COUNT(*) INTO v_count FROM GRHUM.structure_ulr WHERE lc_structure LIKE 'ORG_ACC-ANNULES';
  IF (v_count = 0) THEN
    -- Création de la structure Organismes d'accueil annulés
    GRHUM.Ins_Structure_Ulr ( localCStructOrgaAnnule, localPersIdStructOrgaAnnule, 'ORGANISMES D''ACCUEIL ANNULES', 'ORG_ACC-ANNULES', 'A', localCStructureEntreprise, NULL, NULL,
                  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                  NULL, NULL, NULL, NULL, noIndividuCreateur, noIndividuCreateur, NULL, NULL, NULL, NULL, NULL, NULL,
                  NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );
    INSERT INTO GRHUM.REPART_TYPE_GROUPE(C_STRUCTURE, TGRP_CODE, D_CREATION, D_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION)
      VALUES	(localCStructOrgaAnnule,'G',SYSDATE,SYSDATE,persIdCreateur,persIdCreateur);
  END IF;
  
  SELECT COUNT(*) INTO v_count FROM GRHUM.structure_ulr WHERE lc_structure LIKE 'ORG_ACC-A_VALIDER';
  IF (v_count = 0) THEN
    -- Création de la structure Organismes d'accueil à valider
    GRHUM.Ins_Structure_Ulr ( localCStructOrgaAValider, localPersIdStructOrgaAValider, 'ORGANISMES D''ACCUEIL A VALIDER', 'ORG_ACC-A_VALIDER', 'A', localCStructureEntreprise, NULL, NULL,
                  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                  NULL, NULL, NULL, NULL, noIndividuCreateur, noIndividuCreateur, NULL, NULL, NULL, NULL, NULL, NULL,
                  NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );
    INSERT INTO GRHUM.REPART_TYPE_GROUPE(C_STRUCTURE, TGRP_CODE, D_CREATION, D_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION)
      VALUES	(localCStructOrgaAValider,'G',SYSDATE,SYSDATE,persIdCreateur,persIdCreateur);
  END IF;
  
  SELECT COUNT(*) INTO v_count FROM GRHUM.structure_ulr WHERE lc_structure LIKE 'ORG_ACC-VALIDES';
  IF (v_count = 0) THEN
    -- Création de la structure Organismes d'accueil valides
    GRHUM.Ins_Structure_Ulr ( localCStructOrgaValide, localPersIdStructOrgaValide, 'ORGANISMES D''ACCUEIL VALIDES', 'ORG_ACC-VALIDES', 'A', localCStructureEntreprise, NULL, NULL,
                  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                  NULL, NULL, NULL, NULL, noIndividuCreateur, noIndividuCreateur, NULL, NULL, NULL, NULL, NULL, NULL,
                  NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );
    INSERT INTO GRHUM.REPART_TYPE_GROUPE(C_STRUCTURE, TGRP_CODE, D_CREATION, D_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION)
      VALUES	(localCStructOrgaValide,'G',SYSDATE,SYSDATE,persIdCreateur,persIdCreateur);
  END IF;


	/**
	 * CONTRATS
	 */
 
	-- Ajout du type de classification Convention de stage
	SELECT COUNT(*) INTO v_count FROM ACCORDS.TYPE_CLASSIFICATION_CONTRAT WHERE TCC_ID = 4;
	IF (v_count = 0) THEN
		INSERT INTO ACCORDS.TYPE_CLASSIFICATION_CONTRAT (TCC_CODE, TCC_ID, TCC_LIBELLE) 
			VALUES ('CONV_STAGE', '4', 'Convention de stage');
	END IF;
	
	-- Mise à jour des contrats déjà créés
	UPDATE 
	  ACCORDS.CONTRAT ac
	SET 
	  ac.TCC_ID = 4
	WHERE ac.CON_ORDRE IN
	( SELECT con.CON_ORDRE
	  FROM STAGES.STG_STAGE stg, ACCORDS.CONTRAT con
	  WHERE stg.CON_ORDRE = con.CON_ORDRE
	);

end;


/
