SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°1/1
-- Type : DML
-- Schéma modifié :  STAGES
-- Schéma d'execution du script : STAGES
-- Numéro de version :  0.2.0.0
-- Date de publication : 19/07/2012
-- Licence : CeCILL version 2
--
--

/**
 * Ajout d'un administrateur  
 */

whenever sqlerror exit sql.sqlcode ;

--
-- DB_VERSION
--
INSERT INTO STAGES.DB_VERSION VALUES(STAGES.DB_VERSION_SEQ.NEXTVAL, '0.2.0.0', TO_DATE('19/07/2012', 'DD/MM/YYYY'),NULL,'Admin');

-- Liste des persId des administrateurs, à séparer par des virgules
-- Liste modifiable dans l'application, mais au moins un administrateur doit être créé
INSERT INTO STAGES.STG_CONFIG (SCFG_ID, SCFG_LC, SCFG_VALEUR) VALUES (STAGES.STG_CONFIG_SEQ.NEXTVAL,'admins_pers_id', '24031');

--
-- DB_VERSION
--
UPDATE STAGES.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='0.2.0.0';