SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié : STAGES
-- Schéma d'execution du script : STAGES
-- Numéro de version :  0.6.0.0
-- Date de publication : 11/06/2013
-- Licence : CeCILL version 2
-- 
--

whenever sqlerror exit sql.sqlcode ;

--
-- Ajout d'évenements
--
INSERT INTO STAGES.STG_EVENEMENT_TYPE (SEVT_ID, SEVT_LC, SEVT_LL) 
	VALUES	(STAGES.STG_EVENEMENT_TYPE_SEQ.NEXTVAL, 'ANNPE', 'Annulation validation pédagogique');
INSERT INTO STAGES.STG_EVENEMENT_TYPE (SEVT_ID, SEVT_LC, SEVT_LL) 
	VALUES	(STAGES.STG_EVENEMENT_TYPE_SEQ.NEXTVAL, 'ANNRE', 'Annulation résiliation convention');
INSERT INTO STAGES.STG_EVENEMENT_TYPE (SEVT_ID, SEVT_LC, SEVT_LL) 
	VALUES	(STAGES.STG_EVENEMENT_TYPE_SEQ.NEXTVAL, 'ANNSU', 'Annulation suppression totale convention');
INSERT INTO STAGES.STG_EVENEMENT_TYPE (SEVT_ID, SEVT_LC, SEVT_LL) 
	VALUES	(STAGES.STG_EVENEMENT_TYPE_SEQ.NEXTVAL, 'MODAV', 'Modification d''un avenant');



-- INSERT INTO SCOLARITE.SCOL_MAQUETTE_RESPONSABLE_PAR (MPAR_KEY, PERS_ID, MBPAR_TYPE, FANN_KEY) VALUES ('1274', '91297', 'S', '2012');

