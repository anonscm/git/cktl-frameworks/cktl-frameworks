SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié : STAGES
-- Schéma d'execution du script : STAGES
-- Numéro de version :  0.6.0.0
-- Date de publication : 11/06/2013
-- Licence : CeCILL version 2
-- 
--

whenever sqlerror exit sql.sqlcode ;

--
-- Ajout des états
--
ALTER TABLE STAGES.STG_EVENEMENT 
  ADD (
      SET_ID_OLD NUMBER,
      SET_ID_NEW NUMBER 
);

COMMENT ON COLUMN STAGES.STG_EVENEMENT.SET_ID_OLD IS 'ID de l''état précédent';
COMMENT ON COLUMN STAGES.STG_EVENEMENT.SET_ID_NEW IS 'ID du nouvel état';

--
-- Mise à jour des évènements
--
UPDATE STAGES.STG_EVENEMENT 
   SET SET_ID_OLD = (
       SELECT DECODE (SEVT_LC, 
                     'INIT' , (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 0),
                     'DVALP', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 0),
                     
                     'MODIF', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 0),
                     'VALP' , (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 1),
                     'RVALP', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 1),
                     'VALT' , (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 3),
                     'RVALT', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 3),
                     'NEWTU', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 2),
                     'MODAD', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 4),
                     'VALAD', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 4),
                     'ANNAD', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 5),
                     'EDIT' , (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 5),
                     'SUPPF', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 6),
                     'SIGNA', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 6),
                     'RESIL', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7),
                     'SUPPC', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 0),
                     'NEWAV', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7),
                     'VALAV', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7),
                     'SUPAV', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7),
                     'REFAV', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7),
                     'VERAV', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7),
                     'ANNPE', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 4),
                     'ANNRE', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 98)
                     )
         FROM STAGES.STG_EVENEMENT e JOIN STAGES.STG_EVENEMENT_TYPE t ON e.SEVT_ID = t.SEVT_ID
        WHERE e.SEV_ID = STAGES.STG_EVENEMENT.SEV_ID
    
    
       ),
       SET_ID_NEW = (
       SELECT DECODE (SEVT_LC, 
                     'INIT',  (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 0),
                     'DVALP', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 1),
                     
                     'MODIF', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 0),
                     'VALP' , (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 4),
                     'RVALP', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 0),
                     'VALT' , (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 4),
                     'RVALT', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 2),
                     'NEWTU', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 3),
                     'MODAD', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 4),
                     'VALAD', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 5),
                     'ANNAD', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 4),
                     'EDIT' , (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 6),
                     'SUPPF', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 5),
                     'SIGNA', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7),
                     'RESIL', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 98),
                     'SUPPC', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 99),
                     'NEWAV', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7),
                     'VALAV', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7),
                     'SUPAV', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7),
                     'REFAV', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7),
                     'VERAV', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7),
                     'ANNPE', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 1),
                     'ANNRE', (SELECT SET_ID FROM STAGES.STG_ETAT WHERE SET_ORDRE = 7)
                     )
         FROM STAGES.STG_EVENEMENT e 
         JOIN STAGES.STG_EVENEMENT_TYPE t ON e.SEVT_ID = t.SEVT_ID
        WHERE e.SEV_ID = STAGES.STG_EVENEMENT.SEV_ID
       )
 WHERE SET_ID_OLD is NULL       
;

--
-- Contrainte not null
--
ALTER TABLE STAGES.STG_EVENEMENT  
MODIFY (
	   SET_ID_OLD NOT NULL,
	   SET_ID_NEW NOT NULL
);



ALTER TABLE STAGES.STG_PIECE_JOINTE_REPART 
ADD (
	SPJR_MODELE VARCHAR2(50 BYTE) 
);

COMMENT ON COLUMN STAGES.STG_PIECE_JOINTE_REPART.SPJR_MODELE IS 'Modèle utilisé pour générer la pièce jointe';

--
-- Langue
--
ALTER TABLE STAGES.STG_STAGE DROP COLUMN STG_LANGUE;
ALTER TABLE STAGES.STG_STAGE ADD ( STG_LANGUE VARCHAR2 (5 BYTE) NULL );

COMMENT ON COLUMN STAGES.STG_STAGE.STG_LANGUE IS 'Langue d''édition de la convention, de la forme en_US' ;

--
-- Référence au contrat de responsabilité civile
--
ALTER TABLE STAGES.STG_STAGE ADD ( STG_REF_RESP_CIVILE VARCHAR2 (60 BYTE) NULL );

COMMENT ON COLUMN STAGES.STG_STAGE.STG_REF_RESP_CIVILE IS 'Référence au contrat de responsabilité civile';


--
-- Numéro de téléphone étudiant
--
ALTER TABLE STAGES.STG_STAGE ADD ( 
	NO_TELEPHONE VARCHAR2 (20 BYTE) NOT NULL,
	TYPE_NO      VARCHAR2 ( 3 BYTE) NOT NULL,
	TYPE_TEL     VARCHAR2 ( 4 BYTE) NOT NULL,
);
COMMENT ON COLUMN STAGES.STG_STAGE.NO_TELEPHONE IS 'Numéro de téléphone de l''étudiant' ;
COMMENT ON COLUMN STAGES.STG_STAGE.TYPE_NO      IS 'Type du numéro de téléphone de l''étudiant' ;
COMMENT ON COLUMN STAGES.STG_STAGE.TYPE_TEL     IS 'Type du téléphone de l''étudiant' ;

--
-- Mise à jour des numéros de téléphone dans les conventions existantes pour pouvoir mettre les champs NOT NULL
--

DECLARE
    CURSOR c1 IS SELECT * FROM STAGES.STG_STAGE WHERE NO_TELEPHONE IS NULL;
    total_num number(6);
BEGIN
    total_num := 0;
    FOR unStage IN c1
    LOOP
      UPDATE STAGES.STG_STAGE stg
         SET (stg.NO_TELEPHONE, TYPE_NO, TYPE_TEL) = (
              SELECT * 
                FROM (
                     SELECT t0.NO_TELEPHONE,
                            t0.TYPE_NO, 
                            t0.TYPE_TEL 
                       FROM GRHUM.PERSONNE_TELEPHONE t0,
                            GRHUM.etudiant e, 
                            GRHUM.individu_ulr i
                       
                      WHERE t0.PERS_ID = i.pers_id 
                        AND i.no_individu = e.no_individu
                        AND e.etud_numero = unStage.num_etudiant
                        AND (t0.D_FIN_VAL IS NULL OR t0.D_FIN_VAL > sysdate)
                      ORDER BY RTRIM(t0.TEL_PRINCIPAL) DESC)
               WHERE ROWNUM = 1)
       WHERE stg.num_etudiant = unStage.num_etudiant;
       total_num := total_num + 1;
      
    END LOOP;
    dbms_output.put_line('Nombre de numéros de téléphone mis à jour : ' || total_num);
END;

/

--
-- Contrainte not null
--
ALTER TABLE STAGES.STG_STAGE  
MODIFY (
	   NO_TELEPHONE NOT NULL,
	   TYPE_NO      NOT NULL,	
	   TYPE_TEL     NOT NULL
);


--
-- AVENANTS
--
ALTER TABLE STAGES.STG_AVENANT ADD ( 
     SAV_GRAT_MONTANT NUMBER(15,2),
     SAV_GRAT_DEVISE VARCHAR2(3 BYTE),
     SAV_DUREE_TOTALE_VAL NUMBER (4) , 
     SAV_DUREE_TOTALE_UNITE VARCHAR2 (1),
     C_STRUCTURE_SERVICE NUMBER (12) ,
     ADR_SERVICE_ACCUEIL NUMBER (12)
);

COMMENT ON COLUMN STAGES.STG_AVENANT.SAV_GRAT_MONTANT IS 'Nouveau montant de la gratification';
COMMENT ON COLUMN STAGES.STG_AVENANT.SAV_GRAT_DEVISE IS 'Devise de la nouvelle gratification';
COMMENT ON COLUMN STAGES.STG_AVENANT.SAV_DUREE_TOTALE_VAL IS 'Nouvelle durée totale du stage';
COMMENT ON COLUMN STAGES.STG_AVENANT.SAV_DUREE_TOTALE_UNITE IS 'Unité de la nouvelle durée totale du stage';
COMMENT ON COLUMN STAGES.STG_AVENANT.C_STRUCTURE_SERVICE IS 'Identifiant du service où se déroule le stage (GRHUM.STRUCTURE_ULR ayant pour père la structure d''accueil)' ;
COMMENT ON COLUMN STAGES.STG_AVENANT.ADR_SERVICE_ACCUEIL IS 'Identifiant de l''adresse du service de la structure d''accueil';


