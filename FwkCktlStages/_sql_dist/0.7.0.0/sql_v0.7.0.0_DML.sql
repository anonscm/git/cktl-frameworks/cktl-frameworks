SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°1/1
-- Type : DML
-- Schéma modifié : STAGES
-- Schéma d'execution du script : STAGES
-- Numéro de version :  0.7.0.0
-- Date de publication : 11/09/2013
-- Licence : CeCILL version 2
-- 
--

whenever sqlerror exit sql.sqlcode ;

INSERT INTO STAGES.STG_CONFIG (SCFG_ID,SCFG_LC,SCFG_VALEUR) VALUES (STAGES.STG_CONFIG_SEQ.NEXTVAL,'seuil_alerte','10');
INSERT INTO STAGES.STG_CONFIG (SCFG_ID,SCFG_LC,SCFG_VALEUR) VALUES (STAGES.STG_CONFIG_SEQ.NEXTVAL,'blocage_mails_alerte','N');
INSERT INTO STAGES.STG_CONFIG (SCFG_ID,SCFG_LC,SCFG_VALEUR) VALUES (STAGES.STG_CONFIG_SEQ.NEXTVAL,'blocage_mails_alerte_DDS','N');

INSERT INTO STAGES.DB_VERSION VALUES(STAGES.DB_VERSION_SEQ.NEXTVAL, '0.7.0.0', TO_DATE('11/09/2013', 'DD/MM/YYYY'),sysdate,'Ajout des parametres pour les alertes mail');

COMMIT;




