SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°1/3
-- Type : DDL
-- Schéma modifié :  STAGES
-- Schéma d'execution du script : STAGES
-- Numéro de version :  0.3.0.0
-- Date de publication : 08/10/2012
-- Licence : CeCILL version 2
--
--

/**
 * Création initiale des tables  
 */

whenever sqlerror exit sql.sqlcode ;

--
-- DB_VERSION
--
INSERT INTO STAGES.DB_VERSION VALUES(STAGES.DB_VERSION_SEQ.NEXTVAL, '0.3.0.0', TO_DATE('08/10/2012', 'DD/MM/YYYY'), NULL, 'Admin');


ALTER TABLE STAGES.STG_TYPE 
ADD (
	STY_EC_OBLIGATOIRE VARCHAR2 (1 BYTE) DEFAULT 'N' NOT NULL
);

COMMENT ON COLUMN STAGES.STG_TYPE.STY_EC_OBLIGATOIRE IS 'Indique si ce type de stage doit être lié à un EC (O : oui, N : non, I : oui ou non)';
