SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°3/3
-- Type : DML
-- Schéma modifié :  STAGES
-- Schéma d'execution du script : STAGES
-- Numéro de version :  0.3.0.0
-- Date de publication : 08/10/2012
-- Licence : CeCILL version 2
--
--

whenever sqlerror exit sql.sqlcode ;

-- Taux du plafond horaire de la Sécurite Sociale pour la gratification obligatoire 
INSERT INTO STAGES.STG_CONFIG (SCFG_ID, SCFG_LC, SCFG_VALEUR) VALUES (STAGES.STG_CONFIG_SEQ.NEXTVAL,'tx_plafond_horaire', '12.5');

-- Plafond horaire de la Sécurité Sociale pour la gratification obligatoire
INSERT INTO STAGES.STG_CONFIG (SCFG_ID, SCFG_LC, SCFG_VALEUR) VALUES (STAGES.STG_CONFIG_SEQ.NEXTVAL,'plafond_horaire', '23');

-- Durée mensuelle de travail, en heures
INSERT INTO STAGES.STG_CONFIG (SCFG_ID, SCFG_LC, SCFG_VALEUR) VALUES (STAGES.STG_CONFIG_SEQ.NEXTVAL,'duree_mensuelle', '151.67');

-- Durée à partir de laquelle un doit a obligation d'être gratifié
INSERT INTO STAGES.STG_CONFIG (SCFG_ID, SCFG_LC, SCFG_VALEUR) VALUES (STAGES.STG_CONFIG_SEQ.NEXTVAL,'duree_min_grat_obligatoire', '2');

-- Mise à jour pour le type Stage obligatoire
UPDATE STAGES.STG_TYPE SET STY_EC_OBLIGATOIRE = 'O' WHERE STY_LC = 'STGO';


-- Mise à jour des années
UPDATE STAGES.STG_ANNEE SET SAN_COURANTE = 'N' WHERE SAN_ID = 2011;
INSERT INTO STAGES.STG_ANNEE (SAN_ID, SAN_DEBUT, SAN_FIN, SAN_COURANTE) VALUES ('2012', '2012', '2013', 'O');

-- Ajout de l'état supprimé
INSERT INTO STAGES.STG_ETAT (SET_ID, SET_ORDRE, SET_LC, SET_LL) VALUES (STAGES.STG_ETAT_SEQ.NEXTVAL, '99', 'SUPPR', 'Convention supprimée');

-- Modification et ajout d'évenements
UPDATE STAGES.STG_EVENEMENT_TYPE SET SEVT_LC = 'SUPPF' WHERE SEVT_LC = 'SUPPR';
INSERT INTO STAGES.STG_EVENEMENT_TYPE (SEVT_ID, SEVT_LC, SEVT_LL) VALUES (STAGES.STG_EVENEMENT_TYPE_SEQ.NEXTVAL, 'SUPPC', 'Suppression totale de la convention');

-- Modification de l'ordre des états
ALTER TABLE STAGES.STG_ETAT disable CONSTRAINT UK_SET_ORDRE;
DROP INDEX STAGES.IDX_UK_SET_ORDRE;
UPDATE STAGES.STG_ETAT SET SET_ORDRE = '2' WHERE SET_LC = 'WTUTE';
UPDATE STAGES.STG_ETAT SET SET_ORDRE = '3' WHERE SET_LC = 'WVALT';
CREATE UNIQUE INDEX STAGES.IDX_UK_SET_ORDRE ON STAGES.STG_ETAT (SET_ORDRE ASC);
ALTER TABLE STAGES.STG_ETAT enable CONSTRAINT UK_SET_ORDRE;

--
-- DB_VERSION
--
UPDATE STAGES.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='0.3.0.0';