SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°5/5 FACULTATIF 
-- Type : DML
-- Schéma modifié : GED
-- Schéma d'execution du script : GRHUM
-- Date de publication : 11/06/2013
-- Licence : CeCILL version 2
--
-- Déclare l'application PISTACHE dans GEDFS et défini deux types de documents 
--

whenever sqlerror exit sql.sqlcode ;

declare
  v_count     NUMBER;

begin


SELECT COUNT(*) INTO v_count FROM GED.APPLICATION WHERE APP_CODE LIKE 'STAGES';
IF (v_count = 0) THEN
  INSERT INTO GED.APPLICATION (APP_CODE, APP_LIBELLE) 
    VALUES ('STAGES','Application PISTACHE (Cocktail)');
END IF;

SELECT COUNT(*) INTO v_count FROM GED.CATEGORIE WHERE CAT_CODE LIKE 'STAGE_CONV';
IF (v_count = 0) THEN
  INSERT INTO GED.CATEGORIE (CAT_CODE, CAT_LIBELLE, CAT_REF_FICHIER, CAT_REF_W3)
    VALUES
    (
      'STAGE_CONV',											-- Identifiant du type de document, à renseigner dans Stages.config : GED_CONVENTIONS = STAGE_CONV
      'Conventions des stages',								-- Description
      '/Actes/htdocs/actes/ged/stages/conventions',			-- A MODIFIER suivant votre configuration : chemin où sont stockés les documents sur le serveur 
      'http://www.univ-xxx.fr/actes/ged/stages/conventions'	-- A MODIFIER suivant votre configuration : URL d'accès au dossier
    );
END IF;

SELECT COUNT(*) INTO v_count FROM GED.CATEGORIE WHERE CAT_CODE LIKE 'STAGE_DOC';
IF (v_count = 0) THEN
  INSERT INTO GED.CATEGORIE (CAT_CODE, CAT_LIBELLE, CAT_REF_FICHIER, CAT_REF_W3)
    VALUES
    (
      'STAGE_DOC',											-- Identifiant du type de document, à renseigner dans Stages.config : GED_DOCUMENTS = STAGE_DOC
      'Documents relatifs aux stages',						-- Description
      '/Actes/htdocs/actes/ged/stages/documents',				-- A MODIFIER suivant votre configuration : chemin où sont stockés les documents sur le serveur 
      'http://www.univ-xxx.fr/actes/ged/stages/documents'	-- A MODIFIER suivant votre configuration : URL d'accès au dossier
    );
END IF;

END;
/