SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°1/5
-- Type : DBA
-- Schéma modifié :  STAGES
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  0.6.0.0
-- Date de publication : 11/06/2013
-- Licence : CeCILL version 2
--
--

/**
 * Script de création de l'utilisateur STAGES
 * Pensez à mettre un "vrai" mot de passe (ALTER USER STAGE IDENTIFIED BY unvraimotdepasse;)
 *
 */

whenever sqlerror exit sql.sqlcode ;

-- 
-- On vérifie que l'utilisateur STAGES n'existe pas déjà  
--

/******
DROP USER STAGES CASCADE;
*******/


DECLARE
  nbschemas NUMBER;  
BEGIN
    SELECT count(*) INTO nbschemas
        FROM dba_users
        WHERE username =  'STAGES'
        ORDER BY username;

    IF (nbschemas > 0) THEN
        RAISE_APPLICATION_ERROR(-20104,'Le schema ''STAGES'' existe deja', TRUE);
    END IF;
END;
/

--
--
--

CREATE USER STAGES IDENTIFIED BY stages
	DEFAULT TABLESPACE SCOL
	TEMPORARY TABLESPACE TEMP
	QUOTA UNLIMITED ON SCOL;

GRANT CREATE session   TO STAGES;
GRANT CREATE table     TO STAGES;
GRANT CREATE synonym   TO STAGES;
GRANT CREATE view      TO STAGES;
GRANT CREATE procedure TO STAGES;
GRANT CREATE trigger   TO STAGES;
GRANT CREATE sequence  TO STAGES;

GRANT UNLIMITED TABLESPACE TO STAGES;
GRANT CONNECT TO STAGES;

--

GRANT REFERENCES ON SCOLARITE.SCOL_FORMATION_ANNEE TO STAGES;
GRANT REFERENCES ON SCOLARITE.SCOL_MAQUETTE_EC TO STAGES;

GRANT SELECT ON JEFY_MISSION.WEB_MON TO STAGES;
GRANT SELECT ON JEFY_MISSION.WEB_MISS TO STAGES;

GRANT SELECT,REFERENCES ON MARACUJA.MODE_PAIEMENT TO STAGES;
GRANT SELECT,REFERENCES ON MARACUJA.EXERCICE TO STAGES;

--
