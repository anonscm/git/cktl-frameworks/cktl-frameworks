SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°4/5
-- Type : DML
-- Schéma modifié : GRHUM
-- Schéma d'execution du script : GRHUM
-- Date de publication : 11/06/2013
-- Licence : CeCILL version 2
--
--

whenever sqlerror exit sql.sqlcode ;

/**
 * Structures et contrats
 */

--
-- Création des groupes "Organisme d'accueil" annulés/à valider/valides
--
declare

    localCStructureEntreprise   	GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE;	-- c_structure du groupe Entreprise
    
    localPersIdStructOrgaAnnule		GRHUM.STRUCTURE_ULR.PERS_ID%TYPE;		-- pers_id du groupe Organismes d'accueil annulés
    localPersIdStructOrgaAValider	GRHUM.STRUCTURE_ULR.PERS_ID%TYPE;		-- pers_id du groupe Organismes d'accueil à valider
    localPersIdStructOrgaValide		GRHUM.STRUCTURE_ULR.PERS_ID%TYPE;		-- pers_id du groupe Organismes d'accueil valides
    
    localCStructOrgaAnnule    		GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE;	-- c_structure du groupe Organismes d'accueil annulés
    localCStructOrgaAValider    	GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE;	-- c_structure du groupe Organismes d'accueil à valider
    localCStructOrgaValide    		GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE;	-- c_structure du groupe Organismes d'accueil valides
    
    paramValue                  	GRHUM.GRHUM_PARAMETRES.PARAM_VALUE%TYPE;
    typeIdCStructure				GRHUM.GRHUM_PARAMETRES_TYPE.TYPE_ID%TYPE;
    noIndividuCreateur          	GRHUM.INDIVIDU_ULR.NO_INDIVIDU%TYPE;
    persIdCreateur          		GRHUM.INDIVIDU_ULR.PERS_ID%TYPE;
    v_count                         NUMBER;
    v_count2                        NUMBER;
    

begin

	/**
	 * Structures
	 */

    -- Groupe Entreprise
    SELECT PARAM_VALUE INTO localCStructureEntreprise FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='ANNUAIRE_ENTREPRISE';

    -- grpOwner et grpResponsable
    SELECT no_individu INTO noIndividuCreateur FROM grhum.individu_ulr WHERE pers_id =
     (SELECT PERS_ID FROM GRHUM.COMPTE WHERE UPPER(CPT_LOGIN) =
        (SELECT UPPER(PARAM_VALUE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE =
                (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR')));
                
 	SELECT pers_id INTO persIdCreateur FROM grhum.individu_ulr WHERE pers_id =
     (SELECT PERS_ID FROM GRHUM.COMPTE WHERE UPPER(CPT_LOGIN) =
        (SELECT UPPER(PARAM_VALUE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_ORDRE =
                (SELECT MIN(PARAM_ORDRE) FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR')));
 
 
  SELECT COUNT(*) INTO v_count FROM GRHUM.structure_ulr WHERE lc_structure LIKE 'ORG_ACC-ANNULES';
  IF (v_count = 0) THEN
    -- Création de la structure Organismes d'accueil annulés
    GRHUM.Ins_Structure_Ulr ( localCStructOrgaAnnule, localPersIdStructOrgaAnnule, 'ORGANISMES D''ACCUEIL ANNULES', 'ORG_ACC-ANNULES', 'A', localCStructureEntreprise, NULL, NULL,
                  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                  NULL, NULL, NULL, NULL, noIndividuCreateur, noIndividuCreateur, NULL, NULL, NULL, NULL, NULL, NULL,
                  NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );
    INSERT INTO GRHUM.REPART_TYPE_GROUPE(C_STRUCTURE, TGRP_CODE, D_CREATION, D_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION)
      VALUES	(localCStructOrgaAnnule,'G',SYSDATE,SYSDATE,persIdCreateur,persIdCreateur);
    DBMS_OUTPUT.PUT_LINE('Creation de ORG_ACC-ANNULES (cStructure:' || localCStructOrgaAnnule || ') ');
  ELSE
    -- ORG_ACC-ANNULES existe, on vérifie que la table REPART_TYPE_GROUPE contient bien le persIdCreateur (bug v0.4.0.0)
    SELECT COUNT(*) INTO v_count2 FROM GRHUM.REPART_TYPE_GROUPE rtg, GRHUM.structure_ulr s
    	WHERE s.LC_STRUCTURE LIKE 'ORG_ACC-ANNULES'
    	AND   s.C_STRUCTURE = rtg.C_STRUCTURE
    	AND   rtg.TGRP_CODE = 'G'
    	AND   rtg.PERS_ID_CREATION = noIndividuCreateur;
    	
    IF (v_count2 = 1) THEN
	    SELECT rtg.C_STRUCTURE INTO localCStructOrgaAnnule FROM GRHUM.REPART_TYPE_GROUPE rtg, GRHUM.structure_ulr s
	    	WHERE s.LC_STRUCTURE LIKE 'ORG_ACC-ANNULES'
	    	AND   s.C_STRUCTURE = rtg.C_STRUCTURE
	    	AND   rtg.TGRP_CODE = 'G'
	    	AND   rtg.PERS_ID_CREATION = noIndividuCreateur;
    	
    	UPDATE GRHUM.REPART_TYPE_GROUPE
    		SET PERS_ID_CREATION = persIdCreateur,
    			PERS_ID_MODIFICATION = persIdCreateur
    		WHERE C_STRUCTURE = localCStructOrgaAnnule
	    	AND TGRP_CODE = 'G'
    		AND PERS_ID_CREATION = noIndividuCreateur;
		DBMS_OUTPUT.PUT_LINE('Mise a jour PERS_ID_CREATION et PERS_ID_MODIFICATION : ' || persIdCreateur || ' pour la cStructure:' || localCStructOrgaAnnule);
    END IF;
  END IF;
  
  SELECT COUNT(*) INTO v_count FROM GRHUM.structure_ulr WHERE lc_structure LIKE 'ORG_ACC-A_VALIDER';
  IF (v_count = 0) THEN
    -- Création de la structure Organismes d'accueil à valider
    GRHUM.Ins_Structure_Ulr ( localCStructOrgaAValider, localPersIdStructOrgaAValider, 'ORGANISMES D''ACCUEIL A VALIDER', 'ORG_ACC-A_VALIDER', 'A', localCStructureEntreprise, NULL, NULL,
                  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                  NULL, NULL, NULL, NULL, noIndividuCreateur, noIndividuCreateur, NULL, NULL, NULL, NULL, NULL, NULL,
                  NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );
    INSERT INTO GRHUM.REPART_TYPE_GROUPE(C_STRUCTURE, TGRP_CODE, D_CREATION, D_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION)
      VALUES	(localCStructOrgaAValider,'G',SYSDATE,SYSDATE,persIdCreateur,persIdCreateur);
    DBMS_OUTPUT.PUT_LINE('Creation de ORG_ACC-A_VALIDER (cStructure:' || localCStructOrgaAValider || ') ');
  ELSE
    -- ORG_ACC-A_VALIDER existe, on vérifie que la table REPART_TYPE_GROUPE contient bien le persIdCreateur (bug v0.4.0.0)
    SELECT COUNT(*) INTO v_count2 FROM GRHUM.REPART_TYPE_GROUPE rtg, GRHUM.structure_ulr s
    	WHERE s.LC_STRUCTURE LIKE 'ORG_ACC-A_VALIDER'
    	AND   s.C_STRUCTURE = rtg.C_STRUCTURE
    	AND   rtg.TGRP_CODE = 'G'
    	AND   rtg.PERS_ID_CREATION = noIndividuCreateur;
    	
    IF (v_count2 = 1) THEN
	    SELECT rtg.C_STRUCTURE INTO localCStructOrgaAValider FROM GRHUM.REPART_TYPE_GROUPE rtg, GRHUM.structure_ulr s
	    	WHERE s.LC_STRUCTURE LIKE 'ORG_ACC-A_VALIDER'
	    	AND   s.C_STRUCTURE = rtg.C_STRUCTURE
	    	AND   rtg.TGRP_CODE = 'G'
	    	AND   rtg.PERS_ID_CREATION = noIndividuCreateur;
    	
    	UPDATE GRHUM.REPART_TYPE_GROUPE
    		SET PERS_ID_CREATION = persIdCreateur,
    			PERS_ID_MODIFICATION = persIdCreateur
    		WHERE C_STRUCTURE = localCStructOrgaAValider
	    	AND TGRP_CODE = 'G'
    		AND PERS_ID_CREATION = noIndividuCreateur;
		DBMS_OUTPUT.PUT_LINE('Mise a jour PERS_ID_CREATION et PERS_ID_MODIFICATION : ' || persIdCreateur || ' pour la cStructure:' || localCStructOrgaAValider);
    END IF;
  END IF;
  
  SELECT COUNT(*) INTO v_count FROM GRHUM.structure_ulr WHERE lc_structure LIKE 'ORG_ACC-VALIDES';
  IF (v_count = 0) THEN
    -- Création de la structure Organismes d'accueil valides
    GRHUM.Ins_Structure_Ulr ( localCStructOrgaValide, localPersIdStructOrgaValide, 'ORGANISMES D''ACCUEIL VALIDES', 'ORG_ACC-VALIDES', 'A', localCStructureEntreprise, NULL, NULL,
                  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                  NULL, NULL, NULL, NULL, noIndividuCreateur, noIndividuCreateur, NULL, NULL, NULL, NULL, NULL, NULL,
                  NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL );
    INSERT INTO GRHUM.REPART_TYPE_GROUPE(C_STRUCTURE, TGRP_CODE, D_CREATION, D_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION)
      VALUES	(localCStructOrgaValide,'G',SYSDATE,SYSDATE,persIdCreateur,persIdCreateur);
    DBMS_OUTPUT.PUT_LINE('Creation de ORG_ACC-VALIDES (cStructure:' || localPersIdStructOrgaValide || ') ');
  ELSE
    -- ORG_ACC-VALIDES existe, on vérifie que la table REPART_TYPE_GROUPE contient bien le persIdCreateur (bug v0.4.0.0)
    SELECT COUNT(*) INTO v_count2 FROM GRHUM.REPART_TYPE_GROUPE rtg, GRHUM.structure_ulr s
    	WHERE s.LC_STRUCTURE LIKE 'ORG_ACC-VALIDES'
    	AND   s.C_STRUCTURE = rtg.C_STRUCTURE
    	AND   rtg.TGRP_CODE = 'G'
    	AND   rtg.PERS_ID_CREATION = noIndividuCreateur;
    	
    IF (v_count2 = 1) THEN
	    SELECT rtg.C_STRUCTURE INTO localCStructOrgaValide FROM GRHUM.REPART_TYPE_GROUPE rtg, GRHUM.structure_ulr s
	    	WHERE s.LC_STRUCTURE LIKE 'ORG_ACC-VALIDES'
	    	AND   s.C_STRUCTURE = rtg.C_STRUCTURE
	    	AND   rtg.TGRP_CODE = 'G'
	    	AND   rtg.PERS_ID_CREATION = noIndividuCreateur;
    	
    	UPDATE GRHUM.REPART_TYPE_GROUPE
    		SET PERS_ID_CREATION = persIdCreateur,
    			PERS_ID_MODIFICATION = persIdCreateur
    		WHERE C_STRUCTURE = localCStructOrgaValide
	    	AND TGRP_CODE = 'G'
    		AND PERS_ID_CREATION = noIndividuCreateur;
		DBMS_OUTPUT.PUT_LINE('Mise a jour PERS_ID_CREATION et PERS_ID_MODIFICATION : ' || persIdCreateur || ' pour la cStructure:' || localCStructOrgaValide);
    END IF;
  END IF;


	/**
	 * CONTRATS
	 */
 
	-- Ajout du type de classification Convention de stage
	SELECT COUNT(*) INTO v_count FROM ACCORDS.TYPE_CLASSIFICATION_CONTRAT WHERE TCC_CODE = 'CONV_STAGE';
	IF (v_count = 0) THEN
		INSERT INTO ACCORDS.TYPE_CLASSIFICATION_CONTRAT (TCC_CODE, TCC_ID, TCC_LIBELLE) 
			VALUES ('CONV_STAGE', (SELECT max(TCC_ID)+1 FROM ACCORDS.TYPE_CLASSIFICATION_CONTRAT), 'Convention de stage');
    DBMS_OUTPUT.PUT_LINE('Ajout du type classification CONV_STAGE dans ACCORDS');
	END IF;
	
	-- Mise à jour des contrats déjà créés
	UPDATE 
	  ACCORDS.CONTRAT ac
	SET 
	  ac.TCC_ID = (SELECT TCC_ID FROM ACCORDS.TYPE_CLASSIFICATION_CONTRAT WHERE TCC_CODE = 'CONV_STAGE')
	WHERE ac.CON_ORDRE IN
	( SELECT con.CON_ORDRE
	  FROM STAGES.STG_STAGE stg, ACCORDS.CONTRAT con
	  WHERE stg.CON_ORDRE = con.CON_ORDRE
	);

	--
	-- DB_VERSION
	--

	end;
/
