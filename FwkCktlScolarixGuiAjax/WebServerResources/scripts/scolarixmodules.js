    function getSelectionId(text, li) {
      var chaine = li.innerText;
      if (typeof(chaine)=='undefined') {
        chaine = li.textContent;
      }

	      switch(text.id) {
	        // Module Paiements
	        case 'CodeModePaiement_Paiements_field' :
	          var modePaiement = chaine.substring(3);
		      $('ModePaiement_Paiements').value = modePaiement;
	          break;
	        case 'CodeBanque_Paiements_field' :
	          var codeBanque = chaine.substring(9);
		      $('Banque_Paiements').value = codeBanque;
	          break;
	        
	      }

        var updateContainer = text.getAttribute("updatecontainerid");
        // alert('Container a updater:'+updateContainer);
        if (typeof(updateContainer)!='undefined' && updateContainer!=null && updateContainer.length > 0) {
          eval(updateContainer+'Update()');
        }
    }

    function autoTab(element, e) {
	    var maxlength = element.getAttribute("maxlength");
	    alert(element.form);
	    if ((maxlength == null || this.value.length == maxlength) && 
	        e.KeyCode != 8 && e.keyCode != 16 && e.keyCode != 9) {
	            new Field.activate(findNextElement(element.form, this.getAttribute("tabIndex")));
	        }
	}
	function findNextElement(formindex) {
        // var form = $('FormIdentite');
        elements = form.getElements();
        // elements = new Form.getElements('shippingInfo');
	    for(i = 0; i < elements.length; i++) {
	        element = elements[i];
	        if(parseInt(element.getAttribute("tabIndex")) == (parseInt(index) + 1)) {
	            return element;
	        }
	        
	    }
	    return elements[0];
	}

    function openWinLecteurCheque(href, id, title, closable, width, height, className, updateContainerId) {
	var win = Windows.getWindow(id);
	if (closable==null) {
	  closable=false;
	}
	if (width==null) {
	  width=600;
	}
	if (height==null) {
	  height=200;
	}
	if (typeof(win)=='undefined') {
		if (className==null) {
	  	className="greenlighting";
	 	}
	 	win = new Window(id,{className: className, title: title, url: href, destroyOnClose:true, recenterAuto:true, resizable:true, closable:closable, minimizable:false, maximizable:false,  minWidth:width, minHeight:height,  showEffectOptions: {duration:0.5}});
		var editorOnClose = { 
			onClose: function(eventName, win) {
	 			/* eval(id+'_containerOnCloseUpdate()'); */ 
	 			if (updateContainerId != null) {
	  			eval('parent.'+updateContainerId+'Update()'); 
	   		}
	  	} ,
			onDestroy: function(eventName, win) {
				Windows.removeObserver(editorOnClose);
			} 
	 	};
	 	Windows.addObserver(editorOnClose);  
	 	win.setDestroyOnClose();    
	   
	  win.setZIndex(99999);
	  win.showCenter(true);
	} else {
	   win.showCenter();
	}
}