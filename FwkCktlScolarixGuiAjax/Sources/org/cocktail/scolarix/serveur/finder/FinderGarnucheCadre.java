/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication;
import org.cocktail.scolarix.serveur.ui.EOGarnucheCadre;
import org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class FinderGarnucheCadre extends Finder {

	public static NSArray<EOGarnucheCadre> cadresForApplication(EOEditingContext ec, String appli) {
		NSArray<EOGarnucheCadre> cadres = null;
		if (ec != null && !StringCtrl.isEmpty(appli)) {
			NSDictionary<String, String> bindings = new NSDictionary<String, String>(appli, EOGarnucheCadre.GARNUCHE_CADRE_APPLICATIONS_KEY + "."
					+ EOGarnucheCadreApplication.GARNUCHE_APPLICATION_KEY + "." + EOGarnucheApplication.APPL_CODE_KEY);
			EOQualifier qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(bindings);
			EOSortOrdering ordering = EOSortOrdering.sortOrderingWithKey(EOGarnucheCadre.GARNUCHE_CADRE_APPLICATIONS_KEY + "."
					+ EOGarnucheCadreApplication.CAPP_POSITION_KEY, EOSortOrdering.CompareAscending);
			cadres = EOGarnucheCadre.fetchAll(ec, qualifier, new NSArray<EOSortOrdering>(ordering));
		}

		return cadres;
	}

}
