/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGarnucheCadreDocumentation.java instead.
package org.cocktail.scolarix.serveur.ui;


import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOGarnucheCadreDocumentation extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "GarnucheCadreDocumentation";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.GARNUCHE_CADRE_DOCUMENTATION";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "cdocKey";

	public static final String CDOC_LIEN_KEY = "cdocLien";
	public static final String CDOC_TEXTE_KEY = "cdocTexte";
	public static final String CDOC_TYPE_KEY = "cdocType";

	// Non visible attributes
	public static final String CAPP_KEY_KEY = "cappKey";
	public static final String CDOC_KEY_KEY = "cdocKey";

	// Colkeys
	public static final String CDOC_LIEN_COLKEY = "CDOC_LIEN";
	public static final String CDOC_TEXTE_COLKEY = "CDOC_TEXTE";
	public static final String CDOC_TYPE_COLKEY = "CDOC_TYPE";

	// Non visible colkeys
	public static final String CAPP_KEY_COLKEY = "CAPP_KEY";
	public static final String CDOC_KEY_COLKEY = "CDOC_KEY";

	// Relationships
	public static final String GARNUCHE_CADRE_APPLICATION_KEY = "garnucheCadreApplication";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOGarnucheCadreDocumentation with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param cdocType
	 * @param garnucheCadreApplication
	 * @return EOGarnucheCadreDocumentation
	 */
	public static EOGarnucheCadreDocumentation create(EOEditingContext editingContext, String cdocType, org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication garnucheCadreApplication) {
		EOGarnucheCadreDocumentation eo = (EOGarnucheCadreDocumentation) createAndInsertInstance(editingContext);
		eo.setCdocType(cdocType);
		eo.setGarnucheCadreApplicationRelationship(garnucheCadreApplication);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOGarnucheCadreDocumentation.
	 *
	 * @param editingContext
	 * @return EOGarnucheCadreDocumentation
	 */
	public static EOGarnucheCadreDocumentation create(EOEditingContext editingContext) {
		EOGarnucheCadreDocumentation eo = (EOGarnucheCadreDocumentation) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOGarnucheCadreDocumentation localInstanceIn(EOEditingContext editingContext) {
		EOGarnucheCadreDocumentation localInstance = (EOGarnucheCadreDocumentation) localInstanceOfObject(editingContext, (EOGarnucheCadreDocumentation) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOGarnucheCadreDocumentation localInstanceIn(EOEditingContext editingContext, EOGarnucheCadreDocumentation eo) {
		EOGarnucheCadreDocumentation localInstance = (eo == null) ? null : (EOGarnucheCadreDocumentation) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String cdocLien() {
		return (String) storedValueForKey("cdocLien");
	}

	public void setCdocLien(String value) {
		takeStoredValueForKey(value, "cdocLien");
	}
	public String cdocTexte() {
		return (String) storedValueForKey("cdocTexte");
	}

	public void setCdocTexte(String value) {
		takeStoredValueForKey(value, "cdocTexte");
	}
	public String cdocType() {
		return (String) storedValueForKey("cdocType");
	}

	public void setCdocType(String value) {
		takeStoredValueForKey(value, "cdocType");
	}

	public org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication garnucheCadreApplication() {
		return (org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication)storedValueForKey("garnucheCadreApplication");
	}

	public void setGarnucheCadreApplicationRelationship(org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication oldValue = garnucheCadreApplication();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "garnucheCadreApplication");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "garnucheCadreApplication");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOGarnucheCadreDocumentation.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOGarnucheCadreDocumentation.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOGarnucheCadreDocumentation)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOGarnucheCadreDocumentation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGarnucheCadreDocumentation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOGarnucheCadreDocumentation eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOGarnucheCadreDocumentation)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOGarnucheCadreDocumentation fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOGarnucheCadreDocumentation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOGarnucheCadreDocumentation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOGarnucheCadreDocumentation eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOGarnucheCadreDocumentation)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOGarnucheCadreDocumentation fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOGarnucheCadreDocumentation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOGarnucheCadreDocumentation eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOGarnucheCadreDocumentation ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	
	public static NSArray fetchFetchAll(EOEditingContext editingContext, NSDictionary bindings) {
		EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", "GarnucheCadreDocumentation");
		fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
		return (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	}


	// Internal utilities methods for common use (server AND client)...

	private static EOGarnucheCadreDocumentation createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOGarnucheCadreDocumentation.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOGarnucheCadreDocumentation.ENTITY_NAME + "' !");
		}
		else {
			EOGarnucheCadreDocumentation object = (EOGarnucheCadreDocumentation) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOGarnucheCadreDocumentation localInstanceOfObject(EOEditingContext ec, EOGarnucheCadreDocumentation object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOGarnucheCadreDocumentation " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOGarnucheCadreDocumentation) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
