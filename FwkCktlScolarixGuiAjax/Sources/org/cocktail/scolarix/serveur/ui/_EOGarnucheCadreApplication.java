/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGarnucheCadreApplication.java instead.
package org.cocktail.scolarix.serveur.ui;


import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOGarnucheCadreApplication extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "GarnucheCadreApplication";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.GARNUCHE_CADRE_APPLICATION";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "cappKey";

	public static final String APPL_KEY_KEY = "applKey";
	public static final String CAPP_MODIFIABLE_KEY = "cappModifiable";
	public static final String CAPP_POSITION_KEY = "cappPosition";
	public static final String CAPP_REGROUPEMENT_KEY = "cappRegroupement";
	public static final String CAPP_TYPE_CADRE_KEY = "cappTypeCadre";

	// Non visible attributes
	public static final String CADR_KEY_KEY = "cadrKey";
	public static final String CAPP_KEY_KEY = "cappKey";

	// Colkeys
	public static final String APPL_KEY_COLKEY = "APPL_KEY";
	public static final String CAPP_MODIFIABLE_COLKEY = "CAPP_MODIFIABLE";
	public static final String CAPP_POSITION_COLKEY = "CAPP_POSITION";
	public static final String CAPP_REGROUPEMENT_COLKEY = "CAPP_REGROUPEMENT";
	public static final String CAPP_TYPE_CADRE_COLKEY = "CAPP_TYPE_CADRE";

	// Non visible colkeys
	public static final String CADR_KEY_COLKEY = "CADR_KEY";
	public static final String CAPP_KEY_COLKEY = "CAPP_KEY";

	// Relationships
	public static final String GARNUCHE_APPLICATION_KEY = "garnucheApplication";
	public static final String GARNUCHE_CADRE_KEY = "garnucheCadre";
	public static final String GARNUCHE_CADRE_DOCUMENTATIONS_KEY = "garnucheCadreDocumentations";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOGarnucheCadreApplication with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param applKey
	 * @param cappModifiable
	 * @param cappPosition
	 * @param cappRegroupement
	 * @param cappTypeCadre
	 * @param garnucheApplication
	 * @param garnucheCadre
	 * @return EOGarnucheCadreApplication
	 */
	public static EOGarnucheCadreApplication create(EOEditingContext editingContext, Integer applKey, String cappModifiable, Integer cappPosition, Integer cappRegroupement, String cappTypeCadre, org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication garnucheApplication, org.cocktail.scolarix.serveur.ui.EOGarnucheCadre garnucheCadre) {
		EOGarnucheCadreApplication eo = (EOGarnucheCadreApplication) createAndInsertInstance(editingContext);
		eo.setApplKey(applKey);
		eo.setCappModifiable(cappModifiable);
		eo.setCappPosition(cappPosition);
		eo.setCappRegroupement(cappRegroupement);
		eo.setCappTypeCadre(cappTypeCadre);
		eo.setGarnucheApplicationRelationship(garnucheApplication);
		eo.setGarnucheCadreRelationship(garnucheCadre);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOGarnucheCadreApplication.
	 *
	 * @param editingContext
	 * @return EOGarnucheCadreApplication
	 */
	public static EOGarnucheCadreApplication create(EOEditingContext editingContext) {
		EOGarnucheCadreApplication eo = (EOGarnucheCadreApplication) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOGarnucheCadreApplication localInstanceIn(EOEditingContext editingContext) {
		EOGarnucheCadreApplication localInstance = (EOGarnucheCadreApplication) localInstanceOfObject(editingContext, (EOGarnucheCadreApplication) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOGarnucheCadreApplication localInstanceIn(EOEditingContext editingContext, EOGarnucheCadreApplication eo) {
		EOGarnucheCadreApplication localInstance = (eo == null) ? null : (EOGarnucheCadreApplication) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer applKey() {
		return (Integer) storedValueForKey("applKey");
	}

	public void setApplKey(Integer value) {
		takeStoredValueForKey(value, "applKey");
	}
	public String cappModifiable() {
		return (String) storedValueForKey("cappModifiable");
	}

	public void setCappModifiable(String value) {
		takeStoredValueForKey(value, "cappModifiable");
	}
	public Integer cappPosition() {
		return (Integer) storedValueForKey("cappPosition");
	}

	public void setCappPosition(Integer value) {
		takeStoredValueForKey(value, "cappPosition");
	}
	public Integer cappRegroupement() {
		return (Integer) storedValueForKey("cappRegroupement");
	}

	public void setCappRegroupement(Integer value) {
		takeStoredValueForKey(value, "cappRegroupement");
	}
	public String cappTypeCadre() {
		return (String) storedValueForKey("cappTypeCadre");
	}

	public void setCappTypeCadre(String value) {
		takeStoredValueForKey(value, "cappTypeCadre");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication garnucheApplication() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication)storedValueForKey("garnucheApplication");
	}

	public void setGarnucheApplicationRelationship(org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication oldValue = garnucheApplication();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "garnucheApplication");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "garnucheApplication");
		}
	}
  
	public org.cocktail.scolarix.serveur.ui.EOGarnucheCadre garnucheCadre() {
		return (org.cocktail.scolarix.serveur.ui.EOGarnucheCadre)storedValueForKey("garnucheCadre");
	}

	public void setGarnucheCadreRelationship(org.cocktail.scolarix.serveur.ui.EOGarnucheCadre value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.ui.EOGarnucheCadre oldValue = garnucheCadre();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "garnucheCadre");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "garnucheCadre");
		}
	}
  
	public NSArray garnucheCadreDocumentations() {
		return (NSArray)storedValueForKey("garnucheCadreDocumentations");
	}

	public NSArray garnucheCadreDocumentations(EOQualifier qualifier) {
		return garnucheCadreDocumentations(qualifier, null, false);
	}

	public NSArray garnucheCadreDocumentations(EOQualifier qualifier, boolean fetch) {
		return garnucheCadreDocumentations(qualifier, null, fetch);
	}

	public NSArray garnucheCadreDocumentations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolarix.serveur.ui.EOGarnucheCadreDocumentation.GARNUCHE_CADRE_APPLICATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolarix.serveur.ui.EOGarnucheCadreDocumentation.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = garnucheCadreDocumentations();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToGarnucheCadreDocumentationsRelationship(org.cocktail.scolarix.serveur.ui.EOGarnucheCadreDocumentation object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "garnucheCadreDocumentations");
	}

	public void removeFromGarnucheCadreDocumentationsRelationship(org.cocktail.scolarix.serveur.ui.EOGarnucheCadreDocumentation object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "garnucheCadreDocumentations");
	}

	public org.cocktail.scolarix.serveur.ui.EOGarnucheCadreDocumentation createGarnucheCadreDocumentationsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GarnucheCadreDocumentation");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "garnucheCadreDocumentations");
		return (org.cocktail.scolarix.serveur.ui.EOGarnucheCadreDocumentation) eo;
	}

	public void deleteGarnucheCadreDocumentationsRelationship(org.cocktail.scolarix.serveur.ui.EOGarnucheCadreDocumentation object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "garnucheCadreDocumentations");
				editingContext().deleteObject(object);
			}

	public void deleteAllGarnucheCadreDocumentationsRelationships() {
		Enumeration objects = garnucheCadreDocumentations().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteGarnucheCadreDocumentationsRelationship((org.cocktail.scolarix.serveur.ui.EOGarnucheCadreDocumentation)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOGarnucheCadreApplication.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOGarnucheCadreApplication.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOGarnucheCadreApplication)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOGarnucheCadreApplication fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGarnucheCadreApplication fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOGarnucheCadreApplication eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOGarnucheCadreApplication)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOGarnucheCadreApplication fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOGarnucheCadreApplication fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOGarnucheCadreApplication fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOGarnucheCadreApplication eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOGarnucheCadreApplication)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOGarnucheCadreApplication fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOGarnucheCadreApplication fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOGarnucheCadreApplication eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOGarnucheCadreApplication ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	
	public static NSArray fetchFetchAll(EOEditingContext editingContext, NSDictionary bindings) {
		EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", "GarnucheCadreApplication");
		fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
		return (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	}


	// Internal utilities methods for common use (server AND client)...

	private static EOGarnucheCadreApplication createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOGarnucheCadreApplication.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOGarnucheCadreApplication.ENTITY_NAME + "' !");
		}
		else {
			EOGarnucheCadreApplication object = (EOGarnucheCadreApplication) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOGarnucheCadreApplication localInstanceOfObject(EOEditingContext ec, EOGarnucheCadreApplication object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOGarnucheCadreApplication " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOGarnucheCadreApplication) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
