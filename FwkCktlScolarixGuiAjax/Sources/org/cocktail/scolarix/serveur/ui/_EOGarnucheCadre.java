/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGarnucheCadre.java instead.
package org.cocktail.scolarix.serveur.ui;


import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOGarnucheCadre extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "GarnucheCadre";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.GARNUCHE_CADRE";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "cadrKey";

	public static final String CADR_ABREVIATION_KEY = "cadrAbreviation";
	public static final String CADR_CODE_KEY = "cadrCode";
	public static final String CADR_COMMENTAIRE_KEY = "cadrCommentaire";
	public static final String CADR_NOM_KEY = "cadrNom";

	// Non visible attributes
	public static final String CADR_KEY_KEY = "cadrKey";

	// Colkeys
	public static final String CADR_ABREVIATION_COLKEY = "CADR_ABREVIATION";
	public static final String CADR_CODE_COLKEY = "CADR_CODE";
	public static final String CADR_COMMENTAIRE_COLKEY = "CADR_COMMENTAIRE";
	public static final String CADR_NOM_COLKEY = "CADR_NOM";

	// Non visible colkeys
	public static final String CADR_KEY_COLKEY = "CADR_KEY";

	// Relationships
	public static final String GARNUCHE_CADRE_APPLICATIONS_KEY = "garnucheCadreApplications";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOGarnucheCadre with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @return EOGarnucheCadre
	 */
	public static EOGarnucheCadre create(EOEditingContext editingContext) {
		EOGarnucheCadre eo = (EOGarnucheCadre) createAndInsertInstance(editingContext);
		return eo;
	}


	// Utilities methods

	public EOGarnucheCadre localInstanceIn(EOEditingContext editingContext) {
		EOGarnucheCadre localInstance = (EOGarnucheCadre) localInstanceOfObject(editingContext, (EOGarnucheCadre) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOGarnucheCadre localInstanceIn(EOEditingContext editingContext, EOGarnucheCadre eo) {
		EOGarnucheCadre localInstance = (eo == null) ? null : (EOGarnucheCadre) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String cadrAbreviation() {
		return (String) storedValueForKey("cadrAbreviation");
	}

	public void setCadrAbreviation(String value) {
		takeStoredValueForKey(value, "cadrAbreviation");
	}
	public String cadrCode() {
		return (String) storedValueForKey("cadrCode");
	}

	public void setCadrCode(String value) {
		takeStoredValueForKey(value, "cadrCode");
	}
	public String cadrCommentaire() {
		return (String) storedValueForKey("cadrCommentaire");
	}

	public void setCadrCommentaire(String value) {
		takeStoredValueForKey(value, "cadrCommentaire");
	}
	public String cadrNom() {
		return (String) storedValueForKey("cadrNom");
	}

	public void setCadrNom(String value) {
		takeStoredValueForKey(value, "cadrNom");
	}

	public NSArray garnucheCadreApplications() {
		return (NSArray)storedValueForKey("garnucheCadreApplications");
	}

	public NSArray garnucheCadreApplications(EOQualifier qualifier) {
		return garnucheCadreApplications(qualifier, null, false);
	}

	public NSArray garnucheCadreApplications(EOQualifier qualifier, boolean fetch) {
		return garnucheCadreApplications(qualifier, null, fetch);
	}

	public NSArray garnucheCadreApplications(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication.GARNUCHE_CADRE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = garnucheCadreApplications();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToGarnucheCadreApplicationsRelationship(org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "garnucheCadreApplications");
	}

	public void removeFromGarnucheCadreApplicationsRelationship(org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "garnucheCadreApplications");
	}

	public org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication createGarnucheCadreApplicationsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GarnucheCadreApplication");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "garnucheCadreApplications");
		return (org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication) eo;
	}

	public void deleteGarnucheCadreApplicationsRelationship(org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "garnucheCadreApplications");
				editingContext().deleteObject(object);
			}

	public void deleteAllGarnucheCadreApplicationsRelationships() {
		Enumeration objects = garnucheCadreApplications().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteGarnucheCadreApplicationsRelationship((org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOGarnucheCadre.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOGarnucheCadre.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOGarnucheCadre)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOGarnucheCadre fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGarnucheCadre fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOGarnucheCadre eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOGarnucheCadre)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOGarnucheCadre fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOGarnucheCadre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOGarnucheCadre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOGarnucheCadre eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOGarnucheCadre)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOGarnucheCadre fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOGarnucheCadre fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOGarnucheCadre eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOGarnucheCadre ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	
	public static NSArray fetchFetchAll(EOEditingContext editingContext, NSDictionary bindings) {
		EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", "GarnucheCadre");
		fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
		return (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	}


	// Internal utilities methods for common use (server AND client)...

	private static EOGarnucheCadre createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOGarnucheCadre.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOGarnucheCadre.ENTITY_NAME + "' !");
		}
		else {
			EOGarnucheCadre object = (EOGarnucheCadre) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOGarnucheCadre localInstanceOfObject(EOEditingContext ec, EOGarnucheCadre object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOGarnucheCadre " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOGarnucheCadre) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
