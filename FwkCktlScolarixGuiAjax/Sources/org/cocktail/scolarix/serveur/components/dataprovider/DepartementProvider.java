package org.cocktail.scolarix.serveur.components.dataprovider;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.scolarix.serveur.finder.FinderDepartement;

import com.webobjects.eocontrol.EOEditingContext;

public abstract class DepartementProvider implements CktlAjaxSelect2RemoteDataProvider {

    private EOEditingContext edc;
    
    public DepartementProvider(EOEditingContext edc) {
        this.edc = edc;
    }

    public List<Result> results(String searchTerm) {
        List<Result> resultat = new ArrayList<Result>();
        if (searchTerm != null && searchTerm.length() > 1) {
            for (Object obj : FinderDepartement.getDepartementsFiltre(edc, searchTerm)) {
                EODepartement departement = (EODepartement)obj;
                resultat.add(new Result(departement.cDepartement(), departement.getLibelleAndCode()));
            }
        }
        return resultat;
    }

    public Result selectionInitiale() {
        EODepartement selectedDep = getSelectedDepartement();
        Result result = null;
        if (selectedDep != null) {
            result = new Result(selectedDep.cDepartement(), selectedDep.getLibelleAndCode());
        }
        return result;
    }

    public Result onSelect(String idSelection) {
        EODepartement selectedDep = EODepartement.fetchRequiredByKeyValue(edc, EODepartement.C_DEPARTEMENT_KEY, idSelection);
        setSelectedDepartement(selectedDep);
        return new Result(selectedDep.cDepartement(), selectedDep.getLibelleAndCode());
    }

    public abstract EODepartement getSelectedDepartement();

    public abstract void setSelectedDepartement(EODepartement departement);
    
}
