package org.cocktail.scolarix.serveur.components.dataprovider;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.scolarix.serveur.finder.FinderRne;
import org.cocktail.scolarix.serveur.metier.eos.EORne;

import com.webobjects.eocontrol.EOEditingContext;

public abstract class EtablissementProvider implements CktlAjaxSelect2RemoteDataProvider {

    private EOEditingContext edc;
    
    public EtablissementProvider(EOEditingContext edc) {
        this.edc = edc;
    }

    public List<Result> results(String searchTerm) {
        List<Result> resultat = new ArrayList<Result>();
        if (searchTerm != null && searchTerm.length() > 1) {
            for (Object obj : FinderRne.getRnesFiltre(edc, searchTerm, null, null, null)) {
                EORne rne = (EORne) obj;
                resultat.add(new Result(rne.cRne(), rne.libelleLong()));
            }
        }
        return resultat;
    }

    public Result selectionInitiale() {
        EORne selectedRne = getSelectedRne();
        Result result = null;
        if (selectedRne != null) {
            result = new Result(selectedRne.cRne(), selectedRne.libelleLong());
        }
        return result;
    }

    public Result onSelect(String idSelection) {
        EORne selectedRne = EORne.fetchFirstRequiredByKeyValue(edc, EORne.C_RNE_KEY, idSelection);
        setSelectedRne(selectedRne);
        return new Result(selectedRne.cRne(), selectedRne.libelleLong());
    }

    public abstract EORne getSelectedRne();
    
    public abstract void setSelectedRne(EORne selectedRne);
    
}