package org.cocktail.scolarix.serveur.components.dataprovider;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.scolarix.serveur.finder.FinderBanque;
import org.cocktail.scolarix.serveur.metier.eos.EOBanque;

import com.webobjects.eocontrol.EOEditingContext;

public abstract class BanqueProvider implements CktlAjaxSelect2RemoteDataProvider {

    private EOEditingContext edc;
    
    public BanqueProvider(EOEditingContext edc) {
        this.edc = edc;
    }

    public List<Result> results(String searchTerm) {
        List<Result> resultat = new ArrayList<Result>();
        if (searchTerm != null && searchTerm.length() > 1) {
            for (Object obj : FinderBanque.getBanquesFiltre(edc, searchTerm)) {
                EOBanque banque = (EOBanque) obj;
                resultat.add(new Result(banque.banqCode(), banque.codeEtLibelle()));
            }
        }
        return resultat;
    }

    public Result selectionInitiale() {
        EOBanque selectedBanque = getSelectedBanque();
        Result result = null;
        if (selectedBanque != null) {
            result = new Result(selectedBanque.banqCode(), selectedBanque.codeEtLibelle());
        }
        return result;
    }

    public Result onSelect(String idSelection) {
        EOBanque selectedBanque = EOBanque.fetchFirstRequiredByKeyValue(edc, EOBanque.BANQ_CODE_KEY, idSelection);
        setSelectedBanque(selectedBanque);
        return new Result(selectedBanque.banqCode(), selectedBanque.codeEtLibelle());
    }

    public abstract EOBanque getSelectedBanque();
    
    public abstract void setSelectedBanque(EOBanque selectedBanqueFromRemote);
    
}