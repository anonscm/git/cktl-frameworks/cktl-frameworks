/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.List;

import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.lecteurcheque.serveur.LecteurChequeClient;
import org.cocktail.scolarix.serveur.components.dataprovider.BanqueProvider;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.impression.PrintFactory;
import org.cocktail.scolarix.serveur.metier.eos.EOBanque;
import org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement;
import org.cocktail.scolarix.serveur.metier.eos.EODroitsUniversite;
import org.cocktail.scolarix.serveur.metier.eos.EOPaiement;
import org.cocktail.scolarix.serveur.metier.eos.EOPlanco;
import org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte;
import org.cocktail.scolarix.serveur.process.ProcessDelPaiement;
import org.cocktail.scolarix.serveur.process.ProcessInsPaiement;
import org.cocktail.scolarix.serveur.process.ProcessUpdPaiement;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.qualifiers.ERXKeyValueQualifier;

public class Paiements_Cadre_J extends ModuleDossierAdministratif {

    protected static final String CHEQUE_SERVER_HOSTNAME_CONFIG_KEY = "org.cocktail.scolarix.cheque.server.hostname";
	protected static final String CHEQUE_SERVER_PORT_CONFIG_KEY = "org.cocktail.scolarix.cheque.server.port";
	protected static final Integer DEFAULT_PORT = new Integer(2233);

	protected boolean isBusy = false;

	private EOPaiement unPaiement;
	private EOPaiement lePaiementEnCours;

	private EODetailPaiement unDetailPaiement;

	private boolean isPaiementAjoutEnCours = false;
	private boolean isPaiementModifEnCours = false;

	private boolean isDetailAvecRemboursements = false;

	private List<EORepartitionCompte> modesPaiementPopup = null;

	protected EOBanque banque;

	private EOBanque uneBanque;
	private String codeBanque;

	protected WODisplayGroup dgPaiements = null, dgDroits = null;
	private EODroitsUniversite unDroit;

	// lecteur cheque
	private boolean isLireChequeEnCours = false, isLecteurReady = false;
	private LireChequeThread lireChequeThread;
	private String lecteurChequeContainerId = null;

	private BanqueProvider banqueProvider;
	
	public Paiements_Cadre_J(WOContext context) {
		super(context);
		banqueProvider = new BanqueProvider(edc()) {
		    @Override
		    public void setSelectedBanque(EOBanque selectedBanqueFromRemote) {
		        setBanque(selectedBanqueFromRemote);
		    }

            @Override
            public EOBanque getSelectedBanque() {
                return lePaiementEnCours().toBanque();
            }
		};
	}

	public BanqueProvider getBanqueProvider() {
        return banqueProvider;
    }
	
	public void reset() {
		if (historique() != null && historique().editingContext() != null) {
			dgPaiementsUpdateData();
			dgDroitsUpdateData();
		}
	}

	public WOActionResults printEcheancier() {
		try {
			if (dgPaiements().selectedObject() != null) {
				String cRne = etudiant().rne().cRne();
				Integer paieNumero = ((EOPaiement) dgPaiements().selectedObject()).paieNumero();
				NSData data = PrintFactory.printEcheancier(mySession(), cRne, paieNumero, anneeScolaire());
				String fileName = "dossier_echeancier_" + anneeScolaire().intValue() + "_" + cRne + ".pdf";
				if (data == null) {
					throw new Exception("Impossible d'imprimer l'échéancier de prélèvements " + fileName);
				}
				return PrintFactory.afficherPdf(data, fileName);
			}
			return null;
		}
		catch (Throwable e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	public WODisplayGroup dgPaiements() {
		if (dgPaiements == null) {
			dgPaiements = new WODisplayGroup();
			dgPaiements.setSelectsFirstObjectAfterFetch(true);
			dgPaiements.setSortOrderings(ERXS.ascs(EOPaiement.PAIE_NUMERO_KEY));
			dgPaiementsUpdateData();
		}
		return dgPaiements;
	}

	public void dgPaiementsUpdateData() {
		dgPaiements().setObjectArray(historique().toPaiements());
		setLePaiementEnCours((EOPaiement) dgPaiements().selectedObject());
	}

	public WODisplayGroup dgDroits() {
		if (dgDroits == null) {
			dgDroits = new WODisplayGroup();
			dgDroits.setSelectsFirstObjectAfterFetch(false);
			dgDroits.setSortOrderings(ERXS.ascs(EODroitsUniversite.TO_REPARTITION_COMPTE_KEY + "." + EORepartitionCompte.TO_PLANCO_KEY + "."
					+ EOPlanco.PCO_LIB_KEY));
			dgDroitsUpdateData();
		}
		return dgDroits;
	}

	public void dgDroitsUpdateData() {
		dgDroits().setObjectArray(historique().droitsUniversitairesComplets());
	}

	public void onSelectPaiement() {
		setLePaiementEnCours((EOPaiement) dgPaiements().selectedObject());
	}

	public boolean enabled() {
		Boolean disabled = super.disabled();
		boolean enabled = (disabled == null ? false : disabled.booleanValue());
		boolean enabledInscription = (etudiant().isInscription() || etudiant().isReInscription()) && etudiant().numero() != null;
		boolean enabledAdmission = etudiant().isAdmission();
		return enabled && (etudiant() != null && (enabledInscription || enabledAdmission));
	}

	public Boolean disabled() {
		return !enabled() || utilisateur().isModifDisabled();
	}

	public Boolean disabledEcheancier() {
		if (disabled()) {
			return true;
		}
		return dgPaiements().selectedObject() == null || ((EOPaiement) dgPaiements().selectedObject()).toRepartitionCompte() == null
				|| !((EOPaiement) dgPaiements().selectedObject()).toRepartitionCompte().rcptCode().equals("ECH");
	}

	public boolean isFormDisabled() {
		return disabled() || isNotAjoutOuModifEnCours();
	}

	public boolean isAjoutOuModifEnCours() {
		return isPaiementAjoutEnCours() || isPaiementModifEnCours();
	}

	public boolean isNotAjoutOuModifEnCours() {
		return !isAjoutOuModifEnCours();
	}

	public boolean isPaiementAjoutEnCoursPhase1() {
		return isPaiementAjoutEnCours() && lePaiementEnCours() == null;
	}

	public boolean isPaiementAjoutEnCoursPhase2() {
		return isPaiementAjoutEnCours() && lePaiementEnCours() != null;
	}

	public void validate() throws EtudiantException {
		if (isPaiementAjoutEnCours() == true) {
			throw new EtudiantException("Ajout de paiement en cours, annulez ou validez.");
		}
		else {
			if (isPaiementModifEnCours() == true) {
				throw new EtudiantException("Modification de paiement en cours, annulez ou validez.");
			}
		}
	}

	public String styleForDetailPaiement() {
		if (unDetailPaiement().toPaiement() == null) {
			return "color:#FF0000";
		}
		else {
			if (!unDetailPaiement().toPaiement().equals(dgPaiements().selectedObject())) {
				return "color:lightSlateGrey;";
			}
		}
		return "";
	}

	public List<EORepartitionCompte> modesPaiementPopup() {
		if (modesPaiementPopup == null) {
			ERXKeyValueQualifier typeRepartitionQ = ERXQ.equals(EORepartitionCompte.RCPT_TYPE_REPARTITION_KEY, "REG");
			modesPaiementPopup = EORepartitionCompte.fetchAll(edc(), typeRepartitionQ,
					ERXS.ascInsensitives(EORepartitionCompte.TO_PLANCO_KEY + "." + EOPlanco.PCO_LIB_KEY));
		}
		return modesPaiementPopup;
	}

	public EOBanque banque() {
		return banque;
	}

	public void setBanque(EOBanque banque) {
		this.banque = banque;
		lePaiementEnCours().setToBanqueRelationship(banque);
		if (banque != null) {
			setCodeBanque(banque.banqCode());
		}
	}

	// Paiements .............................

	public void setLePaiementEnCours(EOPaiement lePaiementEnCours) {
		this.lePaiementEnCours = lePaiementEnCours;
		setCodeBanque(null);
		if (lePaiementEnCours != null) {
			if (lePaiementEnCours.toBanque() != null) {
				setCodeBanque(lePaiementEnCours.toBanque().banqCode());
			}
		}
	}

	// Ajout de paiement............................

	public WOComponent ajouterUnPaiement() {
		dgDroits().setSelectedObjects(null);
		if (dgDroits().displayedObjects() != null) {
			dgDroits().displayedObjects().takeValueForKey(null, EODroitsUniversite.DUNIV_MONTANT_LOCAL_KEY);
		}
		// pré-coche les droits à régler normalement...
		NSArray<EODroitsUniversite> droits = historique().droitsUniversitairesSpecifiques();
		if (droits != null) {
			dgDroits().selectObjectsIdenticalTo(droits);
		}
		setLePaiementEnCours(null);
		setIsPaiementAjoutEnCours(true);
		return null;
	}

	public WOActionResults annulerNouveauPaiement() {
		setIsPaiementAjoutEnCours(false);
		return null;
	}

	public WOComponent ajouterNouveauPaiement() {
		if (dgDroits().selectedObjects().count() == 0) {
			return null;
		}
		setLePaiementEnCours(EOPaiement.create(edc(), historique(), dgDroits().selectedObjects()));
		return null;
	}

	public WOActionResults ajouterLePaiementValide() {
		try {
			ProcessInsPaiement.enregistrer(cktlSession().dataBus(), edc(), lePaiementEnCours());
		}
		catch (ScolarixFwkException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
			return null;
		}
		setIsPaiementAjoutEnCours(false);
		dgPaiementsUpdateData();
		historique().resetDroitsUniversitairesSpecifiques();
		return null;
	}

	public WOActionResults ajouterLePaiementAnnule() {
		edc().revert();
		setIsPaiementAjoutEnCours(false);
		dgPaiementsUpdateData();
		return null;
	}

	// Modification de paiement............................

	public WOComponent modifierLePaiement() {
		if (dgPaiements().selectedObject() != null) {
			setLePaiementEnCours((EOPaiement) dgPaiements().selectedObject());
			setIsPaiementModifEnCours(true);
		}
		return null;
	}

	public WOActionResults modifierLePaiementValide() {
		try {
			ProcessUpdPaiement.enregistrer(cktlSession().dataBus(), edc(), lePaiementEnCours());
		}
		catch (ScolarixFwkException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
			return null;
		}
		dgPaiementsUpdateData();
		setIsPaiementModifEnCours(false);
		return null;
	}

	public WOActionResults modifierLePaiementAnnule() {
		edc().revert();
		setIsPaiementModifEnCours(false);
		return null;
	}

	// Suppression de paiement............................

	public WOActionResults supprimerLePaiement() {
		try {
			if (dgPaiements().selectedObject() != null) {
				try {
					ProcessDelPaiement.enregistrer(cktlSession().dataBus(), edc(), (EOPaiement) dgPaiements().selectedObject());
				}
				catch (ScolarixFwkException e) {
					mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
					return null;
				}
				dgPaiementsUpdateData();
				historique().resetDroitsUniversitairesSpecifiques();
			}
		}
		catch (EtudiantException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
			return null;
		}
		return null;
	}

	// Detail paiements .............................

	public NSArray<EODetailPaiement> detailPaiements() {
		return historique().detailPaiements(isDetailAvecRemboursements());
	}

	// Default getters / setters .................................

	public EOPaiement unPaiement() {
		return unPaiement;
	}

	public void setUnPaiement(EOPaiement unPaiement) {
		this.unPaiement = unPaiement;
	}

	public EODetailPaiement unDetailPaiement() {
		return unDetailPaiement;
	}

	public void setUnDetailPaiement(EODetailPaiement unDetailPaiement) {
		this.unDetailPaiement = unDetailPaiement;
	}

	public boolean isPaiementAjoutEnCours() {
		return isPaiementAjoutEnCours;
	}

	public void setIsPaiementAjoutEnCours(boolean isPaiementAjoutEnCours) {
		this.isPaiementAjoutEnCours = isPaiementAjoutEnCours;
		setBusy(isPaiementAjoutEnCours);
	}

	public boolean isPaiementModifEnCours() {
		return isPaiementModifEnCours;
	}

	public void setIsPaiementModifEnCours(boolean isPaiementModifEnCours) {
		this.isPaiementModifEnCours = isPaiementModifEnCours;
		setBusy(isPaiementModifEnCours);
	}

	public EOBanque uneBanque() {
		return uneBanque;
	}

	public void setUneBanque(EOBanque uneBanque) {
		this.uneBanque = uneBanque;
	}

	public String codeBanque() {
		return codeBanque;
	}

	public void setCodeBanque(String codeBanque) {
		this.codeBanque = codeBanque;
	}

	public EOPaiement lePaiementEnCours() {
		return lePaiementEnCours;
	}

	public boolean isDetailAvecRemboursements() {
		return isDetailAvecRemboursements;
	}

	public void setIsDetailAvecRemboursements(boolean isDetailAvecRemboursements) {
		this.isDetailAvecRemboursements = isDetailAvecRemboursements;
	}

	public EODroitsUniversite unDroit() {
		return unDroit;
	}

	public void setUnDroit(EODroitsUniversite unDroit) {
		this.unDroit = unDroit;
	}

	// lecteur de cheque...

	public String getLecteurChequeContainerId() {
		if (lecteurChequeContainerId == null) {
			lecteurChequeContainerId = getComponentId() + "_lecteurCheque";
		}
		return lecteurChequeContainerId;
	}

	protected void setCheque(EOEditingContext ec, Hashtable<String, String> cheque) {
		if (lePaiementEnCours() != null && cheque != null) {
			lePaiementEnCours().setToRepartitionCompteRelationship(
					EORepartitionCompte.fetchByKeyValue(ec, EORepartitionCompte.RCPT_CODE_KEY, cheque.get("typeCheque")));
			setCodeBanque(cheque.get("codeBanque"));
			lePaiementEnCours().setToBanqueRelationship(EOBanque.fetchByKeyValue(ec, EOBanque.BANQ_CODE_KEY, codeBanque()));
			lePaiementEnCours().setPaieNumeroCheque(cheque.get("numCheque"));
			lePaiementEnCours().setPaieNumeroCompte(cheque.get("numCompte"));
		}
	}

	public boolean stopped() {
		return !isLireChequeEnCours();
	}

	public String onRefreshComplete() {
		String s = "";
		if (!isLireChequeEnCours()) {
			s = "if ($('" + getMainContainerId() + "') != null ) {" + getMainContainerId() + "Stop();}";
		}
		if (erreurLecture != null) {
			s += "alert('" + erreurLecture + "');";
			erreurLecture = null;
		}
		return s.length() == 0 ? null : s;
	}

	private String erreurLecture = null;

	public class LireChequeThread extends Thread {
		LecteurChequeClient lec = null;
		CocktailAjaxSession session;
		EOEditingContext editingContext;

		public LireChequeThread(CocktailAjaxSession session, EOEditingContext editingContext, LecteurChequeClient lecteurChequeClient) {
			this.session = session;
			this.editingContext = editingContext;
			this.lec = lecteurChequeClient;
		}

		public void run() {
			try {
				Hashtable<String, String> h = lec.lire();
				if (h != null) {
					setCheque(editingContext, h);
					lec.close();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				erreurLecture = "Problème : " + e.getMessage();
			}
			finally {
				try {
					lec.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				setIsLireChequeEnCours(false);
			}
		}

		public void interrupt() {
			try {
				if (lec != null) {
					lec.annuler();
					lec.close();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				erreurLecture = "Problème : " + e.getMessage();
			}
			super.interrupt();
		}
	}

	public WOActionResults lireCheque() {
		LecteurChequeClient lec = getNewLecteurChequeClient();
		if (lec != null) {
			lireChequeThread = new LireChequeThread(mySession(), edc(), lec);
			lireChequeThread.setPriority(Thread.currentThread().getPriority() - 1);
			lireChequeThread.start();
			setIsLireChequeEnCours(true);
		}
		return null;
	}

	public WOActionResults annulerLireCheque() {
		try {
			if (lireChequeThread != null && lireChequeThread.isAlive()) {
				lireChequeThread.interrupt();
				lireChequeThread.join();
			}
			mySession().addSimpleInfoMessage("ANNULATION", "Lecture du chèque annulée...");
		}
		catch (Exception e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Problème", e.getMessage());
		}
		return null;
	}

	public class ListenerLecteurCheque implements LecteurChequeClient.ILecteurChequeClientListener {
		public void onChequeAnalyse() {
		}

		public void onChequeLu() {
			setLecteurReady(false);
		}

		public void onLecteurReady() {
			setLecteurReady(true);
		}

		public void onLecteurNotReady() {
			setLecteurReady(false);
		}

		public void onLecteurCanceled() {
		}
	}

	private LecteurChequeClient getNewLecteurChequeClient() {
		try {
			return new LecteurChequeClient(new ListenerLecteurCheque(), lecteurChequeServerHostName(), lecteurChequeServerPort().intValue());
		}
		catch (UnknownHostException e1) {
			e1.printStackTrace();
			mySession().addSimpleErrorMessage("Problème", "Host " + lecteurChequeServerHostName() + " inconnu");
		}
		catch (IOException e2) {
			e2.printStackTrace();
			mySession().addSimpleErrorMessage("Problème",
					"Impossible de se connecter sur " + lecteurChequeServerHostName() + ":" + lecteurChequeServerPort());
		}
		catch (Exception e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Problème", e.getMessage());
		}
		return null;
	}

	public String lecteurChequeServerHostName() {
		String lecteurChequeServerHostName = null;
		try {
			lecteurChequeServerHostName = utilisateur().utiChequeServerHostname();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		if (lecteurChequeServerHostName == null) {
			lecteurChequeServerHostName = myApp().config().stringForKey(CHEQUE_SERVER_HOSTNAME_CONFIG_KEY);
		}
		return lecteurChequeServerHostName;
	}

	public Integer lecteurChequeServerPort() {
		Integer lecteurChequeServerPort = null;
		try {
			lecteurChequeServerPort = utilisateur().utiChequeServerPort();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		if (lecteurChequeServerPort == null) {
			int port = myApp().config().intForKey(CHEQUE_SERVER_PORT_CONFIG_KEY);
			if (port != -1) {
				lecteurChequeServerPort = new Integer(port);
			}
		}
		if (lecteurChequeServerPort == null) {
			lecteurChequeServerPort = DEFAULT_PORT;
		}
		return lecteurChequeServerPort;
	}

	public boolean isLireChequeEnCours() {
		return isLireChequeEnCours;
	}

	public void setIsLireChequeEnCours(boolean isLireChequeEnCours) {
		this.isLireChequeEnCours = isLireChequeEnCours;
	}

	public boolean isLecteurReady() {
		return isLecteurReady;
	}

	public void setLecteurReady(boolean lecteurReady) {
		this.isLecteurReady = lecteurReady;
	}

	public boolean isBusy() {
		return isBusy;
	}

	public void setBusy(boolean isBusy) {
		this.isBusy = isBusy;
	}

}