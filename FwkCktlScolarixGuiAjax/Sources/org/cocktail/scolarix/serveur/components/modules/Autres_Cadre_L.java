/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.scolarix.serveur.components.controleurs.CtrlAutres;
import org.cocktail.scolarix.serveur.components.dataprovider.EtablissementProvider;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EORne;
import org.cocktail.scolarix.serveur.ui.EOGarnucheCadreDocumentation;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class Autres_Cadre_L extends ModuleDossierAdministratif {
    
    private CktlAjaxSelect2RemoteDataProvider dataProvider;
    
	public CtrlAutres ctrl;

	public EORne unEtablissement;
	private String codeEtablissement;

	public Autres_Cadre_L(WOContext context) {
	    super(context);
	    ctrl = new CtrlAutres(this, session().defaultEditingContext());
	    dataProvider = new EtablissementProvider(edc()) {
	        @Override
	        public EORne getSelectedRne() {
	            return etablissement();
	        }

	        @Override
	        public void setSelectedRne(EORne selectedRne) {
	            setEtablissement(selectedRne);
	        }
	    };	
	}

	public CktlAjaxSelect2RemoteDataProvider getDataProvider() {
        return dataProvider;
    }
	
	public WOComponent enregistrer() {
		super.enregistrer();
		return null;
	}

	public String disabledTFScript() {
		return super.disabledTFScript();
	}

	public void validate() throws EtudiantException {
		IEtudiant etudiant = etudiant();
		if (etudiant == null) {
			throw new EtudiantException("Pas d'étudiant !!!");
		}
		etudiant.validateAutres();
	}

	public void reset() {
		setCodeEtablissement(null);
	}

	public String consulterLaCharteHref() {
		EOKeyValueQualifier qual = new EOKeyValueQualifier(EOGarnucheCadreDocumentation.CDOC_TYPE_KEY,
				EOQualifier.QualifierOperatorCaseInsensitiveLike, "R");
		EOKeyValueQualifier qual1 = new EOKeyValueQualifier(EOGarnucheCadreDocumentation.CDOC_TEXTE_KEY,
				EOQualifier.QualifierOperatorCaseInsensitiveLike, "CHARTE");
		NSArray<EOGarnucheCadreDocumentation> cadreDocumentsInfo = cadre().garnucheCadreDocumentations(
				new EOAndQualifier(new NSArray<EOQualifier>(new EOKeyValueQualifier[] { qual, qual1 })));
		if (cadreDocumentsInfo != null && cadreDocumentsInfo.count() == 1) {
			EOGarnucheCadreDocumentation document = cadreDocumentsInfo.lastObject();
			return document.cdocLien();
		}
		return null;
	}

	public String onClickConsulterLaCharte() {
		String onClickConsulterLaCharte = "openWinAide('";
		String href = consulterLaCharteHref();
		if (href != null) {
			onClickConsulterLaCharte += href + "','" + "Charte Informatique";
			onClickConsulterLaCharte += "');";
		}
		else {
			onClickConsulterLaCharte = "alert('La charte n\\'est pas consultable pour le moment.');";
		}

		return onClickConsulterLaCharte;
	}

	public String codeEtablissement() {
		if (codeEtablissement == null) {
			if (historique() != null && historique().toRneTrans() != null) {
				setCodeEtablissement(historique().toRneTrans().cRne());
			}
		}
		return codeEtablissement;
	}

	public void setCodeEtablissement(String codeEtablissement) {
		this.codeEtablissement = codeEtablissement;
	}

	public EORne etablissement() {
		return historique().toRneTrans();
	}

	public void setEtablissement(EORne etablissement) {
		historique().setToRneTransRelationship(etablissement);
		if (etablissement != null) {
			setCodeEtablissement(etablissement.cRne());
		}
		else {
			setCodeEtablissement(null);
		}
	}

	public String email() {
		if (etudiant() != null && etudiant().etudiant() != null && etudiant().etudiant().compte() != null
				&& etudiant().etudiant().compte().toCompteEmail() != null && etudiant().etudiant().compte().toCompteEmail().cemEmail() != null) {
			return etudiant().etudiant().compte().toCompteEmail().cemEmail() + "@" + etudiant().etudiant().compte().toCompteEmail().cemDomaine();
		}
		return null;
	}
}