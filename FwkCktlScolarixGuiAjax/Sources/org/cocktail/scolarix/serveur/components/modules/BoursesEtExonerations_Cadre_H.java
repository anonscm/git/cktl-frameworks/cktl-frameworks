/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import org.cocktail.scolarix.serveur.components.controleurs.CtrlBoursesEtExonerations;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

public class BoursesEtExonerations_Cadre_H extends ModuleDossierAdministratif {
	public CtrlBoursesEtExonerations ctrl;

	public BoursesEtExonerations_Cadre_H(WOContext context) {
		super(context);
		ctrl = new CtrlBoursesEtExonerations(this, session().defaultEditingContext());
	}

	public WOComponent enregistrer() {
		super.enregistrer();
		return null;
	}

	public void validate() throws EtudiantException {
		IEtudiant etudiant = etudiant();
		if (etudiant == null) {
			throw new EtudiantException("Pas d'étudiant !!!");
		}
		etudiant.validateBoursesEtExonerations();
	}

	public boolean isNonBoursier() {
		if (historique() != null) {
			Integer histBourse = historique().histBourse();
			if (histBourse == null || histBourse.compareTo(EOHistorique.NON_BOURSIER) == 0) {
				return true;
			}
		}
		return false;
	}

	public void setIsNonBoursier(boolean isNonBoursier) {
		if (historique() != null && isNonBoursier) {
			historique().setHistBourse(EOHistorique.NON_BOURSIER);
		}
	}

	public boolean isBoursier() {
		boolean isBoursier = false;
		if (historique() != null) {
			Integer histBourse = historique().histBourse();
			if (histBourse == null || histBourse.compareTo(EOHistorique.BOURSIER) == 0) {
				isBoursier = true;
			}
		}
		return isBoursier;
	}

	public void setIsBoursier(boolean isBoursier) {
		if (historique() != null && isBoursier) {
			historique().setHistBourse(EOHistorique.BOURSIER);
		}
	}

	public boolean isExonere() {
		boolean isExonere = false;
		if (historique() != null) {
			Integer histBourse = historique().histBourse();
			if (histBourse == null || histBourse.compareTo(EOHistorique.EXONERE) == 0) {
				isExonere = true;
			}
		}
		return isExonere;
	}

	public void setIsExonere(boolean isExonere) {
		if (historique() != null && isExonere) {
			historique().setHistBourse(EOHistorique.EXONERE);
		}
	}

	public boolean isDemandeEnCours() {
		if (historique() != null) {
			Integer histBourse = historique().histBourse();
			if (histBourse != null && histBourse.compareTo(EOHistorique.BOURSIER_DEMANDE_EN_COURS) == 0) {
				return true;
			}
		}
		return false;
	}

	public void setIsDemandeEnCours(boolean isDemandeEnCours) {
		if (historique() != null && isDemandeEnCours) {
			historique().setHistBourse(EOHistorique.BOURSIER_DEMANDE_EN_COURS);
		}
	}

	public boolean isNonBoursierDisabled() {
		if (historique() != null && historique().histAyantDroit() != null && historique().histAyantDroit().intValue() == -2) {
			return true;
		}
		return false;
	}
}