/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.scolarix.serveur.components.controleurs.CtrlAdresses;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.finder.FinderTypeHebergement;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.extensions.appserver.ERXResponseRewriter;

public class Adresses_Cadre_B extends ModuleDossierAdministratif {
	
    private static final long serialVersionUID = -712951002756404315L;

	public CtrlAdresses ctrl;

	public String telephoneAdresseStable;

	public EOPays unPaysAdresseStable;
	private String codePaysAdresseStable;

	private String ligne1AdresseStable;
	private String ligne2AdresseStable;

	private String codePostalAdresseStable;
	private EOCommune uneCommuneAdresseStable;
	private EOCommune laCommuneSelectionneeAdresseStable;

	public String telephoneFixeAdresseUniversitaire;
	public String telephonePortableAdresseUniversitaire;

	public EOPays unPaysAdresseUniversitaire;
	private String codePaysAdresseUniversitaire;

	private String ligne1AdresseUniversitaire;
	private String ligne2AdresseUniversitaire;

	private String codePostalAdresseUniversitaire;
	private EOCommune uneCommuneAdresseUniversitaire;
	private EOCommune laCommuneSelectionneeAdresseUniversitaire;

	private String adresseMEL;

	public Adresses_Cadre_B(WOContext context) {
		super(context);
		ctrl = new CtrlAdresses(this, session().defaultEditingContext());
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		ERXResponseRewriter.addScriptResourceInHead(response, context, "FwkCktlScolarixGuiAjax.framework", "scripts/strings.js");
		ERXResponseRewriter.addScriptResourceInHead(response, context, "FwkCktlScolarixGuiAjax.framework", "scripts/formatteurs.js");
	}
	
	public String getContainerAdressesEntierId() {
		return getComponentId() + "_containerAdressesEntierId";
	}
	
	public String getContainerAdresseId() {
		return getComponentId() + "_containerAdresseId";
	}
	public String getContainerAdresseCPId() {
		return getComponentId() + "_containerAdresseCPId";
	}
	
	public String getContainerAdresseUniversitaireId() {
		return getComponentId() + "_containerAdresseUniversitaireId";
	}

	public void validate() throws EtudiantException {
		IEtudiant etudiant = etudiant();
		if (etudiant == null) {
			throw new EtudiantException("Pas d'étudiant !!!");
		}
		etudiant.validateAdresses();
	}

	public WOComponent enregistrer() {
		super.enregistrer();
		return null;
	}

	public void reset() {
		setCodePaysAdresseStable(null);
		setCodePaysAdresseUniversitaire(null);
		setCodePostalAdresseStable(null);
		setCodePostalAdresseUniversitaire(null);
		if (etudiant() != null && etudiant().edc() != null) {
			if (etudiant().isInscription() || etudiant().isReInscription()) {
				etudiant().setToTypeHebergementRelationship(null);
			}
		}
	}

	public String codePaysAdresseStable() {
		if (codePaysAdresseStable == null) {
			if (etudiant() != null) {
				EOAdresse adresseStable = etudiant().adresseStable();
				if (adresseStable != null && adresseStable.toPays() != null) {
					setCodePaysAdresseStable(adresseStable.toPays().cPays());
				}
			}
		}
		return codePaysAdresseStable;
	}

	public void setCodePaysAdresseStable(String codePaysAdresseStable) {
		this.codePaysAdresseStable = codePaysAdresseStable;
	}

	public EOPays lePaysSelectionneAdresseStable() {
		return etudiant().adresseStable().toPays();
	}

	public void setLePaysSelectionneAdresseStable(EOPays lePaysSelectionneAdresseStable) {
		etudiant().adresseStable().setToPaysRelationship(lePaysSelectionneAdresseStable);
		if (lePaysSelectionneAdresseStable != null) {
			setCodePaysAdresseStable(lePaysSelectionneAdresseStable.cPays());
		}
		else {
			setCodePaysAdresseStable(null);
		}
		if (lePaysSelectionneAdresseStable == null) {
			setLaCommuneSelectionneeAdresseStable(null);
		}
		else {
			if (lePaysSelectionneAdresseStable.isPaysDefaut() == true) {
				if (laCommuneSelectionneeAdresseStable() == null && etudiant() != null && etudiant().adresseStable() != null) {
					etudiant().adresseStable().setVille(null);
				}
			}
			else {
				if (laCommuneSelectionneeAdresseStable() != null) {
					setLaCommuneSelectionneeAdresseStable(null);
				}
				else {
					if (etudiant() != null && etudiant().adresseStable() != null) {
						etudiant().adresseStable().setCPCache("");
						setCodePostalAdresseStable("");
						etudiant().adresseStable().setAdrAdresse1("");
						etudiant().adresseStable().setAdrAdresse2("");
						etudiant().adresseStable().setVille("");
					}
				}
			}
		}
	}

	public String ligne1AdresseStable() {
		return ligne1AdresseStable;
	}

	public void setLigne1AdresseStable(String ligne1AdresseStable) {
		this.ligne1AdresseStable = ligne1AdresseStable;
	}

	public String ligne2AdresseStable() {
		return ligne2AdresseStable;
	}

	public void setLigne2AdresseStable(String ligne2AdresseStable) {
		this.ligne2AdresseStable = ligne2AdresseStable;
	}

	public String codePostalAdresseStable() {
		if (codePostalAdresseStable == null) {
			if (etudiant() != null) {
				EOAdresse adresseStable = etudiant().adresseStable();
				if (adresseStable != null && adresseStable.toPays() != null && adresseStable.toPays().isPaysDefaut()) {
					String codePostal = adresseStable.getCPCache();
					setCodePostalAdresseStable(codePostal);
				}
			}
		}
		return codePostalAdresseStable;
	}

	public void setCodePostalAdresseStable(String codePostalAdresseStable) {
		this.codePostalAdresseStable = codePostalAdresseStable;
	}

	public EOCommune uneCommuneAdresseStable() {
		return uneCommuneAdresseStable;
	}

	public void setUneCommuneAdresseStable(EOCommune uneCommuneAdresseStable) {
		this.uneCommuneAdresseStable = uneCommuneAdresseStable;
	}

	public EOCommune laCommuneSelectionneeAdresseStable() {
		return laCommuneSelectionneeAdresseStable;
	}

	public void setLaCommuneSelectionneeAdresseStable(EOCommune laCommuneSelectionneeAdresseStable) {
		this.laCommuneSelectionneeAdresseStable = laCommuneSelectionneeAdresseStable;
		if (laCommuneSelectionneeAdresseStable != null) {

			//etudiant().adresseStable().setCodePostal(laCommuneSelectionneeAdresseStable.cPostal());
			etudiant().adresseStable().setCPCache(laCommuneSelectionneeAdresseStable.cPostal());
//			etudiant().adresseStable().setVille(laCommuneSelectionneeAdresseStable.lcCom());
			etudiant().adresseStable().setVille(laCommuneSelectionneeAdresseStable.llCom());
			setCodePostalAdresseStable(laCommuneSelectionneeAdresseStable.cPostal());
		} else if (etudiant().adresseStable().toPays() != null && etudiant().adresseStable().toPays().equals(EOPays.CODE_PAYS_FRANCE)) {
			etudiant().adresseStable().setCPCache("");
			etudiant().adresseStable().setVille("");
			setCodePostalAdresseStable(null);
		}
		enregistrer();
	}

	public String codePaysAdresseUniversitaire() {
		if (codePaysAdresseUniversitaire == null) {
			if (etudiant() != null) {
				EOAdresse adresseUniversitaire = etudiant().adresseUniversitaire();
				if (adresseUniversitaire != null && adresseUniversitaire.toPays() != null) {
					setCodePaysAdresseUniversitaire(adresseUniversitaire.toPays().cPays());
				}
			}
		}
		return codePaysAdresseUniversitaire;
	}

	public void setCodePaysAdresseUniversitaire(String codePaysAdresseUniversitaire) {
		this.codePaysAdresseUniversitaire = codePaysAdresseUniversitaire;
	}

	public EOPays lePaysSelectionneAdresseUniversitaire() {
		return etudiant().adresseUniversitaire().toPays();
	}

	public void setLePaysSelectionneAdresseUniversitaire(EOPays lePaysSelectionneAdresseUniversitaire) {
		etudiant().adresseUniversitaire().setToPaysRelationship(lePaysSelectionneAdresseUniversitaire);
		if (lePaysSelectionneAdresseUniversitaire != null) {
			setCodePaysAdresseUniversitaire(lePaysSelectionneAdresseUniversitaire.cPays());
		}
		else {
			setCodePaysAdresseUniversitaire(null);
		}

		if (lePaysSelectionneAdresseUniversitaire == null) {
			setLaCommuneSelectionneeAdresseUniversitaire(null);
		}
		else {
			if (lePaysSelectionneAdresseUniversitaire.isPaysDefaut() == true) {
				if (laCommuneSelectionneeAdresseUniversitaire() == null && etudiant() != null && etudiant().adresseUniversitaire() != null) {
					etudiant().adresseUniversitaire().setVille(null);
				}
			}
			else {
				if (laCommuneSelectionneeAdresseUniversitaire() != null) {
					setLaCommuneSelectionneeAdresseUniversitaire(null);
				}
				else {
					if (etudiant() != null && etudiant().adresseUniversitaire() != null) {
						etudiant().adresseUniversitaire().setCPCache("");
						setCodePostalAdresseUniversitaire("");
						etudiant().adresseUniversitaire().setAdrAdresse1("");
						etudiant().adresseUniversitaire().setAdrAdresse2("");
						etudiant().adresseUniversitaire().setVille("");
					}
				}
			}
		}
	}

	public String ligne1AdresseUniversitaire() {
		return ligne1AdresseUniversitaire;
	}

	public void setLigne1AdresseUniversitaire(String ligne1AdresseUniversitaire) {
		this.ligne1AdresseUniversitaire = ligne1AdresseUniversitaire;
	}

	public String ligne2AdresseUniversitaire() {
		return ligne2AdresseUniversitaire;
	}

	public void setLigne2AdresseUniversitaire(String ligne2AdresseUniversitaire) {
		this.ligne2AdresseUniversitaire = ligne2AdresseUniversitaire;
	}

	public String codePostalAdresseUniversitaire() {
		if (codePostalAdresseUniversitaire == null) {
			if (etudiant() != null) {
				EOAdresse adresseUniversitaire = etudiant().adresseUniversitaire();
				if (adresseUniversitaire != null && adresseUniversitaire.toPays() != null && adresseUniversitaire.toPays().isPaysDefaut()) {
					String codePostal = adresseUniversitaire.getCPCache();
					setCodePostalAdresseUniversitaire(codePostal);
				}
			}
		}
		return codePostalAdresseUniversitaire;
	}

	public void setCodePostalAdresseUniversitaire(String codePostalAdresseUniversitaire) {
		this.codePostalAdresseUniversitaire = codePostalAdresseUniversitaire;
	}

	public EOCommune uneCommuneAdresseUniversitaire() {
		return uneCommuneAdresseUniversitaire;
	}

	public void setUneCommuneAdresseUniversitaire(EOCommune uneCommuneAdresseUniversitaire) {
		this.uneCommuneAdresseUniversitaire = uneCommuneAdresseUniversitaire;
	}

	public EOCommune laCommuneSelectionneeAdresseUniversitaire() {
		return laCommuneSelectionneeAdresseUniversitaire;
	}

	public void setLaCommuneSelectionneeAdresseUniversitaire(EOCommune laCommuneSelectionneeAdresseUniversitaire) {
		this.laCommuneSelectionneeAdresseUniversitaire = laCommuneSelectionneeAdresseUniversitaire;
		if (laCommuneSelectionneeAdresseUniversitaire != null) {
			etudiant().adresseUniversitaire().setCPCache(laCommuneSelectionneeAdresseUniversitaire.cPostal());
//			etudiant().adresseUniversitaire().setVille(laCommuneSelectionneeAdresseUniversitaire.lcCom());
			etudiant().adresseUniversitaire().setVille(laCommuneSelectionneeAdresseUniversitaire.llCom());
			setCodePostalAdresseUniversitaire(laCommuneSelectionneeAdresseUniversitaire.cPostal());
		}
		else {
			etudiant().adresseUniversitaire().setCPCache("");
			etudiant().adresseUniversitaire().setVille("");
			setCodePostalAdresseUniversitaire("");
		}
	}

	public String adresseMEL() {
		return adresseMEL;
	}

	public void setAdresseMEL(String adresseMEL) {
		this.adresseMEL = adresseMEL;
	}

	public Boolean isAdresseEnAttente() {
		Boolean isAdresseEnAttente = Boolean.FALSE;
		IEtudiant etudiant = etudiant();
		if ((etudiant.isPreReInscription() || etudiant.isPreInscription()) && etudiant.toTypeHebergement() != null
				&& etudiant.toTypeHebergement().equals(FinderTypeHebergement.getTypeHebergementEnAttente(etudiant.edc()))) {
			isAdresseEnAttente = Boolean.TRUE;
		}
		return isAdresseEnAttente;
	}

	public void setIsAdresseEnAttente(Boolean isAdresseEnAttente) {
		if (isAdresseEnAttente != null && isAdresseEnAttente.booleanValue()) {
			etudiant().setToTypeHebergementRelationship(FinderTypeHebergement.getTypeHebergementEnAttente(etudiant().edc()));
			ctrl.setNoTelephoneUniversitaireFixe(null);
			setLePaysSelectionneAdresseUniversitaire(null);
			etudiant().adresseUniversitaire().setToPaysRelationship(null);
			etudiant().adresseUniversitaire().setAdrAdresse1(null);
			etudiant().adresseUniversitaire().setAdrAdresse2(null);
			setLaCommuneSelectionneeAdresseUniversitaire(null);
			setCodePostalAdresseUniversitaire(null);
			etudiant().adresseUniversitaire().setCPCache(null);
			// setCommuneAdresseUniversitaire(null);
		}
		else {
			etudiant().setToTypeHebergementRelationship(null);
		}
	}

	public boolean isRecopierAdresseStableDisabled() {
		boolean isRecopierAdresseStableDisabled = false;

		if (disabled()) {
			isRecopierAdresseStableDisabled = true;
		}

		return isRecopierAdresseStableDisabled;
	}

	public boolean lignesAdresseStableDisabled() {
		boolean lignesAdresseStableDisabled = true;
		if (etudiant() != null && !disabled()) {
			EOAdresse adresseStable = etudiant().adresseStable();
			if (adresseStable != null) {
				EOPays pays = adresseStable.toPays();
				if (pays != null) {
					lignesAdresseStableDisabled = false;
				}
			}
		}
		return lignesAdresseStableDisabled;
	}

	public boolean codePostalAdresseStableDisabled() {
		boolean codePostalAdresseStableDisabled = true;
		if (etudiant() != null && !disabled()) {
			EOAdresse adresseStable = etudiant().adresseStable();
			if (adresseStable != null) {
				EOPays pays = adresseStable.toPays();
				if (pays != null && pays.isPaysDefaut()) {
					codePostalAdresseStableDisabled = false;
				}
			}
		}
		return codePostalAdresseStableDisabled;
	}

	public boolean communeAdresseStableDisabled() {
		return !codePostalAdresseStableDisabled();
	}

	public boolean lignesAdresseUniversitaireDisabled() {
		boolean lignesAdresseUniversitaireDisabled = true;
		if (etudiant() != null && !disabled() && !isAdresseEnAttente()) {
			EOAdresse adresseUniversitaire = etudiant().adresseUniversitaire();
			if (adresseUniversitaire != null) {
				EOPays pays = adresseUniversitaire.toPays();
				if (pays != null) {
					lignesAdresseUniversitaireDisabled = false;
				}
			}
		}
		return lignesAdresseUniversitaireDisabled;
	}

	public boolean codePostalAdresseUniversitaireDisabled() {
		boolean codePostalAdresseUniversitaireDisabled = true;
		if (etudiant() != null && !disabled() && !isAdresseEnAttente()) {
			EOAdresse adresseUniversitaire = etudiant().adresseUniversitaire();
			if (adresseUniversitaire != null) {
				EOPays pays = adresseUniversitaire.toPays();
				if (pays != null && pays.isPaysDefaut()) {
					codePostalAdresseUniversitaireDisabled = false;
				}
			}
		}
		return codePostalAdresseUniversitaireDisabled;
	}

	public boolean communeAdresseUniversitaireDisabled() {
		boolean communeAdresseUniversitaireDisabled = true;
		if (etudiant() != null && !disabled() && !isAdresseEnAttente()) {
			EOAdresse adresseUniversitaire = etudiant().adresseUniversitaire();
			if (adresseUniversitaire != null) {
				EOPays pays = adresseUniversitaire.toPays();
				if (pays == null || pays.isPaysDefaut() == false) {
					communeAdresseUniversitaireDisabled = false;
				}
			}
		}
		return communeAdresseUniversitaireDisabled;
	}

}