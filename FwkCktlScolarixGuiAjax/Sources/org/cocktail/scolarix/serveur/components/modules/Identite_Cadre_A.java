/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import java.net.URL;

import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlwebapp.server.CktlResourceManager;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAdmission;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolarix.serveur.components.controleurs.CtrlIdentite;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementSalarie;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXResponseRewriter;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

public class Identite_Cadre_A extends ModuleDossierAdministratif {
	
    private static final long serialVersionUID = -8891608116600551190L;

	public CtrlIdentite ctrl;

	private NSArray<EOCivilite> civilites;
	private EOCivilite uneCivilite;
	private EOCivilite civilite;

	private String nom;
	private String prenom1;
	private String prenom2;
	private String nomMarital;

	private NSTimestamp dateDeNaissance;
	private String lieuDeNaissance;

	public EODepartement unDepartementDeNaissance;
	private String codeDptDeNaissance;

	private EOPays unPaysDeNaissance;
	private String codePaysDeNaissance;

	public EOPays uneNationaliteDeNaissance;
	private String codeNationaliteDeNaissance;

	private EOVEtablissementSalarie unEtablissement;
	private String codeEtablissement;

	private EOScolFormationAdmission uneFormationAdmission, selectedFormationAdmission;

	public Identite_Cadre_A(WOContext context) {
		super(context);
		ctrl = new CtrlIdentite(this, session().defaultEditingContext());
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		ERXResponseRewriter.addScriptResourceInHead(response, context, "FwkCktlScolarixGuiAjax.framework", "scripts/strings.js");
		ERXResponseRewriter.addScriptResourceInHead(response, context, "FwkCktlScolarixGuiAjax.framework", "scripts/formatteurs.js");
	}
	
	public String getContainerPaysId() {
		return getComponentId() + "_ContainerPaysId";
	}
	
	public String getContainerSalarieId() {
		return getComponentId() + "_ContainerSalarieId";
	}
	
	public String getContainerEtablissementId() {
		return getComponentId() + "_ContainerEtablissementId";
	}
	
	public NSArray<EOScolFormationAdmission> listeFormationAdmissions() {
		EOQualifier q = ERXQ.equals(EOScolFormationAdmission.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "." + EOScolFormationAnnee.FANN_DEBUT_KEY,
				etudiant().anneeInscriptionEnCours()).and(ERXQ.equals(EOScolFormationAdmission.FADM_ADMISSION_KEY, "O"));
		ERXSortOrderings sorts = ERXS.ascs(EOScolFormationAdmission.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
				+ EOScolFormationSpecialisation.FSPN_LIBELLE_KEY, EOScolFormationAdmission.FADM_NIVEAU_KEY);
		return EOScolFormationAdmission.fetchAll(edc(), q, sorts);
	}

	public boolean isIdentiteDisabled() {
		return (isModification() != null && isModification().booleanValue()) && (utilisateur() == null || utilisateur().isIdentiteDisabled());
	}

	public boolean isIneDisabled() {
		return (isModification() != null && isModification().booleanValue()) && (utilisateur() == null || utilisateur().isIneDisabled());
	}

	public void validate() throws EtudiantException {
		IEtudiant etudiant = etudiant();
		if (etudiant == null) {
			throw new EtudiantException("Pas d'étudiant !!!");
		}
		etudiant.validateIdentite();

		EOHistorique historique = etudiant.historique(etudiant.anneeInscriptionEnCours());
		if ((historique.histSalarie().equals(EOHistorique.SALARIE_NON) == false) && (historique.histSalarieLibelle() == null)) {
			throw new EtudiantException("Précision sur la position du salarié obligatoire");
		}
	}

	public WOComponent enregistrer() {
		super.enregistrer();
		return null;
	}

	public boolean isPhoto() {
		// FIXME
		return true;
		// return utilisateur() != null && utilisateur().utiPhoto() != null && utilisateur().utiPhoto().intValue() == 1;
	}

	public NSData photo() {
		NSData laPhoto = null;
		if (etudiant() != null && etudiant().individu() != null) {
			if (etudiant().individu().toPhoto() != null) {
				laPhoto = etudiant().individu().toPhoto().datasPhoto();
			}
		}
		if (laPhoto == null) {
			try {
				laPhoto = new NSData(new URL("file:///" + new CktlResourceManager().pathForResource("no_photo.gif")));
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return laPhoto;
	}

	public String openBigPhotoDialog() {
		String s = "openCAW_BigPhotoDialog('";
		if (etudiant() != null && etudiant().individu() != null && etudiant().individu().getNomPrenomAffichage() != null) {
			s = s + etudiant().individu().getNomPrenomAffichage();
		}
		s = s + "');";
		return s;
	}

	public void reset() {
		setCodePaysDeNaissance(null);
		setCodeDptDeNaissance(null);
		setCodeNationaliteDeNaissance(null);
		setCodeEtablissement(null);
	}

	public NSArray<EOCivilite> civilites() {
		if (civilites == null) {
			civilites = ctrl.civilites();
		}
		return civilites;
	}

	public void setCivilites(NSArray<EOCivilite> civilites) {
		this.civilites = civilites;
	}

	public EOCivilite uneCivilite() {
		return uneCivilite;
	}

	public void setUneCivilite(EOCivilite uneCivilite) {
		this.uneCivilite = uneCivilite;
	}

	public EOCivilite civilite() {
		return civilite;
	}

	public void setCivilite(EOCivilite civilite) {
		this.civilite = civilite;
	}

	public String nom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String prenom1() {
		return prenom1;
	}

	public void setPrenom1(String prenom1) {
		this.prenom1 = prenom1;
	}

	public String prenom2() {
		return prenom2;
	}

	public void setPrenom2(String prenom2) {
		this.prenom2 = prenom2;
	}

	public String nomMarital() {
		return nomMarital;
	}

	public void setNomMarital(String nomMarital) {
		this.nomMarital = nomMarital;
	}

	public NSTimestamp dateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(NSTimestamp dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public String lieuDeNaissance() {
		return lieuDeNaissance;
	}

	public void setLieuDeNaissance(String lieuDeNaissance) {
		this.lieuDeNaissance = lieuDeNaissance;
	}

	public String codeDptDeNaissance() {
		if (codeDptDeNaissance == null) {
			if (etudiant() != null && etudiant().individu() != null && etudiant().individu().toDepartement() != null) {
				setCodeDptDeNaissance(etudiant().individu().toDepartement().cDepartement());
			}
		}
		return codeDptDeNaissance;
	}

	public void setCodeDptDeNaissance(String codeDptDeNaissance) {
		this.codeDptDeNaissance = codeDptDeNaissance;
	}

	public String codePaysDeNaissance() {
		if (codePaysDeNaissance == null) {
			if (etudiant() != null && etudiant().individu() != null && etudiant().individu().toPaysNaissance() != null) {
				setCodePaysDeNaissance(etudiant().individu().toPaysNaissance().cPays());
			}
		}
		return codePaysDeNaissance;
	}

	public void setCodePaysDeNaissance(String codePaysDeNaissance) {
		this.codePaysDeNaissance = codePaysDeNaissance;
	}

	public String codeNationaliteDeNaissance() {
		if (codeNationaliteDeNaissance == null) {
			if (etudiant() != null && etudiant().individu() != null && etudiant().individu().toPaysNationalite() != null) {
				setCodeNationaliteDeNaissance(etudiant().individu().toPaysNationalite().cPays());
			}
		}
		return codeNationaliteDeNaissance;
	}

	public void setCodeNationaliteDeNaissance(String codeNationaliteDeNaissance) {
		this.codeNationaliteDeNaissance = codeNationaliteDeNaissance;
	}

	public String codeEtablissement() {
		if (codeEtablissement == null) {
			if (historique() != null && historique().toVEtablissementSalarie() != null) {
				setCodeEtablissement(historique().toVEtablissementSalarie().cRne());
			}
		}
		return codeEtablissement;
	}

	public void setCodeEtablissement(String codeEtablissement) {
		this.codeEtablissement = codeEtablissement;
	}

	public boolean isSalarie() {
		if (etudiant() != null && anneeScolaire() != null) {
			String s = etudiant().salarieEtablissement(anneeScolaire());
			if (s != null && (s.equals(EOHistorique.SALARIE_FONCTIONNAIRE) || s.equals(EOHistorique.SALARIE_AUTRE))) {
				return true;
			}
		}
		return false;
	}

	public boolean isSalarieFonctionnaire() {
		if (etudiant() != null && anneeScolaire() != null) {
			String s = etudiant().salarieEtablissement(anneeScolaire());
			if (s != null && s.equals(EOHistorique.SALARIE_FONCTIONNAIRE)) {
				return true;
			}
		}
		return false;
	}

	public boolean isNomMaritalDisabled() {
		boolean isNomMaritalDisabled = true;
		IEtudiant etudiant = etudiant();
		if (etudiant != null) {
			EOCivilite civilite = etudiant.individu().toCivilite();
			if (civilite != null && civilite.lCivilite().toLowerCase().startsWith("madame")) {
				isNomMaritalDisabled = false;
			}
		}
		return isNomMaritalDisabled;
	}

	public boolean isCelibataireDisabled() {
		boolean isCelibataireDisabled = false;
		IEtudiant etudiant = etudiant();
		if (etudiant != null) {
			isCelibataireDisabled = !(etudiant.isCelibataire());
		}
		return isCelibataireDisabled;
	}

	public boolean isEnCoupleDisabled() {
		boolean isEnCoupleDisabled = false;
		IEtudiant etudiant = etudiant();
		if (etudiant != null) {
			isEnCoupleDisabled = !(etudiant.isEnCouple());
		}
		return isEnCoupleDisabled;
	}
	

	public EOPays lePaysNaissance() {
		return etudiant().individu().toPaysNaissance();
	}

	public void setLePaysNaissance(EOPays lePaysNaissance) {
		
		EOIndividu individu = etudiant().individu();
		individu.setToPaysNaissanceRelationship(lePaysNaissance);
		individu.setToDepartementRelationship(null);
		if (lePaysNaissance != null) {
			setCodePaysDeNaissance(lePaysNaissance.cPays());
		}
//			else {
//				setCodePaysDeNaissance(null);
//			}
		setCodeDptDeNaissance(null);
		
		if (/*individu.toPaysNationalite() == null && */lePaysNaissance != null) {
			individu.setToPaysNationaliteRelationship(lePaysNaissance);
			setCodeNationaliteDeNaissance(lePaysNaissance.cPays());
		}
		
	}

	public EOVEtablissementSalarie unEtablissement() {
		return unEtablissement;
	}

	public void setUnEtablissement(EOVEtablissementSalarie unEtablissement) {
		this.unEtablissement = unEtablissement;
	}

	public String histSalarie() {
		return historique().histSalarie();
	}

	public void setHistSalarie(String histSalarie) {
		historique().setHistSalarie(histSalarie);
		if (isSalarieFonctionnaire() == false) {
			setLeEtablissement(null);
			if (isSalarie() == false) {
				historique().setHistSalarieLibelle(null);
			}
		}
	}

	public EOPays lePaysNationalite() {
		return etudiant().individu().toPaysNationalite();
	}

	public void setLePaysNationalite(EOPays lePaysNationalite) {
		EOIndividu individu = etudiant().individu();
		individu.setToPaysNationaliteRelationship(lePaysNationalite);
		if (lePaysNationalite != null) {
			setCodeNationaliteDeNaissance(lePaysNationalite.cPays());
		}
		else {
			setCodeNationaliteDeNaissance(null);
		}
	}

	public EODepartement leDepartementDeNaissance() {
		return etudiant().individu().toDepartement();
	}

	public void setLeDepartementDeNaissance(EODepartement leDepartementDeNaissance) {
		etudiant().individu().setToDepartementRelationship(leDepartementDeNaissance);
		if (leDepartementDeNaissance != null) {
			setCodeDptDeNaissance(leDepartementDeNaissance.cDepartement());
		}
		else {
			setCodeDptDeNaissance(null);
		}
	}

	public EOVEtablissementSalarie leEtablissement() {
		return historique().toVEtablissementSalarie();
	}

	public void setLeEtablissement(EOVEtablissementSalarie etablissement) {
		historique().setToVEtablissementSalarieRelationship(etablissement);
		if (etablissement != null) {
			setCodeEtablissement(etablissement.cRne());
		}
		else {
			setCodeEtablissement(null);
		}
	}

	public String etudiantIndividuNomAffichage() {
		if (etudiant() == null || etudiant().individu() == null) {
			return "";
		}
		if (etudiant().individu().nomAffichage() != null
				&& etudiant().individu().nomAffichage().equals(etudiant().individu().nomPatronymiqueAffichage())) {
			return "";
		}
		return etudiant().individu().nomAffichage();
	}

	public void setEtudiantIndividuNomAffichage(String etudiantIndividuNomAffichage) {
		if (etudiant() != null && etudiant().individu() != null) {
			etudiant().individu().setNomAffichage(etudiantIndividuNomAffichage);
		}
	}

	public boolean codeDptDeNaissanceDisabled() {
		if (lePaysNaissance() == null) {
			return true;
		}
		return lePaysNaissance().isPaysDefaut() == false;
	}

	public String jsSelectFirstField() {
		if (isInscriptionOuReInscription()) {
			return "Form.Element.activate('CodeINE');";
		}
		return "Form.Element.activate('Civilites');";
	}

	public EOScolFormationAdmission getUneFormationAdmission() {
		return uneFormationAdmission;
	}

	public void setUneFormationAdmission(EOScolFormationAdmission uneFormationAdmission) {
		this.uneFormationAdmission = uneFormationAdmission;
	}

	public EOScolFormationAdmission getSelectedFormationAdmission() {
		if (selectedFormationAdmission == null && etudiant().candidat() != null) {
			selectedFormationAdmission = etudiant().candidat().toFwkScolarite_ScolFormationAdmission();
		}
		return selectedFormationAdmission;
	}

	public void setSelectedFormationAdmission(EOScolFormationAdmission selectedFormationAdmission) {
		this.selectedFormationAdmission = selectedFormationAdmission;
		if (selectedFormationAdmission == null && etudiant().candidat() != null) {
			etudiant().candidat().setToFwkScolarite_ScolFormationSpecialisationRelationship(null);
			etudiant().candidat().setCandAnneeSuivie(null);
			etudiant().candidat().setToFwkScolarite_ScolFormationAdmissionRelationship(null);
		}
		else {
			etudiant().candidat().setToFwkScolarite_ScolFormationSpecialisationRelationship(
					selectedFormationAdmission.toFwkScolarite_ScolFormationSpecialisation());
			etudiant().candidat().setCandAnneeSuivie(selectedFormationAdmission.fadmNiveau());
			etudiant().candidat().setToFwkScolarite_ScolFormationAdmissionRelationship(selectedFormationAdmission);
		}
	}

	public EOPays getUnPaysDeNaissance() {
	    return unPaysDeNaissance;
    }

	public void setUnPaysDeNaissance(EOPays unPaysDeNaissance) {
	    this.unPaysDeNaissance = unPaysDeNaissance;
    }
}