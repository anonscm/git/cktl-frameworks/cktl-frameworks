/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import java.util.GregorianCalendar;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.scolarix.serveur.components.controleurs.CtrlSituationSociale;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOActiviteProfessionnelle;
import org.cocktail.scolarix.serveur.metier.eos.EOCotisation;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOMutuelle;
import org.cocktail.scolarix.serveur.metier.eos.EOOrigineRessources;
import org.cocktail.scolarix.serveur.metier.eos.EOQuotiteTravail;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class SituationSociale_Cadre_I extends ModuleDossierAdministratif {
	public static NSArray<String> listeChoixOuiNon = new NSArray<String>(new String[] { "NON", "OUI" });
	public CtrlSituationSociale ctrl;
	public EOCotisation unMotifNonAffiliation;
	public EOMutuelle unOrganisme;
	public EOOrigineRessources uneOrigineRessources;
	public EOActiviteProfessionnelle uneActiviteProfessionnelle;

	public SituationSociale_Cadre_I(WOContext context) {
		super(context);
		ctrl = new CtrlSituationSociale(this, session().defaultEditingContext());
	}

	public WOComponent enregistrer() {
		super.enregistrer();
		return null;
	}

	public void validate() throws EtudiantException {
		IEtudiant etudiant = etudiant();
		if (etudiant == null) {
			throw new EtudiantException("Pas d'étudiant !!!");
		}
		etudiant.validateSituationSociale();
	}

	public NSTimestamp dateLimiteNonCotisation() {
		if (anneeScolaire() != null) {
			return new NSTimestamp(new GregorianCalendar(anneeScolaire().intValue() - 19, 8, 30).getTime());
		}
		return null;
	}

	public boolean isChoixCotisationNonDisabled() {
		if (isChoixCotisationOuiDisabled() || ( !isChoixCotisationOuiDisabled() && !etudiant().individu().getPaysNaissance().getCode().equals(IPays.CODE_PAYS_FRANCE))) {
			return false;
		}
		if (historique() != null && historique().histAyantDroit() != null && historique().histAyantDroit().intValue() == 1) {
			historique().setHistAyantDroit(new Integer(0));
		}
		return true;
	}

	public boolean isChoixCotisationOuiDisabled() {
		if (etudiant() != null && etudiant().individu() != null && etudiant().individu().dNaissance() != null) {
			if (etudiant().individu().dNaissance().after(dateLimiteNonCotisation()) && etudiant().individu().getPaysNaissance().getCode().equals(IPays.CODE_PAYS_FRANCE)) {
				historique().setHistAyantDroit(new Integer(1));
				return true;
			}
		}
		return false;
	}

	public Integer histAyantDroit() {
		return historique().histAyantDroit();
	}

	public void setHistAyantDroit(Integer histAyantDroit) {
		historique().setHistAyantDroit(histAyantDroit);
		if (histAyantDroit != null) {
			if (histAyantDroit.intValue() == -2) {
				historique().setHistBourse(EOHistorique.BOURSIER_DEMANDE_EN_COURS);
			}
		}
	}

	public String uneOrigineRessourcesDisplayString() {
		return uneOrigineRessources.oresCode() + "-" + uneOrigineRessources.oresLibelle();
	}

	public String uneActiviteProfessionnelleDisplayString() {
		String libelle = uneActiviteProfessionnelle.activLibelle();
		libelle = libelle.substring(0, libelle.length() > 40 ? 40 : libelle.length());
		return uneActiviteProfessionnelle.activCode() + "-" + libelle;
	}

	public Boolean isNoInseeProvisoire() {
		Boolean isNoInseeProvisoire = Boolean.FALSE;
		if (etudiant() != null && etudiant().individu() != null && etudiant().individu().priseCptInsee() != null
				&& etudiant().individu().priseCptInsee().equalsIgnoreCase("P")) {
			isNoInseeProvisoire = Boolean.TRUE;
		}
		return isNoInseeProvisoire;
	}

	public void setIsNoInseeProvisoire(Boolean isNoInseeProvisoire) {
		if (isNoInseeProvisoire != null && isNoInseeProvisoire.booleanValue()) {
			if (isNoInseeProvisoire() == false && etudiant() != null && etudiant().individu() != null) {
				etudiant().individu().setIndNoInseeProv(etudiant().individu().indNoInsee());
				etudiant().individu().setIndCleInseeProv(etudiant().individu().indCleInsee());
				etudiant().individu().setPriseCptInsee("P");
			}
		}
		else {
			if (isNoInseeProvisoire() && etudiant() != null && etudiant().individu() != null) {
				etudiant().individu().setIndNoInsee(etudiant().individu().indNoInseeProv());
				etudiant().individu().setIndCleInsee(etudiant().individu().indCleInseeProv());
				etudiant().individu().setPriseCptInsee("R");
			}
		}
	}

	public boolean isActiviteSalarieeNon() {
		boolean isActiviteSalarieeNon = false;
		IEtudiant etudiant = etudiant();
		if (etudiant != null) {
			EOHistorique historique = etudiant.historique(anneeScolaire());
			if (historique != null) {
				EOQuotiteTravail quotiteTravail = historique.toQuotiteTravail();
				if (quotiteTravail == null || quotiteTravail.qtraCode().equals(EOQuotiteTravail.QTRA_CODE_SANS_OBJET)) {
					isActiviteSalarieeNon = true;
				}
			}
		}
		return isActiviteSalarieeNon;
	}

	public void setIsActiviteSalarieeNon(boolean isActiviteSalarieeNon) {
		IEtudiant etudiant = etudiant();
		if (etudiant != null) {
			EOHistorique historique = etudiant.historique(anneeScolaire());
			if (historique != null && isActiviteSalarieeNon) {
				historique.setToQuotiteTravailRelationship(null);
			}
		}
	}

	public boolean isActiviteSalarieeOuiSuperieure() {
		boolean isActiviteSalarieeOuiSuperieure = false;
		IEtudiant etudiant = etudiant();
		if (etudiant != null) {
			EOHistorique historique = etudiant.historique(anneeScolaire());
			if (historique != null) {
				EOQuotiteTravail quotiteTravail = historique.toQuotiteTravail();
				if (quotiteTravail != null && quotiteTravail.qtraCode().equals(EOQuotiteTravail.QTRA_CODE_ACTIVITE_SALARIEE_SUPERIEURE_OU_EGALE_60H)) {
					isActiviteSalarieeOuiSuperieure = true;
				}
			}
		}
		return isActiviteSalarieeOuiSuperieure;
	}

	public void setIsActiviteSalarieeOuiSuperieure(boolean isActiviteSalarieeOuiSuperieure) {
		IEtudiant etudiant = etudiant();
		if (etudiant != null) {
			EOHistorique historique = etudiant.historique(anneeScolaire());
			if (historique != null && isActiviteSalarieeOuiSuperieure) {
				historique.setToQuotiteTravailRelationship(EOQuotiteTravail.fetchByKeyValue(edc(), EOQuotiteTravail.QTRA_CODE_KEY,
						EOQuotiteTravail.QTRA_CODE_ACTIVITE_SALARIEE_SUPERIEURE_OU_EGALE_60H));
			}
		}
	}

	public boolean isActiviteSalarieeOuiInferieure() {
		boolean isActiviteSalarieeOuiInferieure = false;
		IEtudiant etudiant = etudiant();
		if (etudiant != null) {
			EOHistorique historique = etudiant.historique(anneeScolaire());
			if (historique != null) {
				EOQuotiteTravail quotiteTravail = historique.toQuotiteTravail();
				if (quotiteTravail != null && quotiteTravail.qtraCode().equals(EOQuotiteTravail.QTRA_CODE_ACTIVITE_SALARIEE_INFERIEURE_60H)) {
					isActiviteSalarieeOuiInferieure = true;
				}
			}
		}
		return isActiviteSalarieeOuiInferieure;
	}

	public void setIsActiviteSalarieeOuiInferieure(boolean isActiviteSalarieeOuiInferieure) {
		IEtudiant etudiant = etudiant();
		if (etudiant != null) {
			EOHistorique historique = etudiant.historique(anneeScolaire());
			if (historique != null && isActiviteSalarieeOuiInferieure) {
				historique.setToQuotiteTravailRelationship(EOQuotiteTravail.fetchByKeyValue(edc(), EOQuotiteTravail.QTRA_CODE_KEY,
						EOQuotiteTravail.QTRA_CODE_ACTIVITE_SALARIEE_INFERIEURE_60H));
			}
		}
	}

	public Boolean isOrganismeSelectionne() {
		Boolean isOrganismeSelectionne = Boolean.FALSE;
		if (historique() != null && historique().toMutuelleOrga() != null) {
			EOMutuelle mutuelle = historique().toMutuelleOrga();
			if (unOrganisme.mutLibelle().equals(mutuelle.mutLibelle())) {
				isOrganismeSelectionne = Boolean.TRUE;
			}
		}
		return isOrganismeSelectionne;
	}

	public boolean isChoixCotisationOUIExonere() {
		return (historique() != null && historique().histAyantDroit() != null && historique().histAyantDroit().intValue() == -2);
	}

	public void setIsOrganismeSelectionne(Boolean isOrganismeSelectionne) {
		if (isOrganismeSelectionne != null) {
			EOMutuelle mutuelle = unOrganisme;
			historique().setToMutuelleOrgaRelationship(mutuelle);
		}
	}

	public String onClickDocumentationOrganisme() {
		String onClickDocumentationOrganisme = "openWinAide('";
		String url = unOrganisme.toFwkpers_Adresse().adrUrlPere();
		String titre = unOrganisme.mutLibelle();
		onClickDocumentationOrganisme += url + "','" + titre;
		onClickDocumentationOrganisme += "');";

		return onClickDocumentationOrganisme;
	}

	public String organismeCBId() {
		String organismeCBId = "OrganismeCB_" + unOrganisme.mutCode();
		return organismeCBId;
	}

	public String indNoInsee() {
		if (etudiant() != null && etudiant().individu() != null) {
			if (isNoInseeProvisoire()) {
				return etudiant().individu().indNoInseeProv();
			}
			else {
				return etudiant().individu().indNoInsee();
			}
		}
		return null;
	}

	public void setIndNoInsee(String indNoInsee) {
		if (etudiant() != null && etudiant().individu() != null) {
			if (isNoInseeProvisoire()) {
				etudiant().individu().setIndNoInseeProv(indNoInsee);
			}
			else {
				etudiant().individu().setIndNoInsee(indNoInsee);
			}
		}
	}

	public Integer indCleInsee() {
		if (etudiant() != null && etudiant().individu() != null) {
			if (isNoInseeProvisoire()) {
				return etudiant().individu().indCleInseeProv();
			}
			else {
				return etudiant().individu().indCleInsee();
			}
		}
		return null;
	}

	public void setIndCleInsee(Integer indCleInsee) {
		if (etudiant() != null && etudiant().individu() != null) {
			if (isNoInseeProvisoire()) {
				etudiant().individu().setIndCleInseeProv(indCleInsee);
			}
			else {
				etudiant().individu().setIndCleInsee(indCleInsee);
			}
		}
	}

}