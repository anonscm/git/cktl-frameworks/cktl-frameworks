/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.components.controleurs.CtrlTitreAcces;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.finder.FinderDepartement;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOBac;
import org.cocktail.scolarix.serveur.metier.eos.EOMention;
import org.cocktail.scolarix.serveur.metier.eos.EORne;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

public class TitreAcces_Cadre_D extends ModuleDossierAdministratif {
	
    private static final long serialVersionUID = 7511904972151268782L;
	public CtrlTitreAcces ctrl;
	private String codeBac;
	private String codeMention;
	private String codeEtablissement;
	private String codeDepartement;
	private String codePays;
	public EOBac unTitreAcces;
	public EOMention uneMention;
	public EORne unEtablissement;
	public EODepartement unDepartement;
	public EOPays unPays;

	public TitreAcces_Cadre_D(WOContext context) {
		super(context);
		ctrl = new CtrlTitreAcces(this, session().defaultEditingContext());
	}
	
	public String getContainerBacId() {
		return getComponentId() + "_containerBacId";
	}
	
	public String getContainerEtablissementId() {
		return getComponentId() + "_containerEtablissementId";
	}
	
	public void reset() {
		setCodeBac(null);
		setCodeMention(null);
		setCodePays(null);
		setCodeDepartement(null);
		setCodeEtablissement(null);
	}

	public EOPays pays() {
		return etudiant().paysTitreAcces();
	}

	public void setPays(EOPays pays) {
		etudiant().setPaysTitreAcces(pays);
		if (pays != null) {
			setCodePays(pays.cPays());
			if (pays.isPaysDefaut() == false) {
				setDepartement(null);
			}
		} else {
			setCodePays(null);
			setDepartement(null);
		}
	}

	public EODepartement departement() {
		return etudiant().departementTitreAcces();
	}

	public void setDepartement(EODepartement departement) {
		etudiant().setDepartementTitreAcces(departement);
		if (departement != null) {
			setCodeDepartement(departement.cDepartement());
		} else {
			setCodeDepartement(null);
		}
	}

	public EORne etablissement() {
		return etudiant().etablissementTitreAcces();
	}

	public void setEtablissement(EORne etablissement) {
		etudiant().setEtablissementTitreAcces(etablissement);
		if (etablissement != null) {
			setCodeEtablissement(etablissement.cRne());
			String ville = etablissement.ville();
			if (!StringCtrl.isEmpty(ville) && etudiant().villeTitreAcces() == null) {
				etudiant().setVilleTitreAcces(ville);
			}
			if (etablissement.codePostal() != null) {
				String cDepartement = etablissement.codePostal().substring(0, 2);
				EODepartement departement = FinderDepartement.getDepartementByKey(edc(), "0" + cDepartement);
				if (departement != null) {
					setDepartement(departement);
					// S'il y a un departement, on considere que c'est dans le pays local
					setPays(EOPays.getPaysDefaut(edc()));
				}
			}
		} else {
			setCodeEtablissement(null);
			setDepartement(null);
			setPays(null);
			etudiant().setVilleTitreAcces(null);
		}
	}

	public EOBac titreAcces() {
		return etudiant().titreAcces();
	}

	public void setTitreAcces(EOBac titreAcces) {
		etudiant().setTitreAcces(titreAcces);
		if (titreAcces != null) {
			setCodeBac(titreAcces.bacCode());
		} else {
			setCodeBac(null);
		}
	}

	public WOComponent enregistrer() {
		super.enregistrer();
		return null;
	}

	public String disabledTFScript() {
		String disabledTFScript = "";

		if (etudiant() != null) {
			EOPays pays = etudiant().paysTitreAcces();
			if (pays != null && pays.isPaysDefaut()) {
				disabledTFScript += "Field.enable($('CodeDepartement_TitreAcces_field'));";
				disabledTFScript += "Field.disable($('Etablissement_TitreAcces'));";
			} else {
				disabledTFScript += "Field.disable($('CodeDepartement_TitreAcces_field'));";
				disabledTFScript += "Field.enable($('Etablissement_TitreAcces'));";
			}
			disabledTFScript += super.disabledTFScript();
		}

		return disabledTFScript;
	}

	public void validate() throws EtudiantException {
		IEtudiant etudiant = etudiant();
		if (etudiant == null) {
			throw new EtudiantException("Pas d'étudiant !!!");
		}
		etudiant.validateTitreAcces();
	}

	public String codeBac() {
		if (codeBac == null) {
			if (etudiant() != null && etudiant().titreAcces() != null) {
				setCodeBac(etudiant().titreAcces().bacCode());
			}
		}
		return codeBac;
	}

	public void setCodeBac(String codeBac) {
		this.codeBac = codeBac;
	}

	public String codeMention() {
		if (codeMention == null) {
			if (etudiant() != null && etudiant().mentionTitreAcces() != null) {
				setCodeMention(etudiant().mentionTitreAcces().mentCode());
			}
		}
		return codeMention;
	}

	public void setCodeMention(String codeMention) {
		this.codeMention = codeMention;
	}

	public String codeEtablissement() {
		if (codeEtablissement == null) {
			if (etudiant() != null && etudiant().etablissementTitreAcces() != null) {
				setCodeEtablissement(etudiant().etablissementTitreAcces().cRne());
			}
		}
		return codeEtablissement;
	}

	public void setCodeEtablissement(String codeEtablissement) {
		this.codeEtablissement = codeEtablissement;
	}

	public String codeDepartement() {
		if (codeDepartement == null) {
			if (etudiant() != null) {
				EOPays pays = etudiant().paysTitreAcces();
				EODepartement departement = etudiant().departementTitreAcces();
				if (departement != null && pays != null && pays.isPaysDefaut()) {
					setCodeDepartement(departement.cDepartement());
				}
			}
		}
		return codeDepartement;
	}

	public void setCodeDepartement(String codeDepartement) {
		this.codeDepartement = codeDepartement;
	}

	public String codePays() {
		if (codePays == null) {
			if (etudiant() != null && etudiant().paysTitreAcces() != null) {
				setCodePays(etudiant().paysTitreAcces().cPays());
			}
		}
		return codePays;
	}

	public void setCodePays(String codePays) {
		this.codePays = codePays;
	}

	public boolean isEtablissementTitreAccesDisabled() {
		return pays() == null || pays().isPaysDefaut();
	}

	public boolean isCodeDepartementTitreAccesDisabled() {
		return disabled() || pays() == null || pays().isPaysDefaut() == false;
	}
	
}
