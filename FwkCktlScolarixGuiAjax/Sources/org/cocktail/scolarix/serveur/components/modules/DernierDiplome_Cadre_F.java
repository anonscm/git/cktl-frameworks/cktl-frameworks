/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.scolarix.serveur.components.controleurs.CtrlDernierDiplome;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.finder.FinderDepartement;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EORne;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

public class DernierDiplome_Cadre_F extends ModuleDossierAdministratif {
	public CtrlDernierDiplome ctrl;

	public EORne unEtablissement;
	private String codeEtablissement;
	public EODepartement unDepartement;
	private String codeDepartement;
	public EOPays unPays;
	private String codePays;

	public EOTypeDiplome unTypeDiplome;

	public DernierDiplome_Cadre_F(WOContext context) {
		super(context);
		ctrl = new CtrlDernierDiplome(this, session().defaultEditingContext());
	}
	
	public String getContainerEtablissementId() {
		return getComponentId() + "_containerEtablissementId";
	}
	
	public WOComponent enregistrer() {
		super.enregistrer();
		return null;
	}

	public void validate() throws EtudiantException {
		IEtudiant etudiant = etudiant();
		if (etudiant == null) {
			throw new EtudiantException("Pas d'étudiant !!!");
		}
		etudiant.validateDernierDiplome();
	}

	public void reset() {
		setCodePays(null);
		setCodeDepartement(null);
		setCodeEtablissement(null);
	}

	public String disabledTFScript() {
		String disabledTFScript = "";
		if (etudiant() != null) {
			EOPays pays = historique().toFwkpers_Pays_DerDipl();
			if (pays != null) {
				if (pays.isPaysDefaut()) {
					disabledTFScript += "Field.enable($('CodeDepartement_DernierDiplome_field'));";
					disabledTFScript += "Field.disable($('Etablissement_DernierDiplome'));";
				}
				else {
					disabledTFScript += "Field.disable($('CodeDepartement_DernierDiplome_field'));";
					disabledTFScript += "Field.enable($('Etablissement_DernierDiplome'));";
				}
			}
			disabledTFScript += super.disabledTFScript();
		}
		return disabledTFScript;
	}

	public String codeEtablissement() {
		if (codeEtablissement == null) {
			if (etudiant() != null && historique() != null && historique().toRneDerDipl() != null) {
				setCodeEtablissement(historique().toRneDerDipl().cRne());
			}
		}
		return codeEtablissement;
	}

	public void setCodeEtablissement(String codeEtablissement) {
		this.codeEtablissement = codeEtablissement;
	}

	public EORne etablissement() {
		return historique().toRneDerDipl();
	}

	public void setEtablissement(EORne etablissement) {
		historique().setToRneDerDiplRelationship(etablissement);
		if (etablissement != null) {
			setCodeEtablissement(etablissement.cRne());
			if (etablissement.ville() != null) {
				historique().setHistVilleDerDipl(etablissement.ville());
			}
			if (etablissement.cRne() != null && etablissement.cRne().length() >= 3) {
				String cDepartement = etablissement.cRne().substring(0, 3);
				EODepartement departement = FinderDepartement.getDepartementByKey(edc(), cDepartement);
				if (departement != null) {
					setDepartement(departement);
					historique().setToFwkpers_Pays_DerDiplRelationship(EOPays.getPaysDefaut(edc()));
				}
			}
			etudiant().etudiant().onDernierDiplomeUpdate();
		} else {
			setCodeEtablissement(null);
			setDepartement(null);
			setPays(null);
			historique().setHistVilleDerDipl(null);
		}
	}

	public String codeDepartement() {
		if (codeDepartement == null) {
			if (historique() != null) {
				EOPays pays = historique().toFwkpers_Pays_DerDipl();
				EODepartement departement = historique().toFwkpers_Departement_DerDipl();
				if (departement != null && pays != null && pays.isPaysDefaut()) {
					setCodeDepartement(departement.cDepartement());
				}
			}
		}
		return codeDepartement;
	}

	public void setCodeDepartement(String codeDepartement) {
		// if (codeDepartement == null) {
		// if (etudiant() != null && historique() != null) {
		// EOPays pays = historique().toFwkpers_Pays_DerDipl();
		// EODepartement departement = historique().toFwkpers_Departement_DerDipl();
		// if (departement != null && pays != null && pays.isPaysDefaut()) {
		// setCodeDepartement(departement.cDepartement());
		// }
		// }
		// }
		this.codeDepartement = codeDepartement;
	}

	public EODepartement departement() {
		return historique().toFwkpers_Departement_DerDipl();
	}

	public void setDepartement(EODepartement departement) {
		historique().setToFwkpers_Departement_DerDiplRelationship(departement);
		etudiant().etudiant().onDernierDiplomeUpdate();
		if (departement != null) {
			setCodeDepartement(departement.cDepartement());
		}
		else {
			setCodeDepartement(null);
		}
	}

	public String codePays() {
		if (codePays == null) {
			if (etudiant() != null && historique() != null && historique().toFwkpers_Pays_DerDipl() != null) {
				setCodePays(historique().toFwkpers_Pays_DerDipl().cPays());
			}
		}
		return codePays;
	}

	public void setCodePays(String codePays) {
		this.codePays = codePays;
	}

	public EOPays pays() {
		return historique().toFwkpers_Pays_DerDipl();
	}

	public void setPays(EOPays pays) {
		historique().setToFwkpers_Pays_DerDiplRelationship(pays);
		etudiant().etudiant().onDernierDiplomeUpdate();
		if (pays != null) {
			setCodePays(pays.cPays());
			if (pays.isPaysDefaut() == false) {
				setDepartement(null);
			}
		}
		else {
			setCodePays(null);
		}
	}

	public String histlibelleEtabDerDipl() {
		String histlibelleEtabDerDipl = historique().histVilleDerDipl();
		if (historique().toRneDerDipl() != null) {
			histlibelleEtabDerDipl = historique().toRneDerDipl().llRne();
		}
		return histlibelleEtabDerDipl;
	}

	public void setHistlibelleEtabDerDipl(String histlibelleEtabDerDipl) {
		historique().setHistVilleDerDipl(histlibelleEtabDerDipl);
		etudiant().etudiant().onDernierDiplomeUpdate();
	}

	public String unTypeDiplomeDisplayString() {
		String unTypeDiplomeDisplayString = unTypeDiplome.tdiplCode() + " - " + unTypeDiplome.tdiplLibelle();

		return unTypeDiplomeDisplayString;
	}

	public boolean isEtablissementDernierDiplomeDisabled() {
		return pays() == null || pays().isPaysDefaut();
	}

	public boolean isCodeDepartementDernierDiplomeDisabled() {
		return disabled() || pays() == null || pays().isPaysDefaut() == false;
	}

	public Integer histAnneeDerDipl() {
		return historique().histAnneeDerDipl();
	}

	public void setHistAnneeDerDipl(Integer histAnneeDerDipl) {
		historique().setHistAnneeDerDipl(histAnneeDerDipl);
		etudiant().etudiant().onDernierDiplomeUpdate();
	}

	public String histLibelleDerDipl() {
		return historique().histLibelleDerDipl();
	}

	public void setHistLibelleDerDipl(String histLibelleDerDipl) {
		historique().setHistLibelleDerDipl(histLibelleDerDipl);
		etudiant().etudiant().onDernierDiplomeUpdate();
	}
	
}
