/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import java.util.Hashtable;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.finder.FinderBanque;
import org.cocktail.scolarix.serveur.metier.eos.EOBanque;
import org.cocktail.scolarix.serveur.metier.eos.EOPaiementAdm;
import org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte;
import org.cocktail.scolarix.serveur.process.ProcessDelPaiementAdm;
import org.cocktail.scolarix.serveur.process.ProcessInsPaiementAdm;
import org.cocktail.scolarix.serveur.process.ProcessUpdPaiementAdm;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXS;

public class PaiementsAdmission_Cadre_J extends Paiements_Cadre_J {
	private EOPaiementAdm unPaiementAdm;
	private EOPaiementAdm lePaiementAdmEnCours;

	public PaiementsAdmission_Cadre_J(WOContext context) {
		super(context);
	}

	public void reset() {
		if (etudiant() != null && etudiant().candidat() != null) {
			dgPaiementsUpdateData();
		}
	}

	public WODisplayGroup dgPaiements() {
		if (dgPaiements == null) {
			dgPaiements = new WODisplayGroup();
			dgPaiements.setSelectsFirstObjectAfterFetch(true);
			dgPaiements.setSortOrderings(ERXS.ascs(EOPaiementAdm.PADM_NUMERO_KEY));
			dgPaiementsUpdateData();
		}
		return dgPaiements;
	}

	public void dgPaiementsUpdateData() {
		dgPaiements().setObjectArray(etudiant().candidat().toPaiementAdms());
		setLePaiementAdmEnCours((EOPaiementAdm) dgPaiements().selectedObject());
	}

	public void onSelectPaiement() {
		setLePaiementAdmEnCours((EOPaiementAdm) dgPaiements().selectedObject());
	}

	public void setBanque(EOBanque banque) {
		this.banque = banque;
		lePaiementAdmEnCours().setToBanqueRelationship(banque);
		if (banque != null) {
			setCodeBanque(banque.banqCode());
		}
	}

	public String unPaiementPadmAccompte() {
		return unPaiementAdm() != null && unPaiementAdm().padmAccompte() != null && unPaiementAdm().padmAccompte().intValue() == 1 ? "O" : "N";
	}

	// Paiements .............................

	public void setLePaiementAdmEnCours(EOPaiementAdm lePaiementAdmEnCours) {
		this.lePaiementAdmEnCours = lePaiementAdmEnCours;
		setCodeBanque(null);
		if (lePaiementAdmEnCours != null) {
			if (lePaiementAdmEnCours.toBanque() != null) {
				setCodeBanque(lePaiementAdmEnCours.toBanque().banqCode());
			}
		}
	}

	// Ajout de paiement............................

	public WOComponent ajouterUnPaiement() {
		setLePaiementAdmEnCours(EOPaiementAdm.create(edc(), etudiant().candidat()));
		setIsPaiementAjoutEnCours(true);
		return null;
	}

	public WOActionResults ajouterLePaiementValide() {
		try {
			ProcessInsPaiementAdm.enregistrer(cktlSession().dataBus(), edc(), lePaiementAdmEnCours());
		}
		catch (ScolarixFwkException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
			return null;
		}
		setIsPaiementAjoutEnCours(false);
		dgPaiementsUpdateData();
		return null;
	}

	// Modification de paiement............................

	public WOComponent modifierLePaiement() {
		if (dgPaiements().selectedObject() != null) {
			setLePaiementAdmEnCours((EOPaiementAdm) dgPaiements().selectedObject());
			setIsPaiementModifEnCours(true);
		}
		return null;
	}

	public WOActionResults modifierLePaiementValide() {
		try {
			ProcessUpdPaiementAdm.enregistrer(cktlSession().dataBus(), edc(), lePaiementAdmEnCours());
		}
		catch (ScolarixFwkException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
			return null;
		}
		dgPaiementsUpdateData();
		setIsPaiementModifEnCours(false);
		return null;
	}

	// Suppression de paiement............................

	public WOActionResults supprimerLePaiement() {
		try {
			if (dgPaiements().selectedObject() != null) {
				try {
					ProcessDelPaiementAdm.enregistrer(cktlSession().dataBus(), edc(), (EOPaiementAdm) dgPaiements().selectedObject());
				}
				catch (ScolarixFwkException e) {
					mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
					return null;
				}
				dgPaiementsUpdateData();
			}
		}
		catch (EtudiantException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
			return null;
		}
		return null;
	}

	// Default getters / setters .................................

	public EOPaiementAdm unPaiementAdm() {
		return unPaiementAdm;
	}

	public void setUnPaiementAdm(EOPaiementAdm unPaiementAdm) {
		this.unPaiementAdm = unPaiementAdm;
	}

	public EOPaiementAdm lePaiementAdmEnCours() {
		return lePaiementAdmEnCours;
	}

	// lecteur de cheque...

	protected void setCheque(EOEditingContext ec, Hashtable<String, String> cheque) {
		if (lePaiementAdmEnCours() != null && cheque != null) {
			lePaiementAdmEnCours().setToRepartitionCompteRelationship(
					EORepartitionCompte.fetchByKeyValue(ec, EORepartitionCompte.RCPT_CODE_KEY, cheque.get("typeCheque")));
			setCodeBanque(cheque.get("codeBanque"));
			lePaiementAdmEnCours().setToBanqueRelationship(EOBanque.fetchByKeyValue(ec, EOBanque.BANQ_CODE_KEY, codeBanque()));
			lePaiementAdmEnCours().setPadmNumeroCheque(cheque.get("numCheque"));
			lePaiementAdmEnCours().setPadmNumeroCompte(cheque.get("numCompte"));
		}
	}

	public NSArray<EOBanque> banques() {  
		NSArray<EOBanque> banques = null;  
		String filtre = codeBanque();  
		if (!StringCtrl.isEmpty(filtre)) {  
			banques = FinderBanque.getBanquesFiltre(edc(), filtre);  
		}  
		return banques;  
	}  
	
	public boolean hasBanque() {
		if (lePaiementAdmEnCours().toBanque() == null) {
			return false;
		} else {
			return true;
		}
	}
}