/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.cocktail.scolarix.serveur.impression.PrintFactory;
import org.cocktail.scolarix.serveur.metier.eos.EOGarnucheParametres;
import org.cocktail.scolarix.serveur.metier.eos.EOVSituationDetail;
import org.cocktail.scolarix.serveur.metier.eos.EOVSituationGrhum;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXS;

public class Editions_Cadre_T extends ModuleDossierAdministratif {

	private boolean isSituationOpened = false, isSituationDetailOpened = false;
	private WODisplayGroup dgVSituationGrhum, dgVSituationDetail;
	private EOVSituationGrhum uneVSituationGrhum;
	private EOVSituationDetail uneVSituationDetail;

	public Editions_Cadre_T(WOContext context) {
		super(context);
		reset();
	}

	public void reset() {
		if (etudiant() != null) {
			dgVSituationGrhumUpdateData();
			dgVSituationDetailUpdateData();
		}
	}

	public WODisplayGroup dgVSituationGrhum() {
		if (dgVSituationGrhum == null) {
			dgVSituationGrhum = new WODisplayGroup();
			dgVSituationGrhum.setSelectsFirstObjectAfterFetch(false);
			dgVSituationGrhum.setSortOrderings(ERXS.ascs(EOVSituationGrhum.ANNEE_SCOLAIREN_KEY));
		}
		return dgVSituationGrhum;
	}

	public void dgVSituationGrhumUpdateData() {
		dgVSituationGrhum().setObjectArray(EOVSituationGrhum.fetchAll(edc(), EOVSituationGrhum.NUMERO_ETUDIANT_KEY, etudiant().numero(), null));
	}

	public WODisplayGroup dgVSituationDetail() {
		if (dgVSituationDetail == null) {
			dgVSituationDetail = new WODisplayGroup();
			dgVSituationDetail.setSelectsFirstObjectAfterFetch(false);
			dgVSituationDetail.setSortOrderings(ERXS.ascs(EOVSituationDetail.ANNEE_SCOLAIREN_KEY, EOVSituationDetail.NIVEAU_SEMESTRE_KEY));
		}
		return dgVSituationDetail;
	}

	public void dgVSituationDetailUpdateData() {
		dgVSituationDetail().setObjectArray(EOVSituationDetail.fetchAll(edc(), EOVSituationDetail.NUMERO_ETUDIANT_KEY, etudiant().numero(), null));
	}

	private void closeAll() {
		setSituationOpened(false);
		setSituationDetailOpened(false);
	}

	public WOActionResults situation() {
		closeAll();
		setSituationOpened(true);
		return null;
	}

	public WOActionResults situationDetail() {
		closeAll();
		setSituationDetailOpened(true);
		return null;
	}

	public WOActionResults printDossier() {
		try {
			String cRne = etudiant().rne().cRne();
			Integer etudNumero = etudiant().numero();
			NSData data = PrintFactory.printDossierInscription(mySession(), cRne, etudNumero, anneeScolaire());
			String fileName = "dossier_" + anneeScolaire().intValue() + "_" + cRne + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer le dossier " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		}
		catch (Throwable e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	public WOActionResults printCertificats() {
		try {
			String cRne = etudiant().rne().cRne();
			Integer histNumero = historique().histNumero();
			NSData data = PrintFactory.printCertificats(mySession(), cRne, histNumero, historique().getDateDerniereInscription(), getDateEffetInsc(),
					getDateEffetCouverture(), historique().getCotisationSecuMontant(), historique().isCumulatif() ? "CUMULATIF" : "",
					getSignaturePersId());
			String fileName = "dossier_certificats_" + anneeScolaire().intValue() + "_" + cRne + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer les certificats " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		}
		catch (Throwable e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	public WOActionResults printCertificatsDuplicata() {
		try {
			String cRne = etudiant().rne().cRne();
			Integer histNumero = historique().histNumero();
			NSData data = PrintFactory.printCertificatsDuplicata(mySession(), cRne, histNumero, historique().getDateDerniereInscription(),
					getDateEffetInsc(), getDateEffetCouverture(), historique().getCotisationSecuMontant(), historique().isCumulatif() ? "CUMULATIF"
							: "", getSignaturePersId());
			String fileName = "dossier_certificats_duplicata_" + anneeScolaire().intValue() + "_" + cRne + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer le duplicata de certificats " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		}
		catch (Throwable e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	private Date getDateEffetInsc() {
		Date dateEffetInsc = new Date();
		String dateEffetInscConfig = myApp().config().stringForKey("DATE_EFFET_INSCRIPTION_" + etudiant().rne().cRne());
		if (dateEffetInscConfig == null) {
			dateEffetInscConfig = myApp().config().stringForKey("DATE_EFFET_INSCRIPTION");
		}
		if (dateEffetInscConfig != null) {
			try {
				dateEffetInsc = new SimpleDateFormat("dd/MM/yyyy").parse(dateEffetInscConfig + "/" + anneeScolaire().intValue());
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return dateEffetInsc;
	}

	private Date getDateEffetCouverture() {
		Date dateEffetCouv = new Date();
		String dateEffetCouvConfig = myApp().config().stringForKey("DATE_EFFET_COUVERTURE_" + etudiant().rne().cRne());
		if (dateEffetCouvConfig == null) {
			dateEffetCouvConfig = myApp().config().stringForKey("DATE_EFFET_COUVERTURE");
		}
		if (dateEffetCouvConfig != null) {
			try {
				dateEffetCouv = new SimpleDateFormat("dd/MM/yyyy").parse(dateEffetCouvConfig + "/" + anneeScolaire().intValue());
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return dateEffetCouv;
	}

	private Integer getSignaturePersId() {
		Integer signaturePersId = null;
		int signaturePersIdConfig = myApp().config().intForKey("SIGNATURE_CERTIFICATS_PERSID_" + etudiant().rne().cRne());
		if (signaturePersIdConfig == -1) {
			signaturePersIdConfig = myApp().config().intForKey("SIGNATURE_CERTIFICATS_PERSID");
		}
		if (signaturePersIdConfig != -1) {
			signaturePersId = new Integer(signaturePersIdConfig);
		}
		return signaturePersId;
	}

	public WOActionResults printSituation() {
		try {
			Integer etudNumero = etudiant().numero();
			String cRne = etudiant().rne().cRne();
			StringBuffer idiplNumeros = new StringBuffer();
			List<EOVSituationGrhum> objects = new NSMutableArray<EOVSituationGrhum>();
			for (EOVSituationGrhum vSituationGrhum : (NSArray<EOVSituationGrhum>) dgVSituationGrhum().displayedObjects()) {
				if (vSituationGrhum.isEditionChecked()) {
					objects.add(vSituationGrhum);
					if (idiplNumeros.length() > 0) {
						idiplNumeros.append(",");
					}
					idiplNumeros.append(vSituationGrhum.primaryKey());
				}
			}

			NSData data = PrintFactory.printSituation(mySession(), cRne, etudNumero, idiplNumeros.toString(), objects,
					EOGarnucheParametres.getGarnucheUniversite(edc()), EOGarnucheParametres.getGarnucheVille(edc()),
					EOGarnucheParametres.getGarnucheServiceScolAffichage(edc(), cRne), EOGarnucheParametres.getGarnucheResponsableScol(edc(), cRne),
					EOGarnucheParametres.getGarnucheResponsabiliteScol(edc(), cRne));
			String fileName = "dossier_situation_" + anneeScolaire().intValue() + "_" + cRne + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer la situation " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		}
		catch (Throwable e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	public WOActionResults printSituationDetail() {
		try {
			Integer etudNumero = etudiant().numero();
			String cRne = etudiant().rne().cRne();
			StringBuffer idiplNumeros = new StringBuffer();
			List<EOVSituationDetail> objects = new NSMutableArray<EOVSituationDetail>();
			for (EOVSituationDetail vSituationDetail : (NSArray<EOVSituationDetail>) dgVSituationDetail().displayedObjects()) {
				if (vSituationDetail.isEditionChecked()) {
					objects.add(vSituationDetail);
					if (idiplNumeros.length() > 0) {
						idiplNumeros.append(",");
					}
					idiplNumeros.append("(" + vSituationDetail.idiplNumero() + ", '" + vSituationDetail.niveauSemestre() + "')");
				}
			}

			NSData data = PrintFactory.printSituationDetail(mySession(), cRne, etudNumero, idiplNumeros.toString(), objects,
					EOGarnucheParametres.getGarnucheUniversite(edc()), EOGarnucheParametres.getGarnucheVille(edc()),
					EOGarnucheParametres.getGarnucheServiceScolAffichage(edc(), cRne), EOGarnucheParametres.getGarnucheResponsableScol(edc(), cRne),
					EOGarnucheParametres.getGarnucheResponsabiliteScol(edc(), cRne));
			String fileName = "dossier_situation_detail_" + anneeScolaire().intValue() + "_" + cRne + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer le détail cursus LMD " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		}
		catch (Throwable e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	public boolean enabled() {
		Boolean disabled = super.disabled();
		boolean enabled = (disabled == null ? false : disabled.booleanValue());
		return enabled && (etudiant() != null && etudiant().numero() != null && (etudiant().isInscription() || etudiant().isReInscription()));
	}

	public boolean isSituationOpened() {
		return isSituationOpened;
	}

	public void setSituationOpened(boolean isSituationOpened) {
		this.isSituationOpened = isSituationOpened;
	}

	public boolean isSituationDetailOpened() {
		return isSituationDetailOpened;
	}

	public void setSituationDetailOpened(boolean isSituationDetailOpened) {
		this.isSituationDetailOpened = isSituationDetailOpened;
	}

	public EOVSituationGrhum uneVSituationGrhum() {
		return uneVSituationGrhum;
	}

	public void setUneVSituationGrhum(EOVSituationGrhum uneVSituationGrhum) {
		this.uneVSituationGrhum = uneVSituationGrhum;
	}

	public EOVSituationDetail uneVSituationDetail() {
		return uneVSituationDetail;
	}

	public void setUneVSituationDetail(EOVSituationDetail uneVSituationDetail) {
		this.uneVSituationDetail = uneVSituationDetail;
	}

}