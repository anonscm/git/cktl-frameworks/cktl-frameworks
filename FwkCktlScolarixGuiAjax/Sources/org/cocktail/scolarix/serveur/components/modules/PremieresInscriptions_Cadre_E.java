/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import org.cocktail.scolarix.serveur.components.controleurs.CtrlPremieresInscriptions;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EORne;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

public class PremieresInscriptions_Cadre_E extends ModuleDossierAdministratif {
	
    private static final long serialVersionUID = 5511450938010020027L;
	public CtrlPremieresInscriptions ctrl;
	private String codeUniversite;
	public EORne uneUniversite;

	public PremieresInscriptions_Cadre_E(WOContext context) {
		super(context);
		ctrl = new CtrlPremieresInscriptions(this, session().defaultEditingContext());
	}
	
	public String getContainerAnneeEnseignementSuperieurId() {
		return getComponentId() + "_containerAnneeEnseignementSuperieurId";
	}
	
	public String getContainerAnneeUniversiteFrancaiseId() {
		return getComponentId() + "_containerAnneeUniversiteFrancaiseId";
	}
	
	public String getContainerUniversiteId() {
		return getComponentId() + "_containerUniversiteId";
	}
	
	public String getContainerAnneeUniversiteLocaleId() {
		return getComponentId() + "_containerAnneeUniversiteLocaleId";
	}

	public void reset() {
		setCodeUniversite(null);
	}

	public WOComponent enregistrer() {
		super.enregistrer();
		return null;
	}

	public void validate() throws EtudiantException {
		IEtudiant etudiant = etudiant();
		if (etudiant == null) {
			throw new EtudiantException("Pas d'étudiant !!!");
		}
		etudiant.validatePremiereInscription();
	}

	public String codeUniversite() {
		if (codeUniversite == null) {
			if (etudiant() != null && etudiant().universitePremiereInscription() != null) {
				setCodeUniversite(etudiant().universitePremiereInscription().cRne());
			}
		}
		return codeUniversite;
	}

	public void setCodeUniversite(String codeUniversite) {
		this.codeUniversite = codeUniversite;
	}

	public EORne laUniversite() {
		return etudiant().universitePremiereInscription();
	}

	public void setLaUniversite(EORne laUniversite) {
		etudiant().setUniversitePremiereInscription(laUniversite);
		if (laUniversite != null) {
			setCodeUniversite(laUniversite.cRne());
		}
	}

}