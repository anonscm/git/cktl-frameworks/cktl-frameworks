/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.scolarix.serveur.components.controleurs.CtrlDiplomes;
import org.cocktail.scolarix.serveur.components.dataprovider.DepartementProvider;
import org.cocktail.scolarix.serveur.components.dataprovider.EtablissementProvider;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOCumulatif;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;
import org.cocktail.scolarix.serveur.metier.eos.EOLangue;
import org.cocktail.scolarix.serveur.metier.eos.EOMention;
import org.cocktail.scolarix.serveur.metier.eos.EOResultat;
import org.cocktail.scolarix.serveur.metier.eos.EORne;
import org.cocktail.scolarix.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeEchange;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription;
import org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat;
import org.cocktail.scolarix.serveur.process.ProcessDelInscDipl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

// Generated by the WOLips Templateengine Plug-in at 27 fevr. 2008 10:26:22
public class Diplomes_Cadre_C extends ModuleDossierAdministratif {
	public CtrlDiplomes ctrl;
	private EOScolFormationHabilitation uneFormationEnvisageable;
	private EOScolFormationHabilitation uneAutreFormationEnvisageable;
	private EOScolFormationHabilitation uneAutreFormationEnvisageableSelection;
	private EOVWebInscriptionResultat uneFormationEnCours;
	private EOInscDipl uneFormationEnvisagee, uneFormationEnvisageeAModifier;
	private EOLangue uneLangueVivante;
	private EOTypeInscription unTypeInscription;
	private EOResultat unResultat;
	private EOMention uneMention;
	private EOTypeEchange unTypeEchange;
	private EOCumulatif unCumulatif;

	private String codeUniversite;
	private EORne uneUniversite, laUniversite;

	private String codeDepartement;
	private EODepartement unDepartement, leDepartement;

	private EOInscDipl uneFormationEnvisageeDetaillee;

	private boolean isCumulatifOpened = false;
	
	private CktlAjaxSelect2RemoteDataProvider etablissementDataProvider;
	private CktlAjaxSelect2RemoteDataProvider departementDataProvider;

	public Diplomes_Cadre_C(WOContext context) {
		super(context);
		ctrl = new CtrlDiplomes(this, session().defaultEditingContext());
		etablissementDataProvider = new EtablissementProvider(edc()) {
            
            @Override
            public void setSelectedRne(EORne selectedRne) {
                setLaUniversite(selectedRne);
            }
            
            @Override
            public EORne getSelectedRne() {
                return laUniversite();
            }
        };
        
        departementDataProvider = new DepartementProvider(edc()) {
            @Override
            public EODepartement getSelectedDepartement() {
                return leDepartement();
            }
            
            @Override
            public void setSelectedDepartement(EODepartement departement) {
                setLeDepartement(departement);
            }
        };
	}
	
	public CktlAjaxSelect2RemoteDataProvider getEtablissementDataProvider() {
        return etablissementDataProvider;
    }
	
	public CktlAjaxSelect2RemoteDataProvider getDepartementDataProvider() {
        return departementDataProvider;
    }
	
	public String getContainerDiplomesEntierId() {
		return getComponentId() + "_containerDiplomesEntierId";
	}
	
	public String getContainerFormationsEnvisageablesId() {
		return getComponentId() + "_containerFormationsEnvisageablesId";
	}
	
	public String getContainerFormationsEnvisageesId() {
		return getComponentId() + "_containerFormationsEnvisageesId";
	}
	
	public String getContainerQbeDiplomesId() {
		return getComponentId() + "_containerQbeDiplomesId";
	}
	
	public String getContainerDetailFormationEnvisageeId() {
		return getComponentId() + "_containerDetailFormationEnvisageeId";
	}
	
	public String getContainerCheckBoxesRedReoId() {
		return getComponentId() + "_containerCheckBoxesRedReoId";
	}
	
	public String getContainerTypeEchangeId() {
		return getComponentId() + "_containerTypeEchangeId";
	}
	
	public String getContainerUniversiteId() {
		return getComponentId() + "_containerUniversiteId";
	}
	
	public String getContainerDepartementId() {
		return getComponentId() + "_containerDepartementId";
	}
	
	public void reset() {
		if (ctrl != null) {
			ctrl.setFormationsEnvisageables(null);
			ctrl.setIsAfficherFormationsEnvisageables(false);
		}
		setUneFormationEnvisageeAModifier(null);
		setCodeUniversite(null);
		setCodeDepartement(null);
		setCumulatifOpened(false);
	}

	public void validate() throws EtudiantException {
		IEtudiant etudiant = etudiant();
		if (etudiant == null) {
			throw new EtudiantException("Pas d'étudiant !!!");
		}
		etudiant.validateDiplomes();
	}

	public Integer anneeScolairePrecedente() {
		NSArray<EOVWebInscriptionResultat> a = ctrl.formationsEnCours();
		if (a != null && a.count() > 0) {
			return a.objectAtIndex(0).toFwkScolarite_ScolFormationAnnee().fannDebut();
		}
		return null;
	}

	public String idiplNumero() {
		if (uneFormationEnvisagee() != null) {
			return "idiplNumero: " + uneFormationEnvisagee().idiplNumero();
		}
		return null;
	}

	public EOScolFormationHabilitation uneFormationEnvisageable() {
		return uneFormationEnvisageable;
	}

	public void setUneFormationEnvisageable(EOScolFormationHabilitation uneFormationEnvisageable) {
		this.uneFormationEnvisageable = uneFormationEnvisageable;
	}

	public void onChangeTypeInscription() {
		ctrl.setTypeEchanges(null);
		if (uneFormationEnvisagee().idiplNumero() == null) {
			if (isTypeInscriptionEchange()) {
				uneFormationEnvisagee().setIdiplDiplomable(new Integer(0));
			}
		}
	}

	public EOScolFormationHabilitation uneAutreFormationEnvisageable() {
		return uneAutreFormationEnvisageable;
	}

	public void setUneAutreFormationEnvisageable(EOScolFormationHabilitation uneAutreFormationEnvisageable) {
		this.uneAutreFormationEnvisageable = uneAutreFormationEnvisageable;
	}

	public EOScolFormationHabilitation uneAutreFormationEnvisageableSelection() {
		return uneAutreFormationEnvisageableSelection;
	}

	public void setUneAutreFormationEnvisageableSelection(EOScolFormationHabilitation uneAutreFormationEnvisageableSelection) {
		this.uneAutreFormationEnvisageableSelection = uneAutreFormationEnvisageableSelection;
	}

	public EOVWebInscriptionResultat uneFormationEnCours() {
		return uneFormationEnCours;
	}

	public void setUneFormationEnCours(EOVWebInscriptionResultat uneFormationEnCours) {
		this.uneFormationEnCours = uneFormationEnCours;
	}

	public EOInscDipl uneFormationEnvisagee() {
		return uneFormationEnvisagee;
	}

	public void setUneFormationEnvisagee(EOInscDipl uneFormationEnvisagee) {
		this.uneFormationEnvisagee = uneFormationEnvisagee;
	}

	public WOActionResults ajouterUneFormationEnvisageable() {
		try {
			if (uneFormationEnvisageable() != null) {
				if (uneFormationEnvisageeAModifier() != null) {
					Integer idiplNumero = uneFormationEnvisageeAModifier().idiplNumero();
					etudiant().removeFormationEnvisagee(uneFormationEnvisageeAModifier());
					etudiant().addFormationEnvisagee(uneFormationEnvisageable(), idiplNumero);
					setUneFormationEnvisageeAModifier(null);
				}
				else {
					etudiant().addFormationEnvisagee(uneFormationEnvisageable(), null);
				}
				ctrl.setFormationsEnvisageables(null);
				ctrl.setIsAfficherFormationsEnvisageables(false);
			}
		}
		catch (EtudiantException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
		}
		return null;
	}

	public void ouvrirAutreFormationEnvisageableEnReinscription() {
		ctrl.setIsOuvrirAutreFormationEnvisageableEnReinscription(true);
		ctrl.setFormationsEnvisageables(null);
	}

	public void fermerAutreFormationEnvisageableEnReinscription() {
		ctrl.setIsOuvrirAutreFormationEnvisageableEnReinscription(false);
		ctrl.setFormationsEnvisageables(null);
		ctrl.setGrade(null);
		ctrl.setLibelle(null);
		ctrl.setIsTypeLmdSelected(true);
	}

	public WOActionResults ajouterUneAutreFormationEnvisageable() {
		try {
			etudiant().addFormationEnvisagee(uneAutreFormationEnvisageableSelection(), null);
		}
		catch (EtudiantException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
		}
		return null;
	}

	public void ouvrirDetailUneFormationEnvisagee() {
		setUneFormationEnvisageeDetaillee(uneFormationEnvisagee());
	}

	public void fermerDetailUneFormationEnvisagee() {
		setUneFormationEnvisageeDetaillee(null);
	}

	public void swapDetailUneFormationEnvisagee() {
		if (isDetailFormationEnvisageeOuvert()) {
			fermerDetailUneFormationEnvisagee();
		}
		else {
			ouvrirDetailUneFormationEnvisagee();
		}
	}

	public boolean isDetailFormationEnvisageeOuvert() {
		if (uneFormationEnvisagee() == null || uneFormationEnvisageeDetaillee() == null) {
			return false;
		}
		return uneFormationEnvisagee().equals(uneFormationEnvisageeDetaillee());
	}

	public WOActionResults modifierUneFormationEnvisagee() {
		try {
			setUneFormationEnvisageeAModifier(uneFormationEnvisagee());
			ctrl.setFormationsEnvisageables(null);
			ctrl.setIsAfficherFormationsEnvisageables(true);
		}
		catch (EtudiantException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
		}
		return null;
	}

	public WOActionResults annulerModifierUneFormationEnvisagee() {
		try {
			setUneFormationEnvisageeAModifier(null);
			ctrl.setIsAfficherFormationsEnvisageables(false);
		}
		catch (EtudiantException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
		}
		return null;
	}

	public String onClickBeforeSupprimerFormationEnvisagee() {
		if (etudiant().isInscription() || etudiant().isReInscription()) {
			if (uneFormationEnvisagee().idiplNumero() != null) {
				return "confirm('Vous allez supprimer définitivement et IMMEDIATEMENT (sans avoir besoin de faire enregistrer le dossier) cette inscription... OK ?')";
			}
		}
		return null;
	}

	public WOActionResults supprimerUneFormationEnvisagee() {
		try {
			if (etudiant().isInscription() || etudiant().isReInscription()) {
				if (uneFormationEnvisagee().idiplNumero() != null) {
					try {
						ProcessDelInscDipl.enregistrer(cktlSession().dataBus(), ctrl.edc, uneFormationEnvisagee());
					}
					catch (ScolarixFwkException e) {
						mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
						return null;
					}
				}
			}
			etudiant().removeFormationEnvisagee(uneFormationEnvisagee());
			ctrl.setFormationsEnvisageables(null);
		}
		catch (EtudiantException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
		}
		return null;
	}

	public EOLangue uneLangueVivante() {
		return uneLangueVivante;
	}

	public void setUneLangueVivante(EOLangue uneLangueVivante) {
		this.uneLangueVivante = uneLangueVivante;
	}

	public String styleForTypeAction() {
		String styleForTypeAction = "background-color:";
		if (uneFormationEnvisageable.typeAction() != null
				&& uneFormationEnvisageable.typeAction().equals(EOScolFormationHabilitation.TYPE_ACTION_PROGRESSION)) {
			styleForTypeAction += "#00FF00";
		}
		else
			if (uneFormationEnvisageable.typeAction() != null
					&& uneFormationEnvisageable.typeAction().equals(EOScolFormationHabilitation.TYPE_ACTION_REDOUBLEMENT)) {
				styleForTypeAction += "#FF0000";
			}
			else {
				styleForTypeAction = "";
			}
		return styleForTypeAction;
	}

	public boolean isAjouterFormationEnvisageablePossible() {
		boolean isAjouterFormationEnvisageablePossible = false;

		if (disabled() == null || disabled().booleanValue() == false) {
			if (!isModeConsultation()
					&& (uneFormationEnvisageable().isSelectionnable() == null || uneFormationEnvisageable().isSelectionnable().booleanValue())) {
				isAjouterFormationEnvisageablePossible = true;
			}
		}
		return isAjouterFormationEnvisageablePossible;
	}

	public boolean isModifierFormationEnvisageePossible() {
		if (disabled() != null && disabled().booleanValue() == true) {
			return false;
		}
		if (isModeConsultation()) {
			return false;
		}
		if (etudiant().isInscription() || etudiant().isReInscription()) {
			if (uneFormationEnvisagee().idiplNumero() == null) {
				return false;
			}
		}
		if (isModifierFormationEnvisageeEnCours()) {
			return false;
		}
		if (uneFormationEnvisagee().isSelectionnable() == null || uneFormationEnvisagee().isSelectionnable().booleanValue()) {
			return true;
		}
		return false;
	}

	public boolean isModifierFormationEnvisageeEnCours() {
		return (uneFormationEnvisageeAModifier() != null && uneFormationEnvisageeAModifier().equals(uneFormationEnvisagee()));
	}

	public boolean isSupprimerFormationEnvisageePossible() {
		if (disabled() != null && disabled().booleanValue() == true) {
			return false;
		}
		if (isModeConsultation()) {
			return false;
		}
		if (etudiant().isInscription() || etudiant().isReInscription()) {
			return true;
		}
		if (uneFormationEnvisagee().isSelectionnable() == null || uneFormationEnvisagee().isSelectionnable().booleanValue()) {
			return true;
		}
		return false;
	}

	public EOInscDipl uneFormationEnvisageeDetaillee() {
		return uneFormationEnvisageeDetaillee;
	}

	public void setUneFormationEnvisageeDetaillee(EOInscDipl uneFormationEnvisageeDetaillee) {
		this.uneFormationEnvisageeDetaillee = uneFormationEnvisageeDetaillee;
	}

	public EOTypeInscription unTypeInscription() {
		return unTypeInscription;
	}

	public void setUnTypeInscription(EOTypeInscription unTypeInscription) {
		this.unTypeInscription = unTypeInscription;
	}

	public boolean isTypeInscriptionEchange() {
		return uneFormationEnvisagee().toTypeInscription() != null
				&& uneFormationEnvisagee().toTypeInscription().idiplTypeInscription()
						.equals(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_ECHANGE_INTERNATIONAL);
	}

	public Boolean isRedoublement() {
		Boolean isRedoublement = Boolean.FALSE;
		if (historique().histRedouble() != null && historique().histRedouble().intValue() == EOHistorique.HIST_REDOUBLE_REDOUBLEMENT) {
			isRedoublement = Boolean.TRUE;
		}
		return isRedoublement;
	}

	public void setIsRedoublement(Boolean isRedoublement) {
		if (isRedoublement != null && isRedoublement.booleanValue()) {
			historique().setHistRedouble(EOHistorique.HIST_REDOUBLE_REDOUBLEMENT);
		}
		else {
			historique().setHistRedouble(EOHistorique.HIST_REDOUBLE_RIEN);
		}
	}

	public Boolean isReorientation() {
		Boolean isReorientation = Boolean.FALSE;
		if (historique().histRedouble() != null && historique().histRedouble().intValue() == EOHistorique.HIST_REDOUBLE_REORIENTATION) {
			isReorientation = Boolean.TRUE;
		}
		return isReorientation;
	}

	public void setIsReorientation(Boolean isReorientation) {
		if (isReorientation != null && isReorientation.booleanValue()) {
			historique().setHistRedouble(EOHistorique.HIST_REDOUBLE_REORIENTATION);
		}
		else {
			historique().setHistRedouble(EOHistorique.HIST_REDOUBLE_RIEN);
		}
	}

	public Boolean isFormationDiplomante() {
		Boolean isFormationDiplomante = Boolean.FALSE;
		if (uneFormationEnvisagee().idiplDiplomable() != null && uneFormationEnvisagee().idiplDiplomable().intValue() == 1) {
			isFormationDiplomante = Boolean.TRUE;
		}
		return isFormationDiplomante;
	}

	public void setIsFormationDiplomante(Boolean isFormationDiplomante) {
		if (isFormationDiplomante != null && isFormationDiplomante.booleanValue()) {
			uneFormationEnvisagee().setIdiplDiplomable(new Integer(1));
		}
		else {
			uneFormationEnvisagee().setIdiplDiplomable(new Integer(0));
		}
	}

	public EOResultat unResultat() {
		return unResultat;
	}

	public void setUnResultat(EOResultat unResultat) {
		this.unResultat = unResultat;
	}

	public String unResultatDisplayString() {
		if (unResultat() == null) {
			return null;
		}
		return unResultat().resCode() + " - " + unResultat().resLibelle();
	}

	public String unTypeEchangeDisplayString() {
		if (unTypeEchange() == null) {
			return null;
		}
		String techLibelle = unTypeEchange().techLibelle();
		if (techLibelle != null && techLibelle.length() > 20) {
			techLibelle = techLibelle.substring(0, 20);
		}
		return unTypeEchange().techCode() + " - " + techLibelle;
	}

	public EOMention uneMention() {
		return uneMention;
	}

	public void setUneMention(EOMention uneMention) {
		this.uneMention = uneMention;
	}

	public EOTypeEchange unTypeEchange() {
		return unTypeEchange;
	}

	public void setUnTypeEchange(EOTypeEchange unTypeEchange) {
		this.unTypeEchange = unTypeEchange;
	}

	public Boolean isFLE() {
		Boolean isFLE = Boolean.FALSE;
		if (uneFormationEnvisagee() != null && uneFormationEnvisagee().mstaCode() != null && uneFormationEnvisagee().mstaCode().intValue() == 1) {
			isFLE = Boolean.TRUE;
		}
		return isFLE;
	}

	public void setIsFLE(Boolean isFLE) {
		if (isFLE != null && isFLE.booleanValue()) {
			if (uneFormationEnvisagee() != null) {
				uneFormationEnvisagee().setMstaCode(new Integer(1));
			}
		}
		else {
			if (uneFormationEnvisagee() != null) {
				uneFormationEnvisagee().setMstaCode(new Integer(0));
			}
		}
	}

	public void swapCumulatif() {
		setCumulatifOpened(!isCumulatifOpened());
	}

	public boolean isCumulatifEnabled() {
		return uneFormationEnvisagee() != null && uneFormationEnvisagee().toTypeInscription() != null
				&& uneFormationEnvisagee().toTypeInscription().idiplTypeInscription().equals(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_CUMULATIF);
	}

	public boolean isCumulatifOpened() {
		return isCumulatifEnabled() && isCumulatifOpened;
	}

	public void setCumulatifOpened(boolean isCumulatifOpened) {
		this.isCumulatifOpened = isCumulatifOpened;
	}

	public EOCumulatif getUnCumulatif() {
		return unCumulatif;
	}

	public void setUnCumulatif(EOCumulatif unCumulatif) {
		this.unCumulatif = unCumulatif;
	}

	public String codeUniversite() {
		if (codeUniversite == null) {
			if (historique() != null && historique().toRneAutEtab() != null) {
				setCodeUniversite(historique().toRneAutEtab().cRne());
			}
		}
		return codeUniversite;
	}

	public void setCodeUniversite(String codeUniversite) {
		this.codeUniversite = codeUniversite;
	}

	public EORne uneUniversite() {
		return uneUniversite;
	}

	public void setUneUniversite(EORne uneUniversite) {
		this.uneUniversite = uneUniversite;
	}

	public EORne laUniversite() {
		return laUniversite;
	}

	public void setLaUniversite(EORne laUniversite) {
		this.laUniversite = laUniversite;
		historique().setToRneAutEtabRelationship(laUniversite);
		if (laUniversite != null) {
			setCodeUniversite(laUniversite.cRne());
		}
	}

	public String codeDepartement() {
		if (codeDepartement == null) {
			if (historique() != null && historique().toFwkpers_Departement_AutEtab() != null) {
				setCodeDepartement(historique().toFwkpers_Departement_AutEtab().cDepartement());
			}
		}
		return codeDepartement;
	}

	public void setCodeDepartement(String codeDepartement) {
		this.codeDepartement = codeDepartement;
	}

	public EODepartement unDepartement() {
		return unDepartement;
	}

	public void setUnDepartement(EODepartement unDepartement) {
		this.unDepartement = unDepartement;
	}

	public EODepartement leDepartement() {
		return leDepartement;
	}

	public void setLeDepartement(EODepartement leDepartement) {
		this.leDepartement = leDepartement;
		historique().setToFwkpers_Departement_AutEtabRelationship(leDepartement);
		if (leDepartement != null) {
			setCodeDepartement(leDepartement.cDepartement());
		}
	}

	public EOInscDipl uneFormationEnvisageeAModifier() {
		return uneFormationEnvisageeAModifier;
	}

	public void setUneFormationEnvisageeAModifier(EOInscDipl uneFormationEnvisageeAModifier) {
		this.uneFormationEnvisageeAModifier = uneFormationEnvisageeAModifier;
	}

}