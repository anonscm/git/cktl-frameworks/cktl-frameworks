/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOUtilisateur;
import org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication;
import org.cocktail.scolarix.serveur.ui.EOGarnucheCadreDocumentation;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation._NSStringUtilities;

public abstract class ModuleDossierAdministratif extends CktlAjaxWOComponent {

	private IEtudiant etudiant = null;
	private EOHistorique historique = null;
	private Integer anneeScolaire = null;
	private Boolean disabled = null;
	private EOGarnucheCadreApplication cadre = null;
	Boolean isAfficherInfo = null;

	public ModuleDossierAdministratif(WOContext context) {
		super(context);
	}

	public void awake() {
		super.awake();
		IEtudiant etudiantBinding = (IEtudiant) valueForBinding("etudiant");
		Integer anneeScolaireBinding = (Integer) valueForBinding("anneeScolaire");
		boolean dossierDifferent = false;
		if (etudiantBinding == null || !etudiantBinding.equals(etudiant) || anneeScolaireBinding == null
				|| !anneeScolaireBinding.equals(anneeScolaire)) {
			dossierDifferent = true;
		}

		etudiant = etudiantBinding;
		anneeScolaire = anneeScolaireBinding;
		if (etudiant != null) {
			historique = etudiant.historique(anneeScolaire);
		}

		NSMutableDictionary<String, WOComponent> componentCache = (NSMutableDictionary<String, WOComponent>) valueForBinding("componentCache");
		if (componentCache != null) {
			componentCache.setObjectForKey(this, _NSStringUtilities.lastComponentInString(this.name(), '.'));
			setValueForBinding(componentCache, "componentCache");
		}

		if (dossierDifferent) {
			reset();
		}
		setEtudiant(etudiant);
	}

	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public boolean isBusy() {
		return false;
	}

	public void setEtudiant(IEtudiant etudiant) {
		this.etudiant = etudiant;
		if (etudiant != null) {
			historique = etudiant.historique(anneeScolaire());
		}
	}

	public IEtudiant etudiant() {
		if (etudiant == null) {
			etudiant = (IEtudiant) valueForBinding("etudiant");
		}
		return etudiant;
	}

	public EOHistorique historique() {
		if (historique == null) {
			if (etudiant() != null) {
				historique = etudiant().historique(anneeScolaire());
			}
		}
		return historique;
	}

	public EOUtilisateur utilisateur() {
		return (EOUtilisateur) valueForBinding("utilisateur");
	}

	public boolean isAdmission() {
		return etudiant() != null && etudiant().isAdmission();
	}

	public boolean isInscriptionOuReInscription() {
		return etudiant() != null && (etudiant().isInscription() || etudiant().isReInscription());
	}

	public boolean isPreInscriptionOuPreReInscription() {
		return etudiant() != null && (etudiant().isPreInscription() || etudiant().isPreReInscription());
	}

	public Boolean isModification() {
		return (Boolean) valueForBinding("isModification");
	}

	public Integer anneeScolaire() {
		if (anneeScolaire == null) {
			anneeScolaire = (Integer) valueForBinding("anneeScolaire");
		}
		return anneeScolaire;
	}

	public Boolean disabled() {
		disabled = (Boolean) valueForBinding("disabled");
		if (disabled == null) {
			disabled = Boolean.valueOf(cadre == null || cadre.cappModifiable().equalsIgnoreCase("O") == false);
		}
		return disabled;
	}

	public EOGarnucheCadreApplication cadre() {
		if (cadre == null) {
			cadre = (EOGarnucheCadreApplication) valueForBinding("cadre");
		}
		return cadre;
	}

	public WOComponent enregistrer() {
		setValueForBinding(etudiant, "etudiant");
		return null;
	}

	public String containerCadreID() {
		String containerCadreID = "Container";
		containerCadreID += _NSStringUtilities.lastComponentInString(this.name(), '.');
		return containerCadreID;
	}

	public boolean isModuleValide() {
		return true;
	}

	public void validate() throws EtudiantException {
	}

	public String disabledTFScript() {
		String disabledTFScript = "";

		if (disabled() != null && disabled().booleanValue() == true) {
			String divModuleName = _NSStringUtilities.lastComponentInString(name(), '.');
			disabledTFScript = "var form = $('" + divModuleName + "').getElementsBySelector('form').first();Form.disable(form);";
		}
		// Permet de contourner la "stylisation" d'IE pour les textfield disabled
		disabledTFScript += "var tfs=$$('input[disabled]'); tfs.each(function(tf) {tf.addClassName('disabled');});";

		return disabledTFScript;
	}

	public boolean isModeConsultation() {
		boolean isModeConsultation = false;
		if (hasBinding("modeConsultation")) {
			Boolean modeConsultation = (Boolean) valueForBinding("modeConsultation");
			isModeConsultation = modeConsultation.booleanValue();
		}
		return isModeConsultation;
	}

	public boolean isAfficherCommentaires() {
		boolean isAfficherCommentaires = !isModeConsultation() && etudiant() != null
				&& (etudiant().isPreInscription() || etudiant().isPreReInscription());
		return isAfficherCommentaires;
	}

	public String commentaires() {
		String commentaires = null;
		EOKeyValueQualifier qual = new EOKeyValueQualifier(EOGarnucheCadreDocumentation.CDOC_TYPE_KEY,
				EOQualifier.QualifierOperatorCaseInsensitiveLike, "A");
		NSArray<EOGarnucheCadreDocumentation> cadreDocumentsInfo = cadre().garnucheCadreDocumentations(qual);
		if (cadreDocumentsInfo != null && cadreDocumentsInfo.count() == 1) {
			EOGarnucheCadreDocumentation document = cadreDocumentsInfo.lastObject();
			commentaires = document.cdocTexte();
		}
		if (commentaires == null) {
			if (disabled()) {
				commentaires = "<b>Veuillez signaler les &eacute;ventuelles erreurs lors de votre inscription d&eacute;finitive.</b><BR/>";
			}
			else {
				commentaires = "<b>Veuillez rectifier les donn&eacute;es incorrectes.</b><BR/>";
			}
		}
		return commentaires;
	}

	public String infoAideHref() {
		EOKeyValueQualifier qual = new EOKeyValueQualifier(EOGarnucheCadreDocumentation.CDOC_TYPE_KEY,
				EOQualifier.QualifierOperatorCaseInsensitiveLike, "I");
		NSArray<EOGarnucheCadreDocumentation> cadreDocumentsInfo = cadre().garnucheCadreDocumentations(qual);
		if (cadreDocumentsInfo != null && cadreDocumentsInfo.count() == 1) {
			EOGarnucheCadreDocumentation document = cadreDocumentsInfo.lastObject();
			return document.cdocLien();
		}
		return null;
	}

	public String onClickAide() {
		String url = infoAideHref();
		if (url == null) {
			return null;
		}
		String onClickAide = "openWinAide('";
		String titre = _NSStringUtilities.lastComponentInString(name(), '.');
		titre = titre.substring(0, titre.indexOf('_'));
		titre = _NSStringUtilities.stringMarkingUpcaseTransitionsWithDelimiter(titre, " ");
		onClickAide += url + "','" + titre;
		onClickAide += "');";

		return onClickAide;
	}

	public String containerAideId() {
		String containerAideId = "ContainerId_";
		String titre = _NSStringUtilities.lastComponentInString(name(), '.');

		containerAideId += titre;

		return containerAideId;
	}

	public Boolean isAfficherInfo() {
		if (isAfficherInfo == null) {
			if (infoAideHref() != null) {
				isAfficherInfo = Boolean.TRUE;
			}
			else {
				isAfficherInfo = Boolean.FALSE;
			}
		}
		return isAfficherInfo;
	}

}
