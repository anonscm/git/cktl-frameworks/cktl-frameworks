/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import java.math.BigDecimal;

import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement;
import org.cocktail.scolarix.serveur.metier.eos.EODetailRembourse;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse;
import org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheance;
import org.cocktail.scolarix.serveur.metier.eos.EOPlanco;
import org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte;
import org.cocktail.scolarix.serveur.process.ProcessDelRemboursementPlus;
import org.cocktail.scolarix.serveur.process.ProcessInsRemboursement;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering;

public class Remboursements_Cadre_R extends ModuleDossierAdministratif {

	private boolean isBusy = false;

	private static final BigDecimal FORFAIT_REMB = new BigDecimal(23);
	private WODisplayGroup dgEcheances = null, dgRemboursements = null, dgDetailRemboursements = null, dgDetailPaiements = null;
	private EOPaiementEcheance uneEcheance;
	private EOEtudiantRembourse unRemb, unRembEnCours;
	private EODetailRembourse unDetailRemb;
	private EODetailPaiement unDetailPaiement;
	private EORib unRib = null;
	private boolean isAjoutEnCours = false;

	public Remboursements_Cadre_R(WOContext context) {
		super(context);
	}

	public void reset() {
		if (historique() != null) {
			dgRemboursementsUpdateData();
		}
	}

	public WODisplayGroup dgEcheances() {
		if (dgEcheances == null) {
			dgEcheances = new WODisplayGroup();
			dgEcheances.setSelectsFirstObjectAfterFetch(false);
			dgEcheances.setSortOrderings(ERXS.ascs(EOPaiementEcheance.PECHE_ORDRE_KEY));
			dgEcheancesUpdateData();
		}
		return dgEcheances;
	}

	public void dgEcheancesUpdateData() {
		if (historique().getEcheancierEnCours() != null) {
			dgEcheances().setObjectArray(historique().getEcheancierEnCours().toPaiementEcheances());
		}
		else {
			dgEcheances().setObjectArray(null);
		}
	}

	public WODisplayGroup dgRemboursements() {
		if (dgRemboursements == null) {
			dgRemboursements = new WODisplayGroup();
			dgRemboursements.setSelectsFirstObjectAfterFetch(true);
			dgRemboursements.setSortOrderings(ERXS.ascs(EOEtudiantRembourse.EREMB_NUMERO_KEY));
			dgRemboursementsUpdateData();
		}
		return dgRemboursements;
	}

	public void dgRemboursementsUpdateData() {
		dgRemboursements().setObjectArray(historique().toEtudiantRembourses());
		if (dgRemboursements().displayedObjects() != null && dgRemboursements().displayedObjects().count() > 0) {
			dgRemboursements().setSelectionIndexes(new NSArray<Integer>(Integer.valueOf(0)));
			onSelectRemboursement();
		}
	}

	public WODisplayGroup dgDetailRemboursements() {
		if (dgDetailRemboursements == null) {
			dgDetailRemboursements = new WODisplayGroup();
			dgDetailRemboursements.setSelectsFirstObjectAfterFetch(false);
		}
		return dgDetailRemboursements;
	}

	public WODisplayGroup dgDetailPaiements() {
		if (dgDetailPaiements == null) {
			dgDetailPaiements = new WODisplayGroup();
			dgDetailPaiements.setSelectsFirstObjectAfterFetch(false);
			ERXSortOrdering so1 = ERXS.asc(EODetailPaiement.TO_REPARTITION_COMPTE_KEY + "." + EORepartitionCompte.TO_PLANCO_KEY + "."
					+ EOPlanco.PCO_LIB_KEY);
			ERXSortOrdering so2 = ERXS.desc(EODetailPaiement.DPAIE_MONTANT_KEY);
			dgDetailPaiements.setSortOrderings(so1.then(so2));
		}
		return dgDetailPaiements;
	}

	public void raz() {
		unDetailPaiement.setDpaieMontantRembSaisi(null);
	}

	public void oterCentPourcent() {
		if (unDetailPaiement.dpaieMontantAvecRemboursements().compareTo(new BigDecimal(0)) == 1) {
			unDetailPaiement.setDpaieMontantRembSaisi(unDetailPaiement.dpaieMontantAvecRemboursements());
		}
	}

	public void oterForfait() {
		BigDecimal total = unDetailPaiement.dpaieMontantAvecRemboursements().subtract(FORFAIT_REMB);
		if (total.signum() == 1) {
			unDetailPaiement.setDpaieMontantRembSaisi(total);
		}
	}

	public void dgDetailPaiementsUpdateData() {
		dgDetailPaiements().setObjectArray(historique().detailPaiements(false));
	}

	public boolean hasEcheancierEnCours() {
		return historique().getEcheancierEnCours() != null;
	}

	public WOActionResults onSelectRemboursement() {
		if (dgRemboursements().selectedObject() != null) {
			dgDetailRemboursements().setObjectArray(((EOEtudiantRembourse) dgRemboursements().selectedObject()).toDetailRembourses());
		}
		else {
			dgDetailRemboursements().setObjectArray(null);
		}
		return null;
	}

	public WOActionResults ajouter() {
		dgDetailPaiementsUpdateData();
		setUnRembEnCours(historique().createToEtudiantRemboursesRelationship());
		unRembEnCours().setErembAnnee(etudiant().anneeInscriptionEnCours().toString());
		setAjoutEnCours(true);
		return null;
	}

	public WOActionResults supprimer() {
		try {
			if (dgRemboursements().selectedObject() != null) {
				ProcessDelRemboursementPlus.enregistrer(cktlSession().dataBus(), edc(), (EOEtudiantRembourse) dgRemboursements().selectedObject());
				dgRemboursementsUpdateData();
				mySession().addSimpleInfoMessage("OK", "Remboursement supprimé");
			}
		}
		catch (ScolarixFwkException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
			return null;
		}
		catch (Exception e) {
			mySession().addSimpleErrorMessage("Erreur", e);
			return null;
		}
		return null;
	}

	public WOActionResults validerCreationRemboursement() {
		try {
			NSTimestamp now = DateCtrl.now();
			for (EODetailPaiement detailPaiement : (NSArray<EODetailPaiement>) dgDetailPaiements().displayedObjects()) {
				if (detailPaiement.dpaieMontantRembSaisi() != null) {
					if (detailPaiement.dpaieMontantRembSaisi().compareTo(detailPaiement.dpaieMontantAvecRemboursements()) > 0) {
						throw new Exception("Montant à rembourser trop élevé ! (" + detailPaiement.dpaieMontantRembSaisi() + " > "
								+ detailPaiement.dpaieMontantAvecRemboursements() + ")");
					}
					if (unRembEnCours().toVEtablissementScolarite() == null) {
						unRembEnCours().setToVEtablissementScolariteRelationship(detailPaiement.toVEtablissementScolarite());
					}
					EODetailRembourse detailRembourse = unRembEnCours().createToDetailRemboursesRelationship();
					detailRembourse.setToInscDiplRelationship(detailPaiement.toInscDipl());
					detailRembourse.setToRepartitionCompteRelationship(detailPaiement.toRepartitionCompte());
					detailRembourse.setDrembDateOperation(now);
					detailRembourse.setDrembMontant(detailPaiement.dpaieMontantRembSaisi().negate());
				}
			}
			if (unRembEnCours().toDetailRembourses() == null || unRembEnCours().toDetailRembourses().count() == 0) {
				throw new Exception("Indiquer au moins un montant à rembourser !");
			}
			unRembEnCours().setErembSomme(
					((BigDecimal) unRembEnCours().toDetailRembourses().valueForKey("@sum." + EODetailRembourse.DREMB_MONTANT_KEY)).abs());
			ProcessInsRemboursement.enregistrer(cktlSession().dataBus(), edc(), unRembEnCours());
		}
		catch (ScolarixFwkException e) {
			unRembEnCours().deleteAllToDetailRemboursesRelationships();
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
			return null;
		}
		catch (Exception e) {
			unRembEnCours().deleteAllToDetailRemboursesRelationships();
			mySession().addSimpleErrorMessage("Erreur", e);
			return null;
		}
		setAjoutEnCours(false);
		dgRemboursementsUpdateData();
		mySession().addSimpleInfoMessage("OK", "Remboursement enregistré.");
		return null;
	}

	public WOActionResults annulerCreationRemboursement() {
		edc().revert();
		setAjoutEnCours(false);
		mySession().addSimpleInfoMessage("Annulé", "Création de remboursement annulée.");
		return null;
	}

	public void validate() throws EtudiantException {
		if (isAjoutEnCours() == true) {
			throw new EtudiantException("Ajout de remboursement en cours, annulez ou validez.");
		}
	}

	public boolean enabled() {
		Boolean disabled = super.disabled();
		boolean enabled = (disabled == null ? false : disabled.booleanValue());
		return enabled && (etudiant() != null && etudiant().numero() != null && (etudiant().isInscription() || etudiant().isReInscription()));
	}

	public Boolean disabled() {
		return !enabled() || utilisateur().isModifDisabled();
	}

	public EOEtudiantRembourse getUnRemb() {
		return unRemb;
	}

	public void setUnRemb(EOEtudiantRembourse unRemb) {
		this.unRemb = unRemb;
	}

	public EODetailRembourse getUnDetailRemb() {
		return unDetailRemb;
	}

	public void setUnDetailRemb(EODetailRembourse unDetailRemb) {
		this.unDetailRemb = unDetailRemb;
	}

	public boolean isAjoutEnCours() {
		return isAjoutEnCours;
	}

	public boolean isNotAjoutEnCours() {
		return !isAjoutEnCours;
	}

	public void setAjoutEnCours(boolean isAjoutEnCours) {
		this.isAjoutEnCours = isAjoutEnCours;
		setBusy(isAjoutEnCours);
	}

	public EODetailPaiement getUnDetailPaiement() {
		return unDetailPaiement;
	}

	public void setUnDetailPaiement(EODetailPaiement unDetailPaiement) {
		this.unDetailPaiement = unDetailPaiement;
	}

	public EOEtudiantRembourse unRembEnCours() {
		return unRembEnCours;
	}

	public void setUnRembEnCours(EOEtudiantRembourse unRembEnCours) {
		this.unRembEnCours = unRembEnCours;
	}

	public EORib unRib() {
		return unRib;
	}

	public void setUnRib(EORib unRib) {
		this.unRib = unRib;
	}

	public boolean isBusy() {
		return isBusy;
	}

	public void setBusy(boolean isBusy) {
		this.isBusy = isBusy;
	}

	public EOPaiementEcheance getUneEcheance() {
		return uneEcheance;
	}

	public void setUneEcheance(EOPaiementEcheance uneEcheance) {
		this.uneEcheance = uneEcheance;
	}

}