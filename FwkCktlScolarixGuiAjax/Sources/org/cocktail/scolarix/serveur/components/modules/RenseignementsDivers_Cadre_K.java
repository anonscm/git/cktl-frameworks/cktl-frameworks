/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.modules;

import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.finder.FinderTypeHandicap;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOProfession;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeHandicap;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeRegimeSise;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXS;

public class RenseignementsDivers_Cadre_K extends ModuleDossierAdministratif {
	private NSArray<EOTypeHandicap> handicaps;
	private NSArray<EOProfession> professions;
	private NSArray<EOTypeHandicap> typesFormation;
	private EOTypeHandicap aucunHandicap = null;

	private Boolean isHandicap;
	public EOTypeHandicap unHandicap;

	public EOProfession uneProfession;
	public EOProfession uneProfession2;

	public EOTypeRegimeSise unTypeFormation;

	public RenseignementsDivers_Cadre_K(WOContext context) {
		super(context);
		aucunHandicap = FinderTypeHandicap.getTypeHandicapAucunHandicap(session().defaultEditingContext());
	}

	public WOComponent enregistrer() {
		super.enregistrer();
		return null;
	}

	public String disabledTFScript() {
		return super.disabledTFScript();
	}

	public void validate() throws EtudiantException {
		IEtudiant etudiant = etudiant();
		if (etudiant == null) {
			throw new EtudiantException("Pas d'étudiant !!!");
		}
		etudiant.validateRenseignementsDivers();
	}

	public void reset() {
		setIsHandicap(null);
	}

	public NSArray<EOTypeHandicap> handicaps() {
		if (handicaps == null) {
			handicaps = FinderTypeHandicap.getTypesHandicap(session().defaultEditingContext());
		}
		return handicaps;
	}

	public EOTypeHandicap aucunHandicap() {
		return aucunHandicap;
	}

	public NSArray<EOProfession> professions() {
		if (professions == null) {
			professions = EOProfession.fetchAll(session().defaultEditingContext(), ERXS.ascInsensitives(EOProfession.PRO_LIBELLE_KEY));
		}
		return professions;
	}

	public NSArray<EOTypeHandicap> typesFormation() {
		if (typesFormation == null) {
			typesFormation = EOTypeRegimeSise.fetchAll(session().defaultEditingContext(), ERXS.ascs(EOTypeRegimeSise.TREG_CODE_KEY));
		}
		return typesFormation;
	}

	public void setTypesFormation(NSArray<EOTypeHandicap> typesFormation) {
		this.typesFormation = typesFormation;
	}

	public Boolean isInterruption() {
		return historique() != null && historique().histIntEtud() != null && historique().histIntEtud().intValue() == 1;
	}

	public Boolean isAssurance() {
		return historique() != null && historique().histAssuranceCivile() != null && historique().histAssuranceCivile().intValue() == 1;
	}

	public Integer histAssuranceCivile() {
		return historique().histAssuranceCivile();
	}

	public void setHistAssuranceCivile(Integer value) {
		historique().setHistAssuranceCivile(value);
		if (value != null && value.intValue() == 0) {
			historique().setHistAssuranceCivileCie(null);
			historique().setHistAssuranceCivileNum(null);
		}
	}

	public Boolean isHandicap() {
		if (isHandicap == null) {
			if (historique() != null && historique().toTypeHandicap() != null) {
				if (!historique().toTypeHandicap().equals(aucunHandicap())) {
					isHandicap = Boolean.TRUE;
				}
				else {
					isHandicap = Boolean.FALSE;
				}
			}
		}
		return isHandicap;
	}

	public void setIsHandicap(Boolean isHandicap) {
		this.isHandicap = isHandicap;
	}

	public Boolean isNotHandicap() {
		return isHandicap() == null || !isHandicap();
	}

	public void setIsNotHandicap(Boolean isNotHandicap) {
		this.isHandicap = !isNotHandicap;
		if (historique() != null && isNotHandicap.booleanValue()) {
			historique().setToTypeHandicapRelationship(aucunHandicap());
		}
	}

	public String uneProfessionDisplayString() {
		if (uneProfession == null) {
			return null;
		}
		String proLibelle = uneProfession.proLibelle();
		if (proLibelle != null && proLibelle.length() > 50) {
			proLibelle = proLibelle.substring(0, 50);
		}
		return uneProfession.proCode() + " - " + proLibelle;
	}

	public String uneProfession2DisplayString() {
		if (uneProfession2 == null) {
			return null;
		}
		String proLibelle = uneProfession2.proLibelle();
		if (proLibelle != null && proLibelle.length() > 50) {
			proLibelle = proLibelle.substring(0, 50);
		}
		return uneProfession2.proCode() + " - " + proLibelle;
	}

}