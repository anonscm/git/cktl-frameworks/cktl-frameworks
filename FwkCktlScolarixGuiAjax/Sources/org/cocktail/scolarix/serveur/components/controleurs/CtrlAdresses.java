/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.controleurs;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.components.modules.Adresses_Cadre_B;
import org.cocktail.scolarix.serveur.factory.FactoryAdresse;
import org.cocktail.scolarix.serveur.finder.FinderCommune;
import org.cocktail.scolarix.serveur.finder.FinderPays;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;

import com.webobjects.appserver.WOComponent;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class CtrlAdresses extends CtrlModule {
	private Adresses_Cadre_B wocomponent;

	public CtrlAdresses(Adresses_Cadre_B component) {
		this(component, null);
	}

	public CtrlAdresses(Adresses_Cadre_B component, EOEditingContext edc) {
		super(component, edc);
		wocomponent = (Adresses_Cadre_B) super.wocomponent;
	}

	public NSArray<EOPays> paysAdresseStable() {
		NSArray<EOPays> pays = null;
		String filtre = wocomponent.codePaysAdresseStable();

		if (!StringCtrl.isEmpty(filtre)) {
			pays = FinderPays.getPaysFiltre(edc, filtre);
		}

		return pays;
	}

	public NSArray<EOPays> paysAdresseUniversitaire() {
		NSArray<EOPays> pays = null;
		String filtre = wocomponent.codePaysAdresseUniversitaire();

		if (!StringCtrl.isEmpty(filtre)) {
			pays = FinderPays.getPaysFiltre(edc, filtre);
		}

		return pays;
	}

	public NSArray<EOCommune> communesAdresseStable() {
		NSArray<EOCommune> communes = null;
		String filtre = wocomponent.codePostalAdresseStable();

		if (!StringCtrl.isEmpty(filtre)) {
//			communes = FinderCommune.getCommunesFiltre(edc, filtre);
			communes = EOCommune.getFilteredCommunesSurLl(edc, filtre, wocomponent.unPaysAdresseStable);
		}

		return communes;
	}

	public NSArray<EOCommune> communesAdresseUniversitaire() {
		NSArray<EOCommune> communes = null;
		String filtre = wocomponent.codePostalAdresseUniversitaire();

		if (!StringCtrl.isEmpty(filtre)) {
			communes = FinderCommune.getCommunesFiltre(edc, filtre);
		}

		return communes;
	}

	public WOComponent recopierAdresseStable() {
		// Recopie des differentes valeurs de l'adresse stable
		IEtudiant etudiant = wocomponent.etudiant();
		EOAdresse adresseStable = etudiant.adresseStable();
		if (adresseStable != null) {
			wocomponent.setIsAdresseEnAttente(Boolean.FALSE);
			EOAdresse adresseUniversitaire = etudiant.adresseUniversitaire();
			setNoTelephoneUniversitaireFixe(getNoTelephoneStable());
			FactoryAdresse.recopierAdresseDansAdresse(adresseStable, adresseUniversitaire);
			wocomponent.setCodePaysAdresseUniversitaire(wocomponent.codePaysAdresseStable());
			wocomponent.setCodePostalAdresseUniversitaire(wocomponent.codePostalAdresseStable());
		}
		return null;
	}

	public String getNoTelephoneStable() {
		return wocomponent.etudiant().noTelephoneStable();
	}

	public void setNoTelephoneStable(String noTelephoneStable) {
		wocomponent.etudiant().setNoTelephoneStable(edc, noTelephoneStable);
	}

	public String getNoTelephoneUniversitaireFixe() {
		return wocomponent.etudiant().noTelephoneUniversitaireFixe();
	}

	public void setNoTelephoneUniversitaireFixe(String noTelephoneUniversitaireFixe) {
		wocomponent.etudiant().setNoTelephoneUniversitaireFixe(edc, noTelephoneUniversitaireFixe);
	}

	public String getNoTelephoneUniversitairePortable() {
		return wocomponent.etudiant().noTelephoneUniversitairePortable();
	}

	public void setNoTelephoneUniversitairePortable(String noTelephoneUniversitairePortable) {
		wocomponent.etudiant().setNoTelephoneUniversitairePortable(edc, noTelephoneUniversitairePortable);
	}

}
