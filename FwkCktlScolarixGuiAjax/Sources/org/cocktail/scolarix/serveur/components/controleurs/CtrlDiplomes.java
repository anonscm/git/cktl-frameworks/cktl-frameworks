/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.controleurs;

import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolaritefwk.serveur.interfaces.IScolFormationGrade;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolarix.serveur.components.modules.Diplomes_Cadre_C;
import org.cocktail.scolarix.serveur.finder.FinderDepartement;
import org.cocktail.scolarix.serveur.finder.FinderRne;
import org.cocktail.scolarix.serveur.finder.FinderTypeEchange;
import org.cocktail.scolarix.serveur.finder.FinderTypeInscription;
import org.cocktail.scolarix.serveur.metier.eos.EOCumulatif;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;
import org.cocktail.scolarix.serveur.metier.eos.EOLangue;
import org.cocktail.scolarix.serveur.metier.eos.EOMention;
import org.cocktail.scolarix.serveur.metier.eos.EOResultat;
import org.cocktail.scolarix.serveur.metier.eos.EORne;
import org.cocktail.scolarix.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeEchange;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription;
import org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite;
import org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite;
import org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class CtrlDiplomes extends CtrlModule {
	private Diplomes_Cadre_C wocomponent;
	private NSArray<EOVWebInscriptionResultat> formationsEnCours = null;
	private NSArray<EOTypeInscription> typesInscription = null;
	private NSArray<EOResultat> resultats = null;
	private NSArray<EOMention> mentions = null;
	private NSArray<EOTypeEchange> typeEchanges = null;

	private NSArray<EOScolFormationHabilitation> formationsEnvisageables = null;
	private NSArray<EOScolFormationHabilitation> formationsPossibles = null;

	private NSArray<IScolFormationGrade> gradesGrades = null;
	private NSArray<IScolFormationGrade> gradesFilieres = null;

	private NSArray<EOLangue> languesVivantes = null;
	private NSArray<EOCumulatif> cumulatifs = null;

	private IScolFormationGrade unGrade;
	private IScolFormationGrade grade;
	private String libelle;

	private NSArray<EOVEtablissementScolarite> etablissements;
	private EOVEtablissementScolarite etablissementSelected;
	private boolean isTypeLmdSelected = true, isTypeClassiqueSelected = false;
	private Boolean isAfficherFormationsEnvisageables = null;
	private Boolean isOuvrirAutreFormationEnvisageableEnReinscription = Boolean.FALSE;

	public CtrlDiplomes(Diplomes_Cadre_C component) {
		this(component, null);
	}

	public CtrlDiplomes(Diplomes_Cadre_C component, EOEditingContext edc) {
		super(component, edc);
		wocomponent = (Diplomes_Cadre_C) super.wocomponent;
	}

	public NSArray<EOVWebInscriptionResultat> formationsEnCours() {
		if (formationsEnCours == null) {
			formationsEnCours = wocomponent.etudiant().formationsEnCours();
		}
		return formationsEnCours;
	}

	public NSArray<EOScolFormationHabilitation> formationsEnvisageables() {
		if (formationsEnvisageables == null) {
			languesVivantes = null;
			if (wocomponent.etudiant().isReInscription() && isOuvrirAutreFormationEnvisageableEnReinscription()) {
				formationsEnvisageables = wocomponent.etudiant().formationsPossibles();
			}
			else {
				formationsEnvisageables = wocomponent.etudiant().formationsEnvisageables();
			}
			if (formationsEnvisageables != null) {
				NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
				if (isTypeLmdSelected()) {
					quals.addObject(ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
							+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
							+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY + "." + EOScolFormationGrade.FGRA_VALIDITE_KEY, "O"));
					if (grade() != null) {
						quals.addObject(ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
								+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
								+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY, grade()));
					}
				}
				else {
					quals.addObject(ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
							+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
							+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_FILIERE_KEY + "." + EOScolFormationFiliere.FFIL_VALIDITE_KEY,
							"O"));
					if (grade() != null) {
						quals.addObject(ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
								+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
								+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_FILIERE_KEY, grade()));
					}
				}
				if (libelle() != null) {
					EOKeyValueQualifier qual1 = ERXQ.likeInsensitive(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY
							+ "." + EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
							+ EOScolFormationDiplome.FDIP_LIBELLE_KEY, "*" + libelle() + "*");
					EOKeyValueQualifier qual2 = ERXQ.likeInsensitive(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY
							+ "." + EOScolFormationSpecialisation.FSPN_LIBELLE_KEY, "*" + libelle() + "*");
					quals.addObject(ERXQ.or(qual1, qual2));
				}
				if (etablissementSelected() != null) {
					EOKeyValueQualifier qual1 = ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
							+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
							+ EOScolFormationDiplome.TO_FWK_SCOLARIX_V_ETABLISSEMENT_SCOLARITE_KEY + "." + EOVEtablissementScolarite.TO_RNE_KEY,
							etablissementSelected().toRne());
					EOKeyValueQualifier qual2 = ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
							+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
							+ EOScolFormationDiplome.TO_FWK_SCOLARIX_V_COMPOSANTE_SCOLARITE_KEY + "." + EOVComposanteScolarite.TO_RNE_KEY,
							etablissementSelected().toRne());
					quals.addObject(ERXQ.or(qual1, qual2));
				}
				if (quals.count() > 0) {
					formationsEnvisageables = ERXQ.filtered(formationsEnvisageables, ERXQ.and(quals));
				}
			}
			if (formationsEnvisageables != null) {
				NSMutableArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>(3);
				sortOrderings.addObject(ERXS.asc(EOScolFormationHabilitation.TRI_KEY));
				sortOrderings.addObject(ERXS.asc(EOScolFormationHabilitation.LIBELLE_ABREGE_KEY));
				sortOrderings.addObject(ERXS.asc(EOScolFormationHabilitation.FHAB_NIVEAU_KEY));
				formationsEnvisageables = ERXS.sorted(formationsEnvisageables, sortOrderings);
			}
		}
		return formationsEnvisageables;
	}

	public void setFormationsEnvisageables(NSArray<EOScolFormationHabilitation> formationsEnvisageables) {
		this.formationsEnvisageables = formationsEnvisageables;
	}

	public NSArray<IScolFormationGrade> grades() {
		if (isTypeLmdSelected()) {
			if (gradesGrades == null) {
				gradesGrades = EOScolFormationGrade.fetchAll(edc);
			}
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(gradesGrades,
					new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOScolFormationGrade.TRI_KEY, EOSortOrdering.CompareAscending)));
		}
		else {
			if (gradesFilieres == null) {
				gradesFilieres = EOScolFormationFiliere.fetchAll(
						edc,
						new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOScolFormationFiliere.FFIL_ABREVIATION_KEY,
								EOSortOrdering.CompareCaseInsensitiveAscending)));
			}
			return gradesFilieres;
		}
	}

	public void changeType() {
		setGrade(null);
		rechercherLesFormationsEnvisageables();
	}

	public void afficherFormationsEnvisageables() {
		setIsAfficherFormationsEnvisageables(true);
	}

	public String divFormationsEnvisageablesStyle() {
		if (wocomponent.etudiant().isReInscription()) {
			if (isOuvrirAutreFormationEnvisageableEnReinscription()) {
				return "overflow-x:none;overflow-y:scroll;height:90px;";
			}
			else {
				return "overflow-x:none;overflow-y:scroll;height:120px;";
			}
		}
		else {
			return "overflow-x:none;overflow-y:scroll;height:180px;";
		}
	}

	public boolean isAfficherQbe() {
		return wocomponent.etudiant().isInscription()
				|| (wocomponent.etudiant().isReInscription() && isOuvrirAutreFormationEnvisageableEnReinscription());
	}

	public boolean isAfficherFormationsEnvisageables() {
		if (isAfficherFormationsEnvisageables == null) {
			if (formationsEnvisagees() != null && formationsEnvisagees().count() > 0) {
				isAfficherFormationsEnvisageables = Boolean.FALSE;
			}
			else {
				isAfficherFormationsEnvisageables = Boolean.TRUE;
			}
		}
		return isAfficherFormationsEnvisageables.booleanValue();
	}

	public void setIsAfficherFormationsEnvisageables(boolean isAfficherFormationsEnvisageables) {
		this.isAfficherFormationsEnvisageables = Boolean.valueOf(isAfficherFormationsEnvisageables);
	}

	public void rechercherLesFormationsEnvisageables() {
		formationsEnvisageables = null;
	}

	public NSArray<EOScolFormationHabilitation> formationsPossibles() {
		if (formationsPossibles == null) {
			formationsPossibles = wocomponent.etudiant().formationsPossibles();
		}
		return formationsPossibles;
	}

	public NSArray<EOInscDipl> formationsEnvisagees() {
		// TODO Exception Possible a traiter sur cette api afin d'afficher le message d'erreur
		NSArray<EOInscDipl> a = wocomponent.etudiant().formationsEnvisagees();
		return a;
	}

	public NSArray<EOLangue> languesVivantes() {
		if (languesVivantes == null) {
			languesVivantes = wocomponent.etudiant().languesVivantes();
		}
		return languesVivantes;
	}

	public String libelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public IScolFormationGrade unGrade() {
		return unGrade;
	}

	public void setUnGrade(IScolFormationGrade unGrade) {
		this.unGrade = unGrade;
	}

	public IScolFormationGrade grade() {
		return grade;
	}

	public void setGrade(IScolFormationGrade grade) {
		this.grade = grade;
	}

	public boolean isTypeLmdSelected() {
		return isTypeLmdSelected;
	}

	public void setIsTypeLmdSelected(boolean isTypeLmdSelected) {
		this.isTypeLmdSelected = isTypeLmdSelected;
		this.isTypeClassiqueSelected = !isTypeLmdSelected;
	}

	public boolean isTypeClassiqueSelected() {
		return isTypeClassiqueSelected;
	}

	public void setIsTypeClassiqueSelected(boolean isTypeClassiqueSelected) {
		this.isTypeClassiqueSelected = isTypeClassiqueSelected;
		this.isTypeLmdSelected = !isTypeClassiqueSelected;
	}

	public String noSelectionStringGrade() {
		if (isTypeLmdSelected()) {
			return "Tous";
		}
		return "Toutes";
	}

	public boolean isOuvrirAutreFormationEnvisageableEnReinscription() {
		return isOuvrirAutreFormationEnvisageableEnReinscription.booleanValue();
	}

	public void setIsOuvrirAutreFormationEnvisageableEnReinscription(boolean isOuvrirAutreFormationEnvisageableEnReinscription) {
		this.isOuvrirAutreFormationEnvisageableEnReinscription = Boolean.valueOf(isOuvrirAutreFormationEnvisageableEnReinscription);
	}

	public NSArray<EOTypeInscription> typesInscription() {
		if (typesInscription == null) {
			typesInscription = FinderTypeInscription.getTypeInscriptionsForInscription(edc);
		}
		return typesInscription;
	}

	public NSArray<EOResultat> resultats() {
		if (resultats == null) {
			resultats = EOResultat.fetchAll(
					edc,
					new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOResultat.RES_CODE_KEY,
							EOSortOrdering.CompareCaseInsensitiveAscending)));
		}
		return resultats;
	}

	public NSArray<EOMention> mentions() {
		if (mentions == null) {
			mentions = EOMention.fetchAll(edc,
					new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOMention.MENT_CODE_KEY, EOSortOrdering.CompareAscending)));
		}
		return mentions;
	}

	public NSArray<EOTypeEchange> typeEchanges() {
		if (typeEchanges == null) {
			if (wocomponent.uneFormationEnvisagee().toTypeInscription() != null
					&& wocomponent.uneFormationEnvisagee().toTypeInscription().idiplTypeInscription()
							.equals(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_ECHANGE_INTERNATIONAL)) {
				typeEchanges = FinderTypeEchange.getTypeEchangesArriveeFiltre(edc, null);
			}
			else {
				typeEchanges = FinderTypeEchange.getTypeEchangesDepartFiltre(edc, null);
			}
		}
		return typeEchanges;
	}

	public void setTypeEchanges(NSArray<EOTypeEchange> typeEchanges) {
		this.typeEchanges = typeEchanges;
	}

	public NSArray<EOCumulatif> cumulatifs() {
		if (cumulatifs == null) {
			cumulatifs = EOCumulatif.fetchAll(edc,
					new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOCumulatif.CUM_CODE_KEY, EOSortOrdering.CompareAscending)));
		}
		return cumulatifs;
	}

	public NSArray<EORne> universites() {
		NSArray<EORne> universites = null;
		String filtre = wocomponent.codeUniversite();

		if (!StringCtrl.isEmpty(filtre)) {
			universites = FinderRne.getRnesFiltre(edc, filtre, null, null, null);
		}

		return universites;
	}

	public NSArray<EODepartement> departements() {
		NSArray<EODepartement> departements = null;
		String filtre = wocomponent.codeDepartement();

		if (!StringCtrl.isEmpty(filtre)) {
			departements = FinderDepartement.getDepartementsFiltre(edc, filtre);
		}

		return departements;
	}

	public NSArray<EOVEtablissementScolarite> etablissements() {
		if (etablissements == null) {
			etablissements = EOVEtablissementScolarite.fetchAll(edc);
		}
		return etablissements;
	}

	public void setEtablissements(NSArray<EOVEtablissementScolarite> etablissements) {
		this.etablissements = etablissements;
	}

	public EOVEtablissementScolarite etablissementSelected() {
		if (etablissementSelected == null) {
			etablissementSelected = EOVEtablissementScolarite.fetchFirstByKeyValue(edc, EOVEtablissementScolarite.TO_RNE_KEY, wocomponent.etudiant()
					.rne());
			if (etablissementSelected == null) {
				etablissementSelected = etablissements().lastObject();
			}
		}
		return etablissementSelected;
	}

	public void setEtablissementSelected(EOVEtablissementScolarite etablissementSelected) {
		this.etablissementSelected = etablissementSelected;
	}

}
