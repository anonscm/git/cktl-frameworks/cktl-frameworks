/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.controleurs;

import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.components.modules.Identite_Cadre_A;
import org.cocktail.scolarix.serveur.finder.FinderDepartement;
import org.cocktail.scolarix.serveur.finder.FinderPays;
import org.cocktail.scolarix.serveur.finder.FinderVEtablissementSalarie;
import org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementSalarie;

import com.webobjects.appserver.WOComponent;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class CtrlIdentite extends CtrlModule {

	private Identite_Cadre_A wocomponent = null;

	public CtrlIdentite(WOComponent component) {
		this(component, null);
	}

	public CtrlIdentite(WOComponent component, EOEditingContext edc) {
		super(component, edc);
		wocomponent = (Identite_Cadre_A) super.wocomponent;
	}

	public NSArray<EOCivilite> civilites() {
		NSArray<EOCivilite> civilites = null;

		EOSortOrdering sexeOrdering = EOSortOrdering.sortOrderingWithKey(EOCivilite.SEXE_KEY, EOSortOrdering.CompareAscending);
		EOSortOrdering libelleLongOrdering = EOSortOrdering.sortOrderingWithKey(EOCivilite.L_CIVILITE_KEY, EOSortOrdering.CompareAscending);
		NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(new EOSortOrdering[] { sexeOrdering, libelleLongOrdering });
		civilites = EOCivilite.fetchAll(edc, sortOrderings);

		return civilites;
	}

	public NSArray<EODepartement> departements() {
		NSArray<EODepartement> departements = null;
		String filtre = wocomponent.codeDptDeNaissance();

		if (!StringCtrl.isEmpty(filtre)) {
			departements = FinderDepartement.getDepartementsFiltre(edc, filtre);
		}

		return departements;
	}

	public NSArray<EOPays> pays() {
		NSArray<EOPays> pays = null;
		String filtre = wocomponent.codePaysDeNaissance();

		if (!StringCtrl.isEmpty(filtre)) {
			pays = FinderPays.getPaysFiltre(edc, filtre);
		}

		return pays;
	}

	public NSArray<EOPays> nationalites() {
		NSArray<EOPays> nationalites = null;
		String filtre = wocomponent.codeNationaliteDeNaissance();

		if (!StringCtrl.isEmpty(filtre)) {
			nationalites = FinderPays.getPaysFiltre(edc, filtre);
		}

		return nationalites;
	}

	public NSArray<EOVEtablissementSalarie> etablissements() {
		NSArray<EOVEtablissementSalarie> etablissements = null;
		String filtre = wocomponent.codeEtablissement();
		if (!StringCtrl.isEmpty(filtre)) {
			etablissements = FinderVEtablissementSalarie.getVEtablissementSalariesFiltre(edc, filtre, null, null);
		}
		return etablissements;
	}

}
