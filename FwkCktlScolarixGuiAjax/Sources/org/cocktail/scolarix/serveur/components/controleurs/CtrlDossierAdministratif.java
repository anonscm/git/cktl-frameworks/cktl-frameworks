/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.controleurs;

import org.cocktail.scolarix.serveur.components.DossierAdministratif;
import org.cocktail.scolarix.serveur.finder.FinderGarnucheApplication;
import org.cocktail.scolarix.serveur.finder.FinderGarnucheCadreApplication;
import org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication;
import org.cocktail.scolarix.serveur.ui.EOGarnucheCadreApplication;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

public class CtrlDossierAdministratif {
	private DossierAdministratif component = null;
	private EOEditingContext edc = null;

	private boolean editing = false;

	private NSArray<EOGarnucheCadreApplication> cadresAll = null;

	private EOGarnucheCadreApplication unCadre;
	private EOGarnucheCadreApplication unCadreSelectionne;

	private boolean modeDossier = true;

	private String codeRneBackup = null, appCodeBackup = null;

	public CtrlDossierAdministratif(DossierAdministratif component) {
		super();
		this.component = component;
		if (component != null) {
			edc = component.edc();
		}
		reset();
	}

	public void reset() {
		cadresAll = null;
		setUnCadre(null);
		setUnCadreSelectionne(null);
		setModeDossier(true);
	}

	private void checkForUpdate() {
		if (component.etudiant() == null) {
			reset();
			return;
		}

		String appCode = EOGarnucheApplication.APPL_CODE_INSCRIPTION;
		if (component.etudiant().isPreInscription()) {
			appCode = EOGarnucheApplication.APPL_CODE_PREINSCRIPTION;
		}
		else {
			if (component.etudiant().isPreReInscription()) {
				appCode = EOGarnucheApplication.APPL_CODE_PREREINSCRIPTION;
			}
			else {
				if (component.etudiant().isAdmission()) {
					appCode = EOGarnucheApplication.APPL_CODE_ADMISSION;
				}
			}
		}

		String codeRne = null;
		if (component.etudiant().rne() != null) {
			codeRne = component.etudiant().rne().cRne();
		}
		if (codeRne == null) {
			reset();
			return;
		}

		if (codeRne.equals(codeRneBackup) == false || appCode.equals(appCodeBackup) == false) {
			appCodeBackup = appCode;
			codeRneBackup = codeRne;
			reset();
		}
	}

	public void swapModeDossierOuAction() {
		if (component.isBusy()) {
			component.mySession().addSimpleErrorMessage("Non", "Edition en cours dans le dossier...");
			return;
		}
		setUnCadreSelectionne(null);
		setModeDossier(!isModeDossier());
	}

	public String swapLib() {
		return (isModeDossier() ? "Actions >>>" : "<<< Dossier");
	}

	public NSArray<EOGarnucheCadreApplication> cadres() {
		checkForUpdate();
		if (isModeDossier()) {
			return cadresDossier();
		}
		else {
			return cadresAction();
		}
	}

	public void setUnCadre(EOGarnucheCadreApplication unCadre) {
		this.unCadre = unCadre;
	}

	public EOGarnucheCadreApplication getUnCadre() {
		return unCadre;
	}

	public EOGarnucheCadreApplication getUnCadreSelectionne() {
		checkForUpdate();
		if (unCadreSelectionne == null) {
			NSArray<EOGarnucheCadreApplication> cadres = cadres();
			if (cadres != null && cadres.count() > 0) {
				setUnCadreSelectionne(cadres.objectAtIndex(0));
			}
		}
		return unCadreSelectionne;
	}

	public int getUnCadreSelectionneIndex() {
		EOGarnucheCadreApplication cadreCourant = getUnCadreSelectionne();
		NSArray<EOGarnucheCadreApplication> cadres = cadres();
		if (cadreCourant != null && cadres != null) {
			for (int i = 0; i < cadres.count(); i++) {
				EOGarnucheCadreApplication cadre = cadres.objectAtIndex(i);
				if (cadre.equals(cadreCourant)) {
					return i;
				}
			}
		}
		return -1;
	}

	public void setUnCadreSelectionne(EOGarnucheCadreApplication unCadreSelectionne) {
		this.unCadreSelectionne = unCadreSelectionne;
	}

	private NSArray<EOGarnucheCadreApplication> cadresDossier() {
		NSArray<EOGarnucheCadreApplication> localCadres = getAllCadres();
		if (localCadres != null) {
			localCadres = ERXQ.filtered(localCadres, ERXQ.equals(EOGarnucheCadreApplication.CAPP_TYPE_CADRE_KEY, "D"));
		}
		return localCadres;
	}

	private NSArray<EOGarnucheCadreApplication> cadresAction() {
		NSArray<EOGarnucheCadreApplication> localCadres = getAllCadres();
		if (localCadres != null) {
			localCadres = ERXQ.filtered(localCadres, ERXQ.equals(EOGarnucheCadreApplication.CAPP_TYPE_CADRE_KEY, "A"));
		}
		return localCadres;
	}

	private NSArray<EOGarnucheCadreApplication> getAllCadres() {
		checkForUpdate();
		if (cadresAll == null) {
			String appCode = EOGarnucheApplication.APPL_CODE_INSCRIPTION;
			if (component.etudiant().isPreInscription()) {
				appCode = EOGarnucheApplication.APPL_CODE_PREINSCRIPTION;
			}
			else {
				if (component.etudiant().isPreReInscription()) {
					appCode = EOGarnucheApplication.APPL_CODE_PREREINSCRIPTION;
				}
				else {
					if (component.etudiant().isAdmission()) {
						appCode = EOGarnucheApplication.APPL_CODE_ADMISSION;
					}
				}
			}

			String codeRne = null;
			if (component.etudiant().rne() != null) {
				codeRne = component.etudiant().rne().cRne();
			}
			if (codeRne == null) {
				return null;
			}

			EOGarnucheApplication garnucheApplication = FinderGarnucheApplication.getGarnucheApplication(edc, appCode, codeRne);
			EOSortOrdering cAppPositionOrdering = EOSortOrdering.sortOrderingWithKey(EOGarnucheCadreApplication.CAPP_POSITION_KEY,
					EOSortOrdering.CompareAscending);
			NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(cAppPositionOrdering);
			cadresAll = FinderGarnucheCadreApplication.getGarnucheCadreApplications(edc, garnucheApplication, sortOrderings);
			if (cadresAll != null && cadresAll.count() > 0) {
				setUnCadreSelectionne(cadresAll.objectAtIndex(0));
			}
			else {
				setUnCadreSelectionne(null);
			}
		}
		return cadresAll;
	}

	public Boolean isModification() {
		return component.isModification();
	}

	public boolean isEditing() {
		if (component.hasBinding("editing")) {
			return component.valueForBooleanBinding("editing", false);
		}
		else {
			return editing;
		}
	}

	public boolean isNotEditing() {
		return !isEditing();
	}

	public void setEditing(boolean editing) {
		if (component.hasBinding("editing")) {
			component.setValueForBinding(editing, "editing");
		}
		else {
			this.editing = editing;
		}
	}

	public boolean isModeDossier() {
		return modeDossier;
	}

	public void setModeDossier(boolean modeDossier) {
		this.modeDossier = modeDossier;
	}

}
