/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.controleurs;

import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.components.modules.DernierDiplome_Cadre_F;
import org.cocktail.scolarix.serveur.finder.FinderDepartement;
import org.cocktail.scolarix.serveur.finder.FinderPays;
import org.cocktail.scolarix.serveur.finder.FinderRne;
import org.cocktail.scolarix.serveur.metier.eos.EORne;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class CtrlDernierDiplome extends CtrlModule {
	private DernierDiplome_Cadre_F wocomponent;

	public CtrlDernierDiplome(DernierDiplome_Cadre_F component) {
		this(component, null);
	}

	public CtrlDernierDiplome(DernierDiplome_Cadre_F component, EOEditingContext edc) {
		super(component, edc);
		wocomponent = (DernierDiplome_Cadre_F) super.wocomponent;
	}

	public NSArray<EOTypeDiplome> typesDernierDiplome() {
		NSArray<EOTypeDiplome> types = null;
		EOSortOrdering libelleOrdering = EOSortOrdering.sortOrderingWithKey(EOTypeDiplome.TDIPL_LIBELLE_KEY, EOSortOrdering.CompareAscending);
		types = EOTypeDiplome.fetchAll(edc, new NSArray<EOSortOrdering>(libelleOrdering));
		return types;
	}

//	public NSArray<EORne> etablissements() {
//		NSArray<EORne> etablissements = null;
//		String filtre = wocomponent.codeEtablissement();
//		String departement = wocomponent.codeDepartement();
//		Integer annee = wocomponent.historique().histAnneeDerDipl();
//		if (!StringCtrl.isEmpty(filtre)) {
//			if (!StringCtrl.isEmpty(departement)) {
//				if (departement.startsWith("0")) {
//					departement = departement.substring(1);
//				}
//			}
//			etablissements = FinderRne.getRnesFiltre(edc, filtre, departement, null, annee);
//		}
//
//		return etablissements;
//	}
	
	public NSArray<EORne> etablissements() {
		NSArray<EORne> etablissements = null;
		String filtre = wocomponent.codeEtablissement();
		if (!StringCtrl.isEmpty(filtre)) {
			etablissements = FinderRne.getRnesFiltre(edc, filtre, null, null, null);
		}

		return etablissements;
	}

	public NSArray<EODepartement> departements() {
		NSArray<EODepartement> departements = null;
		String filtre = wocomponent.codeDepartement();

		if (!StringCtrl.isEmpty(filtre)) {
			departements = FinderDepartement.getDepartementsFiltre(edc, filtre);
		}

		return departements;
	}

	public NSArray<EOPays> lesPays() {
		NSArray<EOPays> pays = null;
		String filtre = wocomponent.codePays();

		if (!StringCtrl.isEmpty(filtre)) {
			pays = FinderPays.getPaysFiltre(edc, filtre);
		}

		return pays;
	}

}
