/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components.controleurs;

import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.components.modules.TitreAcces_Cadre_D;
import org.cocktail.scolarix.serveur.finder.FinderBac;
import org.cocktail.scolarix.serveur.finder.FinderDepartement;
import org.cocktail.scolarix.serveur.finder.FinderMention;
import org.cocktail.scolarix.serveur.finder.FinderPays;
import org.cocktail.scolarix.serveur.finder.FinderRne;
import org.cocktail.scolarix.serveur.metier.eos.EOBac;
import org.cocktail.scolarix.serveur.metier.eos.EOMention;
import org.cocktail.scolarix.serveur.metier.eos.EORne;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;

public class CtrlTitreAcces extends CtrlModule {
	private TitreAcces_Cadre_D wocomponent;

	public CtrlTitreAcces(TitreAcces_Cadre_D component) {
		this(component, null);
	}

	public CtrlTitreAcces(TitreAcces_Cadre_D component, EOEditingContext edc) {
		super(component, edc);
		wocomponent = (TitreAcces_Cadre_D) super.wocomponent;
	}

	public NSArray<EOBac> bacs() {
		NSArray<EOBac> bacs = null;
		String filtre = wocomponent.codeBac();

		if (!StringCtrl.isEmpty(filtre)) {
			bacs = FinderBac.getBacsFiltre(edc, filtre);
		}
		else {
			bacs = FinderBac.getBacs(edc, null);
		}
		if (wocomponent.etudiant().anneeObtentionTitreAcces() != null) {
			EOKeyValueQualifier q1 = new EOKeyValueQualifier(EOBac.BAC_DATE_VALIDITE_KEY, EOKeyValueQualifier.QualifierOperatorLessThanOrEqualTo,
					wocomponent.etudiant().anneeObtentionTitreAcces());
			EOKeyValueQualifier q2 = new EOKeyValueQualifier(EOBac.BAC_DATE_VALIDITE_KEY, EOKeyValueQualifier.QualifierOperatorEqual,
					NSKeyValueCoding.NullValue);
			EOQualifier qual1 = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] { q1, q2 }));

			EOKeyValueQualifier q3 = new EOKeyValueQualifier(EOBac.BAC_DATE_INVALIDITE_KEY,
					EOKeyValueQualifier.QualifierOperatorGreaterThanOrEqualTo, wocomponent.etudiant().anneeObtentionTitreAcces());
			EOKeyValueQualifier q4 = new EOKeyValueQualifier(EOBac.BAC_DATE_INVALIDITE_KEY, EOKeyValueQualifier.QualifierOperatorEqual,
					NSKeyValueCoding.NullValue);
			EOQualifier qual2 = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] { q3, q4 }));
			bacs = EOQualifier.filteredArrayWithQualifier(bacs, new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] { qual1, qual2 })));
		}
		return bacs;
	}

	public NSArray<EOMention> mentions() {
		NSArray<EOMention> mentions = null;

		if (mentions == null) {
			mentions = FinderMention.getMentions(edc, null);
		}
		return mentions;
	}

//	public NSArray<EORne> etablissements() {
//		NSArray<EORne> etablissements = null;
//		String filtre = wocomponent.codeEtablissement();
//		String departement = wocomponent.codeDepartement();
//		String ville = wocomponent.etudiant().villeTitreAcces();
//
//		if (!StringCtrl.isEmpty(filtre)) {
//			if (!StringCtrl.isEmpty(departement)) {
//				if (departement.startsWith("0")) {
//					departement = departement.substring(1);
//				}
//			}
//			etablissements = FinderRne.getRnesFiltre(edc, filtre, departement, ville, null);
//		}
//		return etablissements;
//	}

	public NSArray<EORne> etablissements() {
		NSArray<EORne> etablissements = null;
		String filtre = wocomponent.codeEtablissement();

		if (!StringCtrl.isEmpty(filtre)) {
			etablissements = FinderRne.getRnesFiltre(edc, filtre, null, null, null);
		}
		return etablissements;
	}

	public NSArray<EODepartement> departements() {
		NSArray<EODepartement> departements = null;
		String filtre = wocomponent.codeDepartement();

		if (!StringCtrl.isEmpty(filtre)) {
			departements = FinderDepartement.getDepartementsFiltre(edc, filtre);
		}
		return departements;
	}

	public NSArray<EOPays> lesPays() {
		NSArray<EOPays> pays = null;
		String filtre = wocomponent.codePays();

		if (!StringCtrl.isEmpty(filtre)) {
			pays = FinderPays.getPaysFiltre(edc, filtre);
		}
		return pays;
	}

}
