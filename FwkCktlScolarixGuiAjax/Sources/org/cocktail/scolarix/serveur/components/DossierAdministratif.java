/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.components;

import java.util.Iterator;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.scolarix.serveur.components.controleurs.CtrlDossierAdministratif;
import org.cocktail.scolarix.serveur.components.modules.ModuleDossierAdministratif;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EORne;
import org.cocktail.scolarix.serveur.metier.eos.EOUtilisateur;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation._NSStringUtilities;

import er.extensions.appserver.ERXRedirect;

public class DossierAdministratif extends CktlAjaxWOComponent {
	public CtrlDossierAdministratif ctrl = null;

	private boolean displayConfirmationForSave = false;
	private NSMutableDictionary<String, WOComponent> componentCache;

	public DossierAdministratif(WOContext context) {
		super(context);
		ctrl = new CtrlDossierAdministratif(this);
		componentCache = new NSMutableDictionary<String, WOComponent>();
	}

	public void awake() {
		super.awake();
		setValueForBinding(isBusy(), "isBusy");
	}

	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}

	public CocktailAjaxSession session() {
		return (CocktailAjaxSession) super.session();
	}

	public boolean isBusy() {
		if (ctrl.isEditing()) {
			return true;
		}
		Iterator<WOComponent> it = componentCache().values().iterator();
		while (it.hasNext()) {
			ModuleDossierAdministratif mod = (ModuleDossierAdministratif) it.next();
			if (mod.isBusy()) {
				return true;
			}
		}
		return false;
	}

	public boolean isSwapEnabled() {
		return etudiant() != null && (etudiant().isInscription() || etudiant().isReInscription() || etudiant().isAdmission()) && !isBusy();
	}

	public boolean isNouvelEtudiant() {
		if (etudiant() == null) {
			return true;
		}
		boolean isNewAdmission = etudiant().isAdmission() && (etudiant().candidat() == null || etudiant().candidat().primaryKey() == null);
		boolean isNewInscription = !etudiant().isAdmission() && etudiant().numero() == null;
		return isNewAdmission || isNewInscription;
	}

	// Bindings...

	public IEtudiant etudiant() {
		return (IEtudiant) valueForBinding("etudiant");
	}

	public EOHistorique historique() {
		return (EOHistorique) valueForBinding("historique");
	}

	public boolean canClick() {
		return !isBusy();
	}

	public EOUtilisateur utilisateur() {
		return (EOUtilisateur) valueForBinding("utilisateur");
	}

	public Boolean isEditable() {
		Boolean isEditable = Boolean.FALSE;
		if (hasBinding("editable")) {
			isEditable = (Boolean) valueForBinding("editable");
		}
		return isEditable;
	}

	public boolean isModification() {
		Boolean isModif = Boolean.FALSE;
		if (hasBinding("isModification")) {
			isModif = (Boolean) valueForBinding("isModification");
		}
		return isModif;
	}

	public boolean isAnneeCivile() {
		Boolean isAnneeCivile = Boolean.FALSE;
		if (hasBinding("isAnneeCivile")) {
			isAnneeCivile = (Boolean) valueForBinding("isAnneeCivile");
		}
		return isAnneeCivile;
	}

	public String returnPage() {
		return (String) valueForBinding("returnPage");
	}

	public String returnPageOnCancel() {
		return (String) valueForBinding("returnPageOnCancel");
	}

	public String returnPageOnSave() {
		return (String) valueForBinding("returnPageOnSave");
	}

	public String returnPageOnError() {
		return (String) valueForBinding("returnPageOnError");
	}

	//

	public String numerosIndividu() {
		if (etudiant() != null && etudiant().individu() != null) {
			return "noIndividu: " + etudiant().individu().noIndividu() + " - persId: " + etudiant().individu().persId();
		}
		return null;
	}

	public String numeroHistorique() {
		if (etudiant() != null && historique() != null) {
			return "histNumero: " + historique().histNumero();
		}
		return null;
	}

	public void editerDossier() {
		if (etudiant() != null && historique() != null) {
			ctrl.setEditing(true);
		}
	}

	public String anneeScolaireDossierAAfficher() {
		String anneeScolaireDossierAAfficher = "";
		if (etudiant() != null && historique() != null) {
			anneeScolaireDossierAAfficher = String.valueOf(historique().histAnneeScol());
			if (isAnneeCivile() == false) {
				anneeScolaireDossierAAfficher += " / " + String.valueOf(historique().histAnneeScol().intValue() + 1);
			}
		}
		return anneeScolaireDossierAAfficher;
	}

	public String confirmationMessageForSave() {
		if (etudiant() != null && etudiant().numeroINE() == null) {
			if (etudiant().etudReimmatriculation() == null) {
				return "Vous n'avez pas saisi d'identifiant national étudiant. Un nouveau numéro va être créé... OK?";
			}
			else {
				return "Vous avez demandé un identifiant national étudiant provisoire. Un numéro provisoire sera créé... OK?";
			}
		}
		return "Enregistrer ce dossier ?";
	}

	public WOActionResults confirmer() {
		return enregistrer();
	}

	public WOActionResults infirmer() {
		setDisplayConfirmationForSave(false);
		return null;
	}

	public WOActionResults precedent() {
		try {
			ctrl.setUnCadreSelectionne(ctrl.cadres().objectAtIndex(ctrl.getUnCadreSelectionneIndex() - 1));
		}
		catch (Exception e) {
		}
		return null;
	}

	public WOActionResults suivant() {
		try {
			ModuleDossierAdministratif womodule = (ModuleDossierAdministratif) componentCache().objectForKey(
					ctrl.getUnCadreSelectionne().garnucheCadre().componentName());
			womodule.validate();
			ctrl.setUnCadreSelectionne(ctrl.cadres().objectAtIndex(ctrl.getUnCadreSelectionneIndex() + 1));
		}
		catch (EtudiantException e) {
			session().addSimpleErrorMessage("Erreur...", e.getMessageFormatte());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean isPrecedentDisabled() {
		return ctrl.getUnCadreSelectionneIndex() == 0;
	}

	public boolean isSuivantDisabled() {
		return ctrl.getUnCadreSelectionneIndex() == (ctrl.cadres().count() - 1);
	}

	public WOActionResults enregistrer() {
		WOResponse response = null;
		try {
			if (isDisplayConfirmationForSave() == false) {
				// lancé depuis le bouton enregistrer...
				if (etudiant() != null && !etudiant().isAdmission() && etudiant().numeroINE() == null) {
					setDisplayConfirmationForSave(true);
					return null;
				}
			}
			else {
				// lancé depuis la confirmation...
				setDisplayConfirmationForSave(false);
			}

			IEtudiant etudiantTemp = null;
			Integer histAnneeScol = historique().histAnneeScol();
			EORne rne = etudiant().rne();
			if (etudiant().individu() != null) {
				etudiant().individu().setPersIdModification(new Integer(session().connectedUserInfo().persId().intValue()));
			}
			etudiantTemp = etudiant().enregistrer(session().dataBus(), session().defaultEditingContext());
			if (etudiantTemp != null) {
				try {
					System.out.println("[SAVE " + "Insc" + rne.cRne() + "] " + DateCtrl.currentDateTimeString() + " - etudNumero = "
							+ etudiantTemp.numero());
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				etudiantTemp.setAnneeInscriptionEnCours(histAnneeScol);
				// etudiantTemp.setEtudType(EOEtudiant.ETUD_TYPE_INSCRIPTION);
				etudiantTemp.setRne(rne);
				setValueForBinding(etudiantTemp, "etudiant");
				setValueForBinding(etudiantTemp.historique(histAnneeScol), "historique");
				setValueForBinding(null, "returnPage");
				ctrl.setEditing(false);
				resetAllCaches();
				return returnPageComponent(false);
			}
			else {
				CktlLog.rawLog("[DossierAdministratif.java] " + DateCtrl.currentDateTimeString()
						+ " - Erreur innatendue lors de l'enregistrement du dossier:");
				throw new ScolarixFwkException("Une erreur indéterminée a été générée lors de l'enregistrement du dossier.");
			}
		}
		catch (EtudiantException e) {
			response = new WOResponse();
			response.setStatus(500);
			session().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
		}
		catch (ScolarixFwkException e) {
			response = new WOResponse();
			response.setStatus(500);
			String messageErreur = e.getMessageFormatte();
			if (messageErreur == null) {
				messageErreur = "Erreur non déterminée !";
			}
			session().addSimpleErrorMessage("Erreur", messageErreur);
		}
		catch (Exception e) {
			e.printStackTrace();
			response = new WOResponse();
			response.setStatus(500);
			String messageErreur = e.getMessage();
			if (messageErreur == null) {
				messageErreur = "Erreur non déterminée !";
			}
			session().addSimpleErrorMessage("Erreur", messageErreur);
		}
		return null;
	}

	public WOActionResults annuler() {
	    resetAllCaches();
		setValueForBinding(isBusy(), "isBusy");
		return returnPageComponent(true);
	}

	private void resetAllCaches() {
	    session().defaultEditingContext().revert();
	    session().defaultEditingContext().invalidateAllObjects();
	    ctrl.setEditing(false);
	    etudiant().reset();
	    // on force le reset
	    Iterator<WOComponent> it = componentCache().values().iterator();
	    while (it.hasNext()) {
	        ModuleDossierAdministratif mod = (ModuleDossierAdministratif) it.next();
	        mod.reset();
	        mod.setEtudiant(etudiant());
	    }
	}
	
	private WOActionResults returnPageComponent(boolean onCancel) {
		String returnPage = returnPage();
		if (onCancel && returnPageOnCancel() != null) {
			returnPage = returnPageOnCancel();
		}
		if (!onCancel && returnPageOnSave() != null) {
			returnPage = returnPageOnSave();
		}
		if (returnPage != null) {
			ERXRedirect redirectPage = new ERXRedirect(context());
			redirectPage.setComponent(session().getSavedPageWithName(returnPage));
			return redirectPage;
		}
		try {
			// if (etudiant() == null || etudiant().numero() == null) {
			if (etudiant() == null) {
				throw new Exception("Pas d'étudiant en retour d'enregistrement du dossier !");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			ERXRedirect redirectPage = new ERXRedirect(context());
			redirectPage.setComponent(session().getSavedPageWithName(returnPageOnError()));
			return redirectPage;
		}
		return null;
	}

	public String onSuccessAnnuler() {
		if (returnPage() == null && returnPageOnCancel() == null) {
			return "function (oS) {ContainerDossierAdministratifUpdate();}";
		}
		return null;
	}

	public String onCompleteEnregistrer() {
		if (returnPage() == null && returnPageOnSave() == null) {
			return "function (oC) {ContainerDossierAdministratifUpdate();}";
		}
		return null;
	}

	public String cadreTabName() {
		String cadreTabName = null;
		if (ctrl.getUnCadre() != null) {
			cadreTabName = ctrl.getUnCadre().garnucheCadre().cadrCode();
			cadreTabName = _NSStringUtilities.stringMarkingUpcaseTransitionsWithDelimiter(cadreTabName, " ");
			if (ctrl.getUnCadre().garnucheCadre().cadrAbreviation() != null) {
				cadreTabName = ctrl.getUnCadre().garnucheCadre().cadrAbreviation() + " - " + cadreTabName;
			}
		}
		return cadreTabName;
	}

	public WOActionResults afficherCadre() {
		ctrl.setUnCadreSelectionne(ctrl.getUnCadre());
		return null;
	}

	public String containerModuleId() {
		String containerModuleId = "Container";
		containerModuleId += divCadreName();
		return containerModuleId;
	}

	public String divCadreName() {
		String divCadreName = "SansNom";
		if (ctrl.getUnCadreSelectionne() != null) {
			divCadreName = ctrl.getUnCadreSelectionne().garnucheCadre().componentName();
		}
		return divCadreName;
	}

	public WOActionResults submit() {
		return null;
	}

	public boolean isDisplayConfirmationForSave() {
		return displayConfirmationForSave;
	}

	public void setDisplayConfirmationForSave(boolean displayConfirmationForSave) {
		this.displayConfirmationForSave = displayConfirmationForSave;
	}

	public String tabbedPanelTabCss() {
		if (ctrl.getUnCadreSelectionne().equals(ctrl.getUnCadre())) {
			return "tabbedPanelTab-selected";
		}
		return "tabbedPanelTab-unselected";
	}

	public String titleCadre() {
		return ctrl.getUnCadre().garnucheCadre().cadrCommentaire();
	}

	public NSMutableDictionary<String, WOComponent> componentCache() {
		return componentCache;
	}

	public void setComponentCache(NSMutableDictionary<String, WOComponent> componentCache) {
		this.componentCache = componentCache;
	}

}