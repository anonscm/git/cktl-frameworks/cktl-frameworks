package org.cocktail.lecteurcheque.serveur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.Hashtable;

/**
 * @author Marc-Henry DELAVAUD-BOISIS <marc-henry.delavaud-boisis at univ-lr.fr>
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */

/**
 * Client permettant de piloter un lecteur de cheque distant
 */
public class LecteurChequeClient {

	private Socket socket;
	private OutputStream out;
	private BufferedReader br;
	private String typeCheque = null;
	private int lgrCodeBanque;
	private byte[] numCheque, codeBanque, numCompte;
	private boolean canceled = false;

	private ILecteurChequeClientListener listener;

	/**
	 * Constructeur, connexion au serveur avec un timeout de 5 secondes et ouverture de connexion au lecteur de cheque
	 * 
	 * @param _listener
	 *            Facultatif
	 * @param serverHostName
	 *            nom de la machine serveur ou IP sous la forme X.X.X.X
	 * @param port
	 *            du serveur
	 * @throws UnknownHostException
	 *             l'IP du serveur ne peut etre trouvee
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws Exception
	 */
	public LecteurChequeClient(ILecteurChequeClientListener _listener, String serverHostName, int port) throws UnknownHostException, IOException,
			Exception {
		// requete DNS pour obtenir l'ip du serveur
		InetAddress addr = InetAddress.getByName(serverHostName);
		listener = _listener;

		SocketAddress sockaddr = new InetSocketAddress(addr, port);
		socket = new Socket(); // creation du socket
		int timeoutMs = 5000; // 5 seconds
		socket.connect(sockaddr, timeoutMs); // connexion

		out = socket.getOutputStream(); // ouverture du lecteur
		out.write("START\n".getBytes()); // test
		out.flush();
		br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		String result = br.readLine(); // lecture du resultat
		if (result.startsWith("ERROR")) {
			throw new Exception("[Serveur] " + result.substring(6));
		}
		out.write("STOP\n".getBytes());
		out.flush();
	}

	/**
	 * Lire un cheque de type CB(bancaire) ou CCP(postal) Retourne une hashtable dont voici les differentes cles : typeCheque String(CCP ou
	 * CB) numCheque String numero du cheque codeBanque String code de la banque numCompte String numero du compte
	 * 
	 * Les vérifications sont faites sur les differents champs
	 * 
	 * @throws Exception
	 * @return table de hashage qui contient les informations du cheque lu
	 * 
	 */
	public Hashtable<String, String> lire() throws Exception {
		setCanceled(false);
		if (listener != null) {
			listener.onLecteurNotReady();
		}
		out.write(new String("START\n").getBytes());
		out.flush();

		String result = br.readLine(); // lecture du resultat
		System.out.println(" result 1:" + result);
		if (result == null) {
			throw new Exception("resultat null");
		}
		if (result.startsWith("ERROR")) {
			throw new Exception("[Serveur]" + result.substring(6));
		}
		if (result.startsWith("OK")) {
			out.write(new String("READ\n").getBytes());
			out.flush();
			result = br.readLine(); // lecture du resultat
			System.out.println(" result 2: " + result);
			if (result == null) {
				throw new Exception("resultat null");
			}
			if (result.startsWith("ERROR")) {
				throw new Exception("[Serveur]" + result.substring(6));
			}
			if (result.startsWith("OK")) {
				if (listener != null) {
					listener.onLecteurReady();
				}
				System.out.println("Attente de cheque...");
				result = br.readLine();
				System.out.println(" result 3: " + result);
				if (isCanceled()) {
					return null;
				}
				if (listener != null) {
					listener.onChequeLu();
				}
				if (result == null) {
					return null;
				}
			}
			else {
				throw new Exception("Unknown error:" + result);
			}
			switch (result.getBytes()[13]) // traitement du resultat en fct du
											// type de cheque
			{
			case 0x30:
			case 0x32:
				typeCheque = "CCP"; // CCP
				lgrCodeBanque = 2;

				numCheque = new byte[7]; // lecture du numero de cheque
				for (int i = 0; i < 7; i++) {
					numCheque[i] = result.getBytes()[14 + i];
				}

				numCompte = new byte[9]; // lecture du numero de compte
				for (int i = 0; i < 6; i++) {
					numCompte[i] = result.getBytes()[22 + i];
				}

				codeBanque = new byte[2]; // lecture du code banque
				for (int i = 0; i < 2; i++) {
					codeBanque[i] = result.getBytes()[31 + i];
				}
				break;

			case 0x31:
			case 0x33:
				typeCheque = "CB"; // Cheque bancaire
				lgrCodeBanque = 9;

				numCheque = new byte[7]; // lecture du numero de cheque
				for (int i = 0; i < 7; i++) {
					numCheque[i] = result.getBytes()[14 + i];
				}

				codeBanque = new byte[9]; // lecture du code banque
				for (int i = 0; i < 9; i++) {
					codeBanque[i] = result.getBytes()[22 + i];
				}

				numCompte = new byte[11]; // lecture du numero du compte
				for (int i = 0; i < 11; i++) {
					numCompte[i] = result.getBytes()[36 + i];
				}
				break;
			default:
				throw new Exception("Type de cheque inconnu"); // type de
																// cheque
																// inconnu
			}

			if (!(isNoChequeValid(numCheque) && isCodeBankValid(codeBanque) && isNoCompteValid(numCompte))) // Verification
																											// syntaxique
			{
				throw new Exception("Données lues invalides");
			}
			out.write(new String("START\n").getBytes());//
			out.flush();

			// sonnerie pour indiquer lecture ok
			result = br.readLine();
			ring();
			if (listener != null) {
				listener.onChequeAnalyse();
			}

			Hashtable<String, String> cheque = new Hashtable<String, String>(); // construction de la table de
			// hashage contenant le resultat
			cheque.put("typeCheque", new String(this.typeCheque));
			cheque.put("numCheque", new String(this.numCheque));
			cheque.put("codeBanque", new String(this.codeBanque));
			cheque.put("numCompte", new String(this.numCompte));
			return cheque;
		}
		throw new Exception("UNKNOW ERROR");
	}

	/**
	 * Verifie la validite du numero de cheque 2<length<7 et que des chiffres
	 * 
	 * @param data
	 *            numero de cheque
	 * @return true si valide
	 */
	private boolean isNoChequeValid(byte[] data) // 2<length<7 et que des
													// chiffres
	{
		if ((data.length > 7) || (data.length < 2)) {
			return false;
		}
		if (!queDesChiffres(data)) {
			return false;
		}
		return true;
	}

	/**
	 * Verifie la validite du code banque 2<length<lgrCodeBank et que des chiffres
	 * 
	 * @param data
	 *            code de la banque
	 * @return true si valide;
	 */
	private boolean isCodeBankValid(byte[] data) // 2<length<lgrCodeBank et
													// que des chiffres
	{
		if ((data.length > this.lgrCodeBanque) || (data.length < 2)) {
			return false;
		}
		return queDesChiffres(data);
	}

	/**
	 * Verifie la validite du numero du compte 2<length<11 et que des chiffres
	 * 
	 * @param data
	 *            numero du compte
	 * @return true si valide
	 */
	private boolean isNoCompteValid(byte[] data) // 2<length<11 et que des
													// chiffres
	{
		if ((data.length > 11) || (data.length < 2)) {
			return false;
		}
		return queDesChiffres(data);
	}

	/**
	 * Verifie que le contenu n'est composŽ que de chiffres
	 * 
	 * @param data
	 * @return true si que des chiffres
	 */
	private boolean queDesChiffres(byte[] data) {
		for (int i = 0; i < data.length; i++) {
			if ((data[i] < 0x30) || (data[i] > 0x39)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Ecrire un cheque --ATTENTION-- tout ecrire en majuscule
	 * 
	 * @param unite
	 *            Unite du montant (E,FR,EURO...)
	 * @param montant
	 *            syntaxe *X.XX
	 * @param date
	 *            JJMMAAAA
	 * @param lieu
	 * @param ordre
	 * @throws Exception
	 */
	public void ecrire(String unite, String montant, String date, String lieu, String ordre) throws Exception {
		setCanceled(false);
		out.write(new String("START\n").getBytes());//
		out.flush();

		String result = br.readLine(); // lecture du resultat
		if (result == null) {
			throw new Exception("resultat null");
		}
		if (result.startsWith("ERROR")) {
			throw new Exception("[Serveur]" + result.substring(6));
		}
		if (result.startsWith("OK")) {

			out.write(new String("WRITE*" + unite + "*" + montant + "*" + date + "*" + lieu + "*" + ordre + "\n").getBytes()); // le
																																// caractere
																																// *
																																// sert
																																// de
																																// separation
			out.flush();

			result = br.readLine(); // lecture du resultat
			if (result == null) {
				throw new Exception("resultat null");
			}
			if (result.startsWith("ERROR")) // Erreur de lecture du cheque
			{
				throw new Exception("[Serveur]" + result.substring(6));
			}
			if (result.startsWith("OK")) {
				System.out.println("Attente de cheque...");
				result = br.readLine();
			}
			else {
				throw new Exception("Unknow error:" + result);
			}

			if (result.startsWith("ERROR")) {
				throw new Exception("[Serveur]" + result.substring(6));
			}
		}
		out.write("STOP\n".getBytes());
		out.flush();
	}

	/**
	 * Faire sonner le lecteur
	 * 
	 */
	public void ring() throws IOException {
		OutputStream out1 = socket.getOutputStream();
		out1.write(new String("RING\n").getBytes());
		out1.flush();
		br.readLine();
		br.readLine();
	}

	/**
	 * Annulation d'une commande
	 * 
	 * @throws Exception
	 */
	public void annuler() throws Exception {
		OutputStream out1 = socket.getOutputStream();
		out1.write(new String("CANCEL\n").getBytes());
		out1.flush();
		setCanceled(true);
		if (listener != null) {
			listener.onLecteurCanceled();
		}
	}

	/**
	 * Libération du lecteur
	 * 
	 * @throws Exception
	 */
	public void close() throws Exception {
		OutputStream out1 = socket.getOutputStream();
		out1.write(new String("FINISH\n").getBytes());
		out1.flush();
		setCanceled(false);
	}

	protected void finalize() throws Throwable {
		try {
			socket.close();
		}
		finally {
			super.finalize();
		}
	}

	public interface ILecteurChequeClientListener {

		public void onChequeAnalyse();

		public void onChequeLu();

		public void onLecteurReady();

		public void onLecteurNotReady();

		public void onLecteurCanceled();
	}

	public boolean isCanceled() {
		return canceled;
	}

	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}

}
