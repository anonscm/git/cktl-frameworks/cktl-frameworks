/**
  * http://www.softec.lu/site/DevelopersCorner/BootstrapPrototypeConflict
  * Fixe les conflits entre Prototype et Bootstrap
  */
(function (fixBootstrapPrototypeConflicts, jQuery) {
    if (Prototype.BrowserFeatures.ElementExtensions) {
        var disablePrototypeJS = function (method, pluginsToDisable, jQueryArg) {
			var jQueryLocal = jQueryArg || jQuery;   	
            var handler = function (event) {
                event.target[method] = undefined;
                setTimeout(function () {
                    delete event.target[method];
                }, 0);
            };
            pluginsToDisable.each(function (plugin) { 
                jQueryLocal(window).on(method + '.bs.' + plugin, handler);
            });
        };
    	var pluginsToDisable = ['collapse', 'dropdown', 'modal', 'tooltip', 'popover', 'tab', 'validator', 'button'];
    	
        disablePrototypeJS('show', pluginsToDisable);
        disablePrototypeJS('hide', pluginsToDisable);
        
        fixBootstrapPrototypeConflicts.disablePrototypeJS = function(jQuery) {
        	disablePrototypeJS('show', pluginsToDisable, jQuery);
       	 	disablePrototypeJS('hide', pluginsToDisable, jQuery);
        };
    }
    
    
    
}) (window.fixBootstrapPrototypeConflicts = window.fixBootstrapPrototypeConflicts || {}, jQuery);