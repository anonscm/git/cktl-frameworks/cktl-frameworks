package org.cocktail.fwkcktlscolpedagui.serveur;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.ModuleRegister;

import er.extensions.ERXFrameworkPrincipal;

/**
 * Classe principale de framework scolpedaGui
 */
public class FwkCktlScolPedaGui extends ERXFrameworkPrincipal {
	public static final Logger LOG = Logger.getLogger(FwkCktlScolPedaGui.class);

	// Registers the class as the framework principal
	static {
		setUpFrameworkPrincipalClass(FwkCktlScolPedaGui.class);
	}

	@Override
	public void finishInitialization() {
		ModuleRegister moduleRegister = CktlWebApplication.application().getModuleRegister();
		moduleRegister.addModule(new FwkCktlScolPedaGuiModule());
	}

}
