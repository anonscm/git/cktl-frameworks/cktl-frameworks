package org.cocktail.fwkcktlscolpedagui.serveur.components;

import com.google.common.base.Objects;

/**
 * 
 * Petit modèle à cabler sur un composant de type onglet
 * 
 * @author Alexis Tual
 *
 */
public class Tab {

    private String id;
    private String libelle;
    private Tabs tabs;
    private boolean disabled;

    /**
     * @param id l'identifiant du modèle
     * @param libelle le libellé
     */
    public Tab(String id, String libelle) {
        super();
        this.id = id;
        this.libelle = libelle;
        disable();
    }

    @Override
    public String toString() {
        return libelle;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(getId(), getLibelle());
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Tab other = (Tab) obj;
        return Objects.equal(getId(), other.getId())
                && Objects.equal(getLibelle(), other.getLibelle());
    }
 
    public boolean isSelected() {
        return tabs.isTabSelected(this);
    }
    
    /**
     * Sélectionne ou non cet onglet
     * @param value le flag
     */
    public void setSelected(boolean value) {
        tabs.selectTab(this);
    }
    
    public boolean isDisabled() {
        return disabled;
    }
    
    /**
     * désactive cet onglet
     */
    public void disable() {
        this.disabled = true;
    }
    
    /**
     * active cet onglet
     */
    public void enable() {
        this.disabled = false;
    }
    
    public String getId() {
        return id;
    }
    
    public String getLibelle() {
        return libelle;
    }

    public void setTabs(Tabs tabs) {
        this.tabs = tabs;
    }
    
}
