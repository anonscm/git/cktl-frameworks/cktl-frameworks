package org.cocktail.fwkcktlscolpedagui.serveur.components;

import java.text.Format;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.DureeCentiemesEnHeuresCentiemesFormat;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.DureeCentiemesEnHeuresMinutesFormat;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSNumberFormatter;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

/**
 * Composant ancetre de tous les composants des applications scolarité
 * @author lilia drouot
 */
public class ScolPedaComponent extends CktlAjaxWOComponent {
	// Private fields
	private static final long serialVersionUID = 1L;
	public static final String BINDING_EDITING_CONTEXT = "editingContext";
	protected static final Integer NUMBER_OF_OBJECT_PER_BATCH = 15;
	
	private EOEditingContext edc = null;

	private NSNumberFormatter app2DecimalesFormatter;
	private NSNumberFormatter integerNumberFormatter;
	private Format formatterHeures;

	// Constructor
	/**
	 * @param context : contexte d'édition
	 */
	public ScolPedaComponent(WOContext context) {
		super(context);

	}
	
	/**
	 * @return Le binding <i>editingContext</i> s'il est renseigne, sinon l'editingContext de la session. Cette methode peut etre surchargee pour renvoyer un
	 *         nestedEditingContext.
	 */
	@Override public EOEditingContext edc() {
		if (edc == null) {
			if (hasBinding(BINDING_EDITING_CONTEXT)) {
				edc = (EOEditingContext) valueForBinding(BINDING_EDITING_CONTEXT);
			} else {
				edc = ERXEC.newEditingContext();
			}
		}
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	// Properties
	/**
	 * @return formatteur pour les données entières simple
	 */
	public NSNumberFormatter getIntegerNumberFormatter() {
		if (integerNumberFormatter == null) {
			integerNumberFormatter = new NSNumberFormatter();
			integerNumberFormatter.setPattern("0");
		}
		return integerNumberFormatter;
	}

	/**
	 * @return Formatteur a deux decimales a utiliser pour les donnees numeriques non monetaires
	 */
	public NSNumberFormatter getApp2DecimalesFormatter() {
		if (app2DecimalesFormatter == null) {
			app2DecimalesFormatter = new NSNumberFormatter();
			app2DecimalesFormatter.setDecimalSeparator(",");
			app2DecimalesFormatter.setThousandSeparator(" ");

			app2DecimalesFormatter.setHasThousandSeparators(true);
			app2DecimalesFormatter.setPattern("#,##0.00;0,00");
		}
		return app2DecimalesFormatter;
	}

	/**
	 * @return Formatteur pour l'affichage des données horaires
	 */
	public Format getFormatterHeures() {
		if (formatterHeures == null) {
			if (booleanValueForBinding("isAffichageHeuresMinutes", false)) {
				formatterHeures = new DureeCentiemesEnHeuresMinutesFormat();
			} else {			
				formatterHeures = new DureeCentiemesEnHeuresCentiemesFormat();
			}
		}
		
		return formatterHeures;
	}

	// Public Methods	
	
	/**
	 * @param key :identifiant de la chaine à localiser
	 * @return chaine localisée
	 */
	public String localisation(String key) {		
		return mySession().localizer().localizedStringForKey(key);
	}
	
	// Protected Methods
	// Private methods

}
