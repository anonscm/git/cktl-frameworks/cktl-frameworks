package org.cocktail.fwkcktlscolpedagui.serveur.components.providers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.scol.maquette.ComposantRead;
import org.cocktail.scol.maquette.ComposantReadRepository;

import com.mysema.query.types.Predicate;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 *  Alimente la liste des composants
 */
public abstract class ComposantsProvider implements CktlAjaxSelect2RemoteDataProvider {
	
	private NSArray<ComposantRead> composants;
	private ComposantReadRepository composantRepository;
	private EOTypeComposant typeComposant;
	private Integer anneeSelection;
	private Boolean isEnModelisation;
	private Predicate predicate;

	/**
	 * @param edc editingContext de travail
	 * @param composantRepository classe de service qui fournit les composants
	 * @param anneeSelection  : annee de selection des composants
	 * @param isEnModelisation : indique si on selectionne les diplomes se trouvant en modelisation ou non
	 * @param typeComposant : le type du composant sur laquelle s'effectue la recherche de selection
	 */
	public ComposantsProvider(
			ComposantReadRepository composantRepository, 
			Integer anneeSelection, 
			Boolean isEnModelisation, 
			EOTypeComposant typeComposant,
			Predicate predicate) {
		setComposantReadRepository(composantRepository);
		setTypeComposant(typeComposant);
		setAnneeSelection(anneeSelection);
		setIsEnModelisation(isEnModelisation);
		this.predicate = predicate;
	}
	
	public void setComposantReadRepository(ComposantReadRepository composantRepository) {
		this.composantRepository = composantRepository;
	}
		
	public ComposantReadRepository getComposantRepository() {
		return composantRepository;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<Result> results(String searchTerm) {
		List<Result> resultat = new ArrayList<Result>();
		EOQualifier qualifierFiltre = null;
		NSArray<ComposantRead> filteredComposants = getComposants();
		if (StringUtils.isNotBlank(searchTerm)) {
			qualifierFiltre = ERXQ.contains(ComposantRead.LIBELLE_KEY, searchTerm).or(ERXQ.contains(ComposantRead.CODE_KEY, searchTerm));
			filteredComposants = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(getComposants(), qualifierFiltre);
		}
		for (ComposantRead composant : filteredComposants) {
			resultat.add(getResult(composant));
		}
		return resultat;
	}

	/**
	 * {@inheritDoc}
	 */
	public Result onSelect(String idSelection) {
		if (StringUtils.isEmpty(idSelection)) {
			setSelectedComposant(null);
			return null;
		}
		ComposantRead composant = new ComposantRead(Long.parseLong(idSelection));
		setSelectedComposant(composant);
		return getResult(composant);
	}

	/**
	 * {@inheritDoc}
	 */
	public Result selectionInitiale() {
		if (getSelectedComposant() == null) {
			return null;
		} else {
			return getResult(getSelectedComposant());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public abstract ComposantRead getSelectedComposant();

	/**
	 * {@inheritDoc}
	 */
	public abstract void setSelectedComposant(ComposantRead composant);

	private NSArray<ComposantRead> getComposants() {
		if (composants == null) {
			composants = new NSMutableArray<ComposantRead>();
			if (getTypeComposant() != null) {
				Long annee = Long.valueOf(getAnneeSelection());
				if (isEnModelisation() == null || !isEnModelisation()) {
					List<ComposantRead> composantsRecuperes = getComposantRepository().getComposantsMaquettage(annee, predicate, getTypeComposant());
					composants.addAll(composantsRecuperes);
				}
				else if (isEnModelisation()) {
					List<ComposantRead> composantsRecuperes = getComposantRepository().getComposantsModelisation(annee, predicate, getTypeComposant());
					composants.addAll(composantsRecuperes);
				}
			}

		}
		return ERXArrayUtilities.arrayWithoutDuplicates(composants);

	}

	private Result getResult(ComposantRead composant) {
		return new Result(composant.getId().toString(), composant.getCode() + " - " + composant.getLibelleLong());
	}

	public ITypeComposant getTypeComposant() {
		return typeComposant;
	}

	public void setTypeComposant(EOTypeComposant typeComposant) {
		this.typeComposant = typeComposant;
	}

	public Integer getAnneeSelection() {
		return anneeSelection;
	}

	public void setAnneeSelection(Integer anneeSelection) {
		this.anneeSelection = anneeSelection;
	}
	
	public Boolean isEnModelisation() {
		return isEnModelisation;
	}
	
	public void setIsEnModelisation(Boolean isEnModelisation) {
		this.isEnModelisation = isEnModelisation;
	}

}
