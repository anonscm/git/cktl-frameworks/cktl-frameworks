package org.cocktail.fwkcktlscolpedagui.serveur.components.providers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.scol.maquette.AERead;
import org.cocktail.scol.maquette.AEReadRepository;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Fournit une liste de composants de type AE
 */
public abstract class AEsProvider implements CktlAjaxSelect2RemoteDataProvider {
	private List<AERead> aeRead;
	private ImmutableMap<Long, AERead> mapAERead;
	private AEReadRepository aeReadRepository;

	private Integer anneeSelection;
	private Boolean isEnModelisation;

	/**
	 * @param aeReadRepository : service permettant de recuperer une liste d'AE
	 * @param anneeSelection : annee de selection des composants
	 * @param isEnModelisation : indique si on selectionne les diplomes se trouvant en modelisation ou non
	 */
	public AEsProvider(AEReadRepository aeReadRepository, Integer anneeSelection, Boolean isEnModelisation) {
		setAeReadRepository(aeReadRepository);
		setAnneeSelection(anneeSelection);
		setIsEnModelisation(isEnModelisation);
	}

	/**
	 * @return le composant de type AE sélectionné
	 */
	public abstract AERead getSelectedAERead();

	/**
	 * @param aeRead composant de type AE sélectionné
	 */
	public abstract void setSelectedAERead(AERead aeRead);

	public Integer getAnneeSelection() {
		return anneeSelection;
	}

	public void setAnneeSelection(Integer anneeSelection) {
		this.anneeSelection = anneeSelection;
	}

	public boolean isEnModelisation() {
		return isEnModelisation != null && isEnModelisation.booleanValue();
	}

	public void setIsEnModelisation(Boolean isEnModelisation) {
		this.isEnModelisation = isEnModelisation;
	}

	public AEReadRepository getAeReadRepository() {
		return aeReadRepository;
	}

	public void setAeReadRepository(AEReadRepository aeReadRepository) {
		this.aeReadRepository = aeReadRepository;
	}

	public ImmutableMap<Long, AERead> getMapAERead() {
	  return mapAERead;
  }
	
	/**
	 * @return liste des AE fournies par le service
	 */
	public List<AERead> getAEs() {
		if (aeRead == null) {
			aeRead = aeReadRepository.getAEs(new Long(getAnneeSelection()), isEnModelisation());
			mapAERead = Maps.uniqueIndex(aeRead, new Function<AERead, Long>() {
				public Long apply(AERead input) {
					return input.getIdComposant();
				}
			});
		}
		return aeRead;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Result> results(String searchTerm) {
		List<Result> resultat = new ArrayList<Result>();

		List<AERead> aEFiltres = getAEs();
		if (StringUtils.isNotBlank(searchTerm)) {
			aEFiltres = Lists.newArrayList(Collections2.filter(aEFiltres, buildContainsIgnoreCaseNomAffichage(searchTerm)));
		}

		for (AERead ae : aEFiltres) {
			resultat.add(getResult(ae));
		}
		return resultat;
	}

	private Result getResult(AERead aeRead) {
		String libelleAffichage = aeRead.getCodeTypeAE() + " - " + aeRead.getLibelleAE();
		return new Result(aeRead.getIdComposant().toString(), libelleAffichage);
	}

	/**
	 * Filtre par nom d'affichage (case insensitive)
	 * @param searchTerm terme recherché
	 * @return Predicate<IStructure>
	 */
	private Predicate<AERead> buildContainsIgnoreCaseNomAffichage(final String searchTerm) {

		return new Predicate<AERead>() {
			public boolean apply(AERead aeRead) {
				return StringUtils.containsIgnoreCase(aeRead.getLibelleAE(), searchTerm) || StringUtils.containsIgnoreCase(aeRead.getCodeTypeAE(), searchTerm);
			}
		};

	}

	/**
	 * {@inheritDoc}
	 */
	public Result onSelect(String idSelection) {
		// cas de la suppression
		if (StringUtils.isEmpty(idSelection)) {
			setSelectedAERead(null);
			return null;
		}
		AERead aeRead = mapAERead.get(Long.parseLong(idSelection));
		setSelectedAERead(aeRead);
		return getResult(aeRead);
	}

	/**
	 * {@inheritDoc}
	 */
	public Result selectionInitiale() {
		if (getSelectedAERead() == null) {
			return null;
		} else {
			return getResult(getSelectedAERead());
		}
	}
}
