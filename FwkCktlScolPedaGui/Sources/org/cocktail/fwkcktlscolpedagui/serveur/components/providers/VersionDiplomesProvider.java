package org.cocktail.fwkcktlscolpedagui.serveur.components.providers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ComposantRepository;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Produite un ensemble de version de de diplomes au format utilisable par le composant VersionDiplomesSelect2Remote
 */
public abstract class VersionDiplomesProvider implements CktlAjaxSelect2RemoteDataProvider {
	private EOEditingContext edc;
	private NSArray<EOVersionDiplome> versionDiplomes;
	private ComposantRepository composantRepository;
	private Integer anneeSelection;

	/**
	 * @param edc editingContext de travail
	 * @param composantRepository classe de service qui fournit les composants
	 * @param anneeSelection  : annee de selection des composants
	 */
	public VersionDiplomesProvider(EOEditingContext edc, ComposantRepository composantRepository, Integer anneeSelection) {
		this.edc = edc;
		setComposantRepository(composantRepository);
		setAnneeSelection(anneeSelection);
	}

	public void setComposantRepository(ComposantRepository composantRepository) {
	  this.composantRepository = composantRepository;
  }
	
	public ComposantRepository getComposantRepository() {
	  return composantRepository;
  }
	
	/**
	 * {@inheritDoc}
	 */
	public List<Result> results(String searchTerm) {
		List<Result> resultat = new ArrayList<Result>();

		EOQualifier qualifierFiltre = null;
		if (StringUtils.isNotBlank(searchTerm)) {
			qualifierFiltre = EOVersionDiplome.DIPLOME.dot(EODiplome.LIBELLE).contains(searchTerm)
			    .or(EOVersionDiplome.DIPLOME.dot(EODiplome.CODE).contains(searchTerm));
		}
		EOQualifier qualifier = ERXQ.and(getQualifier(), qualifierFiltre);

		for (EOVersionDiplome versiondiplome : ERXArrayUtilities.filteredArrayWithQualifierEvaluation(getVersionDiplomes(), qualifier)) {
			resultat.add(getResult(versiondiplome));
		}

		return resultat;
	}

	/**
	 * {@inheritDoc}
	 */
	public Result onSelect(String idSelection) {
		// cas de la suppression
		if (StringUtils.isEmpty(idSelection)) {
			setSelectedVersionDiplome(null);
			return null;
		}
		EOVersionDiplome versionDiplome = EOVersionDiplome.fetchSco_VersionDiplome(edc, EOVersionDiplome.ID.eq(Integer.parseInt(idSelection)));
		if (versionDiplome == null) {
			return null;
		} else {
			setSelectedVersionDiplome(versionDiplome);
			return getResult(versionDiplome);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Result selectionInitiale() {
		if (getSelectedVersionDiplome() == null) {
			return null;
		} else {
			return getResult(getSelectedVersionDiplome());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public abstract EOVersionDiplome getSelectedVersionDiplome();

	/**
	 * {@inheritDoc}
	 */
	public abstract void setSelectedVersionDiplome(EOVersionDiplome versionDiplome);

	/**
	 * {@inheritDoc}
	 */
	public abstract EOQualifier getQualifier();

	private NSArray<EOVersionDiplome> getVersionDiplomes() {
		if (versionDiplomes == null) {
			List<IComposant> composants =  getComposantRepository().getListeComposants(EOTypeComposant.typeVersionDiplome(edc), getAnneeSelection());
			versionDiplomes = new NSMutableArray<EOVersionDiplome>();
			for (IComposant composant : composants) {
				versionDiplomes.add((EOVersionDiplome) composant);
			}
		}

		return versionDiplomes;
	}

	private Result getResult(EOVersionDiplome versionDiplome) {
		return new Result(versionDiplome.id().toString(), versionDiplome.getDiplome().code() + " - " + versionDiplome.getDiplome().libelle());
	}

	public Integer getAnneeSelection() {
	  return anneeSelection;
  }

	public void setAnneeSelection(Integer anneeSelection) {
	  this.anneeSelection = anneeSelection;
  }

}
