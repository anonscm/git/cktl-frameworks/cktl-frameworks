package org.cocktail.fwkcktlscolpedagui.serveur.components.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ComposantRepository;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXKey;
import er.extensions.foundation.ERXArrayUtilities;

public abstract class ParcoursDiplomesProvider implements CktlAjaxSelect2RemoteDataProvider {

	private List<IParcours> parcours;
	private EOEditingContext edc;
	private Integer anneeSelection;
	
	/**
	 * @param edc editingContext de travail
	 * @param anneeSelection  : annee de selection des composants
	 */
	public ParcoursDiplomesProvider(EOEditingContext edc, Integer anneeSelection) {
		this.edc = edc;
		setAnneeSelection(anneeSelection);
	}
	

	
	public Integer getAnneeSelection() {
		return anneeSelection;
	}

	public void setAnneeSelection(Integer anneeSelection) {
		this.anneeSelection = anneeSelection;
	}
	
	public abstract IParcours getSelectedParcours();
	
	public abstract void setSelectedParcours(IParcours parcours);

	
	public List<Result> results(final String searchTerm) {
		List<Result> resultat = new ArrayList<Result>();

		Collection<IParcours> listeParcours = getParcours();
		
		if (StringUtils.isNotBlank(searchTerm)) {
			listeParcours = Collections2.filter(listeParcours, new Predicate<IParcours>() {
				public boolean apply(IParcours parcours) {
					return StringUtils.containsIgnoreCase(parcours.code(), searchTerm) || StringUtils.containsIgnoreCase(parcours.libelle(), searchTerm);
				}
			}); 
		}
		
		
		for (IParcours parcours : listeParcours) {
			resultat.add(getResult(parcours));
		}

		return resultat;
	}

	private List<IParcours> getParcours() {
		if (parcours == null) {
			EOQualifier q = 
				EOLien._PARENT_VERSION_DIPLOME.dot(EOVersionDiplome.ANNEE).eq(getAnneeSelection())
					.and(EOLien.CHILD.dot(EOComposant.TYPE_COMPOSANT).eq(EOTypeComposant.typeParcours(edc)))
					.and(EOLien._CHILD_PARCOURS.prefix(getQualifier()));
			ERXFetchSpecification<EOLien> fs = new ERXFetchSpecification<EOLien>(EOLien.ENTITY_NAME, q, null);
			NSArray<String> prefetchKeyPaths = new NSMutableArray<String>();
			prefetchKeyPaths.add(EOLien.CHILD.key());
			fs.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
			List<? extends ILien> liens = ERXArrayUtilities.arrayWithoutDuplicates(fs.fetchObjects(edc));			
			parcours = Lists.transform(liens, new Function<ILien, IParcours>() {
				public IParcours apply(ILien lien) {
					return (IParcours) lien.child();
				}
			});
		}
		return parcours;
	}

	public abstract EOQualifier getQualifier();

	
	private Result getResult(IParcours parcours) {
		return new Result(parcours.id().toString(), parcours.code() + " - " + parcours.libelle());
	}
	
	public Result onSelect(String idSelection) {
		if (StringUtils.isEmpty(idSelection)) {
			setSelectedParcours(null);
			return null;
		} else {
			IParcours parcours  = EOParcours.fetchSco_Parcours(edc, EOParcours.ID.eq(Integer.valueOf(idSelection)));
			if (parcours == null) {
				return null;
			} else {
				setSelectedParcours(parcours);
				return getResult(parcours);
			}
		}
	}

	public Result selectionInitiale() {
		if (getSelectedParcours() == null) {
			return null;
		} else {
			return getResult(getSelectedParcours());
		}
	}

}
