package org.cocktail.fwkcktlscolpedagui.serveur.components.providers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXArrayUtilities;

/**
 * Alimente la liste des responsables d'un composant
 */
public abstract class ResponsablesComposantProvider implements CktlAjaxSelect2RemoteDataProvider {
	private EOEditingContext editingContext;
	private static final Integer MAX_SEARCH_RESULT = 100;

	/**
	 * @param editingContext l'editingContext
	 */
	public ResponsablesComposantProvider(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * @param searchTerm le mot clef
	 * @return la liste des individus dont le nom ou le prénom contient la chaine passée.
	 * 
	 * @see EOIndividu#individusInternesWithNameLikeOrFirstNameLike(EOEditingContext, String, int)
	 * 
	 */
	public List<Result> results(String searchTerm) {
		List<Result> resultat = new ArrayList<Result>();
		NSArray<EOIndividu> individus = getIndividus(searchTerm);
		for (IPersonne individu : individus) {
			resultat.add(new Result(individu.persId().toString(), individu.getNomPrenomAffichage()));
		}

		
    try {
	  	EOQualifier qualifierOptionnel = EOStructure.C_STRUCTURE_PERE.eq(EOStructureForGroupeSpec.getStructurePourArchivage(editingContext).cStructure());
			NSArray<EOStructure> structures = EOStructure.structuresByNameAndSigle(editingContext, searchTerm, null, qualifierOptionnel, MAX_SEARCH_RESULT);
			
			structures = ERXArrayUtilities.arrayWithoutDuplicates(structures);
			for (IPersonne structure : structures) {
				 resultat.add(new Result(structure.persId().toString(), structure.getNomPrenomAffichage()));
			}
    } catch (Exception e) {
	    e.printStackTrace();
    }
	
		return resultat;
	}

	private NSArray<EOIndividu> getIndividus(String searchTerm) {
		EOQualifier qualifierNonEtudiant = EOIndividu.TO_COMPTES.dot(EOCompte.TO_VLANS).dot(EOVlans.C_VLAN_KEY).ne(EOVlans.VLAN_E);
		NSArray<EOIndividu> individus = EOIndividu.individusInternesWithNameLikeOrFirstNameLikeWithQualifier(editingContext, searchTerm, qualifierNonEtudiant,
		    MAX_SEARCH_RESULT);
		return individus;
	}

	/**
	 * @param idSelection l'id de la liste selectionné par l'utilisateur
	 * @return l'individu correspondant à l'id sélectionné
	 */
	public Result onSelect(String idSelection) {
		// cas de la suppression
		if (StringUtils.isEmpty(idSelection)) {
			setSelectedPersonne(null);
			return null;
		}
		EOIndividu individu = EOIndividu.fetchByKeyValue(editingContext, EOIndividu.PERS_ID_KEY, Integer.valueOf(idSelection));
		if (individu != null) {
			setSelectedPersonne(individu);
			return new Result(individu.persId().toString(), individu.getNomPrenomAffichage());
		}

		EOStructure structure = EOStructure.fetchByKeyValue(editingContext, EOStructure.PERS_ID_KEY, Integer.valueOf(idSelection));
		if (structure != null) {
			setSelectedPersonne(structure);
			return new Result(structure.persId().toString(), structure.getNomPrenomAffichage());
		}
		return null;
	}

	/**
	 * @return la sélection initiale
	 */
	public Result selectionInitiale() {
		if (getSelectedPersonne() == null) {
			return null;
		} else {
			return new Result(getSelectedPersonne().persId().toString(), getSelectedPersonne().getNomPrenomAffichage());
		}
	}

	/**
	 * @param individu l'individu qui doit être sélectionné
	 */
	public abstract void setSelectedPersonne(IPersonne individu);

	/**
	 * @return l'individu sélectionné
	 */
	public abstract IPersonne getSelectedPersonne();

}
