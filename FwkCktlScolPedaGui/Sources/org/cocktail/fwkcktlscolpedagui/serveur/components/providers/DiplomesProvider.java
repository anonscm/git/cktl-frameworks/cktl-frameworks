package org.cocktail.fwkcktlscolpedagui.serveur.components.providers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.scol.maquette.DiplomeRead;
import org.cocktail.scol.maquette.DiplomeReadService;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;


/**
 * Fournit une liste de diplomes
 */
public abstract class DiplomesProvider implements CktlAjaxSelect2RemoteDataProvider {
	
	private EOEditingContext edc;
	private List<DiplomeRead> diplomes;
	private DiplomeReadService diplomeReadService;
	private Integer anneeSelection;
	private Boolean isEnModelisation;

	/**
	 * @param edc editingContext de travail
	 * @param diplomeReadService classe de service qui fournit les composants
	 * @param anneeSelection  : annee de selection des composants
	 * @param isEnModelisation : indique si on selectionne les diplomes se trouvant en modelisation ou non
	 */
	public DiplomesProvider(EOEditingContext edc, DiplomeReadService diplomeReadService, Integer anneeSelection, Boolean isEnModelisation) {
		this.edc = edc;
		this.diplomeReadService = diplomeReadService;
		setAnneeSelection(anneeSelection);
		setIsEnModelisation(isEnModelisation);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<Result> results(String searchTerm) {
		List<Result> resultat = new ArrayList<Result>();
		
		List<DiplomeRead> diplomesFiltres = getDiplomes();
		if (StringUtils.isNotBlank(searchTerm)) {
			diplomesFiltres = Lists.newArrayList(Collections2.filter(diplomesFiltres, buildContainsIgnoreCaseNomAffichage(searchTerm)));
		}

		for (DiplomeRead diplome : diplomesFiltres) {
			resultat.add(getResult(diplome));
		}

		return resultat;
	}

	/**
	 * Filtre par nom d'affichage (case insensitive)
	 * @param searchTerm terme recherché
	 * @return Predicate<IStructure>
	 */
	private Predicate<DiplomeRead> buildContainsIgnoreCaseNomAffichage(final String searchTerm) {
		
		return new Predicate<DiplomeRead>() {
			public boolean apply(DiplomeRead diplome) {
				return StringUtils.containsIgnoreCase(diplome.getLibelle(), searchTerm) || 
					   StringUtils.containsIgnoreCase(diplome.getCode(), searchTerm);
			}
		};
		
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Result onSelect(String idSelection) {
		// cas de la suppression
		if (StringUtils.isEmpty(idSelection)) {
			setSelectedDiplome(null);
			return null;
		}
		EODiplome diplome = EODiplome.fetchSco_Diplome(edc, EODiplome.ID.eq(Integer.parseInt(idSelection)));
		if (diplome == null) {
			return null;
		} else {
			setSelectedDiplome(diplome);
			DiplomeRead diplomeRead = diplomeToDiplomeRead(diplome);
			return getResult(diplomeRead);
		}
	}

	private DiplomeRead diplomeToDiplomeRead(EODiplome diplome) {
		DiplomeRead diplomeRead = new DiplomeRead(Long.valueOf(diplome.id()), diplome.code(), diplome.libelle(), null);
		return diplomeRead;
	}

	/**
	 * {@inheritDoc}
	 */
	public Result selectionInitiale() {
		if (getSelectedDiplome() == null) {
			return null;
		} else {
			DiplomeRead diplome = diplomeToDiplomeRead(getSelectedDiplome());
			return getResult(diplome);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public abstract EODiplome getSelectedDiplome();

	/**
	 * {@inheritDoc}
	 */
	public abstract void setSelectedDiplome(EODiplome diplome);

	/**
	 * {@inheritDoc}
	 */
	public abstract EOQualifier getQualifier();

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<DiplomeRead> getDiplomes() {
		if (diplomes == null) {
			diplomes = diplomeReadService.getDiplomes(new Long(getAnneeSelection()), isEnModelisation());
		}
		return diplomes;
	}

	private Result getResult(DiplomeRead diplome) {
		String libelleAffichage = diplome.getCode() + " - " + diplome.getLibelle();
		
		if (isEnModelisation()) {
			if (EOStatutFormation.STATUT_EP_CODE.equals(diplome.getCodeStatutFormation())) {
				libelleAffichage += " (P)";
			}
		}
		
		return new Result(diplome.getId().toString(), libelleAffichage);
	}

	public Integer getAnneeSelection() {
		return anneeSelection;
	}

	public void setAnneeSelection(Integer anneeSelection) {
		this.anneeSelection = anneeSelection;
	}
	
	public boolean isEnModelisation() {
		return isEnModelisation != null && isEnModelisation.booleanValue();
	}
	
	public void setIsEnModelisation(Boolean isEnModelisation) {
		this.isEnModelisation = isEnModelisation;
	}
	
}
