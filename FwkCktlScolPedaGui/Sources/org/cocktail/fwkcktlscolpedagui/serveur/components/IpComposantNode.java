package org.cocktail.fwkcktlscolpedagui.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.cocktail.fwkcktlscolpedagui.serveur.components.treetable.ComposantNodeVisitor;

/**
 * 
 * @author jlafourc
 *
 */
public class IpComposantNode extends ComposantNode {

	
	private Boolean cochable; 

	private Boolean isComposantParcoursFilsSemestre;
	private Boolean isComposantAvecParentParcoursFilsSemestreChecked = false;
	private Boolean isComposantAvecParentParcoursFilsSemestre = false;

	private IInscriptionPedagogiqueElement ipElement;
	
	/**
	 * Constructueur obligatoire
	 * @param data le composant
	 * @param parent le parent
	 * @param visible est visible ?
	 */
	public IpComposantNode(IComposant data, CktlTreeTableNode<IComposant> parent, Boolean visible) {
		super(data, parent, visible);
	}
	
	@Override
	protected IpComposantNode getNewInstance(IComposant data, CktlTreeTableNode<IComposant> parent, Boolean visible) {
		return new IpComposantNode(data, parent, visible);
	}
	
	@Override
	public void accept(ComposantNodeVisitor visitor) {
		setVisitor(visitor);
		if (visitor != null) {
			visitor.visit(this);
		}
	}

	
	public void makeVisitorVisitNodeAndChildren() {
		getVisitor().visit(this);
		makeVisitorVisitChildren();
	}
	
	public void makeVisitorVisitChildren() {
		for (CktlTreeTableNode<IComposant> node : getChildren()) {
			IpComposantNode n = (IpComposantNode) node;
			getVisitor().visit(n);
			n.makeVisitorVisitChildren();
		}
	}
	
	
	public Boolean isCochable() {
		return cochable;
	}
	
	public void setCochable(Boolean cochable) {
		this.cochable = cochable;
	}

	public Boolean isComposantParcoursFilsSemestre() {
		return isComposantParcoursFilsSemestre;
	}

	public void setIsComposantParcoursFilsSemestre(Boolean isComposantParcoursFilsSemestre) {
		this.isComposantParcoursFilsSemestre = isComposantParcoursFilsSemestre;
		setIsComposantAvecParentParcoursFilsSemestre(true);
	}


	public Boolean isComposantAvecParentParcoursFilsSemestreChecked() {
		return isComposantAvecParentParcoursFilsSemestreChecked;
	}

	public void setIsComposantAvecParentParcoursFilsSemestreChecked(Boolean isComposantAvecParentParcoursFilsSemestreChecked) {
		this.isComposantAvecParentParcoursFilsSemestreChecked = isComposantAvecParentParcoursFilsSemestreChecked;
		/** Si le parcours semestre parent n'est plus coché, on supprime l'ipElement **/
		
		setChildrenIsComposantAvecParentParcoursFilsSemestreChecked(isComposantAvecParentParcoursFilsSemestreChecked);
	}


	public void setChildrenIsComposantAvecParentParcoursFilsSemestreChecked(Boolean value) {
		for (CktlTreeTableNode<IComposant> node : getChildren()) {
			IpComposantNode n = (IpComposantNode) node;
			n.setIsComposantAvecParentParcoursFilsSemestreChecked(value);
		}
	}

	public Boolean isDisabled() {
		return isComposantAvecParentParcoursFilsSemestre && !isComposantAvecParentParcoursFilsSemestreChecked;
	}
	
	public Boolean isChecked() {
		return ipElement != null;
	}

	public Boolean isComposantAvecParentParcoursFilsSemestre() {
		return isComposantAvecParentParcoursFilsSemestre;
	}

	public void setIsComposantAvecParentParcoursFilsSemestre(Boolean isComposantAvecParentParcoursFilsSemestre) {
		this.isComposantAvecParentParcoursFilsSemestre = isComposantAvecParentParcoursFilsSemestre;
		setChildrenIsComposantAvecParentParcoursFilsSemestre(isComposantAvecParentParcoursFilsSemestre);

	}

	public void setChildrenIsComposantAvecParentParcoursFilsSemestre(Boolean value) {
		for (CktlTreeTableNode<IComposant> node : getChildren()) {
			IpComposantNode n = (IpComposantNode) node;
			n.setIsComposantAvecParentParcoursFilsSemestre(value);
		}
	}

	public IInscriptionPedagogiqueElement getIpElement() {
		return ipElement;
	}

	public void setIpElement(IInscriptionPedagogiqueElement ipElement) {
		this.ipElement = ipElement;
	}
	
	
	@Override
	public Boolean isExpanded() {
		if(!isChecked() && getLien().child().isTypeOf(IUE.class)) {
			return false;
		}
		return true;
	}
}
