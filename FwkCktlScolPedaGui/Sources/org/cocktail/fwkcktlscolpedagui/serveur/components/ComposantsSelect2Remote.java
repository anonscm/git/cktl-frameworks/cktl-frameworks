package org.cocktail.fwkcktlscolpedagui.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpedagui.serveur.components.providers.ComposantsProvider;
import org.cocktail.scol.maquette.ComposantRead;
import org.cocktail.scol.maquette.ComposantReadRepository;

import com.mysema.query.types.Predicate;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * Liste déroulante permettant de sélectionner un composant
 */
public class ComposantsSelect2Remote extends CktlAjaxWOComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_SELECTION = "selection";
	private static final String BINDING_DISABLED = "disabled";
	private static final String BINDING_PREDICATE = "qualifier";
	private static final String BINDING_STYLE = "style";
	private static final String BINDING_TYPE_COMPOSANT = "typeComposant";
	private static final String BINDING_ANNEE_SELECTION = "anneeSelection";
	private static final String BINDING_IS_EN_MODELISATION = "isEnModelisation";
	private static final String BINDING_COMPOSANT_REPOSITORY = "composantRepository";

	private ComposantsProvider composantsDataProvider;

	/**
	 * @param context : contexte d'édition
	 */
	public ComposantsSelect2Remote(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		composantsDataProvider = null;

		super.appendToResponse(response, context);
	}

	@Override
	public EOEditingContext edc() {
		return (EOEditingContext) valueForBinding("editingContext");
	}

	/**
	 * @return le provider de diplomes
	 */
	public ComposantsProvider getComposantsDataProvider() {
		if (composantsDataProvider == null) {
			composantsDataProvider = new ComposantsProvider(getComposantRepository(), getAnneeSelection(), isEnModelisation(), getTypeComposant(), getPredicate()) {

				@Override
				public ComposantRead getSelectedComposant() {
					return toComposantRead(getSelection());
				}

				@Override
				public void setSelectedComposant(ComposantRead composant) {
					setSelection(toEo(composant));
				}

			};
		}
		return composantsDataProvider;
	}

	protected ComposantRead toComposantRead(IComposant selection) {
		ComposantRead composantRead = null;
		if (selection != null) {
			composantRead = new ComposantRead(Long.valueOf(selection.id()));
			composantRead.setCode(selection.code());
			composantRead.setLibelleCourt(selection.libelleCourt());
			composantRead.setLibelleLong(selection.libelle());
		}
		return composantRead;
	}

	private IComposant toEo(ComposantRead composant) {
		IComposant eo = null;
		if (composant != null) {
			eo = EOComposant.fetchById(edc(), composant.getId().intValue());
		}
		return eo;
	}

	private ComposantReadRepository getComposantRepository() {
		return (ComposantReadRepository) valueForBinding(BINDING_COMPOSANT_REPOSITORY);
	}

	public void setComposantsDataProvider(ComposantsProvider composantsDataProvider) {
		this.composantsDataProvider = composantsDataProvider;
	}

	public IComposant getSelection() {
		return (IComposant) valueForBinding(BINDING_SELECTION);
	}

	/**
	 * @param selection la selection
	 */
	public void setSelection(IComposant selection) {
		setValueForBinding(selection, BINDING_SELECTION);
	}

	public Predicate getPredicate() {
		return (Predicate) valueForBinding(BINDING_PREDICATE);
	}

	/**
	 * @return vrai si le composant est inactif
	 */
	public Boolean getDisabled() {
		if (valueForBinding(BINDING_DISABLED) == null) {
			return Boolean.FALSE;
		}
		return (Boolean) valueForBinding(BINDING_DISABLED);
	}

	/**
	 * @param disabled vrai si le composant est inactif
	 */
	public void setDisabled(Boolean disabled) {
		setValueForBinding(disabled, BINDING_DISABLED);
	}

	public String getStyle() {
		return valueForStringBinding(BINDING_STYLE, "");
	}

	/**
	 * @param style : style css du composant
	 */
	public void setStyle(String style) {
		setValueForBinding(style, BINDING_STYLE);
	}

	public EOTypeComposant getTypeComposant() {
		return (EOTypeComposant) valueForBinding(BINDING_TYPE_COMPOSANT);
	}

	/**
	 * @param typeComposant : le type de composant a rechercher
	 */
	public void setTypeComposant(EOTypeComposant typeComposant) {
		setValueForBinding(typeComposant, BINDING_TYPE_COMPOSANT);
	}

	public Integer getAnneeSelection() {
		return (Integer) valueForBinding(BINDING_ANNEE_SELECTION);
	}

	/**
	 * @param anneeSelection : annee de selection des versions des composants
	 */
	public void setAnneeSelection(Integer anneeSelection) {
		setValueForBinding(anneeSelection, BINDING_ANNEE_SELECTION);
	}

	public Boolean isEnModelisation() {
		if (hasBinding(BINDING_IS_EN_MODELISATION)) {
			return (Boolean) valueForBooleanBinding(BINDING_IS_EN_MODELISATION, false);
		} else {
			return null;
		}
	}

	/**
	 * @param isEnModelisation : indique si on affiche les composants visible en modelisation ou pas
	 */
	public void setEnModelisation(Boolean isEnModelisation) {
		setValueForBinding(isEnModelisation, BINDING_IS_EN_MODELISATION);
	}

}