package org.cocktail.fwkcktlscolpedagui.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ComposantRepository;
import org.cocktail.fwkcktlscolpedagui.serveur.components.providers.ParcoursDiplomesProvider;

import com.google.inject.Inject;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * Permet de recherche un parcours parmi les parcours se trouvant$
 * sous les versions de diplome
 */
public class ParcoursDiplomesSelect2Remote extends CktlAjaxWOComponent {
	
	
	private static final String BINDING_SELECTION = "selection";
	private static final String BINDING_ANNEE_SELECTION = "anneeSelection";
	private static final String BINDING_QUALIFIER = "qualifier";
	private static final String BINDING_STYLE = "style";

	
	@Inject
	private ComposantRepository composantRepository;

	private ParcoursDiplomesProvider parcoursDiplomesProvider;
	
    public ParcoursDiplomesSelect2Remote(WOContext context) {
        super(context);
    }
    
    
    public ParcoursDiplomesProvider getParcoursDataProvider() {
    	if (parcoursDiplomesProvider == null) {
    		parcoursDiplomesProvider = new ParcoursDiplomesProvider(edc(), getAnneeSelection()) {
				
				@Override
				public void setSelectedParcours(IParcours parcours) {
					setSelection(parcours);
				}
				
				@Override
				public IParcours getSelectedParcours() {
					return getSelection();
				}

				@Override
				public EOQualifier getQualifier() {
					return (EOQualifier) valueForBinding(BINDING_QUALIFIER);
				}
				
				
			};
    	}
    	
    	return parcoursDiplomesProvider;
    }
    
	public Integer getAnneeSelection() {
		return (Integer) valueForBinding(BINDING_ANNEE_SELECTION);
	}

	public void setAnneeSelection(Integer anneeSelection) {
		setValueForBinding(anneeSelection, BINDING_ANNEE_SELECTION);
	}
	
	public IParcours getSelection() {
		return (IParcours) valueForBinding(BINDING_SELECTION);
	}

	public void setSelection(IParcours selection) {
		setValueForBinding(selection, BINDING_SELECTION);
	}

	public String getStyle() {
		return valueForStringBinding(BINDING_STYLE, "");
	}

	public void setStyle(String style) {
		setValueForBinding(style, BINDING_STYLE);
	}
    
}