package org.cocktail.fwkcktlscolpedagui.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlscolpedagui.serveur.components.providers.ResponsablesComposantProvider;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * Liste déroulante permettant de sélectionner un responsable
 */
public class ResponsablesComposantSelect2Remote extends CktlAjaxWOComponent {
	private static final long serialVersionUID = 1L;
	private static final String BINDING_SELECTION = "selection";
	private static final String BINDING_EDITING_CONTEXT = "editingContext";
	private static final String BINDING_SEARCH_TEXT = "searchText";
	private ResponsablesComposantProvider responsablesComposantProvider;

	/**
	 * @param context contexte d'édition
	 */
	public ResponsablesComposantSelect2Remote(WOContext context) {
		super(context);
	}

	@Override
	public EOEditingContext edc() {
		return (EOEditingContext) valueForBinding(BINDING_EDITING_CONTEXT);
	}
	
	 /**
   * 
   * @return le paysProvider
   */
  public ResponsablesComposantProvider getResponsablesComposantDataProvider() {
      if (responsablesComposantProvider == null) {
          responsablesComposantProvider = new ResponsablesComposantProvider(edc()) {

						@Override
            public void setSelectedPersonne(IPersonne individu) {
	            setSelection(individu);	            
            }
						
						@Override
            public IPersonne getSelectedPersonne() {
	            return getSelection();
            }                         
          };
      }
      return responsablesComposantProvider;
  }
  
  public IPersonne getSelection() {
      return (IPersonne) valueForBinding(BINDING_SELECTION);
  }
  
  /**
   * @param selection la selection
   */
  public void setSelection(IPersonne selection) {
      setValueForBinding(selection, BINDING_SELECTION);
  }
  
  /**
   * @return le texte à afficher dans l'invite de la searchbox
   */
  public String searchText() {
      return stringValueForBinding(BINDING_SEARCH_TEXT, "Rechercher un individu");
  }

}