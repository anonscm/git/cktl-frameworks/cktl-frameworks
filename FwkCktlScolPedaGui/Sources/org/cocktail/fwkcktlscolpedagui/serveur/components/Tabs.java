package org.cocktail.fwkcktlscolpedagui.serveur.components;

import java.util.Arrays;
import java.util.List;

/**
 * Modèle correspondant à une liste d'onglets, à cabler sur un composant de type onglets.
 * @author Alexis Tual
 */
public class Tabs {

	private List<Tab> tabs;
	private Tab selectedTab;

	/**
	 * @param tabs la liste des onglets (l'ordre est important)
	 */
	public Tabs(Tab... tabs) {
		this.tabs = Arrays.asList(tabs);
		for (Tab tab : tabs) {
			tab.setTabs(this);
		}
	}

	/**
	 * @param tab un onglet
	 * @return true si l'onglet est sélectionné
	 */
	public boolean isTabSelected(Tab tab) {
		return selectedTab.equals(tab);
	}

	/**
	 * Sélectionne l'onglet donné
	 * @param selectedTab un onglet
	 */
	public void selectTab(Tab selectedTab) {
		this.selectedTab = selectedTab;
	}

	/**
	 * Sélectionne l'onglet suivant
	 */
	public void selectTabSuivant() {
		int indexSelected = tabs.indexOf(selectedTab);
		int indexSuivant = indexSelected + 1;
		if (indexSuivant <= tabs.size() - 1) {
			selectTab(tabs.get(indexSuivant));
		}
	}

	/**
	 * Sélectionne l'onglet suivant
	 */
	public void selectTabPrecedent() {
		int indexSelected = tabs.indexOf(selectedTab);
		if (indexSelected > 0) {
			selectTab(tabs.get(indexSelected - 1));
		}
	}
	
	public Tab getSelectedTab() {
		return selectedTab;
	}
	
	public void setSelectedTab(Tab selectedTab) {
		selectTab(selectedTab);
	}
	
	public List<Tab> getTabs() {
		return tabs;
	}

}
