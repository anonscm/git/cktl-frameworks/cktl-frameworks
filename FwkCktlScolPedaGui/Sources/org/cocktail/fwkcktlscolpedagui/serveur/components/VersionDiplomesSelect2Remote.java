package org.cocktail.fwkcktlscolpedagui.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ComposantRepository;
import org.cocktail.fwkcktlscolpedagui.serveur.components.providers.VersionDiplomesProvider;

import com.google.inject.Inject;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * Ce composant permet de selectionner une version de diplome dans une liste déroulante
 */
public class VersionDiplomesSelect2Remote extends CktlAjaxWOComponent {
	private static final long serialVersionUID = 1L;
	private static final String BINDING_SELECTION = "selection";
	private static final String BINDING_DISABLED = "disabled";
	private static final String BINDING_QUALIFIER = "qualifier";
	private static final String BINDING_STYLE = "style";
	private static final String BINDING_ANNEE_SELECTION = "anneeSelection";

	@Inject
	private ComposantRepository composantRepository;
	private VersionDiplomesProvider versionDiplomesDataProvider;

	/**
	 * @param context : contexte d'édition
	 */
	public VersionDiplomesSelect2Remote(WOContext context) {
		super(context);
	}

	@Override
    public void appendToResponse(WOResponse response, WOContext context) {
		versionDiplomesDataProvider = null;
    	
    	super.appendToResponse(response, context);
    }
	
	@Override
	public EOEditingContext edc() {
		return (EOEditingContext) valueForBinding("editingContext");
	}

	/**
	 * @return le provider de version de diplomes
	 */
	public VersionDiplomesProvider getVersionDiplomesDataProvider() {
		if (versionDiplomesDataProvider == null) {
			versionDiplomesDataProvider = new VersionDiplomesProvider(edc(), composantRepository, getAnneeSelection()) {

				@Override
				public EOVersionDiplome getSelectedVersionDiplome() {
					return getSelection();
				}

				@Override
				public void setSelectedVersionDiplome(EOVersionDiplome versionDiplome) {
					setSelection(versionDiplome);
				}

				@Override
				public EOQualifier getQualifier() {
					return getQualifierDiplomes();
				}

			};
		}
		return versionDiplomesDataProvider;
	}

	public void setVersionDiplomesDataProvider(VersionDiplomesProvider versionDiplomesDataProvider) {
		this.versionDiplomesDataProvider = versionDiplomesDataProvider;
	}

	public EOVersionDiplome getSelection() {
		return (EOVersionDiplome) valueForBinding(BINDING_SELECTION);
	}

	/**
	 * @param selection la selection
	 */
	public void setSelection(EOVersionDiplome selection) {
		setValueForBinding(selection, BINDING_SELECTION);
	}

	public EOQualifier getQualifierDiplomes() {
		return (EOQualifier) valueForBinding(BINDING_QUALIFIER);
	}

	/**
	 * @return vrai si le composant est inactif
	 */
	public Boolean getDisabled() {
		if (valueForBinding(BINDING_DISABLED) == null) {
			return Boolean.FALSE;
		}
		return (Boolean) valueForBinding(BINDING_DISABLED);
	}

	/**
	 * @param disabled vrai si le composant est inactif
	 */
	public void setDisabled(Boolean disabled) {
		setValueForBinding(disabled, BINDING_DISABLED);
	}

	public String getStyle() {
		return valueForStringBinding(BINDING_STYLE, "");
	}

	/**
	 * @param style : style css du composant
	 */
	public void setStyle(String style) {
		setValueForBinding(style, BINDING_STYLE);
	}

	public Integer getAnneeSelection() {
		return (Integer) valueForBinding(BINDING_ANNEE_SELECTION);
	}

	/**
	 * @param anneeSelection : annee de selection des versions de diplomes
	 */
	public void setAnneeSelection(Integer anneeSelection) {
		setValueForBinding(anneeSelection, BINDING_ANNEE_SELECTION);
	}
}