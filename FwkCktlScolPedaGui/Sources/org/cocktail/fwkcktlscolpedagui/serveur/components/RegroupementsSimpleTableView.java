package org.cocktail.fwkcktlscolpedagui.serveur.components;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EORegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CktlFiltreUtils;
import org.cocktail.scol.maquette.ComposantRead;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;

/**
 * Interface pour l'affichage d'un tableau contenant des Regroupements
 * @author laetitia
 */
public class RegroupementsSimpleTableView extends ScolPedaComponent {
	private static final long serialVersionUID = 1L;

	private static final String BINDING_DISPLAYGROUP = "displayGroup";
	private static final String BINDING_SELECTEDITEM = "selectedItem";
	private static final String BINDING_SHOWFILTERS = "showFilters";
	private static final String BINDING_SHOWTOOLBAR = "showToolbar";
	private static final String BINDING_SHOWTOOLTIP = "showToolTip";
	private static final String BINDING_ISFILTERAUTOMATIC = "isFilterAutomatic";
	private static final String BINDING_USEFIXEDHEADER = "useFixedHeader";
	private static final String BINDING_HEIGHT = "height";
	private static final String BINDING_WIDTH = "width";
	private static final String BINDING_ONSELECT = "onSelect";
	private static final String BINDING_ACTIONUPDATE = "actionUpdate";

	// Variables des filtres de recherche
	private String tokenCodeRegroupement = null;
	private String tokenLibelleCourtRegroupement = null;
	private String tokenLibelleRegroupement = null;
	private String tokenCreditECTS;
	////private String tokenDomaine;
	////private String tokenStructure;
	////private String tokenImplantationGeo;

	// Table de recherche des Regroupements
	private ERXDisplayGroup<ComposantRead> displayGroup = null;
	// private ERXDatabaseDataSource datasource = null;
	private ComposantRead currentItem;
	private ComposantRead selectedItem = null;
	private EOQualifier rechercheQualifier = null;

	/**
	 * Constructeur
	 * @param context : le contexte d'edition
	 */
	public RegroupementsSimpleTableView(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(com.webobjects.appserver.WOResponse response, WOContext context) {
		displayGroup = null;
		filtrer();
		super.appendToResponse(response, context);
	}

	public String getRegroupementsSimpleTableViewContainerId() {
		return getComponentId() + "_regroupementsSimpleTableViewContainerId";
	}

	public String getTableViewContainerId() {
		return getComponentId() + "_tableViewContainerId";
	}

	public String getTableViewId() {
		return getComponentId() + "_tableViewId";
	}

	public String getBoutonsContainerId() {
		return getComponentId() + "_boutonsContainerId";
	}

	@SuppressWarnings("unchecked")
	private ERXDisplayGroup<ComposantRead> displayGroupBinding() {
		return (ERXDisplayGroup<ComposantRead>) valueForBinding(BINDING_DISPLAYGROUP);
	}

	/**
	 * @return Boolean : retourne le showFilters passe en binding
	 */
	public Boolean showFilters() {
		return valueForBooleanBinding(BINDING_SHOWFILTERS, false);
	}

	/**
	 * @return Boolean : retourne le showToolbar passe en binding
	 */
	public Boolean showToolbar() {
		return valueForBooleanBinding(BINDING_SHOWTOOLBAR, false);
	}
	
	/**
	 * @return Boolean : retourne le showToolTip passe en binding
	 */
	public Boolean showToolTip() {
		return valueForBooleanBinding(BINDING_SHOWTOOLTIP, true);
	}
	
	/**
	 * @return Boolean : retourne le isFilterAutomatic passe en binding
	 */
	public Boolean isFilterAutomatic() {
		return valueForBooleanBinding(BINDING_ISFILTERAUTOMATIC, false);
	}
	
	/**
	 * @return Boolean : retourne le useFixedHeader passe en binding
	 */
	public Boolean useFixedHeader() {
		return valueForBooleanBinding(BINDING_USEFIXEDHEADER, true);
	}

	/**
	 * @return String : retourne la taille height passe en binding
	 */
	public String height() {
		return valueForStringBinding(BINDING_HEIGHT, "");
	}

	/**
	 * @return String : retourne la taille width passe en binding
	 */
	public String width() {
		return valueForStringBinding(BINDING_WIDTH, "");
	}

	// ** FILTRES DE RECHERCHE **/
	public String getTokenCodeRegroupement() {
		return tokenCodeRegroupement;
	}

	public void setTokenCodeRegroupement(String tokenCodeRegroupement) {
		this.tokenCodeRegroupement = tokenCodeRegroupement;
	}

	public String getTokenLibelleCourtRegroupement() {
		return tokenLibelleCourtRegroupement;
	}

	public void setTokenLibelleCourtRegroupement(String tokenLibelleCourtRegroupement) {
		this.tokenLibelleCourtRegroupement = tokenLibelleCourtRegroupement;
	}

	public String getTokenLibelleRegroupement() {
		return tokenLibelleRegroupement;
	}

	public void setTokenLibelleRegroupement(String tokenLibelleRegroupement) {
		this.tokenLibelleRegroupement = tokenLibelleRegroupement;
	}

	public String getTokenCreditECTS() {
		return tokenCreditECTS;
	}

	public void setTokenCreditECTS(String tokenCreditECTS) {
		this.tokenCreditECTS = tokenCreditECTS;
	}


	/**
	 * Filtre la liste des regroupements selon la liste des criteres
	 * @return doNothing (reste sur la page)
	 */
	public WOActionResults filtrer() {
		if (displayGroup() != null) {
			String tokenECTSTemp = null;
			if (getTokenCreditECTS() != null) {
				tokenECTSTemp = getTokenCreditECTS().replace(",", ".");
			}

			CktlFiltreUtils filtreUtils = new CktlFiltreUtils();
			displayGroup().setQualifier(ERXQ.and(rechercheQualifier(),
					filtreUtils.getLikeQualifier(ComposantRead.CODE_KEY, getTokenCodeRegroupement()),
					filtreUtils.getLikeQualifier(ComposantRead.LIBELLE_COURT_KEY, getTokenLibelleCourtRegroupement()),
					filtreUtils.getLikeQualifier(ComposantRead.LIBELLE_KEY, getTokenLibelleRegroupement()),
					filtreUtils.getContainsQualifier(ComposantRead.CREDIT_ECTS_NUMBER_KEY, tokenECTSTemp)));

			displayGroup().fetch();
			setSelectedItem(displayGroup().selectedObject());

			actionUpdate();
			
		}

		return doNothing();

	}

	/* SIMPLE TABLE VIEW * */
	public ComposantRead getCurrentItem() {
		return currentItem;
	}

	public void setCurrentItem(ComposantRead currentItem) {
		this.currentItem = currentItem;
	}

	public ComposantRead getSelectedItem() {
		return selectedItem;
	}

	/**
	 * Set le Regroupement en parametre et le set aussi au BINDING_SELECTEDITEM
	 * @param selectedItem : le Regroupement selectionne
	 */
	public void setSelectedItem(ComposantRead selectedItem) {
		this.selectedItem = selectedItem;
		
		EORegroupement regroupement = null;
		if(selectedItem!=null) {
			regroupement = EORegroupement.fetchSco_Regroupement(edc(), EORegroupement.ID.eq(selectedItem.getId().intValue()));
		}
		setValueForBinding(regroupement, BINDING_SELECTEDITEM);
		
	}

	/**
	 * @return boolean : indique si un item est selectionne dans la liste
	 */
	public boolean isSelectedItem() {
		return (getSelectedItem() == null);
	}
	
	public EORegroupement getCurrentRegroupement() {
		EORegroupement regroupement = null;
		if(currentItem!=null) {
			regroupement = EORegroupement.fetchSco_Regroupement(edc(), EORegroupement.ID.eq(currentItem.getId().intValue()));
		}
		
		return regroupement;
	}

	/**
	 * delegate class
	 * handle the current selected item
	 */
	public class DisplayGroupDelegate {

		/**
		 * @param group : le groupe d'objets du tableau
		 */
		public void displayGroupDidChangeSelectedObjects(final WODisplayGroup group) {
			@SuppressWarnings("unchecked")
			ERXDisplayGroup<ComposantRead> _groupe = (ERXDisplayGroup<ComposantRead>) group;
			if (_groupe.selectedObject() != null) {
				setSelectedItem(_groupe.selectedObject());
			} else {
				setSelectedItem(null);
			}
		}
	}

	/**
	 * @return true if the search results are empty
	 */
	public Boolean resultatsRechercheVides() {
		return (displayGroup().allObjects().count() == 0);
	}

	/**
	 * @return EOQualifier : le qualifier de recherche passe en binding
	 */
	public EOQualifier rechercheQualifier() {
		if (rechercheQualifier == null) {
			rechercheQualifier = displayGroupBinding().qualifier();
		}

		return rechercheQualifier;
	}

	public void setRechercheQualifier(EOQualifier rechercheQualifier) {
		this.rechercheQualifier = rechercheQualifier;
	}

	/**
	 * @return ERXDisplayGroup : display group des Regroupements affiches
	 */
	public ERXDisplayGroup<ComposantRead> displayGroup() {
		if (displayGroup == null) {
			displayGroup = new ERXDisplayGroup<ComposantRead>();

			displayGroup.setDataSource(displayGroupBinding().dataSource());
			displayGroup.setDelegate(new DisplayGroupDelegate());

			displayGroup.setSortOrderings(displayGroupBinding().sortOrderings());
			displayGroup.setQualifier(displayGroupBinding().qualifier());
			setRechercheQualifier(displayGroupBinding().qualifier());

			displayGroup.setSelectsFirstObjectAfterFetch(displayGroupBinding().selectsFirstObjectAfterFetch());
			displayGroup.setSelectedObject(displayGroupBinding().selectedObject());
			displayGroup.setNumberOfObjectsPerBatch(displayGroupBinding().numberOfObjectsPerBatch());
			displayGroup.fetch();

		}

		return displayGroup;
	}

	/** ACTIONS **/

	/**
	 * methode appelee lorsqu'on souhaite appelée une fonction d'update
	 * @return null (reste sur la page).
	 */
	public WOActionResults actionUpdate() {
		if (hasBinding(BINDING_ACTIONUPDATE)) {
			return performParentAction(stringValueForBinding(BINDING_ACTIONUPDATE, null));
		} else {
			return doNothing();
		}
	}
	
	/**
	 * methode appelee a la suite du click sur une ligne du tableau.
	 * @return null (reste sur la page).
	 */
	public WOActionResults onSelect() {
		if (hasBinding(BINDING_ONSELECT)) {
			return performParentAction(stringValueForBinding(BINDING_ONSELECT, null));
		} else {
			return doNothing();
		}
	}

}