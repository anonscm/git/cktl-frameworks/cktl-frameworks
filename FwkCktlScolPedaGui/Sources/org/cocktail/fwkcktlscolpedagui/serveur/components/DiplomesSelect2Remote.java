package org.cocktail.fwkcktlscolpedagui.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpedagui.serveur.components.providers.DiplomesProvider;
import org.cocktail.scol.maquette.DiplomeReadService;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;


/**
 * Ce composant permet de selectionner une version de diplome dans une liste déroulante
 */
public class DiplomesSelect2Remote extends CktlAjaxWOComponent {
	
	private static final long serialVersionUID = 1L;
	private static final String BINDING_SELECTION = "selection";
	private static final String BINDING_DISABLED = "disabled";
	private static final String BINDING_QUALIFIER = "qualifier";
	private static final String BINDING_STYLE = "style";
	private static final String BINDING_ANNEE_SELECTION = "anneeSelection";
	private static final String BINDING_IS_EN_MODELISATION = "isEnModelisation";

	private DiplomesProvider diplomesDataProvider;

	
	/**
	 * @param context : contexte d'édition
	 */
    public DiplomesSelect2Remote(WOContext context) {
        super(context);
    }
    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
    	diplomesDataProvider = null;
    	
    	super.appendToResponse(response, context);
    }
    
    @Override
	public EOEditingContext edc() {
		return (EOEditingContext) valueForBinding("editingContext");
	}
    
    private DiplomeReadService diplomeReadService() {
    	return (DiplomeReadService) valueForBinding("diplomeReadService");
    }
    
	/**
	 * @return le provider de diplomes
	 */
	public DiplomesProvider getDiplomesDataProvider() {
		if (diplomesDataProvider == null) {
			diplomesDataProvider = new DiplomesProvider(edc(), diplomeReadService(), getAnneeSelection(), isEnModelisation()) {

				@Override
				public EODiplome getSelectedDiplome() {
					return getSelection();
				}

				@Override
				public void setSelectedDiplome(EODiplome diplome) {
					setSelection(diplome);
				}

				@Override
				public EOQualifier getQualifier() {
					return getQualifierDiplomes();
				}

			};
		}
		return diplomesDataProvider;
	}

	public void setDiplomesDataProvider(DiplomesProvider diplomesDataProvider) {
		this.diplomesDataProvider = diplomesDataProvider;
	}

	public EODiplome getSelection() {
		return (EODiplome) valueForBinding(BINDING_SELECTION);
	}

	/**
	 * @param selection la selection
	 */
	public void setSelection(EODiplome selection) {
		setValueForBinding(selection, BINDING_SELECTION);
	}

	public EOQualifier getQualifierDiplomes() {
		return (EOQualifier) valueForBinding(BINDING_QUALIFIER);
	}

	/**
	 * @return vrai si le composant est inactif
	 */
	public Boolean getDisabled() {
		if (valueForBinding(BINDING_DISABLED) == null) {
			return Boolean.FALSE;
		}
		return (Boolean) valueForBinding(BINDING_DISABLED);
	}

	/**
	 * @param disabled vrai si le composant est inactif
	 */
	public void setDisabled(Boolean disabled) {
		setValueForBinding(disabled, BINDING_DISABLED);
	}

	public String getStyle() {
		return valueForStringBinding(BINDING_STYLE, "");
	}

	/**
	 * @param style : style css du composant
	 */
	public void setStyle(String style) {
		setValueForBinding(style, BINDING_STYLE);
	}

	public Integer getAnneeSelection() {
		return (Integer) valueForBinding(BINDING_ANNEE_SELECTION);
	}

	/**
	 * @param anneeSelection : annee de selection des diplomes
	 */
	public void setAnneeSelection(Integer anneeSelection) {
		setValueForBinding(anneeSelection, BINDING_ANNEE_SELECTION);
	}
	
	public Boolean isEnModelisation() {
		if (hasBinding(BINDING_IS_EN_MODELISATION)) {
			return (Boolean) valueForBooleanBinding(BINDING_IS_EN_MODELISATION, false);
		}
		else {
			return null;
		}
	}

	/**
	 * @param isEnModelisation : indique si on affiche les diplomes visible en modelisation ou pas
	 */
	public void setEnModelisation(Boolean isEnModelisation) {
		setValueForBinding(isEnModelisation, BINDING_IS_EN_MODELISATION);
	}
	
}