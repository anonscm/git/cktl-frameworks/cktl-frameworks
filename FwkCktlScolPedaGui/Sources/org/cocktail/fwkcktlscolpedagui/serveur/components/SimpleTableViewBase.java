package org.cocktail.fwkcktlscolpedagui.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;

/**
 * @author lilia
 *         Implemente un squelette de base pour les tables simpleTableView utilisées dans les applis scolarite
 * @param <T> : type de l'élement qui est affiché dans la simple table view
 */
public abstract class SimpleTableViewBase<T> extends CktlAjaxWOComponent {
	// Private fields

	private static final long serialVersionUID = 1L;
	private static final String BINDING_DISPLAYGROUP = "displayGroup";
	private static final String BINDING_SELECTEDITEM = "selectedItem";
	private static final String BINDING_SHOWFILTERS = "showFilters";
	private static final String BINDING_SHOWTOOLBAR = "showToolbar";
	private static final String BINDING_SHOWTOOLTIP = "showToolTip";
	private static final String BINDING_ISFILTERAUTOMATIC = "isFilterAutomatic";
	private static final String BINDING_USEFIXEDHEADER = "useFixedHeader";
	private static final String BINDING_HEIGHT = "height";
	private static final String BINDING_WIDTH = "width";
	private static final String BINDING_ONSELECT = "onSelect";
	private static final String BINDING_ACTIONUPDATE = "actionUpdate";

	private ERXDisplayGroup<T> displayGroup = null;
	private T currentItem;
	private T selectedItem = null;
	private EOQualifier rechercheQualifier = null;

	// Constructor
	/**
	 * @param context : contexte d'édition
	 */
	public SimpleTableViewBase(WOContext context) {
		super(context);
	}

	// Properties
	public String getSimpleTableViewContainerId() {
		return getComponentId() + "_simpleTableViewContainerId";
	}

	public String getTableViewContainerId() {
		return getComponentId() + "_tableViewContainerId";
	}

	public String getTableViewId() {
		return getComponentId() + "_tableViewId";
	}

	public String getBoutonsContainerId() {
		return getComponentId() + "_boutonsContainerId";
	}

	@SuppressWarnings("unchecked")
	private ERXDisplayGroup<T> displayGroupBinding() {
		return (ERXDisplayGroup<T>) valueForBinding(BINDING_DISPLAYGROUP);
	}

	/**
	 * @return Boolean : retourne le showFilters passe en binding
	 */
	public Boolean showFilters() {
		return valueForBooleanBinding(BINDING_SHOWFILTERS, false);
	}

	/**
	 * @return Boolean : retourne le showToolbar passe en binding
	 */
	public Boolean showToolbar() {
		return valueForBooleanBinding(BINDING_SHOWTOOLBAR, false);
	}

	/**
	 * @return Boolean : retourne le showToolTip passe en binding
	 */
	public Boolean showToolTip() {
		return valueForBooleanBinding(BINDING_SHOWTOOLTIP, true);
	}
	
	/**
	 * @return Boolean : retourne le isFilterAutomatic passe en binding
	 */
	public Boolean isFilterAutomatic() {
		return valueForBooleanBinding(BINDING_ISFILTERAUTOMATIC, false);
	}

	/**
	 * @return Boolean : retourne le useFixedHeader passe en binding
	 */
	public Boolean useFixedHeader() {
		return valueForBooleanBinding(BINDING_USEFIXEDHEADER, true);
	}

	/**
	 * @return String : retourne la taille height passe en binding
	 */
	public String height() {
		return valueForStringBinding(BINDING_HEIGHT, "");
	}

	/**
	 * @return String : retourne la taille width passe en binding
	 */
	public String width() {
		return valueForStringBinding(BINDING_WIDTH, "");
	}

	/* SIMPLE TABLE VIEW * */
	public T getCurrentItem() {
		return currentItem;
	}

	public void setCurrentItem(T currentItem) {
		this.currentItem = currentItem;
	}

	public T getSelectedItem() {
		return selectedItem;
	}

	/**
	 * Set le diplome en parametre et le set aussi au BINDING_SELECTEDITEM
	 * @param selectedItem : le diplome selectionne
	 */
	public void setSelectedItem(T selectedItem) {
		this.selectedItem = selectedItem;
		setValueForBinding(selectedItem, BINDING_SELECTEDITEM);
	}

	/**
	 * @return boolean : indique si un item est selectionne dans la liste
	 */
	public boolean isSelectedItem() {
		return (getSelectedItem() == null);
	}

	// Public Methods
	@Override
	public void appendToResponse(com.webobjects.appserver.WOResponse response, WOContext context) {
		displayGroup = null;

		super.appendToResponse(response, context);

	}

	/**
	 * delegate class
	 * handle the current selected item
	 */
	public class DisplayGroupDelegate {

		/**
		 * @param group : le groupe d'objets du tableau
		 */
		public void displayGroupDidChangeSelectedObjects(final WODisplayGroup group) {
			@SuppressWarnings("unchecked")
			ERXDisplayGroup<T> _groupe = (ERXDisplayGroup<T>) group;
			if (_groupe.selectedObject() != null) {
				setSelectedItem(_groupe.selectedObject());
			} else {
				setSelectedItem(null);
			}
		}
	}

	/**
	 * @return true if the search results are empty
	 */
	public Boolean resultatsRechercheVides() {
		return (displayGroup().allObjects().count() == 0);
	}

	/**
	 * @return EOQualifier : le qualifier de recherche passe en binding
	 */
	public EOQualifier rechercheQualifier() {
		if (rechercheQualifier == null) {
			rechercheQualifier = displayGroupBinding().qualifier();
		}

		return rechercheQualifier;
	}

	public void setRechercheQualifier(EOQualifier rechercheQualifier) {
		this.rechercheQualifier = rechercheQualifier;
	}

	/**
	 * @return ERXDisplayGroup : display group des diplomes affiches
	 */
	public ERXDisplayGroup<T> displayGroup() {
		if (displayGroup == null) {
			displayGroup = new ERXDisplayGroup<T>();

			displayGroup.setDataSource(displayGroupBinding().dataSource());
			displayGroup.setDelegate(new DisplayGroupDelegate());

			displayGroup.setSortOrderings(displayGroupBinding().sortOrderings());
			displayGroup.setQualifier(displayGroupBinding().qualifier());
			setRechercheQualifier(displayGroupBinding().qualifier());

			displayGroup.setSelectsFirstObjectAfterFetch(displayGroupBinding().selectsFirstObjectAfterFetch());
			displayGroup.setSelectedObject(displayGroupBinding().selectedObject());
			displayGroup.setNumberOfObjectsPerBatch(displayGroupBinding().numberOfObjectsPerBatch());
			displayGroup.fetch();

		}

		return displayGroup;
	}

	/** ACTIONS **/

	/**
	 * methode appelee lorsqu'on souhaite appelée une fonctiond d'update
	 * @return null (reste sur la page).
	 */
	public WOActionResults actionUpdate() {
		if (hasBinding(BINDING_ACTIONUPDATE)) {
			return performParentAction(stringValueForBinding(BINDING_ACTIONUPDATE, null));
		} else {
			return doNothing();
		}
	}

	/**
	 * methode appelee a la suite du click sur une ligne du tableau.
	 * @return null (reste sur la page).
	 */
	public WOActionResults onSelect() {
		if (hasBinding(BINDING_ONSELECT)) {
			return performParentAction(stringValueForBinding(BINDING_ONSELECT, null));
		} else {
			return doNothing();
		}
	}

	// Protected Methods
	// Private methods

}
