package org.cocktail.fwkcktlscolpedagui.serveur.components;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxComponent;
import er.ajax.CktlAjaxUtils;

public class GenericAjaxLoader extends AjaxComponent {

	private static final long serialVersionUID = 1L;

	public GenericAjaxLoader(WOContext context) {
        super(context);
    }

	@Override
	protected void addRequiredWebResources(WOResponse response) {
		CktlAjaxUtils.addStylesheetResourceInHead(context(), response, "FwkCktlScolPedaGui.framework", "styles/fwkCktlScolpedaGui.css");
	}

	@Override
	public WOActionResults handleRequest(WORequest request, WOContext context) {
		return null;
	}
}