package org.cocktail.fwkcktlscolpedagui.serveur.components;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CktlFiltreUtils;
import org.cocktail.scol.maquette.DiplomeAccrediteRead;

import com.google.common.base.Joiner;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;

/**
 * Interface pour l'affichage d'un tableau contenant des diplomes
 * @author laetitia
 */
public class DiplomesSimpleTableView extends ScolPedaComponent {
	private static final long serialVersionUID = 1L;
	private static final String SEPARATOR = ", ";

	private static final String BINDING_DISPLAYGROUP = "displayGroup";
	private static final String BINDING_SELECTEDITEM = "selectedItem";
	private static final String BINDING_SHOWFILTERS = "showFilters";
	private static final String BINDING_SHOWTOOLBAR = "showToolbar";
	private static final String BINDING_SHOWTOOLTIP = "showToolTip";
	private static final String BINDING_ISFILTERAUTOMATIC = "isFilterAutomatic";
	private static final String BINDING_ISSHOWCOLOR = "isShowColor";
	private static final String BINDING_HEIGHT = "height";
	private static final String BINDING_WIDTH = "width";
	private static final String BINDING_ONSELECT = "onSelect";
	private static final String BINDING_ACTIONUPDATE = "actionUpdate";

	private static final String LABEL_COLOR_SURLIGNAGNE_SISE_MANQUANT = "cktlajaxtableviewCaseOrangeClair";

	// Variables des filtres de recherche
	private String tokenCodeDiplome = null;
	private String tokenLibelleCourtDiplome = null;
	private String tokenLibelleDiplome = null;
	private String tokenSISEDiplome;
	private String tokenSISEEtablissementDiplome;
	private String tokenTypeDiplome;
	private String tokenGrade;
	private String tokenStatut;
	private String tokenComposante;

	// Table de recherche des diplômes
	private ERXDisplayGroup<DiplomeAccrediteRead> displayGroup = null;
	// private ERXDatabaseDataSource datasource = null;
	private DiplomeAccrediteRead currentItem;
	private DiplomeAccrediteRead selectedItem = null;
	private EOQualifier rechercheQualifier = null;

	/**
	 * Constructeur
	 * @param context : le contexte d'edition
	 */
	public DiplomesSimpleTableView(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(com.webobjects.appserver.WOResponse response, WOContext context) {
		displayGroup = null;
		filtrer();
		super.appendToResponse(response, context);

	}

	public String getDiplomesSimpleTableViewContainerId() {
		return getComponentId() + "_diplomesSimpleTableViewContainerId";
	}

	public String getTableViewContainerId() {
		return getComponentId() + "_tableViewContainerId";
	}

	public String getTableViewId() {
		return getComponentId() + "_tableViewId";
	}

	public String getBoutonsContainerId() {
		return getComponentId() + "_boutonsContainerId";
	}

	@SuppressWarnings("unchecked")
	private ERXDisplayGroup<EODiplome> displayGroupBinding() {
		return (ERXDisplayGroup<EODiplome>) valueForBinding(BINDING_DISPLAYGROUP);
	}

	/**
	 * @return Boolean : retourne le showFilters passe en binding
	 */
	public Boolean showFilters() {
		return valueForBooleanBinding(BINDING_SHOWFILTERS, false);
	}

	/**
	 * @return Boolean : retourne le showToolbar passe en binding
	 */
	public Boolean showToolbar() {
		return valueForBooleanBinding(BINDING_SHOWTOOLBAR, false);
	}

	/**
	 * @return Boolean : retourne le showToolTip passe en binding
	 */
	public Boolean showToolTip() {
		return valueForBooleanBinding(BINDING_SHOWTOOLTIP, true);
	}

	/**
	 * @return Boolean : retourne le isFilterAutomatic passe en binding
	 */
	public Boolean isFilterAutomatic() {
		return valueForBooleanBinding(BINDING_ISFILTERAUTOMATIC, false);
	}

	/**
	 * @return Boolean : retourne le isShowColor passe en binding
	 */
	public Boolean isShowColor() {
		return valueForBooleanBinding(BINDING_ISSHOWCOLOR, false);
	}

	/**
	 * @return String : retourne la taille height passe en binding
	 */
	public String height() {
		return valueForStringBinding(BINDING_HEIGHT, "");
	}

	/**
	 * @return String : retourne la taille width passe en binding
	 */
	public String width() {
		return valueForStringBinding(BINDING_WIDTH, "");
	}

	/**
	 * @return String : la couleur d'affichage
	 */
	public String getDisplayColor() {
		if (isShowColor() && getCurrentItem().getHabilitation() != null && getCurrentItem().getCodeSiseEtab() == null) {
			return LABEL_COLOR_SURLIGNAGNE_SISE_MANQUANT;
		}

		return "";
	}

	// ** FILTRES DE RECHERCHE **/
	public String getTokenCodeDiplome() {
		return tokenCodeDiplome;
	}

	public void setTokenCodeDiplome(String tokenCodeDiplome) {
		this.tokenCodeDiplome = tokenCodeDiplome;
	}

	public String getTokenLibelleCourtDiplome() {
		return tokenLibelleCourtDiplome;
	}

	public void setTokenLibelleCourtDiplome(String tokenLibelleCourtDiplome) {
		this.tokenLibelleCourtDiplome = tokenLibelleCourtDiplome;
	}

	public String getTokenLibelleDiplome() {
		return tokenLibelleDiplome;
	}

	public void setTokenLibelleDiplome(String tokenLibelleDiplome) {
		this.tokenLibelleDiplome = tokenLibelleDiplome;
	}

	public String getTokenSISEDiplome() {
		return tokenSISEDiplome;
	}

	public void setTokenSISEDiplome(String tokenSISEDiplome) {
		this.tokenSISEDiplome = tokenSISEDiplome;
	}

	public String getTokenSISEEtablissementDiplome() {
		return tokenSISEEtablissementDiplome;
	}

	public void setTokenSISEEtablissementDiplome(String tokenSISEEtablissementDiplome) {
		this.tokenSISEEtablissementDiplome = tokenSISEEtablissementDiplome;
	}

	public String getTokenTypeDiplome() {
		return tokenTypeDiplome;
	}

	public void setTokenTypeDiplome(String tokenTypeDiplome) {
		this.tokenTypeDiplome = tokenTypeDiplome;
	}

	public String getTokenStatut() {
		return tokenStatut;
	}

	public void setTokenStatut(String tokenStatut) {
		this.tokenStatut = tokenStatut;
	}

	public String getTokenGrade() {
		return tokenGrade;
	}

	public void setTokenGrade(String tokenGrade) {
		this.tokenGrade = tokenGrade;
	}

	public String getTokenComposante() {
	  return tokenComposante;
  }
	
	public void setTokenComposante(String tokenComposante) {
	  this.tokenComposante = tokenComposante;
  }
	/**
	 * Filtre la liste des diplomes selon la liste des criteres
	 * @return doNothing (reste sur la page)
	 */
	public WOActionResults filtrer() {
		if (displayGroup() != null) {
			CktlFiltreUtils filtreUtils = new CktlFiltreUtils();
			displayGroup().setQualifier(
				    ERXQ.and(rechercheQualifier(), filtreUtils.getLikeQualifier(DiplomeAccrediteRead.TYPE_FORMATION_KEY, getTokenTypeDiplome()),
				        filtreUtils.getLikeQualifier(DiplomeAccrediteRead.SISE_KEY, getTokenSISEDiplome()),
				        filtreUtils.getLikeQualifier(DiplomeAccrediteRead.SISE_ETAB_STR_KEY, getTokenSISEEtablissementDiplome()),
				        filtreUtils.getLikeQualifier(DiplomeAccrediteRead.CODE_KEY, getTokenCodeDiplome()),
				        filtreUtils.getLikeQualifier(DiplomeAccrediteRead.LIBELLE_COURT_KEY, getTokenLibelleCourtDiplome()),
				        filtreUtils.getLikeQualifier(DiplomeAccrediteRead.LIBELLE_KEY, getTokenLibelleDiplome()),
				        filtreUtils.getLikeQualifier(DiplomeAccrediteRead.GRADE_UNIVERSITAIRE_KEY, getTokenGrade()),
				        filtreUtils.getLikeQualifier(DiplomeAccrediteRead.STATUT_FORMATION_KEY, getTokenStatut())));
			displayGroup().fetch();
			setSelectedItem(displayGroup().selectedObject());

			actionUpdate();

		}

		return doNothing();

	}

	/* SIMPLE TABLE VIEW * */
	public DiplomeAccrediteRead getCurrentItem() {
		return currentItem;
	}

	public void setCurrentItem(DiplomeAccrediteRead currentItem) {
		this.currentItem = currentItem;
	}

	public DiplomeAccrediteRead getSelectedItem() {
		return selectedItem;
	}

	/**
	 * Set le diplome en parametre et le set aussi au BINDING_SELECTEDITEM
	 * @param selectedItem : le diplome selectionne
	 */
	public void setSelectedItem(DiplomeAccrediteRead selectedItem) {
		this.selectedItem = selectedItem;
		
		EODiplome diplome = null;
		if(selectedItem!=null) {
			diplome = EODiplome.fetchSco_Diplome(edc(), EODiplome.ID.eq(selectedItem.getId().intValue()));
		}
		setValueForBinding(diplome, BINDING_SELECTEDITEM);
		
	}

	/**
	 * @return boolean : indique si un item est selectionne dans la liste
	 */
	public boolean isSelectedItem() {
		return (getSelectedItem() == null);
	}
	
	/**
	 * delegate class handle the current selected item
	 */
	public class DisplayGroupDelegate {

		/**
		 * @param group : le groupe d'objets du tableau
		 */
		public void displayGroupDidChangeSelectedObjects(final WODisplayGroup group) {
			@SuppressWarnings("unchecked")
			ERXDisplayGroup<DiplomeAccrediteRead> _groupe = (ERXDisplayGroup<DiplomeAccrediteRead>) group;
			if (_groupe.selectedObject() != null) {
				setSelectedItem(_groupe.selectedObject());
			} else {
				setSelectedItem(null);
			}
		}
	}

	/**
	 * @return true if the search results are empty
	 */
	public Boolean resultatsRechercheVides() {
		return (displayGroup().allObjects().count() == 0);
	}

	/**
	 * @return EOQualifier : le qualifier de recherche passe en binding
	 */
	public EOQualifier rechercheQualifier() {
		if (rechercheQualifier == null) {
			rechercheQualifier = displayGroupBinding().qualifier();
		}

		return rechercheQualifier;
	}

	public void setRechercheQualifier(EOQualifier rechercheQualifier) {
		this.rechercheQualifier = rechercheQualifier;
	}

	/**
	 * @return ERXDisplayGroup : display group des diplomes affiches
	 */
	public ERXDisplayGroup<DiplomeAccrediteRead> displayGroup() {
		if (displayGroup == null) {
			displayGroup = new ERXDisplayGroup<DiplomeAccrediteRead>();

			displayGroup.setDataSource(displayGroupBinding().dataSource());
			displayGroup.setDelegate(new DisplayGroupDelegate());

			displayGroup.setSortOrderings(displayGroupBinding().sortOrderings());
			displayGroup.setQualifier(displayGroupBinding().qualifier());
			setRechercheQualifier(displayGroupBinding().qualifier());

			displayGroup.setSelectsFirstObjectAfterFetch(displayGroupBinding().selectsFirstObjectAfterFetch());
			displayGroup.setSelectedObject(displayGroupBinding().selectedObject());
			displayGroup.setNumberOfObjectsPerBatch(displayGroupBinding().numberOfObjectsPerBatch());
			displayGroup.fetch();

		}

		return displayGroup;
	}

	/** ACTIONS **/

	/**
	 * methode appelee lorsqu'on souhaite appelée une fonctiond d'update
	 * @return null (reste sur la page).
	 */
	public WOActionResults actionUpdate() {
		if (hasBinding(BINDING_ACTIONUPDATE)) {
			return performParentAction(stringValueForBinding(BINDING_ACTIONUPDATE, null));
		} else {
			return doNothing();
		}
	}

	/**
	 * methode appelee a la suite du click sur une ligne du tableau.
	 * @return null (reste sur la page).
	 */
	public WOActionResults onSelect() {
		if (hasBinding(BINDING_ONSELECT)) {
			return performParentAction(stringValueForBinding(BINDING_ONSELECT, null));
		} else {
			return doNothing();
		}
	}
	
	public String currentItemStructures() {
		return Joiner.on(SEPARATOR).join(currentItem.getStructures());
	}

}