package org.cocktail.fwkcktlscolpedagui.serveur.components.treetable;

import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.TreeTableNode;
import org.cocktail.fwkcktlscolpedagui.serveur.components.ComposantNode;
import org.cocktail.fwkcktlscolpedagui.serveur.components.IpComposantNode;

public interface ComposantNodeVisitor {

	void visit(ComposantNode node);
	void visit(IpComposantNode node);
    
}
