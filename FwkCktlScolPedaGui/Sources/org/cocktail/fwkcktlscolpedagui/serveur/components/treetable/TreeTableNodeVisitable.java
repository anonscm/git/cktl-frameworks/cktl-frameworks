package org.cocktail.fwkcktlscolpedagui.serveur.components.treetable;


public interface TreeTableNodeVisitable {

    void accept(ComposantNodeVisitor visitor);
    
}
