package org.cocktail.fwkcktlscolpedagui.serveur.components;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CktlFiltreUtils;
import org.cocktail.scol.maquette.ComposantRead;

import com.google.common.base.Joiner;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;

/**
 * Cette classe représente le tableau "liste des parcours".
 * 
 * @author Pascal MACOUIN
 */
public class ParcoursSimpleTableView extends ScolPedaComponent {
	private static final long serialVersionUID = -2843804215549510676L;
	private static final String SEPARATOR = ", ";

	private static final String BINDING_DISPLAYGROUP = "displayGroup";
	private static final String BINDING_SELECTEDITEM = "selectedItem";
	private static final String BINDING_SHOWFILTERS = "showFilters";
	private static final String BINDING_SHOWTOOLBAR = "showToolbar";
	private static final String BINDING_SHOWTOOLTIP = "showToolTip";
	private static final String BINDING_ISFILTERAUTOMATIC = "isFilterAutomatic";
	private static final String BINDING_USEFIXEDHEADER = "useFixedHeader";
	private static final String BINDING_HEIGHT = "height";
	private static final String BINDING_WIDTH = "width";
	private static final String BINDING_ONSELECT = "onSelect";
	private static final String BINDING_ACTIONUPDATE = "actionUpdate";
	
	private ERXDisplayGroup<ComposantRead> displayGroup;
	private ComposantRead currentItem;
	private ComposantRead selectedItem = null;
	private EOQualifier rechercheQualifier = null;

	private String tokenCodeParcours;
	private String tokenLibelleParcours;
	private String tokenComposante;
	private String tokenLocalisation;
	
	/**
	 * Constructeur.
	 * @param context Un context WO
	 */
	public ParcoursSimpleTableView(WOContext context) {
		super(context);
	}
	
	@Override
	public void appendToResponse(com.webobjects.appserver.WOResponse response, WOContext context) {
		displayGroup = null;
		filtrer();
		super.appendToResponse(response, context);
	}
	
	public String getParcoursSimpleTableViewContainerId() {
		return getComponentId() + "_parcoursSimpleTableViewContainerId";
	}
	
	public String getTableViewContainerId() {
		return getComponentId() + "_tableViewContainerId";
	}
	
	public String getTableViewId() {
		return getComponentId() + "_tableViewId";
	}
	
	@SuppressWarnings("unchecked")
	private ERXDisplayGroup<ComposantRead> getDisplayGroupBinding() {
		return (ERXDisplayGroup<ComposantRead>) valueForBinding(BINDING_DISPLAYGROUP);
	}
	
	/**
	 * @return Boolean : retourne le showFilters passe en binding
	 */
	public Boolean showFilters() {
		return valueForBooleanBinding(BINDING_SHOWFILTERS, true);
	}

	/**
	 * @return Boolean : retourne le showToolbar passe en binding
	 */
	public Boolean showToolbar() {
		return valueForBooleanBinding(BINDING_SHOWTOOLBAR, false);
	}
	
	/**
	 * @return Boolean : retourne le showToolTip passe en binding
	 */
	public Boolean showToolTip() {
		return valueForBooleanBinding(BINDING_SHOWTOOLTIP, true);
	}
	
	/**
	 * @return Boolean : retourne le isFilterAutomatic passe en binding
	 */
	public Boolean isFilterAutomatic() {
		return valueForBooleanBinding(BINDING_ISFILTERAUTOMATIC, false);
	}
	
	/**
	 * @return Boolean : retourne le useFixedHeader passe en binding
	 */
	public Boolean useFixedHeader() {
		return valueForBooleanBinding(BINDING_USEFIXEDHEADER, false);
	}

	/**
	 * @return String : retourne la taille height passe en binding
	 */
	public String height() {
		return valueForStringBinding(BINDING_HEIGHT, "");
	}

	/**
	 * @return String : retourne la taille width passe en binding
	 */
	public String width() {
		return valueForStringBinding(BINDING_WIDTH, "");
	}
	
	/**
	 * @return Display group des parcours affichées
	 */
	public ERXDisplayGroup<ComposantRead> displayGroup() {
		if (displayGroup == null) {
			ERXDisplayGroup<ComposantRead> displayGroupBinding = getDisplayGroupBinding();
			
			displayGroup = new ERXDisplayGroup<ComposantRead>();

			displayGroup.setDataSource(displayGroupBinding.dataSource());
			displayGroup.setDelegate(new DisplayGroupDelegate());

			displayGroup.setSortOrderings(displayGroupBinding.sortOrderings());
			displayGroup.setQualifier(displayGroupBinding.qualifier());

			displayGroup.setSelectsFirstObjectAfterFetch(displayGroupBinding.selectsFirstObjectAfterFetch());
			displayGroup.setSelectedObject(displayGroupBinding.selectedObject());
			displayGroup.setNumberOfObjectsPerBatch(displayGroupBinding.numberOfObjectsPerBatch());
			displayGroup.fetch();
		}

		return displayGroup;
	}

	/**
	 * delegate class
	 * handle the current selected item
	 */
	public class DisplayGroupDelegate {
		/**
		 * @param group : le groupe d'objets du tableau
		 */
		public void displayGroupDidChangeSelectedObjects(final WODisplayGroup group) {
			@SuppressWarnings("unchecked")
			ERXDisplayGroup<ComposantRead> _groupe = (ERXDisplayGroup<ComposantRead>) group;
			if (_groupe.selectedObject() != null) {
				setSelectedItem(_groupe.selectedObject());
			} else {
				setSelectedItem(null);
			}
		}
	}

	public ComposantRead getCurrentItem() {
		return currentItem;
	}
	
	public void setCurrentItem(ComposantRead currentItem) {
		this.currentItem = currentItem;
	}
	
	public ComposantRead getSelectedItem() {
		return selectedItem;
	}

	/**
	 * Set le parcours en parametre et le set aussi au BINDING_SELECTEDITEM
	 * @param selectedItem : le parcours selectionne
	 */
	public void setSelectedItem(ComposantRead selectedItem) {
		this.selectedItem = selectedItem;
		
		EOParcours parcours = null;
		if(selectedItem!=null) {
			parcours = EOParcours.fetchSco_Parcours(edc(), EOParcours.ID.eq(selectedItem.getId().intValue()));
		}
		setValueForBinding(parcours, BINDING_SELECTEDITEM);
	}

	/**
	 * @return boolean : indique si un item est selectionne dans la liste
	 */
	public boolean isSelectedItem() {
		return (getSelectedItem() == null);
	}
	
	public EOParcours getCurrentParcours() {
		EOParcours parcours = null;
		if(currentItem!=null) {
			parcours = EOParcours.fetchSco_Parcours(edc(), EOParcours.ID.eq(currentItem.getId().intValue()));
		}
		
		return parcours;
	}
	
	//** FILTRES DE RECHERCHE **/
	public String getTokenCodeParcours() {
		return tokenCodeParcours;
	}
	
	public void setTokenCodeParcours(String tokenCodeParcours) {
		this.tokenCodeParcours = tokenCodeParcours;
	}
	
	public String getTokenLibelleParcours() {
		return tokenLibelleParcours;
	}
	
	public void setTokenLibelleParcours(String tokenLibelleParcours) {
		this.tokenLibelleParcours = tokenLibelleParcours;
	}
	
	public String getTokenComposante() {
		return tokenComposante;
	}
	
	public void setTokenComposante(String tokenComposante) {
		this.tokenComposante = tokenComposante;
	}
	
	public String getTokenLocalisation() {
		return tokenLocalisation;
	}
	
	public void setTokenLocalisation(String tokenLocalisation) {
		this.tokenLocalisation = tokenLocalisation;
	}
	
	/**
	 * @return EOQualifier : le qualifier de recherche passe en binding
	 */
	public EOQualifier rechercheQualifier() {
		if (rechercheQualifier == null) {
			rechercheQualifier = getDisplayGroupBinding().qualifier();
		}

		return rechercheQualifier;
	}

	public void setRechercheQualifier(EOQualifier rechercheQualifier) {
		this.rechercheQualifier = rechercheQualifier;
	}
	
	/**
	 * Filtrer la liste des parcours selon les critères.
	 * @return doNothing (reste sur la page)
	 */
	public WOActionResults filtrer() {
		if (displayGroup() != null) {
			CktlFiltreUtils filtreUtils = new CktlFiltreUtils();
			displayGroup().setQualifier(ERXQ.and(rechercheQualifier(),
					filtreUtils.getLikeQualifier(ComposantRead.CODE_KEY, getTokenCodeParcours()),
					filtreUtils.getLikeQualifier(ComposantRead.LIBELLE_KEY, getTokenLibelleParcours())));

			displayGroup().fetch();
			setSelectedItem(displayGroup().selectedObject());
			
			actionUpdate();
		}

		return doNothing();
	}
	
	/**
	 * Méthode appelée lorsqu'on souhaite appeler une fonction d'update
	 * @return null (reste sur la page).
	 */
	public WOActionResults actionUpdate() {
		if (hasBinding(BINDING_ACTIONUPDATE)) {
			return performParentAction(stringValueForBinding(BINDING_ACTIONUPDATE, null));
		} else {
			return doNothing();
		}
	}
	
	/**
	 * Méthode appelée a la suite du click sur une ligne du tableau.
	 * @return null (reste sur la page).
	 */
	public WOActionResults onSelect() {
		if (hasBinding(BINDING_ONSELECT)) {
			return performParentAction(stringValueForBinding(BINDING_ONSELECT, null));
		} else {
			return doNothing();
		}
	}
	
	public String currentItemStructures() {
		return Joiner.on(SEPARATOR).join(currentItem.getStructures());
	}
	
	public String currentItemImplantations() {
		return Joiner.on(SEPARATOR).join(currentItem.getImplantations());
	}
	
}