package org.cocktail.fwkcktlscolpedagui.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlscolpedagui.serveur.components.providers.AEsProvider;
import org.cocktail.scol.maquette.AERead;
import org.cocktail.scol.maquette.AEReadRepository;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

/**
 * Permet de sélectionner dans une liste les liens dont l'enfant est un AE
 */
public class AEsSelect2Remote extends CktlAjaxWOComponent {
	private static final long serialVersionUID = 1L;
	private AEsProvider aesProvider;

	private static final String BINDING_AE_READ_SERVICE = "aeReadService";
	private static final String BINDING_IS_EN_MODELISATION = "isEnModelisation";
	private static final String BINDING_ANNEE_SELECTION = "anneeSelection";
	private static final String BINDING_STYLE = "style";
	private static final String BINDING_DISABLED = "disabled";
	private static final String BINDING_SELECTION = "selection";
	/**
	 * @param context contexte d'edition
	 */
	public AEsSelect2Remote(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		aesProvider = null;
		super.appendToResponse(response, context);
	}

	public AEReadRepository getAEReadRepository() {
		return (AEReadRepository) valueForBinding(BINDING_AE_READ_SERVICE);
	}

	public Integer getAnneeSelection() {
		return (Integer) valueForBinding(BINDING_ANNEE_SELECTION);
	}

	/**
	 * @param anneeSelection : annee de selection des liens
	 */
	public void setAnneeSelection(Integer anneeSelection) {
		setValueForBinding(anneeSelection, BINDING_ANNEE_SELECTION);
	}

	/**
	 * @return <code>true</code> si on affiche les liens visible uniqument en modelisation 
	 */
	public Boolean isEnModelisation() {
		if (hasBinding(BINDING_IS_EN_MODELISATION)) {
			return (Boolean) valueForBooleanBinding(BINDING_IS_EN_MODELISATION, false);
		} else {
			return null;
		}
	}

	/**
	 * @param isEnModelisation <code>true</code> si on affiche les liens visible uniqument en modelisation 
	 */
	public void setEnModelisation(Boolean isEnModelisation) {
		setValueForBinding(isEnModelisation, BINDING_IS_EN_MODELISATION);
	}

	/**
	 * @return the lienAEsProvider
	 */
	public AEsProvider getAesProvider() {
		if (aesProvider == null) {
			aesProvider = new AEsProvider(getAEReadRepository(), getAnneeSelection(), isEnModelisation()) {

				@Override
				public AERead getSelectedAERead() {
					return getSelection();
				}

				@Override
				public void setSelectedAERead(AERead lien) {
					setSelection(lien);
				}

			};
		}
		return aesProvider;
	}
	
	public String getStyle() {
		return valueForStringBinding(BINDING_STYLE, "");
	}

	/**
	 * @param style : style css du composant
	 */
	public void setStyle(String style) {
		setValueForBinding(style, BINDING_STYLE);
	}
	
	/**
	 * @return vrai si le composant est inactif
	 */
	public Boolean getDisabled() {
		if (valueForBinding(BINDING_DISABLED) == null) {
			return Boolean.FALSE;
		}
		return (Boolean) valueForBinding(BINDING_DISABLED);
	}

	/**
	 * @param disabled vrai si le composant est inactif
	 */
	public void setDisabled(Boolean disabled) {
		setValueForBinding(disabled, BINDING_DISABLED);
	}
	
	public AERead getSelection() {
		return (AERead) valueForBinding(BINDING_SELECTION);
	}

	/**
	 * @param selection la selection
	 */
	public void setSelection(AERead selection) {
		setValueForBinding(selection, BINDING_SELECTION);
	}
}