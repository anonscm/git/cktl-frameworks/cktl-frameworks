package org.cocktail.fwkcktlscolpedagui.serveur.components;

import java.util.Collection;

import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeLien;
import org.cocktail.fwkcktlscolpedagui.serveur.components.treetable.ComposantNodeVisitor;
import org.cocktail.fwkcktlscolpedagui.serveur.components.treetable.TreeTableNodeVisitable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Spécialisation d'un CtlTreeTableNode pour les IComposant
 */
public class ComposantNode extends CktlTreeTableNode<IComposant> implements TreeTableNodeVisitable {
	// Private fields
	private NSArray<CktlTreeTableNode<IComposant>> childrenNodes;
	private NSArray<EOTypeComposant> typeComposantsNonAffichables;
	private Collection<EOTypeComposant> typeComposantsSelectionnables;

	private NSArray<IComposant> composantsAffichables;
	private EOLien lien = null;
	private EOLienComposer lienComposer = null;

	private ComposantNodeVisitor visitor;

	// Constructor

	/**
	 * @param data : contenu du noeud
	 * @param parent : parent du noeud
	 * @param visible : true si le noeud est visible
	 */
	public ComposantNode(IComposant data, CktlTreeTableNode<IComposant> parent, Boolean visible) {
		super(data, parent, visible);
	}

	protected ComposantNode getNewInstance(IComposant data, CktlTreeTableNode<IComposant> parent, Boolean visible) {
		return new ComposantNode(data, parent, visible);
	}

	private EOEditingContext getEditingContext() {
		return getDataAsComposant().editingContext();
	}

	private EOComposant getDataAsComposant() {
		return (EOComposant) getData();
	}

	// Properties
	/**
	 * @return lien entre le noeud et son parent
	 */
	public EOLien getLien() {
	    if (lien == null) {
	        EOComposant parent = (EOComposant) getParentForLien();
	        EOTypeLien typeLien = (EOTypeLien) getTypeLienForLien();
	        NSArray<EOLien> liens = parent.liensParents(EOLien.CHILD.eq(this.getDataAsComposant()).and(EOLien.TYPE_LIEN.eq(typeLien)));
	        if (liens.size() >= 1) {
	            lien = liens.get(0);
	        }
	    }
	    return lien;
	}

	private ITypeLien getTypeLienForLien() {
		if (this.getData().getClass().equals(EOVersionDiplome.class)) {
			return EOTypeLien.typeVersionner(getEditingContext());
		} else {
			return EOTypeLien.typeComposer(getEditingContext());
		}
	}

	private IComposant getParentForLien() {
		if (this.getData().getClass().equals(EOVersionDiplome.class)) {
			return ((EOVersionDiplome) this.getData()).getDiplome();
		} else {
			return this.getParent().getData();
		}
	}

	/**
	 * @return l'id lien contexte construit à partir des parents du ComposantNode
	 */
	public String getIdLienContexte() {
		StringBuilder builder = new StringBuilder();
		ComposantNode parent = this;
		while (parent != null) {
			builder.insert(0, 
					ILienConsultation.ID_LIEN_CONTEXTE_SEPERATOR + parent.getLien().id().toString());
			parent = (ComposantNode) parent.getParent();
		};
		return builder.toString();
	}
	
	/**
	 * @return lienComposer de type LienComposer entre le noeud et son parent
	 */
	public ILienComposer getLienComposer() {
	    if (lienComposer == null && getParent() != null) {
	        EOComposant parent = (EOComposant) getParentForLien();
	        EOTypeLien typeLien = EOTypeLien.typeComposer(getEditingContext());
	        NSArray<EOLien> liens = parent.liensParents(EOLien.CHILD.eq(this.getDataAsComposant()).and(EOLien.TYPE_LIEN.eq(typeLien)));
	        if (liens.size() >= 1) {
	            lienComposer = (EOLienComposer) liens.get(0);
	        }
	    }
	    return lienComposer;
	}

	public NSArray<EOTypeComposant> getTypeComposantsNonAffichables() {
		return this.typeComposantsNonAffichables;
	}

	public void setTypeComposantsNonAffichables(NSArray<EOTypeComposant> typeComposantsNonAffichables) {
		this.typeComposantsNonAffichables = typeComposantsNonAffichables;
	}

	/**
	 * @return les types de composants qi'on ne peut pas sélectionner dans l'interface au sens cliquer sur la ligne
	 */
	public Collection<EOTypeComposant> getTypeComposantsSelectionnables() {
		return typeComposantsSelectionnables;
	}

	public void setTypeComposantsSelectionnables(Collection<EOTypeComposant> typeComposantsSelectionnables) {
		this.typeComposantsSelectionnables = typeComposantsSelectionnables;
	}

	public NSArray<IComposant> getComposantsAffichables() {
		return composantsAffichables;
	}

	public void setComposantsAffichables(NSArray<IComposant> composantsAffichables) {
		this.composantsAffichables = composantsAffichables;
	}

	@Override
	public NSArray<CktlTreeTableNode<IComposant>> getChildren() {

		if (childrenNodes == null) {
			childrenNodes = new NSMutableArray<CktlTreeTableNode<IComposant>>();

			for (IComposant c : getDataAsComposant().childs(EOTypeLien.typeComposer(getEditingContext()), null, EOLien.ORDRE.ascs())) {
				Boolean isVisible = true;
				if (getTypeComposantsNonAffichables() != null && getTypeComposantsNonAffichables().contains(c.typeComposant())) {
					isVisible = false;
				} else if (getComposantsAffichables() != null && !getComposantsAffichables().contains(c)) {
					isVisible = false;
				}
				// TODO cast à supprimer, modification du parametre T de la classe en IComposant à prévoir
				ComposantNode composantNode = getNewInstance((IComposant) c, this, isVisible);
				composantNode.setTypeComposantsNonAffichables(getTypeComposantsNonAffichables());
				composantNode.setTypeComposantsSelectionnables(getTypeComposantsSelectionnables());
				composantNode.setComposantsAffichables(getComposantsAffichables());
				composantNode.accept(getVisitor());

				/** on le fait en dernier car c'est ce qui compte le plus **/
				if (!isVisible()) {
					composantNode.setVisible(false);
				}				
				// si composant archivé, on ne le met pas dans l'arbre
				if (!Boolean.TRUE.equals(composantNode.getData().sysArchive())) {
					childrenNodes.add(composantNode);
				}
			}
		}
		return childrenNodes;
	}

	/**
	 * @return l'ensemble des composants en commançant par la racine.
	 */
	public NSArray<IComposant> getToutComposants() {
		NSArray<IComposant> composants = new NSMutableArray<IComposant>();
		remplitComposants(this.getRoot(), composants);
		return composants;
	}

	@Override
	public Boolean isSelectable() {
		if (getTypeComposantsSelectionnables() != null) {
			return getTypeComposantsSelectionnables().contains(getData().typeComposant());
		}
		return true;
	}

	private void remplitComposants(CktlTreeTableNode<IComposant> node, NSArray<IComposant> composants) {
		composants.add(node.getData());
		for (CktlTreeTableNode<IComposant> childNode : node.getChildren()) {
			remplitComposants((ComposantNode) childNode, composants);
		}
	}

	private ComposantNode getRoot() {
		ComposantNode root = this;
		while (root.getParent() != null) {
			root = (ComposantNode) root.getParent();
		}
		return root;
	}

	/**
	 * @return nom de l'image du composant
	 */
	public String nomImage() {
		// a bouger
		return "images/Matrice/" + getDataAsComposant().getTypeComposant().nom() + ".png";
	}

	/**
	 * {@inheritDoc}
	 */
	public void accept(ComposantNodeVisitor visitor) {
		setVisitor(visitor);
		if (visitor != null) {
			visitor.visit(this);
		}
	}

	public ComposantNodeVisitor getVisitor() {
		return visitor;
	}

	public void setVisitor(ComposantNodeVisitor visitor) {
		this.visitor = visitor;
	}

}
