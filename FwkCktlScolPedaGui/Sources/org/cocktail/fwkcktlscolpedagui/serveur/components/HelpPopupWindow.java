package org.cocktail.fwkcktlscolpedagui.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;


/**
 *  Composant de fenêtre d'aide contextuelle 
 */
public class HelpPopupWindow extends ScolPedaComponent {
	
    private static final long serialVersionUID = -7892712427281619270L;
	private static final String BINDING_TITLE = "title";
	private static final String BINDING_TEXT = "text";
	private static final String BINDING_WIDTH = "width";
	private static final String BINDING_HEIGHT = "height";

	private static final Integer WIDTH = 300;
	private static final Integer HEIGHT = 200;
	
	
    /**
     * @param context : contexte d'edition
     */
    public HelpPopupWindow(WOContext context) {
        super(context);
    }

    public String getPopupWindowId() {
        return getComponentId() + "_popupWindowId"; 
      }

	public String getTitle() {
		return valueForStringBinding(BINDING_TITLE, localisation("HelpPopupWindow.title"));
	}
	
	public String getText() {
		return valueForStringBinding(BINDING_TEXT, "");
	}

	public Integer getWidth() {
		return valueForIntegerBinding(BINDING_WIDTH, WIDTH);
	}

	public Integer getHeight() {
		return valueForIntegerBinding(BINDING_HEIGHT, HEIGHT);
	}
 
	/**
	 * @return null (ouvre une fenêtre pop-up)
	 */
	public WOActionResults openPopupWindow() {
		CktlAjaxWindow.open(context(), getPopupWindowId());
		
		return doNothing();
	}
	
	/**
	 * @return message d'aide affiche par le composant
	 */
	public String onClick() {
		return "alert('" + getText() + "')";
	}

	
}