package org.cocktail.fwkcktlscolpedagui.serveur;

import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ComposantRepository;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ComposantRepositoryEOF;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.webobjects.eocontrol.EOEditingContext;
import com.woinject.WOSessionScoped;

import er.extensions.eof.ERXEC;

/**
 * Cette classe permet de lier les classes injectees
 */
public class FwkCktlScolPedaGuiModule extends AbstractModule {

	/** {@inheritDoc} */
	@Override
	protected void configure() {
		 bind(ComposantRepository.class).to(ComposantRepositoryEOF.class);
	}

//	/**
//	 * @return editing context pour la session
//	 */
//	@Provides
//	@WOSessionScoped
//	public EOEditingContext editingContext() {
//		return ERXEC.newEditingContext();
//	}

	private <T> Class<? extends T> getImplementationClassFor(Class<T> baseClass) {
		try {
			String implementationClassName = System.getProperty(baseClass.getSimpleName());
			@SuppressWarnings("unchecked")
			Class<? extends T> clazz = (Class<? extends T>) Class.forName(implementationClassName);
			return clazz;
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Impossible de trouver une classe implementant " + baseClass.getName(), e);
		}
	}
}
