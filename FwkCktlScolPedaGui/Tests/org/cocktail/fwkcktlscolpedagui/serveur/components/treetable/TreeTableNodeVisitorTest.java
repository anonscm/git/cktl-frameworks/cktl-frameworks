package org.cocktail.fwkcktlscolpedagui.serveur.components.treetable;

import static org.fest.assertions.api.Assertions.assertThat;

import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpedagui.serveur.components.ComposantNode;
import org.cocktail.fwkcktlscolpedagui.serveur.components.IpComposantNode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.foundation.NSArray;

@RunWith(value=MockitoJUnitRunner.class)
public class TreeTableNodeVisitorTest {
    
    @Spy
    ComposantNode rootNode = new ComposantNode(null, null, true);

    @Spy
    ComposantNode dummyNode1 = new ComposantNode(null, rootNode, true);
    
    @Spy
    ComposantNode dummyNode2 = new ComposantNode(null, rootNode, true);
    
    @Spy
    ComposantNode dummyLeaf1 = new ComposantNode(null, dummyNode1, true);
    
    @Spy
    ComposantNode dummyLeaf2 = new ComposantNode(null, dummyNode1, true);

    
    ComposantNodeVisitor invisibleVisitor = new ComposantNodeDummyVisitor();
    
    @Before
    public void setUp() {
    	Mockito.doReturn(new NSArray<CktlTreeTableNode<IComposant>>(dummyNode1, dummyNode2)).when(rootNode).getChildren();
    	Mockito.doReturn(new NSArray<CktlTreeTableNode<IComposant>>(dummyLeaf1, dummyLeaf2)).when(dummyNode1).getChildren();
    	Mockito.doReturn(new NSArray<CktlTreeTableNode<EOComposant>>()).when(dummyNode2).getChildren();
    	Mockito.doReturn(new NSArray<CktlTreeTableNode<EOComposant>>()).when(dummyLeaf1).getChildren();
    	Mockito.doReturn(new NSArray<CktlTreeTableNode<EOComposant>>()).when(dummyLeaf2).getChildren();
    }
    
    @Test
    public void testVisitFromRoot() {
        rootNode.accept(invisibleVisitor);
        assertThat(rootNode.isVisible()).isFalse();
        assertThat(dummyNode2.isVisible()).isTrue();
        assertThat(dummyNode1.isVisible()).isTrue();
        assertThat(dummyLeaf1.isVisible()).isTrue();
        assertThat(dummyLeaf2.isVisible()).isTrue();
    }

    
    public static class ComposantNodeDummyVisitor implements ComposantNodeVisitor {

        public void visit(ComposantNode node) {
            node.setVisible(false);
        }

		public void visit(IpComposantNode node) {
		}

    }
    
    
}