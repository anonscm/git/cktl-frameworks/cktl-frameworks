Ajax.Responders.register({

  onLoading: function(request) {
  	setTimeout(function() {
  		var nbRequest = Ajax.activeRequestCount;
  		if (nbRequest > 0) {
			jQuery('body').addClass("loading");
  		}
  	}, 1000);
  },
  
  onComplete: function(request) {
	jQuery('body').removeClass("loading");
  }

});
			
