package org.cocktail.fwkcktlpeche.droits;

/**
 * Enumère tous les droits 'fonction' disponibles pour l'application 'Peche'.
 *
 * @author Pascal MACOUIN
 */
public enum Fonction {
	// Catégorie 'Accès Peche'
	PECHE ("PECHE" , "Accès Peche" , "Droit donnant accès à l'application Peche.") , 

	// Catégorie 'Enseignants'
	ENSEIGNANTS ("ENSEIGNANTS" , "Enseignants" , "Droit donnant accès aux sous-menus 'Fiche'") , 
	REPARTITION_CROISEE ("REPARTITION_CROISEE" , "Enseignants" , "Droit donnant accès au sous-menu 'Répartition croisée'") , 
	PARAM_ABSENCE ("PARAM_ABSENCE" , "Enseignants" , "Droit donnant accès à la surcharge du nombre d'heures d'absence dans PECHE") , 
	REPARTITION_HCOMP ("REPARTITION_HCOMP" , "Enseignants" , "Droit donnant accès à la répartition des heures complémentaires pour les statutaires") , 
	REPORT_HCOMP ("REPORT_HCOMP" , "Enseignants" , "Droit donnant accès au report des heures complémentaires pour les statutaires") , 
	SAISIE_REH_VACATAIRE ("SAISIE_REH_VACATAIRE" , "Enseignants" , "Droit donnant accès à la saisie des REH des enseignants vacataires") , 
	SAISIE_REH_STATUTAIRE ("SAISIE_REHSTATUTAIRE" , "Enseignants" , "Droit donnant accès à la saisie des REH des enseignants statutaires") , 
	
	// Catégorie 'Enseignements'
	OFFRE_FORMATION ("OFFRE_FORMATION" , "Enseignements" , "Droit donnant accès au sous-menu 'Offre de formation'") , 
	PARAM_NB_GROUPE ("PARAM_NB_GROUPE" , "Enseignements" , "Droit donnant accès à la surcharge du nombre de groupe dans PECHE") , 
	PARAM_NB_HEURE_MAQUETTE ("PARAM_NB_HEURES" , "Enseignements" , "Droit donnant accès à la surcharge du nombre d'heures maquette dans PECHE") , 
	UE ("UE" , "Enseignements" , "Droit donnant accès au sous-menu 'UE Flottantes'") , 

	// Catégorie 'Fiches et validation'
	VALID_FICHE_DRH ("VALID_DRH" , "Fiches et validation" , "Droit donnant accès à la validation des fiches en tant que DRH") , 
	VALID_FICHE_PRESIDENT ("VALID_PRESIDENT" , "Fiches et validation" , "Droit donnant accès à la validation des fiches en tant que président") , 
	VALID_FICHE_ENSEIGNANT ("VALID_ENSEIGNANT" , "Fiches et validation" , "Droit donnant accès à la validation des fiches en tant qu'enseignant") , 
	VALID_FICHE_DIRECTEUR_COMPOSANTE ("VALID_DIR_COMP" , "Fiches et validation" , "Droit donnant accès à la validation des fiches en tant que directeur de composante") , 
	VALID_FICHE_DIRECTEUR_DEPARTEMENT ("VALID_DIR_DEP" , "Fiches et validation" , "Droit donnant accès à la validation des fiches en tant que directeur de département") , 	
	VALID_FICHE_DIRECTEUR_COLLEGE ("VALID_DIR_COLL" , "Fiches et validation" , "Droit donnant accès à la validation des fiches en tant que directeur de collège") , 
	VALID_FICHE_REPARTITEUR ("VALID_REPARTITEUR" , "Fiches et validation" , "Droit donnant accès à la validation des fiches en tant que répartiteur") , 
	HEURES_A_PAYER ("HEURES_A_PAYER" , "Fiches et validation" , "Droit donnant accès a l'édition des heures à payer") , 
	MISE_EN_PAIEMENT ("MISE_EN_PAIEMENT" , "Fiches et validation" , "Droit donnant accès à la validation des mises en paiement") , 	
	
	// Catégorie 'Paramétrages'
	PARAM_ENSEIGNANTS_GENERIQUES ("PARAM_ENS_GENERIQUES" , "Paramétrages" , "Droit donnant accès à la gestion des enseignants génériques") , 
	PARAM_CIRCUITS_VALIDATION ("PARAM_CIRCUITS_VAL" , "Paramétrages" , "Droit donnant accès à la gestion des circuits de validation") , 
	PARAM_TYPES_HORAIRES ("PARAM_TYPES_HORAIRES" , "Paramétrages" , "Droit donnant accès à la gestion des types d'AP HETD") , 
	PARAM_PARAMETRES_POPULATIONS ("PARAM_P_POPULATIONS" , "Paramétrages" , "Droit donnant accès à la gestion des paramètres des populations HETD") , 
	PARAM_ACTIVITES ("PARAM_ACTIVITES" , "Paramétrages" , "Droit donnant accès à la gestion des activités REH") , 
	PARAM_TAUX_HORAIRES ("PARAM_TAUX_HORAIRES" , "Paramétrages" , "Droit donnant accès à la gestion des taux horaires") , 
	PARAM_FLUX_PAIEMENT ("PARAM_FLUX_PAIEMENT" , "Paramétrages" , "Droit donnant accès à la gestion des types de flux de paiement") , 
	PARAM_UEF_CHOIX_COMPOSANTES ("PARAM_UEF_COMP" , "Paramétrages" , "Droit donnant accès à la gestion du choix des composantes pour les UE Flottantes"),
	PARAM_DUPLICATION ("PARAM_DUPLICATION" , "Paramétrages" , "Droit donnant accès à la gestion de la duplication des enseignants d'une année à l'autre"),
	PARAM_GESTION_DROITS ("PARAM_PERIMETRE_DONNEES", "Paramétrages", "Droit donnant accès au périmètre des données"),
	PARAM_PLAFONDS("PARAM_PLAFONDS", "Paramétrages" , "Droit donnant accès à la gestion des taux horaires"); 
	
	/** Identifiant interne de la fonction (cf table GD_FONCTION). */
	private String idFonction;
	
	/** Catégorie de la fonction (FON_CATEGORIE de la table GD_FONCTION) */
	private String categorie;
	
	/** Description de la fonction (FON_DESCRIPTION de la table GD_FONCTION) */
	private String description; 

	/**
	 * Retourne l'identifiant interne de la fonction.
	 *
	 * @return L'identifiant interne de la fonction
	 */
	public String getIdFonction() {
		return idFonction;
	}

	/**
	 * Retourne la categorie de la fonction.
	 *
	 * @return La categorie de la fonction
	 */
	public String getCategorie() {
		return categorie;
	}

	/**
	 * Retourne la description de la fonction.
	 *
	 * @return La description de la fonction
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Contructeur.
	 *
	 * @param idFonction L'identifiant interne de la fonction
	 */
	Fonction(String idFonction , String categorie , String description) {
		this.idFonction = idFonction;
		this.categorie = categorie;
		this.description = description;
	}

}
