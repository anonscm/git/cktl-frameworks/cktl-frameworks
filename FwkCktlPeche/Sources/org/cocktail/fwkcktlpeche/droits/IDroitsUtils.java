package org.cocktail.fwkcktlpeche.droits;

/**
 * Interface pour la gestion des droits
 *
 * @author Chama LAATIK
 */
public interface IDroitsUtils {

	/**
	 * Est-ce que la fonction est autorisée en consultation?
	 * @param fctId : fonction concernée
	 * @return vrai/faux
	 */
	boolean fonctionAutoriseeEnConsultation(String fctId);

	/**
	 * Est-ce que la fonction est autorisée en modification?
	 * @param fctId : fonction concernée
	 * @return vrai/faux
	 */
	boolean fonctionAutoriseeEnModification(String fctId);

	/**
	 * Est-ce que les données sont autorisés en consultation?
	 * @param dataId : donnée concernée
	 * @return vrai/faux
	 */
	boolean donneeAccessibleEnLecture(String dataId);

	/**
	 * Est-ce que les données sont autorisés en modification?
	 * @param dataId : donnée concernée
	 * @return vrai/faux
	 */
	boolean donneeAccessibleEnModification(String dataId);

}