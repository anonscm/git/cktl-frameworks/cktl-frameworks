package org.cocktail.fwkcktlpeche.droits;


/**
 * Classe permettant la gestion des droits
 *
 * @author Yannick MAURAY
 * @author Chama LAATIK
 */
public final class DroitsUtils implements IDroitsUtils {

	private PecheAutorisationsCache cache;

	/**
	 * Constructeur
	 * @param cache : classe du fwkPersonne
	 */
	public DroitsUtils(PecheAutorisationsCache cache) {
		this.cache = cache;
	}

	/**
	 * Est-ce que la fonction est autorisée en consultation?
	 * @param fctId : fonction concernée
	 * @return vrai/faux
	 */
	public boolean fonctionAutoriseeEnConsultation(String fctId) {
		return cache.hasDroitConnaissanceOnFonction(fctId) || cache.hasDroitUtilisationOnFonction(fctId);
	}

	/**
	 * Est-ce que la fonction est autorisée en modification?
	 * @param fctId : fonction concernée
	 * @return vrai/faux
	 */
	public boolean fonctionAutoriseeEnModification(String fctId) {
		return cache.hasDroitUtilisationOnFonction(fctId);
	}

	/**
	 * Est-ce que les données sont autorisés en consultation?
	 * @param dataId : donnée concernée
	 * @return vrai/faux
	 */
	public boolean donneeAccessibleEnLecture(String dataId) {
		return cache.hasDroitCreationOnDonnee(dataId) || cache.hasDroitLectureOnDonnee(dataId) || cache.hasDroitModificationOnDonnee(dataId) || cache.hasDroitSuppressionOnDonnee(dataId);
	}

	/**
	 * Est-ce que les données sont autorisés en modification?
	 * @param dataId : donnée concernée
	 * @return vrai/faux
	 */
	public boolean donneeAccessibleEnModification(String dataId) {
		return cache.hasDroitModificationOnDonnee(dataId);
	}
}
