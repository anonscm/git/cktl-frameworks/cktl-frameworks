package org.cocktail.fwkcktlpeche.droits;

import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.metier.droits.AutorisationsCache;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;

/**
 * Classe implémentant les méthodes de vérification des droits par profil pour l'application Peche "en dur".
 *
 * @author Chama LAATIK
 * @author Pascal MACOUIN
 *
 */
public class PecheAutorisationsCache extends AutorisationsCache {

	/** Nom de l'application (dans la table GD_APPLICATION). */
	public static final String APP_PECHE = "PECHE";

	/**
	 * Constructeur rendu privé.
	 *
	 * @param appStrId Nom de l'application dans GRHUM.GD_APPLICATION
	 * @param persId persId de la personne connectée
	 */
	private PecheAutorisationsCache(String appStrId, Integer persId) {
		super(appStrId, persId);
	}

	/**
	 * Constructeur.
	 *
	 * Instancie un cache d'autorisation pour l'application PECHE.
	 *
	 * @param persId persId de la personne connectée
	 */
	public PecheAutorisationsCache(Integer persId) {
		this(APP_PECHE, persId);
	}

	// Méthodes utilitaires pour l'affichage de menu
	// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	// Menu 'Enseignants'
	/**
	 * Est-ce que j'ai le droit de voir le menu "Enseignants" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuEnseignants() {
		return hasDroitShowMenuGroupEnseignantsPopulation()
				|| hasDroitShowMenuGroupEnseignantsRepartitionCroisee()
				|| hasDroitShowMenuGroupEnseignantsFicheVoeux()
				|| hasDroitShowMenuGroupEnseignantFicheService();
	}

	/**
	 * Est-ce que j'ai le droit de voir le groupe "Population" du menu "Enseignants" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignantsPopulation() {
		return hasDroitShowEnseignants()
				|| hasDroitShowParametrageEnseignantsGeneriques();
	}

	/**
	 * Est-ce que j'ai le droit de voir le groupe "Répartition croisée" du menu "Enseignants" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignantsRepartitionCroisee() {
		return hasDroitShowRepartitionCroisee();
	}
	
	/**
	 * Est-ce que j'ai le droit de voir le groupe "Fiche de voeux" du menu "Enseignants" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignantsFicheVoeux() {
		return hasDroitShowListeFichesVoeux() || hasDroitShowEnseignantFicheVoeux();
	}

	// Menu 'Enseignements'
	/**
	 * Est-ce que j'ai le droit de voir le menu "Enseignements" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuEnseignements() {
		return hasDroitShowMenuGroupEnseignementsFormation()
				|| hasDroitShowMenuGroupEnseignementsUe();
	}

	/**
	 * Est-ce que j'ai le droit de voir le groupe "Formation" du menu "Enseignements" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignementsFormation() {
		return hasDroitShowOffreFormation();
	}

	/**
	 * Est-ce que j'ai le droit de voir le groupe "UE" du menu "Enseignements" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignementsUe() {
		return hasDroitShowUe();
	}

	// Menu 'Paramétrages'
	/**
	 * Est-ce que j'ai le droit de voir le menu "Paramétrage" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuParametrage() {
		return hasDroitShowMenuGroupParametrageParametrages()
				|| hasDroitShowMenuGroupParametrageHetd()
				|| hasDroitShowMenuGroupParametrageMep()
				|| hasDroitShowParametrageGestionDroits();
	}

	/**
	 * Est-ce que j'ai le droit de voir le groupe "Paramétrages" du menu "Paramétrage" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupParametrageParametrages() {
		return hasDroitShowParametrageCircuitsValidation()
				|| hasDroitShowParametrageActivites();
	}

	/**
	 * Est-ce que j'ai le droit de voir le groupe "MEP" du menu "Paramétrage" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupParametrageMep() {
		return hasDroitShowParametrageTauxHoraires()
				|| hasDroitShowParametrageTypeFluxPaiement();
	}
	/**
	 * Est-ce que j'ai le droit de voir le groupe "HETD" du menu "Paramétrage" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupParametrageHetd() {
		return hasDroitShowParametrageTypesHoraires()
				|| hasDroitShowParametrageParametresPopulations();
	}

	// Méthodes d'accès au droits de niveau fonction
	// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	/**
	 * Est-ce que j'ai le droit d'accéder à l'application 'peche' ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitAccesPeche() {
		return hasDroitUtilisationPeche() || hasDroitConnaissancePeche();
	}

	
	/**
	 * Est-ce que j'ai le droit d'accéder à l'application 'peche' avec les droits de modifications ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationPeche() {
		return hasDroitUtilisationOnFonction(Fonction.PECHE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur Peche ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissancePeche() {
		return hasDroitConnaissanceOnFonction(Fonction.PECHE);
	}

	/**
	 * Est-ce que j'ai le droit de voir les enseignants ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowEnseignants() {
		return (hasDroitUtilisationEnseignants() || hasDroitConnaissanceEnseignants());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les enseignants ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationEnseignants() {
		return hasDroitUtilisationOnFonction(Fonction.ENSEIGNANTS);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les enseignants ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceEnseignants() {
		return hasDroitConnaissanceOnFonction(Fonction.ENSEIGNANTS);
	}

	/**
	 * Est-ce que j'ai le droit de voir les répartitions croisées ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowRepartitionCroisee() {
		return (hasDroitUtilisationRepartitionCroisee() || hasDroitConnaissanceRepartitionCroisee());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les répartitions croisées ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationRepartitionCroisee() {
		return hasDroitUtilisationOnFonction(Fonction.REPARTITION_CROISEE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les répartitions croisées ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceRepartitionCroisee() {
		return hasDroitConnaissanceOnFonction(Fonction.REPARTITION_CROISEE);
	}
	
	
	/**
	 * Est-ce que j'ai le droit de voir les répartitions des heures complémentaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowRepartitionHeuresComplementaires() {
		return (hasDroitUtilisationRepartitionHeuresComplementaires() || hasDroitConnaissanceRepartitionHeuresComplementaires());
	}

	/**
	 * Est-ce que j'ai le droit de saisir la répartition des heures complémentaires pour un enseignant statutaire ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationRepartitionHeuresComplementaires() {
		return hasDroitUtilisationOnFonction(Fonction.REPARTITION_HCOMP);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les répartitions des heures complémentaires ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceRepartitionHeuresComplementaires() {
		return hasDroitConnaissanceOnFonction(Fonction.REPARTITION_HCOMP);
	}
	
	/**
	 * Est-ce que j'ai le droit de voir le report des heures complémentaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowReportHeuresComplementaires() {
		return (hasDroitUtilisationReportHeuresComplementaires() || hasDroitConnaissanceReportHeuresComplementaires());
	}

	/**
	 * Est-ce que j'ai le droit de saisir le report des heures complémentaires pour un enseignant statutaire ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationReportHeuresComplementaires() {
		return hasDroitUtilisationOnFonction(Fonction.REPORT_HCOMP);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le report des heures complémentaires ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceReportHeuresComplementaires() {
		return hasDroitConnaissanceOnFonction(Fonction.REPORT_HCOMP);
	}
	
	/**
	 * Est-ce que j'ai le droit de voir ma fiche en tant qu'enseignant ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignantFicheService() {
		return (hasDroitUtilisationValidationParEnseignant());
	}
	
	/**
	 * Est-ce que j'ai le droit de voir ma fiche de voeux en tant qu'enseignant ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowEnseignantFicheVoeux() {
		return (hasDroitUtilisationValidationParEnseignant());
	}
	
	/**
	 * Est-ce que j'ai le droit de voir la liste des fiches de voeux ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowListeFichesVoeux() {
		return (hasDroitUtilisationValidationParRepartiteur());
	}

	/**
	 * Est-ce que j'ai le droit de voir les offres de formation ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowOffreFormation() {
		return (hasDroitUtilisationOffreFormation() || hasDroitConnaissanceOffreFormation());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les offres de formation ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationOffreFormation() {
		return hasDroitUtilisationOnFonction(Fonction.OFFRE_FORMATION);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les offres de formation ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceOffreFormation() {
		return hasDroitConnaissanceOnFonction(Fonction.OFFRE_FORMATION);
	}

	/**
	 * Est-ce que j'ai le droit de voir les UE ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowUe() {
		return (hasDroitUtilisationUe() || hasDroitConnaissanceUe());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les UE ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationUe() {
		return hasDroitUtilisationOnFonction(Fonction.UE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les UE ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceUe() {
		return hasDroitConnaissanceOnFonction(Fonction.UE);
	}

	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant que directeur de composante ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParDirecteurComposante() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_DIRECTEUR_COMPOSANTE);
	}

	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant que directeur de département ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParDirecteurDepartement() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_DIRECTEUR_DEPARTEMENT);
	}
	
	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant que directeur de collège ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParDirecteurCollege() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_DIRECTEUR_COLLEGE);
	}
	
	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant que DRH ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParDrh() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_DRH);
	}

	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant qu'enseignant ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParEnseignant() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_ENSEIGNANT);
	}

	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant que président ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParPresident() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_PRESIDENT);
	}

	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant que répartiteur ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParRepartiteur() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_REPARTITEUR);
	}

	/**
	 * Est-ce que j'ai le droit de voir les activités ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageActivites() {
		return (hasDroitUtilisationParametrageActivites() || hasDroitConnaissanceParametrageActivites());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les activités ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageActivites() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_ACTIVITES);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les activités ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageActivites() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_ACTIVITES);
	}

	/**
	 * Est-ce que j'ai le droit de voir les circuits de validation ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageCircuitsValidation() {
		return (hasDroitUtilisationParametrageCircuitsValidation() || hasDroitConnaissanceParametrageCircuitsValidation());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les circuits de validation ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageCircuitsValidation() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_CIRCUITS_VALIDATION);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les circuits de validation ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageCircuitsValidation() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_CIRCUITS_VALIDATION);
	}

	/**
	 * Est-ce que j'ai le droit de voir les enseignants génériques ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageEnseignantsGeneriques() {
		return (hasDroitUtilisationParametrageEnseignantsGeneriques() || hasDroitConnaissanceParametrageEnseignantsGeneriques());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les enseignants génériques ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageEnseignantsGeneriques() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_ENSEIGNANTS_GENERIQUES);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le paramétrage des enseignants génériques ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageEnseignantsGeneriques() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_ENSEIGNANTS_GENERIQUES);
	}

	/**
	 * Est-ce que j'ai le droit de voir l'établissement ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageUefChoixComposantes() {
		return (hasDroitUtilisationParametrageUefChoixComposantes() || hasDroitConnaissanceParametrageUefChoixComposantes());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) l'établissement ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageUefChoixComposantes() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_UEF_CHOIX_COMPOSANTES);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le paramétrage établissement ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageUefChoixComposantes() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_UEF_CHOIX_COMPOSANTES);
	}

	/**
	 * Est-ce que j'ai le droit d'utiliser la duplication ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageDuplication() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_DUPLICATION);
	}
	
	/**
	 * Est-ce que j'ai le droit de voir la gestion des droits ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageGestionDroits() {
		return (hasDroitConnaissanceParametrageGestionDroits() || hasDroitUtilisationParametrageGestionDroits());
	}
	
	/**
	 * Est-ce que j'ai le droit de connaissance sur la gestion des droits ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageGestionDroits() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_GESTION_DROITS);
	}
	
	/**
	 * Est-ce que j'ai le droit d'utiliser la gestion des droits ?
	 * 
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageGestionDroits() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_GESTION_DROITS);
	}
	
	/**
	 * Est-ce que j'ai le droit de voir les paramètres des populations ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageParametresPopulations() {
		return (hasDroitUtilisationParametrageParametresPopulations() || hasDroitConnaissanceParametrageParametresPopulations());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les paramètres des populations ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageParametresPopulations() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_PARAMETRES_POPULATIONS);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le paramétrage des populations ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageParametresPopulations() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_PARAMETRES_POPULATIONS);
	}

	/**
	 * Est-ce que j'ai le droit de voir le paramétrage des types d'horaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageTypesHoraires() {
		return (hasDroitUtilisationParametrageTypesHoraires() || hasDroitConnaissanceParametrageTypesHoraires());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) le paramétrage des types d'horaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageTypesHoraires() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_TYPES_HORAIRES);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le paramétrage des types horaires ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageTypesHoraires() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_TYPES_HORAIRES);
	}

	/**
	 * Est-ce que j'ai le droit de voir la surcharge du nombre de groupe ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageNbGroupe() {
		return (hasDroitUtilisationParametrageNbGroupe() || hasDroitConnaissanceParametrageNbGroupe());
	}

	/**
	 * Est-ce que j'ai le droit de surcharger le nombre de groupe dans l'offre de formation ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageNbGroupe() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_NB_GROUPE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la surcharge du nombre de groupe dans l'offre de formation ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageNbGroupe() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_NB_GROUPE);
	}

	/**
	 * Est-ce que j'ai le droit de voir la surcharge du nombre d'heures maquette ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageNbHeureMaquette() {
		return (hasDroitUtilisationParametrageNbHeureMaquette() || hasDroitConnaissanceParametrageNbHeureMaquette());
	}

	/**
	 * Est-ce que j'ai le droit de surcharger le nombre d'heures maquette dans l'offre de formation ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageNbHeureMaquette() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_NB_HEURE_MAQUETTE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la surcharge du nombre d'heures maquette dans l'offre de formation ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageNbHeureMaquette() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_NB_HEURE_MAQUETTE);
	}
	
	/**
	 * Est-ce que j'ai le droit de voir la surcharge du détail de l'offre de formation (nombre de groupe ou nombre 
	 * d'heures maquette des AP) ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageDetailOffreFormation() {
		return (hasDroitShowParametrageNbGroupe() || hasDroitShowParametrageNbHeureMaquette());
	}
	
	/**
	 * Est-ce que j'ai le droit de voir la surcharge du nombre d'heures de l'absence d'un enseignant ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageHeuresAbsence() {
		return (hasDroitUtilisationParametrageHeuresAbsence() || hasDroitConnaissanceParametrageHeuresAbsence());
	}

	/**
	 * Est-ce que j'ai le droit de surcharger le nombre d'heures de l'absence d'un enseignant ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageHeuresAbsence() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_ABSENCE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la surcharge du nombre d'heures de l'absence d'un enseignant ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageHeuresAbsence() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_ABSENCE);
	}

	/**
	 * Est-ce que j'ai le droit de voir le paramétrage des taux horaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageTauxHoraires() {
		return (hasDroitUtilisationParametrageTauxHoraires() || hasDroitConnaissanceParametrageTauxHoraires());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) le paramétrage des taux horaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageTauxHoraires() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_TAUX_HORAIRES);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le paramétrage des taux horaires ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageTauxHoraires() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_TAUX_HORAIRES);
	}

	/**
	 * Est-ce que j'ai le droit de voir le paramétrage des types de flux de paiement ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageTypeFluxPaiement() {
		return (hasDroitUtilisationParametrageTypeFluxPaiement() || hasDroitConnaissanceParametrageTypeFluxPaiement());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) le paramétrage des types de flux de paiement ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageTypeFluxPaiement() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_FLUX_PAIEMENT);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le paramétrage des types de flux de paiement ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceParametrageTypeFluxPaiement() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_FLUX_PAIEMENT);
	}

	
	/**
	 * Est-ce que j'ai le droit de voir les champs de saisie des heures à payer ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowSaisieHeuresAPayer() {
		return (hasDroitUtilisationSaisieHeuresAPayer() || hasDroitConnaissanceSaisieHeuresAPayer());
	}

	/**
	 * Est-ce que j'ai le droit de gérer les heures à payer ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationSaisieHeuresAPayer() {
		return hasDroitUtilisationOnFonction(Fonction.HEURES_A_PAYER);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la gestion des heures à payer ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceSaisieHeuresAPayer() {
		return hasDroitConnaissanceOnFonction(Fonction.HEURES_A_PAYER);
	}

	/**
	 * Est-ce que j'ai le droit de voir la mise en paiement ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMiseEnPaiement() {
		return (hasDroitUtilisationMiseEnPaiement() || hasDroitConnaissanceMiseEnPaiement());
	}

	/**
	 * Est-ce que j'ai le droit de gérer la mise en paiement ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationMiseEnPaiement() {
		return hasDroitUtilisationOnFonction(Fonction.MISE_EN_PAIEMENT);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la mise en paiement ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceMiseEnPaiement() {
		return hasDroitConnaissanceOnFonction(Fonction.MISE_EN_PAIEMENT);
	}

	
	/**
	 * Est-ce que j'ai le droit de saisir la répartition REH des vacataires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationSaisieRehVacataire() {
		return hasDroitUtilisationOnFonction(Fonction.SAISIE_REH_VACATAIRE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la répartition REH des vacataires ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceSaisieRehVacataire() {
		return hasDroitConnaissanceOnFonction(Fonction.SAISIE_REH_VACATAIRE);
	}

	
	/**
	 * Est-ce que j'ai le droit de voir la répartition REH des vacataires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowSaisieRehVacataire() {
		return (hasDroitUtilisationSaisieRehVacataire() || hasDroitConnaissanceSaisieRehVacataire());
	}
	
	/**
	 * Est-ce que j'ai le droit de saisir la répartition REH des statutaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationSaisieRehStatutaire() {
		return hasDroitUtilisationOnFonction(Fonction.SAISIE_REH_STATUTAIRE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la répartition REH des statutaires ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceSaisieRehStatutaire() {
		return hasDroitConnaissanceOnFonction(Fonction.SAISIE_REH_STATUTAIRE);
	}

	
	/**
	 * Est-ce que j'ai le droit de voir la répartition REH des statutaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowSaisieRehStatutaire() {
		return (hasDroitUtilisationSaisieRehStatutaire() || hasDroitConnaissanceSaisieRehStatutaire());
	}
	
	/**
	 * Est-ce que j'ai le droit de voir le bouton "Validation par UE / EC" ?
	 * 
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParUeEc() {
		return hasDroitUtilisationValidationParRepartiteur() 
				|| hasDroitUtilisationValidationParDirecteurComposante() 
				|| hasDroitUtilisationValidationParDirecteurDepartement(); 
	}
	
	
	/**
	 * @return <code>true</code>
	 * @deprecated A supprimer
	 */
	public boolean hasDroitUtilisationService() {
		// TODO Méthode à supprimer
		return true;
	}
	/**
	 * @return <code>true</code>
	 * @deprecated A supprimer
	 */
	public boolean hasDroitConnaissanceService() {
		// TODO Méthode à supprimer
		return true;
	}

	// Méthodes privée de transcodification enum Fonction => String
	// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	/**
	 * Méthode transcodant le paramètre de type {@link Fonction} en String.
	 *
	 * @param fonction Le droit fonction
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitUtilisationOnFonction(Fonction fonction) {
		return hasDroitUtilisationOnFonction(fonction.getIdFonction());
	}

	/**
	 * Méthode transcodant le paramètre de type {@link Fonction} en String.
	 *
	 * @param fonction Le droit fonction
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceOnFonction(Fonction fonction) {
		return hasDroitConnaissanceOnFonction(fonction.getIdFonction());
	}

	// Méthodes d'accès au droits de prise d'un chemin dans un circuit
	// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	/**
	 * Est-ce que j'ai le droit d'emprunter (ou de voir) le chemin depuis l'étape sur laquelle est la demande ?
	 *
	 * @param mode "U" pour tester le droit "Utilisable" ou "C" pour le droit "Connaissance"
	 * @param demande La demande
	 * @param typeChemin Le type de chemin
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitOnChemin(String mode, EODemande demande, TypeCheminPeche typeChemin) {
		EOEtape etapeDemande = demande.toEtape();

		EOChemin chemin = etapeDemande.cheminEmpruntable(typeChemin.getCodeTypeChemin());

		// Si pas de chemin empruntable
		if (chemin == null) {
			return false;
		}

		EtapePeche enumEtape = EtapePeche.getEtape(etapeDemande.toCircuitValidation().codeCircuitValidation(), etapeDemande.codeEtape());
		Fonction[] listeFonctionsAutorisees = enumEtape.getListeFonctionsAutorisees();

		if (listeFonctionsAutorisees == null) {
			// Pas de droits particulier à avoir pour gérer cette étape
			return true;
		}

		// A-t-on un des droits en question ?
		for (int i = 0; i < listeFonctionsAutorisees.length; i++) {
			Fonction fonction = listeFonctionsAutorisees[i];

			if (("U".equals(mode) && hasDroitUtilisationOnFonction(fonction))
					|| ("C".equals(mode) && hasDroitConnaissanceOnFonction(fonction))) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Est-ce que j'ai le droit de voir le chemin depuis l'étape sur laquelle est la demande ?
	 *
	 * @param demande La demande
	 * @param typeChemin Le type de chemin à prendre
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowChemin(EODemande demande, TypeCheminPeche typeChemin) {
		return hasDroitUtilisationChemin(demande, typeChemin) || hasDroitConnaissanceChemin(demande, typeChemin);
	}

	/**
	 * Est-ce que j'ai le droit d'emprunter le chemin depuis l'étape sur laquelle est la demande ?
	 * <p>
	 * Il faut que la demande soit à l'étape indiquée pour avoir le droit.
	 *
	 * @param demande La demande
	 * @param etape L'étape d'origine
	 * @param typeChemin Le type de chemin
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationChemin(EODemande demande, EtapePeche etape, TypeCheminPeche typeChemin) {
		// Pour avoir le droit d'emprunter ce chemin, l'étape en cours de la demande doit être la même que l'étape de départ
		if (etape != null && !etape.getCodeEtape().equals(demande.toEtape().codeEtape())) {
			return false;
		}

		return hasDroitOnChemin("U", demande, typeChemin);
	}

	/**
	 * Est-ce que j'ai le droit d'emprunter le chemin depuis l'étape sur laquelle est la demande ?
	 *
	 * @param demande La demande
	 * @param typeChemin Le type de chemin
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationChemin(EODemande demande, TypeCheminPeche typeChemin) {
		return hasDroitOnChemin("U", demande, typeChemin);
	}

	/**
	 * Est-ce que j'ai le droit de voir le chemin empruntable depuis l'étape sur laquelle est la demande ?
	 *
	 * @param demande La demande
	 * @param typeChemin Le type de chemin
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceChemin(EODemande demande, TypeCheminPeche typeChemin) {
		return hasDroitOnChemin("C", demande, typeChemin);
	}
}
