package org.cocktail.fwkcktlpeche.circuitvalidation;

/**
 * Enumère tous les type de chemins possible dans les circuits de l'application 'peche'.
 *
 * @author Pascal MACOUIN
 */
public enum TypeCheminPeche {
	/** VALIDER **/
	VALIDER ("VALIDER"),
	/** Refuser. */
	REFUSER ("REFUSER");

	/** Code du type de chemin. */
	private String codeTypeChemin;

	/**
	 * Retourne le code du type de chemin.
	 *
	 * @return Le code du type de chemin
	 */
	public String getCodeTypeChemin() {
		return codeTypeChemin;
	}

	/**
	 * Constructeur.
	 *
	 * @param codeTypeChemin Un code type de chemin
	 */
	TypeCheminPeche(String codeTypeChemin) {
		this.codeTypeChemin = codeTypeChemin;
	}
}
