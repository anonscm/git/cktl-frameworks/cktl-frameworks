package org.cocktail.fwkcktlpeche.circuitvalidation;

import org.cocktail.fwkcktlpeche.droits.Fonction;
import org.cocktail.fwkcktlpersonne.common.metier.droits.DroitsHelper;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Permet l'envoie de mail automatique aux personnes ayants les droits sur l'étape d'arrivée.
 * @author etienne
 *
 */
public class CircuitEnvoieMail {

	
	private EOEditingContext edc = null;
	
	
	/**
	 * Constructeur.
	 * @param edc
	 */
	public CircuitEnvoieMail(EOEditingContext edc) {
		this.edc = edc;
	}
	
	
	
	/**
	 * Renvoi la liste des individus ayant les droits sur l'étape passé en paramettre.
	 * @param etape
	 * @return
	 */
	private NSMutableArray<Integer> getIndividusConcernes(EtapePeche etape) {
		//On récupere les droits de l'étape
		Fonction[] fonctionsAuthorisees = etape.getListeFonctionsAutorisees();
		
		//On récuperes tous les individu ayant ces fonctions
		NSMutableArray<Integer> listeIDIndividu = new NSMutableArray<Integer>();
		for(Fonction f : fonctionsAuthorisees) {
			EOGdFonction fonction = EOGdFonction.fetchByKeyValue(edc, "FON_ID_INTERNE", f);   //TODO rajouter test sur APP_ID = id de peche
			NSArray<Integer> persIds = DroitsHelper.personnesWithDroitOnFonction(edc, "U", fonction);
			listeIDIndividu.addObjectsFromArray(persIds);
		}
		
		return listeIDIndividu;
	}
	
	
	
}
