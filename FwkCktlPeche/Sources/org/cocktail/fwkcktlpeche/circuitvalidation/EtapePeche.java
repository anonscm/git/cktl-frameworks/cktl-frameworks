package org.cocktail.fwkcktlpeche.circuitvalidation;

import org.cocktail.fwkcktlpeche.droits.Fonction;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;

/**
 * Enumère toutes les étapes des circuits gérés dans 'Peche'.
 * <p>
 * Chaque étape précise quelles sont les autorisations nécessaires pour faire avancer une demande depuis cette étape.
 * <p>
 * Par exemple :
 *   Pour l'étape {@link #VALID_REPARTITEUR},
 *     il faut être autorisé à la fonction {@link Fonction#VALID_FICHE_REPARTITEUR}.
 *   Pour l'étape {@link #VALID_ENSEIGNANT_FICHE_VOEUX},
 *     il faut être autorisé à la fonction {@link Fonction#VALID_FICHE_ENSEIGNANT}
 *     ou à la fonction {@link Fonction#VALID_FICHE_REPARTITEUR}.
 * 
 * @author Pascal MACOUIN
 */
public enum EtapePeche {
	/** Etape "Fiche à valider par le répartiteur". */
	VALID_REPARTITEUR ("VALID_REPARTITEUR", null, "En validation par le répartiteur", "Visa répartiteur", Fonction.VALID_FICHE_REPARTITEUR),
	/** Etape "Fiche à valider par l'enseignant". */
	VALID_ENSEIGNANT ("VALID_ENSEIGNANT", null, "En validation par l'enseignant", "Visa enseignant", Fonction.VALID_FICHE_ENSEIGNANT),
	/** Etape "Fiche à valider par l'enseignant" pour le circuit "Fiche de voeux". */
	VALID_ENSEIGNANT_FICHE_VOEUX ("VALID_ENSEIGNANT", CircuitPeche.PECHE_VOEUX, "En validation par l'enseignant", "Visa enseignant", Fonction.VALID_FICHE_ENSEIGNANT, Fonction.VALID_FICHE_REPARTITEUR),
	/** Etape "Fiche à valider par le directeur de composante". */
	VALID_DIR_COMPOSANTE ("VALID_DIR_COMPOSANTE", null, "En validation par le dir. comp.", "Visa composante", Fonction.VALID_FICHE_DIRECTEUR_COMPOSANTE),
	/** Etape "Fiche à valider par le directeur de département". */
	VALID_DIR_DEPARTEM ("VALID_DIR_DEPARTEM", null, "En validation par le dir. dép.", "Visa département", Fonction.VALID_FICHE_DIRECTEUR_DEPARTEMENT),
	/** Etape "Fiche à valider par le directeur de collège". */
	VALID_DIR_COLLEGE ("VALID_DIR_COLLEGE", null, "En validation par le dir. coll.", "Visa collège", Fonction.VALID_FICHE_DIRECTEUR_COLLEGE),
	/** Etape "Fiche à valider par la DRH". */
	VALID_DRH ("VALID_DRH", null, "En validation par la DRH", "Visa DRH", Fonction.VALID_FICHE_DRH),
	/** Etape "Fiche à valider par le président". */
	VALID_PRESIDENT ("VALID_PRESIDENT", null, "En validation par le président", "Visa présidence", Fonction.VALID_FICHE_PRESIDENT),
	/** Etape finale "Fiche validée". */
	VALIDEE ("VALIDEE", null, "Fiche validée", "");

	/** Code de l'étape. */
	private String codeEtape;
	
	/**
	 * Circuit de l'étape.
	 * <p>
	 * Ce circuit peut être <code>null</code> pour indiqué "peu importe le circuit" pour une étape multi-circuit.
	 */
	private CircuitPeche circuit;
	
	/** Le texte de l'état de la demande pour cette étape. */
	private String etatDemande;
	
	/** Le libellé du visa pour cette étape. */
	private String libelleVisa;
	
	/**
	 * Liste des fonctions autorisées à faire avancer une demande depuis cette étape.
	 * <p>
	 * Si cette liste est <code>null</code>, n'importe qui à les droits sur cette étape.
	 */
	private Fonction[] listeFonctionsAutorisees;
	
	/**
	 * Constructeur.
	 * 
	 * @param codeEtape Le code de l'étape
	 * @param circuit Le code du circuit (<code>null</code> pour "n'importe quel circuit")
	 * @param etatDemande Le texte de l'état de la demande pour cette étape
	 * @param libelleVisa Libellé du visa (devant la date de visa)
	 * @param fonctions Liste des fonction autorisées à faire avancer une demande depuis cette étape (<code>null</code> pour "n'importe qui")
	 */
	EtapePeche(String codeEtape, CircuitPeche circuit, String etatDemande, String libelleVisa, Fonction... fonctions) {
		this.circuit = circuit;
		this.codeEtape = codeEtape;
		this.etatDemande = etatDemande;
		this.libelleVisa = libelleVisa;
		this.listeFonctionsAutorisees = fonctions;
	}

	/**
	 * Retourne l'étape correspondant à l'étape.
	 * <p>
	 * L'étape retournée en priorité est celle correspondant au code circuit et au code étape.
	 * Si elle n'est pas trouvé celle correspondant au code étape qui à un code circuit à <code>null</code>.
	 * 
	 * @param etape Une étape
	 * @return L'étape si trouvée ou <code>null</code>
	 */
	public static EtapePeche getEtape(EOEtape etape) {
		return getEtape(etape.toCircuitValidation().codeCircuitValidation(), etape.codeEtape());
	}
	
	/**
	 * Retourne l'étape correspondant au code circuit et au code étape.
	 * <p>
	 * L'étape retournée en priorité est celle correspondant au code circuit et au code étape.
	 * Si elle n'est pas trouvé celle correspondant au code étape qui à un code circuit à <code>null</code>.
	 * 
	 * @param codeCircuit Un code circuit
	 * @param codeEtape Un code étape
	 * @return L'étape si trouvée ou <code>null</code>
	 */
	public static EtapePeche getEtape(String codeCircuit, String codeEtape) {
		EtapePeche resultat = null;
		
		for (int i = 0; i < EtapePeche.values().length; i++) {
			EtapePeche etape = EtapePeche.values()[i];
			
			if (etape.codeEtape.equals(codeEtape)) {
				if (etape.circuit == null) {
					resultat = etape;
				} else if (etape.circuit.getCodeCircuit().equals(codeCircuit)) {
					// Niveau le plus fin trouvé, on sort
					resultat = etape;
					break;
				}
			}
		}
		
		return resultat;
	}
	
	/**
	 * Retourne le code étape de cette étape.
	 * 
	 * @return Le code étape
	 */
	public String getCodeEtape() {
		return codeEtape;
	}

	/**
	 * Retourne le texte de l'état de la demande pour cette étape.
	 * 
	 * @return Le texte de l'état de la demande
	 */
	public String getEtatDemande() {
		return etatDemande;
	}

	/**
	 * Retourne le libellé du visa pour cette étape.
	 * 
	 * @return Le libellé du visa
	 */
	public String getLibelleVisa() {
		return libelleVisa;
	}

	/**
	 * Retourne la liste des fonctions autorisées. Peut retourner <code>null</code>.
	 * 
	 * @return La liste des fonctions autorisées. Peut retourner <code>null</code>.
	 */
	public Fonction[] getListeFonctionsAutorisees() {
		return listeFonctionsAutorisees;
	}
}
