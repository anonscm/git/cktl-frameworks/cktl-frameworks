package org.cocktail.fwkcktlpeche.circuitvalidation;

import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;

/**
 * Enumère tous les circuits de l'application 'peche'.
 *
 * @author Pascal MACOUIN
 */
public enum CircuitPeche {
	/** Circuit de la fiche de service prévisionnel pour les statutaires. */
	PECHE_STATUTAIRE_PREVISIONNEL ("PECHE_PREV_STATUTAIRE", false),
	/** Circuit de la fiche de service des statutaires. */
	PECHE_STATUTAIRE ("PECHE_STATUTAIRE", true),
	/** Circuit de la fiche de service prévisionnel pour les vacataires. **/
	PECHE_VACATAIRE_PREVISIONNEL ("PECHE_PREV_VACATAIRE", false),
	/** Circuit de la fiche de service du vacataire. */
	PECHE_VACATAIRE ("PECHE_VACATAIRE", true),
	/** Circuit de la fiche de voeux du statutaire. */
	PECHE_VOEUX ("PECHE_VOEUX", true);
	
	/** Code du circuit. */
	private String codeCircuit;

	/** Est-ce que le circuit est le circuit fiche de service définitif ? */
	private boolean circuitDefinitif;

	/**
	 * Retourne le code du circuit.
	 *
	 * @return Le code du circuit
	 */
	public String getCodeCircuit() {
		return codeCircuit;
	}

	/**
	 * Est-ce que le circuit est le circuit fiche de service définitif ?
	 * 
	 * @return <code>true</code> si c'est un circuit définitif
	 */
	public boolean isCircuitDefinitif() {
		return circuitDefinitif;
	}

	/**
	 * Constructeur.
	 *
	 * @param codeCircuit Un code circuit
	 */
	CircuitPeche(String codeCircuit, boolean circuitDefinitif) {
		this.codeCircuit = codeCircuit;
		this.circuitDefinitif = circuitDefinitif;
	}
	
	/**
	 * Retourne le circuit correspondant à ce circuit.
	 * 
	 * @param circuit Un circuit
	 * @return Le circuit si trouvée ou <code>null</code>
	 */
	public static CircuitPeche getCircuit(EOCircuitValidation circuit) {
		return getCircuit(circuit.codeCircuitValidation());
	}
	
	/**
	 * Retourne le circuit correspondant à ce circuit.
	 * 
	 * @param codeCircuit Un code circuit
	 * @return Le circuit si trouvée ou <code>null</code>
	 */
	public static CircuitPeche getCircuit(String codeCircuit) {
		CircuitPeche resultat = null;
		
		for (int i = 0; i < CircuitPeche.values().length; i++) {
			CircuitPeche circuit = CircuitPeche.values()[i];
			
			if (circuit.codeCircuit.equals(codeCircuit)) {
				resultat = circuit;
				break;
			}
		}
		
		return resultat;
	}	
}
