// DO NOT EDIT.  Make changes to EOMailModel.java instead.
package org.cocktail.fwkcktlpeche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOMailModel extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "MailModel";

  // Attribute Keys
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> MESSAGE = new ERXKey<String>("message");
  public static final ERXKey<Integer> NB_PARAMETRES = new ERXKey<Integer>("nbParametres");
  // Relationship Keys

  // Attributes
  public static final String ID_KEY = ID.key();
  public static final String MESSAGE_KEY = MESSAGE.key();
  public static final String NB_PARAMETRES_KEY = NB_PARAMETRES.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOMailModel.class);

  public EOMailModel localInstanceIn(EOEditingContext editingContext) {
    EOMailModel localInstance = (EOMailModel)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOMailModel.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOMailModel.LOG.isDebugEnabled()) {
    	_EOMailModel.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMailModel.ID_KEY);
  }

  public String message() {
    return (String) storedValueForKey(_EOMailModel.MESSAGE_KEY);
  }

  public void setMessage(String value) {
    if (_EOMailModel.LOG.isDebugEnabled()) {
    	_EOMailModel.LOG.debug( "updating message from " + message() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMailModel.MESSAGE_KEY);
  }

  public Integer nbParametres() {
    return (Integer) storedValueForKey(_EOMailModel.NB_PARAMETRES_KEY);
  }

  public void setNbParametres(Integer value) {
    if (_EOMailModel.LOG.isDebugEnabled()) {
    	_EOMailModel.LOG.debug( "updating nbParametres from " + nbParametres() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMailModel.NB_PARAMETRES_KEY);
  }


  public static EOMailModel createMailModel(EOEditingContext editingContext, Integer id
, String message
, Integer nbParametres
) {
    EOMailModel eo = (EOMailModel) EOUtilities.createAndInsertInstance(editingContext, _EOMailModel.ENTITY_NAME);    
		eo.setId(id);
		eo.setMessage(message);
		eo.setNbParametres(nbParametres);
    return eo;
  }

  public static ERXFetchSpecification<EOMailModel> fetchSpec() {
    return new ERXFetchSpecification<EOMailModel>(_EOMailModel.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOMailModel> fetchAllMailModels(EOEditingContext editingContext) {
    return _EOMailModel.fetchAllMailModels(editingContext, null);
  }

  public static NSArray<EOMailModel> fetchAllMailModels(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMailModel.fetchMailModels(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMailModel> fetchMailModels(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOMailModel> fetchSpec = new ERXFetchSpecification<EOMailModel>(_EOMailModel.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMailModel> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOMailModel fetchMailModel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMailModel.fetchMailModel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMailModel fetchMailModel(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMailModel> eoObjects = _EOMailModel.fetchMailModels(editingContext, qualifier, null);
    EOMailModel eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one MailModel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMailModel fetchRequiredMailModel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMailModel.fetchRequiredMailModel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMailModel fetchRequiredMailModel(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMailModel eoObject = _EOMailModel.fetchMailModel(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no MailModel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMailModel localInstanceIn(EOEditingContext editingContext, EOMailModel eo) {
    EOMailModel localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
