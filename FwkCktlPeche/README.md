Gestion d'un Circuit de Validation
==================================

Tout d'abord, quelques notions qui serviront pour la suite :

- Un circuit de validation est composé d'étapes ;
- Les étapes sont reliées entres elles par des chemins ;
- Ces chemins sont unidirectionnels ;
- Un chemin relie une étape de départ à une étape d'arrivée ;
- Une seule étape sur un circuit donné peut être l'étape initiale ;
- Ce qui circule sur le circuit est une demande ;
- Quand une demande est créée, elle est placée sur l'étape initiale ;
- Une demande porte sur un sujet ;
- Elle n'a pas besoin de connaître spécifiquement son sujet ;
- Une demande peut avancer vers une autre étape s'il existe un chemin ;
- Chaque changement d'étape donne lieu à une historisation.


Un circuit de validation est donc l'ensemble des étapes qui le composent. Ce circuit peut-être représenté sous forme de graphe :

Exemple :

    +----------------+   +---------+                 +---------+
    | Etape initiale |-->| Etape 2 |---------------->| Etape 4 |
    +----------------+   +---------+                 +---------+
                              |                           ^
                              |        +---------+        |
                              +------->| Etape 3 |--------+
                                       +---------+

Dans cet exemple, il n'y a qu'un chemin pour passer de l'étape initiale à l'étape 2. Pour quitter l'étape 2, en revanche, il y a deux chemins
possibles. C'est l'application utilisatrice qui décidera quel chemin
emprunter.

Il n'existe pas encore (au 25/10/2012) d'interface pour créer des circuits. Il sont donc entré à la main dans la base de données.

La classe EODemande
-------------------

    +----------------------+
    |      EODemande       |
    +----------------------+
    | creerDemande()       |
    | faireAvancer()       |
    | faireAvancerVers()   |
    | qualifierPourEtape() |
    +----------------------+

Utilisation de l'outil
======================

Création d'une demande
----------------------

>     EODemande.creerDemande(
>             EOEditingContext ec, /* L'éditing context pour accéder à la base */
>             String codeCircuit, /* Le circuit dans lequel on va placer la demande */
>             Integer persIdCreation /* la personne qui créer la demande */
>     );

Exemple :

>     EODemande demande = EODemande.creerDemande(ec, "DEFAUT", appUser.persId());

Ceci permet de créer une demande en base de donnée, et de déclencher
l'enregistrement d'une trace de création.

La demande est automatiquement placée sur l'étape initiale du circuit.

Lire une demande depuis la base de données
------------------------------------------

Il n'y a pas de méthode pour lire directement une demande depuis la base. En effet, en général on souhaite charger également les sujets associés aux demandes.

L'outil fournit donc une méthode qui fabrique un EOQualifier permettant de charger les demandes en même temps que les sujets.

>     EOQualifier.qualifierPourEtape(
>             String codeEtape, /* L'étape */
>             String codeCircuit /* Le circuit */
>     );

Exemple :

>     EOQualifier qualifier = EODemande.qualifierPourEtape(EOFicheService.DEMANDE, "ETAPE_2", "DEFAUT");
>     NSArray<EOFicheService> fiches = EOFicheServices.fetchFicheServices(ec, qualifier, null);

Ici EOFicheService.DEMANDE indique que dans l'EOModeler, on a créé une relation entre EOFicheService et EODemande, et que cette relation se nomme "demande".
Elle permettra de faire "ficher.demande()" pour récupérer la demande :

>     EODemande demande = fiches.get(0).demande();

Le qualifier peut être utiliser pour rafiner une recherche :

>     EOQualifier qualifier = EOFicheService.REPARTITEUR.eq(repartiteur).and(EODemande.qualifierPourEtape(EOFicheService.DEMANDE, "ETAPE_2", "DEFAUT"));

Faire avancer une demande sur le circuit
----------------------------------------

Pour faire avancer une demande à une étape suivante, il faut appeler

>     EODemande.faireAvancerVers(String codeEtape, Integer persIdModification);

Exemple :

>     demande.faireAvancerVers("ETAPE_3", appUser.persId());

L'étape d'arrivée doit être directement reliée à l'étape de départ faute de quoi une exception est levée.

Dans le cas ou il n'existe qu'un seul chemin entre l'étape de départ et
l'étape d'arrivée, il est possible d'appeler la méthode

>     EODemande.faireAvancer(Integer persIdModification());

Une exception est levée s'il n'y a pas de chemin partant de l'étape de départ, ou s'il en existe plusieurs.

