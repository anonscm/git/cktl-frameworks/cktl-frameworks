WHENEVER SQLERROR EXIT SQL.SQLCODE;

--Passez ces drop avant de faire les insertions, sql developper a tendance a commencer les sequences à partir de 2
DROP SEQUENCE GRH_PECHE.CIRCUIT_VALIDATION_SEQ;
DROP SEQUENCE GRH_PECHE.ETAPE_SEQ;
DROP SEQUENCE GRH_PECHE.CHEMIN_SEQ;
CREATE SEQUENCE GRH_PECHE.CIRCUIT_VALIDATION_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;
CREATE SEQUENCE GRH_PECHE.ETAPE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;
CREATE SEQUENCE GRH_PECHE.CHEMIN_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

DECLARE
	persid_createur integer;
	flag integer;
	id_app number(12,0);
	id_circuit number;
	id_etape_depart number;
	id_etape_arrivee number;
	id_chemin number;
BEGIN
	
	  -- recup d'un createur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de recuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	-- Application
	-- ***********
	SELECT APP_ID INTO id_app FROM GRHUM.GD_APPLICATION WHERE APP_STR_ID = 'PECHE';
	
	-- Circuit 'PECHE_PREVISIONNEL'
	-- ****************************
	id_circuit := GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_PREVISIONNEL', 1, 'Circuit par défaut : REPARTITEUR, DIRECTEUR COMPOSANTE, ENSEIGNANT, PRESIDENT', id_app, 'O', persid_createur);

    -- Etapes
    id_etape_depart := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');
	id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DIR_COMPOSANTE', 'Validation par le directeur de la composante', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_PRESIDENT', 'Validation par le président', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	
  	
	-- Circuit 'PECHE TITULAIRE'
	-- *************************
	id_circuit := GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_TITULAIRE', 1, 'Circuit pour les titulaires : REPARTITEUR, ENSEIGNANT, DIRECTEUR COMPOSANTE, PRESIDENT', id_app, 'O', persid_createur);

    -- Etapes
    id_etape_depart := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');
	id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DIR_COMPOSANTE', 'Validation par le directeur de la composante', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_PRESIDENT', 'Validation par le président', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
  
  -- Etape non branchée (optionnelle)
    id_etape_depart := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_depart, 'VALID_DRH', 'Validation par la direction des ressoures humaines', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, null, 'VALIDER', 'Valider');
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, null, 'REFUSER', 'Refuser');
    
	-- Circuit 'PECHE_VACATAIRE'
	-- *************************
	id_circuit := GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_VACATAIRE', 1, 'Circuit où la DRH est obligatoire : REPARTITEUR, DRH, ENSEIGNANT, DIRECTEUR COMPOSANTE, PRESIDENT', id_app, 'O', persid_createur);

    -- Etapes
    id_etape_depart := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');
	id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DRH', 'Validation par la direction des ressoures humaines', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DIR_COMPOSANTE', 'Validation par le directeur de la composante', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_PRESIDENT', 'Validation par le président', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	
	-- Circuit 'PECHE_VOEUX'
	-- *********************
	id_circuit := GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_VOEUX', 1, 'Circuit de la fiche de vœux : ENSEIGNANT, REPARTITEUR', id_app, 'O', persid_createur);

    -- Etapes
    id_etape_depart := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit, 'O');
	id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
    
	id_etape_depart := id_etape_arrivee;
    id_etape_arrivee := GRH_PECHE.ETAPE_SEQ.NEXTVAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	id_chemin := GRH_PECHE.CHEMIN_SEQ.NEXTVAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	
	--Validation
	commit;
END;
/
