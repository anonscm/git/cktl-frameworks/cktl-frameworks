package org.cocktail.fwkcktlpeche.droits;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import com.wounit.rules.MockEditingContext;

public class DroitsUtilsTest {

	@Rule
	public MockEditingContext ec = new MockEditingContext("FwkCktlPeche");

	private PecheAutorisationsCache cache;

	@Before
	public void setUp() throws Exception {
		cache = Mockito.mock(PecheAutorisationsCache.class);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDroitsFonctions() {
		IDroitsUtils droitsUtils = new DroitsUtils(cache);
		
		Mockito.when(cache.hasDroitConnaissanceOnFonction("FCT1")).thenReturn(true);
		Mockito.when(cache.hasDroitUtilisationOnFonction("FCT1")).thenReturn(true);
		Assert.assertTrue(droitsUtils.fonctionAutoriseeEnConsultation("FCT1"));
		Assert.assertTrue(droitsUtils.fonctionAutoriseeEnModification("FCT1"));
		
		Mockito.when(cache.hasDroitConnaissanceOnFonction("FCT1")).thenReturn(false);
		Mockito.when(cache.hasDroitUtilisationOnFonction("FCT1")).thenReturn(true);
		Assert.assertTrue(droitsUtils.fonctionAutoriseeEnConsultation("FCT1"));
		Assert.assertTrue(droitsUtils.fonctionAutoriseeEnModification("FCT1"));
		
		Mockito.when(cache.hasDroitConnaissanceOnFonction("FCT1")).thenReturn(true);
		Mockito.when(cache.hasDroitUtilisationOnFonction("FCT1")).thenReturn(false);
		Assert.assertTrue(droitsUtils.fonctionAutoriseeEnConsultation("FCT1"));
		Assert.assertFalse(droitsUtils.fonctionAutoriseeEnModification("FCT1"));
		
		Mockito.when(cache.hasDroitConnaissanceOnFonction("FCT1")).thenReturn(false);
		Mockito.when(cache.hasDroitUtilisationOnFonction("FCT1")).thenReturn(false);
		Assert.assertFalse(droitsUtils.fonctionAutoriseeEnConsultation("FCT1"));
		Assert.assertFalse(droitsUtils.fonctionAutoriseeEnModification("FCT1"));
	}
	
	@Test
	public void testDroitsDonees() {
		IDroitsUtils droitsUtils = new DroitsUtils(cache);
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(false);
		Assert.assertFalse(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertFalse(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(false);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertFalse(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(false);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertFalse(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(false);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertFalse(droitsUtils.donneeAccessibleEnModification("DATA1"));

		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(false);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertTrue(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(false);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertTrue(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(false);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertTrue(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(false);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertTrue(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(true);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertFalse(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(true);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertFalse(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(true);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertFalse(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(true);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertFalse(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(true);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertTrue(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(true);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertTrue(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(false);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(true);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertTrue(droitsUtils.donneeAccessibleEnModification("DATA1"));
		
		Mockito.when(cache.hasDroitCreationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitLectureOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitModificationOnDonnee("DATA1")).thenReturn(true);
		Mockito.when(cache.hasDroitSuppressionOnDonnee("DATA1")).thenReturn(true);
		Assert.assertTrue(droitsUtils.donneeAccessibleEnLecture("DATA1"));
		Assert.assertTrue(droitsUtils.donneeAccessibleEnModification("DATA1"));
	}
}
