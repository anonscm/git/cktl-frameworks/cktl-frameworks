package org.cocktail.fwkcktlpeche.circuitvalidation;

import junit.framework.Assert;

import org.junit.Test;

public class EtapeTest {

	/**
	 * Test de la méthode {@link EtapePeche#getEtape(String, String)}.
	 */
	@Test
	public void testGetEtape() {
		// Recherche d'une étape qui n'existe pas
		EtapePeche etape = EtapePeche.getEtape("BIDON", "BIDON");
		Assert.assertNull(etape);
		
		// Recherche d'une étape multi-circuit
		etape = EtapePeche.getEtape("BIDON", EtapePeche.VALID_DRH.getCodeEtape());
		Assert.assertNotNull(etape);
		Assert.assertEquals(EtapePeche.VALID_DRH, etape);

		// Recherche d'une étape multi-circuit pour laquelle il y a une spécificité sur un autre circuit
		etape = EtapePeche.getEtape("BIDON", EtapePeche.VALID_ENSEIGNANT.getCodeEtape());
		Assert.assertNotNull(etape);
		Assert.assertEquals(EtapePeche.VALID_ENSEIGNANT, etape);

		// Recherche d'une étape mono-circuit pour laquelle il y a une spécificité sur un circuit
		etape = EtapePeche.getEtape(CircuitPeche.PECHE_VOEUX.getCodeCircuit(), EtapePeche.VALID_ENSEIGNANT.getCodeEtape());
		Assert.assertNotNull(etape);
		Assert.assertEquals(EtapePeche.VALID_ENSEIGNANT_FICHE_VOEUX, etape);
	}
}
