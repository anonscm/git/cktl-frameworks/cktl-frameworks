function openWinRechercheSelect (href, id, title, closable, width, height, className, updateContainerId) {
	  var win = Windows.getWindow(id);

	  if (closable==null) {
	    closable=false;
	  }
	  if (width==null) {
	    width=300;
	  }
	  if (height==null) {
	    height=200;
	  }
	  if (typeof(win)=='undefined') {
		  if (className==null) {
			  className="greenlighting";
		  }
	  	win = new Window(id,{className: className, title: title, url: href, destroyOnClose:true, recenterAuto:true, resizable:true, closable:closable, minimizable:false, maximizable:false,  minWidth:width, minHeight:height,  showEffectOptions: {duration:0.5}});
		var editorOnClose = { 
			onClose: function(eventName, win) {
				//eval(id+'_containerOnCloseUpdate()'); 
				if (updateContainerId != null) {
					eval(updateContainerId+'Update()'); 
				}
			} ,
			onDestroy: function(eventName, win) {
				Windows.removeObserver(editorOnClose);
			} 
		};
		Windows.addObserver(editorOnClose);		
		win.setDestroyOnClose();	  	
	  	
	  	win.setZIndex(99999);
	  	win.showCenter(true);
	  } else {
	    win.showCenter();
	  }
}

closeWindow = function(id, event) {
	Windows.close(id, event);
}
