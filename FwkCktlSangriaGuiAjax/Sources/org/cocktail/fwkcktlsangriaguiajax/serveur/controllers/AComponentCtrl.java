package org.cocktail.fwkcktlsangriaguiajax.serveur.controllers;


import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.eocontrol.EOEditingContext;


public abstract class AComponentCtrl {
	
	private AFwkCktlSangriaComponent component;
	
	public AComponentCtrl(AFwkCktlSangriaComponent component) {
		this.component = component;
	}
	
	protected EOEditingContext edc() {
		return component.edc();
	}	
	
	public AFwkCktlSangriaComponent getComponent() {
		return component;
	}
	
	public void setComponent(AFwkCktlSangriaComponent component) {
		this.component = component;
	}	

}
