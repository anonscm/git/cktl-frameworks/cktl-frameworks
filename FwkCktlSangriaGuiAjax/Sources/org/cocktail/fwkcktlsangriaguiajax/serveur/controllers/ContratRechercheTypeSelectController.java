package org.cocktail.fwkcktlsangriaguiajax.serveur.controllers;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.server.metier.convention.EOTypeContrat;
import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderTypeContrat;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlgfcguiajax.server.controler.CktlAjaxControler;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.ContratRechercheTypeSelect;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;

import er.ajax.AjaxTreeModel;
import er.ajax.CktlAjaxModalDialog;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class ContratRechercheTypeSelectController  extends CktlAjaxControler {
	
	private Object _delegate;
	private TypeContrat rootNatureContrat;
	private TypeContrat uneNatureContrat;
	private ContratRechercheTypeSelect wocomponent;
	
	public ContratRechercheTypeSelectController(ContratRechercheTypeSelect component) {
		this(component, null);
	}
	public ContratRechercheTypeSelectController(ContratRechercheTypeSelect component,EOEditingContext edc) {
		super(component, edc);
		wocomponent = (ContratRechercheTypeSelect)super.wocomponent;
	}
	

	public WOActionResults afficherNatureContratSelectionnee() {
		TypeContrat natureContratNode = uneNatureContrat();
		if (natureContratNode != null) {
			((ContratRechercheTypeSelect)wocomponent).setSelection(natureContratNode);
		}
		CktlAjaxWindow.close(wocomponent.context(),wocomponent.typeContratSearchWindowId());
		return null;
	}
	
	public Boolean natureContratSelectionneeEstCategorie() {
		return uneNatureContrat().tyconNature().equalsIgnoreCase("CATEGORIE");
	}

	public TypeContrat rootNatureContrat() {
		if (rootNatureContrat == null) {
			FinderTypeContrat ftc = new FinderTypeContrat(edc);
			try {
				rootNatureContrat = ftc.findWithTyconIdInterne("RACINE");
			} catch (ExceptionFinder e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
		}
		return rootNatureContrat;
	}

	/**
	 * @param rootOrgan the rootNatureContrat to set
	 */
	public void setRootNatureContrat(TypeContrat rootNatureContrat) {
		this.rootNatureContrat = rootNatureContrat;
	}

	/*
	/**
	 * @return the TypeContrat
	 */
	public TypeContrat uneNatureContrat() {
		return uneNatureContrat;
	}

	/**
	 * @param unOrgan the unOrgan to set
	 */
	public void setUneNatureContrat(TypeContrat uneNatureContrat) {
		this.uneNatureContrat = uneNatureContrat;
	}


	public Object delegate() {
		if (_delegate == null) {
			_delegate = new ContratRechercheTypeSelectController.Delegate();
		}
		return _delegate;

	}
	public static class Delegate implements AjaxTreeModel.Delegate {

		public NSArray childrenTreeNodes(Object node) {
			NSArray childrenTreeNodes = null;
			TypeContrat treeNode = (TypeContrat)node;
			childrenTreeNodes = treeNode.fils();
			EOSortOrdering libelleOrdering = EOSortOrdering.sortOrderingWithKey(EOTypeContrat.TYCON_LIBELLE_KEY, EOSortOrdering.CompareAscending); 
			childrenTreeNodes = EOSortOrdering.sortedArrayUsingKeyOrderArray(childrenTreeNodes, new NSArray(libelleOrdering));
			return childrenTreeNodes;
		}

		public boolean isLeaf(Object node) {
			boolean isLeaf = true;
			TypeContrat treeNode = (TypeContrat)node;
			NSArray fils = treeNode.fils();
			if (fils!=null && fils.count()>0) {
				isLeaf = false;
			}
			return isLeaf;
		}

		public Object parentTreeNode(Object node) {
			TypeContrat pere = null;
			TypeContrat treeNode = (TypeContrat)node;
			if (treeNode != null && treeNode.pere() != null) {
				pere = treeNode.pere();
			}
			
			return pere;
		}
	}
}
