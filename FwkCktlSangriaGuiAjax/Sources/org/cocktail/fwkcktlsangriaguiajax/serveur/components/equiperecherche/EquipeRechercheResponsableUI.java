package org.cocktail.fwkcktlsangriaguiajax.serveur.components.equiperecherche;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

public class EquipeRechercheResponsableUI extends AFwkCktlSangriaComponent {
	
	private Boolean afficherTousLesResponsables = false;
	
	private EOIndividu responsableSelectionne;
	
	private NSTimestamp dateDebutResponsabilite;
	private NSTimestamp dateFinResponsabilite;
	
	
	
	public static final String BINDING_equipeRecherche = "equipeRecherche";
	private EOStructure equipeRecherche;

	private final static String ETAPE_AJOUT_RESPONSABLE = "etapeAjoutResponsable";
	private final static String ETAPE_CHOIX_DATE = "etapeChoixDate";
	
	private String etapeAssistantResponsable = ETAPE_AJOUT_RESPONSABLE;
	
	private EORepartAssociation currentAffectation;
	private EOIndividu currentResponsable;
	private Integer affectationResponsableIndex;
	
	
	private EOIndividu gestionnaireAdminEtFinEquipe;

//	private EOEditingContext personnesEditingContext;
	
    public EquipeRechercheResponsableUI(WOContext context) {
        super(context);
    }
	
//    public EOEditingContext personnesEditingContext() {
//		if(personnesEditingContext == null) {
//			personnesEditingContext = ERXEC.newEditingContext(edc());
//		}
//		return personnesEditingContext;
//	}
    
	public EOStructure equipeRecherche() {
		if(equipeRecherche == null) {
			if(hasBinding(BINDING_equipeRecherche)) {
				equipeRecherche = (EOStructure) valueForBinding(BINDING_equipeRecherche);
			}
		}
		
		return equipeRecherche;
	}
    
    public WOActionResults switchAffichageResponsables() {
    	afficherTousLesResponsables = !afficherTousLesResponsables;
    	return doNothing();
    }
    
    public Boolean getAfficherTousLesResponsables() {
		return afficherTousLesResponsables;
	}

	public void setAfficherTousLesResponsables(Boolean afficherTousLesResponsables) {
		this.afficherTousLesResponsables = afficherTousLesResponsables;
	}

	public String responsableSearchWindowId() {
    	return getComponentId() + "_responsableSearchWindow";
    }
    
    public String responsablesContainerId() {
    	return getComponentId() + "_responsablesContainer";
    }
    
    public String assistantResponsableContainerId() {
    	return getComponentId() + "_assistantResponsableContainer";
    }
       
    public String validationAjoutNotificationEchecId() {
    	return getComponentId() + "_validationAjoutNotificationEchec";
    }
    
    public String textAffichageResponsables() {
    	if(afficherTousLesResponsables) {
    		return "Masquer l'historique";
    	}
    	else {
    		return "Afficher l'historique";
    	}
    }

	public void setResponsableSelectionne(EOIndividu responsableSelectionne) {
		this.responsableSelectionne = responsableSelectionne;
	}

	public EOIndividu responsableSelectionne() {
		return responsableSelectionne;
	}
	
	public WOActionResults onSelectResponsable() {
		if(responsableSelectionne() == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez sélectionner une personne");
//			personnesEditingContext().revert();
			return doNothing();
		}
//		try {
//			responsableSelectionne().setValidationEditingContext(personnesEditingContext());
//			responsableSelectionne().setPersIdModification(utilisateurPersId());
//			personnesEditingContext().saveChanges();
//		} catch (ValidationException e) {
//			personnesEditingContext().revert();
//			session().addSimpleErrorMessage("Erreur", e);
//			return doNothing();
//		}
		
		this.etapeAssistantResponsable = ETAPE_CHOIX_DATE;
		return doNothing();
	}
	
	
	public WOActionResults retourSelectionResponsable() {
		this.etapeAssistantResponsable = ETAPE_AJOUT_RESPONSABLE;
		return doNothing();
	}
	
	public Boolean isEtapeAjoutResponsable() {
		return etapeAssistantResponsable.equalsIgnoreCase(ETAPE_AJOUT_RESPONSABLE);
	}
	
	public Boolean isEtapeChoixDate() {
		return etapeAssistantResponsable.equalsIgnoreCase(ETAPE_CHOIX_DATE);
	}
	
	public String jsOnFailureSelectResponsable() {
		return "Des informations relatives au respnsable sélectionné sont invalides. Vous ne pouvez pas utiliser cette personne pour le moment.";
	}

	public void setDateDebutResponsabilite(NSTimestamp dateDebutResponsabilite) {
		this.dateDebutResponsabilite = dateDebutResponsabilite;
	}

	public NSTimestamp dateDebutResponsabilite() {
		return dateDebutResponsabilite;
	}

	public void setDateFinResponsabilite(NSTimestamp dateFinResponsabilite) {
		this.dateFinResponsabilite = dateFinResponsabilite;
	}

	public NSTimestamp dateFinResponsabilite() {
		return dateFinResponsabilite;
	}
	
	
	
	public WOActionResults validerAjoutResponsable() {
		
		if(dateDebutResponsabilite == null) {
			session().addSimpleErrorMessage("Erreur", "La date de début doit être précisée");
			context().response().setStatus(500);
			return doNothing();			
		}
		if(dateFinResponsabilite == null) {
			session().addSimpleErrorMessage("Erreur", "La date de fin doit être précisée");
			context().response().setStatus(500);
			return doNothing();			
		}		
		if(dateDebutResponsabilite().after(dateFinResponsabilite())) {
			session().addSimpleErrorMessage("Erreur", "La date de début doit être inférieure à la date de fin");
			context().response().setStatus(500);
			return doNothing();
		}
		
		
		EOQualifier qualifier = ERXQ.and(
											ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, getFactoryAssociation().responsableAdministratifAssociation(edc())),
											ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY, equipeRecherche()),
											ERXQ.or(
													ERXQ.and(
															ERXQ.greaterThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, dateDebutResponsabilite()),
															ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_OUVERTURE_KEY, dateDebutResponsabilite())
															)
													,
													ERXQ.and(
															ERXQ.greaterThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, dateFinResponsabilite()),
															ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_OUVERTURE_KEY, dateFinResponsabilite())
															)
													)
										);
		ERXFetchSpecification<EORepartAssociation> fetchSpecification = new ERXFetchSpecification<EORepartAssociation>(EORepartAssociation.ENTITY_NAME, qualifier, null);
		fetchSpecification.setIncludeEditingContextChanges(true);
		NSArray<EORepartAssociation> repartAssociations = fetchSpecification.fetchObjects(edc());
		EORepartAssociation repartAssociation = null;
		if(!repartAssociations.isEmpty()) {
			repartAssociation = repartAssociations.get(0);
		}
		
		if(!(repartAssociation == null)) {
				EOIndividu individuChevauche = (EOIndividu) repartAssociation.toIndividusAssocies().get(0);
				NSTimestampFormatter nsTimestampFormatter = new NSTimestampFormatter("%d.%m.%y");
				
				session().addSimpleErrorMessage("Erreur", "La période de responsabilité choisie se chevauche avec celle de " + individuChevauche.getNomAndPrenom() + " du " + nsTimestampFormatter.format(repartAssociation.rasDOuverture()) + " au " + nsTimestampFormatter.format(repartAssociation.rasDFermeture()));
				context().response().setStatus(500);
				return doNothing();				
		}
		
		EORepartAssociation affectationNouveauResponsable = EORepartAssociation.createEORepartAssociation(edc(), responsableSelectionne().persId(), 1, getFactoryAssociation().responsableAdministratifAssociation(edc()), equipeRecherche());
		affectationNouveauResponsable.setRasDOuverture(dateDebutResponsabilite());
		affectationNouveauResponsable.setRasDFermeture(dateFinResponsabilite());
		affectationNouveauResponsable.setPersIdModification(getUtilisateurPersId());
		affectationNouveauResponsable.addToToIndividusAssociesRelationship(responsableSelectionne().localInstanceIn(edc()));
		return doNothing();
	}
	
	
	public String jsOnSuccesSelection() {
		return "function() {" + assistantResponsableContainerId() + "Update();}";

	}
    
	public String jsOnEchecSelection() {
		return "function(){show_" + validationAjoutNotificationEchecId() + "();}";
	}
	
	public String jsOnSuccesValidationAjout() {
		return "function() {" + assistantResponsableContainerId() + "Update();" + "Windows.close('"+ responsableSearchWindowId() +"_win', event);}";
	}
	
	public String jsOnEchecValidationAjout() {
		return "function(){show_" + validationAjoutNotificationEchecId() + "();}";
	}
	
	public NSArray<EORepartAssociation> affectationsResponsables() {
		
		EOQualifier qualifier = ERXQ.and(
											ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, getFactoryAssociation().responsableAdministratifAssociation(edc())),
											ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY, equipeRecherche())
										);
		
		NSArray<EOSortOrdering> orderings = new NSMutableArray<EOSortOrdering>();
		EOSortOrdering sortOrdering = new EOSortOrdering(EORepartAssociation.RAS_D_OUVERTURE_KEY, EOSortOrdering.CompareDescending); 
		orderings.add(sortOrdering);
		ERXFetchSpecification<EORepartAssociation> fetchSpecification = new ERXFetchSpecification<EORepartAssociation>(EORepartAssociation.ENTITY_NAME, qualifier, orderings);
		fetchSpecification.setIncludeEditingContextChanges(true);
		return fetchSpecification.fetchObjects(edc());

	}

	public void setCurrentAffectation(EORepartAssociation currentAffectation) {
		this.currentAffectation = currentAffectation;
	}

	public EORepartAssociation currentAffectation() {
		return currentAffectation;
	}

	public EOIndividu currentResponsable() {
		return (EOIndividu) currentAffectation().toIndividusAssocies().get(0);
	}
	
	public Boolean isPremierResponsable() {
		return affectationResponsableIndex == 0;
	}
	public Integer getAffectationResponsableIndex() {
		return affectationResponsableIndex;
	}

	public void setAffectationResponsableIndex(Integer affectationResponsableIndex) {
		this.affectationResponsableIndex = affectationResponsableIndex;
	}
	
	
	public WOActionResults supprimerAffectationResponsable() {
		currentAffectation().setToAssociationRelationship(null);
		currentAffectation().setToPersonne(null);
		currentAffectation().setToStructureRelationship(null);
		edc().deleteObject(currentAffectation());
		return doNothing();
	}


	
	/// Gestionnaire admin et fin
	
	public void setGestionnaireAdminEtFinEquipe(
			EOIndividu gestionnaireAdminEtFinEquipe) {
		this.gestionnaireAdminEtFinEquipe = gestionnaireAdminEtFinEquipe;
	}

	public EOIndividu gestionnaireAdminEtFinEquipe() {
		if(gestionnaireAdminEtFinEquipe == null) {
			EORepartAssociation ancienGestionnaireAffectation = null;

			EOQualifier qualifierAncienGestionnaireAffectation = ERXQ.and(
																		ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, getFactoryAssociation().gestionnaireAdminEtFinAssociation(edc())),
																		ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY, equipeRecherche())
																	);
			ERXFetchSpecification<EORepartAssociation> fetchSpecification = new ERXFetchSpecification<EORepartAssociation>(EORepartAssociation.ENTITY_NAME, qualifierAncienGestionnaireAffectation, null);
			fetchSpecification.setIncludeEditingContextChanges(true);
			
			NSArray<EORepartAssociation> possiblesAffectations = fetchSpecification.fetchObjects(edc());

			if(!possiblesAffectations.isEmpty()) {
				ancienGestionnaireAffectation =  (EORepartAssociation) possiblesAffectations.get(0);
				gestionnaireAdminEtFinEquipe = (EOIndividu) ancienGestionnaireAffectation.toIndividusAssocies().get(0);
			}
			
		}
		return gestionnaireAdminEtFinEquipe;
	}
	
	public String gestionnaireAdminEtFinContainerId() {
		return getComponentId() + "_gestionnaireAdminEtFinContainer";
	}
	
	public WOActionResults onSelectGestionnaireAdminFin() {
		EORepartAssociation ancienGestionnaireAffectation = null;
		
//		try {
			

			EOQualifier qualifierAncienGestionnaireAffectation = ERXQ.and(
																		ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, getFactoryAssociation().gestionnaireAdminEtFinAssociation(edc())),
																		ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY, equipeRecherche())
																	);
			ERXFetchSpecification<EORepartAssociation> fetchSpecification = new ERXFetchSpecification<EORepartAssociation>(EORepartAssociation.ENTITY_NAME, qualifierAncienGestionnaireAffectation, null);
			fetchSpecification.setIncludeEditingContextChanges(true);
			
			NSArray<EORepartAssociation> possiblesAffectations = fetchSpecification.fetchObjects(edc());

			if(!possiblesAffectations.isEmpty()) {
				ancienGestionnaireAffectation =  (EORepartAssociation) possiblesAffectations.get(0);
			}
			
//			gestionnaireAdminEtFinEquipe().setValidationEditingContext(personnesEditingContext());
//			gestionnaireAdminEtFinEquipe().setPersIdModification(utilisateurPersId());
//			personnesEditingContext().saveChanges();
			
			if(!(ancienGestionnaireAffectation == null)) {
				ancienGestionnaireAffectation.setToAssociationRelationship(null);
				ancienGestionnaireAffectation.setToPersonne(null);
				ancienGestionnaireAffectation.setToStructureRelationship(null);
				edc().deleteObject(ancienGestionnaireAffectation);
			}
			
			EOStructureForGroupeSpec.affecterPersonneDansGroupe(edc(), gestionnaireAdminEtFinEquipe(), equipeRecherche, getUtilisateurPersId(), getFactoryAssociation().gestionnaireAdminEtFinAssociation(edc()));
//		} catch (ValidationException e) {
//			personnesEditingContext().revert();
//			setGestionnaireAdminEtFinEquipe((EOIndividu) ancienGestionnaireAffectation.toPersonneElt());
//			session().addSimpleErrorMessage("Erreur", e);
//		}
		return doNothing();
	}
	

}