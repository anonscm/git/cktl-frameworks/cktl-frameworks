package org.cocktail.fwkcktlsangriaguiajax.serveur.components.equiperecherche;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.util.PersonnelRechercheUtilities;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.validation.ERXValidationException;

public class EquipeRechechePersonnelsUI extends AFwkCktlSangriaComponent {
	
	public static final String BINDING_equipeRecherche = "equipeRecherche"; 
	
	
	public static final String REPART_ASSOCIATION_COURANT_KEY = "repartAssociationCourant.";
	
	public static final String MEMBRE_KEY = "membreCourant";
	public static final String GRADE_KEY = "gradeCourant";
	public static final String D_DEBUT_KEY = EORepartAssociation.RAS_D_OUVERTURE_KEY;
	public static final String D_FIN_KEY = EORepartAssociation.RAS_D_FERMETURE_KEY;
	
	
	

	protected Boolean typeAjoutRechercheSelected = true;
	protected Boolean typeAjoutListeSelected = false;
	
	public NSMutableArray<String> _colonnesKeys = null;
	public NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = null;

	private EORepartAssociation repartAssociationCourant = null;
	private EORepartAssociation selectedRepartAssociation = null;
	private ERXDisplayGroup<EORepartAssociation> membresDisplayGroup = null;
	
	
	private Integer etapeCourante = 1;
	
	private EORepartAssociation editingRepartMembre;

	
	private EOIndividu nouveauMembre;
	private NSTimestamp nouvelleDateDebut;
	private NSTimestamp nouvelleDateFin;
	
	private EOEditingContext membreEditingContext = null;
	
	private Boolean showDetails = false;
	
	private ERXDisplayGroup<EOIndividu> membresPossiblesDisplayGroup;
	private EOIndividu currentMembrePossible;


  private Boolean _wantRefreshNouveauMembreSearch = false;
	
	
	
    public EquipeRechechePersonnelsUI(WOContext context) {
        super(context);
    }
    

	
    
    public String membresTableViewId() {
    	return getComponentId() + "_membresTableView";
    }
    
    public String ajouterMembreWindowId() {
    	return getComponentId() + "_ajouterMembreWindow";
    }
    
    public String ajouterMembreWindowContainerId() {
      return getComponentId() + "_ajouterMembreWindowContainer";
    }
    
    public String boutonsEtapeUneContainerId() {
    	return getComponentId() + "_boutonsEtapeUneContainer";
    }
    
    public String etapesContainerId() {
    	return getComponentId() + "_etapesContainer";
    }
    
    public String membresTableViewContainerId() {
    	return getComponentId() + "_membresTableViewContainer";
    }
    
    public String boutonsEtapeDeuxContainerId() {
    	return getComponentId() + "_boutonsEtapeDeuxContainer";
    }
    
    public String boutonsContainerId() {
    	return getComponentId() + "_boutonsContainer";
    }
    
    public String modifierMembreWindowId() {
    	return getComponentId() + "_modifierMembreWindow";
    }
    
    public EOStructure equipeRecherche() {
    	return (EOStructure) valueForBinding(BINDING_equipeRecherche);
    }
    
    public String membresPossiblesTableViewId() {
      return getComponentId() + "_membresPossiblesTableView";
    }
    
    

	private NSMutableArray<String> _colonnesKeys() {
		if(_colonnesKeys == null) {
			_colonnesKeys = new NSMutableArray<String>();
			_colonnesKeys().add(MEMBRE_KEY);
			_colonnesKeys().add(D_DEBUT_KEY);
			_colonnesKeys().add(D_FIN_KEY);
		}
		return _colonnesKeys;
	}
	
	private NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap() {
		if(_colonnesMap == null) {
			
			_colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();
			
			CktlAjaxTableViewColumn membre_colonne = new CktlAjaxTableViewColumn();
			membre_colonne.setLibelle("Membre");
			CktlAjaxTableViewColumnAssociation membre_colonne_ass = new CktlAjaxTableViewColumnAssociation(MEMBRE_KEY + "." + EOIndividu.NOM_PRENOM_AFFICHAGE_KEY, "");
			membre_colonne.setAssociations(membre_colonne_ass);
			_colonnesMap.takeValueForKey(membre_colonne, MEMBRE_KEY);
			
			CktlAjaxTableViewColumn d_debut_colonne = new CktlAjaxTableViewColumn();
			d_debut_colonne.setLibelle("Du");
			CktlAjaxTableViewColumnAssociation d_debut_colonne_ass = new CktlAjaxTableViewColumnAssociation(REPART_ASSOCIATION_COURANT_KEY + D_DEBUT_KEY, "");
			d_debut_colonne_ass.setDateformat("%d/%m/%y");
			d_debut_colonne.setAssociations(d_debut_colonne_ass);
			_colonnesMap.takeValueForKey(d_debut_colonne, D_DEBUT_KEY);
			
			CktlAjaxTableViewColumn d_fin_colonne = new CktlAjaxTableViewColumn();
			d_fin_colonne.setLibelle("Au");
			CktlAjaxTableViewColumnAssociation d_fin_colonne_ass = new CktlAjaxTableViewColumnAssociation(REPART_ASSOCIATION_COURANT_KEY + D_FIN_KEY, "");
			d_fin_colonne_ass.setDateformat("%d/%m/%y");
			d_fin_colonne.setAssociations(d_fin_colonne_ass);
			_colonnesMap.takeValueForKey(d_fin_colonne, D_FIN_KEY);
			
		}
		return _colonnesMap;
	
	}
    
    
    public NSArray<CktlAjaxTableViewColumn> colonnes() {
    	NSMutableArray<CktlAjaxTableViewColumn> colonnes = new NSMutableArray<CktlAjaxTableViewColumn>();
    	for(String key : _colonnesKeys()) {
    		colonnes.add((CktlAjaxTableViewColumn) _colonnesMap().valueForKey(key));
    	}
    	return colonnes;
    }

	public void setRepartAssociationCourant(EORepartAssociation repartAssociationCourant) {
		this.repartAssociationCourant = repartAssociationCourant;
	}

	public EORepartAssociation repartAssociationCourant() {
		return repartAssociationCourant;
	}
	
	public EOIndividu membreCourant() {
		return (EOIndividu) repartAssociationCourant().toIndividusAssocies().get(0);
	}
	
	public EOGrade gradeCourant() {
		return PersonnelRechercheUtilities.gradePourPersonnel(edc(), membreCourant());
	}
	
	public EOQualifier responsablesQualifier() {
		
		EOQualifier qualifier = ERXQ.and(
				ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY, equipeRecherche()),
				ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, getFactoryAssociation().membreAssociation(edc()))
		);
		
		if(!showDetails) {
			EOQualifier dateQualifier = ERXQ.and( 
				ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_OUVERTURE_KEY, MyDateCtrl.getDateJour()),
				ERXQ.or(
						ERXQ.greaterThan(EORepartAssociation.RAS_D_FERMETURE_KEY, MyDateCtrl.getDateJour()),
						ERXQ.isNull(EORepartAssociation.RAS_D_FERMETURE_KEY)
				)
			);
			qualifier = ERXQ.and(qualifier, dateQualifier);
		}
		
		return qualifier;
		
	}
	
	public NSArray<EORepartAssociation> repartsMembres() {
		NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EORepartAssociation.RAS_D_OUVERTURE_KEY, EOSortOrdering.CompareDescending));
		ERXFetchSpecification<EORepartAssociation> fetchSpecification = new ERXFetchSpecification<EORepartAssociation>(EORepartAssociation.ENTITY_NAME, responsablesQualifier(), sortOrderings);
		fetchSpecification.setIncludeEditingContextChanges(true);
		return fetchSpecification.fetchObjects(edc());
	}
	
	public void refreshMembresDisplayGroup() {
		responsablesDisplayGroup().setObjectArray(repartsMembres());
	}
	
	public WODisplayGroup responsablesDisplayGroup() {
		if(membresDisplayGroup == null) {
			membresDisplayGroup = new ERXDisplayGroup<EORepartAssociation>();
			membresDisplayGroup.setDelegate(new ResponsablesDisplayGroupDelegate());
			membresDisplayGroup.setObjectArray(repartsMembres());
			//membresDisplayGroup.setNumberOfObjectsPerBatch(-1);
			
			membresDisplayGroup.setSelectsFirstObjectAfterFetch(true);
			membresDisplayGroup.fetch();
		}
		return membresDisplayGroup;
	}
    
    public void setSelectedRepartAssociation(EORepartAssociation selectedRepartAssociation) {
		this.selectedRepartAssociation = selectedRepartAssociation;
	}

	public EORepartAssociation selectedRepartAssociation() {
		return selectedRepartAssociation;
	}

	public class ResponsablesDisplayGroupDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedRepartAssociation((EORepartAssociation) group.selectedObject());
		}
    }
	
    public Boolean etapeUne() {
    	return etapeCourante == 1;
    }
    
    public Boolean etapeDeux() {
    	return etapeCourante == 2;
    }
    
    public EOEditingContext membreEditingContext() {
    	if(membreEditingContext == null) {
    		membreEditingContext = ERXEC.newEditingContext(edc());
    	}
    	return membreEditingContext;
    }
    
    public WOActionResults ajouterMembre() {
    	setNouvelleDateDebut(null);
    	setNouvelleDateFin(null);
    	return doNothing();
    }

	public void setNouveauMembre(EOIndividu nouveauMembre) {
		this.nouveauMembre = nouveauMembre;
	}

	public EOIndividu nouveauMembre() {
		return this.nouveauMembre;
	}
	
	public void setNouvelleDateDebut(NSTimestamp nouvelleDateDebut) {
		this.nouvelleDateDebut = nouvelleDateDebut;
	}
	
	public NSTimestamp nouvelleDateDebut() {
		return this.nouvelleDateDebut;
	}

	public void setNouvelleDateFin(NSTimestamp nouvelleDateFin) {
		this.nouvelleDateFin = nouvelleDateFin;
	}
	
	public NSTimestamp nouvelleDateFin() {
		return this.nouvelleDateFin;
	}
	
	public Boolean noNouveauMembreSelected() {
	  if(getTypeAjoutRechercheSelected()) {
	    return nouveauMembre() == null;
	  }
	  return false;
	}
	
	public WOActionResults passageEtapeDeux() {
	  if(getTypeAjoutRechercheSelected()) {
	    if(nouveauMembre() == null) {
	      session().addSimpleErrorMessage("Erreur", "Vous devez sélectionner un membre");
	      return doNothing();
	    }
	  }
	  if(getTypeAjoutListeSelected()) {
	    if(getMembresPossiblesDisplayGroup().selectedObjects().isEmpty()) {
	      session().addSimpleErrorMessage("Erreur", "Vous devez sélétionner au moins un membre");
	      return doNothing();
	    }
	  }
		etapeCourante = 2;
		return doNothing();
	}
	
	public WOActionResults retourEtapeUne() {
		etapeCourante = 1;
		return doNothing();
	}
	
	public WOActionResults annulerAjoutMembre() {
		membreEditingContext().revert();
		setNouveauMembre(null);
		setMembresPossiblesDisplayGroup(null);
		setWantRefreshNouveauMembreSearch(true);
		CktlAjaxWindow.close(context(), ajouterMembreWindowId());
		return doNothing();
	}
	
	public WOActionResults validerAjoutMembre() {
		if(nouvelleDateDebut() == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez sélectionner une date de début");
			return doNothing();
		}
		if(getTypeAjoutRechercheSelected()) {
		  PersonnelRechercheUtilities.affecterPersonneDansEquipe(membreEditingContext(), nouveauMembre(), equipeRecherche().localInstanceIn(membreEditingContext()), nouvelleDateDebut(), nouvelleDateFin(), getUtilisateurPersId());
		}
		if(getTypeAjoutListeSelected()) {
		  for(EOIndividu nouveauMembre : getMembresPossiblesDisplayGroup().selectedObjects()) {
		    PersonnelRechercheUtilities.affecterPersonneDansEquipe(membreEditingContext(), nouveauMembre, equipeRecherche().localInstanceIn(membreEditingContext()), nouvelleDateDebut(), nouvelleDateFin(), getUtilisateurPersId());
		  }
		}
		try {
			membreEditingContext().saveChanges();
			setMembresPossiblesDisplayGroup(null);
			setNouveauMembre(null);
			setWantRefreshNouveauMembreSearch(true);
		}
		catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		refreshMembresDisplayGroup();
		etapeCourante = 1;
		CktlAjaxWindow.close(context(), ajouterMembreWindowId());

		return doNothing();
	}
	
	public EORepartAssociation editingRepartMembre() {
		return editingRepartMembre;
	}

	public void setEditingRepartMembre(
			EORepartAssociation editingRepartResponsable) {
		this.editingRepartMembre = editingRepartResponsable;
	}
	
	public WOActionResults modifierMembre() {
		setEditingRepartMembre(selectedRepartAssociation().localInstanceIn(membreEditingContext()));
		return doNothing();
	}
	
	public WOActionResults annulerModificationMembre() {
		membreEditingContext().revert();
		CktlAjaxWindow.close(context(), modifierMembreWindowId());
		return doNothing();
	}
	
	public WOActionResults validerModificationMembre() {
		if(editingRepartMembre().rasDOuverture() == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez sélectionner une date de début");
			return doNothing();
		}
		try {
			membreEditingContext().saveChanges();
		}
		catch (ERXValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		refreshMembresDisplayGroup();
		CktlAjaxWindow.close(context(), modifierMembreWindowId());
		return doNothing();
	}
	
	public Boolean isModifierDisabled() {
		return selectedRepartAssociation() == null;
	}
	
	public WOActionResults supprimerMembre() {
		selectedRepartAssociation().setToAssociationRelationship(null);
		selectedRepartAssociation().setToPersonne(null);
		selectedRepartAssociation().setToStructureRelationship(null);
		edc().deleteObject(selectedRepartAssociation());
		refreshMembresDisplayGroup();
		return doNothing();
	}
	
	public WOActionResults switchShowDetails() {
		showDetails = !showDetails;
		refreshMembresDisplayGroup();
		AjaxUpdateContainer.updateContainerWithID(membresTableViewContainerId(), context());
		AjaxUpdateContainer.updateContainerWithID(boutonsContainerId(), context());
		return doNothing();
	}
	
	public Boolean showDetails() {
		return showDetails;
	}






  public Boolean getTypeAjoutRechercheSelected() {
    return typeAjoutRechercheSelected;
  }




  public void setTypeAjoutRechercheSelected(Boolean typeAjoutRechercheSelected) {
    this.typeAjoutRechercheSelected = typeAjoutRechercheSelected;
  }




  public Boolean getTypeAjoutListeSelected() {
    return typeAjoutListeSelected;
  }




  public void setTypeAjoutListeSelected(Boolean typeAjoutListeSelected) {
    this.typeAjoutListeSelected = typeAjoutListeSelected;
  }




  public ERXDisplayGroup<EOIndividu> getMembresPossiblesDisplayGroup() {
    if(membresPossiblesDisplayGroup == null) {
      membresPossiblesDisplayGroup = new ERXDisplayGroup<EOIndividu>();
      membresPossiblesDisplayGroup.setObjectArray(getMembresPossibles());
    }
    return membresPossiblesDisplayGroup;
  }




  private NSArray<EOIndividu> getMembresPossibles() {
    
    EOStructure unite = StructuresRechercheHelper.unitePourEquipe(edc(), equipeRecherche());
    NSArray<EOAssociation> typesMembresUnites = getFactoryAssociation().associationsFilleDe(getFactoryAssociation().typesMembresUniteRechercheAssociation(edc()), edc());
    NSArray<EOIndividu> membresUnite = ERXArrayUtilities.arrayByAddingObjectsFromArrayWithoutDuplicates(
        (NSArray<EOIndividu>) unite.toRepartAssociationsElts(EORepartAssociation.TO_ASSOCIATION.in(typesMembresUnites)).valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY),
        ERXArrayUtilities.flatten((NSArray) unite.toRepartStructuresElts().valueForKey(EORepartStructure.TO_INDIVIDU_ELTS_KEY))   
    );
    NSArray<EOIndividu> membresEquipe = (NSArray<EOIndividu>) equipeRecherche().toRepartAssociationsElts(EORepartAssociation.TO_ASSOCIATION.eq(getFactoryAssociation().membreAssociation(edc()))).valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
    return ERXEOControlUtilities.localInstancesOfObjects(membreEditingContext(), EOSortOrdering.sortedArrayUsingKeyOrderArray(ERXArrayUtilities.arrayMinusArray(membresUnite, membresEquipe), EOIndividu.NOM_AFFICHAGE.ascs()));
    
  }




  public void setMembresPossiblesDisplayGroup(ERXDisplayGroup<EOIndividu> membresPossiblesDisplayGroup) {
    this.membresPossiblesDisplayGroup = membresPossiblesDisplayGroup;
  }




  public EOIndividu getCurrentMembrePossible() {
    return currentMembrePossible;
  }




  public void setCurrentMembrePossible(EOIndividu currentMembrePossible) {
    this.currentMembrePossible = currentMembrePossible;
  }




  /**
   * @return the wantRefreshNouveauMembreSearch
   */
  public Boolean isWantRefreshNouveauMembreSearch() {
    return _wantRefreshNouveauMembreSearch;
  }




  /**
   * @param wantRefreshNouveauMembreSearch the wantRefreshNouveauMembreSearch to set
   */
  public void setWantRefreshNouveauMembreSearch(Boolean wantRefreshNouveauMembreSearch) {
    _wantRefreshNouveauMembreSearch = wantRefreshNouveauMembreSearch;
  }
	
	
	
}