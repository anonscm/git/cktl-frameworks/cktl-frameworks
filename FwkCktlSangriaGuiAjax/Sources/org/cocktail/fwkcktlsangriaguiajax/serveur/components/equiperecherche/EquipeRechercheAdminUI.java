package org.cocktail.fwkcktlsangriaguiajax.serveur.components.equiperecherche;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;

public class EquipeRechercheAdminUI extends AFwkCktlSangriaComponent {
	

	public static final String BINDING_equipeRecherche = "equipeRecherche";
	private EOStructure equipeRecherche;

	
    public EquipeRechercheAdminUI(WOContext context) {
        super(context);
    }

	
	
	public EOStructure equipeRecherche() {
		return (EOStructure) valueForBinding(BINDING_equipeRecherche);
	}
	
	

}
