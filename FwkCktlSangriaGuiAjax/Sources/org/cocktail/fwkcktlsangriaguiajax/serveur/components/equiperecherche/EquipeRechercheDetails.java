package org.cocktail.fwkcktlsangriaguiajax.serveur.components.equiperecherche;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;

/**
 * Details de l'equipe de recherche
 */
public class EquipeRechercheDetails extends AFwkCktlSangriaComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_EQUIPE_RECHERCHE = "equipeRecherche";
	
	private EOStructure equipeDeRecherche;
	private Boolean forceRefreshResponsables = false;
	private Boolean forceRefreshResponsablesAdministratifs = false;
	private Boolean forceRefreshMembres = false;

	/**
	 * Constructeur
	 * @param context contexte
	 */
	public EquipeRechercheDetails(WOContext context) {
		super(context);
	}

	public void setEquipeDeRecherche(EOStructure equipeDeRecherche) {
		this.equipeDeRecherche = equipeDeRecherche;
	}

	/**
	 * @return la structure de recherche dont on veut avoir les informations
	 */
	public EOStructure getEquipeDeRecherche() {
		EOStructure _equipeRecherche = (EOStructure) valueForBinding(BINDING_EQUIPE_RECHERCHE);
		if (!_equipeRecherche.equals(equipeDeRecherche)) {
			equipeDeRecherche = _equipeRecherche;
			setForceRefreshResponsables(true);
			setForceRefreshResponsablesAdministratifs(true);
			setForceRefreshMembres(true);
		}
		return equipeDeRecherche;
	}

	public EOAssociation getResponsableAdministratifAssociation() {
		return getFactoryAssociation().responsableAdministratifAssociation(edc());
	}

	public EOAssociation getResponsableAssociation() {
		return getFactoryAssociation().responsableAssociation(edc());
	}

	public EOAssociation getMembresAssociation() {
		return getFactoryAssociation().membreAssociation(edc());
	}

	public Boolean getForceRefreshResponsables() {
		return forceRefreshResponsables;
	}

	public void setForceRefreshResponsables(Boolean forceRefresh) {
		this.forceRefreshResponsables = forceRefresh;
	}

	public Boolean getForceRefreshResponsablesAdministratifs() {
		return forceRefreshResponsablesAdministratifs;
	}

	public void setForceRefreshResponsablesAdministratifs(Boolean forceRefreshResponsablesAdministratifs) {
		this.forceRefreshResponsablesAdministratifs = forceRefreshResponsablesAdministratifs;
	}

	public Boolean getForceRefreshMembres() {
		return forceRefreshMembres;
	}

	public void setForceRefreshMembres(Boolean forceRefreshMembres) {
		this.forceRefreshMembres = forceRefreshMembres;
	}

}