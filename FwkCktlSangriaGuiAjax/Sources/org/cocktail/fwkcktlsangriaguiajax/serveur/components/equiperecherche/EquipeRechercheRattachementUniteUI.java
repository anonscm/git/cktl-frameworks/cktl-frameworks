package org.cocktail.fwkcktlsangriaguiajax.serveur.components.equiperecherche;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

public class EquipeRechercheRattachementUniteUI extends AFwkCktlSangriaComponent {
	
	public static final String BINDING_equipeRecherche = "equipeRecherche";
	private EOStructure equipeRecherche;

	private EOStructure currentUniteRecherche;
	private EOStructure uniteRecherche;

	
    public EquipeRechercheRattachementUniteUI(WOContext context) {
        super(context);
    }

    
	public EOStructure equipeRecherche() {
		if(equipeRecherche == null) {
			if(hasBinding(BINDING_equipeRecherche)) {
				equipeRecherche = (EOStructure) valueForBinding(BINDING_equipeRecherche);
			}
		}
		
		return equipeRecherche;
	}
	
    /*******************************************/
	/*** Gestion de l'unité de recherche ***/
	
    public NSArray<EOStructure> listeDesUnitesRecherche() {
    	EOQualifier qualifierForUnites = ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_LA);
    	EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
    	return EOStructure.fetchAll(edc(), qualifierForUnites, new NSArray<EOSortOrdering>(order));
    }
	
	public void setUniteRecherche(EOStructure uniteRecherche) {
		this.uniteRecherche = uniteRecherche;
	}
	
	
	public EOStructure uniteRecherche() {
		// On va chercher l'unité uniquement si l'équipe n'est pas nouvelle 
		if(!equipeRecherche().hasTemporaryGlobalID()) {
			if(this.uniteRecherche == null) {
				EOAssociation equipeDeRechercheAssociation = getFactoryAssociation().equipeDeRechercheAssociation(edc());
				
				EOQualifier qualifier = ERXQ.and(
						ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, equipeDeRechercheAssociation),
						ERXQ.equals(EORepartAssociation.PERS_ID_KEY, equipeRecherche().persId())
						);
				EORepartAssociation repartAssociation = EORepartAssociation.fetchFirstByQualifier(edc(), qualifier, null);
				if(repartAssociation != null) {
					this.uniteRecherche = repartAssociation.toStructure();
				}
				
			}
		}
		
		return this.uniteRecherche;
	}
	
	public String uniteRechercheContainerId() {
		return getComponentId() + "_uniteRechercheContainer";
	}
	
	public String uniteRechercheSearchWindowId() {
		return getComponentId() + "_uniteRechercheSearchWindow";
	}
	
	public String jsOpenUniteRechercheSearch() {
		return "openCAW_" +  uniteRechercheSearchWindowId() + "();return false;";
	}
	
	public WOActionResults onSuppressionUniteRecherche() {
		EOAssociation equipeDeRechercheAssociation = getFactoryAssociation().equipeDeRechercheAssociation(edc());
		
		EOQualifier qualifier = ERXQ.and(
				ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, equipeDeRechercheAssociation),
				ERXQ.equals(EORepartAssociation.PERS_ID_KEY, equipeRecherche().persId()),
				ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY, uniteRecherche())
				);
		
		ERXFetchSpecification<EORepartAssociation> fetchSpecification = new ERXFetchSpecification<EORepartAssociation>(EORepartAssociation.ENTITY_NAME, qualifier, null);
		fetchSpecification.setIncludeEditingContextChanges(true);
		EORepartAssociation repartAssociation = fetchSpecification.fetchObjects(edc()).get(0);
		
		repartAssociation.setToStructureRelationship(null);
		repartAssociation.setToPersonne(null);
		repartAssociation.setToAssociationRelationship(null);
		edc().deleteObject(repartAssociation);

		setUniteRecherche(null);
		
		return doNothing();
	}
    
	public String jsOnSelect() {
		return "function() {" + uniteRechercheContainerId() + "Update(); Windows.close('"+ uniteRechercheSearchWindowId() +"_win', event);}";
	}
	
	public String jsOnFailureSelectUniteRecherche() {
		return "function(){alert('Des informations relatives à cette unité de recherche sélectionnée sont invalides. Vous ne pouvez pas utiliser cette structure pour le moment.');}";
	}

	public void setCurrentUniteRecherche(EOStructure currentUniteRecherche) {
		this.currentUniteRecherche = currentUniteRecherche;
	}

	public EOStructure getCurrentUniteRecherche() {
		return currentUniteRecherche;
	}
	
	public WOActionResults selectionnerUniteRecherche() {
//		try {
//			getCurrentUniteRecherche().setValidationEditingContext(personnesEditingContext());
//			getCurrentUniteRecherche().setPersIdModification(utilisateurPersId());
//			personnesEditingContext().saveChanges();
			if(uniteRecherche() != null) {
				onSuppressionUniteRecherche();
			}
			EOAssociation equipeDeRechercheAssociation = getFactoryAssociation().equipeDeRechercheAssociation(edc());
			setUniteRecherche(getCurrentUniteRecherche());
			EOStructureForGroupeSpec.affecterPersonneDansGroupe(edc(), equipeRecherche(), getCurrentUniteRecherche(), getUtilisateurPersId(), equipeDeRechercheAssociation);
//		} catch (ValidationException e) {
//			personnesEditingContext().revert();
//		}

		return doNothing();
	}
}