package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchepartiefinui;

import com.webobjects.appserver.WOContext;

import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

public class ContratRecherchePartieFinUI extends AFwkCktlSangriaComponent {
	
	private final static String BINDING_consultation = "consultation";
	private final static String BINDING_modification = "modification";
	
    public ContratRecherchePartieFinUI(WOContext context) {
        super(context);
    }
    
    public Boolean consultation() {
    	if(hasBinding(BINDING_consultation)) {
    		return (Boolean) valueForBinding(BINDING_consultation);
    	}
    	else {
    		return false;
    	}
    }
    
    public Boolean modification() {
    	if(consultation()) {
    		return false;
    	}
    	else if(hasBinding(BINDING_modification)) {
    		return (Boolean) valueForBinding(BINDING_modification);
    	}
    	else {
    		return false;
    	}
    	
    }
    
    	
}