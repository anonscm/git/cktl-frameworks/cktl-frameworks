package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats;

import java.util.Calendar;
import java.util.Date;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.AvenantEvenement;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlevenement.common.util.DateUtilities;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementType;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class ContratNotesUI extends AFwkCktlSangriaComponent {
	
	private static final String BINDING_contrat = "contrat";
	private static final String BINDING_readOnly = "readOnly";
	
	private static final String NOTE_EVT_ID_INTERNE = "NOTE";
	
	private NSArray<EOEvenement> notes;
	private EOArrayDataSource notesDataSource;
	private ERXDisplayGroup<EOEvenement> notesDisplayGroup;
	
	private EOEvenement selectedNote;
	private EOEvenement currentNote;
	
	private EOEvenementType evenementTypeNote;
	
	private EOEditingContext editingContextPourEdition;
	
	private String editedNoteObjet;
	
    public ContratNotesUI(WOContext context) {
        super(context);
    }
    
    public void actualisertable() {
    	System.out.println("ACTUALISATION");
    	AjaxUpdateContainer.updateContainerWithID(notesTableViewContainerId(), context());
    }
    
    public EOEditingContext editingContextPourEdition() {
    	if(editingContextPourEdition == null) {
    		editingContextPourEdition = ERXEC.newEditingContext(edc());
    	}
    	return editingContextPourEdition;
    }
    
    public Contrat contrat() {
    	return (Contrat) valueForBinding(BINDING_contrat);
    }
    
    public Boolean readOnly() {
    	return valueForBooleanBinding(BINDING_readOnly, false);
    }
    
    public EOEvenementType evenementTypeNote() {
    	if(evenementTypeNote == null) {
    		EOQualifier qualifier = ERXQ.equals(EOEvenementType.ID_INTERNE_KEY, NOTE_EVT_ID_INTERNE);
    		evenementTypeNote = EOEvenementType.fetchFirstByQualifier(edc(), qualifier);
    	}
		return evenementTypeNote;
	}

	public String notesTableViewId() {
    	return getComponentId() + "_notesTableView";
    }
    
    public String boutonsContainerId() {
    	return getComponentId() + "_boutonsContainer";
    }
    
    public String notesTableViewContainerId() {
    	return getComponentId() + "_notesTableViewContainer";
    }

    public String nouvelleNoteWindowId() {
		return getComponentId() + "_nouvelleNoteWindow";
	}
    
    public String editedNoteWindowId() {
		return getComponentId() + "_editedNoteWindow";
	}
    
    
	public EOEvenement selectedNote() {
		return selectedNote;
	}

	public void setSelectedNote(EOEvenement selectedNote) {
		this.selectedNote = selectedNote;
	}

	public NSArray<EOEvenement> notes() {
		if(notes == null) {
			String evtTypeIdKeyPath = ERXQ.keyPath(AvenantEvenement.EVENEMENT_KEY, EOEvenement.TYPE_KEY, EOEvenementType.ID_INTERNE_KEY);
			EOQualifier qualifier = ERXQ.equals(evtTypeIdKeyPath, NOTE_EVT_ID_INTERNE);
			
			notes = (NSArray<EOEvenement>) contrat().avenantZero().avenantEvenements(qualifier).valueForKey(AvenantEvenement.EVENEMENT_KEY);
		}
		return notes;
	}

	public EOArrayDataSource notesDataSource() {
		if(notesDataSource == null) {
			notesDataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForEntityName(EOEvenement.ENTITY_NAME), null);
			notesDataSource.setArray(notes());
		}
		return notesDataSource;
	}

	public ERXDisplayGroup<EOEvenement> notesDisplayGroup() {
		if(notesDisplayGroup == null) {
			notesDisplayGroup = new ERXDisplayGroup<EOEvenement>();
			notesDisplayGroup.setDataSource(notesDataSource());
			notesDisplayGroup.setDelegate(this);
			notesDisplayGroup.setSortOrderings(ERXS.descs(EOEvenement.DATE_CREATION_KEY));
			notesDisplayGroup.setSelectsFirstObjectAfterFetch(true);
			notesDisplayGroup.fetch();
		}
		return notesDisplayGroup;
	}
	
	public void displayGroupDidChangeSelection(WODisplayGroup group) {
		ERXDisplayGroup<EOEvenement> notesDisplayGroup = (ERXDisplayGroup<EOEvenement>) group;
		if(notesDisplayGroup.selectedObject() != null) {
			setSelectedNote(notesDisplayGroup().selectedObject());
		}
	}

	public EOEvenement currentNote() {
		return currentNote;
	}

	public void setCurrentNote(EOEvenement currentNote) {
		this.currentNote = currentNote;
	}
	
	public WOActionResults ajouterNote() {
		setEditedNoteObjet("");
		return doNothing();
	}
	
	public WOActionResults validerAjouterNote() {
		EOPersonne utilisateur = EOPersonne.fetchFirstByQualifier(editingContextPourEdition(), EOPersonne.PERS_ID.eq(getUtilisateurPersId()));
		EOEvenement nouvelleNote = FwkCktlEvenementUtil.creerNouvelEvenement(utilisateur, editingContextPourEdition());
		nouvelleNote.setTypeRelationship(evenementTypeNote().localInstanceIn(editingContextPourEdition()));
		nouvelleNote.setObjet(editedNoteObjet());
		Avenant avenantZero = contrat().avenantZero().localInstanceIn(editingContextPourEdition());
		
		AvenantEvenement avenantEvenement = AvenantEvenement.create(editingContextPourEdition(), avenantZero, nouvelleNote);
		avenantZero.addToAvenantEvenementsRelationship(avenantEvenement);
        avenantZero.setShouldNotValidate(true);

		try {
			editingContextPourEdition().saveChanges();
			notesDataSource().insertObject(nouvelleNote);
			notesDisplayGroup().fetch();
			AjaxUpdateContainer.updateContainerWithID(notesTableViewContainerId(), context());
			CktlAjaxWindow.close(context(), nouvelleNoteWindowId());
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			editingContextPourEdition().revert();
		}
		return doNothing();
	}
	
	public WOActionResults annulerAjouterNote() {
		editingContextPourEdition().revert();
		CktlAjaxWindow.close(context(), nouvelleNoteWindowId());
		return doNothing();
	}
	
	public WOActionResults editerNote() {
		setEditedNoteObjet(selectedNote().objet());
		return doNothing();
	}
	
	public WOActionResults validerEditerNote() {
		selectedNote().setObjet(editedNoteObjet());
		Date datePrevue = new Date();
		datePrevue = DateUtilities.decaler(datePrevue, 2, Calendar.HOUR_OF_DAY);
		selectedNote().setDatePrevue(new NSTimestamp(datePrevue));
		try {
			editingContextPourEdition().saveChanges();
			AjaxUpdateContainer.updateContainerWithID(notesTableViewContainerId(), context());
			CktlAjaxWindow.close(context(), editedNoteWindowId());
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			editingContextPourEdition().revert();
		}
		return doNothing();
	}
	
	public WOActionResults annulerEditerNote() {
		editingContextPourEdition().revert();
		CktlAjaxWindow.close(context(), editedNoteWindowId());
		return doNothing();
	}
	
	public WOActionResults supprimerNote() {
		contrat().avenantZero()
			.removeFromEvenementsRelationship(selectedNote());
		//editingContextPourEdition().deleteObject(selectedNote());
		try {
			editingContextPourEdition().saveChanges();
			notesDataSource().deleteObject(selectedNote());
			notesDisplayGroup().fetch();
			
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			editingContextPourEdition().revert();
		}
		return doNothing();
	}

	public String editedNoteObjet() {
		return editedNoteObjet;
	}

	public void setEditedNoteObjet(String editedNoteObjet) {
		this.editedNoteObjet = editedNoteObjet;
	}
	
	public Boolean hasSelectedNote() {
		return notesDisplayGroup().selectedObject() != null;
	}
	
}