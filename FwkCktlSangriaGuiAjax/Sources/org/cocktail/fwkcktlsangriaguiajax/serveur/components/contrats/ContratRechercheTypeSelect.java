package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;


import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxSelectWOComponent;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.fwkcktlsangriaguiajax.serveur.controllers.ContratRechercheTypeSelectController;

public class ContratRechercheTypeSelect extends CktlAjaxSelectWOComponent {
    
    public static final String VALUE_BDG = "value";
	public static final String BINDING_editingContext = "editingContext";
	

	private ContratRechercheTypeSelectController ctrl;
	private TypeContrat selection;
	
	public ContratRechercheTypeSelect(WOContext context) {
        super(context);
        //EOEditingContext ed = (EOEditingContext)valueForBinding(CktlAjaxOrganSelect.EDC_BDG);
        //if (ed == null) ed = context.session().defaultEditingContext(); 
                
    	
    }
	
	public String typeContratSearchWindowId() {
		return getComponentId() + "_typeContratSearchWindow";
	}
	
    
	@Override
    public EOEditingContext edc() {
    	return (EOEditingContext)valueForBinding(BINDING_editingContext);
	}

	
	/**
	 * @return the ctrl
	 */
	public ContratRechercheTypeSelectController ctrl() {
		if (ctrl == null) {
			ctrl = new ContratRechercheTypeSelectController(this, edc());
		}
		return ctrl;
	}


	/**
	 * @param ctrl the ctrl to set
	 */
	public void setCtrl(ContratRechercheTypeSelectController ctrl) {
		this.ctrl = ctrl;
	}


	/**
	 * @return the organSelectionne
	 */
	public TypeContrat selection() {
		return selection;
	}

	/**
	 * @param organSelectionne the organSelectionne to set
	 */
	public void setSelection(TypeContrat selection) {
		this.selection = selection;
		setValueForBinding(selection, ContratRechercheTypeSelect.SELECTION_BDG);
	}

	public void onClose() {
		// ctrl().setRootNatureContrat(null);
		// setIsTreeViewOpened(Boolean.FALSE);
	}

	public TypeContrat treeRootObject() {
		return (TypeContrat) valueForBinding(TREE_ROOT_OBJECT_BDG);
	}
}