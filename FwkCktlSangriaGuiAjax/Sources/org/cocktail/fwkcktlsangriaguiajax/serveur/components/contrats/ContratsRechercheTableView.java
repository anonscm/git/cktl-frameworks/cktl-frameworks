package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXGenericRecord;

public class ContratsRechercheTableView extends AFwkCktlSangriaComponent {
	
	private final String CONTRAT_COURANT_KEY = "contratCourant";
	private final String BY_PASS_CONTRAT_RECHERCHE = "bypassContratRecherche";
	
	private final String NUMERO_KEY = "NUMERO";
	private final String INTITULE_KEY = "INTITULE";
	private final String TYPE_KEY = "TYPE";
	
    private EOContratRecherche contratRechercheCourant;
    private Contrat contratCourant;
    
	private NSMutableArray<String> _colonnesKeys = null; 
	private NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = null;

    
	public ContratsRechercheTableView(WOContext context) {
        super(context);
    }
    
    
    
	public String contratTableViewId() {
		return getComponentId() + "_contratTableView";
	}
	


		
	public void setContratCourant(ERXGenericRecord contratCourant) {
	    if(bypassContratRecherche()) {
	        this.contratCourant = (Contrat) contratCourant;
	    }
	    else {
	        this.contratRechercheCourant = (EOContratRecherche) contratCourant;
	    }
	}



	public ERXGenericRecord contratCourant() {
	    if(bypassContratRecherche()) {
	        return contratCourant;
	    }
	    else {
	        return contratRechercheCourant;
	    }
	}
	
	public Boolean bypassContratRecherche() {
	    return booleanValueForBinding(BY_PASS_CONTRAT_RECHERCHE, false);
	}



	private NSMutableArray<String> _colonnesKeys() {
		if(_colonnesKeys == null) {
			_colonnesKeys = new NSMutableArray<String>();
			_colonnesKeys().add(NUMERO_KEY);
			_colonnesKeys().add(INTITULE_KEY);
			_colonnesKeys().add(TYPE_KEY);
		}
		return _colonnesKeys;
	}
	
	private NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap() {
		if(_colonnesMap == null) {
			
			_colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();
			
			CktlAjaxTableViewColumn numero_colonne = new CktlAjaxTableViewColumn();
			numero_colonne.setLibelle("Numéro");
			numero_colonne.setRowCssClass("alignToCenter useMinWidth nowrap");
			
			CktlAjaxTableViewColumnAssociation numero_colonne_ass;
			if(bypassContratRecherche()) {
			    numero_colonne_ass = new CktlAjaxTableViewColumnAssociation(CONTRAT_COURANT_KEY + "." + "exerciceEtIndex", "");
			    numero_colonne.setOrderKeyPath("exerciceEtIndex");
			} else {
			    numero_colonne_ass = new CktlAjaxTableViewColumnAssociation(CONTRAT_COURANT_KEY + "." + EOContratRecherche.NUMERO_KEY, "");
			    numero_colonne.setOrderKeyPath(EOContratRecherche.NUMERO_KEY);
			}
			numero_colonne.setAssociations(numero_colonne_ass);
			_colonnesMap.takeValueForKey(numero_colonne, NUMERO_KEY);
	
			CktlAjaxTableViewColumn intitule_colonne = new CktlAjaxTableViewColumn();
			intitule_colonne.setLibelle("Intitulé");
			intitule_colonne.setRowCssClass("alignToLeft");
			CktlAjaxTableViewColumnAssociation intitule_colonne_ass;
	         if(bypassContratRecherche()) {
	             intitule_colonne_ass = new CktlAjaxTableViewColumnAssociation(CONTRAT_COURANT_KEY + "." + Contrat.CON_OBJET_KEY, "");
	         } else {
	             intitule_colonne_ass = new CktlAjaxTableViewColumnAssociation(CONTRAT_COURANT_KEY + "." + EOContratRecherche.OBJET_KEY, "");
	         }
			intitule_colonne.setAssociations(intitule_colonne_ass);
			_colonnesMap.takeValueForKey(intitule_colonne, INTITULE_KEY);
			
			CktlAjaxTableViewColumn type_colonne = new CktlAjaxTableViewColumn();
			type_colonne.setLibelle("Type");
			type_colonne.setRowCssClass("alignToLeft");
			CktlAjaxTableViewColumnAssociation type_colonne_ass;
	         if(bypassContratRecherche()) {
	             type_colonne_ass = new CktlAjaxTableViewColumnAssociation(CONTRAT_COURANT_KEY + "." + Contrat.TYPE_CONTRAT_KEY + "." + TypeContrat.TYCON_LIBELLE_KEY, "");
	             type_colonne.setOrderKeyPath(Contrat.TYPE_CONTRAT_KEY + "." + TypeContrat.TYCON_LIBELLE_KEY);
	         } else {
	             type_colonne_ass = new CktlAjaxTableViewColumnAssociation(CONTRAT_COURANT_KEY + "." + EOContratRecherche.CONTRAT_KEY +  "." + Contrat.TYPE_CONTRAT_KEY + "." + TypeContrat.TYCON_LIBELLE_KEY, "");
	             type_colonne.setOrderKeyPath(EOContratRecherche.CONTRAT_KEY + "." + Contrat.TYPE_CONTRAT_KEY + "." + TypeContrat.TYCON_LIBELLE_KEY);
	         }
			type_colonne.setAssociations(type_colonne_ass);
			_colonnesMap.takeValueForKey(type_colonne, TYPE_KEY);
			
		}
		return _colonnesMap;
	
	}
    
    
    public NSArray<CktlAjaxTableViewColumn> colonnes() {
    	NSMutableArray<CktlAjaxTableViewColumn> colonnes = new NSMutableArray<CktlAjaxTableViewColumn>();
    	for(String key : _colonnesKeys()) {
    		colonnes.add((CktlAjaxTableViewColumn) _colonnesMap().valueForKey(key));
    	}
    	return colonnes;
    }
}