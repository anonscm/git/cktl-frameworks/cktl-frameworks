package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryAvenant;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSForwardException;

import er.ajax.AjaxUploadProgress;
import er.ajax.AjaxUtils;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXProperties;

public class ContratRechercheDocumentGestion extends AFwkCktlSangriaComponent {

	


	private EOContratRecherche contratRecherche;

	private String sousCategorieGedi;
	private Courrier document;

	
	public NSData _data;
	public String _finalFilePath, _filePath;
	public AjaxUploadProgress _uploadProgress;
	private Avenant avenant;
	private Boolean withImmediateSave = Boolean.FALSE;
	
	private String modalBoxId;

	
	public ContratRechercheDocumentGestion(WOContext context) {
        super(context);
    }
	public ContratRechercheDocumentGestion(WOContext context, Boolean withImmediateSave) {
        super(context);
    }

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlwonder.js");
	}

   // Gestion des bindings
	
    public EOContratRecherche contratRecherche() {
    	return  contratRecherche;
    }
    
    public void setContratRecherche(EOContratRecherche contratRecherche) {
    	this.contratRecherche = contratRecherche;
    }
	
	public void setSousCategorieGedi(String sousCategorieGedi) {
		this.sousCategorieGedi = sousCategorieGedi;
	}
	public String getSousCategorieGedi() {
		return sousCategorieGedi;
	}
	public WOActionResults ajouterUnDocument() {
		if (contratRecherche().contrat().avenantZero() != null) {
			try { 
				NSArray<Avenant> avenants = contratRecherche().contrat().avenantsDontInitialNonSupprimes();
				EOUtilisateur utilisateur = EOUtilisateur.fetchByQualifier(edc(), ERXQ.equals(EOUtilisateur.PERS_ID_KEY, getUtilisateurPersId()));
				// fa.ajouterDocument(edc(), utilisateur, avtZero, document());
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
		}

		return doNothing();


	}

	
	public String categorieGedfs() {
		return  ERXProperties.stringForKey("CATEGORIE_GEDFS_CODE");
	}
	
	public Integer gediRoot() {
		return ERXProperties.intForKey("ROOT_GED_SANGRIA"); 
	}

	
	public void setModalBoxId(String modalBoxId) {
		this.modalBoxId = modalBoxId;
	}
	public String getModalBoxId() {
		return modalBoxId;
	}
	
	public WOActionResults uploadFinished() {
		System.out.println("FileUploadExample.uploadFinished: FINISHED!");
		return null;
	}



	public String failedFunction() {
		String failedFunction = null;
		failedFunction = "alert('Pour une raison technique, le serveur de gestion de documents est actuellement indisponible.Veuillez re-essayer plus tard.')";
		return failedFunction;
	}
	public void setDocument(Courrier document) {
		this.document = document;
	}
	public Courrier document() {
		return document;
	}
	
	public WOActionResults annuler() {
		
		if (getModalBoxId() != null) {
			CktlAjaxWindow.close(context(),getModalBoxId());
		}
		return null;
	}

}