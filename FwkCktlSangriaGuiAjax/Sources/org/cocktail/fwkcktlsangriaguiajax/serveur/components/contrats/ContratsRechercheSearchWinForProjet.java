package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats;

import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.ICktlAjaxWindowWrapperActionsDelegate;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class ContratsRechercheSearchWinForProjet extends AFwkCktlSangriaComponent {
	
	private WOActionResults creerContratRechercheAction;
	private ICktlAjaxWindowWrapperActionsDelegate rechercheContratActionDelegate;
	

	private EOContratRecherche selection;

	
	
	
    public ContratsRechercheSearchWinForProjet(WOContext context) {
        super(context);
    }

	
	
	
	public WOActionResults creerNouveauContrat() {
		return creerContratRechercheAction();
	}
	
	public void setSelection(EOContratRecherche selection) {
		this.selection = selection;
	}

	public EOContratRecherche selection() {
		return selection;
	}

	public void setCreerContratRechercheAction(
			WOActionResults creerContratRechercheAction) {
		this.creerContratRechercheAction = creerContratRechercheAction;
	}

	public WOActionResults creerContratRechercheAction() {
		return creerContratRechercheAction;
	}

	public void setRechercheContratActionDelegate(
			ICktlAjaxWindowWrapperActionsDelegate rechercheContratActionDelegate) {
		this.rechercheContratActionDelegate = rechercheContratActionDelegate;
	}

	public ICktlAjaxWindowWrapperActionsDelegate rechercheContratActionDelegate() {
		return rechercheContratActionDelegate;
	}

}