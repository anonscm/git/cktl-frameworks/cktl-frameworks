package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchepartieadminui;

import org.cocktail.fwkcktlpersonne.common.metier.EOLangue;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class ContratRecherchePartieAdminUIR extends AFwkCktlSangriaComponent {
	
    public static final String BINDING_CONTRAT_RECHERCHE = "contratRecherche";

    private EOContratRecherche contratRecherche;
	
    private EOLangue currentLangue;
    
    private Boolean notesFolded = true;
    
    public ContratRecherchePartieAdminUIR(WOContext context) {
        super(context);
    }
    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
    	setNotesFolded(true);
    	super.appendToResponse(response, context);
    }
    
    public EOContratRecherche contratRecherche() {
    	if (hasBinding(BINDING_CONTRAT_RECHERCHE)) {
    		contratRecherche = (EOContratRecherche) valueForBinding(BINDING_CONTRAT_RECHERCHE);
    	}
    	return contratRecherche;
    }
    
    public String suiviAdministratifDisplayString() {
    	return (String) EOContratRecherche.Nomenclatures.suivisAdministratifs().valueForKey(contratRecherche().suiviAdministratif());
    }

	public void setCurrentLangue(EOLangue currentLangue) {
		this.currentLangue = currentLangue;
	}

	public EOLangue getCurrentLangue() {
		return currentLangue;
	}

	public NSArray<EOLangue> languesDuContrat() {
		NSMutableArray<EOLangue> langues = ((NSArray<EOLangue>) contratRecherche().toRepartContratLangues().valueForKey(EORepartContratLangue.LANGUE_KEY)).mutableClone();
		EOSortOrdering llOrdering = EOSortOrdering.sortOrderingWithKey(EOLangue.LL_LANGUE_KEY, EOSortOrdering.CompareAscending);
		EOSortOrdering.sortArrayUsingKeyOrderArray(langues, new NSArray<EOSortOrdering>(llOrdering));
		return langues;
	}
	
	public String notesButtonsContainerId() {
    	return getComponentId() + "_notesButtonsContainer";
    }
    
    public String notesContainerId() {
    	return getComponentId() + "_notesContainer";
    }
    
    public Boolean notesFolded() {
		return notesFolded;
	}

	public void setNotesFolded(Boolean notesFolded) {
		this.notesFolded = notesFolded;
	}
	
	public WOActionResults foldNotes() {
		setNotesFolded(true);
		return doNothing();
	}
	
	public WOActionResults unFoldNotes() {
		setNotesFolded(false);
		return doNothing();
	}
	
	public String onClickFoldNotes() {
		return "Effect.BlindUp($('" + notesContainerId() + "'), {duration:0.5});";
	}
	
	public String onClickUnFoldNotes() {
		return "Effect.BlindDown($('" + notesContainerId() + "'), {duration:0.5});";
	}
    
}