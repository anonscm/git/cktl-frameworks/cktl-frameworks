package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchepartiefinui;

import java.math.BigDecimal;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.ModeGestion;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTva;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXStringUtilities;

public class ContratRecherchePartieFinUIM extends AFwkCktlSangriaComponent {

	private EOContratRecherche contratRecherche;

	public final static String BINDING_contratRecherche = "contratRecherche";

	private NSArray<EOTva> tauxDeTva;
	private EOTva unTauxDeTVA;
	private EOTva leTauxDeTVASelectionne;

	private EOStructure etablisssementGestionnaireFinancier;

	// private EOEditingContext personnesEditingContext = null;

	private NSArray<ModeGestion> listeModesGestion;
	private ModeGestion currentModeGestion;

	public ContratRecherchePartieFinUIM(WOContext context) {
		super(context);
	}

	public String montantPrelevementContainerId() {
		return getComponentId() + "MontantPrelevementContainer";
	}


	public String etablissementGestionnaireUpdateContainerId() {
		return getComponentId() + "etablissementGestionnaireUpdateContainer";
	}

	public String tfMontantHtId() {
		return "tfMontantHt";
	}

	public String tfTauxPrelevementId() {
		return "tfTauxPrelevement";
	}

	public String getMontantsFinanciersContainerId() {
		return getComponentId() + "_montantsFinanciersContainer";
	}


	// Gestion des bindings

	public EOContratRecherche contratRecherche() {
		if (hasBinding(BINDING_contratRecherche)) {
			contratRecherche = (EOContratRecherche) valueForBinding(BINDING_contratRecherche);
		}
		return contratRecherche;

	}

	public NSArray<EOTva> getTauxDeTVA() {
		if (tauxDeTva == null) {
			tauxDeTva = 
				EOTva.fetchAll(
					edc(), 
					ERXQ.equals(
						EOTva.TYPE_ETAT_KEY, 
						EOTypeEtat.getTypeEtat(edc(), EOTypeEtat.ETAT_VALIDE)
					),
					ERXS.ascs(EOTva.TVA_TAUX_KEY)
				);
		}
		return tauxDeTva;
	}

	/**
	 * @return the unTauxDeTVA
	 */
	public EOTva unTauxDeTVA() {
		return unTauxDeTVA;
	}

	/**
	 * @param unTauxDeTVA the unTauxDeTVA to set
	 */
	public void setUnTauxDeTVA(EOTva unTauxDeTVA) {
		this.unTauxDeTVA = unTauxDeTVA;
	}

	/**
	 * @return the leTauxDeTVASelectionne
	 */
	public EOTva leTauxDeTVASelectionne() {
		return contratRecherche().contrat().avenantZero().tva();
	}

	/**
	 * @param leTauxDeTVASelectionne the leTauxDeTVASelectionne to set
	 */
	public void setLeTauxDeTVASelectionne(EOTva leTauxDeTVASelectionne) {
		contratRecherche().contrat().avenantZero().setTvaRelationship(leTauxDeTVASelectionne);
	}

	public String getCodeSensMontantFinancierEntrant() {
		return EOContratRecherche.CODE_SENS_MONTANT_FINANCIER_ENTRANT;
	}

	public String getCodeSensMontantFinancierSortant() {
		return EOContratRecherche.CODE_SENS_MONTANT_FINANCIER_SORTANT;
	}
	
	public WOActionResults onUpdateTauxPrelevement() {
		Avenant avenant = contratRecherche().contrat().avenantZero();
		BigDecimal mtHT = avenant.avtMontantHt();
		float tauxPrelevement = (contratRecherche().tauxPrelevementEffectue() == null) ? 0 : contratRecherche().tauxPrelevementEffectue();
		contratRecherche().setMontantPrelevementEffectue(mtHT.multiply(BigDecimal.valueOf(tauxPrelevement)).divide(BigDecimal.valueOf(100)).floatValue());
		return doNothing();
	}

	public void setEtablisssementGestionnaireFinancier(EOStructure etablisssementGestionnaireFinancier) {
		this.etablisssementGestionnaireFinancier = etablisssementGestionnaireFinancier;
		contratRecherche().setEtablisssementGestionnaireFinancier(etablisssementGestionnaireFinancier(), getUtilisateurPersId());
	}

	public EOStructure etablisssementGestionnaireFinancier() {
		if (etablisssementGestionnaireFinancier == null) {
			// etablisssementGestionnaireFinancier = contratRecherche().contrat().centreResponsabilite();
			etablisssementGestionnaireFinancier = contratRecherche().etablisssementGestionnaireFinancier();
		}
		return etablisssementGestionnaireFinancier;
	}

	public NSArray<ModeGestion> listeModesGestion() {
		if (listeModesGestion == null) {
			listeModesGestion = ModeGestion.fetchAll(edc());
		}
		return listeModesGestion;
	}

	public void setListeModesGestion(NSArray<ModeGestion> listeModesGestion) {
		this.listeModesGestion = listeModesGestion;
	}

	public ModeGestion currentModeGestion() {
		return currentModeGestion;
	}

	public void setCurrentModeGestion(ModeGestion currentModeGestion) {
		this.currentModeGestion = currentModeGestion;
	}

	public Boolean isContratSansIncidenceFinanciere() {
		return ERXStringUtilities.stringEqualsString(contratRecherche().contrat().modeDeGestion().mgLibelleCourt(), ModeGestion.MODE_GESTION_SIF);
	}
	
	public void setContratSansIncidenceFinanciere(Boolean contratSansIncidenceFinanciere) {
		
	}

	public WOActionResults onChangeModeGestion() {
		if (contratRecherche().contrat().etablissement() == null) {

			contratRecherche().contrat().setEtablissementRelationship((EOStructure) ERXArrayUtilities.firstObject(EOIndividu.fetchFirstByQualifier(edc(), EOIndividu.PERS_ID.eq(getUtilisateurPersId())).getEtablissementsAffectation(null)), 
					getUtilisateurPersId());
		}
		if (isContratSansIncidenceFinanciere()) {
			try {
				contratRecherche().supprimerPartenaire(etablisssementGestionnaireFinancier(), getUtilisateurPersId());
			} catch (Exception e) {
				session().addSimpleErrorMessage("Erreur", "Erreur lors de la suppression de l'établissement gestionnaire financier : " + e.getMessage());
				e.printStackTrace();
			}
		} else if (etablisssementGestionnaireFinancier() != null) { // cas où on passe d'une convention sans incidence a une autre convention pour forcer l'ajout du partenaire
			setEtablisssementGestionnaireFinancier(etablisssementGestionnaireFinancier());
		}
		return doNothing();
	}

}