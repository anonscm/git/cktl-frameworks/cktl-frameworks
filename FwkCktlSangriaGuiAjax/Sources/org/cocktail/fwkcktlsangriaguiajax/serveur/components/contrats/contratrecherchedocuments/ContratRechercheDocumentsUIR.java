package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchedocuments;

import com.webobjects.appserver.WOContext;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche.UniteRechercheComponentUI;

public class ContratRechercheDocumentsUIR extends UniteRechercheComponentUI {
    public ContratRechercheDocumentsUIR(WOContext context) {
        super(context);
    }
}