package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrechercheidentificationui;

import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXProperties;

public class ContratRechercheIdentificationUIM extends AFwkCktlSangriaComponent {
    
	private static final String PARAM_GROUPE_PARTENAIRES_RECHERCHE = "GROUPE_PARTENAIRES_RECHERCHE";

	
	public final static String BINDING_contratRecherche = "contratRecherche";

	
	public EOStructure laboratoireAAjouter;
	public EOStructure laboratoireASupprimer;
	public EOStructure laboratoireSelectionne;
	
	private EOStructure currentLaboratoirePorteur;

	
	public EOIndividu responsableScientifiqueAAjouter;
	public EOIndividu responsableScientifiqueASupprimer;
	
	public IPersonne partenaireAAjouter;	
	private IPersonne currentPartenaire;
	
	private EODomaineScientifique domaineScientifique;
	
	private EOStructure currentStructureRecherche;
	private EOIndividu currentResponsableScientifique;
	
	
	private EOStructure selectedStructureRecherche;
	
//	private EOEditingContext personnesEditingContext = null;
	
	private Boolean resetPartenairesSearch = false;
	
	public ContratRechercheIdentificationUIM(WOContext context) {
        super(context);
    }
		
//
//	public EOEditingContext personnesEditingContext() {
//		if(personnesEditingContext == null) {
//			personnesEditingContext = ERXEC.newEditingContext(edc());
//		}
//		return personnesEditingContext;
//	}
	
	public String informationsAdministrativesContainerId() {
		return getComponentId() + "_informationsAdministrativesContainer";
	}
	
	public String structureRecherchePrincipaleRadioButtonName() {
	  return getComponentId() + "_structureRecherchePrincipaleRadioButton";
	}
	
	public TypeContrat getTypeContrat() {
		return contratRecherche().contrat().typeContrat();
	}
	
	public void setTypeContrat(TypeContrat typeContrat) {
		contratRecherche().contrat().setTypeContratRelationship(typeContrat);
	}
    
   // Gestion des bindings
	
    public EOContratRecherche contratRecherche() {
    	return (EOContratRecherche) valueForBinding(BINDING_contratRecherche);
    }
    
	public EOQualifier qualifierForGroupes() {
		return ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_LA);
	}
	
	public EOQualifier qualifierForGroupePartenairesRecherche() {
		if(ERXProperties.booleanForKey("org.cocktail.fwkcktlsangriaguiajax.contratsrecherche.AllerChercherDansPartenairesRecherche")) {
			String codeStructureGroupePartenairesRecherche = EOGrhumParametres.parametrePourCle(edc(), PARAM_GROUPE_PARTENAIRES_RECHERCHE);
			return EOStructure.C_STRUCTURE.eq(codeStructureGroupePartenairesRecherche);
		} else {
			return null;
		}
		
	}
	
	
	// LABORATOIRE CONCERNE
	
	public EOStructure getLaboratoireAAjouter() {
		return laboratoireAAjouter;
	}

	public void setLaboratoireAAjouter(EOStructure laboratoireAAjouter) {
		this.laboratoireAAjouter = laboratoireAAjouter;
	}

	public EOStructure getLaboratoireASupprimer() {
		return laboratoireASupprimer;
	}

	public void setLaboratoireASupprimer(EOStructure laboratoireASupprimer) {
		this.laboratoireASupprimer = laboratoireASupprimer;
	}
	

	public EOStructure getLaboratoireSelectionne() {
		return laboratoireSelectionne;
	}

	public void setLaboratoireSelectionne(EOStructure laboratoireSelectionne) {
		this.laboratoireSelectionne = laboratoireSelectionne;
	}
	
	// RESPONSABLE SCIENTIFIQUE
	
	public IPersonne partenaireAAjouter() {
		return partenaireAAjouter;
	}

	public void setPartenaireAAjouter(IPersonne partenaireAAjouter) {
		this.partenaireAAjouter = partenaireAAjouter;
	}
	
	public NSArray<EODomaineScientifique> domainesScientifiques() {
		return EODomaineScientifique.fetchAll(edc());
	}

	public void setDomaineScientifique(EODomaineScientifique domaineScientifique) {
		this.domaineScientifique = domaineScientifique;
	}

	public EODomaineScientifique getDomaineScientifique() {
		return domaineScientifique;
	}

	/*******************************************/
	/*** Gestion des laboratoires porteurs ***/

	
	public void setSelectedStructureRecherche(EOStructure selectedStructureRecherche) {
		this.selectedStructureRecherche = selectedStructureRecherche;
	}

	public EOStructure selectedStructureRecherche() {
		return selectedStructureRecherche;
	}

	public void setCurrentLaboratoirePorteur(EOStructure currentLaboratoirePorteur) {
		this.currentLaboratoirePorteur = currentLaboratoirePorteur;
	}
	
	public EOStructure currentLaboratoirePorteur() {
		return this.currentLaboratoirePorteur;
	}
	
	public String laboratoiresPorteursContainerId() {
		return getComponentId() + "_laboratoiresPorteursContainer";
	}
	
	public String laboratoirePorteurSearchWindowId() {
		return getComponentId() + "_laboratoirePorteurSearchWindow";
	}

	public WOActionResults onSuppressionLaboratoirePorteur() {
		try {
			NSArray<EOIndividu> responsablesScientifiques = responsablesScientifiques();
			for (EOIndividu responsableScientifique : responsablesScientifiques) {
				contratRecherche().supprimerResponsableScientifiquePourStructureRecherche(responsableScientifique, currentLaboratoirePorteur(), getUtilisateurPersId());
			}
			contratRecherche().supprimerStructureRechercheConcernee(currentLaboratoirePorteur(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleLocalizedErrorMessage("attention", "erreur.suppression.structurePorteuse");		
		}
		return doNothing();
	}

	public void setCurrentStructureRecherche(EOStructure currentStructureRecherche) {
		this.currentStructureRecherche = currentStructureRecherche;
	}

	public EOStructure currentStructureRecherche() {
		return currentStructureRecherche;
	}
	
	public WOActionResults selectionnerStructureRecherche() {
		
		try {
			contratRecherche().ajouterStructureConcernee(selectedStructureRecherche(), getUtilisateurPersId());
			CktlAjaxWindow.close(context(), laboratoirePorteurSearchWindowId());
		} catch (Exception e) {
			session().addSimpleLocalizedErrorMessage("attention", "erreur.ajout.structurePorteuse");		
		}

		return doNothing();
		
	}
	
	public WOActionResults annulerSelectionnerLaboratoire() {
		CktlAjaxWindow.close(context(), laboratoirePorteurSearchWindowId());
		return doNothing();
	}
	
	public Boolean isStructureRecherchePrincipale() {
	  return ERXEOControlUtilities.eoEquals(currentLaboratoirePorteur(), contratRecherche().structureRecherchePrincipale());
	}
	
	public void setStructureRecherchePrincipale(Boolean principale) {
	  if(principale) {
	    contratRecherche().setStructureRecherchePrincipale(currentLaboratoirePorteur(), getUtilisateurPersId());
	  }
	}
	

	/*******************************************/
	/*** Gestion des responsables scientifiques ***/
	
	public void setCurrentResponsableScientifique(EOIndividu currentResponsableScientifique) {
		this.currentResponsableScientifique = currentResponsableScientifique;
	}

	public EOIndividu currentResponsableScientifique() {
		return currentResponsableScientifique;
	}
    
	public String responsablesScientifiquesContainerId() {
		return getComponentId() + "_responsablesScientifiquesContainer_" + currentLaboratoirePorteur().persId();
	}
	
	public String responsableScientifiqueSearchWindowId() {
		return getComponentId() + "_responsableScientifiqueSearchWindow_" + currentLaboratoirePorteur().persId();
	}
	
	public WOActionResults onAjoutResponsableScientifiqueOk() {
		if(responsableScientifiqueAAjouter() != null) {
			try {
				contratRecherche().ajouterResponsableScientifiquePourStructureRecherche(responsableScientifiqueAAjouter(), currentLaboratoirePorteur());
				CktlAjaxWindow.close(context(), responsableScientifiqueSearchWindowId());
				AjaxUpdateContainer.updateContainerWithID(laboratoiresPorteursContainerId(), context());
			} catch (Exception e) {
				session().addSimpleLocalizedErrorMessage("attention", "erreur.ajout.responsableScientifique");		
			}
		} else {
			session().addSimpleErrorMessage("Erreur", "Vous devez sélectionner une personne");
		}

		
		return doNothing();
	}
	
	public WOActionResults onAjoutResponsableScientifiqueAnnuler() {
//		personnesEditingContext().revert();
		CktlAjaxWindow.close(context(), responsableScientifiqueSearchWindowId());
		return doNothing();
	}
	
	public WOActionResults onSuppressionResponsableScientifique() {
		try {
			contratRecherche().supprimerResponsableScientifiquePourStructureRecherche(currentResponsableScientifique(), currentLaboratoirePorteur(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleLocalizedErrorMessage("attention", "erreur.suppression.responsableScientifique");		
		}
		return doNothing();
	}
	
	public EOIndividu responsableScientifiqueAAjouter() {
		return responsableScientifiqueAAjouter;
	}

	public void setResponsableScientifiqueAAjouter(EOIndividu responsableScientifiqueAAjouter) {
		this.responsableScientifiqueAAjouter = responsableScientifiqueAAjouter;
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOIndividu> responsablesScientifiques() {
		return contratRecherche().responsablesScientifiquesPourLaboratoire(currentLaboratoirePorteur());
	}
	
    public EOQualifier qualifierForResponsables() {
    	if(currentLaboratoirePorteur() == null) {
    		return null;
    	}
    	NSArray<EOStructure> filles = currentLaboratoirePorteur().getStructuresFilles();
    	return EOIndividu.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).eq(currentLaboratoirePorteur())
    		.or(EOIndividu.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).in(filles));
    }
	
	/*******************************************/
	/*** Gestion des partenaires ***/
	
	public void setCurrentPartenaire(IPersonne currentPartenaire) {
		this.currentPartenaire = currentPartenaire;
	}

	public IPersonne currentPartenaire() {
		return currentPartenaire;
	}
    
	public String partenairesContainerId() {
		return getComponentId() + "_partenairesContainer";
	}
	
	public String partenaireSearchWindowId() {
		return getComponentId() + "_partenaireSearchWindow";
	}
	
	public WOActionResults onAjoutPartenaireOk() {

		if (partenaireAAjouter() != null) {
			try {
				contratRecherche().ajouterPartenaire(partenaireAAjouter());
				CktlAjaxWindow.close(context(), partenaireSearchWindowId());
				setResetPartenairesSearch(true);
			} catch (Exception e) {
				session().addSimpleLocalizedErrorMessage("attention", "erreur.ajout.partenaire");		
			}
		} else {
			session().addSimpleErrorMessage("Erreur",
					"Vous devez sélectionner un partenaire");
		}

		return doNothing();
	}
	
	public WOActionResults onAjoutPartenaireAnnuler() {
		CktlAjaxWindow.close(context(), partenaireSearchWindowId());
		setResetPartenairesSearch(true);
		return doNothing();
	}
	
	public WOActionResults onSuppressionPartenaire() {
		try {
			contratRecherche().supprimerPartenaire(currentPartenaire(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleLocalizedErrorMessage("attention", "erreur.suppression.partenaire");		
		}
		return doNothing();
	}

	public void setCurrentExercice(Integer currentExercice) {
		EOExerciceCocktail exercice = (EOExerciceCocktail) EOUtilities.objectMatchingKeyAndValue(edc(), EOExerciceCocktail.ENTITY_NAME, EOExerciceCocktail.EXE_EXERCICE_KEY, currentExercice);
		contratRecherche().contrat().setExerciceCocktailRelationship(exercice);
	}

	public Integer currentExercice() {
		return contratRecherche().contrat().exerciceCocktail().exeExercice();
	}


	public Boolean resetPartenairesSearch() {
		return resetPartenairesSearch;
	}


	public void setResetPartenairesSearch(Boolean resetPartenairesSearch) {
		this.resetPartenairesSearch = resetPartenairesSearch;
	}


	
	
	
	

	
	

    
}