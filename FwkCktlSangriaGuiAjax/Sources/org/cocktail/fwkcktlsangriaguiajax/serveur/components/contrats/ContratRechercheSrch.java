package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.finder.FinderContratRecherche;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;

public class ContratRechercheSrch extends AFwkCktlSangriaComponent {
    
	private final String BINDING_selectedContratRecherche = "selectedContratRecherche";
	private final String BINDING_selectedContrat = "selectedContrat";
	
	private Integer annee;
	private Integer numero;
	private EOStructure structureRechercheConcernee;
	private EOIndividu responsableScientifique;
	private EOStructure partenaire;
	
	private FinderContratRecherche finderContratRecherche;
	
	private ERXDisplayGroup<EOContratRecherche> dgContratRecherche = null;

	private EOContratRecherche selectedContratRecherche;
	private Contrat selectedContrat;
	
    public ContratRechercheSrch(WOContext context) {
        super(context);
    }
    
    public String formulaireContainerId() {
    	return getComponentId() + "_formulaireContainer";
    }
    
    public String tableViewContainerId() {
    	return getComponentId() + "_tableViewContainer";
    }

	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	public Integer annee() {
		return annee;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer numero() {
		return numero;
	}

	public void setStructureRechercheConcernee(
			EOStructure structureRechercheConcernee) {
		this.structureRechercheConcernee = structureRechercheConcernee;
	}

	public EOStructure structureRechercheConcernee() {
		return structureRechercheConcernee;
	}

	public void setResponsableScientifique(EOIndividu responsableScientifique) {
		this.responsableScientifique = responsableScientifique;
	}

	public EOIndividu responsableScientifique() {
		return responsableScientifique;
	}

	public void setPartenaire(EOStructure partenaire) {
		this.partenaire = partenaire;
	}

	public EOStructure partenaire() {
		return partenaire;
	}
    
	
	private FinderContratRecherche finderContratRecherche() {
		if (finderContratRecherche == null) {
			finderContratRecherche = new FinderContratRecherche(edc());
		}
		return finderContratRecherche;
		
	}
	
	public WOActionResults lancerRecherche() {

		finderContratRecherche().clear();

		finderContratRecherche().setExercice(annee());
		finderContratRecherche().setOrdre(numero());
		finderContratRecherche().setStructureRecherche(structureRechercheConcernee());
		finderContratRecherche().setResponsableScientifique(responsableScientifique());
		finderContratRecherche().setPartenaire(partenaire()); 

		try {
			NSArray<EOContratRecherche> resultats = finderContratRecherche().findContratRecherches();
			dgContratRecherche().setObjectArray(resultats);
			if (resultats.isEmpty()) {
			    setSelectedContratRecherche(null);
				session().addSimpleInfoMessage("Attention", "La recherche n'a donné aucun résultat.");
			}
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}


		return doNothing();
	}
	
	   public Boolean hasResultatsDeRecherche() {
	        return dgContratRecherche().allObjects().size() > 0;
	    }
	
	
	public WOActionResults resetFormulaire() {
        // CHAMPS DU FORMULAIRE
		setAnnee(null);
		setNumero(null);
		setStructureRechercheConcernee(null);
		setResponsableScientifique(null);
		setPartenaire(null);
		
	      // RESET DU DG
        dgContratRecherche().setObjectArray(null);
        
        // MISE A JOUR  DE L'INTERFACE
        AjaxUpdateContainer.updateContainerWithID(tableViewContainerId(), context());
        AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
        
        // DESLECTION DE L'AAP
        setSelectedContratRecherche(null);

		return doNothing();
	}
	
	public ERXDisplayGroup<EOContratRecherche> dgContratRecherche() {

		if (dgContratRecherche == null) {
			dgContratRecherche = new ERXDisplayGroup<EOContratRecherche>();
			dgContratRecherche.setDelegate(this);
		}
		return dgContratRecherche;

	}

	public void setSelectedContratRecherche(EOContratRecherche selectedContratRecherche) {
		this.selectedContratRecherche = selectedContratRecherche;
		setValueForBinding(selectedContratRecherche, BINDING_selectedContratRecherche);
	}

	public EOContratRecherche selectedContratRecherche() {
		selectedContratRecherche = (EOContratRecherche) valueForBinding(BINDING_selectedContratRecherche);
		return selectedContratRecherche;
	}

	public Contrat selectedContrat() {
        return selectedContrat;
    }

    public void setSelectedContrat(Contrat selectedContrat) {
        this.selectedContrat = selectedContrat;
    }
    
	public void displayGroupDidChangeSelection(WODisplayGroup group) {
		ERXDisplayGroup<EOContratRecherche> contratRechercheGroup = (ERXDisplayGroup<EOContratRecherche>) group;
		if (group.selectedObject() != null) {
			setSelectedContratRecherche(contratRechercheGroup.selectedObject());
		}
	}
}