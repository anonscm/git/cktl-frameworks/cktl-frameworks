package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchenotes;

import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;

public class ContratRechercheNotesUIM extends AFwkCktlSangriaComponent {
	private static final long serialVersionUID = 3321026233567753525L;

	public ContratRechercheNotesUIM(WOContext context) {
        super(context);
    }
}