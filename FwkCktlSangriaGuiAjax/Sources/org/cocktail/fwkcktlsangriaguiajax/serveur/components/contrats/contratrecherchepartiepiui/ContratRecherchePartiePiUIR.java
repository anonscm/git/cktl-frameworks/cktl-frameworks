package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchepartiepiui;

import org.cocktail.fwkcktldroitsutils.common.finders.EOUtilisateurFinder;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class ContratRecherchePartiePiUIR extends AFwkCktlSangriaComponent {

	private EOContratRecherche contratRecherche;
	
	public final static String BINDING_contratRecherche = "contratRecherche";

	

	private String currentApportDrv;
	private String currentApportDaj;
	private String currentTypeDeResultats;
	private EOIndividu currentDestinataireAlerteSuivi;
	
	public ContratRecherchePartiePiUIR(WOContext context) {
        super(context);
    }
    
    public EOContratRecherche contratRecherche() {
		if(hasBinding(BINDING_contratRecherche))
			contratRecherche = (EOContratRecherche) valueForBinding(BINDING_contratRecherche);
		
		return contratRecherche;
	
    }

	public String currentTypeRedactionContratDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesRedactionContrat().valueForKey(contratRecherche().redactionContrat());
	}
	
	public String currentTypeRedactionClausesPiDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesRedactionClausesPi().valueForKey(contratRecherche().redactionClausesPi());
	}
	
	
	public NSArray<String> apportsContratsSurDrvContrat() {
		return ((NSMutableArray<String>) contratRecherche().toRepartApportDrvContrats().valueForKey(EORepartApportDrvCont.APPORT_CONT_DRV_KEY)).mutableClone();
	}
	
	public void setCurrentApportDrv(String currentApportDrv) {
		this.currentApportDrv = currentApportDrv;
	}

	public String getCurrentApportDrv() {
		return currentApportDrv;
	}
	
	public String currentApportDrvDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesApportDrvContrat().valueForKey(currentApportDrv);
	}
	
	
	
	public NSArray<String> apportsContratsSurDajContrat() {
		return ((NSMutableArray<String>) contratRecherche().toRepartApportDajContrats().valueForKey(EORepartApportDajCont.APPORT_CONT_DAJ_KEY)).mutableClone();
	}
	
	public void setCurrentApportDaj(String currentApportDaj) {
		this.currentApportDaj = currentApportDaj;
	}

	public String getCurrentApportDaj() {
		return currentApportDaj;
	}

	public String currentApportDajDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesApportDajContrat().valueForKey(currentApportDaj);
	}
 
	// PROPRIETE DES RESULTATS
	
	public String getCurrentProprieteDesResultatsDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesProprieteResultats().valueForKey(contratRecherche().proprieteDesResultats());
	}

	// EXPLOITATION DES RESULTATS
	
	public String getCurrentExploitationDesResultatsDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesExploitationResultats().valueForKey(contratRecherche().exploitationDesResultats());
	}
	
	
	public NSArray<String> typesDeResultatsContrat() {
		return ((NSMutableArray<String>) contratRecherche().toRepartTypeResultatsContrats().valueForKey(EORepartTypeResultatsContrat.TYPE_RESULTATS_CONTRAT_KEY)).mutableClone();
	}
	
	public void setCurrentTypeDeResultats(String currentTypeDeResultats) {
		this.currentTypeDeResultats = currentTypeDeResultats;
	}

	public String getCurrentTypeDeResultats() {
		return currentTypeDeResultats;
	}

	public String currentTypeDeResultatsDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesDeResultats().valueForKey(currentTypeDeResultats);
	}
	
	public String suiviValoDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typeSuiviValo().valueForKey(contratRecherche().suiviValo()); 
	}

	public void setCurrentDestinataireAlerteSuivi(
			EOIndividu currentDestinataireAlerteSuivi) {
		this.currentDestinataireAlerteSuivi = currentDestinataireAlerteSuivi;
	}

	public EOIndividu getCurrentDestinataireAlerteSuivi() {
		return currentDestinataireAlerteSuivi;
	}
	
	public EOUtilisateur utilisateur() {
		return EOUtilisateurFinder.fecthUtilisateurByPersId(edc(), getUtilisateurPersId());
	}
	
	
	
	
	
}