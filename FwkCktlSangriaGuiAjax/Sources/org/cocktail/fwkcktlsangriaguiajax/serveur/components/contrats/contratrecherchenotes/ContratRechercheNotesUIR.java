package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchenotes;

import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;

public class ContratRechercheNotesUIR extends AFwkCktlSangriaComponent {
	private static final long serialVersionUID = -3182599252616182599L;

	public ContratRechercheNotesUIR(WOContext context) {
        super(context);
    }
}