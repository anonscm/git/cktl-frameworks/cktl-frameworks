package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSPropertyListSerialization;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;

public class RapporteursTableView extends AFwkCktlSangriaComponent {
	
	private static final String BINDING_aap = "aap";
	
	
	private NSMutableDictionary dicoTableViewRapporteurs;
	private WODisplayGroup dgRepartRapporteurs;
	private boolean refreshRapporteurs;	
	
	EORepartAapAvisRapporteur repartAapAvisRapporteur;

	private EOEditingContext personnesEditingContext = null;

	private String currentAvisKey;
	private String currentNoteKey;


	private EOIndividu currentRapporteur;
	private EOIndividu rapporteurAAjouter;
	
    public RapporteursTableView(WOContext context) {
        super(context);
    }
    
	public EOEditingContext personnesEditingContext() {
		if(personnesEditingContext == null) {
			personnesEditingContext = ERXEC.newEditingContext(edc());
		}
		return personnesEditingContext;
	}
    
    
    public String tableViewRapporteursId() {
    	return getComponentId() + "_TableViewRapporteurs";
    }
 
    public String tableViewContainerId() {
    	return getComponentId() + "_TableViewContainer";
    }
    
    
    
    
    public EOAap getAap() {
    	return (EOAap) valueForBinding(BINDING_aap);
    }
    

    
	public NSMutableDictionary dicoTableViewRapporteurs() {
		if (dicoTableViewRapporteurs == null) {
			NSData data = new NSData(application().resourceManager().bytesForResourceNamed("TableViewRapporteurs.plist", frameworkName(), NSArray.EmptyArray));
			dicoTableViewRapporteurs = new NSMutableDictionary((NSDictionary) NSPropertyListSerialization.propertyListFromData(data, "UTF-8"));
		}
		return dicoTableViewRapporteurs;
	}
	
	
	public WODisplayGroup dgRepartRapporteurs() {
		
		
		
		if (dgRepartRapporteurs==null || refreshRapporteurs) {
			
			refreshRapporteurs = false;
			

			
			dgRepartRapporteurs = new ERXDisplayGroup<EORepartAapAvisRapporteur>();
	        dgRepartRapporteurs.setObjectArray(getAap().toRepartAapAvisRapporteurs());
	        
		}
		return dgRepartRapporteurs;
	}

	public EORepartAapAvisRapporteur getRepartAapAvisRapporteur() {
		return repartAapAvisRapporteur;
	}

	public void setRepartAapAvisRapporteur(
			EORepartAapAvisRapporteur repartAapAvisRapporteur) {
		this.repartAapAvisRapporteur = repartAapAvisRapporteur;
	}
	
	public NSDictionary<String, String> getListeDesAvis() {
		return getRepartAapAvisRapporteur().getListeDesAvis();
	}

	public String getAvisDisplayString() {
		return (String) getListeDesAvis().valueForKey(currentAvisKey);
	}
	
	public String getNoteDisplayString() {
		return (String) getRepartAapAvisRapporteur().getListeDesNotes().valueForKey(currentNoteKey);
	}
	
	public void setCurrentAvisKey(String currentAvisKey) {
		this.currentAvisKey = currentAvisKey;
	}

	public String getCurrentAvisKey() {
		return currentAvisKey;
	}



	public void setCurrentNoteKey(String currentNoteKey) {
		this.currentNoteKey = currentNoteKey;
	}



	public String getCurrentNoteKey() {
		return currentNoteKey;
	}

	public void setCurrentRapporteur(EOIndividu currentRapporteur) {
		this.currentRapporteur = currentRapporteur;
	}

	public EOIndividu currentRapporteur() {
		return currentRapporteur;
	}
	
	public EOIndividu rapporteurAAjouter() {
		return rapporteurAAjouter;
	}

	public void setRapporteurAAjouter(EOIndividu rapporteurAAjouter) {
		this.rapporteurAAjouter = rapporteurAAjouter;
	}

	
	public String rapporteurSearchWindowId() {
		return getComponentId() + "_rapporteurSearchWindow";
	}
	
	public WOActionResults onAjoutRapporteur() {
		
		try {
			rapporteurAAjouter().setValidationEditingContext(personnesEditingContext());
			rapporteurAAjouter().setPersIdModification(
					getUtilisateurPersId());
			personnesEditingContext().saveChanges();
			
			EORepartAapAvisRapporteur newRepartAapAvisRapporteur = new EORepartAapAvisRapporteur();
			edc().insertObject(newRepartAapAvisRapporteur);
			
			newRepartAapAvisRapporteur.setPersIdCreation(getUtilisateurPersId());
			newRepartAapAvisRapporteur.setPersIdModification(getUtilisateurPersId());
			newRepartAapAvisRapporteur.setDCreation(new NSTimestamp());
			newRepartAapAvisRapporteur.setDModification(new NSTimestamp());
			newRepartAapAvisRapporteur.setRapporteurRelationship(rapporteurAAjouter().localInstanceIn(edc()));
			newRepartAapAvisRapporteur.setAapRelationship(getAap());
			
			getAap().addToToRepartAapAvisRapporteursRelationship(newRepartAapAvisRapporteur);
			refreshRapporteurs = true;
		} catch (Exception e) {
			personnesEditingContext().revert();
			session().addSimpleErrorMessage(
					"Erreur",
					"Des informations relatives au rapporteur sélectionné sont invalides. Vous ne pouvez pas utiliser cette personne pour le moment.");
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	
	public WOActionResults onSuppressionRapporteur() {
		if(dgRepartRapporteurs().selectedObject() != null) {
			EORepartAapAvisRapporteur repartAapAvisRapporteur = (EORepartAapAvisRapporteur) dgRepartRapporteurs().selectedObject();
			repartAapAvisRapporteur.setPersIdModification(getUtilisateurPersId());
			dgRepartRapporteurs().deleteSelection();
			repartAapAvisRapporteur.setAapRelationship(null);
			edc().deleteObject(repartAapAvisRapporteur);
		}
		return doNothing();
	}
	

	
	
}