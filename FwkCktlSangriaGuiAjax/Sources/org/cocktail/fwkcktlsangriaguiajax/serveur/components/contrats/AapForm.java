package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats;

import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

public class AapForm extends AFwkCktlSangriaComponent {
    public AapForm(WOContext context) {
        super(context);
    }
    
    public final static String BINDING_aap = "aap";
    public final static String BINDING_onDeleteUpdateContainerId = "onDeleteUpdateContainerId";
    public final static String BINDING_deleteAapActionName = "deleteAapActionName";
    
    private EOAap aap;
    
    

    
   // Gestion des bindings
	
    public EOAap aap() {

    	if(hasBinding(BINDING_aap))
    		aap = (EOAap) valueForBinding(BINDING_aap);
    	return aap;
    	
    }
    
    public String onDeleteUpdateContainerId() {
    	if(hasBinding(BINDING_onDeleteUpdateContainerId))
    		return (String) valueForBinding(BINDING_onDeleteUpdateContainerId);
    	return null;
    }
    
    public String deleteAapActionName() {
    	if(hasBinding(BINDING_deleteAapActionName))
    		return (String) valueForBinding(BINDING_deleteAapActionName);
    	return null;
    }
	
    
    
	public WOActionResults enregistrerLesModications() {
		try {
			aap().setDModification(new NSTimestamp());
			edc().saveChanges();
		}
		catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	
	public WOActionResults supprimerAap() {
		try {
			performParentAction(deleteAapActionName());
		}
		catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur",e);
		}
		return doNothing();
	}
	
	public String onSuccessEnregistrerLesModifications() {
		return "function() { " + updateContainerID() + "Update(); " + "alert(\"L'AAP a été mis à jour correctement\"); }";
	}

	
	public String onSuccessSupprimerAap() {
		return "function() { " + onDeleteUpdateContainerId() + "Update(); " +  updateContainerID() + "Update();  }";
	}

	
}