package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchepartiefinui;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.ModeGestion;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;

import er.extensions.foundation.ERXStringUtilities;

public class ContratRecherchePartieFinUIR extends AFwkCktlSangriaComponent {
	
	private EOContratRecherche contratRecherche;
	
	public final static String BINDING_contratRecherche = "contratRecherche";
	
    public ContratRecherchePartieFinUIR(WOContext context) {
        super(context);
    }
    
   // Gestion des bindings
	
    public EOContratRecherche contratRecherche() {
    		if(hasBinding(BINDING_contratRecherche))
    			contratRecherche = (EOContratRecherche) valueForBinding(BINDING_contratRecherche);
    	return contratRecherche;
    	
    }
    
	public String leTauxDeTVASelectionne() {
		
		String leTauxDeTVASelectionne = null;
		if (leTauxDeTVASelectionne == null) {
			Avenant avenant = contratRecherche().contrat().avenantZero();
			BigDecimal montantHT = avenant.avtMontantHt();
			BigDecimal montantTTC = avenant.avtMontantTtc();
			if (montantHT != null && montantHT.doubleValue()>0 && montantTTC != null) {
				double taux = (montantTTC.doubleValue()-montantHT.doubleValue())/montantHT.doubleValue()*100;
				String masque = "#0.##";
				DecimalFormatSymbols dfs = new DecimalFormatSymbols();
				dfs.setDecimalSeparator('.');
				DecimalFormat formatter = new DecimalFormat(masque, dfs);
				leTauxDeTVASelectionne = formatter.format(taux);
			}
		}
		return leTauxDeTVASelectionne;
	}
    
	public String getLibelleSensMontantFinancier() {
		return contratRecherche().getLibelleSensMontantFinancier();
	}
	
	public Boolean isContratSansIncidenceFinanciere() {
		return ERXStringUtilities.stringEqualsString(contratRecherche().contrat().modeDeGestion().mgLibelleCourt(), ModeGestion.MODE_GESTION_SIF);
	}
	
}