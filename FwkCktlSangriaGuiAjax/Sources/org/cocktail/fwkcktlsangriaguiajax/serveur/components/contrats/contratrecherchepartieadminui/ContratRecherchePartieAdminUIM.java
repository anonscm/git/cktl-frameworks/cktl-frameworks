package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchepartieadminui;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOLangue;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue;
import org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat;
import org.cocktail.fwkcktlrecherche.server.metier.EOZoneGeographique;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;

public class ContratRecherchePartieAdminUIM extends AFwkCktlSangriaComponent {
	
	private EOContratRecherche contratRecherche;
	
	private String currentSuiviAdmin; 
	private EOZoneGeographique currentZoneGeographique;
	
	
	private EOLangue currentLangue;
	private EOLangue currentContratLangue;
	
	private NSArray<EOLangue> selectedLangues;
	private NSArray<EOLangue> selectedContratLangues;

	private EOIndividu createurDuDossier;
	private EOIndividu selectedCreateurDuDossier;
	
	private EOIndividu validateurJuridique;
   	private EOIndividu selectedValidateurJuridique;

	private EOIndividu validateurFinancier;
   	private EOIndividu selectedValidateurFinancier;

	private EOIndividu validateurValo;
   	private EOIndividu selectedValidateurValo;

	public static final String BINDING_CONTRAT_RECHERCHE = "contratRecherche";

	private Boolean notesFolded = true;

	private static final int VALIDATION_JURIDIQUE = 1;
	private static final int VALIDATION_FINANCIERE = 2;
	private static final int VALIDATION_VALORISATION = 3;
	private int typeValidation;
	private String commentaireAAjouter;
	
	
    public ContratRecherchePartieAdminUIM(WOContext context) {
        super(context);
    }
    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
    	setNotesFolded(true);
    	super.appendToResponse(response, context);
    }
    
   // Gestion des bindings
	
    public EOContratRecherche contratRecherche() {
    	if (hasBinding(BINDING_CONTRAT_RECHERCHE)) {
			contratRecherche = (EOContratRecherche) valueForBinding(BINDING_CONTRAT_RECHERCHE);
    	}
    	return contratRecherche;
    	
    }
    
    public String notesButtonsContainerId() {
    	return getComponentId() + "_notesButtonsContainer";
    }
    
    public String notesContainerId() {
    	return getComponentId() + "_notesContainer";
    }
    
    public String dateOuvertureDossierTextFieldId() {
    	return getComponentId() +  "dateOuvertureDossierTextField";
    }
    
    public String createurDossierUpdateContainerId() {
    	return getComponentId() + "_createurDossierUpdateContainer";
    }
 

    public String getValidationFinanciereContainerId() {
    	return getComponentId() + "_validationFinanciereContainer";
    }
    
    public String getValidationJuridiqueContainerId() {
    	return getComponentId() + "_validationJuridiqueContainer";
    }
    
    public String getValidationValoContainerId() {
    	return getComponentId() + "_validationValoContainer";
    }
    
    
    public String selectionnerCreateurDuDossierWindowId() {
    	return getComponentId() + "_selectionnerCreateurDuDossierWindow";
    }
    
    public String createurDuDossierContainerId() {
    	return getComponentId() + "_createurDuDossierContainer";
    }
   
    public String selectionnerCreateurDuDossierButtonsContainerId() {
    	return getComponentId() + "_selectionnerCreateurDuDossierButtonsContainer";
    }
    
    public String selectionnerValidateurFinancierWindowId() {
    	return getComponentId() + "_selectionnerValidateurFinancierWindow";
    }
    
    public String validateurFinancierContainerId() {
    	return getComponentId() + "_validateurFinancierContainer";
    }
    
    public String selectionnerValidateurFinancierButtonsContainerId() {
    	return getComponentId() + "_selectionnerValidateurFinancierButtonsContainer";
    }
    
    public String selectionnerValidateurJuridiqueWindowId() {
    	return getComponentId() + "_selectionnerValidateurJuridiqueWindow";
    }
    
    public String validateurJuridiqueContainerId() {
    	return getComponentId() + "_validateurJuridiqueContainer";
    }
    
    public String selectionnerValidateurJuridiqueButtonsContainerId() {
    	return getComponentId() + "_selectionnerValidateurJuridiqueButtonsContainer";
    }
    
    public String selectionnerValidateurValoWindowId() {
    	return getComponentId() + "_selectionnerValidateurValoWindow";
    }
    
    public String validateurValoContainerId() {
    	return getComponentId() + "_validateurValoContainer";
    }
    
    public String selectionnerValidateurValoButtonsContainerId() {
    	return getComponentId() + "_selectionnerValidateurValoButtonsContainer";
    }
    
    public String evenementsContainerId() {
    	return getComponentId() + "_evenementsContainer";
    }
    
    public String ajoutCommentaireWindowId() {
    	return getComponentId() + "_ajoutCommentaireWindowId";
    }
    
    // ID's des composants
    
    public String languesContainerId() {
    	return getComponentId() + "LanguesContainer";
    }

    
    public NSArray<String> suivisAdministratifs() {
    	return EOContratRecherche.Nomenclatures.suivisAdministratifs().allKeys();
    }
    
    
	// GESTION DES LANGUES
	public NSArray<EOLangue> languesDisponibles() {
		NSMutableArray<EOLangue> toutesLesLangues = EOLangue.fetchAll(edc(), ERXQ.in(EOLangue.C_LANGUE_KEY, EOContratRecherche.Nomenclatures.selectionLangueContrats())).mutableClone();
		toutesLesLangues.removeAll(languesDuContrat());
		EOSortOrdering llOrdering = EOSortOrdering.sortOrderingWithKey(EOLangue.LL_LANGUE_KEY, EOSortOrdering.CompareAscending);
		EOSortOrdering.sortArrayUsingKeyOrderArray(toutesLesLangues, new NSArray<EOSortOrdering>(llOrdering));
		return toutesLesLangues;
	}
	
	public NSArray<EOLangue> languesDuContrat() {
		NSMutableArray<EOLangue> langues = ((NSArray<EOLangue>) contratRecherche.toRepartContratLangues().valueForKey(EORepartContratLangue.LANGUE_KEY)).mutableClone();
		EOSortOrdering llOrdering = EOSortOrdering.sortOrderingWithKey(EOLangue.LL_LANGUE_KEY, EOSortOrdering.CompareAscending);
		EOSortOrdering.sortArrayUsingKeyOrderArray(langues, new NSArray<EOSortOrdering>(llOrdering));
		return langues;
	}
	
	
	public void setCurrentSuiviAdmin(String currentSuiviAdmin) {
		this.currentSuiviAdmin = currentSuiviAdmin;
	}


	public String getCurrentSuiviAdmin() {
		return currentSuiviAdmin;
	}
    
	public void setCurrentZoneGeographique(EOZoneGeographique currentZoneGeographique) {
		this.currentZoneGeographique = currentZoneGeographique;
	}


	public EOZoneGeographique getCurrentZoneGeographique() {
		return currentZoneGeographique;
	}


	public void setCurrentLangue(EOLangue currentLangue) {
		this.currentLangue = currentLangue;
	}


	public EOLangue getCurrentLangue() {
		return currentLangue;
	}


	public void setCurrentContratLangue(EOLangue currentContratLangue) {
		this.currentContratLangue = currentContratLangue;
	}


	public EOLangue getCurrentContratLangue() {
		return currentContratLangue;
	}


	public void setSelectedLangues(NSArray<EOLangue> selectedLangues) {
		this.selectedLangues = selectedLangues;
	}


	public NSArray<EOLangue> getSelectedLangues() {
		return selectedLangues;
	}


	public void setSelectedContratLangues(NSArray<EOLangue> selectedContratLangues) {
		this.selectedContratLangues = selectedContratLangues;
	}


	public NSArray<EOLangue> getSelectedContratLangues() {
		return selectedContratLangues;
	}


	public String getSuiviAdminDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.suivisAdministratifs().valueForKey(currentSuiviAdmin);
	}
	
	public WOActionResults ajouterLesLangues() {
		if (selectedLangues != null) {
			for (EOLangue l : selectedLangues) {
				if (!languesDuContrat().contains(l)) {
					EORepartContratLangue repartContratLangue = new EORepartContratLangue();
					edc().insertObject(repartContratLangue);
					repartContratLangue.setLangueRelationship(l);
					repartContratLangue.setContratRechercheRelationship(contratRecherche);
					repartContratLangue.setPersIdCreation(getUtilisateurPersId());
					repartContratLangue.setPersIdModification(getUtilisateurPersId());
					repartContratLangue.setDCreation(DateCtrl.getDateJour());
					repartContratLangue.setDModification(DateCtrl.getDateJour());
				}
			}
		}
		return doNothing();
	}
	
	public WOActionResults supprimerLesLangues() {
		if (selectedContratLangues != null) {
			for (EOLangue l : selectedContratLangues) {
				NSArray<EORepartContratLangue> repartContratLangues = contratRecherche.toRepartContratLangues();	
				for (int i = 0; i < repartContratLangues.count(); i++) {
					EORepartContratLangue repartContratLangue = repartContratLangues.get(i);
					if (repartContratLangue.langue().equals(l)) {
						contratRecherche.removeFromToRepartContratLanguesRelationship(repartContratLangue);
						edc().deleteObject(repartContratLangue);	
					}
				}
			}
		}
		return doNothing();
	}
	



	public void setCreateurDuDossier(EOIndividu createurDuDossier) {
		this.createurDuDossier = createurDuDossier;
	}


	public EOIndividu createurDuDossier() {
		if (createurDuDossier == null) {
			createurDuDossier = contratRecherche().getCreateurDuDossier();
		}
		return createurDuDossier;
	}
	
	
	public void setSelectedCreateurDuDossier(EOIndividu selectedCreateurDuDossier) {
		this.selectedCreateurDuDossier = selectedCreateurDuDossier;
	}




	public EOIndividu selectedCreateurDuDossier() {
		return selectedCreateurDuDossier;
	}

	public Boolean hasNotSelectedCreateurDuDossier() {
		return selectedCreateurDuDossier() == null;
	}
	
	
	public WOActionResults selectionnerCreateurDuDossier() {
		setSelectedCreateurDuDossier(null);
		return doNothing();
	}
	
	public WOActionResults validerSelectionnerCreateurDuDossier() {
//		try {
//			selectedCreateurDuDossier().setValidationEditingContext(personnesEditingContext());
//			selectedCreateurDuDossier().setPersIdModification(utilisateurPersId());
//			personnesEditingContext().saveChanges();
//			setCreateurDuDossier(selectedCreateurDuDossier().localInstanceIn(edc()));
			setCreateurDuDossier(selectedCreateurDuDossier());
			contratRecherche().setCreateurDuDossier(createurDuDossier(), getUtilisateurPersId());
			CktlAjaxWindow.close(context(), selectionnerCreateurDuDossierWindowId());
//		} catch (ValidationException e) {
//			personnesEditingContext().revert();
//			session().addSimpleErrorMessage("Erreur", e);
//		}
		return doNothing();
	}

	
	public WOActionResults annulerSelectionnerCreateurDuDossier() {
//		personnesEditingContext().revert();
		CktlAjaxWindow.close(context(), selectionnerCreateurDuDossierWindowId());
		return doNothing();
	}




	public WOActionResults onSelectCreateurDossier() {
//		try {
//			createurDuDossier().setValidationEditingContext(personnesEditingContext());
//			createurDuDossier().setPersIdModification(utilisateurPersId());
//			personnesEditingContext().saveChanges();
//	  contratRecherche().setCreateurDuDossier(createurDuDossier().localInstanceIn(edc()), utilisateurPersId());
			contratRecherche().setCreateurDuDossier(createurDuDossier(), getUtilisateurPersId());
//		} catch (ValidationException e) {
//			personnesEditingContext().revert();
//			session().addSimpleErrorMessage("Erreur", e);
//			setCreateurDuDossier(contratRecherche().getCreateurDuDossier());
//		}
		return doNothing();
	}



    public void setSelectedValidateurFinancier(EOIndividu selectedValidateurFinancier) {
		this.selectedValidateurFinancier = selectedValidateurFinancier;
	}

	public EOIndividu selectedValidateurFinancier() {
		return selectedValidateurFinancier;
	}
	
	
	public Boolean hasNotSelectedValidateurFinancier() {
		return selectedValidateurFinancier() == null;
	}
	
	
	public WOActionResults selectionnerValidateurFinancier() {
		setSelectedValidateurFinancier(null);
		return doNothing();
	}
	
	public WOActionResults validerSelectionnerValidateurFinancier() {
//		try {
//			selectedValidateurFinancier().setValidationEditingContext(personnesEditingContext());
//			selectedValidateurFinancier().setPersIdModification(utilisateurPersId());
//			personnesEditingContext().saveChanges();
//	  setValidateurFinancier(selectedValidateurFinancier().localInstanceIn(edc()));
//	  contratRecherche().setValidateurFinancier(validateurFinancier().localInstanceIn(edc()), utilisateurPersId());
			setValidateurFinancier(selectedValidateurFinancier());
			contratRecherche().setValidateurFinancier(validateurFinancier(), getUtilisateurPersId());
			CktlAjaxWindow.close(context(), selectionnerValidateurFinancierWindowId());
//		} catch (ValidationException e) {
//			personnesEditingContext().revert();
//			session().addSimpleErrorMessage("Erreur", e);
//		}
		return doNothing();
	}

	
	public WOActionResults annulerSelectionnerValidateurFinancier() {
//		personnesEditingContext().revert();
		CktlAjaxWindow.close(context(), selectionnerValidateurFinancierWindowId());
		return doNothing();
	}
	
	

	
	public WOActionResults onSelectValidateurJuridique() {
//		try {
//			validateurJuridique().setValidationEditingContext(personnesEditingContext());
//			validateurJuridique().setPersIdModification(utilisateurPersId());
//			personnesEditingContext().saveChanges();
//	  contratRecherche().setValidateurJuridique(validateurJuridique().localInstanceIn(edc()), utilisateurPersId());
			contratRecherche().setValidateurJuridique(validateurJuridique(), getUtilisateurPersId());
//		} catch (ValidationException e) {
//			personnesEditingContext().revert();
//			setValidateurJuridique(contratRecherche().getValidateurJuridique());
//			session().addSimpleErrorMessage("Erreur", e);
//		}
		return doNothing();
	}
	
	public void setValidateurJuridique(EOIndividu validateurJuridique) {
		this.validateurJuridique = validateurJuridique;
	}


	public EOIndividu validateurJuridique() {
		if (validateurJuridique == null) {
			validateurJuridique = contratRecherche().getValidateurJuridique();
		}
		return validateurJuridique;
	}
	

    public void setSelectedValidateurJuridique(EOIndividu selectedValidateurJuridique) {
		this.selectedValidateurJuridique = selectedValidateurJuridique;
	}

	public EOIndividu selectedValidateurJuridique() {
		return selectedValidateurJuridique;
	}
	
	
	public Boolean hasNotSelectedValidateurJuridique() {
		return selectedValidateurJuridique() == null;
	}
	
	
	public WOActionResults selectionnerValidateurJuridique() {
		setSelectedValidateurJuridique(null);
		return doNothing();
	}
	
	public WOActionResults validerSelectionnerValidateurJuridique() {
//		try {
//			selectedValidateurJuridique().setValidationEditingContext(personnesEditingContext());
//			selectedValidateurJuridique().setPersIdModification(utilisateurPersId());
//			personnesEditingContext().saveChanges();
//	  setValidateurJuridique(selectedValidateurJuridique().localInstanceIn(edc()));
//	  contratRecherche().setValidateurJuridique(validateurJuridique().localInstanceIn(edc()), utilisateurPersId());
			setValidateurJuridique(selectedValidateurJuridique());
			contratRecherche().setValidateurJuridique(validateurJuridique(), getUtilisateurPersId());
			CktlAjaxWindow.close(context(), selectionnerValidateurJuridiqueWindowId());
//		} catch (ValidationException e) {
//			personnesEditingContext().revert();
//			session().addSimpleErrorMessage("Erreur", e);
//		}
		return doNothing();
	}

	
	public WOActionResults annulerSelectionnerValidateurJuridique() {
//		personnesEditingContext().revert();
		CktlAjaxWindow.close(context(), selectionnerValidateurJuridiqueWindowId());
		return doNothing();
	}
	
	public void setValidateurFinancier(EOIndividu validateurFinancier) {
		this.validateurFinancier = validateurFinancier;
	}


	public EOIndividu validateurFinancier() {
		if (validateurFinancier == null) {
			validateurFinancier = contratRecherche().validateurFinancier();
		}
		return validateurFinancier;
	}

	public void setValidateurValo(EOIndividu validateurValo) {
		this.validateurValo = validateurValo;
	}


	public EOIndividu validateurValo() {
		if (validateurValo == null) {
			validateurValo = contratRecherche().validateurValo();
		}
		return validateurValo;
	}
	

    public void setSelectedValidateurValo(EOIndividu selectedValidateurValo) {
		this.selectedValidateurValo = selectedValidateurValo;
	}

	public EOIndividu selectedValidateurValo() {
		return selectedValidateurValo;
	}
	
	
	public Boolean hasNotSelectedValidateurValo() {
		return selectedValidateurValo() == null;
	}
	
	
	public WOActionResults selectionnerValidateurValo() {
		setSelectedValidateurValo(null);
		return doNothing();
	}
	
	public WOActionResults validerSelectionnerValidateurValo() {
//		try {
//			selectedValidateurValo().setValidationEditingContext(personnesEditingContext());
//			selectedValidateurValo().setPersIdModification(utilisateurPersId());
//			personnesEditingContext().saveChanges();
//	  setValidateurValo(selectedValidateurValo());
//	  contratRecherche().setValidateurValo(validateurValo().localInstanceIn(edc()), utilisateurPersId());
			setValidateurValo(selectedValidateurValo());
			contratRecherche().setValidateurValo(validateurValo(), getUtilisateurPersId());
			CktlAjaxWindow.close(context(), selectionnerValidateurValoWindowId());
//		} catch (ValidationException e) {
//			personnesEditingContext().revert();
//			session().addSimpleErrorMessage("Erreur", e); 
//		}
		return doNothing();
	}

	
	public WOActionResults annulerSelectionnerValidateurValo() {
//		personnesEditingContext().revert();
		CktlAjaxWindow.close(context(), selectionnerValidateurValoWindowId());
		return doNothing();
	}




	public Boolean notesFolded() {
		return notesFolded;
	}

	public void setNotesFolded(Boolean notesFolded) {
		this.notesFolded = notesFolded;
	}
	
	
	public WOActionResults foldNotes() {
		setNotesFolded(true);
		return doNothing();
	}
	
	public WOActionResults unFoldNotes() {
		setNotesFolded(false);
		return doNothing();
	}
	
	
	public String onClickFoldNotes() {
		return "Effect.BlindUp($('" + notesContainerId() + "'), {duration:0.5});";
	}
	
	public String onClickUnFoldNotes() {
		return "Effect.BlindDown($('" + notesContainerId() + "'), {duration:0.5});";
	}
	
	public Boolean getPeutDemanderValidation() {
		return contratRecherche().getPeutDemanderValidation(getUtilisateurPersId());
	}
	
	public Boolean getPeutEffectuerValidationFinanciere() {
		return contratRecherche().getPeutEffectuerValidationFinanciere(getUtilisateurPersId());
	}
	
	public Boolean getPeutEffectuerValidationValo() {
		return contratRecherche().getPeutEffectuerValidationValo(getUtilisateurPersId());
	}
	
	public Boolean getPeutEffectuerValidationJuridique() {
		return contratRecherche().getPeutEffectuerValidationJuridique(getUtilisateurPersId());
	}
	

	/**
	 * @return rien 
	 */
	public WOActionResults demanderLaValidationJuridique() {
		contratRecherche().demanderLaValidationJuridique(getUtilisateurPersId());
		session().addSimpleSuccessMessage("La demande a bien été prise en compte");
		return doNothing();
	}
	
	/**
	 * @return rien 
	 */
	public WOActionResults demanderLaValidationFinanciere() {
		contratRecherche().demanderLaValidationFinanciere(getUtilisateurPersId());
		session().addSimpleSuccessMessage("La demande a bien été prise en compte");
		return doNothing();
	}
	
	/**
	 * @return rien 
	 */
	public WOActionResults demanderLaValidationValo() {
		contratRecherche().demanderLaValidationValo(getUtilisateurPersId());
		session().addSimpleSuccessMessage("La demande a bien été prise en compte");
		return doNothing();
	}
	
	/**
	 * @return rien
	 */
	public WOActionResults effectuerValidationFinanciere() {
		
		contratRecherche().effectuerValidationFinanciere(getUtilisateurPersId());
		
		try {
			edc().saveChanges();
			contratRecherche().envoyerMailValidation(getUtilisateurPersId(), EOValidationContrat.T_VALIDATION_FINANCIERE);
			session().addSimpleSuccessMessage("La validation a bien été prise en compte");
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
			return doNothing();
		}
		
		
		return doNothing();
	}

	/**
	 * @return rien
	 */
	public WOActionResults effectuerValidationValo() {
		
		contratRecherche().effectuerValidationValo(getUtilisateurPersId());
		
		try {
			edc().saveChanges();
			contratRecherche().envoyerMailValidation(getUtilisateurPersId(), EOValidationContrat.T_VALIDATION_VALO);
			session().addSimpleSuccessMessage("La validation a bien été prise en compte");
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
			return doNothing();
		}
		
		
		return doNothing();
	}
	
	/**
	 * @return rien
	 */
	public WOActionResults effectuerValidationJuridique() {
		
		contratRecherche().effectuerValidationJuridique(getUtilisateurPersId());
		
		try {
			edc().saveChanges();
			contratRecherche().envoyerMailValidation(getUtilisateurPersId(), EOValidationContrat.T_VALIDATION_JURIDIQUE);
			session().addSimpleSuccessMessage("La validation a bien été prise en compte");
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
			return doNothing();
		}
		
		
		return doNothing();
	}

	public WOActionResults ajoutCommentaireValidationJuridique() {
		setTypeValidation(VALIDATION_JURIDIQUE);
		return doNothing();
	}
	
	public WOActionResults ajoutCommentaireValidationFinanciere() {
		setTypeValidation(VALIDATION_FINANCIERE);
		return doNothing();
	}

	
	public WOActionResults ajoutCommentaireValidationValorisation() {
		setTypeValidation(VALIDATION_VALORISATION);
		return doNothing();
	}
	
	/**
	 * Action lancée lors de la validation d'un nouveau commentaire
	 * Actualise le cadre avec le nouveau commentaire
	 * @return null
	 */
	public WOActionResults validerAjoutCommentaire() {
		String nouveauCommentaire = "\n\n";
		nouveauCommentaire += "[Ajout par " + getApplicationUser().getNomAndPrenom() + " le " + DateCtrl.dateToString(DateCtrl.now(), "%d/%m/%Y %H:%M") + "]";
		nouveauCommentaire += "\n";
		nouveauCommentaire += getCommentaireAAjouter();
		String containerToUpdate = "";
		switch (getTypeValidation()) {
		case VALIDATION_JURIDIQUE:
			contratRecherche().validationJuridique().setCommentaire(contratRecherche().validationJuridique().commentaire() + nouveauCommentaire);
			containerToUpdate = getValidationJuridiqueContainerId();
			break;
		case VALIDATION_FINANCIERE:
			contratRecherche().validationFinanciere().setCommentaire(contratRecherche().validationFinanciere().commentaire() + nouveauCommentaire);
			containerToUpdate = getValidationFinanciereContainerId();
			break;
		case VALIDATION_VALORISATION:
			contratRecherche().validationValorisation().setCommentaire(contratRecherche().validationValorisation().commentaire() + nouveauCommentaire);
			containerToUpdate = getValidationValoContainerId();
			break;
		default:
			doNothing();
		}
		setCommentaireAAjouter(null);
		try {
			edc().saveChanges();
			AjaxUpdateContainer.updateContainerWithID(containerToUpdate, context());
			CktlAjaxWindow.close(context(), ajoutCommentaireWindowId());
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			edc().revert();
		}
		return doNothing();
	}
	
	public WOActionResults annulerAjoutCommentaire() {
		setCommentaireAAjouter(null);
		CktlAjaxWindow.close(context(), ajoutCommentaireWindowId());
		return doNothing();
	}

	public String getCommentaireAAjouter() {
		return commentaireAAjouter;
	}

	public void setCommentaireAAjouter(String commentaireAAjouter) {
		this.commentaireAAjouter = commentaireAAjouter;
	}

	public int getTypeValidation() {
		return typeValidation;
	}

	public void setTypeValidation(int typeValidation) {
		this.typeValidation = typeValidation;
	}
	
}