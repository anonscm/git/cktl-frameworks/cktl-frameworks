package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class ContratRechercheResume extends AFwkCktlSangriaComponent {
	
	
    public final static String BINDING_contratRecherche = "contratRecherche";

    private EOContratRecherche contratRecherche;
    
    private EOStructure currentLaboratoirePorteur;
	private String currentResponsableScientifiqueAvecLabo;
    
    
    public ContratRechercheResume(WOContext context) {
        super(context);
    }
    
	
    public EOContratRecherche contratRecherche() {
    	if (hasBinding(BINDING_contratRecherche)) {
    		contratRecherche = (EOContratRecherche) valueForBinding(BINDING_contratRecherche);
    	}
    	return contratRecherche;
    }


	public void setCurrentLaboratoirePorteur(EOStructure currentLaboratoirePorteur) {
		this.currentLaboratoirePorteur = currentLaboratoirePorteur;
	}


	public EOStructure getCurrentLaboratoirePorteur() {
		return currentLaboratoirePorteur;
	}
	/*
	 * Creation d'un liste contenant les responsables scientifiques avec leur labos
	 */
	public NSArray<String> responsablesScientifiquesAvecLabos() {
		NSArray<String> resultats = new NSMutableArray<String>();
		
		NSArray<EOStructure> labos = contratRecherche().structureRechercheConcernees();
		for (EOStructure labo : labos) {
			NSArray<EOIndividu> responsablesScientifiques = contratRecherche().responsablesScientifiquesPourLaboratoire(labo);
			for (EOIndividu responsable : responsablesScientifiques) {
				resultats.add(responsable.getNomAndPrenom() + " pour " + labo.lcStructure());
			}
		} 
		
		return resultats;
	}


	public void setCurrentResponsableScientifiqueAvecLabo(
			String currentResponsableScientifiqueAvecLabo) {
		this.currentResponsableScientifiqueAvecLabo = currentResponsableScientifiqueAvecLabo;
	}


	public String getCurrentResponsableScientifiqueAvecLabo() {
		return currentResponsableScientifiqueAvecLabo;
	}
    
}