package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchepartiepiui;

import org.cocktail.fwkcktldroitsutils.common.finders.EOUtilisateurFinder;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class ContratRecherchePartiePiUIM extends AFwkCktlSangriaComponent {
	
	private EOContratRecherche contratRecherche;
	
	public final static String BINDING_contratRecherche = "contratRecherche";
	
	private String currentTypeRedactionContrat;
	private String currentTypeRedactionClausesPi;
	private String currentProprieteDesResultats;

	private EOIndividu currentDestinataireAlerte;
	
	private String typeSuiviValoCourant;
	
	private NSArray<String> selectedApportsDrvDisponibles;
	private NSArray<String> selectedApportsDrvContrat;
	private String currentApportDrv;
	
	private NSArray<String> selectedApportsDajDisponibles;
	private NSArray<String> selectedApportsDajContrat;
	private String currentApportDaj;

	private String currentExploitationDesResultats;

	private NSArray<String> selectedTypesDeResultatsDisponibles;

	private NSArray<String> selectedTypesDeResultatsContrat;

	private String currentTypeDeResultats;

	private EOIndividu destinataireAlerteAAjouter;
	
	private EOEvenement evenement;
	
    public ContratRecherchePartiePiUIM(WOContext context) {
        super(context);
    }
    
   // Gestion des bindings
	
    public EOContratRecherche contratRecherche() {
    		if(hasBinding(BINDING_contratRecherche))
    			contratRecherche = (EOContratRecherche) valueForBinding(BINDING_contratRecherche);
    	return contratRecherche;
    	
    }
    
    // identification des composants
    
    public String apportDrvContainerId() {
    	return getComponentId() + "ApportDrvContainer";
    }
    
    public String apportDajContainerId() {
    	return getComponentId() + "ApportDajContainer";
    }
    
    public String typesResultatsContainerId() {
    	return getComponentId() + "TypesResultatsContainer";
    }
    
    public String suiviValoEvtUpdateContainerId() {
    	return getComponentId() + "SuiviValoEvtUpdateContainerId";
    }
    
    
    public NSArray<String> typesRedactionContrat() {
    	return EOContratRecherche.Nomenclatures.typesRedactionContrat().allKeys();
    }

    public NSArray<String> typesRedactionClausesPi() {
    	return EOContratRecherche.Nomenclatures.typesRedactionClausesPi().allKeys();
    }
    
    public NSArray<String> typesProprieteResultats() {
    	return EOContratRecherche.Nomenclatures.typesProprieteResultats().allKeys();
    }
    
    public NSArray<String> typesExploitationResultats() {
    	return EOContratRecherche.Nomenclatures.typesExploitationResultats().allKeys();
    }
    
    public NSArray<String> typeSuiviValo() {
    	return EOContratRecherche.Nomenclatures.typeSuiviValo().allKeys();
    }
    

	public void setCurrentTypeRedactionContrat(
			String currentTypeRedactionContrat) {
		this.currentTypeRedactionContrat = currentTypeRedactionContrat;
	}

	public String getCurrentTypeRedactionContrat() {
		return currentTypeRedactionContrat;
	}

	public void setCurrentTypeRedactionClausesPi(
			String currentTypeRedactionClausesPi) {
		this.currentTypeRedactionClausesPi = currentTypeRedactionClausesPi;
	}

	public String getCurrentTypeRedactionClausesPi() {
		return currentTypeRedactionClausesPi;
	}
	
	public String currentTypeRedactionContratDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesRedactionContrat().valueForKey(currentTypeRedactionContrat);
	}
	
	public String currentTypeRedactionClausesPiDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesRedactionClausesPi().valueForKey(currentTypeRedactionClausesPi);
	}
	
	// GESTION DES APPORTS DRV
	
	public NSArray<String> apportsContratsSurDrvDisponibles() {
		NSMutableArray<String> apports = EOContratRecherche.Nomenclatures.typesApportDrvContrat().allKeys().mutableClone();
		apports.removeAll(apportsContratsSurDrvContrat());
		return apports;
	}
	
	public NSArray<String> apportsContratsSurDrvContrat() {
		return ((NSMutableArray<String>) contratRecherche().toRepartApportDrvContrats().valueForKey(EORepartApportDrvCont.APPORT_CONT_DRV_KEY)).mutableClone();
	}

	public void setSelectedApportsDrvDisponibles(
			NSArray<String> selectedApportsDrvDisponibles) {
		this.selectedApportsDrvDisponibles = selectedApportsDrvDisponibles;
	}

	public NSArray<String> getSelectedApportsDrvDisponibles() {
		return selectedApportsDrvDisponibles;
	}

	public void setSelectedApportsDrvContrat(
			NSArray<String> selectedApportsDrvContrat) {
		this.selectedApportsDrvContrat = selectedApportsDrvContrat;
	}

	public NSArray<String> getSelectedApportsDrvContrat() {
		return selectedApportsDrvContrat;
	}
	
	public void setCurrentApportDrv(String currentApportDrv) {
		this.currentApportDrv = currentApportDrv;
	}

	public String getCurrentApportDrv() {
		return currentApportDrv;
	}
	
	public String currentApportDrvDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesApportDrvContrat().valueForKey(currentApportDrv);
	}
	
	public WOActionResults ajouterApportDrv() {
		if(selectedApportsDrvDisponibles != null)
			for(String apport : selectedApportsDrvDisponibles) {
				if(!apportsContratsSurDrvContrat().contains(apport)) {
					EORepartApportDrvCont repartApportDrv = new EORepartApportDrvCont();
					edc().insertObject(repartApportDrv);
					repartApportDrv.setApportContDrv(apport);
					repartApportDrv.setContratRelationship(contratRecherche);
					repartApportDrv.setPersIdCreation(getUtilisateurPersId());
					repartApportDrv.setPersIdModification(getUtilisateurPersId());
					repartApportDrv.setDCreation(MyDateCtrl.getDateJour());
					repartApportDrv.setDModification(MyDateCtrl.getDateJour());
				}
			}
		return doNothing();
	}
	
	public WOActionResults supprimerApportDrv() {
		if(selectedApportsDrvContrat != null)
			for(String apport : selectedApportsDrvContrat) {
				NSArray<EORepartApportDrvCont> repartApportsDrv = contratRecherche().toRepartApportDrvContrats();	
				for(int i = 0; i < repartApportsDrv.count(); i++) {
					EORepartApportDrvCont repartApportDrv = repartApportsDrv.get(i);
					if(repartApportDrv.apportContDrv().equals(apport)) {
						contratRecherche().removeFromToRepartApportDrvContratsRelationship(repartApportDrv);
						edc().deleteObject(repartApportDrv);	
					}
				}
			}
		return doNothing();
	}
	
	public Boolean hasAugmentationMontant() {
		return apportsContratsSurDrvContrat().contains("augm_mf");
	}
	
	
	// GESTION DES APPORTS DAJ

	public NSArray<String> apportsContratsSurDajDisponibles() {
		NSMutableArray<String> apports = EOContratRecherche.Nomenclatures.typesApportDajContrat().allKeys().mutableClone();
		apports.removeAll(apportsContratsSurDajContrat());
		return apports;
	}
	
	public NSArray<String> apportsContratsSurDajContrat() {
		return ((NSMutableArray<String>) contratRecherche().toRepartApportDajContrats().valueForKey(EORepartApportDajCont.APPORT_CONT_DAJ_KEY)).mutableClone();
	}

	public void setSelectedApportsDajDisponibles(
			NSArray<String> selectedApportsDajDisponibles) {
		this.selectedApportsDajDisponibles = selectedApportsDajDisponibles;
	}

	public NSArray<String> getSelectedApportsDajDisponibles() {
		return selectedApportsDajDisponibles;
	}

	public void setSelectedApportsDajContrat(
			NSArray<String> selectedApportsDajContrat) {
		this.selectedApportsDajContrat = selectedApportsDajContrat;
	}

	public NSArray<String> getSelectedApportsDajContrat() {
		return selectedApportsDajContrat;
	}
	
	public void setCurrentApportDaj(String currentApportDaj) {
		this.currentApportDaj = currentApportDaj;
	}

	public String getCurrentApportDaj() {
		return currentApportDaj;
	}

	public String currentApportDajDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesApportDajContrat().valueForKey(currentApportDaj);
	}
	
	public WOActionResults ajouterApportDaj() {
		if(selectedApportsDajDisponibles != null)
			for(String apport : selectedApportsDajDisponibles) {
				if(!apportsContratsSurDajContrat().contains(apport)) {
					EORepartApportDajCont repartApportDaj = new EORepartApportDajCont();
					edc().insertObject(repartApportDaj);
					repartApportDaj.setApportContDaj(apport);
					repartApportDaj.setContratRelationship(contratRecherche);
					repartApportDaj.setPersIdCreation(getUtilisateurPersId());
					repartApportDaj.setPersIdModification(getUtilisateurPersId());
					repartApportDaj.setDCreation(MyDateCtrl.getDateJour());
					repartApportDaj.setDModification(MyDateCtrl.getDateJour());
				}
			}
		return doNothing();
	}
	
	public WOActionResults supprimerApportDaj() {
		if(selectedApportsDajContrat != null)
			for(String apport : selectedApportsDajContrat) {
				NSArray<EORepartApportDajCont> repartApportsDaj = contratRecherche().toRepartApportDajContrats();	
				for(int i = 0; i < repartApportsDaj.count(); i++) {
					EORepartApportDajCont repartApportDaj = repartApportsDaj.get(i);
					if(repartApportDaj.apportContDaj().equals(apport)) {
						contratRecherche().removeFromToRepartApportDajContratsRelationship(repartApportDaj);
						edc().deleteObject(repartApportDaj);	
					}
				}
			}
		return doNothing();
	}
	
	

	// PROPRIETE DES RESULTATS

	public String getCurrentProprieteDesResultats() {
		return currentProprieteDesResultats;
	}

	public void setCurrentProprieteDesResultats(String currentProprieteDesResultats) {
		this.currentProprieteDesResultats = currentProprieteDesResultats;
	} 
	
	public String getCurrentProprieteDesResultatsDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesProprieteResultats().valueForKey(currentProprieteDesResultats);
	}

	// EXPLOITATION DES RESULTATS

	public String getCurrentExploitationDesResultats() {
		return currentExploitationDesResultats;
	}

	public void setCurrentExploitationDesResultats(String currentExploitationDesResultats) {
		this.currentExploitationDesResultats = currentExploitationDesResultats;
	} 
	
	public String getCurrentExploitationDesResultatsDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesExploitationResultats().valueForKey(currentExploitationDesResultats);
	}
	
	// GESTION DES TYPES DE RESULTATS

	public NSArray<String> typesDeResultatsDisponibles() {
		NSMutableArray<String> types = EOContratRecherche.Nomenclatures.typesDeResultats().allKeys().mutableClone();
		types.removeAll(typesDeResultatsContrat());
		return types;
	}
	
	public NSArray<String> typesDeResultatsContrat() {
		return ((NSMutableArray<String>) contratRecherche().toRepartTypeResultatsContrats().valueForKey(EORepartTypeResultatsContrat.TYPE_RESULTATS_CONTRAT_KEY)).mutableClone();
	}

	public void setSelectedTypesDeResultatsDisponibles(
			NSArray<String> selectedTypesDeResultatsDisponibles) {
		this.selectedTypesDeResultatsDisponibles = selectedTypesDeResultatsDisponibles;
	}

	public NSArray<String> getSelectedTypesDeResultatsDisponibles() {
		return selectedTypesDeResultatsDisponibles;
	}

	public void setSelectedTypesDeResultatsContrat(
			NSArray<String> selectedTypesDeResultatsContrat) {
		this.selectedTypesDeResultatsContrat = selectedTypesDeResultatsContrat;
	}

	public NSArray<String> getSelectedTypesDeResultatsContrat() {
		return selectedTypesDeResultatsContrat;
	}
	
	public void setCurrentTypeDeResultats(String currentTypeDeResultats) {
		this.currentTypeDeResultats = currentTypeDeResultats;
	}

	public String getCurrentTypeDeResultats() {
		return currentTypeDeResultats;
	}

	public String currentTypeDeResultatsDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typesDeResultats().valueForKey(currentTypeDeResultats);
	}
	
	public WOActionResults ajouterTypesDeResultats() {
		if(selectedTypesDeResultatsDisponibles != null)
			for(String type : selectedTypesDeResultatsDisponibles) {
				if(!typesDeResultatsContrat().contains(type)) {
					EORepartTypeResultatsContrat repartTypeResultats = new EORepartTypeResultatsContrat();
					edc().insertObject(repartTypeResultats);
					repartTypeResultats.setTypeResultatsContrat(type);
					repartTypeResultats.setContratRelationship(contratRecherche);
					repartTypeResultats.setPersIdCreation(getUtilisateurPersId());
					repartTypeResultats.setPersIdModification(getUtilisateurPersId());
					repartTypeResultats.setDCreation(MyDateCtrl.getDateJour());
					repartTypeResultats.setDModification(MyDateCtrl.getDateJour());
				}
			}
		return doNothing();
	}
	
	public WOActionResults supprimerTypesDeResultats() {
		if(selectedTypesDeResultatsContrat != null)
			for(String type : selectedTypesDeResultatsContrat) {
				NSArray<EORepartTypeResultatsContrat> repartTypeResultatsContrats = contratRecherche().toRepartTypeResultatsContrats();	
				for(int i = 0; i < repartTypeResultatsContrats.count(); i++) {
					EORepartTypeResultatsContrat repartTypeResultatsContrat = repartTypeResultatsContrats.get(i);
					if(repartTypeResultatsContrat.typeResultatsContrat().equals(type)) {
						contratRecherche().removeFromToRepartTypeResultatsContratsRelationship(repartTypeResultatsContrat);
						edc().deleteObject(repartTypeResultatsContrat);	
					}
				}
			}
		return doNothing();
	}
	
	// SUIVI VALO
	
	public void setTypeSuiviValoCourant(String typeSuiviValoCourant) {
		this.typeSuiviValoCourant = typeSuiviValoCourant;
	}

	public String getTypeSuiviValoCourant() {
		return typeSuiviValoCourant;
	}
	

	
	public String suiviValoDisplayString() {
		return (String) EOContratRecherche.Nomenclatures.typeSuiviValo().valueForKey(typeSuiviValoCourant); 
	}
		
	/*******************************************/
	/*** Gestion des destinataires alerte ***/
	
	public EOUtilisateur utilisateur() {
		return EOUtilisateurFinder.fecthUtilisateurByPersId(edc(), getUtilisateurPersId());
	}
	
	public WOActionResults creerEvtAlerteSuivi() {
		 
		if(contratRecherche().suiviValoEvenement() == null) {
			evenement = FwkCktlEvenementUtil.creerNouvelEvenement(EOUtilisateurFinder.fecthUtilisateurByPersId(edc(), getUtilisateurPersId()).toPersonne(), edc());
			evenement.setPersidModif(getUtilisateurPersId());
			contratRecherche().setSuiviValoEvenementRelationship(evenement);
		}
		
		return doNothing();
	}
	
}