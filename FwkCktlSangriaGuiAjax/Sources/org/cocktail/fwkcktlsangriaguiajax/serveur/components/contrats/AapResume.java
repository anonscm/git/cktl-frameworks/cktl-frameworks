package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEOControlUtilities;

public class AapResume extends AFwkCktlSangriaComponent {
	
	
    public final static String BINDING_aap = "aap";

    private EOAap aap;
    
    private EOStructure currentLaboratoirePorteur;
	private String currentResponsableScientifiqueAvecLabo;

	private EOStructure currentPartenaire;
    
    
    public AapResume(WOContext context) {
        super(context);
    }
    
	
    public EOAap aap() {
    	if(hasBinding(BINDING_aap))
    		aap = (EOAap) valueForBinding(BINDING_aap);
    	return aap;
    }


	public void setCurrentLaboratoirePorteur(EOStructure currentLaboratoirePorteur) {
		this.currentLaboratoirePorteur = currentLaboratoirePorteur;
	}


	public EOStructure getCurrentLaboratoirePorteur() {
		return currentLaboratoirePorteur;
	}
	/*
	 * Création d'un liste contenant les responsables scientifiques avec leur labos
	 */
	public NSArray<String> responsablesScientifiquesAvecLabos() {
		NSArray<String> resultats = new NSMutableArray<String>();
		
		NSArray<EOStructure> labos = aap().getStructuresRecherchesConcernees();
		for(EOStructure labo : labos) {
			NSArray<EOIndividu> responsablesScientifiques = aap().responsablesScientifiquesPourStructureRecherche(labo);
			for(EOIndividu responsable : responsablesScientifiques) {
				resultats.add(responsable.getNomAndPrenom() + " pour " + labo.lcStructure());
			}
		} 
		
		return resultats;
	}


	public void setCurrentResponsableScientifiqueAvecLabo(
			String currentResponsableScientifiqueAvecLabo) {
		this.currentResponsableScientifiqueAvecLabo = currentResponsableScientifiqueAvecLabo;
	}


	public String getCurrentResponsableScientifiqueAvecLabo() {
		return currentResponsableScientifiqueAvecLabo;
	}
    
	public EOStructure getCurrentPartenaire() {
		return currentPartenaire;
	}
	
	public void setCurrentPartenaire(EOStructure currentPartenaire) {
		this.currentPartenaire = currentPartenaire;
	}
	
	public boolean isLaboratoirePrincipal() {
		return ERXEOControlUtilities.eoEquals(getCurrentLaboratoirePorteur(), aap().contrat().centreResponsabilite());
	}
	
}