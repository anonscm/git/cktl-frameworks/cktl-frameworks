package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchenotes;

import org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche.UniteRechercheComponentUI;

import com.webobjects.appserver.WOContext;

public class ContratRechercheNotesUI extends UniteRechercheComponentUI {
	private static final long serialVersionUID = 6965666832274086341L;

	public ContratRechercheNotesUI(WOContext context) {
        super(context);
    }
}