package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats;

import org.cocktail.cocowork.server.metier.convention.AvenantEvenement;
import org.cocktail.cocowork.server.metier.convention.ModeGestion;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementType;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSBundle;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.components.ERXComponent;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXDictionaryUtilities;
import er.extensions.foundation.ERXStringUtilities;

public class ContratRechercheExportExcelValo extends ERXComponent {

	private static final String NOTE_EVT_ID_INTERNE = "NOTE";

	
	private NSArray<EOContratRecherche> contrats;
	private EOContratRecherche currentContrat;
	
	private NSArray<IPersonne> partenaires;
	private IPersonne currentPartenaire;
	
	private NSArray<EOStructure> labos;
	private EOStructure currentLabo;
	
	
    public ContratRechercheExportExcelValo(WOContext context) {
        super(context);
    }

    public NSDictionary<?,?> excelStylesData() {
    	NSDictionary<?,?> dict = ERXDictionaryUtilities.dictionaryFromPropertyList("ExcelStyles", NSBundle.mainBundle());
    	return dict;
    }
    
	public NSArray<EOContratRecherche> contrats() {
		return contrats;
	}

	public void setContrats(NSArray<EOContratRecherche> contrats) {
		this.contrats = contrats;
	}

	public EOContratRecherche currentContrat() {
		return currentContrat;
	}

	public void setCurrentContrat(EOContratRecherche currentContrat) {
		this.currentContrat = currentContrat;
	}

	public NSArray<IPersonne> partenaires() {
		partenaires = currentContrat().partenaires();
		return partenaires;
	}

	public void setPartenaires(NSArray<IPersonne> partenaires) {
		this.partenaires = partenaires;
	}

	public IPersonne currentPartenaire() {
		return currentPartenaire;
	}

	public void setCurrentPartenaire(IPersonne currentPartenaire) {
		this.currentPartenaire = currentPartenaire;
	}

	public NSArray<EOStructure> labos() {
		labos = currentContrat().structureRechercheConcernees();
		return labos;
	}

	public void setLabos(NSArray<EOStructure> labos) {
		this.labos = labos;
	}

	public EOStructure currentLabo() {
		return currentLabo;
	}

	public void setCurrentLabo(EOStructure currentLabo) {
		this.currentLabo = currentLabo;
	}
	
	
	public String listeLaboratoires() {
		NSArray<String> libellesLaboratoires = (NSArray<String>) labos().valueForKey(EOStructure.LC_STRUCTURE_KEY);
		return libellesLaboratoires.componentsJoinedByString(" / ");
	}
	
	public String listePartenaires() {
		NSArray<String> libellesPartenaires = (NSArray<String>) partenaires().valueForKey(IPersonne.NOM_PRENOM_AFFICHAGE_KEY);
		return libellesPartenaires.componentsJoinedByString(" / ");
	}
	
	public String responsablesScientifiques() {
		NSArray<String> nomsPrenoms = (NSArray<String>) currentContrat().responsablesScientifiques().valueForKey(EOIndividu.NOM_PRENOM_AFFICHAGE_KEY);
		return nomsPrenoms.componentsJoinedByString(" / ");
	}
	
	public String suiviAdministratif() {
		return (String) EOContratRecherche.Nomenclatures.suivisAdministratifs().valueForKey(currentContrat().suiviAdministratif());
	}
	
	public String derniereNote() {
		String evtTypeIdKeyPath = ERXQ.keyPath(AvenantEvenement.EVENEMENT_KEY, EOEvenement.TYPE_KEY, EOEvenementType.ID_INTERNE_KEY);
		EOQualifier qualifier = ERXQ.equals(evtTypeIdKeyPath, NOTE_EVT_ID_INTERNE);
		
		NSArray<EOEvenement> notes = (NSArray<EOEvenement>) currentContrat().contrat().avenantZero().avenantEvenements(qualifier, ERXS.descs(AvenantEvenement.EVENEMENT_KEY + "." + EOEvenement.DATE_CREATION_KEY), false)
				.valueForKey(AvenantEvenement.EVENEMENT_KEY);
		
		if (notes.isEmpty()) {
			return "";
		} else {
			return notes.get(0).objet();
		}

	}
	

  public String currentEtablissementGestionnaire() {
    if (ERXStringUtilities.stringEqualsString(currentContrat().contrat().modeDeGestion().mgLibelleCourt(), ModeGestion.MODE_GESTION_SIF)) {
      return "";
    } else  {
      return currentContrat().contrat().etablissement().lcStructure();
    }
    
  }
  
	public String proprieteDesResultats() {
		if (currentContrat().proprieteDesResultats() != null) {
			return (String) EOContratRecherche.Nomenclatures.typesProprieteResultats().valueForKey(currentContrat().proprieteDesResultats());
		} else {
			return null;
		}
	}
	
	public String exploitationDesResultats() {
		if (currentContrat().exploitationDesResultats() != null) {
			return (String) EOContratRecherche.Nomenclatures.typesExploitationResultats().valueForKey(currentContrat().exploitationDesResultats());
		} else {
			return null;
		}
	}
	
	public String typeResultatsContrats() {
		NSArray<String> libelles = new NSMutableArray<String>();
		for (EORepartTypeResultatsContrat typeResultatsContrat : currentContrat().toRepartTypeResultatsContrats()) {
			libelles.add((String) EOContratRecherche.Nomenclatures.typesDeResultats().valueForKey(typeResultatsContrat.typeResultatsContrat()));
		} 
		return libelles.componentsJoinedByString(" / ");
	}

	public Boolean isCurrentContratSansIncidenceFinanciere() {
		return currentContrat().isContratSansIncidenceFinanciere();
	}
	
	
}