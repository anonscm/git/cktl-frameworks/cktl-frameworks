package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchedocuments;

import com.webobjects.appserver.WOContext;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche.UniteRechercheComponentUI;

public class ContratRechercheDocumentsUI extends UniteRechercheComponentUI {
	private static final long serialVersionUID = -305109188922795075L;

	public ContratRechercheDocumentsUI(WOContext context) {
        super(context);
    }
}