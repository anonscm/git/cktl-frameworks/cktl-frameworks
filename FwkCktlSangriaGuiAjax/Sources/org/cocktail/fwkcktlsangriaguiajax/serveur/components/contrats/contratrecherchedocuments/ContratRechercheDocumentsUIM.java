package org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.contratrecherchedocuments;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.AvenantDocument;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryAvenant;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche.UniteRechercheComponentUI;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;

public class ContratRechercheDocumentsUIM extends UniteRechercheComponentUI {
	
	public final static String BINDING_contratRecherche = "contratRecherche";
	public final static String BINDING_readOnly = "readOnly";
	
	private AvenantDocument selectedAvenantDocument;
	private EODocument nouveauDocument;
	
	private ERXDisplayGroup<AvenantDocument> dgDocuments;	
	private EOArrayDataSource documentDataSource = null;
	private FactoryAvenant factoryAvenant;
	private EOContratRecherche contratRecherche;
	
	private EOEditingContext _editingContext = null;
	
	

	
    public ContratRechercheDocumentsUIM(WOContext context) {
        super(context);
    }

    @Override
    public EOEditingContext edc() {
    	if (_editingContext == null) {
    		_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
    	}
    	return _editingContext;
    }
	    
    public String ajouterDocumentWindowId() {
    	return getComponentId() + "_ajouterDocumentWindow";
    }
   
    public String documentsContainerId() {
    	return getComponentId() + "_documentsContainer";
    }
    
    
		
	public void setContratRecherche(EOContratRecherche contratRecherche) {
		this.contratRecherche = contratRecherche;
	}

	public EOContratRecherche contratRecherche() {
    	if(hasBinding(BINDING_contratRecherche)) {
    		EOContratRecherche _contratRecherche = (EOContratRecherche) valueForBinding(BINDING_contratRecherche);
    		contratRecherche  = _contratRecherche.localInstanceIn(edc());
    	}
    	return contratRecherche;	
    }

	public void setFactoryAvenant(FactoryAvenant factoryAvenant) {
		this.factoryAvenant = factoryAvenant;
	}

	public FactoryAvenant factoryAvenant() {
		if(factoryAvenant == null) {
			factoryAvenant = new FactoryAvenant(edc(), false);
		}
		return factoryAvenant;
	}
	
	
	public EOArrayDataSource documentDataSource() {
		if(documentDataSource == null) {
			documentDataSource = new EOArrayDataSource(
					EOClassDescription.classDescriptionForClass(AvenantDocument.class),
					edc()
				);
		}
		return documentDataSource;
	}
	
	
	public ERXDisplayGroup<AvenantDocument> dgDocuments() {
		
 		if (dgDocuments == null) {
 			
 				dgDocuments = new ERXDisplayGroup<AvenantDocument>();

		        NSMutableArray<AvenantDocument> avenantDocuments = new NSMutableArray<AvenantDocument>();
		        for(Avenant avenant : (NSArray<Avenant>) contratRecherche().contrat().avenantsDontInitialNonSupprimes()) {
					avenantDocuments.addObjectsFromArray(avenant.avenantDocuments());
				}
		        documentDataSource().setArray(avenantDocuments);
		        dgDocuments.setDataSource(documentDataSource());
		        dgDocuments.fetch();

 		}
	
		return dgDocuments;
	}


	public WOActionResults validerAjout() {
			ApplicationUser applicationUser = new ApplicationUser(edc(), getUtilisateurPersId());
	        AvenantDocument _avenantDocument = factoryAvenant().ajouterDocument(edc(), applicationUser, contratRecherche().contrat().avenantZero(), nouveauDocument());
	        try {
	            edc().saveChanges();
	            documentDataSource().insertObject(_avenantDocument);
	            dgDocuments().fetch();
	            CktlAjaxWindow.close(context());
	        } catch (ValidationException e) {
				session().addSimpleErrorMessage("Erreur", e);
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
	        return doNothing();
	}
	
	public WOActionResults annulerAjout() {
		edc().revert();
		CktlAjaxWindow.close(context());
		return doNothing();
	}

	public boolean isSupprimerUnDocumentEnabled() {
		return !isReadOnly() && dgDocuments().selectedObject() != null;
	}
	
	public WOActionResults supprimerUnDocument() {
		AvenantDocument leDocumentAArchiver = (AvenantDocument) dgDocuments().selectedObject();

		if (leDocumentAArchiver != null) {
			try {
				ApplicationUser applicationUser = new ApplicationUser(edc(), getUtilisateurPersId());
				documentDataSource().deleteObject(leDocumentAArchiver);
				
				factoryAvenant().supprimerDocument(edc(), leDocumentAArchiver, applicationUser, null);
				edc().saveChanges();
			} catch (ValidationException e) {
				edc().revert();
				session().addSimpleErrorMessage("Erreur", e);
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
		}
		
		return null;
	}

	
	public AvenantDocument selectedAvenantDocument() {
		return selectedAvenantDocument;
	}

	public void setSelectedAvenantDocument(AvenantDocument selectedAvenantDocument) {
		this.selectedAvenantDocument = selectedAvenantDocument;
	}

	public EODocument nouveauDocument() {
		return nouveauDocument;
	}

	public void setNouveauDocument(EODocument nouveauDocument) {
		this.nouveauDocument = nouveauDocument;
	}
	
	public Boolean isReadOnly() {
		return booleanValueForBinding(BINDING_readOnly, false);
	}

}