package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;

import com.webobjects.appserver.WOContext;

public abstract class ModuleProjetScientifique extends ModuleFwkCktlRecherche {

	
	public static final String BINDING_projetScientifique = "projetScientifique";
	
	private EOProjetScientifique projetScientifique;
	
	public ModuleProjetScientifique(WOContext context) {
		super(context);
	}

	
	public EOProjetScientifique projetScientifique() {
		if(projetScientifique == null)
			if(hasBinding(BINDING_projetScientifique)) {
				projetScientifique = (EOProjetScientifique) valueForBinding(BINDING_projetScientifique);
			}
		return projetScientifique;
	}
	
}
