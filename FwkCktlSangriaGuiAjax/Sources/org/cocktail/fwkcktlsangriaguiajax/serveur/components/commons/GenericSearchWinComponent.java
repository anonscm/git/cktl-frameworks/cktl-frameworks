package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;

import er.ajax.AjaxUtils;

public class GenericSearchWinComponent extends AFwkCktlSangriaComponent implements ISearchWin {

	
	public static final String ACTION_OK = "Ok";
	private EOGenericRecord selection;
	private String winId;
	private String containerOnCloseID;
	private String lastAction;
	private EOEditingContext edc;
	
	public GenericSearchWinComponent(WOContext context) {
		super(context);
	}
	
	@Override
	public EOEditingContext edc() {
		return edc;
	}

	public void setEditingContext(EOEditingContext edc) {
		this.edc = edc;
	}


	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		AjaxUtils.addStylesheetResourceInHead(context, response, frameworkName(), "styles/fwkcktlrechercheguiajax.css");
		setLastAction(null);
	}

	public WOActionResults onOk() {
		setLastAction(ACTION_OK);
		return null;
	}

	public WOActionResults onCancel() {
		setSelection(null);
		return null;
	}

	public String jsOnCancel() {
		String cmd = "function() {parent.closeWindow('" + getWinId() + "', null);}";
		return cmd;
	}

	public String jsOnOk() {
		String cmd = "function() {parent.closeWindow('" + getWinId() + "', null);}";
		return cmd;
	}

	public EOGenericRecord getSelection() {
		return selection;
	}

	public void setSelection(EOGenericRecord selection) {
		this.selection = selection;
	}

	public String getWinId() {
		return winId;
	}

	public void setWinId(String winId) {
		this.winId = winId;
	}

	public String getContainerOnCloseID() {
		return containerOnCloseID;
	}

	public void setContainerOnCloseID(String containerOnCloseID) {
		this.containerOnCloseID = containerOnCloseID;
	}

	public String getLastAction() {
		return lastAction;
	}

	public void setLastAction(String lastAction) {
		this.lastAction = lastAction;
	}
	

}
