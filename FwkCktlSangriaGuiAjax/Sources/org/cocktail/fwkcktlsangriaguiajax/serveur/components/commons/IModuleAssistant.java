package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

public interface IModuleAssistant {
	public abstract void onPrecedent();
	public abstract boolean isPrecedentDisabled();
	public abstract void onSuivant();
	public abstract boolean isSuivantDisabled();
	public abstract boolean valider();
	public abstract boolean isTerminerDisabled();
}
