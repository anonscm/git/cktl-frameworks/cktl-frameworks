package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;

import com.webobjects.appserver.WOContext;

public abstract class ModuleContratRecherche extends ModuleFwkCktlRecherche {

	
	public static final String BINDING_contratRecherche = "contratRecherche";
	
	private EOContratRecherche contratRecherche;
	
	public ModuleContratRecherche(WOContext context) {
		super(context);
	}

	
	public EOContratRecherche contratRecherche() {
		if(contratRecherche == null)
			if(hasBinding(BINDING_contratRecherche)) {
				contratRecherche = (EOContratRecherche) valueForBinding(BINDING_contratRecherche);
			}
		return contratRecherche;
	}
	
}
