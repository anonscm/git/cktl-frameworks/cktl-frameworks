package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOProgramme;
import org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxTreeModel;

public class ProgrammesTreeView extends AFwkCktlSangriaComponent {

  public static final String BINDING_financeur = "financeur";
  public static final String BINDING_selection = "selection";

  private ProgrammeTreeViewNode rootNode = null;
  private ProgrammeTreeViewNode currentNode;
  private ProgrammesTreeView.Delegate programmeTreeDelegate = null;
  private AjaxTreeModel treeModel = null;
  
  public ProgrammesTreeView(WOContext context) {
    super(context);
  }

  public EOStructure financeur() {
    return (EOStructure) valueForBinding(BINDING_financeur);
  }
  
  public NSArray<EOProgramme> selection() {
    if (hasBinding(BINDING_selection)) {
      return (NSArray<EOProgramme>) valueForBinding(BINDING_selection);
    }
    return null;
  }

  public String programmeTreeId() {
    return getComponentId() + "_programmeTree";
  }
  
  public String nodeContainerId() {
    return currentNodeId() + "_actionsContainer";
  }
  
  public ProgrammeTreeViewNode rootNode() {
    if (rootNode == null) {
      rootNode = new ProgrammeTreeViewNode(null, new EmptyRootObject());
    }
    return rootNode;
  }
  
  
  public ProgrammeTreeViewNode getCurrentNode() {
    return currentNode;
  }

  public void setCurrentNode(ProgrammeTreeViewNode currentNode) {
    this.currentNode = currentNode;
  }

  public ProgrammesTreeView.Delegate programmeTreeDelegate() {
    if (programmeTreeDelegate == null) {
      programmeTreeDelegate = new ProgrammesTreeView.Delegate();
    }
    return programmeTreeDelegate;
  }
  

  public AjaxTreeModel getTreeModel() {
    if (treeModel == null) {
      treeModel = new AjaxTreeModel();
    }
    return treeModel;
  }

  public void setTreeModel(AjaxTreeModel treeModel) {
    this.treeModel = treeModel;
  }

  public Boolean isCurrentNodeSelected() {
    if (getCurrentNode() != null && getCurrentNode().object() != null && selection() != null) { 
      return selection().contains(getCurrentNode().object());
    }
    return false;
  }
  
  public String currentNodeClass() {
    if (isCurrentNodeSelected()) {
      return "programmeTreeViewSelectedItem";
    }
    return "programmeTreeViewUnselectedItem";
  }

  public WOActionResults selectionnerProgramme() {
    selection().add((EOProgramme) getCurrentNode().object());
	if (getCurrentNode().children().size() > 0) {
		session().addSimpleInfoMessage(localize("attention") , localize("financeursEtProgrammes.selectionSousProgrammes"));
	}
    return doNothing();
  }

  public WOActionResults deselectionnerProgramme() {
    selection().remove((EOProgramme) getCurrentNode().object());
    return doNothing();
  }
  
  public String currentNodeId() {
    if (getCurrentNode().isAnnee()) {
      return getComponentId() + "_ANNEE_" + getCurrentNode().annee();
    } else if (getCurrentNode().isProgramme()) {
      EOProgramme _programme = (EOProgramme) getCurrentNode().object();
      return getComponentId() + "_PROGRAMME_" + _programme.code().replaceAll("\\.", "_") + "_ANNEE_" + getCurrentNode().annee();
    } else {
      return "";
    }
  }
  
  public String onCompleteSelectionnerProgramme() {
    String cssSelectedClass = "programmeTreeViewSelectedItem";
    String cssUnselectedClass = "programmeTreeViewUnselectedItem";

    EOProgramme _programme = (EOProgramme) getCurrentNode().object();
    String _nodePartialId = getComponentId() + "_PROGRAMME_" + _programme.code().replaceAll("\\.", "_");
    String _onComplete = "function(){";
    _onComplete += "var selections = $$('span[id^=" + _nodePartialId + "]'); selections.each(function(e){e.addClassName('" + cssSelectedClass + "');});";
    _onComplete += "var selections = $$('span[id^=" + _nodePartialId + "]'); selections.each(function(e){e.removeClassName('" + cssUnselectedClass + "');});";
    _onComplete += "var selections = $$('span[id^=" + _nodePartialId + "]'); selections.each(function(e){AUL.update(e.getAttribute('id'));});";
    _onComplete += "}";
    return _onComplete;
  }
  
  public String onCompleteDeselectionnerProgramme() {
    String cssSelectedClass = "programmeTreeViewSelectedItem";
    String cssUnselectedClass = "programmeTreeViewUnselectedItem";
    EOProgramme _programme = (EOProgramme) getCurrentNode().object();
    String _nodePartialId = getComponentId() + "_PROGRAMME_" + _programme.code().replaceAll("\\.", "_");
    String _onComplete = "function(){";
    _onComplete += "var selections = $$('span[id^=" + _nodePartialId + "]'); selections.each(function(e){e.addClassName('" + cssUnselectedClass + "');});";
    _onComplete += "var selections = $$('span[id^=" + _nodePartialId + "]'); selections.each(function(e){e.removeClassName('" + cssSelectedClass + "');});";
    _onComplete += "var selections = $$('span[id^=" + _nodePartialId + "]'); selections.each(function(e){AUL.update(e.getAttribute('id'));});";
    _onComplete += "}";
    return _onComplete;
  }
  
  public static class Delegate implements AjaxTreeModel.Delegate {

    public NSArray childrenTreeNodes(Object node) {
      if (node != null) {
        return ((ProgrammeTreeViewNode) node).children();
      }
      return NSArray.EmptyArray;
    }

    public boolean isLeaf(Object node) {
      if (node != null) {
        return ((ProgrammeTreeViewNode) node).isLeaf();
      }
      return true;
    }

    public Object parentTreeNode(Object node) {
      if (node != null) {
        return ((ProgrammeTreeViewNode) node).parent();
      }
      return null;
    }
  }
  
  
  public class ProgrammeTreeViewNode {
    private NSArray<ProgrammeTreeViewNode> children;
    private ProgrammeTreeViewNode parent;
    private Object object;

    public ProgrammeTreeViewNode(ProgrammeTreeViewNode parent, Object obj) {
      object = obj;
      this.parent = parent;
    }

    public NSArray<ProgrammeTreeViewNode> children() {
      if (children == null) {
        if (object() instanceof EmptyRootObject) {
          children = nodesForObjects(EOProgrammeAnnee.anneesPourStructure(edc(), financeur()));
        } else if (isAnnee()) {
          Integer annee = (Integer) object();
          children = nodesForObjects(EOProgramme.programmesDuFinanceur(edc(), financeur(), annee));
        } else if (isProgramme()) {
          EOProgramme programme = (EOProgramme) object();
          if (programme.niveau() == EOProgramme.NIVEAU_PROGRAMME) {
            Integer annee = (Integer) parent().object();
            children = nodesForObjects(programme.programmesFils(annee));
          } else {
            children = NSArray.emptyArray();
          }
        }
      }
      return children;
    }

    public void setChildren(NSArray<ProgrammeTreeViewNode> children) {
      this.children = children;
    }

    
    public boolean isAnnee() {
      if (object() != null && object()  instanceof Integer) {
        return true;
      }
      return false;
    }
    
    public Integer annee() {
      ProgrammeTreeViewNode _node = this;
      while (_node.isProgramme()) {
        _node = _node.parent();
      }
      return (Integer) _node.object();
    }
    
    public boolean isProgramme() {
      if (object() != null && object()  instanceof EOProgramme) {
        return true;
      }
      return false;
    }
    
    public boolean isLeaf() {
      return (children().count() == 0);
    }

    public ProgrammeTreeViewNode parent() {
      return parent;
    }

    public void setParent(ProgrammeTreeViewNode node) {
      parent = node;
    }

    public Object object() {
      return object;
    }
    
    @Override
    public String toString() {
      return object != null ? object.toString() : super.toString();
    }
    
    @SuppressWarnings("rawtypes")
    public NSArray<ProgrammeTreeViewNode> nodesForObjects(NSArray objects) {
      NSArray<ProgrammeTreeViewNode> _nodes = new NSMutableArray<ProgrammesTreeView.ProgrammeTreeViewNode>();
      for (Object _object : objects) {
        ProgrammeTreeViewNode _node = new ProgrammeTreeViewNode(this, _object);
        _nodes.add(_node);
      }
      return _nodes;
    }

  }
  
  public class EmptyRootObject {

    public EmptyRootObject() {
    }

  }

}