package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class CktlAjaxWindowWrapper extends AFwkCktlSangriaComponent {
	
	private final String BINDING_actionsDelegate = "actionsDelegate";
	
    public CktlAjaxWindowWrapper(WOContext context) {
        super(context);
    }
    
    public String fooContainerId() {
    	return getComponentId() + "_fooComponentId";
    }
    
    public WOActionResults annuler() {
    	return delegate().annuler();
    }
    
    public WOActionResults ok() {
    	return delegate().ok();
    }
    
    public Boolean afficherLesBoutons() {
    	return hasBinding(BINDING_actionsDelegate);
    }
    
    
    private ICktlAjaxWindowWrapperActionsDelegate delegate() {
    	if(hasBinding(BINDING_actionsDelegate)) {
    		return (ICktlAjaxWindowWrapperActionsDelegate) valueForBinding(BINDING_actionsDelegate);
    	}
    	return null;
    }
    
    
}