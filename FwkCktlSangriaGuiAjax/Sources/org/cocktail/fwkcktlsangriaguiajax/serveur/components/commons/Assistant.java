package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.IModuleAssistant;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValueUtilities;

public class Assistant extends AFwkCktlSangriaComponent {
		
	public static final String MODULES_BDG = "modules";
	public static final String ETAPES_BDG = "etapes";
	public static final String ACTIVE_MODULE_IDX_BDG = "indexModuleActif";
	public static final String ACTION_TERMINER_BDG = "terminer";
	public static final String LIBELLE_TERMINER_BDG = "libelleTerminer";
	public static final String ACTION_APRES_TERMINER_BDG = "apresTerminer";
	public static final String ACTION_ANNULER_BDG = "annuler";
	public static final String LIBELLE_ANNULER_BDG = "libelleAnnuler";
	
	
	
	private NSArray modules;
	
	private Integer indexModuleActif;
	private String libelleAnnuler;
	private String libelleTerminer;
	
	private IModuleAssistant module;
	
	public NSArray<String> etapes;
	public String uneEtape;
	
	public Assistant(WOContext context) {
		super(context);
	}
	
	public String assistantContentContainerId() {
		return getComponentId() + "_assistantContentContainer";
	}

	public void reset() {
		modules = null;
		indexModuleActif = null;
		module = null;
	}
	
	public String moduleName() {
		String moduleName = null;
		if (modules() != null && modules().count()>0) {
			moduleName = (String)modules().objectAtIndex(indexModuleActif().intValue());
		}
		return moduleName;
	}
	
	public NSArray modules() {
		modules = (NSArray)valueForBinding(MODULES_BDG);
		return modules;
	}
 
	public void setModules(NSArray modules) {
		this.modules = modules;
	}
	
	public Integer indexModuleActif() {
		indexModuleActif = (Integer)valueForBinding(ACTIVE_MODULE_IDX_BDG);
		
		return indexModuleActif;
	}

	public void setIndexModuleActif(Integer indexModuleActif) {
		this.indexModuleActif = indexModuleActif;
		setValueForBinding(indexModuleActif, ACTIVE_MODULE_IDX_BDG);
	}
	
	public NSArray<String> etapes() {
		etapes = (NSArray<String>)valueForBinding(ETAPES_BDG);
		return etapes;
	}
	
	public void setEtapes(NSArray<String> etapes) {
		this.etapes = etapes;
	}
	
	public String styleForEtape() {
		String styleForEtape = null;
		if (etapes().indexOf(uneEtape) == indexModuleActif().intValue()) {
			styleForEtape = "height:30px;font-size: medium;color:#006699; text-align: center;text-shadow: rgb(203, 212, 75) 0em 0em 0.3em;font-weight:bold;";
		} else {
			styleForEtape = "height:30px;font-weight:bold;";
		}
		return styleForEtape;
	}
	
	public boolean isEtapeActive() {
		return etapes().indexOf(uneEtape) == indexModuleActif().intValue();
	}
	
	public WOActionResults annuler() {
		//setIndexModuleActif(null);
		return performParentAction((String) valueForBinding(ACTION_ANNULER_BDG));
	}
	
	
	public WOActionResults terminer() {
		WOActionResults result = null;
		try {
			if (module().valider()) {
				performParentAction(ACTION_TERMINER_BDG);
			} else {
				return doNothing();
			}
		} catch (NSForwardException e) {
			if(e.getCause() instanceof NSValidation.ValidationException) {
				session().addSimpleErrorMessage("Erreur", e.getCause());
				return doNothing();
			}
			throw NSForwardException._runtimeExceptionForThrowable(e);
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
		
		return apresTerminer();
	}
	
	
	public boolean isPrecedentDisabled() {
		return indexModuleActif().intValue()<=0 || module().isPrecedentDisabled();
	}
	
	public WOActionResults precedent() {
		try {
			module().onPrecedent();
			int newIndex = indexModuleActif().intValue()-1;
			if (newIndex>=0) {
				setIndexModuleActif(Integer.valueOf(newIndex));
			}
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e); 
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
		
		return doNothing();
	}
	
	public boolean isSuivantDisabled() {
		return indexModuleActif().intValue()>=modules().count()-1 || module().isSuivantDisabled();
	}
	
	public boolean isTerminerDisabled() {
		boolean isTerminerDisabled = module().isTerminerDisabled();
		return isTerminerDisabled;
	}
	
	public WOActionResults suivant() {
		try {
			module().onSuivant();
			if (module().valider()) {
				int newIndex = indexModuleActif().intValue()+1;
				if (newIndex < modules().count()) {
					setIndexModuleActif(Integer.valueOf(newIndex));
				}
			}
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
		return doNothing();
	}
	
	
	
	
	public WOComponent assistant() {
		return this;
	}
	
	
	/**
	 * @return the module
	 */
	public IModuleAssistant module() {
		return module;
	}
	
	
	/**
	 * @param module the module to set
	 */
	public void setModule(IModuleAssistant module) {
		this.module = module;
	}
	
	
	public WOActionResults apresTerminer() {
		return performParentAction((String) valueForBinding(ACTION_APRES_TERMINER_BDG));
	}
	
	
	/**
	 * @return the libelleAnnuler
	 */
	public String libelleAnnuler() {
		if (hasBinding(LIBELLE_ANNULER_BDG)) {
			libelleAnnuler = (String)valueForBinding(LIBELLE_ANNULER_BDG);
		} else {
			libelleAnnuler = "Annuler";
		}
		return libelleAnnuler;
	}
	
	
	/**
	 * @param libelleAnnuler the libelleAnnuler to set
	 */
	public void setLibelleAnnuler(String libelleAnnuler) {
		this.libelleAnnuler = libelleAnnuler;
	}
	
	/**
	 * @return the libelleTerminer
	 */
	public String libelleTerminer() {
		if (hasBinding(LIBELLE_TERMINER_BDG)) {
			libelleTerminer = (String)valueForBinding(LIBELLE_TERMINER_BDG);
		} else {
			libelleTerminer = "Terminer";
		}
		return libelleTerminer;
	}
	
	
	/**
	 * @param libelleTerminer the libelleTerminer to set
	 */
	public void setLibelleTerminer(String libelleTerminer) {
		this.libelleTerminer = libelleTerminer;
	}

	public boolean isPrecedentEnabled() {
		return !isPrecedentDisabled();
	}

	public boolean isSuivantEnabled() {
		return !isSuivantDisabled();
	}

	public boolean isTerminerEnabled() {
		return !isTerminerDisabled();
	}


	
}
