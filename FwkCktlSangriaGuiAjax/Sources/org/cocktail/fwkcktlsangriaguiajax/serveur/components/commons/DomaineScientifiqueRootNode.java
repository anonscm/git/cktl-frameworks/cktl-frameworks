package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXProperties;

/**
 * 
 * @author Julien Lafourcade
 *
 */
public class DomaineScientifiqueRootNode extends DomaineScientifiqueNode {

	private EOEditingContext edc;
	
	private List<CktlTreeTableNode<EODomaineScientifique>> children;

	private static String PROPERTY_dsVersion = "org.cocktail.fwkcktlsangriaguiajax.dsversion";

	/**
	 * 
	 * @param edc L'editing context dans lequel on va charger les nodes
	 */
	public DomaineScientifiqueRootNode(EOEditingContext edc) {
		super(null, null, false, true);
		this.edc = edc;
	}
	
	@Override
	public List<CktlTreeTableNode<EODomaineScientifique>> getChildren() {
		if (children == null) {
			children = new ArrayList<CktlTreeTableNode<EODomaineScientifique>>();
			EOQualifier qualifier = EODomaineScientifique.PARENT.isNull();
			if (dsVersion() != 0) {
				qualifier = ERXQ.and(qualifier, EODomaineScientifique.DS_VERSION.eq(dsVersion()));
			}
			NSArray<EODomaineScientifique> enfant = EODomaineScientifique.fetchAll(edc, qualifier);
			for (EODomaineScientifique ds : enfant) {
				DomaineScientifiqueNode child = new DomaineScientifiqueNode(ds, this, true, false);
				children.add(child);
			}
		}
		return children;
	}

	/**
	 * 
	 * @return la version des domaines scientifiques à aller chercher
	 */
	public int dsVersion() {
		return ERXProperties.intForKeyWithDefault(PROPERTY_dsVersion, 0);
	}
	
}