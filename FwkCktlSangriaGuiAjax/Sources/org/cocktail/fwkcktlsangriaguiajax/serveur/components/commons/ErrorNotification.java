package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import com.webobjects.appserver.WOContext;

public class ErrorNotification extends AFwkCktlSangriaComponent {
    public ErrorNotification(WOContext context) {
        super(context);
    }
    
    public String messageContainerId() {
    	return getComponentId() + "messageContainer";
    }
    
}