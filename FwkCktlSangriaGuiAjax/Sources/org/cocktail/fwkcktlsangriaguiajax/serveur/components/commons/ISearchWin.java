package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;


import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOGenericRecord;

public interface ISearchWin extends WOActionResults {

	public static final String ACTION_OK = "Ok";

	public EOGenericRecord getSelection();
	public void setWinId(String winId);
	public void setContainerOnCloseID(String containerOnCloseID);
	public String getLastAction();
	public void setLastAction(String lastAction);

}
