package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;

/**
 * 
 * @author Julien Lafourcade
 *
 */
public class DomaineScientifiqueNode extends CktlTreeTableNode<EODomaineScientifique> {

	private List<CktlTreeTableNode<EODomaineScientifique>> children;
	
	/**
	 * 
	 * @param data le domaine scientifique à ajouter à la node
	 * @param parent la node parente
	 * @param visible la node est-elle visible ?
	 * @param expanded la node est-elle dépliée ?
	 */
	public DomaineScientifiqueNode(EODomaineScientifique data, CktlTreeTableNode<EODomaineScientifique> parent, Boolean visible, Boolean expanded) {
		super(data, parent, visible, expanded);
	}

	@Override
	public List<CktlTreeTableNode<EODomaineScientifique>> getChildren() {
		if (children == null) {
			children = new ArrayList<CktlTreeTableNode<EODomaineScientifique>>();
			for (EODomaineScientifique ds : getData().enfants()) {
				DomaineScientifiqueNode child = new DomaineScientifiqueNode(ds, this, true, false);
				children.add(child);
			}
		}
		return children;
	}
}