package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.appserver.WOContext;

public abstract class ModuleEquipeRecherche extends ModuleFwkCktlRecherche {

	public static final String BINDING_equipeRecherche = "equipeRecherche";
	
	private EOStructure equipeRecherche;
	
	public ModuleEquipeRecherche(WOContext context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public EOStructure equipeRecherche() {
		if(equipeRecherche == null) {
			if(hasBinding(BINDING_equipeRecherche)) {
				equipeRecherche = (EOStructure) valueForBinding(BINDING_equipeRecherche);
			}
		}
		
		return equipeRecherche;
	}

	
}
