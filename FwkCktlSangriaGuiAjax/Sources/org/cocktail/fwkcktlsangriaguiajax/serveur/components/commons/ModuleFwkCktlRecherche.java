package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.IModuleAssistant;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;


public abstract class ModuleFwkCktlRecherche extends AFwkCktlSangriaComponent implements IModuleAssistant {

	public static final String MODULE_BDG = "module";
	public static final String ERREUR_SAISIE_MESSAGE_BDG = "erreurSaisieMessages";
	
	private IModuleAssistant module;
	
	
	public ModuleFwkCktlRecherche(WOContext context) {
		super(context);
		//setModule(this);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		
		setModule(this);
		
		super.appendToResponse(response, context);
	}
	
	
	public boolean isPrecedentDisabled() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isSuivantDisabled() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isTerminerDisabled() {
		// TODO Auto-generated method stub
		return false;
	}

	public void onPrecedent() {
		// TODO Auto-generated method stub

	}

	public void onSuivant() {
		// TODO Auto-generated method stub

	}

	public  abstract boolean valider();

	/**
	 * @return the module
	 */
	public IModuleAssistant module() {
		return this;
	}

	/**
	 * @param module the module to set
	 */
	public void setModule(IModuleAssistant module) {
		this.module = module;
		setValueForBinding(module, MODULE_BDG);
	}
	
	public void setErreurSaisieMessages(NSArray<String> erreurSaisieMessages) {
		setValueForBinding(erreurSaisieMessages, ERREUR_SAISIE_MESSAGE_BDG);
	}
	
}
