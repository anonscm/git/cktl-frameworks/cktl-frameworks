package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import org.cocktail.fwkcktlrecherche.server.metier.EOAap;

import com.webobjects.appserver.WOContext;


public abstract class ModuleAap extends ModuleFwkCktlRecherche {

	public static final String BINDING_app = "aap";
	
	private EOAap aap;
	
	public ModuleAap(WOContext context) {
		super(context);
	}

	
	public EOAap aap() {
		if(aap == null)
			if(hasBinding(BINDING_app)) {
				aap = (EOAap) valueForBinding(BINDING_app);
			}
		return aap;
	}
	

	
	
}
