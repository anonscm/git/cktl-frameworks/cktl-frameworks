package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class DomaineScientifiqueSelect extends AFwkCktlSangriaComponent {
    
	private static String BINDING_selectedDomaineScientifique = "selectedDomaineScientifique"; 
	private static String BINDING_icon = "icon"; 
	private static String BINDING_showLabel = "showLabel"; 
	private static String BINDING_showBoutonSupprimer = "showBoutonSupprimer"; 

	
	private NSArray<EODomaineScientifique> domainesScientifiques = null;
	private EODomaineScientifique domaineScientifique;
	
	private String fenetreDomainesId;
	private String selectedDomaineScientifiqueContainerId;
	
	private DomaineScientifiqueNode currentDsNode;
	private DomaineScientifiqueNode selectedDsNode;
	private DomaineScientifiqueRootNode rootDsNode;
	
	/**
	 * 
	 * @param context 
	 */
	public DomaineScientifiqueSelect(WOContext context) {
        super(context);
        fenetreDomainesId = getComponentId() + "_fenetreDomaines";
        selectedDomaineScientifiqueContainerId = getComponentId() + "_selectedDomaineScientifiqueContainer";
    }
    
	public String getFenetreDomainesId() { return fenetreDomainesId; }
	public String getSelectedDomaineScientifiqueContainerId() { return selectedDomaineScientifiqueContainerId; }

	
	public NSArray<EODomaineScientifique> domainesScientifiques() {
		if(domainesScientifiques == null) {
			domainesScientifiques = EODomaineScientifique.fetchAll(edc());
		}
		return domainesScientifiques;
	}

	public EODomaineScientifique domaineScientifique() {
		return domaineScientifique;
	}

	public void setDomaineScientifique(EODomaineScientifique domaineScientifique) {
		this.domaineScientifique = domaineScientifique;
	}

	public EODomaineScientifique selectedDomaineScientifique() {
		return (EODomaineScientifique) valueForBinding(BINDING_selectedDomaineScientifique);
	}

	public void setSelectedDomaineScientifique(EODomaineScientifique selectedDomaineScientifique) {
		setValueForBinding(selectedDomaineScientifique, BINDING_selectedDomaineScientifique);
	}
    
    public DomaineScientifiqueNode getCurrentDsNode() {
		return currentDsNode;
	}

	public void setCurrentDsNode(DomaineScientifiqueNode currentDsNode) {
		this.currentDsNode = currentDsNode;
	}

	public DomaineScientifiqueNode getSelectedDsNode() {
		return selectedDsNode;
	}

	public void setSelectedDsNode(DomaineScientifiqueNode selectedDsNode) {
		this.selectedDsNode = selectedDsNode;
	}

	/**
	 * Lazy initialization de la root node 
	 * @return la root node
	 */
	public DomaineScientifiqueRootNode getRootDsNode() {
		if (rootDsNode == null) {
			rootDsNode = new DomaineScientifiqueRootNode(edc());
		}
		return rootDsNode;
	}

	public void setRootDsNode(DomaineScientifiqueRootNode rootDsNode) {
		this.rootDsNode = rootDsNode;
	}
	
	public WOActionResults valider() {
		if (getSelectedDsNode() != null) {
			setSelectedDomaineScientifique(getSelectedDsNode().getData());
			CktlAjaxWindow.close(context(), getFenetreDomainesId());
		} else {
			session().addSimpleErrorMessage("Attention", "Vous devez sélectionner un domaine scientifique");
		}
		return doNothing();
	}

	public String icon() {
		return stringValueForBinding(BINDING_icon, "find");
	}
	
	public Boolean showLabel() {
		return booleanValueForBinding(BINDING_showLabel, true);
	}
	
	public Boolean showBoutonSupprimer() {
		return booleanValueForBinding(BINDING_showBoutonSupprimer, false);
	}
	
	public WOActionResults ouverture() {
		setSelectedDsNode(null);
		return doNothing();
	}
	
	public WOActionResults annulerDomaineScientifique() {
		setSelectedDomaineScientifique(null);
		return doNothing();
	}
}