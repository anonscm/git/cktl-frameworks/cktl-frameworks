package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxUtils;
import er.extensions.appserver.ERXResponseRewriter;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

public class DialogBox extends CktlAjaxWOComponent {

	private static final String BINDING_dialogBoxId = "dialogBoxId";
	private static final String BINDING_dialogBoxClass = "dialogBoxClass";
	private static final String BINDING_title = "title";
	private static final String BINDING_width = "width";
	private static final String BINDING_height = "height";
	private static final String BINDING_closable = "closable";
	private static final String BINDING_resizable = "resizable";

	
	private String dialogClass;
	private String dialogBoxId;
	private String title;
	private String width;
	private String height;
	private Boolean closable;
	private Boolean resizable;
	
	public DialogBox(WOContext context) {
        super(context);
    }
	
	
	

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
	
		
			
		super.appendToResponse(response, context);
		String script = "";
		script += "function open" + getDialogBoxId() + "Win() {";
		script += "Dialog.info($('" + 
			getDialogBoxId() + "').innerHTML, {windowParameters: {id: \"" +
			getDialogBoxId() + "Win\", className:\"" + 
			getDialogClass() + "\", title:\"" + 
			getTitle() + "\", width:\"" + 
			getWidth() + "\", height:\"" +
			getHeight() + "\", closable:" +
			getClosable() + ", resizable:" +
			getResizable() +
			"}});"    +         
			"}";
		script += "function close" +  getDialogBoxId() + "Win() {" + 
			"Windows.getWindow('" + getDialogBoxId() + "Win').close();" + 
			"}";
		ERXResponseRewriter.addScriptCodeInHead(response, context, script);
		
		
	}




	public void setDialogClass(String dialogClass) {
		this.dialogClass = dialogClass;
	}
/*
	public String getDialogClass() {
		return dialogClass;
	}
*/
	public void setDialogBoxId(String dialogBoxId) {
		this.dialogBoxId = dialogBoxId;
	}
/*
	public String getDialogBoxId() {
		return dialogBoxId;
	}
*/
	public void setWidth(String width) {
		this.width = width;
	}
/*
	public String getWidth() {
		return width;
	}
*/
	public void setTitle(String title) {
		this.title = title;
	}
/*
	public String getTitle() {
		return title;
	}
*/
	public String getDialogClass() {
		if (hasBinding(BINDING_dialogBoxClass)) {
			return (String) valueForBinding(BINDING_dialogBoxClass);
		}
		else {
			return "bluelighting";
		}
	}

	public String getDialogBoxId() {
		return (String) valueForBinding(BINDING_dialogBoxId);
	}

	public String getWidth() {
		if (hasBinding(BINDING_width)) {
			return (String) valueForBinding(BINDING_width);
		}
		else {
			return "450";
		}
	}

	public String getTitle() {
		if (hasBinding(BINDING_title)) {
			return (String) valueForBinding(BINDING_title);
		}
		else {
			return "";
		}	
	}




	public void setHeight(String height) {
		this.height = height;
	}




	public String getHeight() {
		if (hasBinding(BINDING_height)) {
			return (String) valueForBinding(BINDING_height);
		}
		else {
			return "450";
		}
	}




	public void setClosable(Boolean closable) {
		this.closable = closable;
	}




	public Boolean getClosable() {
		if (hasBinding(BINDING_closable)) {
			return (Boolean) valueForBinding(BINDING_closable);
		}
		else {
			return false;
		}
	}




	public void setResizable(Boolean resizable) {
		this.resizable = resizable;
	}




	public Boolean getResizable() {
		if (hasBinding(BINDING_resizable)) {
			return (Boolean) valueForBinding(BINDING_resizable);
		}
		else {
			return false;
		}
	}	
   
    
    
}

