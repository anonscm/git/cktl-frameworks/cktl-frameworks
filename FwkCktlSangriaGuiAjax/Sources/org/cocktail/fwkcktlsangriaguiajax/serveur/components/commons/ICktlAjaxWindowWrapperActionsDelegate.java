package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import com.webobjects.appserver.WOActionResults;

public interface ICktlAjaxWindowWrapperActionsDelegate {
	public WOActionResults ok();
	public WOActionResults annuler();
}
