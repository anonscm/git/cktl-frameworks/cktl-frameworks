package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import com.webobjects.appserver.WOContext;

public class InfoNotification extends AFwkCktlSangriaComponent {
    public InfoNotification(WOContext context) {
        super(context);
    }
    
    public String messageContainerId() {
    	return getComponentId() + "messageContainer";
    }
}