package org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons;

import java.util.UUID;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlResourceProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxApplication;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneTableView;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUtils;
import er.extensions.eof.ERXEC;

/**
 * 
 * @author Julien Lafourcade
 * 
 */
public class AFwkCktlSangriaComponent extends CktlAjaxWOComponent implements CktlResourceProvider {

	private static final long serialVersionUID = 4907290521705549242L;

	private static final String SESSION_notificationContainerId = "notitficationContainerId";
	private static final String SESSION_KEY_applicationUser = "applicationUser";

	public static final String BINDING_utilisateurPersId = "utilisateurPersId";
	public static final String BINDING_editingContext = "editingContext";
	public static final String BINDING_updateContainerID = "updateContainerID";

	public final String colonnesIndividusTableView = new NSArray<String>(PersonneTableView.COL_NUMERO_KEY, PersonneTableView.COL_NOM_KEY, PersonneTableView.COL_DATE_NAISSANCE_KEY, PersonneTableView.COL_IS_INTERNE_KEY).componentsJoinedByString(",");
	
	private EOEditingContext edc;

	/**
	 * 
	 * @param context
	 *            le context
	 */
	public AFwkCktlSangriaComponent(WOContext context) {
		super(context);
	}

	@Override
	public CocktailAjaxSession session() {
		return (CocktailAjaxSession) super.session();
	}

	@Override
	public CocktailAjaxApplication application() {
		return (CocktailAjaxApplication) super.application();
	}

	/**
	 * 
	 * @param key
	 *            la clé de la chaine de caratère
	 * @return la chaine si elle existe
	 */
	public String localize(String key) {
		return session().localizer().localizedStringForKey(key);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		appendStyleSheets(response, context);
	}

	public void appendStyleSheets(WOResponse response, WOContext context) {
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/default.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/alert.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/lighting.css");

		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommon.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommonBleu.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlSangriaGuiAjax.framework", "styles/fwkcktlrechercheguiajax.css");

		AjaxUtils.addStylesheetResourceInHead(context, response, "Ajax", "default_ajaxupload.css");
	}

	/**
	 * @return l'editing context pour le composant
	 */
	public EOEditingContext edc() {
		if (edc != null) {
			return edc;
		} else if (hasBinding(BINDING_editingContext)) {
			return (EOEditingContext) valueForBinding(BINDING_editingContext);
		} else {
			edc = ERXEC.newEditingContext();
			return edc;
		}
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	/**
	 * 
	 * @return le persId de l'utilisateur connecté
	 */
	public Integer getUtilisateurPersId() {
		return session().connectedUserInfo().persId().intValue();
	}

	/**
	 * @return le container a mette à jour par le composant
	 */
	public String updateContainerID() {
		return (String) valueForBinding(BINDING_updateContainerID);
	}

	/**
	 * 
	 * @return le container utilisé pour l'affichage des notifications
	 */
	public String notificationContainerId() {
		if (session().objectForKey(SESSION_notificationContainerId) == null) {
			String notificationContainerId = "cktl_" + UUID.randomUUID().toString().replaceAll("-", "_") + "_notificationContainer";
			session().setObjectForKey(notificationContainerId, SESSION_notificationContainerId);
		}

		return (String) session().objectForKey(SESSION_notificationContainerId);
	}

	/**
	 * 
	 * @return l'instance correspondant à l'utilisateur connecté
	 */
	public ApplicationUser getApplicationUser() {
		if (session().objectForKey(SESSION_KEY_applicationUser) == null) {
			ApplicationUser applicationUser = new ApplicationUser(edc(), getUtilisateurPersId());
			session().setObjectForKey(applicationUser, SESSION_KEY_applicationUser);
		}

		return (ApplicationUser) session().objectForKey(SESSION_KEY_applicationUser);
	}

	public FactoryAssociation getFactoryAssociation() {
		return FactoryAssociation.shared();
	}

	/**
	 * Implémentation qui remet les feuilles de styles en cas de bug, surtout
	 * avec les ModalWindows
	 * 
	 * @param response
	 *            la response
	 * @param context
	 *            le context
	 */
	public void injectResources(WOResponse response, WOContext context) {
		appendStyleSheets(response, context);
	}


	
}
