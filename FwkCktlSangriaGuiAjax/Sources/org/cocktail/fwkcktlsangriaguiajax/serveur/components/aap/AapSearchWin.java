package org.cocktail.fwkcktlsangriaguiajax.serveur.components.aap;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.finder.FinderAap;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSPropertyListSerialization;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.foundation.ERXArrayUtilities;

public class AapSearchWin extends AComponent {
	
	
	private static Logger logger = Logger.getLogger(AapSearchWin.class);
			
	////////////
	////// Liste des bindings du composants
	////////////
	/** The Constant BINDING_displayGroup. */
	private static final String BINDING_displayGroup = "displayGroup";
	
	/** The Constant BINDING_selectedAap. */
	private static final String BINDING_selectedAap = "selectedAap";
		
	/** The Constant BINDING_callbackOnSelectionnerAap. */
	private static final String BINDING_callbackOnSelectionnerAap = "callbackOnSelectionnerAap";
	
	private static final String BINDING_projetScientifique = "projetScientifique";
	
	private static final String BINDING_ddsAAjouter = "ddsAAjouter";
	
	private static final String BINDING_creationDdsPageName = "creationDdsPageName";
	
	private EOAap selection;
	
	/** The exercice aap. */
	private String exerciceAap;
	
	/** The numero aap. */
	private String numeroAap;
	
	/** The laboratoire aap. */
	private EOStructure laboratoireAap;
	
	/** The financeur aap. */
	private EOStructure financeurAap;
	
	/** The current financeur. */
	private EOStructure currentFinanceur;
	
	/** The responsable scientifique aap. */
	private EOIndividu responsableScientifiqueAap;
	
	/** The partenaire aap. */
	private EOStructure partenaireAap;
	
	/** The aap courant. */
	private EOAap aapCourant;
	
	/** The selected aap. */
	private EOAap selectedAap;
	
	/** The current type recherche. */
	private String currentTypeRecherche;
		
	/** The dico table view aap. */
	private NSMutableDictionary dicoTableViewAap;
	
	/** The dg aap. */
	private WODisplayGroup dgAap;
	
	/** The erreurs messages. */
	private NSArray<String> erreursMessages;
	
	/** The has searched aap. */
	private Boolean hasSearchedAap = false;
	
	/** The finder aap. */
	private FinderAap finderAap;
	
	private EOProjetScientifique projetScientifique;
	


	public AapSearchWin(WOContext context) {
        super(context);
    }
    
    /**
	 * Raz.
	 */
	public void raz() {
		setExerciceAap(null);
		setNumeroAap(null);
		setLaboratoireAap(null);
		setResponsableScientifiqueAap(null);
		setFinanceurAap(null);
		setPartenaireAap(null);
		setCurrentFinanceur(null);
		setAapCourant(null);
		setSelection(null);
		dgAap= null;
		hasSearchedAap = false;
		finderAap = null;
	}
	
	/**
	 * Raz recherche.
	 *
	 * @return the wO action results
	 */
	public WOActionResults razRecherche() {
		raz();
		return doNothing();
	}
	
	/**
	 * Gets the exercice aap.
	 *
	 * @return the exercice aap
	 */
	public String getExerciceAap() {
		return exerciceAap;
	}

	/**
	 * Sets the exercice aap.
	 *
	 * @param exerciceAap the new exercice aap
	 */
	public void setExerciceAap(String exerciceAap) {
		this.exerciceAap = exerciceAap;
	}

	/**
	 * Gets the numero aap.
	 *
	 * @return the numero aap
	 */
	public String getNumeroAap() {
		return numeroAap;
	}

	/**
	 * Sets the numero aap.
	 *
	 * @param numeroAap the new numero aap
	 */
	public void setNumeroAap(String numeroAap) {
		this.numeroAap = numeroAap;
	}

	/**
	 * Gets the aap courant.
	 *
	 * @return the aap courant
	 */
	public EOAap getAapCourant() {
		return aapCourant;
	}

	/**
	 * Sets the aap courant.
	 *
	 * @param aapCourant the new aap courant
	 */
	public void setAapCourant(EOAap aapCourant) {
		this.aapCourant = aapCourant;
	}

	/**
	 * Sets the erreurs messages.
	 *
	 * @param erreursMessages the new erreurs messages
	 */
	public void setErreursMessages(NSArray<String> erreursMessages) {
		this.erreursMessages = erreursMessages;
	}

	/**
	 * Gets the erreurs messages.
	 *
	 * @return the erreurs messages
	 */
	public NSArray<String> getErreursMessages() {
		return erreursMessages;
	}
	
	/**
	 * Sets the selected aap.
	 *
	 * @param selectedAap the new selected aap
	 */
	public void setSelectedAap(EOAap selectedAap) {
		this.selectedAap = selectedAap;
	}

	/**
	 * Gets the selected aap.
	 *
	 * @return the selected aap
	 */
	public EOAap getSelectedAap() {
		return selectedAap;
	}

	/**
	 * Checks for selected aap.
	 *
	 * @return the boolean
	 */
	public Boolean hasSelectedAap() {
		return (getSelection() != null);
	}

	/**
	 * Checks for searched for aap.
	 *
	 * @return the boolean
	 */
	public Boolean hasSearchedForAap() {
		return hasSearchedAap;
	}
	
	/**
	 * Finder aap.
	 *
	 * @return the finder aap
	 */
	private FinderAap finderAap() {	
		if(finderAap == null)
			finderAap = new FinderAap(edc());
		return finderAap;
	}

	/**
	 * Dico table view aap.
	 *
	 * @return the nS mutable dictionary
	 */
	public NSMutableDictionary dicoTableViewAap() {
		if (dicoTableViewAap == null) {
			NSData data = new NSData(application().resourceManager().bytesForResourceNamed("TableViewAap.plist", frameworkName(), NSArray.EmptyArray));
			dicoTableViewAap = new NSMutableDictionary((NSDictionary) NSPropertyListSerialization.propertyListFromData(data, "UTF-8"));
		}
		return dicoTableViewAap;
	}
	
	
	/**
	 * Dg aap.
	 *
	 * @return the wO display group
	 */
	public WODisplayGroup dgAap() {
		
		if(dgAap == null) {
			dgAap = new ERXDisplayGroup<EOAap>();
			dgAap.setDelegate(new DgAapDelegate());
			//dgAap.setNumberOfObjectsPerBatch(10);
		}
		return dgAap;

	}
	
	/**
	 * The Class DgAapDelegate.
	 */
	public class DgAapDelegate {
		
		/**
		 * Display group did change selection.
		 *
		 * @param group the group
		 */
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			if(group.selectedObject() != null) {
				//edc().revert();
				setSelection( (EOAap) group.selectedObject());
				//edc().saveChanges();
			}
		}
	}
	
	/**
	 * Formulaire recherche aap id.
	 *
	 * @return the string
	 */
	public String formulaireRechercheAapId() {
		return getComponentId() + "_formulaireRechercheAap";
	}

	/**
	 * Aap display container id.
	 *
	 * @return the string
	 */
	public String aapDisplayContainerId() {
		return getComponentId() + "_aapDisplayContainer";
	}
	
	/**
	 * Aap table view id.
	 *
	 * @return the string
	 */
	public String aapTableViewId() {
		return getComponentId() + "_aapTableView";
	}
	
	/**
	 * Aap table view container id.
	 *
	 * @return the string
	 */
	public String aapTableViewContainerId() {
		return getComponentId() + "_aapTableViewContainer";
	}	
	
	/**
	 * On selectionner personne.
	 *
	 * @return the wO action results
	 */
	public WOActionResults onSelectionnerPersonne() {
		return null;
	}

	// 
	
	/**
	 * Lancer recherche.
	 *
	 * @return the wO action results
	 */
	public WOActionResults lancerRecherche() {
		
		finderAap().clear();
		
		NSMutableArray<String> erreurs = new NSMutableArray<String>();
		
		
		try {
			if(!MyStringCtrl.isEmpty(exerciceAap))
				finderAap().setExercice(Integer.parseInt(exerciceAap));				
		}
		catch (NumberFormatException nfe) {
			erreurs.add("Erreur à la saisie de l'exercice");
		}
		try {
			if(!MyStringCtrl.isEmpty(numeroAap))
			 finderAap().setOrdre(Integer.parseInt(numeroAap));
		}
		catch (NumberFormatException nfe) {
			erreurs.add("Erreur à la saisie du numéro");
		}
		
		setErreursMessages(erreurs);
		
		if(! erreurs.isEmpty()) {
			context().response().setStatus(500);
			return doNothing(); 
		}
		
		finderAap().setLaboratoire(laboratoireAap);
		finderAap().setResponsableScientifique(responsableScientifiqueAap);
		finderAap().setFinanceur(financeurAap);
		finderAap().setPartenaire(partenaireAap);
		
		try {
			NSArray<EOAap> resultats = finderAap().find();
			dgAap().setObjectArray(resultats);
			if(resultats.isEmpty())
				selectedAap = null;
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
		
		setErreursMessages(erreurs);
		
		if(! erreurs.isEmpty()) {
			context().response().setStatus(500);
			return doNothing(); 
		}
		
		hasSearchedAap = true;
		
		return doNothing();
	}
		
	/**
	 * On success lancer recherche.
	 *
	 * @return the string
	 */
	public String onSuccessLancerRecherche() {
		return "function() { " + getContainerErreurMsgId() + "Update();"  + aapTableViewContainerId()  + "Update(); }";
	}
	
	/**
	 * On failure lancer recherche.
	 *
	 * @return the string
	 */
	public String onFailureLancerRecherche() {
		return "function() { " + getContainerErreurMsgId() + "Update();}"; 
	}
	
	

	/**
	 * Sets the laboratoire aap.
	 *
	 * @param laboratoireAap the new laboratoire aap
	 */
	public void setLaboratoireAap(EOStructure laboratoireAap) {
		this.laboratoireAap = laboratoireAap;
	}

	/**
	 * Gets the laboratoire aap.
	 *
	 * @return the laboratoire aap
	 */
	public EOStructure getLaboratoireAap() {
		return laboratoireAap;
	}

    /**
     * Sets the partenaire aap.
     *
     * @param partenaireAap the new partenaire aap
     */
    public void setPartenaireAap(EOStructure partenaireAap) {
		this.partenaireAap = partenaireAap;
	}

	/**
	 * Gets the partenaire aap.
	 *
	 * @return the partenaire aap
	 */
	public EOStructure getPartenaireAap() {
		return partenaireAap;
	}

	/**
	 * Liste des financeurs.
	 *
	 * @return the nS array
	 */
	  public NSArray<EOStructure> listeDesFinanceurs() {
		    
		    try {
		    	EOStructure groupeFinanceurs = EOStructureForGroupeSpec.getGroupeForParamKey(edc(), "GROUPE_FINANCEURS_AAP");
		    	return
		    			EOStructure.LL_STRUCTURE.ascInsensitives().sorted(
		    					ERXArrayUtilities.arrayBySelectingInstancesOfClass(
		    							EOStructureForGroupeSpec.getPersonnesInGroupe(groupeFinanceurs), 
		    							EOStructure.class
		    						)
		    					);
		    }
		    catch (Exception e) {
		    	logger.warn("Attention le groupe des financeurs n'est pas défini");
		    	return new NSArray<EOStructure>();
		    }
		    
		    
	}

	/**
	 * Sets the financeur aap.
	 *
	 * @param financeurAap the new financeur aap
	 */
	public void setFinanceurAap(EOStructure financeurAap) {
		this.financeurAap = financeurAap;
	}

	/**
	 * Gets the financeur aap.
	 *
	 * @return the financeur aap
	 */
	public EOStructure getFinanceurAap() {
		return financeurAap;
	}

	/**
	 * Sets the responsable scientifique aap.
	 *
	 * @param responsableScientifiqueAap the new responsable scientifique aap
	 */
	public void setResponsableScientifiqueAap(EOIndividu responsableScientifiqueAap) {
		this.responsableScientifiqueAap = responsableScientifiqueAap;
	}

	/**
	 * Gets the responsable scientifique aap.
	 *
	 * @return the responsable scientifique aap
	 */
	public EOIndividu getResponsableScientifiqueAap() {
		return responsableScientifiqueAap;
	}

	/**
	 * Sets the current financeur.
	 *
	 * @param currentFinanceur the new current financeur
	 */
	public void setCurrentFinanceur(EOStructure currentFinanceur) {
		this.currentFinanceur = currentFinanceur;
	}

	/**
	 * Gets the current financeur.
	 *
	 * @return the current financeur
	 */
	public EOStructure getCurrentFinanceur() {
		return currentFinanceur;
	}
	
    public EOProjetScientifique getProjetScientifique() {
		return (EOProjetScientifique) valueForBinding(BINDING_projetScientifique);
	}

	public void setProjetScientifique(EOProjetScientifique projetScientifique) {
		this.projetScientifique = projetScientifique;
	}
	
	public WOActionResults creerNouvelAap() {
		WOComponent aapCreation = pageWithName((String) valueForBinding(BINDING_creationDdsPageName));
		aapCreation.takeValueForKey(getProjetScientifique(), BINDING_projetScientifique);
		//AapCreation aapCreation = (AapCreation) pageWithName(AapCreation.class.getName());
		//aapCreation.setProjetScientifique(getProjetScientifique());
		return aapCreation;
	}

	public void setSelection(EOAap selection) {
		this.selection = selection;
		setValueForBinding(selection, BINDING_ddsAAjouter);
	}

	public EOAap getSelection() {
		return selection;
	}
}