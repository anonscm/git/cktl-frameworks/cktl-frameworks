package org.cocktail.fwkcktlsangriaguiajax.serveur.components.personnes;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.GenericSearchWinComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

public class PersonneSearchWin extends GenericSearchWinComponent {
	
	
	Boolean showIndividus;
	Boolean showPersonnesExternes;
	Boolean showPersonnesInternes;
	Boolean showStructures;
	EOQualifier qualifierForGroupes;
	EOQualifier qualifierForIndividus;
	
	public EOQualifier getQualifierForIndividus() {
		return qualifierForIndividus;
	}

	public void setQualifierForIndividus(EOQualifier qualifierForIndividus) {
		this.qualifierForIndividus = qualifierForIndividus;
	}

	public Boolean getShowIndividus() {
		return showIndividus;
	}

	public void setShowIndividus(Boolean showIndividus) {
		this.showIndividus = showIndividus;
	}

	public Boolean getShowPersonnesExternes() {
		return showPersonnesExternes;
	}

	public void setShowPersonnesExternes(Boolean showPersonnesExternes) {
		this.showPersonnesExternes = showPersonnesExternes;
	}

	public Boolean getShowPersonnesInternes() {
		return showPersonnesInternes;
	}

	public void setShowPersonnesInternes(Boolean showPersonnesInternes) {
		this.showPersonnesInternes = showPersonnesInternes;
	}

	public Boolean getShowStructures() {
		return showStructures;
	}

	public void setShowStructures(Boolean showStructures) {
		this.showStructures = showStructures;
	}

	public EOQualifier getQualifierForGroupes() {
		return qualifierForGroupes;
	}

	public void setQualifierForGroupes(EOQualifier qualifierForGroupes) {
		this.qualifierForGroupes = qualifierForGroupes;
	}

	public PersonneSearchWin(WOContext context) {
		super(context);
	}

	public IPersonne getSelectedPersonne() {
		return (IPersonne) getSelection();
	}

	
	
    
}