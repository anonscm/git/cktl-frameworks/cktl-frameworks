package org.cocktail.fwkcktlsangriaguiajax.serveur.components.personnes;

import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneSrch;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ResponsablesScientifiquesListeLiensUI extends AFwkCktlSangriaComponent {
	
	public static final String BINDING_qualifierForGroupes = "qualifierForGroupes";
	public static final String BINDING_qualifierForResponsables = "qualifierForResponsables";
	private static final String BINDING_callbackOnAjouterResponsable = "callbackOnAjouter";
	private static final String BINDING_callbackOnSupprimerResponsable = "callbackOnSupprimer";
	
    public ResponsablesScientifiquesListeLiensUI(WOContext context) {
        super(context);
    }
    
    
    public WOActionResults onAjoutResponsable() {
    	return performParentAction((String) valueForBinding(BINDING_callbackOnAjouterResponsable));
    }
    
    public WOActionResults onSupprimerResponsable() {
    	return performParentAction((String) valueForBinding(BINDING_callbackOnSupprimerResponsable));
    }
    
    
    public NSDictionary<String, Object> srchComponentAttributes() {
    	
    	NSDictionary<String, Object> componentAttributes = new NSMutableDictionary<String, Object>();
    	
    	if(hasBinding(BINDING_qualifierForGroupes))
    		componentAttributes.put(BINDING_qualifierForGroupes, valueForBinding(BINDING_qualifierForGroupes));
    	if(hasBinding(BINDING_utilisateurPersId))
    		componentAttributes.put(BINDING_utilisateurPersId, valueForBinding(BINDING_utilisateurPersId));
    	if(hasBinding(BINDING_editingContext))
    		componentAttributes.put(BINDING_editingContext, valueForBinding(BINDING_editingContext));
    	
    	componentAttributes.put(PersonneSrch.BINDING_showIndividus, Boolean.TRUE);
    	componentAttributes.put(PersonneSrch.BINDING_showPersonnesExternes, Boolean.FALSE);
    	componentAttributes.put(PersonneSrch.BINDING_showPersonnesInternes, Boolean.TRUE);
    	componentAttributes.put(PersonneSrch.BINDING_showStructures, Boolean.FALSE);
    	if(hasBinding(BINDING_qualifierForResponsables))
    		componentAttributes.put(PersonneSrch.BINDING_qualifierForIndividus, valueForBinding(BINDING_qualifierForResponsables));
    	
    	return componentAttributes;
    	
    }
}