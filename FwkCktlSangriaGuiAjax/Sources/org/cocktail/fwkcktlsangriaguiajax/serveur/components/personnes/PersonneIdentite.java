package org.cocktail.fwkcktlsangriaguiajax.serveur.components.personnes;

import com.webobjects.appserver.WOContext;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

public class PersonneIdentite extends AFwkCktlSangriaComponent {
	
	private final static String BINDING_personne = "personne";
	
	
    public PersonneIdentite(WOContext context) {
        super(context);
    }
    
    public EOIndividu personne() {
    	return (EOIndividu) valueForBinding(BINDING_personne);
    }
    
    
    
    
    
}