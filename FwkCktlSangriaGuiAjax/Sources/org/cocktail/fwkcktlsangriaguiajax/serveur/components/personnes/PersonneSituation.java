package org.cocktail.fwkcktlsangriaguiajax.serveur.components.personnes;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.util.PersonnelRechercheUtilities;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;

public class PersonneSituation extends AFwkCktlSangriaComponent {
	
	private final static String BINDING_personne = "personne";
	
	
    public PersonneSituation(WOContext context) {
        super(context);
    }
    
    public EOIndividu personne() {
    	return (EOIndividu) valueForBinding(BINDING_personne);
    }
    
    public String employeur() {
    	return PersonnelRechercheUtilities.employeurPourPersonnel(edc(), personne());
    }
    
    public EOGrade grade() {
    	return PersonnelRechercheUtilities.gradePourPersonnel(edc(), personne());
    }
    
}