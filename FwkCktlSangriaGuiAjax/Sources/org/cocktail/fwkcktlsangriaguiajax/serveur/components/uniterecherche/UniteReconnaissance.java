package org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche;

import org.cocktail.fwkcktlrecherche.server.metier.EOLabels;
import org.cocktail.fwkcktlrecherche.server.metier.EOReconnaissanceUnite;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class UniteReconnaissance extends UniteRechercheComponentUI {
    
	private static final long serialVersionUID = 1L;
	private static final String BINDING_RECONNAISSANCE = "reconnaissance";
    private static final String BINDING_SHOW_LABEL = "showLabel";
	private EOReconnaissanceUnite reconnaissanceUnite;
	private EOLabels currentLabel;
	
	/**
	 * Constructeur
	 * @param context contexte
	 */
	public UniteReconnaissance(WOContext context) {
        super(context);
    }

	public String getDateDebutReconnaissanceId() {
		return getComponentId() + "_dateDebutReconnaissanceId";
	}
	
	public String getDateFinReconnaissanceId() {
		return getComponentId() + "_dateFinReconnaissanceId";
	}
	
	public void setReconnaissanceUnite(EOReconnaissanceUnite reconnaissanceUnite) {
		this.reconnaissanceUnite = reconnaissanceUnite;
	}

	public EOReconnaissanceUnite getReconnaissanceUnite() {
		return (EOReconnaissanceUnite) valueForBinding(BINDING_RECONNAISSANCE);
	}

	public void setCurrentLabel(EOLabels currentLabel) {
		this.currentLabel = currentLabel;
	}

	public EOLabels getCurrentLabel() {
		return currentLabel;
	}
	
	public NSArray<EOLabels> getListeDesLabels() {
		return EOLabels.fetchAll(edc());
	}
	
	public Boolean showLabel() {
		if (hasBinding(BINDING_SHOW_LABEL)) {
			return (Boolean) valueForBinding(BINDING_SHOW_LABEL);
		} else {
			return true;
		}
	}
    
}
