package org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlgedguiajax.serveur.controlers.CktlGedCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationMesr;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

import er.ajax.AjaxUpdateContainer;

public class UniteRechercheEvaluationUIM extends UniteRechercheComponentUI {
	
	private static final long serialVersionUID = 1L;
	private Integer anneeAjoutEvaluation;
	private EOEvaluationMesr currentEvaluation;
	private EOEvaluationMesr selectedEvaluation;
	private Boolean wantRefreshDocuments;
	private String currentAvisKey;
	
	private String currentNote;
	
	public static final String BINDING_UNITE_RECHERCHE = "uniteRecherche";
	private String _titreAjoutEvaluation;

    public UniteRechercheEvaluationUIM(WOContext context) {
        super(context);
    }
    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
		setWantRefreshDocuments(true);
    	super.appendToResponse(response, context);
    }
    
	public NSArray<String> notesPossibles() {
		return new NSArray<String>("A+", "A", "B", "C", "D");
	}
	
	
	public void setCurrentNote(String currentNote) {
		this.currentNote = currentNote;
	}

	public String currentNote() {
		return currentNote;
	}

	public EOStructure uniteRecherche() {
		return (EOStructure) valueForBinding(BINDING_UNITE_RECHERCHE);
	}
    
    public String ajoutEvaluationWindowId() {
    	return getComponentId() + "_ajoutEvaluationWindow";
    }
    
    public String evaluationSelectionContainerId() {
    	return getComponentId() + "_evaluationSelectionContainer";
    }

    public String evaluationContainerId() {
    	return getComponentId() + "_evaluationContainer";
    }
    
    public String ajouterRapportAeresWindowId() {
    	return getComponentId() + "_ajouterRapportAeresWindow";
    }
    
    public String ajouterReponseAeresWindowId() {
    	return getComponentId() + "_ajouterReponseAeresWindow";
    }
    
    public String ajouterAvisDefMstpWindowId() {
    	return getComponentId() + "_ajouterAvisDefMstpWindow";
    }
    
    
    public NSArray<EOEvaluationMesr> evaluationsPourUniteRecherche() {
    	return StructuresRechercheHelper.evaluationsPourUniteRecherche(edc(), uniteRecherche(), true);
    }
    
    
    /************************************/
    /*** Selection des evaluations ***/
    
	public void setCurrentEvaluation(EOEvaluationMesr currentEvaluation) {
		this.currentEvaluation = currentEvaluation;
	}

	public EOEvaluationMesr currentEvaluation() {
		return currentEvaluation;
	}

	public void setSelectedEvaluation(EOEvaluationMesr selectedEvaluation) {
		this.selectedEvaluation = selectedEvaluation;
	}

	public EOEvaluationMesr selectedEvaluation() {
		return selectedEvaluation;
	}

	/************************************/
    /*** Ajout des evaluations ***/
    
	public void setAnneeAjoutEvaluation(Integer anneeAjoutEvaluation) {
		this.anneeAjoutEvaluation = anneeAjoutEvaluation;
	}

	public Integer anneeAjoutEvaluation() {
		return anneeAjoutEvaluation;
	}

	public WOActionResults ajouterNouvelleEvaluation() {
	  if (anneeAjoutEvaluation() == null || getTitreAjoutEvaluation() == null) {
	    session().addSimpleErrorMessage("Erreur", "Vous devez renseigner l'année et le titre");
	    return doNothing();
	  }
		EOEvaluationMesr evaluationMesr = EOEvaluationMesr.creerInstance(edc());
		evaluationMesr.setAnnee(anneeAjoutEvaluation());
		evaluationMesr.setTitre(getTitreAjoutEvaluation());
		evaluationMesr.setDCreation(DateCtrl.getDateJour());
		evaluationMesr.setDModification(DateCtrl.getDateJour());
		evaluationMesr.setPersIdCreation(getUtilisateurPersId());
		evaluationMesr.setPersIdModification(getUtilisateurPersId());
		evaluationMesr.setUniteRelationship(uniteRecherche());	
		AjaxUpdateContainer.updateContainerWithID(evaluationContainerId(), context());
		AjaxUpdateContainer.updateContainerWithID(evaluationSelectionContainerId(), context());
		CktlAjaxWindow.close(context());
		return doNothing();
	}
		
	/***********************************/
	/*** Détail des evaluations ***/
	
    
    public void setCurrentAvisKey(String currentAvisKey) {
		this.currentAvisKey = currentAvisKey;
	}

	public String currentAvisKey() {
		return currentAvisKey;
	}   
	
	public NSDictionary<String, String> listeDesAvis() {
		return EOAap.Nomenclatures.getListeDesAvis();	
	}
	
	public String currentAvisDisplayString() {	
		return listeDesAvis().objectForKey(currentAvisKey()); 
	}

	
	
	
	/**********************************/
	/*** GED RAPPORT AERES***/
	
	public EODocument rapportAeres() {
		return currentEvaluation().rapportAeres();
	}
	
	public void setRapportAeres(EODocument rapportAeres) {
		currentEvaluation().setRapportAeresRelationship(rapportAeres);
	}
	
	public WOActionResults validerAjoutRapportAeres() {
		CktlAjaxWindow.close(context(), ajouterRapportAeresWindowId()); 
		return doNothing();
	}
	
	public WOActionResults annulerAjoutRapportAeres() {
		CktlAjaxWindow.close(context(), ajouterRapportAeresWindowId());
		return doNothing();
	}
	
    public WOActionResults supprimerRapportAeres() {
    	CktlGedCtrl.supprimerUnDocument(rapportAeres());
    	currentEvaluation().setRapportAeresRelationship(null);
    	return doNothing();
    }
	
	/**********************************/
	/*** GED REPONSE AERES***/
	
	public EODocument reponseAeres() {
		return currentEvaluation().reponseAeres();
	}
	
	public void setReponseAeres(EODocument reponseAeres) {
		currentEvaluation().setReponseAeresRelationship(reponseAeres);
	}
	
	public WOActionResults validerAjoutReponseAeres() {
		CktlAjaxWindow.close(context());
		return doNothing();
	}
	
	public WOActionResults annulerAjoutReponseAeres() {
		CktlAjaxWindow.close(context());
		return doNothing();
	}
	
    public WOActionResults supprimerReponseAeres() {
    	CktlGedCtrl.supprimerUnDocument(reponseAeres());
    	currentEvaluation().setReponseAeresRelationship(null);
    	return doNothing();
    }
	
	/**********************************/
	/*** GED AVIS MSTP ***/
	
	public EODocument avisDefMstp() {
		return currentEvaluation().avisDefinitifMstp();
	}
	
	public void setAvisDefMstp(EODocument avisDefMstp) {
		currentEvaluation().setAvisDefinitifMstpRelationship(avisDefMstp);
	}
	
	public WOActionResults validerAjoutAvisDefMstp() {
		CktlAjaxWindow.close(context());
		return doNothing();
	}
	
	public WOActionResults annulerAjoutAvisDefMstp() {
		CktlAjaxWindow.close(context());
		return doNothing();
	}
	
    public WOActionResults supprimerAvisDefMstp() {
    	CktlGedCtrl.supprimerUnDocument(avisDefMstp());
    	currentEvaluation().setAvisDefinitifMstpRelationship(null);
    	return doNothing();
    }

    /**
     * @return the titreAjoutEvaluation
     */
    public String getTitreAjoutEvaluation() {
      return _titreAjoutEvaluation;
    }

    /**
     * @param titreAjoutEvaluation the titreAjoutEvaluation to set
     */
    public void setTitreAjoutEvaluation(String titreAjoutEvaluation) {
      _titreAjoutEvaluation = titreAjoutEvaluation;
    }

    public WOActionResults supprimerEvaluation() {
      if (selectedEvaluation() == null) {
        session().addSimpleInfoMessage(null, "Aucune évaluation supprimée");
        return doNothing();
      }
      selectedEvaluation().delete();
      try {
        edc().saveChanges();
        AjaxUpdateContainer.updateContainerWithID(evaluationContainerId(), context());
        setSelectedEvaluation(null);
      } catch (ValidationException e) {
        session().addSimpleErrorMessage("Erreur", e);
      }
      return doNothing();
    }

    public WOActionResults ajoutEvaluation() {
      setAnneeAjoutEvaluation(null);
      setTitreAjoutEvaluation(null);
      return doNothing();
    }
	
    
    public Boolean uneEvaluationEstSelectionnee() {
    	return selectedEvaluation() != null; 
    }

	public Boolean getWantRefreshDocuments() {
		return wantRefreshDocuments;
	}

	public void setWantRefreshDocuments(Boolean wantRefreshDocuments) {
		this.wantRefreshDocuments = wantRefreshDocuments;
	}
	
	public WOActionResults voirEvaluation() {
		setWantRefreshDocuments(true);
		return doNothing();
	}
}
