package org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.EOIndividuDiplome;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividusGradesService;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau;
import org.cocktail.fwkcktlpersonne.common.metier.EODiplome;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.SPgetCGradeAgent;
import org.cocktail.fwkcktlrecherche.server.util.PersonnelRechercheUtilities;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXTimestampUtilities;

public class UniteRecherchePersonnelsUIM extends AFwkCktlSangriaComponent {

	private final static String BINDING_uniteRecherche = "uniteRecherche";

	private final static String KEY_NOINDIVIDU = "NOINDIVIDU";
	private final static String KEY_NOM = "NOM";
	private final static String KEY_PRENOM = "PRENOM";
	private final static String KEY_GRADE = "GRADE";
	private final static String KEY_ASSCODE = "ASSID";
	private final static String KEY_STATUT = "STATUT";
	private final static String KEY_DEBUT = "DEBUT";
	private final static String KEY_FIN = "FIN";
	private final static String KEY_HDR = "HDR";

	private Integer etapeCourante = 1;

	private NSArray<NSDictionary<String, Object>> membresPourUniteRows = null;
	private ERXDisplayGroup<NSDictionary<String, Object>> membresUniteRowsDisplayGroup = null;
	private EOArrayDataSource membresUniteRowsDataSource = null;
	private NSDictionary<String, Object> selectedMembreRow = null;
	private NSDictionary<String, Object> currentMembreRow = null;
	private EOEditingContext editingContextPourEdition = null;

	private NSArray<EOAssociation> statutsMembres = null;
	private EOAssociation currentStatutMembre;
	private EOAssociation editingStatutMembre;

	private EOIndividu nouveauMembre;

	private NSTimestamp editingDateDebut;
	private NSTimestamp editingDateFin;

	private EOAssociation selectedMembreStatut = null;
	private NSTimestamp selectedMembreDebut = null;
	private NSTimestamp selectedMembreFin = null;

	private Boolean showDetails = true;

	private final static Integer AFFICHAGE_GLOBAL = 0;
	private final static Integer AFFICHAGE_PAR_EQUIPE = 1;

	private Integer modeAffichage = AFFICHAGE_GLOBAL;

	private EOStructure currentEquipeRecherche;

	private Boolean wantMembreSrchReset;

	private String _filterNomToken;

	/***** DEBUT ATTRIBUTS NOUVELLE IMPLEMENTATION ******/
	
	
	
	/***** FIN ATTRIBUTS NOUVELLE IMPLEMENTATION *****/
	
	
	public UniteRecherchePersonnelsUIM(WOContext context) {
		super(context);
	}

	public String membresTableViewId() {
		return getComponentId() + "_membresTableView";
	}

	public String ajouterMembreWindowId() {
		return getComponentId() + "_ajouterMembreWindow";
	}

	public String boutonsEtapeUneContainerId() {
		return getComponentId() + "_boutonsEtapeUneContainer";
	}

	public String etapesContainerId() {
		return getComponentId() + "_etapesContainer";
	}

	public String membresTableViewContainerId() {
		return getComponentId() + "_membresTableViewContainer";
	}

	public String boutonsEtapeDeuxContainerId() {
		return getComponentId() + "_boutonsEtapeDeuxContainer";
	}

	public String boutonsContainerId() {
		return getComponentId() + "_boutonsContainer";
	}

	public String modifierMembreWindowId() {
		return getComponentId() + "_modifierMembreWindow";
	}

	public String equipesRechercheContainerId() {
		return getComponentId() + "_equipesRechercheContainer";
	}

	public EOStructure uniteRecherche() {
		return (EOStructure) valueForBinding(BINDING_uniteRecherche);
	}

	public NSArray<EOAssociation> statutsMembres() {
		if (statutsMembres == null) {
			EOAssociation association = getFactoryAssociation()
					.typesMembresUniteRechercheAssociation(
							editingContextPourEdition());
			statutsMembres = association.getFils(editingContextPourEdition());
		}
		return statutsMembres;
	}

	public Boolean isModeAffichageGlobal() {
		return modeAffichage.equals(AFFICHAGE_GLOBAL);
	}

	public Boolean isModeAffichageParEquipe() {
		return modeAffichage.equals(AFFICHAGE_PAR_EQUIPE);
	}

	public WOActionResults passerVersAffichageGlobal() {
		try {
			edc().saveChanges();
			resetMembresRowsData();
			modeAffichage = AFFICHAGE_GLOBAL;
			AjaxUpdateContainer.updateContainerWithID(boutonsContainerId(),
					context());
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}

		return doNothing();
	}

	public WOActionResults passerVersAffichageParEquipes() {

		try {
			edc().saveChanges();
			resetMembresRowsData();
			modeAffichage = AFFICHAGE_PAR_EQUIPE;
			AjaxUpdateContainer.updateContainerWithID(boutonsContainerId(),
					context());
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}

		return doNothing();
	}

	public Boolean etapeUne() {
		return etapeCourante == 1;
	}

	public Boolean etapeDeux() {
		return etapeCourante == 2;
	}

	public void resetAjoutMembreWindow() {
		setWantMembreSrchReset(true);
		setEditingDateDebut(null);
		setEditingDateFin(null);
		setEditingStatutMembre(null);
		etapeCourante = 1;
	}

	public Boolean wantMembreSrchReset() {
		return wantMembreSrchReset;
	}

	public void setWantMembreSrchReset(Boolean wantMembreSrchReset) {
		this.wantMembreSrchReset = wantMembreSrchReset;
	}

	public WOActionResults ajouterMembre() {
		resetAjoutMembreWindow();
		return doNothing();
	}

	public void setNouveauMembre(EOIndividu nouveauMembre) {
		this.nouveauMembre = nouveauMembre;
	}

	public EOIndividu nouveauMembre() {
		return this.nouveauMembre;
	}

	public WOActionResults passageEtapeDeux() {
		if (nouveauMembre() == null) {
			session().addSimpleErrorMessage("Erreur",
					"Vous devez choisir un individu");
			return doNothing();
		}
		etapeCourante = 2;

		return doNothing();
	}

	public WOActionResults retourEtapeUne() {
		etapeCourante = 1;
		return doNothing();
	}

	public WOActionResults annulerAjoutMembre() {
		editingContextPourEdition().revert();
		CktlAjaxWindow.close(context(), ajouterMembreWindowId());
		return doNothing();
	}

	public WOActionResults validerAjoutMembre() {

		if (editingDateDebut() == null && editingDateFin() != null) {
			session()
					.addSimpleErrorMessage("Erreur",
							"Si la date de fin est renseignée, la date de début doit l'être aussi");
			return doNothing();
		}
		if (editingDateDebut() != null && editingDateFin() != null
				&& editingDateDebut().after(editingDateFin())) {
			session().addSimpleErrorMessage("Erreur",
					"La date de fin doit être postérieure à la date de début");
			return doNothing();
		}
		if (editingStatutMembre() == null && editingDateDebut() != null) {
			session().addSimpleErrorMessage("Erreur",
					"Si la date est renseignée, vous devez choisir le statut");
			return doNothing();
		}

		PersonnelRechercheUtilities.affecterPersonneDansStructureRecherche(
				editingContextPourEdition(), nouveauMembre(), uniteRecherche()
						.localInstanceIn(editingContextPourEdition()),
				editingDateDebut(), editingDateFin(), editingStatutMembre(),
				getUtilisateurPersId());

		try {
			// nouveauMembre().setValidationEditingContext(editingContextPourEdition());
			// nouveauMembre().setPersIdModification(utilisateurPersId());
			editingContextPourEdition().saveChanges();
			ajouterMembreRow(nouveauMembre(), editingStatutMembre(),
					editingDateDebut(), editingDateFin());
			membresUniteRowsDisplayGroup().fetch();
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			return doNothing();
		}

		CktlAjaxWindow.close(context(), ajouterMembreWindowId());

		return doNothing();
	}

	public WOActionResults modifierMembre() {

		resetModificationMembreWindow();

		if (selectedMembreRow().valueForKey(KEY_ASSCODE) != NSKeyValueCoding.NullValue) {
			String _selectedMembreStatutCode = (String) selectedMembreRow()
					.valueForKey(KEY_ASSCODE);
			EOAssociation _selectedMembreStatut = EOAssociation
					.fetchFirstByQualifier(editingContextPourEdition(),
							EOAssociation.ASS_CODE
									.eq(_selectedMembreStatutCode));
			setEditingStatutMembre(_selectedMembreStatut);
			setSelectedMembreStatut(_selectedMembreStatut);
		} else {
			setSelectedMembreStatut(null);
		}

		if (selectedMembreRow().valueForKey(KEY_DEBUT) != NSKeyValueCoding.NullValue) {
			NSTimestamp _selectedMembreDebut = (NSTimestamp) selectedMembreRow()
					.valueForKey(KEY_DEBUT);
			setEditingDateDebut(_selectedMembreDebut);
			setSelectedMembreDebut(_selectedMembreDebut);
		} else {
			setSelectedMembreDebut(null);
		}

		if (selectedMembreRow().valueForKey(KEY_FIN) != NSKeyValueCoding.NullValue) {
			NSTimestamp _selectedMembreFin = (NSTimestamp) selectedMembreRow()
					.valueForKey(KEY_FIN);
			setEditingDateFin(_selectedMembreFin);
			setSelectedMembreFin(_selectedMembreFin);
		} else {
			setSelectedMembreFin(null);
		}

		return doNothing();
	}

	public void resetModificationMembreWindow() {
		setEditingDateDebut(null);
		setEditingDateFin(null);
		setEditingStatutMembre(null);
	}

	public WOActionResults annulerModificationMembre() {
		editingContextPourEdition().revert();
		CktlAjaxWindow.close(context(), modifierMembreWindowId());
		return doNothing();
	}

	public WOActionResults validerModificationMembre() {

		if (editingDateDebut() == null && editingDateFin() != null) {
			session()
					.addSimpleErrorMessage("Erreur",
							"Si la date de fin est renseignée, la date de début doit l'être aussi");
			return doNothing();
		}
		if (editingDateDebut() != null && editingDateFin() != null
				&& editingDateDebut().after(editingDateFin())) {
			session().addSimpleErrorMessage("Erreur",
					"La date de fin doit être postérieure à la date de début");
			return doNothing();
		}
		if (editingStatutMembre() == null) {
			session().addSimpleErrorMessage("Erreur",
					"Le statut doit être renseigné");
			return doNothing();
		}

		Long _selectedMembreNoIndividu = (Long) selectedMembreRow()
				.valueForKey(KEY_NOINDIVIDU);
		EOIndividu _selectedMembre = EOIndividu
				.fetchFirstByQualifier(editingContextPourEdition(),
						EOIndividu.NO_INDIVIDU.eq(_selectedMembreNoIndividu
								.intValue()));

		if (selectedMembreStatut() != null) {
			// On ne fait l'edition dans le nested mais dans le principal
			// directement
			EOQualifier _qualifierForRepartAssociation = ERXQ.and(
					EORepartAssociation.TO_ASSOCIATION
							.eq(selectedMembreStatut().localInstanceIn(edc())),
					EORepartAssociation.TO_STRUCTURE.eq(uniteRecherche()),
					EORepartAssociation.PERS_ID.eq(_selectedMembre.persId()));

			ERXFetchSpecification<EORepartAssociation> _fs = new ERXFetchSpecification<EORepartAssociation>(
					EORepartAssociation.ENTITY_NAME,
					_qualifierForRepartAssociation, null);
			_fs.setIncludeEditingContextChanges(true);
			NSArray<EORepartAssociation> _resultats = edc()
					.objectsWithFetchSpecification(_fs);

			if (!_resultats.isEmpty()) {
				EORepartAssociation _resultat = _resultats.get(0);
				_resultat.setToAssociationRelationship(editingStatutMembre()
						.localInstanceIn(edc()));
				_resultat.setRasDOuverture(editingDateDebut());
				_resultat.setRasDFermeture(editingDateFin());
			}
		} else {
			EORepartAssociation _nouveauStatut = EORepartAssociation
					.creerInstance(editingContextPourEdition());
			_nouveauStatut.setToAssociationRelationship(editingStatutMembre());
			_nouveauStatut.setRasDOuverture(editingDateDebut());
			_nouveauStatut.setRasDFermeture(editingDateFin());
			_nouveauStatut.setToStructureRelationship(uniteRecherche()
					.localInstanceIn(editingContextPourEdition()));
			_nouveauStatut.setToPersonne(_selectedMembre);
			_nouveauStatut.setPersIdCreation(getUtilisateurPersId());
			_nouveauStatut.setPersIdModification(getUtilisateurPersId());
			_nouveauStatut.setDCreation(ERXTimestampUtilities.today());
			_nouveauStatut.setDModification(ERXTimestampUtilities.today());
			// EOStructureForGroupeSpec.definitUnRole(editingContextPourEdition(),
			// _selectedMembre, editingStatutMembre(),
			// uniteRecherche().localInstanceIn(editingContextPourEdition()),
			// utilisateurPersId(), editingDateDebut(), editingDateFin(), null,
			// null, null, false);
			try {
				// _selectedMembre.setValidationEditingContext(editingContextPourEdition());
				// _selectedMembre.setPersIdModification(utilisateurPersId());
				editingContextPourEdition().saveChanges();
			} catch (ValidationException e) {
				session().addSimpleErrorMessage("Erreur", e);
				return doNothing();
			}
		}

		selectedMembreRow().takeValueForKey(editingStatutMembre().assCode(),
				KEY_ASSCODE);
		selectedMembreRow().takeValueForKey(editingStatutMembre().assLibelle(),
				KEY_STATUT);
		selectedMembreRow().takeValueForKey(editingDateDebut(), KEY_DEBUT);
		selectedMembreRow().takeValueForKey(editingDateFin(), KEY_FIN);

		membresUniteRowsDisplayGroup().fetch();

		CktlAjaxWindow.close(context(), modifierMembreWindowId());
		return doNothing();
	}

	public Boolean isModifierDisabled() {
		return selectedMembreRow() == null;
	}

	public WOActionResults supprimerMembre() {

		Long _selectedMembreNoIndividu = (Long) selectedMembreRow()
				.valueForKey(KEY_NOINDIVIDU);
		EOIndividu _selectedMembre = EOIndividu
				.fetchFirstByQualifier(editingContextPourEdition(),
						EOIndividu.NO_INDIVIDU.eq(_selectedMembreNoIndividu
								.intValue()));

		if (selectedMembreRow().valueForKey(KEY_ASSCODE) != NSKeyValueCoding.NullValue) {
			String _selectedMembreStatutCode = (String) selectedMembreRow()
					.valueForKey(KEY_ASSCODE);
			EOAssociation _selectedMembreStatut = EOAssociation
					.fetchFirstByQualifier(editingContextPourEdition(),
							EOAssociation.ASS_CODE
									.eq(_selectedMembreStatutCode));
			setEditingStatutMembre(_selectedMembreStatut);
			setSelectedMembreStatut(_selectedMembreStatut);
		} else {
			setSelectedMembreStatut(null);
		}

		if (selectedMembreStatut() != null) {
			// On ne fait l'edition dans le nested mais dans le principal
			// directement
			EOQualifier _qualifierForRepartAssociation = ERXQ.and(
					EORepartAssociation.TO_ASSOCIATION
							.eq(selectedMembreStatut().localInstanceIn(edc())),
					EORepartAssociation.TO_STRUCTURE.eq(uniteRecherche()),
					EORepartAssociation.PERS_ID.eq(_selectedMembre.persId()));

			ERXFetchSpecification<EORepartAssociation> _fs = new ERXFetchSpecification<EORepartAssociation>(
					EORepartAssociation.ENTITY_NAME,
					_qualifierForRepartAssociation, null);
			_fs.setIncludeEditingContextChanges(true);
			NSArray<EORepartAssociation> _resultats = edc()
					.objectsWithFetchSpecification(_fs);

			if (!_resultats.isEmpty()) {
				EORepartAssociation _resultat = _resultats.get(0);
				/*
				 * _resultat.setToAssociationRelationship(null);
				 * _resultat.setToPersonne(null);
				 * _resultat.setToStructureRelationship(null);
				 */
				edc().deleteObject(_resultat);
			}
		}

		EOQualifier _qualifierForRepartStructure = ERXQ.and(
				EORepartStructure.TO_STRUCTURE_GROUPE.eq(uniteRecherche()),
				EORepartStructure.PERS_ID.eq(_selectedMembre.persId()));

		ERXFetchSpecification<EORepartStructure> _fs = new ERXFetchSpecification<EORepartStructure>(
				EORepartStructure.ENTITY_NAME, _qualifierForRepartStructure,
				null);
		_fs.setIncludeEditingContextChanges(true);
		NSArray<EORepartStructure> _resultats = edc()
				.objectsWithFetchSpecification(_fs);

		if (!_resultats.isEmpty()) {
			EORepartStructure _resultat = _resultats.get(0);
			/*
			 * _resultat.setToStructureGroupeRelationship(null);
			 * _resultat.setToPersonneElt(null);
			 */
			edc().deleteObject(_resultat);
		}

		membresUniteRowsDataSource().deleteObject(selectedMembreRow());
		membresUniteRowsDisplayGroup().fetch();

		return doNothing();
	}

	public WOActionResults switchShowDetails() {
		showDetails = !showDetails;

		if (!showDetails) {
			membresUniteRowsDisplayGroup().setQualifier(
					membresActuelsQualifier());
		} else {
			membresUniteRowsDisplayGroup().setQualifier(null);
		}
		membresUniteRowsDisplayGroup().fetch();

		AjaxUpdateContainer.updateContainerWithID(
				membresTableViewContainerId(), context());
		AjaxUpdateContainer.updateContainerWithID(boutonsContainerId(),
				context());
		return doNothing();
	}

	private EOQualifier membresActuelsQualifier() {
		return ERXQ.and(ERXQ.or(
				ERXQ.lessThanOrEqualTo(KEY_DEBUT, DateCtrl.now()),
				ERXQ.isNull(KEY_DEBUT)), ERXQ.or(
				ERXQ.greaterThanOrEqualTo(KEY_FIN, DateCtrl.now()),
				ERXQ.isNull(KEY_FIN)));
	}

	public Boolean showDetails() {
		return showDetails;
	}

	// EQUIPES DE RECHERCHE

	public void setCurrentEquipeRecherche(EOStructure currentEquipeRecherche) {
		this.currentEquipeRecherche = currentEquipeRecherche;
	}

	public EOStructure currentEquipeRecherche() {
		return currentEquipeRecherche;
	}

	public NSArray<EORepartAssociation> tousRepartsEquipes() {

		EOQualifier qualifier = ERXQ.and(ERXQ.in(
				EORepartAssociation.TO_STRUCTURE_KEY,
				equipesRecherchePourUniteRecherche()), ERXQ.or(ERXQ.equals(
				EORepartAssociation.TO_ASSOCIATION_KEY, getFactoryAssociation()
						.membrePermanentAssociation(edc())), ERXQ.equals(
				EORepartAssociation.TO_ASSOCIATION_KEY, getFactoryAssociation()
						.membreNonPermanentAssociation(edc()))));

		if (!showDetails) {
			EOQualifier dateQualifier = ERXQ.and(ERXQ.lessThanOrEqualTo(
					EORepartAssociation.RAS_D_OUVERTURE_KEY,
					MyDateCtrl.getDateJour()), ERXQ.or(ERXQ.greaterThan(
					EORepartAssociation.RAS_D_FERMETURE_KEY,
					MyDateCtrl.getDateJour()), ERXQ
					.isNull(EORepartAssociation.RAS_D_FERMETURE_KEY)));
			qualifier = ERXQ.and(qualifier, dateQualifier);
		}

		NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(
				EOSortOrdering.sortOrderingWithKey(
						EORepartAssociation.RAS_D_OUVERTURE_KEY,
						EOSortOrdering.CompareDescending));
		ERXFetchSpecification<EORepartAssociation> fetchSpecification = new ERXFetchSpecification<EORepartAssociation>(
				EORepartAssociation.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpecification.setIncludeEditingContextChanges(true);

		NSArray<EORepartAssociation> repartsMembres = fetchSpecification
				.fetchObjects(edc());

		return repartsMembres;

	}

	public Boolean currentMembreInEquipes() {
		return true;
	}

	public Boolean hasEquipes() {
		return !equipesRecherchePourUniteRecherche().isEmpty();
	}

	public NSArray<EOStructure> equipesRecherchePourUniteRecherche() {
		return StructuresRechercheHelper.equipesDeRecherchePourUniteRecherche(
				edc(), uniteRecherche());
	}

	public String membresUniteTableViewHeight() {
		if (isModeAffichageParEquipe()) {
			return "150px";
		} else {
			return "450px";
		}
	}

	private String requeteMembresPourUnite() {

		String assCodes = "'"
				+ ((NSArray<String>) statutsMembres().valueForKey(
						EOAssociation.ASS_CODE_KEY))
						.componentsJoinedByString("','") + "'";
		String requete = "select i.no_individu as "
				+ KEY_NOINDIVIDU
				+ ", i.nom_affichage  as "
				+ KEY_NOM
				+ ", i.prenom_affichage  as "
				+ KEY_PRENOM
				+ ", g.ll_grade  as "
				+ KEY_GRADE
				+ ", (select a.ass_code from association a where a.ass_id = ra.ass_id)  as "
				+ KEY_ASSCODE
				+ ", (select a.ass_libelle from association a where a.ass_id = ra.ass_id)  as "
				+ KEY_STATUT
				+ ", ra.ras_d_ouverture  as "
				+ KEY_DEBUT
				+ ", ra.ras_d_fermeture  as "
				+ KEY_FIN
				+ ", idip.D_DIPLOME as "
				+ KEY_HDR
				+ " from (select pers_id, c_structure from repart_structure union select pers_id, c_structure from repart_association where ass_id in (select a.ass_id from association a where a.ass_code in ("
				+ assCodes
				+ "))) rs"
				+ " inner join individu_ulr i on rs.pers_id = i.pers_id and i.tem_valide = 'O' and rs.c_structure = '"
				+ uniteRecherche().cStructure()
				+ "'"
				+ " left outer join grade g on g.c_grade = MANGUE.GET_C_GRADE_AGENT(i.no_individu)"
				+ " left outer join repart_association ra on ra.pers_id = i.pers_id and ra.c_structure = rs.c_structure"
				+ " and ra.ass_id in (select a.ass_id from association a where a.ass_code in ("
				+ assCodes
				+ "))"
				+ " left outer join mangue.individu_diplomes idip on i.no_individu = idip.no_individu and c_diplome = '0731000'";

		if (modeAffichage.equals(AFFICHAGE_PAR_EQUIPE)) {
			requete += " where i.no_individu not in (select distinct(i.no_individu) from individu_ulr i"
					+ " inner join repart_structure rs on i.pers_id = rs.pers_id"
					+ " inner join structure_ulr s on rs.c_structure in ("
					+ " select s.c_structure from structure_ulr s"
					+ " inner join repart_association ra on s.pers_id = ra.pers_id and ra.c_structure = '"
					+ uniteRecherche().cStructure()
					+ "'"
					+ " and ra.ass_id = (select ass_id from association where association.ass_code = '"
					+ getFactoryAssociation().equipeDeRechercheAssociation(edc())
							.assCode() + "')))";
		}

		return requete;

	}

	public NSArray<NSDictionary<String, Object>> membresPourUniteRows() {
		if (membresPourUniteRows == null) {
			membresPourUniteRows = EOUtilities.rawRowsForSQL(edc(),
					"FwkCktlPersonne", requeteMembresPourUnite(), null);
		}
		return membresPourUniteRows;
	}

	public EOArrayDataSource membresUniteRowsDataSource() {
		if (membresUniteRowsDataSource == null) {
			membresUniteRowsDataSource = new EOArrayDataSource(
					EOClassDescription
							.classDescriptionForClass(NSDictionary.class),
					null);
			membresUniteRowsDataSource.setArray(membresPourUniteRows());
		}
		return membresUniteRowsDataSource;
	}

	public ERXDisplayGroup<NSDictionary<String, Object>> membresUniteRowsDisplayGroup() {

		if (membresUniteRowsDisplayGroup == null) {

			membresUniteRowsDisplayGroup = new ERXDisplayGroup<NSDictionary<String, Object>>();
			membresUniteRowsDisplayGroup
					.setDataSource(membresUniteRowsDataSource());
			membresUniteRowsDisplayGroup
					.setDelegate(new MembresUniteRowsDisplayGroupDelegate());
			membresUniteRowsDisplayGroup
					.setQualifier(membresActuelsQualifier());
			membresUniteRowsDisplayGroup.fetch();
		}
		return membresUniteRowsDisplayGroup;
	}

	public void resetMembresRowsData() {
		membresPourUniteRows = null;
		membresUniteRowsDataSource = null;
		membresUniteRowsDisplayGroup = null;
	}

	public NSDictionary<String, Object> selectedMembreRow() {
		return selectedMembreRow;
	}

	public void setSelectedMembreRow(
			NSDictionary<String, Object> selectedMembreRow) {
		this.selectedMembreRow = selectedMembreRow;
	}

	public NSDictionary<String, Object> currentMembreRow() {
		return currentMembreRow;
	}

	public void setCurrentMembreRow(
			NSDictionary<String, Object> currentMembreRow) {
		this.currentMembreRow = currentMembreRow;
	}

	public void ajouterMembreRow(EOIndividu individu, EOAssociation statut,
			NSTimestamp debut, NSTimestamp fin) {

		NSDictionary<String, Object> _membreRow = new NSMutableDictionary<String, Object>();

		_membreRow.takeValueForKey(individu.noIndividu().longValue(),
				KEY_NOINDIVIDU);
		_membreRow.takeValueForKey(individu.nomAffichage(), KEY_NOM);
		_membreRow.takeValueForKey(individu.prenomAffichage(), KEY_PRENOM);

		try {
			String _codeGrade = SPgetCGradeAgent.getCGradeForAgent(
					editingContextPourEdition(), individu.noIndividu());
			if (_codeGrade != null) {
				EOGrade _grade = EOGrade.fetchFirstByQualifier(
						editingContextPourEdition(),
						ERXQ.equals(EOGrade.C_GRADE_KEY, _codeGrade));
				_membreRow.takeValueForKey(_grade.llGrade(), KEY_GRADE);
			}
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
		if (statut != null) {
			_membreRow.takeValueForKey(statut.assCode(), KEY_ASSCODE);
			_membreRow.takeValueForKey(statut.assLibelle(), KEY_STATUT);
			_membreRow.takeValueForKey(debut, KEY_DEBUT);
			_membreRow.takeValueForKey(fin, KEY_FIN);
		}

		membresUniteRowsDataSource().insertObject(_membreRow);

	}

	public String currentMembreLibelle() {
		String nom = (String) currentMembreRow().valueForKey(KEY_NOM);
		String prenom = (String) currentMembreRow().valueForKey(KEY_PRENOM);
		return nom + " " + prenom;
	}

	public String currentMembreGrade() {
		if (currentMembreRow().valueForKey(KEY_GRADE) == NSKeyValueCoding.NullValue) {
			return "-";
		}
		return (String) currentMembreRow().valueForKey(KEY_GRADE);
	}

	public String currentMembreStatut() {
		if (currentMembreRow().valueForKey(KEY_STATUT) == NSKeyValueCoding.NullValue) {
			return "-";
		}
		return (String) currentMembreRow().valueForKey(KEY_STATUT);
	}

	public NSTimestamp currentMembreDebut() {
		if (currentMembreRow().valueForKey(KEY_DEBUT) == NSKeyValueCoding.NullValue) {
			return null;
		}
		return (NSTimestamp) currentMembreRow().valueForKey(KEY_DEBUT);
	}

	public NSTimestamp currentMembreFin() {
		if (currentMembreRow().valueForKey(KEY_FIN) == NSKeyValueCoding.NullValue) {
			return null;
		}
		return (NSTimestamp) currentMembreRow().valueForKey(KEY_FIN);
	}

	public NSTimestamp currentMembreHdr() {
		if (currentMembreRow().valueForKey(KEY_HDR) == NSKeyValueCoding.NullValue) {
			return null;
		}
		return (NSTimestamp) currentMembreRow().valueForKey(KEY_HDR);
	}

	public class MembresUniteRowsDisplayGroupDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			ERXDisplayGroup<NSDictionary<String, Object>> membresRowsGroup = (ERXDisplayGroup<NSDictionary<String, Object>>) group;
			if (membresRowsGroup.selectedObject() != null) {
				setSelectedMembreRow(membresRowsGroup.selectedObject());
			}
		}
	}

	public EOEditingContext editingContextPourEdition() {
		if (editingContextPourEdition == null) {
			editingContextPourEdition = ERXEC.newEditingContext(edc());
		}
		return editingContextPourEdition;
	}

	/**
	 * @return the editingStatutMembre
	 */
	public EOAssociation editingStatutMembre() {
		return editingStatutMembre;
	}

	/**
	 * @param editingStatutMembre
	 *            the editingStatutMembre to set
	 */
	public void setEditingStatutMembre(EOAssociation editingStatutMembre) {
		this.editingStatutMembre = editingStatutMembre;
	}

	/**
	 * @return the editingDateDebut
	 */
	public NSTimestamp editingDateDebut() {
		return editingDateDebut;
	}

	/**
	 * @param editingDateDebut
	 *            the editingDateDebut to set
	 */
	public void setEditingDateDebut(NSTimestamp editingDateDebut) {
		this.editingDateDebut = editingDateDebut;
	}

	/**
	 * @return the editingDateFin
	 */
	public NSTimestamp editingDateFin() {
		return editingDateFin;
	}

	/**
	 * @param editingDateFin
	 *            the editingDateFin to set
	 */
	public void setEditingDateFin(NSTimestamp editingDateFin) {
		this.editingDateFin = editingDateFin;
	}

	public EOAssociation currentStatutMembre() {
		return currentStatutMembre;
	}

	public void setCurrentStatutMembre(EOAssociation currentStatutMembre) {
		this.currentStatutMembre = currentStatutMembre;
	}

	public EOAssociation selectedMembreStatut() {
		return selectedMembreStatut;
	}

	public void setSelectedMembreStatut(EOAssociation selectedMembreStatut) {
		this.selectedMembreStatut = selectedMembreStatut;
	}

	public NSTimestamp selectedMembreDebut() {
		return selectedMembreDebut;
	}

	public void setSelectedMembreDebut(NSTimestamp selectedMembreDebut) {
		this.selectedMembreDebut = selectedMembreDebut;
	}

	public NSTimestamp selectedMembreFin() {
		return selectedMembreFin;
	}

	public void setSelectedMembreFin(NSTimestamp selectedMembreFin) {
		this.selectedMembreFin = selectedMembreFin;
	}

	/**
	 * @return the filterNomToken
	 */
	public String getFilterNomToken() {
		return _filterNomToken;
	}

	/**
	 * @param filterNomToken
	 *            the filterNomToken to set
	 */
	public void setFilterNomToken(String filterNomToken) {
		_filterNomToken = filterNomToken;
	}

	public WOActionResults onFilterNomChange() {
		return doNothing();
	}

	/***** DEBUT METHODES NOUVELLE IMPLEMENTATION ******/


}