package org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlged.serveur.metier.Configuration;
import org.cocktail.fwkcktlged.serveur.metier.Document;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlged.serveur.metier.EOTypeDocument;
import org.cocktail.fwkcktlged.serveur.metier.service.DocumentService;
import org.cocktail.fwkcktlged.serveur.metier.service.DocumentServiceImpl;
import org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument;
import org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationMesr;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSForwardException;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXTimestampUtilities;

public class UniteRechercheEvaluationDocuments extends AFwkCktlSangriaComponent {

	private static final long serialVersionUID = 7854844159117520194L;

	public final String OBJET_KEY = EOEvaluationDocument.DOCUMENT.dot(EODocument.OBJET_KEY).key();
	public final String TYPE_KEY = EOEvaluationDocument.DOCUMENT.dot(EODocument.TO_TYPE_DOCUMENT_KEY).dot(EOTypeDocument.TD_LIBELLE_KEY).key();

	private ERXDisplayGroup<EOEvaluationDocument> displayGroup;
	private EOArrayDataSource dataSource;
	private EOEvaluationDocument currentEvaluationDocument;
	private EODocument nouveauDocument;
	private Boolean wantRefresh = false;

	private NSData documentData;
	private String documentObjet;
	private String documentCommentaire;
	private String documentFileName;
	private String documentMotsClefs;
	private EOTypeDocument documentType;
	private NSArray<EOTypeDocument> types;

	private DocumentService documentService;

	public final String ajouterDocumentWindowId = getComponentId() + "_ajouterDocumentWindow";
	public final String documentsTableViewId = getComponentId() + "_documentsTableView";

	private final String BINDING_evaluation = "evaluation";
	private final String BINDING_wantRefresh = "wantRefresh";
	private EOEditingContext _editingContext;

	public UniteRechercheEvaluationDocuments(WOContext context) {
		super(context);
	}

	public String getBoutonsAjoutDocumentContainerId() {
		return getComponentId() + "_boutonsAjoutDocumentContainer";
	}

	@Override
	public EOEditingContext edc() {
		if (_editingContext == null) {
			_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
		}
		return _editingContext;
	}

	public ERXDisplayGroup<EOEvaluationDocument> getDisplayGroup() {
		if (displayGroup == null || getWantRefresh()) {
			if (getWantRefresh()) {
				setWantRefresh(false);
				setDataSource(null);
			}
			displayGroup = new ERXDisplayGroup<EOEvaluationDocument>();
			displayGroup.setDataSource(getDataSource());
			// displayGroup.setDelegate();
			displayGroup.setQualifier(null);
			displayGroup.fetch();
		}
		return displayGroup;
	}

	public void setDisplayGroup(ERXDisplayGroup<EOEvaluationDocument> displayGroup) {
		this.displayGroup = displayGroup;
	}

	public EOArrayDataSource getDataSource() {
		if (dataSource == null) {
			dataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(EOEvaluationDocument.class), edc());
			dataSource.setArray(getEvaluation().evaluationDocuments());
		}
		return dataSource;
	}

	public EOEvaluationMesr getEvaluation() {
		return (EOEvaluationMesr) valueForBinding(BINDING_evaluation);
	}

	public void setDataSource(EOArrayDataSource dataSource) {
		this.dataSource = dataSource;
	}

	public EOEvaluationDocument getCurrentEvaluationDocument() {
		return currentEvaluationDocument;
	}

	public void setCurrentEvaluationDocument(EOEvaluationDocument currentEvaluationDocument) {
		this.currentEvaluationDocument = currentEvaluationDocument;
	}

	public EODocument getNouveauDocument() {
		return nouveauDocument;
	}

	public void setNouveauDocument(EODocument nouveauDocument) {
		this.nouveauDocument = nouveauDocument;
	}

	public WOActionResults validerAjout() {

		if (getNouveauDocument() == null) {
			session().addSimpleErrorMessage("Erreur", "Une erreur est survenue lors de l'ajout du document");
			return doNothing();
		}

		EOEvaluationDocument evaluationDocument = EOEvaluationDocument.creerInstance(edc());
		evaluationDocument.setDocumentRelationship(getNouveauDocument());
		evaluationDocument.setEvaluationRelationship(getEvaluation().localInstanceIn(edc()));
		evaluationDocument.setPersIdCreation(getUtilisateurPersId());
		evaluationDocument.setDCreation(ERXTimestampUtilities.today());
		try {
			edc().saveChanges();
			getDataSource().insertObject(evaluationDocument);
			getDisplayGroup().fetch();
			CktlAjaxWindow.close(context());
			session().addSimpleSuccessMessage("Succés", "Le document a bien été ajouté");
			setWantRefresh(true);
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}

		return doNothing();
	}

	public WOActionResults annulerAjout() {
		edc().revert();
		CktlAjaxWindow.close(context());
		return doNothing();
	}

	public WOActionResults supprimerUnDocument() {
		EOEvaluationDocument evaluationDocument = (EOEvaluationDocument) getDisplayGroup().selectedObject();

		if (evaluationDocument != null) {
			try {
				getDataSource().deleteObject(evaluationDocument);
				getDisplayGroup().fetch();
				EODocument document = evaluationDocument.document();
				evaluationDocument.setDocumentRelationship(null);
				getEvaluation().localInstanceIn(edc()).removeFromEvaluationDocumentsRelationship(evaluationDocument);
				document.delete();
				evaluationDocument.delete();
				edc().saveChanges();
			} catch (ValidationException e) {
				edc().revert();
				session().addSimpleErrorMessage("Erreur", e);
			}
		}

		return doNothing();
	}

	public boolean isSupprimerUnDocumentEnabled() {
		return getDisplayGroup().selectedObject() != null;
	}

	public Boolean getWantRefresh() {
		return booleanValueForBinding(BINDING_wantRefresh, false);
	}

	public void setWantRefresh(Boolean wantRefresh) {
		setValueForBinding(wantRefresh, BINDING_wantRefresh);
	}

	public String getDocumentObjet() {
		return documentObjet;
	}

	public void setDocumentObjet(String documentObjet) {
		this.documentObjet = documentObjet;
	}

	public NSData getDocumentData() {
		return documentData;
	}

	public void setDocumentData(NSData documentData) {
		this.documentData = documentData;
	}

	public String getDocumentCommentaire() {
		return documentCommentaire;
	}

	public void setDocumentCommentaire(String documentCommentaire) {
		this.documentCommentaire = documentCommentaire;
	}

	public String getDocumentFileName() {
		return documentFileName;
	}

	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}

	public String getDocumentMotsClefs() {
		return documentMotsClefs;
	}

	public void setDocumentMotsClefs(String documentMotsClefs) {
		this.documentMotsClefs = documentMotsClefs;
	}

	public EOTypeDocument getDocumentType() {
		return documentType;
	}

	public void setDocumentType(EOTypeDocument documentType) {
		this.documentType = documentType;
	}

	public NSArray<EOTypeDocument> getTypes() {
		if (types == null) {
			EOQualifier qualifier = ERXQ.contains(EOTypeDocument.TD_STR_ID_KEY, "gfc.acte");
			types = EOTypeDocument.fetchAll(edc(), qualifier, null);
		}
		return types;
	}

	public DocumentService getDocumentService() {
		if (documentService == null) {
			documentService = new DocumentServiceImpl(edc(), Configuration.configFromProperties("StructureRecherche.document"));
		}
		return documentService;
	}

	public WOActionResults validerAjoutDocument() {
		Document document = getDocumentService().creerFileDocument(getDocumentType().tdStrId(), getDocumentFileName(), getDocumentData().bytes(), getUtilisateurPersId());

		if (document == null) {
			session().addSimpleErrorMessage("Erreur", "Impossible de charger le nouveau document");
			edc().revert();
			return doNothing();
		}

		document.setObjet(getDocumentObjet());
		document.setCommentaire(getDocumentCommentaire());
		document.setMotsClefs(getDocumentMotsClefs());

		EOEvaluationDocument evaluationDocument = EOEvaluationDocument.creerInstance(edc());
		evaluationDocument.setDocumentRelationship((EODocument) document);
		evaluationDocument.setEvaluationRelationship(getEvaluation().localInstanceIn(edc()));
		evaluationDocument.setPersIdCreation(getUtilisateurPersId());
		evaluationDocument.setDCreation(ERXTimestampUtilities.today());

		try {
			edc().saveChanges();
			getDataSource().insertObject(evaluationDocument);
			getDisplayGroup().fetch();
			clear();
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			edc().revert();
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}

		return doNothing();

	}

	private void clear() {
		setDocumentObjet(null);
		setDocumentCommentaire(null);
		setDocumentMotsClefs(null);
		setDocumentData(null);
		setDocumentType(null);
		setDocumentFileName(null);
	}

	public WOActionResults ajouterDocument() {
		return doNothing();
	}

	public WOActionResults annulerAjoutDocument() {
		return null;
	}

	public Boolean getNePeutValiderAjout() {
		return getDocumentObjet() == null || getDocumentData() == null || getDocumentType() == null;
	}

}