package org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.appserver.WOContext;

public class UniteRechercheGestionFinanciereUIM extends UniteRechercheComponentUI {
	
	private static final String BINDING_uniteRecherche = "uniteRecherche";
	
    public UniteRechercheGestionFinanciereUIM(WOContext context) {
        super(context);
    }
    
    public EOStructure uniteRecherche() {
    	return (EOStructure) valueForBinding(BINDING_uniteRecherche);
    }
}