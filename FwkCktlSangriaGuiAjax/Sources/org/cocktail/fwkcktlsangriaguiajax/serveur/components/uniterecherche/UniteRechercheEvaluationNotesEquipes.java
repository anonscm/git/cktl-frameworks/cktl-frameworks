package org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationMesr;
import org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXTimestampUtilities;

public class UniteRechercheEvaluationNotesEquipes extends AFwkCktlSangriaComponent {
  
    public final static String BINDING_evaluation = "evaluation";
  
    private EOEvaluationNoteEquipe currentEvaluationNote;
    private String currentNote;
    private EOStructure currentEquipe;
    private EOStructure equipeAAjouter;
    private String noteAAjouter;
    
    public UniteRechercheEvaluationNotesEquipes(WOContext context) {
        super(context);
    }
    
    public NSArray<String> notesPossibles() {
      return new NSArray<String>("A+","A","B","C","D");
    }
    
    public EOEvaluationMesr evaluation() {
      return (EOEvaluationMesr) valueForBinding(BINDING_evaluation);
    }
    
    public NSArray<EOStructure> listeEquipes() {
      return ERXArrayUtilities.arrayMinusArray(
          StructuresRechercheHelper.equipesDeRecherchePourUniteRecherche(edc(), evaluation().unite()),
          (NSArray<EOStructure>) evaluationNoteEquipes().valueForKey(EOEvaluationNoteEquipe.EQUIPE_KEY)
         );
    }
    
    public NSArray<EOEvaluationNoteEquipe> evaluationNoteEquipes() {
      return evaluation().evaluationNoteEquipes();
    }

    public EOEvaluationNoteEquipe getCurrentEvaluationNote() {
      return currentEvaluationNote;
    }

    public void setCurrentEvaluationNote(EOEvaluationNoteEquipe currentEvaluationNote) {
      this.currentEvaluationNote = currentEvaluationNote;
    }

    public String getCurrentNote() {
      return currentNote;
    }

    public void setCurrentNote(String currentNote) {
      this.currentNote = currentNote;
    }

    public EOStructure getCurrentEquipe() {
      return currentEquipe;
    }

    public void setCurrentEquipe(EOStructure currentEquipe) {
      this.currentEquipe = currentEquipe;
    }

    public EOStructure getEquipeAAjouter() {
      return equipeAAjouter;
    }

    public void setEquipeAAjouter(EOStructure equipeAAjouter) {
      this.equipeAAjouter = equipeAAjouter;
    }

    public String getNoteAAjouter() {
      return noteAAjouter;
    }

    public void setNoteAAjouter(String noteAAjouter) {
      this.noteAAjouter = noteAAjouter;
    }

    public WOActionResults ajouterNote() {
      if(getEquipeAAjouter() == null || getNoteAAjouter() == null) {
        session().addSimpleErrorMessage("Erreur", "Vous devez choisir une équipe et une note");
        return doNothing();
      }
      
      EOEvaluationNoteEquipe nouvelleEvaluationNoteEquipe = EOEvaluationNoteEquipe.creerInstance(edc());
      nouvelleEvaluationNoteEquipe.setEquipeRelationship(getEquipeAAjouter());
      nouvelleEvaluationNoteEquipe.setEvaluationRelationship(evaluation());
      nouvelleEvaluationNoteEquipe.setNote(getNoteAAjouter());
      nouvelleEvaluationNoteEquipe.setPersIdCreation(getUtilisateurPersId());
      nouvelleEvaluationNoteEquipe.setDCreation(ERXTimestampUtilities.today());
      
      
      setEquipeAAjouter(null);
      setNoteAAjouter(null);
      
      return null;
    }
    
    public WOActionResults supprimerNote() {
      evaluation().deleteEvaluationNoteEquipesRelationship(getCurrentEvaluationNote());
      getCurrentEvaluationNote().delete();
      return doNothing();
    }
    
}