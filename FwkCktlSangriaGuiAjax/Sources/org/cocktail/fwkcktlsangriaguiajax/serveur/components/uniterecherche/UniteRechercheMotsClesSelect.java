package org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOMotCle;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleUniteRecherche;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

public class UniteRechercheMotsClesSelect extends AFwkCktlSangriaComponent {
    
	public final static String BINDING_uniteRecherche = "uniteRecherche"; 
	
	private EOMotCle currentMotCle;
	private EOMotCle selectedMotCle;
	
	private NSArray<EOMotCle> motsCles;
	private EOArrayDataSource motsClesDataSource;
	private ERXDisplayGroup<EOMotCle> motsClesDisplayGroup;
	
	private Boolean afficherTousLesMotsCles = false;

	
	
	public UniteRechercheMotsClesSelect(WOContext context) {
        super(context);
    }
	
	public String motsClesWindowId() {
		return getComponentId() + "_motsClesWindow";
	}
	
	public String motsClesContainerId() {
		return getComponentId() + "_motsClesContainer";
	}
	
	public String motsClesTableViewId() {
		return getComponentId() + "_motsClesTableView";
	}
	
	public final String motsClesPossiblesContainerId = getComponentId() + "_motsClesPossiblesContainer";
	
	public EOStructure uniteRecherche() {
		edc();
		return (EOStructure) valueForBinding(BINDING_uniteRecherche);
	}
	
	public NSArray<EOMotCle> motsCles() {
		if(motsCles == null) {
			motsCles = StructuresRechercheHelper.motsClesDefinitsPourUniteRecherche(edc(), uniteRecherche());
		}
		return motsCles;
	}
	
	public NSArray<EOMotCle> motsClesPossibles() {
		return StructuresRechercheHelper.motsClesPossiblesPourUniteRecherche(edc(), uniteRecherche(), !getAfficherTousLesMotsCles());
	}

	public void setCurrentMotCle(EOMotCle currentMotCle) {
		this.currentMotCle = currentMotCle;
	}

	public EOMotCle currentMotCle() {
		return currentMotCle;
	}
	
	public WOActionResults selectionnerMotCle() {
		EORepartMotCleUniteRecherche motCleUniteRecherche = new EORepartMotCleUniteRecherche();
		edc().insertObject(motCleUniteRecherche);
		motCleUniteRecherche.setMotCleRelationship(currentMotCle());
		motCleUniteRecherche.setUniteRechercheRelationship(uniteRecherche());
		motsClesDataSource().insertObject(currentMotCle());
		motsClesDisplayGroup().fetch();
		CktlAjaxWindow.close(context(), motsClesWindowId());
		return doNothing();
	}
	
	public WOActionResults supprimerMotCle() {
		EOQualifier qualifier = ERXQ.and(
			ERXQ.equals(EORepartMotCleUniteRecherche.UNITE_RECHERCHE_KEY, uniteRecherche()),
			ERXQ.equals(EORepartMotCleUniteRecherche.MOT_CLE_KEY, selectedMotCle())
		);
		ERXFetchSpecification<EORepartMotCleUniteRecherche> fetchSpecification = new ERXFetchSpecification<EORepartMotCleUniteRecherche>(EORepartMotCleUniteRecherche.ENTITY_NAME, qualifier, null);
		fetchSpecification.setIncludeEditingContextChanges(true);
		EORepartMotCleUniteRecherche repartMotCleUniteRecherche = fetchSpecification.fetchObjects(edc()).get(0);
		edc().deleteObject(repartMotCleUniteRecherche);
		motsClesDataSource().deleteObject(selectedMotCle());
		motsClesDisplayGroup().fetch();
		return doNothing();
	}

	public EOArrayDataSource motsClesDataSource() {
		if(motsClesDataSource == null) {
			motsClesDataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForEntityName(EOMotCle.ENTITY_NAME), edc());
			motsClesDataSource.setArray(motsCles());
		}
		return motsClesDataSource;
	}

	public ERXDisplayGroup<EOMotCle> motsClesDisplayGroup() {
		if(motsClesDisplayGroup == null) {
			motsClesDisplayGroup = new ERXDisplayGroup<EOMotCle>();
			motsClesDisplayGroup.setDataSource(motsClesDataSource());
			motsClesDisplayGroup.setDelegate(this);
			motsClesDisplayGroup.setSelectsFirstObjectAfterFetch(true);
			motsClesDisplayGroup.fetch();
		}
		return motsClesDisplayGroup;
	}
	
	public void displayGroupDidChangeSelection(WODisplayGroup group) {
		ERXDisplayGroup<EOMotCle> displayGroup = (ERXDisplayGroup<EOMotCle>) group;
		if(displayGroup.selectedObject() != null) {
			setSelectedMotCle(displayGroup.selectedObject());
		}
	}

	public EOMotCle selectedMotCle() {
		return selectedMotCle;
	}

	public void setSelectedMotCle(EOMotCle selectedMotCle) {
		this.selectedMotCle = selectedMotCle;
	}

  public Boolean getAfficherTousLesMotsCles() {
    return afficherTousLesMotsCles;
  }

  public void setAfficherTousLesMotsCles(Boolean afficherTousLesMotsCles) {
    this.afficherTousLesMotsCles = afficherTousLesMotsCles;
  }


	
}