package org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

/**
 * Onglet des equipes de recherche 
 */
public class UniteRechercheEquipesUIM extends UniteRechercheComponentUI {
	
	private static final long serialVersionUID = 1L;
	public static final String BINDING_UNITE_RECHERCHE = "uniteRecherche";
	public static final String CURRENT_EQUIPE_RECHERCHE_KEY = "currentEquipeDeRecherche";

	private EOEditingContext creationEquipesEditingContext = null;
	private EORepartAssociation currentRepartAssociationEquipeDeRecherche;
	private EORepartAssociation selectedRepartAssociationEquipeDeRecherche;
	private EORepartAssociation editingRepartAssociationEquipeDeRecherche;
	private WODisplayGroup equipesDisplayGroup = null;
	private NSTimestamp dateDebutEquipeRecherche;
	private NSTimestamp dateFinEquipeRecherche;
	private EOStructure nouvelleEquipeRecherche;
	private boolean showDetails = false;
	private NSArray<EORepartAssociation> equipesDeRechercheAvecDetails;
	private NSArray<EORepartAssociation> equipesDeRechercheSansDetails;

	/**
	 * Constructeur
	 * @param context contexte
	 */
    public UniteRechercheEquipesUIM(WOContext context) {
        super(context);
    }
    
    /* id générés */
    
    public String getCreationEquipeRechercheWindowId() {
    	return getComponentId() + "_creationEquipeRechecheWindowId";
    }
    
    public String getListeEquipesContainerId() {
    	return getComponentId() + "_listeEquipesContainer";
    }

    public String getEquipesRechercheTableViewId() {
    	return getComponentId() + "_equipesRechercheTableView"; 
    }
    
    public String getModificationEquipeRechercheWindowId() {
    	return getComponentId() + "_modificationEquipeRechercheWindowId";
    }
    
    /* Bindings */
    
    /**
     * @return structure passée en binding
     */
	public EOStructure uniteRecherche() {
		return (EOStructure) valueForBinding(BINDING_UNITE_RECHERCHE);
	}
	
	public boolean isShowDetails() {
		return showDetails;
	}

	public void setShowDetails(boolean showDetails) {
		this.showDetails = showDetails;
	}
	
	/**
	 * @return Change données affichées dans la table des equipes de recherche
	 */
	public WOActionResults switchShowDetails() {
		showDetails = !showDetails;
		refreshResponsablesDisplayGroup();
		AjaxUpdateContainer.updateContainerWithID(getListeEquipesContainerId(), context());
		return doNothing();
	}
	
	/**
	 * Met à jour les données affichées dans la table des equipes de recherche
	 */
	public void refreshResponsablesDisplayGroup() {
		equipesDisplayGroup().setObjectArray(equipesDeRecherche());
	}
	
	/**
	 * @return DisplayGroup des equipes de recherche de la structure
	 */
	public WODisplayGroup equipesDisplayGroup() {
		if (equipesDisplayGroup == null) {
			equipesDisplayGroup = new ERXDisplayGroup<EORepartAssociation>();
			equipesDisplayGroup.setDelegate(new EquipesDisplayGroupDelegate());
			equipesDisplayGroup.setObjectArray(equipesDeRecherche());
			equipesDisplayGroup.setSelectsFirstObjectAfterFetch(true);
		}
		return equipesDisplayGroup;
	}

	/**
	 * DisplayGroupDelegate
	 */
	public class EquipesDisplayGroupDelegate {
	
		/**
		 * @param group : groupe d'éléments sélectionnés
		 */
		public void displayGroupDidChangeSelectedObjects(WODisplayGroup group) {
			@SuppressWarnings("unchecked")
			ERXDisplayGroup<EORepartAssociation> _groupe = (ERXDisplayGroup<EORepartAssociation>) group;
			if (_groupe.selectedObject() != null) {
				setSelectedRepartAssociationEquipeDeRecherche(_groupe.selectedObject());
			} else {
				setSelectedRepartAssociationEquipeDeRecherche(null);
			}
		}

	}
	/**
	 * @return les données a afficher ds le tableau
	 */
	public NSArray<EORepartAssociation> equipesDeRecherche() {
		if (equipesDeRechercheAvecDetails == null) {
			equipesDeRechercheAvecDetails = StructuresRechercheHelper.repartAssociationsEquipesDeRecherchePourUniteRecherche(edc(), uniteRecherche());
		}
		if (equipesDeRechercheSansDetails == null) {
			equipesDeRechercheSansDetails = StructuresRechercheHelper.repartAssociationsEquipesDeRechercheActuellesPourUniteRecherche(edc(), uniteRecherche());
		}
		if (isShowDetails()) {
			return equipesDeRechercheAvecDetails;
		} else {
			return equipesDeRechercheSansDetails;
		}
	}
	
	/**
	 * Supprime une equipe de recherche 
	 * @return null
	 */
	public WOActionResults supprimerEquipeDeRecherche() {
		if (getSelectedRepartAssociationEquipeDeRecherche() != null) {
			getSelectedRepartAssociationEquipeDeRecherche().setToAssociationRelationship(null);
			getSelectedRepartAssociationEquipeDeRecherche().setToPersonne(null);
			getSelectedRepartAssociationEquipeDeRecherche().setToStructureRelationship(null);
			if (equipesDeRechercheAvecDetails != null && equipesDeRechercheAvecDetails.contains(getSelectedRepartAssociationEquipeDeRecherche())) {
				equipesDeRechercheAvecDetails.remove(getSelectedRepartAssociationEquipeDeRecherche());
			}
			if (equipesDeRechercheSansDetails != null && equipesDeRechercheSansDetails.contains(getSelectedRepartAssociationEquipeDeRecherche())) {
				equipesDeRechercheSansDetails.remove(getSelectedRepartAssociationEquipeDeRecherche());
			}
			edc().deleteObject(getSelectedRepartAssociationEquipeDeRecherche());
			setSelectedRepartAssociationEquipeDeRecherche(null);
			refreshResponsablesDisplayGroup();
		}
		return doNothing();
	}
	
	/**
	 * Action effectuée au moment du click sur le bouton ajouter equipe de recherche
	 * @return null
	 */
	public WOActionResults creerNouvelleEquipeRecherche() {
		
		EOStructure groupeDesEquipesRecherche;
		try {
			groupeDesEquipesRecherche = EOStructureForGroupeSpec.getGroupeForParamKey(creationEquipesEditingContext(), "GROUPE_EQUIPES_RECHERCHE");
			NSArray<EOTypeGroupe> groupes = EOTypeGroupe.fetchAll(creationEquipesEditingContext(), ERXQ.in(EOTypeGroupe.TGRP_CODE_KEY, new NSArray<String>("ER", "O", "G")));
			nouvelleEquipeRecherche = EOStructureForGroupeSpec.creerGroupe(creationEquipesEditingContext(), getUtilisateurPersId(), null, null, groupes, groupeDesEquipesRecherche);
			EOStructureForGroupeSpec.affecterPersonneDansGroupe(creationEquipesEditingContext(), nouvelleEquipeRecherche, uniteRecherche().localInstanceIn(creationEquipesEditingContext()), getUtilisateurPersId(), FactoryAssociation.shared().equipeDeRechercheAssociation(creationEquipesEditingContext()));
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}		
		return doNothing();
	}
	
	public boolean isBoutonEditDisabled() {
		return (getSelectedRepartAssociationEquipeDeRecherche() == null);
	}

	public void setCurrentRepartAssociationEquipeDeRecherche(EORepartAssociation currentRepartAssociationEquipeDeRecherche) {
		this.currentRepartAssociationEquipeDeRecherche = currentRepartAssociationEquipeDeRecherche;
	}

	public EORepartAssociation getCurrentRepartAssociationEquipeDeRecherche() {
		return currentRepartAssociationEquipeDeRecherche;
	}
	
	public void setSelectedRepartAssociationEquipeDeRecherche(EORepartAssociation selectedRepartAssociationEquipeDeRecherche) {
		this.selectedRepartAssociationEquipeDeRecherche = selectedRepartAssociationEquipeDeRecherche;
	}

	public EORepartAssociation getSelectedRepartAssociationEquipeDeRecherche() {
		return selectedRepartAssociationEquipeDeRecherche;
	}
	
	public EOAssociation getEquipeRechercheAssociation() {
		return getFactoryAssociation().equipeDeRechercheAssociation(edc());
	}
	
	/* Fenetre creation equipe de recherche */
	
	/**
	 * @return editingContext de l'equipe créée
	 */
	public EOEditingContext creationEquipesEditingContext() {
		if (creationEquipesEditingContext == null) {
			creationEquipesEditingContext = ERXEC.newEditingContext();
		}
		return creationEquipesEditingContext;
	}
	
	/**
	 * Cree une nouvelle equipe de recherche
	 * @return null
	 */
	public WOActionResults validerCreationNouvelleEquipeRecherche() {
		try {
			creationEquipesEditingContext().saveChanges();
			
			equipesDisplayGroup = null;
			equipesDeRechercheAvecDetails = null;
			equipesDeRechercheSansDetails = null;
			
			CktlAjaxWindow.close(context(), getCreationEquipeRechercheWindowId());
			
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	
	/**
	 * Annule la creation d'une equipe de recherche
	 * @return null
	 */
	public WOActionResults annulerCreationNouvelleEquipeRecherche() {
		creationEquipesEditingContext().revert();
		CktlAjaxWindow.close(context(), getCreationEquipeRechercheWindowId());
		return doNothing();
	}
	
    public EOStructure getNouvelleEquipeRecherche() {
    	return nouvelleEquipeRecherche;
    }
	
    public void setNouvelleEquipeRecherche(EOStructure nouvelleEquipeRecherche) {
    	this.nouvelleEquipeRecherche = nouvelleEquipeRecherche;
    }
	
	/* Fenetre modification equipe de recherche */
	
	/**
	 * @return null
	 */
	public WOActionResults modifierResponsable() {
		setDateDebutEquipeRecherche(getSelectedRepartAssociationEquipeDeRecherche().rasDOuverture());
		setDateFinEquipeRecherche(getSelectedRepartAssociationEquipeDeRecherche().rasDFermeture());
		setEditingRepartAssociationEquipeDeRecherche(getSelectedRepartAssociationEquipeDeRecherche());
		return doNothing();
	}
    
	public NSTimestamp getDateDebutEquipeRecherche() {
		return dateDebutEquipeRecherche;
	}

	public void setDateDebutEquipeRecherche(NSTimestamp dateDebutEquipeRecherche) {
		this.dateDebutEquipeRecherche = dateDebutEquipeRecherche;
	}

	public NSTimestamp getDateFinEquipeRecherche() {
		return dateFinEquipeRecherche;
	}

	public void setDateFinEquipeRecherche(NSTimestamp dateFinEquipeRecherche) {
		this.dateFinEquipeRecherche = dateFinEquipeRecherche;
	}
	
	/**
	 * @return null
	 */
	public WOActionResults annulerModificationEquipeRecherche() {
		CktlAjaxWindow.close(context(), getModificationEquipeRechercheWindowId());
		return doNothing();
	}
	
	/**
	 * @return null
	 */
	public WOActionResults validerModificationEquipeRecherche() {
		if (getDateDebutEquipeRecherche() == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez sélectionner une date de début");
			return doNothing();
		}
		
		if (getDateFinEquipeRecherche() != null && getDateDebutEquipeRecherche().compareTo(getDateFinEquipeRecherche()) == 1) {
			session().addSimpleErrorMessage("Erreur", "La date de début doit être antérieure à la date de fin");
			return doNothing();
		}

		getEditingRepartAssociationEquipeDeRecherche().setRasDOuverture(getDateDebutEquipeRecherche());
		getEditingRepartAssociationEquipeDeRecherche().setRasDFermeture(getDateFinEquipeRecherche());
		
		CktlAjaxWindow.close(context(), getModificationEquipeRechercheWindowId());
		return doNothing();
	}

	public EORepartAssociation getEditingRepartAssociationEquipeDeRecherche() {
		return editingRepartAssociationEquipeDeRecherche;
	}

	public void setEditingRepartAssociationEquipeDeRecherche(EORepartAssociation editingRepartAssociationEquipeDeRecherche) {
		this.editingRepartAssociationEquipeDeRecherche = editingRepartAssociationEquipeDeRecherche;
	}
	
}