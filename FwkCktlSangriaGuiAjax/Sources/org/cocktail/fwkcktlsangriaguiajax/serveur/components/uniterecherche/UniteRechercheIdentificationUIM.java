package org.cocktail.fwkcktlsangriaguiajax.serveur.components.uniterecherche;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOReconnaissanceUnite;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;

public class UniteRechercheIdentificationUIM extends UniteRechercheComponentUI {
    
	public static final String BINDING_uniteRecherche = "uniteRecherche";
	private Boolean isEditing;
	
	private EOStructure currentTutelle;
	
	private EOReconnaissanceUnite currentReconnaisance;
	
	private EODomaineScientifique currentDomaineScientifique;
	private EODomaineScientifique selectedDomaineScientifique;
	
	private EOStructure currentEtablissementRattachementSecondaire;
	private EOStructure tutelleAAjouter;
	private EOStructure ERSAAjouter;
	private NSArray<EOReconnaissanceUnite> reconnaissancesUnites;
	private EOAssociation responsableAdministratifAssociation = null;
	private EOAssociation directionAssociation = null;
	private EOAssociation directeurAdjointAssociation = null;
//	private EOEditingContext personnesEditingContext;
	
	public UniteRechercheIdentificationUIM(WOContext context) {
        super(context);
    }
	
//	public EOEditingContext personnesEditingContext() {
//		if(personnesEditingContext == null) {
//			personnesEditingContext = ERXEC.newEditingContext(edc());
//		}
//		return personnesEditingContext;
//	}
	
	public EOStructure uniteRecherche() {
		return (EOStructure) valueForBinding(BINDING_uniteRecherche);
	}

	public void setIsEditing(Boolean isEditing) {
		this.isEditing = isEditing;
	}

	public Boolean isEditing() {
		return isEditing;
	}
	
	public String tutellesContainerId() {
		return getComponentId() + "_tutellesContainer";
	}
	
	public String winTutelleSelectionContainerOnCloseID() {
		return winTutelleSelectionID() + "_containerOnClose";
	}

	public String winTutelleSelectionID() {
		return getComponentId() + "winTutelleSelection";
	}
	
	public String ersContainerId() {
		return getComponentId() + "_ersContainer";
	}
	
	public String winERSSelectionContainerOnCloseID() {
		return winERSSelectionID() + "_containerOnClose";
	}

	public String winERSSelectionID() {
		return getComponentId() + "winERSSelection";
	}
	

	public void setCurrentERS(
			EOStructure currentEtablissementRattachementSecondaire) {
		this.currentEtablissementRattachementSecondaire = currentEtablissementRattachementSecondaire;
	}

	public EOStructure currentERS() {
		return currentEtablissementRattachementSecondaire;
	}

	
	public NSArray<EODomaineScientifique> domainesScientifiques() {
		return EODomaineScientifique.fetchAll(edc());
	}

	public void setSelectedDomaineScientifique(EODomaineScientifique selectedDomaineScientifique) {
		this.selectedDomaineScientifique = selectedDomaineScientifique;
	}

	public EODomaineScientifique selectedDomaineScientifique() {
		return selectedDomaineScientifique;
	}

	public void setCurrentDomaineScientifique(EODomaineScientifique currentDomaineScientifique) {
		this.currentDomaineScientifique = currentDomaineScientifique;
	}

	public EODomaineScientifique currentDomaineScientifique() {
		return currentDomaineScientifique;
	}
	
	public EODomaineScientifique domaineScientifiquePrincpalPourUniteRecherche() {
		return StructuresRechercheHelper.domaineScientifiquePrincipalUniteRecherche(edc(), uniteRecherche(), false);
	}
	
	public void setDomaineScientifiquePrincpalPourUniteRecherche(EODomaineScientifique domaineScientifique) {
		StructuresRechercheHelper.setDomaineScientifiquePrincipalUniteRecherche(edc(), domaineScientifique, uniteRecherche(), false);
	}
/*	
	public EODomaineScientifique domaineScientifiqueSecondairePourUniteRecherche() {
		return StructureRechercheUtilities.domaineScientifiqueSecondaireUniteRecherche(edc(), uniteRecherche(), false);
	}
	
	public void setDomaineScientifiqueSecondairePourUniteRecherche(EODomaineScientifique domaineScientifique) {
		StructureRechercheUtilities.setDomaineScientifiqueSecondaireUniteRecherche(edc(), domaineScientifique, uniteRecherche(), false);
	}
*/	
	/*********************************************************/
	/*** Gestion des tutelles ***/

	
	public void setCurrentTutelle(EOStructure currentTutelle) {
		this.currentTutelle = currentTutelle;
	}

	public EOStructure currentTutelle() {
		return currentTutelle;
	}
	
	public NSArray<EOStructure> tutellesPourUniteRecherche()  {
		//return StructureRechercheUtilities.tutellesUniteRecherche(edc(), uniteRecherche());
		return StructuresRechercheHelper.tutellesStructureRecherche(edc(), uniteRecherche());
	}
	
	
	public EOStructure tutelleAAjouter() {
		return tutelleAAjouter;
	}

	public void setTutelleAAjouter(EOStructure tutelleAAjouter) {
		this.tutelleAAjouter = tutelleAAjouter;
	}
	
	public String tutelleSearchWindowId() {
		return getComponentId() + "_tutelleSearchWindow";
	}
	
	public String jsOpenTutelleSearch() {
		return "openCAW_" +  tutelleSearchWindowId() + "();return false;";
	}
	
	public WOActionResults onAjoutTutelle() {
		StructuresRechercheHelper.ajouterTutellePourStructureRecherche(edc(), tutelleAAjouter(), uniteRecherche(), getUtilisateurPersId());
		if (StructuresRechercheHelper.tutellesStructureRecherche(edc(), uniteRecherche()).count() == 1) {
			StructuresRechercheHelper.definirTutellePrincipalePourStructureRecherche(edc(), tutelleAAjouter(), uniteRecherche(), getUtilisateurPersId());
		}
		AjaxUpdateContainer.updateContainerWithID(tutellesContainerId(), context());
		CktlAjaxWindow.close(context(), tutelleSearchWindowId());
		return doNothing();
	}
	
	public WOActionResults annulerAjoutTuelle() {
		CktlAjaxWindow.close(context(), tutelleSearchWindowId());
		return doNothing();
	}
	
	public WOActionResults onSuppressionTutelle() {

		try {
			StructuresRechercheHelper.supprimerTutellePourStructureRecherche(edc(), currentTutelle(), uniteRecherche(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	
	/*********************************************************/
	/*** Gestion des ERS ***/

	public NSArray<EOStructure> etablissementsDeRattachementPourUniteRecherche() {
		return StructuresRechercheHelper.etablissementsRattachementPourStructureRecherche(edc(), uniteRecherche());
	}
	
	public EOStructure etablissementRattachementPrincipalPourUniteRecherche() {
		return StructuresRechercheHelper.etablissementRattachementPrincipalPourStructureRecherche(edc(), uniteRecherche());
	}
	
	public EOStructure ERSAAjouter() {
		return ERSAAjouter;
	}

	public void setERSAAjouter(EOStructure ERSAAjouter) {
		this.ERSAAjouter = ERSAAjouter;
	}

	public String ERSSearchWindowId() {
		return getComponentId() + "_ERSSearchWindow";
	}

	public WOActionResults onAjoutERS() {
		if (ERSAAjouter() == null) {
			session().addSimpleErrorMessage(null, "Vous devez sélectionner un établissement à ajouter");
			return doNothing();
		}
//		try {
//			ERSAAjouter().setValidationEditingContext(personnesEditingContext());
//			ERSAAjouter().setPersIdModification(utilisateurPersId());
//			personnesEditingContext().saveChanges();
//			StructuresRechercheHelper.ajouterEtablissementRattachementPourUniteRecherche(edc(), ERSAAjouter().localInstanceIn(edc()), uniteRecherche(), utilisateurPersId());
			StructuresRechercheHelper.ajouterEtablissementRattachementPourUniteRecherche(edc(), ERSAAjouter(), uniteRecherche(), getUtilisateurPersId());
			AjaxUpdateContainer.updateContainerWithID(ersContainerId(), context());
			CktlAjaxWindow.close(context(), ERSSearchWindowId());
//		} catch (ValidationException e) {
//			personnesEditingContext().revert();
//			session().addSimpleErrorMessage("Erreur", e);
//		}
		return doNothing();
	}

	public WOActionResults annulerAjoutERS() {
//		personnesEditingContext().revert();
		CktlAjaxWindow.close(context(), ERSSearchWindowId());
		return doNothing();
	}
	
	public WOActionResults onSuppressionERS() {
		try {
			StructuresRechercheHelper.supprimerEtablissementRattachementPourStructureRecherche(edc(), currentERS(), uniteRecherche(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	
	public Boolean isCurrentEtablissementRattachementPrincipal() {
		if (StructuresRechercheHelper.etablissementRattachementPrincipalPourStructureRecherche(edc(), uniteRecherche()) != null) {
			return StructuresRechercheHelper.etablissementRattachementPrincipalPourStructureRecherche(edc(), uniteRecherche()).equals(currentERS());
		}
		return false;
	}
	
	public WOActionResults setIsCurrentEtablissementRattachementPrincipal(Boolean isCurrentEtablissementRattachementPrincipal) {
		StructuresRechercheHelper.definirEtablissementRattachementPrincipalPourUniteRecherche(edc(), currentERS(), uniteRecherche(), getUtilisateurPersId());
		return doNothing();
	}
	
	public Boolean isCurrentTutellePrincipale() {
		if (StructuresRechercheHelper.tutellePrincipalePourStructureRecherche(edc(), uniteRecherche()) != null) {
			return StructuresRechercheHelper.tutellePrincipalePourStructureRecherche(edc(), uniteRecherche()).equals(currentTutelle());
		}
		return false;
	}
	
	public WOActionResults setIsCurrentTutellePrincipale(Boolean isCurrentTutellePrincipale) {
		StructuresRechercheHelper.definirTutellePrincipalePourStructureRecherche(edc(), currentTutelle(), uniteRecherche(), getUtilisateurPersId());
		return doNothing();
	}


	public EOAssociation responsableAdministratifAssociation() {
		return getFactoryAssociation().secretraireAdministrativeAssociation(edc());
	}

	public EOAssociation directionAssociation() {
		return getFactoryAssociation().directionAssociation(edc());
	}
	
	public EOAssociation directeurAdjointAssociation() {
		return getFactoryAssociation().directeurAdjointAssociation(edc());
	}
	
	
	 public EOQualifier qualifierForGroupeTutelles() {
	   String codeStructureGroupeTutelles = EOGrhumParametres.parametrePourCle(edc(), "org.cocktail.sangria.groupes.tutelles");
	    if (codeStructureGroupeTutelles != null) {
	      return EOStructure.C_STRUCTURE.eq(codeStructureGroupeTutelles);
	    } else {
	      return null;
	    }
	    
	  }
	 
	 public NSArray<EOStructure> listeDesTutelles() {
		 return StructuresRechercheHelper.listeDesTutelles(edc());
	 }
	  
	 public String getEcoleDoctoraleRattachement() {
		NSArray<EORepartAssociation> ras = StructuresRechercheHelper.repartsAssociationsEcolesDoctoralesDUneStructure(edc(), uniteRecherche());
		String ecolesDoctorales = "";
		for (EORepartAssociation ra : ras) {
			if (!ecolesDoctorales.equals("")) {
				ecolesDoctorales += ", ";
			}
			ecolesDoctorales += ra.toStructure().lcStructure();
		}
		return ecolesDoctorales;
	 }
	 
	 public String getParticipationStructureFederative() {
		NSArray<EORepartAssociation> ras = StructuresRechercheHelper.repartsAssociationsFederationsRechercheDUneStructure(edc(), uniteRecherche());
		String federations = "";
		for (EORepartAssociation ra : ras) {
			if (!federations.equals("")) {
				federations += ", ";
			}
			federations += ra.toStructure().lcStructure();
		}
		return federations;
	 }

}