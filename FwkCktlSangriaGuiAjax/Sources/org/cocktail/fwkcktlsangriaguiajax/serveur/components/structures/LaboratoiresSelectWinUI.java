package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.GenericSearchWinComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

public class LaboratoiresSelectWinUI extends AFwkCktlSangriaComponent {
	
	private final static String BINDING_selection = "selection";
	private final static String BINDING_windId = "winId";
	private final static String BINDING_callbackOnSelectionner = "callbackOnSelectionner";
	
	private EOStructure currentLaboratoire;
	
    public LaboratoiresSelectWinUI(WOContext context) {
        super(context);
    }
    
    public NSArray<EOStructure> listeDesLaboratoires() {
    	EOQualifier qualifierForLabos = ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_LA);
    	EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
    	return EOStructure.fetchAll(edc(), qualifierForLabos, new NSArray<EOSortOrdering>(order));
    }
    
    public String getWinId() {
    	if(hasBinding(BINDING_windId)) {
    		return (String) valueForBinding(BINDING_windId);
    	}
    	
    	return "";
    }
    
    
	public String jsOnSelect() {
		return "function() {" + updateContainerID() + "Update(); Windows.close('"+ getWinId() +"_win', event);}";
	}

	public void setCurrentLaboratoire(EOStructure currentLaboratoire) {
		this.currentLaboratoire = currentLaboratoire;
	}

	public EOStructure getCurrentLaboratoire() {
		return currentLaboratoire;
	}
	
	public WOActionResults selectionnerLaboratoire() {
		setSelection(getCurrentLaboratoire());
		return doNothing();
	}
	
	public void setSelection(EOStructure selection) {
		if(hasBinding(BINDING_selection)) {
			setValueForBinding(selection, BINDING_selection);
		}
	}
	

}