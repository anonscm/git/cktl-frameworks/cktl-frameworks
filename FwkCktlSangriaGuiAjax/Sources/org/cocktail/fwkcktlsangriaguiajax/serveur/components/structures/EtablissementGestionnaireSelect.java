package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXArrayUtilities;

public class EtablissementGestionnaireSelect extends AFwkCktlSangriaComponent {
    
	private static Logger logger = Logger.getLogger(EtablissementGestionnaireSelect.class);
	
	private static String BINDING_etablissementGestionnaire = "etablissementGestionnaire";
	
	private EOStructure etablissementGestionnaire;
	
	private EOStructure currentEtablissement;
	
	
	
	
	public EtablissementGestionnaireSelect(WOContext context) {
        super(context);
    }

	public void setEtablissementGestionnaire(EOStructure etablissementGestionnaire) {
		this.etablissementGestionnaire = etablissementGestionnaire;
		setValueForBinding(etablissementGestionnaire, BINDING_etablissementGestionnaire);
	}

	public EOStructure getEtablissementGestionnaire() {
		return (EOStructure) valueForBinding(BINDING_etablissementGestionnaire);
	}

	public WOActionResults supprimerSelection() {
		setEtablissementGestionnaire(null);
		return doNothing();
	}
    
    
    public NSArray<EOStructure> listeDesEtablissements() {
        
        try {
        	EOStructure groupeEtablissements = EOStructureForGroupeSpec.getGroupeForParamKey(edc(), "GROUPE_ETAB_GEST_FIN_AAP");
        	return
        			EOStructure.LL_STRUCTURE.ascInsensitives().sorted(
        					ERXArrayUtilities.arrayBySelectingInstancesOfClass(
        							EOStructureForGroupeSpec.getPersonnesInGroupe(groupeEtablissements), 
        							EOStructure.class
        						)
        					);
        }
        catch (Exception e) {
        	logger.warn("Attention le groupe des etablissements gestionnaires n'est pas défini");
        	return new NSArray<EOStructure>();
        }
        
        
      }

	public void setCurrentEtablissement(EOStructure currentEtablissement) {
		this.currentEtablissement = currentEtablissement;
	}

	public EOStructure getCurrentEtablissement() {
		return currentEtablissement;
	}
    
	public WOActionResults selectionnerGestionnaire() {
		try {
			setEtablissementGestionnaire(getCurrentEtablissement());
			CktlAjaxWindow.close(context(),"EtablissementGestionnaireSelectDialog");
			return doNothing();
		}
		catch (ValidationException e) {
			edc().revert();
			session().addSimpleErrorMessage("Erreur", e);
			return doNothing();
		}
		
	}
	
	public String jsOpenEtablissementGestionnaireSearch() {
		return "openCAW_EtablissementGestionnaireSelectDialog();return false;";
	}




	
}