package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

public class LaboratoireDetailsPrincipal extends AFwkCktlSangriaComponent { 
	
	private final static String BINDING_componentAssociations = "componentAssociations";
	
	
	
    public LaboratoireDetailsPrincipal(WOContext context) {
        super(context);
    }
    
    @Override
    public void awake() {
    	super.awake();
    	
    	NSMutableDictionary<String, Object> componentAssociations = getComponentAssociations();
    	NSArray<String> keys = componentAssociations.allKeys();
    	
    	for(String key : keys) {
    		takeValueForKey(componentAssociations.valueForKey(key), key);
    	}
    	
    }
    
    public NSMutableDictionary<String, Object> getComponentAssociations() {
    	return (NSMutableDictionary<String, Object>) valueForBinding(BINDING_componentAssociations);
    }
    
    
}