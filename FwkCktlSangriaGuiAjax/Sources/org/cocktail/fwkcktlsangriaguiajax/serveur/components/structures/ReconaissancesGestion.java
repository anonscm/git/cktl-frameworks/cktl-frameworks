package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOReconnaissanceUnite;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class ReconaissancesGestion extends AFwkCktlSangriaComponent {
    
	private final static String BINDING_structureRecherche = "structureRecherche";
	private final static String BINDING_showLabel = "showLabel";
	
	private EOReconnaissanceUnite currentReconnaisance;
	private NSArray<EOReconnaissanceUnite> reconnaissancesUnites;

	
	
	
	public ReconaissancesGestion(WOContext context) {
        super(context);
    }
	
	public String reconnaissancesContainerId() {
		return getComponentId() + "_reconnaissanceContainer";
	}
	


	public EOStructure structureRecherche() {
		return (EOStructure) valueForBinding(BINDING_structureRecherche);
	}

	public Boolean showLabel() {
		if(hasBinding(BINDING_showLabel)) {
			return (Boolean) valueForBinding(BINDING_showLabel);
		}
		else {
			return true;
		}
	}
	
	
	public void setCurrentReconnaisance(EOReconnaissanceUnite currentReconnaisance) {
		this.currentReconnaisance = currentReconnaisance;
	}

	public EOReconnaissanceUnite currentReconnaisance() {
		return currentReconnaisance;
	}
	
	public WOActionResults ajouterUneReconnaissance() {
		EOReconnaissanceUnite reconnaissanceUnite = (EOReconnaissanceUnite) EOUtilities.createAndInsertInstance(edc(), EOReconnaissanceUnite.ENTITY_NAME);
		reconnaissanceUnite.setUniteRelationship(structureRecherche());
		reconnaissanceUnite.setDCreation(new NSTimestamp());
		reconnaissanceUnite.setDModification(new NSTimestamp());
		reconnaissanceUnite.setPersIdCreation(getUtilisateurPersId());
		reconnaissanceUnite.setPersIdModification(getUtilisateurPersId()); 
		refreshReconnaissances();
		return doNothing();
	}
	
	public WOActionResults refreshReconnaissances() {
		reconnaissancesUnites = null;
		return doNothing();
	}
	
	
	
	public NSArray<EOReconnaissanceUnite> reconnaissances() {
		if(reconnaissancesUnites == null || reconnaissancesUnites.isEmpty()) {
			reconnaissancesUnites = EOSortOrdering.sortedArrayUsingKeyOrderArray(StructuresRechercheHelper.reconnaissancesPourUniteRecherche(edc(), structureRecherche(), true), new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOReconnaissanceUnite.REC_DATE_KEY, EOSortOrdering.CompareDescending)));
		}
		return reconnaissancesUnites; 
	}


	
	public WOActionResults supprimerReconnaissance() {
		currentReconnaisance().setUniteRelationship(null);
		edc().deleteObject(currentReconnaisance());
		refreshReconnaissances();
		return doNothing();
	}
    
}