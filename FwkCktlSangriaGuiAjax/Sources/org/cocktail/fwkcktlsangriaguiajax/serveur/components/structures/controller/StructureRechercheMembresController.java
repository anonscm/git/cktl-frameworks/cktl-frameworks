package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures.controller;


import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EOIndividuDiplome;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividusGradesService;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EODiplome;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.metier.service.StatutAffectesService;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures.StructureRechercheMembres;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableSet;
import com.webobjects.foundation.NSSet;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXProperties;
import er.extensions.foundation.ERXTimestampUtilities;

public class StructureRechercheMembresController {

	private static final String ORG_COCKTAIL_SANGRIA_STRUCTURES_PERSONNELS_SOURCES = "org.cocktail.sangria.structures.personnels.sources";

	private StructureRechercheMembres component;

	private static final String CODE_DIPLOME_HDR = "0731000";
	private static final String CODE_DIMPLOME_DOCTORAT_ETAT = "1000000";
	
	public static final String PARAM_AFFECTATION_SEULEMENT = "org.cocktail.sangria.structures.personnels.affectation.seulement";
	
	private NSArray<String> codesDiplomes = new NSArray<String>(CODE_DIPLOME_HDR, CODE_DIMPLOME_DOCTORAT_ETAT);

	private NSArray<EOAssociation> statutsMembres = null;
	
	private EOEditingContext editionMembreEditingContext;

	private EORepartAssociation editingMembre;
	private EORepartStructure editingMembreRepartStructure;
	
	private EOArrayDataSource informationsMembresDataSource;
	private ERXDisplayGroup<InformationsMembre> informationsMembresDisplayGroup;
	private InformationsMembre currentInformationsMembre;
	
	private NSTimestamp dateAffichage = ERXTimestampUtilities.today();

	private NSArray<EORepartAssociation> tousLesRolesDansLaStructure;
	private EORepartAssociation currentRoleDansStructure;
	
	private Boolean isPhysalisEnProduction;
	
	private EOEditingContext edc;
	
	public StructureRechercheMembresController(EOEditingContext edc, StructureRechercheMembres component) {
		this.edc = edc;
		this.component = component;
	}

	public NSArray<String> getSources() {
		return ERXProperties.componentsSeparatedByString(ORG_COCKTAIL_SANGRIA_STRUCTURES_PERSONNELS_SOURCES, ",");
	}

	public EOEditingContext edc() {
//		return component.edc();
		return edc;
	}

	public EOEditingContext getEditionMembreEditingContext() {
		if (editionMembreEditingContext == null) {
			editionMembreEditingContext = ERXEC.newEditingContext(edc());
		}
		return editionMembreEditingContext;
	}
	
	public NSArray<EOAssociation> statutsMembres() {
		if (statutsMembres == null) {
			EOAssociation statutsMembresAssociation = component.typesMembresAssociation();
			statutsMembres = statutsMembresAssociation.getFils(edc());
		}
		
		return statutsMembres; 
	}
	
	public NSArray<EOAssociation> statutsMembresPourEdition() {
		return ERXEOControlUtilities.localInstancesOfObjects(getEditionMembreEditingContext(), statutsMembres());
	}
	
	public EOStructure structureRecherche() {
		return component.structureRecherche();
	}
	
	public ERXDisplayGroup<InformationsMembre> getInformationsMembresDisplayGroup() {
		if (informationsMembresDisplayGroup == null) {
			informationsMembresDisplayGroup = new ERXDisplayGroup<StructureRechercheMembresController.InformationsMembre>();
			informationsMembresDisplayGroup.setDataSource(getInformationsMembresDataSource());
			informationsMembresDisplayGroup.setSortOrderings(InformationsMembre.INDIVIDU.dot(EOIndividu.NOM_PRENOM_AFFICHAGE_KEY).ascs());
			informationsMembresDisplayGroup.fetch();
		}
		return informationsMembresDisplayGroup;
	}

	public EOArrayDataSource getInformationsMembresDataSource() {
		if (informationsMembresDataSource == null) {
			EOClassDescription classDescription = EOClassDescription.classDescriptionForClass(InformationsMembre.class);
			informationsMembresDataSource = new EOArrayDataSource(classDescription, edc());
			informationsMembresDataSource.setArray(initialiserInformationsMembres());
		}
		return informationsMembresDataSource;
	}

	public InformationsMembre getCurrentInformationsMembre() {
		return currentInformationsMembre;
	}

	public void setCurrentInformationsMembre(InformationsMembre currentInformationsMembre) {
		this.currentInformationsMembre = currentInformationsMembre;
	}
	
	private boolean canEditDoctorant(EOIndividu individu) {
		EOQualifier qualifier = EODoctorantThese.TO_DOCTORANT.dot(EODoctorant.TO_INDIVIDU_FWKPERS).eq(individu);
		NSArray<EODoctorantThese> theses = EODoctorantThese.fetchAll(edc(), qualifier);
		return (theses.count() == 0);
	}
	
	@SuppressWarnings("unchecked")
	public NSArray<InformationsMembre> initialiserInformationsMembres() {

		List<String> sources = getSources();
		Map<Integer, InformationsMembre> informationsMembres = new HashMap<Integer, InformationsMembre>();
		NSSet<EOIndividu> individus = new NSMutableSet<EOIndividu>();
		
		for (String source : sources) {
			if (StringUtils.equals(source, "RS")) {
				Collection<EORepartStructure> repartStructures = getRepartStructures(structureRecherche());
				for (EORepartStructure rs : repartStructures) {
					if (isPhysalisEnProduction()) {
						informationsMembres.put(rs.toIndividu().persId(), creerInformationsMembre(rs.toIndividu(), null, null, canEditDoctorant(rs.toIndividu()), null));
					} else {
						informationsMembres.put(rs.toIndividu().persId(), creerInformationsMembre(rs.toIndividu(), null, null, true, null));
					}
					individus.add(rs.toIndividu());
				}
			} else if (StringUtils.equals(source, "RA")) {
				Collection<EORepartAssociation> repartAssociations = getRepartAssociations(structureRecherche(), getDateAffichage());		
				for (EORepartAssociation ra : repartAssociations) {
					EOIndividu individu = (EOIndividu) ra.toPersonne();
					if (isPhysalisEnProduction()) {
						informationsMembres.put(ra.toPersonne().persId(), creerInformationsMembre(individu, ra.rasDOuverture(), ra.rasDFermeture(), canEditDoctorant(individu), ra.toAssociation()));
					} else {
						informationsMembres.put(ra.toPersonne().persId(), creerInformationsMembre(individu, ra.rasDOuverture(), ra.rasDFermeture(), true, ra.toAssociation()));
					}
					individus.add(individu);
				}
			} else if (StringUtils.equals(source, "A")) {
				NSArray<EOAffectation> affectations = getAffectations(structureRecherche(), getDateAffichage());
				NSArray<EOIndividu> individusAffectes = (NSArray<EOIndividu>) affectations.valueForKey(EOAffectation.TO_INDIVIDU_KEY);
				NSDictionary<EOIndividu, EOAssociation> statutsAffectes = StatutAffectesService.creerNouvelleInstance(edc()).statutsPourIndividusAffectesEtDate(individusAffectes, getDateAffichage());
				for (EOAffectation a : affectations) {
					informationsMembres.put(a.toIndividu().persId(), creerInformationsMembre(a.toIndividu(), a.dDebAffectation(), a.dFinAffectation(), false, statutsAffectes.objectForKey(a.toIndividu())));
					individus.add(a.toIndividu());
				}
			}
		}
		
		/** Récupération des grades **/
		IndividusGradesService individusGradesService = IndividusGradesService.creerNouvelleInstance(edc());
		NSDictionary<EOIndividu, EOGrade> grades = individusGradesService.gradesPourIndividus(individus.allObjects(), getDateAffichage());
		
		/** Récupération des diplomes **/
		ImmutableMap<Integer, EOIndividuDiplome> individusDiplomes = diplomesPourIndividus(individus.allObjects());
		for (EOIndividu individu : individus) {
			informationsMembres.get(individu.persId()).setGrade(grades.get(individu));
			informationsMembres.get(individu.persId()).setDiplome(individusDiplomes.get(individu.persId()));
		}
		NSArray<InformationsMembre> infos =  new NSMutableArray<InformationsMembre>(informationsMembres.values());
		Collections.sort(infos, new Comparator<InformationsMembre>() {
			public int compare(InformationsMembre i1, InformationsMembre i2) {
				return i1.getIndividu().nomPrenom().compareTo(i1.getIndividu().nomPrenom());
			}
		});		
		return infos;
	}

	private InformationsMembre creerInformationsMembre(EOIndividu individu, NSTimestamp debut, NSTimestamp fin, Boolean modifiable, EOAssociation statut) {
		InformationsMembre informationsMembre = new InformationsMembre();
		informationsMembre.setIndividu(individu);
		informationsMembre.setDebut(debut);
		informationsMembre.setFin(fin);
		informationsMembre.setModifiable(modifiable);
		informationsMembre.setStatut(statut);
		return informationsMembre;
	}

	public Collection<EORepartAssociation> getRepartAssociations(EOStructure structure, NSTimestamp dateAffichage) {
		NSArray<EOAssociation> statutsMembresAvecAffectatioNsArray = statutsMembres();
		EOQualifier qualifier =
			EORepartAssociation.TO_STRUCTURE.eq(structure)
				.and(EORepartAssociation.TO_ASSOCIATION.in(statutsMembresAvecAffectatioNsArray))
				.and(EORepartAssociation.RAS_D_OUVERTURE.lessThanOrEqualTo(dateAffichage))
				.and(
					EORepartAssociation.RAS_D_FERMETURE.isNull()
					.or(EORepartAssociation.RAS_D_FERMETURE.greaterThanOrEqualTo(dateAffichage))
				);
		ERXFetchSpecification<EORepartAssociation> fs = new ERXFetchSpecification<EORepartAssociation>(EORepartAssociation.ENTITY_NAME, qualifier, null);
//		NSArray<String> prefetchingKeyPaths = new NSMutableArray<String>();
//		prefetchingKeyPaths.add(EORepartAssociation.TO_INDIVIDUS_ASSOCIES.key());
//		fs.setPrefetchingRelationshipKeyPaths(prefetchingKeyPaths);
		NSArray<EORepartAssociation> ras = fs.fetchObjects(edc());
		return Collections2.filter(ras, new Predicate<EORepartAssociation>() {
			public boolean apply(EORepartAssociation ra) {
				return ra.toPersonne() instanceof IIndividu;
			}
		});
	}

	public Collection<EORepartStructure> getRepartStructures(EOStructure structure) {
		EOQualifier qualifier = EORepartStructure.TO_STRUCTURE_GROUPE.eq(structure);
		ERXFetchSpecification<EORepartStructure> fs = new ERXFetchSpecification<EORepartStructure>(EORepartStructure.ENTITY_NAME, qualifier, null);
//		NSArray<String> prefetchingKeyPaths = new NSMutableArray<String>();
//		prefetchingKeyPaths.add(EORepartStructure.TO_INDIVIDU_ELTS.key());
//		fs.setPrefetchingRelationshipKeyPaths(prefetchingKeyPaths);
		NSArray<EORepartStructure> rs = fs.fetchObjects(edc());  
		return Collections2.filter(rs, new Predicate<EORepartStructure>() {
			public boolean apply(EORepartStructure rs) {
				return rs.toIndividu() != null;
			}
		});
	}

	public NSArray<EOAffectation> getAffectations(EOStructure structure, NSTimestamp dateAffichage) {
		EOQualifier qualifierAffectations = 
			EOAffectation.TO_STRUCTURE.eq(structure)
				.and(EOAffectation.D_DEB_AFFECTATION.lessThanOrEqualTo(dateAffichage))
				.and(
					EOAffectation.D_FIN_AFFECTATION.isNull()
						.or(EOAffectation.D_FIN_AFFECTATION.greaterThanOrEqualTo(dateAffichage))
				)
				.and(EOAffectation.TEM_VALIDE.eq("O"));
		ERXFetchSpecification<EOAffectation> fs = new ERXFetchSpecification<EOAffectation>(EOAffectation.ENTITY_NAME, qualifierAffectations, null);
		NSArray<String> prefetchingKeyPaths = new NSMutableArray<String>();
		prefetchingKeyPaths.add(EOAffectation.TO_INDIVIDU.key());
		fs.setPrefetchingRelationshipKeyPaths(prefetchingKeyPaths);
		return fs.fetchObjects(edc());
	}
	
	private Boolean isAffichageAffectationsSeulement() {
		return ERXProperties.booleanForKeyWithDefault(PARAM_AFFECTATION_SEULEMENT, false);
	}
	
	private ImmutableMap<Integer, EOIndividuDiplome> diplomesPourIndividus(
			NSArray<EOIndividu> individus) {
		NSArray<EOIndividuDiplome> individusDiplomes = 
				EOIndividuDiplome.fetch(
						edc(), 
						ERXQ.and(
								ERXQ.in(EOIndividuDiplome.TO_INDIVIDU_KEY, individus),
								ERXQ.in(ERXQ.keyPath(EOIndividuDiplome.TO_DIPLOME_KEY, EODiplome.C_DIPLOME_KEY), codesDiplomes)
							),
						null
					);
		individusDiplomes = ERXArrayUtilities.arrayWithoutDuplicateKeyValue(individusDiplomes, EOIndividuDiplome.TO_INDIVIDU_KEY);
		return Maps.uniqueIndex(individusDiplomes, new Function<EOIndividuDiplome, Integer>() {
			public Integer apply(EOIndividuDiplome individuDiplome) {
				return individuDiplome.toIndividu().persId();
			}
		});
	}

	
	private EOIndividuDiplome diplomePourIndividu(EOIndividu individu) {
		return diplomesPourIndividus(new NSArray<EOIndividu>(individu)).get(individu.persId());
	}
	
	
	public EORepartAssociation getEditingMembre() {
		return editingMembre;
	}

	public void setEditingMembre(EORepartAssociation editingMembre) {
		this.editingMembre = editingMembre;
	}

	public void initialiserEditingMembrePourAjout() {
		editingMembre = new EORepartAssociation();
		getEditionMembreEditingContext().insertObject(editingMembre);
		
		editingMembre.setToStructureRelationship(structureRecherche());
		editingMembre.setPersIdCreation(component.getUtilisateurPersId());
		editingMembre.setPersIdModification(component.getUtilisateurPersId());
		editingMembre.setDCreation(ERXTimestampUtilities.today());
		editingMembre.setDModification(ERXTimestampUtilities.today());
		
		// On créé aussi le repart_structure
		editingMembreRepartStructure = new EORepartStructure();
		getEditionMembreEditingContext().insertObject(editingMembreRepartStructure);

		editingMembreRepartStructure.setToStructureGroupeRelationship(structureRecherche().localInstanceIn(getEditionMembreEditingContext()));
		editingMembreRepartStructure.setPersIdCreation(component.getUtilisateurPersId());
		editingMembreRepartStructure.setPersIdModification(component.getUtilisateurPersId());
		editingMembreRepartStructure.setDCreation(ERXTimestampUtilities.today());
		editingMembreRepartStructure.setDModification(ERXTimestampUtilities.today());
		
	}
	

	/**
	 * 
	 */
	public void initialiserEditingMembrePourEdition() {
		
		InformationsMembre informationsMembre = getInformationsMembresDisplayGroup().selectedObject();
		
		NSArray<EORepartAssociation> repartAssociations = new NSArray<EORepartAssociation>();
		
		if (informationsMembre.getStatut() != null) {
		
			EOQualifier qualifier = 
				ERXQ.and(
					ERXQ.equals(EORepartAssociation.PERS_ID_KEY, informationsMembre.getIndividu().persId()),
					ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, informationsMembre.getStatut().localInstanceIn(getEditionMembreEditingContext())),
					ERXQ.equals(EORepartAssociation.RAS_D_OUVERTURE_KEY, informationsMembre.getDebut()),
					ERXQ.equals(EORepartAssociation.RAS_D_FERMETURE_KEY, informationsMembre.getFin())
				);

			repartAssociations = structureRecherche().localInstanceIn(getEditionMembreEditingContext()).toRepartAssociationsElts(qualifier);
		}

		if (repartAssociations.isEmpty()) {
			editingMembre = new EORepartAssociation();
			getEditionMembreEditingContext().insertObject(editingMembre);
			
			editingMembre.setToStructureRelationship(structureRecherche());
			editingMembre.setPersIdCreation(component.getUtilisateurPersId());
			editingMembre.setPersIdModification(component.getUtilisateurPersId());
			editingMembre.setDCreation(ERXTimestampUtilities.today());
			editingMembre.setDModification(ERXTimestampUtilities.today());
			
			EOIndividu individu = informationsMembre.getIndividu().localInstanceIn(getEditionMembreEditingContext());
			editingMembre.setToPersonne(individu);
		} else {
			editingMembre = ERXArrayUtilities.firstObject(repartAssociations).localInstanceIn(getEditionMembreEditingContext());
		}
		
	}

	public void enregistrerNouveauMembre() throws NSValidation.ValidationException {
		
		if (editingMembre.toPersonneElt() == null) {
			throw new NSValidation.ValidationException("Vous devez sélectionner un individu");
		}
		
		verificationsSaisiesEditionMembre();
		
		EOIndividu individu = (EOIndividu) editingMembre.toPersonneElt();
		
		// on vérifie si un repart association existe entre la personne, le labo, l'association mais sans date
		EORepartAssociation repartAssociation = repartAssociationIndividuLaboratoireSansPeriode(individu, structureRecherche(), editingMembre.toAssociation());
		if (repartAssociation != null) {
			repartAssociation.setRasDOuverture(editingMembre.rasDOuverture());
			repartAssociation.setRasDFermeture(editingMembre.rasDFermeture());
			repartAssociation.setToAssociationRelationship(editingMembre.toAssociation());
			
			editingMembre = repartAssociation;
			individu = (EOIndividu) editingMembre.toPersonneElt();
		}
		
		if (EOStructureForGroupeSpec.isPersonneInGroupe(individu.localInstanceIn(edc()), structureRecherche())) {
			editingMembreRepartStructure.setToStructureGroupeRelationship(null);
			editingMembreRepartStructure.delete();
		} else {
			editingMembreRepartStructure.setToPersonneElt(individu);
		}
		
		// pas de try, catch car il y a un throw sur la méthode
		getEditionMembreEditingContext().saveChanges();

		InformationsMembre informationsMembre = new InformationsMembre();
		informationsMembre.setIndividu(individu);
		informationsMembre.setGrade(IndividusGradesService.creerNouvelleInstance(getEditionMembreEditingContext()).gradePourIndividu(individu, ERXTimestampUtilities.today()));
		informationsMembre.setDiplome(diplomePourIndividu(individu));
		informationsMembre.setStatut(editingMembre.toAssociation());
		informationsMembre.setDebut(editingMembre.rasDOuverture());
		informationsMembre.setFin(editingMembre.rasDFermeture());
		
		// on retire de la liste le membre qui existe deja sans données
		if (repartAssociation != null) {
			for (Object membre : getInformationsMembresDataSource().fetchObjects()) {
				if (individu.noIndividu().equals(((InformationsMembre) membre).getIndividu().noIndividu())) {
					getInformationsMembresDataSource().deleteObject(membre);
				}
			}
		} 
		
		getInformationsMembresDataSource().insertObject(informationsMembre);
		getInformationsMembresDisplayGroup().fetch();
	}

	private void verificationsSaisiesEditionMembre() throws NSValidation.ValidationException {
		
		if (editingMembre.toAssociation() == null) {
			throw new NSValidation.ValidationException("Vous devez sélectionner un statut");
		}
		if (editingMembre.rasDOuverture() == null) {
			throw new NSValidation.ValidationException("Vous devez renseigner la date de début");
		}
		if (editingMembre.rasDFermeture() != null) {
			if (editingMembre.rasDFermeture().before(editingMembre.rasDOuverture())) {
				throw new NSValidation.ValidationException("La date de fin est inférieure à la date de début");
			}
		}
		
		EOIndividu individu = (EOIndividu) editingMembre.toPersonneElt();

		if (individuEstDejaMembreSurPeriode(individu, editingMembre)) {
			throw new NSValidation.ValidationException("L'individu est déjà présent sur tout ou partie de la période choisie");
		}

	}
	
	public void enregistrerMembreExistant() throws NSValidation.ValidationException {
		
		verificationsSaisiesEditionMembre();
		
		InformationsMembre informationsMembre = getInformationsMembresDisplayGroup().selectedObject();
		
		EORepartAssociation repartAssociationEntreGroupePartenaireEtLaboratoire = getRepartAssociationEntreGroupePartenaireEtLaboratoire(getEditionMembreEditingContext(), informationsMembre);
		if (repartAssociationEntreGroupePartenaireEtLaboratoire != null) {
			repartAssociationEntreGroupePartenaireEtLaboratoire.setRasDOuverture(editingMembre.rasDOuverture());
			repartAssociationEntreGroupePartenaireEtLaboratoire.setRasDFermeture(editingMembre.rasDFermeture());
		}
		
		// on vérifie si un repart association existe entre la personne, le labo, l'association mais sans date
		EORepartAssociation repartAssociation = repartAssociationIndividuLaboratoireSansPeriode(informationsMembre.getIndividu(), structureRecherche(), editingMembre.toAssociation());
		if (repartAssociation != null) {
			repartAssociation.setRasDOuverture(editingMembre.rasDOuverture());
			repartAssociation.setRasDFermeture(editingMembre.rasDFermeture());
			repartAssociation.setToAssociationRelationship(editingMembre.toAssociation());
			
			// pour empecher la création d'un nouvel repart association (dans initialiserEditingMembrePourEdition())
			getEditionMembreEditingContext().revert();
			
			editingMembre = repartAssociation;
		}
		
		// pas de try, catch car il y a un throw sur la méthode
		getEditionMembreEditingContext().saveChanges();

		informationsMembre.setStatut(editingMembre.toAssociation());
		informationsMembre.setDebut(editingMembre.rasDOuverture());
		informationsMembre.setFin(editingMembre.rasDFermeture());
	}

	public void annulerEditionMembre() {
		editingMembre = null;
		editingMembreRepartStructure = null;
		getEditionMembreEditingContext().revert();
	}
	
	private Boolean individuEstDejaMembreSurPeriode(EOIndividu individu, EORepartAssociation periode) {
		NSArray<EORepartAssociation> repartAssociations = structureRecherche().toRepartAssociationsElts(
				ERXQ.and(
					ERXQ.equals(EORepartAssociation.PERS_ID_KEY, individu.persId()),
					ERXQ.in(EORepartAssociation.TO_ASSOCIATION_KEY, statutsMembres()),
					ERXQ.or(
							EORepartAssociation.RAS_D_OUVERTURE.between(periode.rasDOuverture(), periode.rasDFermeture(), true),
							EORepartAssociation.RAS_D_FERMETURE.between(periode.rasDOuverture(), periode.rasDFermeture(), true)
					)
				)
			);
		// si le tableau contient seulement la periode passee en parametres
		NSArray<EORepartAssociation> t = repartAssociations.mutableClone();
		for (EORepartAssociation ra : repartAssociations) {
			if (ERXEOControlUtilities.eoEquals(repartAssociations.get(0), periode)) {
				t.remove(ra);
			}
		}
		return t.count() > 0;
	}
	
	private EORepartAssociation repartAssociationIndividuLaboratoireSansPeriode(EOIndividu individu, EOStructure laboratoire, EOAssociation association) {
		
		EOQualifier qualifier = ERXQ.and(
				EORepartAssociation.TO_ASSOCIATION.eq(association),
				EORepartAssociation.PERS_ID.eq(individu.persId()),
				EORepartAssociation.TO_STRUCTURE.eq(laboratoire), 
				EORepartAssociation.RAS_D_OUVERTURE.isNull(),
				EORepartAssociation.RAS_D_FERMETURE.isNull()
				);
		return EORepartAssociation.fetchFirstByQualifier(edc(), qualifier);
	}
	
	public void supprimerMembreSelectionne(Integer persIdUtilisateur) throws Exception {
		InformationsMembre informationsMembre = getInformationsMembresDisplayGroup().selectedObject();
		
		NSArray<EORepartAssociation> repartAssociations = structureRecherche().toRepartAssociationsElts(
				ERXQ.and(
					ERXQ.equals(EORepartAssociation.TO_PERSONNE_ELT_KEY, informationsMembre.getIndividu()),
					ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, informationsMembre.getStatut()),
					ERXQ.equals(EORepartAssociation.RAS_D_OUVERTURE_KEY, informationsMembre.getDebut()),
					ERXQ.equals(EORepartAssociation.RAS_D_FERMETURE_KEY, informationsMembre.getFin())
				)
			);
		EORepartAssociation repartAssociation = ERXArrayUtilities.firstObject(repartAssociations);
		
		NSArray<EORepartStructure> repartStructures = structureRecherche().toRepartStructuresElts(
				ERXQ.equals(EORepartStructure.TO_PERSONNE_ELT_KEY, informationsMembre.getIndividu())
			);
		EORepartStructure repartStructure = ERXArrayUtilities.firstObject(repartStructures);
		
		//suppression du repart association entre le labo et le contrat de these
		ContratPartenaire cpToDelete = null;
		EODoctorant doctorant = EODoctorant.fetchFirstByQualifier(getEditionMembreEditingContext(), EODoctorant.TO_INDIVIDU_FWKPERS.eq(informationsMembre.getIndividu()));
		NSArray<EODoctorantThese> lesTheses = doctorant.toDoctorantTheses();
		for (EODoctorantThese laThese : lesTheses) {
			ContratPartenaire cp = laThese.toContrat().partenaireForPersId(structureRecherche().persId());
			if (cp != null) {
				EORepartAssociation ra = EORepartAssociation.fetchFirstByQualifier(getEditionMembreEditingContext(), 
						ERXQ.and(
								EORepartAssociation.TO_STRUCTURE.eq(cp.contrat().groupePartenaire()),
								EORepartAssociation.PERS_ID.eq(structureRecherche().persId())
						));
				if (ra.rasDOuverture().equals(informationsMembre.getDebut()) && ra.rasDFermeture().equals(informationsMembre.getFin())) {
					cpToDelete = cp;
				}
				
			}
			
			//Suppression du repartStructure entre le contrat et le laboratoire
			/*NSArray<EORepartStructure> repartStructuresContratLaboratoire = laThese.toContrat().groupePartenaire().toRepartStructuresElts(
					ERXQ.equals(EORepartStructure.TO_PERSONNE_ELT_KEY, structureRecherche().localInstanceIn(getEditionMembreEditingContext()))
				);
			EORepartStructure repartStructureContratLaboratoire = ERXArrayUtilities.firstObject(repartStructuresContratLaboratoire);
			if (repartStructureContratLaboratoire != null) {
				laThese.toContrat().groupePartenaire().deleteToRepartStructuresEltsRelationship(repartStructureContratLaboratoire);
			}*/
			
		}
		
		if (repartAssociation != null) {
			structureRecherche().deleteToRepartAssociationsEltsRelationship(repartAssociation);
		}
		
		if (repartStructure != null) {
			structureRecherche().deleteToRepartStructuresEltsRelationship(repartStructure);
		}
		
		FactoryContratPartenaire fcp = new FactoryContratPartenaire(getEditionMembreEditingContext(), true);
		try {
			fcp.supprimerLeRole(cpToDelete, FactoryAssociation.shared().laboratoireTheseAssociation(getEditionMembreEditingContext()));
			fcp.supprimerContratPartenaire(cpToDelete, persIdUtilisateur);
			getEditionMembreEditingContext().saveChanges();
		} catch (Exception e) {
			getEditionMembreEditingContext().revert();
			throw e;
		}
		
		getInformationsMembresDataSource().deleteObject(informationsMembre);
		getInformationsMembresDisplayGroup().fetch();
	}

	public void reinitialiserLesDonnees() {
		informationsMembresDataSource = null;
		informationsMembresDisplayGroup = null;
	}
	
	public NSTimestamp getDateAffichage() {
		return dateAffichage;
	}

	public void setDateAffichage(NSTimestamp dateAffichage) {
		this.dateAffichage = dateAffichage;
	}

	public NSArray<EORepartAssociation> getTousLesRolesDansLaStructure() {
		if (editingMembre.toPersonne() != null) {
			NSArray<EOAssociation> statutsMembresAvecAffectatioNsArray = 
					statutsMembres().arrayByAddingObject(FactoryAssociation.shared().affectation(edc()));
			tousLesRolesDansLaStructure = EORepartAssociation
			.fetchAll(
					edc(),
					EORepartAssociation.TO_STRUCTURE.eq(structureRecherche())
					.and(EORepartAssociation.TO_ASSOCIATION.in(statutsMembresAvecAffectatioNsArray))
					.and(EORepartAssociation.PERS_ID.eq(editingMembre.toPersonne().persId()))
				);
		} else {
			tousLesRolesDansLaStructure = new NSArray<EORepartAssociation>();
		}
		return tousLesRolesDansLaStructure;
	}

	public void setTousLesRolesDansLaStructure(NSArray<EORepartAssociation> tousLesRolesDansLaStructure) {
		this.tousLesRolesDansLaStructure = tousLesRolesDansLaStructure;
	}
	
	public EORepartAssociation getCurrentRoleDansStructure() {
		return currentRoleDansStructure;
	}

	public void setCurrentRoleDansStructure(EORepartAssociation currentRoleDansStructure) {
		this.currentRoleDansStructure = currentRoleDansStructure;
	}
	
	private EORepartAssociation getRepartAssociationEntreGroupePartenaireEtLaboratoire(EOEditingContext edc, InformationsMembre informationsMembre) {
		EODoctorant doctorant = EODoctorant.fetchFirstByQualifier(edc, EODoctorant.TO_INDIVIDU_FWKPERS.eq(informationsMembre.getIndividu()));
		if (doctorant != null) {
			NSArray<EODoctorantThese> lesTheses = doctorant.toDoctorantTheses();
			for (EODoctorantThese laThese : lesTheses) {
				ContratPartenaire cp = laThese.toContrat().partenaireForPersId(structureRecherche().persId());
				if (cp != null) {
					EORepartAssociation ra = EORepartAssociation.fetchFirstByQualifier(edc, 
							ERXQ.and(
									EORepartAssociation.TO_STRUCTURE.eq(cp.contrat().groupePartenaire()),
									EORepartAssociation.PERS_ID.eq(structureRecherche().persId())
							));
					return ra;
				}
			}
		}
		return null;
	}
	
	private boolean isPhysalisEnProduction() {
		if (isPhysalisEnProduction == null) {
			isPhysalisEnProduction = FwkCktlPersonne.paramManager.isCodeActivationActif(ParametresRecherche.PARAM_PHYSALIS_UTILISATION);
		}
		return isPhysalisEnProduction;
	}
	
	public static class InformationsMembre implements NSKeyValueCoding {

		public static final ERXKey<EOIndividu> INDIVIDU = new ERXKey<EOIndividu>("individu");
		public static final ERXKey<NSTimestamp> DEBUT = new ERXKey<NSTimestamp>("debut");
		public static final ERXKey<NSTimestamp> FIN = new ERXKey<NSTimestamp>("fin");
		
		private EOIndividu individu;
		private EOGrade grade;
		private NSTimestamp debut;
		private NSTimestamp fin;
		private EOAssociation statut;
		private EOIndividuDiplome diplome;
		private Boolean modifiable = true;
		

		public EOIndividu getIndividu() {
			return individu;
		}

		public void setIndividu(EOIndividu individu) {
			this.individu = individu;
		}

		public EOGrade getGrade() {
			return grade;
		}

		public void setGrade(EOGrade grade) {
			this.grade = grade;
		}

		public NSTimestamp getDebut() {
			return debut;
		}

		public void setDebut(NSTimestamp debut) {
			this.debut = debut;
		}

		public NSTimestamp getFin() {
			return fin;
		}

		public void setFin(NSTimestamp fin) {
			this.fin = fin;
		}

		public EOAssociation getStatut() {
			return statut;
		}

		public void setStatut(EOAssociation statut) {
			this.statut = statut;
		}

		public EOIndividuDiplome getDiplome() {
			return diplome;
		}

		public void setDiplome(EOIndividuDiplome diplome) {
			this.diplome = diplome;
		}

		public void takeValueForKey(Object value, String key) {
			NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, value,
					key);
		}

		public Object valueForKey(String key) {
			return NSKeyValueCoding.DefaultImplementation
					.valueForKey(this, key);
		}

		public Boolean getModifiable() {
			return modifiable;
		}

		public void setModifiable(Boolean modifiable) {
			this.modifiable = modifiable;
		}

	}

}
