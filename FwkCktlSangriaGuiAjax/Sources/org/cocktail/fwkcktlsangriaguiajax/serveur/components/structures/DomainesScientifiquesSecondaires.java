package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class DomainesScientifiquesSecondaires extends AFwkCktlSangriaComponent {
	
	private final static String BINDING_structure = "structure";
	
	
	private EODomaineScientifique currentDomaineScientifique;
	private EODomaineScientifique currentDomaineScientifiqueForSelect;
	private EODomaineScientifique selectedDomaineScientifique;
	
    public DomainesScientifiquesSecondaires(WOContext context) {
        super(context);
    }
    
    public String domainesScientifiquesContainerId() {
    	return getComponentId() + "_domainesScientifiquesContainer";
    }
    
    public String domainesScientifiquesSelectWindow() {
    	return getComponentId() + "_domainesScientifiquesSelectWindow";
    }
    
    
    public EOStructure structure() {
    	return (EOStructure) valueForBinding(BINDING_structure);
    }
    
    public NSArray<EODomaineScientifique> domaineScientifiques() {
    	return StructuresRechercheHelper.domainesScientifiquesSecondaireUniteRecherches(edc(), structure(), true);
    }
    
	public NSArray<EODomaineScientifique> domainesScientifiquesForSelect() {
		return EODomaineScientifique.fetchAll(edc());
	}

	public void setCurrentDomaineScientifique(EODomaineScientifique currentDomaineScientifique) {
		this.currentDomaineScientifique = currentDomaineScientifique;
	}

	public EODomaineScientifique currentDomaineScientifique() {
		return currentDomaineScientifique;
	}

	public void setCurrentDomaineScientifiqueForSelect(
			EODomaineScientifique currentDomaineScientifiqueForSelect) {
		this.currentDomaineScientifiqueForSelect = currentDomaineScientifiqueForSelect;
	}

	public EODomaineScientifique currentDomaineScientifiqueForSelect() {
		return currentDomaineScientifiqueForSelect;
	}

	public void setSelectedDomaineScientifique(
			EODomaineScientifique selectedDomaineScientifique) {
		this.selectedDomaineScientifique = selectedDomaineScientifique;
	}

	public EODomaineScientifique selectedDomaineScientifique() {
		return selectedDomaineScientifique;
	}
	
	public WOActionResults ajouterDomaineScientifique() {
		StructuresRechercheHelper.ajouterDomaineScientifiqueSecondaireUniteRecherche(edc(), currentDomaineScientifiqueForSelect(), structure(), true);
		CktlAjaxWindow.close(context(), domainesScientifiquesSelectWindow());
		return doNothing();
	}
	
	public WOActionResults supprimerDomaineScientifique() {
		StructuresRechercheHelper.supprimerDomaineScientifiqueSecondaireUniteRecherche(edc(), currentDomaineScientifique(), structure(), true);
		return doNothing();
	}
	
	
	
    
}