package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class StructureRechercheSelect extends AFwkCktlSangriaComponent {
	
	public final String BINDING_onSelectionnerAction = "onSelectionnerAction"; 
	public final String BINDING_selectedStructureRecherche = "selectedStructureRecherche"; 

	private EOStructure currentStructureRecherche;
	private EOStructure selectedStructureRecherche;
	
	
	private Boolean ongletUnitesDeRechercSelected = true;
	private Boolean ongletEcolesDoctoralesSelected = false;
	private Boolean ongletFederationsDeRechercheSelected = false;
	private Boolean ongletCellulesDeValorisationSelected = false;
	private Boolean ongletAutresStructuresDeRechercheSelected = false;
	
	
    public StructureRechercheSelect(WOContext context) {
        super(context);
    }
    
    public String ongletsId() {
    	return getComponentId() + "_onglets";
    }
    
	public String ongletUnitesRechercheId() {
		return getComponentId() + "_ongletUnitesRecherche";
	}
	
	public String ongletEcolesDoctoralesId() {
		return getComponentId() + "_ongletEcolesDoctorales";
	}
	
	public String ongletFederationsDeRechercheId() {
		return getComponentId() + "_ongletFederationsDeRecherche";
	}
	
	public String ongletCellulesDeValorisationId() {
		return getComponentId() + "_ongletCellulesDeValorisation";
	}
	
	public String ongletAutresStructuresDeRechercheId() {
		return getComponentId() + "_ongletAutresStructuresDeRecherche";
	}
	
	
	public void setOngletUnitesDeRechercSelected(Boolean ongletUnitesDeRechercSelected) {
		this.ongletUnitesDeRechercSelected = ongletUnitesDeRechercSelected;
	}

	public Boolean ongletUnitesDeRechercSelected() {
		return ongletUnitesDeRechercSelected;
	}

	public void setOngletEcolesDoctoralesSelected(Boolean ongletEcolesDoctoralesSelected) {
		this.ongletEcolesDoctoralesSelected = ongletEcolesDoctoralesSelected;
	}

	public Boolean ongletEcolesDoctoralesSelected() {
		return ongletEcolesDoctoralesSelected;
	}
	
	public void setOngletFederationsDeRechercheSelected(Boolean ongletFederationsDeRechercheSelected) {
		this.ongletFederationsDeRechercheSelected = ongletFederationsDeRechercheSelected;
	}

	public Boolean ongletFederationsDeRechercheSelected() {
		return ongletFederationsDeRechercheSelected;
	}

	public void setOngletCellulesDeValorisationSelected(Boolean ongletCellulesDeValorisationSelected) {
		this.ongletCellulesDeValorisationSelected = ongletCellulesDeValorisationSelected;
	}

	public Boolean ongletCellulesDeValorisationSelected() {
		return ongletCellulesDeValorisationSelected;
	}

	public void setOngletAutresStructuresDeRechercheSelected(Boolean ongletAutresStructuresDeRechercheSelected) {
		this.ongletAutresStructuresDeRechercheSelected = ongletAutresStructuresDeRechercheSelected;
	}

	public Boolean ongletAutresStructuresDeRechercheSelected() {
		return ongletAutresStructuresDeRechercheSelected;
	}
	
	
    public NSArray<EOStructure> listeDesLaboratoires() {
    	return StructuresRechercheHelper.unitesDeRecherche(edc());
    }
    
    public NSArray<EOStructure> listeDesEcolesDoctorales() {
    	return StructuresRechercheHelper.ecolesDoctorale(edc());
    }
    
    public NSArray<EOStructure> listeDesFederationsDeRecherche() {
    	return StructuresRechercheHelper.federationsDeRecherche(edc());
    }
    
    public NSArray<EOStructure> listeDesCellulesDeValorisation() {
    	return StructuresRechercheHelper.cellulesDeValorisation(edc());
    }
    
    public NSArray<EOStructure> listeDesAutresStructures() {
    	return StructuresRechercheHelper.autresStructuresDeRecherche(edc());
    }
	

	public void setCurrentStructureRecherche(EOStructure currentStructureRecherche) {
		this.currentStructureRecherche = currentStructureRecherche;
	}

	public EOStructure currentStructureRecherche() {
		return currentStructureRecherche;
	}
	
	public WOActionResults selectionnerStructureRecherche() {
		setSelectedStructureRecherche(currentStructureRecherche());
		String onSelectionnerAction = (String) valueForBinding(BINDING_onSelectionnerAction);
		return performParentAction(onSelectionnerAction);
	}
	public void setSelectedStructureRecherche(EOStructure selectedStructureRecherche) {
		this.selectedStructureRecherche = selectedStructureRecherche;
		setValueForBinding(selectedStructureRecherche, BINDING_selectedStructureRecherche);
	}
	public EOStructure selectedStructureRecherche() {
		return selectedStructureRecherche;
	}

}