package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering;

public class StructureRechercheAutoComplete extends AFwkCktlSangriaComponent {
    
	private final String BINDING_selectedStructure = "selectedStructure";
	private final String BINDING_reset = "reset";
	
	private String autoCompleteId;
    
    private NSArray<EOStructure> structuresRecherche = null;
    private EOStructure currentStructure;
    private EOStructure selectedStructure;
    
    
    private String token;
    
	public StructureRechercheAutoComplete(WOContext context) {
        super(context);
    }
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if(wantReset()) {
			setToken(null);
			setWantReset(false);
		}
		super.appendToResponse(response, context);
	}
    
	public Boolean wantReset() {
		return booleanValueForBinding(BINDING_reset, false);
	}
	
	public void setWantReset(Boolean reset) {
		setValueForBinding(reset, BINDING_reset);
	}
	
    public String getAutoCompleteId() {
        if (autoCompleteId == null)
            autoCompleteId = ERXWOContext.safeIdentifierName(context(), true);
        return autoCompleteId;
    }
    
    public String selectionnerStructureRechercheWindowId() {
    	return getComponentId() + "_selectionnerStructureRechercheWindow";
    }
    
    public String autoCompleteContainerId() {
    	return getComponentId() + "_autoCompleteContainer";
    }
    
    public NSArray<EOStructure> structuresRecherche() {
        if (structuresRecherche == null) {
        	EOQualifier structureRechercheQualifier = ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, "SR");
        	ERXSortOrdering sortOrdering = ERXSortOrdering.sortOrderingWithKey(EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
        	structuresRecherche = EOStructure.fetchAll(edc(), structureRechercheQualifier , sortOrdering.array());
        }
        
        EOQualifier tokenQualifier = ERXQ.or(
        									ERXQ.contains(EOStructure.LL_STRUCTURE_KEY, token()),
        									ERXQ.contains(EOStructure.LC_STRUCTURE_KEY, token())
        								);
        
        return EOQualifier.filteredArrayWithQualifier(structuresRecherche, tokenQualifier);
    }


	public void setToken(String token) {
		this.token = token;
	}


	public String token() {
		return token;
	}


	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
	}


	public EOStructure currentStructure() {
		return currentStructure;
	}


	public void setSelectedStructure(EOStructure selectedStructure) {
		this.selectedStructure = selectedStructure;
		setValueForBinding(selectedStructure, BINDING_selectedStructure);
	}


	public EOStructure selectedStructure() {
		selectedStructure = (EOStructure) valueForBinding(BINDING_selectedStructure);
		return selectedStructure;
	}
    

	public WOActionResults selectionnerStructureRecherche() {
		CktlAjaxWindow.close(context(), selectionnerStructureRechercheWindowId());
		setToken(selectedStructure().llStructure());
		return doNothing();
		
	}
	
	public WOActionResults annulerSelectionnerStructureRecherche() {
		CktlAjaxWindow.close(context(), selectionnerStructureRechercheWindowId());
		return doNothing();
	}
	
	
	
}