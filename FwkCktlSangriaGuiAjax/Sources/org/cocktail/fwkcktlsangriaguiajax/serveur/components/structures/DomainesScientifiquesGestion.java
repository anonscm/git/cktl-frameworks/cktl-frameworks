package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxTVFavouriteCell;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartStructureRechercheDomaineScientifique;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.foundation.ERXTimestampUtilities;

public class DomainesScientifiquesGestion extends AFwkCktlSangriaComponent {
	
	private static String BINDING_structureDeRecherche = "structureDeRecherche"; 
	
	private static NSArray<CktlAjaxTableViewColumn> colonnes = null;
	
	private NSArray<EORepartStructureRechercheDomaineScientifique> repartsDomainesScientifiques = null;
	
	private EORepartStructureRechercheDomaineScientifique currentRepartDomaineScientifique;
	private static String CURRENT_REPART_DOMAINE_SCIENTIFQUE_KEY = "currentRepartDomaineScientifique";
	
	private EORepartStructureRechercheDomaineScientifique selectedRepartDomaineScientifique = null; 
	
	private ERXDisplayGroup<EORepartStructureRechercheDomaineScientifique> repartsDomainesScientifiquesDisplayGroup = null;
	
	private EODomaineScientifique currentDomaineScientifique;

	private EODomaineScientifique domaineScientifiqueAAjouter;
	
	
    public DomainesScientifiquesGestion(WOContext context) {
        super(context);
    }
    
    public EOStructure structureDeRecherche() {
    	return (EOStructure) valueForBinding(BINDING_structureDeRecherche);
    }
    
    public String domainesScientifiquesTableViewId() {
    	return getComponentId() + "_domainesScientifiquesTableView";
    }
    
    public String ajoutDomaineScientifiqueWindowId() {
    	return getComponentId() + "_ajoutDomaineScientifiqueWindow";
    }
    
    public String domainesScientifiquesTableViewContainerId() {
    	return getComponentId() + "_domainesScientifiquesTableViewContainer";
    }
    
    public static NSArray<CktlAjaxTableViewColumn> _colonnes() {
      synchronized (DomainesScientifiquesGestion.class) {
   
      	if(colonnes == null) {
      		
      		colonnes = new NSMutableArray<CktlAjaxTableViewColumn>();
      		
      		CktlAjaxTableViewColumn principalColonne = new CktlAjaxTableViewColumn();
      		principalColonne.setLibelle("&nbsp;");
      		principalColonne.setComponent(CktlAjaxTVFavouriteCell.class.getSimpleName());
      		CktlAjaxTableViewColumnAssociation principalColonneAssociation = new CktlAjaxTableViewColumnAssociation();
      		principalColonne.setRowCssClass("alignToCenter useMinWidth nowrap");
      		principalColonneAssociation.setObjectForKey(CURRENT_REPART_DOMAINE_SCIENTIFQUE_KEY  + "." + EORepartStructureRechercheDomaineScientifique.PRINCIPAL_KEY, "favourite");
      		principalColonne.setAssociations(principalColonneAssociation);
      		colonnes.add(principalColonne);
  
      		CktlAjaxTableViewColumn numeroColonne = new CktlAjaxTableViewColumn();
      		numeroColonne.setLibelle("Numéro");
      		numeroColonne.setRowCssClass("alignToCenter useMinWidth nowrap");
      		CktlAjaxTableViewColumnAssociation numeroColonneAssociation = new CktlAjaxTableViewColumnAssociation(CURRENT_REPART_DOMAINE_SCIENTIFQUE_KEY + "." + EORepartStructureRechercheDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY + "." + EODomaineScientifique.DS_CODE_KEY, " ");
      		numeroColonne.setAssociations(numeroColonneAssociation);
      		colonnes.add(numeroColonne);
  
      		CktlAjaxTableViewColumn libelleColonne = new CktlAjaxTableViewColumn();
      		libelleColonne.setLibelle("Libellé");
      		CktlAjaxTableViewColumnAssociation libelleColonneAssociation = new CktlAjaxTableViewColumnAssociation(CURRENT_REPART_DOMAINE_SCIENTIFQUE_KEY + "." + EORepartStructureRechercheDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY + "." + EODomaineScientifique.DS_LIBELLE_KEY, " ");
      		libelleColonne.setAssociations(libelleColonneAssociation);
      		colonnes.add(libelleColonne);
      		
      	}
      }
    	return colonnes;
    }
    
    public NSArray<CktlAjaxTableViewColumn> colonnes() {
    	return _colonnes().immutableClone();
	}

	public NSArray<EORepartStructureRechercheDomaineScientifique> repartsDomainesScientifiques() {
		if(repartsDomainesScientifiques == null) {
			repartsDomainesScientifiques = EORepartStructureRechercheDomaineScientifique.fetchAll(edc(), EORepartStructureRechercheDomaineScientifique.UNITE_RECHERCHE.eq(structureDeRecherche()), EORepartStructureRechercheDomaineScientifique.PRINCIPAL.ascs());
		}
		return repartsDomainesScientifiques;
	}

	public void setRepartsDomainesScientifiques(NSArray<EORepartStructureRechercheDomaineScientifique> repartsDomainesScientifiques) {
		this.repartsDomainesScientifiques = repartsDomainesScientifiques;
	}

	public EORepartStructureRechercheDomaineScientifique currentRepartDomaineScientifique() {
		return currentRepartDomaineScientifique;
	}

	public void setCurrentRepartDomaineScientifique(EORepartStructureRechercheDomaineScientifique currentRepartDomaineScientifique) {
		this.currentRepartDomaineScientifique = currentRepartDomaineScientifique;
	}

	public EORepartStructureRechercheDomaineScientifique selectedRepartDomaineScientifique() {
		return selectedRepartDomaineScientifique;
	}

	public void setSelectedRepartDomaineScientifique(EORepartStructureRechercheDomaineScientifique selectedRepartDomaineScientifique) {
		this.selectedRepartDomaineScientifique = selectedRepartDomaineScientifique;
	}
	
	public ERXDisplayGroup<EORepartStructureRechercheDomaineScientifique> repartsDomainesScientifiquesDisplayGroup() {
		if(repartsDomainesScientifiquesDisplayGroup == null) {
			repartsDomainesScientifiquesDisplayGroup = new ERXDisplayGroup<EORepartStructureRechercheDomaineScientifique>();
			repartsDomainesScientifiquesDisplayGroup.setDelegate(this);
			repartsDomainesScientifiquesDisplayGroup.setObjectArray(repartsDomainesScientifiques());
			repartsDomainesScientifiquesDisplayGroup.setFetchesOnLoad(true);
		}
		return repartsDomainesScientifiquesDisplayGroup;
	}
	
	public void displayGroupDidChangeSelection(WODisplayGroup group) {
		setSelectedRepartDomaineScientifique((EORepartStructureRechercheDomaineScientifique) group.selectedObject());
	}
	
	public NSArray<EODomaineScientifique> domainesScientifiques() {
		return EODomaineScientifique.fetchAll(edc(), EODomaineScientifique.DS_CODE.ascs());
	}

	public EODomaineScientifique currentDomaineScientifique() {
		return currentDomaineScientifique;
	}

	public void setCurrentDomaineScientifique(EODomaineScientifique currentDomaineScientifique) {
		this.currentDomaineScientifique = currentDomaineScientifique;
	}
	
	public WOActionResults selectionnerDomaineScientifque() {
		NSArray<EODomaineScientifique> domainesDeLaStructure = (NSArray<EODomaineScientifique>) repartsDomainesScientifiquesDisplayGroup().allObjects().valueForKey(EORepartStructureRechercheDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY);
		if(domainesDeLaStructure.contains(getDomaineScientifiqueAAjouter())) {
			session().addSimpleErrorMessage("Erreur", "La structure est déjà rattachée au domaine scientifique sélectionné");
			return doNothing();
		}
		
		EORepartStructureRechercheDomaineScientifique _repartStructureRechercheDomaineScientifique = EORepartStructureRechercheDomaineScientifique.creerInstance(edc());
		_repartStructureRechercheDomaineScientifique.setDomaineScientifiqueRelationship(getDomaineScientifiqueAAjouter());
		_repartStructureRechercheDomaineScientifique.setUniteRechercheRelationship(structureDeRecherche());
		_repartStructureRechercheDomaineScientifique.setPrincipal(false);
		_repartStructureRechercheDomaineScientifique.setPersIdCreation(getUtilisateurPersId());
		_repartStructureRechercheDomaineScientifique.setPersIdModification(getUtilisateurPersId());
		_repartStructureRechercheDomaineScientifique.setDCreation(ERXTimestampUtilities.today());
		_repartStructureRechercheDomaineScientifique.setDModification(ERXTimestampUtilities.today());
		
		repartsDomainesScientifiquesDisplayGroup().insertObjectAtIndex(_repartStructureRechercheDomaineScientifique, 
																		repartsDomainesScientifiquesDisplayGroup().indexOfLastDisplayedObject());
		
		AjaxUpdateContainer.updateContainerWithID(domainesScientifiquesTableViewContainerId(), context());
		
		return doNothing();
	}
	
	public WOActionResults supprimerSelectedDomaineScientifique() {
		if(repartsDomainesScientifiquesDisplayGroup().allObjects().count() == 1) {
			session().addSimpleErrorMessage("Attention", "Attention, il s'agit du dernier domaine scientifique." +
															"Vous devez en ajouter un autre avant de pouvoir supprimer celui-ci.");
			return doNothing();			
		}
		if(selectedRepartDomaineScientifique().principal()) {
			session().addSimpleErrorMessage("Attention", "Avant de pouvoir supprimer le domaine " +
															"scientifique sélectionné, vous devez choisir un autre domaine scientifique principal");
			return doNothing();
		}
		EORepartStructureRechercheDomaineScientifique _selectedRepartDomaineScientifique = selectedRepartDomaineScientifique();
		_selectedRepartDomaineScientifique.setDomaineScientifiqueRelationship(null);
		_selectedRepartDomaineScientifique.setUniteRechercheRelationship(null);
		repartsDomainesScientifiquesDisplayGroup().deleteSelection();
		edc().deleteObject(_selectedRepartDomaineScientifique);
		return doNothing();
	}
	
	public WOActionResults definirCommePrincipal() {
		for(EORepartStructureRechercheDomaineScientifique r : repartsDomainesScientifiquesDisplayGroup().allObjects()) {
			if(r.principal()) {
				r.setPrincipal(false);
			}
		}
		selectedRepartDomaineScientifique().setPrincipal(true);
		return doNothing();
	}

	
	public EODomaineScientifique getDomaineScientifiqueAAjouter() {
		return domaineScientifiqueAAjouter;
	}

	public void setDomaineScientifiqueAAjouter(EODomaineScientifique domaineScientifiqueAAjouter) {
		this.domaineScientifiqueAAjouter = domaineScientifiqueAAjouter;
		selectionnerDomaineScientifque();
	}

}