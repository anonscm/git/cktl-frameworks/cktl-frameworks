package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures.controller.StructureRechercheMembresController;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSValidation;

import er.ajax.AjaxUpdateContainer;
import er.extensions.foundation.ERXTimestampUtilities;

/**
 * 
 * @author jlafourc
 * 
 */
public class StructureRechercheMembres extends AFwkCktlSangriaComponent {

	private static final long serialVersionUID = -1546395938458352434L;

	private static final String BINDING_STRUCTURE_RECHERCHE = "structureRecherche";
	private static final String BINDING_TYPE_MEMBRES_ASSOCIATION = "typesMembresAssociation";

	public static final String MODE_AJOUT = "MODE_AJOUT";
	public static final String MODE_EDITION = "MODE_EDITION";

	private StructureRechercheMembresController controller;

	private Boolean resetMembreSrch;

	private String mode;

	private EOAssociation currentEditingStatutMembre;

	public StructureRechercheMembres(WOContext context) {
		super(context);
	}

	public StructureRechercheMembresController getController() {
		if (controller == null) {
			controller = new StructureRechercheMembresController(edc(), this);
		}
		return controller;
	}

	public String getMembresTableViewId() {
		return getComponentId() + "_membresTableView";
	}

	public String getMembresTableViewContainerId() {
		return getComponentId() + "_membresTableViewContainer";
	}

	public String getEditionMembreWindowId() {
		return getComponentId() + "_editionMembreWindow";
	}

	public String getBoutonsContainerId() {
		return getComponentId() + "_boutonsContainer";
	}

	public String getEditingStatutMembreId() {
		return getComponentId() + "_editingStatutMembre";
	}

	public String getEditingDateDebutId() {
		return getComponentId() + "_editingDateDebut";
	}

	public String getEditingDateFinId() {
		return getComponentId() + "_editingDateFin";
	}

	public String getRolesDansLaStructureContainerId() {
		return getComponentId() + "_rolesDansLaStructureContainer";
	}

	public EOStructure structureRecherche() {
		return (EOStructure) valueForBinding(BINDING_STRUCTURE_RECHERCHE);
	}

	public EOAssociation typesMembresAssociation() {
		return (EOAssociation) valueForBinding(BINDING_TYPE_MEMBRES_ASSOCIATION);
	}

	public Boolean isModeEdition() {
		return StringUtils.equals(MODE_EDITION, mode);
	}

	public Boolean isModeAjout() {
		return StringUtils.equals(MODE_AJOUT, mode);
	}

	public WOActionResults ajoutMembre() {

		mode = MODE_AJOUT;

		getController().initialiserEditingMembrePourAjout();
		CktlAjaxWindow.open(context(), getEditionMembreWindowId(), "Ajouter un membre");
		return doNothing();

	}

	public WOActionResults editerMembre() {
		mode = MODE_EDITION;
		getController().initialiserEditingMembrePourEdition();
		CktlAjaxWindow.open(context(), getEditionMembreWindowId(), "Editer un membre");
		return doNothing();

	}

	public WOActionResults supprimerMembre() {
		try {
			getController().supprimerMembreSelectionne(getUtilisateurPersId());
			AjaxUpdateContainer.updateContainerWithID(getMembresTableViewContainerId(), context());
			session().addSimpleSuccessMessage("Succès", "Le membre a bien été supprimé");
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}

	public EOAssociation getCurrentEditingStatutMembre() {
		return currentEditingStatutMembre;
	}

	public void setCurrentEditingStatutMembre(EOAssociation currentEditingStatutMembre) {
		this.currentEditingStatutMembre = currentEditingStatutMembre;
	}

	public WOActionResults valider() {
		if (isModeAjout()) {
			try {
				getController().enregistrerNouveauMembre();
				CktlAjaxWindow.close(context(), getEditionMembreWindowId());
				AjaxUpdateContainer.updateContainerWithID(getMembresTableViewContainerId(), context());
				setResetMembreSrch(true);
				session().addSimpleSuccessMessage("Succès", "Le membre a bien été ajouté");
			} catch (NSValidation.ValidationException e) {
				session().addSimpleErrorMessage("Erreur", e);
			}
		}
		if (isModeEdition()) {
			try {
				getController().enregistrerMembreExistant();
				CktlAjaxWindow.close(context(), getEditionMembreWindowId());
				AjaxUpdateContainer.updateContainerWithID(getMembresTableViewContainerId(), context());
				session().addSimpleSuccessMessage("Succès", "Le membre a bien été modifié");
			} catch (NSValidation.ValidationException e) {
				session().addSimpleErrorMessage("Erreur", e);
			}
		}
		return doNothing();
	}

	public WOActionResults annuler() {
		CktlAjaxWindow.close(context(), getEditionMembreWindowId());
		controller.annulerEditionMembre();
		setResetMembreSrch(true);
		return doNothing();
	}

	public Boolean getResetMembreSrch() {
		return resetMembreSrch;
	}

	public void setResetMembreSrch(Boolean resetMembreSrch) {
		this.resetMembreSrch = resetMembreSrch;
	}

	public WOActionResults afficherAjourdhui() {
		getController().setDateAffichage(ERXTimestampUtilities.today());
		getController().reinitialiserLesDonnees();
		return doNothing();
	}

	public WOActionResults afficherALaDate() {
		getController().reinitialiserLesDonnees();
		return doNothing();
	}

	public Boolean peutEditerMembre() {

		Boolean peutEditerMembre = false;

		Boolean aUneSelection = getController().getInformationsMembresDisplayGroup().selectedObject() != null;

		if (aUneSelection) {
			peutEditerMembre = getController().getInformationsMembresDisplayGroup().selectedObject().getModifiable();
		}

		return peutEditerMembre;

	}

	public Boolean peutSupprimerMembre() {
		return peutEditerMembre();
	}

}