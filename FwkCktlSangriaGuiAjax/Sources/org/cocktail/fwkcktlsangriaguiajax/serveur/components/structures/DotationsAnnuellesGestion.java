package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODotationAnnuelle;
import org.cocktail.fwkcktlrecherche.server.metier.EOTypeDotation;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSMutableSet;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXFetchSpecification;

public class DotationsAnnuellesGestion extends AFwkCktlSangriaComponent {
	
	public static final String BINDING_structureRecherche = "structureRecherche";
	
	
	private final static String DOTATION_KEY = "dotation";
	private EODotationAnnuelle dotation;
	
	private final static String ANNEE_KEY = "ANNEE";
	private final static String MONTANT_KEY = "MONTANT";
	private final static String STRUCTURE_KEY = "STRUCTURE";
	private final static String TYPE_DOTATION_KEY = "TYPE_DOTATION";
	
	
	private NSMutableArray<String> _colonnesKeys = null;
	private NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = null;
	
	private WODisplayGroup dotationsAnnuellesDisplayGroup = null;
	
	private  EODotationAnnuelle selectedDotationAnnuelle;
	
	private EODotationAnnuelle editingDotationAnnelle;
	private EOEditingContext dotationEditingContext = null;
	
	private EOStructure currentStructureProvenance;
	private EOTypeDotation currentTypeDotation;
	
	private Boolean editingDotation = false;
	
    public DotationsAnnuellesGestion(WOContext context) {
        super(context);
    }
    
    // Identification des composants graphiques
    
    public String dotationsAnnuellesTableViewId() {
    	return getComponentId() + "_dotationsAnnuellesTableView";
    }
    
    public String dotationsAnnuellesTableViewContainerId() {
    	return getComponentId() + "_dotationsAnnuellesTableViewContainer";
    }
    
    public String ajoutDotationWindowId() {
    	return getComponentId() + "_ajoutDotationWindowId";
    }
    
    public String detailsDotationContainerId() {
    	return getComponentId() + "_detailsDotationContainer";
    }
    
    public EOEditingContext dotationEditingContext() {
    	if(dotationEditingContext == null) {
    		dotationEditingContext = ERXEC.newEditingContext(edc());
    	}
    	return dotationEditingContext;
    }
    
    public EOStructure structureRecherche() {
    	return (EOStructure) valueForBinding(BINDING_structureRecherche);
    }
    
    public NSArray<EODotationAnnuelle> dotationsAnnuelles() {
    	EOQualifier qualifier = EODotationAnnuelle.STRUCTURE.eq(structureRecherche());
    	ERXFetchSpecification<EODotationAnnuelle> fetchSpecification = new ERXFetchSpecification<EODotationAnnuelle>(EODotationAnnuelle.ENTITY_NAME, qualifier, EODotationAnnuelle.ANNEE.descs());
    	fetchSpecification.setIncludeEditingContextChanges(true);
    	return fetchSpecification.fetchObjects(edc());
    }

    // Configuration de la TableView
    
	private NSMutableArray<String> _colonnesKeys() {
		if(_colonnesKeys == null) {
			_colonnesKeys = new NSMutableArray<String>();
			_colonnesKeys().add(ANNEE_KEY);
			_colonnesKeys().add(MONTANT_KEY);
			_colonnesKeys().add(STRUCTURE_KEY);
			_colonnesKeys().add(TYPE_DOTATION_KEY);
		}
		return _colonnesKeys;
	}
	
	private NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap() {
		if(_colonnesMap == null) {
			
			_colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();
			
			CktlAjaxTableViewColumn annee_colonne = new CktlAjaxTableViewColumn();
			annee_colonne.setLibelle("Année");
			annee_colonne.setOrderKeyPath(EODotationAnnuelle.ANNEE_KEY);
			CktlAjaxTableViewColumnAssociation annee_colonne_ass = new CktlAjaxTableViewColumnAssociation(DOTATION_KEY + "." + EODotationAnnuelle.ANNEE, "");
			annee_colonne.setAssociations(annee_colonne_ass);
			_colonnesMap.takeValueForKey(annee_colonne, ANNEE_KEY);
	
			CktlAjaxTableViewColumn montant_colonne = new CktlAjaxTableViewColumn();
			montant_colonne.setLibelle("Montant");
			montant_colonne.setOrderKeyPath(EODotationAnnuelle.MONTANT_KEY);
			CktlAjaxTableViewColumnAssociation montant_colonne_ass = new CktlAjaxTableViewColumnAssociation(DOTATION_KEY + "." + EODotationAnnuelle.MONTANT, "");
			montant_colonne_ass.setNumberformat("#,##0.00 €");
			montant_colonne.setAssociations(montant_colonne_ass);
			_colonnesMap.takeValueForKey(montant_colonne, MONTANT_KEY);
			
			CktlAjaxTableViewColumn structure_colonne = new CktlAjaxTableViewColumn();
			structure_colonne.setLibelle("Provenance");
			structure_colonne.setOrderKeyPath(EODotationAnnuelle.STRUCTURE_PROVENANCE_KEY + "." + EOStructure.LC_STRUCTURE_KEY);
			CktlAjaxTableViewColumnAssociation structure_colonne_ass = new CktlAjaxTableViewColumnAssociation(DOTATION_KEY + "." + EODotationAnnuelle.STRUCTURE_PROVENANCE_KEY + "." + EOStructure.STR_AFFICHAGE_KEY, "");
			structure_colonne.setAssociations(structure_colonne_ass);
			_colonnesMap.takeValueForKey(structure_colonne, STRUCTURE_KEY);
			
			CktlAjaxTableViewColumn type_dotation_colonne = new CktlAjaxTableViewColumn();
			type_dotation_colonne.setLibelle("Type");
			type_dotation_colonne.setOrderKeyPath(EODotationAnnuelle.TYPE_DOTATION_KEY + "." + EOTypeDotation.LIBELLE_KEY);
			CktlAjaxTableViewColumnAssociation d_fin_colonne_ass = new CktlAjaxTableViewColumnAssociation(DOTATION_KEY + "." + EODotationAnnuelle.TYPE_DOTATION_KEY + "." + EOTypeDotation.LIBELLE_KEY, "");
			type_dotation_colonne.setAssociations(d_fin_colonne_ass);
			_colonnesMap.takeValueForKey(type_dotation_colonne, TYPE_DOTATION_KEY);
			
			
		}
		return _colonnesMap;
	
	}
    
    
    public NSArray<CktlAjaxTableViewColumn> colonnes() {
    	NSMutableArray<CktlAjaxTableViewColumn> colonnes = new NSMutableArray<CktlAjaxTableViewColumn>();
    	for(String key : _colonnesKeys()) {
    		colonnes.add((CktlAjaxTableViewColumn) _colonnesMap().valueForKey(key));
    	}
    	return colonnes;
    }

    

    
    
	public WODisplayGroup dotationsAnnuellesDisplayGroup() {
		if(dotationsAnnuellesDisplayGroup == null) {
			dotationsAnnuellesDisplayGroup = new ERXDisplayGroup<EORepartAssociation>();
			dotationsAnnuellesDisplayGroup.setDelegate(new DotationsAnnuellesDisplayGroupDelegate());
			dotationsAnnuellesDisplayGroup.setObjectArray(dotationsAnnuelles());	
			dotationsAnnuellesDisplayGroup.setSelectsFirstObjectAfterFetch(true);
			dotationsAnnuellesDisplayGroup.fetch();
		}
		return dotationsAnnuellesDisplayGroup;
	}
	
	public Boolean isSelectionOnTableViewEnabled() {
		return !isEditingDotation();
	}
	
	public class DotationsAnnuellesDisplayGroupDelegate {	
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedDotationAnnuelle((EODotationAnnuelle) group.selectedObject());
		}
    }
    
	public void setSelectedDotationAnnuelle(EODotationAnnuelle selectedDotationAnnuelle) {
		this.selectedDotationAnnuelle = selectedDotationAnnuelle;
	}

	public EODotationAnnuelle selectedDotationAnnuelle() {
		return selectedDotationAnnuelle;
	}

	
	
	public void setDotation(EODotationAnnuelle dotation) {
		this.dotation = dotation;
	}

	public EODotationAnnuelle dotation() {
		return dotation;
	}

	public void setEditingDotationAnnelle(EODotationAnnuelle editingDotationAnnelle) {
		this.editingDotationAnnelle = editingDotationAnnelle;
	}

	public EODotationAnnuelle editingDotationAnnelle() {
		return editingDotationAnnelle;
	}

	public WOActionResults creerNouvelleDotation() {
		editingDotationAnnelle = EODotationAnnuelle.creerInstance(dotationEditingContext(), getUtilisateurPersId()); 
		editingDotationAnnelle.setStructureRelationship(structureRecherche().localInstanceIn(dotationEditingContext()));
		return doNothing();
	}
	
	public WOActionResults validerCreationNouvelleDotation() {
		try {
			dotationEditingContext().saveChanges();
			CktlAjaxWindow.close(context(), ajoutDotationWindowId());
			dotationsAnnuellesDisplayGroup().setObjectArray(dotationsAnnuelles());
			AjaxUpdateContainer.updateContainerWithID(dotationsAnnuellesTableViewContainerId(), context());
			session().addSimpleInfoMessage("Ok", "La dotation a bien été ajoutée. Pensez à enregistrer les modifications");
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	
	public WOActionResults annulerCreationNouvelleDotation() {
		dotationEditingContext().revert();
		CktlAjaxWindow.close(context(), ajoutDotationWindowId());
		return doNothing();
	}

	public void setCurrentStructureProvenance(EOStructure currentStructureProvenance) {
		this.currentStructureProvenance = currentStructureProvenance;
	}

	public EOStructure currentStructureProvenance() {
		return currentStructureProvenance;
	}
	
	public void setSelectedStructureProvenance(EOStructure selectedStructureProvenance) {
		editingDotationAnnelle().setStructureProvenanceRelationship(selectedStructureProvenance);
	}

	public EOStructure selectedStructureProvenance() {
		return editingDotationAnnelle().structureProvenance();
	}

	public NSArray<EOStructure> possiblesStructuresProvenancePourStructureRecherche() {
		NSMutableSet<EOStructure> structures = new NSMutableSet<EOStructure>();
		structures.addAll(StructuresRechercheHelper.tutellesStructureRecherche(dotationEditingContext(), structureRecherche().localInstanceIn(dotationEditingContext())));
		structures.addAll(StructuresRechercheHelper.etablissementsRattachementPourStructureRecherche(dotationEditingContext(), structureRecherche().localInstanceIn(dotationEditingContext())));
		return structures.allObjects();
	}
	
	public void setCurrentTypeDotation(EOTypeDotation currentTypeDotation) {
		this.currentTypeDotation = currentTypeDotation;
	}

	public EOTypeDotation currentTypeDotation() {
		return currentTypeDotation;
	}

	public void setSelectedTypeDotation(EOTypeDotation selectedTypeDotation) {
		editingDotationAnnelle().setTypeDotationRelationship(selectedTypeDotation);
	}

	public EOTypeDotation selectedTypeDotation() {
		return editingDotationAnnelle().typeDotation();
	}
	
	public NSArray<EOTypeDotation> possiblesTypesDotations() {
		return EOTypeDotation.fetchAll(dotationEditingContext());
	}

	public void setEditingDotation(Boolean editingDotation) {
		this.editingDotation = editingDotation;
	}

	public Boolean isEditingDotation() {
		return editingDotation;
	}
	
	public WOActionResults editerDotation() {
		setEditingDotation(true);
		setEditingDotationAnnelle(selectedDotationAnnuelle().localInstanceIn(dotationEditingContext()));
		AjaxUpdateContainer.updateContainerWithID(dotationsAnnuellesTableViewContainerId(), context());
		return doNothing();
	}
	
	public WOActionResults validerModificationsDotation() {
		try {
			dotationEditingContext().saveChanges();
			session().addSimpleInfoMessage("Ok", "La dotation a bien été modifiée. Pensez à enregistrer les modifications");
			AjaxUpdateContainer.updateContainerWithID(dotationsAnnuellesTableViewContainerId(), context());
			setEditingDotation(false);
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	
	public WOActionResults annulerModificationsDotation() {
		dotationEditingContext().revert();
		setEditingDotation(false);
		AjaxUpdateContainer.updateContainerWithID(dotationsAnnuellesTableViewContainerId(), context());
		return doNothing();
	}
	
	public WOActionResults supprimerDotation() {
		try {
			dotationEditingContext().deleteObject(selectedDotationAnnuelle().localInstanceIn(dotationEditingContext()));
			dotationEditingContext().saveChanges();
			AjaxUpdateContainer.updateContainerWithID(detailsDotationContainerId(), context());
			dotationsAnnuellesDisplayGroup().setObjectArray(dotationsAnnuelles());
			AjaxUpdateContainer.updateContainerWithID(dotationsAnnuellesTableViewContainerId(), context());
			session().addSimpleInfoMessage("Ok", "La dotation a bien été supprimée. Pensez à enregistrer les modifications");
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}

}