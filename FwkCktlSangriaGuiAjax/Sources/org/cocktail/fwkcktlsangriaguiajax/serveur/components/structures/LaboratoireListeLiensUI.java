package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures;

import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneSrch;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class LaboratoireListeLiensUI extends AFwkCktlSangriaComponent {
	
	public static final String BINDING_qualifierForGroupes = "qualifierForGroupes";
	private static final String BINDING_callbackOnAjouterLaboratoire = "callbackOnAjouter";
	private static final String BINDING_callbackOnSupprimerLaboratoire = "callbackOnSupprimer";
	
    public LaboratoireListeLiensUI(WOContext context) {
        super(context);
    }
    
    
    public WOActionResults onAjoutLaboratoire() {
    	return performParentAction((String) valueForBinding(BINDING_callbackOnAjouterLaboratoire));
    }
    
    public WOActionResults onSupprimerLaboratoire() {
    	return performParentAction((String) valueForBinding(BINDING_callbackOnSupprimerLaboratoire));
    }
    
    
    public NSDictionary<String, Object> srchComponentAttributes() {
    	
    	NSDictionary<String, Object> componentAttributes = new NSMutableDictionary<String, Object>();
    	
    	if(hasBinding(BINDING_qualifierForGroupes))
    		componentAttributes.put(BINDING_qualifierForGroupes, valueForBinding(BINDING_qualifierForGroupes));
    	if(hasBinding(BINDING_utilisateurPersId))
    		componentAttributes.put(BINDING_utilisateurPersId, valueForBinding(BINDING_utilisateurPersId));
    	if(hasBinding(BINDING_editingContext))
    		componentAttributes.put(BINDING_editingContext, valueForBinding(BINDING_editingContext));
    	
    	componentAttributes.put(PersonneSrch.BINDING_showIndividus, Boolean.FALSE);
    	componentAttributes.put(PersonneSrch.BINDING_showPersonnesExternes, Boolean.FALSE);
    	componentAttributes.put(PersonneSrch.BINDING_showPersonnesInternes, Boolean.TRUE);
    	componentAttributes.put(PersonneSrch.BINDING_showStructures, Boolean.TRUE);

    	
    	return componentAttributes;
    	
    }
}