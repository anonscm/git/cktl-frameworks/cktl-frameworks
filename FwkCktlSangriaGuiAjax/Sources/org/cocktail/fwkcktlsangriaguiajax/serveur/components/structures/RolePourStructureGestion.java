package org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXTimestampUtilities;

public class RolePourStructureGestion extends AFwkCktlSangriaComponent {
	
	public static final String BINDING_structure = "structure"; 
  public static final String BINDING_titre = "titre";
  public static final String BINDING_titreFenetre = "titreFenetre";
	public static final String BINDING_titreColonne = "titreColonne";
	public static final String BINDING_role = "role";
	public static final String BINDING_checkRoleUniqueSurPeriode = "checkRoleUniqueSurPeriode";
	
	public static final String BINDING_showPersonnesExternes = "showPersonnesExternes";
	public static final String BINDING_showStructures = "showStructures";
	public static final String BINDING_showPersonnesInternes = "showPersonnesInternes";
	public static final String BINDING_showRadioInterneExterne = "showRadioInterneExterne";
	public static final String BINDING_showIndividus = "showIndividus";
	
	public static final String BINDING_forceRefresh = "forceRefresh";
	
	public static final String REPART_ASSOCIATION_COURANT_KEY = "repartAssociationCourant.";
	
	public static final String RESPONSABLE_KEY = "responsableCourant";
	public static final String D_DEBUT_KEY = EORepartAssociation.RAS_D_OUVERTURE_KEY;
	public static final String D_FIN_KEY = EORepartAssociation.RAS_D_FERMETURE_KEY;
	
	public NSMutableArray<String> _colonnesKeys = null;
	public NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = null;

	private EORepartAssociation repartAssociationCourant = null;
	private EORepartAssociation selectedRepartAssociation = null;
	private ERXDisplayGroup<EORepartAssociation> responsablesDisplayGroup = null;
	
	
	private Integer etapeCourante = 1;
	
	private EORepartAssociation editingRepartResponsable;

	private NSTimestamp dateDebutCreation;
	private NSTimestamp dateFinCreation;
	
	private IPersonne nouveauResponsable;
//	private EOEditingContext responsableEditingContext = null;
	
	private Boolean showDetails = false;
	
	public Boolean forceRefresh() {
		Boolean _forceRefresh = booleanValueForBinding(BINDING_forceRefresh, false);
		if(_forceRefresh) {
			setValueForBinding(false, BINDING_forceRefresh);
		}
		return _forceRefresh; 
	}
	
    public RolePourStructureGestion(WOContext context) {
        super(context);
    }
    

	
    
    public String responsablesTableViewId() {
    	return getComponentId() + "_responsablesTableView";
    }
    
    public String ajouterResponsableWindowId() {
    	return getComponentId() + "_ajouterResponsableWindow";
    }
    
    public String boutonsEtapeUneContainerId() {
    	return getComponentId() + "_boutonsEtapeUneContainer";
    }
    
    public String etapesContainerId() {
    	return getComponentId() + "_etapesContainer";
    }
    
    public String responsablesTableViewContainerId() {
    	return getComponentId() + "_responsablesTableViewContainer";
    }
    
    public String boutonsEtapeDeuxContainerId() {
    	return getComponentId() + "_boutonsEtapeDeuxContainer";
    }
    
    public String boutonsContainerId() {
    	return getComponentId() + "_boutonsContainer";
    }
    
    public String modifierResponsableWindowId() {
    	return getComponentId() + "_modifierResponsableWindow";
    }
    
    public void setDateDebutCreation(NSTimestamp dateDebutCreation) {
		this.dateDebutCreation = dateDebutCreation;
	}




	public NSTimestamp dateDebutCreation() {
		return dateDebutCreation;
	}




	public void setDateFinCreation(NSTimestamp dateFinCreation) {
		this.dateFinCreation = dateFinCreation;
	}




	public NSTimestamp dateFinCreation() {
		return dateFinCreation;
	}




	public EOStructure structure() {
    	return (EOStructure) valueForBinding(BINDING_structure);
    }
    
    public EOAssociation role() {
    	return (EOAssociation) valueForBinding(BINDING_role);
    }
    
    public String titreColonne() {
    	return (String) valueForBinding(BINDING_titreColonne); 
    }

    public String titre() {
    	return (String) valueForBinding(BINDING_titre); 
    }
    
    public String titreFenetre() {
      return (String) valueForBinding(BINDING_titreFenetre);
    }
    
    public Boolean checkRoleUniqueSurPeriode() {
    	return booleanValueForBinding(BINDING_checkRoleUniqueSurPeriode, false);
    }
    
    
	private NSMutableArray<String> _colonnesKeys() {
		if(_colonnesKeys == null) {
			_colonnesKeys = new NSMutableArray<String>();
			_colonnesKeys().add(RESPONSABLE_KEY);
			_colonnesKeys().add(D_DEBUT_KEY);
			_colonnesKeys().add(D_FIN_KEY);
		}
		return _colonnesKeys;
	}
	
	private NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap() {
		if(_colonnesMap == null) {
			
			_colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();
			
			CktlAjaxTableViewColumn responsable_colonne = new CktlAjaxTableViewColumn();
			responsable_colonne.setLibelle(titreColonne());
			CktlAjaxTableViewColumnAssociation responsable_colonne_ass = new CktlAjaxTableViewColumnAssociation(RESPONSABLE_KEY + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY, "");
			responsable_colonne.setAssociations(responsable_colonne_ass);
			_colonnesMap.takeValueForKey(responsable_colonne, RESPONSABLE_KEY);
	
			CktlAjaxTableViewColumn d_debut_colonne = new CktlAjaxTableViewColumn();
			d_debut_colonne.setLibelle("Du");
			d_debut_colonne.setRowCssClass("alignToCenter useMinWidth nowrap");
			CktlAjaxTableViewColumnAssociation d_debut_colonne_ass = new CktlAjaxTableViewColumnAssociation(REPART_ASSOCIATION_COURANT_KEY + D_DEBUT_KEY, "-");
			d_debut_colonne_ass.setDateformat("%d/%m/%y");
			d_debut_colonne.setAssociations(d_debut_colonne_ass);
			_colonnesMap.takeValueForKey(d_debut_colonne, D_DEBUT_KEY);
			
			CktlAjaxTableViewColumn d_fin_colonne = new CktlAjaxTableViewColumn();
			d_fin_colonne.setLibelle("Au");
			d_fin_colonne.setRowCssClass("alignToCenter useMinWidth nowrap");
			CktlAjaxTableViewColumnAssociation d_fin_colonne_ass = new CktlAjaxTableViewColumnAssociation(REPART_ASSOCIATION_COURANT_KEY + D_FIN_KEY, "-");
			d_fin_colonne_ass.setDateformat("%d/%m/%y");
			d_fin_colonne.setAssociations(d_fin_colonne_ass);
			_colonnesMap.takeValueForKey(d_fin_colonne, D_FIN_KEY);
			
		}
		return _colonnesMap;
	
	}
    
    
    public NSArray<CktlAjaxTableViewColumn> colonnes() {
    	NSMutableArray<CktlAjaxTableViewColumn> colonnes = new NSMutableArray<CktlAjaxTableViewColumn>();
    	for(String key : _colonnesKeys()) {
    		colonnes.add((CktlAjaxTableViewColumn) _colonnesMap().valueForKey(key));
    	}
    	return colonnes;
    }

	public void setRepartAssociationCourant(EORepartAssociation repartAssociationCourant) {
		this.repartAssociationCourant = repartAssociationCourant;
	}

	public EORepartAssociation repartAssociationCourant() {
		return repartAssociationCourant;
	}
	
	public IPersonne responsableCourant() {
		return repartAssociationCourant().toPersonneElt();
	}
	

	
	public NSArray<EORepartAssociation> repartsResponsables() {
		EOQualifier qualifier = EORepartAssociation.TO_ASSOCIATION.eq(role());
		if(!showDetails()) {
			EOQualifier dateQualifier = ERXQ.and( 
					EORepartAssociation.RAS_D_OUVERTURE.lessThanOrEqualTo(ERXTimestampUtilities.today()),
					ERXQ.or(
							EORepartAssociation.RAS_D_FERMETURE.greaterThanOrEqualTo(ERXTimestampUtilities.today()),
							EORepartAssociation.RAS_D_FERMETURE.isNull()
					)
				);
				qualifier = ERXQ.and(qualifier, dateQualifier);
		}
		NSArray<EOSortOrdering> sortOrderings = ERXS.ascInsensitives(ERXQ.keyPath(EORepartAssociation.TO_PERSONNE_ELT_KEY, IPersonne.PERS_LIBELLE_KEY));
		return ERXS.sorted(structure().toRepartAssociationsElts(qualifier), sortOrderings);
	}
	
	public void refreshResponsablesDisplayGroup() {
		responsablesDisplayGroup().setObjectArray(repartsResponsables());
	}
	
	public WODisplayGroup responsablesDisplayGroup() {
		if(responsablesDisplayGroup == null || forceRefresh() == true) {
			responsablesDisplayGroup = new ERXDisplayGroup<EORepartAssociation>();
			responsablesDisplayGroup.setDelegate(new ResponsablesDisplayGroupDelegate());
			responsablesDisplayGroup.setObjectArray(repartsResponsables());
		}
		return responsablesDisplayGroup;
	}
	
	public void setResponsableDisplayGroup(ERXDisplayGroup<EORepartAssociation> displayGroup) {
		responsablesDisplayGroup = displayGroup;
	}
    
    public void setSelectedRepartAssociation(EORepartAssociation selectedRepartAssociation) {
		this.selectedRepartAssociation = selectedRepartAssociation;
	}

	public EORepartAssociation selectedRepartAssociation() {
		return selectedRepartAssociation;
	}

	public class ResponsablesDisplayGroupDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedRepartAssociation((EORepartAssociation) group.selectedObject());
		}
    }
	
    public Boolean etapeUne() {
    	return etapeCourante == 1;
    }
    
    public Boolean etapeDeux() {
    	return etapeCourante == 2;
    }
    
//    public EOEditingContext responsableEditingContext() {
//    	if(responsableEditingContext == null) {
//    		responsableEditingContext = ERXEC.newEditingContext(edc());
//    	}
//    	return responsableEditingContext;
//    }
    
    public WOActionResults ajouterResponsable() {
    	editingRepartResponsable = EORepartAssociation.creerInstance(edc());
    	
    	
    	editingRepartResponsable().setToAssociationRelationship(role());
    	editingRepartResponsable().setToStructureRelationship(structure());
    	editingRepartResponsable().setPersIdCreation(getUtilisateurPersId());
    	editingRepartResponsable().setPersIdModification(getUtilisateurPersId());
    	editingRepartResponsable().setDCreation(new NSTimestamp());
    	editingRepartResponsable().setDModification(new NSTimestamp());

    	etapeCourante = 1;
    	return doNothing();
    }

	public void setNouveauResponsable(IPersonne nouveauResponsable) {
		this.nouveauResponsable = nouveauResponsable;
	}

	public IPersonne nouveauResponsable() {
		return this.nouveauResponsable;
	}
    
	public Boolean noNouveauResponsableSelected() {
		return nouveauResponsable() == null;
	}
	
	public Boolean noNouveauResponsableDateDebutSelected() {
		return editingRepartResponsable().rasDOuverture() == null;
	}
	
	public WOActionResults passageEtapeDeux() {
		if(nouveauResponsable() == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez sélectionner un responsable");
			return doNothing();
		}

		etapeCourante = 2;
		return doNothing();
	}
	
	public WOActionResults retourEtapeUne() {
		etapeCourante = 1;
		return doNothing();
	}
	
	public WOActionResults annulerAjoutResponsable() {
//		responsableEditingContext().revert();
		editingRepartResponsable().delete();
		CktlAjaxWindow.close(context(), ajouterResponsableWindowId());
		return doNothing();
	}
	
	public WOActionResults validerAjoutResponsable() {
    	editingRepartResponsable().setRasDOuverture(dateDebutCreation());
    	editingRepartResponsable().setRasDFermeture(dateFinCreation());
    	
		if(editingRepartResponsable().rasDOuverture() == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez sélectionner une date de début");
			return doNothing();
		}
		if(editingRepartResponsable().rasDFermeture() != null) {
			if(editingRepartResponsable().rasDOuverture().compareTo(editingRepartResponsable().rasDFermeture()) == 1) {
				session().addSimpleErrorMessage("Erreur", "La date de début doit être antérieure à la date de fin");
				return doNothing();
			}
		}
		if(checkRoleUniqueSurPeriode()) {
			EOQualifier testQualifier =
					ERXQ.and(
						ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY, editingRepartResponsable().toStructure()),
						ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, editingRepartResponsable().toAssociation()),
						ERXQ.notEquals(EORepartAssociation.PERS_ID_KEY, editingRepartResponsable().persId()),
						ERXQ.or(
							ERXQ.between(EORepartAssociation.RAS_D_OUVERTURE_KEY, editingRepartResponsable().rasDOuverture(), editingRepartResponsable().rasDFermeture()),
							ERXQ.between(EORepartAssociation.RAS_D_FERMETURE_KEY, editingRepartResponsable().rasDOuverture(), editingRepartResponsable().rasDFermeture())
						)
					);
			ERXFetchSpecification<EORepartAssociation> fetchSpecification = new ERXFetchSpecification<EORepartAssociation>(EORepartAssociation.ENTITY_NAME, testQualifier, null);
			fetchSpecification.setIncludeEditingContextChanges(true);
			NSArray<EORepartAssociation> repartAssociations = fetchSpecification.fetchObjects(edc());
			if(!repartAssociations.isEmpty()) {
				session().addSimpleErrorMessage("Erreur", "Les dates se chevauches avec une autre personne");
				return doNothing();
			}
		}

		try {
			
//			nouveauResponsable().setValidationEditingContext(responsableEditingContext());
//			nouveauResponsable().setPersIdModification(utilisateurPersId());
			EOStructureForGroupeSpec.affecterPersonneDansGroupe(edc(), nouveauResponsable(), structure(), getUtilisateurPersId());
			editingRepartResponsable().setToPersonne(nouveauResponsable());
//			responsableEditingContext().saveChanges();
			
		}
		catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			return doNothing();
		}
		
		refreshResponsablesDisplayGroup();
		etapeCourante = 1;
		CktlAjaxWindow.close(context(), ajouterResponsableWindowId());

		return doNothing();
	}
	
	public EORepartAssociation editingRepartResponsable() {
		return editingRepartResponsable;
	}

	public void setEditingRepartResponsable(
			EORepartAssociation editingRepartResponsable) {
		this.editingRepartResponsable = editingRepartResponsable;
	}
	
	public WOActionResults modifierResponsable() {
		setDateDebutCreation(selectedRepartAssociation().rasDOuverture());
		setDateFinCreation(selectedRepartAssociation().rasDFermeture());
		setEditingRepartResponsable(selectedRepartAssociation());
		return doNothing();
	}
	
	public WOActionResults annulerModificationResponsable() {
//		responsableEditingContext().revert();
		CktlAjaxWindow.close(context(), modifierResponsableWindowId());
		return doNothing();
	}
	
	public WOActionResults validerModificationResponsable() {
    	editingRepartResponsable().setRasDOuverture(dateDebutCreation());
    	editingRepartResponsable().setRasDFermeture(dateFinCreation());

		if(editingRepartResponsable().rasDOuverture() == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez sélectionner une date de début");
			return doNothing();
		}
		if(editingRepartResponsable().rasDFermeture() != null && editingRepartResponsable().rasDOuverture().compareTo(editingRepartResponsable().rasDFermeture()) == 1) {
			session().addSimpleErrorMessage("Erreur", "La date de début doit être antérieure à la date de fin");
			return doNothing();
		}
		if(checkRoleUniqueSurPeriode()) {
			EOQualifier testQualifier =
					ERXQ.and(
						ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY, editingRepartResponsable().toStructure()),
						ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, editingRepartResponsable().toAssociation()),
						ERXQ.notEquals(EORepartAssociation.PERS_ID_KEY, editingRepartResponsable().persId()),
						ERXQ.or(
							ERXQ.between(EORepartAssociation.RAS_D_OUVERTURE_KEY, editingRepartResponsable().rasDOuverture(), editingRepartResponsable().rasDFermeture()),
							ERXQ.between(EORepartAssociation.RAS_D_FERMETURE_KEY, editingRepartResponsable().rasDOuverture(), editingRepartResponsable().rasDFermeture())
						)
					);
			ERXFetchSpecification<EORepartAssociation> fetchSpecification = new ERXFetchSpecification<EORepartAssociation>(EORepartAssociation.ENTITY_NAME, testQualifier, null);
			fetchSpecification.setIncludeEditingContextChanges(true);
			NSArray<EORepartAssociation> repartAssociations = fetchSpecification.fetchObjects(edc());
			if(!repartAssociations.isEmpty()) {
				session().addSimpleErrorMessage("Erreur", "Les dates se chevauches avec une autre personne");
				return doNothing();
			}
		}
//		try {
//			responsableEditingContext().saveChanges();
//		}
//		catch (ValidationException e) {
//			session().addSimpleErrorMessage("Erreur", e);
//		}
		refreshResponsablesDisplayGroup();
		CktlAjaxWindow.close(context(), modifierResponsableWindowId());
		return doNothing();
	}
	
	public Boolean isModifierDisabled() {
		return selectedRepartAssociation() == null;
	}
	
	public WOActionResults supprimerResponsable() {
		if(selectedRepartAssociation() != null) {
			selectedRepartAssociation().setToAssociationRelationship(null);
			selectedRepartAssociation().setToPersonne(null);
			selectedRepartAssociation().setToStructureRelationship(null);
			edc().deleteObject(selectedRepartAssociation());
			refreshResponsablesDisplayGroup();
		}
		return doNothing();
	}
	
	public WOActionResults switchShowDetails() {
		showDetails = !showDetails;
		refreshResponsablesDisplayGroup();
		AjaxUpdateContainer.updateContainerWithID(responsablesTableViewContainerId(), context());
		AjaxUpdateContainer.updateContainerWithID(boutonsContainerId(), context());
		return doNothing();
	}
	
	public Boolean showDetails() {
		return showDetails;
	}
	
	public Boolean showPersonnesExternes() {
		return booleanValueForBinding(BINDING_showPersonnesExternes, true);
	}
	public Boolean showStructures() {
		return booleanValueForBinding(BINDING_showStructures, false);
	}
	public Boolean showPersonnesInternes() {
		return booleanValueForBinding(BINDING_showPersonnesInternes, true);
	}
	public Boolean showIndividus() {
		return booleanValueForBinding(BINDING_showIndividus, true);
	}
	
	public Boolean showRadioInterneExterne() {
		return booleanValueForBinding(BINDING_showRadioInterneExterne, false);
	}
	
	
	
}