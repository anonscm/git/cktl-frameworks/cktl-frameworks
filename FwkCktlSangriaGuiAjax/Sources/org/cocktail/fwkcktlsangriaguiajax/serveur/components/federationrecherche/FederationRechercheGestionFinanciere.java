package org.cocktail.fwkcktlsangriaguiajax.serveur.components.federationrecherche;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;

public class FederationRechercheGestionFinanciere extends AFwkCktlSangriaComponent {

	private static final String BINDING_federationRecherche = "federationRecherche";
	
    public FederationRechercheGestionFinanciere(WOContext context) {
        super(context);
    }
    
    
    public EOStructure federationRecherche() {
    	return (EOStructure) valueForBinding(BINDING_federationRecherche);
    }
}