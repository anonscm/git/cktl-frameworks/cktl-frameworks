package org.cocktail.fwkcktlsangriaguiajax.serveur.components.federationrecherche;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;

public class FederationRechercheUnites extends AFwkCktlSangriaComponent {
	
	private final static String BINDING_federationRecherche = "federationRecherche";
	
	private EOAssociation membreAssociation;

		
    public FederationRechercheUnites(WOContext context) {
        super(context);
    }
    
    
    public EOStructure federationRecherche() {
    	return (EOStructure) valueForBinding(BINDING_federationRecherche);
    }

	public EOAssociation membreAssociation() {
		return membreAssociation = getFactoryAssociation().membreAssociation(edc());
	}
	
}