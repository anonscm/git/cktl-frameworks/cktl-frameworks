package org.cocktail.fwkcktlsangriaguiajax.serveur.components.federationrecherche;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;

public class FederationRechercheIdentification extends AFwkCktlSangriaComponent {
    
	public static final String BINDING_federationRecherche = "federationRecherche";
	private Boolean isEditing;
	
	private EOStructure currentTutelle;
	
	
	private EODomaineScientifique currentDomaineScientifique;
	private EODomaineScientifique selectedDomaineScientifique;
	
	private EOStructure currentEtablissementRattachementSecondaire;
	private EOStructure tutelleAAjouter;
	private EOStructure ERSAAjouter;
	
	private EOAssociation responsableAdministratifAssociation = null;
	private EOAssociation directionAssociation = null;

	private EOAssociation directeurAdjointAssociation = null;
//	private EOEditingContext personnesEditingContext;

	public FederationRechercheIdentification(WOContext context) {
        super(context);
    }
	
//	public EOEditingContext personnesEditingContext() {
//		if(personnesEditingContext == null) {
//			personnesEditingContext = ERXEC.newEditingContext(edc());
//		}
//		return personnesEditingContext;
//	}
	
	public EOStructure federationRecherche() {
		return (EOStructure) valueForBinding(BINDING_federationRecherche);
	}

	public void setIsEditing(Boolean isEditing) {
		this.isEditing = isEditing;
	}

	public Boolean isEditing() {
		return isEditing;
	}
	
	public String tutellesContainerId() {
		return getComponentId() + "_tutellesContainer";
	}
	
	public String winTutelleSelectionContainerOnCloseID() {
		return winTutelleSelectionID() + "_containerOnClose";
	}

	public String winTutelleSelectionID() {
		return getComponentId() + "winTutelleSelection";
	}
	
	public String ersContainerId() {
		return getComponentId() + "_ersContainer";
	}
	
	public String winERSSelectionContainerOnCloseID() {
		return winERSSelectionID() + "_containerOnClose";
	}

	public String winERSSelectionID() {
		return getComponentId() + "winERSSelection";
	}
	


	public void setCurrentTutelle(EOStructure currentTutelle) {
		this.currentTutelle = currentTutelle;
	}

	public EOStructure currentTutelle() {
		return currentTutelle;
	}
	

	public void setCurrentERS(
			EOStructure currentEtablissementRattachementSecondaire) {
		this.currentEtablissementRattachementSecondaire = currentEtablissementRattachementSecondaire;
	}

	public EOStructure currentERS() {
		return currentEtablissementRattachementSecondaire;
	}

	
	public NSArray<EODomaineScientifique> domainesScientifiques() {
		return EODomaineScientifique.fetchAll(edc());
	}

	public void setSelectedDomaineScientifique(EODomaineScientifique selectedDomaineScientifique) {
		this.selectedDomaineScientifique = selectedDomaineScientifique;
	}

	public EODomaineScientifique selectedDomaineScientifique() {
		return selectedDomaineScientifique;
	}

	public void setCurrentDomaineScientifique(EODomaineScientifique currentDomaineScientifique) {
		this.currentDomaineScientifique = currentDomaineScientifique;
	}

	public EODomaineScientifique currentDomaineScientifique() {
		return currentDomaineScientifique;
	}
	
	public EODomaineScientifique domaineScientifiquePrincpalPourUniteRecherche() {
		return StructuresRechercheHelper.domaineScientifiquePrincipalUniteRecherche(edc(), federationRecherche(), false);
	}
	
	public void setDomaineScientifiquePrincpalPourUniteRecherche(EODomaineScientifique domaineScientifique) {
		StructuresRechercheHelper.setDomaineScientifiquePrincipalUniteRecherche(edc(), domaineScientifique, federationRecherche(), false);
	}
	

	
	/*********************************************************/
	/*** Gestion des tutelles ***/

	public NSArray<EOStructure> tutellesPourUniteRecherche()  {
		//return StructureRechercheUtilities.tutellesUniteRecherche(edc(), federationRecherche());
		return StructuresRechercheHelper.tutellesStructureRecherche(edc(), federationRecherche());
	}
	
	
	public EOStructure tutelleAAjouter() {
		return tutelleAAjouter;
	}

	public void setTutelleAAjouter(EOStructure tutelleAAjouter) {
		this.tutelleAAjouter = tutelleAAjouter;
	}
	
	public String tutelleSearchWindowId() {
		return getComponentId() + "_tutelleSearchWindow";
	}
	
	
	public WOActionResults onAjoutTutelle() {
		StructuresRechercheHelper.ajouterTutellePourStructureRecherche(edc(), tutelleAAjouter(), federationRecherche(), getUtilisateurPersId());
		if(StructuresRechercheHelper.tutellesStructureRecherche(edc(), federationRecherche()).count() == 1) {
			StructuresRechercheHelper.definirTutellePrincipalePourStructureRecherche(edc(), tutelleAAjouter(), federationRecherche(), getUtilisateurPersId());
		}
		AjaxUpdateContainer.updateContainerWithID(tutellesContainerId(), context());
		CktlAjaxWindow.close(context(), tutelleSearchWindowId());

		return doNothing();
	}
	
	public WOActionResults annulerAjoutTutelle() {
		CktlAjaxWindow.close(context(), tutelleSearchWindowId());
		return doNothing();
	}
	
	public WOActionResults onSuppressionTutelle() {
		try {
			StructuresRechercheHelper.supprimerTutellePourStructureRecherche(edc(), currentTutelle(), federationRecherche(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	

	
	/*********************************************************/
	/*** Gestion des ERS ***/

	public NSArray<EOStructure> etablissementsDeRattachementPourUniteRecherche() {
		return StructuresRechercheHelper.etablissementsRattachementPourStructureRecherche(edc(), federationRecherche());
	}
	
	public EOStructure etablissementRattachementPrincipalPourUniteRecherche() {
		return StructuresRechercheHelper.etablissementRattachementPrincipalPourStructureRecherche(edc(), federationRecherche());
	}
	
	public EOStructure ERSAAjouter() {
		return ERSAAjouter;
	}

	public void setERSAAjouter(EOStructure ERSAAjouter) {
		this.ERSAAjouter = ERSAAjouter;
	}

	public String ERSSearchWindowId() {
		return getComponentId() + "_ERSSearchWindow";
	}

	public String jsOpenERSSearch() {
		return "openCAW_" +  ERSSearchWindowId() + "();return false;";
	}

	public WOActionResults onAjoutERS() {
//		try {
//			ERSAAjouter().setValidationEditingContext(personnesEditingContext());
//			ERSAAjouter().setPersIdModification(utilisateurPersId());
//			personnesEditingContext().saveChanges();
//	  StructuresRechercheHelper.ajouterEtablissementRattachementPourUniteRecherche(edc(), ERSAAjouter().localInstanceIn(edc()), federationRecherche(), utilisateurPersId());
			StructuresRechercheHelper.ajouterEtablissementRattachementPourUniteRecherche(edc(), ERSAAjouter(), federationRecherche(), getUtilisateurPersId());
			CktlAjaxWindow.close(context(),ERSSearchWindowId());
			AjaxUpdateContainer.updateContainerWithID(ersContainerId(), context());
//		} catch (ValidationException e) {
//			personnesEditingContext().revert();
//			session().addSimpleErrorMessage("Erreur", e);
//		}
		return doNothing();
	}
	
	public WOActionResults annulerAjoutERS() {
//		personnesEditingContext().revert();
		CktlAjaxWindow.close(context(), ERSSearchWindowId());
		return doNothing();
	}

	public WOActionResults onSuppressionERS() {
		try {
			StructuresRechercheHelper.supprimerEtablissementRattachementPourStructureRecherche(edc(), currentERS(), federationRecherche(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}

	
	public Boolean isCurrentEtablissementRattachementPrincipal() {
		if (StructuresRechercheHelper.etablissementRattachementPrincipalPourStructureRecherche(edc(), federationRecherche()) != null) {
			return StructuresRechercheHelper.etablissementRattachementPrincipalPourStructureRecherche(edc(), federationRecherche()).equals(currentERS());
		}
		return false;
	}
	
	public WOActionResults setIsCurrentEtablissementRattachementPrincipal(Boolean isCurrentEtablissementRattachementPrincipal) {
		StructuresRechercheHelper.definirEtablissementRattachementPrincipalPourUniteRecherche(edc(), currentERS(), federationRecherche(), getUtilisateurPersId());
		return doNothing();
	}
	
	public Boolean isCurrentTutellePrincipale() {
		if (StructuresRechercheHelper.tutellePrincipalePourStructureRecherche(edc(), federationRecherche()) != null) {
			return StructuresRechercheHelper.tutellePrincipalePourStructureRecherche(edc(), federationRecherche()).equals(currentTutelle());
		}
		return false;
	}
	
	public WOActionResults setIsCurrentTutellePrincipale(Boolean isCurrentTutellePrincipale) {
		StructuresRechercheHelper.definirTutellePrincipalePourStructureRecherche(edc(), currentTutelle(), federationRecherche(), getUtilisateurPersId());
		return doNothing();
	}
	

	public EOAssociation responsableAdministratifAssociation() {
		return getFactoryAssociation().secretraireAdministrativeAssociation(edc());
	}

	public EOAssociation directionAssociation() {
		return getFactoryAssociation().directionAssociation(edc());
	}
	
	public EOAssociation directeurAdjointAssociation() {
		return getFactoryAssociation().directeurAdjointAssociation(edc());
	}
	
  public EOQualifier qualifierForGroupeTutelles() {
    String codeStructureGroupeTutelles = EOGrhumParametres.parametrePourCle(edc(), "org.cocktail.sangria.groupes.tutelles");
     if(codeStructureGroupeTutelles != null) {
       return EOStructure.C_STRUCTURE.eq(codeStructureGroupeTutelles);
     } else {
       return null;
     }
     
   }
  
	 public NSArray<EOStructure> listeDesTutelles() {
		 return StructuresRechercheHelper.listeDesTutelles(edc());
	 }
	  
	
}