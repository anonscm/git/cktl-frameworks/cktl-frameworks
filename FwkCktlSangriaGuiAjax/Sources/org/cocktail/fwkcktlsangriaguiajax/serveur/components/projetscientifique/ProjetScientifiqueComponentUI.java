package org.cocktail.fwkcktlsangriaguiajax.serveur.components.projetscientifique;

import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;

public class ProjetScientifiqueComponentUI extends AFwkCktlSangriaComponent {
	
	private final static String BINDING_consultation = "consultation";
	private final static String BINDING_modification = "modification";
	public final static String BINDING_projetScientifique = "projetScientifique";
	
	
	public ProjetScientifiqueComponentUI(WOContext context) {
		super(context);
	}

	public EOProjetScientifique projetScientifique() {
		return (EOProjetScientifique) valueForBinding(BINDING_projetScientifique);
	}

    
    public Boolean consultation() {
    	if(hasBinding(BINDING_consultation)) {
    		return (Boolean) valueForBinding(BINDING_consultation);
    	}
    	else {
    		return false;
    	}
    }
    
    public Boolean modification() {
    	if(consultation()) {
    		return false;
    	}
    	else if(hasBinding(BINDING_modification)) {
    		return (Boolean) valueForBinding(BINDING_modification);
    	}
    	else {
    		return false;
    	}
    	
    }
    
}
