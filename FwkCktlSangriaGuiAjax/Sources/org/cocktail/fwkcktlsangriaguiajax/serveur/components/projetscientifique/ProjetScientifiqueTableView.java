package org.cocktail.fwkcktlsangriaguiajax.serveur.components.projetscientifique;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class ProjetScientifiqueTableView extends AFwkCktlSangriaComponent {
    
    
    private final String PROJET_COURANT_KEY = "projetScientifique";
    
    private final String NUMERO_KEY = "NUMERO";
    private final String INTITULE_KEY = "INTITULE";
    
    private EOProjetScientifique _projetScientifique;
    
    private NSMutableArray<String> _colonnesKeys = null; 
    private NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = null;

    
    public ProjetScientifiqueTableView(WOContext context) {
        super(context);
    }
    
    public EOProjetScientifique projetScientifique() {
        return _projetScientifique;
    }

    public void setProjetScientifique(EOProjetScientifique projetScientifique) {
        _projetScientifique = projetScientifique;
    }
    
    public String contratTableViewId() {
        return getComponentId() + "_contratTableView";
    }
    




    private NSMutableArray<String> _colonnesKeys() {
        if(_colonnesKeys == null) {
            _colonnesKeys = new NSMutableArray<String>();
            _colonnesKeys().add(NUMERO_KEY);
            _colonnesKeys().add(INTITULE_KEY);
        }
        return _colonnesKeys;
    }
    
    private NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap() {
        if(_colonnesMap == null) {
            
            _colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();
            
            CktlAjaxTableViewColumn numero_colonne = new CktlAjaxTableViewColumn();
            numero_colonne.setLibelle("Numéro");
            numero_colonne.setRowCssClass("alignToCenter useMinWidth nowrap");
            CktlAjaxTableViewColumnAssociation numero_colonne_ass = new CktlAjaxTableViewColumnAssociation(PROJET_COURANT_KEY + "." + EOProjetScientifique.INDEX_KEY, "");
            numero_colonne.setAssociations(numero_colonne_ass);
            _colonnesMap.takeValueForKey(numero_colonne, NUMERO_KEY);
    
            CktlAjaxTableViewColumn intitule_colonne = new CktlAjaxTableViewColumn();
            intitule_colonne.setLibelle("Intitulé");
            intitule_colonne.setRowCssClass("alignToLeft useMaxWidth");
            CktlAjaxTableViewColumnAssociation intitule_colonne_ass = new CktlAjaxTableViewColumnAssociation(PROJET_COURANT_KEY + "." + EOProjetScientifique.INTITULE_KEY, "");
            intitule_colonne.setAssociations(intitule_colonne_ass);
            _colonnesMap.takeValueForKey(intitule_colonne, INTITULE_KEY);
            
        }
        return _colonnesMap;
    
    }
    
    
    public NSArray<CktlAjaxTableViewColumn> colonnes() {
        NSMutableArray<CktlAjaxTableViewColumn> colonnes = new NSMutableArray<CktlAjaxTableViewColumn>();
        for(String key : _colonnesKeys()) {
            colonnes.add((CktlAjaxTableViewColumn) _colonnesMap().valueForKey(key));
        }
        return colonnes;
    }





}