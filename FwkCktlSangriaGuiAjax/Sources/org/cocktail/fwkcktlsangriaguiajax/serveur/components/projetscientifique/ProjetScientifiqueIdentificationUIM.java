package org.cocktail.fwkcktlsangriaguiajax.serveur.components.projetscientifique;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;

public class ProjetScientifiqueIdentificationUIM extends ProjetScientifiqueComponentUI {

    public EOStructure laboratoireAAjouter;
    public EOIndividu responsableScientifiqueAAjouter;
    private EOStructure currentLaboratoirePorteur;
    private EOStructure currentLaboratoire;

    private EOIndividu currentResponsableScientifique;
    private EOIndividu coordinateurProjet;
//    private EOEditingContext personnesEditingContext;

    private EOStructure selectedLaboratoire;

    public ProjetScientifiqueIdentificationUIM(WOContext context) {
        super(context);
    }
//
//    public EOEditingContext personnesEditingContext() {
//        if (personnesEditingContext == null) {
//            personnesEditingContext = ERXEC.newEditingContext(edc());
//        }
//        return personnesEditingContext;
//    }

    public EOQualifier qualifierForGroupes() {
        return ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_LA);
    }

    /*******************************************/
    /*** Gestion des laboratoires porteurs ***/

    public NSArray<EOStructure> listeDesLaboratoires() {
        EOQualifier qualifierForLabos = ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_LA);
        EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
        return EOStructure.fetchAll(edc(), qualifierForLabos, new NSArray<EOSortOrdering>(order));
    }

    public void setCurrentLaboratoirePorteur(EOStructure currentLaboratoirePorteur) {
        this.currentLaboratoirePorteur = currentLaboratoirePorteur;
    }

    public EOStructure currentLaboratoirePorteur() {
        return this.currentLaboratoirePorteur;
    }

    public String laboratoiresPorteursContainerId() {
        return getComponentId() + "_laboratoiresPorteursContainer";
    }

    public String laboratoirePorteurSearchWindowId() {
        return getComponentId() + "_laboratoirePorteurSearchWindow";
    }

    public String jsOpenLaboratoirePorteurSearch() {
        return "openCAW_" + laboratoirePorteurSearchWindowId() + "();return false;";
    }

    public WOActionResults onSuppressionLaboratoirePorteur() {
        try {
			projetScientifique().supprimerStructureRechercheConcernee(currentLaboratoirePorteur(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleLocalizedErrorMessage("attention", "erreur.suppression.structurePorteuse");		
		}
        return doNothing();
    }

    public String jsOnSelect() {
        return "function() {" + laboratoiresPorteursContainerId() + "Update(); Windows.close('" + laboratoirePorteurSearchWindowId() + "_win', event);}";
    }

    public void setCurrentLaboratoire(EOStructure currentLaboratoire) {
        this.currentLaboratoire = currentLaboratoire;
    }

    public EOStructure getCurrentLaboratoire() {
        return currentLaboratoire;
    }

    public WOActionResults selectionnerLaboratoire() {
        try {
			projetScientifique().ajouterStructureRechercheConcernee(getSelectedLaboratoire());
		} catch (Exception e) {
			session().addSimpleLocalizedErrorMessage("attention", "erreur.ajout.structurePorteuse");		
		}
        CktlAjaxWindow.close(context());
        return doNothing();
    }

    /*******************************************/
    /*** Gestion des responsables scientifiques ***/

    public void setCurrentResponsableScientifique(EOIndividu currentResponsableScientifique) {
        this.currentResponsableScientifique = currentResponsableScientifique;
    }

    public EOIndividu currentResponsableScientifique() {
        return currentResponsableScientifique;
    }

    public String responsablesScientifiquesContainerId() {
        return getComponentId() + "_responsablesScientifiquesContainer_" + currentLaboratoirePorteur().persId();
    }

    public String responsableScientifiqueSearchWindowId() {
        return getComponentId() + "_responsableScientifiqueSearchWindow_" + currentLaboratoirePorteur().persId();
    }

    public WOActionResults onAjoutResponsableScientifique() {
        try {
			projetScientifique().ajouterResponsableScientifiquePourStructureRecherche(responsableScientifiqueAAjouter(), currentLaboratoirePorteur());
		} catch (Exception e) {
			session().addSimpleLocalizedErrorMessage("attention", "erreur.ajout.responsableScientifique");		
		}
        AjaxUpdateContainer.updateContainerWithID(laboratoiresPorteursContainerId(), context());
        CktlAjaxWindow.close(context(), responsableScientifiqueSearchWindowId());
        return doNothing();
    }

    public WOActionResults annulerAjoutResponsableScientifique() {
        CktlAjaxWindow.close(context(), responsableScientifiqueSearchWindowId());
        return doNothing();
    }

    public WOActionResults onSuppressionResponsableScientifique() {
        try {
			projetScientifique().supprimerResponsableScientifiquePourStructureRecherche(currentResponsableScientifique(), currentLaboratoirePorteur(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleLocalizedErrorMessage("attention", "erreur.suppression.responsableScientifique");		
		}
        return doNothing();
    }

    public EOIndividu responsableScientifiqueAAjouter() {
        return responsableScientifiqueAAjouter;
    }

    public void setResponsableScientifiqueAAjouter(EOIndividu responsableScientifiqueAAjouter) {
        this.responsableScientifiqueAAjouter = responsableScientifiqueAAjouter;
    }

    @SuppressWarnings("unchecked")
    public NSArray<EOIndividu> getResponsablesScientifiques() {
        return projetScientifique().getResponsablesScientifiquesForStructureRecherche(currentLaboratoirePorteur());
    }

    public EOQualifier qualifierForResponsables() {
    	if(currentLaboratoirePorteur() == null) {
    		return null;
    	}
    	NSArray<EOStructure> filles = currentLaboratoirePorteur().getStructuresFilles();
    	return EOIndividu.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).eq(currentLaboratoirePorteur())
    		.or(EOIndividu.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).in(filles));
    }

    public String jsOnSuccessAjoutResponsableScientifiquOk() {
        return "function() {" + laboratoiresPorteursContainerId() + "Update(); Windows.close('" + responsableScientifiqueSearchWindowId() + "_win', event);}";
    }

    public String jsOnSuccessAjoutResponsableScientifiquCancel() {
        return "function() {Windows.close('" + responsableScientifiqueSearchWindowId() + "_win', event);}";
    }

    public void setCurrentExercice(Integer currentExercice) {
        EOExerciceCocktail exercice = (EOExerciceCocktail) EOUtilities.objectMatchingKeyAndValue(edc(), EOExerciceCocktail.ENTITY_NAME, EOExerciceCocktail.EXE_EXERCICE_KEY, currentExercice);
        projetScientifique().contratProjet().setExerciceCocktailRelationship(exercice);
        projetScientifique().projet().setExerciceCocktailRelationship(exercice);
    }

    public Integer currentExercice() {
        return projetScientifique().contratProjet().exerciceCocktail().exeExercice();
    }

    public String coordinateurProjetUpdateContainerId() {
        return getComponentId() + "_coordinateurProjetUpdateContainer";
    }

    public WOActionResults onSelectCoordinateurProjet() {
    	projetScientifique().setCoordinateurProjet(coordinateurProjet(), getUtilisateurPersId());
        return doNothing();
    }

    public void setCoordinateurProjet(EOIndividu coordinateurProjet) {
        this.coordinateurProjet = coordinateurProjet;
    }

    public EOIndividu coordinateurProjet() {
        if (coordinateurProjet == null) {
            coordinateurProjet = projetScientifique().getCoordinateurProjet();
        }
        return coordinateurProjet;
    }

    public EOStructure getSelectedLaboratoire() {
        return selectedLaboratoire;
    }

    public void setSelectedLaboratoire(EOStructure selectedLaboratoire) {
        this.selectedLaboratoire = selectedLaboratoire;
    }

    public WOActionResults annulerSelectionnerLaboratoire() {
        CktlAjaxWindow.close(context(), laboratoirePorteurSearchWindowId());
        return doNothing();
    }
    
    
    public EOAssociation coordinateurDuProjetRole() {
        return FactoryAssociation.shared().coordinateurProjetAssociation(edc());
    }

}