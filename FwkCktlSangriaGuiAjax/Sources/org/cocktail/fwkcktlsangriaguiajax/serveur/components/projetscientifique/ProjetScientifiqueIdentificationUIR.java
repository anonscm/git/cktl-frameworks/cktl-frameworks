package org.cocktail.fwkcktlsangriaguiajax.serveur.components.projetscientifique;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class ProjetScientifiqueIdentificationUIR extends ProjetScientifiqueComponentUI {
	private static final long serialVersionUID = 1253855514598181679L;
	
	private EOStructure currentLaboratoirePorteur;
	private String currentResponsableScientifiqueAvecLabo;
	private EORepartAssociation currentCoordinateur;
	private int index;
    
	
    public ProjetScientifiqueIdentificationUIR(WOContext context) {
        super(context);
    }

	public void setCurrentLaboratoirePorteur(EOStructure currentLaboratoirePorteur) {
		this.currentLaboratoirePorteur = currentLaboratoirePorteur;
	}

	public EOStructure getCurrentLaboratoirePorteur() {
		return currentLaboratoirePorteur;
	}
	/*
	 * Création d'un liste contenant les responsables scientifiques avec leur labos
	 */
	public NSArray<String> responsablesScientifiquesAvecLabos() {
		NSArray<String> resultats = new NSMutableArray<String>();
		
		NSArray<EOStructure> labos = projetScientifique().getStructuresRechercheConcernees();
		for(EOStructure labo : labos) {
			NSArray<EOIndividu> responsablesScientifiques = projetScientifique().getResponsablesScientifiquesForStructureRecherche(labo);
			for(EOIndividu responsable : responsablesScientifiques) {
				resultats.add(responsable.getNomAndPrenom() + " pour " + labo.lcStructure());
			}
		} 
		
		return resultats;
	}

	public void setCurrentResponsableScientifiqueAvecLabo(
			String currentResponsableScientifiqueAvecLabo) {
		this.currentResponsableScientifiqueAvecLabo = currentResponsableScientifiqueAvecLabo;
	}

	public String getCurrentResponsableScientifiqueAvecLabo() {
		return currentResponsableScientifiqueAvecLabo;
	}
	
	public NSArray<EORepartAssociation> getCoordinateurs() {
		EOQualifier qualifier = EORepartAssociation.TO_ASSOCIATION.eq(FactoryAssociation.shared().coordinateurProjetAssociation(edc()));

		EOStructure localInstance = (EOStructure) EOUtilities.localInstanceOfObject(edc(), projetScientifique().contratProjet().groupePartenaire());
	    if (localInstance == null) {
	      throw new IllegalStateException("You attempted to localInstance " + projetScientifique().contratProjet().groupePartenaire() + ", which has not yet committed.");
	    }
	    
	    return localInstance.toRepartAssociationsElts(qualifier);
	}

	public EORepartAssociation getCurrentCoordinateur() {
		return currentCoordinateur;
	}

	public void setCurrentCoordinateur(EORepartAssociation currentCoordinateur) {
		this.currentCoordinateur = currentCoordinateur;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
	public Boolean isFirst() {
		if(index == 0) {
			return true;
		}
		return false;
	}

}