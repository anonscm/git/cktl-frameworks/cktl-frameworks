package org.cocktail.fwkcktlsangriaguiajax.serveur.components.ecoledoctorale;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;

public class EcoleDoctoraleIdentification extends AFwkCktlSangriaComponent {

	public static final String BINDING_ecoleDoctorale = "ecoleDoctorale";

	private Boolean isEditing;

	private EOStructure currentTutelle;

	private EODomaineScientifique currentDomaineScientifique;
	private EODomaineScientifique selectedDomaineScientifique;

	private EOStructure currentEtablissementRattachementSecondaire;
	private EOStructure tutelleAAjouter;
	private EOStructure ERSAAjouter;

	public final String tutellesContainerId = getComponentId() + "_tutellesContainer";
	public final String winTutelleSelectionID = getComponentId() + "winTutelleSelection";
	public final String winTutelleSelectionContainerOnCloseID = winTutelleSelectionID + "_containerOnClose";
	public final String ersContainerId = getComponentId() + "_ersContainer";
	public final String winERSSelectionID = getComponentId() + "_winERSSelection";
	public final String winERSSelectionContainerOnCloseID = winERSSelectionID + "_containerOnClose";
	public final String ersSearchWindowId = getComponentId() + "_ERSSearchWindow";

	public EcoleDoctoraleIdentification(WOContext context) {
		super(context);
	}

	public EOStructure ecoleDoctorale() {
		return (EOStructure) valueForBinding(BINDING_ecoleDoctorale);
	}

	public void setIsEditing(Boolean isEditing) {
		this.isEditing = isEditing;
	}

	public Boolean isEditing() {
		return isEditing;
	}

	public void setCurrentTutelle(EOStructure currentTutelle) {
		this.currentTutelle = currentTutelle;
	}

	public EOStructure currentTutelle() {
		return currentTutelle;
	}

	public void setCurrentERS(EOStructure currentEtablissementRattachementSecondaire) {
		this.currentEtablissementRattachementSecondaire = currentEtablissementRattachementSecondaire;
	}

	public EOStructure currentERS() {
		return currentEtablissementRattachementSecondaire;
	}

	public NSArray<EODomaineScientifique> domainesScientifiques() {
		return EODomaineScientifique.fetchAll(edc());
	}

	public void setSelectedDomaineScientifique(EODomaineScientifique selectedDomaineScientifique) {
		this.selectedDomaineScientifique = selectedDomaineScientifique;
	}

	public EODomaineScientifique selectedDomaineScientifique() {
		return selectedDomaineScientifique;
	}

	public void setCurrentDomaineScientifique(EODomaineScientifique currentDomaineScientifique) {
		this.currentDomaineScientifique = currentDomaineScientifique;
	}

	public EODomaineScientifique currentDomaineScientifique() {
		return currentDomaineScientifique;
	}

	public EODomaineScientifique domaineScientifiquePrincpalPourUniteRecherche() {
		return StructuresRechercheHelper.domaineScientifiquePrincipalUniteRecherche(edc(), ecoleDoctorale(), false);
	}

	public void setDomaineScientifiquePrincpalPourUniteRecherche(EODomaineScientifique domaineScientifique) {
		StructuresRechercheHelper.setDomaineScientifiquePrincipalUniteRecherche(edc(), domaineScientifique, ecoleDoctorale(), false);
	}

	/*********************************************************/
	/*** Gestion des tutelles ***/

	public NSArray<EOStructure> tutellesPourUniteRecherche() {
		return StructuresRechercheHelper.tutellesStructureRecherche(edc(), ecoleDoctorale());
		// return StructureRechercheUtilities.tutellesUniteRecherche(edc(),
		// ecoleDoctorale());
	}

	public EOStructure tutelleAAjouter() {
		return tutelleAAjouter;
	}

	public void setTutelleAAjouter(EOStructure tutelleAAjouter) {
		this.tutelleAAjouter = tutelleAAjouter;
	}

	public String tutelleSearchWindowId() {
		return getComponentId() + "_tutelleSearchWindow";
	}

	public String jsOpenTutelleSearch() {
		return "openCAW_" + tutelleSearchWindowId() + "();return false;";
	}

	public WOActionResults onAjoutTutelle() {
		StructuresRechercheHelper.ajouterTutellePourStructureRecherche(edc(), tutelleAAjouter(), ecoleDoctorale(), getUtilisateurPersId());
		if (StructuresRechercheHelper.tutellesStructureRecherche(edc(), ecoleDoctorale()).count() == 1) {
			StructuresRechercheHelper.definirTutellePrincipalePourStructureRecherche(edc(), tutelleAAjouter(), ecoleDoctorale(), getUtilisateurPersId());
		}
		AjaxUpdateContainer.updateContainerWithID(tutellesContainerId, context());
		CktlAjaxWindow.close(context(), tutelleSearchWindowId());

		return doNothing();

	}

	public WOActionResults annulerAjoutTutelle() {
		CktlAjaxWindow.close(context(), tutelleSearchWindowId());
		return doNothing();
	}

	public WOActionResults onSuppressionTutelle() {
		try {
			StructuresRechercheHelper.supprimerTutellePourStructureRecherche(edc(), currentTutelle(), ecoleDoctorale(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}

	/*********************************************************/
	/*** Gestion des ERS ***/

	public NSArray<EOStructure> etablissementsDeRattachementPourUniteRecherche() {
		return StructuresRechercheHelper.etablissementsRattachementPourStructureRecherche(edc(), ecoleDoctorale());
	}

	public EOStructure etablissementRattachementPrincipalPourUniteRecherche() {
		return StructuresRechercheHelper.etablissementRattachementPrincipalPourStructureRecherche(edc(), ecoleDoctorale());
	}

	public EOStructure ERSAAjouter() {
		return ERSAAjouter;
	}

	public void setERSAAjouter(EOStructure ERSAAjouter) {
		this.ERSAAjouter = ERSAAjouter;
	}

	public WOActionResults onAjoutERS() {

		StructuresRechercheHelper.ajouterEtablissementRattachementPourUniteRecherche(edc(), ERSAAjouter(), ecoleDoctorale(), getUtilisateurPersId());
		AjaxUpdateContainer.updateContainerWithID(ersContainerId, context());
		CktlAjaxWindow.close(context(), ersSearchWindowId);
		return doNothing();

	}

	public WOActionResults annulerAjoutErs() {
		CktlAjaxWindow.close(context(), ersSearchWindowId);
		return doNothing();
	}

	public WOActionResults onSuppressionERS() {
		try {
			StructuresRechercheHelper.supprimerEtablissementRattachementPourStructureRecherche(edc(), currentERS(), ecoleDoctorale(), getUtilisateurPersId());
			AjaxUpdateContainer.updateContainerWithID(ersContainerId, context());
			CktlAjaxWindow.close(context(), ersSearchWindowId);
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}

	public Boolean isCurrentEtablissementRattachementPrincipal() {
		if (StructuresRechercheHelper.etablissementRattachementPrincipalPourStructureRecherche(edc(), ecoleDoctorale()) != null) {
			return StructuresRechercheHelper.etablissementRattachementPrincipalPourStructureRecherche(edc(), ecoleDoctorale()).equals(currentERS());
		}
		return false;
	}

	public WOActionResults setIsCurrentEtablissementRattachementPrincipal(Boolean isCurrentEtablissementRattachementPrincipal) {
		StructuresRechercheHelper.definirEtablissementRattachementPrincipalPourUniteRecherche(edc(), currentERS(), ecoleDoctorale(), getUtilisateurPersId());
		return doNothing();
	}

	public Boolean isCurrentTutellePrincipale() {
		if (StructuresRechercheHelper.tutellePrincipalePourStructureRecherche(edc(), ecoleDoctorale()) != null) {
			return StructuresRechercheHelper.tutellePrincipalePourStructureRecherche(edc(), ecoleDoctorale()).equals(currentTutelle());
		}
		return false;
	}

	public WOActionResults setIsCurrentTutellePrincipale(Boolean isCurrentTutellePrincipale) {
		StructuresRechercheHelper.definirTutellePrincipalePourStructureRecherche(edc(), currentTutelle(), ecoleDoctorale(), getUtilisateurPersId());
		return doNothing();
	}

	public EOAssociation responsableAdministratifAssociation() {
		return getFactoryAssociation().secretraireAdministrativeAssociation(edc());
	}

	public EOAssociation directionAssociation() {
		return getFactoryAssociation().directionAssociation(edc());
	}

	public EOAssociation directionAdjointeAssociation() {
		return getFactoryAssociation().directeurAdjointAssociation(edc());
	}

	public EOQualifier qualifierForGroupeTutelles() {
		String codeStructureGroupeTutelles = EOGrhumParametres.parametrePourCle(edc(), "org.cocktail.sangria.groupes.tutelles");
		if (codeStructureGroupeTutelles != null) {
			return EOStructure.C_STRUCTURE.eq(codeStructureGroupeTutelles);
		} else {
			return null;
		}


	}
	
	 public NSArray<EOStructure> listeDesTutelles() {
		 return StructuresRechercheHelper.listeDesTutelles(edc());
	 }

}