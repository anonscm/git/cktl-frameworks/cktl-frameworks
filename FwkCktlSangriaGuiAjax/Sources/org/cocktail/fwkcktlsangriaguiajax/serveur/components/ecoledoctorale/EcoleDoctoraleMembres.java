package org.cocktail.fwkcktlsangriaguiajax.serveur.components.ecoledoctorale;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;

public class EcoleDoctoraleMembres extends AFwkCktlSangriaComponent {
	
	public static final String BINDING_ecoleDoctorale = "ecoleDoctorale";
	private EOAssociation membreAssociation;


	
    public EcoleDoctoraleMembres(WOContext context) {
        super(context);
    }
    
	public EOStructure ecoleDoctorale() {
		return (EOStructure) valueForBinding(BINDING_ecoleDoctorale);
	}
	
	public EOAssociation membreAssociation() {
		return getFactoryAssociation().membreAssociation(edc());
	}
}