package org.cocktail.fwkcktlsangriaguiajax.serveur.components.ecoledoctorale;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

import com.webobjects.appserver.WOContext;

public class EcoleDoctoraleGestionFinanciere extends AFwkCktlSangriaComponent {

	private static final String BINDING_ecoleDoctorale = "ecoleDoctorale";
	
	
    public EcoleDoctoraleGestionFinanciere(WOContext context) {
        super(context);
    }
 
    
    public EOStructure ecoleDoctorale() {
    	return (EOStructure) valueForBinding(BINDING_ecoleDoctorale);
    }

}