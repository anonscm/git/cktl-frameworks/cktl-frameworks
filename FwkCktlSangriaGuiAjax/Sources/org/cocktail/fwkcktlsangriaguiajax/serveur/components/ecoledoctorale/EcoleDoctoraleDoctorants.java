package org.cocktail.fwkcktlsangriaguiajax.serveur.components.ecoledoctorale;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;

import er.extensions.appserver.ERXDisplayGroup;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;

public class EcoleDoctoraleDoctorants extends AFwkCktlSangriaComponent {
	
	private final static String BINDING_ecoleDoctorale = "ecoleDoctorale";
	
	private final static String KEY_NOINDIVIDU = "NOINDIVIDU";
	private final static String KEY_NOM = "NOM";
	private final static String KEY_PRENOM = "PRENOM";
	private final static String KEY_LABO = "LABO";
	
	private NSDictionary<String, Object> currentDoctorantRow = null;
	private NSArray<NSDictionary<String, Object>> doctorantsPourEcoleRows = null;
	private ERXDisplayGroup<NSDictionary<String, Object>> doctorantsEcoleRowsDisplayGroup = null;
	private EOArrayDataSource doctorantsEcoleRowsDataSource = null;

	
    public EcoleDoctoraleDoctorants(WOContext context) {
        super(context);
    }
    
    public String doctorantsTableViewId() {
    	return getComponentId() + "_doctorantsTableView";
    }
    public EOStructure ecoleDoctorale() {
    	return (EOStructure) valueForBinding(BINDING_ecoleDoctorale);
    }

	private String requeteDoctorantsPourEcole() {
		
		String assCodeEcole = getFactoryAssociation().membreAssociation(edc()).assCode();
		String assCodeDoctorant = getFactoryAssociation().doctorantAssociation(edc()).assCode();
		String requete = 
				"select distinct i.no_individu as " + KEY_NOINDIVIDU + ", i.nom_affichage as " + KEY_NOM + ", i.prenom_affichage as " + KEY_PRENOM + ", s.ll_structure as " + KEY_LABO + " from repart_association ra" +
				  " inner join structure_ulr s on ra.pers_id = s.pers_id and ra.ass_id = (select ass_id from association where ass_code = '"+ assCodeEcole + "') and ra.c_structure = '" + ecoleDoctorale().cStructure() + "'" +
				    " inner join repart_association ra_doctorant on ra_doctorant.c_structure = s.c_structure and ra_doctorant.ass_id = (select ass_id from association where ass_code = '" + assCodeDoctorant + "')" +
				      " inner join individu_ulr i on i.pers_id = ra_doctorant.pers_id" +
				        " order by i.nom_affichage";
		
		return requete;
		
	}
	
	public NSArray<NSDictionary<String, Object>> doctorantsPourEcoleRows() { 
		if(doctorantsPourEcoleRows == null) {
			doctorantsPourEcoleRows = EOUtilities.rawRowsForSQL(edc(), "FwkCktlPersonne", requeteDoctorantsPourEcole(), null);
		}
		return doctorantsPourEcoleRows;
	}

	public EOArrayDataSource doctorantsEcoleRowsDataSource() {
		if(doctorantsEcoleRowsDataSource == null) {
			doctorantsEcoleRowsDataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(NSDictionary.class), null);
			doctorantsEcoleRowsDataSource.setArray(doctorantsPourEcoleRows());
		}
		return doctorantsEcoleRowsDataSource;
	}
	
	public ERXDisplayGroup<NSDictionary<String, Object>> doctorantsEcoleRowsDisplayGroup() {
		if(doctorantsEcoleRowsDisplayGroup == null) {
			doctorantsEcoleRowsDisplayGroup = new ERXDisplayGroup<NSDictionary<String,Object>>();
			doctorantsEcoleRowsDisplayGroup.setDataSource(doctorantsEcoleRowsDataSource());
			doctorantsEcoleRowsDisplayGroup.fetch();
		}
		return doctorantsEcoleRowsDisplayGroup;
	}
	
	public void resetDoctorantsRowsData() {
		doctorantsPourEcoleRows = null;
		doctorantsEcoleRowsDataSource().setArray(doctorantsPourEcoleRows());
		doctorantsEcoleRowsDisplayGroup().fetch();
	}
	
	
	public NSDictionary<String, Object> currentDoctorantRow() {
		return currentDoctorantRow;
	}

	public void setCurrentDoctorantRow(NSDictionary<String, Object> currentDoctorantRow) {
		this.currentDoctorantRow = currentDoctorantRow;
	}
	
	public String currentDoctorantLibelle() {
		String nom = (String) currentDoctorantRow().valueForKey(KEY_NOM);
		String prenom = (String) currentDoctorantRow().valueForKey(KEY_PRENOM);
		return  nom + " " + prenom;
	}
	
	public String currentDoctorantLabo() {
		return (String) currentDoctorantRow().valueForKey(KEY_LABO);
	}
	
	public WOActionResults refreshTableView() {
		resetDoctorantsRowsData();
		return doNothing();
	}

}