Template WOLIps pour le développement des applications Cocktail 
-----------------------------------------------------------------------
version 0.9.0.0

rodolphe.prin@univ-lr.fr


Installation
********************
- Dossier "CktlWoTemplate" a stocker dans un des dossiers suivants :
# /Library/Application Support/WOLips/Project Templates
# YourHomeDir\Documents and Settings\Application Data\WOLips\Project Templates
# YourHomeDir\Documents and Settings\AppData\Local\WOLips\Project Templates
# ~/Library/Application Support/WOLips/Project Templates


Utilisation
********************
Dans Eclipse 3.3.1(+) / WOLips 3.3.x(+) :
- File / New project  / WOLips / WOProject from template
- Selectionner CktlWoTemplate
- Choisissez le type de projet (Framework ou Application)
- Choisissez le framework de depart
- Une fois le projet créé, executez la tâche ant "run_me" définie dans 
le fichier "ant_run_me_1st.xml" a la racine de votre projet (dans la vue Ant, ajouter le build file ant_run_me_1st.xml etr doublecliquez sur la tâche).
- Faites un refresh du projet (permet au projet d'etre construit, logiquement les erreurs disparaissent).
- Modifiez le fichier de config (nom_application.conf) situé a la racine de votre projet en fonction de vos parametres
(notamment l'url du serveur saut si vous en utilisez un)
-  run / open run dialog -> lancez l'application avec le lanceur correspondant au nom de l'application
- Si votre application est une application javaclient, un "external Tools" est fourni pour lancer la partie client "<nom application>Client". 
Vous devez cependant le modifier l'argument -classpath pour récupérer la bibliothèque ClientStd.jar (ou bien les jars WebObjects Client).

Modeles
********************
Stockez vos modeles dans le dossier Resources (si vous ne nommez pas votre modele principal du nom de votre projet, modifiez la methode mainModelName de la classe application. 


Generation des entites du modele
********************
Le dossier _EOGenTemplates contient des templates pour l'outil EOGenerator. 
Cet outil est disponible sur http://www.rubicode.com/Software/EOGenerator/ mais a priori il va etre abandonné par WOLips pour un outil basé sur Velocity (qui n'est pas stabilisé aujourd'hui).
Installez-le par exemple dans /Developper/EOGenerator.
Des externals tools sont fournis (a adapter) pour generer les classes metiers. 
  

Deploiement
********************
Une tache ant pour simplifier le deploiement est fournie dans le fichier build.xml :
Executez la tache Cktl_Package_me pour créer automatiquement une archive du .woa avec le n° de version inclus dans le nom. Le projet compilé est récupéré après construction par le moteur WOlips a partir du dossier "dest.dir" (par défaut celui du projet).
Ce n° de version est recupere a partir de la classe VersionMe. L'archive est stockee dans le dossier specifie dans le fichier 
build.properties (par defaut le dossier cktl_deploy dans votre workspace Eclipse).
Cette tache permet également de nettoyer le(s) modèle(s) de votre application (suppression des lignes URL, username et password).


Classpath
********************
Le classpath est généré automatiquement en tenant compte des options choisies.
Chaque entrée du classpath est construite à partir des variables eclipse NEXT_ROOT et NEXT_LOCAL_ROOT définies par WOLips.

NB : le classpath généré considère que les framework Cocktail sont installés dans 
NEXT_LOCAL_ROOT/Library/WebObjects/Applications/Frameworks/ 
et que les jars tiers (pilote jdbc, ...) sont stockes dans 
NEXT_LOCAL_ROOT/Library/WebObjects/Extensions/


Arborescence
********************
L'arborescence du projet doit être la suivante:
src : (sources)
Components : composants Html
Client : arborescence du client
Client/src : sources de la partie JavaClient
Common/src : sources qu seront compilées a la fois pour le serveur et le client
Libraries : Jars tiers utilisés par votre application (autres que ceux des frameworks communs aux developpements Cocktail)
Resources : resources propres à votre application (seront inclus dans xx.woa/Contents/Resources)
WebServerResources : resources web propres à votre application (seront inclus dans xx.woa/Contents/WebServerResources) 
_sql_dist : dossier dans lequel vous pouvez stocker les différents scripts de mises à jour sql pour chaque version de votre application (un sous-dossier par version).
javaclientbuild.xml : tache ant utilisée pour construire la partie client des projet javaclient. Cette tache est utilisée par un "External Builder" défini dans les propriétés du projet. 

Nommage des packages Java
********************
Package de base :
org.cocktail.nom_application

Serveurs :
org.cocktail.nom_application.serveur
org.cocktail.nom_application.serveur.metier.eos


Composants html :
org.cocktail.nom_application.serveur.components

Client :
org.cocktail.nom_application.client
org.cocktail.nom_application.client.metier.eos

Interface client :
org.cocktail.nom_application.client.gui



Explications sur le fonctionnement du template
********************
Le fichier template.xml est utilisé par le moteur de "Custom templates" de WOlips pour proposer les différentes options de création.
Les options récupérées dans template.xml le sont en tant que variables Velocity (http://velocity.apache.org/engine/devel/vtl-reference-guide.html) et exploitées par le moteur de WOlips. 
Ces variables sont ensuite utilisées dans les différents fichiers qui composent le template.
Lorsque le projet est créé à partir du template, toute l'arborescence du template est recopiée. 
Les limitations du moteur de template de WOlips font qu'on a besoin d'une tache ant pour terminer l'initialisation (l'arborescence n'est pas exactement la même s'il s'agit ou non d'un projet JavaClient, les builders changent s'il s'agit d'un framework, etc.).
Donc l'initialisation finale est réalisée à partir de cette tache ant et consiste essentiellement à intégré les bons fichiers au bon endroit en fonction du type de projet choisi. Ces fichiers sont créés à partir de ceux contenus dans le dossier _templates  

Le fonctionnement de ce template est très dépendant de l'architecture des projets Eclipse/WOlips. Si l'architecture de l'un ou de l'autre change, le template peut ne plus fonctionner tel quel, il faudra l'adapter.       


Problemes connues
********************
- la compilation automatique de la partie javaclient ne se declenche pas toujours. Je ne sais pas d'ou ca vient... 
Dans ce cas faites un Project/Build All.

test




