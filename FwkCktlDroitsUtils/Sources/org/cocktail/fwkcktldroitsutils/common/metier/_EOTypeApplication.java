/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOTypeApplication.java instead.
package org.cocktail.fwkcktldroitsutils.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOTypeApplication extends  AfwkDroitUtilsRecord {
	public static final String ENTITY_NAME = "FwkDroitsUtils_TypeApplication";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.TYPE_application";


//Attribute Keys
	public static final ERXKey<String> TYAP_LIBELLE = new ERXKey<String>("tyapLibelle");
	public static final ERXKey<String> TYAP_STRID = new ERXKey<String>("tyapStrid");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EODomaine> TO_DOMAINE = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EODomaine>("toDomaine");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction> TO_FONCTIONS = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction>("toFonctions");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tyapId";

	public static final String TYAP_LIBELLE_KEY = "tyapLibelle";
	public static final String TYAP_STRID_KEY = "tyapStrid";

//Attributs non visibles
	public static final String DOM_ID_KEY = "domId";
	public static final String TYAP_ID_KEY = "tyapId";

//Colonnes dans la base de donnees
	public static final String TYAP_LIBELLE_COLKEY = "tyap_libelle";
	public static final String TYAP_STRID_COLKEY = "tyap_strid";

	public static final String DOM_ID_COLKEY = "DOM_ID";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";


	// Relationships
	public static final String TO_DOMAINE_KEY = "toDomaine";
	public static final String TO_FONCTIONS_KEY = "toFonctions";



	// Accessors methods
	public String tyapLibelle() {
	 return (String) storedValueForKey(TYAP_LIBELLE_KEY);
	}

	public void setTyapLibelle(String value) {
	 takeStoredValueForKey(value, TYAP_LIBELLE_KEY);
	}

	public String tyapStrid() {
	 return (String) storedValueForKey(TYAP_STRID_KEY);
	}

	public void setTyapStrid(String value) {
	 takeStoredValueForKey(value, TYAP_STRID_KEY);
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EODomaine toDomaine() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EODomaine)storedValueForKey(TO_DOMAINE_KEY);
	}

	public void setToDomaineRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EODomaine value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EODomaine oldValue = toDomaine();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DOMAINE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_DOMAINE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction> toFonctions() {
	 return (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction>)storedValueForKey(TO_FONCTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction> toFonctions(EOQualifier qualifier) {
	 return toFonctions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction> toFonctions(EOQualifier qualifier, boolean fetch) {
	 return toFonctions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction> toFonctions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldroitsutils.common.metier.EOFonction.TO_TYPE_APPLICATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldroitsutils.common.metier.EOFonction.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toFonctions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToFonctionsRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOFonction object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_FONCTIONS_KEY);
	}
	
	public void removeFromToFonctionsRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOFonction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_FONCTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldroitsutils.common.metier.EOFonction createToFonctionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldroitsutils.common.metier.EOFonction.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_FONCTIONS_KEY);
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOFonction) eo;
	}
	
	public void deleteToFonctionsRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOFonction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_FONCTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToFonctionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction> objects = toFonctions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToFonctionsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOTypeApplication avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOTypeApplication createEOTypeApplication(EOEditingContext editingContext				, String tyapLibelle
							, String tyapStrid
					, org.cocktail.fwkcktldroitsutils.common.metier.EODomaine toDomaine					) {
	 EOTypeApplication eo = (EOTypeApplication) EOUtilities.createAndInsertInstance(editingContext, _EOTypeApplication.ENTITY_NAME);	 
							eo.setTyapLibelle(tyapLibelle);
									eo.setTyapStrid(tyapStrid);
						 eo.setToDomaineRelationship(toDomaine);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeApplication creerInstance(EOEditingContext editingContext) {
		EOTypeApplication object = (EOTypeApplication)EOUtilities.createAndInsertInstance(editingContext, _EOTypeApplication.ENTITY_NAME);
  		return object;
		}

	

  public EOTypeApplication localInstanceIn(EOEditingContext editingContext) {
    EOTypeApplication localInstance = (EOTypeApplication)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeApplication> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeApplication> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeApplication> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeApplication> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeApplication> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeApplication> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeApplication> eoObjects = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeApplication>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeApplication fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeApplication fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTypeApplication> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeApplication eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeApplication)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeApplication fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeApplication fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTypeApplication> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeApplication eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeApplication)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeApplication fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeApplication eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeApplication ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeApplication fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
