/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldroitsutils.common.metier;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;


public class EOJefyAdminParametre extends _EOJefyAdminParametre {

    public EOJefyAdminParametre() {
        super();
    }

    

    
    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
        super.validateBeforeTransactionSave();
    }

    


	/**
	 * Recherche d'un parametre par son code et son exercice.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param cle
	 *        code du parametre
	 * @param exercice
	 *        exercice pour lequel on cherche ce parametre
	 * @return
	 *        un EOJefyAdminParametre
	 */
	public static final EOJefyAdminParametre getParametre(EOEditingContext ed, String cle, EOExercice exercice) {
		return getUnParametre(ed, cle, exercice);
	}

	/**
	 * Recherche la liste des parametres correspondants a un code et un exercice.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param cle
	 *        code du parametre
	 * @param exercice
	 *        exercice pour lequel on cherche ce parametre
	 * @return
	 *        un NSArray de EOJefyAdminParametre
	 */
	public static final NSArray getParametres(EOEditingContext ed, String cle, EOExercice exercice) {
		return getLesParametres(ed, cle, exercice);
	}

	/**
	 * Recherche d'un parametre par son code et son exercice.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param cle
	 *        code du parametre
	 * @param exercice
	 *        exercice pour lequel on cherche ce parametre
	 * @return
	 *        un EOJefyAdminParametre
	 */
	private static EOJefyAdminParametre getUnParametre(EOEditingContext ed, String cle, EOExercice exercice) {
		EOQualifier qual1 = new EOKeyValueQualifier(EOJefyAdminParametre.TO_EXERCICE_KEY , EOQualifier.QualifierOperatorEqual, exercice);		
		EOQualifier qual2 = new EOKeyValueQualifier(EOJefyAdminParametre.PAR_KEY_KEY , EOQualifier.QualifierOperatorEqual, cle);		
		
		NSArray res = EOJefyAdminParametre.fetchAll(ed, new EOAndQualifier(new NSArray(new Object[]{qual1, qual2})), null) ;
		return (EOJefyAdminParametre) (res.count()>0 ?res.objectAtIndex(0): null);
	}

	/**
	 * Recherche la liste des parametres correspondants a un code et un exercice.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param cle
	 *        code du parametre
	 * @param exercice
	 *        exercice pour lequel on cherche ce parametre
	 * @return
	 *        un NSArray de EOJefyAdminParametre
	 */
	private static NSArray getLesParametres(EOEditingContext ed, String cle, EOExercice exercice) {
		//NSArray parametres=null;

//		NSArray arrayParametres=fetchParametres(ed);
//
//		parametres=new NSArray((NSArray)(EOQualifier.filteredArrayWithQualifier(arrayParametres, 
//				EOQualifier.qualifierWithQualifierFormat(EOJefyAdminParametre.PAR_KEY_KEY+"=%@ and "+
//						EOJefyAdminParametre.TO_EXERCICE_KEY+"=%@", new NSArray(new Object[] { cle, exercice })))));

		NSArray parametres = fetchAll(ed, EOQualifier.qualifierWithQualifierFormat(EOJefyAdminParametre.PAR_KEY_KEY+"=%@ and "+
				EOJefyAdminParametre.TO_EXERCICE_KEY+"=%@", new NSArray(new Object[] { cle, exercice })), null);
		
		if (parametres==null || parametres.count()==0)
			return null;

		return parametres;
	}


    
}
