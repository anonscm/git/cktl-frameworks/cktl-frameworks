/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOUtilisateurInfo.java instead.
package org.cocktail.fwkcktldroitsutils.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOUtilisateurInfo extends  AfwkDroitUtilsRecord {
	public static final String ENTITY_NAME = "FwkDroitsUtils_UtilisateurInfo";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.v_Utilisateur_Info";


//Attribute Keys
	public static final ERXKey<String> CPT_LOGIN = new ERXKey<String>("cptLogin");
	public static final ERXKey<String> EMAIL = new ERXKey<String>("email");
	public static final ERXKey<Double> NO_INDIVIDU = new ERXKey<Double>("noIndividu");
	public static final ERXKey<String> NOM_PATRONYMIQUE = new ERXKey<String>("nomPatronymique");
	public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
	public static final ERXKey<String> PERS_LC = new ERXKey<String>("persLc");
	public static final ERXKey<String> PERS_LIBELLE = new ERXKey<String>("persLibelle");
	public static final ERXKey<NSTimestamp> UTL_FERMETURE = new ERXKey<NSTimestamp>("utlFermeture");
	public static final ERXKey<NSTimestamp> UTL_OUVERTURE = new ERXKey<NSTimestamp>("utlOuverture");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> TO_UTILISATEUR = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("toUtilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "utlOrdre";

	public static final String CPT_LOGIN_KEY = "cptLogin";
	public static final String EMAIL_KEY = "email";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_LC_KEY = "persLc";
	public static final String PERS_LIBELLE_KEY = "persLibelle";
	public static final String UTL_FERMETURE_KEY = "utlFermeture";
	public static final String UTL_OUVERTURE_KEY = "utlOuverture";

//Attributs non visibles
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String CPT_LOGIN_COLKEY = "cpt_login";
	public static final String EMAIL_COLKEY = "email";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_LC_COLKEY = "PERS_LC";
	public static final String PERS_LIBELLE_COLKEY = "PERS_LIBELLE";
	public static final String UTL_FERMETURE_COLKEY = "UTL_FERMETURE";
	public static final String UTL_OUVERTURE_COLKEY = "UTL_OUVERTURE";

	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";



	// Accessors methods
	public String cptLogin() {
	 return (String) storedValueForKey(CPT_LOGIN_KEY);
	}

	public void setCptLogin(String value) {
	 takeStoredValueForKey(value, CPT_LOGIN_KEY);
	}

	public String email() {
	 return (String) storedValueForKey(EMAIL_KEY);
	}

	public void setEmail(String value) {
	 takeStoredValueForKey(value, EMAIL_KEY);
	}

	public Double noIndividu() {
	 return (Double) storedValueForKey(NO_INDIVIDU_KEY);
	}

	public void setNoIndividu(Double value) {
	 takeStoredValueForKey(value, NO_INDIVIDU_KEY);
	}

	public String nomPatronymique() {
	 return (String) storedValueForKey(NOM_PATRONYMIQUE_KEY);
	}

	public void setNomPatronymique(String value) {
	 takeStoredValueForKey(value, NOM_PATRONYMIQUE_KEY);
	}

	public Integer persId() {
	 return (Integer) storedValueForKey(PERS_ID_KEY);
	}

	public void setPersId(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_KEY);
	}

	public String persLc() {
	 return (String) storedValueForKey(PERS_LC_KEY);
	}

	public void setPersLc(String value) {
	 takeStoredValueForKey(value, PERS_LC_KEY);
	}

	public String persLibelle() {
	 return (String) storedValueForKey(PERS_LIBELLE_KEY);
	}

	public void setPersLibelle(String value) {
	 takeStoredValueForKey(value, PERS_LIBELLE_KEY);
	}

	public NSTimestamp utlFermeture() {
	 return (NSTimestamp) storedValueForKey(UTL_FERMETURE_KEY);
	}

	public void setUtlFermeture(NSTimestamp value) {
	 takeStoredValueForKey(value, UTL_FERMETURE_KEY);
	}

	public NSTimestamp utlOuverture() {
	 return (NSTimestamp) storedValueForKey(UTL_OUVERTURE_KEY);
	}

	public void setUtlOuverture(NSTimestamp value) {
	 takeStoredValueForKey(value, UTL_OUVERTURE_KEY);
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateur() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
	}

	public void setToUtilisateurRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = toUtilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
	 }
	}


	/**
	* Créer une instance de EOUtilisateurInfo avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOUtilisateurInfo createEOUtilisateurInfo(EOEditingContext editingContext												, Integer persId
									, String persLibelle
									, NSTimestamp utlOuverture
										) {
	 EOUtilisateurInfo eo = (EOUtilisateurInfo) EOUtilities.createAndInsertInstance(editingContext, _EOUtilisateurInfo.ENTITY_NAME);	 
															eo.setPersId(persId);
											eo.setPersLibelle(persLibelle);
											eo.setUtlOuverture(utlOuverture);
							 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOUtilisateurInfo creerInstance(EOEditingContext editingContext) {
		EOUtilisateurInfo object = (EOUtilisateurInfo)EOUtilities.createAndInsertInstance(editingContext, _EOUtilisateurInfo.ENTITY_NAME);
  		return object;
		}

	

  public EOUtilisateurInfo localInstanceIn(EOEditingContext editingContext) {
    EOUtilisateurInfo localInstance = (EOUtilisateurInfo)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo> eoObjects = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOUtilisateurInfo fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOUtilisateurInfo fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOUtilisateurInfo> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateurInfo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateurInfo)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateurInfo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateurInfo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOUtilisateurInfo> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateurInfo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateurInfo)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOUtilisateurInfo fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateurInfo eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateurInfo ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateurInfo fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
