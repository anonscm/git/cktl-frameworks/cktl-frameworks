/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOUtilisateurFonctionExercice.java instead.
package org.cocktail.fwkcktldroitsutils.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOUtilisateurFonctionExercice extends  AfwkDroitUtilsRecord {
	public static final String ENTITY_NAME = "FwkDroitsUtils_UtilisateurFonctionExercice";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.UTILISATEUR_FONCT_EXERCICE";


//Attribute Keys
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> TO_UTILISATEUR_FONCTION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction>("toUtilisateurFonction");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ufeId";


//Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String UFE_ID_KEY = "ufeId";
	public static final String UF_ORDRE_KEY = "ufOrdre";

//Colonnes dans la base de donnees

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String UFE_ID_COLKEY = "ufe_id";
	public static final String UF_ORDRE_COLKEY = "uf_ORDRE";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_UTILISATEUR_FONCTION_KEY = "toUtilisateurFonction";



	// Accessors methods
	public org.cocktail.fwkcktldroitsutils.common.metier.EOExercice toExercice() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction toUtilisateurFonction() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction)storedValueForKey(TO_UTILISATEUR_FONCTION_KEY);
	}

	public void setToUtilisateurFonctionRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction oldValue = toUtilisateurFonction();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_FONCTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_FONCTION_KEY);
	 }
	}


	/**
	* Créer une instance de EOUtilisateurFonctionExercice avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOUtilisateurFonctionExercice createEOUtilisateurFonctionExercice(EOEditingContext editingContext		, org.cocktail.fwkcktldroitsutils.common.metier.EOExercice toExercice		, org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction toUtilisateurFonction					) {
	 EOUtilisateurFonctionExercice eo = (EOUtilisateurFonctionExercice) EOUtilities.createAndInsertInstance(editingContext, _EOUtilisateurFonctionExercice.ENTITY_NAME);	 
				 eo.setToExerciceRelationship(toExercice);
				 eo.setToUtilisateurFonctionRelationship(toUtilisateurFonction);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOUtilisateurFonctionExercice creerInstance(EOEditingContext editingContext) {
		EOUtilisateurFonctionExercice object = (EOUtilisateurFonctionExercice)EOUtilities.createAndInsertInstance(editingContext, _EOUtilisateurFonctionExercice.ENTITY_NAME);
  		return object;
		}

	

  public EOUtilisateurFonctionExercice localInstanceIn(EOEditingContext editingContext) {
    EOUtilisateurFonctionExercice localInstance = (EOUtilisateurFonctionExercice)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> eoObjects = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOUtilisateurFonctionExercice fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOUtilisateurFonctionExercice fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOUtilisateurFonctionExercice> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateurFonctionExercice eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateurFonctionExercice)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateurFonctionExercice fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateurFonctionExercice fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOUtilisateurFonctionExercice> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateurFonctionExercice eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateurFonctionExercice)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOUtilisateurFonctionExercice fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateurFonctionExercice eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateurFonctionExercice ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateurFonctionExercice fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
