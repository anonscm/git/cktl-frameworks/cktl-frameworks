/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOVlans.java instead.
package org.cocktail.fwkcktldroitsutils.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOVlans extends  AfwkDroitUtilsRecord {
	public static final String ENTITY_NAME = "FwkDroitsUtils_Vlans";
	public static final String ENTITY_TABLE_NAME = "GRHUM.VLANS";


//Attribute Keys
	public static final ERXKey<String> C_VLAN = new ERXKey<String>("cVlan");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> LISTE__IP = new ERXKey<String>("liste_Ip");
	public static final ERXKey<String> LL_VLAN = new ERXKey<String>("llVlan");
	public static final ERXKey<Integer> PRIORITE = new ERXKey<Integer>("priorite");
	public static final ERXKey<String> PRISE__COMPTE = new ERXKey<String>("prise_Compte");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeCryptage> TO_TYPE_CRYPTAGE = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeCryptage>("toTypeCryptage");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeVpn> TO_TYPE_VPN = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeVpn>("toTypeVpn");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cVlan";

	public static final String C_VLAN_KEY = "cVlan";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LISTE__IP_KEY = "liste_Ip";
	public static final String LL_VLAN_KEY = "llVlan";
	public static final String PRIORITE_KEY = "priorite";
	public static final String PRISE__COMPTE_KEY = "prise_Compte";

//Attributs non visibles
	public static final String TCRY_ORDRE_KEY = "tcryOrdre";
	public static final String TVPN_CODE_KEY = "tvpnCode";

//Colonnes dans la base de donnees
	public static final String C_VLAN_COLKEY = "C_VLAN";
	public static final String D_CREATION_COLKEY = "d_Creation";
	public static final String D_MODIFICATION_COLKEY = "d_Modification";
	public static final String LISTE__IP_COLKEY = "liste_Ip";
	public static final String LL_VLAN_COLKEY = "LL_VLAN";
	public static final String PRIORITE_COLKEY = "PRIORITE";
	public static final String PRISE__COMPTE_COLKEY = "prise_Compte";

	public static final String TCRY_ORDRE_COLKEY = "tcry_Ordre";
	public static final String TVPN_CODE_COLKEY = "tvpn_Code";


	// Relationships
	public static final String TO_TYPE_CRYPTAGE_KEY = "toTypeCryptage";
	public static final String TO_TYPE_VPN_KEY = "toTypeVpn";



	// Accessors methods
	public String cVlan() {
	 return (String) storedValueForKey(C_VLAN_KEY);
	}

	public void setCVlan(String value) {
	 takeStoredValueForKey(value, C_VLAN_KEY);
	}

	public NSTimestamp dCreation() {
	 return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dModification() {
	 return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
	 takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	public String liste_Ip() {
	 return (String) storedValueForKey(LISTE__IP_KEY);
	}

	public void setListe_Ip(String value) {
	 takeStoredValueForKey(value, LISTE__IP_KEY);
	}

	public String llVlan() {
	 return (String) storedValueForKey(LL_VLAN_KEY);
	}

	public void setLlVlan(String value) {
	 takeStoredValueForKey(value, LL_VLAN_KEY);
	}

	public Integer priorite() {
	 return (Integer) storedValueForKey(PRIORITE_KEY);
	}

	public void setPriorite(Integer value) {
	 takeStoredValueForKey(value, PRIORITE_KEY);
	}

	public String prise_Compte() {
	 return (String) storedValueForKey(PRISE__COMPTE_KEY);
	}

	public void setPrise_Compte(String value) {
	 takeStoredValueForKey(value, PRISE__COMPTE_KEY);
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOTypeCryptage toTypeCryptage() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOTypeCryptage)storedValueForKey(TO_TYPE_CRYPTAGE_KEY);
	}

	public void setToTypeCryptageRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOTypeCryptage value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOTypeCryptage oldValue = toTypeCryptage();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CRYPTAGE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CRYPTAGE_KEY);
	 }
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOTypeVpn toTypeVpn() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOTypeVpn)storedValueForKey(TO_TYPE_VPN_KEY);
	}

	public void setToTypeVpnRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOTypeVpn value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOTypeVpn oldValue = toTypeVpn();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_VPN_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_VPN_KEY);
	 }
	}


	/**
	* Créer une instance de EOVlans avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOVlans createEOVlans(EOEditingContext editingContext				, String cVlan
							, NSTimestamp dCreation
							, NSTimestamp dModification
													, org.cocktail.fwkcktldroitsutils.common.metier.EOTypeCryptage toTypeCryptage							) {
	 EOVlans eo = (EOVlans) EOUtilities.createAndInsertInstance(editingContext, _EOVlans.ENTITY_NAME);	 
							eo.setCVlan(cVlan);
									eo.setDCreation(dCreation);
									eo.setDModification(dModification);
														 eo.setToTypeCryptageRelationship(toTypeCryptage);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVlans creerInstance(EOEditingContext editingContext) {
		EOVlans object = (EOVlans)EOUtilities.createAndInsertInstance(editingContext, _EOVlans.ENTITY_NAME);
  		return object;
		}

	

  public EOVlans localInstanceIn(EOEditingContext editingContext) {
    EOVlans localInstance = (EOVlans)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOVlans> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOVlans> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOVlans> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOVlans> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOVlans> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOVlans> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOVlans> eoObjects = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOVlans>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVlans fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVlans fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOVlans> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVlans eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVlans)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVlans fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVlans fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOVlans> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVlans eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVlans)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVlans fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVlans eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVlans ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVlans fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
