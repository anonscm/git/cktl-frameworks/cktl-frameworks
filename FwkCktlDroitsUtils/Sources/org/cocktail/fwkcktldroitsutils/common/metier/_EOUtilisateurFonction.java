/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOUtilisateurFonction.java instead.
package org.cocktail.fwkcktldroitsutils.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOUtilisateurFonction extends  AfwkDroitUtilsRecord {
	public static final String ENTITY_NAME = "FwkDroitsUtils_UtilisateurFonction";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.UTILISATEUR_FONCT";


//Attribute Keys
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction> TO_FONCTION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOFonction>("toFonction");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> TO_UTILISATEUR = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("toUtilisateur");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> TO_UTILISATEUR_FONCTION_EXERCICES = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice>("toUtilisateurFonctionExercices");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion> TO_UTILISATEUR_FONCTION_GESTIONS = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion>("toUtilisateurFonctionGestions");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ufOrdre";


//Attributs non visibles
	public static final String FON_ORDRE_KEY = "fonOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";
	public static final String UF_ORDRE_KEY = "ufOrdre";

//Colonnes dans la base de donnees

	public static final String FON_ORDRE_COLKEY = "FON_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";
	public static final String UF_ORDRE_COLKEY = "uf_ORDRE";


	// Relationships
	public static final String TO_FONCTION_KEY = "toFonction";
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";
	public static final String TO_UTILISATEUR_FONCTION_EXERCICES_KEY = "toUtilisateurFonctionExercices";
	public static final String TO_UTILISATEUR_FONCTION_GESTIONS_KEY = "toUtilisateurFonctionGestions";



	// Accessors methods
	public org.cocktail.fwkcktldroitsutils.common.metier.EOFonction toFonction() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOFonction)storedValueForKey(TO_FONCTION_KEY);
	}

	public void setToFonctionRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOFonction value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOFonction oldValue = toFonction();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FONCTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_FONCTION_KEY);
	 }
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateur() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
	}

	public void setToUtilisateurRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = toUtilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> toUtilisateurFonctionExercices() {
	 return (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice>)storedValueForKey(TO_UTILISATEUR_FONCTION_EXERCICES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> toUtilisateurFonctionExercices(EOQualifier qualifier) {
	 return toUtilisateurFonctionExercices(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> toUtilisateurFonctionExercices(EOQualifier qualifier, boolean fetch) {
	 return toUtilisateurFonctionExercices(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> toUtilisateurFonctionExercices(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice.TO_UTILISATEUR_FONCTION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toUtilisateurFonctionExercices();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToUtilisateurFonctionExercicesRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_UTILISATEUR_FONCTION_EXERCICES_KEY);
	}
	
	public void removeFromToUtilisateurFonctionExercicesRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_UTILISATEUR_FONCTION_EXERCICES_KEY);
	}
	
	public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice createToUtilisateurFonctionExercicesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_UTILISATEUR_FONCTION_EXERCICES_KEY);
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice) eo;
	}
	
	public void deleteToUtilisateurFonctionExercicesRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_UTILISATEUR_FONCTION_EXERCICES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToUtilisateurFonctionExercicesRelationships() {
	 Enumeration<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice> objects = toUtilisateurFonctionExercices().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToUtilisateurFonctionExercicesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion> toUtilisateurFonctionGestions() {
	 return (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion>)storedValueForKey(TO_UTILISATEUR_FONCTION_GESTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion> toUtilisateurFonctionGestions(EOQualifier qualifier) {
	 return toUtilisateurFonctionGestions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion> toUtilisateurFonctionGestions(EOQualifier qualifier, boolean fetch) {
	 return toUtilisateurFonctionGestions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion> toUtilisateurFonctionGestions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion.TO_UTILISATEUR_FONCTION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toUtilisateurFonctionGestions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToUtilisateurFonctionGestionsRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_UTILISATEUR_FONCTION_GESTIONS_KEY);
	}
	
	public void removeFromToUtilisateurFonctionGestionsRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_UTILISATEUR_FONCTION_GESTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion createToUtilisateurFonctionGestionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_UTILISATEUR_FONCTION_GESTIONS_KEY);
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion) eo;
	}
	
	public void deleteToUtilisateurFonctionGestionsRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_UTILISATEUR_FONCTION_GESTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToUtilisateurFonctionGestionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionGestion> objects = toUtilisateurFonctionGestions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToUtilisateurFonctionGestionsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOUtilisateurFonction avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOUtilisateurFonction createEOUtilisateurFonction(EOEditingContext editingContext		, org.cocktail.fwkcktldroitsutils.common.metier.EOFonction toFonction		, org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateur					) {
	 EOUtilisateurFonction eo = (EOUtilisateurFonction) EOUtilities.createAndInsertInstance(editingContext, _EOUtilisateurFonction.ENTITY_NAME);	 
				 eo.setToFonctionRelationship(toFonction);
				 eo.setToUtilisateurRelationship(toUtilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOUtilisateurFonction creerInstance(EOEditingContext editingContext) {
		EOUtilisateurFonction object = (EOUtilisateurFonction)EOUtilities.createAndInsertInstance(editingContext, _EOUtilisateurFonction.ENTITY_NAME);
  		return object;
		}

	

  public EOUtilisateurFonction localInstanceIn(EOEditingContext editingContext) {
    EOUtilisateurFonction localInstance = (EOUtilisateurFonction)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> eoObjects = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOUtilisateurFonction fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOUtilisateurFonction fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOUtilisateurFonction> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateurFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateurFonction)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateurFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateurFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOUtilisateurFonction> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateurFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateurFonction)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOUtilisateurFonction fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateurFonction eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateurFonction ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateurFonction fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
