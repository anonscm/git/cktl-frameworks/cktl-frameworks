/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldroitsutils.common.metier;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktldroitsutils.common.CktlCallEOUtilities;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOTemporaryGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXGenericRecord;

/**
 * Classe abstraite pour les entités métier du framework FwkCktlDroitUtils.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public abstract class AfwkDroitUtilsRecord extends ERXGenericRecord {

	public static final String OUI = "O";
	public static final String NON = "N";

	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String PERS_ID_SUPPRESSION_KEY = "persIdSuppression";
	private NSMutableArray specificites = new NSMutableArray();
	private NSArray attributes = null;
	private Map attributesTaillesMax;
	private NSArray attributesObligatoires;

	private ApplicationUser createur;
	private ApplicationUser modificateur;
	private ApplicationUser suppresseur;
	private Integer persIdSuppression;

	/**
	 * @return La date actuelle.
	 */
	public NSTimestamp now() {
		return new NSTimestamp(new Date());
	}

	/**
	 * Verifie si la longueur d'une chaine est bien inferieure a un maximum.
	 * 
	 * @param s
	 * @param l -1 si illimite.
	 * @return true si l=-1 ou si le nombre de caracteres de s est inferieur ou egal a l.
	 */
	protected boolean verifieLongueurMax(String s, int l) {
		return (l == -1 || s == null || s.trim().length() <= l);
	}

	/**
	 * Verifie si la longeur d'une chaine est bien superieure a un maximum.
	 * 
	 * @param s
	 * @param l
	 * @return true si l=0 ou si le nombre de caracteres de s est superieur ou egal a l.
	 */
	protected boolean verifieLongueurMin(String s, int l) {
		return (s == null || s.trim().length() >= l);
	}

	/**
	 * Nettoie toutes les chaines de l'objet (en effectuant un trim). A appeler en debut de ValidateObjectMetier.
	 */
	protected void trimAllString() {
		EOEnterpriseObject obj = this;
		NSArray atts = obj.attributeKeys();
		for (int i = 0; i < atts.count(); i++) {
			String array_element = (String) atts.objectAtIndex(i);
			if (obj.valueForKey(array_element) != null && obj.valueForKey(array_element) instanceof String) {
				obj.takeValueForKey(((String) obj.valueForKey(array_element)).trim(), array_element);
			}
		}
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		callOnAwakeFromInsertionForSpecificites();
	}

	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		callOnValidateForInsertOnSpecificites();
		super.validateForInsert();
	}

	@Override
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		callOnValidateForUpdateOnSpecificites();
		super.validateForUpdate();
	}

	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
		callOnValidateForDeleteOnSpecificites();
	}

	@Override
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		callOnValidateObjectMetierOnSpecificites();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		callOnValidateBeforeTransactionSaveOnSpecificites();
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		if (eoenterpriseobject == null) {
			return null;
		}
		else {
			com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
			return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);
		}
	}

	public static AfwkDroitUtilsRecord createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}

	/**
	 * @param eoeditingcontext
	 * @param s
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau est affecte a l'objet juste apres sa creation, avant l'insertion dans
	 *            l'editing context.
	 * @return
	 */
	public static AfwkDroitUtilsRecord createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			AfwkDroitUtilsRecord eoenterpriseobject = (AfwkDroitUtilsRecord) eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			if (specificites != null) {
				Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
				while (enumeration.hasMoreElements()) {
					AfwkDroitUtilsRecord.ISpecificite specificite = enumeration.nextElement();
					eoenterpriseobject.registerSpecificite(specificite);
				}
			}
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	/**
	 * Interface a respecter pour ajouter des specificites métier à l'objet. Pour chaque objet déclaré comme spécificité, les méthodes seront appelées
	 * lors des étapes de la vie de l'objet, que ce soit lors de la création ou lors des étapes de validation. Ceci permet par exemple d'ajouter du
	 * code pour définir les règles métier d'un partenaire (qui est une spécificité d'un objet {@link EOStructure}). <br/>
	 * 
	 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
	 */
	public interface ISpecificite {

		/**
		 * Appele lors de l'insertion dans l'editing context.<br/>
		 * NB :
		 * 
		 * @param afwkPersRecord
		 */
		public void onAwakeFromInsertion(AfwkDroitUtilsRecord afwkPersRecord);

		/**
		 * Appele par le saveChanges lors de l'insertion d'un objet (par validateForUpdate)
		 * 
		 * @param afwkPersRecord L'objet sur lequel porte la methode.
		 */
		public void onValidateForInsert(AfwkDroitUtilsRecord afwkPersRecord) throws NSValidation.ValidationException;

		/**
		 * Appele par le saveChanges lors de la mise a jour d'un objet (par validateForUpdate)
		 * 
		 * @param afwkPersRecord L'objet sur lequel porte la methode.
		 */
		public void onValidateForUpdate(AfwkDroitUtilsRecord afwkPersRecord) throws NSValidation.ValidationException;

		/**
		 * Appele par le saveChanges lors lors de la suppression d'un objet (par validateForDelete)
		 * 
		 * @param afwkPersRecord L'objet sur lequel porte la methode.
		 */
		public void onValidateForDelete(AfwkDroitUtilsRecord afwkPersRecord) throws NSValidation.ValidationException;

		/**
		 * Appele avant l'enregistrement d'un objet (par validateObjectMetier)
		 * 
		 * @param afwkPersRecord L'objet sur lequel porte la methode.
		 */
		public void onValidateObjectMetier(AfwkDroitUtilsRecord afwkPersRecord) throws NSValidation.ValidationException;

		/**
		 * Appele avant l'enregistrement d'un objet (par ValidateBeforeTransactionSave)
		 * 
		 * @param afwkPersRecord L'objet sur lequel porte la methode.
		 */
		public void onValidateBeforeTransactionSave(AfwkDroitUtilsRecord afwkDroitUtilsRecord) throws NSValidation.ValidationException;

	}

	/**
	 * Ajoute un specificite pour les operations de validation de l'objet metier.
	 * 
	 * @param specificite
	 */
	public void registerSpecificite(ISpecificite specificite) {
		if (specificites == null) {
			specificites = new NSMutableArray();
		}
		if (specificites.indexOfObject(specificite) == NSArray.NotFound) {
			specificites.addObject(specificite);
		}
	}

	/**
	 * Supprime une specificite la liste des specificites.
	 * 
	 * @param specificite
	 */
	public void unregisterSpecificite(ISpecificite specificite) {
		if (specificites != null) {
			if (specificites.indexOfObject(specificite) == NSArray.NotFound) {
				specificites.removeObject(specificite);
			}
		}
	}

	private void callOnAwakeFromInsertionForSpecificites() {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onAwakeFromInsertion(this);
			}
		}
	}

	private void callOnValidateForDeleteOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateForDelete(this);
			}
		}
	}

	private void callOnValidateForInsertOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateForInsert(this);
			}
		}
	}

	private void callOnValidateForUpdateOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateForUpdate(this);
			}
		}
	}

	private void callOnValidateObjectMetierOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateObjectMetier(this);
			}
		}
	}

	private void callOnValidateBeforeTransactionSaveOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateBeforeTransactionSave(this);
			}
		}
	}

	public NSMutableArray getSpecificites() {
		return specificites;
	}

	public void setSpecificites(NSMutableArray specificites) {
		this.specificites = specificites;
	}

	//	/**
	//	 * @return Les attributs de l'entité.
	//	 */
	//	public NSArray attributes() {
	//		if (attributes == null) {
	//			attributes = EOUtilities.entityForObject(this.editingContext(), this).classProperties();
	//		}
	//		return attributes;
	//	}

	/**
	 * @return Une map avec en clé le nom de l'attribut et en valeur un integer contenant la taille max de l'attribut.
	 */
	public Map attributesTaillesMax() {
		if (attributesTaillesMax == null) {
			attributesTaillesMax = CktlCallEOUtilities.attributeMaxSizesForEntityName(this.editingContext(), entityName());
		}
		return attributesTaillesMax;
	}

	//	public Map attributesTaillesMax() {		
	//		if (attributesTaillesMax == null) {
	//			attributesTaillesMax = new HashMap();
	//			Enumeration enumeration = attributes().objectEnumerator();
	//			while (enumeration.hasMoreElements()) {
	//				EOProperty object = (EOProperty) enumeration.nextElement();
	//				if (object instanceof EOAttribute) {
	//					if (((EOAttribute) object).width() > 0) {
	//						attributesTaillesMax.put(object.name(), Integer.valueOf(((EOAttribute) object).width()));
	//					}
	//				}
	//				
	//			}
	//		}
	//		return attributesTaillesMax;
	//	}

	public NSArray attributesObligatoires() {
		if (attributesObligatoires == null) {
			attributesObligatoires = CktlCallEOUtilities.requiredAttributeForEntityName(this.editingContext(), entityName());
		}
		return attributesObligatoires;
	}

	//	public NSArray attributesObligatoires() {
	//		if (attributesObligatoires == null) {
	//			NSMutableArray _attributesObligatoires = new NSMutableArray();
	//			Enumeration enumeration = attributes().objectEnumerator();
	//			while (enumeration.hasMoreElements()) {
	//				EOProperty object = (EOProperty) enumeration.nextElement();
	//				if (object instanceof EOAttribute) {
	//					if (!((EOAttribute) object).allowsNull()) {
	//						_attributesObligatoires.addObject(object.name());
	//					}
	//				}
	//			}
	//			attributesObligatoires = _attributesObligatoires.immutableClone();
	//		}
	//		return attributesObligatoires;
	//	}

	/**
	 * Vérife si les champs obligatoires sont bien saisis (en fonction de ce qui est indiqué dans le modèle)
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkContraintesObligatoires() throws NSValidation.ValidationException {
		Iterator iterator = attributesObligatoires().iterator();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			if (valueForKey(key) == null) {
				throw new NSValidation.ValidationException("Le champ " + getDisplayName(key) + " est obligatoire.");
			}
		}
	}

	/**
	 * Renvoie le nom d'affichage de la propriété. Utilise {@link #displayNames()}.
	 * 
	 * @param propertyName nom de la propriété (attribut ou relation)
	 * @return
	 */
	public String getDisplayName(String propertyName) {
		if (displayNames().get(propertyName) != null) {
			return (String) displayNames().get(propertyName);
		}
		return propertyName;
	}

	/**
	 * Renvoie une Map contenant en clé le nom de la propriété et en valeur le nom d'affichage (parlant) de cette propriété. Par exemple <CP, Code
	 * Postal>. Par défaut la Map est vide, il faut surcharger la méthode.
	 */
	public Map displayNames() {
		return new HashMap();
	}

	/**
	 * Verifie la longueur maximale des champs saisis (a partir de la taille indiquée dans le modèle).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkContraintesLongueursMax() throws NSValidation.ValidationException {
		Iterator iterator = attributesTaillesMax().keySet().iterator();
		System.out.println(attributesTaillesMax().keySet());
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			//System.out.println("verif :" + getDisplayName(key) + " : " + ((Integer) attributesTaillesMax().get(key)).intValue());
			if (valueForKey(key) != null && ((String) valueForKey(key)).length() > ((Integer) attributesTaillesMax().get(key)).intValue()) {
				throw new NSValidation.ValidationException("La taille du champ " + getDisplayName(key) + " ne doit pas dépasser " + ((Integer) attributesTaillesMax().get(key)).intValue() + " caractères.");
			}
		}
	}

	/**
	 * @return le createur de l'enregistrement (si le champ persIdCreation est renseigne).
	 */
	public ApplicationUser getCreateur() {
		Integer persId = (Integer) valueForKey(PERS_ID_CREATION_KEY);
		if ((createur == null && persId != null) || (createur != null && !createur.getPersonne().persId().equals(persId))) {
			if (persId == null) {
				createur = null;
			}
			else {
				createur = new ApplicationUser(editingContext(), persId);
			}
		}
		return createur;
	}

	/**
	 * @return le modificateur de l'enregistrement (si le champ persIdModification est renseigne).
	 */
	public ApplicationUser getModificateur() {
		Integer persId = (Integer) valueForKey(PERS_ID_MODIFICATION_KEY);
		if ((modificateur == null && persId != null) || (modificateur != null && !modificateur.getPersonne().persId().equals(persId))) {
			if (persId == null) {
				modificateur = null;
			}
			else {
				modificateur = new ApplicationUser(editingContext(), persId);
			}
		}
		return modificateur;
	}

	/**
	 * @return le modificateur de l'enregistrement (si le champ persIdModification est renseigne).
	 */
	public ApplicationUser getSuppresseur() {
		Integer persId = (Integer) valueForKey(PERS_ID_SUPPRESSION_KEY);
		if ((suppresseur == null && persId != null) || (suppresseur != null && !suppresseur.getPersonne().persId().equals(persId))) {
			if (persId == null) {
				suppresseur = null;
			}
			else {
				suppresseur = new ApplicationUser(editingContext(), persId);
			}
		}
		return suppresseur;
	}

	/**
	 * @return true si l'objet possede un globalID temporaire (n'existe pas encore dans la base de données).
	 */
	public boolean hasTemporaryGlobalID() {
		return (globalID() != null && globalID() instanceof EOTemporaryGlobalID);
	}

	/**
	 * @return le globalID de l'objet à partir de l'editingContext associé. Null si pas d'editingContext.
	 */
	public EOGlobalID globalID() {
		if (editingContext() == null) {
			return null;
		}
		return globalID(this.editingContext());
	}

	/**
	 * @param ec
	 * @return le globalID de l'objet à partir d'ec.
	 */
	public EOGlobalID globalID(EOEditingContext ec) {
		return ec.globalIDForObject(this);
	}

	public Integer persIdSuppression() {
		return persIdSuppression;
	}

	public void setPersIdModification(Integer value) {
		persIdSuppression = value;
	}

	public String dressExceptionMsg(Exception e) {
		return this.getClass().getSimpleName() + " : " + toDisplayString() + ":" + e.getLocalizedMessage();
	}

	public String toDisplayString() {
		return toString();
	}
}
