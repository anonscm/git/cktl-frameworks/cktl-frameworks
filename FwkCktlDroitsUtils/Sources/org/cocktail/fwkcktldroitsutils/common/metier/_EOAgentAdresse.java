/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOAgentAdresse.java instead.
package org.cocktail.fwkcktldroitsutils.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOAgentAdresse extends  AfwkDroitUtilsRecord {
	public static final String ENTITY_NAME = "FwkDroitsUtils_AgentAdresses";
	public static final String ENTITY_TABLE_NAME = "grhum.Agent_adresses";


//Attribute Keys
	public static final ERXKey<String> AGT_AJOUT = new ERXKey<String>("agtAjout");
	public static final ERXKey<String> AGT_CIVILITE_MODIFS = new ERXKey<String>("agtCiviliteModifs");
	public static final ERXKey<String> AGT_COMPTE = new ERXKey<String>("agtCompte");
	public static final ERXKey<String> AGT_COMPTE_MODIFS = new ERXKey<String>("agtCompteModifs");
	public static final ERXKey<String> AGT_COMPTE_SUPPRESSION = new ERXKey<String>("agtCompteSuppression");
	public static final ERXKey<String> AGT_CPT_TEMPO = new ERXKey<String>("agtCptTempo");
	public static final ERXKey<String> AGT_FORUM = new ERXKey<String>("agtForum");
	public static final ERXKey<String> AGT_LOGIN = new ERXKey<String>("agtLogin");
	public static final ERXKey<String> AGT_PHOTO = new ERXKey<String>("agtPhoto");
	public static final ERXKey<String> AGT_SUPPRESSION = new ERXKey<String>("agtSuppression");
	public static final ERXKey<String> AGT_TEM_VALIDE = new ERXKey<String>("agtTemValide");
	public static final ERXKey<String> AGT_TOUT = new ERXKey<String>("agtTout");
	public static final ERXKey<String> AGT_VOIR_INFOS_PERSO = new ERXKey<String>("agtVoirInfosPerso");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
// Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "agtLogin";

	public static final String AGT_AJOUT_KEY = "agtAjout";
	public static final String AGT_CIVILITE_MODIFS_KEY = "agtCiviliteModifs";
	public static final String AGT_COMPTE_KEY = "agtCompte";
	public static final String AGT_COMPTE_MODIFS_KEY = "agtCompteModifs";
	public static final String AGT_COMPTE_SUPPRESSION_KEY = "agtCompteSuppression";
	public static final String AGT_CPT_TEMPO_KEY = "agtCptTempo";
	public static final String AGT_FORUM_KEY = "agtForum";
	public static final String AGT_LOGIN_KEY = "agtLogin";
	public static final String AGT_PHOTO_KEY = "agtPhoto";
	public static final String AGT_SUPPRESSION_KEY = "agtSuppression";
	public static final String AGT_TEM_VALIDE_KEY = "agtTemValide";
	public static final String AGT_TOUT_KEY = "agtTout";
	public static final String AGT_VOIR_INFOS_PERSO_KEY = "agtVoirInfosPerso";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_INDIVIDU_KEY = "noIndividu";

//Attributs non visibles

//Colonnes dans la base de donnees
	public static final String AGT_AJOUT_COLKEY = "agt_Ajout";
	public static final String AGT_CIVILITE_MODIFS_COLKEY = "agt_Civilite_Modifs";
	public static final String AGT_COMPTE_COLKEY = "agt_Compte";
	public static final String AGT_COMPTE_MODIFS_COLKEY = "agt_Compte_Modifs";
	public static final String AGT_COMPTE_SUPPRESSION_COLKEY = "AGT_COMPTE_SUPPR";
	public static final String AGT_CPT_TEMPO_COLKEY = "agt_Cpt_Tempo";
	public static final String AGT_FORUM_COLKEY = "AGT_FORUM";
	public static final String AGT_LOGIN_COLKEY = "agt_Login";
	public static final String AGT_PHOTO_COLKEY = "agt_Photo";
	public static final String AGT_SUPPRESSION_COLKEY = "agt_Suppression";
	public static final String AGT_TEM_VALIDE_COLKEY = "AGT_TEM_VALIDE";
	public static final String AGT_TOUT_COLKEY = "agt_Tout";
	public static final String AGT_VOIR_INFOS_PERSO_COLKEY = "AGT_VOIR_INFOS_PERSO";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NO_INDIVIDU_COLKEY = "no_Individu";



	// Relationships



	// Accessors methods
	public String agtAjout() {
	 return (String) storedValueForKey(AGT_AJOUT_KEY);
	}

	public void setAgtAjout(String value) {
	 takeStoredValueForKey(value, AGT_AJOUT_KEY);
	}

	public String agtCiviliteModifs() {
	 return (String) storedValueForKey(AGT_CIVILITE_MODIFS_KEY);
	}

	public void setAgtCiviliteModifs(String value) {
	 takeStoredValueForKey(value, AGT_CIVILITE_MODIFS_KEY);
	}

	public String agtCompte() {
	 return (String) storedValueForKey(AGT_COMPTE_KEY);
	}

	public void setAgtCompte(String value) {
	 takeStoredValueForKey(value, AGT_COMPTE_KEY);
	}

	public String agtCompteModifs() {
	 return (String) storedValueForKey(AGT_COMPTE_MODIFS_KEY);
	}

	public void setAgtCompteModifs(String value) {
	 takeStoredValueForKey(value, AGT_COMPTE_MODIFS_KEY);
	}

	public String agtCompteSuppression() {
	 return (String) storedValueForKey(AGT_COMPTE_SUPPRESSION_KEY);
	}

	public void setAgtCompteSuppression(String value) {
	 takeStoredValueForKey(value, AGT_COMPTE_SUPPRESSION_KEY);
	}

	public String agtCptTempo() {
	 return (String) storedValueForKey(AGT_CPT_TEMPO_KEY);
	}

	public void setAgtCptTempo(String value) {
	 takeStoredValueForKey(value, AGT_CPT_TEMPO_KEY);
	}

	public String agtForum() {
	 return (String) storedValueForKey(AGT_FORUM_KEY);
	}

	public void setAgtForum(String value) {
	 takeStoredValueForKey(value, AGT_FORUM_KEY);
	}

	public String agtLogin() {
	 return (String) storedValueForKey(AGT_LOGIN_KEY);
	}

	public void setAgtLogin(String value) {
	 takeStoredValueForKey(value, AGT_LOGIN_KEY);
	}

	public String agtPhoto() {
	 return (String) storedValueForKey(AGT_PHOTO_KEY);
	}

	public void setAgtPhoto(String value) {
	 takeStoredValueForKey(value, AGT_PHOTO_KEY);
	}

	public String agtSuppression() {
	 return (String) storedValueForKey(AGT_SUPPRESSION_KEY);
	}

	public void setAgtSuppression(String value) {
	 takeStoredValueForKey(value, AGT_SUPPRESSION_KEY);
	}

	public String agtTemValide() {
	 return (String) storedValueForKey(AGT_TEM_VALIDE_KEY);
	}

	public void setAgtTemValide(String value) {
	 takeStoredValueForKey(value, AGT_TEM_VALIDE_KEY);
	}

	public String agtTout() {
	 return (String) storedValueForKey(AGT_TOUT_KEY);
	}

	public void setAgtTout(String value) {
	 takeStoredValueForKey(value, AGT_TOUT_KEY);
	}

	public String agtVoirInfosPerso() {
	 return (String) storedValueForKey(AGT_VOIR_INFOS_PERSO_KEY);
	}

	public void setAgtVoirInfosPerso(String value) {
	 takeStoredValueForKey(value, AGT_VOIR_INFOS_PERSO_KEY);
	}

	public NSTimestamp dCreation() {
	 return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dModification() {
	 return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
	 takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	public Integer noIndividu() {
	 return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
	}

	public void setNoIndividu(Integer value) {
	 takeStoredValueForKey(value, NO_INDIVIDU_KEY);
	}


	/**
	* Créer une instance de EOAgentAdresse avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOAgentAdresse createEOAgentAdresse(EOEditingContext editingContext																		, String agtLogin
															, String agtVoirInfosPerso
							, NSTimestamp dCreation
							, NSTimestamp dModification
										) {
	 EOAgentAdresse eo = (EOAgentAdresse) EOUtilities.createAndInsertInstance(editingContext, _EOAgentAdresse.ENTITY_NAME);	 
																					eo.setAgtLogin(agtLogin);
																	eo.setAgtVoirInfosPerso(agtVoirInfosPerso);
									eo.setDCreation(dCreation);
									eo.setDModification(dModification);
							 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAgentAdresse creerInstance(EOEditingContext editingContext) {
		EOAgentAdresse object = (EOAgentAdresse)EOUtilities.createAndInsertInstance(editingContext, _EOAgentAdresse.ENTITY_NAME);
  		return object;
		}

	

  public EOAgentAdresse localInstanceIn(EOEditingContext editingContext) {
    EOAgentAdresse localInstance = (EOAgentAdresse)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOAgentAdresse> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOAgentAdresse> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOAgentAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOAgentAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOAgentAdresse> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOAgentAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOAgentAdresse> eoObjects = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOAgentAdresse>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAgentAdresse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAgentAdresse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOAgentAdresse> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAgentAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAgentAdresse)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAgentAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAgentAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOAgentAdresse> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAgentAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAgentAdresse)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAgentAdresse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAgentAdresse eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAgentAdresse ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAgentAdresse fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
