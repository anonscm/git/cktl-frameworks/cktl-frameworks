/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOUtilisateur.java instead.
package org.cocktail.fwkcktldroitsutils.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOUtilisateur extends  AfwkDroitUtilsRecord {
	public static final String ENTITY_NAME = "FwkDroitsUtils_Utilisateur";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.UTILISATEUR";


//Attribute Keys
	public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
	public static final ERXKey<NSTimestamp> UTL_FERMETURE = new ERXKey<NSTimestamp>("utlFermeture");
	public static final ERXKey<Integer> UTL_ORDRE = new ERXKey<Integer>("utlOrdre");
	public static final ERXKey<NSTimestamp> UTL_OUVERTURE = new ERXKey<NSTimestamp>("utlOuverture");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonne");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeEtat> TO_TYPE_ETAT = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOTypeEtat>("toTypeEtat");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> TO_UTILISATEUR_FONCTIONS = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction>("toUtilisateurFonctions");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo> TO_UTILISATEUR_INFO = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo>("toUtilisateurInfo");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "utlOrdre";

	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String UTL_FERMETURE_KEY = "utlFermeture";
	public static final String UTL_ORDRE_KEY = "utlOrdre";
	public static final String UTL_OUVERTURE_KEY = "utlOuverture";

//Attributs non visibles
	public static final String TYET_ID_KEY = "tyetId";
	public static final String PERS_ID_KEY = "persId";

//Colonnes dans la base de donnees
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String UTL_FERMETURE_COLKEY = "UTL_FERMETURE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";
	public static final String UTL_OUVERTURE_COLKEY = "UTL_OUVERTURE";

	public static final String TYET_ID_COLKEY = "tyet_id";
	public static final String PERS_ID_COLKEY = "PERS_ID";


	// Relationships
	public static final String TO_PERSONNE_KEY = "toPersonne";
	public static final String TO_TYPE_ETAT_KEY = "toTypeEtat";
	public static final String TO_UTILISATEUR_FONCTIONS_KEY = "toUtilisateurFonctions";
	public static final String TO_UTILISATEUR_INFO_KEY = "toUtilisateurInfo";



	// Accessors methods
	public Integer noIndividu() {
	 return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
	}

	public void setNoIndividu(Integer value) {
	 takeStoredValueForKey(value, NO_INDIVIDU_KEY);
	}

	public NSTimestamp utlFermeture() {
	 return (NSTimestamp) storedValueForKey(UTL_FERMETURE_KEY);
	}

	public void setUtlFermeture(NSTimestamp value) {
	 takeStoredValueForKey(value, UTL_FERMETURE_KEY);
	}

	public Integer utlOrdre() {
	 return (Integer) storedValueForKey(UTL_ORDRE_KEY);
	}

	public void setUtlOrdre(Integer value) {
	 takeStoredValueForKey(value, UTL_ORDRE_KEY);
	}

	public NSTimestamp utlOuverture() {
	 return (NSTimestamp) storedValueForKey(UTL_OUVERTURE_KEY);
	}

	public void setUtlOuverture(NSTimestamp value) {
	 takeStoredValueForKey(value, UTL_OUVERTURE_KEY);
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonne() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(TO_PERSONNE_KEY);
	}

	public void setToPersonneRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_KEY);
	 }
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOTypeEtat toTypeEtat() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOTypeEtat)storedValueForKey(TO_TYPE_ETAT_KEY);
	}

	public void setToTypeEtatRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOTypeEtat oldValue = toTypeEtat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ETAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ETAT_KEY);
	 }
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo toUtilisateurInfo() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo)storedValueForKey(TO_UTILISATEUR_INFO_KEY);
	}

	public void setToUtilisateurInfoRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo oldValue = toUtilisateurInfo();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_INFO_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_INFO_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> toUtilisateurFonctions() {
	 return (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction>)storedValueForKey(TO_UTILISATEUR_FONCTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> toUtilisateurFonctions(EOQualifier qualifier) {
	 return toUtilisateurFonctions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> toUtilisateurFonctions(EOQualifier qualifier, boolean fetch) {
	 return toUtilisateurFonctions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> toUtilisateurFonctions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction.TO_UTILISATEUR_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toUtilisateurFonctions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToUtilisateurFonctionsRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_UTILISATEUR_FONCTIONS_KEY);
	}
	
	public void removeFromToUtilisateurFonctionsRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_UTILISATEUR_FONCTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction createToUtilisateurFonctionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_UTILISATEUR_FONCTIONS_KEY);
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction) eo;
	}
	
	public void deleteToUtilisateurFonctionsRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_UTILISATEUR_FONCTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToUtilisateurFonctionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction> objects = toUtilisateurFonctions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToUtilisateurFonctionsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOUtilisateur avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOUtilisateur createEOUtilisateur(EOEditingContext editingContext								, Integer utlOrdre
							, NSTimestamp utlOuverture
					, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonne		, org.cocktail.fwkcktldroitsutils.common.metier.EOTypeEtat toTypeEtat		, org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurInfo toUtilisateurInfo					) {
	 EOUtilisateur eo = (EOUtilisateur) EOUtilities.createAndInsertInstance(editingContext, _EOUtilisateur.ENTITY_NAME);	 
											eo.setUtlOrdre(utlOrdre);
									eo.setUtlOuverture(utlOuverture);
						 eo.setToPersonneRelationship(toPersonne);
				 eo.setToTypeEtatRelationship(toTypeEtat);
				 eo.setToUtilisateurInfoRelationship(toUtilisateurInfo);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOUtilisateur creerInstance(EOEditingContext editingContext) {
		EOUtilisateur object = (EOUtilisateur)EOUtilities.createAndInsertInstance(editingContext, _EOUtilisateur.ENTITY_NAME);
  		return object;
		}

	

  public EOUtilisateur localInstanceIn(EOEditingContext editingContext) {
    EOUtilisateur localInstance = (EOUtilisateur)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> eoObjects = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOUtilisateur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOUtilisateur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOUtilisateur> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOUtilisateur> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOUtilisateur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
