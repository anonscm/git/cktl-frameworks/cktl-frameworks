/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOPersonne.java instead.
package org.cocktail.fwkcktldroitsutils.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOPersonne extends  AfwkDroitUtilsRecord {
	public static final String ENTITY_NAME = "FwkDroitsUtils_Personne";
	public static final String ENTITY_TABLE_NAME = "GRHUM.PERSONNE";


//Attribute Keys
	public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
	public static final ERXKey<String> PERS_LC = new ERXKey<String>("persLc");
	public static final ERXKey<String> PERS_LIBELLE = new ERXKey<String>("persLibelle");
	public static final ERXKey<String> PERS_NOMPTR = new ERXKey<String>("persNomptr");
	public static final ERXKey<Integer> PERS_ORDRE = new ERXKey<Integer>("persOrdre");
	public static final ERXKey<String> PERS_TYPE = new ERXKey<String>("persType");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> TO_COMPTES = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte>("toComptes");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "persId";

	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_LC_KEY = "persLc";
	public static final String PERS_LIBELLE_KEY = "persLibelle";
	public static final String PERS_NOMPTR_KEY = "persNomptr";
	public static final String PERS_ORDRE_KEY = "persOrdre";
	public static final String PERS_TYPE_KEY = "persType";

//Attributs non visibles

//Colonnes dans la base de donnees
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_LC_COLKEY = "PERS_LC";
	public static final String PERS_LIBELLE_COLKEY = "PERS_LIBELLE";
	public static final String PERS_NOMPTR_COLKEY = "PERS_NOMPTR";
	public static final String PERS_ORDRE_COLKEY = "PERS_ORDRE";
	public static final String PERS_TYPE_COLKEY = "PERS_TYPE";



	// Relationships
	public static final String TO_COMPTES_KEY = "toComptes";



	// Accessors methods
	public Integer persId() {
	 return (Integer) storedValueForKey(PERS_ID_KEY);
	}

	public void setPersId(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_KEY);
	}

	public String persLc() {
	 return (String) storedValueForKey(PERS_LC_KEY);
	}

	public void setPersLc(String value) {
	 takeStoredValueForKey(value, PERS_LC_KEY);
	}

	public String persLibelle() {
	 return (String) storedValueForKey(PERS_LIBELLE_KEY);
	}

	public void setPersLibelle(String value) {
	 takeStoredValueForKey(value, PERS_LIBELLE_KEY);
	}

	public String persNomptr() {
	 return (String) storedValueForKey(PERS_NOMPTR_KEY);
	}

	public void setPersNomptr(String value) {
	 takeStoredValueForKey(value, PERS_NOMPTR_KEY);
	}

	public Integer persOrdre() {
	 return (Integer) storedValueForKey(PERS_ORDRE_KEY);
	}

	public void setPersOrdre(Integer value) {
	 takeStoredValueForKey(value, PERS_ORDRE_KEY);
	}

	public String persType() {
	 return (String) storedValueForKey(PERS_TYPE_KEY);
	}

	public void setPersType(String value) {
	 takeStoredValueForKey(value, PERS_TYPE_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> toComptes() {
	 return (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte>)storedValueForKey(TO_COMPTES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> toComptes(EOQualifier qualifier) {
	 return toComptes(qualifier, null);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> toComptes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		 NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> results;
			   results = toComptes();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 return results;
	}
	
	public void addToToComptesRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOCompte object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_COMPTES_KEY);
	}
	
	public void removeFromToComptesRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOCompte object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTES_KEY);
	}
	
	public org.cocktail.fwkcktldroitsutils.common.metier.EOCompte createToComptesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldroitsutils.common.metier.EOCompte.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_COMPTES_KEY);
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOCompte) eo;
	}
	
	public void deleteToComptesRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOCompte object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToComptesRelationships() {
	 Enumeration<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> objects = toComptes().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToComptesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOPersonne avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOPersonne createEOPersonne(EOEditingContext editingContext				, Integer persId
									, String persLibelle
									, Integer persOrdre
							, String persType
								) {
	 EOPersonne eo = (EOPersonne) EOUtilities.createAndInsertInstance(editingContext, _EOPersonne.ENTITY_NAME);	 
							eo.setPersId(persId);
											eo.setPersLibelle(persLibelle);
											eo.setPersOrdre(persOrdre);
									eo.setPersType(persType);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersonne creerInstance(EOEditingContext editingContext) {
		EOPersonne object = (EOPersonne)EOUtilities.createAndInsertInstance(editingContext, _EOPersonne.ENTITY_NAME);
  		return object;
		}

	

  public EOPersonne localInstanceIn(EOEditingContext editingContext) {
    EOPersonne localInstance = (EOPersonne)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> eoObjects = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPersonne fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPersonne fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPersonne> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersonne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersonne)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPersonne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPersonne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPersonne> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersonne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersonne)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPersonne fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersonne eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersonne ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPersonne fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
