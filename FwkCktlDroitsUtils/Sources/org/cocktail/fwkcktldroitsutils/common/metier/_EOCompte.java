/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCompte.java instead.
package org.cocktail.fwkcktldroitsutils.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCompte extends  AfwkDroitUtilsRecord {
	public static final String ENTITY_NAME = "FwkDroitsUtils_Compte";
	public static final String ENTITY_TABLE_NAME = "GRHUM.COMPTE";


//Attribute Keys
	public static final ERXKey<String> CPT_CHARTE = new ERXKey<String>("cptCharte");
	public static final ERXKey<String> CPT_COMMENTAIRE = new ERXKey<String>("cptCommentaire");
	public static final ERXKey<Integer> CPT_CREATEUR = new ERXKey<Integer>("cptCreateur");
	public static final ERXKey<String> CPT_CRYPTE = new ERXKey<String>("cptCrypte");
	public static final ERXKey<NSTimestamp> CPT_DEBUT_VALIDE = new ERXKey<NSTimestamp>("cptDebutValide");
	public static final ERXKey<NSTimestamp> CPT_FIN_VALIDE = new ERXKey<NSTimestamp>("cptFinValide");
	public static final ERXKey<Integer> CPT_GID = new ERXKey<Integer>("cptGid");
	public static final ERXKey<String> CPT_HOME = new ERXKey<String>("cptHome");
	public static final ERXKey<String> CPT_LISTE_ROUGE = new ERXKey<String>("cptListeRouge");
	public static final ERXKey<String> CPT_LOGIN = new ERXKey<String>("cptLogin");
	public static final ERXKey<Integer> CPT_MODIFICATEUR = new ERXKey<Integer>("cptModificateur");
	public static final ERXKey<String> CPT_PASSWD = new ERXKey<String>("cptPasswd");
	public static final ERXKey<String> CPT_PASSWD_CLAIR = new ERXKey<String>("cptPasswdClair");
	public static final ERXKey<String> CPT_PRINCIPAL = new ERXKey<String>("cptPrincipal");
	public static final ERXKey<String> CPT_SHELL = new ERXKey<String>("cptShell");
	public static final ERXKey<String> CPT_TYPE = new ERXKey<String>("cptType");
	public static final ERXKey<Integer> CPT_UID = new ERXKey<Integer>("cptUid");
	public static final ERXKey<String> CPT_VALIDE = new ERXKey<String>("cptValide");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail> TO_COMPTE_EMAILS = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail>("toCompteEmails");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOVlans> TO_VLANS = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOVlans>("toVlans");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cptOrdre";

	public static final String CPT_CHARTE_KEY = "cptCharte";
	public static final String CPT_COMMENTAIRE_KEY = "cptCommentaire";
	public static final String CPT_CREATEUR_KEY = "cptCreateur";
	public static final String CPT_CRYPTE_KEY = "cptCrypte";
	public static final String CPT_DEBUT_VALIDE_KEY = "cptDebutValide";
	public static final String CPT_FIN_VALIDE_KEY = "cptFinValide";
	public static final String CPT_GID_KEY = "cptGid";
	public static final String CPT_HOME_KEY = "cptHome";
	public static final String CPT_LISTE_ROUGE_KEY = "cptListeRouge";
	public static final String CPT_LOGIN_KEY = "cptLogin";
	public static final String CPT_MODIFICATEUR_KEY = "cptModificateur";
	public static final String CPT_PASSWD_KEY = "cptPasswd";
	public static final String CPT_PASSWD_CLAIR_KEY = "cptPasswdClair";
	public static final String CPT_PRINCIPAL_KEY = "cptPrincipal";
	public static final String CPT_SHELL_KEY = "cptShell";
	public static final String CPT_TYPE_KEY = "cptType";
	public static final String CPT_UID_KEY = "cptUid";
	public static final String CPT_VALIDE_KEY = "cptValide";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_KEY = "persId";

//Attributs non visibles
	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String TCRY_ORDRE_KEY = "tcryOrdre";
	public static final String TVPN_CODE_KEY = "tvpnCode";
	public static final String CPT_VLAN_KEY = "cptVlan";

//Colonnes dans la base de donnees
	public static final String CPT_CHARTE_COLKEY = "CPT_CHARTE";
	public static final String CPT_COMMENTAIRE_COLKEY = "CPT_COMMENTAIRE";
	public static final String CPT_CREATEUR_COLKEY = "CPT_CREATEUR";
	public static final String CPT_CRYPTE_COLKEY = "CPT_CRYPTE";
	public static final String CPT_DEBUT_VALIDE_COLKEY = "CPT_DEBUT_VALIDE";
	public static final String CPT_FIN_VALIDE_COLKEY = "CPT_FIN_VALIDE";
	public static final String CPT_GID_COLKEY = "CPT_GID";
	public static final String CPT_HOME_COLKEY = "CPT_HOME";
	public static final String CPT_LISTE_ROUGE_COLKEY = "CPT_LISTE_ROUGE";
	public static final String CPT_LOGIN_COLKEY = "CPT_LOGIN";
	public static final String CPT_MODIFICATEUR_COLKEY = "CPT_MODIFICATEUR";
	public static final String CPT_PASSWD_COLKEY = "CPT_PASSWD";
	public static final String CPT_PASSWD_CLAIR_COLKEY = "CPT_PASSWD_CLAIR";
	public static final String CPT_PRINCIPAL_COLKEY = "CPT_PRINCIPAL";
	public static final String CPT_SHELL_COLKEY = "CPT_SHELL";
	public static final String CPT_TYPE_COLKEY = "CPT_TYPE";
	public static final String CPT_UID_COLKEY = "CPT_UID";
	public static final String CPT_VALIDE_COLKEY = "CPT_VALIDE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";
	public static final String TCRY_ORDRE_COLKEY = "tcry_Ordre";
	public static final String TVPN_CODE_COLKEY = "TVPN_CODE";
	public static final String CPT_VLAN_COLKEY = "CPT_VLAN";


	// Relationships
	public static final String TO_COMPTE_EMAILS_KEY = "toCompteEmails";
	public static final String TO_VLANS_KEY = "toVlans";



	// Accessors methods
	public String cptCharte() {
	 return (String) storedValueForKey(CPT_CHARTE_KEY);
	}

	public void setCptCharte(String value) {
	 takeStoredValueForKey(value, CPT_CHARTE_KEY);
	}

	public String cptCommentaire() {
	 return (String) storedValueForKey(CPT_COMMENTAIRE_KEY);
	}

	public void setCptCommentaire(String value) {
	 takeStoredValueForKey(value, CPT_COMMENTAIRE_KEY);
	}

	public Integer cptCreateur() {
	 return (Integer) storedValueForKey(CPT_CREATEUR_KEY);
	}

	public void setCptCreateur(Integer value) {
	 takeStoredValueForKey(value, CPT_CREATEUR_KEY);
	}

	public String cptCrypte() {
	 return (String) storedValueForKey(CPT_CRYPTE_KEY);
	}

	public void setCptCrypte(String value) {
	 takeStoredValueForKey(value, CPT_CRYPTE_KEY);
	}

	public NSTimestamp cptDebutValide() {
	 return (NSTimestamp) storedValueForKey(CPT_DEBUT_VALIDE_KEY);
	}

	public void setCptDebutValide(NSTimestamp value) {
	 takeStoredValueForKey(value, CPT_DEBUT_VALIDE_KEY);
	}

	public NSTimestamp cptFinValide() {
	 return (NSTimestamp) storedValueForKey(CPT_FIN_VALIDE_KEY);
	}

	public void setCptFinValide(NSTimestamp value) {
	 takeStoredValueForKey(value, CPT_FIN_VALIDE_KEY);
	}

	public Integer cptGid() {
	 return (Integer) storedValueForKey(CPT_GID_KEY);
	}

	public void setCptGid(Integer value) {
	 takeStoredValueForKey(value, CPT_GID_KEY);
	}

	public String cptHome() {
	 return (String) storedValueForKey(CPT_HOME_KEY);
	}

	public void setCptHome(String value) {
	 takeStoredValueForKey(value, CPT_HOME_KEY);
	}

	public String cptListeRouge() {
	 return (String) storedValueForKey(CPT_LISTE_ROUGE_KEY);
	}

	public void setCptListeRouge(String value) {
	 takeStoredValueForKey(value, CPT_LISTE_ROUGE_KEY);
	}

	public String cptLogin() {
	 return (String) storedValueForKey(CPT_LOGIN_KEY);
	}

	public void setCptLogin(String value) {
	 takeStoredValueForKey(value, CPT_LOGIN_KEY);
	}

	public Integer cptModificateur() {
	 return (Integer) storedValueForKey(CPT_MODIFICATEUR_KEY);
	}

	public void setCptModificateur(Integer value) {
	 takeStoredValueForKey(value, CPT_MODIFICATEUR_KEY);
	}

	public String cptPasswd() {
	 return (String) storedValueForKey(CPT_PASSWD_KEY);
	}

	public void setCptPasswd(String value) {
	 takeStoredValueForKey(value, CPT_PASSWD_KEY);
	}

	public String cptPasswdClair() {
	 return (String) storedValueForKey(CPT_PASSWD_CLAIR_KEY);
	}

	public void setCptPasswdClair(String value) {
	 takeStoredValueForKey(value, CPT_PASSWD_CLAIR_KEY);
	}

	public String cptPrincipal() {
	 return (String) storedValueForKey(CPT_PRINCIPAL_KEY);
	}

	public void setCptPrincipal(String value) {
	 takeStoredValueForKey(value, CPT_PRINCIPAL_KEY);
	}

	public String cptShell() {
	 return (String) storedValueForKey(CPT_SHELL_KEY);
	}

	public void setCptShell(String value) {
	 takeStoredValueForKey(value, CPT_SHELL_KEY);
	}

	public String cptType() {
	 return (String) storedValueForKey(CPT_TYPE_KEY);
	}

	public void setCptType(String value) {
	 takeStoredValueForKey(value, CPT_TYPE_KEY);
	}

	public Integer cptUid() {
	 return (Integer) storedValueForKey(CPT_UID_KEY);
	}

	public void setCptUid(Integer value) {
	 takeStoredValueForKey(value, CPT_UID_KEY);
	}

	public String cptValide() {
	 return (String) storedValueForKey(CPT_VALIDE_KEY);
	}

	public void setCptValide(String value) {
	 takeStoredValueForKey(value, CPT_VALIDE_KEY);
	}

	public NSTimestamp dCreation() {
	 return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dModification() {
	 return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
	 takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	public Integer persId() {
	 return (Integer) storedValueForKey(PERS_ID_KEY);
	}

	public void setPersId(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_KEY);
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOVlans toVlans() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOVlans)storedValueForKey(TO_VLANS_KEY);
	}

	public void setToVlansRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOVlans value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOVlans oldValue = toVlans();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_VLANS_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_VLANS_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail> toCompteEmails() {
	 return (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail>)storedValueForKey(TO_COMPTE_EMAILS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail> toCompteEmails(EOQualifier qualifier) {
	 return toCompteEmails(qualifier, null);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail> toCompteEmails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		 NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail> results;
			   results = toCompteEmails();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 return results;
	}
	
	public void addToToCompteEmailsRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_COMPTE_EMAILS_KEY);
	}
	
	public void removeFromToCompteEmailsRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTE_EMAILS_KEY);
	}
	
	public org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail createToCompteEmailsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_COMPTE_EMAILS_KEY);
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail) eo;
	}
	
	public void deleteToCompteEmailsRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTE_EMAILS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToCompteEmailsRelationships() {
	 Enumeration<org.cocktail.fwkcktldroitsutils.common.metier.EOCompteEmail> objects = toCompteEmails().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToCompteEmailsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOCompte avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCompte createEOCompte(EOEditingContext editingContext				, String cptCharte
									, Integer cptCreateur
							, String cptCrypte
															, String cptListeRouge
							, String cptLogin
							, Integer cptModificateur
							, String cptPasswd
									, String cptPrincipal
													, String cptValide
							, NSTimestamp dCreation
							, NSTimestamp dModification
							, Integer persId
					, org.cocktail.fwkcktldroitsutils.common.metier.EOVlans toVlans					) {
	 EOCompte eo = (EOCompte) EOUtilities.createAndInsertInstance(editingContext, _EOCompte.ENTITY_NAME);	 
							eo.setCptCharte(cptCharte);
											eo.setCptCreateur(cptCreateur);
									eo.setCptCrypte(cptCrypte);
																	eo.setCptListeRouge(cptListeRouge);
									eo.setCptLogin(cptLogin);
									eo.setCptModificateur(cptModificateur);
									eo.setCptPasswd(cptPasswd);
											eo.setCptPrincipal(cptPrincipal);
															eo.setCptValide(cptValide);
									eo.setDCreation(dCreation);
									eo.setDModification(dModification);
									eo.setPersId(persId);
						 eo.setToVlansRelationship(toVlans);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCompte creerInstance(EOEditingContext editingContext) {
		EOCompte object = (EOCompte)EOUtilities.createAndInsertInstance(editingContext, _EOCompte.ENTITY_NAME);
  		return object;
		}

	

  public EOCompte localInstanceIn(EOEditingContext editingContext) {
    EOCompte localInstance = (EOCompte)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte> eoObjects = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOCompte>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCompte fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCompte fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCompte> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCompte eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCompte)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCompte fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCompte fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCompte> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCompte eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCompte)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCompte fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCompte eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCompte ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCompte fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
