/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldroitsutils.common.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;

/**
 * Permet d'inscrire des logs dans la console. A remplacer par les appels direct a log4j.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 * @deprecated
 */
public class LRLogger {

	private final static Logger logger = Logger.getLogger("CktlLog");

	/** Niveau le plus verbeux. Tous les meessages seront affiches. */
	public static final int LVL_VERBOSE = 40;

	/**
	 * Niveau de debug. Seuls les messages de debugs, informations, warnings et erreurs seront affiches.
	 */
	public static final int LVL_DEBUG = 30;

	/**
	 * Niveau informatif. Seuls les informations, warnings et erreurs seront affiches.
	 */
	public static final int LVL_INFO = 20;

	/** Niveau Warning. Seuls les warning et erreurs seront affiches */
	public static final int LVL_WARNING = 10;

	/** Niveau Error. Seules les erreurs seront affiches */
	public static final int LVL_ERROR = 0;

	/** Niveau None. Aucun message de log ne sera affiche. */
	public static final int LVL_NONE = -1;

	/** Niveau le plus verbeux. Tous les messages seront affiches. */
	public static final String VERBOSE = "VERBOSE";

	/**
	 * Niveau de debug. Seuls les messages de debugs, informations, warnings et erreurs seront affiches.
	 */
	public static final String DEBUG = "DEBUG";

	/**
	 * Niveau informatif. Seuls les informations, warnings et erreurs seront affiches.
	 */
	public static final String INFO = "INFO";

	/** Niveau Warning. Seuls les warning et erreurs seront affiches */
	public static final String WARNING = "WARNING";

	/** Niveau Error. Seules les erreurs seront affiches */
	public static final String ERROR = "ERROR";

	/** Niveau None. Aucun message de log ne sera affiche. */
	public static final String NONE = "NONE";

	protected static String PREFIX_FAILED = ":-(   ATTENTION >>>>";
	protected static String PREFIX_SUCCESS = ":-)   ";

	public static int currentLevel = LVL_INFO;
	public static int lenForSeparator = 80;

	public static String prefix = "";

	protected static boolean includeTimestamp = false;

	/**
	 * Longueur par defaut de l'espace reserve a l'affichage de la cle lorsqu'on affiche une cle/valeur.
	 */
	public static int defaultKeyLength = 35;

	private static String className;

	/**
	 * Cree un titre entoure par des caracteres etoiles.
	 * 
	 * @param str
	 */
	protected static String createBigTitle(final String str) {
		final StringBuffer s = new StringBuffer();
		s.append("\n");
		s.append(createSeparator("*"));
		s.append("\n");
		final int nb = lenForSeparator / 2 - str.length() / 2 - 1;
		if (nb > 0) {
			final String sep = StringCtrl.extendWithChars("", "*", nb, false);
			s.append(sep);
			s.append(str);
			s.append(sep);
		}
		else {
			s.append(str);
		}
		s.append("\n");
		s.append(createSeparator("*"));
		s.append("\n");
		return s.toString();
	}

	/**
	 * Cree un titre et le souligne avec des tirets.
	 * 
	 * @param str
	 * @return
	 */
	protected static String createTitle(final String str) {
		final StringBuffer s = new StringBuffer();
		s.append("\n");
		s.append(StringCtrl.capitalize(str));
		final int nb = str.length();
		final String sep = StringCtrl.extendWithChars("", "-", nb, false);
		s.append("\n");
		s.append(sep);
		s.append("\n");
		return s.toString();
	}

	/**
	 * Ajoute un message precede par ":-(   ATTENTION >>>>".
	 * 
	 * @param str
	 */
	protected static String createFailed(final String str) {
		return PREFIX_FAILED + str;
	}

	/**
	 * Ajoute un message precede d'un smiley :-).
	 * 
	 * @param str
	 */
	protected static String createSuccess(final String str) {
		return PREFIX_SUCCESS + str;
	}

	protected static String createKeyValue(final String key, final Object value) {
		return createKeyValue(key, value, defaultKeyLength);
	}

	/**
	 * Ajoute au log une paire cle = valeur en specifiant le nombre de caracteres a utiliser pour creer la colonne de la cle.
	 * 
	 * @param key
	 * @param value
	 * @param keyLength
	 */
	protected static String createKeyValue(final String key, final Object value, int keyLength) {
		if (key.length() > keyLength) {
			keyLength = key.length();
		}
		return StringCtrl.extendWithChars(key, " ", keyLength, false) + " = " + value;
	}

	protected static String createSeparator(String sep) {
		if (sep == null) {
			sep = "-";
		}
		return StringCtrl.extendWithChars("", sep, lenForSeparator, false);
	}

	/**
	 * Inscrit ou non (suivant le niveau) le message (fait appel a la methode out).
	 * 
	 * @param obj
	 * @param level
	 */
	protected static void log(final Object obj, int level) {
		if (level <= currentLevel) {
			out(obj);
		}
	}

	/**
	 * Inscrit un message de log de niveau Verbose (trace).
	 * 
	 * @param obj
	 */
	public static void verbose(final Object obj) {
		log(obj, LVL_VERBOSE);
	}

	/**
	 * Inscrit un message de log de niveau Verbose (sous forme cle = valeur).
	 * 
	 * @param obj
	 */
	public static void verbose(final String key, final Object obj) {
		logKeyValue(key, obj, LVL_VERBOSE);
	}

	/**
	 * Inscrit un message de log de niveau Debug.
	 * 
	 * @param obj
	 */
	public static void debug(final Object obj) {
		log(obj, LVL_DEBUG);
	}

	/**
	 * Recupere et inscrit ma methode appelante.
	 */
	public static void debugCaller() {
		debug(" >> " + getCallMethod(false));
	}

	/**
	 * Inscrit un message de log de niveau Debug (sous forme cle = valeur).
	 * 
	 * @param obj
	 */
	public static void debug(final String key, final Object obj) {
		logKeyValue(key, obj, LVL_DEBUG);
	}

	/**
	 * Inscrit un message de log de niveau Information.
	 * 
	 * @param obj
	 */
	public static void info(final Object obj) {
		log(obj, LVL_INFO);
	}

	/**
	 * Inscrit un message de log de niveau Information (sous forme cle = valeur).
	 * 
	 * @param obj
	 */
	public static void info(final String key, final Object obj) {
		logKeyValue(key, obj, LVL_INFO);
	}

	/**
	 * Inscrit un message de log de niveau Warning.
	 * 
	 * @param obj
	 */
	public static void warning(final Object obj) {
		log(obj, LVL_WARNING);
	}

	/**
	 * Inscrit un message de log de niveau Error.
	 * 
	 * @param obj
	 */
	public static void error(final Object obj) {
		log(obj, LVL_ERROR);
	}

	/**
	 * Cree un titre entoure par des caracteres etoiles.
	 * 
	 * @param string
	 * @param level Niveau de log pour l'affichage. Si le niveau actuel est inferieur a <i>level</i>, l'inscription dans le log n'est pas effectuee.
	 */
	public static void logBigTitle(final String string, int level) {
		if (level <= currentLevel) {
			out(createBigTitle(string));
		}
	}

	/**
	 * Cree un titre et le souligne avec des tirets.
	 * 
	 * @param string
	 * @param level Niveau de log pour l'affichage. Si le niveau actuel est inferieur a <i>level</i>, l'inscription dans le log n'est pas effectuee.
	 */
	public static void logTitle(final String string, int level) {
		if (level <= currentLevel) {
			out(createTitle(string));
		}
	}

	/**
	 * Ajoute au log une paire cle = valeur en specifiant le nombre de caracteres a utiliser pour creer la colonne de la cle.
	 * 
	 * @param key
	 * @param value
	 * @param level Niveau de log pour l'affichage. Si le niveau actuel est inferieur a <i>level</i>, l'inscription dans le log n'est pas effectuee.
	 */
	public static void logKeyValue(final String key, final Object value, int level) {
		if (level <= currentLevel) {
			out(createKeyValue(key, value));
		}
	}

	/**
	 * Ajoute un message precede d'un smiley :-).
	 * 
	 * @param string
	 * @param level Niveau de log pour l'affichage. Si le niveau actuel est inferieur a <i>level</i>, l'inscription dans le log n'est pas effectuee.
	 */
	public static void logSuccess(final String string, int level) {
		if (level <= currentLevel) {
			out(createSuccess(string));
		}
	}

	public static void logFailed(final String string, int level) {
		if (level <= currentLevel) {
			out(createFailed(string));
		}
	}

	/**
	 * @param obj
	 */
	protected synchronized static void out(final Object obj) {
		if (obj instanceof EOEditingContext) {
			out((EOEditingContext) obj);
		}
		if (obj instanceof NSArray) {
			out((NSArray) obj);
		}
		else if (obj instanceof EOEnterpriseObject) {
			out((EOEnterpriseObject) obj);
		}
		else if (obj instanceof Vector) {
			out((Vector) obj);
		}
		else if (obj instanceof Map) {
			out((Map) obj);
		}
		else {

			StringBuffer sb = new StringBuffer("");
			if (includeTimestamp) {
				sb.append("[" + DateCtrl.currentDateTimeString() + "] ");
			}
			if (prefix != null) {
				sb.append(prefix);
			}
			if (obj == null) {
				sb.append("null");
			}
			else {
				sb.append(obj);
			}
			write(sb.toString());
		}
		//write("");
	}

	/**
	 * Methode finale qui ecrit dans la console (via System.out.println). A deriver pour changer la destination de l'ecriture.
	 * 
	 * @param obj
	 */
	protected synchronized static void write(final String obj) {
		myCktlLogWriter.appendln(obj);
		// System.out.println(obj);
	}

	protected synchronized static void out(final Vector objects) {
		Iterator iter = objects.iterator();
		int i = 0;
		while (iter.hasNext()) {
			out(new String("" + i + "-->") + iter.next());
			i++;
		}
		write("");
	}

	protected synchronized static void out(final Map objects) {
		final Iterator iter = objects.keySet().iterator();
		int i = 0;
		while (iter.hasNext()) {
			final Object obj = iter.next();
			out("" + obj + "-->" + objects.get(obj));
			i++;
		}
		write("");
	}

	protected synchronized static void out(final NSArray objects) {
		final Enumeration enumeration = objects.objectEnumerator();
		int i = 0;

		while (enumeration.hasMoreElements()) {
			final Object obj = enumeration.nextElement();
			out("--- " + i + " --- " + obj.getClass().getName());
			out(obj);
			out("----------------------");
			i++;
		}
		write("");
	}

	protected synchronized static void out(Exception e) {
		out("EXCEPTION : ");
		e.printStackTrace();
	}

	protected synchronized static void out(final EOEditingContext object) {
		if (object != null) {
			write("****************************************************************");
			write("Details des objets de l'editingContext " + object.toString());
			write("");
			write("------INSERTEDOBJECTS in " + object.toString() + "  : " + object.insertedObjects().count() + " objets");
			out(object.insertedObjects());
			write("");
			write("------UPDATEDOBJECTS  in " + object.toString() + "  : " + object.updatedObjects().count() + " objets");
			out(object.updatedObjects());
			write("");
			write("------DELETEDOBJECTS  in " + object.toString() + "  : " + object.deletedObjects().count() + " objets");
			out(object.deletedObjects());
			write("");
			write("****************************************************************");
			write("");

		}
		out("");
	}

	protected synchronized static void out(final EOEnterpriseObject object) {
		if (object != null) {
			out(object.getClass().getName() + " - ID = " + (object.editingContext() != null ? object.editingContext().globalIDForObject(object).toString() + " - EC = " + object.editingContext().toString() : "n/a"));

			final Iterator iter = object.attributeKeys().vector().iterator();
			while (iter.hasNext()) {
				final String obj = (String) iter.next();
				out("" + obj + "-->" + object.valueForKey(obj));
			}

			Iterator iter2 = object.toOneRelationshipKeys().vector().iterator();
			while (iter2.hasNext()) {
				String obj = (String) iter2.next();
				out("" + obj + "-->" + object.valueForKey(obj));
			}
			Iterator iter3 = object.toManyRelationshipKeys().vector().iterator();
			while (iter3.hasNext()) {
				String obj = (String) iter3.next();
				out("" + obj + "-->" + object.valueForKey(obj));
			}
		}
		out("");
	}

	/**
	 * @return Le niveau de log actif.
	 * @see CktlLogger#LVL_VERBOSE
	 * @see CktlLogger#LVL_DEBUG
	 * @see CktlLogger#LVL_INFO
	 * @see CktlLogger#LVL_WARNING
	 * @see CktlLogger#LVL_ERROR
	 * @see CktlLogger#LVL_NONE *
	 */
	public static final int getCurrentLevel() {
		return currentLevel;
	}

	/**
	 * Défini le niveau de log actif.
	 * 
	 * @see CktlLogger#LVL_VERBOSE
	 * @see CktlLogger#LVL_DEBUG
	 * @see CktlLogger#LVL_INFO
	 * @see CktlLogger#LVL_WARNING
	 * @see CktlLogger#LVL_ERROR
	 * @see CktlLogger#LVL_NONE
	 * @param currentLevel
	 */
	public static final void setCurrentLevel(int currentLevel) {
		LRLogger.currentLevel = currentLevel;
	}

	/**
	 * Défini le niveau de log actif.
	 * 
	 * @see CktlLogger#VERBOSE
	 * @see CktlLogger#DEBUG
	 * @see CktlLogger#INFO
	 * @see CktlLogger#WARNING
	 * @see CktlLogger#ERROR
	 * @see CktlLogger#NONE
	 * @param currentLevel Le niveau de log.
	 */
	public static final void setCurrentLevel(String currentLevel) {
		int lvl = LVL_INFO;
		if (VERBOSE.equals(currentLevel)) {
			lvl = LVL_VERBOSE;
		}
		else if (DEBUG.equals(currentLevel)) {
			lvl = LVL_DEBUG;
		}
		else if (INFO.equals(currentLevel)) {
			lvl = LVL_INFO;
		}
		else if (WARNING.equals(currentLevel)) {
			lvl = LVL_WARNING;
		}
		else if (ERROR.equals(currentLevel)) {
			lvl = LVL_ERROR;
		}
		else if (NONE.equals(currentLevel)) {
			lvl = LVL_NONE;
		}
		setCurrentLevel(lvl);
	}

	// *************************************************
	// Methode pour compatibilite avec CktlLog.
	// *************************************************

	/**
	 * Definit le prefix pour les messages affiches avec la methode <code>trace</code>. Attention, si le prefix doit etre separe du message par un
	 * espace, celui-la doit etre inclu dans <code>prefix</code>.
	 * 
	 * @deprecated
	 */
	public static void setTracePrefix(String prefix) {
		// verbosePrefix = prefix;
	}

	/**
	 * Affiche un message sur la sortie standard sans aucun pre-formatage.
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est <code>LEVEL_BASIC</code>.
	 * 
	 * @deprecated Utilisez info a la place.
	 */
	public static void rawLog(String message) {
		info(message);
		// printMessage(message, LEVEL_BASIC);
	}

	/**
	 * <p>
	 * La valeur <code>level</code> indique le niveau de la verbosite dans lequel le message doit etre affiche.
	 * 
	 * @deprecated
	 */
	public static void rawLog(String message, int level) {
		printMessage(message, level);
	}

	/**
	 * Affiche un message sur la sortie standard. Le message sera precede par le temps de l'appel de cette methode.
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est <code>LEVEL_BASIC</code>.
	 * 
	 * @deprecated
	 */
	public static void log(String message) {
		log(message, LVL_DEBUG);
	}

	/**
	 * Affiche un message et un commentaire sur la sortie standard. Le message sera precede par le temps de l'appel de cette methode. Le commentaire
	 * sera affiche sur une nouvelle ligne et precede par le symbole "&gt;&gt; ".
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est <code>LEVEL_BASIC</code>.
	 * 
	 * @deprecated
	 */
	public static void log(String message, String comment) {
		// prinMessage fait la meme chose, mais on le refait ici pour
		// eviter de faire les calculs inutils
		if (currentLevel < LVL_INFO)
			return;
		log("[" + DateCtrl.currentDateTimeString() + "] " + message, LVL_INFO);
		if (comment != null)
			log(StringCtrl.trimText(StringCtrl.quoteText(comment, ">> ")), LVL_INFO);

		// // prinMessage fait la meme chose, mais on le refait ici pour
		// // eviter de faire les calculs inutils
		// if (currentLevel < LEVEL_BASIC) return;
		// printMessage("["+DateCtrl.currentDateTimeString()+"] "+message,
		// LEVEL_BASIC);
		// if (comment != null)
		// printMessage(StringCtrl.trimText(StringCtrl.quoteText(comment,
		// ">> ")), LEVEL_BASIC);
	}

	/**
	 * Affiche un message sur la sortie standard. Le message sera precede par le nom de la classe et de la methode qui a appele la methode
	 * <code>trace</code>.
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est <code>LEVEL_DEBUG</code>.
	 * 
	 * @deprecated Utilisez debug a la place
	 */
	public static void trace(String message) {
		trace(message, false);
	}

	/**
	 * Affiche un message sur la sortie standard. Le message sera precede par le nom de la classe et de la methode qui a appele la methode
	 * <code>trace</code>. Si <code>includeOrigin</code> est <i>true</i>, alors le nom de la methode appelant cette-derniere sera egalement incluse.
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est <code>LEVEL_DEBUG</code>.
	 * 
	 * @deprecated Utilisez la methode debug a la place.
	 */
	public static void trace(String message, boolean withOrigin) {
		// prinMessage fait la meme chose, mais on le refait ici pour
		// eviter de faire les calculs inutils
		if (currentLevel < LVL_DEBUG)
			return;
		StringBuffer msb = new StringBuffer();
		msb.append(getCallMethod(withOrigin));
		if (message != null)
			msb.append(" : ").append(message);
		printMessage(msb.toString(), LVL_DEBUG);
		// if (currentLevel < LEVEL_DEBUG) return;
		// StringBuffer msb = new StringBuffer();
		// msb.append(tracePrefix).append(getCallMethod(withOrigin));
		// if (message != null) msb.append(" : ").append(message);
		// printMessage(msb.toString(), LEVEL_DEBUG);
	}

	/**
	 * Affiche la valeur <code>varValue</code> associee a la valeur <code>varName</code> sur la sortie standard. Affichage a la forme "
	 * <code>variable&nbsp;=&nbsp;valeur</code>".
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est <code>LEVEL_DEBUG</code>.
	 * 
	 * @deprecated Utilisez la methode debug a la place.
	 */
	public static void trace(String varName, Object varValue) {
		// trace(varName+" = "+varValue.toString(), false);
		debug(varName, varValue);
	}

	/**
	 * Affiche un message sur la sortie standard sans aucune modification de son format.
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est <code>LEVEL_DEBUG</code>. Utilisez la methode debug a la place.
	 * 
	 * @deprecated
	 */
	public static void rawTrace(String message) {
		// rawLog(message, LEVEL_DEBUG);
		log(message, LVL_DEBUG);
	}

	/**
	 * Retourne le message d'erreur de l'exception ou de l'erreur <code>ex</code>. Le message sera suivie par le nom de la classe de l'exception
	 * <code>ex</code>.
	 * <p>
	 * Dans cerains cas une exception ne contient aucun message d'erreur. Dans ce cas, le nom de la classe de l'exception ex seul est retourne.
	 * </p>
	 */
	public static String getMessageForException(Throwable ex) {
		String message = null;
		if (ex != null) {
			message = ex.getClass().getName();
			if (ex.getMessage() != null)
				message += " : " + ex.getMessage();
		}
		return message;
	}

	/**
	 * Retourne une chaine indiquant une classe et une methode de l'enchainement des appels des methodes. Toutes les appels relatives a la classe
	 * <code>CktlLog</code> sont ignores. On retourne le nom de la derniere methode qui a fait appel a une methode de <code>CktlLog</code>. Si
	 * <code>withOrigin</code> est <i>true</i> alors le nom de la methode appelant cette derniere sera egalement inclu.
	 * <p>
	 * Cette methode retourne <i>null</i> si le nom de la methode ne peut pas etre detecte.
	 */
	protected static String getCallMethod(boolean withOrigin) {
		StringWriter sw = new StringWriter();
		(new Throwable()).printStackTrace(new PrintWriter(sw));
		String stack = sw.toString();
		// On detecte le dernier appel a "CktlLog"
		int startPos = stack.lastIndexOf(className()) + className().length();
		// De la, on trouve la methode appelante
		StringBuffer methodName = new StringBuffer();
		methodName.append(extractMethodName(stack, startPos));
		if (withOrigin) {
			methodName.append("\n   ");
			methodName.append(extractMethodName(stack, startPos + 3 + methodName.length()));
		}
		return methodName.toString();
	}

	/**
	 * Retourne le nom de la methode qui se situe dans la position <code>startPos</code> dans l'arborescence des appels des mesthodes
	 * <code>stack</code> ("stack trace"). Retourne null si aucun nom de la metode ne peut pas etre trouvee dans la position indiquee.
	 */
	private static String extractMethodName(String stack, int startPos) {
		startPos = stack.indexOf("at ", startPos) + 3;
		int endPos = stack.indexOf(")", startPos) + 1;
		if (endPos <= startPos)
			return null;
		return stack.substring(startPos, endPos);
	}

	/**
	 * Retourne le nom de la classe dans laquelle se trouve cette methode statique. Cette methode permet de recuperer le nom de la classe meme si son
	 * nom est change ou si une autre classe herite de celle-ci.
	 * <p>
	 * Cette methode permettra d'obtenir le nom de la classe en cours dans le context statique, car les appels aux methodes de type
	 * <code>getClass().getName()</code> ne peuvent pas etre faits dans ce cas.
	 * </p>
	 * <p>
	 * L'implementation de cette methode utilise une classe <i>anonyme</i> interne qui herite de la classe <code>SecurityManager</code> et fait appel
	 * a sa methode <code>getClassContext</code>.
	 * </p>
	 * <p>
	 * Pour assurer le fonctionnement correct de la methode, elle ne doit pas etre redefinie dans le sous-classes de <code>CktlLog</code>. Pour cette
	 * raison, la methode est declaree <i>final</i>.
	 * </p>
	 */
	public static final String className() {
		if (className == null) {
			// On va utiliser la classe anonyme qui herite de SecurityManager
			className = (new SecurityManager() {
				// La nouvelle methode de la classe anonyme. Elle retourne le
				// nom
				// de la classe ("CktlLog").
				public String getClassName() {
					// [0] - la class anonyme, [1] - la classe englobante
					return getClassContext()[1].getName();
				}
			}).getClassName();
		}
		return className;
	}

	// /**
	// * Retourne le nom de la classe dans laquelle se trouve cette methode
	// * statique. Cette methode permet de recuperer le nom de la classe meme
	// * si son nom est change ou si une autre classe herite de celle-ci.
	// */
	// public static final String className() {
	// if (className == null) className = (new
	// CurrentClassGetter()).getClassName();
	// return className;
	// }
	//  
	// /**
	// * Cette classe permettra d'obtenir le nom de la classe en cours dans
	// * les methodes statiques. On ne peut plus faire l'appel a la methode
	// * <code>getClass().getName()</code> dans le context statique.
	// *
	// * <p>Cette methode permettra garder le meme code meme si le nom
	// * de la classe <code>CktlLog</code> est modifie (modification du code
	// * ou l'heritage).
	// */
	// private static class CurrentClassGetter extends SecurityManager {
	// public String getClassName() {
	// return getClassContext()[1].getName();
	// }
	// }

	/**
	 * Interface a definir pour effectuer des logs personnalises. Appeler ensuite
	 * {@link #setMyCktlLogWriter(org.cocktail.fwkcktlwebapp.common.CktlLog.ICktlLogWriter)} pour parametrer {@link CktlLog} avec ce nouveau writer
	 */
	public static interface ICktlLogWriter {
		void appendln(String message);
	}

	/**
	 * Le CktlLogWriter par defaut qui utilise NSLog.appendln
	 */
	private static class DefaultCktlLogWriter implements ICktlLogWriter {
		public void appendln(String message) {
			// NSLog.out.appendln(message);
			//System.out.println(message);
			logger.info(message);

		}
	}

	private static ICktlLogWriter myCktlLogWriter = new DefaultCktlLogWriter();

	/**
	 * Redefinir le writter de log par defaut (si vous voulez que la sortie ne soit pas System.out.println)
	 */
	public static void setMyCktlLogWriter(ICktlLogWriter aCktlLogWriter) {
		myCktlLogWriter = aCktlLogWriter;
	}

	/**
	 * Effectue l'affichage du message sur la sortie.
	 * 
	 * @deprecated
	 */
	private synchronized static void printMessage(String message, int level) {
		log(message, level);
		// if (currentLevel >= level) {
		// myCktlLogWriter.appendln(message);
		// }
	}

	public static boolean isIncludeTimestamp() {
		return includeTimestamp;
	}

	/**
	 * Indique au moteur de log d'affichier la date
	 * 
	 * @param includeTimestamp
	 */
	public static void setIncludeTimestamp(boolean includeTimestamp) {
		LRLogger.includeTimestamp = includeTimestamp;
	}

	/**
	 * @deprecated
	 */
	public static final int LEVEL_SILENT = LVL_NONE;

	/**
	 * La constante de niveau de verbosite. Elle indique les appels aux methodes "<code>log</code>
	 * " uniquement seront traites. Les appels aux methodes " <code>trace</code>" sont ignores (les messages "debug").
	 * 
	 * @deprecated
	 * @see LVL_INFO
	 */
	public static final int LEVEL_BASIC = LVL_INFO;

	/**
	 * La constante de niveau de verbosite. Elle indique que les appels a tous les methodes seront traites, y compris les appels aux "
	 * <code>trace</code> " (les messages "debug).
	 * 
	 * @deprecated
	 */
	public static final int LEVEL_DEBUG = LVL_DEBUG;

}
