/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldroitsutils.common.util;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSSelector;

/**
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class MyEOQualifierCtrl {

	/**
	 * @param keyName
	 * @param selector
	 * @param values
	 * @return Un tableau de EOKeyValueQualifier (un pour chaque element du tableau values). 
	 */
	public static NSArray arrayOfKeyValueQualifierForValues(String keyName, NSSelector selector, NSArray values) {
		return arrayOfKeyValueQualifierForValues(keyName, selector, values, null, null);
	}
	
	
	/**
	 * @param keyName
	 * @param selector
	 * @param values
	 * @param prefixForValue prefixe facultatif a rajouter a la valeur (par exemple pour un like)
	 * @param suffixForValue suffixe facultatif a rajouter a la valeur (par exemple pour un like)
	 * @return Un tableau de EOKeyValueQualifier (un pour chaque element du tableau values).
	 */
	public static NSArray arrayOfKeyValueQualifierForValues(String keyName, NSSelector selector, NSArray values, String prefixForValue, String suffixForValue) {
		NSMutableArray quals = new NSMutableArray();
		if (values != null) {
			for (int i = 0; i < values.count(); i++) {
				Object val = values.objectAtIndex(i);
				if (prefixForValue != null) {
					val = prefixForValue + val;
				}
				if (suffixForValue != null) {
					val = val + suffixForValue;
				}
				quals.addObject(new EOKeyValueQualifier(keyName, selector, val));
			}
		}
		return quals.immutableClone();
	}
	
	
	
	
}
