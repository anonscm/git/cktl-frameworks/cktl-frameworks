/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldroitsutils.common.util;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Vector;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;





/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 *
 */
public abstract class AUtils {


    private static final DateFormat FORMAT_YYYY = new SimpleDateFormat("yyyy");


	public static EOEnterpriseObject instanceForEntity(EOEditingContext ec, String entity) {
        EOClassDescription description = (EOClassDescription) EOClassDescription.classDescriptionForEntityName(entity);
        if (description == null) {
            throw new RuntimeException("Impossible de recuperer la description de l'entite  \"" + entity + "\" ");
        }
        EOEnterpriseObject object = description.createInstanceWithEditingContext(ec, null);
        return object;
    }

 
    public static BigDecimal computeSumForKeyBigDecimal(NSArray eo, String keyBigDecimal) {
		if (eo==null || eo.count()==0)
			return new BigDecimal(0);
    	return (BigDecimal)eo.valueForKeyPath("@sum."+keyBigDecimal);
    }

    public static boolean inferieur(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = true;

        if (resultat == 0)
            reponse = false;

        if (resultat == 1)
            reponse = false;

        return reponse;
    }

    public static boolean inferieurOuEgal(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = true;

        if (resultat == 0)
            reponse = true;

        if (resultat == 1)
            reponse = false;

        return reponse;
    }

    public static boolean superieur(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = false;

        if (resultat == 0)
            reponse = false;

        if (resultat == 1)
            reponse = true;

        return reponse;
    }

    public static boolean superieurOuEgal(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = false;

        if (resultat == 0)
            reponse = true;

        if (resultat == 1)
            reponse = true;

        return reponse;
    }

    public static boolean egal(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = false;

        if (resultat == 0)
            reponse = true;

        if (resultat == 1)
            reponse = false;

        return reponse;
    }

    public static boolean different(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = true;

        if (resultat == 0)
            reponse = false;

        if (resultat == 1)
            reponse = true;

        return reponse;
    }

    /**
     * @return Un gregorianCalendar initialise e la date du jour (sans les
     *         heures/minutes/etc). Utilisez getToday().getTime() pour recuperer
     *         la date du jour nettoyee des secondes sous forme de Date.
     */
    private final static GregorianCalendar getToday() {
        final GregorianCalendar gc = new GregorianCalendar();
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.SECOND, 0);
        gc.set(Calendar.MILLISECOND, 0);
        return gc;
    }

    /**
     * @return la date en cours (sans les heures, minutes, secondes, etc.) 
     */
    public final static NSTimestamp getDateJour() {
        return new NSTimestamp(getToday().getTime());
    }

    /**
     * Calcule la somme de toutes les valeurs contenues dans un champ des
     * EOEnterpriseObjects contenues dans le tableau.
     * 
     * @param array
     *            Tableau d'EOEnterpriseObjects
     * @param keyName
     *            Nom de l'attribut qui contient les valeurs e sommer (le champ
     *            doit etre un BigDecimal)
     * @return
     */
    public static final BigDecimal calcSommeOfBigDecimals(final NSArray array, final String keyName) {
        return calcSommeOfBigDecimals(array.vector(), keyName);
    }

    public static final BigDecimal calcSommeOfBigDecimals(final Vector array, final String keyName) {
        BigDecimal res = new BigDecimal(0).setScale(2);
        Iterator iter = array.iterator();
        while (iter.hasNext()) {
            EOEnterpriseObject element = (EOEnterpriseObject) iter.next();
            res = res.add((BigDecimal) element.valueForKey(keyName));
        }
        return res;
    }

    /**
     * Methode qui <b>tente</b> de contourner le bug EOF qui se produit lors
     * d'un saveChanges avec l'erreur "reentered responseToMessage()".<br>
     * <b>Il faut appeler cette methode avant de creer un descendant
     * d'EOCustomObject, donc bien avant le saveChanges()</b><br>
     * 
     * Le principe est d'appeler la methode
     * EOClassDescription.classDescriptionForEntityName("A") pour chaque
     * relation de l'objet qu'on va creer. Il faut appeler cette methode avant
     * de creer un objet. Par exemple dans le cas d'un objet Facture qui a des
     * objets Ligne, appeler
     * EOClassDescription.classDescriptionForEntityName("Facture") avant de
     * creer un objet Ligne. Repeter l'operation pour toutes les relations de
     * l'objet.
     * 
     * @param list
     *            Liste de String identifiant une entite du modele.
     * @see "http://www.omnigroup.com/mailman/archive/webobjects-dev/2002-May/023698.html"
     */
    public static final void fixWoBug_responseToMessage(final String[] list) {
        for (int i = 0; i < list.length; i++) {
            EOClassDescription.classDescriptionForEntityName(list[i]);
        }
    }
    

    


//	
//	/**
//	 * Effectue un trim sur tous les champs de type chaine de l'objet passé en parametre.
//	 * @param obj
//	 */
//	public static void trimAllString(EOEnterpriseObject obj) {
//		NSArray atts = obj.attributeKeys();
//		for (int i = 0; i < atts.count(); i++) {
//			String array_element = (String) atts.objectAtIndex(i);
//			if (obj.valueForKey(array_element)!=null && obj.valueForKey(array_element) instanceof String) {
//				obj.takeValueForKey(((String)obj.valueForKey(array_element)).trim(),array_element);
//			}
//		}
//	}
//	
    
	public static NSTimestamp now() {
		return new NSTimestamp();
	}
	
	/**
	 * Retourne une chaine de charactère sans les charactères non numeriques, ou une chaine vide si s=null.
	 * @param s
	 * @return 
	 */
	public static String keepDigits(String s) {
		StringBuffer sb = new StringBuffer();
		s = MyStringCtrl.normalize(s);
		for (int i = 0; i < s.length(); i++) {
			if (MyStringCtrl.isBasicDigit(s.charAt(i))) {
				sb.append(s.charAt(i));
			}
		}
		return sb.toString();
	}


	public static String currentExercice() {
		return FORMAT_YYYY.format(now());
	}
		
	
	
}
