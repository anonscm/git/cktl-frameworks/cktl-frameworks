/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldroitsutils.common.finders;

import org.cocktail.fwkcktldroitsutils.common.metier.EOCompte;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderPersonne {

	/**
	 * trouve un personne d'apres son persid
	 */
	public static EOPersonne findPersonneForPersId(EOEditingContext ec, Number persid) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPersonne.PERS_ID_KEY + " = %@", new NSArray(persid));
		EOFetchSpecification fs = new EOFetchSpecification(EOPersonne.ENTITY_NAME, qual, null);

		try {
			return (EOPersonne) ec.objectsWithFetchSpecification(fs).lastObject();
		} catch (Exception e) {
			return null;
		}
	}

	//	public static EOPersonne getPersonneForLoginPass(EOEditingContext ec, String login, String pass) {
	//		NSMutableArray quals = new NSMutableArray();
	//		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartCompte.COMPTE_KEY + "." + EOCompte.CPT_LOGIN_KEY + " = %@", new NSArray(login)));
	//		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartCompte.COMPTE_KEY + "." + EOCompte.CPT_PASSWD_KEY + " = %@", new NSArray(pass)));
	//
	//		EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EORepartCompte.COMPTE_KEY + "." + EOCompte.CPT_VLAN_KEY, EOSortOrdering.CompareAscending);
	//		EOFetchSpecification fs = new EOFetchSpecification(EORepartCompte.ENTITY_NAME, new EOAndQualifier(quals), new NSArray(new Object[] {
	//			sort1
	//		}));
	//
	//		NSArray res = ec.objectsWithFetchSpecification(fs);
	//		if (res.count() > 0) {
	//			return ((EORepartCompte) res.objectAtIndex(0)).personne();
	//		}
	//		return null;
	//	}
	//
	//	public static EOPersonne getPersonneForLogin(EOEditingContext ec, String login) {
	//		NSMutableArray quals = new NSMutableArray();
	//		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartCompte.COMPTE_KEY + "." + EOCompte.CPT_LOGIN_KEY + " = %@", new NSArray(login)));
	//
	//		EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EORepartCompte.COMPTE_KEY + "." + EOCompte.CPT_VLAN_KEY, EOSortOrdering.CompareAscending);
	//		EOFetchSpecification fs = new EOFetchSpecification(EORepartCompte.ENTITY_NAME, new EOAndQualifier(quals), new NSArray(new Object[] {
	//			sort1
	//		}));
	//
	//		NSArray res = ec.objectsWithFetchSpecification(fs);
	//		if (res.count() > 0) {
	//			return ((EORepartCompte) res.objectAtIndex(0)).personne();
	//		}
	//		return null;
	//	}

	public static EOPersonne fetchPersonneForLoginPass(EOEditingContext ec, String login, String pass) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOPersonne.TO_COMPTES_KEY + "." + EOCompte.CPT_LOGIN_KEY, EOQualifier.QualifierOperatorEqual, login));
		quals.addObject(new EOKeyValueQualifier(EOPersonne.TO_COMPTES_KEY + "." + EOCompte.CPT_PASSWD_KEY, EOQualifier.QualifierOperatorEqual, pass));

		NSArray res = EOPersonne.fetchAll(ec, new EOAndQualifier(quals), null);
		if (res.count() > 0) {
			return ((EOPersonne) res.objectAtIndex(0));
		}

		//EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOCompte.CPT_VLAN_KEY, EOSortOrdering.CompareAscending);
		return null;
	}

	public static EOPersonne fetchPersonneForLogin(EOEditingContext ec, String login) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOPersonne.TO_COMPTES_KEY + "." + EOCompte.CPT_LOGIN_KEY, EOQualifier.QualifierOperatorEqual, login));
		//    	quals.addObject(new EOKeyValueQualifier(EOCompte.CPT_PASSWD_KEY, EOQualifier.QualifierOperatorEqual, pass));              

		//EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOCompte.CPT_VLAN_KEY, EOSortOrdering.CompareAscending);
		NSArray res = EOPersonne.fetchAll(ec, new EOAndQualifier(quals), null);
		if (res.count() > 0) {
			return ((EOPersonne) res.objectAtIndex(0));
		}
		return null;
	}

}
