/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldroitsutils.common.finders;

/**
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class FinderException extends Exception {

	   public FinderException() {
	        super();
	    }

	    public FinderException(String message) {
	        super(message);
	    }

	    public FinderException(Throwable cause) {
	        super(cause);
	    }

	    public FinderException(String message, Throwable cause) {
	        super(message, cause);
	    }
	    
	    /**
	     * 
	     * @param message message de l'exception.  
	     * @param params Tableau de String contenant les valeurs. La premiere valeur remplacera %0 dans message, la deuxieme remplacera %1, etc. 
	     */
	    public FinderException(String message, String[] params) {
	        super(prepareMsg(message, params));
	        
	    }
	    
	    public FinderException(String message, String[] params, Throwable cause) {
	        super(prepareMsg(message, params), cause);
	        
	    }    
	    
	    
	    public static final String prepareMsg(String message, String[] params) {
	        String msg = message;
	        for (int i = 0; i < params.length; i++) {
	            String string = params[i];
	            message = message.replaceAll("%"+i, string);
	        }
	        return msg;
	    }

}
