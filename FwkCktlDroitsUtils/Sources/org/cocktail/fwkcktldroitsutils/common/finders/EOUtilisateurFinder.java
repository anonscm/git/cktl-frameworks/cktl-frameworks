/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldroitsutils.common.finders;

import org.cocktail.fwkcktldroitsutils.common.metier.EOExercice;
import org.cocktail.fwkcktldroitsutils.common.metier.EOFonction;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktldroitsutils.common.metier.EOTypeApplication;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonctionExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EOUtilisateurFinder extends AFinder {

	private static final String UTILISATEURFONCTION_EST_NUL = "utilisateurfonction est nul.";
	public static final String MSG_USER_UNKNOWN_NOIND = "Impossible de recuperer l'utilisateur correspondant au no individu= %0";
	public static final String MSG_USER_UNKNOWN_PERSID = "Impossible de recuperer l'utilisateur correspondant au pers_id= %0";
	public static final String MSG_USER_UNKNOWN_UTL_ORDRE = "Impossible de recuperer l'utilisateur correspondant a utl_ordre= %0";
	public static final String MSG_USER_UNKNOWN_NOM_PRENOM = "Impossible de recuperer l'utilisateur correspondant aux nom et prenom= %0, %1";

	public static final EOQualifier qualPersId(final Integer persId) {
		return EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.TO_PERSONNE_KEY + QUAL_POINT + EOPersonne.PERS_ID_KEY + QUAL_EQUALS, new NSArray(new Object[] { persId }));
	}

	public static final EOQualifier qualNoIndividu(final Integer noIndividu) {
		return EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.TO_PERSONNE_KEY + QUAL_POINT + EOPersonne.PERS_ORDRE_KEY + QUAL_EQUALS + " and " + EOUtilisateur.TO_PERSONNE_KEY + QUAL_POINT + EOPersonne.PERS_TYPE_KEY + "<>", new NSArray(new Object[] { noIndividu, EOPersonne.PERS_TYPE_STR }));
	}

	public static final EOQualifier qualUtlOrdre(final Integer utlOrdre) {
		return EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.UTL_ORDRE_KEY + QUAL_EQUALS, new NSArray(new Object[] { utlOrdre }));
	}

	public static final EOQualifier qualUtilisateurValide() {
		return EOUtilisateur.QUALIFIER_UTILISATEUR_VALIDE;
	}

	public static final EOQualifier qualUtilisateurNonValide() {
		return EOUtilisateur.QUALIFIER_UTILISATEUR_NON_VALIDE;
	}

	/**
	 * Renvoie un utilisateur en fonction de son persId.
	 * 
	 * @param ec
	 * @param noIndividu
	 */
	public static EOUtilisateur fecthUtilisateurByPersId(final EOEditingContext ec, final Integer persId) {
		if (persId == null) {
			return null;
		}
		final EOEnterpriseObject tmp = fetchObject(ec, EOUtilisateur.ENTITY_NAME, new EOAndQualifier(new NSArray(new Object[] { qualPersId(persId), qualUtilisateurValide() })), null, true);
		return (EOUtilisateur) tmp;
	}

	public static EOUtilisateur fecthUtilisateurByUtlOrdre(final EOEditingContext ec, final Integer utlOrdre) {
		final EOEnterpriseObject tmp = fetchObject(ec, EOUtilisateur.ENTITY_NAME, new EOAndQualifier(new NSArray(new Object[] { qualUtlOrdre(utlOrdre), qualUtilisateurValide() })), null, true);
		return (EOUtilisateur) tmp;
	}

	public static EOUtilisateur fecthUtilisateurByNoIndividu(final EOEditingContext ec, final Integer noIndividu) {
		final EOEnterpriseObject tmp = fetchObject(ec, EOUtilisateur.ENTITY_NAME, new EOAndQualifier(new NSArray(new Object[] { qualNoIndividu(noIndividu), qualUtilisateurValide() })), null, true);
		return (EOUtilisateur) tmp;
	}

	//	public static NSArray getMyAppUtilisateurFonctionsForUtilisateur(final EOEditingContext ec, final EOUtilisateur utilisateur) {
	//	    if (utilisateur!=null) {
	//            return EOQualifier.filteredArrayWithQualifier(utilisateur.utilisateurFonctions(), qualFonctionsMyTypeApplication());
	//        }
	//        return new NSArray();
	//        
	//	}

	public static EOUtilisateurFonction fetchUtilisateurFonctionForUtilisateurAndFonction(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOFonction fonction) {
		if (utilisateur != null) {
			return (EOUtilisateurFonction) fetchObject(ec, EOUtilisateurFonction.ENTITY_NAME, EOUtilisateurFonction.TO_UTILISATEUR_KEY + AFinder.QUAL_EQUALS + AFinder.QUAL_AND + EOUtilisateurFonction.TO_FONCTION_KEY + "=%@", new NSArray(new Object[] { utilisateur, fonction }), null, true);
		}
		return null;
	}

	/**
	 * Renvoie la liste des utilisateurs disposant d'une fonction particulière
	 * 
	 * @param ec
	 * @param fonction
	 * @return
	 */
	public static NSArray fetchUtilisateursForFonction(final EOEditingContext ec, final String fonIdInterne) {
		if (fonIdInterne != null) {
			return fetchArray(ec, EOUtilisateur.ENTITY_NAME, EOUtilisateur.TO_UTILISATEUR_FONCTIONS_KEY + AFinder.QUAL_POINT + EOUtilisateurFonction.TO_FONCTION_KEY + AFinder.QUAL_POINT + EOFonction.FON_ID_INTERNE_KEY + AFinder.QUAL_EQUALS, new NSArray(new Object[] { fonIdInterne }), null, false);
		}
		return new NSArray();
	}

	public static EOUtilisateurFonction getUtilisateurFonctionForUtilisateurAndFonction(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOFonction fonction) {
		final NSArray tmp;
		if (fonction == null || utilisateur == null) {
			return null;
		}

		NSArray auts = utilisateur.toUtilisateurFonctions();
		if (auts == null) {
			auts = new NSArray();
		}

		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.TO_FONCTION_KEY + AFinder.QUAL_EQUALS, new NSArray(new Object[] { fonction }));
		tmp = EOQualifier.filteredArrayWithQualifier(auts, qual);

		if (tmp.count() == 0) {
			return null;
		}
		return (EOUtilisateurFonction) tmp.objectAtIndex(0);
	}

	public static EOUtilisateurFonctionExercice getUtilisateurFonctionExercice(EOEditingContext editingContext, EOUtilisateurFonction utilisateurfonction, EOExercice exercice) throws FinderException {
		final NSArray tmp;
		if (utilisateurfonction == null) {
			throw new FinderException(UTILISATEURFONCTION_EST_NUL);
		}

		NSArray auts = utilisateurfonction.toUtilisateurFonctionExercices();
		if (auts == null) {
			auts = new NSArray();
		}

		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonctionExercice.TO_EXERCICE_KEY + AFinder.QUAL_EQUALS, new NSArray(new Object[] { exercice }));
		tmp = EOQualifier.filteredArrayWithQualifier(auts, qual);

		if (tmp.count() == 0) {
			return null;
		}
		return (EOUtilisateurFonctionExercice) tmp.objectAtIndex(0);
	}

	/**
	 * Renvoie tous les enrtegistrements de la table utilisateur.
	 * 
	 * @param nom
	 *            filtre sur le nom si rempli
	 * @param refresh
	 *            refresh des objets ou pas
	 * @param ec
	 */
	public static NSArray fetchAllUtilisateursValides(final EOEditingContext ec, String nom, final boolean refresh) {
		final NSMutableArray larray = new NSMutableArray();
		larray.addObject(EOUtilisateur.SORT_UTL_NOM_KEY_ASC);
		larray.addObject(EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC);
		//        final NSArray prefetch = new NSArray(new Object[]{ EOUtilisateur.INDIVIDU_KEY });
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(qualUtilisateurValide());
		if (nom != null && nom.trim().length() > 0) {
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.TO_PERSONNE_KEY + "." + EOPersonne.PERS_LIBELLE_KEY + AFinder.QUAL_CASE_INSENSITIVE_LIKE, new NSArray(new Object[] { "*" + nom + "*" }));
			quals.addObject(qual);
		}
		NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, new EOAndQualifier(quals), larray, refresh);
		//        return res;
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] { EOUtilisateur.SORT_TYPE_UTILISATEUR_ASC, EOUtilisateur.SORT_UTL_NOM_KEY_ASC, EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC }));
		//        return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, larray);
	}

	/**
	 * Renvoie les utilisateurs qui ont au moins une fonction affectée pour
	 * l'application.
	 * 
	 * @param ec
	 * @param typeApplication
	 * @return
	 */
	public static NSArray fetchAllUtilisateursValidesForApplication(final EOEditingContext ec, final EOTypeApplication typeApplication) {
		final NSMutableArray larray = new NSMutableArray();
		larray.addObject(EOUtilisateur.SORT_UTL_NOM_KEY_ASC);
		larray.addObject(EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC);
		//        final NSArray prefetch = new NSArray(new Object[]{ EOUtilisateur.INDIVIDU_KEY });
		if (typeApplication != null) {
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.TO_UTILISATEUR_FONCTIONS_KEY + QUAL_POINT + EOUtilisateurFonction.TO_FONCTION_KEY + QUAL_POINT + EOFonction.TO_TYPE_APPLICATION_KEY + QUAL_EQUALS, new NSArray(new Object[] { typeApplication }));
			final EOQualifier qualfinal = new EOAndQualifier(new NSArray(new Object[] { qual, qualUtilisateurValide() }));
			final NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, qualfinal, larray, true, true, false, null);
			return res;
			//            return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, larray);
		}

		final EOQualifier qualfinal = new EOAndQualifier(new NSArray(new Object[] { qualUtilisateurValide() }));
		final NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, qualfinal, larray, true);
		//        return res;
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] { EOUtilisateur.SORT_TYPE_UTILISATEUR_ASC, EOUtilisateur.SORT_UTL_NOM_KEY_ASC, EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC }));
	}

	public static NSArray fetchAllUtilisateursValidesForFonction(final EOEditingContext ec, final EOFonction fonction) {
		final NSMutableArray larray = new NSMutableArray();
		larray.addObject(EOUtilisateur.SORT_UTL_NOM_KEY_ASC);
		larray.addObject(EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC);
		if (fonction != null) {
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.TO_UTILISATEUR_FONCTIONS_KEY + QUAL_POINT + EOUtilisateurFonction.TO_FONCTION_KEY + QUAL_EQUALS, new NSArray(new Object[] { fonction }));
			final EOQualifier qualfinal = new EOAndQualifier(new NSArray(new Object[] { qual, qualUtilisateurValide() }));
			final NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, qualfinal, larray, true, true, false, null);
			return res;
		}

		final EOQualifier qualfinal = new EOAndQualifier(new NSArray(new Object[] { qualUtilisateurValide() }));
		final NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, qualfinal, larray, true);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] { EOUtilisateur.SORT_TYPE_UTILISATEUR_ASC, EOUtilisateur.SORT_UTL_NOM_KEY_ASC, EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC }));
	}

}
