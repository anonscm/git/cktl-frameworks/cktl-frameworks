/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldroitsutils.common;

import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe pour prendre en charge les appels aux anciennes méthodes de FwkCktlJavaclienSupport (abandonné).
 * 
 * @author rprin
 */
public class CktlCallEOUtilities {
	/**
	 * Chemin d'acces en keyValueCoding pour acceder a la variable representant le callDelegate (l'instance de l'objet qui va effectivement effectuer
	 * le traitement)
	 */
	//public static final String REMOTE_PATH = "fwkCktlJavaClientSupport_" + "CktlEOUtilities";

	//	private static String EODISTRIBUTEDOBJECTSTORE_CLASS_NAME = "com.webobjects.eodistribution.client.EODistributedObjectStore";
	//	private static String CLIENT_INVOKE_CLASSNAME = "org.cocktail.fwkcktljavaclientsupport.client.remotecall.ServerCallCktlEOUtilities";
	//	private static String SERVER_INVOKE_CLASSNAME = "com.webobjects.eoaccess.EOUtilities";

	public static final String executeStoredProcedureNamed = "executeStoredProcedureNamed";
	public static final String primaryKeyForObject = "primaryKeyForObject";
	public static final String rawRowsForSQL = "rawRowsForSQL";
	public static final String objectsWithFetchSpecificationAndBindings = "objectsWithFetchSpecificationAndBindings";
	public static final String requiredAttributesForObject = "requiredAttributesForObject";
	public static final String attributesMaxSizesForObject = "attributesMaxSizesForObject";

	//
	//	public static boolean isClientEditingContext(EOEditingContext editingContext) {
	//		EOObjectStore objectStore = editingContext.parentObjectStore();
	//		boolean isClient = true;
	//		try {
	//			Class myEODistributedObjectStore = Class.forName(EODISTRIBUTEDOBJECTSTORE_CLASS_NAME);
	//			isClient = myEODistributedObjectStore.isInstance(objectStore);
	//		} catch (Exception e) {
	//			isClient = false;
	//		}
	//		return isClient;
	//	}

	public static NSDictionary primaryKeyForObject(EOEditingContext ec, EOEnterpriseObject eo) throws Exception {
		return EOUtilities.primaryKeyForObject(ec, eo);
	}

	public static NSArray rawRowsForSQL(EOEditingContext ec, String modelName, String sqlString, NSArray keys) throws Throwable {
		return EOUtilities.rawRowsForSQL(ec, modelName, sqlString, keys);
	}

	public static NSDictionary executeStoredProcedureNamed(EOEditingContext ec, String name, NSDictionary args) throws Throwable {
		return EOUtilities.executeStoredProcedureNamed(ec, name, args);
	}

	//
	//	public static NSDictionary attributeMaxSizesForEntityName(EOEditingContext ec, String name) {
	//
	//		String classEODescriptionName = "com.webobjects.eoaccess.EOEntityClassDescription";
	//		String classEOAttributeName = "com.webobjects.eoaccess.EOAttribute";
	//
	//		if (isClientEditingContext(ec)) {
	//			//classEODescriptionName = "com.webobjects.eodistribution.client.EODistributedClassDescription";
	//			classEOAttributeName = "com.webobjects.eodistribution.client.EOAttribute";
	//		}
	//
	//		try {
	//			Class myEOClassDescription = Class.forName(classEODescriptionName);
	//			Method myEOClassDescription_classDescriptionForEntityName = myEOClassDescription.getDeclaredMethod("classDescriptionForEntityName", new Class[] {
	//				String.class
	//			});
	//			Method myEOClassDescription_attributeNamed = myEOClassDescription.getDeclaredMethod("attributeNamed", new Class[] {
	//				String.class
	//			});
	//
	//			EOClassDescription eoclassdescription = (EOClassDescription) myEOClassDescription_classDescriptionForEntityName.invoke(null, new Object[] {
	//				name
	//			});
	//			if (eoclassdescription == null) {
	//				throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + name + "' !");
	//			}
	//
	//			Class myeoAttribute = Class.forName(classEOAttributeName);
	//			Method myeoAttribute_width = myeoAttribute.getDeclaredMethod("width", new Class[] {});
	//
	//			NSMutableDictionary res = new NSMutableDictionary();
	//			Enumeration enumeration = eoclassdescription.attributeKeys().objectEnumerator();
	//			while (enumeration.hasMoreElements()) {
	//				String attName = (String) enumeration.nextElement();
	//				Object object = myEOClassDescription_attributeNamed.invoke(eoclassdescription, new Object[] {
	//					attName
	//				});
	//				Number width = (Number) myeoAttribute_width.invoke(object, new Object[] {});
	//				if (width != null && width.intValue() > 0) {
	//					res.takeValueForKey(new Integer(width.intValue()), attName);
	//				}
	//			}
	//			return res.immutableClone();
	//
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//			return null;
	//		}
	//
	//	}

	public static Map attributeMaxSizesForEntityName(EOEditingContext ec, String name) {

		HashMap res = new HashMap();

		EOClassDescription _classDescription = EOClassDescription.classDescriptionForEntityName(name);

		String classEODescriptionName = "com.webobjects.eoaccess.EOEntityClassDescription";
		String classEOAttributeName = "com.webobjects.eoaccess.EOAttribute";
		//String attributeNamedName = "attributeNamed";

		try {
			//Classe serveur ou client
			Class myEOClassDescription = Class.forName(classEODescriptionName);
			Class myeoAttribute = Class.forName(classEOAttributeName);

			Method myeoAttribute_width = myeoAttribute.getDeclaredMethod("width", new Class[] {});
			//			
			//			Method myEOClassDescription_classDescriptionForEntityName = myEOClassDescription.getDeclaredMethod("classDescriptionForEntityName", new Class[] {
			//				String.class
			//			});
			////			

			Class myEntityClass = Class.forName("com.webobjects.eoaccess.EOEntity");
			Method myEOClassDescription_entity = myEOClassDescription.getDeclaredMethod("entity", new Class[] {

					});

			Method myEntityClass_attributeNamed = myEntityClass.getDeclaredMethod("attributeNamed", new Class[] {
					String.class
			});

			Object entity = myEOClassDescription_entity.invoke(_classDescription, new Object[] {});

			//NSMutableDictionary res = new NSMutableDictionary();
			//				Enumeration enumeration = _classDescription.attributeKeys().objectEnumerator();
			Enumeration enumeration = _classDescription.attributeKeys().objectEnumerator();
			while (enumeration.hasMoreElements()) {
				String attName = (String) enumeration.nextElement();

				Object object = myEntityClass_attributeNamed.invoke(entity, new Object[] {
						attName
				});

				Number width = (Number) myeoAttribute_width.invoke(object, new Object[] {});
				if (width != null && width.intValue() > 0) {
					//res.takeValueForKey(new Integer(width.intValue()), attName);
					res.put(attName, new Integer(width.intValue()));
				}
			}

			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static NSArray requiredAttributeForEntityName(EOEditingContext ec, String name) {

		EOClassDescription _classDescription = EOClassDescription.classDescriptionForEntityName(name);

		String classEODescriptionName = "com.webobjects.eoaccess.EOEntityClassDescription";
		String classEOAttributeName = "com.webobjects.eoaccess.EOAttribute";
		//String attributeNamedName = "attributeNamed";

		try {
			//Classe serveur ou client
			Class myEOClassDescription = Class.forName(classEODescriptionName);
			Class myeoAttribute = Class.forName(classEOAttributeName);

			Method myeoAttribute_allowsNull = myeoAttribute.getDeclaredMethod("allowsNull", new Class[] {});
			//			
			//			Method myEOClassDescription_classDescriptionForEntityName = myEOClassDescription.getDeclaredMethod("classDescriptionForEntityName", new Class[] {
			//				String.class
			//			});
			////			

			Class myEntityClass = Class.forName("com.webobjects.eoaccess.EOEntity");
			Method myEOClassDescription_entity = myEOClassDescription.getDeclaredMethod("entity", new Class[] {

					});

			Method myEntityClass_attributeNamed = myEntityClass.getDeclaredMethod("attributeNamed", new Class[] {
					String.class
			});

			Object entity = myEOClassDescription_entity.invoke(_classDescription, new Object[] {});

			NSMutableArray res = new NSMutableArray();
			Enumeration enumeration = _classDescription.attributeKeys().objectEnumerator();
			while (enumeration.hasMoreElements()) {
				String attName = (String) enumeration.nextElement();

				Object object = myEntityClass_attributeNamed.invoke(entity, new Object[] {
						attName
				});
				Boolean xxx = (Boolean) myeoAttribute_allowsNull.invoke(object, new Object[] {});
				if (!xxx.booleanValue()) {
					res.addObject(attName);
				}
			}
			return res.immutableClone();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static NSArray objectsWithFetchSpecificationAndBindings(EOEditingContext ec, String entityName, String fetchSpecName, NSDictionary bindings) throws Throwable {
		return EOUtilities.objectsWithFetchSpecificationAndBindings(ec, entityName, fetchSpecName, bindings);
	}

	public static NSArray objectsMatchingKeyAndValue(EOEditingContext ec, String entityName, String key, Object value) {
		NSDictionary dict = new NSDictionary(value, key);
		NSArray results = objectsMatchingValues(ec, entityName, dict);
		return results;
	}

	public static NSArray objectsMatchingValues(EOEditingContext ec, String name, NSDictionary values) {
		EOQualifier qualifier = EOQualifier.qualifierToMatchAllValues(values);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(name, qualifier, null);
		NSArray results = ec.objectsWithFetchSpecification(fetchSpec);
		return results;
	}

	/**
	 * @param editingContext
	 * @return L'editingContext de plus haut niveau (avant enregistrement dans la base ). Renvoi null si editingContext est nul.
	 */
	public static EOEditingContext getTopLevelEditingContext(EOEditingContext editingContext) {
		if (editingContext == null) {
			return null;
		}
		EOEditingContext edc = editingContext;
		while (edc != null && !(edc.parentObjectStore() instanceof EOObjectStoreCoordinator)) {
			edc = (EOEditingContext) edc.parentObjectStore();
		}
		return edc;
	}

}
