package org.cocktail.fwkcktlcompta.client.sepasdd.helpers;

import java.util.Map;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddOrigineHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.origines.CommonSepaSddOrigineEntityCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class SepaSddOrigineClientHelper extends SepaSddOrigineHelper {



	private static SepaSddOrigineClientHelper sharedInstance = new SepaSddOrigineClientHelper();

	public static SepaSddOrigineClientHelper getSharedInstance() {
		return sharedInstance;
	}

	public Map<String, ? extends CommonSepaSddOrigineEntityCtrl> getSepaSddOriginesMap(EOEditingContext editingContext) {
		if (sepaSddOriginesMap == null) {
			try {
				sepaSddOriginesMap = buildOriginesCtrl(EOSepaSddOrigineType.fetchAll(editingContext));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sepaSddOriginesMap;
	}

	public NSArray fetchOrigineTypesValides(EOEditingContext editingContext) {
		EOQualifier qualifier = new EOKeyValueQualifier(EOSepaSddOrigineType.VALIDE_KEY, EOQualifier.QualifierOperatorEqual, ISepaSddOrigineType.ETAT_VALIDE);
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(EOSepaSddOrigineType.TYPE_ORDRE_KEY, EOSortOrdering.CompareAscending);
		return EOSepaSddOrigineType.fetchAll(editingContext, qualifier, new NSArray(new Object[] {
			sort
		}));
	}

	// TODO normalement on peut remplacer les appels a cette methode par l'appel a la methdoe ayant la meme signature qui se trouve dans commons.sepasdd.helpers
	public ISepaSddOrigineType fetchOrigineTypeValide(EOEditingContext editingContext, String code) {
		if (code == null) {
			return null;
		}

		EOQualifier qualType = new EOKeyValueQualifier(EOSepaSddOrigineType.TYPE_CODE_KEY, EOQualifier.QualifierOperatorEqual, code);
		NSArray types = EOSepaSddOrigineType.fetchAll(editingContext, qualType, NSArray.emptyArray());

		ISepaSddOrigineType type = null;
		if (types.count() > 0) {
			type = (ISepaSddOrigineType) types.objectAtIndex(0);
		}

		return type;
	}

}
