package org.cocktail.fwkcktlcompta.client.sepasdd.factories;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.client.metier.EOPrelevementFichier;
import org.cocktail.fwkcktlcompta.client.metier.EORecouvrement;
import org.cocktail.fwkcktlcompta.client.metier.EOTypeRecouvrement;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

@Deprecated
public class SepaSddPrelevementFichierFactory {

	public EOPrelevementFichier creerPrelevementFichier(EOEditingContext ed, EORecouvrement recouvrement, EOTypeRecouvrement typeRecouvrement, EOJefyAdminUtilisateur utilisateur, String contenu, NSTimestamp creation, NSTimestamp dateRemise, NSTimestamp dateReglement, String libelle,
			BigDecimal ficpMontant,
			Integer ficpNbPrelevements, Integer ficpNumero, String ficpCodeOp) {
		final EOPrelevementFichier monVirementFichier = EOPrelevementFichier.creerInstance(ed);

		monVirementFichier.setFicpContenu(contenu);
		monVirementFichier.setFicpDateCreation(creation);
		monVirementFichier.setFicpDateModif(creation);
		monVirementFichier.setFicpLibelle(libelle);
		monVirementFichier.setFicpDateRemise(dateRemise);
		monVirementFichier.setFicpMontant(ficpMontant);
		monVirementFichier.setFicpNbPrelevements(ficpNbPrelevements);
		monVirementFichier.setFicpDateReglement(dateReglement);
		monVirementFichier.setFicpNumero(ficpNumero);
		monVirementFichier.setFicpCodeOp(ficpCodeOp);

		monVirementFichier.setToRecouvrementRelationship(recouvrement);
		monVirementFichier.setToUtilisateurRelationship(utilisateur);
		monVirementFichier.setToTypeRecouvrementRelationship(typeRecouvrement);

		return monVirementFichier;
	}
}
