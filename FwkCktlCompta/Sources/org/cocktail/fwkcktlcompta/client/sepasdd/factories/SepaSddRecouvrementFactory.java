package org.cocktail.fwkcktlcompta.client.sepasdd.factories;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.client.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.client.metier.EORecouvrement;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.client.metier.EOTypeRecouvrement;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

@Deprecated
public class SepaSddRecouvrementFactory {

	public EORecouvrement creerRecouvrement(EOEditingContext ed, EOComptabilite comptabilite, EOJefyAdminExercice exercice, EOJefyAdminUtilisateur utilisateur, EOTypeRecouvrement typeRecouvrement, NSTimestamp creation, BigDecimal montant, Integer nbVirements, Integer numero) {
		final EORecouvrement monRecouvrement = EORecouvrement.creerInstance(ed);
		monRecouvrement.setRecoDateCreation(creation);
		monRecouvrement.setRecoMontant(montant);
		monRecouvrement.setRecoNb(nbVirements);
		monRecouvrement.setRecoNumero(numero);

		monRecouvrement.setToComptabiliteRelationship(comptabilite);
		monRecouvrement.setToExerciceRelationship(exercice);
		monRecouvrement.setToUtilisateurRelationship(utilisateur);
		monRecouvrement.setToTypeRecouvrementRelationship(typeRecouvrement);
		return monRecouvrement;
	}

	//
	//	/**
	//	 * Passe tous les prélèvements associés au recouvrement à l'état EOPrelevement.ETAT_PRELEVE ("PRELEVE");
	//	 * 
	//	 * @param recouvrement
	//	 */
	//	public final void fermerPrelevements(final EORecouvrement recouvrement) {
	//		SepaSddEcheanceHelper.getSharedInstance().changeEtatOfEcheancesAPrelevees(recouvrement.toSepaSddEcheances());
	//
	//	}

	//
	//
	public void associerEcheances(EORecouvrement recouvrement, NSArray echeances) {
		for (int i = 0; i < echeances.count(); i++) {
			final EOSepaSddEcheance element = (EOSepaSddEcheance) echeances.objectAtIndex(i);
			element.setToRecouvrementRelationship(recouvrement);
		}

	}
}
