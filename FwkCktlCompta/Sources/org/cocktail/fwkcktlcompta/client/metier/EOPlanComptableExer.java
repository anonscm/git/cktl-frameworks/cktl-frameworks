/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.client.metier;

import org.cocktail.fwkcktlcompta.common.entities.IPlanComptableExer;
import org.cocktail.fwkcktlcompta.common.rules.PlanComptableExerRule;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPlanComptableExer extends _EOPlanComptableExer implements IPlanComptableExer {


	public static EOQualifier QUAL_VALIDE = new EOKeyValueQualifier(EOPlanComptableExer.PCO_VALIDITE_KEY, EOQualifier.QualifierOperatorEqual, etatValide);
	public static EOQualifier QUAL_CLASSE_4 = EOQualifier.qualifierWithQualifierFormat("pcoNum like '" + CLASSE4 + "*'", null);
	public static EOQualifier QUAL_CLASSE_5 = EOQualifier.qualifierWithQualifierFormat("pcoNum like '" + CLASSE5 + "*'", null);
	public static EOQualifier QUAL_EMARGEABLE = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PCO_EMARGEMENT_KEY + "=%@", new NSArray(new Object[] {
			EOPlanComptableExer.pcoEmargementOui
	}));
	public static final EOQualifier QUAL_GRANDLIVREVALEURSINACTIVES = new EOKeyValueQualifier(EOPlanComptable.PCO_NUM_KEY, EOQualifier.QualifierOperatorLike, "86*");

	public EOPlanComptableExer() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		PlanComptableExerRule.getSharedInstance().validate(this);
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * @return La classe du compte (premier caractère du pcoNum ou null).
	 */
	public final String getClasseCompte() {
		if (pcoNum() != null) {
			return pcoNum().substring(0, 1);
		}
		return null;
	}

	/**
	 * @return Le chapitre du compte (deux premiers caractères du pcoNum ou null).
	 */
	public final String getChapitreCompte() {
		if (pcoNum() != null && pcoNum().length() >= 2) {
			return pcoNum().substring(0, 2);
		}
		return null;
	}

	public final String pcoNumEtPcoLibelle() {
		return pcoNum() + " : " + pcoLibelle();
	}

	public String toString() {
		return pcoNumEtPcoLibelle();
	}

	public boolean isAmortissementPossible() {
		return CLASSE2.equals(getClasseCompte());
	}

	public boolean isClasse() {
		return pcoNiveau().intValue() == 1;
	}

	public boolean isChapitre() {
		return pcoNiveau().intValue() == 2;
	}

	public static EOPlanComptableExer getCompte(EOEditingContext editingContext, EOJefyAdminExercice exercice, String pcoNum) {
		EOQualifier qual1 = new EOKeyValueQualifier(EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, pcoNum);
		EOQualifier qual2 = new EOKeyValueQualifier(EOPlanComptableExer.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
		return EOPlanComptableExer.fetchByQualifier(editingContext, new EOAndQualifier(new NSArray(new Object[] {
				qual1, qual2
		})));
	}

	/**
	 * @param editingContext
	 * @param exercice
	 * @param pcoNum
	 * @return Le premier compte "parent" trouvé.
	 */
	public static EOPlanComptableExer getComptePere(EOEditingContext editingContext, EOJefyAdminExercice exercice, String pcoNum) {
		if (StringCtrl.isEmpty(pcoNum)) {
			return null;
		}
		pcoNum = pcoNum.substring(0, pcoNum.length() - 1);
		//System.out.println(pcoNum);
		EOQualifier qual1 = new EOKeyValueQualifier(EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, pcoNum);
		EOQualifier qual2 = new EOKeyValueQualifier(EOPlanComptableExer.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
		EOPlanComptableExer res = EOPlanComptableExer.fetchByQualifier(editingContext, new EOAndQualifier(new NSArray(new Object[] {
				qual1, qual2
		})));
		if (res == null) {
			return getComptePere(editingContext, exercice, pcoNum);
		}
		return res;
	}

	public boolean isValide() {
		return etatValide.equals(pcoValidite());
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOJefyAdminExercice exercice) {
		EOQualifier qual = new EOKeyValueQualifier(EOPlanComptableExer.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
		return fetchAll(editingContext, qual, null, false);
	}

	public static NSArray fetchAllValidesLike(EOEditingContext editingContext, EOJefyAdminExercice exercice, String like) {
		NSMutableArray quals = new NSMutableArray();

		quals.addObject(new EOKeyValueQualifier(EOPlanComptableExer.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice));
		quals.addObject(EOPlanComptableExer.QUAL_VALIDE);
		if (!StringCtrl.isEmpty(like)) {
			quals.addObject(new EOKeyValueQualifier(EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorLike, like));
		}

		return fetchAll(editingContext, new EOAndQualifier(quals), null, false);
	}

}
