// _EOSepaSddMandat.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSepaSddMandat.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOSepaSddMandat extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddMandat";
	public static final String ENTITY_TABLE_NAME = "maracuja.SEPA_SDD_MANDAT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idSepaSddMandat";

	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String C_TYPE_PRELEVEMENT_KEY = "cTypePrelevement";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MANDAT_CREATION_KEY = "dMandatCreation";
	public static final String D_MANDAT_SIGNATURE_KEY = "dMandatSignature";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIBELLE_KEY = "libelle";
	public static final String NUMERO_KEY = "numero";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String REF_APPLI_CREATION_KEY = "refAppliCreation";
	public static final String RUM_KEY = "rum";

// Attributs non visibles
	public static final String CREANCIER_PERS_ID_KEY = "creancierPersId";
	public static final String DEBITEUR_ADR_ORDRE_KEY = "debiteurAdrOrdre";
	public static final String DEBITEUR_PERS_ID_KEY = "debiteurPersId";
	public static final String DEBITEUR_RIB_ORDRE_KEY = "debiteurRibOrdre";
	public static final String ID_SEPA_SDD_MANDAT_KEY = "idSepaSddMandat";
	public static final String ID_SEPA_SDD_PARAM_KEY = "idSepaSddParam";
	public static final String TIERS_CREANCIER_PERS_ID_KEY = "tiersCreancierPersId";
	public static final String TIERS_DEBITEUR_PERS_ID_KEY = "tiersDebiteurPersId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String COMMENTAIRE_COLKEY = "commentaire";
	public static final String C_TYPE_PRELEVEMENT_COLKEY = "C_TYPE_PRELEVEMENT";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MANDAT_CREATION_COLKEY = "D_mandat_CREATION";
	public static final String D_MANDAT_SIGNATURE_COLKEY = "D_mandat_signature";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String LIBELLE_COLKEY = "libelle";
	public static final String NUMERO_COLKEY = "numero";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String REF_APPLI_CREATION_COLKEY = "ref_appli_creation";
	public static final String RUM_COLKEY = "RUM";

	public static final String CREANCIER_PERS_ID_COLKEY = "CREANCIER_PERS_ID";
	public static final String DEBITEUR_ADR_ORDRE_COLKEY = "DEBITEUR_ADR_ORDRE";
	public static final String DEBITEUR_PERS_ID_COLKEY = "DEBITEUR_PERS_ID";
	public static final String DEBITEUR_RIB_ORDRE_COLKEY = "DEBITEUR_RIB_ORDRE";
	public static final String ID_SEPA_SDD_MANDAT_COLKEY = "ID_SEPA_SDD_MANDAT";
	public static final String ID_SEPA_SDD_PARAM_COLKEY = "ID_SEPA_SDD_PARAM";
	public static final String TIERS_CREANCIER_PERS_ID_COLKEY = "TIERS_CREANCIER_PERS_ID";
	public static final String TIERS_DEBITEUR_PERS_ID_COLKEY = "TIERS_DEBITEUR_PERS_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String TO_CREANCIER_PERSONNE_KEY = "toCreancierPersonne";
	public static final String TO_DEBITEUR_ADRESSE_KEY = "toDebiteurAdresse";
	public static final String TO_DEBITEUR_PERSONNE_KEY = "toDebiteurPersonne";
	public static final String TO_DEBITEUR_RIB_KEY = "toDebiteurRib";
	public static final String TO_MODIFICATEUR_KEY = "toModificateur";
	public static final String TO_SEPA_SDD_ECHEANCIERS_KEY = "toSepaSddEcheanciers";
	public static final String TO_SEPA_SDD_MANDAT_HISTOS_KEY = "toSepaSddMandatHistos";
	public static final String TO_SEPA_SDD_PARAM_KEY = "toSepaSddParam";
	public static final String TO_TIERS_CREANCIER_PERSONNE_KEY = "toTiersCreancierPersonne";
	public static final String TO_TIERS_DEBITEUR_PERSONNE_KEY = "toTiersDebiteurPersonne";
	public static final String TO_TYPE_ETAT_KEY = "toTypeEtat";



	// Accessors methods
  public String commentaire() {
    return (String) storedValueForKey(COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    takeStoredValueForKey(value, COMMENTAIRE_KEY);
  }

  public String cTypePrelevement() {
    return (String) storedValueForKey(C_TYPE_PRELEVEMENT_KEY);
  }

  public void setCTypePrelevement(String value) {
    takeStoredValueForKey(value, C_TYPE_PRELEVEMENT_KEY);
  }

  public String dCreation() {
    return (String) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(String value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public String dMandatCreation() {
    return (String) storedValueForKey(D_MANDAT_CREATION_KEY);
  }

  public void setDMandatCreation(String value) {
    takeStoredValueForKey(value, D_MANDAT_CREATION_KEY);
  }

  public String dMandatSignature() {
    return (String) storedValueForKey(D_MANDAT_SIGNATURE_KEY);
  }

  public void setDMandatSignature(String value) {
    takeStoredValueForKey(value, D_MANDAT_SIGNATURE_KEY);
  }

  public String dModification() {
    return (String) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(String value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    takeStoredValueForKey(value, LIBELLE_KEY);
  }

  public String numero() {
    return (String) storedValueForKey(NUMERO_KEY);
  }

  public void setNumero(String value) {
    takeStoredValueForKey(value, NUMERO_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String refAppliCreation() {
    return (String) storedValueForKey(REF_APPLI_CREATION_KEY);
  }

  public void setRefAppliCreation(String value) {
    takeStoredValueForKey(value, REF_APPLI_CREATION_KEY);
  }

  public String rum() {
    return (String) storedValueForKey(RUM_KEY);
  }

  public void setRum(String value) {
    takeStoredValueForKey(value, RUM_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toCreancierPersonne() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_CREANCIER_PERSONNE_KEY);
  }

  public void setToCreancierPersonneRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toCreancierPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CREANCIER_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CREANCIER_PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse toDebiteurAdresse() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse)storedValueForKey(TO_DEBITEUR_ADRESSE_KEY);
  }

  public void setToDebiteurAdresseRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse oldValue = toDebiteurAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEBITEUR_ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEBITEUR_ADRESSE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toDebiteurPersonne() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_DEBITEUR_PERSONNE_KEY);
  }

  public void setToDebiteurPersonneRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toDebiteurPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEBITEUR_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEBITEUR_PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib toDebiteurRib() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib)storedValueForKey(TO_DEBITEUR_RIB_KEY);
  }

  public void setToDebiteurRibRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib oldValue = toDebiteurRib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEBITEUR_RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEBITEUR_RIB_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toModificateur() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_MODIFICATEUR_KEY);
  }

  public void setToModificateurRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toModificateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODIFICATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODIFICATEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddParam toSepaSddParam() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddParam)storedValueForKey(TO_SEPA_SDD_PARAM_KEY);
  }

  public void setToSepaSddParamRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddParam value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOSepaSddParam oldValue = toSepaSddParam();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SEPA_SDD_PARAM_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SEPA_SDD_PARAM_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toTiersCreancierPersonne() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_TIERS_CREANCIER_PERSONNE_KEY);
  }

  public void setToTiersCreancierPersonneRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toTiersCreancierPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TIERS_CREANCIER_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TIERS_CREANCIER_PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toTiersDebiteurPersonne() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_TIERS_DEBITEUR_PERSONNE_KEY);
  }

  public void setToTiersDebiteurPersonneRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toTiersDebiteurPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TIERS_DEBITEUR_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TIERS_DEBITEUR_PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat toTypeEtat() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat)storedValueForKey(TO_TYPE_ETAT_KEY);
  }

  public void setToTypeEtatRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat oldValue = toTypeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ETAT_KEY);
    }
  }
  
  public NSArray toSepaSddEcheanciers() {
    return (NSArray)storedValueForKey(TO_SEPA_SDD_ECHEANCIERS_KEY);
  }

  public NSArray toSepaSddEcheanciers(EOQualifier qualifier) {
    return toSepaSddEcheanciers(qualifier, null, false);
  }

  public NSArray toSepaSddEcheanciers(EOQualifier qualifier, Boolean fetch) {
    return toSepaSddEcheanciers(qualifier, null, fetch);
  }

  public NSArray toSepaSddEcheanciers(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier.TO_SEPA_SDD_MANDAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSepaSddEcheanciers();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSepaSddEcheanciersRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIERS_KEY);
  }

  public void removeFromToSepaSddEcheanciersRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIERS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier createToSepaSddEcheanciersRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_SepaSddEcheancier");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_SEPA_SDD_ECHEANCIERS_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier) eo;
  }

  public void deleteToSepaSddEcheanciersRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIERS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToSepaSddEcheanciersRelationships() {
    Enumeration objects = toSepaSddEcheanciers().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSepaSddEcheanciersRelationship((org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier)objects.nextElement());
    }
  }

  public NSArray toSepaSddMandatHistos() {
    return (NSArray)storedValueForKey(TO_SEPA_SDD_MANDAT_HISTOS_KEY);
  }

  public NSArray toSepaSddMandatHistos(EOQualifier qualifier) {
    return toSepaSddMandatHistos(qualifier, null, false);
  }

  public NSArray toSepaSddMandatHistos(EOQualifier qualifier, Boolean fetch) {
    return toSepaSddMandatHistos(qualifier, null, fetch);
  }

  public NSArray toSepaSddMandatHistos(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandatHisto.TO_SEPA_SDD_MANDAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandatHisto.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSepaSddMandatHistos();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSepaSddMandatHistosRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandatHisto object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_MANDAT_HISTOS_KEY);
  }

  public void removeFromToSepaSddMandatHistosRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandatHisto object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_MANDAT_HISTOS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandatHisto createToSepaSddMandatHistosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_SepaSddMandatHisto");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_SEPA_SDD_MANDAT_HISTOS_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandatHisto) eo;
  }

  public void deleteToSepaSddMandatHistosRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandatHisto object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_MANDAT_HISTOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToSepaSddMandatHistosRelationships() {
    Enumeration objects = toSepaSddMandatHistos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSepaSddMandatHistosRelationship((org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandatHisto)objects.nextElement());
    }
  }


  public static EOSepaSddMandat createFwkCktlCompta_SepaSddMandat(EOEditingContext editingContext, String cTypePrelevement
, String dCreation
, String dMandatCreation
, String dModification
, String numero
, Integer persIdCreation
, Integer persIdModification
, String refAppliCreation
, String rum
, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toCreancierPersonne, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toDebiteurPersonne, org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib toDebiteurRib, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toModificateur, org.cocktail.fwkcktlcompta.client.metier.EOSepaSddParam toSepaSddParam, org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat toTypeEtat) {
    EOSepaSddMandat eo = (EOSepaSddMandat) createAndInsertInstance(editingContext, _EOSepaSddMandat.ENTITY_NAME);    
		eo.setCTypePrelevement(cTypePrelevement);
		eo.setDCreation(dCreation);
		eo.setDMandatCreation(dMandatCreation);
		eo.setDModification(dModification);
		eo.setNumero(numero);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setRefAppliCreation(refAppliCreation);
		eo.setRum(rum);
    eo.setToCreancierPersonneRelationship(toCreancierPersonne);
    eo.setToDebiteurPersonneRelationship(toDebiteurPersonne);
    eo.setToDebiteurRibRelationship(toDebiteurRib);
    eo.setToModificateurRelationship(toModificateur);
    eo.setToSepaSddParamRelationship(toSepaSddParam);
    eo.setToTypeEtatRelationship(toTypeEtat);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOSepaSddMandat.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOSepaSddMandat.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOSepaSddMandat creerInstance(EOEditingContext editingContext) {
		  		EOSepaSddMandat object = (EOSepaSddMandat)createAndInsertInstance(editingContext, _EOSepaSddMandat.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOSepaSddMandat localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSepaSddMandat)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOSepaSddMandat localInstanceIn(EOEditingContext editingContext, EOSepaSddMandat eo) {
    EOSepaSddMandat localInstance = (eo == null) ? null : (EOSepaSddMandat)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOSepaSddMandat#localInstanceIn a la place.
   */
	public static EOSepaSddMandat localInstanceOf(EOEditingContext editingContext, EOSepaSddMandat eo) {
		return EOSepaSddMandat.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOSepaSddMandat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOSepaSddMandat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSepaSddMandat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSepaSddMandat)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSepaSddMandat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSepaSddMandat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSepaSddMandat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSepaSddMandat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOSepaSddMandat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSepaSddMandat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSepaSddMandat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSepaSddMandat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
