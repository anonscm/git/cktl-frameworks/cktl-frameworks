// _EOBrouillardDetail.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBrouillardDetail.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBrouillardDetail extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_BrouillardDetail";
	public static final String ENTITY_TABLE_NAME = "maracuja.brouillard_Detail";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "brodId";

	public static final String BROD_COMMENTAIRE_KEY = "brodCommentaire";
	public static final String BROD_CREDIT_KEY = "brodCredit";
	public static final String BROD_DEBIT_KEY = "brodDebit";
	public static final String BROD_INDEX_KEY = "brodIndex";
	public static final String BROD_LIBELLE_KEY = "brodLibelle";
	public static final String BROD_PCO_LIBELLE_KEY = "brodPcoLibelle";
	public static final String BROD_PCO_NUM_KEY = "brodPcoNum";
	public static final String BROD_POSTIT_KEY = "brodPostit";
	public static final String BROD_SENS_KEY = "brodSens";

// Attributs non visibles
	public static final String BROD_ID_KEY = "brodId";
	public static final String BRO_ID_KEY = "broId";
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String GES_CODE_KEY = "gesCode";

//Colonnes dans la base de donnees
	public static final String BROD_COMMENTAIRE_COLKEY = "brod_commentaire";
	public static final String BROD_CREDIT_COLKEY = "brod_credit";
	public static final String BROD_DEBIT_COLKEY = "brod_debit";
	public static final String BROD_INDEX_COLKEY = "brod_index";
	public static final String BROD_LIBELLE_COLKEY = "brod_libelle";
	public static final String BROD_PCO_LIBELLE_COLKEY = "brod_pco_libelle";
	public static final String BROD_PCO_NUM_COLKEY = "brod_PCO_NUM";
	public static final String BROD_POSTIT_COLKEY = "brod_Postit";
	public static final String BROD_SENS_COLKEY = "brod_sens";

	public static final String BROD_ID_COLKEY = "brod_id";
	public static final String BRO_ID_COLKEY = "bro_id";
	public static final String ECD_ORDRE_COLKEY = "ecd_Ordre";
	public static final String GES_CODE_COLKEY = "ges_code";


	// Relationships
	public static final String TO_BROUILLARD_KEY = "toBrouillard";
	public static final String TO_ECRITURE_DETAIL_KEY = "toEcritureDetail";
	public static final String TO_GESTION_KEY = "toGestion";



	// Accessors methods
  public String brodCommentaire() {
    return (String) storedValueForKey(BROD_COMMENTAIRE_KEY);
  }

  public void setBrodCommentaire(String value) {
    takeStoredValueForKey(value, BROD_COMMENTAIRE_KEY);
  }

  public java.math.BigDecimal brodCredit() {
    return (java.math.BigDecimal) storedValueForKey(BROD_CREDIT_KEY);
  }

  public void setBrodCredit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BROD_CREDIT_KEY);
  }

  public java.math.BigDecimal brodDebit() {
    return (java.math.BigDecimal) storedValueForKey(BROD_DEBIT_KEY);
  }

  public void setBrodDebit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BROD_DEBIT_KEY);
  }

  public Integer brodIndex() {
    return (Integer) storedValueForKey(BROD_INDEX_KEY);
  }

  public void setBrodIndex(Integer value) {
    takeStoredValueForKey(value, BROD_INDEX_KEY);
  }

  public String brodLibelle() {
    return (String) storedValueForKey(BROD_LIBELLE_KEY);
  }

  public void setBrodLibelle(String value) {
    takeStoredValueForKey(value, BROD_LIBELLE_KEY);
  }

  public String brodPcoLibelle() {
    return (String) storedValueForKey(BROD_PCO_LIBELLE_KEY);
  }

  public void setBrodPcoLibelle(String value) {
    takeStoredValueForKey(value, BROD_PCO_LIBELLE_KEY);
  }

  public String brodPcoNum() {
    return (String) storedValueForKey(BROD_PCO_NUM_KEY);
  }

  public void setBrodPcoNum(String value) {
    takeStoredValueForKey(value, BROD_PCO_NUM_KEY);
  }

  public String brodPostit() {
    return (String) storedValueForKey(BROD_POSTIT_KEY);
  }

  public void setBrodPostit(String value) {
    takeStoredValueForKey(value, BROD_POSTIT_KEY);
  }

  public String brodSens() {
    return (String) storedValueForKey(BROD_SENS_KEY);
  }

  public void setBrodSens(String value) {
    takeStoredValueForKey(value, BROD_SENS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOBrouillard toBrouillard() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOBrouillard)storedValueForKey(TO_BROUILLARD_KEY);
  }

  public void setToBrouillardRelationship(org.cocktail.fwkcktlcompta.client.metier.EOBrouillard value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOBrouillard oldValue = toBrouillard();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BROUILLARD_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BROUILLARD_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOEcritureDetail toEcritureDetail() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOEcritureDetail)storedValueForKey(TO_ECRITURE_DETAIL_KEY);
  }

  public void setToEcritureDetailRelationship(org.cocktail.fwkcktlcompta.client.metier.EOEcritureDetail value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOEcritureDetail oldValue = toEcritureDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ECRITURE_DETAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ECRITURE_DETAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
  }

  public void setToGestionRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGestion oldValue = toGestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
    }
  }
  

  public static EOBrouillardDetail createFwkCktlCompta_BrouillardDetail(EOEditingContext editingContext, java.math.BigDecimal brodCredit
, java.math.BigDecimal brodDebit
, Integer brodIndex
, String brodPcoNum
, String brodSens
, org.cocktail.fwkcktlcompta.client.metier.EOBrouillard toBrouillard, org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion) {
    EOBrouillardDetail eo = (EOBrouillardDetail) createAndInsertInstance(editingContext, _EOBrouillardDetail.ENTITY_NAME);    
		eo.setBrodCredit(brodCredit);
		eo.setBrodDebit(brodDebit);
		eo.setBrodIndex(brodIndex);
		eo.setBrodPcoNum(brodPcoNum);
		eo.setBrodSens(brodSens);
    eo.setToBrouillardRelationship(toBrouillard);
    eo.setToGestionRelationship(toGestion);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOBrouillardDetail.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOBrouillardDetail.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOBrouillardDetail creerInstance(EOEditingContext editingContext) {
		  		EOBrouillardDetail object = (EOBrouillardDetail)createAndInsertInstance(editingContext, _EOBrouillardDetail.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOBrouillardDetail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBrouillardDetail)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOBrouillardDetail localInstanceIn(EOEditingContext editingContext, EOBrouillardDetail eo) {
    EOBrouillardDetail localInstance = (eo == null) ? null : (EOBrouillardDetail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOBrouillardDetail#localInstanceIn a la place.
   */
	public static EOBrouillardDetail localInstanceOf(EOEditingContext editingContext, EOBrouillardDetail eo) {
		return EOBrouillardDetail.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOBrouillardDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOBrouillardDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBrouillardDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBrouillardDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBrouillardDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBrouillardDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBrouillardDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBrouillardDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOBrouillardDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBrouillardDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBrouillardDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBrouillardDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
