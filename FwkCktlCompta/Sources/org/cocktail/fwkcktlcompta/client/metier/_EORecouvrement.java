// _EORecouvrement.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORecouvrement.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORecouvrement extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Recouvrement";
	public static final String ENTITY_TABLE_NAME = "maracuja.RECOUVREMENT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "recoOrdre";

	public static final String RECO_DATE_CREATION_KEY = "recoDateCreation";
	public static final String RECO_MONTANT_KEY = "recoMontant";
	public static final String RECO_NB_KEY = "recoNb";
	public static final String RECO_NUMERO_KEY = "recoNumero";

// Attributs non visibles
	public static final String COM_ORDRE_KEY = "comOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String RECO_ORDRE_KEY = "recoOrdre";
	public static final String TREC_ORDRE_KEY = "trecOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String RECO_DATE_CREATION_COLKEY = "RECO_DATE_CREATION";
	public static final String RECO_MONTANT_COLKEY = "RECO_MONTANT";
	public static final String RECO_NB_COLKEY = "RECO_NB";
	public static final String RECO_NUMERO_COLKEY = "RECO_NUMERO";

	public static final String COM_ORDRE_COLKEY = "COM_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String RECO_ORDRE_COLKEY = "RECO_ORDRE";
	public static final String TREC_ORDRE_COLKEY = "TREC_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String TO_COMPTABILITE_KEY = "toComptabilite";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_PRELEVEMENT_FICHIERS_KEY = "toPrelevementFichiers";
	public static final String TO_SEPA_SDD_ECHEANCES_KEY = "toSepaSddEcheances";
	public static final String TO_TYPE_RECOUVREMENT_KEY = "toTypeRecouvrement";
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";



	// Accessors methods
  public NSTimestamp recoDateCreation() {
    return (NSTimestamp) storedValueForKey(RECO_DATE_CREATION_KEY);
  }

  public void setRecoDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, RECO_DATE_CREATION_KEY);
  }

  public java.math.BigDecimal recoMontant() {
    return (java.math.BigDecimal) storedValueForKey(RECO_MONTANT_KEY);
  }

  public void setRecoMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RECO_MONTANT_KEY);
  }

  public Integer recoNb() {
    return (Integer) storedValueForKey(RECO_NB_KEY);
  }

  public void setRecoNb(Integer value) {
    takeStoredValueForKey(value, RECO_NB_KEY);
  }

  public Integer recoNumero() {
    return (Integer) storedValueForKey(RECO_NUMERO_KEY);
  }

  public void setRecoNumero(Integer value) {
    takeStoredValueForKey(value, RECO_NUMERO_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOComptabilite toComptabilite() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOComptabilite)storedValueForKey(TO_COMPTABILITE_KEY);
  }

  public void setToComptabiliteRelationship(org.cocktail.fwkcktlcompta.client.metier.EOComptabilite value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOComptabilite oldValue = toComptabilite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COMPTABILITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_COMPTABILITE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice toExercice() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOTypeRecouvrement toTypeRecouvrement() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOTypeRecouvrement)storedValueForKey(TO_TYPE_RECOUVREMENT_KEY);
  }

  public void setToTypeRecouvrementRelationship(org.cocktail.fwkcktlcompta.client.metier.EOTypeRecouvrement value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOTypeRecouvrement oldValue = toTypeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur toUtilisateur() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
  }

  public void setToUtilisateurRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur oldValue = toUtilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
    }
  }
  
  public NSArray toPrelevementFichiers() {
    return (NSArray)storedValueForKey(TO_PRELEVEMENT_FICHIERS_KEY);
  }

  public NSArray toPrelevementFichiers(EOQualifier qualifier) {
    return toPrelevementFichiers(qualifier, null, false);
  }

  public NSArray toPrelevementFichiers(EOQualifier qualifier, Boolean fetch) {
    return toPrelevementFichiers(qualifier, null, fetch);
  }

  public NSArray toPrelevementFichiers(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOPrelevementFichier.TO_RECOUVREMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOPrelevementFichier.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPrelevementFichiers();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPrelevementFichiersRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPrelevementFichier object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRELEVEMENT_FICHIERS_KEY);
  }

  public void removeFromToPrelevementFichiersRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPrelevementFichier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRELEVEMENT_FICHIERS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOPrelevementFichier createToPrelevementFichiersRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_PrelevementFichier");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRELEVEMENT_FICHIERS_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOPrelevementFichier) eo;
  }

  public void deleteToPrelevementFichiersRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPrelevementFichier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRELEVEMENT_FICHIERS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPrelevementFichiersRelationships() {
    Enumeration objects = toPrelevementFichiers().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPrelevementFichiersRelationship((org.cocktail.fwkcktlcompta.client.metier.EOPrelevementFichier)objects.nextElement());
    }
  }

  public NSArray toSepaSddEcheances() {
    return (NSArray)storedValueForKey(TO_SEPA_SDD_ECHEANCES_KEY);
  }

  public NSArray toSepaSddEcheances(EOQualifier qualifier) {
    return toSepaSddEcheances(qualifier, null, false);
  }

  public NSArray toSepaSddEcheances(EOQualifier qualifier, Boolean fetch) {
    return toSepaSddEcheances(qualifier, null, fetch);
  }

  public NSArray toSepaSddEcheances(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance.TO_RECOUVREMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSepaSddEcheances();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSepaSddEcheancesRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCES_KEY);
  }

  public void removeFromToSepaSddEcheancesRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCES_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance createToSepaSddEcheancesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_SepaSddEcheance");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_SEPA_SDD_ECHEANCES_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance) eo;
  }

  public void deleteToSepaSddEcheancesRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToSepaSddEcheancesRelationships() {
    Enumeration objects = toSepaSddEcheances().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSepaSddEcheancesRelationship((org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance)objects.nextElement());
    }
  }


  public static EORecouvrement createFwkCktlCompta_Recouvrement(EOEditingContext editingContext, NSTimestamp recoDateCreation
, java.math.BigDecimal recoMontant
, Integer recoNb
, Integer recoNumero
, org.cocktail.fwkcktlcompta.client.metier.EOComptabilite toComptabilite, org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice toExercice, org.cocktail.fwkcktlcompta.client.metier.EOTypeRecouvrement toTypeRecouvrement, org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur toUtilisateur) {
    EORecouvrement eo = (EORecouvrement) createAndInsertInstance(editingContext, _EORecouvrement.ENTITY_NAME);    
		eo.setRecoDateCreation(recoDateCreation);
		eo.setRecoMontant(recoMontant);
		eo.setRecoNb(recoNb);
		eo.setRecoNumero(recoNumero);
    eo.setToComptabiliteRelationship(toComptabilite);
    eo.setToExerciceRelationship(toExercice);
    eo.setToTypeRecouvrementRelationship(toTypeRecouvrement);
    eo.setToUtilisateurRelationship(toUtilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EORecouvrement.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EORecouvrement.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EORecouvrement creerInstance(EOEditingContext editingContext) {
		  		EORecouvrement object = (EORecouvrement)createAndInsertInstance(editingContext, _EORecouvrement.ENTITY_NAME);
		  		return object;
			}


		
  	  public EORecouvrement localInstanceIn(EOEditingContext editingContext) {
	  		return (EORecouvrement)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EORecouvrement localInstanceIn(EOEditingContext editingContext, EORecouvrement eo) {
    EORecouvrement localInstance = (eo == null) ? null : (EORecouvrement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORecouvrement#localInstanceIn a la place.
   */
	public static EORecouvrement localInstanceOf(EOEditingContext editingContext, EORecouvrement eo) {
		return EORecouvrement.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EORecouvrement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORecouvrement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORecouvrement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORecouvrement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORecouvrement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORecouvrement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORecouvrement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORecouvrement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORecouvrement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORecouvrement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORecouvrement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORecouvrement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
