// _EOJefyRecetteFacture.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJefyRecetteFacture.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOJefyRecetteFacture extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_JefyRecette_Facture";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.facture";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "facId";

	public static final String ECHE_ID_KEY = "echeId";
	public static final String FAC_DATE_SAISIE_KEY = "facDateSaisie";
	public static final String FAC_HT_SAISIE_KEY = "facHtSaisie";
	public static final String FAC_LIB_KEY = "facLib";
	public static final String FAC_MONTANT_BUDGETAIRE_KEY = "facMontantBudgetaire";
	public static final String FAC_MONTANT_BUDGETAIRE_RESTE_KEY = "facMontantBudgetaireReste";
	public static final String FAC_NUMERO_KEY = "facNumero";
	public static final String FAC_TTC_SAISIE_KEY = "facTtcSaisie";
	public static final String FAC_TVA_SAISIE_KEY = "facTvaSaisie";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FAC_ID_KEY = "facId";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PERS_ID_KEY = "persId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYAP_ID_KEY = "tyapId";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ECHE_ID_COLKEY = "ECHE_ID";
	public static final String FAC_DATE_SAISIE_COLKEY = "FAC_DATE_SAISIE";
	public static final String FAC_HT_SAISIE_COLKEY = "FAC_HT_SAISIE";
	public static final String FAC_LIB_COLKEY = "FAC_LIB";
	public static final String FAC_MONTANT_BUDGETAIRE_COLKEY = "FAC_MONTANT_BUDGETAIRE";
	public static final String FAC_MONTANT_BUDGETAIRE_RESTE_COLKEY = "FAC_MONTANT_BUDGETAIRE_RESTE";
	public static final String FAC_NUMERO_COLKEY = "FAC_NUMERO";
	public static final String FAC_TTC_SAISIE_COLKEY = "FAC_TTC_SAISIE";
	public static final String FAC_TVA_SAISIE_COLKEY = "FAC_TVA_SAISIE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FAC_ID_COLKEY = "FAC_ID";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String TO_FACTURE_PAPIER_KEY = "toFacturePapier";
	public static final String TO_RECETTES_KEY = "toRecettes";



	// Accessors methods
  public Integer echeId() {
    return (Integer) storedValueForKey(ECHE_ID_KEY);
  }

  public void setEcheId(Integer value) {
    takeStoredValueForKey(value, ECHE_ID_KEY);
  }

  public NSTimestamp facDateSaisie() {
    return (NSTimestamp) storedValueForKey(FAC_DATE_SAISIE_KEY);
  }

  public void setFacDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, FAC_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal facHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FAC_HT_SAISIE_KEY);
  }

  public void setFacHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAC_HT_SAISIE_KEY);
  }

  public String facLib() {
    return (String) storedValueForKey(FAC_LIB_KEY);
  }

  public void setFacLib(String value) {
    takeStoredValueForKey(value, FAC_LIB_KEY);
  }

  public java.math.BigDecimal facMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(FAC_MONTANT_BUDGETAIRE_KEY);
  }

  public void setFacMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAC_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal facMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(FAC_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setFacMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAC_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public Integer facNumero() {
    return (Integer) storedValueForKey(FAC_NUMERO_KEY);
  }

  public void setFacNumero(Integer value) {
    takeStoredValueForKey(value, FAC_NUMERO_KEY);
  }

  public java.math.BigDecimal facTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FAC_TTC_SAISIE_KEY);
  }

  public void setFacTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAC_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal facTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FAC_TVA_SAISIE_KEY);
  }

  public void setFacTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAC_TVA_SAISIE_KEY);
  }

  public NSArray toFacturePapier() {
    return (NSArray)storedValueForKey(TO_FACTURE_PAPIER_KEY);
  }

  public NSArray toFacturePapier(EOQualifier qualifier) {
    return toFacturePapier(qualifier, null, false);
  }

  public NSArray toFacturePapier(EOQualifier qualifier, Boolean fetch) {
    return toFacturePapier(qualifier, null, fetch);
  }

  public NSArray toFacturePapier(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacturePapier.TO_FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacturePapier.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toFacturePapier();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToFacturePapierRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacturePapier object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_FACTURE_PAPIER_KEY);
  }

  public void removeFromToFacturePapierRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacturePapier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_FACTURE_PAPIER_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacturePapier createToFacturePapierRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_JefyRecette_FacturePapier");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_FACTURE_PAPIER_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacturePapier) eo;
  }

  public void deleteToFacturePapierRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacturePapier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_FACTURE_PAPIER_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToFacturePapierRelationships() {
    Enumeration objects = toFacturePapier().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToFacturePapierRelationship((org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacturePapier)objects.nextElement());
    }
  }

  public NSArray toRecettes() {
    return (NSArray)storedValueForKey(TO_RECETTES_KEY);
  }

  public NSArray toRecettes(EOQualifier qualifier) {
    return toRecettes(qualifier, null, false);
  }

  public NSArray toRecettes(EOQualifier qualifier, Boolean fetch) {
    return toRecettes(qualifier, null, fetch);
  }

  public NSArray toRecettes(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette.TO_FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRecettes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRecettesRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_RECETTES_KEY);
  }

  public void removeFromToRecettesRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RECETTES_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette createToRecettesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_JefyRecette_Recette");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_RECETTES_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette) eo;
  }

  public void deleteToRecettesRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RECETTES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRecettesRelationships() {
    Enumeration objects = toRecettes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRecettesRelationship((org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette)objects.nextElement());
    }
  }


  public static EOJefyRecetteFacture createFwkCktlCompta_JefyRecette_Facture(EOEditingContext editingContext, NSTimestamp facDateSaisie
, java.math.BigDecimal facHtSaisie
, java.math.BigDecimal facMontantBudgetaire
, java.math.BigDecimal facMontantBudgetaireReste
, Integer facNumero
, java.math.BigDecimal facTtcSaisie
, java.math.BigDecimal facTvaSaisie
) {
    EOJefyRecetteFacture eo = (EOJefyRecetteFacture) createAndInsertInstance(editingContext, _EOJefyRecetteFacture.ENTITY_NAME);    
		eo.setFacDateSaisie(facDateSaisie);
		eo.setFacHtSaisie(facHtSaisie);
		eo.setFacMontantBudgetaire(facMontantBudgetaire);
		eo.setFacMontantBudgetaireReste(facMontantBudgetaireReste);
		eo.setFacNumero(facNumero);
		eo.setFacTtcSaisie(facTtcSaisie);
		eo.setFacTvaSaisie(facTvaSaisie);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOJefyRecetteFacture.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOJefyRecetteFacture.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOJefyRecetteFacture creerInstance(EOEditingContext editingContext) {
		  		EOJefyRecetteFacture object = (EOJefyRecetteFacture)createAndInsertInstance(editingContext, _EOJefyRecetteFacture.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOJefyRecetteFacture localInstanceIn(EOEditingContext editingContext) {
	  		return (EOJefyRecetteFacture)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOJefyRecetteFacture localInstanceIn(EOEditingContext editingContext, EOJefyRecetteFacture eo) {
    EOJefyRecetteFacture localInstance = (eo == null) ? null : (EOJefyRecetteFacture)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOJefyRecetteFacture#localInstanceIn a la place.
   */
	public static EOJefyRecetteFacture localInstanceOf(EOEditingContext editingContext, EOJefyRecetteFacture eo) {
		return EOJefyRecetteFacture.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOJefyRecetteFacture fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOJefyRecetteFacture fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJefyRecetteFacture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJefyRecetteFacture)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJefyRecetteFacture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJefyRecetteFacture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJefyRecetteFacture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJefyRecetteFacture)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOJefyRecetteFacture fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJefyRecetteFacture eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJefyRecetteFacture ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJefyRecetteFacture fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
