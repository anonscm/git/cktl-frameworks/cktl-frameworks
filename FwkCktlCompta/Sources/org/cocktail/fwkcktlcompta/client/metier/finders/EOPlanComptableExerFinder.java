/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.client.metier.finders;

import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.client.metier.EOPlanComptable;
import org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlcompta.client.metier.EOPlancoCredit;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org <rodolphe.prin at univ-lr.fr>
 */

public class EOPlanComptableExerFinder extends ZFinder {
	public static final NSArray SORT_PCO_NUM_ASC = new NSArray(EOSortOrdering.sortOrderingWithKey(EOPlanComptableExer.PCO_NUM_KEY, EOSortOrdering.CompareAscending));

	public static final EOPlanComptableExer getPlancoExerForPcoNum(EOEditingContext ec, EOJefyAdminExercice exercice, String pcoNum, final boolean refresh) {
		final NSArray res = getPlancoExersWithQual(ec, exercice, null, refresh);
		final EOQualifier qual = new EOKeyValueQualifier(EOPlanComptable.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, pcoNum);
		final NSArray res2 = EOQualifier.filteredArrayWithQualifier(res, qual);
		if (res2.count() > 0) {
			return (EOPlanComptableExer) res2.objectAtIndex(0);
		}
		return null;
	}

	public static final NSArray getPlancoExersForPcoNumLike(EOEditingContext ec, EOJefyAdminExercice exercice, String pcoNum, final boolean refresh) {
		final EOQualifier qual = new EOKeyValueQualifier(EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorLike, pcoNum + "*");
		final NSArray res = getPlancoExersWithQual(ec, exercice, qual, refresh);
		return res;
	}

	public static NSArray getPlancoExerValidesWithCond(EOEditingContext ec, EOJefyAdminExercice exercice, String cond, NSArray params, final boolean refresh) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(cond, params);
		return getPlancoExerValidesWithQual(ec, exercice, qual, refresh);
	}

	public static NSArray getPlancoExerValidesWithQual(EOEditingContext ec, EOJefyAdminExercice exercice, EOQualifier qual, final boolean refresh) {
		return _getPlancoExerValidesWithQual(ec, exercice, qual, refresh);
	}

	public static NSArray getPlancoExersWithQual(EOEditingContext ec, EOJefyAdminExercice exercice, EOQualifier qual, final boolean refresh) {
		return _getPlancoExerWithQual(ec, exercice, qual, refresh);
	}

	public static EOPlanComptableExer findPlanComptableExerForPcoNumInList(NSArray planComptableList, String pcoNum) {
		final NSArray tmp = EOQualifier.filteredArrayWithQualifier(planComptableList, new EOKeyValueQualifier(EOPlanComptable.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, pcoNum));
		if (tmp.count() == 1) {
			return ((EOPlanComptableExer) tmp.objectAtIndex(0));
		}
		return null;
	}
	
	public static NSArray findPlanComptableExerForTypeCredit(EOEditingContext ec, PlanComptablePourTypeCreditSpecification spec) {
	    EOQualifier compteValide = EOQualifier.qualifierWithQualifierFormat(
	        EOPlanComptableExer.PCO_VALIDITE_KEY + QUAL_EQUALS, 
            new NSArray(new Object[] {spec.getCompteValide()}));
	        
        EOQualifier compteTypeRelationValide = EOQualifier.qualifierWithQualifierFormat(
            EOPlanComptableExer.TO_PLANCO_CREDITS_KEY + QUAL_POINT + EOPlancoCredit.PCC_ETAT_KEY + QUAL_EQUALS, 
            new NSArray(new Object[] {spec.getCompteTypeRelationValide()}));
        
        EOQualifier exerciceCompte = EOQualifier.qualifierWithQualifierFormat(
            EOPlanComptableExer.TO_EXERCICE_KEY + QUAL_EQUALS, 
            new NSArray(new Object[] {spec.getExercice()}));
        
        EOQualifier exerciceTypeCredit = EOQualifier.qualifierWithQualifierFormat(
            EOPlanComptableExer.TO_PLANCO_CREDITS_KEY + QUAL_POINT + EOPlancoCredit.TO_TYPE_CREDIT_KEY + QUAL_POINT + EOTypeCredit.EXERCICE_KEY + QUAL_EQUALS, 
            new NSArray(new Object[] {spec.getExercice()}));
        
        EOQualifier compteTypeRelationNature = EOQualifier.qualifierWithQualifierFormat(
            EOPlanComptableExer.TO_PLANCO_CREDITS_KEY + QUAL_POINT + EOPlancoCredit.PLA_QUOI_KEY + QUAL_EQUALS, 
            new NSArray(new Object[] {spec.getCompteTypeRelationNature()}));

        EOQualifier typeCredit = EOQualifier.qualifierWithQualifierFormat(
            EOPlanComptableExer.TO_PLANCO_CREDITS_KEY + QUAL_POINT + EOPlancoCredit.TO_TYPE_CREDIT_KEY + QUAL_POINT + EOTypeCredit.TCD_TYPE_KEY + QUAL_EQUALS, 
            new NSArray(new Object[] {spec.getTypeCredit()}));

        NSMutableArray qualifiers = new NSMutableArray(new Object[] {compteValide, compteTypeRelationValide, exerciceCompte, exerciceTypeCredit, compteTypeRelationNature, typeCredit});
        
        if (spec.getCompteLibellePartiel() != null) {
            EOQualifier libelle = new EOKeyValueQualifier(
                EOPlanComptableExer.PCO_LIBELLE_KEY, 
                EOQualifier.QualifierOperatorCaseInsensitiveLike, 
                QUAL_ETOILE + spec.getCompteLibellePartiel() + QUAL_ETOILE);
            qualifiers.addObject(libelle);
        }
        
        if (spec.getCompteNumeroPartiel() != null) {
            EOQualifier numero = new EOKeyValueQualifier(
                EOPlanComptableExer.PCO_NUM_KEY, 
                EOQualifier.QualifierOperatorLike, 
                spec.getCompteNumeroPartiel() + QUAL_ETOILE);
            qualifiers.addObject(numero);
        }
        
        return fetchArray(ec, EOPlanComptableExer.ENTITY_NAME, new EOAndQualifier(qualifiers), SORT_PCO_NUM_ASC, true, true, false, null);
    }
	
	private static NSArray _getPlancoExerValidesWithQual(EOEditingContext ec, EOJefyAdminExercice exercice, EOQualifier qual, final boolean refresh) {
		final EOQualifier qualValide = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PCO_VALIDITE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				EOPlanComptableExer.etatValide
		}));
		final EOQualifier quals = (qual != null ? new EOAndQualifier(new NSArray(new Object[] {
				qualValide, qual
		})) : new EOAndQualifier(new NSArray(new Object[] {
				qualValide
		})));
		return _getPlancoExerWithQual(ec, exercice, quals, refresh);
	}

	private static NSArray _getPlancoExerWithQual(EOEditingContext ec, EOJefyAdminExercice exercice, EOQualifier qual, final boolean refresh) {
		final EOQualifier qualExercice = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.TO_EXERCICE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				exercice
		}));

		final EOQualifier quals = (qual != null ? new EOAndQualifier(new NSArray(new Object[] {
				qualExercice, qual
		})) : new EOAndQualifier(new NSArray(new Object[] {
				qualExercice
		})));
		return ZFinder.fetchArray(ec, EOPlanComptableExer.ENTITY_NAME, quals, SORT_PCO_NUM_ASC, refresh);
	}

}
