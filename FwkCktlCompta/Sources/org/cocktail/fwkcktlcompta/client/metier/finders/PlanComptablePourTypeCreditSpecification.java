package org.cocktail.fwkcktlcompta.client.metier.finders;

import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice;

public class PlanComptablePourTypeCreditSpecification {

    private String compteValide;
    private String compteTypeRelationValide;
    private EOJefyAdminExercice exercice;
    private String compteTypeRelationNature;
    private String typeCredit;
    private String compteLibellePartiel;
    private String compteNumeroPartiel;
    
    public PlanComptablePourTypeCreditSpecification(String compteValide, String compteTypeRelationValide,
                    EOJefyAdminExercice exercice, String compteTyperelationNature, String typeCredit,
                    String compteLibellePartiel, String compteNumeroPartiel) {
        this.compteValide = compteValide;
        this.compteTypeRelationValide = compteTypeRelationValide;
        this.exercice = exercice;
        this.compteTypeRelationNature = compteTyperelationNature;
        this.typeCredit = typeCredit;
        this.compteLibellePartiel = compteLibellePartiel;
        this.compteNumeroPartiel = compteNumeroPartiel;
    }
    
    public String getCompteValide() {
        return compteValide;
    }
    public String getCompteTypeRelationValide() {
        return compteTypeRelationValide;
    }
    public EOJefyAdminExercice getExercice() {
        return exercice;
    }
    public String getCompteTypeRelationNature() {
        return compteTypeRelationNature;
    }
    public String getTypeCredit() {
        return typeCredit;
    }
    public String getCompteLibellePartiel() {
        return compteLibellePartiel;
    }
    public String getCompteNumeroPartiel() {
        return compteNumeroPartiel;
    }
}
