/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.client.metier;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOGrhumPersonne extends _EOGrhumPersonne implements IGrhumPersonne {

	public static final String NOM_AND_PRENOM_KEY = "NomAndPrenom";
	public static final String DERIV_FOU_CODE_KEY = "derivFouCode";
	public static final String DERIV_FOURNIS = "derivFournis";
	public static final String DERIV_FOU_TYPE_KEY = "derivFouType";
	public static final String DERIV_RIBS_VALIDE_KEY = "derivRibValides";

	public EOGrhumPersonne() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 *
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public String getNomAndPrenom() {
		return (persLc() != null ? persLc() + " " : "") + persLibelle();
	}

	public String getDerivFouCode() {
		EOGrhumFournis fou = getDerivFournis();
		if (fou != null) {
			return fou.fouCode();
		}
		return null;
	}

	public String getDerivFouType() {
		EOGrhumFournis fou = getDerivFournis();
		if (fou != null) {
			return fou.fouType();
		}
		return null;
	}

	public String getDerivRibValides() {
		String res = "";
		EOGrhumFournis fou = getDerivFournis();
		if (fou != null) {
			NSArray ribsValides = fou.toRibsValide();
			if (ribsValides.count() == 0) {
				return null;
			}

			if (ribsValides.count() > 1) {
				res += "(*) ";
			}
			res += ((EOGrhumRib) fou.toRibsValide().objectAtIndex(0)).bicEtIban();
		}
		return res;
	}

	public EOGrhumFournis getDerivFournis() {
		NSArray res = toFournises(new EOKeyValueQualifier(EOGrhumFournis.FOU_VALIDE_KEY, EOQualifier.QualifierOperatorNotEqual, "A"));
		if (res.count() == 1) {
			return (EOGrhumFournis) res.objectAtIndex(0);
		}
		return null;
	}

	public String getNomAndPrenomAndCode() {
		return getNomAndPrenom() + (getDerivFouCode() != null ? " (" + getDerivFouCode() + ")" : "");
	}

	/**
	 * @param editingContext
	 * @param nom
	 * @return Des personne fournisseurs de type C ou T
	 */
	public static NSArray fetchDebiteurs(EOEditingContext editingContext, String nom, int fetchLimit) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOGrhumPersonne.TO_FOURNISES_KEY + "." + EOGrhumFournis.FOU_VALIDE_KEY, EOQualifier.QualifierOperatorNotEqual, "A"));
		quals.addObject(new EOKeyValueQualifier(EOGrhumPersonne.TO_FOURNISES_KEY + "." + EOGrhumFournis.FOU_TYPE_KEY, EOQualifier.QualifierOperatorNotEqual, "F"));
		quals.addObject(new EOKeyValueQualifier(EOGrhumPersonne.PERS_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, nom));

		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOGrhumPersonne.PERS_LIBELLE_KEY, EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOGrhumPersonne.PERS_LC_KEY, EOSortOrdering.CompareAscending));

		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(quals), sorts);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setFetchLimit(fetchLimit);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	/**
	 * @param editingContext
	 * @param nom
	 * @return Des personne fournisseurs de type C ou T
	 */
	public static NSArray fetchTiersDebiteurs(EOEditingContext editingContext, String nom, int fetchLimit) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOGrhumPersonne.PERS_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, nom));

		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOGrhumPersonne.PERS_LIBELLE_KEY, EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOGrhumPersonne.PERS_LC_KEY, EOSortOrdering.CompareAscending));

		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(quals), sorts);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setFetchLimit(fetchLimit);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	public NSArray toRepartPersonneAdressesValides() {
		return (NSArray) toRepartPersonneAdresses(new EOKeyValueQualifier(EOGrhumRepartPersonneAdresse.RPA_VALIDE_KEY, EOQualifier.QualifierOperatorEqual, "O"));
	}
}
