// _EOGrhumAdresse.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGrhumAdresse.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOGrhumAdresse extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Grhum_Adresse";
	public static final String ENTITY_TABLE_NAME = "GRHUM.ADRESSE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "adrOrdre";

	public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
	public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
	public static final String ADR_BP_KEY = "adrBp";
	public static final String ADR_GPS_LATITUDE_KEY = "adrGpsLatitude";
	public static final String ADR_GPS_LONGITUDE_KEY = "adrGpsLongitude";
	public static final String ADR_LISTE_ROUGE_KEY = "adrListeRouge";
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String ADR_URL_PERE_KEY = "adrUrlPere";
	public static final String BIS_TER_KEY = "bisTer";
	public static final String C_IMPLANTATION_KEY = "cImplantation";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String CP_ETRANGER_KEY = "cpEtranger";
	public static final String C_VOIE_KEY = "cVoie";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LOCALITE_KEY = "localite";
	public static final String NOM_VOIE_KEY = "nomVoie";
	public static final String NO_VOIE_KEY = "noVoie";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String TEM_PAYE_UTIL_KEY = "temPayeUtil";
	public static final String VILLE_KEY = "ville";

// Attributs non visibles
	public static final String ADR_URL_RELATIVE_KEY = "adrUrlRelative";
	public static final String ADR_URL_TEMPLATE_KEY = "adrUrlTemplate";
	public static final String C_PAYS_KEY = "cPays";

//Colonnes dans la base de donnees
	public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
	public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
	public static final String ADR_BP_COLKEY = "ADR_BP";
	public static final String ADR_GPS_LATITUDE_COLKEY = "adr_Gps_latitude";
	public static final String ADR_GPS_LONGITUDE_COLKEY = "adr_Gps_Longitude";
	public static final String ADR_LISTE_ROUGE_COLKEY = "ADR_LISTE_ROUGE";
	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String ADR_URL_PERE_COLKEY = "ADR_URL_PERE";
	public static final String BIS_TER_COLKEY = "BIS_TER";
	public static final String C_IMPLANTATION_COLKEY = "C_IMPLANTATION";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String CP_ETRANGER_COLKEY = "CP_ETRANGER";
	public static final String C_VOIE_COLKEY = "C_VOIE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
	public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String LOCALITE_COLKEY = "LOCALITE";
	public static final String NOM_VOIE_COLKEY = "NOM_VOIE";
	public static final String NO_VOIE_COLKEY = "NO_VOIE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String TEM_PAYE_UTIL_COLKEY = "TEM_PAYE_UTIL";
	public static final String VILLE_COLKEY = "VILLE";

	public static final String ADR_URL_RELATIVE_COLKEY = "ADR_URL_RELATIVE";
	public static final String ADR_URL_TEMPLATE_COLKEY = "ADR_URL_TEMPLATE";
	public static final String C_PAYS_COLKEY = "C_PAYS";


	// Relationships
	public static final String TO_PAYS_KEY = "toPays";



	// Accessors methods
  public String adrAdresse1() {
    return (String) storedValueForKey(ADR_ADRESSE1_KEY);
  }

  public void setAdrAdresse1(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE1_KEY);
  }

  public String adrAdresse2() {
    return (String) storedValueForKey(ADR_ADRESSE2_KEY);
  }

  public void setAdrAdresse2(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE2_KEY);
  }

  public String adrBp() {
    return (String) storedValueForKey(ADR_BP_KEY);
  }

  public void setAdrBp(String value) {
    takeStoredValueForKey(value, ADR_BP_KEY);
  }

  public java.math.BigDecimal adrGpsLatitude() {
    return (java.math.BigDecimal) storedValueForKey(ADR_GPS_LATITUDE_KEY);
  }

  public void setAdrGpsLatitude(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ADR_GPS_LATITUDE_KEY);
  }

  public java.math.BigDecimal adrGpsLongitude() {
    return (java.math.BigDecimal) storedValueForKey(ADR_GPS_LONGITUDE_KEY);
  }

  public void setAdrGpsLongitude(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ADR_GPS_LONGITUDE_KEY);
  }

  public String adrListeRouge() {
    return (String) storedValueForKey(ADR_LISTE_ROUGE_KEY);
  }

  public void setAdrListeRouge(String value) {
    takeStoredValueForKey(value, ADR_LISTE_ROUGE_KEY);
  }

  public Integer adrOrdre() {
    return (Integer) storedValueForKey(ADR_ORDRE_KEY);
  }

  public void setAdrOrdre(Integer value) {
    takeStoredValueForKey(value, ADR_ORDRE_KEY);
  }

  public String adrUrlPere() {
    return (String) storedValueForKey(ADR_URL_PERE_KEY);
  }

  public void setAdrUrlPere(String value) {
    takeStoredValueForKey(value, ADR_URL_PERE_KEY);
  }

  public String bisTer() {
    return (String) storedValueForKey(BIS_TER_KEY);
  }

  public void setBisTer(String value) {
    takeStoredValueForKey(value, BIS_TER_KEY);
  }

  public String cImplantation() {
    return (String) storedValueForKey(C_IMPLANTATION_KEY);
  }

  public void setCImplantation(String value) {
    takeStoredValueForKey(value, C_IMPLANTATION_KEY);
  }

  public String codePostal() {
    return (String) storedValueForKey(CODE_POSTAL_KEY);
  }

  public void setCodePostal(String value) {
    takeStoredValueForKey(value, CODE_POSTAL_KEY);
  }

  public String cpEtranger() {
    return (String) storedValueForKey(CP_ETRANGER_KEY);
  }

  public void setCpEtranger(String value) {
    takeStoredValueForKey(value, CP_ETRANGER_KEY);
  }

  public String cVoie() {
    return (String) storedValueForKey(C_VOIE_KEY);
  }

  public void setCVoie(String value) {
    takeStoredValueForKey(value, C_VOIE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey(D_DEB_VAL_KEY);
  }

  public void setDDebVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEB_VAL_KEY);
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey(D_FIN_VAL_KEY);
  }

  public void setDFinVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_VAL_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String localite() {
    return (String) storedValueForKey(LOCALITE_KEY);
  }

  public void setLocalite(String value) {
    takeStoredValueForKey(value, LOCALITE_KEY);
  }

  public String nomVoie() {
    return (String) storedValueForKey(NOM_VOIE_KEY);
  }

  public void setNomVoie(String value) {
    takeStoredValueForKey(value, NOM_VOIE_KEY);
  }

  public String noVoie() {
    return (String) storedValueForKey(NO_VOIE_KEY);
  }

  public void setNoVoie(String value) {
    takeStoredValueForKey(value, NO_VOIE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String temPayeUtil() {
    return (String) storedValueForKey(TEM_PAYE_UTIL_KEY);
  }

  public void setTemPayeUtil(String value) {
    takeStoredValueForKey(value, TEM_PAYE_UTIL_KEY);
  }

  public String ville() {
    return (String) storedValueForKey(VILLE_KEY);
  }

  public void setVille(String value) {
    takeStoredValueForKey(value, VILLE_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPays toPays() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPays)storedValueForKey(TO_PAYS_KEY);
  }

  public void setToPaysRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPays value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPays oldValue = toPays();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_KEY);
    }
  }
  

  public static EOGrhumAdresse createFwkCktlCompta_Grhum_Adresse(EOEditingContext editingContext, String adrListeRouge
, Integer adrOrdre
, NSTimestamp dCreation
, NSTimestamp dModification
, String temPayeUtil
, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPays toPays) {
    EOGrhumAdresse eo = (EOGrhumAdresse) createAndInsertInstance(editingContext, _EOGrhumAdresse.ENTITY_NAME);    
		eo.setAdrListeRouge(adrListeRouge);
		eo.setAdrOrdre(adrOrdre);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemPayeUtil(temPayeUtil);
    eo.setToPaysRelationship(toPays);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOGrhumAdresse.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOGrhumAdresse.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOGrhumAdresse creerInstance(EOEditingContext editingContext) {
		  		EOGrhumAdresse object = (EOGrhumAdresse)createAndInsertInstance(editingContext, _EOGrhumAdresse.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOGrhumAdresse localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGrhumAdresse)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOGrhumAdresse localInstanceIn(EOEditingContext editingContext, EOGrhumAdresse eo) {
    EOGrhumAdresse localInstance = (eo == null) ? null : (EOGrhumAdresse)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOGrhumAdresse#localInstanceIn a la place.
   */
	public static EOGrhumAdresse localInstanceOf(EOEditingContext editingContext, EOGrhumAdresse eo) {
		return EOGrhumAdresse.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOGrhumAdresse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOGrhumAdresse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGrhumAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGrhumAdresse)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGrhumAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGrhumAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGrhumAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGrhumAdresse)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOGrhumAdresse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGrhumAdresse eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGrhumAdresse ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGrhumAdresse fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
