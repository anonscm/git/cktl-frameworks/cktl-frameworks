// _EOSepaSddEcheanceEcd.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSepaSddEcheanceEcd.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOSepaSddEcheanceEcd extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddEcheanceEcd";
	public static final String ENTITY_TABLE_NAME = "maracuja.sepa_Sdd_Echeance_Ecd";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idSepaSddEcheanceEcd";


// Attributs non visibles
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String ID_SEPA_SDD_ECHEANCE_KEY = "idSepaSddEcheance";
	public static final String ID_SEPA_SDD_ECHEANCE_ECD_KEY = "idSepaSddEcheanceEcd";

//Colonnes dans la base de donnees

	public static final String ECD_ORDRE_COLKEY = "ecd_ORDRE";
	public static final String ID_SEPA_SDD_ECHEANCE_COLKEY = "ID_sepa_sdd_echeance";
	public static final String ID_SEPA_SDD_ECHEANCE_ECD_COLKEY = "ID_sepa_sdd_echeance_ecd";


	// Relationships
	public static final String TO_ECRITURE_DETAIL_KEY = "toEcritureDetail";
	public static final String TO_SEPA_SDD_ECHEANCE_KEY = "toSepaSddEcheance";



	// Accessors methods
  public org.cocktail.fwkcktlcompta.client.metier.EOEcritureDetail toEcritureDetail() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOEcritureDetail)storedValueForKey(TO_ECRITURE_DETAIL_KEY);
  }

  public void setToEcritureDetailRelationship(org.cocktail.fwkcktlcompta.client.metier.EOEcritureDetail value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOEcritureDetail oldValue = toEcritureDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ECRITURE_DETAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ECRITURE_DETAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance toSepaSddEcheance() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance)storedValueForKey(TO_SEPA_SDD_ECHEANCE_KEY);
  }

  public void setToSepaSddEcheanceRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance oldValue = toSepaSddEcheance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SEPA_SDD_ECHEANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SEPA_SDD_ECHEANCE_KEY);
    }
  }
  

  public static EOSepaSddEcheanceEcd createFwkCktlCompta_SepaSddEcheanceEcd(EOEditingContext editingContext, org.cocktail.fwkcktlcompta.client.metier.EOEcritureDetail toEcritureDetail, org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance toSepaSddEcheance) {
    EOSepaSddEcheanceEcd eo = (EOSepaSddEcheanceEcd) createAndInsertInstance(editingContext, _EOSepaSddEcheanceEcd.ENTITY_NAME);    
    eo.setToEcritureDetailRelationship(toEcritureDetail);
    eo.setToSepaSddEcheanceRelationship(toSepaSddEcheance);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOSepaSddEcheanceEcd.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOSepaSddEcheanceEcd.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOSepaSddEcheanceEcd creerInstance(EOEditingContext editingContext) {
		  		EOSepaSddEcheanceEcd object = (EOSepaSddEcheanceEcd)createAndInsertInstance(editingContext, _EOSepaSddEcheanceEcd.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOSepaSddEcheanceEcd localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSepaSddEcheanceEcd)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOSepaSddEcheanceEcd localInstanceIn(EOEditingContext editingContext, EOSepaSddEcheanceEcd eo) {
    EOSepaSddEcheanceEcd localInstance = (eo == null) ? null : (EOSepaSddEcheanceEcd)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOSepaSddEcheanceEcd#localInstanceIn a la place.
   */
	public static EOSepaSddEcheanceEcd localInstanceOf(EOEditingContext editingContext, EOSepaSddEcheanceEcd eo) {
		return EOSepaSddEcheanceEcd.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOSepaSddEcheanceEcd fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOSepaSddEcheanceEcd fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSepaSddEcheanceEcd eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSepaSddEcheanceEcd)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSepaSddEcheanceEcd fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSepaSddEcheanceEcd fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSepaSddEcheanceEcd eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSepaSddEcheanceEcd)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOSepaSddEcheanceEcd fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSepaSddEcheanceEcd eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSepaSddEcheanceEcd ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSepaSddEcheanceEcd fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
