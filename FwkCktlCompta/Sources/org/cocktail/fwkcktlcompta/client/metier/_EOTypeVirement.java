// _EOTypeVirement.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeVirement.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTypeVirement extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_TypeVirement";
	public static final String ENTITY_TABLE_NAME = "maracuja.TYPE_VIREMENT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tviOrdre";

	public static final String MOD_DOM_KEY = "modDom";
	public static final String TVI_FORMAT_KEY = "tviFormat";
	public static final String TVI_LIBELLE_KEY = "tviLibelle";
	public static final String TVI_VALIDITE_KEY = "tviValidite";

// Attributs non visibles
	public static final String TVI_ORDRE_KEY = "tviOrdre";

//Colonnes dans la base de donnees
	public static final String MOD_DOM_COLKEY = "MOD_DOM";
	public static final String TVI_FORMAT_COLKEY = "TVI_FORMAT";
	public static final String TVI_LIBELLE_COLKEY = "TVI_LIBELLE";
	public static final String TVI_VALIDITE_COLKEY = "TVI_VALIDITE";

	public static final String TVI_ORDRE_COLKEY = "TVI_ORDRE";


	// Relationships
	public static final String TO_VIREMENT_PARAM_SEPAS_KEY = "toVirementParamSepas";



	// Accessors methods
  public String modDom() {
    return (String) storedValueForKey(MOD_DOM_KEY);
  }

  public void setModDom(String value) {
    takeStoredValueForKey(value, MOD_DOM_KEY);
  }

  public String tviFormat() {
    return (String) storedValueForKey(TVI_FORMAT_KEY);
  }

  public void setTviFormat(String value) {
    takeStoredValueForKey(value, TVI_FORMAT_KEY);
  }

  public String tviLibelle() {
    return (String) storedValueForKey(TVI_LIBELLE_KEY);
  }

  public void setTviLibelle(String value) {
    takeStoredValueForKey(value, TVI_LIBELLE_KEY);
  }

  public String tviValidite() {
    return (String) storedValueForKey(TVI_VALIDITE_KEY);
  }

  public void setTviValidite(String value) {
    takeStoredValueForKey(value, TVI_VALIDITE_KEY);
  }

  public NSArray toVirementParamSepas() {
    return (NSArray)storedValueForKey(TO_VIREMENT_PARAM_SEPAS_KEY);
  }

  public NSArray toVirementParamSepas(EOQualifier qualifier) {
    return toVirementParamSepas(qualifier, null, false);
  }

  public NSArray toVirementParamSepas(EOQualifier qualifier, Boolean fetch) {
    return toVirementParamSepas(qualifier, null, fetch);
  }

  public NSArray toVirementParamSepas(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOVirementParamSepa.TO_TYPE_VIREMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOVirementParamSepa.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toVirementParamSepas();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToVirementParamSepasRelationship(org.cocktail.fwkcktlcompta.client.metier.EOVirementParamSepa object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_VIREMENT_PARAM_SEPAS_KEY);
  }

  public void removeFromToVirementParamSepasRelationship(org.cocktail.fwkcktlcompta.client.metier.EOVirementParamSepa object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_VIREMENT_PARAM_SEPAS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOVirementParamSepa createToVirementParamSepasRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_VirementParamSepa");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_VIREMENT_PARAM_SEPAS_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOVirementParamSepa) eo;
  }

  public void deleteToVirementParamSepasRelationship(org.cocktail.fwkcktlcompta.client.metier.EOVirementParamSepa object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_VIREMENT_PARAM_SEPAS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToVirementParamSepasRelationships() {
    Enumeration objects = toVirementParamSepas().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToVirementParamSepasRelationship((org.cocktail.fwkcktlcompta.client.metier.EOVirementParamSepa)objects.nextElement());
    }
  }


  public static EOTypeVirement createFwkCktlCompta_TypeVirement(EOEditingContext editingContext, String tviLibelle
, String tviValidite
) {
    EOTypeVirement eo = (EOTypeVirement) createAndInsertInstance(editingContext, _EOTypeVirement.ENTITY_NAME);    
		eo.setTviLibelle(tviLibelle);
		eo.setTviValidite(tviValidite);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOTypeVirement.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOTypeVirement.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOTypeVirement creerInstance(EOEditingContext editingContext) {
		  		EOTypeVirement object = (EOTypeVirement)createAndInsertInstance(editingContext, _EOTypeVirement.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOTypeVirement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeVirement)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOTypeVirement localInstanceIn(EOEditingContext editingContext, EOTypeVirement eo) {
    EOTypeVirement localInstance = (eo == null) ? null : (EOTypeVirement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOTypeVirement#localInstanceIn a la place.
   */
	public static EOTypeVirement localInstanceOf(EOEditingContext editingContext, EOTypeVirement eo) {
		return EOTypeVirement.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOTypeVirement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTypeVirement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeVirement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeVirement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeVirement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeVirement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeVirement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeVirement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOTypeVirement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeVirement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeVirement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeVirement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
