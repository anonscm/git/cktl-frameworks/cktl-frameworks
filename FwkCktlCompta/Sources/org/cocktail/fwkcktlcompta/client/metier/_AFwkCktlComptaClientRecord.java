package org.cocktail.fwkcktlcompta.client.metier;

import org.cocktail.fwkcktlcompta.client.util.ClientCktlEOControlUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.foundation.NSArray;

public abstract class _AFwkCktlComptaClientRecord extends EOGenericRecord {

	protected String _primaryKey = null;

	/**
	 * @return La valeur de la clé primaire sous forme de String.
	 */
	public String primaryKey() {
		if (_primaryKey == null) {
			_primaryKey = ClientCktlEOControlUtilities.primaryKeyStringForObject(this);
		}
		return _primaryKey;
	}

	public Boolean isNewObject() {
		EOGlobalID gid = editingContext().globalIDForObject(this);
		return gid.isTemporary();
	}

	public static NSArray primaryKeyArrayForObject(EOEnterpriseObject obj) {
		EOEditingContext ec = obj.editingContext();
		if (ec == null) {
			return null;
		}
		EOGlobalID gid = ec.globalIDForObject(obj);
		if (gid.isTemporary()) {
			return null;
		}
		EOKeyGlobalID kGid = (EOKeyGlobalID) gid;
		return kGid.keyValuesArray();
	}

	
	
	
	
}
