// _EOGrhumRepartPersonneAdresse.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGrhumRepartPersonneAdresse.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOGrhumRepartPersonneAdresse extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Grhum_RepartPersonneAdresse";
	public static final String ENTITY_TABLE_NAME = "GRHUM.REPART_PERSONNE_ADRESSE";



	// Attributes


	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String E_MAIL_KEY = "eMail";
	public static final String PERS_ID_KEY = "persId";
	public static final String RPA_PRINCIPAL_KEY = "rpaPrincipal";
	public static final String RPA_VALIDE_KEY = "rpaValide";
	public static final String TADR_CODE_KEY = "tadrCode";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String E_MAIL_COLKEY = "E_MAIL";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String RPA_PRINCIPAL_COLKEY = "RPA_PRINCIPAL";
	public static final String RPA_VALIDE_COLKEY = "RPA_VALIDE";
	public static final String TADR_CODE_COLKEY = "TADR_CODE";



	// Relationships
	public static final String TO_ADRESSE_KEY = "toAdresse";
	public static final String TO_PERSONNE_KEY = "toPersonne";
	public static final String TO_TYPE_ADRESSE_KEY = "toTypeAdresse";



	// Accessors methods
  public Integer adrOrdre() {
    return (Integer) storedValueForKey(ADR_ORDRE_KEY);
  }

  public void setAdrOrdre(Integer value) {
    takeStoredValueForKey(value, ADR_ORDRE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String eMail() {
    return (String) storedValueForKey(E_MAIL_KEY);
  }

  public void setEMail(String value) {
    takeStoredValueForKey(value, E_MAIL_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public String rpaPrincipal() {
    return (String) storedValueForKey(RPA_PRINCIPAL_KEY);
  }

  public void setRpaPrincipal(String value) {
    takeStoredValueForKey(value, RPA_PRINCIPAL_KEY);
  }

  public String rpaValide() {
    return (String) storedValueForKey(RPA_VALIDE_KEY);
  }

  public void setRpaValide(String value) {
    takeStoredValueForKey(value, RPA_VALIDE_KEY);
  }

  public String tadrCode() {
    return (String) storedValueForKey(TADR_CODE_KEY);
  }

  public void setTadrCode(String value) {
    takeStoredValueForKey(value, TADR_CODE_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse toAdresse() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse)storedValueForKey(TO_ADRESSE_KEY);
  }

  public void setToAdresseRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse oldValue = toAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ADRESSE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonne() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_PERSONNE_KEY);
  }

  public void setToPersonneRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumTypeAdresse toTypeAdresse() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumTypeAdresse)storedValueForKey(TO_TYPE_ADRESSE_KEY);
  }

  public void setToTypeAdresseRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumTypeAdresse value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumTypeAdresse oldValue = toTypeAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ADRESSE_KEY);
    }
  }
  

  public static EOGrhumRepartPersonneAdresse createFwkCktlCompta_Grhum_RepartPersonneAdresse(EOEditingContext editingContext, Integer adrOrdre
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persId
, String rpaPrincipal
, String tadrCode
, org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse toAdresse, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonne, org.cocktail.fwkcktlcompta.client.metier.EOGrhumTypeAdresse toTypeAdresse) {
    EOGrhumRepartPersonneAdresse eo = (EOGrhumRepartPersonneAdresse) createAndInsertInstance(editingContext, _EOGrhumRepartPersonneAdresse.ENTITY_NAME);    
		eo.setAdrOrdre(adrOrdre);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersId(persId);
		eo.setRpaPrincipal(rpaPrincipal);
		eo.setTadrCode(tadrCode);
    eo.setToAdresseRelationship(toAdresse);
    eo.setToPersonneRelationship(toPersonne);
    eo.setToTypeAdresseRelationship(toTypeAdresse);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOGrhumRepartPersonneAdresse.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOGrhumRepartPersonneAdresse.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOGrhumRepartPersonneAdresse creerInstance(EOEditingContext editingContext) {
		  		EOGrhumRepartPersonneAdresse object = (EOGrhumRepartPersonneAdresse)createAndInsertInstance(editingContext, _EOGrhumRepartPersonneAdresse.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOGrhumRepartPersonneAdresse localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGrhumRepartPersonneAdresse)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOGrhumRepartPersonneAdresse localInstanceIn(EOEditingContext editingContext, EOGrhumRepartPersonneAdresse eo) {
    EOGrhumRepartPersonneAdresse localInstance = (eo == null) ? null : (EOGrhumRepartPersonneAdresse)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOGrhumRepartPersonneAdresse#localInstanceIn a la place.
   */
	public static EOGrhumRepartPersonneAdresse localInstanceOf(EOEditingContext editingContext, EOGrhumRepartPersonneAdresse eo) {
		return EOGrhumRepartPersonneAdresse.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOGrhumRepartPersonneAdresse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOGrhumRepartPersonneAdresse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGrhumRepartPersonneAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGrhumRepartPersonneAdresse)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGrhumRepartPersonneAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGrhumRepartPersonneAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGrhumRepartPersonneAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGrhumRepartPersonneAdresse)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOGrhumRepartPersonneAdresse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGrhumRepartPersonneAdresse eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGrhumRepartPersonneAdresse ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGrhumRepartPersonneAdresse fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
