// _EOModeRecouvrement.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOModeRecouvrement.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOModeRecouvrement extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_ModeRecouvrement";
	public static final String ENTITY_TABLE_NAME = "maracuja.Mode_recouvrement";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "modOrdre";

	public static final String MOD_CODE_KEY = "modCode";
	public static final String MOD_DOM_KEY = "modDom";
	public static final String MOD_LIBELLE_KEY = "modLibelle";
	public static final String MOD_VALIDITE_KEY = "modValidite";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String PCO_NUM_PAIEMENT_KEY = "pcoNumPaiement";
	public static final String PCO_NUM_VISA_KEY = "pcoNumVisa";

//Colonnes dans la base de donnees
	public static final String MOD_CODE_COLKEY = "mod_code";
	public static final String MOD_DOM_COLKEY = "mod_dom";
	public static final String MOD_LIBELLE_COLKEY = "MOD_LIBELLE";
	public static final String MOD_VALIDITE_COLKEY = "MOD_VALIDITE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String PCO_NUM_PAIEMENT_COLKEY = "PCO_NUM_PAIEMENT";
	public static final String PCO_NUM_VISA_COLKEY = "PCO_NUM_VISA";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_PLAN_COMPTABLE_EXER_PAIEMENT_KEY = "toPlanComptableExerPaiement";
	public static final String TO_PLAN_COMPTABLE_EXER_VISA_KEY = "toPlanComptableExerVisa";



	// Accessors methods
  public String modCode() {
    return (String) storedValueForKey(MOD_CODE_KEY);
  }

  public void setModCode(String value) {
    takeStoredValueForKey(value, MOD_CODE_KEY);
  }

  public String modDom() {
    return (String) storedValueForKey(MOD_DOM_KEY);
  }

  public void setModDom(String value) {
    takeStoredValueForKey(value, MOD_DOM_KEY);
  }

  public String modLibelle() {
    return (String) storedValueForKey(MOD_LIBELLE_KEY);
  }

  public void setModLibelle(String value) {
    takeStoredValueForKey(value, MOD_LIBELLE_KEY);
  }

  public String modValidite() {
    return (String) storedValueForKey(MOD_VALIDITE_KEY);
  }

  public void setModValidite(String value) {
    takeStoredValueForKey(value, MOD_VALIDITE_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer toPlanComptableExerPaiement() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer)storedValueForKey(TO_PLAN_COMPTABLE_EXER_PAIEMENT_KEY);
  }

  public void setToPlanComptableExerPaiementRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer oldValue = toPlanComptableExerPaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_EXER_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_EXER_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer toPlanComptableExerVisa() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer)storedValueForKey(TO_PLAN_COMPTABLE_EXER_VISA_KEY);
  }

  public void setToPlanComptableExerVisaRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer oldValue = toPlanComptableExerVisa();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_EXER_VISA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_EXER_VISA_KEY);
    }
  }
  

  public static EOModeRecouvrement createFwkCktlCompta_ModeRecouvrement(EOEditingContext editingContext, String modCode
, String modDom
, String modLibelle
, String modValidite
) {
    EOModeRecouvrement eo = (EOModeRecouvrement) createAndInsertInstance(editingContext, _EOModeRecouvrement.ENTITY_NAME);    
		eo.setModCode(modCode);
		eo.setModDom(modDom);
		eo.setModLibelle(modLibelle);
		eo.setModValidite(modValidite);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOModeRecouvrement.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOModeRecouvrement.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOModeRecouvrement creerInstance(EOEditingContext editingContext) {
		  		EOModeRecouvrement object = (EOModeRecouvrement)createAndInsertInstance(editingContext, _EOModeRecouvrement.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOModeRecouvrement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOModeRecouvrement)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOModeRecouvrement localInstanceIn(EOEditingContext editingContext, EOModeRecouvrement eo) {
    EOModeRecouvrement localInstance = (eo == null) ? null : (EOModeRecouvrement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOModeRecouvrement#localInstanceIn a la place.
   */
	public static EOModeRecouvrement localInstanceOf(EOEditingContext editingContext, EOModeRecouvrement eo) {
		return EOModeRecouvrement.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOModeRecouvrement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOModeRecouvrement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOModeRecouvrement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOModeRecouvrement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOModeRecouvrement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOModeRecouvrement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOModeRecouvrement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOModeRecouvrement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOModeRecouvrement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOModeRecouvrement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOModeRecouvrement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOModeRecouvrement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
