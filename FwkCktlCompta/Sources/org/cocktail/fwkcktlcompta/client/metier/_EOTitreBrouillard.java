// _EOTitreBrouillard.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTitreBrouillard.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTitreBrouillard extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_TitreBrouillard";
	public static final String ENTITY_TABLE_NAME = "maracuja.Titre_Brouillard";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tibOrdre";

	public static final String TIB_MONTANT_KEY = "tibMontant";
	public static final String TIB_OPERATION_KEY = "tibOperation";
	public static final String TIB_SENS_KEY = "tibSens";

// Attributs non visibles
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String REC_ID_KEY = "recId";
	public static final String TIB_ORDRE_KEY = "tibOrdre";
	public static final String TIT_ID_KEY = "titId";

//Colonnes dans la base de donnees
	public static final String TIB_MONTANT_COLKEY = "tib_Montant";
	public static final String TIB_OPERATION_COLKEY = "tib_Operation";
	public static final String TIB_SENS_COLKEY = "tib_Sens";

	public static final String ECD_ORDRE_COLKEY = "ECD_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_Code";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String REC_ID_COLKEY = "rec_id";
	public static final String TIB_ORDRE_COLKEY = "TIB_ORDRE";
	public static final String TIT_ID_COLKEY = "TIT_ID";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_GESTION_KEY = "toGestion";
	public static final String TO_PLAN_COMPTABLE_EXER_KEY = "toPlanComptableExer";
	public static final String TO_RECETTE_KEY = "toRecette";



	// Accessors methods
  public java.math.BigDecimal tibMontant() {
    return (java.math.BigDecimal) storedValueForKey(TIB_MONTANT_KEY);
  }

  public void setTibMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TIB_MONTANT_KEY);
  }

  public String tibOperation() {
    return (String) storedValueForKey(TIB_OPERATION_KEY);
  }

  public void setTibOperation(String value) {
    takeStoredValueForKey(value, TIB_OPERATION_KEY);
  }

  public String tibSens() {
    return (String) storedValueForKey(TIB_SENS_KEY);
  }

  public void setTibSens(String value) {
    takeStoredValueForKey(value, TIB_SENS_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
  }

  public void setToGestionRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGestion oldValue = toGestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer toPlanComptableExer() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer)storedValueForKey(TO_PLAN_COMPTABLE_EXER_KEY);
  }

  public void setToPlanComptableExerRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer oldValue = toPlanComptableExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_EXER_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EORecette toRecette() {
    return (org.cocktail.fwkcktlcompta.client.metier.EORecette)storedValueForKey(TO_RECETTE_KEY);
  }

  public void setToRecetteRelationship(org.cocktail.fwkcktlcompta.client.metier.EORecette value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EORecette oldValue = toRecette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RECETTE_KEY);
    }
  }
  

  public static EOTitreBrouillard createFwkCktlCompta_TitreBrouillard(EOEditingContext editingContext, java.math.BigDecimal tibMontant
, String tibSens
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice, org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion, org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer toPlanComptableExer) {
    EOTitreBrouillard eo = (EOTitreBrouillard) createAndInsertInstance(editingContext, _EOTitreBrouillard.ENTITY_NAME);    
		eo.setTibMontant(tibMontant);
		eo.setTibSens(tibSens);
    eo.setToExerciceRelationship(toExercice);
    eo.setToGestionRelationship(toGestion);
    eo.setToPlanComptableExerRelationship(toPlanComptableExer);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOTitreBrouillard.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOTitreBrouillard.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOTitreBrouillard creerInstance(EOEditingContext editingContext) {
		  		EOTitreBrouillard object = (EOTitreBrouillard)createAndInsertInstance(editingContext, _EOTitreBrouillard.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOTitreBrouillard localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTitreBrouillard)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOTitreBrouillard localInstanceIn(EOEditingContext editingContext, EOTitreBrouillard eo) {
    EOTitreBrouillard localInstance = (eo == null) ? null : (EOTitreBrouillard)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOTitreBrouillard#localInstanceIn a la place.
   */
	public static EOTitreBrouillard localInstanceOf(EOEditingContext editingContext, EOTitreBrouillard eo) {
		return EOTitreBrouillard.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOTitreBrouillard fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTitreBrouillard fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTitreBrouillard eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTitreBrouillard)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTitreBrouillard fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTitreBrouillard fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTitreBrouillard eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTitreBrouillard)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOTitreBrouillard fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTitreBrouillard eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTitreBrouillard ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTitreBrouillard fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
