// _EORecette.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORecette.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORecette extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Recette";
	public static final String ENTITY_TABLE_NAME = "maracuja.Recette";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "recId";

	public static final String MOD_CODE_KEY = "modCode";
	public static final String REC_DATE_KEY = "recDate";
	public static final String REC_DATE_LIMITE_PAIEMENT_KEY = "recDateLimitePaiement";
	public static final String REC_DEBITEUR_KEY = "recDebiteur";
	public static final String REC_IMPUTTVA_KEY = "recImputtva";
	public static final String REC_LIBELLE_KEY = "recLibelle";
	public static final String REC_LIGNE_BUDGETAIRE_KEY = "recLigneBudgetaire";
	public static final String REC_MONNAIE_KEY = "recMonnaie";
	public static final String REC_MONT_KEY = "recMont";
	public static final String REC_MONTANT_DISQUETTE_KEY = "recMontantDisquette";
	public static final String REC_MONT_TTC_KEY = "recMontTtc";
	public static final String REC_MONTTVA_KEY = "recMonttva";
	public static final String REC_NUM_KEY = "recNum";
	public static final String REC_ORDRE_KEY = "recOrdre";
	public static final String REC_PIECE_KEY = "recPiece";
	public static final String REC_REF_KEY = "recRef";
	public static final String REC_STAT_KEY = "recStat";
	public static final String REC_SUPPRESSION_KEY = "recSuppression";
	public static final String REC_TYPE_KEY = "recType";
	public static final String REC_VIREMENT_KEY = "recVirement";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String ORG_ORDRE_INTERNE_KEY = "orgOrdreInterne";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String REC_ID_KEY = "recId";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String TIT_ID_KEY = "titId";
	public static final String TIT_ORDRE_KEY = "titOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String MOD_CODE_COLKEY = "MOD_CODE";
	public static final String REC_DATE_COLKEY = "rec_DATE";
	public static final String REC_DATE_LIMITE_PAIEMENT_COLKEY = "rec_DATE_limite_paiement";
	public static final String REC_DEBITEUR_COLKEY = "rec_DEBITEUR";
	public static final String REC_IMPUTTVA_COLKEY = "rec_IMPUTTVA";
	public static final String REC_LIBELLE_COLKEY = "rec_libelle";
	public static final String REC_LIGNE_BUDGETAIRE_COLKEY = "rec_Ligne_Budgetaire";
	public static final String REC_MONNAIE_COLKEY = "rec_MONNAIE";
	public static final String REC_MONT_COLKEY = "rec_MONT";
	public static final String REC_MONTANT_DISQUETTE_COLKEY = "rec_Montant_Disquette";
	public static final String REC_MONT_TTC_COLKEY = "TIT_MONTTC";
	public static final String REC_MONTTVA_COLKEY = "rec_MONTTVA";
	public static final String REC_NUM_COLKEY = "rec_NUM";
	public static final String REC_ORDRE_COLKEY = "REC_ORDRE";
	public static final String REC_PIECE_COLKEY = "rec_PIECE";
	public static final String REC_REF_COLKEY = "rec_REF";
	public static final String REC_STAT_COLKEY = "rec_STAT";
	public static final String REC_SUPPRESSION_COLKEY = "rec_Suppression";
	public static final String REC_TYPE_COLKEY = "rec_TYPE";
	public static final String REC_VIREMENT_COLKEY = "rec_VIREMENT";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String ORG_ORDRE_COLKEY = "org_ordre";
	public static final String ORG_ORDRE_INTERNE_COLKEY = "rec_INTERNE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String REC_ID_COLKEY = "REC_ID";
	public static final String RIB_ORDRE_COLKEY = "rib_ordre";
	public static final String TIT_ID_COLKEY = "TIT_ID";
	public static final String TIT_ORDRE_COLKEY = "tit_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_FOURNIS_KEY = "toFournis";
	public static final String TO_GESTION_KEY = "toGestion";
	public static final String TO_MODE_PAIEMENT_KEY = "toModePaiement";
	public static final String TO_MODE_RECOUVREMENT_KEY = "toModeRecouvrement";
	public static final String TO_ORGAN_KEY = "toOrgan";
	public static final String TO_ORGAN_INTERNE_KEY = "toOrganInterne";
	public static final String TO_PLAN_COMPTABLE_EXER_KEY = "toPlanComptableExer";
	public static final String TO_RECETTE_CTRL_PLANCO_KEY = "toRecetteCtrlPlanco";
	public static final String TO_RIB_KEY = "toRib";
	public static final String TO_TITRE_KEY = "toTitre";
	public static final String TO_TITRE_DETAIL_ECRITURES_KEY = "toTitreDetailEcritures";
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";



	// Accessors methods
  public String modCode() {
    return (String) storedValueForKey(MOD_CODE_KEY);
  }

  public void setModCode(String value) {
    takeStoredValueForKey(value, MOD_CODE_KEY);
  }

  public NSTimestamp recDate() {
    return (NSTimestamp) storedValueForKey(REC_DATE_KEY);
  }

  public void setRecDate(NSTimestamp value) {
    takeStoredValueForKey(value, REC_DATE_KEY);
  }

  public NSTimestamp recDateLimitePaiement() {
    return (NSTimestamp) storedValueForKey(REC_DATE_LIMITE_PAIEMENT_KEY);
  }

  public void setRecDateLimitePaiement(NSTimestamp value) {
    takeStoredValueForKey(value, REC_DATE_LIMITE_PAIEMENT_KEY);
  }

  public String recDebiteur() {
    return (String) storedValueForKey(REC_DEBITEUR_KEY);
  }

  public void setRecDebiteur(String value) {
    takeStoredValueForKey(value, REC_DEBITEUR_KEY);
  }

  public String recImputtva() {
    return (String) storedValueForKey(REC_IMPUTTVA_KEY);
  }

  public void setRecImputtva(String value) {
    takeStoredValueForKey(value, REC_IMPUTTVA_KEY);
  }

  public String recLibelle() {
    return (String) storedValueForKey(REC_LIBELLE_KEY);
  }

  public void setRecLibelle(String value) {
    takeStoredValueForKey(value, REC_LIBELLE_KEY);
  }

  public String recLigneBudgetaire() {
    return (String) storedValueForKey(REC_LIGNE_BUDGETAIRE_KEY);
  }

  public void setRecLigneBudgetaire(String value) {
    takeStoredValueForKey(value, REC_LIGNE_BUDGETAIRE_KEY);
  }

  public String recMonnaie() {
    return (String) storedValueForKey(REC_MONNAIE_KEY);
  }

  public void setRecMonnaie(String value) {
    takeStoredValueForKey(value, REC_MONNAIE_KEY);
  }

  public java.math.BigDecimal recMont() {
    return (java.math.BigDecimal) storedValueForKey(REC_MONT_KEY);
  }

  public void setRecMont(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_MONT_KEY);
  }

  public java.math.BigDecimal recMontantDisquette() {
    return (java.math.BigDecimal) storedValueForKey(REC_MONTANT_DISQUETTE_KEY);
  }

  public void setRecMontantDisquette(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_MONTANT_DISQUETTE_KEY);
  }

  public java.math.BigDecimal recMontTtc() {
    return (java.math.BigDecimal) storedValueForKey(REC_MONT_TTC_KEY);
  }

  public void setRecMontTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_MONT_TTC_KEY);
  }

  public java.math.BigDecimal recMonttva() {
    return (java.math.BigDecimal) storedValueForKey(REC_MONTTVA_KEY);
  }

  public void setRecMonttva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_MONTTVA_KEY);
  }

  public Integer recNum() {
    return (Integer) storedValueForKey(REC_NUM_KEY);
  }

  public void setRecNum(Integer value) {
    takeStoredValueForKey(value, REC_NUM_KEY);
  }

  public Integer recOrdre() {
    return (Integer) storedValueForKey(REC_ORDRE_KEY);
  }

  public void setRecOrdre(Integer value) {
    takeStoredValueForKey(value, REC_ORDRE_KEY);
  }

  public Integer recPiece() {
    return (Integer) storedValueForKey(REC_PIECE_KEY);
  }

  public void setRecPiece(Integer value) {
    takeStoredValueForKey(value, REC_PIECE_KEY);
  }

  public String recRef() {
    return (String) storedValueForKey(REC_REF_KEY);
  }

  public void setRecRef(String value) {
    takeStoredValueForKey(value, REC_REF_KEY);
  }

  public String recStat() {
    return (String) storedValueForKey(REC_STAT_KEY);
  }

  public void setRecStat(String value) {
    takeStoredValueForKey(value, REC_STAT_KEY);
  }

  public String recSuppression() {
    return (String) storedValueForKey(REC_SUPPRESSION_KEY);
  }

  public void setRecSuppression(String value) {
    takeStoredValueForKey(value, REC_SUPPRESSION_KEY);
  }

  public String recType() {
    return (String) storedValueForKey(REC_TYPE_KEY);
  }

  public void setRecType(String value) {
    takeStoredValueForKey(value, REC_TYPE_KEY);
  }

  public String recVirement() {
    return (String) storedValueForKey(REC_VIREMENT_KEY);
  }

  public void setRecVirement(String value) {
    takeStoredValueForKey(value, REC_VIREMENT_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice toExercice() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumFournis toFournis() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumFournis)storedValueForKey(TO_FOURNIS_KEY);
  }

  public void setToFournisRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumFournis value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumFournis oldValue = toFournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_FOURNIS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
  }

  public void setToGestionRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGestion oldValue = toGestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOModePaiement toModePaiement() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOModePaiement)storedValueForKey(TO_MODE_PAIEMENT_KEY);
  }

  public void setToModePaiementRelationship(org.cocktail.fwkcktlcompta.client.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOModePaiement oldValue = toModePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOModeRecouvrement toModeRecouvrement() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOModeRecouvrement)storedValueForKey(TO_MODE_RECOUVREMENT_KEY);
  }

  public void setToModeRecouvrementRelationship(org.cocktail.fwkcktlcompta.client.metier.EOModeRecouvrement value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOModeRecouvrement oldValue = toModeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODE_RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan toOrgan() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(TO_ORGAN_KEY);
  }

  public void setToOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = toOrgan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan toOrganInterne() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(TO_ORGAN_INTERNE_KEY);
  }

  public void setToOrganInterneRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = toOrganInterne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORGAN_INTERNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORGAN_INTERNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer toPlanComptableExer() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer)storedValueForKey(TO_PLAN_COMPTABLE_EXER_KEY);
  }

  public void setToPlanComptableExerRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer oldValue = toPlanComptableExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_EXER_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecetteCtrlPlanco toRecetteCtrlPlanco() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecetteCtrlPlanco)storedValueForKey(TO_RECETTE_CTRL_PLANCO_KEY);
  }

  public void setToRecetteCtrlPlancoRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecetteCtrlPlanco value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecetteCtrlPlanco oldValue = toRecetteCtrlPlanco();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RECETTE_CTRL_PLANCO_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RECETTE_CTRL_PLANCO_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib toRib() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib)storedValueForKey(TO_RIB_KEY);
  }

  public void setToRibRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib oldValue = toRib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RIB_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOTitre toTitre() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOTitre)storedValueForKey(TO_TITRE_KEY);
  }

  public void setToTitreRelationship(org.cocktail.fwkcktlcompta.client.metier.EOTitre value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOTitre oldValue = toTitre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TITRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur toUtilisateur() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
  }

  public void setToUtilisateurRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur oldValue = toUtilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
    }
  }
  
  public NSArray toTitreDetailEcritures() {
    return (NSArray)storedValueForKey(TO_TITRE_DETAIL_ECRITURES_KEY);
  }

  public NSArray toTitreDetailEcritures(EOQualifier qualifier) {
    return toTitreDetailEcritures(qualifier, null, false);
  }

  public NSArray toTitreDetailEcritures(EOQualifier qualifier, Boolean fetch) {
    return toTitreDetailEcritures(qualifier, null, fetch);
  }

  public NSArray toTitreDetailEcritures(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOTitreDetailEcriture.TO_RECETTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOTitreDetailEcriture.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toTitreDetailEcritures();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToTitreDetailEcrituresRelationship(org.cocktail.fwkcktlcompta.client.metier.EOTitreDetailEcriture object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_TITRE_DETAIL_ECRITURES_KEY);
  }

  public void removeFromToTitreDetailEcrituresRelationship(org.cocktail.fwkcktlcompta.client.metier.EOTitreDetailEcriture object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_TITRE_DETAIL_ECRITURES_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOTitreDetailEcriture createToTitreDetailEcrituresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_TitreDetailEcriture");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_TITRE_DETAIL_ECRITURES_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOTitreDetailEcriture) eo;
  }

  public void deleteToTitreDetailEcrituresRelationship(org.cocktail.fwkcktlcompta.client.metier.EOTitreDetailEcriture object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_TITRE_DETAIL_ECRITURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToTitreDetailEcrituresRelationships() {
    Enumeration objects = toTitreDetailEcritures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToTitreDetailEcrituresRelationship((org.cocktail.fwkcktlcompta.client.metier.EOTitreDetailEcriture)objects.nextElement());
    }
  }


  public static EORecette createFwkCktlCompta_Recette(EOEditingContext editingContext, NSTimestamp recDate
, String recLibelle
, String recMonnaie
, java.math.BigDecimal recMont
, java.math.BigDecimal recMontantDisquette
, java.math.BigDecimal recMontTtc
, java.math.BigDecimal recMonttva
, Integer recOrdre
, String recStat
, String recSuppression
, String recType
, org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice toExercice, org.cocktail.fwkcktlcompta.client.metier.EOGrhumFournis toFournis, org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion, org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer toPlanComptableExer, org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecetteCtrlPlanco toRecetteCtrlPlanco, org.cocktail.fwkcktlcompta.client.metier.EOTitre toTitre, org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur toUtilisateur) {
    EORecette eo = (EORecette) createAndInsertInstance(editingContext, _EORecette.ENTITY_NAME);    
		eo.setRecDate(recDate);
		eo.setRecLibelle(recLibelle);
		eo.setRecMonnaie(recMonnaie);
		eo.setRecMont(recMont);
		eo.setRecMontantDisquette(recMontantDisquette);
		eo.setRecMontTtc(recMontTtc);
		eo.setRecMonttva(recMonttva);
		eo.setRecOrdre(recOrdre);
		eo.setRecStat(recStat);
		eo.setRecSuppression(recSuppression);
		eo.setRecType(recType);
    eo.setToExerciceRelationship(toExercice);
    eo.setToFournisRelationship(toFournis);
    eo.setToGestionRelationship(toGestion);
    eo.setToPlanComptableExerRelationship(toPlanComptableExer);
    eo.setToRecetteCtrlPlancoRelationship(toRecetteCtrlPlanco);
    eo.setToTitreRelationship(toTitre);
    eo.setToUtilisateurRelationship(toUtilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EORecette.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EORecette.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EORecette creerInstance(EOEditingContext editingContext) {
		  		EORecette object = (EORecette)createAndInsertInstance(editingContext, _EORecette.ENTITY_NAME);
		  		return object;
			}


		
  	  public EORecette localInstanceIn(EOEditingContext editingContext) {
	  		return (EORecette)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EORecette localInstanceIn(EOEditingContext editingContext, EORecette eo) {
    EORecette localInstance = (eo == null) ? null : (EORecette)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORecette#localInstanceIn a la place.
   */
	public static EORecette localInstanceOf(EOEditingContext editingContext, EORecette eo) {
		return EORecette.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EORecette fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORecette fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORecette)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORecette)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORecette fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORecette eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORecette ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORecette fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
