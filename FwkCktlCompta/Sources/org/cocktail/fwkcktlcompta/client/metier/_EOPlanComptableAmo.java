// _EOPlanComptableAmo.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPlanComptableAmo.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPlanComptableAmo extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_PlanComptableAmo";
	public static final String ENTITY_TABLE_NAME = "maracuja.Plan_Comptable_amo";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pcoaId";

	public static final String PCOA_LIBELLE_KEY = "pcoaLibelle";
	public static final String PCOA_NUM_KEY = "pcoaNum";

// Attributs non visibles
	public static final String PCOA_ID_KEY = "pcoaId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String PCOA_LIBELLE_COLKEY = "pcoa_libelle";
	public static final String PCOA_NUM_COLKEY = "PCOa_NUM";

	public static final String PCOA_ID_COLKEY = "pcoa_id";
	public static final String TYET_ID_COLKEY = "tyet_id";


	// Relationships
	public static final String TO_TYPE_ETAT_KEY = "toTypeEtat";



	// Accessors methods
  public String pcoaLibelle() {
    return (String) storedValueForKey(PCOA_LIBELLE_KEY);
  }

  public void setPcoaLibelle(String value) {
    takeStoredValueForKey(value, PCOA_LIBELLE_KEY);
  }

  public String pcoaNum() {
    return (String) storedValueForKey(PCOA_NUM_KEY);
  }

  public void setPcoaNum(String value) {
    takeStoredValueForKey(value, PCOA_NUM_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat toTypeEtat() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat)storedValueForKey(TO_TYPE_ETAT_KEY);
  }

  public void setToTypeEtatRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat oldValue = toTypeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ETAT_KEY);
    }
  }
  

  public static EOPlanComptableAmo createFwkCktlCompta_PlanComptableAmo(EOEditingContext editingContext, String pcoaLibelle
, String pcoaNum
, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat toTypeEtat) {
    EOPlanComptableAmo eo = (EOPlanComptableAmo) createAndInsertInstance(editingContext, _EOPlanComptableAmo.ENTITY_NAME);    
		eo.setPcoaLibelle(pcoaLibelle);
		eo.setPcoaNum(pcoaNum);
    eo.setToTypeEtatRelationship(toTypeEtat);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPlanComptableAmo.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPlanComptableAmo.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOPlanComptableAmo creerInstance(EOEditingContext editingContext) {
		  		EOPlanComptableAmo object = (EOPlanComptableAmo)createAndInsertInstance(editingContext, _EOPlanComptableAmo.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOPlanComptableAmo localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPlanComptableAmo)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPlanComptableAmo localInstanceIn(EOEditingContext editingContext, EOPlanComptableAmo eo) {
    EOPlanComptableAmo localInstance = (eo == null) ? null : (EOPlanComptableAmo)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPlanComptableAmo#localInstanceIn a la place.
   */
	public static EOPlanComptableAmo localInstanceOf(EOEditingContext editingContext, EOPlanComptableAmo eo) {
		return EOPlanComptableAmo.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPlanComptableAmo fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPlanComptableAmo fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPlanComptableAmo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPlanComptableAmo)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPlanComptableAmo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPlanComptableAmo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPlanComptableAmo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPlanComptableAmo)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPlanComptableAmo fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPlanComptableAmo eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPlanComptableAmo ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPlanComptableAmo fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
