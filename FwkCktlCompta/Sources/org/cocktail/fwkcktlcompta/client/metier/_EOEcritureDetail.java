// _EOEcritureDetail.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEcritureDetail.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEcritureDetail extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_EcritureDetail";
	public static final String ENTITY_TABLE_NAME = "maracuja.Ecriture_Detail";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ecdOrdre";

	public static final String ECD_COMMENTAIRE_KEY = "ecdCommentaire";
	public static final String ECD_CREDIT_KEY = "ecdCredit";
	public static final String ECD_DEBIT_KEY = "ecdDebit";
	public static final String ECD_INDEX_KEY = "ecdIndex";
	public static final String ECD_LIBELLE_KEY = "ecdLibelle";
	public static final String ECD_MONTANT_KEY = "ecdMontant";
	public static final String ECD_POSTIT_KEY = "ecdPostit";
	public static final String ECD_RESTE_EMARGER_KEY = "ecdResteEmarger";
	public static final String ECD_SECONDAIRE_KEY = "ecdSecondaire";
	public static final String ECD_SENS_KEY = "ecdSens";

// Attributs non visibles
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String ECR_ORDRE_KEY = "ecrOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String ECD_COMMENTAIRE_COLKEY = "ecd_commentaire";
	public static final String ECD_CREDIT_COLKEY = "ecd_credit";
	public static final String ECD_DEBIT_COLKEY = "ecd_debit";
	public static final String ECD_INDEX_COLKEY = "ecd_index";
	public static final String ECD_LIBELLE_COLKEY = "ecd_libelle";
	public static final String ECD_MONTANT_COLKEY = "ecd_montant";
	public static final String ECD_POSTIT_COLKEY = "ecd_Postit";
	public static final String ECD_RESTE_EMARGER_COLKEY = "ecd_Reste_Emarger";
	public static final String ECD_SECONDAIRE_COLKEY = "ecd_Secondaire";
	public static final String ECD_SENS_COLKEY = "ecd_sens";

	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String ECD_ORDRE_COLKEY = "ecd_ORDRE";
	public static final String ECR_ORDRE_COLKEY = "ecr_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String TO_ACCORDS_CONTRAT_KEY = "toAccordsContrat";
	public static final String TO_BROUILLARD_DETAILS_KEY = "toBrouillardDetails";
	public static final String TO_ECRITURE_KEY = "toEcriture";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_GESTION_KEY = "toGestion";
	public static final String TO_PLAN_COMPTABLE_EXER_KEY = "toPlanComptableExer";



	// Accessors methods
  public String ecdCommentaire() {
    return (String) storedValueForKey(ECD_COMMENTAIRE_KEY);
  }

  public void setEcdCommentaire(String value) {
    takeStoredValueForKey(value, ECD_COMMENTAIRE_KEY);
  }

  public java.math.BigDecimal ecdCredit() {
    return (java.math.BigDecimal) storedValueForKey(ECD_CREDIT_KEY);
  }

  public void setEcdCredit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECD_CREDIT_KEY);
  }

  public java.math.BigDecimal ecdDebit() {
    return (java.math.BigDecimal) storedValueForKey(ECD_DEBIT_KEY);
  }

  public void setEcdDebit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECD_DEBIT_KEY);
  }

  public Integer ecdIndex() {
    return (Integer) storedValueForKey(ECD_INDEX_KEY);
  }

  public void setEcdIndex(Integer value) {
    takeStoredValueForKey(value, ECD_INDEX_KEY);
  }

  public String ecdLibelle() {
    return (String) storedValueForKey(ECD_LIBELLE_KEY);
  }

  public void setEcdLibelle(String value) {
    takeStoredValueForKey(value, ECD_LIBELLE_KEY);
  }

  public java.math.BigDecimal ecdMontant() {
    return (java.math.BigDecimal) storedValueForKey(ECD_MONTANT_KEY);
  }

  public void setEcdMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECD_MONTANT_KEY);
  }

  public String ecdPostit() {
    return (String) storedValueForKey(ECD_POSTIT_KEY);
  }

  public void setEcdPostit(String value) {
    takeStoredValueForKey(value, ECD_POSTIT_KEY);
  }

  public java.math.BigDecimal ecdResteEmarger() {
    return (java.math.BigDecimal) storedValueForKey(ECD_RESTE_EMARGER_KEY);
  }

  public void setEcdResteEmarger(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECD_RESTE_EMARGER_KEY);
  }

  public String ecdSecondaire() {
    return (String) storedValueForKey(ECD_SECONDAIRE_KEY);
  }

  public void setEcdSecondaire(String value) {
    takeStoredValueForKey(value, ECD_SECONDAIRE_KEY);
  }

  public String ecdSens() {
    return (String) storedValueForKey(ECD_SENS_KEY);
  }

  public void setEcdSens(String value) {
    takeStoredValueForKey(value, ECD_SENS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOAccordsContrat toAccordsContrat() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOAccordsContrat)storedValueForKey(TO_ACCORDS_CONTRAT_KEY);
  }

  public void setToAccordsContratRelationship(org.cocktail.fwkcktlcompta.client.metier.EOAccordsContrat value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOAccordsContrat oldValue = toAccordsContrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ACCORDS_CONTRAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ACCORDS_CONTRAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOEcriture toEcriture() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOEcriture)storedValueForKey(TO_ECRITURE_KEY);
  }

  public void setToEcritureRelationship(org.cocktail.fwkcktlcompta.client.metier.EOEcriture value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOEcriture oldValue = toEcriture();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ECRITURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ECRITURE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice toExercice() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
  }

  public void setToGestionRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGestion oldValue = toGestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer toPlanComptableExer() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer)storedValueForKey(TO_PLAN_COMPTABLE_EXER_KEY);
  }

  public void setToPlanComptableExerRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer oldValue = toPlanComptableExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_EXER_KEY);
    }
  }
  
  public NSArray toBrouillardDetails() {
    return (NSArray)storedValueForKey(TO_BROUILLARD_DETAILS_KEY);
  }

  public NSArray toBrouillardDetails(EOQualifier qualifier) {
    return toBrouillardDetails(qualifier, null, false);
  }

  public NSArray toBrouillardDetails(EOQualifier qualifier, Boolean fetch) {
    return toBrouillardDetails(qualifier, null, fetch);
  }

  public NSArray toBrouillardDetails(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOBrouillardDetail.TO_ECRITURE_DETAIL_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOBrouillardDetail.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toBrouillardDetails();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToBrouillardDetailsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOBrouillardDetail object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_BROUILLARD_DETAILS_KEY);
  }

  public void removeFromToBrouillardDetailsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOBrouillardDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BROUILLARD_DETAILS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOBrouillardDetail createToBrouillardDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_BrouillardDetail");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_BROUILLARD_DETAILS_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOBrouillardDetail) eo;
  }

  public void deleteToBrouillardDetailsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOBrouillardDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BROUILLARD_DETAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToBrouillardDetailsRelationships() {
    Enumeration objects = toBrouillardDetails().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToBrouillardDetailsRelationship((org.cocktail.fwkcktlcompta.client.metier.EOBrouillardDetail)objects.nextElement());
    }
  }


  public static EOEcritureDetail createFwkCktlCompta_EcritureDetail(EOEditingContext editingContext, Integer ecdIndex
, java.math.BigDecimal ecdMontant
, java.math.BigDecimal ecdResteEmarger
, String ecdSens
, org.cocktail.fwkcktlcompta.client.metier.EOEcriture toEcriture, org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice toExercice, org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion, org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer toPlanComptableExer) {
    EOEcritureDetail eo = (EOEcritureDetail) createAndInsertInstance(editingContext, _EOEcritureDetail.ENTITY_NAME);    
		eo.setEcdIndex(ecdIndex);
		eo.setEcdMontant(ecdMontant);
		eo.setEcdResteEmarger(ecdResteEmarger);
		eo.setEcdSens(ecdSens);
    eo.setToEcritureRelationship(toEcriture);
    eo.setToExerciceRelationship(toExercice);
    eo.setToGestionRelationship(toGestion);
    eo.setToPlanComptableExerRelationship(toPlanComptableExer);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOEcritureDetail.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOEcritureDetail.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOEcritureDetail creerInstance(EOEditingContext editingContext) {
		  		EOEcritureDetail object = (EOEcritureDetail)createAndInsertInstance(editingContext, _EOEcritureDetail.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOEcritureDetail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEcritureDetail)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOEcritureDetail localInstanceIn(EOEditingContext editingContext, EOEcritureDetail eo) {
    EOEcritureDetail localInstance = (eo == null) ? null : (EOEcritureDetail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOEcritureDetail#localInstanceIn a la place.
   */
	public static EOEcritureDetail localInstanceOf(EOEditingContext editingContext, EOEcritureDetail eo) {
		return EOEcritureDetail.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOEcritureDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEcritureDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEcritureDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEcritureDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEcritureDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEcritureDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEcritureDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEcritureDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOEcritureDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEcritureDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEcritureDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEcritureDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
