// _EOSepaSddEcheancier.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSepaSddEcheancier.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOSepaSddEcheancier extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddEcheancier";
	public static final String ENTITY_TABLE_NAME = "maracuja.SEPA_SDD_ECHEANCIER";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idSepaSddEcheancier";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_SEPA_SDD_MANDAT_KEY = "idSepaSddMandat";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";

// Attributs non visibles
	public static final String ID_SEPA_SDD_ECHEANCIER_KEY = "idSepaSddEcheancier";
	public static final String IS_SEPA_SDD_ORIGINE_KEY = "isSepaSddOrigine";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ID_SEPA_SDD_MANDAT_COLKEY = "ID_SEPA_SDD_MANDAT";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";

	public static final String ID_SEPA_SDD_ECHEANCIER_COLKEY = "ID_SEPA_SDD_ECHEANCIER";
	public static final String IS_SEPA_SDD_ORIGINE_COLKEY = "ID_SEPA_SDD_origine";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";


	// Relationships
	public static final String TO_MODIFICATEUR_KEY = "toModificateur";
	public static final String TO_SEPA_SDD_ECHEANCES_KEY = "toSepaSddEcheances";
	public static final String TO_SEPA_SDD_ECHEANCIER_ECDS_KEY = "toSepaSddEcheancierEcds";
	public static final String TO_SEPA_SDD_MANDAT_KEY = "toSepaSddMandat";
	public static final String TO_SEPA_SDD_ORIGINE_KEY = "toSepaSddOrigine";



	// Accessors methods
  public String dCreation() {
    return (String) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(String value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public String dModification() {
    return (String) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(String value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer idSepaSddMandat() {
    return (Integer) storedValueForKey(ID_SEPA_SDD_MANDAT_KEY);
  }

  public void setIdSepaSddMandat(Integer value) {
    takeStoredValueForKey(value, ID_SEPA_SDD_MANDAT_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toModificateur() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_MODIFICATEUR_KEY);
  }

  public void setToModificateurRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toModificateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODIFICATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODIFICATEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat toSepaSddMandat() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat)storedValueForKey(TO_SEPA_SDD_MANDAT_KEY);
  }

  public void setToSepaSddMandatRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat oldValue = toSepaSddMandat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SEPA_SDD_MANDAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SEPA_SDD_MANDAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigine toSepaSddOrigine() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigine)storedValueForKey(TO_SEPA_SDD_ORIGINE_KEY);
  }

  public void setToSepaSddOrigineRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigine value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigine oldValue = toSepaSddOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SEPA_SDD_ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SEPA_SDD_ORIGINE_KEY);
    }
  }
  
  public NSArray toSepaSddEcheances() {
    return (NSArray)storedValueForKey(TO_SEPA_SDD_ECHEANCES_KEY);
  }

  public NSArray toSepaSddEcheances(EOQualifier qualifier) {
    return toSepaSddEcheances(qualifier, null, false);
  }

  public NSArray toSepaSddEcheances(EOQualifier qualifier, Boolean fetch) {
    return toSepaSddEcheances(qualifier, null, fetch);
  }

  public NSArray toSepaSddEcheances(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance.TO_SEPA_SDD_ECHEANCIER_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSepaSddEcheances();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSepaSddEcheancesRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCES_KEY);
  }

  public void removeFromToSepaSddEcheancesRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCES_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance createToSepaSddEcheancesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_SepaSddEcheance");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_SEPA_SDD_ECHEANCES_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance) eo;
  }

  public void deleteToSepaSddEcheancesRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToSepaSddEcheancesRelationships() {
    Enumeration objects = toSepaSddEcheances().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSepaSddEcheancesRelationship((org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance)objects.nextElement());
    }
  }

  public NSArray toSepaSddEcheancierEcds() {
    return (NSArray)storedValueForKey(TO_SEPA_SDD_ECHEANCIER_ECDS_KEY);
  }

  public NSArray toSepaSddEcheancierEcds(EOQualifier qualifier) {
    return toSepaSddEcheancierEcds(qualifier, null, false);
  }

  public NSArray toSepaSddEcheancierEcds(EOQualifier qualifier, Boolean fetch) {
    return toSepaSddEcheancierEcds(qualifier, null, fetch);
  }

  public NSArray toSepaSddEcheancierEcds(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierEcd.TO_SEPA_SDD_ECHEANCIER_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierEcd.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSepaSddEcheancierEcds();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSepaSddEcheancierEcdsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierEcd object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIER_ECDS_KEY);
  }

  public void removeFromToSepaSddEcheancierEcdsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierEcd object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIER_ECDS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierEcd createToSepaSddEcheancierEcdsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_SepaSddEcheancierEcd");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_SEPA_SDD_ECHEANCIER_ECDS_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierEcd) eo;
  }

  public void deleteToSepaSddEcheancierEcdsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierEcd object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIER_ECDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToSepaSddEcheancierEcdsRelationships() {
    Enumeration objects = toSepaSddEcheancierEcds().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSepaSddEcheancierEcdsRelationship((org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierEcd)objects.nextElement());
    }
  }


  public static EOSepaSddEcheancier createFwkCktlCompta_SepaSddEcheancier(EOEditingContext editingContext, String dCreation
, String dModification
, Integer idSepaSddMandat
, Integer persIdCreation
, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toModificateur, org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat toSepaSddMandat, org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigine toSepaSddOrigine) {
    EOSepaSddEcheancier eo = (EOSepaSddEcheancier) createAndInsertInstance(editingContext, _EOSepaSddEcheancier.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdSepaSddMandat(idSepaSddMandat);
		eo.setPersIdCreation(persIdCreation);
    eo.setToModificateurRelationship(toModificateur);
    eo.setToSepaSddMandatRelationship(toSepaSddMandat);
    eo.setToSepaSddOrigineRelationship(toSepaSddOrigine);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOSepaSddEcheancier.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOSepaSddEcheancier.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOSepaSddEcheancier creerInstance(EOEditingContext editingContext) {
		  		EOSepaSddEcheancier object = (EOSepaSddEcheancier)createAndInsertInstance(editingContext, _EOSepaSddEcheancier.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOSepaSddEcheancier localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSepaSddEcheancier)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOSepaSddEcheancier localInstanceIn(EOEditingContext editingContext, EOSepaSddEcheancier eo) {
    EOSepaSddEcheancier localInstance = (eo == null) ? null : (EOSepaSddEcheancier)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOSepaSddEcheancier#localInstanceIn a la place.
   */
	public static EOSepaSddEcheancier localInstanceOf(EOEditingContext editingContext, EOSepaSddEcheancier eo) {
		return EOSepaSddEcheancier.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOSepaSddEcheancier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOSepaSddEcheancier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSepaSddEcheancier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSepaSddEcheancier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSepaSddEcheancier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSepaSddEcheancier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSepaSddEcheancier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSepaSddEcheancier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOSepaSddEcheancier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSepaSddEcheancier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSepaSddEcheancier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSepaSddEcheancier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
