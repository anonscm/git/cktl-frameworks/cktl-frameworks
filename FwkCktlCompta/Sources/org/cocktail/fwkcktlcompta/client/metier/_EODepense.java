// _EODepense.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODepense.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EODepense extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Depense";
	public static final String ENTITY_TABLE_NAME = "maracuja.Depense";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "depId";

	public static final String DEP_ADRESSE_KEY = "depAdresse";
	public static final String DEP_DATE_COMPTA_KEY = "depDateCompta";
	public static final String DEP_DATE_FOURNIS_KEY = "depDateFournis";
	public static final String DEP_DATE_RECEPTION_KEY = "depDateReception";
	public static final String DEP_DATE_SERVICE_KEY = "depDateService";
	public static final String DEP_ETAT_KEY = "depEtat";
	public static final String DEP_FOURNISSEUR_KEY = "depFournisseur";
	public static final String DEP_HT_KEY = "depHt";
	public static final String DEP_LIGNE_BUDGETAIRE_KEY = "depLigneBudgetaire";
	public static final String DEP_LOT_KEY = "depLot";
	public static final String DEP_MARCHES_KEY = "depMarches";
	public static final String DEP_MONTANT_DISQUETTE_KEY = "depMontantDisquette";
	public static final String DEP_NOMENCLATURE_KEY = "depNomenclature";
	public static final String DEP_NUMERO_KEY = "depNumero";
	public static final String DEP_ORDRE_KEY = "depOrdre";
	public static final String DEP_REJET_KEY = "depRejet";
	public static final String DEP_SUPPRESSION_KEY = "depSuppression";
	public static final String DEP_TTC_KEY = "depTtc";
	public static final String DEP_TVA_KEY = "depTva";
	public static final String MAN_ORDRE_KEY = "manOrdre";

// Attributs non visibles
	public static final String DEP_ID_KEY = "depId";
	public static final String DEP_RIB_KEY = "depRib";
	public static final String ECD_ORDRE_EMA_KEY = "ecdOrdreEma";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MAN_ID_KEY = "manId";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String PCO_ORDRE_KEY = "pcoOrdre";
	public static final String TCD_ORDRE_KEY = "tcd_ordre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String DEP_ADRESSE_COLKEY = "dep_Adresse";
	public static final String DEP_DATE_COMPTA_COLKEY = "dep_Date_Compta";
	public static final String DEP_DATE_FOURNIS_COLKEY = "dep_Date_fournis";
	public static final String DEP_DATE_RECEPTION_COLKEY = "dep_Date_Reception";
	public static final String DEP_DATE_SERVICE_COLKEY = "dep_Date_Service";
	public static final String DEP_ETAT_COLKEY = "dep_Etat";
	public static final String DEP_FOURNISSEUR_COLKEY = "dep_Fournisseur";
	public static final String DEP_HT_COLKEY = "dep_Ht";
	public static final String DEP_LIGNE_BUDGETAIRE_COLKEY = "dep_Ligne_Budgetaire";
	public static final String DEP_LOT_COLKEY = "dep_Lot";
	public static final String DEP_MARCHES_COLKEY = "dep_Marches";
	public static final String DEP_MONTANT_DISQUETTE_COLKEY = "dep_Montant_Disquette";
	public static final String DEP_NOMENCLATURE_COLKEY = "dep_Nomenclature";
	public static final String DEP_NUMERO_COLKEY = "dep_Numero";
	public static final String DEP_ORDRE_COLKEY = "dep_ORDRE";
	public static final String DEP_REJET_COLKEY = "dep_Rejet";
	public static final String DEP_SUPPRESSION_COLKEY = "dep_Suppression";
	public static final String DEP_TTC_COLKEY = "dep_Ttc";
	public static final String DEP_TVA_COLKEY = "dep_Tva";
	public static final String MAN_ORDRE_COLKEY = "MAN_ORDRE";

	public static final String DEP_ID_COLKEY = "dep_id";
	public static final String DEP_RIB_COLKEY = "dep_Rib";
	public static final String ECD_ORDRE_EMA_COLKEY = "ecd_ordre_ema";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "fou_Ordre";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String MAN_ID_COLKEY = "man_id";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String ORG_ORDRE_COLKEY = "org_ordre";
	public static final String PCO_ORDRE_COLKEY = "PCO_ORDRE";
	public static final String TCD_ORDRE_COLKEY = "tcd_ordre";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_FOURNIS_KEY = "toFournis";
	public static final String TO_GESTION_KEY = "toGestion";
	public static final String TO_MANDAT_KEY = "toMandat";
	public static final String TO_MODE_PAIEMENT_KEY = "toModePaiement";
	public static final String TO_ORGAN_KEY = "toOrgan";
	public static final String TO_PLAN_COMPTABLE_EXER_KEY = "toPlanComptableExer";
	public static final String TO_RIB_KEY = "toRib";
	public static final String TO_TYPE_CREDIT_KEY = "toTypeCredit";
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";



	// Accessors methods
  public String depAdresse() {
    return (String) storedValueForKey(DEP_ADRESSE_KEY);
  }

  public void setDepAdresse(String value) {
    takeStoredValueForKey(value, DEP_ADRESSE_KEY);
  }

  public NSTimestamp depDateCompta() {
    return (NSTimestamp) storedValueForKey(DEP_DATE_COMPTA_KEY);
  }

  public void setDepDateCompta(NSTimestamp value) {
    takeStoredValueForKey(value, DEP_DATE_COMPTA_KEY);
  }

  public NSTimestamp depDateFournis() {
    return (NSTimestamp) storedValueForKey(DEP_DATE_FOURNIS_KEY);
  }

  public void setDepDateFournis(NSTimestamp value) {
    takeStoredValueForKey(value, DEP_DATE_FOURNIS_KEY);
  }

  public NSTimestamp depDateReception() {
    return (NSTimestamp) storedValueForKey(DEP_DATE_RECEPTION_KEY);
  }

  public void setDepDateReception(NSTimestamp value) {
    takeStoredValueForKey(value, DEP_DATE_RECEPTION_KEY);
  }

  public NSTimestamp depDateService() {
    return (NSTimestamp) storedValueForKey(DEP_DATE_SERVICE_KEY);
  }

  public void setDepDateService(NSTimestamp value) {
    takeStoredValueForKey(value, DEP_DATE_SERVICE_KEY);
  }

  public String depEtat() {
    return (String) storedValueForKey(DEP_ETAT_KEY);
  }

  public void setDepEtat(String value) {
    takeStoredValueForKey(value, DEP_ETAT_KEY);
  }

  public String depFournisseur() {
    return (String) storedValueForKey(DEP_FOURNISSEUR_KEY);
  }

  public void setDepFournisseur(String value) {
    takeStoredValueForKey(value, DEP_FOURNISSEUR_KEY);
  }

  public java.math.BigDecimal depHt() {
    return (java.math.BigDecimal) storedValueForKey(DEP_HT_KEY);
  }

  public void setDepHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_HT_KEY);
  }

  public String depLigneBudgetaire() {
    return (String) storedValueForKey(DEP_LIGNE_BUDGETAIRE_KEY);
  }

  public void setDepLigneBudgetaire(String value) {
    takeStoredValueForKey(value, DEP_LIGNE_BUDGETAIRE_KEY);
  }

  public String depLot() {
    return (String) storedValueForKey(DEP_LOT_KEY);
  }

  public void setDepLot(String value) {
    takeStoredValueForKey(value, DEP_LOT_KEY);
  }

  public String depMarches() {
    return (String) storedValueForKey(DEP_MARCHES_KEY);
  }

  public void setDepMarches(String value) {
    takeStoredValueForKey(value, DEP_MARCHES_KEY);
  }

  public java.math.BigDecimal depMontantDisquette() {
    return (java.math.BigDecimal) storedValueForKey(DEP_MONTANT_DISQUETTE_KEY);
  }

  public void setDepMontantDisquette(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_MONTANT_DISQUETTE_KEY);
  }

  public String depNomenclature() {
    return (String) storedValueForKey(DEP_NOMENCLATURE_KEY);
  }

  public void setDepNomenclature(String value) {
    takeStoredValueForKey(value, DEP_NOMENCLATURE_KEY);
  }

  public String depNumero() {
    return (String) storedValueForKey(DEP_NUMERO_KEY);
  }

  public void setDepNumero(String value) {
    takeStoredValueForKey(value, DEP_NUMERO_KEY);
  }

  public Integer depOrdre() {
    return (Integer) storedValueForKey(DEP_ORDRE_KEY);
  }

  public void setDepOrdre(Integer value) {
    takeStoredValueForKey(value, DEP_ORDRE_KEY);
  }

  public String depRejet() {
    return (String) storedValueForKey(DEP_REJET_KEY);
  }

  public void setDepRejet(String value) {
    takeStoredValueForKey(value, DEP_REJET_KEY);
  }

  public String depSuppression() {
    return (String) storedValueForKey(DEP_SUPPRESSION_KEY);
  }

  public void setDepSuppression(String value) {
    takeStoredValueForKey(value, DEP_SUPPRESSION_KEY);
  }

  public java.math.BigDecimal depTtc() {
    return (java.math.BigDecimal) storedValueForKey(DEP_TTC_KEY);
  }

  public void setDepTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_TTC_KEY);
  }

  public java.math.BigDecimal depTva() {
    return (java.math.BigDecimal) storedValueForKey(DEP_TVA_KEY);
  }

  public void setDepTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_TVA_KEY);
  }

  public Integer manOrdre() {
    return (Integer) storedValueForKey(MAN_ORDRE_KEY);
  }

  public void setManOrdre(Integer value) {
    takeStoredValueForKey(value, MAN_ORDRE_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFournis() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFournis)storedValueForKey(TO_FOURNIS_KEY);
  }

  public void setToFournisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOFournis oldValue = toFournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_FOURNIS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
  }

  public void setToGestionRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGestion oldValue = toGestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOMandat toMandat() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOMandat)storedValueForKey(TO_MANDAT_KEY);
  }

  public void setToMandatRelationship(org.cocktail.fwkcktlcompta.client.metier.EOMandat value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOMandat oldValue = toMandat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MANDAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MANDAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOModePaiement toModePaiement() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOModePaiement)storedValueForKey(TO_MODE_PAIEMENT_KEY);
  }

  public void setToModePaiementRelationship(org.cocktail.fwkcktlcompta.client.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOModePaiement oldValue = toModePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan toOrgan() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(TO_ORGAN_KEY);
  }

  public void setToOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = toOrgan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer toPlanComptableExer() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer)storedValueForKey(TO_PLAN_COMPTABLE_EXER_KEY);
  }

  public void setToPlanComptableExerRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer oldValue = toPlanComptableExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_EXER_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORib toRib() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORib)storedValueForKey(TO_RIB_KEY);
  }

  public void setToRibRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORib value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORib oldValue = toRib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RIB_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit toTypeCredit() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)storedValueForKey(TO_TYPE_CREDIT_KEY);
  }

  public void setToTypeCreditRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit oldValue = toTypeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CREDIT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateur() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
  }

  public void setToUtilisateurRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = toUtilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
    }
  }
  

  public static EODepense createFwkCktlCompta_Depense(EOEditingContext editingContext, String depEtat
, String depFournisseur
, java.math.BigDecimal depHt
, String depLigneBudgetaire
, java.math.BigDecimal depMontantDisquette
, String depNumero
, Integer depOrdre
, String depSuppression
, java.math.BigDecimal depTtc
, java.math.BigDecimal depTva
, Integer manOrdre
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice, org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFournis, org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion, org.cocktail.fwkcktlcompta.client.metier.EOMandat toMandat, org.cocktail.fwkcktlcompta.client.metier.EOModePaiement toModePaiement, org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer toPlanComptableExer, org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateur) {
    EODepense eo = (EODepense) createAndInsertInstance(editingContext, _EODepense.ENTITY_NAME);    
		eo.setDepEtat(depEtat);
		eo.setDepFournisseur(depFournisseur);
		eo.setDepHt(depHt);
		eo.setDepLigneBudgetaire(depLigneBudgetaire);
		eo.setDepMontantDisquette(depMontantDisquette);
		eo.setDepNumero(depNumero);
		eo.setDepOrdre(depOrdre);
		eo.setDepSuppression(depSuppression);
		eo.setDepTtc(depTtc);
		eo.setDepTva(depTva);
		eo.setManOrdre(manOrdre);
    eo.setToExerciceRelationship(toExercice);
    eo.setToFournisRelationship(toFournis);
    eo.setToGestionRelationship(toGestion);
    eo.setToMandatRelationship(toMandat);
    eo.setToModePaiementRelationship(toModePaiement);
    eo.setToPlanComptableExerRelationship(toPlanComptableExer);
    eo.setToUtilisateurRelationship(toUtilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EODepense.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EODepense.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EODepense creerInstance(EOEditingContext editingContext) {
		  		EODepense object = (EODepense)createAndInsertInstance(editingContext, _EODepense.ENTITY_NAME);
		  		return object;
			}


		
  	  public EODepense localInstanceIn(EOEditingContext editingContext) {
	  		return (EODepense)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EODepense localInstanceIn(EOEditingContext editingContext, EODepense eo) {
    EODepense localInstance = (eo == null) ? null : (EODepense)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EODepense#localInstanceIn a la place.
   */
	public static EODepense localInstanceOf(EOEditingContext editingContext, EODepense eo) {
		return EODepense.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EODepense fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODepense fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODepense eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODepense)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODepense fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODepense fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODepense eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODepense)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EODepense fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODepense eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODepense ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODepense fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
