/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.client.metier;

import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumAdresse;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumRib;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.common.entities.ITracable;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddParam;
import org.cocktail.fwkcktlcompta.common.sepasdd.rules.SepaSddMandatRule;
import org.cocktail.fwkcktlcompta.common.util.WebObjectConversionUtil;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOSepaSddMandat extends _EOSepaSddMandat implements ISepaSddMandat, ITracable {

	public EOSepaSddMandat() {
		super();
	}

	public String nomPrenomDebiteur() {
		if (toDebiteurPersonne() == null) {
			return null;
		}
		return toDebiteurPersonne().getNomAndPrenom();
	}

	public String bicIbanDebiteur() {
		if (toDebiteurRib() == null) {
			return null;
		}
		return toDebiteurRib().bicEtIban();
	}

	public List<ISepaSddEcheancier> echeanciers() {
		return WebObjectConversionUtil.asList(toSepaSddEcheanciers());
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 *
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele à partir des factories.
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		SepaSddMandatRule.getSharedInstance().validateSepaSddMandat(this);
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
	}

	public Integer idMandat() {
	    final EOKeyGlobalID gid = (EOKeyGlobalID) toEnterpriseObject().editingContext().globalIDForObject(toEnterpriseObject());
	    return (Integer) gid.keyValuesArray().objectAtIndex(0);
	}
	
	public String dateCreation() {
		return dCreation();
	}

	public void setDateCreation(String dateCreation) {
		setDCreation(dateCreation);
	}

	public String dateModification() {
		return dModification();
	}

	public void setDateModification(String dateModification) {
		setDModification(dateModification);
	}

	public String tiersDebiteurNomPrenom() {
		if (toTiersDebiteurPersonne() == null) {
			return null;
		}
		return toTiersDebiteurPersonne().getNomAndPrenom();
	}

	/**
	 * Retourne le client (debiteur) à l'origine du paiement.
	 * Il s'agit du Tiers Debiteur si renseigné ; du debiteur le cas echeant.
	 * @return le tiers debiteur si non vide ; le debiteur sinon.
	 */
	public IGrhumPersonne clientPersonne() {
		if (toTiersDebiteurPersonne() != null) {
			return toTiersDebiteurPersonne();
		}

		return toDebiteurPersonne();
	}

	public Boolean isRecurrent() {
		return SepaSddMandatRule.getSharedInstance().isMandatRecurrent(this);

	}

	public NSArray getEcheanciersTries(boolean fetch) {
		return toSepaSddEcheanciers(null, new NSArray(new Object[] {
			ISepaSddEcheancier.SORT_DATE_MODIFICATION_DESC
		}), fetch);
	}

	public void setToTypeEtatRelationship(IJefyAdminTypeEtat typeEtat) {
		super.setToTypeEtatRelationship((EOJefyAdminTypeEtat) typeEtat);
	}

	public void setToModificateurRelationship(IGrhumPersonne personne) {
		super.setToModificateurRelationship((EOGrhumPersonne) personne);
	}

	public void setToCreancierPersonneRelationship(IGrhumPersonne personne) {
		super.setToCreancierPersonneRelationship((EOGrhumPersonne) personne);
	}

	public EOEnterpriseObject toEnterpriseObject() {
		return (EOEnterpriseObject) this;
	}

	public void setToDebiteurPersonneRelationship(IGrhumPersonne personne) {
		super.setToDebiteurPersonneRelationship((EOGrhumPersonne) personne);
	}

	public void setToDebiteurAdresseRelationship(IGrhumAdresse adresse) {
		super.setToDebiteurAdresseRelationship((EOGrhumAdresse) adresse);
	}

	public void setToTiersDebiteurPersonneRelationship(IGrhumPersonne personne) {
		super.setToTiersDebiteurPersonneRelationship((EOGrhumPersonne) personne);
	}

    public void setToDebiteurRibRelationship(IGrhumRib rib) {
        super.setToDebiteurRibRelationship((EOGrhumRib) rib);
    }

    public void setToSepaSddParamRelationship(ISepaSddParam param) {
        super.setToSepaSddParamRelationship((EOSepaSddParam) param);
    }
    
}
