// _EOSepaSddMandatHisto.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSepaSddMandatHisto.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOSepaSddMandatHisto extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddMandatHisto";
	public static final String ENTITY_TABLE_NAME = "maracuja.SEPA_SDD_MANDAT_HISTO";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idSepaSddMandatHisto";

	public static final String CREANCIER_ICS_KEY = "creancierIcs";
	public static final String CREANCIER_NOM_KEY = "creancierNom";
	public static final String C_TYPE_PRELEVEMENT_KEY = "cTypePrelevement";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEBITEUR_BIC_KEY = "debiteurBic";
	public static final String DEBITEUR_IBAN_KEY = "debiteurIban";
	public static final String DEBITEUR_NOM_KEY = "debiteurNom";
	public static final String LIBELLE_KEY = "libelle";
	public static final String RUM_KEY = "rum";

// Attributs non visibles
	public static final String CREANCIER_ID_KEY = "creancierId";
	public static final String DEBITEUR_ID_KEY = "debiteurId";
	public static final String ID_SEPA_SDD_MANDAT_KEY = "idSepaSddMandat";
	public static final String ID_SEPA_SDD_MANDAT_HISTO_KEY = "idSepaSddMandatHisto";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String CREANCIER_ICS_COLKEY = "CREANCIER_ICS";
	public static final String CREANCIER_NOM_COLKEY = "CREANCIER_NOM";
	public static final String C_TYPE_PRELEVEMENT_COLKEY = "C_TYPE_PRELEVEMENT";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String DEBITEUR_BIC_COLKEY = "DEBITEUR_BIC";
	public static final String DEBITEUR_IBAN_COLKEY = "DEBITEUR_IBAN";
	public static final String DEBITEUR_NOM_COLKEY = "DEBITEUR_NOM";
	public static final String LIBELLE_COLKEY = "LIBELLE";
	public static final String RUM_COLKEY = "RUM";

	public static final String CREANCIER_ID_COLKEY = "CREANCIER_ID";
	public static final String DEBITEUR_ID_COLKEY = "DEBITEUR_ID";
	public static final String ID_SEPA_SDD_MANDAT_COLKEY = "ID_SEPA_SDD_MANDAT";
	public static final String ID_SEPA_SDD_MANDAT_HISTO_COLKEY = "ID_SEPA_SDD_MANDAT_HISTO";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String TO_CREANCIER_PERSONNE_KEY = "toCreancierPersonne";
	public static final String TO_DEBITEUR_PERSONNE_KEY = "toDebiteurPersonne";
	public static final String TO_MODIFICATEUR_KEY = "toModificateur";
	public static final String TO_SEPA_SDD_MANDAT_KEY = "toSepaSddMandat";
	public static final String TO_TYPE_ETAT_KEY = "toTypeEtat";



	// Accessors methods
  public String creancierIcs() {
    return (String) storedValueForKey(CREANCIER_ICS_KEY);
  }

  public void setCreancierIcs(String value) {
    takeStoredValueForKey(value, CREANCIER_ICS_KEY);
  }

  public String creancierNom() {
    return (String) storedValueForKey(CREANCIER_NOM_KEY);
  }

  public void setCreancierNom(String value) {
    takeStoredValueForKey(value, CREANCIER_NOM_KEY);
  }

  public String cTypePrelevement() {
    return (String) storedValueForKey(C_TYPE_PRELEVEMENT_KEY);
  }

  public void setCTypePrelevement(String value) {
    takeStoredValueForKey(value, C_TYPE_PRELEVEMENT_KEY);
  }

  public String dCreation() {
    return (String) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(String value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public String debiteurBic() {
    return (String) storedValueForKey(DEBITEUR_BIC_KEY);
  }

  public void setDebiteurBic(String value) {
    takeStoredValueForKey(value, DEBITEUR_BIC_KEY);
  }

  public String debiteurIban() {
    return (String) storedValueForKey(DEBITEUR_IBAN_KEY);
  }

  public void setDebiteurIban(String value) {
    takeStoredValueForKey(value, DEBITEUR_IBAN_KEY);
  }

  public String debiteurNom() {
    return (String) storedValueForKey(DEBITEUR_NOM_KEY);
  }

  public void setDebiteurNom(String value) {
    takeStoredValueForKey(value, DEBITEUR_NOM_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    takeStoredValueForKey(value, LIBELLE_KEY);
  }

  public String rum() {
    return (String) storedValueForKey(RUM_KEY);
  }

  public void setRum(String value) {
    takeStoredValueForKey(value, RUM_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toCreancierPersonne() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_CREANCIER_PERSONNE_KEY);
  }

  public void setToCreancierPersonneRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toCreancierPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CREANCIER_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CREANCIER_PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toDebiteurPersonne() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_DEBITEUR_PERSONNE_KEY);
  }

  public void setToDebiteurPersonneRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toDebiteurPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEBITEUR_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEBITEUR_PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toModificateur() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_MODIFICATEUR_KEY);
  }

  public void setToModificateurRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toModificateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODIFICATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODIFICATEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat toSepaSddMandat() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat)storedValueForKey(TO_SEPA_SDD_MANDAT_KEY);
  }

  public void setToSepaSddMandatRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat oldValue = toSepaSddMandat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SEPA_SDD_MANDAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SEPA_SDD_MANDAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat toTypeEtat() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat)storedValueForKey(TO_TYPE_ETAT_KEY);
  }

  public void setToTypeEtatRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat oldValue = toTypeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ETAT_KEY);
    }
  }
  

  public static EOSepaSddMandatHisto createFwkCktlCompta_SepaSddMandatHisto(EOEditingContext editingContext, String creancierIcs
, String creancierNom
, String cTypePrelevement
, String dCreation
, String debiteurBic
, String debiteurIban
, String debiteurNom
, String libelle
, String rum
, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toCreancierPersonne, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toDebiteurPersonne, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toModificateur, org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat toSepaSddMandat, org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat toTypeEtat) {
    EOSepaSddMandatHisto eo = (EOSepaSddMandatHisto) createAndInsertInstance(editingContext, _EOSepaSddMandatHisto.ENTITY_NAME);    
		eo.setCreancierIcs(creancierIcs);
		eo.setCreancierNom(creancierNom);
		eo.setCTypePrelevement(cTypePrelevement);
		eo.setDCreation(dCreation);
		eo.setDebiteurBic(debiteurBic);
		eo.setDebiteurIban(debiteurIban);
		eo.setDebiteurNom(debiteurNom);
		eo.setLibelle(libelle);
		eo.setRum(rum);
    eo.setToCreancierPersonneRelationship(toCreancierPersonne);
    eo.setToDebiteurPersonneRelationship(toDebiteurPersonne);
    eo.setToModificateurRelationship(toModificateur);
    eo.setToSepaSddMandatRelationship(toSepaSddMandat);
    eo.setToTypeEtatRelationship(toTypeEtat);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOSepaSddMandatHisto.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOSepaSddMandatHisto.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOSepaSddMandatHisto creerInstance(EOEditingContext editingContext) {
		  		EOSepaSddMandatHisto object = (EOSepaSddMandatHisto)createAndInsertInstance(editingContext, _EOSepaSddMandatHisto.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOSepaSddMandatHisto localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSepaSddMandatHisto)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOSepaSddMandatHisto localInstanceIn(EOEditingContext editingContext, EOSepaSddMandatHisto eo) {
    EOSepaSddMandatHisto localInstance = (eo == null) ? null : (EOSepaSddMandatHisto)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOSepaSddMandatHisto#localInstanceIn a la place.
   */
	public static EOSepaSddMandatHisto localInstanceOf(EOEditingContext editingContext, EOSepaSddMandatHisto eo) {
		return EOSepaSddMandatHisto.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOSepaSddMandatHisto fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOSepaSddMandatHisto fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSepaSddMandatHisto eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSepaSddMandatHisto)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSepaSddMandatHisto fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSepaSddMandatHisto fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSepaSddMandatHisto eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSepaSddMandatHisto)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOSepaSddMandatHisto fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSepaSddMandatHisto eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSepaSddMandatHisto ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSepaSddMandatHisto fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
