/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.client.metier;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.IFwkCktlComptaParam;
import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IModeRecouvrement;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.client.metier.EOModeRecouvrement;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOCoriandreInscription extends _EOCoriandreInscription implements ISepaSddOrigineEntity {

    private static EOGlobalID mrCoriandreGlobalId;

    /** A initialiser par l'application */
    public static String CORIANDRE_ECHEANCIER_MODE_RECOUVREMENT_CODE = null;
    
    public EOCoriandreInscription() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    public Integer origineExercice() {
        return toExercice().exeExercice();
    }

    public String origineDebiteurNomComplet() {
        return toPersonne().getNomAndPrenomAndCode();
    }

    public Integer origineIdentifiant() {
        final EOKeyGlobalID gid = (EOKeyGlobalID) editingContext().globalIDForObject(this);
        return (Integer) gid.keyValuesArray().objectAtIndex(0);
    }

    public String origineNumero() {
        return idPaiement().toString();
    }

    public String origineLibelle() {
        return "Inscription Etudiant num. " + etudNumero().intValue() + " / " + idPaiement();
    }

    public BigDecimal origineMontant() {
        return montantPaiement();
    }

    public String getOrigineLibelleComplet() {
        return "Inscription Etudiant num. " + etudNumero().intValue() + " / " + idPaiement();
    }

    public String getOrigineLibellePourDebiteur() {
        return getOrigineLibelleComplet();
    }

    public IGrhumPersonne origineDebiteurPersonne() {
        return toPersonne();
    }

    public IModeRecouvrement modeRecouvrement() {
        return modeRecouvrementScolarix();
    }

    public void onEcheanceRejetee(ISepaSddEcheance echeance) {

    }

    public void onEcheancePrelevee(ISepaSddEcheance echeance) {

    }

    public List<IEcritureDetail> ecritureDetailsContrepartiePriseEnCharge() {
        return null;
    }

    public EOModeRecouvrement modeRecouvrementScolarix() {
        if (mrCoriandreGlobalId == null) {
            String code = CORIANDRE_ECHEANCIER_MODE_RECOUVREMENT_CODE;
            if (code == null) {
                throw new RuntimeException("@developper : Champ statique EOScolarixInscription.SCOLARIX_ECHEANCIER_MODE_RECOUVREMENT_CODE non renseigné. Vous devez le renseigner lors de l'initialisation de l'application.");
            }
            EOExercice exercice = EOExercice.getExerciceOuvert(editingContext());
            EOQualifier qual1 = new EOKeyValueQualifier(EOModeRecouvrement.MOD_CODE_KEY, EOQualifier.QualifierOperatorEqual, code);
            EOQualifier qual2 = new EOKeyValueQualifier(EOModeRecouvrement.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
            EOModeRecouvrement tmp = EOModeRecouvrement.fetchByQualifier(editingContext(), new EOAndQualifier(new NSArray(new Object[] {
                    qual1, qual2
            })));
            if (tmp == null) {
                throw new RuntimeException("Impossible de trouver le mode de recouvrement " + code + " pour l'exercice " + exercice.exeExercice() + ". Vérifiez la valeur du parametre " + IFwkCktlComptaParam.SEPASDDMANDAT_SCOLARIX_MODERECOUVREMENTCODEDEFAUT
                        + ", ceui-ci doit indiquer un code de mode de recouvrement valide, de type ECHEANCHIER.");
            }
            mrCoriandreGlobalId = editingContext().globalIDForObject(tmp);
        }

        if (mrCoriandreGlobalId != null) {
            return (EOModeRecouvrement) editingContext().faultForGlobalID(mrCoriandreGlobalId, editingContext());
        }
        return null;
    }
    
}
