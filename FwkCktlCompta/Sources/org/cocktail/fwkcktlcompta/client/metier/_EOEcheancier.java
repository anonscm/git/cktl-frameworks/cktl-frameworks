// _EOEcheancier.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEcheancier.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEcheancier extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Echeancier";
	public static final String ENTITY_TABLE_NAME = "maracuja.Echeancier";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "echeEcheancierOrdre";

	public static final String AUTORIS_SIGNEE_KEY = "autorisSignee";
	public static final String DATE1ER_PRELEVEMENT_KEY = "date1erPrelevement";
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_MODIF_KEY = "dateModif";
	public static final String ETAT_PRELEVEMENT_KEY = "etatPrelevement";
	public static final String LIBELLE_KEY = "libelle";
	public static final String MONTANT_KEY = "montant";
	public static final String MONTANT_EN_LETTRES_KEY = "montantEnLettres";
	public static final String NOMBRE_PRELEVEMENTS_KEY = "nombrePrelevements";
	public static final String NUMERO_INDEX_KEY = "numeroIndex";
	public static final String PRISE_EN_CHARGE_KEY = "priseEnCharge";
	public static final String REF_FACTURE_EXTERNE_KEY = "refFactureExterne";
	public static final String SUPPRIME_KEY = "supprime";

// Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String ECHE_ECHEANCIER_ORDRE_KEY = "echeEcheancierOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PERS_ID_KEY = "persId";
	public static final String REC_ID_KEY = "recId";

//Colonnes dans la base de donnees
	public static final String AUTORIS_SIGNEE_COLKEY = "ECHE_AUTORIS_SIGNEE";
	public static final String DATE1ER_PRELEVEMENT_COLKEY = "ECHE_DATE_1ERE_ECHEANCE";
	public static final String DATE_CREATION_COLKEY = "ECHE_DATE_CREATION";
	public static final String DATE_MODIF_COLKEY = "ECHE_DATE_MODIF";
	public static final String ETAT_PRELEVEMENT_COLKEY = "ECHE_ETAT_PRELEVEMENT";
	public static final String LIBELLE_COLKEY = "ECHE_LIBELLE";
	public static final String MONTANT_COLKEY = "ECHE_MONTANT";
	public static final String MONTANT_EN_LETTRES_COLKEY = "ECHE_MONTANT_EN_LETTRES";
	public static final String NOMBRE_PRELEVEMENTS_COLKEY = "ECHE_NOMBRE_ECHEANCES";
	public static final String NUMERO_INDEX_COLKEY = "ECHE_NUMERO_INDEX";
	public static final String PRISE_EN_CHARGE_COLKEY = "ECHE_PRISE_EN_CHARGE";
	public static final String REF_FACTURE_EXTERNE_COLKEY = "ECHE_REF_FACTURE_EXTERNE";
	public static final String SUPPRIME_COLKEY = "ECHE_SUPPRIME";

	public static final String BOR_ID_COLKEY = "bor_id";
	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String ECHE_ECHEANCIER_ORDRE_COLKEY = "ECHE_ECHEANCIER_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PERS_ID_COLKEY = "pers_Id";
	public static final String REC_ID_COLKEY = "REC_ID";


	// Relationships
	public static final String TO_BORDEREAU_KEY = "toBordereau";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_PERSONNE_KEY = "toPersonne";
	public static final String TO_PRELEVEMENTS_KEY = "toPrelevements";
	public static final String TO_RECETTE_KEY = "toRecette";



	// Accessors methods
  public String autorisSignee() {
    return (String) storedValueForKey(AUTORIS_SIGNEE_KEY);
  }

  public void setAutorisSignee(String value) {
    takeStoredValueForKey(value, AUTORIS_SIGNEE_KEY);
  }

  public NSTimestamp date1erPrelevement() {
    return (NSTimestamp) storedValueForKey(DATE1ER_PRELEVEMENT_KEY);
  }

  public void setDate1erPrelevement(NSTimestamp value) {
    takeStoredValueForKey(value, DATE1ER_PRELEVEMENT_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateModif() {
    return (NSTimestamp) storedValueForKey(DATE_MODIF_KEY);
  }

  public void setDateModif(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIF_KEY);
  }

  public String etatPrelevement() {
    return (String) storedValueForKey(ETAT_PRELEVEMENT_KEY);
  }

  public void setEtatPrelevement(String value) {
    takeStoredValueForKey(value, ETAT_PRELEVEMENT_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    takeStoredValueForKey(value, LIBELLE_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_KEY);
  }

  public String montantEnLettres() {
    return (String) storedValueForKey(MONTANT_EN_LETTRES_KEY);
  }

  public void setMontantEnLettres(String value) {
    takeStoredValueForKey(value, MONTANT_EN_LETTRES_KEY);
  }

  public Integer nombrePrelevements() {
    return (Integer) storedValueForKey(NOMBRE_PRELEVEMENTS_KEY);
  }

  public void setNombrePrelevements(Integer value) {
    takeStoredValueForKey(value, NOMBRE_PRELEVEMENTS_KEY);
  }

  public Integer numeroIndex() {
    return (Integer) storedValueForKey(NUMERO_INDEX_KEY);
  }

  public void setNumeroIndex(Integer value) {
    takeStoredValueForKey(value, NUMERO_INDEX_KEY);
  }

  public String priseEnCharge() {
    return (String) storedValueForKey(PRISE_EN_CHARGE_KEY);
  }

  public void setPriseEnCharge(String value) {
    takeStoredValueForKey(value, PRISE_EN_CHARGE_KEY);
  }

  public String refFactureExterne() {
    return (String) storedValueForKey(REF_FACTURE_EXTERNE_KEY);
  }

  public void setRefFactureExterne(String value) {
    takeStoredValueForKey(value, REF_FACTURE_EXTERNE_KEY);
  }

  public String supprime() {
    return (String) storedValueForKey(SUPPRIME_KEY);
  }

  public void setSupprime(String value) {
    takeStoredValueForKey(value, SUPPRIME_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOBordereau toBordereau() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOBordereau)storedValueForKey(TO_BORDEREAU_KEY);
  }

  public void setToBordereauRelationship(org.cocktail.fwkcktlcompta.client.metier.EOBordereau value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOBordereau oldValue = toBordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonne() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_PERSONNE_KEY);
  }

  public void setToPersonneRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EORecette toRecette() {
    return (org.cocktail.fwkcktlcompta.client.metier.EORecette)storedValueForKey(TO_RECETTE_KEY);
  }

  public void setToRecetteRelationship(org.cocktail.fwkcktlcompta.client.metier.EORecette value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EORecette oldValue = toRecette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RECETTE_KEY);
    }
  }
  
  public NSArray toPrelevements() {
    return (NSArray)storedValueForKey(TO_PRELEVEMENTS_KEY);
  }

  public NSArray toPrelevements(EOQualifier qualifier) {
    return toPrelevements(qualifier, null, false);
  }

  public NSArray toPrelevements(EOQualifier qualifier, Boolean fetch) {
    return toPrelevements(qualifier, null, fetch);
  }

  public NSArray toPrelevements(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOPrelevement.TO_ECHEANCIER_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOPrelevement.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPrelevements();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPrelevementsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPrelevement object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRELEVEMENTS_KEY);
  }

  public void removeFromToPrelevementsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPrelevement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRELEVEMENTS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOPrelevement createToPrelevementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_Prelevement");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRELEVEMENTS_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOPrelevement) eo;
  }

  public void deleteToPrelevementsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPrelevement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRELEVEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPrelevementsRelationships() {
    Enumeration objects = toPrelevements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPrelevementsRelationship((org.cocktail.fwkcktlcompta.client.metier.EOPrelevement)objects.nextElement());
    }
  }


  public static EOEcheancier createFwkCktlCompta_Echeancier(EOEditingContext editingContext, String autorisSignee
, NSTimestamp date1erPrelevement
, NSTimestamp dateCreation
, String libelle
, java.math.BigDecimal montant
, Integer nombrePrelevements
, Integer numeroIndex
, String priseEnCharge
, String supprime
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonne) {
    EOEcheancier eo = (EOEcheancier) createAndInsertInstance(editingContext, _EOEcheancier.ENTITY_NAME);    
		eo.setAutorisSignee(autorisSignee);
		eo.setDate1erPrelevement(date1erPrelevement);
		eo.setDateCreation(dateCreation);
		eo.setLibelle(libelle);
		eo.setMontant(montant);
		eo.setNombrePrelevements(nombrePrelevements);
		eo.setNumeroIndex(numeroIndex);
		eo.setPriseEnCharge(priseEnCharge);
		eo.setSupprime(supprime);
    eo.setToExerciceRelationship(toExercice);
    eo.setToPersonneRelationship(toPersonne);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOEcheancier.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOEcheancier.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOEcheancier creerInstance(EOEditingContext editingContext) {
		  		EOEcheancier object = (EOEcheancier)createAndInsertInstance(editingContext, _EOEcheancier.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOEcheancier localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEcheancier)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOEcheancier localInstanceIn(EOEditingContext editingContext, EOEcheancier eo) {
    EOEcheancier localInstance = (eo == null) ? null : (EOEcheancier)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOEcheancier#localInstanceIn a la place.
   */
	public static EOEcheancier localInstanceOf(EOEditingContext editingContext, EOEcheancier eo) {
		return EOEcheancier.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOEcheancier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEcheancier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEcheancier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEcheancier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEcheancier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEcheancier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEcheancier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEcheancier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOEcheancier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEcheancier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEcheancier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEcheancier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
