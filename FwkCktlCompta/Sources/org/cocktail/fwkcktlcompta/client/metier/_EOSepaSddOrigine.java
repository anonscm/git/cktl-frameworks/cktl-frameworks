// _EOSepaSddOrigine.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSepaSddOrigine.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOSepaSddOrigine extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddOrigine";
	public static final String ENTITY_TABLE_NAME = "maracuja.SEPA_SDD_ORIGINE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idSepaSddOrigine";

	public static final String ORIGINE_ID_KEY = "origineId";

// Attributs non visibles
	public static final String ID_SEPA_SDD_ORIGINE_KEY = "idSepaSddOrigine";
	public static final String ID_SEPA_SDD_ORIGINE_TYPE_KEY = "idSepaSddOrigineType";

//Colonnes dans la base de donnees
	public static final String ORIGINE_ID_COLKEY = "ORIGINE_ID";

	public static final String ID_SEPA_SDD_ORIGINE_COLKEY = "ID_SEPA_SDD_ORIGINE";
	public static final String ID_SEPA_SDD_ORIGINE_TYPE_COLKEY = "ID_SEPA_SDD_ORIGINE_TYPE";


	// Relationships
	public static final String TO_SEPA_SDD_ECHEANCIERS_KEY = "toSepaSddEcheanciers";
	public static final String TO_SEPA_SDD_ORIGINE_TYPE_KEY = "toSepaSddOrigineType";



	// Accessors methods
  public Integer origineId() {
    return (Integer) storedValueForKey(ORIGINE_ID_KEY);
  }

  public void setOrigineId(Integer value) {
    takeStoredValueForKey(value, ORIGINE_ID_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigineType toSepaSddOrigineType() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigineType)storedValueForKey(TO_SEPA_SDD_ORIGINE_TYPE_KEY);
  }

  public void setToSepaSddOrigineTypeRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigineType value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigineType oldValue = toSepaSddOrigineType();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SEPA_SDD_ORIGINE_TYPE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SEPA_SDD_ORIGINE_TYPE_KEY);
    }
  }
  
  public NSArray toSepaSddEcheanciers() {
    return (NSArray)storedValueForKey(TO_SEPA_SDD_ECHEANCIERS_KEY);
  }

  public NSArray toSepaSddEcheanciers(EOQualifier qualifier) {
    return toSepaSddEcheanciers(qualifier, null, false);
  }

  public NSArray toSepaSddEcheanciers(EOQualifier qualifier, Boolean fetch) {
    return toSepaSddEcheanciers(qualifier, null, fetch);
  }

  public NSArray toSepaSddEcheanciers(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier.TO_SEPA_SDD_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSepaSddEcheanciers();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSepaSddEcheanciersRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIERS_KEY);
  }

  public void removeFromToSepaSddEcheanciersRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIERS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier createToSepaSddEcheanciersRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_SepaSddEcheancier");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_SEPA_SDD_ECHEANCIERS_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier) eo;
  }

  public void deleteToSepaSddEcheanciersRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIERS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToSepaSddEcheanciersRelationships() {
    Enumeration objects = toSepaSddEcheanciers().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSepaSddEcheanciersRelationship((org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier)objects.nextElement());
    }
  }


  public static EOSepaSddOrigine createFwkCktlCompta_SepaSddOrigine(EOEditingContext editingContext, Integer origineId
, org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigineType toSepaSddOrigineType) {
    EOSepaSddOrigine eo = (EOSepaSddOrigine) createAndInsertInstance(editingContext, _EOSepaSddOrigine.ENTITY_NAME);    
		eo.setOrigineId(origineId);
    eo.setToSepaSddOrigineTypeRelationship(toSepaSddOrigineType);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOSepaSddOrigine.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOSepaSddOrigine.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOSepaSddOrigine creerInstance(EOEditingContext editingContext) {
		  		EOSepaSddOrigine object = (EOSepaSddOrigine)createAndInsertInstance(editingContext, _EOSepaSddOrigine.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOSepaSddOrigine localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSepaSddOrigine)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOSepaSddOrigine localInstanceIn(EOEditingContext editingContext, EOSepaSddOrigine eo) {
    EOSepaSddOrigine localInstance = (eo == null) ? null : (EOSepaSddOrigine)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOSepaSddOrigine#localInstanceIn a la place.
   */
	public static EOSepaSddOrigine localInstanceOf(EOEditingContext editingContext, EOSepaSddOrigine eo) {
		return EOSepaSddOrigine.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOSepaSddOrigine fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOSepaSddOrigine fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSepaSddOrigine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSepaSddOrigine)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSepaSddOrigine fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSepaSddOrigine fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSepaSddOrigine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSepaSddOrigine)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOSepaSddOrigine fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSepaSddOrigine eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSepaSddOrigine ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSepaSddOrigine fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
