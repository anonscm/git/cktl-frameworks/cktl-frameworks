/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.*;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;

public class EOAccordsContrat extends _EOAccordsContrat {
	public static final EOSortOrdering SORT_EXERCICE_ASC = EOSortOrdering.sortOrderingWithKey(EOAccordsContrat.TO_EXERCICE_KEY + "." + IJefyAdminExercice.I_EXE_EXERCICE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_CON_INDEX_ASC = EOSortOrdering.sortOrderingWithKey(EOAccordsContrat.CON_INDEX_KEY, EOSortOrdering.CompareAscending);

    public EOAccordsContrat() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

	public static final EOQualifier qualForExerciceEquals(final Integer s) {
		if (s != null) {
			return new EOKeyValueQualifier(TO_EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, s);
		}
		return new EOAndQualifier(new NSArray());
	}

	public static EOQualifier qualForIndexEquals(final Integer s) {
		if (s != null) {
			return new EOKeyValueQualifier(CON_INDEX_KEY, EOQualifier.QualifierOperatorEqual, s);
		}
		return new EOAndQualifier(new NSArray());
	}

	public static EOQualifier qualForPartenaireLike(String s) {
		if (s != null) {
			return new EOKeyValueQualifier(TO_PERSONNE_PARTENAIRE_KEY + "." + IGrhumPersonne.I_PERS_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + s + "*");
		}
		return new EOAndQualifier(new NSArray());
	}

	public static EOQualifier qualForServiceLike(String s) {
		if (s != null) {
			return new EOKeyValueQualifier(TO_PERSONNE_SERVICE_KEY + "." + IGrhumPersonne.I_PERS_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + s + "*");
		}
		return new EOAndQualifier(new NSArray());
	}

	public static Object qualForObjetLike(String s) {
		if (s != null) {
			return new EOKeyValueQualifier(CON_OBJET_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + s + "*");
		}
		return new EOAndQualifier(new NSArray());
	}

}
