// _EOJefyRecetteFacturePapier.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJefyRecetteFacturePapier.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOJefyRecetteFacturePapier extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_JefyRecette_FacturePapier";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.facture_papier";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fapId";

	public static final String ECHE_ID_KEY = "echeId";
	public static final String FAP_APPLY_TVA_KEY = "fapApplyTva";
	public static final String FAP_COMMENTAIRE_CLIENT_KEY = "fapCommentaireClient";
	public static final String FAP_COMMENTAIRE_PREST_KEY = "fapCommentairePrest";
	public static final String FAP_DATE_KEY = "fapDate";
	public static final String FAP_DATE_LIMITE_PAIEMENT_KEY = "fapDateLimitePaiement";
	public static final String FAP_DATE_REGLEMENT_KEY = "fapDateReglement";
	public static final String FAP_DATE_VALIDATION_CLIENT_KEY = "fapDateValidationClient";
	public static final String FAP_DATE_VALIDATION_PREST_KEY = "fapDateValidationPrest";
	public static final String FAP_LIB_KEY = "fapLib";
	public static final String FAP_NUMERO_KEY = "fapNumero";
	public static final String FAP_REF_KEY = "fapRef";
	public static final String FAP_REFERENCE_REGLEMENT_KEY = "fapReferenceReglement";
	public static final String FAP_REMISE_GLOBALE_KEY = "fapRemiseGlobale";
	public static final String FAP_TOTAL_HT_KEY = "fapTotalHt";
	public static final String FAP_TOTAL_TTC_KEY = "fapTotalTtc";
	public static final String FAP_TOTAL_TVA_KEY = "fapTotalTva";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PCO_NUM_CTP_KEY = "pcoNumCtp";
	public static final String PCO_NUM_TVA_KEY = "pcoNumTva";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FAC_ID_KEY = "facId";
	public static final String FAP_ID_KEY = "fapId";
	public static final String FAP_UTL_VALIDATION_CLIENT_KEY = "fapUtlValidationClient";
	public static final String FAP_UTL_VALIDATION_PREST_KEY = "fapUtlValidationPrest";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String FOU_ORDRE_PREST_KEY = "fouOrdrePrest";
	public static final String LOLF_ID_KEY = "lolfId";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PERS_ID_KEY = "persId";
	public static final String PREST_ID_KEY = "prestId";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String TYPU_ID_KEY = "typuId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ECHE_ID_COLKEY = "ECHE_ID";
	public static final String FAP_APPLY_TVA_COLKEY = "FAP_APPLY_TVA";
	public static final String FAP_COMMENTAIRE_CLIENT_COLKEY = "FAP_COMMENTAIRE_CLIENT";
	public static final String FAP_COMMENTAIRE_PREST_COLKEY = "FAP_COMMENTAIRE_PREST";
	public static final String FAP_DATE_COLKEY = "FAP_DATE";
	public static final String FAP_DATE_LIMITE_PAIEMENT_COLKEY = "FAP_DATE_LIMITE_PAIEMENT";
	public static final String FAP_DATE_REGLEMENT_COLKEY = "FAP_DATE_REGLEMENT";
	public static final String FAP_DATE_VALIDATION_CLIENT_COLKEY = "FAP_DATE_VALIDATION_CLIENT";
	public static final String FAP_DATE_VALIDATION_PREST_COLKEY = "FAP_DATE_VALIDATION_PREST";
	public static final String FAP_LIB_COLKEY = "FAP_LIB";
	public static final String FAP_NUMERO_COLKEY = "FAP_NUMERO";
	public static final String FAP_REF_COLKEY = "FAP_REF";
	public static final String FAP_REFERENCE_REGLEMENT_COLKEY = "FAP_REFERENCE_REGLEMENT";
	public static final String FAP_REMISE_GLOBALE_COLKEY = "FAP_REMISE_GLOBALE";
	public static final String FAP_TOTAL_HT_COLKEY = "FAP_TOTAL_HT";
	public static final String FAP_TOTAL_TTC_COLKEY = "FAP_TOTAL_TTC";
	public static final String FAP_TOTAL_TVA_COLKEY = "FAP_TOTAL_TVA";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PCO_NUM_CTP_COLKEY = "PCO_NUM_CTP";
	public static final String PCO_NUM_TVA_COLKEY = "PCO_NUM_TVA";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FAC_ID_COLKEY = "FAC_ID";
	public static final String FAP_ID_COLKEY = "FAP_ID";
	public static final String FAP_UTL_VALIDATION_CLIENT_COLKEY = "fap_utl_validation_client";
	public static final String FAP_UTL_VALIDATION_PREST_COLKEY = "fap_utl_validation_prest";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String FOU_ORDRE_PREST_COLKEY = "FOU_ORDRE_PREST";
	public static final String LOLF_ID_COLKEY = "LOLF_ID";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PREST_ID_COLKEY = "PREST_ID";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String TYPU_ID_COLKEY = "TYPU_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_FACTURE_KEY = "toFacture";
	public static final String TO_MODE_RECOUVREMENT_KEY = "toModeRecouvrement";
	public static final String TO_PERSONNE_KEY = "toPersonne";
	public static final String TO_RIB_KEY = "toRib";



	// Accessors methods
  public Integer echeId() {
    return (Integer) storedValueForKey(ECHE_ID_KEY);
  }

  public void setEcheId(Integer value) {
    takeStoredValueForKey(value, ECHE_ID_KEY);
  }

  public String fapApplyTva() {
    return (String) storedValueForKey(FAP_APPLY_TVA_KEY);
  }

  public void setFapApplyTva(String value) {
    takeStoredValueForKey(value, FAP_APPLY_TVA_KEY);
  }

  public String fapCommentaireClient() {
    return (String) storedValueForKey(FAP_COMMENTAIRE_CLIENT_KEY);
  }

  public void setFapCommentaireClient(String value) {
    takeStoredValueForKey(value, FAP_COMMENTAIRE_CLIENT_KEY);
  }

  public String fapCommentairePrest() {
    return (String) storedValueForKey(FAP_COMMENTAIRE_PREST_KEY);
  }

  public void setFapCommentairePrest(String value) {
    takeStoredValueForKey(value, FAP_COMMENTAIRE_PREST_KEY);
  }

  public NSTimestamp fapDate() {
    return (NSTimestamp) storedValueForKey(FAP_DATE_KEY);
  }

  public void setFapDate(NSTimestamp value) {
    takeStoredValueForKey(value, FAP_DATE_KEY);
  }

  public NSTimestamp fapDateLimitePaiement() {
    return (NSTimestamp) storedValueForKey(FAP_DATE_LIMITE_PAIEMENT_KEY);
  }

  public void setFapDateLimitePaiement(NSTimestamp value) {
    takeStoredValueForKey(value, FAP_DATE_LIMITE_PAIEMENT_KEY);
  }

  public NSTimestamp fapDateReglement() {
    return (NSTimestamp) storedValueForKey(FAP_DATE_REGLEMENT_KEY);
  }

  public void setFapDateReglement(NSTimestamp value) {
    takeStoredValueForKey(value, FAP_DATE_REGLEMENT_KEY);
  }

  public NSTimestamp fapDateValidationClient() {
    return (NSTimestamp) storedValueForKey(FAP_DATE_VALIDATION_CLIENT_KEY);
  }

  public void setFapDateValidationClient(NSTimestamp value) {
    takeStoredValueForKey(value, FAP_DATE_VALIDATION_CLIENT_KEY);
  }

  public NSTimestamp fapDateValidationPrest() {
    return (NSTimestamp) storedValueForKey(FAP_DATE_VALIDATION_PREST_KEY);
  }

  public void setFapDateValidationPrest(NSTimestamp value) {
    takeStoredValueForKey(value, FAP_DATE_VALIDATION_PREST_KEY);
  }

  public String fapLib() {
    return (String) storedValueForKey(FAP_LIB_KEY);
  }

  public void setFapLib(String value) {
    takeStoredValueForKey(value, FAP_LIB_KEY);
  }

  public Integer fapNumero() {
    return (Integer) storedValueForKey(FAP_NUMERO_KEY);
  }

  public void setFapNumero(Integer value) {
    takeStoredValueForKey(value, FAP_NUMERO_KEY);
  }

  public String fapRef() {
    return (String) storedValueForKey(FAP_REF_KEY);
  }

  public void setFapRef(String value) {
    takeStoredValueForKey(value, FAP_REF_KEY);
  }

  public String fapReferenceReglement() {
    return (String) storedValueForKey(FAP_REFERENCE_REGLEMENT_KEY);
  }

  public void setFapReferenceReglement(String value) {
    takeStoredValueForKey(value, FAP_REFERENCE_REGLEMENT_KEY);
  }

  public java.math.BigDecimal fapRemiseGlobale() {
    return (java.math.BigDecimal) storedValueForKey(FAP_REMISE_GLOBALE_KEY);
  }

  public void setFapRemiseGlobale(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAP_REMISE_GLOBALE_KEY);
  }

  public java.math.BigDecimal fapTotalHt() {
    return (java.math.BigDecimal) storedValueForKey(FAP_TOTAL_HT_KEY);
  }

  public void setFapTotalHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAP_TOTAL_HT_KEY);
  }

  public java.math.BigDecimal fapTotalTtc() {
    return (java.math.BigDecimal) storedValueForKey(FAP_TOTAL_TTC_KEY);
  }

  public void setFapTotalTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAP_TOTAL_TTC_KEY);
  }

  public java.math.BigDecimal fapTotalTva() {
    return (java.math.BigDecimal) storedValueForKey(FAP_TOTAL_TVA_KEY);
  }

  public void setFapTotalTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAP_TOTAL_TVA_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public String pcoNumCtp() {
    return (String) storedValueForKey(PCO_NUM_CTP_KEY);
  }

  public void setPcoNumCtp(String value) {
    takeStoredValueForKey(value, PCO_NUM_CTP_KEY);
  }

  public String pcoNumTva() {
    return (String) storedValueForKey(PCO_NUM_TVA_KEY);
  }

  public void setPcoNumTva(String value) {
    takeStoredValueForKey(value, PCO_NUM_TVA_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice toExercice() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacture toFacture() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacture)storedValueForKey(TO_FACTURE_KEY);
  }

  public void setToFactureRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacture value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacture oldValue = toFacture();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FACTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_FACTURE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOModeRecouvrement toModeRecouvrement() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOModeRecouvrement)storedValueForKey(TO_MODE_RECOUVREMENT_KEY);
  }

  public void setToModeRecouvrementRelationship(org.cocktail.fwkcktlcompta.client.metier.EOModeRecouvrement value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOModeRecouvrement oldValue = toModeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODE_RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonne() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_PERSONNE_KEY);
  }

  public void setToPersonneRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib toRib() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib)storedValueForKey(TO_RIB_KEY);
  }

  public void setToRibRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib oldValue = toRib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RIB_KEY);
    }
  }
  

  public static EOJefyRecetteFacturePapier createFwkCktlCompta_JefyRecette_FacturePapier(EOEditingContext editingContext, String fapApplyTva
, NSTimestamp fapDate
, String fapLib
, Integer fapNumero
, java.math.BigDecimal fapTotalHt
, java.math.BigDecimal fapTotalTtc
, java.math.BigDecimal fapTotalTva
, org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice toExercice, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonne) {
    EOJefyRecetteFacturePapier eo = (EOJefyRecetteFacturePapier) createAndInsertInstance(editingContext, _EOJefyRecetteFacturePapier.ENTITY_NAME);    
		eo.setFapApplyTva(fapApplyTva);
		eo.setFapDate(fapDate);
		eo.setFapLib(fapLib);
		eo.setFapNumero(fapNumero);
		eo.setFapTotalHt(fapTotalHt);
		eo.setFapTotalTtc(fapTotalTtc);
		eo.setFapTotalTva(fapTotalTva);
    eo.setToExerciceRelationship(toExercice);
    eo.setToPersonneRelationship(toPersonne);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOJefyRecetteFacturePapier.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOJefyRecetteFacturePapier.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOJefyRecetteFacturePapier creerInstance(EOEditingContext editingContext) {
		  		EOJefyRecetteFacturePapier object = (EOJefyRecetteFacturePapier)createAndInsertInstance(editingContext, _EOJefyRecetteFacturePapier.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOJefyRecetteFacturePapier localInstanceIn(EOEditingContext editingContext) {
	  		return (EOJefyRecetteFacturePapier)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOJefyRecetteFacturePapier localInstanceIn(EOEditingContext editingContext, EOJefyRecetteFacturePapier eo) {
    EOJefyRecetteFacturePapier localInstance = (eo == null) ? null : (EOJefyRecetteFacturePapier)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOJefyRecetteFacturePapier#localInstanceIn a la place.
   */
	public static EOJefyRecetteFacturePapier localInstanceOf(EOEditingContext editingContext, EOJefyRecetteFacturePapier eo) {
		return EOJefyRecetteFacturePapier.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOJefyRecetteFacturePapier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOJefyRecetteFacturePapier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJefyRecetteFacturePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJefyRecetteFacturePapier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJefyRecetteFacturePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJefyRecetteFacturePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJefyRecetteFacturePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJefyRecetteFacturePapier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOJefyRecetteFacturePapier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJefyRecetteFacturePapier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJefyRecetteFacturePapier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJefyRecetteFacturePapier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
