/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.client.metier;

import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;
import org.cocktail.fwkcktlcompta.common.helpers.JefyAdminExerciceHelper;

import com.webobjects.foundation.NSValidation;

public class EOJefyAdminExercice extends _EOJefyAdminExercice implements IJefyAdminExercice {


	public EOJefyAdminExercice() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * @return Le libellé correspondant au champ exeStat.
	 */
	public final String getEtatLibelle() {
		return JefyAdminExerciceHelper.getSharedInstance().getEtatLibelle(this);
	}

	/**
	 * @return Le libelle correspondant au champ exeType.
	 */
	public final String getTypeLibelle() {
		return JefyAdminExerciceHelper.getSharedInstance().getTypeLibelle(this);
	}

	public final boolean estClos() {
		return JefyAdminExerciceHelper.getSharedInstance().estClos(this);
	}

	public final boolean estOuvert() {
		return JefyAdminExerciceHelper.getSharedInstance().estOuvert(this);
	}

	public final boolean estRestreint() {
		return JefyAdminExerciceHelper.getSharedInstance().estRestreint(this);
	}

	public final boolean estPreparation() {
		return JefyAdminExerciceHelper.getSharedInstance().estPreparation(this);
	}

	public final boolean estTresorerie() {
		return JefyAdminExerciceHelper.getSharedInstance().estTresorerie(this);
	}

	public final boolean estComptable() {
		return JefyAdminExerciceHelper.getSharedInstance().estComptable(this);
	}

	public EOJefyAdminExercice getNextEOExercice() {
		EOJefyAdminExercice exerciceNext = EOJefyAdminExercice.fetchByKeyValue(editingContext(), EOJefyAdminExercice.EXE_EXERCICE_KEY, Integer.valueOf(exeExercice().intValue() + 1));
		return exerciceNext;
	}

	public EOJefyAdminExercice getPrevEOExercice() {
		EOJefyAdminExercice exerciceNext = EOJefyAdminExercice.fetchByKeyValue(editingContext(), EOJefyAdminExercice.EXE_EXERCICE_KEY, Integer.valueOf(exeExercice().intValue() - 1));
		return exerciceNext;
	}

}
