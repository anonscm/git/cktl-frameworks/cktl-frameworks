// _EOBordereauRejet.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBordereauRejet.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBordereauRejet extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_BordereauRejet";
	public static final String ENTITY_TABLE_NAME = "maracuja.Bordereau_Rejet";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "brjOrdre";

	public static final String BRJ_ETAT_KEY = "brjEtat";
	public static final String BRJ_NUM_KEY = "brjNum";

// Attributs non visibles
	public static final String BRJ_ORDRE_KEY = "brjOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String TBO_ORDRE_KEY = "tboOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String BRJ_ETAT_COLKEY = "brj_etat";
	public static final String BRJ_NUM_COLKEY = "brj_num";

	public static final String BRJ_ORDRE_COLKEY = "BRJ_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_GESTION_KEY = "toGestion";
	public static final String TO_TYPE_BORDEREAU_KEY = "toTypeBordereau";
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";



	// Accessors methods
  public String brjEtat() {
    return (String) storedValueForKey(BRJ_ETAT_KEY);
  }

  public void setBrjEtat(String value) {
    takeStoredValueForKey(value, BRJ_ETAT_KEY);
  }

  public Integer brjNum() {
    return (Integer) storedValueForKey(BRJ_NUM_KEY);
  }

  public void setBrjNum(Integer value) {
    takeStoredValueForKey(value, BRJ_NUM_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
  }

  public void setToGestionRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGestion oldValue = toGestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOTypeBordereau toTypeBordereau() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOTypeBordereau)storedValueForKey(TO_TYPE_BORDEREAU_KEY);
  }

  public void setToTypeBordereauRelationship(org.cocktail.fwkcktlcompta.client.metier.EOTypeBordereau value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOTypeBordereau oldValue = toTypeBordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateur() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
  }

  public void setToUtilisateurRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = toUtilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
    }
  }
  

  public static EOBordereauRejet createFwkCktlCompta_BordereauRejet(EOEditingContext editingContext, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice, org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion, org.cocktail.fwkcktlcompta.client.metier.EOTypeBordereau toTypeBordereau, org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateur) {
    EOBordereauRejet eo = (EOBordereauRejet) createAndInsertInstance(editingContext, _EOBordereauRejet.ENTITY_NAME);    
    eo.setToExerciceRelationship(toExercice);
    eo.setToGestionRelationship(toGestion);
    eo.setToTypeBordereauRelationship(toTypeBordereau);
    eo.setToUtilisateurRelationship(toUtilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOBordereauRejet.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOBordereauRejet.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOBordereauRejet creerInstance(EOEditingContext editingContext) {
		  		EOBordereauRejet object = (EOBordereauRejet)createAndInsertInstance(editingContext, _EOBordereauRejet.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOBordereauRejet localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBordereauRejet)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOBordereauRejet localInstanceIn(EOEditingContext editingContext, EOBordereauRejet eo) {
    EOBordereauRejet localInstance = (eo == null) ? null : (EOBordereauRejet)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOBordereauRejet#localInstanceIn a la place.
   */
	public static EOBordereauRejet localInstanceOf(EOEditingContext editingContext, EOBordereauRejet eo) {
		return EOBordereauRejet.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOBordereauRejet fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOBordereauRejet fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBordereauRejet eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBordereauRejet)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBordereauRejet fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBordereauRejet fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBordereauRejet eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBordereauRejet)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOBordereauRejet fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBordereauRejet eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBordereauRejet ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBordereauRejet fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
