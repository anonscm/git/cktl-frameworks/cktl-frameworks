// _EOSepaSddParam.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSepaSddParam.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOSepaSddParam extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddParam";
	public static final String ENTITY_TABLE_NAME = "maracuja.SEPA_SDD_PARAM";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idSepaSddParam";

	public static final String CREANCIER_ICS_KEY = "creancierIcs";
	public static final String DEVISE_KEY = "devise";
	public static final String DFT_IBAN_KEY = "dftIban";
	public static final String DFT_TITULAIRE_KEY = "dftTitulaire";
	public static final String EMETTEUR_NOM_KEY = "emetteurNom";
	public static final String ETAT_KEY = "etat";
	public static final String ID_SEPA_SDD_PARAM_KEY = "idSepaSddParam";
	public static final String TENEUR_COMPTE_BIC_KEY = "teneurCompteBic";
	public static final String TENEUR_COMPTE_CODIQUE_KEY = "teneurCompteCodique";
	public static final String TENEUR_COMPTE_IBAN_KEY = "teneurCompteIban";
	public static final String TENEUR_COMPTE_NOM_KEY = "teneurCompteNom";
	public static final String TRANSFERT_ID_KEY = "transfertId";

// Attributs non visibles
	public static final String CREANCIER_PERS_ID_KEY = "creancierPersId";
	public static final String TREC_ORDRE_KEY = "trecOrdre";

//Colonnes dans la base de donnees
	public static final String CREANCIER_ICS_COLKEY = "CREANCIER_ICS";
	public static final String DEVISE_COLKEY = "DEVISE";
	public static final String DFT_IBAN_COLKEY = "DFT_IBAN";
	public static final String DFT_TITULAIRE_COLKEY = "DFT_TITULAIRE";
	public static final String EMETTEUR_NOM_COLKEY = "EMETTEUR_NOM";
	public static final String ETAT_COLKEY = "ETAT";
	public static final String ID_SEPA_SDD_PARAM_COLKEY = "ID_SEPA_SDD_PARAM";
	public static final String TENEUR_COMPTE_BIC_COLKEY = "TENEUR_COMPTE_BIC";
	public static final String TENEUR_COMPTE_CODIQUE_COLKEY = "TENEUR_COMPTE_CODIQUE";
	public static final String TENEUR_COMPTE_IBAN_COLKEY = "TENEUR_COMPTE_IBAN";
	public static final String TENEUR_COMPTE_NOM_COLKEY = "TENEUR_COMPTE_NOM";
	public static final String TRANSFERT_ID_COLKEY = "TRANSFERT_ID";

	public static final String CREANCIER_PERS_ID_COLKEY = "CREANCIER_PERS_ID";
	public static final String TREC_ORDRE_COLKEY = "TREC_ORDRE";


	// Relationships
	public static final String TO_PERSONNE_CREANCIER_KEY = "toPersonneCreancier";
	public static final String TO_TYPE_RECOUVREMENT_KEY = "toTypeRecouvrement";



	// Accessors methods
  public String creancierIcs() {
    return (String) storedValueForKey(CREANCIER_ICS_KEY);
  }

  public void setCreancierIcs(String value) {
    takeStoredValueForKey(value, CREANCIER_ICS_KEY);
  }

  public String devise() {
    return (String) storedValueForKey(DEVISE_KEY);
  }

  public void setDevise(String value) {
    takeStoredValueForKey(value, DEVISE_KEY);
  }

  public String dftIban() {
    return (String) storedValueForKey(DFT_IBAN_KEY);
  }

  public void setDftIban(String value) {
    takeStoredValueForKey(value, DFT_IBAN_KEY);
  }

  public String dftTitulaire() {
    return (String) storedValueForKey(DFT_TITULAIRE_KEY);
  }

  public void setDftTitulaire(String value) {
    takeStoredValueForKey(value, DFT_TITULAIRE_KEY);
  }

  public String emetteurNom() {
    return (String) storedValueForKey(EMETTEUR_NOM_KEY);
  }

  public void setEmetteurNom(String value) {
    takeStoredValueForKey(value, EMETTEUR_NOM_KEY);
  }

  public String etat() {
    return (String) storedValueForKey(ETAT_KEY);
  }

  public void setEtat(String value) {
    takeStoredValueForKey(value, ETAT_KEY);
  }

  public Integer idSepaSddParam() {
    return (Integer) storedValueForKey(ID_SEPA_SDD_PARAM_KEY);
  }

  public void setIdSepaSddParam(Integer value) {
    takeStoredValueForKey(value, ID_SEPA_SDD_PARAM_KEY);
  }

  public String teneurCompteBic() {
    return (String) storedValueForKey(TENEUR_COMPTE_BIC_KEY);
  }

  public void setTeneurCompteBic(String value) {
    takeStoredValueForKey(value, TENEUR_COMPTE_BIC_KEY);
  }

  public String teneurCompteCodique() {
    return (String) storedValueForKey(TENEUR_COMPTE_CODIQUE_KEY);
  }

  public void setTeneurCompteCodique(String value) {
    takeStoredValueForKey(value, TENEUR_COMPTE_CODIQUE_KEY);
  }

  public String teneurCompteIban() {
    return (String) storedValueForKey(TENEUR_COMPTE_IBAN_KEY);
  }

  public void setTeneurCompteIban(String value) {
    takeStoredValueForKey(value, TENEUR_COMPTE_IBAN_KEY);
  }

  public String teneurCompteNom() {
    return (String) storedValueForKey(TENEUR_COMPTE_NOM_KEY);
  }

  public void setTeneurCompteNom(String value) {
    takeStoredValueForKey(value, TENEUR_COMPTE_NOM_KEY);
  }

  public String transfertId() {
    return (String) storedValueForKey(TRANSFERT_ID_KEY);
  }

  public void setTransfertId(String value) {
    takeStoredValueForKey(value, TRANSFERT_ID_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonneCreancier() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_PERSONNE_CREANCIER_KEY);
  }

  public void setToPersonneCreancierRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toPersonneCreancier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_CREANCIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_CREANCIER_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOTypeRecouvrement toTypeRecouvrement() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOTypeRecouvrement)storedValueForKey(TO_TYPE_RECOUVREMENT_KEY);
  }

  public void setToTypeRecouvrementRelationship(org.cocktail.fwkcktlcompta.client.metier.EOTypeRecouvrement value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOTypeRecouvrement oldValue = toTypeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_RECOUVREMENT_KEY);
    }
  }
  

  public static EOSepaSddParam createFwkCktlCompta_SepaSddParam(EOEditingContext editingContext, String creancierIcs
, String devise
, String dftIban
, String dftTitulaire
, String emetteurNom
, String etat
, Integer idSepaSddParam
, String teneurCompteBic
, String teneurCompteCodique
, String teneurCompteIban
, String teneurCompteNom
, String transfertId
, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonneCreancier, org.cocktail.fwkcktlcompta.client.metier.EOTypeRecouvrement toTypeRecouvrement) {
    EOSepaSddParam eo = (EOSepaSddParam) createAndInsertInstance(editingContext, _EOSepaSddParam.ENTITY_NAME);    
		eo.setCreancierIcs(creancierIcs);
		eo.setDevise(devise);
		eo.setDftIban(dftIban);
		eo.setDftTitulaire(dftTitulaire);
		eo.setEmetteurNom(emetteurNom);
		eo.setEtat(etat);
		eo.setIdSepaSddParam(idSepaSddParam);
		eo.setTeneurCompteBic(teneurCompteBic);
		eo.setTeneurCompteCodique(teneurCompteCodique);
		eo.setTeneurCompteIban(teneurCompteIban);
		eo.setTeneurCompteNom(teneurCompteNom);
		eo.setTransfertId(transfertId);
    eo.setToPersonneCreancierRelationship(toPersonneCreancier);
    eo.setToTypeRecouvrementRelationship(toTypeRecouvrement);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOSepaSddParam.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOSepaSddParam.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOSepaSddParam creerInstance(EOEditingContext editingContext) {
		  		EOSepaSddParam object = (EOSepaSddParam)createAndInsertInstance(editingContext, _EOSepaSddParam.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOSepaSddParam localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSepaSddParam)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOSepaSddParam localInstanceIn(EOEditingContext editingContext, EOSepaSddParam eo) {
    EOSepaSddParam localInstance = (eo == null) ? null : (EOSepaSddParam)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOSepaSddParam#localInstanceIn a la place.
   */
	public static EOSepaSddParam localInstanceOf(EOEditingContext editingContext, EOSepaSddParam eo) {
		return EOSepaSddParam.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOSepaSddParam fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOSepaSddParam fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSepaSddParam eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSepaSddParam)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSepaSddParam fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSepaSddParam fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSepaSddParam eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSepaSddParam)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOSepaSddParam fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSepaSddParam eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSepaSddParam ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSepaSddParam fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
