/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.client.metier;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.ITracable;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheanceHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.rules.SepaSddEcheancierRule;
import org.cocktail.fwkcktlcompta.common.util.WebObjectConversionUtil;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSValidation;

public class EOSepaSddEcheancier extends _EOSepaSddEcheancier implements ISepaSddEcheancier, ITracable {

	public EOSepaSddEcheancier() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 *
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		SepaSddEcheancierRule.getSharedInstance().validateSepaSddEcheancier(this);
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/** METHODES PROVENANTS INTERFACE METIER */


	public ISepaSddEcheance creerEcheance() {
		return creerEcheance(editingContext());
	}
	
	public ISepaSddEcheance creerEcheance(EOEditingContext edc) {
		ISepaSddEcheance echeance = EOSepaSddEcheance.creerInstance(edc);
		echeance.setEcheancier(this);
		return echeance;
	}

	public BigDecimal montantAPayer() {
		if (toSepaSddOrigine() == null || toSepaSddOrigine().toEntity() == null) {

			return null;
		}
		return SepaSddEcheanceHelper.getSharedInstance().calculeMontantAPayerDesEcheancesNonAnnulees(toSepaSddEcheances());
	}

	public BigDecimal montantPremiereEcheance() {
		BigDecimal montantPremiereEcheance = null;

		ISepaSddEcheance premiereEcheance = premiereEcheance();
		if (premiereEcheance != null) {
			montantPremiereEcheance = premiereEcheance.montantAPayer();
		}
		return montantPremiereEcheance;
	}

	public List<ISepaSddEcheance> echeances() {
		return WebObjectConversionUtil.asList(toSepaSddEcheances());
	}

	public Integer nombreEcheancesPrevues() {
		return SepaSddEcheancierHelper.getSharedInstance().getNombreEcheancesNonAnnulees(this);
	}

	public String datePremiereEcheance() {
		String datePremiereEcheance = null;

		ISepaSddEcheance premiereEcheance = premiereEcheance();
		if (premiereEcheance != null) {
			datePremiereEcheance = premiereEcheance.dPrevue();
		}
		return datePremiereEcheance;
	}

	private ISepaSddEcheance premiereEcheance() {
		if (echeances() == null) {
			return null;
		}

		ISepaSddEcheance premiereEcheance = null;
		if (echeances().size() > 0) {
			premiereEcheance = echeances().get(0);
		}

		return premiereEcheance;
	}

	public void ajouterEcheances(List<ISepaSddEcheance> echeances) {
		if (echeances == null) {
			return;
		}

		for (ISepaSddEcheance echeance : echeances) {
			addToToSepaSddEcheancesRelationship((EOSepaSddEcheance) echeance);
		}
	}

	public void setDateCreation(String dateCreation) {
		setDCreation(dateCreation);
	}

	public String dateCreation() {
		return dCreation();
	}

	public void setDateModification(String dateModification) {
		setDModification(dateModification);
	}

	public String dateModification() {
		return dModification();
	}

	public Integer derniereModificationPar() {
		return modificateur().persId();
	}

	public ISepaSddMandat mandat() {
		return toSepaSddMandat();
	}

	public void setMandat(ISepaSddMandat mandat) {
		setToSepaSddMandatRelationship((EOSepaSddMandat) mandat);
	}

	public void supprimerEcheances() {
		NSArray echeances = SepaSddEcheancierHelper.getSharedInstance().getEcheancesTrieesParDateEcheanceASC(this, false);
		Enumeration<org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance> objects = echeances.immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToSepaSddEcheancesRelationship(objects.nextElement());
		}
	}

	public EOEnterpriseObject toEnterpriseObject() {
		return (EOEnterpriseObject) this;
	}

	public void setToModificateurPersonne(IGrhumPersonne grhumPersonne) {
		setToModificateurRelationship((EOGrhumPersonne) grhumPersonne);

	}

	public IGrhumPersonne modificateur() {
		return toModificateur();
	}

	public NSArray getEcheancesTrieesParDateEcheanceASC(Boolean fetch) {
		return getEcheancesTrieesParEtat(null, null, fetch);
	}

	public NSArray getEcheancesTrieesParDateEcheanceASCAvecEtatDifferentDe(Etat etat, Boolean fetch) {
		return getEcheancesTrieesParEtat(etat, EOQualifier.QualifierOperatorNotEqual, fetch);
	}

	public NSArray getEcheancesTrieesParDateEcheanceASCAvecEtat(Etat etat, Boolean fetch) {
		return getEcheancesTrieesParEtat(etat, EOQualifier.QualifierOperatorEqual, fetch);
	}

	private NSArray getEcheancesTrieesParEtat(Etat etat, NSSelector selector, Boolean fetch) {
		EOQualifier qual = null;
		if (etat != null && selector != null) {
			qual = new EOKeyValueQualifier(EOSepaSddEcheance.ETAT_KEY, selector, etat.toString());
		}

		return toSepaSddEcheances(qual, new NSArray(new Object[] {
				SepaSddEcheanceHelper.SORT_DATE_ECHEANCE_ASC
		}), fetch);
	}

	public ISepaSddOrigine creerOrigine() {
		return EOSepaSddOrigine.creerInstance(editingContext());
	}

	public ISepaSddOrigine origine() {
		return toSepaSddOrigine();
	}

	public void setToSepaSddOrigineRelationship(ISepaSddOrigine origine) {
		setToSepaSddOrigineRelationship((EOSepaSddOrigine) origine);
	}

	public String getReference() {
		return mandat().rum();
	}

	public Integer id() {
		if (isNewObject()) {
			return null;
		}
		return Integer.valueOf(primaryKey());
	}

	public NSArray toEcritureDetailsPriseEncharge() {
		NSArray echeancierEcds = toSepaSddEcheancierEcds();
		NSArray ecds = (NSArray) echeancierEcds.valueForKey(EOSepaSddEcheancierEcd.TO_ECRITURE_DETAIL_KEY);
		return ecds;
	}

}
