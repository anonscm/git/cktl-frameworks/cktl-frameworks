// _EOScolarixInscription.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolarixInscription.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOScolarixInscription extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_ScolarixInscription";
	public static final String ENTITY_TABLE_NAME = "maracuja.v_scolarix_inscription";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "identifiant";

	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String HIST_NUMERO_KEY = "histNumero";
	public static final String IDENTIFIANT_KEY = "identifiant";
	public static final String PAIE_DATE_KEY = "paieDate";
	public static final String PAIE_NUMERO_KEY = "paieNumero";
	public static final String PAIE_SOMME_KEY = "paieSomme";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PERS_ID_KEY = "persId";

//Colonnes dans la base de donnees
	public static final String ETUD_NUMERO_COLKEY = "etud_numero";
	public static final String HIST_NUMERO_COLKEY = "hist_numero";
	public static final String IDENTIFIANT_COLKEY = "identifiant";
	public static final String PAIE_DATE_COLKEY = "paie_date";
	public static final String PAIE_NUMERO_COLKEY = "paie_numero";
	public static final String PAIE_SOMME_COLKEY = "paie_somme";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PERS_ID_COLKEY = "PERS_ID";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_PERSONNE_KEY = "toPersonne";



	// Accessors methods
  public Integer etudNumero() {
    return (Integer) storedValueForKey(ETUD_NUMERO_KEY);
  }

  public void setEtudNumero(Integer value) {
    takeStoredValueForKey(value, ETUD_NUMERO_KEY);
  }

  public Integer histNumero() {
    return (Integer) storedValueForKey(HIST_NUMERO_KEY);
  }

  public void setHistNumero(Integer value) {
    takeStoredValueForKey(value, HIST_NUMERO_KEY);
  }

  public Integer identifiant() {
    return (Integer) storedValueForKey(IDENTIFIANT_KEY);
  }

  public void setIdentifiant(Integer value) {
    takeStoredValueForKey(value, IDENTIFIANT_KEY);
  }

  public NSTimestamp paieDate() {
    return (NSTimestamp) storedValueForKey(PAIE_DATE_KEY);
  }

  public void setPaieDate(NSTimestamp value) {
    takeStoredValueForKey(value, PAIE_DATE_KEY);
  }

  public Integer paieNumero() {
    return (Integer) storedValueForKey(PAIE_NUMERO_KEY);
  }

  public void setPaieNumero(Integer value) {
    takeStoredValueForKey(value, PAIE_NUMERO_KEY);
  }

  public java.math.BigDecimal paieSomme() {
    return (java.math.BigDecimal) storedValueForKey(PAIE_SOMME_KEY);
  }

  public void setPaieSomme(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAIE_SOMME_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice toExercice() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonne() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_PERSONNE_KEY);
  }

  public void setToPersonneRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_KEY);
    }
  }
  

  public static EOScolarixInscription createFwkCktlCompta_ScolarixInscription(EOEditingContext editingContext, Integer etudNumero
, Integer histNumero
, Integer identifiant
, Integer paieNumero
, java.math.BigDecimal paieSomme
, org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice toExercice, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonne) {
    EOScolarixInscription eo = (EOScolarixInscription) createAndInsertInstance(editingContext, _EOScolarixInscription.ENTITY_NAME);    
		eo.setEtudNumero(etudNumero);
		eo.setHistNumero(histNumero);
		eo.setIdentifiant(identifiant);
		eo.setPaieNumero(paieNumero);
		eo.setPaieSomme(paieSomme);
    eo.setToExerciceRelationship(toExercice);
    eo.setToPersonneRelationship(toPersonne);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOScolarixInscription.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOScolarixInscription.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOScolarixInscription creerInstance(EOEditingContext editingContext) {
		  		EOScolarixInscription object = (EOScolarixInscription)createAndInsertInstance(editingContext, _EOScolarixInscription.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOScolarixInscription localInstanceIn(EOEditingContext editingContext) {
	  		return (EOScolarixInscription)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOScolarixInscription localInstanceIn(EOEditingContext editingContext, EOScolarixInscription eo) {
    EOScolarixInscription localInstance = (eo == null) ? null : (EOScolarixInscription)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOScolarixInscription#localInstanceIn a la place.
   */
	public static EOScolarixInscription localInstanceOf(EOEditingContext editingContext, EOScolarixInscription eo) {
		return EOScolarixInscription.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOScolarixInscription fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOScolarixInscription fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOScolarixInscription eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOScolarixInscription)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOScolarixInscription fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOScolarixInscription fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOScolarixInscription eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOScolarixInscription)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOScolarixInscription fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOScolarixInscription eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOScolarixInscription ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOScolarixInscription fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
