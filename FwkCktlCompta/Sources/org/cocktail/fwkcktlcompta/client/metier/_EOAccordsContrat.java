// _EOAccordsContrat.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAccordsContrat.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOAccordsContrat extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_AccordsContrat";
	public static final String ENTITY_TABLE_NAME = "maracuja.V_accords_contrat";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "conOrdre";

	public static final String CON_CR_KEY = "conCr";
	public static final String CON_DATE_CLOTURE_KEY = "conDateCloture";
	public static final String CON_DATE_FIN_PAIEMENT_KEY = "conDateFinPaiement";
	public static final String CON_DATE_VALID_ADM_KEY = "conDateValidAdm";
	public static final String CON_INDEX_KEY = "conIndex";
	public static final String CON_OBJET_KEY = "conObjet";
	public static final String CON_REFERENCE_EXTERNE_KEY = "conReferenceExterne";
	public static final String CON_SUPPR_KEY = "conSuppr";
	public static final String NUMERO_KEY = "numero";
	public static final String PERS_ID_PARTENAIRE_KEY = "persIdPartenaire";
	public static final String PERS_ID_SERVICE_KEY = "persIdService";

// Attributs non visibles
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String CON_CR_COLKEY = "CON_CR";
	public static final String CON_DATE_CLOTURE_COLKEY = "CON_DATE_CLOTURE";
	public static final String CON_DATE_FIN_PAIEMENT_COLKEY = "CON_DATE_FIN_PAIEMENT";
	public static final String CON_DATE_VALID_ADM_COLKEY = "con_date_valid_adm";
	public static final String CON_INDEX_COLKEY = "CON_INDEX";
	public static final String CON_OBJET_COLKEY = "CON_OBJET";
	public static final String CON_REFERENCE_EXTERNE_COLKEY = "CON_reference_externe";
	public static final String CON_SUPPR_COLKEY = "CON_SUPPR";
	public static final String NUMERO_COLKEY = "NUMERO";
	public static final String PERS_ID_PARTENAIRE_COLKEY = "pers_id_partenaire";
	public static final String PERS_ID_SERVICE_COLKEY = "pers_id_service";

	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_PERSONNE_PARTENAIRE_KEY = "toPersonnePartenaire";
	public static final String TO_PERSONNE_SERVICE_KEY = "toPersonneService";



	// Accessors methods
  public String conCr() {
    return (String) storedValueForKey(CON_CR_KEY);
  }

  public void setConCr(String value) {
    takeStoredValueForKey(value, CON_CR_KEY);
  }

  public NSTimestamp conDateCloture() {
    return (NSTimestamp) storedValueForKey(CON_DATE_CLOTURE_KEY);
  }

  public void setConDateCloture(NSTimestamp value) {
    takeStoredValueForKey(value, CON_DATE_CLOTURE_KEY);
  }

  public NSTimestamp conDateFinPaiement() {
    return (NSTimestamp) storedValueForKey(CON_DATE_FIN_PAIEMENT_KEY);
  }

  public void setConDateFinPaiement(NSTimestamp value) {
    takeStoredValueForKey(value, CON_DATE_FIN_PAIEMENT_KEY);
  }

  public NSTimestamp conDateValidAdm() {
    return (NSTimestamp) storedValueForKey(CON_DATE_VALID_ADM_KEY);
  }

  public void setConDateValidAdm(NSTimestamp value) {
    takeStoredValueForKey(value, CON_DATE_VALID_ADM_KEY);
  }

  public Integer conIndex() {
    return (Integer) storedValueForKey(CON_INDEX_KEY);
  }

  public void setConIndex(Integer value) {
    takeStoredValueForKey(value, CON_INDEX_KEY);
  }

  public String conObjet() {
    return (String) storedValueForKey(CON_OBJET_KEY);
  }

  public void setConObjet(String value) {
    takeStoredValueForKey(value, CON_OBJET_KEY);
  }

  public String conReferenceExterne() {
    return (String) storedValueForKey(CON_REFERENCE_EXTERNE_KEY);
  }

  public void setConReferenceExterne(String value) {
    takeStoredValueForKey(value, CON_REFERENCE_EXTERNE_KEY);
  }

  public String conSuppr() {
    return (String) storedValueForKey(CON_SUPPR_KEY);
  }

  public void setConSuppr(String value) {
    takeStoredValueForKey(value, CON_SUPPR_KEY);
  }

  public String numero() {
    return (String) storedValueForKey(NUMERO_KEY);
  }

  public void setNumero(String value) {
    takeStoredValueForKey(value, NUMERO_KEY);
  }

  public Integer persIdPartenaire() {
    return (Integer) storedValueForKey(PERS_ID_PARTENAIRE_KEY);
  }

  public void setPersIdPartenaire(Integer value) {
    takeStoredValueForKey(value, PERS_ID_PARTENAIRE_KEY);
  }

  public Integer persIdService() {
    return (Integer) storedValueForKey(PERS_ID_SERVICE_KEY);
  }

  public void setPersIdService(Integer value) {
    takeStoredValueForKey(value, PERS_ID_SERVICE_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice toExercice() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonnePartenaire() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_PERSONNE_PARTENAIRE_KEY);
  }

  public void setToPersonnePartenaireRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toPersonnePartenaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_PARTENAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_PARTENAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonneService() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_PERSONNE_SERVICE_KEY);
  }

  public void setToPersonneServiceRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toPersonneService();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_SERVICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_SERVICE_KEY);
    }
  }
  

  public static EOAccordsContrat createFwkCktlCompta_AccordsContrat(EOEditingContext editingContext, Integer conIndex
, String conObjet
, String numero
, Integer persIdService
, org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice toExercice, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonneService) {
    EOAccordsContrat eo = (EOAccordsContrat) createAndInsertInstance(editingContext, _EOAccordsContrat.ENTITY_NAME);    
		eo.setConIndex(conIndex);
		eo.setConObjet(conObjet);
		eo.setNumero(numero);
		eo.setPersIdService(persIdService);
    eo.setToExerciceRelationship(toExercice);
    eo.setToPersonneServiceRelationship(toPersonneService);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOAccordsContrat.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOAccordsContrat.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOAccordsContrat creerInstance(EOEditingContext editingContext) {
		  		EOAccordsContrat object = (EOAccordsContrat)createAndInsertInstance(editingContext, _EOAccordsContrat.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOAccordsContrat localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAccordsContrat)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOAccordsContrat localInstanceIn(EOEditingContext editingContext, EOAccordsContrat eo) {
    EOAccordsContrat localInstance = (eo == null) ? null : (EOAccordsContrat)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOAccordsContrat#localInstanceIn a la place.
   */
	public static EOAccordsContrat localInstanceOf(EOEditingContext editingContext, EOAccordsContrat eo) {
		return EOAccordsContrat.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOAccordsContrat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOAccordsContrat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAccordsContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAccordsContrat)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAccordsContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAccordsContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAccordsContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAccordsContrat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOAccordsContrat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAccordsContrat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAccordsContrat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAccordsContrat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
