// _EOJefyRecetteRecetteCtrlPlanco.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJefyRecetteRecetteCtrlPlanco.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOJefyRecetteRecetteCtrlPlanco extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_JefyRecette_RecetteCtrlPlanco";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.RECETTE_CTRL_PLANCO";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rpcoId";

	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String RPCO_DATE_SAISIE_KEY = "rpcoDateSaisie";
	public static final String RPCO_HT_SAISIE_KEY = "rpcoHtSaisie";
	public static final String RPCO_TTC_SAISIE_KEY = "rpcoTtcSaisie";
	public static final String RPCO_TVA_SAISIE_KEY = "rpcoTvaSaisie";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String REC_ID_KEY = "recId";
	public static final String RPCO_ID_KEY = "rpcoId";
	public static final String TBO_ORDRE_KEY = "tboOrdre";
	public static final String TIT_ID_KEY = "titId";

//Colonnes dans la base de donnees
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String RPCO_DATE_SAISIE_COLKEY = "RPCO_DATE_SAISIE";
	public static final String RPCO_HT_SAISIE_COLKEY = "RPCO_HT_SAISIE";
	public static final String RPCO_TTC_SAISIE_COLKEY = "RPCO_TTC_SAISIE";
	public static final String RPCO_TVA_SAISIE_COLKEY = "RPCO_TVA_SAISIE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String REC_ID_COLKEY = "REC_ID";
	public static final String RPCO_ID_COLKEY = "RPCO_ID";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";
	public static final String TIT_ID_COLKEY = "TIT_ID";


	// Relationships
	public static final String TO_COMPTA_RECETTES_KEY = "toComptaRecettes";
	public static final String TO_JEFY_RECETTE_RECETTE_KEY = "toJefyRecetteRecette";
	public static final String TO_TITRE_KEY = "toTitre";



	// Accessors methods
  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public NSTimestamp rpcoDateSaisie() {
    return (NSTimestamp) storedValueForKey(RPCO_DATE_SAISIE_KEY);
  }

  public void setRpcoDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, RPCO_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal rpcoHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPCO_HT_SAISIE_KEY);
  }

  public void setRpcoHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPCO_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal rpcoTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPCO_TTC_SAISIE_KEY);
  }

  public void setRpcoTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPCO_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal rpcoTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPCO_TVA_SAISIE_KEY);
  }

  public void setRpcoTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPCO_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette toJefyRecetteRecette() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette)storedValueForKey(TO_JEFY_RECETTE_RECETTE_KEY);
  }

  public void setToJefyRecetteRecetteRelationship(org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette oldValue = toJefyRecetteRecette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_JEFY_RECETTE_RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_JEFY_RECETTE_RECETTE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOTitre toTitre() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOTitre)storedValueForKey(TO_TITRE_KEY);
  }

  public void setToTitreRelationship(org.cocktail.fwkcktlcompta.client.metier.EOTitre value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOTitre oldValue = toTitre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TITRE_KEY);
    }
  }
  
  public NSArray toComptaRecettes() {
    return (NSArray)storedValueForKey(TO_COMPTA_RECETTES_KEY);
  }

  public NSArray toComptaRecettes(EOQualifier qualifier) {
    return toComptaRecettes(qualifier, null, false);
  }

  public NSArray toComptaRecettes(EOQualifier qualifier, Boolean fetch) {
    return toComptaRecettes(qualifier, null, fetch);
  }

  public NSArray toComptaRecettes(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EORecette.TO_RECETTE_CTRL_PLANCO_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EORecette.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toComptaRecettes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToComptaRecettesRelationship(org.cocktail.fwkcktlcompta.client.metier.EORecette object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_COMPTA_RECETTES_KEY);
  }

  public void removeFromToComptaRecettesRelationship(org.cocktail.fwkcktlcompta.client.metier.EORecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTA_RECETTES_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EORecette createToComptaRecettesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_Recette");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_COMPTA_RECETTES_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EORecette) eo;
  }

  public void deleteToComptaRecettesRelationship(org.cocktail.fwkcktlcompta.client.metier.EORecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTA_RECETTES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToComptaRecettesRelationships() {
    Enumeration objects = toComptaRecettes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToComptaRecettesRelationship((org.cocktail.fwkcktlcompta.client.metier.EORecette)objects.nextElement());
    }
  }


  public static EOJefyRecetteRecetteCtrlPlanco createFwkCktlCompta_JefyRecette_RecetteCtrlPlanco(EOEditingContext editingContext, NSTimestamp rpcoDateSaisie
, java.math.BigDecimal rpcoHtSaisie
, java.math.BigDecimal rpcoTtcSaisie
, java.math.BigDecimal rpcoTvaSaisie
, org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecette toJefyRecetteRecette) {
    EOJefyRecetteRecetteCtrlPlanco eo = (EOJefyRecetteRecetteCtrlPlanco) createAndInsertInstance(editingContext, _EOJefyRecetteRecetteCtrlPlanco.ENTITY_NAME);    
		eo.setRpcoDateSaisie(rpcoDateSaisie);
		eo.setRpcoHtSaisie(rpcoHtSaisie);
		eo.setRpcoTtcSaisie(rpcoTtcSaisie);
		eo.setRpcoTvaSaisie(rpcoTvaSaisie);
    eo.setToJefyRecetteRecetteRelationship(toJefyRecetteRecette);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOJefyRecetteRecetteCtrlPlanco.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOJefyRecetteRecetteCtrlPlanco.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOJefyRecetteRecetteCtrlPlanco creerInstance(EOEditingContext editingContext) {
		  		EOJefyRecetteRecetteCtrlPlanco object = (EOJefyRecetteRecetteCtrlPlanco)createAndInsertInstance(editingContext, _EOJefyRecetteRecetteCtrlPlanco.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOJefyRecetteRecetteCtrlPlanco localInstanceIn(EOEditingContext editingContext) {
	  		return (EOJefyRecetteRecetteCtrlPlanco)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOJefyRecetteRecetteCtrlPlanco localInstanceIn(EOEditingContext editingContext, EOJefyRecetteRecetteCtrlPlanco eo) {
    EOJefyRecetteRecetteCtrlPlanco localInstance = (eo == null) ? null : (EOJefyRecetteRecetteCtrlPlanco)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOJefyRecetteRecetteCtrlPlanco#localInstanceIn a la place.
   */
	public static EOJefyRecetteRecetteCtrlPlanco localInstanceOf(EOEditingContext editingContext, EOJefyRecetteRecetteCtrlPlanco eo) {
		return EOJefyRecetteRecetteCtrlPlanco.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOJefyRecetteRecetteCtrlPlanco fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOJefyRecetteRecetteCtrlPlanco fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJefyRecetteRecetteCtrlPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJefyRecetteRecetteCtrlPlanco)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJefyRecetteRecetteCtrlPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJefyRecetteRecetteCtrlPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJefyRecetteRecetteCtrlPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJefyRecetteRecetteCtrlPlanco)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOJefyRecetteRecetteCtrlPlanco fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJefyRecetteRecetteCtrlPlanco eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJefyRecetteRecetteCtrlPlanco ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJefyRecetteRecetteCtrlPlanco fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
