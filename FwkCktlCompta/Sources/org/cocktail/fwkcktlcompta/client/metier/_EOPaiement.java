// _EOPaiement.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPaiement.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPaiement extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Paiement";
	public static final String ENTITY_TABLE_NAME = "maracuja.PAIEMENT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "paiOrdre";

	public static final String PAI_DATE_CREATION_KEY = "paiDateCreation";
	public static final String PAI_MONTANT_KEY = "paiMontant";
	public static final String PAI_NB_VIREMENTS_KEY = "paiNbVirements";
	public static final String PAI_NUMERO_KEY = "paiNumero";

// Attributs non visibles
	public static final String COM_ORDRE_KEY = "comOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PAI_ORDRE_KEY = "paiOrdre";
	public static final String TVI_ORDRE_KEY = "tviOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String PAI_DATE_CREATION_COLKEY = "pai_Date_Creation";
	public static final String PAI_MONTANT_COLKEY = "pai_Montant";
	public static final String PAI_NB_VIREMENTS_COLKEY = "pai_Nb_Virements";
	public static final String PAI_NUMERO_COLKEY = "pai_Numero";

	public static final String COM_ORDRE_COLKEY = "com_Ordre";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PAI_ORDRE_COLKEY = "pai_ORDRE";
	public static final String TVI_ORDRE_COLKEY = "tvi_Ordre";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String TO_COMPTABILITE_KEY = "toComptabilite";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_MANDATS_KEY = "toMandats";
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";



	// Accessors methods
  public NSTimestamp paiDateCreation() {
    return (NSTimestamp) storedValueForKey(PAI_DATE_CREATION_KEY);
  }

  public void setPaiDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, PAI_DATE_CREATION_KEY);
  }

  public java.math.BigDecimal paiMontant() {
    return (java.math.BigDecimal) storedValueForKey(PAI_MONTANT_KEY);
  }

  public void setPaiMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAI_MONTANT_KEY);
  }

  public Integer paiNbVirements() {
    return (Integer) storedValueForKey(PAI_NB_VIREMENTS_KEY);
  }

  public void setPaiNbVirements(Integer value) {
    takeStoredValueForKey(value, PAI_NB_VIREMENTS_KEY);
  }

  public Integer paiNumero() {
    return (Integer) storedValueForKey(PAI_NUMERO_KEY);
  }

  public void setPaiNumero(Integer value) {
    takeStoredValueForKey(value, PAI_NUMERO_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOComptabilite toComptabilite() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOComptabilite)storedValueForKey(TO_COMPTABILITE_KEY);
  }

  public void setToComptabiliteRelationship(org.cocktail.fwkcktlcompta.client.metier.EOComptabilite value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOComptabilite oldValue = toComptabilite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COMPTABILITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_COMPTABILITE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateur() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
  }

  public void setToUtilisateurRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = toUtilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
    }
  }
  
  public NSArray toMandats() {
    return (NSArray)storedValueForKey(TO_MANDATS_KEY);
  }

  public NSArray toMandats(EOQualifier qualifier) {
    return toMandats(qualifier, null, false);
  }

  public NSArray toMandats(EOQualifier qualifier, Boolean fetch) {
    return toMandats(qualifier, null, fetch);
  }

  public NSArray toMandats(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOMandat.TO_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOMandat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toMandats();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToMandatsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOMandat object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_MANDATS_KEY);
  }

  public void removeFromToMandatsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOMandat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MANDATS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOMandat createToMandatsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_Mandat");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_MANDATS_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOMandat) eo;
  }

  public void deleteToMandatsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOMandat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MANDATS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToMandatsRelationships() {
    Enumeration objects = toMandats().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToMandatsRelationship((org.cocktail.fwkcktlcompta.client.metier.EOMandat)objects.nextElement());
    }
  }


  public static EOPaiement createFwkCktlCompta_Paiement(EOEditingContext editingContext, NSTimestamp paiDateCreation
, java.math.BigDecimal paiMontant
, Integer paiNbVirements
, Integer paiNumero
, org.cocktail.fwkcktlcompta.client.metier.EOComptabilite toComptabilite, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice, org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateur) {
    EOPaiement eo = (EOPaiement) createAndInsertInstance(editingContext, _EOPaiement.ENTITY_NAME);    
		eo.setPaiDateCreation(paiDateCreation);
		eo.setPaiMontant(paiMontant);
		eo.setPaiNbVirements(paiNbVirements);
		eo.setPaiNumero(paiNumero);
    eo.setToComptabiliteRelationship(toComptabilite);
    eo.setToExerciceRelationship(toExercice);
    eo.setToUtilisateurRelationship(toUtilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPaiement.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPaiement.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOPaiement creerInstance(EOEditingContext editingContext) {
		  		EOPaiement object = (EOPaiement)createAndInsertInstance(editingContext, _EOPaiement.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOPaiement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPaiement)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPaiement localInstanceIn(EOEditingContext editingContext, EOPaiement eo) {
    EOPaiement localInstance = (eo == null) ? null : (EOPaiement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPaiement#localInstanceIn a la place.
   */
	public static EOPaiement localInstanceOf(EOEditingContext editingContext, EOPaiement eo) {
		return EOPaiement.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPaiement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPaiement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPaiement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPaiement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPaiement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPaiement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPaiement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPaiement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
