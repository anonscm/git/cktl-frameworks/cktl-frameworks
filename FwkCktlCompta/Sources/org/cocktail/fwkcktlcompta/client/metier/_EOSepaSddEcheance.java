// _EOSepaSddEcheance.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSepaSddEcheance.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOSepaSddEcheance extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddEcheance";
	public static final String ENTITY_TABLE_NAME = "maracuja.SEPA_SDD_ECHEANCE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idSepaSddEcheance";

	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String CREANCIER_ICS_KEY = "creancierIcs";
	public static final String CREANCIER_ID_KEY = "creancierId";
	public static final String CREANCIER_NOM_KEY = "creancierNom";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEBITEUR_BIC_KEY = "debiteurBic";
	public static final String DEBITEUR_IBAN_KEY = "debiteurIban";
	public static final String DEBITEUR_ID_KEY = "debiteurId";
	public static final String DEBITEUR_NOM_KEY = "debiteurNom";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_PRELEVE_KEY = "dPreleve";
	public static final String D_PREVUE_KEY = "dPrevue";
	public static final String ETAT_KEY = "etat";
	public static final String MONTANT_KEY = "montant";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String RUM_KEY = "rum";
	public static final String SDD_TYPE_OP_KEY = "sddTypeOp";

// Attributs non visibles
	public static final String ID_SEPA_SDD_ECHEANCE_KEY = "idSepaSddEcheance";
	public static final String ID_SEPA_SDD_ECHEANCIER_KEY = "idSepaSddEcheancier";
	public static final String RECO_ORDRE_KEY = "recoOrdre";

//Colonnes dans la base de donnees
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String CREANCIER_ICS_COLKEY = "CREANCIER_ICS";
	public static final String CREANCIER_ID_COLKEY = "CREANCIER_ID";
	public static final String CREANCIER_NOM_COLKEY = "CREANCIER_NOM";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String DEBITEUR_BIC_COLKEY = "DEBITEUR_BIC";
	public static final String DEBITEUR_IBAN_COLKEY = "DEBITEUR_IBAN";
	public static final String DEBITEUR_ID_COLKEY = "DEBITEUR_ID";
	public static final String DEBITEUR_NOM_COLKEY = "DEBITEUR_NOM";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_PRELEVE_COLKEY = "D_PRELEVE";
	public static final String D_PREVUE_COLKEY = "D_PREVUE";
	public static final String ETAT_COLKEY = "ETAT";
	public static final String MONTANT_COLKEY = "MONTANT";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String RUM_COLKEY = "RUM";
	public static final String SDD_TYPE_OP_COLKEY = "SDD_TYPE_OP";

	public static final String ID_SEPA_SDD_ECHEANCE_COLKEY = "ID_SEPA_SDD_ECHEANCE";
	public static final String ID_SEPA_SDD_ECHEANCIER_COLKEY = "ID_SEPA_SDD_ECHEANCIER";
	public static final String RECO_ORDRE_COLKEY = "RECO_ORDRE";


	// Relationships
	public static final String TO_MODIFICATEUR_KEY = "toModificateur";
	public static final String TO_RECOUVREMENT_KEY = "toRecouvrement";
	public static final String TO_SEPA_SDD_ECHEANCE_ECDS_KEY = "toSepaSddEcheanceEcds";
	public static final String TO_SEPA_SDD_ECHEANCIER_KEY = "toSepaSddEcheancier";



	// Accessors methods
  public String commentaire() {
    return (String) storedValueForKey(COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    takeStoredValueForKey(value, COMMENTAIRE_KEY);
  }

  public String creancierIcs() {
    return (String) storedValueForKey(CREANCIER_ICS_KEY);
  }

  public void setCreancierIcs(String value) {
    takeStoredValueForKey(value, CREANCIER_ICS_KEY);
  }

  public String creancierId() {
    return (String) storedValueForKey(CREANCIER_ID_KEY);
  }

  public void setCreancierId(String value) {
    takeStoredValueForKey(value, CREANCIER_ID_KEY);
  }

  public String creancierNom() {
    return (String) storedValueForKey(CREANCIER_NOM_KEY);
  }

  public void setCreancierNom(String value) {
    takeStoredValueForKey(value, CREANCIER_NOM_KEY);
  }

  public String dCreation() {
    return (String) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(String value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public String debiteurBic() {
    return (String) storedValueForKey(DEBITEUR_BIC_KEY);
  }

  public void setDebiteurBic(String value) {
    takeStoredValueForKey(value, DEBITEUR_BIC_KEY);
  }

  public String debiteurIban() {
    return (String) storedValueForKey(DEBITEUR_IBAN_KEY);
  }

  public void setDebiteurIban(String value) {
    takeStoredValueForKey(value, DEBITEUR_IBAN_KEY);
  }

  public String debiteurId() {
    return (String) storedValueForKey(DEBITEUR_ID_KEY);
  }

  public void setDebiteurId(String value) {
    takeStoredValueForKey(value, DEBITEUR_ID_KEY);
  }

  public String debiteurNom() {
    return (String) storedValueForKey(DEBITEUR_NOM_KEY);
  }

  public void setDebiteurNom(String value) {
    takeStoredValueForKey(value, DEBITEUR_NOM_KEY);
  }

  public String dModification() {
    return (String) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(String value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String dPreleve() {
    return (String) storedValueForKey(D_PRELEVE_KEY);
  }

  public void setDPreleve(String value) {
    takeStoredValueForKey(value, D_PRELEVE_KEY);
  }

  public String dPrevue() {
    return (String) storedValueForKey(D_PREVUE_KEY);
  }

  public void setDPrevue(String value) {
    takeStoredValueForKey(value, D_PREVUE_KEY);
  }

  public String etat() {
    return (String) storedValueForKey(ETAT_KEY);
  }

  public void setEtat(String value) {
    takeStoredValueForKey(value, ETAT_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String rum() {
    return (String) storedValueForKey(RUM_KEY);
  }

  public void setRum(String value) {
    takeStoredValueForKey(value, RUM_KEY);
  }

  public String sddTypeOp() {
    return (String) storedValueForKey(SDD_TYPE_OP_KEY);
  }

  public void setSddTypeOp(String value) {
    takeStoredValueForKey(value, SDD_TYPE_OP_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toModificateur() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_MODIFICATEUR_KEY);
  }

  public void setToModificateurRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toModificateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODIFICATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODIFICATEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EORecouvrement toRecouvrement() {
    return (org.cocktail.fwkcktlcompta.client.metier.EORecouvrement)storedValueForKey(TO_RECOUVREMENT_KEY);
  }

  public void setToRecouvrementRelationship(org.cocktail.fwkcktlcompta.client.metier.EORecouvrement value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EORecouvrement oldValue = toRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier toSepaSddEcheancier() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier)storedValueForKey(TO_SEPA_SDD_ECHEANCIER_KEY);
  }

  public void setToSepaSddEcheancierRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier oldValue = toSepaSddEcheancier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SEPA_SDD_ECHEANCIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SEPA_SDD_ECHEANCIER_KEY);
    }
  }
  
  public NSArray toSepaSddEcheanceEcds() {
    return (NSArray)storedValueForKey(TO_SEPA_SDD_ECHEANCE_ECDS_KEY);
  }

  public NSArray toSepaSddEcheanceEcds(EOQualifier qualifier) {
    return toSepaSddEcheanceEcds(qualifier, null, false);
  }

  public NSArray toSepaSddEcheanceEcds(EOQualifier qualifier, Boolean fetch) {
    return toSepaSddEcheanceEcds(qualifier, null, fetch);
  }

  public NSArray toSepaSddEcheanceEcds(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheanceEcd.TO_SEPA_SDD_ECHEANCE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheanceEcd.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSepaSddEcheanceEcds();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSepaSddEcheanceEcdsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheanceEcd object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCE_ECDS_KEY);
  }

  public void removeFromToSepaSddEcheanceEcdsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheanceEcd object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCE_ECDS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheanceEcd createToSepaSddEcheanceEcdsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_SepaSddEcheanceEcd");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_SEPA_SDD_ECHEANCE_ECDS_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheanceEcd) eo;
  }

  public void deleteToSepaSddEcheanceEcdsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheanceEcd object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCE_ECDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToSepaSddEcheanceEcdsRelationships() {
    Enumeration objects = toSepaSddEcheanceEcds().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSepaSddEcheanceEcdsRelationship((org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheanceEcd)objects.nextElement());
    }
  }


  public static EOSepaSddEcheance createFwkCktlCompta_SepaSddEcheance(EOEditingContext editingContext, String dCreation
, String dModification
, String dPrevue
, String etat
, java.math.BigDecimal montant
, Integer persIdCreation
, Integer persIdModification
, org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toModificateur, org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier toSepaSddEcheancier) {
    EOSepaSddEcheance eo = (EOSepaSddEcheance) createAndInsertInstance(editingContext, _EOSepaSddEcheance.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setDPrevue(dPrevue);
		eo.setEtat(etat);
		eo.setMontant(montant);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
    eo.setToModificateurRelationship(toModificateur);
    eo.setToSepaSddEcheancierRelationship(toSepaSddEcheancier);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOSepaSddEcheance.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOSepaSddEcheance.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOSepaSddEcheance creerInstance(EOEditingContext editingContext) {
		  		EOSepaSddEcheance object = (EOSepaSddEcheance)createAndInsertInstance(editingContext, _EOSepaSddEcheance.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOSepaSddEcheance localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSepaSddEcheance)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOSepaSddEcheance localInstanceIn(EOEditingContext editingContext, EOSepaSddEcheance eo) {
    EOSepaSddEcheance localInstance = (eo == null) ? null : (EOSepaSddEcheance)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOSepaSddEcheance#localInstanceIn a la place.
   */
	public static EOSepaSddEcheance localInstanceOf(EOEditingContext editingContext, EOSepaSddEcheance eo) {
		return EOSepaSddEcheance.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOSepaSddEcheance fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOSepaSddEcheance fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSepaSddEcheance eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSepaSddEcheance)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSepaSddEcheance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSepaSddEcheance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSepaSddEcheance eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSepaSddEcheance)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOSepaSddEcheance fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSepaSddEcheance eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSepaSddEcheance ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSepaSddEcheance fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
