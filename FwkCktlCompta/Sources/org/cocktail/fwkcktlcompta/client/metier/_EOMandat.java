// _EOMandat.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMandat.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOMandat extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Mandat";
	public static final String ENTITY_TABLE_NAME = "maracuja.Mandat";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "manId";

	public static final String MAN_ATTENTE_DATE_KEY = "manAttenteDate";
	public static final String MAN_ATTENTE_OBJET_KEY = "manAttenteObjet";
	public static final String MAN_DATE_REMISE_KEY = "manDateRemise";
	public static final String MAN_DATE_VISA_PRINC_KEY = "manDateVisaPrinc";
	public static final String MAN_ETAT_KEY = "manEtat";
	public static final String MAN_ETAT_REMISE_KEY = "manEtatRemise";
	public static final String MAN_HT_KEY = "manHt";
	public static final String MAN_MOTIF_REJET_KEY = "manMotifRejet";
	public static final String MAN_NB_PIECE_KEY = "manNbPiece";
	public static final String MAN_NUMERO_KEY = "manNumero";
	public static final String MAN_NUMERO_REJET_KEY = "manNumeroRejet";
	public static final String MAN_ORDRE_KEY = "manOrdre";
	public static final String MAN_ORIGINE_KEY_KEY = "manOrigineKey";
	public static final String MAN_ORIGINE_LIB_KEY = "manOrigineLib";
	public static final String MAN_TTC_KEY = "manTtc";
	public static final String MAN_TVA_KEY = "manTva";
	public static final String PREST_ID_KEY = "prestId";

// Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String BRJ_ORDRE_KEY = "brjOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MAN_ATTENTE_PAIEMENT_KEY = "manAttentePaiement";
	public static final String MAN_ID_KEY = "manId";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String PAI_ORDRE_KEY = "paiOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String RIB_ORDRE_COMPTABLE_KEY = "ribOrdreComptable";
	public static final String RIB_ORDRE_ORDONNATEUR_KEY = "ribOrdreOrdonnateur";
	public static final String TOR_ORDRE_KEY = "torOrdre";
	public static final String UTL_ORDRE_ATTENTE_KEY = "utlOrdreAttente";

//Colonnes dans la base de donnees
	public static final String MAN_ATTENTE_DATE_COLKEY = "man_attente_date";
	public static final String MAN_ATTENTE_OBJET_COLKEY = "man_attente_objet";
	public static final String MAN_DATE_REMISE_COLKEY = "man_Date_Remise";
	public static final String MAN_DATE_VISA_PRINC_COLKEY = "man_Date_Visa_Princ";
	public static final String MAN_ETAT_COLKEY = "man_etat";
	public static final String MAN_ETAT_REMISE_COLKEY = "man_Etat_Remise";
	public static final String MAN_HT_COLKEY = "man_Ht";
	public static final String MAN_MOTIF_REJET_COLKEY = "man_Motif_Rejet";
	public static final String MAN_NB_PIECE_COLKEY = "man_NB_PIECE";
	public static final String MAN_NUMERO_COLKEY = "man_Numero";
	public static final String MAN_NUMERO_REJET_COLKEY = "man_Numero_rejet";
	public static final String MAN_ORDRE_COLKEY = "MAN_ORDRE";
	public static final String MAN_ORIGINE_KEY_COLKEY = "man_orgine_key";
	public static final String MAN_ORIGINE_LIB_COLKEY = "man_Origine_Lib";
	public static final String MAN_TTC_COLKEY = "man_Ttc";
	public static final String MAN_TVA_COLKEY = "man_Tva";
	public static final String PREST_ID_COLKEY = "prest_id";

	public static final String BOR_ID_COLKEY = "BOR_id";
	public static final String BRJ_ORDRE_COLKEY = "BRJ_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "fou_Ordre";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String MAN_ATTENTE_PAIEMENT_COLKEY = "man_attente_paiement";
	public static final String MAN_ID_COLKEY = "man_id";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String ORG_ORDRE_COLKEY = "org_ordre";
	public static final String ORI_ORDRE_COLKEY = "ori_ordre";
	public static final String PAI_ORDRE_COLKEY = "pai_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String RIB_ORDRE_COMPTABLE_COLKEY = "rib_ordre_comptable";
	public static final String RIB_ORDRE_ORDONNATEUR_COLKEY = "rib_ordre_ordonnateur";
	public static final String TOR_ORDRE_COLKEY = "tor_Ordre";
	public static final String UTL_ORDRE_ATTENTE_COLKEY = "utl_ordre_attente";


	// Relationships
	public static final String TO_BORDEREAU_KEY = "toBordereau";
	public static final String TO_BORDEREAU_REJET_KEY = "toBordereauRejet";
	public static final String TO_DEPENSES_KEY = "toDepenses";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_FOURNIS_KEY = "toFournis";
	public static final String TO_GESTION_KEY = "toGestion";
	public static final String TO_MANDAT_BROUILLARDS_KEY = "toMandatBrouillards";
	public static final String TO_MODE_PAIEMENT_KEY = "toModePaiement";
	public static final String TO_ORGAN_KEY = "toOrgan";
	public static final String TO_ORIGINE_KEY = "toOrigine";
	public static final String TO_PAIEMENT_KEY = "toPaiement";
	public static final String TO_PLAN_COMPTABLE_EXER_KEY = "toPlanComptableExer";
	public static final String TO_RIB_KEY = "toRib";
	public static final String TO_RIB_ORDONNATEUR_KEY = "toRibOrdonnateur";
	public static final String TO_TYPE_ORIGINE_BORDEREAU_KEY = "toTypeOrigineBordereau";



	// Accessors methods
  public NSTimestamp manAttenteDate() {
    return (NSTimestamp) storedValueForKey(MAN_ATTENTE_DATE_KEY);
  }

  public void setManAttenteDate(NSTimestamp value) {
    takeStoredValueForKey(value, MAN_ATTENTE_DATE_KEY);
  }

  public String manAttenteObjet() {
    return (String) storedValueForKey(MAN_ATTENTE_OBJET_KEY);
  }

  public void setManAttenteObjet(String value) {
    takeStoredValueForKey(value, MAN_ATTENTE_OBJET_KEY);
  }

  public NSTimestamp manDateRemise() {
    return (NSTimestamp) storedValueForKey(MAN_DATE_REMISE_KEY);
  }

  public void setManDateRemise(NSTimestamp value) {
    takeStoredValueForKey(value, MAN_DATE_REMISE_KEY);
  }

  public NSTimestamp manDateVisaPrinc() {
    return (NSTimestamp) storedValueForKey(MAN_DATE_VISA_PRINC_KEY);
  }

  public void setManDateVisaPrinc(NSTimestamp value) {
    takeStoredValueForKey(value, MAN_DATE_VISA_PRINC_KEY);
  }

  public String manEtat() {
    return (String) storedValueForKey(MAN_ETAT_KEY);
  }

  public void setManEtat(String value) {
    takeStoredValueForKey(value, MAN_ETAT_KEY);
  }

  public String manEtatRemise() {
    return (String) storedValueForKey(MAN_ETAT_REMISE_KEY);
  }

  public void setManEtatRemise(String value) {
    takeStoredValueForKey(value, MAN_ETAT_REMISE_KEY);
  }

  public java.math.BigDecimal manHt() {
    return (java.math.BigDecimal) storedValueForKey(MAN_HT_KEY);
  }

  public void setManHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MAN_HT_KEY);
  }

  public String manMotifRejet() {
    return (String) storedValueForKey(MAN_MOTIF_REJET_KEY);
  }

  public void setManMotifRejet(String value) {
    takeStoredValueForKey(value, MAN_MOTIF_REJET_KEY);
  }

  public Integer manNbPiece() {
    return (Integer) storedValueForKey(MAN_NB_PIECE_KEY);
  }

  public void setManNbPiece(Integer value) {
    takeStoredValueForKey(value, MAN_NB_PIECE_KEY);
  }

  public Integer manNumero() {
    return (Integer) storedValueForKey(MAN_NUMERO_KEY);
  }

  public void setManNumero(Integer value) {
    takeStoredValueForKey(value, MAN_NUMERO_KEY);
  }

  public Integer manNumeroRejet() {
    return (Integer) storedValueForKey(MAN_NUMERO_REJET_KEY);
  }

  public void setManNumeroRejet(Integer value) {
    takeStoredValueForKey(value, MAN_NUMERO_REJET_KEY);
  }

  public Integer manOrdre() {
    return (Integer) storedValueForKey(MAN_ORDRE_KEY);
  }

  public void setManOrdre(Integer value) {
    takeStoredValueForKey(value, MAN_ORDRE_KEY);
  }

  public Integer manOrigineKey() {
    return (Integer) storedValueForKey(MAN_ORIGINE_KEY_KEY);
  }

  public void setManOrigineKey(Integer value) {
    takeStoredValueForKey(value, MAN_ORIGINE_KEY_KEY);
  }

  public String manOrigineLib() {
    return (String) storedValueForKey(MAN_ORIGINE_LIB_KEY);
  }

  public void setManOrigineLib(String value) {
    takeStoredValueForKey(value, MAN_ORIGINE_LIB_KEY);
  }

  public java.math.BigDecimal manTtc() {
    return (java.math.BigDecimal) storedValueForKey(MAN_TTC_KEY);
  }

  public void setManTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MAN_TTC_KEY);
  }

  public java.math.BigDecimal manTva() {
    return (java.math.BigDecimal) storedValueForKey(MAN_TVA_KEY);
  }

  public void setManTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MAN_TVA_KEY);
  }

  public Integer prestId() {
    return (Integer) storedValueForKey(PREST_ID_KEY);
  }

  public void setPrestId(Integer value) {
    takeStoredValueForKey(value, PREST_ID_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOBordereau toBordereau() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOBordereau)storedValueForKey(TO_BORDEREAU_KEY);
  }

  public void setToBordereauRelationship(org.cocktail.fwkcktlcompta.client.metier.EOBordereau value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOBordereau oldValue = toBordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOBordereauRejet toBordereauRejet() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOBordereauRejet)storedValueForKey(TO_BORDEREAU_REJET_KEY);
  }

  public void setToBordereauRejetRelationship(org.cocktail.fwkcktlcompta.client.metier.EOBordereauRejet value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOBordereauRejet oldValue = toBordereauRejet();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BORDEREAU_REJET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BORDEREAU_REJET_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFournis() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFournis)storedValueForKey(TO_FOURNIS_KEY);
  }

  public void setToFournisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOFournis oldValue = toFournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_FOURNIS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
  }

  public void setToGestionRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGestion oldValue = toGestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOModePaiement toModePaiement() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOModePaiement)storedValueForKey(TO_MODE_PAIEMENT_KEY);
  }

  public void setToModePaiementRelationship(org.cocktail.fwkcktlcompta.client.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOModePaiement oldValue = toModePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan toOrgan() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(TO_ORGAN_KEY);
  }

  public void setToOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = toOrgan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOOrigine toOrigine() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOOrigine)storedValueForKey(TO_ORIGINE_KEY);
  }

  public void setToOrigineRelationship(org.cocktail.fwkcktlcompta.client.metier.EOOrigine value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOOrigine oldValue = toOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORIGINE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOPaiement toPaiement() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOPaiement)storedValueForKey(TO_PAIEMENT_KEY);
  }

  public void setToPaiementRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPaiement value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOPaiement oldValue = toPaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer toPlanComptableExer() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer)storedValueForKey(TO_PLAN_COMPTABLE_EXER_KEY);
  }

  public void setToPlanComptableExerRelationship(org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer oldValue = toPlanComptableExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_EXER_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORib toRib() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORib)storedValueForKey(TO_RIB_KEY);
  }

  public void setToRibRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORib value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORib oldValue = toRib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RIB_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORib toRibOrdonnateur() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORib)storedValueForKey(TO_RIB_ORDONNATEUR_KEY);
  }

  public void setToRibOrdonnateurRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORib value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORib oldValue = toRibOrdonnateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RIB_ORDONNATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RIB_ORDONNATEUR_KEY);
    }
  }
  
  public com.webobjects.eocontrol.EOGenericRecord toTypeOrigineBordereau() {
    return (com.webobjects.eocontrol.EOGenericRecord)storedValueForKey(TO_TYPE_ORIGINE_BORDEREAU_KEY);
  }

  public void setToTypeOrigineBordereauRelationship(com.webobjects.eocontrol.EOGenericRecord value) {
    if (value == null) {
    	com.webobjects.eocontrol.EOGenericRecord oldValue = toTypeOrigineBordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ORIGINE_BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ORIGINE_BORDEREAU_KEY);
    }
  }
  
  public NSArray toDepenses() {
    return (NSArray)storedValueForKey(TO_DEPENSES_KEY);
  }

  public NSArray toDepenses(EOQualifier qualifier) {
    return toDepenses(qualifier, null, false);
  }

  public NSArray toDepenses(EOQualifier qualifier, Boolean fetch) {
    return toDepenses(qualifier, null, fetch);
  }

  public NSArray toDepenses(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EODepense.TO_MANDAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EODepense.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toDepenses();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToDepensesRelationship(org.cocktail.fwkcktlcompta.client.metier.EODepense object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_DEPENSES_KEY);
  }

  public void removeFromToDepensesRelationship(org.cocktail.fwkcktlcompta.client.metier.EODepense object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DEPENSES_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EODepense createToDepensesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_Depense");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_DEPENSES_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EODepense) eo;
  }

  public void deleteToDepensesRelationship(org.cocktail.fwkcktlcompta.client.metier.EODepense object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DEPENSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToDepensesRelationships() {
    Enumeration objects = toDepenses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToDepensesRelationship((org.cocktail.fwkcktlcompta.client.metier.EODepense)objects.nextElement());
    }
  }

  public NSArray toMandatBrouillards() {
    return (NSArray)storedValueForKey(TO_MANDAT_BROUILLARDS_KEY);
  }

  public NSArray toMandatBrouillards(EOQualifier qualifier) {
    return toMandatBrouillards(qualifier, null, false);
  }

  public NSArray toMandatBrouillards(EOQualifier qualifier, Boolean fetch) {
    return toMandatBrouillards(qualifier, null, fetch);
  }

  public NSArray toMandatBrouillards(EOQualifier qualifier, NSArray sortOrderings, Boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.client.metier.EOMandatBrouillard.TO_MANDAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlcompta.client.metier.EOMandatBrouillard.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toMandatBrouillards();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToMandatBrouillardsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOMandatBrouillard object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_MANDAT_BROUILLARDS_KEY);
  }

  public void removeFromToMandatBrouillardsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOMandatBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MANDAT_BROUILLARDS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOMandatBrouillard createToMandatBrouillardsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_MandatBrouillard");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_MANDAT_BROUILLARDS_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOMandatBrouillard) eo;
  }

  public void deleteToMandatBrouillardsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOMandatBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MANDAT_BROUILLARDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToMandatBrouillardsRelationships() {
    Enumeration objects = toMandatBrouillards().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToMandatBrouillardsRelationship((org.cocktail.fwkcktlcompta.client.metier.EOMandatBrouillard)objects.nextElement());
    }
  }


  public static EOMandat createFwkCktlCompta_Mandat(EOEditingContext editingContext, String manEtat
, java.math.BigDecimal manHt
, Integer manNumero
, Integer manOrdre
, java.math.BigDecimal manTtc
, java.math.BigDecimal manTva
, org.cocktail.fwkcktlcompta.client.metier.EOBordereau toBordereau, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice, org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFournis, org.cocktail.fwkcktlcompta.client.metier.EOGestion toGestion, org.cocktail.fwkcktlcompta.client.metier.EOModePaiement toModePaiement, org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer toPlanComptableExer, com.webobjects.eocontrol.EOGenericRecord toTypeOrigineBordereau) {
    EOMandat eo = (EOMandat) createAndInsertInstance(editingContext, _EOMandat.ENTITY_NAME);    
		eo.setManEtat(manEtat);
		eo.setManHt(manHt);
		eo.setManNumero(manNumero);
		eo.setManOrdre(manOrdre);
		eo.setManTtc(manTtc);
		eo.setManTva(manTva);
    eo.setToBordereauRelationship(toBordereau);
    eo.setToExerciceRelationship(toExercice);
    eo.setToFournisRelationship(toFournis);
    eo.setToGestionRelationship(toGestion);
    eo.setToModePaiementRelationship(toModePaiement);
    eo.setToPlanComptableExerRelationship(toPlanComptableExer);
    eo.setToTypeOrigineBordereauRelationship(toTypeOrigineBordereau);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOMandat.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOMandat.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOMandat creerInstance(EOEditingContext editingContext) {
		  		EOMandat object = (EOMandat)createAndInsertInstance(editingContext, _EOMandat.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOMandat localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMandat)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOMandat localInstanceIn(EOEditingContext editingContext, EOMandat eo) {
    EOMandat localInstance = (eo == null) ? null : (EOMandat)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOMandat#localInstanceIn a la place.
   */
	public static EOMandat localInstanceOf(EOEditingContext editingContext, EOMandat eo) {
		return EOMandat.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOMandat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOMandat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMandat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMandat)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMandat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMandat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMandat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMandat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOMandat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMandat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMandat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMandat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
