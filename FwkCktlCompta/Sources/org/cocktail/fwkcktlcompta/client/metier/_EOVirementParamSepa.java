// _EOVirementParamSepa.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVirementParamSepa.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVirementParamSepa extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_VirementParamSepa";
	public static final String ENTITY_TABLE_NAME = "maracuja.VIREMENT_PARAM_SEPA";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "vpsOrdre";

	public static final String TVI_ORDRE_KEY = "tviOrdre";
	public static final String VPS_DFT_DEVISE_KEY = "vpsDftDevise";
	public static final String VPS_DFT_IBAN_KEY = "vpsDftIban";
	public static final String VPS_DFT_TITULAIRE_KEY = "vpsDftTitulaire";
	public static final String VPS_DFT_TRANSFERT_ID_KEY = "vpsDftTransfertId";
	public static final String VPS_EMETTEUR_NOM_KEY = "vpsEmetteurNom";
	public static final String VPS_ETAT_KEY = "vpsEtat";
	public static final String VPS_TG_BIC_KEY = "vpsTgBic";
	public static final String VPS_TG_CODIQUE_KEY = "vpsTgCodique";
	public static final String VPS_TG_IBAN_KEY = "vpsTgIban";
	public static final String VPS_TG_NOM_KEY = "vpsTgNom";

// Attributs non visibles
	public static final String VPS_ORDRE_KEY = "vpsOrdre";

//Colonnes dans la base de donnees
	public static final String TVI_ORDRE_COLKEY = "TVI_ORDRE";
	public static final String VPS_DFT_DEVISE_COLKEY = "VPS_DFT_DEVISE";
	public static final String VPS_DFT_IBAN_COLKEY = "VPS_DFT_IBAN";
	public static final String VPS_DFT_TITULAIRE_COLKEY = "VPS_DFT_TITULAIRE";
	public static final String VPS_DFT_TRANSFERT_ID_COLKEY = "VPS_DFT_TRANSFERT_ID";
	public static final String VPS_EMETTEUR_NOM_COLKEY = "VPS_EMETTEUR_NOM";
	public static final String VPS_ETAT_COLKEY = "VPS_ETAT";
	public static final String VPS_TG_BIC_COLKEY = "VPS_TG_BIC";
	public static final String VPS_TG_CODIQUE_COLKEY = "VPS_TG_CODIQUE";
	public static final String VPS_TG_IBAN_COLKEY = "VPS_TG_IBAN";
	public static final String VPS_TG_NOM_COLKEY = "VPS_TG_NOM";

	public static final String VPS_ORDRE_COLKEY = "VPS_ORDRE";


	// Relationships
	public static final String TO_TYPE_VIREMENT_KEY = "toTypeVirement";



	// Accessors methods
  public Integer tviOrdre() {
    return (Integer) storedValueForKey(TVI_ORDRE_KEY);
  }

  public void setTviOrdre(Integer value) {
    takeStoredValueForKey(value, TVI_ORDRE_KEY);
  }

  public String vpsDftDevise() {
    return (String) storedValueForKey(VPS_DFT_DEVISE_KEY);
  }

  public void setVpsDftDevise(String value) {
    takeStoredValueForKey(value, VPS_DFT_DEVISE_KEY);
  }

  public String vpsDftIban() {
    return (String) storedValueForKey(VPS_DFT_IBAN_KEY);
  }

  public void setVpsDftIban(String value) {
    takeStoredValueForKey(value, VPS_DFT_IBAN_KEY);
  }

  public String vpsDftTitulaire() {
    return (String) storedValueForKey(VPS_DFT_TITULAIRE_KEY);
  }

  public void setVpsDftTitulaire(String value) {
    takeStoredValueForKey(value, VPS_DFT_TITULAIRE_KEY);
  }

  public String vpsDftTransfertId() {
    return (String) storedValueForKey(VPS_DFT_TRANSFERT_ID_KEY);
  }

  public void setVpsDftTransfertId(String value) {
    takeStoredValueForKey(value, VPS_DFT_TRANSFERT_ID_KEY);
  }

  public String vpsEmetteurNom() {
    return (String) storedValueForKey(VPS_EMETTEUR_NOM_KEY);
  }

  public void setVpsEmetteurNom(String value) {
    takeStoredValueForKey(value, VPS_EMETTEUR_NOM_KEY);
  }

  public String vpsEtat() {
    return (String) storedValueForKey(VPS_ETAT_KEY);
  }

  public void setVpsEtat(String value) {
    takeStoredValueForKey(value, VPS_ETAT_KEY);
  }

  public String vpsTgBic() {
    return (String) storedValueForKey(VPS_TG_BIC_KEY);
  }

  public void setVpsTgBic(String value) {
    takeStoredValueForKey(value, VPS_TG_BIC_KEY);
  }

  public String vpsTgCodique() {
    return (String) storedValueForKey(VPS_TG_CODIQUE_KEY);
  }

  public void setVpsTgCodique(String value) {
    takeStoredValueForKey(value, VPS_TG_CODIQUE_KEY);
  }

  public String vpsTgIban() {
    return (String) storedValueForKey(VPS_TG_IBAN_KEY);
  }

  public void setVpsTgIban(String value) {
    takeStoredValueForKey(value, VPS_TG_IBAN_KEY);
  }

  public String vpsTgNom() {
    return (String) storedValueForKey(VPS_TG_NOM_KEY);
  }

  public void setVpsTgNom(String value) {
    takeStoredValueForKey(value, VPS_TG_NOM_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOTypeVirement toTypeVirement() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOTypeVirement)storedValueForKey(TO_TYPE_VIREMENT_KEY);
  }

  public void setToTypeVirementRelationship(org.cocktail.fwkcktlcompta.client.metier.EOTypeVirement value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOTypeVirement oldValue = toTypeVirement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_VIREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_VIREMENT_KEY);
    }
  }
  

  public static EOVirementParamSepa createFwkCktlCompta_VirementParamSepa(EOEditingContext editingContext, Integer tviOrdre
, String vpsDftDevise
, String vpsDftIban
, String vpsDftTitulaire
, String vpsDftTransfertId
, String vpsEmetteurNom
, String vpsEtat
, String vpsTgBic
, String vpsTgCodique
, String vpsTgIban
, String vpsTgNom
, org.cocktail.fwkcktlcompta.client.metier.EOTypeVirement toTypeVirement) {
    EOVirementParamSepa eo = (EOVirementParamSepa) createAndInsertInstance(editingContext, _EOVirementParamSepa.ENTITY_NAME);    
		eo.setTviOrdre(tviOrdre);
		eo.setVpsDftDevise(vpsDftDevise);
		eo.setVpsDftIban(vpsDftIban);
		eo.setVpsDftTitulaire(vpsDftTitulaire);
		eo.setVpsDftTransfertId(vpsDftTransfertId);
		eo.setVpsEmetteurNom(vpsEmetteurNom);
		eo.setVpsEtat(vpsEtat);
		eo.setVpsTgBic(vpsTgBic);
		eo.setVpsTgCodique(vpsTgCodique);
		eo.setVpsTgIban(vpsTgIban);
		eo.setVpsTgNom(vpsTgNom);
    eo.setToTypeVirementRelationship(toTypeVirement);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOVirementParamSepa.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOVirementParamSepa.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOVirementParamSepa creerInstance(EOEditingContext editingContext) {
		  		EOVirementParamSepa object = (EOVirementParamSepa)createAndInsertInstance(editingContext, _EOVirementParamSepa.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOVirementParamSepa localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVirementParamSepa)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOVirementParamSepa localInstanceIn(EOEditingContext editingContext, EOVirementParamSepa eo) {
    EOVirementParamSepa localInstance = (eo == null) ? null : (EOVirementParamSepa)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOVirementParamSepa#localInstanceIn a la place.
   */
	public static EOVirementParamSepa localInstanceOf(EOEditingContext editingContext, EOVirementParamSepa eo) {
		return EOVirementParamSepa.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOVirementParamSepa fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOVirementParamSepa fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVirementParamSepa eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVirementParamSepa)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVirementParamSepa fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVirementParamSepa fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVirementParamSepa eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVirementParamSepa)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOVirementParamSepa fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVirementParamSepa eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVirementParamSepa ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVirementParamSepa fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
