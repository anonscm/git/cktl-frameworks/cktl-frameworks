// _EOSepaSddOrigineType.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSepaSddOrigineType.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOSepaSddOrigineType extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddOrigineType";
	public static final String ENTITY_TABLE_NAME = "maracuja.SEPA_SDD_ORIGINE_TYPE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idSepaSddOrigineType";

	public static final String GENERATION_ECRITURES_KEY = "generationEcritures";
	public static final String TYPE_CLASS_KEY = "typeClass";
	public static final String TYPE_CODE_KEY = "typeCode";
	public static final String TYPE_COMMENTAIRE_KEY = "typeCommentaire";
	public static final String TYPE_NOM_KEY = "typeNom";
	public static final String TYPE_ORDRE_KEY = "typeOrdre";
	public static final String TYPE_TABLE_KEY = "typeTable";
	public static final String TYPE_TABLE_CLE_KEY = "typeTableCle";
	public static final String VALIDE_KEY = "valide";

// Attributs non visibles
	public static final String ID_SEPA_SDD_ORIGINE_TYPE_KEY = "idSepaSddOrigineType";

//Colonnes dans la base de donnees
	public static final String GENERATION_ECRITURES_COLKEY = "GEN_ECRITURES";
	public static final String TYPE_CLASS_COLKEY = "TYPE_CLASS";
	public static final String TYPE_CODE_COLKEY = "TYPE_CODE";
	public static final String TYPE_COMMENTAIRE_COLKEY = "TYPE_COMMENTAIRE";
	public static final String TYPE_NOM_COLKEY = "TYPE_NOM";
	public static final String TYPE_ORDRE_COLKEY = "TYPE_ordre";
	public static final String TYPE_TABLE_COLKEY = "TYPE_TABLE";
	public static final String TYPE_TABLE_CLE_COLKEY = "TYPE_TABLE_CLE";
	public static final String VALIDE_COLKEY = "VALIDE";

	public static final String ID_SEPA_SDD_ORIGINE_TYPE_COLKEY = "ID_SEPA_SDD_ORIGINE_TYPE";


	// Relationships



	// Accessors methods
  public String generationEcritures() {
    return (String) storedValueForKey(GENERATION_ECRITURES_KEY);
  }

  public void setGenerationEcritures(String value) {
    takeStoredValueForKey(value, GENERATION_ECRITURES_KEY);
  }

  public String typeClass() {
    return (String) storedValueForKey(TYPE_CLASS_KEY);
  }

  public void setTypeClass(String value) {
    takeStoredValueForKey(value, TYPE_CLASS_KEY);
  }

  public String typeCode() {
    return (String) storedValueForKey(TYPE_CODE_KEY);
  }

  public void setTypeCode(String value) {
    takeStoredValueForKey(value, TYPE_CODE_KEY);
  }

  public String typeCommentaire() {
    return (String) storedValueForKey(TYPE_COMMENTAIRE_KEY);
  }

  public void setTypeCommentaire(String value) {
    takeStoredValueForKey(value, TYPE_COMMENTAIRE_KEY);
  }

  public String typeNom() {
    return (String) storedValueForKey(TYPE_NOM_KEY);
  }

  public void setTypeNom(String value) {
    takeStoredValueForKey(value, TYPE_NOM_KEY);
  }

  public Integer typeOrdre() {
    return (Integer) storedValueForKey(TYPE_ORDRE_KEY);
  }

  public void setTypeOrdre(Integer value) {
    takeStoredValueForKey(value, TYPE_ORDRE_KEY);
  }

  public String typeTable() {
    return (String) storedValueForKey(TYPE_TABLE_KEY);
  }

  public void setTypeTable(String value) {
    takeStoredValueForKey(value, TYPE_TABLE_KEY);
  }

  public String typeTableCle() {
    return (String) storedValueForKey(TYPE_TABLE_CLE_KEY);
  }

  public void setTypeTableCle(String value) {
    takeStoredValueForKey(value, TYPE_TABLE_CLE_KEY);
  }

  public String valide() {
    return (String) storedValueForKey(VALIDE_KEY);
  }

  public void setValide(String value) {
    takeStoredValueForKey(value, VALIDE_KEY);
  }


  public static EOSepaSddOrigineType createFwkCktlCompta_SepaSddOrigineType(EOEditingContext editingContext, String generationEcritures
, String typeClass
, String typeCode
, String typeCommentaire
, String typeNom
, Integer typeOrdre
, String typeTable
, String typeTableCle
, String valide
) {
    EOSepaSddOrigineType eo = (EOSepaSddOrigineType) createAndInsertInstance(editingContext, _EOSepaSddOrigineType.ENTITY_NAME);    
		eo.setGenerationEcritures(generationEcritures);
		eo.setTypeClass(typeClass);
		eo.setTypeCode(typeCode);
		eo.setTypeCommentaire(typeCommentaire);
		eo.setTypeNom(typeNom);
		eo.setTypeOrdre(typeOrdre);
		eo.setTypeTable(typeTable);
		eo.setTypeTableCle(typeTableCle);
		eo.setValide(valide);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOSepaSddOrigineType.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOSepaSddOrigineType.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOSepaSddOrigineType creerInstance(EOEditingContext editingContext) {
		  		EOSepaSddOrigineType object = (EOSepaSddOrigineType)createAndInsertInstance(editingContext, _EOSepaSddOrigineType.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOSepaSddOrigineType localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSepaSddOrigineType)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOSepaSddOrigineType localInstanceIn(EOEditingContext editingContext, EOSepaSddOrigineType eo) {
    EOSepaSddOrigineType localInstance = (eo == null) ? null : (EOSepaSddOrigineType)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOSepaSddOrigineType#localInstanceIn a la place.
   */
	public static EOSepaSddOrigineType localInstanceOf(EOEditingContext editingContext, EOSepaSddOrigineType eo) {
		return EOSepaSddOrigineType.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOSepaSddOrigineType fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOSepaSddOrigineType fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSepaSddOrigineType eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSepaSddOrigineType)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSepaSddOrigineType fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSepaSddOrigineType fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSepaSddOrigineType eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSepaSddOrigineType)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOSepaSddOrigineType fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSepaSddOrigineType eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSepaSddOrigineType ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSepaSddOrigineType fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
