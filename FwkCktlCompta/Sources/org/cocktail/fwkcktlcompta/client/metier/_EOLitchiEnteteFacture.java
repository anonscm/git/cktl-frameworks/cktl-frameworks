// _EOLitchiEnteteFacture.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOLitchiEnteteFacture.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOLitchiEnteteFacture extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Litchi_EnteteFacture";
	public static final String ENTITY_TABLE_NAME = "LITCHI.ENTETE_FACTURE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idFacture";

	public static final String ADRESSE1_KEY = "adresse1";
	public static final String ADRESSE2_KEY = "adresse2";
	public static final String CIVILITE_KEY = "civilite";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_ECHEANCE_KEY = "dateEcheance";
	public static final String DATE_MODIFICATION_NOTE_KEY = "dateModificationNote";
	public static final String DATE_PRELEVEMENT_KEY = "datePrelevement";
	public static final String DATE_REMBOURSEMENT_KEY = "dateRemboursement";
	public static final String DATE_VALIDATION_KEY = "dateValidation";
	public static final String ID_FACTURE_KEY = "idFacture";
	public static final String ID_FACTURE_ORIGINE_KEY = "idFactureOrigine";
	public static final String ID_TYPE_FACTURE_KEY = "idTypeFacture";
	public static final String INDICATION_ABANDON_KEY = "indicationAbandon";
	public static final String INDICATION_PRELEVEMENT_KEY = "indicationPrelevement";
	public static final String LIB_FACTURE_KEY = "libFacture";
	public static final String MNT_A_PAYER_HT_KEY = "mntAPayerHt";
	public static final String MNT_A_PAYER_TTC_KEY = "mntAPayerTtc";
	public static final String MNT_A_PAYER_TVA_KEY = "mntAPayerTva";
	public static final String MNT_FACTURE_HT_KEY = "mntFactureHt";
	public static final String MNT_FACTURE_TTC_KEY = "mntFactureTtc";
	public static final String MNT_FACTURE_TVA_KEY = "mntFactureTva";
	public static final String NOM_PERSONNE_KEY = "nomPersonne";
	public static final String NOTE_KEY = "note";
	public static final String NOTE_PUBLIC_KEY = "notePublic";
	public static final String NUM_COMMANDE_KEY = "numCommande";
	public static final String NUM_FACTURE_KEY = "numFacture";
	public static final String PAYS_KEY = "pays";
	public static final String PERSID_CREATEUR_KEY = "persidCreateur";
	public static final String PERSID_MODIFICATION_NOTE_KEY = "persidModificationNote";
	public static final String PERSID_PAYEUR_KEY = "persidPayeur";
	public static final String PERSID_VALIDATEUR_KEY = "persidValidateur";
	public static final String PRENOM_PERSONNE_KEY = "prenomPersonne";
	public static final String REGLEMENT_COUNT_KEY = "reglementCount";
	public static final String SOMME_PAYE_KEY = "sommePaye";
	public static final String VILLE_KEY = "ville";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String ADRESSE1_COLKEY = "ADRESSE_1";
	public static final String ADRESSE2_COLKEY = "ADRESSE_2";
	public static final String CIVILITE_COLKEY = "CIVILITE";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String DATE_CREATION_COLKEY = "DATE_CREATION";
	public static final String DATE_ECHEANCE_COLKEY = "DATE_ECHEANCE";
	public static final String DATE_MODIFICATION_NOTE_COLKEY = "DATE_MODIFICATION_NOTE";
	public static final String DATE_PRELEVEMENT_COLKEY = "DATE_PRELEVEMENT";
	public static final String DATE_REMBOURSEMENT_COLKEY = "DATE_REMBOURSEMENT";
	public static final String DATE_VALIDATION_COLKEY = "DATE_VALIDATION";
	public static final String ID_FACTURE_COLKEY = "ID_FACTURE";
	public static final String ID_FACTURE_ORIGINE_COLKEY = "ID_FACTURE_ORIGINE";
	public static final String ID_TYPE_FACTURE_COLKEY = "ID_TYPE_FACTURE";
	public static final String INDICATION_ABANDON_COLKEY = "INDICATION_ABANDON";
	public static final String INDICATION_PRELEVEMENT_COLKEY = "INDICATION_PRELEVEMENT";
	public static final String LIB_FACTURE_COLKEY = "LIB_FACTURE";
	public static final String MNT_A_PAYER_HT_COLKEY = "MNT_A_PAYER_HT";
	public static final String MNT_A_PAYER_TTC_COLKEY = "MNT_A_PAYER_TTC";
	public static final String MNT_A_PAYER_TVA_COLKEY = "MNT_A_PAYER_TVA";
	public static final String MNT_FACTURE_HT_COLKEY = "MNT_FACTURE_HT";
	public static final String MNT_FACTURE_TTC_COLKEY = "MNT_FACTURE_TTC";
	public static final String MNT_FACTURE_TVA_COLKEY = "MNT_FACTURE_TVA";
	public static final String NOM_PERSONNE_COLKEY = "NOM_PERSONNE";
	public static final String NOTE_COLKEY = "NOTE";
	public static final String NOTE_PUBLIC_COLKEY = "NOTE_PUBLIC";
	public static final String NUM_COMMANDE_COLKEY = "NUM_COMMANDE";
	public static final String NUM_FACTURE_COLKEY = "NUM_FACTURE";
	public static final String PAYS_COLKEY = "PAYS";
	public static final String PERSID_CREATEUR_COLKEY = "PERSID_CREATEUR";
	public static final String PERSID_MODIFICATION_NOTE_COLKEY = "PERSID_MODIFICATION_NOTE";
	public static final String PERSID_PAYEUR_COLKEY = "PERSID_PAYEUR";
	public static final String PERSID_VALIDATEUR_COLKEY = "PERSID_VALIDATEUR";
	public static final String PRENOM_PERSONNE_COLKEY = "PRENOM_PERSONNE";
	public static final String REGLEMENT_COUNT_COLKEY = "reglementCount";
	public static final String SOMME_PAYE_COLKEY = "sommePaye";
	public static final String VILLE_COLKEY = "VILLE";



	// Relationships
	public static final String TO_PERSONNE_KEY = "toPersonne";
	public static final String TO_TYPE_FACTURE_KEY = "toTypeFacture";



	// Accessors methods
  public String adresse1() {
    return (String) storedValueForKey(ADRESSE1_KEY);
  }

  public void setAdresse1(String value) {
    takeStoredValueForKey(value, ADRESSE1_KEY);
  }

  public String adresse2() {
    return (String) storedValueForKey(ADRESSE2_KEY);
  }

  public void setAdresse2(String value) {
    takeStoredValueForKey(value, ADRESSE2_KEY);
  }

  public String civilite() {
    return (String) storedValueForKey(CIVILITE_KEY);
  }

  public void setCivilite(String value) {
    takeStoredValueForKey(value, CIVILITE_KEY);
  }

  public String codePostal() {
    return (String) storedValueForKey(CODE_POSTAL_KEY);
  }

  public void setCodePostal(String value) {
    takeStoredValueForKey(value, CODE_POSTAL_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateEcheance() {
    return (NSTimestamp) storedValueForKey(DATE_ECHEANCE_KEY);
  }

  public void setDateEcheance(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_ECHEANCE_KEY);
  }

  public NSTimestamp dateModificationNote() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_NOTE_KEY);
  }

  public void setDateModificationNote(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_NOTE_KEY);
  }

  public NSTimestamp datePrelevement() {
    return (NSTimestamp) storedValueForKey(DATE_PRELEVEMENT_KEY);
  }

  public void setDatePrelevement(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_PRELEVEMENT_KEY);
  }

  public NSTimestamp dateRemboursement() {
    return (NSTimestamp) storedValueForKey(DATE_REMBOURSEMENT_KEY);
  }

  public void setDateRemboursement(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_REMBOURSEMENT_KEY);
  }

  public NSTimestamp dateValidation() {
    return (NSTimestamp) storedValueForKey(DATE_VALIDATION_KEY);
  }

  public void setDateValidation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_VALIDATION_KEY);
  }

  public Long idFacture() {
    return (Long) storedValueForKey(ID_FACTURE_KEY);
  }

  public void setIdFacture(Long value) {
    takeStoredValueForKey(value, ID_FACTURE_KEY);
  }

  public Long idFactureOrigine() {
    return (Long) storedValueForKey(ID_FACTURE_ORIGINE_KEY);
  }

  public void setIdFactureOrigine(Long value) {
    takeStoredValueForKey(value, ID_FACTURE_ORIGINE_KEY);
  }

  public Long idTypeFacture() {
    return (Long) storedValueForKey(ID_TYPE_FACTURE_KEY);
  }

  public void setIdTypeFacture(Long value) {
    takeStoredValueForKey(value, ID_TYPE_FACTURE_KEY);
  }

  public Boolean indicationAbandon() {
    return (Boolean) storedValueForKey(INDICATION_ABANDON_KEY);
  }

  public void setIndicationAbandon(Boolean value) {
    takeStoredValueForKey(value, INDICATION_ABANDON_KEY);
  }

  public Boolean indicationPrelevement() {
    return (Boolean) storedValueForKey(INDICATION_PRELEVEMENT_KEY);
  }

  public void setIndicationPrelevement(Boolean value) {
    takeStoredValueForKey(value, INDICATION_PRELEVEMENT_KEY);
  }

  public String libFacture() {
    return (String) storedValueForKey(LIB_FACTURE_KEY);
  }

  public void setLibFacture(String value) {
    takeStoredValueForKey(value, LIB_FACTURE_KEY);
  }

  public java.math.BigDecimal mntAPayerHt() {
    return (java.math.BigDecimal) storedValueForKey(MNT_A_PAYER_HT_KEY);
  }

  public void setMntAPayerHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_A_PAYER_HT_KEY);
  }

  public java.math.BigDecimal mntAPayerTtc() {
    return (java.math.BigDecimal) storedValueForKey(MNT_A_PAYER_TTC_KEY);
  }

  public void setMntAPayerTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_A_PAYER_TTC_KEY);
  }

  public java.math.BigDecimal mntAPayerTva() {
    return (java.math.BigDecimal) storedValueForKey(MNT_A_PAYER_TVA_KEY);
  }

  public void setMntAPayerTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_A_PAYER_TVA_KEY);
  }

  public java.math.BigDecimal mntFactureHt() {
    return (java.math.BigDecimal) storedValueForKey(MNT_FACTURE_HT_KEY);
  }

  public void setMntFactureHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_FACTURE_HT_KEY);
  }

  public java.math.BigDecimal mntFactureTtc() {
    return (java.math.BigDecimal) storedValueForKey(MNT_FACTURE_TTC_KEY);
  }

  public void setMntFactureTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_FACTURE_TTC_KEY);
  }

  public java.math.BigDecimal mntFactureTva() {
    return (java.math.BigDecimal) storedValueForKey(MNT_FACTURE_TVA_KEY);
  }

  public void setMntFactureTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MNT_FACTURE_TVA_KEY);
  }

  public String nomPersonne() {
    return (String) storedValueForKey(NOM_PERSONNE_KEY);
  }

  public void setNomPersonne(String value) {
    takeStoredValueForKey(value, NOM_PERSONNE_KEY);
  }

  public String note() {
    return (String) storedValueForKey(NOTE_KEY);
  }

  public void setNote(String value) {
    takeStoredValueForKey(value, NOTE_KEY);
  }

  public String notePublic() {
    return (String) storedValueForKey(NOTE_PUBLIC_KEY);
  }

  public void setNotePublic(String value) {
    takeStoredValueForKey(value, NOTE_PUBLIC_KEY);
  }

  public String numCommande() {
    return (String) storedValueForKey(NUM_COMMANDE_KEY);
  }

  public void setNumCommande(String value) {
    takeStoredValueForKey(value, NUM_COMMANDE_KEY);
  }

  public String numFacture() {
    return (String) storedValueForKey(NUM_FACTURE_KEY);
  }

  public void setNumFacture(String value) {
    takeStoredValueForKey(value, NUM_FACTURE_KEY);
  }

  public String pays() {
    return (String) storedValueForKey(PAYS_KEY);
  }

  public void setPays(String value) {
    takeStoredValueForKey(value, PAYS_KEY);
  }

  public Integer persidCreateur() {
    return (Integer) storedValueForKey(PERSID_CREATEUR_KEY);
  }

  public void setPersidCreateur(Integer value) {
    takeStoredValueForKey(value, PERSID_CREATEUR_KEY);
  }

  public Integer persidModificationNote() {
    return (Integer) storedValueForKey(PERSID_MODIFICATION_NOTE_KEY);
  }

  public void setPersidModificationNote(Integer value) {
    takeStoredValueForKey(value, PERSID_MODIFICATION_NOTE_KEY);
  }

  public Integer persidPayeur() {
    return (Integer) storedValueForKey(PERSID_PAYEUR_KEY);
  }

  public void setPersidPayeur(Integer value) {
    takeStoredValueForKey(value, PERSID_PAYEUR_KEY);
  }

  public Integer persidValidateur() {
    return (Integer) storedValueForKey(PERSID_VALIDATEUR_KEY);
  }

  public void setPersidValidateur(Integer value) {
    takeStoredValueForKey(value, PERSID_VALIDATEUR_KEY);
  }

  public String prenomPersonne() {
    return (String) storedValueForKey(PRENOM_PERSONNE_KEY);
  }

  public void setPrenomPersonne(String value) {
    takeStoredValueForKey(value, PRENOM_PERSONNE_KEY);
  }

  public Integer reglementCount() {
    return (Integer) storedValueForKey(REGLEMENT_COUNT_KEY);
  }

  public void setReglementCount(Integer value) {
    takeStoredValueForKey(value, REGLEMENT_COUNT_KEY);
  }

  public java.math.BigDecimal sommePaye() {
    return (java.math.BigDecimal) storedValueForKey(SOMME_PAYE_KEY);
  }

  public void setSommePaye(java.math.BigDecimal value) {
    takeStoredValueForKey(value, SOMME_PAYE_KEY);
  }

  public String ville() {
    return (String) storedValueForKey(VILLE_KEY);
  }

  public void setVille(String value) {
    takeStoredValueForKey(value, VILLE_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne toPersonne() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne)storedValueForKey(TO_PERSONNE_KEY);
  }

  public void setToPersonneRelationship(org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne oldValue = toPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOLitchiTypeFacture toTypeFacture() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOLitchiTypeFacture)storedValueForKey(TO_TYPE_FACTURE_KEY);
  }

  public void setToTypeFactureRelationship(org.cocktail.fwkcktlcompta.client.metier.EOLitchiTypeFacture value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOLitchiTypeFacture oldValue = toTypeFacture();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_FACTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_FACTURE_KEY);
    }
  }
  

  public static EOLitchiEnteteFacture createFwkCktlCompta_Litchi_EnteteFacture(EOEditingContext editingContext, Long idFacture
) {
    EOLitchiEnteteFacture eo = (EOLitchiEnteteFacture) createAndInsertInstance(editingContext, _EOLitchiEnteteFacture.ENTITY_NAME);    
		eo.setIdFacture(idFacture);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOLitchiEnteteFacture.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOLitchiEnteteFacture.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOLitchiEnteteFacture creerInstance(EOEditingContext editingContext) {
		  		EOLitchiEnteteFacture object = (EOLitchiEnteteFacture)createAndInsertInstance(editingContext, _EOLitchiEnteteFacture.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOLitchiEnteteFacture localInstanceIn(EOEditingContext editingContext) {
	  		return (EOLitchiEnteteFacture)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOLitchiEnteteFacture localInstanceIn(EOEditingContext editingContext, EOLitchiEnteteFacture eo) {
    EOLitchiEnteteFacture localInstance = (eo == null) ? null : (EOLitchiEnteteFacture)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOLitchiEnteteFacture#localInstanceIn a la place.
   */
	public static EOLitchiEnteteFacture localInstanceOf(EOEditingContext editingContext, EOLitchiEnteteFacture eo) {
		return EOLitchiEnteteFacture.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOLitchiEnteteFacture fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOLitchiEnteteFacture fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOLitchiEnteteFacture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOLitchiEnteteFacture)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOLitchiEnteteFacture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOLitchiEnteteFacture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOLitchiEnteteFacture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOLitchiEnteteFacture)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOLitchiEnteteFacture fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOLitchiEnteteFacture eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOLitchiEnteteFacture ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOLitchiEnteteFacture fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
