// _EOOrigine.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOrigine.java instead.
package org.cocktail.fwkcktlcompta.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOOrigine extends  _AFwkCktlComptaClientRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Origine";
	public static final String ENTITY_TABLE_NAME = "maracuja.Origine";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "oriOrdre";

	public static final String ORI_ENTITE_KEY = "oriEntite";
	public static final String ORIKEY_ENTITE_KEY = "orikeyEntite";
	public static final String ORI_KEY_NAME_KEY = "oriKeyName";
	public static final String ORI_LIBELLE_KEY = "oriLibelle";

// Attributs non visibles
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String TOP_ORDRE_KEY = "topOrdre";

//Colonnes dans la base de donnees
	public static final String ORI_ENTITE_COLKEY = "ori_Entite";
	public static final String ORIKEY_ENTITE_COLKEY = "ori_key_Entite";
	public static final String ORI_KEY_NAME_COLKEY = "ori_Key_Name";
	public static final String ORI_LIBELLE_COLKEY = "ori_Libelle";

	public static final String ORI_ORDRE_COLKEY = "ori_Ordre";
	public static final String TOP_ORDRE_COLKEY = "top_ORDRE";


	// Relationships
	public static final String TO_ORGAN_KEY = "toOrgan";
	public static final String TO_TYPE_OPERATION_KEY = "toTypeOperation";



	// Accessors methods
  public String oriEntite() {
    return (String) storedValueForKey(ORI_ENTITE_KEY);
  }

  public void setOriEntite(String value) {
    takeStoredValueForKey(value, ORI_ENTITE_KEY);
  }

  public Integer orikeyEntite() {
    return (Integer) storedValueForKey(ORIKEY_ENTITE_KEY);
  }

  public void setOrikeyEntite(Integer value) {
    takeStoredValueForKey(value, ORIKEY_ENTITE_KEY);
  }

  public String oriKeyName() {
    return (String) storedValueForKey(ORI_KEY_NAME_KEY);
  }

  public void setOriKeyName(String value) {
    takeStoredValueForKey(value, ORI_KEY_NAME_KEY);
  }

  public String oriLibelle() {
    return (String) storedValueForKey(ORI_LIBELLE_KEY);
  }

  public void setOriLibelle(String value) {
    takeStoredValueForKey(value, ORI_LIBELLE_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan toOrgan() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(TO_ORGAN_KEY);
  }

  public void setToOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = toOrgan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.client.metier.EOTypeOperation toTypeOperation() {
    return (org.cocktail.fwkcktlcompta.client.metier.EOTypeOperation)storedValueForKey(TO_TYPE_OPERATION_KEY);
  }

  public void setToTypeOperationRelationship(org.cocktail.fwkcktlcompta.client.metier.EOTypeOperation value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.client.metier.EOTypeOperation oldValue = toTypeOperation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_OPERATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_OPERATION_KEY);
    }
  }
  

  public static EOOrigine createFwkCktlCompta_Origine(EOEditingContext editingContext) {
    EOOrigine eo = (EOOrigine) createAndInsertInstance(editingContext, _EOOrigine.ENTITY_NAME);    
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOOrigine.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOOrigine.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOOrigine creerInstance(EOEditingContext editingContext) {
		  		EOOrigine object = (EOOrigine)createAndInsertInstance(editingContext, _EOOrigine.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOOrigine localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOrigine)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOOrigine localInstanceIn(EOEditingContext editingContext, EOOrigine eo) {
    EOOrigine localInstance = (eo == null) ? null : (EOOrigine)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOOrigine#localInstanceIn a la place.
   */
	public static EOOrigine localInstanceOf(EOEditingContext editingContext, EOOrigine eo) {
		return EOOrigine.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOOrigine fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOOrigine fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOrigine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOrigine)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOrigine fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOrigine fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOrigine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOrigine)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOOrigine fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOrigine eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOrigine ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOrigine fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
