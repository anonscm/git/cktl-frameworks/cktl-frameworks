package org.cocktail.fwkcktlcompta.client.util;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSPropertyListSerialization;

public class ClientCktlEOControlUtilities {

	/** logging support */
	public static final Logger log = Logger.getLogger(ClientCktlEOControlUtilities.class);



	/**
	 * Returns the propertylist-encoded string representation of the primary key for a given object.
	 * 
	 * @param eo object to get the primary key for.
	 * @return string representation of the primary key of the object.
	 */
	public static String primaryKeyStringForObject(EOEnterpriseObject eo) {
		return _stringForPrimaryKey(primaryKeyObjectForObject(eo));
	}

	/**
	 * Returns the propertylist-encoded string representation of the global ID.
	 * 
	 * @param gid the global id of the oject to get the primary key for.
	 * @return string representation of the primary key of the object.
	 */
	public static String primaryKeyStringForGlobalID(EOKeyGlobalID gid) {
		if (gid.keyValuesArray().count() > 1) {
			return _stringForPrimaryKey(gid.keyValuesArray());
		}
		return _stringForPrimaryKey(gid.keyValuesArray().lastObject());
	}

	/**
	 * Returns the propertylist-encoded string representation of the primary key for a given object. Made public only for ERXGenericRecord.
	 * 
	 * @param pk the primary key
	 * @return string representation of the primary key.
	 */
	// FIXME ak this PK creation is too byzantine
	public static String _stringForPrimaryKey(Object pk) {
		if (pk == null)
			return null;
		if (pk instanceof String || pk instanceof Number) {
			return pk.toString();
		}
		return NSPropertyListSerialization.stringFromPropertyList(pk);
	}

	/**
	 * Returns either the single object the PK consist of or the NSArray of its values if the key is compound.
	 * 
	 * @param eo object to get the primary key for.
	 * @return single object or NSArray
	 */
	public static Object primaryKeyObjectForObject(EOEnterpriseObject eo) {
		NSArray arr = primaryKeyArrayForObject(eo);
		if (arr != null && arr.count() == 1)
			return arr.lastObject();
		return arr;
	}

	/**
	 * Gives the primary key array for a given enterprise object. This has the advantage of not firing the fault of the object, unlike the method in
	 * {@link com.webobjects.eoaccess.EOUtilities EOUtilities}.
	 * 
	 * @param obj enterprise object to get the primary key array from.
	 * @return array of all the primary key values for the object.
	 */
	public static NSArray primaryKeyArrayForObject(EOEnterpriseObject obj) {
		EOEditingContext ec = obj.editingContext();
		if (ec == null) {
			//you don't have an EC! Bad EO. We can do nothing.
			return null;
		}
		EOGlobalID gid = ec.globalIDForObject(obj);
		if (gid.isTemporary()) {
			//no pk yet assigned
			return null;
		}
		EOKeyGlobalID kGid = (EOKeyGlobalID) gid;
		return kGid.keyValuesArray();
	}


}
