package org.cocktail.fwkcktlcompta.client.remotecalls;

import org.cocktail.fwkcktlcompta.client.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;

public class ServerCallCompta extends ServerCall {

	public static final String METHOD_A_DES_ECHEANCIERS = "clientSideRequestHasEcheanciers";
	public static final String METHOD_GET_CONFIG = "clientSideRequestGetConfig";

	protected static String getRemoteKeyPath() {
		return SESSION_KEY + "." + "remoteDelegateCompta";
	}

	/**
	 * @return true si la facture papier a au moins un mandat sepa rattache ; false sinon.
	 */
	public static Boolean clientSideRequestHasEcheanciers(EOEditingContext ec, String origineType, Integer origineEntityId) {
		return (Boolean) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec,
				getRemoteKeyPath(),
				METHOD_A_DES_ECHEANCIERS,
				new Class[] {String.class, Integer.class},
				new Object[] {origineType, origineEntityId},
				false);

	}

	public static Object clientSideRequestGetConfig(EOEditingContext ec, String param) {
		return (Object) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec,
				getRemoteKeyPath(),
				METHOD_GET_CONFIG,
				new Class[] {
					String.class
				},
				new Object[] {
					param
				},
				false);
	}

	public static Integer clientSideRequestNextNumeroRecouvrement(EOEditingContext ec, EOComptabilite comptabilite, EOJefyAdminExercice exercice) {
		return (Integer) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), "clientSideRequestNextNumeroRecouvrement", new Class[] {
				EOEnterpriseObject.class, EOEnterpriseObject.class
		}, new Object[] {
				comptabilite, exercice
		}, false);
	}

	public static void clientSideRequestInvalidateObjectsOnServer(EOEditingContext ec, NSArray gids) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), "clientSideRequestInvalidateObjectsOnServer", new Class[] {
				NSArray.class,
		}, new Object[] {
				gids
		}, false);
	}
}
