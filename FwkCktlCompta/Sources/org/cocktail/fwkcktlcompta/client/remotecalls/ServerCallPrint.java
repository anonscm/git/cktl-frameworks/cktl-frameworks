package org.cocktail.fwkcktlcompta.client.remotecalls;

import java.io.File;
import java.io.FileOutputStream;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

public class ServerCallPrint extends ServerCall {

	/**
	 * Un champ de ce nom doit etre présent dans la classe Session de l'application qui offre l'accès à la classe CktlComptaRemoteDelegatePrint.
	 */
	public static final String REMOTE_REPORT = "remoteDelegatePrint";


	public static final String PDF_MANDAT_PRELEVEMENT_SEPA = "MandatDePrelevementSepa.pdf";
	public static final String PDF_ECHEANCIER_PRELEVEMENT_SEPA = "NotificationPrealableEcheancesSepa.pdf";

	public static final String JASPERFILENAME_MANDAT_PRELEVEMENT_SEPA = "sepa_mandat_prelevement.jasper";
	public static final String JASPERFILENAME_ECHEANCIER_PRELEVEMENT_SEPA = "sepa_notification_prealable_echeances.jasper";

	private static final String CLIENT_SIDE_REQUEST_PRINT_AND_WAIT = "clientSideRequestPrintAndWait";


	protected static String getRemoteKeyPath() {
		return SESSION_KEY + "." + REMOTE_REPORT;
	}

	public static String clientSideRequestPrintAndWait(EOEditingContext ec, String temporaryDir, String report, String pdfFileName, String sql, NSDictionary parametres) throws Exception {
		return imprimerReportAndWait(ec, temporaryDir, parametres, report, pdfFileName, sql);
	}

	/**
	 * Fait appel au serveur pour imprimer un report, et enregistre le flux PDF dans un fichier temporaire.
	 * La méthode renvoie le chemin d'accès au fichier.
	 */
	private static final String imprimerReportAndWait(EOEditingContext ec, String temporaryDir, NSDictionary parametres, String jasperFileName, String pdfFileName, String sqlQuery) throws Exception {
		try {
			String filePath = temporaryDir + "compta_" + pdfFileName;

			// Si le fichier existe, tenter de le supprimer.
			// Si pas possible, message d'erreur comme quoi il est ouvert
			File test = new File(filePath);
			if (test.exists()) {
				if (!test.delete()) {
					throw new Exception("Impossible de supprimer le fichier temporaire " + filePath + ". Il est vraissemblablement déjà ouvert, si c'est le cas, fermez-le avant de relancer l'impression.");
				}
			}

			NSData datas = (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec,
					getRemoteKeyPath(),
					CLIENT_SIDE_REQUEST_PRINT_AND_WAIT,
					new Class[] {String.class, String.class, NSDictionary.class},
					new Object[] {jasperFileName, sqlQuery, parametres},
					false);

			if (datas == null) {
				throw new Exception("Impossible de récupérer le flux PDF.");
			}

			try {
				FileOutputStream fileOutputStream = new FileOutputStream(filePath);
				datas.writeToStream(fileOutputStream);
				fileOutputStream.close();
			} catch (Exception exception) {
				throw new Exception("Impossible d'ecrire le fichier PDF sur le disque. Vérifiez qu'un autre devis n'est pas déjà ouvert.\n" + exception.getMessage());
			}

			// Vérifier que le fichier a bien ete cree
			try {
				File tmpFile = new File(filePath);
				if (!tmpFile.exists()) {
					throw new Exception("Le fichier " + filePath + " n'existe pas.");
				}
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}

			return filePath;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Impossible d'imprimer. " + e.getMessage());
		}
	}

}
