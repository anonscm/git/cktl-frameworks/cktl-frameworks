/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcompta.client.remotecalls;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class ServerCall {
	public static final String SESSION_KEY = "session";

	/**
	 * Returns the byte array for a given stream.
	 * 
	 * @param in stream to get the bytes from
	 * @throws IOException if things go wrong
	 * @return byte array of the stream.
	 */
	public static byte[] bytesFromInputStream(InputStream in) throws IOException {
		if (in == null)
			throw new IllegalArgumentException("null input stream");

		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		int read = -1;
		byte[] buf = new byte[1024 * 50];
		while ((read = in.read(buf)) != -1) {
			bout.write(buf, 0, read);
		}

		return bout.toByteArray();
	}

	/**
	 * Returns a string from the input stream using the specified encoding.
	 * 
	 * @param in stream to read
	 * @param encoding to be used, null will use the default
	 * @return string representation of the stream.
	 */
	public static String stringFromInputStream(InputStream in, String encoding) throws IOException {
		return new String(bytesFromInputStream(in), encoding);
	}

	/**
	 * Returns a string from the input stream using the default encoding.
	 * 
	 * @param in stream to read
	 * @return string representation of the stream.
	 */
	public static String stringFromInputStream(InputStream in) throws IOException {
		return new String(bytesFromInputStream(in));
	}

}
