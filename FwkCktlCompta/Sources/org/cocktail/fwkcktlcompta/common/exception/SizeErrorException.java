package org.cocktail.fwkcktlcompta.common.exception;

public class SizeErrorException extends Exception {

	public SizeErrorException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

}
