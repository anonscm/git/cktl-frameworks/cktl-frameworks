package org.cocktail.fwkcktlcompta.common;

public interface IFwkCktlComptaParam {
	public static final String PRELEVEMENTNATIONAL_ENABLED = "org.cocktail.gfc.prelevementnational.enabled";
	public static final String SEPASDDMANDAT_ENABLED = "org.cocktail.gfc.sepasddmandat.enabled";
	public static final String SEPASDDMANDAT_NBECHEANCES = "org.cocktail.gfc.sepasddmandat.nbecheances";
	public static final String SEPASDDMANDAT_UTILISATIONEXCLUSIVE = "org.cocktail.gfc.sepasddmandat.utilisationexclusive";
	public static final String SEPASDDMANDAT_DATEFINMISENPLACE = "org.cocktail.gfc.sepasddmandat.datefinmisenplace";
	public static final String SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_FIRST = "org.cocktail.gfc.sepasddmandat.typeoperation.nbjoursavantremise.first";
	public static final String SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_OOFF = "org.cocktail.gfc.sepasddmandat.typeoperation.nbjoursavantremise.ooff";
	public static final String SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_RCUR = "org.cocktail.gfc.sepasddmandat.typeoperation.nbjoursavantremise.rcur";
	public static final String SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_FNAL = "org.cocktail.gfc.sepasddmandat.typeoperation.nbjoursavantremise.fnal";
	public static final String SEPASDDMANDAT_NB_JOURS_MOIS_SUIVANT = "org.cocktail.gfc.sepasddmandat.dateecheance.nbjoursmoissuivant";
	public static final String SEPASDDMANDAT_NB_JOURS_MINIMUM_AVANT_DEUXIEME_ECHEANCE = "org.cocktail.gfc.sepasddmandat.dateecheance.nbjoursminimumavantdeuxiemeecheance";
	public static final String SEPASDDMANDAT_NUMERO_JOUR = "org.cocktail.gfc.sepasddmandat.dateecheance.numerojour";
	public static final String SEPASDDMANDAT_NB_ECHEANCES = "org.cocktail.gfc.sepasddmandat.nbecheances";
	public static final String SEPASDDMANDAT_NB_MOIS_AVANT_CADUCITE = "org.cocktail.gfc.sepasddmandat.nbmoisavantcaducite";
	public static final String SEPASDDMANDAT_SCOLARIX_MODERECOUVREMENTCODEDEFAUT = "org.cocktail.gfc.sepasddmandat.scolarixmoderecouvrementcodedefaut";
	public static final String SEPASDDMANDAT_CORIANDRE_MODERECOUVREMENTCODEDEFAUT = "org.cocktail.gfc.sepasddmandat.coriandremoderecouvrementcodedefaut";

}
