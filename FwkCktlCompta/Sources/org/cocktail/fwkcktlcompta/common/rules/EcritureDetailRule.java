package org.cocktail.fwkcktlcompta.common.rules;

import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.helpers.EcritureDetailHelper;
import org.cocktail.fwkcktlcompta.common.helpers.JefyAdminExerciceHelper;

import com.webobjects.foundation.NSValidation;

public class EcritureDetailRule {
	private static EcritureDetailRule sharedInstance = new EcritureDetailRule();

	public static EcritureDetailRule getSharedInstance() {
		return sharedInstance;
	}
	
	public void checkExerciceClos(IEcritureDetail ecritureDetail) {
		if (JefyAdminExerciceHelper.getSharedInstance().estClos(ecritureDetail.toExercice())) {
			throw new NSValidation.ValidationException("Impossible de créer ou modifier une écriture car l'exercice" + ecritureDetail.toExercice().exeExercice() + " est clôs.");
		}
	}

	public void checkDebitEtCreditNuls(IEcritureDetail ecritureDetail) {
		if ((ecritureDetail.ecdDebit() == null || ecritureDetail.ecdDebit().doubleValue() == 0d) && (ecritureDetail.ecdCredit() == null || ecritureDetail.ecdCredit().doubleValue() == 0d)) {
			throw new NSValidation.ValidationException("Le débit et le crédit du détail d'écriture ne peuvent être nuls tous les deux.");
		}
	}

	public void validate(IEcritureDetail ecritureDetail) {
		try {
		checkExerciceClos(ecritureDetail);
		checkPlanComptableExerValide(ecritureDetail);
		checkDebitEtCreditNuls(ecritureDetail);
		} catch (Exception e) {
			throw new NSValidation.ValidationException("Erreur lors de la validation du detail d'écriture " + EcritureDetailHelper.getSharedInstance().getLibelleComplet(ecritureDetail) + ": " + e.getMessage());
		}
	}

	public void checkPlanComptableExerValide(IEcritureDetail ecritureDetail) {
		PlanComptableExerRule.getSharedInstance().checkValide(ecritureDetail.toPlanComptableExer());
		
	}
	
	
	
	
}
