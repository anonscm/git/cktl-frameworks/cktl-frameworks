package org.cocktail.fwkcktlcompta.common.rules;

import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IEmargement;
import org.cocktail.fwkcktlcompta.common.entities.IGestion;
import org.cocktail.fwkcktlcompta.common.helpers.EmargementHelper;
import org.cocktail.fwkcktlcompta.common.helpers.GestionHelper;
import org.cocktail.fwkcktlcompta.common.helpers.JefyAdminExerciceHelper;
import org.cocktail.fwkcktlcompta.common.util.ZListUtil;

import com.webobjects.foundation.NSValidation;

public class EmargementRule {
	private static EmargementRule sharedInstance = new EmargementRule();

	public static EmargementRule getSharedInstance() {
		return sharedInstance;
	}
	
	public void checkExerciceClos(IEmargement emargement) {
		if (JefyAdminExerciceHelper.getSharedInstance().estClos(emargement.toExercice())) {
			throw new NSValidation.ValidationException("Impossible de créer ou modifier un émargement car l'exercice" + emargement.toExercice().exeExercice() + " est clôs.");
		}
	}

	public void checkNumero(IEmargement emargement) {
		if (emargement.emaNumero() == null) {
			throw new NSValidation.ValidationException("Le numero de l'emargement ne peut pas être vide.");
		}
	}

	public void checkPlusieursGestionDansDetails(IEmargement emargement) {
		List<IGestion> gestions = EmargementHelper.getSharedInstance().getGestionsDesEcrituresDetailsEmarges(emargement);
		if (gestions.size() > 1) {
			List<String> gesCodes = GestionHelper.getGescodeList(gestions);
			throw new NSValidation.ValidationException("Plusieurs codes gestions trouvés dans les écritures concernées par l'émargement (" + ZListUtil.toCommaSeparatedString(gesCodes) + "). Impossible de réaliser l'émargement.");
		}
	}

	public void validate(IEmargement emargement) {
		checkExerciceClos(emargement);
		checkNumero(emargement);
		checkPlusieursGestionDansDetails(emargement);
	}

}
