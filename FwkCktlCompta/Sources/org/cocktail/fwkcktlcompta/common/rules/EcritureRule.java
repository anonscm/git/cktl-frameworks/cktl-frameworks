package org.cocktail.fwkcktlcompta.common.rules;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IEcriture;
import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.helpers.EcritureHelper;
import org.cocktail.fwkcktlcompta.common.helpers.JefyAdminExerciceHelper;
import org.cocktail.fwkcktlcompta.common.util.CktlArrayUtilities;

import com.webobjects.foundation.NSValidation;

public class EcritureRule {
	private static EcritureRule sharedInstance = new EcritureRule();

	public static EcritureRule getSharedInstance() {
		return sharedInstance;
	}
	
	public Boolean isEquilibree(IEcriture ecriture, StringBuilder sb) {
		Boolean res = Boolean.TRUE;
		BigDecimal debits = EcritureHelper.getSharedInstance().getMontantDebits(ecriture);
		BigDecimal credits = EcritureHelper.getSharedInstance().getMontantCredits(ecriture);
		if (!debits.equals(credits)) {
			sb.append("Ecriture non équilibrée (Débits="+debits+" / Crédits="+credits+")");
			res = Boolean.FALSE;
		}
		return res;
	}
	

	public void checkEquilibree(IEcriture ecriture) {
		StringBuilder sb = new StringBuilder();
		if (!isEquilibree(ecriture, sb)) {
			throw new NSValidation.ValidationException(sb.toString());
		}
	}
	
	
	public void checkExerciceClos(IEcriture ecriture) {
		if ( JefyAdminExerciceHelper.getSharedInstance().estClos(ecriture.toExercice())) {
			throw new NSValidation.ValidationException("Impossible de créer ou modifier une écriture car l'exercice" + ecriture.toExercice().exeExercice() + " est clôs.");
		}
	}

	public void checkNumero(IEcriture ecriture) {
		if (ecriture.ecrNumero() == null) {
			throw new NSValidation.ValidationException("Le numero de l'ecriture ne peut pas être vide.");
		}
	}

	public void checkExerciceCoherentAvecDetails(IEcriture ecriture) {
		List<IEcritureDetail> ecds = CktlArrayUtilities.toList(ecriture.toEcritureDetails());
		for (IEcritureDetail iEcritureDetail : ecds) {
			if (!isExerciceCoherentAvecDetailExercice(ecriture, iEcritureDetail)) {
				throw new NSValidation.ValidationException("L'exercice de l'ecriture doit être le même que celui des détails.");
			}
			
			if (!isExerciceCoherentAvecDetailCompte(ecriture, iEcritureDetail)) {
			    throw new NSValidation.ValidationException("L'exercice de l'ecriture doit être le même que celui des comptes de détails.");
			}
		}
	}
	
	protected boolean isExerciceCoherentAvecDetailExercice(IEcriture ecriture, IEcritureDetail ecritureDetail) {
	    return ecritureDetail.toExercice().exeExercice().equals(ecriture.toExercice().exeExercice());
	}
	
	protected boolean isExerciceCoherentAvecDetailCompte(IEcriture ecriture, IEcritureDetail ecritureDetail) {
        return ecritureDetail.toPlanComptableExer().toExercice().exeExercice().equals(ecriture.toExercice().exeExercice());
    }    

	public void validate(IEcriture ecriture) {
		checkExerciceClos(ecriture);
		checkExerciceCoherentAvecDetails(ecriture);
		checkNumero(ecriture);
		checkEquilibree(ecriture);
	}
}
