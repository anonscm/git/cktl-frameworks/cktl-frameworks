package org.cocktail.fwkcktlcompta.common.rules;

import org.cocktail.fwkcktlcompta.common.entities.IPlanComptableExer;
import org.cocktail.fwkcktlcompta.common.helpers.PlanComptableExerHelper;

import com.webobjects.foundation.NSValidation;

public class PlanComptableExerRule {
	private static PlanComptableExerRule sharedInstance = new PlanComptableExerRule();

	public static PlanComptableExerRule getSharedInstance() {
		return sharedInstance;
	}
	

	public void validate(IPlanComptableExer planComptableExer) {
	}

	public void checkValide(IPlanComptableExer planComptableExer) {
		if (!PlanComptableExerHelper.getSharedInstance().isValide(planComptableExer)) {
			throw new NSValidation.ValidationException("Le compte " + planComptableExer.pcoNum() + " n'est pas valide sur l'exercice " + planComptableExer.toExercice().exeExercice() + ".");
		}
	}

}
