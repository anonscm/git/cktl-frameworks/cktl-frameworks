/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcompta.common.finders;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumFournis;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumRib;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderGrhumRib {

	public static final String ENTITY_NAME = "FwkCktlCompta_Grhum_Rib";

	/**
	 * Moche car duplication avec le client....
	 * @param editingContext
	 * @param debiteurPersonne
	 * @return
	 */
	public static NSArray fetchRibsValidesPourPersonne(EOEditingContext editingContext, IGrhumPersonne debiteurPersonne) {
		if (debiteurPersonne == null) {
			return NSArray.EmptyArray;
		}
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(IGrhumRib.I_RIB_VALIDE_KEY, EOQualifier.QualifierOperatorEqual, "O"));
		quals.addObject(new EOKeyValueQualifier(IGrhumRib.I_TO_FOURNIS_KEY + "." + IGrhumFournis.I_TO_PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, debiteurPersonne));

		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(IGrhumRib.I_D_CREATION_KEY, EOSortOrdering.CompareDescending));
		return fetchAll(editingContext, new EOAndQualifier(quals), sorts);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
		EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		NSArray eoObjects = (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
}
