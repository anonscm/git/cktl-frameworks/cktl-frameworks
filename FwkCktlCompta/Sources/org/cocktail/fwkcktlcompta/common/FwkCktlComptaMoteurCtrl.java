package org.cocktail.fwkcktlcompta.common;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * @author rprin
 */
public class FwkCktlComptaMoteurCtrl {

	public static enum Contexte {
		SERVEUR,
		CLIENT
	}

	public static final String METHOD_GET_CONFIG = "getConfig";

	private static FwkCktlComptaMoteurCtrl sharedInstance = new FwkCktlComptaMoteurCtrl();

	private Map<String, Method> methods = new HashMap<String, Method>();

	private Contexte contexteApplication;
	private String reportsBaseLocation;
	private String refApplicationCreation;

	private FwkCktlComptaMoteurCtrl() {
	}

	/**
	 * Initialise le moteur SEPA en fonction du contexte d'utilisation .
	 * @param contexte SERVEUR ou CLIENT
	 */
	public void init(Contexte contexte) {
		setContexteApplication(contexte);
	}

	public static FwkCktlComptaMoteurCtrl getSharedInstance() {
		return sharedInstance;
	}

	public Contexte getContexteApplication() {
		if (contexteApplication == null) {
			throw new RuntimeException("Le moteur SepaSdd n'a pas été initialisé. Appelez la méthode FwkCktlComptaMoteurCtrl.getSharedInstance().init(FwkCktlComptaMoteurCtrl.Contexte) au lancement de l'application (serveur et client).");
		}
		return contexteApplication;
	}

	public void setContexteApplication(Contexte contexteApplication) {
		this.contexteApplication = contexteApplication;
		getMethods().clear();
		try {
			if (isOnClient()) {
				initialiseMethodsForClient();
			}
			else {
				initialiseMethodsForServer();
			}
		} catch (Exception e) {
			throw new RuntimeException("Le moteur SepaSdd n'a pas pu etre initialisé.", e);
		}

	}

	private void initialiseMethodsForServer() throws ClassNotFoundException, SecurityException, NoSuchMethodException {
		getMethods().put(METHOD_GET_CONFIG, Class.forName("org.cocktail.fwkcktlcompta.server.remotes.RemoteDelegateCompta").getMethod("getConfig", String.class));
	}

	private void initialiseMethodsForClient() throws SecurityException, NoSuchMethodException, ClassNotFoundException {
		getMethods().put(METHOD_GET_CONFIG, Class.forName("org.cocktail.fwkcktlcompta.client.remotecalls.ServerCallCompta").getMethod("clientSideRequestGetConfig", EOEditingContext.class, String.class));
	}

	/**
	 * @return TRUE si le contexte de l'application est initialisé coté client.
	 */
	public Boolean isOnClient() {
		return Contexte.CLIENT.equals(getContexteApplication());
	}

	public String getReportsBaseLocation() {
		return reportsBaseLocation;
	}

	public void setReportsBaseLocation(String reportsBaseLocation) {
		this.reportsBaseLocation = reportsBaseLocation;
	}

	public Map<String, Method> getMethods() {
		return methods;
	}

	public String getRefApplicationCreation() {
		return refApplicationCreation;
	}

	public void setRefApplicationCreation(String refApplicationCreation) {
		this.refApplicationCreation = refApplicationCreation;
	}
}
