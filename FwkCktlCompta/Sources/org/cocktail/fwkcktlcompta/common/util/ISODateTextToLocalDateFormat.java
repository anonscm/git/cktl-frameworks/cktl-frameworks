package org.cocktail.fwkcktlcompta.common.util;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

import org.joda.time.ReadablePartial;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Permet de formater des dates initialement représentée sous forme de String ISO (du type YYYY-MM-DD) en dates locales. Le parse n'est pas
 * implémenté.
 * 
 * @author rprin
 */
public class ISODateTextToLocalDateFormat extends Format {
	public static final String FRENCH_SHORT_DATE_PATTERN = "dd/MM/yyyy";
	public static final String FRENCH_DATETIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

	private static final long serialVersionUID = 1L;
	private String pattern;

	/**
	 * @param pattern pattern pour le format de la date (cf. {@link DateTimeFormatter})
	 */
	public ISODateTextToLocalDateFormat(String pattern) {
		this.pattern = pattern;
	}

	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		try {
			ReadablePartial dt = null;
			dt = DateConversionUtil.sharedInstance().parseDateTimeSilent((String) obj);
			if (dt == null) {
				dt = DateConversionUtil.sharedInstance().parseDateSilent((String) obj);
			}
			DateTimeFormatter jodaOutputFormat = DateTimeFormat.forPattern(pattern);
			return new StringBuffer(jodaOutputFormat.print(dt));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * non implémenté.
	 *
	 * @return null
	 */
	@Override
	public Object parseObject(String source, ParsePosition pos) {
		return null;
	}
}
