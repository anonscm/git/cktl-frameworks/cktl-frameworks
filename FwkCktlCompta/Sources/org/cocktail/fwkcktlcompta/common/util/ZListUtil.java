package org.cocktail.fwkcktlcompta.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ZListUtil {

	public static String toCommaSeparatedString(List<?> list) {
		StringBuilder result = new StringBuilder();
		for (Object object : list) {
			result.append(object.toString());
			result.append(",");
		}
		return result.length() > 0 ? result.substring(0, result.length() - 1) : "";
	}

	public static List<String> createListFromCommaSeparatedStringAndTrim(String string) {
		if (string == null) {
			return new ArrayList<String>();
		}
		List<String> tmp = Arrays.asList(string.split("\\s*,\\s*"));
		//return tmp;
		List<String> out = new ArrayList<String>();
		for (String string2 : tmp) {
			out.add(string2.trim());
		}
		return out;
	}
}
