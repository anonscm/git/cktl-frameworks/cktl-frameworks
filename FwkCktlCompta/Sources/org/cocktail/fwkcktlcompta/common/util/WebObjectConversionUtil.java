package org.cocktail.fwkcktlcompta.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.webobjects.foundation.NSArray;

public class WebObjectConversionUtil {

    private WebObjectConversionUtil() {
    }

    @SuppressWarnings("unchecked")
	public static <T> List<T> asList(NSArray objects) {
    	if (objects == null) {
    		return Collections.emptyList();
    	}

    	List<T> asList = new ArrayList<T>();
    	for (int idx = 0; idx < objects.count(); idx++) {
    		asList.add((T) objects.objectAtIndex(idx));
    	}

    	return asList;
    }

    @SuppressWarnings("unchecked")
	public static NSArray asRawNSArray(Collection<?> collection) {
    	return new NSArray(collection.toArray());
    }

}
