package org.cocktail.fwkcktlcompta.common.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import com.webobjects.eocontrol.EOQualifierEvaluation;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSKeyValueCodingAdditions;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableSet;
import com.webobjects.foundation.NSRange;
import com.webobjects.foundation.NSSet;

/**
 * Certaines methodes recuperees de ERXArrayUtilities.
 * 
 * @author rprin
 */
public class CktlArrayUtilities {

	/**
	 * Filters any kinds of collections that implements {@link Iterator} interface such as {@link com.webobjects.foundation.NSArray NSArray},
	 * {@link com.webobjects.foundation.NSSet NSSet}, {@link Vector} and {@link Hashtable} using the
	 * {@link com.webobjects.eocontrol.EOQualifierEvaluation EOQualifierEvaluation} interface.
	 * 
	 * @param iterator to be filtered; to obtain an iterator, use iterator() for the java collections
	 * @param qualifier to do the filtering
	 * @return true if there is at least one match
	 */
	public static boolean iteratorHasMatchWithQualifierEvaluation(Iterator<?> iterator, EOQualifierEvaluation qualifier) {
		while (iterator.hasNext()) {
			Object object = iterator.next();
			if (qualifier.evaluateWithObject(object))
				return true;
		}
		return false;
	}

	/**
	 * Filters any kind of collections that implements {@link Iterator} interface such as {@link ArrayList}, {@link HashMap}, {@link SortedSet} and
	 * {@link TreeSet} using the {@link com.webobjects.eocontrol.EOQualifierEvaluation EOQualifierEvaluation} interface.
	 * 
	 * @param iterator to be filtered; use iterator() to obtain an iterator from the collections
	 * @param qualifier to do the filtering
	 * @return array of filtered results.
	 */
	public static NSArray filteredArrayWithQualifierEvaluation(Iterator iterator, EOQualifierEvaluation qualifier) {
		NSMutableArray result = new NSMutableArray();
		while (iterator.hasNext()) {
			Object object = iterator.next();
			if (qualifier.evaluateWithObject(object))
				result.addObject(object);
		}
		return result;
	}

	/**
	 * Filters out duplicates of an array of objects based on the value of the given key path off of those objects. Objects with a null value will be
	 * skipped, too.
	 * 
	 * @param objects array of objects
	 * @param key keypath to be evaluated off of every object
	 * @return filter array of objects based on the value of a keypath.
	 */
	public static NSArray arrayWithoutDuplicateKeyValue(NSArray objects, String key) {
		NSMutableSet present = new NSMutableSet();
		NSMutableArray result = new NSMutableArray();
		for (Enumeration e = objects.objectEnumerator(); e.hasMoreElements();) {
			Object o = e.nextElement();
			Object value = (Object) NSKeyValueCodingAdditions.Utility.valueForKeyPath(o, key);
			if (value != null && !present.containsObject(value)) {
				present.addObject(value);
				result.addObject(o);
			}
		}
		return result;
	}

	/**
	 * Subtracts the contents of one array from another. Objecthe order of the array should be preseved.
	 * 
	 * @param main array to have values removed from it.
	 * @param minus array of values to remove from the main array
	 * @return array after performing subtraction.
	 */
	public static NSArray arrayMinusArray(NSArray main, NSArray<?> minus) {
		NSSet<Object> minusSet = new NSSet<Object>(minus);
		NSMutableArray mutableResult = new NSMutableArray(main.count());
		Enumeration e = main.objectEnumerator();
		while (e.hasMoreElements()) {
			Object obj = e.nextElement();
			if (!minusSet.containsObject(obj))
				mutableResult.addObject(obj);
		}
		return mutableResult.immutableClone();
	}

	/**
	 * Subtracts a single object from an array.
	 * 
	 * @param main array to have value removed from it.
	 * @param object to be removed
	 * @return array after performing subtraction.
	 */
	public static NSArray arrayMinusObject(NSArray main, Object object) {
		NSMutableArray mutable = new NSMutableArray(main);
		mutable.removeObject(object);
		return mutable.immutableClone();
	}

	/**
	 * Creates an array preserving order by adding all of the non-duplicate values from the second array to the first.
	 * 
	 * @param a1 first array
	 * @param a2 second array
	 * @return array containing all of the elements of the first array and all of the non-duplicate elements of the second array.
	 */
	public static NSArray arrayByAddingObjectsFromArrayWithoutDuplicates(NSArray a1, NSArray a2) {
		if (a2.count() == 0)
			return (NSArray) a1;

		NSMutableArray result = new NSMutableArray(a1);
		addObjectsFromArrayWithoutDuplicates(result, a2);
		return result;
	}

	/**
	 * Creates an array that has all of the objects of the parameter array without the first object.
	 * 
	 * @param array the array to use to create the result
	 * @return an array containing all objects but the first of the parameter array. if null is passed, null is returned. if the parameter array is
	 *         empty, an empty array is returned.
	 */
	public static NSArray arrayByRemovingFirstObject(NSArray array) {
		NSArray result = null;

		if (array != null) {
			final int count = array.count();

			result = count > 1 ? array.subarrayWithRange(new NSRange(1, count - 1)) : NSArray.EmptyArray;
		}

		return result;
	}

	/**
	 * Adds the object to the mutable array if the object is not null.
	 * 
	 * @param array mutable array where non-null object will be added
	 * @param object to be added to array
	 */
	public static void safeAddObject(NSMutableArray array, Object object) {
		if (array != null && object != null) {
			array.addObject(object);
		}
	}

	/**
	 * Adds all of the non-duplicate elements from the second array to the mutable array.
	 * 
	 * @param a1 mutable array where non-duplicate objects are added
	 * @param a2 array to be added to a1
	 */
	public static void addObjectsFromArrayWithoutDuplicates(NSMutableArray a1, NSArray a2) {
		NSMutableSet resultSet = new NSMutableSet(a1);
		for (Enumeration e = a2.objectEnumerator(); e.hasMoreElements();) {
			Object elt = e.nextElement();
			if (!resultSet.containsObject(elt)) {
				a1.addObject(elt);
				resultSet.addObject(elt);
			}
		}
	}

	/**
	 * Recursively flattens an array of arrays and individual objects into a single array of elements.<br/>
	 * <br/>
	 * For example:<br/>
	 * <code>NSArray foos;</code> //Assume exists<br/>
	 * <code>NSArray bars = (NSArray)foos.valueForKey("toBars");</code> In this case if <code>foos</code> contained five elements then the array
	 * <code>bars</code> will contain five arrays each corresponding to what <code>aFoo.toBars</code> would return. To have the entire collection of
	 * <code>bars</code> in one single array you would call: <code>NSArray allBars = flatten(bars)</code>
	 * 
	 * @param originalArray array to be flattened
	 * @param filterDuplicates determines if the duplicate values should be filtered
	 * @return an array containing all of the elements from all of the arrays contained within the array passed in. (Optionally, with duplicate
	 *         elements filtered out)
	 */
	@SuppressWarnings("unchecked")
	public static NSArray flatten(NSArray<?> originalArray, boolean filterDuplicates) {
		NSArray<?> flattenedArray = flatten(originalArray);

		if (filterDuplicates) {
			return arrayWithoutDuplicates(flattenedArray);
		}
		return flattenedArray;
	}

	/**
	 * Recursively flattens an array of arrays and individual objects into a single array of elements.<br/>
	 * <br/>
	 * For example:<br/>
	 * <code>NSArray foos;</code> //Assume exists<br/>
	 * <code>NSArray bars = (NSArray)foos.valueForKey("toBars");</code> In this case if <code>foos</code> contained five elements then the array
	 * <code>bars</code> will contain five arrays each corresponding to what <code>aFoo.toBars</code> would return. To have the entire collection of
	 * <code>bars</code> in one single array you would call: <code>NSArray allBars = flatten(bars)</code>
	 * 
	 * @param originalArray array to be flattened
	 * @return an array containing all of the elements from all of the arrays contained within the array passed in.
	 */
	@SuppressWarnings("unchecked")
	public static NSArray flatten(NSArray<?> originalArray) {
		if (originalArray == null || originalArray.count() < 1) {
			return originalArray;
		}

		NSMutableArray<Object> newArray = null; // Not gonna create a new array if we don't actually need to flatten
		for (int i = 0; i < originalArray.count(); i++) {
			Object element = originalArray.objectAtIndex(i);
			if (element instanceof NSArray) {
				if (newArray == null) {
					// Turns out we actually need to flatten
					newArray = new NSMutableArray<Object>();
					for (int backfillIndex = 0; backfillIndex < i; backfillIndex++) {
						// backfill any singles we put off copying
						newArray.addObject(originalArray.objectAtIndex(backfillIndex));
					}
				}

				NSArray<?> flattenedChildArray = flatten((NSArray<?>) element);
				newArray.addObjectsFromArray(flattenedChildArray);
			}
			else if (newArray != null) {
				newArray.addObject(element);
			} // Otherwise let's put off copying the elment, the backfill section above will take care of it.
		}

		// CLEANUP: Arguably safer to return the immutable array we are declared as returning
		return (newArray != null ? newArray : originalArray);
	}


	/**
	 * Performs multiple key-value coding calls against an array or an object.
	 * 
	 * @param array collection or object to be acted upon.
	 * @param paths array of keypaths.
	 * @return for collections, returns an array containing an array of values for every keypath. For objects, returns an array containing a value for
	 *         every keypath.
	 */
	@SuppressWarnings("unchecked")
	public static NSArray valuesForKeyPaths(Object array, NSArray<String> paths) {
		NSMutableArray<Object> result = new NSMutableArray<Object>();

		Enumeration<String> e = paths.objectEnumerator();
		while (e.hasMoreElements()) {
			Object value = NSKeyValueCodingAdditions.Utility.valueForKeyPath(array, e.nextElement());
			result.addObject(value != null ? value : NSKeyValueCoding.NullValue);
		}
		return result;
	}

	/**
	 * Returns the first object of the array. If the array is null or empty, null is returned.
	 * 
	 * @param array the array to search.
	 * @return the first object in array. null if array is empty or if array is null.
	 */
	public static Object firstObject(NSArray array) {
		Object result = null;

		if (array != null && array.count() > 0)
			result = array.objectAtIndex(0);

		return result;
	}

	/**
	 * Finds the index of the first object in the array with a given value for a given keypath. Assumes that all objects in the array either are
	 * NSKeyValueCoding.NullValue or have the given keypath.
	 * 
	 * @param array the array to search.
	 * @param value the value to look for.
	 * @param keyPath the keypath to use to compare to value.
	 * @return index of the first object with the qualification. -1 if none matches.
	 */
	public static int indexOfFirstObjectWithValueForKeyPath(NSArray<?> array, Object value, String keyPath) {
		final int count = array.count();
		int result = -1;
		int i = 0;
		while (i < count) {
			Object currentObject = array.objectAtIndex(i);
			if (currentObject != NSKeyValueCoding.NullValue) {
				Object currentValue = NSKeyValueCodingAdditions.Utility.valueForKeyPath(currentObject, keyPath);
				currentValue = (currentValue == NSKeyValueCoding.NullValue ? null : currentValue);
				if (currentValue == value || (value != null && value.equals(currentValue))) {
					return i;
				}
			}
			i++;
		}

		return result;
	}

	/**
	 * Finds the first object in the array with a given value for a given key path.
	 * 
	 * @param array the array to search.
	 * @param value the value to look for.
	 * @param keyPath the keypath to use to compare to value.
	 * @return first object in the array with the qualification. null if none matches.
	 */
	public static Object firstObjectWithValueForKeyPath(NSArray array, Object value, String keyPath) {
		final int index = indexOfFirstObjectWithValueForKeyPath(array, value, keyPath);

		return index >= 0 ? array.objectAtIndex(index) : null;
	}

	/**
	 * Shorter name for arrayWithoutDuplicates, which I always forget the name of.
	 * 
	 * @param type of the array
	 * @param array the array to return distinct values from
	 * @return an array of distinct elements from the input array
	 */
	public static NSArray distinct(NSArray array) {
		return arrayWithoutDuplicates(array);
	}

	/**
	 * Filters out all of the duplicate objects in a given array.<br/>
	 * Preserves the order now.
	 * 
	 * @param anArray to be filtered
	 * @return filtered array.
	 */
	public static NSArray arrayWithoutDuplicates(NSArray anArray) {
		NSMutableArray result = new NSMutableArray();
		NSMutableSet already = new NSMutableSet();
		for (Object object : anArray) {
			if (!already.containsObject(object)) {
				already.addObject(object);
				result.addObject(object);
			}
		}
		return result;
	}
	
	
	public static <E> List<E> toList(NSArray array) {
		ArrayList<E> maListe = new ArrayList<E>();
		for (int i = 0; i < array.count(); i++) {
			E array_element = (E) array.objectAtIndex(i);
			maListe.add(array_element);
		}
		return maListe;
	}


}
