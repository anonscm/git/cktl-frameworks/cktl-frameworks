package org.cocktail.fwkcktlcompta.common.util;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.HashMap;
import java.util.Map;

public class BooleanFormat extends Format {

	public static final String TRUE_LABEL = "Oui";
	public static final String FALSE_LABEL = "Non";
	public static final Map<Boolean, String> BOOLEAN_LABELS = new HashMap<Boolean, String>();

	static {
		BOOLEAN_LABELS.put(Boolean.TRUE, TRUE_LABEL);
		BOOLEAN_LABELS.put(Boolean.FALSE, FALSE_LABEL);
	}

	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		String objText = null;
		if (obj instanceof Boolean) {
			Boolean asBoolean = (Boolean) obj;
			objText = BOOLEAN_LABELS.get(asBoolean);
		} else {
			objText = obj.toString();
		}
		return toAppendTo.append(objText);
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		return toBoolean(source) ? Boolean.TRUE : Boolean.FALSE;
	}

	private boolean toBoolean(String name) {
		return ((name != null) && name.equalsIgnoreCase(TRUE_LABEL));
	}

}
