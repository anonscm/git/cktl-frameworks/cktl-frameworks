package org.cocktail.fwkcktlcompta.common.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class ZStreamUtil {

	/**
	 * Returns the byte array for a given stream.
	 * 
	 * @param in stream to get the bytes from
	 * @throws IOException if things go wrong
	 * @return byte array of the stream.
	 */
	public static byte[] bytesFromInputStream(InputStream in) throws IOException {
		if (in == null)
			throw new IllegalArgumentException("null input stream");

		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		int read = -1;
		byte[] buf = new byte[1024 * 50];
		while ((read = in.read(buf)) != -1) {
			bout.write(buf, 0, read);
		}

		return bout.toByteArray();
	}

	/**
	 * Returns a string from the input stream using the specified encoding.
	 * 
	 * @param in stream to read
	 * @param encoding to be used, null will use the default
	 * @return string representation of the stream.
	 */
	public static String stringFromInputStream(InputStream in, String encoding) throws IOException {
		return new String(bytesFromInputStream(in), encoding);
	}

	/**
	 * Returns a string from the input stream using the default encoding.
	 * 
	 * @param in stream to read
	 * @return string representation of the stream.
	 */
	public static String stringFromInputStream(InputStream in) throws IOException {
		return new String(bytesFromInputStream(in));
	}
}
