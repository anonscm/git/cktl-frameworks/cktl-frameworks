package org.cocktail.fwkcktlcompta.common.util;

import java.util.Iterator;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;
import org.cocktail.fwkcktlcompta.common.entities.IFwkCktlComptaEntity;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class CktlEOControlUtilities {

	/** logging support */
	public static final Logger log = Logger.getLogger(CktlEOControlUtilities.class);



	/**
	 * Joins the given qualifiers with an AND. One or both arguments may be null, if both are null, null is returned.
	 * 
	 * @param q1
	 * @param q2
	 */
	public static EOQualifier andQualifier(EOQualifier q1, EOQualifier q2) {
		if (q1 == null) {
			return q2;
		}
		if (q2 == null) {
			return q1;
		}
		return new EOAndQualifier(new NSArray(new Object[] {
				q1, q2
		}));
	}

	/**
	 * Joins the given qualifiers with an OR. One or both arguments may be null, if both are null, null is returned.
	 * 
	 * @param q1
	 * @param q2
	 */
	public static EOQualifier orQualifier(EOQualifier q1, EOQualifier q2) {
		if (q1 == null) {
			return q2;
		}
		if (q2 == null) {
			return q1;
		}
		return new EOOrQualifier(new NSArray(new Object[] {
				q1, q2
		}));
	}

	/**
	 * @param edc
	 * @param record
	 * @return
	 */
	public static NSDictionary committedSnapshot(EOEditingContext edc, IFwkCktlComptaEntity record) {
		if (!isNewObject(edc, record)) {
			return edc.committedSnapshotForObject((EOEnterpriseObject) record);
		}
		return new NSDictionary();
	}

	public static boolean isNewObject(EOEditingContext edc, IFwkCktlComptaEntity record) {
		return edc.globalIDForObject((EOEnterpriseObject) record).isTemporary();
	}

	public static NSArray faultsFromGlobalIds(EOEditingContext edc, NSArray gids, Class entityClass) throws Exception {
		NSMutableArray locs = new NSMutableArray();
		Iterator<EOGlobalID> iterdep = gids.iterator();
		while (iterdep.hasNext()) {
			EOGlobalID gid = (EOGlobalID) iterdep.next();
			Object dep = entityClass.cast(edc.faultForGlobalID(gid, edc));
			if (dep == null) {
				throw new Exception("Impossible de récupérer l'objet " + entityClass.getName() + " pour le gid = " + gid);
			}
			locs.addObject(dep);
		}
		return locs.immutableClone();

	}

	public static void refaultObject(EOEnterpriseObject eo) {
		if (eo != null && !eo.isFault()) {
			EOEditingContext ec = eo.editingContext();
			NSArray<EOGlobalID> gids = new NSArray<EOGlobalID>(ec.globalIDForObject(eo));
			ec.invalidateObjectsWithGlobalIDs(gids);
		}
	}

	public static void invalidateObjects(NSArray eos) {
		if (eos.count() > 0) {
			if (FwkCktlComptaMoteurCtrl.getSharedInstance().isOnClient()) {
				EOEditingContext ec = ((EOEnterpriseObject) eos.objectAtIndex(0)).editingContext();
				org.cocktail.fwkcktlcompta.client.remotecalls.ServerCallCompta.clientSideRequestInvalidateObjectsOnServer(ec, globalIdsForObjects(eos));
			}
			for (int i = 0; i < eos.count(); i++) {
				refaultObject((EOEnterpriseObject) eos.objectAtIndex(i));
			}
		}
	}

	public static NSArray globalIdsForObjects(NSArray eos) {
		NSMutableArray gids = new NSMutableArray();
		for (int i = 0; i < eos.count(); i++) {
			EOEnterpriseObject eo = (EOEnterpriseObject) eos.objectAtIndex(i);
			gids.addObject(eo.editingContext().globalIDForObject(eo));
		}
		return gids.immutableClone();
	}

}
