package org.cocktail.fwkcktlcompta.common.util;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.common.FwkCktlComptaUtil;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.ReadablePartial;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Gere les dates liees au SEPA. Permet format et parse de date avec le format ISO8601 etendu. Exploite les classes de la bibiothèque joda-time.
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class DateConversionUtil {
	public static Logger LOG = Logger.getLogger(DateConversionUtil.class.getName());
	public static final DateTimeFormatter DATE_WITH_TIME_FORMATTER = ISODateTimeFormat.dateHourMinuteSecondMillis();
	public static final DateTimeFormatter DATE_WITHOUT_TIME_FORMATTER = ISODateTimeFormat.date();
	public static final DateTimeFormatter DATE_WITHOPTIONALTIME_PARSER = ISODateTimeFormat.dateOptionalTimeParser();
	public static final DateTimeFormatter DATE_SHORT_FORMATTER = new DateTimeFormatterBuilder()
																	.appendDayOfMonth(2)
																	.appendLiteral('/')
																	.appendMonthOfYear(2)
																	.appendLiteral('/')
															    	.appendYear(4, 4)
															    	.toFormatter();

	private static final DateConversionUtil sharedInstance = new DateConversionUtil();

	/**
	 * @param dt la date/heure
	 * @return la date et l'heure sous forme de chaine du type yyyy-MM-dd'T'HH:mm:ss.SSS
	 */
	public String formatDateWithTimeISO(ReadablePartial dt) {
		return DATE_WITH_TIME_FORMATTER.print(dt);
	}

	/**
	 * @param dt la date/heure
	 * @return la date seule sous forme de chaine du type yyyy-MM-dd
	 */
	public String formatDateWithoutTimeISO(ReadablePartial dt) {
		return DATE_WITHOUT_TIME_FORMATTER.print(dt);
	}

	/**
	 * @param instant la date/heure sous la forme d'un entier
	 * @return la date seule sous forme de chaine du type yyyy-MM-dd
	 */
	public String formatDateWithoutTimeISO(long instant) {
		return DATE_WITHOUT_TIME_FORMATTER.print(instant);
	}

	public String formatDateShort(ReadablePartial dt) {
		return DATE_SHORT_FORMATTER.print(dt);
	}

	/**
	 * Convertit une chaine de caracteres en date/heure sans se préoccuper du timezone. Si string n'est pas valide, un illegalArgmentException est
	 * déclenché.
	 *
	 * @param string Une chaine de caractere représentant une date/heure avec ms, du type yyyy-MM-dd'T'HH:mm:ss.SSS. Exemple 2013-06-25T12:11:59.850
	 * @return Une date/heure sans les donnees de timezone
	 */
	public LocalDateTime parseDateTime(String string) {
		LocalDateTime res = DATE_WITHOPTIONALTIME_PARSER.parseLocalDateTime(string);
		return res;
	}

	/**
	 * Convertit une chaine de caracteres en date seule sans se préoccuper du timezone. Si string n'est pas valide une illegalArgumentException est
	 * déclenchée..
	 *
	 * @param string Une chaine de caractere représentant une date/heure, du type yyyy-MM-dd'T'HH:mm:ss.SSS
	 * @return Une date sans les heures
	 * @throws Exception
	 */
	public LocalDate parseDate(String string) {
		try {
		LocalDate res = DATE_WITHOPTIONALTIME_PARSER.parseLocalDate(string);
		return res;
		} catch (java.lang.NoSuchMethodError e1) {
			e1.printStackTrace();

			throw new java.lang.NoSuchMethodError("Erreur liée au classpath : une ancienne version de jodatime (1.6 ? ) doit etre chargee au lieu d'une version >= 2.2.  " + FwkCktlComptaUtil.sharedInstance().getClasspathString());
		}

	}

	/**
	 * Convertit une chaine de caracteres en date seule sans se préoccuper du timezone. Si string n'est pas valide, null est renvoyé.
	 *
	 * @param string Une chaine de caractere représentant une date/heure, du type yyyy-MM-dd'T'HH:mm:ss.SSS
	 * @return Une date sans les heures
	 */
	public LocalDate parseDateSilent(String string) {
		try {
			LocalDate res = parseDate(string);
			return res;
		} catch (Exception e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Erreur d'analyse de date : " + string + ". " + e.getClass().getName() + ":" + e.getMessage());
			}
			return null;
		}
	}

	/**
	 * Convertit une chaine de caracteres en date/heure sans se préoccuper du timezone. Si string n'est pas valide, null est renvoyé.
	 *
	 * @param string Une chaine de caractere représentant une date/heure avec ms, du type yyyy-MM-dd'T'HH:mm:ss.SSS. Exemple 2013-06-25T12:11:59.850
	 * @return Une date/heure sans les donnees de timezone
	 */
	public LocalDateTime parseDateTimeSilent(String string) {
		try {
			LocalDateTime res = parseDateTime(string);
			return res;
		} catch (Exception e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Erreur d'analyse de date : " + string + ". " + e.getClass().getName() + ":" + e.getMessage());
			}
			return null;
		}
	}

	/**
	 * @param string Une date/heure
	 * @return True si string représente une date/heure valide au format ISO
	 */
	public Boolean isValidDateTimeISO(String string) {
		return (parseDateTimeSilent(string) != null);
	}

	/**
	 * @param string Une date
	 * @return True si string représente une date valide au format ISO
	 */
	public Boolean isValidDateISO(String string) {
		return (parseDateSilent(string) != null);
	}

	public static DateConversionUtil sharedInstance() {
		return sharedInstance;
	}

}
