package org.cocktail.fwkcktlcompta.common.util;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

public class CktlConfigUtil {
	private static final String OUI = "OUI";
	private static final String O = "O";
	private static final String UN = "1";
	private static final String TRUE = "TRUE";

	public static Boolean getBooleanValueForConfig(String val) {
		if (StringCtrl.isEmpty(val)) {
			return false;
		}
		val = val.toUpperCase();
		return (OUI.equals(val) || O.equals(val) || UN.equals(val) || TRUE.equals(val));
	}
}
