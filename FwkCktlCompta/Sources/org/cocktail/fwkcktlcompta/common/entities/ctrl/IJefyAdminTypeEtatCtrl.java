package org.cocktail.fwkcktlcompta.common.entities.ctrl;

import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminTypeEtat;

import com.webobjects.eocontrol.EOEditingContext;


public class IJefyAdminTypeEtatCtrl extends CommonEntityCtrl {
	public static final String ENTITY_NAME = "FwkCktlCompta_JefyAdmin_TypeEtat";
	public static final String CLIENT_CLASS_ENTITY_NAME = "org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat";
	public static final String SERVER_CLASS_ENTITY_NAME = "org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminTypeEtat";

	private static IJefyAdminTypeEtatCtrl sharedInstance = new IJefyAdminTypeEtatCtrl();

	public static final IJefyAdminTypeEtatCtrl getSharedInstance() {
		return sharedInstance;
	}
	@Override
	protected String clientClassNameForEntity() {
		return CLIENT_CLASS_ENTITY_NAME;
	}

	@Override
	protected String serverClassNameForEntity() {
		return SERVER_CLASS_ENTITY_NAME;
	}

	@Override
	protected String entityName() {
		return ENTITY_NAME;
	}

	public IJefyAdminTypeEtat getTypeEtat(EOEditingContext editingContext, String etatLibelle) throws Exception {
		return (IJefyAdminTypeEtat) fetchByKeyValue(editingContext, IJefyAdminTypeEtat.I_TYET_LIBELLE_KEY, etatLibelle);
	}

}
