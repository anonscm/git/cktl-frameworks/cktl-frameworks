package org.cocktail.fwkcktlcompta.common.entities.ctrl;

import java.lang.reflect.Method;

import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public abstract class CommonEntityCtrl {
	private Class entityClass;

	public final Class getEntityClass() throws ClassNotFoundException {
		if (entityClass == null) {
			if (FwkCktlComptaMoteurCtrl.getSharedInstance().isOnClient()) {
				entityClass = Class.forName(clientClassNameForEntity());
			}
			else {
				entityClass = Class.forName(serverClassNameForEntity());
			}
		}
		return entityClass;
	}

	protected abstract String clientClassNameForEntity();
	protected abstract String serverClassNameForEntity();
	protected abstract String entityName();

	protected NSArray fetchAll(Class entityClass, EOEditingContext editingContext, EOQualifier qual, NSArray sorts) throws Exception {
		Method fetchAllMethod = entityClass.getMethod("fetchAll", EOEditingContext.class, EOQualifier.class, NSArray.class);
		NSArray res = (NSArray) fetchAllMethod.invoke(null, editingContext, qual, sorts);
		return res;
	}

	protected EOEnterpriseObject fetchByKeyValue(Class entityClass, EOEditingContext editingContext, String keyName, Object value) throws Exception {
		NSArray res = fetchAll(entityClass, editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
		if (res.count() == 0) {
			return null;
		}
		if (res.count() == 1) {
			return (EOEnterpriseObject) res.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("La requête renvoie plusieurs réponses");
		}
	}



	public NSArray commonFetchSpec(EOEditingContext editingContext, EOQualifier qual, NSArray sorts, int numberMaxOfResult) throws Exception {
		EOFetchSpecification spec = new EOFetchSpecification(entityName(), qual, new NSArray(sorts));
		spec.setFetchLimit(numberMaxOfResult);
		NSArray res = editingContext.objectsWithFetchSpecification(spec);
		return res;
	}

	public NSArray fetchAll(EOEditingContext editingContext, EOQualifier qual, NSArray sorts) throws Exception {
		return fetchAll(getEntityClass(), editingContext, qual, sorts);
	}

	public EOEnterpriseObject fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws Exception {
		return fetchByKeyValue(getEntityClass(), editingContext, keyName, value);
	}

	protected EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}


}
