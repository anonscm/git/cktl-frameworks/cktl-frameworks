package org.cocktail.fwkcktlcompta.common.entities;

public interface IPrelevementFichier {

	IRecouvrement toRecouvrement();

	String ficpContenu();

}
