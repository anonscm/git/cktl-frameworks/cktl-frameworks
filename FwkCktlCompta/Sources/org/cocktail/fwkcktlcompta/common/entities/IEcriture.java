package org.cocktail.fwkcktlcompta.common.entities;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public interface IEcriture {
	static final String I_ECR_NUMERO_KEY = "ecrNumero";

	static final EOSortOrdering SORT_ECR_NUMERO_ASC = EOSortOrdering.sortOrderingWithKey(IEcriture.I_ECR_NUMERO_KEY, EOSortOrdering.CompareAscending);

	static final String ECR_ETAT_VALIDE = "VALIDE";
	static final String ECR_ETAT_ANNULE = "ANNULE";

	NSArray toEcritureDetails();

	IJefyAdminExercice toExercice();

	Integer ecrNumero();

	String ecrLibelle();

	NSTimestamp ecrDate();

}
