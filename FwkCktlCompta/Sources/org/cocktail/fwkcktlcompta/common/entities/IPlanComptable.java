package org.cocktail.fwkcktlcompta.common.entities;

public interface IPlanComptable {

    String libelleLong();

}
