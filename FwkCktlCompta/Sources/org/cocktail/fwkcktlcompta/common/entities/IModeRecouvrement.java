package org.cocktail.fwkcktlcompta.common.entities;

public interface IModeRecouvrement {

	public IPlanComptableExer toPlanComptableExerPaiement();

	public IPlanComptableExer toPlanComptableExerVisa();
}
