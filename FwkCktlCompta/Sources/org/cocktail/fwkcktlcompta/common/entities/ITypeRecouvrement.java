package org.cocktail.fwkcktlcompta.common.entities;

public interface ITypeRecouvrement {
	final static String TREC_FORMAT_BDF = "BDF";
	final static String TREC_FORMAT_SEPASDD = "SEPASDD";

	String trecFormat();
}
