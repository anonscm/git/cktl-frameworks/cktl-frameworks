package org.cocktail.fwkcktlcompta.common.entities;

public interface ITypeBordereau {

	// Accessors methods
	String tboLibelle();

	void setTboLibelle(String value);

	String tboSousType();

	void setTboSousType(String value);

	String tboType();

	void setTboType(String value);

}