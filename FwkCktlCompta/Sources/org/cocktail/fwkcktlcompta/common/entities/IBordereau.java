package org.cocktail.fwkcktlcompta.common.entities;

import java.util.List;

public interface IBordereau {

	String borEtat();
	
    Integer borNum();
    
    List<? extends IBordereauBrouillard> toBordereauBrouillards();

    List<? extends IBordereauInfo> toBordereauInfos();

    ITypeBordereau toTypeBordereau();
    
    IGestion toGestion();
}
