package org.cocktail.fwkcktlcompta.common.entities;

public interface IJefyAdminTypeEtat {

	public static final String I_TYET_LIBELLE_KEY = "tyetLibelle";

	public static final String ETAT_VALIDE = "VALIDE";
	public static final String ETAT_ANNULE = "ANNULE";
	public static final String ETAT_OUI = "OUI";
	public static final String ETAT_NON = "NON";
	public static final String ETAT_SUPPRIME = "SUPPRIME";
	public static final String ETAT_EN_ATTENTE = "EN ATTENTE";
	public static final String ETAT_A_VALIDER = "A VALIDER";
	public static final String ETAT_DECAISSABLE = "OPERATION DECAISSABLE";
	public static final String ETAT_NON_DECAISSABLE = "OPERATION NON DECAISSABLE";
	public static final String ETAT_NE_RIEN_FAIRE = "NE RIEN FAIRE";
	public static final String ETAT_BLOCAGE = "BLOCAGE";
	public static final String ETAT_ALERTE = "ALERTE";
	public static final String ETAT_RECETTES = "RECETTES";
	public static final String ETAT_DEPENSES = "DEPENSES";
	public static final String ETAT_TOUS = "TOUS";
	public static final String ETAT_TOUTES = "TOUTES";
	public static final String ETAT_AUCUN = "AUCUN";
	public static final String ETAT_AUCUNE = "AUCUNE";
	public static final String ETAT_EN_COURS = "EN COURS";

	String tyetLibelle();

}
