package org.cocktail.fwkcktlcompta.common.entities;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;

public interface IJefyAdminExercice {
	static final String I_EXE_EXERCICE_KEY = "exeExercice";
	static final String I_EXE_STAT_KEY = "exeStat";
	static final String I_EXE_TYPE_KEY = "exeType";

	static final String EXE_ETAT_OUVERT = "O";
	static final String EXE_ETAT_CLOS = "C";
	static final String EXE_ETAT_RESTREINT = "R";
	static final String EXE_ETAT_PREPARATION = "P";

	static final String EXE_ETAT_OUVERT_LIBELLE = "Ouvert";
	static final String EXE_ETAT_CLOS_LIBELLE = "Clos";
	static final String EXE_ETAT_RESTREINT_LIBELLE = "Restreint";
	static final String EXE_ETAT_PREPARATION_LIBELLE = "Préparation";

	static final String EXE_TYPE_COMPTABLE = "C";
	static final String EXE_TYPE_TRESORERIE = "T";

	static final String EXE_TYPE_COMPTABLE_LIBELLE = "Comptable";
	static final String EXE_TYPE_TRESORERIE_LIBELLE = "Trésorerie";

	static final EOSortOrdering SORT_EXE_EXERCICE_DESC = EOSortOrdering.sortOrderingWithKey(I_EXE_EXERCICE_KEY, EOSortOrdering.CompareDescending);
	static final EOQualifier QUAL_OUVERT = new EOKeyValueQualifier(I_EXE_STAT_KEY, EOQualifier.QualifierOperatorEqual, EXE_ETAT_OUVERT);
	static final EOQualifier QUAL_RESTREINT = new EOKeyValueQualifier(I_EXE_STAT_KEY, EOQualifier.QualifierOperatorEqual, EXE_ETAT_RESTREINT);


	String exeStat();

	String exeType();

	Integer exeExercice();

}
