package org.cocktail.fwkcktlcompta.common.entities;

public interface IEmargementDetail {
	IEcritureDetail toEcdSource();

	IEcritureDetail toEcdDestination();
}
