package org.cocktail.fwkcktlcompta.common.entities;

import java.math.BigDecimal;

public interface IEcritureDetail {
	public static final String ECD_SENS_DEBIT = "D";
	public static final String ECD_SENS_CREDIT = "C";
	public static final String I_ECD_SENS_KEY = "ecdSens";
	public static final String I_TO_EXERCICE_KEY = "toExercice";

	BigDecimal ecdMontant();

	IJefyAdminExercice toExercice();

	BigDecimal ecdResteEmarger();

	IPlanComptableExer toPlanComptableExer();

	IEcriture toEcriture();

	Integer ecdIndex();

	String ecdSens();

	String ecdLibelle();

	IGestion toGestion();

	BigDecimal ecdCredit();

	BigDecimal ecdDebit();

}
