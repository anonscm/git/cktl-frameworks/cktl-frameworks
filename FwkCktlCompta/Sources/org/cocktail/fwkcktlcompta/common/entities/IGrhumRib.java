package org.cocktail.fwkcktlcompta.common.entities;

public interface IGrhumRib {
	String I_RIB_VALIDE_KEY = "ribValide";
	String I_TO_FOURNIS_KEY = "toFournis";
	String I_D_CREATION_KEY = "dCreation";

	String RIB_VALIDE = "O";
	String RIB_ANNULE = "A";

	String bicEtIban();

	String ribValide();

	IGrhumBanque toBanque();

	String iban();

	String ribTitco();

	boolean isRibIntlComplet();

	IGrhumFournis toFournis();

}
