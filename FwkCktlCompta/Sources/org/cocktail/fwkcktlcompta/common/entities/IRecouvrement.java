package org.cocktail.fwkcktlcompta.common.entities;

import com.webobjects.eocontrol.EOSortOrdering;

public interface IRecouvrement {
	public static final String I_RECO_DATE_CREATION_KEY = "recoDateCreation";
	public static final EOSortOrdering SORT_DATE_DESC = EOSortOrdering.sortOrderingWithKey(IRecouvrement.I_RECO_DATE_CREATION_KEY, EOSortOrdering.CompareDescending);

	ITypeRecouvrement toTypeRecouvrement();

	Integer recoNumero();

}
