package org.cocktail.fwkcktlcompta.common.entities;

public interface ITitreDetailEcriture {
	public static final String TDE_ORIGINE_VISA = "VISA";
	public static final String TDE_ORIGINE_VISA_CTP = "VISA CTP";

	String tdeOrigine();

	IEcritureDetail toEcritureDetail();

}
