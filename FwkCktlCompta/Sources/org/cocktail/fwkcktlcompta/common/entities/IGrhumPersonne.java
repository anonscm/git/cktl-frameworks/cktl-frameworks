package org.cocktail.fwkcktlcompta.common.entities;


public interface IGrhumPersonne {
	public static final String I_PERS_LIBELLE_KEY = "persLibelle";

	String persLc();

	String persLibelle();

	IGrhumFournis getDerivFournis();

	public Integer persId();

}
