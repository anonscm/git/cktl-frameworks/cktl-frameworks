package org.cocktail.fwkcktlcompta.common.entities;

public interface ITracable {
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String TO_MODIFICATEUR_KEY = "toModificateur";

	public String dCreation();

	public String dModification();

	public Integer persIdCreation();

	public IGrhumPersonne toModificateur();
}
