package org.cocktail.fwkcktlcompta.common.entities;

public interface IPlanComptableExer {

	static final String PCONUMETPCOLIBELLE_KEY = "pcoNumEtPcoLibelle";
	static final String CLASSE_COMPTE_KEY = "classeCompte";

	static final String I_PCO_NUM_KEY = "pcoNum";

	static final String etatAnnule = "ANNULE";
	static final String etatValide = "VALIDE";

	static final String pcoBudgetaireOui = "O";
	static final String pcoBudgetaireNon = "N";
	static final String pcoEmargementOui = "O";
	static final String pcoEmargementNon = "N";
	static final String pcoNatureDepense = "D";
	static final String pcoNatureRecette = "R";
	static final String pcoNatureTiers = "T";

	static final String pcoJBeOui = "O";
	static final String pcoJBeNon = "N";
	static final String pcoJExerciceOui = "O";
	static final String pcoJExerciceNon = "N";
	static final String pcoJFinExerciceOui = "O";
	static final String pcoJFinExerciceNon = "N";

	static final String PCOSENSSOLDE_DEBIT = "D";
	static final String PCOSENSSOLDE_CREDIT = "C";
	static final String PCOSENSSOLDE_LES2 = "2";
	static final String PCOSENSSOLDE_ZERO = "0";

	static final String CLASSE0 = "0";
	static final String CLASSE1 = "1";
	static final String CLASSE2 = "2";
	static final String CLASSE3 = "3";
	static final String CLASSE4 = "4";
	static final String CLASSE5 = "5";
	static final String CLASSE6 = "6";
	static final String CLASSE7 = "7";
	static final String CLASSE8 = "8";
	static final String CLASSE9 = "9";

	String pcoNum();

	String pcoValidite();

	IJefyAdminExercice toExercice();
}
