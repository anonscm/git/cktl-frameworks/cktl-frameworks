package org.cocktail.fwkcktlcompta.common.entities;

import com.webobjects.foundation.NSArray;

public interface IGrhumFournis {

	String I_TO_PERSONNE_KEY = "toPersonne";

	String fouCode();
	String fouType();
	NSArray toRibsValide();

	String fouValide();

}
