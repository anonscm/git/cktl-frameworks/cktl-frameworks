package org.cocktail.fwkcktlcompta.common.entities;

public interface IGrhumBanque {

	String bic();

	String domiciliation();

	void setBic(String value);

}
