package org.cocktail.fwkcktlcompta.common.entities;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public interface IEmargement {
	static String I_EMA_NUMERO_KEY = "emaNumero";
	static String EMA_ETAT_VALIDE = "VALIDE";
	static String EMA_ETAT_ANNULE = "ANNULE";
	static Object SORT_EMA_NUMERO_ASC = EOSortOrdering.sortOrderingWithKey(IEmargement.I_EMA_NUMERO_KEY, EOSortOrdering.CompareAscending);;

	Integer emaNumero();

	IJefyAdminExercice toExercice();

	NSArray toEmargementDetails();

}
