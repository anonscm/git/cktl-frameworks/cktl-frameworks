package org.cocktail.fwkcktlcompta.common.echeancier.helpers;

import org.cocktail.fwkcktlcompta.common.echeancier.entities.IEcheancier;
import org.cocktail.fwkcktlcompta.common.echeancier.entities.IPrelevement;


public class EcheancierHelper {
	private static EcheancierHelper sharedInstance = new EcheancierHelper();

	public static EcheancierHelper getSharedInstance() {
		return sharedInstance;
	}

	/**
	 * Met les prelevement de l'echeancier a ATTENTE.
	 */
	public void validerEcheancier(IEcheancier echeancier) throws Exception {
		for (int i = 0; i < echeancier.prelevements().count(); i++) {
			IPrelevement prelev = (IPrelevement) echeancier.prelevements().objectAtIndex(i);
			if (!IPrelevement.ETAT_INVALIDE.equals(prelev.prelevEtatMaracuja())) {
				throw new Exception("Au moins un des prélèvements n'est pas à l'état INVALIDE");
			}
			prelev.setPrelevEtatMaracuja(IPrelevement.ETAT_ATTENTE);
		}
		echeancier.setEtatPrelevement(IEcheancier.ETAT_PRELEVEMENT_VALIDE);
	}
}
