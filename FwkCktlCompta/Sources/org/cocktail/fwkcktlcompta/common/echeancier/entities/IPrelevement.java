package org.cocktail.fwkcktlcompta.common.echeancier.entities;

public interface IPrelevement {

	public static final String ETAT_INVALIDE = "INVALIDE";
	public static final String ETAT_ATTENTE = "ATTENTE";
	public static final String ETAT_PRELEVE = "PRELEVE";
	public static final String ETAT_CONFIRME = "CONFIRME";
	public static final String ETAT_REJETE = "REJETE";
	public static final String ETAT_SUPPRIME = "SUPPRIME";

	public static final String PRELEVMONTANTKEY = "prelevMontant";
	public static final String PRELEVEMENTNUMANDTOTAL_KEY = "prelevementNumAndTotal";
	public static final String ECHEANCIER_MONTANT_TOTAL_KEY = "echeancierMontantTotal";
	public static final String CLIENTNOMANDPRENOM_KEY = "clientNomAndPrenom";
	public static final String DATEPRELEVEMENTCORRIGEE_KEY = "datePrelevementCorrigee";

	String prelevEtatMaracuja();

	void setPrelevEtatMaracuja(String etatAttente);

}
