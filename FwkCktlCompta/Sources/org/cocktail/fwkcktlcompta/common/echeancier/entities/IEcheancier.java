package org.cocktail.fwkcktlcompta.common.echeancier.entities;

import com.webobjects.foundation.NSArray;

public interface IEcheancier {

	public static final String ETAT_PRELEVEMENT_VALIDE = "V";;

	public NSArray prelevements();

	public void setEtatPrelevement(String etatPrelevementValide);

}
