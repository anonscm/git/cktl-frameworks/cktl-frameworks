package org.cocktail.fwkcktlcompta.common;

import java.net.URL;
import java.net.URLClassLoader;


public class FwkCktlComptaUtil {

	private static FwkCktlComptaUtil sharedInstance = new FwkCktlComptaUtil();

	public static FwkCktlComptaUtil sharedInstance() {
		return sharedInstance;
	}
	public String getClasspathString() {
		StringBuffer classpath = new StringBuffer();
		ClassLoader applicationClassLoader = this.getClass().getClassLoader();
		if (applicationClassLoader == null) {
			applicationClassLoader = ClassLoader.getSystemClassLoader();
		}
		URL[] urls = ((URLClassLoader) applicationClassLoader).getURLs();
		for (int i = 0; i < urls.length; i++) {
			classpath.append(urls[i].getFile()).append("\r\n");
		}

		return classpath.toString();
	}
}
