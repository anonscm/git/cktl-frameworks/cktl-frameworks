package org.cocktail.fwkcktlcompta.common.sepasdd.remises;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.cocktail.fwkcktlcompta.common.IFwkCktlComptaParam;
import org.cocktail.fwkcktlcompta.common.helpers.ParamHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.TypeOperation;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheanceHelper;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.joda.time.LocalDate;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Permet de regrouper des echeances en fonction des règles de création des remises SDD (Cf. {@link SepaSddEcheancesCombinaison})
 * 
 * @author rprin
 */
public class SepaSddEcheancesRegroupementFactory {

	/**
	 * @param lesEcheances les ISepaSddEcheance à regrouper
	 * @return une map contenant pour chaque combinaison les echeances à prendre en compte (equivalent de chaque fichier généré).
	 * @throws Exception
	 */
	@SuppressWarnings({
			"rawtypes", "unchecked"
	})
	public Map<SepaSddEcheancesCombinaison, NSArray> creerRegroupements(EOEditingContext edc, NSArray lesEcheances) throws Exception {
		//	Chaque fichier au format xml, ne peut être que mono-type de prélèvement, mono-date
		//	d’échéance, mono-créancier (nom et BIC-IBAN), mono-tiers créancier et mono-ICS.

		Map<SepaSddEcheancesCombinaison, NSArray> regroupements = new LinkedHashMap<SepaSddEcheancesCombinaison, NSArray>();

		//Déterminer le type de chaque échéance
		SepaSddEcheanceHelper.getSharedInstance().determinerEtAffecterLesTypeOperationTmp(lesEcheances);
		ArrayList<TypeOperation> lesTypeOperation = SepaSddEcheanceHelper.getSharedInstance().listerLesTypeOperationTmp(lesEcheances);
		ArrayList<String> lesDatePrevue = SepaSddEcheanceHelper.getSharedInstance().listerLesDatePrevue(lesEcheances);

		//Créer les combinaisons possibles
		ArrayList<SepaSddEcheancesCombinaison> combinaisonsPotentielles = new ArrayList<SepaSddEcheancesCombinaison>();
		Iterator<TypeOperation> typeOperationsIt = lesTypeOperation.iterator();
		while (typeOperationsIt.hasNext()) {
			ISepaSddEcheance.TypeOperation typeOperation = (ISepaSddEcheance.TypeOperation) typeOperationsIt.next();
			Iterator<String> datePrevuesIt = lesDatePrevue.iterator();
			while (datePrevuesIt.hasNext()) {
				String datePrevue = (String) datePrevuesIt.next();
				String datePrelevement = datePrevue;
				combinaisonsPotentielles.add(new SepaSddEcheancesCombinaison(typeOperation, datePrevue, datePrelevement));
			}
		}

		corrigerDatePrelevementSelonDelai(edc, combinaisonsPotentielles);
		
		
		Collections.sort(combinaisonsPotentielles, new TriSepaSddEcheancesCombinaisonDatePrelevement());

		
		
		
		//Regrouper les echeances par combinaison
		Iterator<SepaSddEcheancesCombinaison> combinaisonsIt = combinaisonsPotentielles.iterator();
		while (combinaisonsIt.hasNext()) {
			SepaSddEcheancesCombinaison combinaison = (SepaSddEcheancesCombinaison) combinaisonsIt.next();
			EOQualifier qualDatePrevue = new EOKeyValueQualifier(ISepaSddEcheance.I_D_PREVUE_KEY, EOQualifier.QualifierOperatorEqual, combinaison.getDatePrevue());
			EOQualifier qualTypeOperation = new EOKeyValueQualifier(ISepaSddEcheance.I_SDD_TYPE_OP_TMP_KEY, EOQualifier.QualifierOperatorEqual, combinaison.getTypeOperation().toString());
			NSArray echeancesPourLaCombinaison = EOQualifier.filteredArrayWithQualifier(lesEcheances, new EOAndQualifier(new NSArray(new Object[] {
					qualDatePrevue, qualTypeOperation
			})));
			if (echeancesPourLaCombinaison.count() > 0) {
				regroupements.put(combinaison, echeancesPourLaCombinaison);
			}
		}

		return regroupements;
	}

	private void corrigerDatePrelevementSelonDelai(EOEditingContext edc, ArrayList<SepaSddEcheancesCombinaison> combinaisonsPotentielles) throws Exception {
		for (SepaSddEcheancesCombinaison sepaSddEcheancesCombinaison : combinaisonsPotentielles) {
			corrigerDatePrelevementSelonDelai(edc, sepaSddEcheancesCombinaison);
		}

	}

	private void corrigerDatePrelevementSelonDelai(EOEditingContext edc, SepaSddEcheancesCombinaison sepaSddEcheancesCombinaison) throws Exception {
		Integer delai = getDelaiSelonTypeOperation(edc, sepaSddEcheancesCombinaison.getTypeOperation());
		LocalDate dateDuJour = new LocalDate();
		LocalDate datePrelevement = DateConversionUtil.sharedInstance().parseDateSilent(sepaSddEcheancesCombinaison.getDatePrelevement());
		LocalDate datePrelevementMinimum = dateDuJour.plusDays(delai);
		if (datePrelevement.isBefore(datePrelevementMinimum)) {
			sepaSddEcheancesCombinaison.setRemarques("Date d'échéance dépassée, correction de la date de prélèvement");
			sepaSddEcheancesCombinaison.setDatePrelevement(DateConversionUtil.sharedInstance().formatDateWithoutTimeISO(datePrelevementMinimum));
		}
	}
	private Integer getDelaiSelonTypeOperation(EOEditingContext edc, ISepaSddEcheance.TypeOperation typeOperation) throws Exception {
		String res = null;
		Integer delai = null;
		try {
			if (ISepaSddEcheance.TypeOperation.FRST.equals(typeOperation)) {
				res = (String) ParamHelper.getSharedInstance().getConfig(edc, IFwkCktlComptaParam.SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_FIRST);
			}
			else if (ISepaSddEcheance.TypeOperation.RCUR.equals(typeOperation)) {
				res = (String) ParamHelper.getSharedInstance().getConfig(edc, IFwkCktlComptaParam.SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_RCUR);
			}
			else if (ISepaSddEcheance.TypeOperation.OOFF.equals(typeOperation)) {
				res = (String) ParamHelper.getSharedInstance().getConfig(edc, IFwkCktlComptaParam.SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_OOFF);
			}
			else if (ISepaSddEcheance.TypeOperation.FNAL.equals(typeOperation)) {
				res = (String) ParamHelper.getSharedInstance().getConfig(edc, IFwkCktlComptaParam.SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_OOFF);
			}
			if (res != null) {
				delai = Integer.valueOf(res);
			}
		} catch (Exception e) {
			throw new Exception("Erreur lors de la récupération des paramètres de délai org.cocktail.gfc.sepasddmandat.typeoperation.nbjoursavantremise.*", e);
		}
		return delai;
	}





}
