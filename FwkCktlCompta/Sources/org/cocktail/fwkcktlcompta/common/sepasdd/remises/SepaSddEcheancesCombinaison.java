package org.cocktail.fwkcktlcompta.common.sepasdd.remises;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.TypeOperation;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;

/**
 * Représente une combinaison possible pour la génération des fichiers SEPA. Au final on a un fichier par combinaison.
 * 
 * @author rprin
 */
public class SepaSddEcheancesCombinaison implements NSKeyValueCoding {
	public static final String TYPE_OPERATION_KEY = "typeOperation";
	public static final String DATE_PREVUE_KEY = "datePrevue";
	public static final String DATE_PRELEVEMENT_KEY = "datePrelevement";
	public static final String REMARQUES_KEY = "remarques";

	private TypeOperation typeOperation;
	private String datePrevue;
	private String datePrelevement;
	private String remarques;

	public SepaSddEcheancesCombinaison(TypeOperation typeOperation, String datePrevue, String datePrelevement) {
		setDatePrevue(datePrevue);
		setTypeOperation(typeOperation);
		setDatePrelevement(datePrelevement);

	}

	public TypeOperation getTypeOperation() {
		return typeOperation;
	}

	public void setTypeOperation(TypeOperation typeOperation) {
		this.typeOperation = typeOperation;
	}

	public String getDatePrevue() {
		return datePrevue;
	}

	public void setDatePrevue(String datePrevue) {
		this.datePrevue = datePrevue;
	}



	public void takeValueForKey(Object arg0, String arg1) {
		if (DATE_PREVUE_KEY.equals(arg1)) {
			setDatePrevue((String) arg0);
		}
		else if (TYPE_OPERATION_KEY.equals(arg0)) {
			setTypeOperation((TypeOperation) arg0);
		}
		else if (DATE_PRELEVEMENT_KEY.equals(arg0)) {
			setDatePrelevement((String) arg0);
		}
	}

	public Object valueForKey(String arg0) {
		if (DATE_PREVUE_KEY.equals(arg0)) {
			return datePrevue;
		}
		else if (TYPE_OPERATION_KEY.equals(arg0)) {
			return typeOperation;
		}
		else if (DATE_PRELEVEMENT_KEY.equals(arg0)) {
			return datePrelevement;
		}
		else if (REMARQUES_KEY.equals(arg0)) {
			return remarques;
		}
		return null;
	}

	public static SepaSddEcheancesCombinaison fromString(String string) {
		NSArray parts = NSArray.componentsSeparatedByString(string, "|");
		if (parts.count() == 3) {
			SepaSddEcheancesCombinaison cominCombinaison = new SepaSddEcheancesCombinaison(ISepaSddEcheance.TypeOperation.valueOf((String) parts.objectAtIndex(0)), (String) parts.objectAtIndex(1), (String) parts.objectAtIndex(2));
			return cominCombinaison;
		}
		return null;
	}

	public String toString() {
		return getTypeOperation().toString() + "|" + getDatePrevue() + "|" + getDatePrelevement();
	}

	public String getDatePrelevement() {
		return datePrelevement;
	}

	public void setDatePrelevement(String datePrelevement) {
		this.datePrelevement = datePrelevement;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datePrelevement == null) ? 0 : datePrelevement.hashCode());
		result = prime * result + ((datePrevue == null) ? 0 : datePrevue.hashCode());
		result = prime * result + ((typeOperation == null) ? 0 : typeOperation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SepaSddEcheancesCombinaison other = (SepaSddEcheancesCombinaison) obj;
		if (datePrelevement == null) {
			if (other.datePrelevement != null)
				return false;
		}
		else if (!datePrelevement.equals(other.datePrelevement))
			return false;
		if (datePrevue == null) {
			if (other.datePrevue != null)
				return false;
		}
		else if (!datePrevue.equals(other.datePrevue))
			return false;
		if (typeOperation != other.typeOperation)
			return false;
		return true;
	}

	public void setRemarques(String string) {
		this.remarques = string;

	}

}