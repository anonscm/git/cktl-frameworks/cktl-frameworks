package org.cocktail.fwkcktlcompta.common.sepasdd.remises;

import java.util.Comparator;

import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.joda.time.LocalDate;

public class TriSepaSddEcheancesCombinaisonDatePrelevement implements Comparator<SepaSddEcheancesCombinaison> {

	public int compare(SepaSddEcheancesCombinaison combinaison1, SepaSddEcheancesCombinaison combinaison2) {
		if (combinaison1 == combinaison2) {
			return 0;
		}

		if (combinaison1.getDatePrelevement() == null && combinaison2.getDatePrelevement() == null) {
			return 0;
		}

		if (combinaison1.getDatePrelevement() == null && combinaison2 != null) {
			return -1;
		}

		if (combinaison1 != null && combinaison2.getDatePrelevement() == null) {
			return 1;
		}

		DateConversionUtil dateUtil = new DateConversionUtil();
		LocalDate datePrelement1 = dateUtil.parseDate(combinaison1.getDatePrelevement());
		LocalDate datePrelement2 = dateUtil.parseDate(combinaison2.getDatePrelevement());
		return datePrelement1.compareTo(datePrelement2);
	}

}
