package org.cocktail.fwkcktlcompta.common.sepasdd;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;

public interface ISepaSddEcheancierContrainte {

	boolean apply(ISepaSddEcheancier echeancier);

}
