package org.cocktail.fwkcktlcompta.common.sepasdd.generateurs;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.joda.time.LocalDate;

public class SepaSddGenerateurEcheancierPremiereEcheanceFixe extends SepaSddGenerateurEcheancierConstant implements IGenerateurEcheancier {

	private BigDecimal montantPremiereEcheance;

	public SepaSddGenerateurEcheancierPremiereEcheanceFixe(BigDecimal montantAPayer, Integer nbEcheances,
			LocalDate datePremiereEcheance, BigDecimal montantPremiereEcheance, Integer persId, IGrhumPersonne modificateur) {

		super(montantAPayer, nbEcheances, datePremiereEcheance, persId, modificateur);
		this.montantPremiereEcheance = montantPremiereEcheance;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean peutGenererEcheances(ISepaSddEcheancier echeancier) {
		return echeancierDefini(echeancier) && montantAPayerDefini() && montantPremiereEcheanceDefinie()
				&& datePremiereEcheanceDefinie() && montantPremiereEcheanceValide();
	}

	protected boolean montantAPayerDefini() {
		return getMontantAPayer() != null;
	}

	protected boolean nombreEcheancesDefini() {
		return getNombreEcheancesPrevues() != null;
	}

	protected boolean montantPremiereEcheanceDefinie() {
		return getMontantPremiereEcheance() != null;
	}

	protected boolean datePremiereEcheanceDefinie() {
		return getDatePremiereEcheance() != null;
	}

	protected boolean montantPremiereEcheanceValide() {
		return getMontantPremiereEcheance().compareTo(getMontantAPayer()) <= 0;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ISepaSddEcheance> genererEcheances(ISepaSddEcheancier echeancier) {
		if (!peutGenererEcheances(echeancier)) {
			return Collections.emptyList();
		}

		if (isEcheanceUnique(getNombreEcheancesPrevues())) {
			return Collections.singletonList(
					creerEcheance(echeancier, getDatePremiereEcheance(), getMontantPremiereEcheance(), getModificateur()));
		}

		return genererMultiplesEcheances(echeancier);
	}

	protected boolean isEcheanceUnique(Integer nbEcheances) {
		return nbEcheances == null || nbEcheances.intValue() <= 1;
	}

	protected List<ISepaSddEcheance> genererMultiplesEcheances(ISepaSddEcheancier echeancier) {
		List<ISepaSddEcheance> echeances = new ArrayList<ISepaSddEcheance>();

		Integer nbEcheances = getNombreEcheancesPrevues();
		BigDecimal montantAPayer = getMontantAPayer();
		BigDecimal montantPremiereEcheanceLocal = getMontantPremiereEcheance();
		BigDecimal montantAutrescheancesARepartir = montantAPayer.subtract(montantPremiereEcheanceLocal);
		BigDecimal montantAutrescheancesUnitaireConstant = calculerMontantUnitaireConstant(montantAutrescheancesARepartir, nbEcheances - 1);
		LocalDate datePremiereEcheance = getDatePremiereEcheance();

		IGenerateurDatesEcheances localGenerateurDatesEcheances = generateurDatesEcheances(datePremiereEcheance, nbEcheances);
		LocalDate dateEcheance = datePremiereEcheance;

		echeances.add(creerEcheance(echeancier, dateEcheance, montantPremiereEcheanceLocal, getModificateur()));

		BigDecimal sommeEcheancesSansCompterLaDerniere = montantPremiereEcheanceLocal;
		dateEcheance = localGenerateurDatesEcheances.genererProchaineDate(dateEcheance);
		for (int idx = 1; idx < nbEcheances; idx++) {
			BigDecimal montantEcheance;

			// traite la derniere echeance
			if (idx == (nbEcheances.intValue() - 1)) {
				montantEcheance = montantAPayer.subtract(sommeEcheancesSansCompterLaDerniere)
						.setScale(getPrecisionMonetaireAttendue(), RoundingMode.HALF_UP);
			} else {
				montantEcheance = montantAutrescheancesUnitaireConstant;
			}
			echeances.add(creerEcheance(echeancier, dateEcheance, montantEcheance, getModificateur()));
			dateEcheance = localGenerateurDatesEcheances.genererProchaineDate(dateEcheance);
			sommeEcheancesSansCompterLaDerniere = sommeEcheancesSansCompterLaDerniere.add(montantEcheance);
		}

		return echeances;
	}

	public BigDecimal getMontantPremiereEcheance() {
		return montantPremiereEcheance;
	}

	public void setMontantPremiereEcheance(BigDecimal montantPremiereEcheance) {
		this.montantPremiereEcheance = montantPremiereEcheance;
	}
}
