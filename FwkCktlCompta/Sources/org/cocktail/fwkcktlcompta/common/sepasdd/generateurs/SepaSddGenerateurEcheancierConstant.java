package org.cocktail.fwkcktlcompta.common.sepasdd.generateurs;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.exceptions.InvalidEcheancierException;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import com.webobjects.eocontrol.EOEditingContext;

public class SepaSddGenerateurEcheancierConstant implements IGenerateurEcheancier {

	private int precisionMonetaireAttendue;
	private BigDecimal montantAPayer;
	private LocalDate datePremiereEcheance;
	private Integer nombreEcheancesPrevues;
	private Integer persId;
	private IGenerateurDatesEcheances generateurDatesEcheances;
	private IGrhumPersonne modificateur;

	public SepaSddGenerateurEcheancierConstant(BigDecimal montantAPayer, Integer nombreEcheancesPrevues, LocalDate datePremiereEcheance, Integer persId, IGrhumPersonne modificateur) {
		this.montantAPayer = montantAPayer;
		this.nombreEcheancesPrevues = nombreEcheancesPrevues;
		this.datePremiereEcheance = datePremiereEcheance;
		this.persId = persId;
		this.precisionMonetaireAttendue = PRECISION_MONETAIRE_PAR_DEFAUT;
		this.modificateur = modificateur;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean peutGenererEcheances(ISepaSddEcheancier echeancier) {
		return echeancierDefini(echeancier) && montantAPayerDefini() && datePremiereEcheanceDefinie();
	}

	protected boolean echeancierDefini(ISepaSddEcheancier echeancier) {
		return echeancier != null;
	}

	protected boolean montantAPayerDefini() {
		return montantAPayer != null;
	}

	protected boolean datePremiereEcheanceDefinie() {
		return datePremiereEcheance != null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ISepaSddEcheance> genererEcheances(ISepaSddEcheancier echeancier) {
		if (!peutGenererEcheances(echeancier)) {
			return Collections.emptyList();
		}

		BigDecimal montantTotal = montantAPayer;
		Integer nbEcheances = nombreEcheancesPrevues;

		List<ISepaSddEcheance> echeances = new ArrayList<ISepaSddEcheance>();
		if (nbEcheances == null) {
			ISepaSddEcheance echeanceUnique = creerEcheance(echeancier, datePremiereEcheance, montantAPayer, modificateur);
			echeances.add(echeanceUnique);
			return echeances;
		}

		BigDecimal montantUnitaireConstant = calculerMontantUnitaireConstant(montantTotal, nbEcheances);
		BigDecimal sommeEcheancesSansCompterLaDerniere = montantUnitaireConstant.multiply(BigDecimal.valueOf(nbEcheances - 1));
		BigDecimal montantEcheanceAvecRattrapage = montantTotal.subtract(sommeEcheancesSansCompterLaDerniere)
				.setScale(getPrecisionMonetaireAttendue(), RoundingMode.HALF_UP);

		IGenerateurDatesEcheances localGenerateurDatesEcheances = generateurDatesEcheances(datePremiereEcheance, nbEcheances);
		LocalDate dateEcheance = datePremiereEcheance;
		for (int idx = 0; idx < nbEcheances; idx++) {
			BigDecimal montantEcheance = montantEcheanceEnFonctionDeSaPosition(montantUnitaireConstant, montantEcheanceAvecRattrapage, idx, nbEcheances.intValue());
			echeances.add(creerEcheance(echeancier, dateEcheance, montantEcheance, modificateur));
			dateEcheance = localGenerateurDatesEcheances.genererProchaineDate(dateEcheance);
		}

		return echeances;
	}

	public void validerEcheancier(ISepaSddEcheancier echeancier) throws InvalidEcheancierException {
		return;
	}

	protected BigDecimal montantEcheanceEnFonctionDeSaPosition(BigDecimal montantUnitaireConstant, BigDecimal montantEcheanceAvecRattrapage, int position, int nbEcheances) {
		if (montantEcheanceAvecRattrapage.compareTo(montantUnitaireConstant) >= 0 && position == 0) {
			return montantEcheanceAvecRattrapage;
		} else if (montantEcheanceAvecRattrapage.compareTo(montantUnitaireConstant) < 0 && position == nbEcheances - 1) {
			return montantEcheanceAvecRattrapage;
		}

		return montantUnitaireConstant;
	}

	protected BigDecimal calculerMontantUnitaireConstant(BigDecimal montantTotal, Integer nbEcheances) {
		if (nbEcheances.intValue() == 0) {
			return BigDecimal.ZERO;
		}
		return montantTotal.divide(BigDecimal.valueOf(nbEcheances), getPrecisionMonetaireAttendue(), RoundingMode.HALF_UP);
	}

	public ISepaSddEcheance creerEcheance(ISepaSddEcheancier echeancier, LocalDate dateEcheance, BigDecimal montantEcheance, IGrhumPersonne modificateur) {
		return creerEcheance(null, echeancier, dateEcheance, montantEcheance, modificateur);
	}
	
	
	public ISepaSddEcheance creerEcheance(EOEditingContext editingContext, ISepaSddEcheancier echeancier, LocalDate dateEcheance, BigDecimal montantEcheance, IGrhumPersonne modificateur) {
		
		ISepaSddEcheance echeance;
		if (editingContext == null) {
			echeance = echeancier.creerEcheance();
		} else {
			echeance = echeancier.creerEcheance(editingContext);
		}
		mettreAJourEcheance(echeancier, dateEcheance, montantEcheance,
				modificateur, echeance);

		return echeance;
	}

	private void mettreAJourEcheance(ISepaSddEcheancier echeancier,
			LocalDate dateEcheance, BigDecimal montantEcheance,
			IGrhumPersonne modificateur, ISepaSddEcheance echeance) {
		
		echeance.setEcheancier(echeancier);
		echeance.setCreateurId(getPersId());
		echeance.setDerniereModificationPar(getPersId());
		echeance.setDateCreation(DateConversionUtil.sharedInstance().formatDateWithTimeISO(new LocalDateTime()));
		echeance.setDateModification(DateConversionUtil.sharedInstance().formatDateWithTimeISO(new LocalDateTime()));
		echeance.setEtatEnum(Etat.ATTENTE);
		echeance.setDatePrevue(DateConversionUtil.sharedInstance().formatDateWithoutTimeISO(dateEcheance));
		echeance.setToModificateurPersonne(modificateur);

		BigDecimal montantEcheanceValidee = BigDecimal.ZERO;
		if (montantEcheance != null) {
			montantEcheanceValidee = montantEcheance;
		}
		echeance.setMontantAPayer(montantEcheanceValidee.setScale(getPrecisionMonetaireAttendue(), RoundingMode.HALF_UP));

		//	SepaSddEcheanceHelper.getSharedInstance().determineEtAffecteTypeOperation(echeance, false);
	}

	protected IGenerateurDatesEcheances generateurDatesEcheances(LocalDate datePremiereEcheance, Integer nbEcheances) {
		if (getGenerateurDatesEcheances() != null) {
			return getGenerateurDatesEcheances();
		}
		return new SepaSddGenerateurDatesEcheancesMensuels(datePremiereEcheance, nbEcheances);
	}

	public void setPrecisionMonetaireAttendue(int nbDecimales) {
		this.precisionMonetaireAttendue = nbDecimales;
	}

	public int getPrecisionMonetaireAttendue() {
		return precisionMonetaireAttendue;
	}

	public Integer getPersId() {
		return persId;
	}

	public IGenerateurDatesEcheances getGenerateurDatesEcheances() {
		return generateurDatesEcheances;
	}

	public void setGenerateurDatesEcheances(IGenerateurDatesEcheances generateurDatesEcheances) {
		this.generateurDatesEcheances = generateurDatesEcheances;
	}

	public IGrhumPersonne getModificateur() {
		return modificateur;
	}

	public BigDecimal getMontantAPayer() {
		return montantAPayer;
	}

	public void setMontantAPayer(BigDecimal montantAPayer) {
		this.montantAPayer = montantAPayer;
	}

	public LocalDate getDatePremiereEcheance() {
		return datePremiereEcheance;
	}

	public void setDatePremiereEcheance(LocalDate datePremiereEcheance) {
		this.datePremiereEcheance = datePremiereEcheance;
	}

	public Integer getNombreEcheancesPrevues() {
		return nombreEcheancesPrevues;
	}

	public void setNombreEcheancesPrevues(Integer nombreEcheancesPrevues) {
		this.nombreEcheancesPrevues = nombreEcheancesPrevues;
	}

}
