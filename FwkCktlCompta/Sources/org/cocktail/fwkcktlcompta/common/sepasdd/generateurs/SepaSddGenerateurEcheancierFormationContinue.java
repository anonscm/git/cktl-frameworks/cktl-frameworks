package org.cocktail.fwkcktlcompta.common.sepasdd.generateurs;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.exceptions.InvalidEcheancierException;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcompta.common.util.ISODateTextToLocalDateFormat;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class SepaSddGenerateurEcheancierFormationContinue extends SepaSddGenerateurEcheancierConstant implements IGenerateurEcheancier {

	private static final BigDecimal PAIEMENT_3X = BigDecimal.valueOf(3d);
	private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormat.forPattern(ISODateTextToLocalDateFormat.FRENCH_SHORT_DATE_PATTERN);

	private LocalDate contraintesDateDebutEcheance;
	private LocalDate contraintesDateFinEcheance;

	public SepaSddGenerateurEcheancierFormationContinue(BigDecimal montantAPayer, LocalDate datePremiereEcheance, Integer nombreEcheancesPrevues, Date contraintesDateDebutEcheance, Date contraintesDateFinEcheance, Integer persId, IGrhumPersonne prsModificateur) {
		super(montantAPayer, nombreEcheancesPrevues, datePremiereEcheance, persId, prsModificateur);
		initContrainteDatePremiereEcheance(contraintesDateDebutEcheance);
		initContrainteDateDerniereEcheance(contraintesDateFinEcheance);
	}

	protected void initContrainteDatePremiereEcheance(Date contraintesDateDebutEcheance) {
		if (contraintesDateDebutEcheance == null) {
			this.contraintesDateDebutEcheance = LocalDate.now();
		} else {
			this.contraintesDateDebutEcheance = new LocalDate(contraintesDateDebutEcheance.getTime());
		}
	}

	protected void initContrainteDateDerniereEcheance(Date contraintesDateFinEcheance) {
		if (contraintesDateFinEcheance != null) {
			this.contraintesDateFinEcheance = new LocalDate(contraintesDateFinEcheance.getTime());
		}
	}

	public List<ISepaSddEcheance> genererEcheances(ISepaSddEcheancier echeancier) {
		if (!peutGenererEcheances(echeancier)) {
			return Collections.emptyList();
		}

		List<ISepaSddEcheance> echeances = new ArrayList<ISepaSddEcheance>();

		if (getNombreEcheancesPrevues() == null) {
			ISepaSddEcheance echeanceUnique = creerEcheance(echeancier, getDatePremiereEcheance(), getMontantAPayer(), getModificateur());
			echeances.add(echeanceUnique);
			return echeances;
		}

		echeances = genererMultiplesEcheances(echeancier);

		return echeances;
	}

	public void validerEcheancier(ISepaSddEcheancier echeancier) throws InvalidEcheancierException {
		if (echeancier == null || echeancier.echeances() == null) {
			return;
		}

		List<ISepaSddEcheance> echeances = echeancier.echeances();
		int nbEcheancesGenerees = echeances.size();
		if (nbEcheancesGenerees == 0) {
			return;
		}

		//FIXME voir si besoin appel à SepaSddObjectsRule.validerEcheancier

		ISepaSddEcheance premiereEcheance = echeances.get(0);
		LocalDate datePremiereEcheance = DateConversionUtil.sharedInstance().parseDate(premiereEcheance.dPrevue());
		if (contraintesDateDebutEcheance != null && datePremiereEcheance.isBefore(contraintesDateDebutEcheance)) {
			throw new InvalidEcheancierException(
					"Il existe une contrainte sur la date de première échéance : \nelle ne doit pas être antérieure au "
					+ DATETIME_FORMATTER.print(contraintesDateDebutEcheance) + ".\n\n"
					+ "Veuillez corriger la date de première échéance, svp...");
		}

		ISepaSddEcheance derniereEcheances = echeances.get(nbEcheancesGenerees - 1);
		LocalDate dateDerniereEcheance = DateConversionUtil.sharedInstance().parseDate(derniereEcheances.dPrevue());
		if (contraintesDateFinEcheance != null && dateDerniereEcheance.isAfter(contraintesDateFinEcheance)) {
			throw new InvalidEcheancierException(
					"Il existe une contrainte sur la date de dernière échéance : \nelle ne doit pas être postérieure au "
					+ DATETIME_FORMATTER.print(contraintesDateFinEcheance) + ".\n"
					+ "Veuillez corriger le nombre d'échéances, svp...");
		}
	}

	protected List<ISepaSddEcheance> genererMultiplesEcheances(ISepaSddEcheancier echeancier) {
		List<ISepaSddEcheance> echeances = new ArrayList<ISepaSddEcheance>();

		Integer nbEcheances = getNombreEcheancesPrevues();
		BigDecimal montantAPayer = getMontantAPayer();
		BigDecimal montantPremiereEcheance = calculerMontantPremiereEcheance(montantAPayer);
		BigDecimal montantAutrescheancesARepartir = montantAPayer.subtract(montantPremiereEcheance);
		BigDecimal montantAutrescheancesUnitaireConstant = calculerMontantUnitaireConstant(montantAutrescheancesARepartir, nbEcheances - 1);

		String datePremiereEcheanceAsString = echeancier.datePremiereEcheance();
		LocalDate datePremiereEcheance = DateConversionUtil.sharedInstance().parseDate(datePremiereEcheanceAsString);
		IGenerateurDatesEcheances localGenerateurDatesEcheances = generateurDatesEcheances(datePremiereEcheance, nbEcheances);
		LocalDate dateEcheance = datePremiereEcheance;

		echeances.add(creerEcheance(echeancier, dateEcheance, montantPremiereEcheance, getModificateur()));

		BigDecimal sommeEcheancesSansCompterLaDerniere = montantPremiereEcheance;
		dateEcheance = localGenerateurDatesEcheances.genererProchaineDate(dateEcheance);
		for (int idx = 1; idx < nbEcheances; idx++) {
			BigDecimal montantEcheance;

			// traite la derniere echeance
			if (idx == (nbEcheances.intValue() - 1)) {
				montantEcheance = montantAPayer.subtract(sommeEcheancesSansCompterLaDerniere)
						.setScale(getPrecisionMonetaireAttendue(), RoundingMode.HALF_UP);
			} else {
				montantEcheance = montantAutrescheancesUnitaireConstant;
			}
			echeances.add(creerEcheance(echeancier, dateEcheance, montantEcheance, getModificateur()));
			dateEcheance = localGenerateurDatesEcheances.genererProchaineDate(dateEcheance);
			sommeEcheancesSansCompterLaDerniere = sommeEcheancesSansCompterLaDerniere.add(montantEcheance);
		}

		return echeances;
	}

	protected BigDecimal calculerMontantPremiereEcheance(BigDecimal montantTotal) {
		return montantTotal.divide(PAIEMENT_3X, getPrecisionMonetaireAttendue(), RoundingMode.HALF_UP);
	}
}
