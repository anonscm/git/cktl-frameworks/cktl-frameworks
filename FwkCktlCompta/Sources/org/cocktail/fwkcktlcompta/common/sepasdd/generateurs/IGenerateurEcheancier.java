package org.cocktail.fwkcktlcompta.common.sepasdd.generateurs;

import java.util.List;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.exceptions.InvalidEcheancierException;

public interface IGenerateurEcheancier {

	/** Nombre de decimales par défaut. */
	int PRECISION_MONETAIRE_PAR_DEFAUT = 2;

	/**
	 * Definit la precision (nombre de décimales) à utiliser lors des calculs des montants des echeances.
	 * @param nbDecimales nombre de décimales des echeances.
	 */
	void setPrecisionMonetaireAttendue(int nbDecimales);

	/**
	 * Permet de verifier que l'ensemble des informations requises à la generation des echeances est present.
	 * @param echeancier echeancier sur lequel les echeances doivent etre creees.
	 * @return true si le generateur peut generer les echeances ; false sinon.
	 */
	boolean peutGenererEcheances(ISepaSddEcheancier echeancier);

	/**
	 * Genere une liste d'echeances.
	 * @param echeancier echeancier sur lequel les echeances doivent etre creees.
	 * @return la liste des echeances.
	 */
	List<ISepaSddEcheance> genererEcheances(ISepaSddEcheancier echeancier);

	/**
	 * Valide les données de l'echeancier et de ses echeances.
	 * Cette méthode devrait p.e etre dans un autre objet.
	 * @param echeancier echeancier a valider.
	 * @throws InvalidEcheancierException si l'echeancier est invalide.
	 */
	void validerEcheancier(ISepaSddEcheancier echeancier) throws InvalidEcheancierException;
}
