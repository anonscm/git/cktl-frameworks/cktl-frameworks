package org.cocktail.fwkcktlcompta.common.sepasdd.generateurs;

import org.joda.time.LocalDate;

public interface IGenerateurDatesEcheances {

	boolean peutGenererDatesEcheances();
	LocalDate genererProchaineDate(LocalDate dateEcheancesPrecedentes);
}
