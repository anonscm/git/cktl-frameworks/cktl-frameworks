package org.cocktail.fwkcktlcompta.common.sepasdd.generateurs;

import java.util.List;

import org.joda.time.LocalDate;

public class SepaSddGenerateurDatesEcheancesMensuels implements IGenerateurDatesEcheances {

	private static final int ECART_UN_MOIS = 1;

	private LocalDate datePremiereEcheance;
	private Integer nbEcheances;

	public SepaSddGenerateurDatesEcheancesMensuels(LocalDate datePremiereEcheance, Integer nbEcheances) {
		this.datePremiereEcheance = datePremiereEcheance;
		this.nbEcheances = nbEcheances;
	}

	public boolean peutGenererDatesEcheances() {
		return datePremiereEcheance != null && nbEcheances != null && nbEcheances.intValue() >= 1;
	}

	protected void genererDatesEcheances(List<LocalDate> echeances, LocalDate datePremiereEcheance, int nbEcheances) {
		if (echeances == null) {
			return;
		}

		if (echeances.isEmpty()) {
			echeances.add(datePremiereEcheance);
		}

	    if (nbEcheances <= 1) {
	        return;
	    }

		LocalDate dateDerniereEcheance = echeances.get(echeances.size() - 1);
		LocalDate dateProchaineEcheance = genererProchaineDate(dateDerniereEcheance);
		echeances.add(dateProchaineEcheance);
		genererDatesEcheances(echeances, datePremiereEcheance, nbEcheances - 1);
	}

	public LocalDate genererProchaineDate(LocalDate dateEcheancesPrecedentes) {
		return dateEcheancesPrecedentes.plusMonths(ECART_UN_MOIS);
	}

}
