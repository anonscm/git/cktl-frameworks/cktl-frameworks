package org.cocktail.fwkcktlcompta.common.sepasdd.rules;

import org.cocktail.fwkcktlcompta.common.IFwkCktlComptaParam;
import org.cocktail.fwkcktlcompta.common.helpers.ParamHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheanceHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHelper;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.joda.time.LocalDate;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class SepaSddMandatRule {

	public static final String ERR_MANDAT_SUPPRESSION_IMPOSSIBLE_CAR_ECHEANCIERS = "Ce mandat ne peut être supprimé car des échéanciers existent.";

	private static Integer nbMoisAvantCaducite = null;
	private static SepaSddMandatRule sharedInstance = new SepaSddMandatRule();

	public static SepaSddMandatRule getSharedInstance() {
		return sharedInstance;
	}

	/**
	 * Vérifie que le mandat est correctement renseigné.
	 *
	 * @param mandat Le mandat SEPA à tester
	 * @throws NSValidation.ValidationException Lorsqu'une erreur est détectée
	 */
	public void validateSepaSddMandat(ISepaSddMandat mandat) throws NSValidation.ValidationException {
		if (mandat.dMandatCreation() == null) {
			throw new NSValidation.ValidationException("La date du mandat est obligatoire");
		} else if (mandat.toSepaSddParam() == null) {
			throw new NSValidation.ValidationException("L'identification du compte créancier est obligatoire");
		} else if (!"VALIDE".equals(mandat.toSepaSddParam().etat())) {
			throw new NSValidation.ValidationException("Le compte créancier n'est pas valide");
		} else if (mandat.toCreancierPersonne() == null) {
			throw new NSValidation.ValidationException("Le créancier est obligatoire");
		} else if (mandat.toDebiteurPersonne() == null) {
			throw new NSValidation.ValidationException("Le débiteur est obligatoire");
		} else if (mandat.toDebiteurAdresse() == null) {
		    throw new NSValidation.ValidationException("L'adresse du débiteur est obligatoire");
		} else if (mandat.toDebiteurRib() == null) {
		    throw new NSValidation.ValidationException("Les coordonnées bancaires du débiteur sont obligatoires");
		}

		//Verifier que les dates sont au bon format
		DateConversionUtil dcu = new DateConversionUtil();
		if (!dcu.isValidDateISO(mandat.dMandatCreation())) {
			throw new NSValidation.ValidationException("La date du mandat n'est pas au bon format " + DateConversionUtil.DATE_WITHOUT_TIME_FORMATTER.toString());
		} else if (mandat.dMandatSignature() != null && !dcu.isValidDateISO(mandat.dMandatSignature())) {
			throw new NSValidation.ValidationException("La date de signature n'est pas au bon format " + DateConversionUtil.DATE_WITHOUT_TIME_FORMATTER.toString());
		} else if (mandat.refAppliCreation() == null) {
			throw new NSValidation.ValidationException("La référence de l'application à l'origine de la création du mandat SEPA SDD est obligatoire.");
		}

		if (mandat.dMandatSignature() == null && SepaSddMandatHelper.getSharedInstance().isValide(mandat)) {
			throw new NSValidation.ValidationException("Une mandat sans date de signature ne peut pas être valide.");
		}
	}

	public void checkMandatSupprimable(ISepaSddMandat mandat) throws NSValidation.ValidationException {
		if (!SepaSddMandatHelper.getSharedInstance().isSuppressionPossible(mandat)) {
			throw new NSValidation.ValidationException(ERR_MANDAT_SUPPRESSION_IMPOSSIBLE_CAR_ECHEANCIERS);
		}
	}

	public Boolean isPlusieursEcheanciers(ISepaSddMandat mandat) {
		return mandat.echeanciers().size() > 1;
	}

	public Boolean isMandatPonctuelEtPlusieursEcheanciers(ISepaSddMandat mandat) {
		return !mandat.isRecurrent() && isPlusieursEcheanciers(mandat);
	}

	public void checkMandatPonctuelEtPlusieursEcheanciers(ISepaSddMandat mandat) {
		if (isMandatPonctuelEtPlusieursEcheanciers(mandat)) {
			throw new NSValidation.ValidationException("Un mandat de type Ponctuel be peut avoir plusieurs échéanciers.");
		}
	}

	public Boolean isMandatRecurrent(ISepaSddMandat mandat) {
		return ISepaSddMandat.TypePrelevement.R.toString().equals(mandat.cTypePrelevement());
	}

	public void checkMandatValidable(ISepaSddMandat mandat) {
		if (!isDateSignatureRemplie(mandat)) {
			throw new NSValidation.ValidationException("Le mandat ne peut pas être validé car la date de signature n'est pas renseignée.");
		}
	}

	private boolean isDateSignatureRemplie(ISepaSddMandat mandat) {
		return mandat.dMandatSignature() != null;
	}

	public void checkCoherenceEcheancesTypeOperations(ISepaSddMandat mandat) {
		NSArray echeances = SepaSddMandatHelper.getSharedInstance().getEcheancesTrieesParEtatDifferentDe(mandat, ISepaSddEcheance.Etat.ANNULE, false);
		if (!mandat.isRecurrent()) {
			if ((echeances.count() == 1) && !ISepaSddEcheance.TypeOperation.OOFF.toString().equals(((ISepaSddEcheance) echeances.objectAtIndex(0)).sddTypeOp())) {
				throw new NSValidation.ValidationException("l'échéance d'un mandat de type ponctuel doit être du type OOFF");
			}
		}
		else {
			if (SepaSddEcheanceHelper.getSharedInstance().isPlusieursEcheancesDeType(echeances, ISepaSddEcheance.TypeOperation.FRST)) {
				throw new NSValidation.ValidationException("Une seule échéance par mandat peut avoir le type FRST.");
			}
			else if (SepaSddEcheanceHelper.getSharedInstance().isPlusieursEcheancesSurMemeDateQueOperationDuType(echeances, ISepaSddEcheance.TypeOperation.FRST)) {
				throw new NSValidation.ValidationException("Plusieurs échéances d'un même mandat ne peuvent être prélevées à la même date que la première opération (FRST)");
			}
			else if (SepaSddEcheanceHelper.getSharedInstance().isPlusieursEcheancesDeType(echeances, ISepaSddEcheance.TypeOperation.FNAL)) {
				throw new NSValidation.ValidationException("Une seule échéance par mandat peut avoir le type FNAL.");
			}
		}
	}

	public void checkMandatValide(ISepaSddMandat mandat) {
		if (!SepaSddMandatHelper.getSharedInstance().isValide(mandat)) {
			throw new NSValidation.ValidationException("Le mandat SDD n'est pas valide : " + SepaSddMandatHelper.getSharedInstance().getLibelleMandatComplet(mandat));
		}

	}

	/**
	 * @param mandat Le mandat
	 * @param dateIso Une date sous la forme yyyy-mm-dd
	 * @return True si le mandat est caduc à la date spécifiée par dateIso(aucune échéance présentée au prélèvement depuis le nombre de mois indiqués
	 *         dans le parametre org.cocktail.gfc.sepasdd.nombredemoisavantcaducitemandat)
	 * @throws Exception
	 */
	public Boolean isMandatCaduc(EOEditingContext edc, ISepaSddMandat mandat, String dateIso) throws Exception {
		NSArray echeances = SepaSddMandatHelper.getSharedInstance().getEcheancesPreleveesOuConfirmeesOuRejeteesTrieesParDateEcheanceASC(mandat, false);
		if (mandat.dMandatSignature() == null) {
			return Boolean.FALSE;
		}
		LocalDate derniereDateMandat = DateConversionUtil.sharedInstance().parseDate(mandat.dMandatSignature());
		if (echeances.count() > 0) {
			ISepaSddEcheance lastEcheance = (ISepaSddEcheance) echeances.objectAtIndex(echeances.count() - 1);
			if (lastEcheance.dPreleve() == null) {
				derniereDateMandat = DateConversionUtil.sharedInstance().parseDate(lastEcheance.dPrevue());
			}
			else {
				derniereDateMandat = DateConversionUtil.sharedInstance().parseDate(lastEcheance.dPreleve());
			}
		}

		LocalDate dateTest = DateConversionUtil.sharedInstance().parseDate(dateIso);
		if (nbMoisAvantCaducite == null) {
			nbMoisAvantCaducite = Integer.valueOf((String) ParamHelper.getSharedInstance().getConfig(edc, IFwkCktlComptaParam.SEPASDDMANDAT_NB_MOIS_AVANT_CADUCITE));
		}
		if (dateTest.isAfter(derniereDateMandat.plusMonths(nbMoisAvantCaducite))) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;

	}




}
