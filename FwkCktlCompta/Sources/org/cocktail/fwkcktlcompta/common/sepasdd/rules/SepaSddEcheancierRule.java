package org.cocktail.fwkcktlcompta.common.sepasdd.rules;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.common.helpers.EcritureDetailHelper;
import org.cocktail.fwkcktlcompta.common.rules.EcritureDetailRule;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcompta.common.util.CktlEOControlUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSValidation;

@SuppressWarnings("rawtypes")
public class SepaSddEcheancierRule {
	private static SepaSddEcheancierRule sharedInstance = new SepaSddEcheancierRule();
	private static final Logger LOG = Logger.getLogger(SepaSddEcheancierRule.class);

	public static SepaSddEcheancierRule getSharedInstance() {
		return sharedInstance;
	}

	/**
	 * @param echeancier l'echeancier à tester
	 * @throws NSValidation.ValidationException Lorsqu'une erreur est détectée
	 */
	public void validateSepaSddEcheancier(ISepaSddEcheancier echeancier) throws NSValidation.ValidationException {
		if (echeancier.toSepaSddOrigine() == null) {
			throw new NSValidation.ValidationException("L'origine de l'échéancier est obligatoire.");
		}
		if (echeancier.mandat() == null) {
			throw new NSValidation.ValidationException("Le mandat est obligatoire");
		}

		checkOrigineDebiteurDifferentDeMandatTiersDebiteur(echeancier);
		checkOrigineDebiteurDifferentDeMandatDebiteur(echeancier);
		checkOrigineUtiliseePourAutresEcheanciers(echeancier);

		if (!echeancier.mandat().isRecurrent()) {
			checkEcheanceNonAnnuleeDeTypeOOF(echeancier);
		}

		checkChangementOrigineSiEcheancesNonAnnuleeExiste(echeancier);

		try {
			StringBuilder msg = new StringBuilder();
			boolean res = isPremiereEcheanceDateEcheanceAnterieureADatePremiereEcheanceSurMemeMandatPlusDelai(echeancier.editingContext(), echeancier, msg);
			if (res) {
				throw new NSValidation.ValidationException(msg.toString());
			}
		} catch (Exception e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage());
			}
		}

	}

	public Boolean isOrigineDebiteurDifferentDeMandatTiersDebiteur(ISepaSddEcheancier echeancier) {
		ISepaSddMandat mandat = echeancier.mandat();
		ISepaSddOrigineEntity origineEntity = echeancier.toSepaSddOrigine().toEntity();
		return mandat.toTiersDebiteurPersonne() != null && !origineEntity.origineDebiteurPersonne().persId().equals(mandat.toTiersDebiteurPersonne().persId());
	}

	public void checkOrigineDebiteurDifferentDeMandatTiersDebiteur(ISepaSddEcheancier echeancier) throws NSValidation.ValidationException {
		ISepaSddMandat mandat = echeancier.mandat();
		ISepaSddOrigineEntity origineEntity = echeancier.toSepaSddOrigine().toEntity();
		if (mandat.toTiersDebiteurPersonne() != null && isOrigineDebiteurDifferentDeMandatTiersDebiteur(echeancier)) {
			throw new NSValidation.ValidationException("Un tiers débiteur est spécifié pour le mandat SEPA SDD (" +
					mandat.toTiersDebiteurPersonne().persLibelle() + "). Ce tiers débiteur doit être celui de l'objet à l'origine de l'échancier et non "
					+ origineEntity.origineDebiteurPersonne().persLibelle() + ".");
		}
	}

	public Boolean isOrigineDebiteurDifferentDeMandatDebiteur(ISepaSddEcheancier echeancier) {
		ISepaSddMandat mandat = echeancier.mandat();
		ISepaSddOrigineEntity origineEntity = echeancier.toSepaSddOrigine().toEntity();
		return !origineEntity.origineDebiteurPersonne().persId().equals(mandat.toDebiteurPersonne().persId());
	}

	/**
	 * //si tiers debiteur specifie au niveau du mandat, l'origine de l'echeancier doit dependre de ce tiers debiteur, sinon elle doit dependre du
	 * debiteur
	 * 
	 * @param echeancier
	 */
	public void checkOrigineDebiteurDifferentDeMandatDebiteur(ISepaSddEcheancier echeancier) throws NSValidation.ValidationException {
		ISepaSddMandat mandat = echeancier.mandat();
		ISepaSddOrigineEntity origineEntity = echeancier.toSepaSddOrigine().toEntity();
		if (mandat.toTiersDebiteurPersonne() == null && isOrigineDebiteurDifferentDeMandatDebiteur(echeancier)) {
			throw new NSValidation.ValidationException("Le débiteur spécifié pour le mandat et le débiteur de l'objet à l'origine de l'échancier ne correspondent pas (" +
					mandat.toDebiteurPersonne().persLibelle() + " / " + origineEntity.origineDebiteurPersonne().persLibelle() + "). Vous pouvez affecter un tiers débiteur au niveau du mandat si besoin.");
		}
	}

	public void checkEcheanceNonAnnuleeDeTypeOOF(ISepaSddEcheancier echeancier) throws NSValidation.ValidationException {
		if (!ISepaSddEcheance.TypeOperation.OOFF.equals(((ISepaSddEcheance) echeancier.getEcheancesTrieesParDateEcheanceASCAvecEtatDifferentDe(ISepaSddEcheance.Etat.ANNULE, false).objectAtIndex(0)).sddTypeOp())) {
			throw new NSValidation.ValidationException("L'échéance d'un mandat de type ponctuel doit être de type " + ISepaSddEcheance.TypeOperation.OOFF.toString());
		}
	}

	public Boolean isPlusieursEcheancesNonAnnulees(ISepaSddEcheancier echeancier) {
		NSArray res = echeancier.getEcheancesTrieesParDateEcheanceASCAvecEtatDifferentDe(ISepaSddEcheance.Etat.ANNULE, false);
		return res.count() > 1;
	}

	/**
	 * @param echeancier echeancier concerné
	 * @return true si l'origine de l'echeancier est utilisée par un autre échéancier dépendant d'un mandat à l'état VALIDE
	 * @throws Exception si erreur lors de la verification
	 */
	public Boolean isOrigineUtiliseePourAutresEcheanciers(ISepaSddEcheancier echeancier, StringBuilder erreurs) {
		NSArray echeanciersAvecMemeOrigine = echeancier.toSepaSddOrigine().toSepaSddEcheanciers(getQualForMandatNonAnnule());
		Boolean res = Boolean.FALSE;
		if (echeanciersAvecMemeOrigine.count() == 0) {
			res = Boolean.FALSE;
		}
		else if (echeanciersAvecMemeOrigine.count() > 1 || !echeancier.equals(echeanciersAvecMemeOrigine.objectAtIndex(0))) {
			res = Boolean.TRUE;
			ISepaSddEcheancier ech = (ISepaSddEcheancier) echeanciersAvecMemeOrigine.objectAtIndex(0);
			erreurs.append("Une origine ne peut être utilisée que par un seul échéancier. L'origine est déjà affectée à un autre échéancier appartenant au mandat rum = " + ech.getReference());
		}
		return res;
	}

	/**
	 * @param echeancier echeancier concerné
	 * @throws NSValidation.ValidationException Si l'origine de l'echeancier est utilisée par un autre échéancier dépendant d'un mandat à l'état
	 *             VALIDE.
	 */
	public void checkOrigineUtiliseePourAutresEcheanciers(ISepaSddEcheancier echeancier) throws NSValidation.ValidationException {
		try {
			StringBuilder erreurs = new StringBuilder();
			if (isOrigineUtiliseePourAutresEcheanciers(echeancier, erreurs)) {
				throw new NSValidation.ValidationException(erreurs.toString());
			}
		} catch (NSValidation.ValidationException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new NSValidation.ValidationException("Erreur lors de la recherche d'origine affectée à un autre échéancier. Consultez les logs." + e.getMessage());
		}

	}

	private EOQualifier getQualForMandatNonAnnule() {
		return new EOKeyValueQualifier(ISepaSddEcheancier.TO_I_SEPA_SDD_MANDAT_KEY + "." + ISepaSddMandat.TO_I_TYPE_ETAT_KEY + "." + IJefyAdminTypeEtat.I_TYET_LIBELLE_KEY, EOQualifier.QualifierOperatorNotEqual, IJefyAdminTypeEtat.ETAT_ANNULE);
	}

	public Boolean isPremiereEcheanceDateEcheanceAnterieureADatePremiereEcheanceSurMemeMandatPlusDelai(EOEditingContext edc, ISepaSddEcheancier echeancier, StringBuilder msg) throws Exception {
		ISepaSddEcheance echeance = SepaSddEcheancierHelper.getSharedInstance().getPremiereEcheanceNonAnnulee(echeancier);
		return SepaSddEcheanceRule.getSharedInstance().isDateEcheanceAnterieureADatePremiereEcheanceSurMemeMandatPlusDelai(edc, echeance, msg);

	}

	public Boolean isChangementOrigineSiEcheancesNonAnnuleeExiste(ISepaSddEcheancier echeancier, StringBuilder msg) {
		Boolean res = Boolean.FALSE;
		if (!CktlEOControlUtilities.isNewObject(echeancier.editingContext(), echeancier)) {
			NSDictionary dico = CktlEOControlUtilities.committedSnapshot(echeancier.editingContext(), echeancier);
			ISepaSddOrigine origineOld = (ISepaSddOrigine) dico.valueForKey(ISepaSddEcheancier.TO_ISEPA_SDD_ORIGINE_KEY);
			ISepaSddOrigine origineNew = echeancier.toSepaSddOrigine();
			if (!origineNew.equals(origineOld)) {
				res = (SepaSddEcheancierHelper.getSharedInstance().getEcheancesNonAnnuleesTrieesParDateEcheanceASC(echeancier, false).count() > 0);
				if (res) {
					msg.append("Impossible de modifier l'origine d'un échéancier si celui-ci comporte des échéances.");
				}
			}
		}
		return res;
	}
	
	public void checkChangementOrigineSiEcheancesNonAnnuleeExiste(ISepaSddEcheancier echeancier) {
		StringBuilder msg = new StringBuilder();
		if (isChangementOrigineSiEcheancesNonAnnuleeExiste(echeancier, msg)) {
			throw new NSValidation.ValidationException(msg.toString());
		}
	}

	public Boolean isEcriturePriseEnChargeDebitDefinie(ISepaSddEcheancier echeancier) {
		return SepaSddEcheancierHelper.getSharedInstance().getLastEcritureDetailPriseEnCharge(echeancier) != null;
	}

	public Boolean isEcriturePriseEnChargeDebitTotalementEmargee(ISepaSddEcheancier echeancier) {
		return EcritureDetailHelper.getSharedInstance().isTotalementEmargee(SepaSddEcheancierHelper.getSharedInstance().getLastEcritureDetailPriseEnCharge(echeancier));
	}
	
	public void checkEcriturePriseEnChargeDebitDefinieEtNonTotalementEmargee(ISepaSddEcheancier echeancier) {
	    if (!echeancier.toSepaSddOrigine().toSepaSddOrigineType().isGenerationEcrituresAutomatique()) {
	        return;
	    }
	    
		if (!isEcriturePriseEnChargeDebitDefinie(echeancier)) {
			throw new NSValidation.ValidationException("L'écriture de prise en charge de l'échéancier n'a pas été trouvée (l'échéancier n'a pas du être pris en charge). ");
		}
		if (isEcriturePriseEnChargeDebitTotalementEmargee(echeancier)) {
			throw new NSValidation.ValidationException("L'écriture de prise en charge de l'échéancier est totalement émargée. ");
		}
	}

	public void checkEcheancesSupprimables(ISepaSddEcheancier echeancier) {
		for (ISepaSddEcheance currentEcheance : echeancier.echeances()) {
			SepaSddEcheanceRule.getSharedInstance().checkEcheanceSupprimable(currentEcheance);
		}
	}

}
