package org.cocktail.fwkcktlcompta.common.sepasdd.rules;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.common.IFwkCktlComptaParam;
import org.cocktail.fwkcktlcompta.common.ZConst;
import org.cocktail.fwkcktlcompta.common.helpers.ParamHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheanceHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHelper;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcompta.common.util.ISODateTextToLocalDateFormat;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/**
 * Classe contenant les méthodes de vérification de cohérence sur les objets métiers liés au SEPA. A appeler dans les methodes de validation des
 * objets metiers clients ou serveur.
 *
 * @author rprin
 */
public class SepaSddEcheanceRule {
	private static SepaSddEcheanceRule sharedInstance = new SepaSddEcheanceRule();
	private static final Logger LOG = Logger.getLogger(SepaSddEcheanceRule.class);
	private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormat.forPattern(ISODateTextToLocalDateFormat.FRENCH_SHORT_DATE_PATTERN);

	public static SepaSddEcheanceRule getSharedInstance() {
		return sharedInstance;
	}

	/**
	 * @param echeance l'echeance à tester
	 * @throws NSValidation.ValidationException Lorsqu'une erreur est détectée
	 */
	public void validateSepaSddEcheance(ISepaSddEcheance echeance) throws NSValidation.ValidationException {
		if (echeance.echeancier() == null) {
			throw new NSValidation.ValidationException("L'échéancier est obligatoire");
		}
		checkDateEcheanceAttenteAnterieureDateDuJour(echeance);
		checkIncoherenceTypeOperationEtEtat(echeance);
		checkIncoherenceMontant(echeance);
		checkEcheancePreleveeOuConfirmeeOuRejeteeIncoherente(echeance);
		checkEcheancePreleveeEtMandatNonValide(echeance);
	}

	public void checkEcheanceSupprimable(ISepaSddEcheance echeance) throws NSValidation.ValidationException {
		StringBuilder sw = new StringBuilder();
		if (!isSupprimable(echeance, sw)) {
			throw new NSValidation.ValidationException(sw.toString());
		}
	}

	public Boolean isDateEcheanceAnterieureADatePremiereEcheanceSurMemeMandatPlusDelai(EOEditingContext edc, ISepaSddEcheance echeance, StringBuilder msg) throws Exception {
		if (SepaSddEcheanceHelper.getSharedInstance().isEcheanceAnnulee(echeance)) {
			return Boolean.FALSE;
		}
		NSArray autresEcheancesDuMandat = SepaSddEcheanceHelper.getSharedInstance().getAutresEcheancesDuMandatTrieesParDateASC(echeance);
		autresEcheancesDuMandat = SepaSddEcheanceHelper.getSharedInstance().filtrerEcheancesNonAnnulees(autresEcheancesDuMandat);
		if (autresEcheancesDuMandat.count() == 0) {
			return Boolean.FALSE;
		}
		ISepaSddEcheance premiereEcheance = (ISepaSddEcheance) autresEcheancesDuMandat.objectAtIndex(0);
		LocalDate datePremiereEcheance = SepaSddEcheanceHelper.getSharedInstance().getDateEcheance(premiereEcheance);
		LocalDate dateEcheance = SepaSddEcheanceHelper.getSharedInstance().getDateEcheance(echeance);

		if (datePremiereEcheance.isAfter(dateEcheance)) {
			return Boolean.FALSE;
		}
		Integer delai = Integer.valueOf((String) ParamHelper.getSharedInstance().getConfig(edc, IFwkCktlComptaParam.SEPASDDMANDAT_NB_JOURS_MINIMUM_AVANT_DEUXIEME_ECHEANCE));
		LocalDate dateMinimumAvantNouvelleEcheance = datePremiereEcheance.plusDays(delai);
		//FIXME attendre que la premiere echeance soit "confirmee" avant de permettre la deuxieme ?
		boolean res = (dateEcheance.isBefore(dateMinimumAvantNouvelleEcheance));
		if (res) {
			msg.append("Le mandat " + echeance.echeancier().mandat().rum() + " comporte déjà des échéances. La deuxième échéance d'un mandat doit être planifiée au moins " + delai + " jours après la première.");
			if (LOG.isDebugEnabled()) {
				LOG.debug(msg.toString());
			}
		}
		return res;

	}

	public void checkDateEcheanceAnterieureADatePremiereEcheanceSurMemeMandatPlusDelai(EOEditingContext edc, ISepaSddEcheance echeance) throws NSValidation.ValidationException {
		try {
			StringBuilder msg = new StringBuilder();
			if (isDateEcheanceAnterieureADatePremiereEcheanceSurMemeMandatPlusDelai(edc, echeance, msg)) {
				throw new NSValidation.ValidationException(msg.toString());
			}
		} catch (NSValidation.ValidationException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new NSValidation.ValidationException("Erreur lors de la verification DateEcheanceAnterieureADatePremiereEcheancePreleveeSurMemeMandatPlusDelai." + e.getMessage() + " Consultez les logs.");
		}

	}

	public Boolean isDateEcheanceAttenteAnterieureDateDuJour(ISepaSddEcheance echeance, StringBuilder msg) {
		Boolean res = Boolean.FALSE;
		if (SepaSddEcheanceHelper.getSharedInstance().isEcheanceAttente(echeance)) {
			LocalDate dateEcheance = SepaSddEcheanceHelper.getSharedInstance().getDateEcheance(echeance);
			LocalDate dateNow = LocalDate.now();
			res = (dateEcheance.isBefore(dateNow));
			if (res) {
				msg.append("La date d'échéance (" + DATETIME_FORMATTER.print(dateEcheance)
						+ ") ne peut pas être antérieure à la date du jour ("
						+ DATETIME_FORMATTER.print(dateNow) + " )");
			}
		}
		return res;
	}

	public void checkDateEcheanceAttenteAnterieureDateDuJour(ISepaSddEcheance echeance) {
		StringBuilder msg = new StringBuilder();
		if (isDateEcheanceAttenteAnterieureDateDuJour(echeance, msg)) {
			throw new NSValidation.ValidationException(msg.toString());
		}
	}

	public void checkEcheancePreleveeOuConfirmeeOuRejeteeIncoherente(ISepaSddEcheance echeance) {
		if (SepaSddEcheanceHelper.getSharedInstance().isEcheancePreleveeOuConfirmeeOuRejetee(echeance)) {
			try {
				checkAttributsHistorisationComplets(echeance);
				if (StringCtrl.isEmpty(echeance.sddTypeOp())) {
					throw new NSValidation.ValidationException("Le type d'opération n'est pas renseigné.");
				}
				if (echeance.toRecouvrement() == null) {
					throw new NSValidation.ValidationException("L'échéance n'est pas reliée à un recouvrement.");
				}

			} catch (Exception e) {
				throw new NSValidation.ValidationException("Incoherence sur l'echeance :" + e.getMessage());
			}
		}
	}

	public Boolean isAttributsHistorisationComplets(ISepaSddEcheance echeance) {
		Boolean incomplet = Boolean.FALSE;
		incomplet = incomplet || StringCtrl.isEmpty(echeance.rum());
		incomplet = incomplet || StringCtrl.isEmpty(echeance.debiteurBic());
		incomplet = incomplet || StringCtrl.isEmpty(echeance.debiteurIban());
		incomplet = incomplet || StringCtrl.isEmpty(echeance.debiteurId());
		incomplet = incomplet || StringCtrl.isEmpty(echeance.debiteurNom());
		incomplet = incomplet || StringCtrl.isEmpty(echeance.creancierIcs());
		incomplet = incomplet || StringCtrl.isEmpty(echeance.creancierNom());
		return !incomplet;
	}

	public void checkAttributsHistorisationComplets(ISepaSddEcheance echeance) {
		if (!isAttributsHistorisationComplets(echeance)) {
			throw new NSValidation.ValidationException("Les informations historiques ne sont pas renseignées au niveau de l'échéance.");
		}
	}

	public void checkIncoherenceMontant(ISepaSddEcheance echeance) {
		if (echeance.montantAPayer().compareTo(BigDecimal.ZERO) <= 0) {
			throw new NSValidation.ValidationException("Le montant de l'échéance ne peut être inférieur ou égal à 0.");
		}
	}

	public void checkIncoherenceTypeOperationEtEtat(ISepaSddEcheance echeance) {
		SepaSddEcheanceHelper sddEcheanceHelper = SepaSddEcheanceHelper.getSharedInstance();
		if (echeance.sddTypeOp() != null) {
			if (sddEcheanceHelper.isEcheanceAnnulee(echeance) || sddEcheanceHelper.isEcheanceAttente(echeance)) {
				throw new NSValidation.ValidationException("Une échéance annulée ou en attente ne peut avoir un typeOperation défini.");
			}
		}
		else if (!(sddEcheanceHelper.isEcheanceAnnulee(echeance) || sddEcheanceHelper.isEcheanceAttente(echeance))) {
			throw new NSValidation.ValidationException("Une échéance qui n'est pas annulée ou en attente doit avoir un typeOperation défini.");
		}
	}

	/**
	 * Verifie si dans une liste d'echeances plusieurs echeances appartiennent au meme mandat. Utile lors de la génération des fichiers de prélèvement
	 * FIRST ou ce cas n'est pas autorisé.
	 *
	 * @param echeances
	 */
	public void checkPlusieursEcheancesPourMemeMandat(NSArray echeances) {
		StringBuilder swErreurs = new StringBuilder();
		if (echeances.count() > 0) {
			if (isPlusieursEcheancesPourMemeMandat(echeances, swErreurs)) {
				if (swErreurs.length() > 0) {
					throw new NSValidation.ValidationException(swErreurs.toString());
				}
			}
		}
	}

	public Boolean isPlusieursEcheancesPourMemeMandat(NSArray echeances, StringBuilder swErreurs) {
		Boolean res = Boolean.FALSE;
		if (echeances.count() > 0) {
			List<ISepaSddMandat> listMandats = SepaSddEcheanceHelper.getSharedInstance().listerLesMandat(echeances);
			for (ISepaSddMandat iSepaSddMandat : listMandats) {
				EOQualifier qual = new EOKeyValueQualifier(ISepaSddEcheance.I_ECHEANCIER_KEY + "." + ISepaSddEcheancier.I_MANDAT_KEY, EOQualifier.QualifierOperatorEqual, iSepaSddMandat);
				NSArray<ISepaSddEcheance> echeancesParMandat = EOQualifier.filteredArrayWithQualifier(echeances, qual);
				if (echeancesParMandat.count() > 1) {
					res = Boolean.TRUE;
					String erreur = "Plusieurs échéances trouvées pour le mandat " + iSepaSddMandat.rum() + " (" + iSepaSddMandat.nomPrenomDebiteur() + "):";
					for (ISepaSddEcheance iSepaSddEcheance : echeancesParMandat) {
						swErreurs.append(iSepaSddEcheance.dPrevue() + " (" + iSepaSddEcheance.montantAPayer() + ") / ");
					}
					swErreurs.append(erreur);
				}
			}
		}
		return res;
	}

	public Boolean isEcheancePreleveeEtMandatNonValide(ISepaSddEcheance echeance) {
		if (SepaSddEcheanceHelper.getSharedInstance().isEcheancePreleveeOuConfirmee(echeance)) {
			if (!SepaSddMandatHelper.getSharedInstance().isValide(echeance.echeancier().mandat())) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	public void checkEcheancePreleveeEtMandatNonValide(ISepaSddEcheance echeance) {
		if (isEcheancePreleveeEtMandatNonValide(echeance)) {
			throw new NSValidation.ValidationException("Une échéance ne peut être prélevée alors qu'elle dépend d'un mandat non valide.");
		}
	}

	public boolean isSupprimable(ISepaSddEcheance echeance) {
		return (echeance.etatAsEnum().equals(Etat.ANNULE) || echeance.etatAsEnum().equals(Etat.ATTENTE));
	}

	public boolean isSupprimable(ISepaSddEcheance echeance, StringBuilder swErreurs) {
		boolean res = isSupprimable(echeance);
		if (!res) {
			swErreurs.append("L'échéance du " + ZConst.DATEISO_FORMAT_DATESHORT.format(echeance.dPrevue()) + " n'est pas supprimable.");
		}
		return res;
	}

	public void checkEcheancePeutEtrePrelevee(ISepaSddEcheance echeance) throws Exception {
		String erreurs = "";
		ISepaSddEcheancier echeancier = echeance.echeancier();
		ISepaSddMandat mandat = echeancier.mandat();
		String dateIso = DateConversionUtil.sharedInstance().formatDateWithoutTimeISO(LocalDate.now());
		if (SepaSddMandatRule.getSharedInstance().isMandatCaduc(mandat.editingContext(), mandat, dateIso)) {
			erreurs += "Le mandat est caduc. ";
		}

		if (mandat.toDebiteurRib() == null) {
			erreurs += "Rib débiteur non défini. ";
		}
		if (!mandat.toDebiteurRib().isRibIntlComplet()) {
			erreurs += "Rib débiteur incomplet. ";
		}

		//Verifier que l'ecriture de prise en charge de l'echeancier existe et n'est pas totalement émargée
		try {
			SepaSddEcheancierRule.getSharedInstance().checkEcriturePriseEnChargeDebitDefinieEtNonTotalementEmargee(echeancier);
		} catch (Exception e) {
			erreurs += e.getMessage();
		}
		//verifier que l'échéance n'est pas une echeance posterieure à une premiere echeance de mandat qui ne serait pas encore prelevee

		NSArray echeancesPrelev = SepaSddMandatHelper.getSharedInstance().getEcheancesPreleveesOuConfirmeesOuRejeteesTrieesParDateEcheanceASC(mandat, false);
		if (echeancesPrelev.count() == 0) {
			NSArray echeancesAttente = SepaSddMandatHelper.getSharedInstance().getEcheancesAttenteTrieesParDateEcheanceASC(mandat, false);
			if (echeancesAttente.indexOfObject(echeance) > 0) {
				erreurs += "La première échéance du mandat n'a pas été prélevée. ";
			}
		}

		if (erreurs.length() > 0) {
			throw new Exception(erreurs);
		}

	}

}
