package org.cocktail.fwkcktlcompta.common.sepasdd.helpers;

import java.util.Comparator;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.joda.time.LocalDate;

public class TriEcheancesParDatePrevue implements Comparator<ISepaSddEcheance> {

	public int compare(ISepaSddEcheance echeance1, ISepaSddEcheance echeance2) {
		if (echeance1 == echeance2) {
			return 0;
		}

		if (dateEcheanceNonRenseignee(echeance1) && dateEcheanceNonRenseignee(echeance2)) {
			return 0;
		}

		if (dateEcheanceNonRenseignee(echeance1) && echeance2 != null) {
			return -1;
		}

		if (echeance1 != null && dateEcheanceNonRenseignee(echeance2)) {
			return 1;
		}

		DateConversionUtil dateUtil = new DateConversionUtil();
		LocalDate datePrelement1 = dateUtil.parseDate(echeance1.dPrevue());
		LocalDate datePrelement2 = dateUtil.parseDate(echeance2.dPrevue());
		return datePrelement1.compareTo(datePrelement2);
	}

	protected boolean dateEcheanceNonRenseignee(ISepaSddEcheance echeance) {
		if (echeance == null || echeance.dPrevue() == null) {
			return true;
		}
		return false;
	}
}
