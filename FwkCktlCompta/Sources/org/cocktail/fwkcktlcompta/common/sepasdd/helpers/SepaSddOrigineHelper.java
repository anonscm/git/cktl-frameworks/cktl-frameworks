package org.cocktail.fwkcktlcompta.common.sepasdd.helpers;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ctrl.ISepaSddOrigineCtrl;
import org.cocktail.fwkcktlcompta.common.sepasdd.origines.CommonSepaSddOrigineEntityCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class SepaSddOrigineHelper {
	protected static Map<String, ? extends CommonSepaSddOrigineEntityCtrl> sepaSddOriginesMap;
	private static SepaSddOrigineHelper sharedInstance = new SepaSddOrigineHelper();

	public static SepaSddOrigineHelper getSharedInstance() {
		return sharedInstance;
	}

	/**
	 * @param typeOrigines
	 * @return Une map contenant les objets controleurs pour chaque type d'origine.
	 * @throws Exception Si une excceptio est déclenchée ici, il est possible que le contenu de la table maracuja.sepasdd_origine_type ne soit pas
	 *             cohérent avec les framweorks (référence à une classe non instanciable
	 */
	public Map<String, ? extends CommonSepaSddOrigineEntityCtrl> buildOriginesCtrl(NSArray typeOrigines) throws Exception {
		Map<String, CommonSepaSddOrigineEntityCtrl> res = new HashMap<String, CommonSepaSddOrigineEntityCtrl>();
		for (int i = 0; i < typeOrigines.count(); i++) {
			try {
				ISepaSddOrigineType typeOrigine = (ISepaSddOrigineType) typeOrigines.objectAtIndex(i);
				String eoCtrlClassName = typeOrigine.typeClass();

				Class<CommonSepaSddOrigineEntityCtrl> eoCtrlClass = (Class<CommonSepaSddOrigineEntityCtrl>) Class.forName(eoCtrlClassName);
				try {
					CommonSepaSddOrigineEntityCtrl ctr = eoCtrlClass.newInstance();
					res.put(typeOrigine.typeNom(), ctr);
				} catch (Exception e) {
					throw new Exception("Impossible d'instancier la classe " + eoCtrlClassName, e);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return res;
	}

	/**
	 * @param sepaSddOrigine
	 * @return Le type et le libellé complete de l'entité d'origine, à destination de l'utilisateur.
	 */
	public String getTypeAndLibelleCompletOfOrigine(ISepaSddOrigine sepaSddOrigine) {
		String res = sepaSddOrigine.toSepaSddOrigineType().typeNom();
		if (sepaSddOrigine.toEntity() != null) {
			res += " : " + sepaSddOrigine.toEntity().getOrigineLibelleComplet();
		}
		return res;
	}

	public String getLibellePourDebiteur(ISepaSddOrigine sepaSddOrigine) {
		return sepaSddOrigine.toEntity().getOrigineLibellePourDebiteur();
	}

	public ISepaSddOrigine creerOuRecupererOrigine(EOEditingContext edc, ISepaSddOrigineType origineType, ISepaSddOrigineEntity origineEntity) throws Exception {
		if (origineType == null) {
			throw new Exception("Le type d'origine est obligatoire");
		}
		if (origineEntity == null) {
			throw new Exception("L'entité origine est obligatoire");
		}
		ISepaSddOrigine origine = ISepaSddOrigineCtrl.getSharedInstance().fetchOrigine(edc, origineType, origineEntity);
		if (origine == null) {
			origine = ISepaSddOrigineCtrl.getSharedInstance().creerInstance(edc);
			origine.setOrigineId(origineEntity.origineIdentifiant());
			origine.setToSepaSddOrigineTypeRelationship(origineType);

		}
		return origine;
	}

	public boolean isOrigineDebiteurDefini(ISepaSddOrigine origine) {
		return origine != null && origine.toEntity() != null && origine.toEntity().origineDebiteurPersonne() != null;
	}

	public ISepaSddEcheancier premierEcheancierPourOrigine(ISepaSddOrigine origine) {
		if (origine == null) {
			return null;
		}

		if (origine.toSepaSddEcheanciers() == null || origine.toSepaSddEcheanciers().count() == 0) {
			return null;
		}

		return (ISepaSddEcheancier) origine.toSepaSddEcheanciers().objectAtIndex(0);
	}

	public ISepaSddOrigineType fetchOrigineTypeValide(EOEditingContext editingContext, String code) {
		if (code == null) {
			return null;
		}

		EOQualifier qualType = new EOKeyValueQualifier(ISepaSddOrigineType.I_TYPE_CODE_KEY, EOQualifier.QualifierOperatorEqual, code);
		NSArray<EOSortOrdering> sort = NSArray.emptyArray();
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ISepaSddOrigineType.I_ENTITY_NAME, qualType, sort);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(false);
		NSArray types = editingContext.objectsWithFetchSpecification(fetchSpec);

		ISepaSddOrigineType type = null;
		if (types.count() > 0) {
			type = (ISepaSddOrigineType) types.objectAtIndex(0);
		}

		return type;
	}

}
