package org.cocktail.fwkcktlcompta.common.sepasdd.helpers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.ZConst;
import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.helpers.EcritureDetailHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.TypeOperation;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheanceEcd;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.util.CktlArrayUtilities;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.joda.time.LocalDate;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXStringUtilities;

public class SepaSddEcheanceHelper {
	public static EOSortOrdering SORT_DATE_ECHEANCE_ASC = EOSortOrdering.sortOrderingWithKey(ISepaSddEcheance.I_D_PREVUE_KEY, EOSortOrdering.CompareAscending);
	//public static EOSortOrdering SORT_NUMERO_ASC = EOSortOrdering.sortOrderingWithKey(ISepaSddEcheance.I_NUMERO_KEY, EOSortOrdering.CompareAscending);

	private static SepaSddEcheanceHelper sharedInstance = new SepaSddEcheanceHelper();

	public static SepaSddEcheanceHelper getSharedInstance() {
		return sharedInstance;
	}

	public NSArray triLesEcheancesParDateEcheanceASC(NSArray echeances) {
		NSArray sortedRes = EOSortOrdering.sortedArrayUsingKeyOrderArray(echeances, new NSArray(new Object[] {
				SepaSddEcheanceHelper.SORT_DATE_ECHEANCE_ASC
		}));
		return sortedRes;
	}

	/**
	 * @param echeance
	 * @return le type d'echeance en fonction de ce qui s'est déja passé sur le mandat avec les autres echeances. Si l'echeance n'est pas à attente,
	 *         le type d'operation deja affecté est renvoyé.
	 */
	@SuppressWarnings("rawtypes")
	public TypeOperation determineTypeOperation(ISepaSddEcheance echeance, boolean fetch) {
		if (!SepaSddEcheanceHelper.getSharedInstance().isEcheanceAttente(echeance)) {
			return ISepaSddEcheance.TypeOperation.valueOf(echeance.sddTypeOpTmp());
		}
		if (!echeance.echeancier().mandat().isRecurrent()) {
			return ISepaSddEcheance.TypeOperation.OOFF;
		}
		ISepaSddMandat mandat = echeance.echeancier().mandat();
		NSArray echeancesPrelevees = SepaSddMandatHelper.getSharedInstance().getEcheancesPreleveesOuConfirmeesTrieesParDateEcheanceASC(mandat, true);

		//si l'echeance précédente a été rejetée, il faut la représenter en FRST
		if (echeancesPrelevees.count() == 0) {
			return ISepaSddEcheance.TypeOperation.FRST;
		}
		
		NSArray echeancesPreleveesOuConfirmeesOuRejetees = SepaSddMandatHelper.getSharedInstance().getEcheancesPreleveesOuConfirmeesOuRejeteesTrieesParDateEcheanceASC(mandat, true);
		ISepaSddEcheance lastEcheancePrelevee = (ISepaSddEcheance) echeancesPreleveesOuConfirmeesOuRejetees.objectAtIndex(echeancesPrelevees.count() - 1);
		if (!lastEcheancePrelevee.debiteurBic().equals(echeance.echeancier().mandat().toDebiteurRib().toBanque().bic())) {
			return ISepaSddEcheance.TypeOperation.FRST;
		}

		return ISepaSddEcheance.TypeOperation.RCUR;
	}

	public void determineEtAffecteTypeOperationTmp(ISepaSddEcheance echeance, boolean fetch) {
		ISepaSddEcheance.TypeOperation typeOp = determineTypeOperation(echeance, fetch);
		echeance.setTypeOperationTmp(typeOp);
	}

	@SuppressWarnings("rawtypes")
	public void determinerEtAffecterLesTypeOperationTmp(NSArray lesEcheances) {
		for (int i = 0; i < lesEcheances.count(); i++) {
			ISepaSddEcheance echeance = (ISepaSddEcheance) lesEcheances.objectAtIndex(i);
			determineEtAffecteTypeOperationTmp(echeance, true);
		}
	}

	@SuppressWarnings("rawtypes")
	public ArrayList<String> listerLesDatePrevue(NSArray lesEcheances) {
		ArrayList<String> datePrevues = new ArrayList<String>();
		for (int i = 0; i < lesEcheances.count(); i++) {
			ISepaSddEcheance echeance = (ISepaSddEcheance) lesEcheances.objectAtIndex(i);
			String datePrevue = echeance.dPrevue();
			if (!datePrevues.contains(datePrevue)) {
				datePrevues.add(datePrevue);
			}
		}
		return datePrevues;
	}

	@SuppressWarnings("rawtypes")
	public ArrayList<TypeOperation> listerLesTypeOperationTmp(NSArray lesEcheances) {
		ArrayList<TypeOperation> typeOperations = new ArrayList<ISepaSddEcheance.TypeOperation>();
		for (int i = 0; i < lesEcheances.count(); i++) {
			ISepaSddEcheance echeance = (ISepaSddEcheance) lesEcheances.objectAtIndex(i);
			TypeOperation typeOperation = TypeOperation.valueOf(echeance.sddTypeOpTmp());
			if (!typeOperations.contains(typeOperation)) {
				typeOperations.add(typeOperation);
			}
		}
		return typeOperations;
	}

	public boolean isPlusieursEcheancesDeType(NSArray echeances, ISepaSddEcheance.TypeOperation typeOperation) {
		EOQualifier qual = new EOKeyValueQualifier(ISepaSddEcheance.I_SDD_TYPE_OP_KEY, EOQualifier.QualifierOperatorEqual, typeOperation.toString());
		return EOQualifier.filteredArrayWithQualifier(echeances, qual).count() > 1;
	}

	public ISepaSddEcheance getEcheanceDeType(NSArray echeances, ISepaSddEcheance.TypeOperation typeOperation) {
		EOQualifier qual = new EOKeyValueQualifier(ISepaSddEcheance.I_SDD_TYPE_OP_KEY, EOQualifier.QualifierOperatorEqual, typeOperation.toString());
		NSArray res = EOQualifier.filteredArrayWithQualifier(echeances, qual);
		if (res.count() == 0) {
			return null;
		}
		return (ISepaSddEcheance) res.objectAtIndex(0);
	}

	public NSArray<ISepaSddEcheance> getEcheancesDeType(NSArray<ISepaSddEcheance> echeances, ISepaSddEcheance.TypeOperation typeOperation) {
		EOQualifier qual = new EOKeyValueQualifier(ISepaSddEcheance.I_SDD_TYPE_OP_KEY, EOQualifier.QualifierOperatorEqual, typeOperation.toString());
		return EOQualifier.filteredArrayWithQualifier(echeances, qual);
	}

	public boolean isPlusieursEcheancesSurMemeDateQueOperationDuType(NSArray echeances, ISepaSddEcheance.TypeOperation typeOperation) {
		ISepaSddEcheance echeFirst = getEcheanceDeType(echeances, typeOperation);
		if (echeFirst != null) {
			String dateEcheFirst = echeFirst.dPrevue();

			EOQualifier qual2 = new EOKeyValueQualifier(ISepaSddEcheance.I_D_PREVUE_KEY, EOQualifier.QualifierOperatorEqual, dateEcheFirst);
			if (EOQualifier.filteredArrayWithQualifier(echeances, new EOAndQualifier(new NSArray(new Object[] {
					qual2
			}))).count() > 1) {
				return true;
			}
		}
		return false;
	}

	public boolean isPlusieursEcheancesALaMemeDate(NSArray echeances, ArrayList<String> datesDoublons) {
		datesDoublons.clear();
		ArrayList<String> lesDatePrevues = SepaSddEcheanceHelper.getSharedInstance().listerLesDatePrevue(echeances);
		Iterator<String> datePrevuesIt = lesDatePrevues.iterator();
		while (datePrevuesIt.hasNext()) {
			String datePrevue = (String) datePrevuesIt.next();
			EOQualifier qual = new EOKeyValueQualifier(ISepaSddEcheance.I_D_PREVUE_KEY, EOQualifier.QualifierOperatorEqual, datePrevue);

			if (EOQualifier.filteredArrayWithQualifier(echeances, qual).count() > 1) {
				datesDoublons.add(datePrevue);
			}
		}
		return datesDoublons.size() > 0;
	}

	public NSArray getAutresEcheancesDuMandatTrieesParDateASC(ISepaSddEcheance echeance) {
		ISepaSddMandat mandat = echeance.echeancier().mandat();
		NSArray res = SepaSddMandatHelper.getSharedInstance().getEcheancesTrieesParDateEcheanceASC(mandat, false);
		return CktlArrayUtilities.arrayMinusObject(res, echeance);
	}

	public LocalDate getDateEcheance(ISepaSddEcheance echeance) {
		return DateConversionUtil.sharedInstance().parseDateSilent(echeance.dPrevue());
	}

	public NSArray filtrerEcheancesNonAnnulees(NSArray echeances) {
		EOQualifier qual = ISepaSddEcheance.QUAL_ETAT_NON_ANNULEES;
		return EOQualifier.filteredArrayWithQualifier(echeances, qual);
	}

	public boolean isEcheancePrelevee(ISepaSddEcheance echeance) {
		return ISepaSddEcheance.Etat.PRELEVE.equals(echeance.etatAsEnum());
	}

	public boolean isEcheanceAnnulee(ISepaSddEcheance echeance) {
		return ISepaSddEcheance.Etat.ANNULE.equals(echeance.etatAsEnum());
	}

	public boolean isEcheanceAttente(ISepaSddEcheance echeance) {
		return ISepaSddEcheance.Etat.ATTENTE.equals(echeance.etatAsEnum());
	}

	public boolean isEcheanceRejetee(ISepaSddEcheance echeance) {
		return ISepaSddEcheance.Etat.REJETE.equals(echeance.etatAsEnum());
	}

	public boolean isEcheanceConfirmee(ISepaSddEcheance echeance) {
		return ISepaSddEcheance.Etat.CONFIRME.equals(echeance.etatAsEnum());
	}

	public boolean isEcheancePreleveeOuConfirmee(ISepaSddEcheance echeance) {
		return isEcheancePrelevee(echeance) || isEcheanceConfirmee(echeance);
	}

	public boolean isEcheancePreleveeOuConfirmeeOuRejetee(ISepaSddEcheance echeance) {
		return isEcheancePreleveeOuConfirmee(echeance) || isEcheanceRejetee(echeance);
	}

	/**
	 * @param echeance
	 * @return La dernière échéance remise du mandat avant celle passée en parametre.
	 */
	public ISepaSddEcheance echeancePrecedenteRemiseDuMandat(ISepaSddEcheance echeance) {
		NSArray allEcheancesPrelevees = SepaSddMandatHelper.getSharedInstance().getEcheancesRemisesTrieesParDateEcheanceASC(echeance.echeancier().mandat(), false);
		if (allEcheancesPrelevees.count() == 0) {
			return null;
		}
		EOQualifier qual = new EOKeyValueQualifier(ISepaSddEcheance.I_D_PREVUE_KEY, EOQualifier.QualifierOperatorLessThan, echeance.dPrevue());
		NSArray res = EOQualifier.filteredArrayWithQualifier(allEcheancesPrelevees, qual);
		if (res.count() == 0) {
			return null;
		}
		return (ISepaSddEcheance) res.objectAtIndex(res.count() - 1);

	}

	/**
	 * @param echeance
	 * @return Le numero de l'echeance à prélevée au sein de l'échéancier.
	 */
	public Integer getNumeroEcheanceAPreleverPourEcheancier(ISepaSddEcheance echeance) {
		//recuperer les echeances deja prelevées, confirmees ou rejetees
		NSArray echeancesDejaRemises = SepaSddEcheancierHelper.getSharedInstance().getEcheancesRemisesTrieesParDateEcheanceASC(echeance.echeancier(), false);
		EOQualifier qual = new EOKeyValueQualifier(ISepaSddEcheance.I_D_PREVUE_KEY, EOQualifier.QualifierOperatorLessThan, echeance.dPrevue());
		NSArray res = EOQualifier.filteredArrayWithQualifier(echeancesDejaRemises, qual);
		return Integer.valueOf(res.count() + 1);
	}

	/**
	 * @param echeances
	 * @return La somme des montants à payer
	 */
	public BigDecimal calculeMontantAPayerDesEcheancesNonAnnulees(NSArray echeances) {
		BigDecimal sommeEcheances = BigDecimal.ZERO.setScale(2);
		for (int i = 0; i < echeances.count(); i++) {
			ISepaSddEcheance echeance = (ISepaSddEcheance) echeances.objectAtIndex(i);
			if (!Etat.ANNULE.equals(echeance.etatAsEnum())) {
				sommeEcheances = sommeEcheances.add(echeance.montantAPayer());
			}
		}
		return sommeEcheances;
	}

	public void changeEtatOfEcheancesAPreleve(NSArray echeances) {
		changeEtatOfEcheances(echeances, ISepaSddEcheance.Etat.PRELEVE);
	}

	public void changeEtatOfEcheancesAConfirme(NSArray echeances) {
		changeEtatOfEcheances(echeances, ISepaSddEcheance.Etat.CONFIRME);
	}

	public void changeEtatOfEcheancesARejete(NSArray echeances) {
		changeEtatOfEcheances(echeances, ISepaSddEcheance.Etat.REJETE);
	}
	
	public void changeEtatOfEcheancesAAnnule(NSArray echeances) {
		changeEtatOfEcheances(echeances, ISepaSddEcheance.Etat.ANNULE);
	}

	public void changeEtatOfEcheances(NSArray echeances, ISepaSddEcheance.Etat etat) {
		for (int i = 0; i < echeances.count(); i++) {
			ISepaSddEcheance echeance = (ISepaSddEcheance) echeances.objectAtIndex(i);
			echeance.setEtatEnum(etat);
		}
	}

	/**
	 * Affecte un type d'opération définitif.
	 * 
	 * @param echeances
	 * @param typeOperation
	 */
	public void affecteTypeOperationOfEcheances(NSArray echeances, TypeOperation typeOperation) {
		for (int i = 0; i < echeances.count(); i++) {
			ISepaSddEcheance echeance = (ISepaSddEcheance) echeances.objectAtIndex(i);
			echeance.setTypeOperation(typeOperation);
		}
	}

	public List<ISepaSddMandat> listerLesMandat(NSArray<ISepaSddEcheance> echeances) {
		List<ISepaSddMandat> listMandats = new ArrayList<ISepaSddMandat>();
		for (ISepaSddEcheance iSepaSddEcheance : echeances) {
			ISepaSddMandat mandat = iSepaSddEcheance.echeancier().mandat();
			if (!listMandats.contains(mandat)) {
				listMandats.add(mandat);
			}
		}
		return listMandats;
	}

	/**
	 * Attention : ne pas appeler cette methode alors que les echeances sont en train d'etre modifiées ou supprimees.
	 * 
	 * @param echeance
	 * @return
	 */
	public String getLibelleEcheance(ISepaSddEcheance echeance) {
		Integer echeanceNumero = SepaSddEcheanceHelper.getSharedInstance().getNumeroEcheanceAPreleverPourEcheancier(echeance);
		Integer echeanceNb = SepaSddEcheancierHelper.getSharedInstance().getNombreEcheancesNonAnnulees(echeance.echeancier());
		String numeroSurTotal = "";
		if (echeanceNb.intValue() > 1) {
			numeroSurTotal = echeanceNumero.toString() + "/" + echeanceNb.toString();
		}
		String echanceDate = ZConst.DATEISO_FORMAT_DATESHORT.format(echeance.dPrevue());
		return numeroSurTotal + " du " + echanceDate;
	}

	public String getLibelleEcheanceComplet(ISepaSddEcheance echeance) {
		return SepaSddEcheancierHelper.getSharedInstance().getLibelleEcheancierComplet(echeance.echeancier()) + " / Echeance " + getLibelleEcheance(echeance);
	}

	/**
	 * @param echeance
	 * @return l'écriture en débit la plus récente associée à l'échéance (soit une écriture de prélèvement, soit une écriture de confirmation ou de
	 *         rejet).
	 */
	public IEcritureDetail getLastEcritureDetailDebit(ISepaSddEcheance echeance) {
		List<IEcritureDetail> ecds = getEcritureDetailDebits(echeance);
		Collections.sort(ecds, new TriEcritureDetailParDate());
		return ecds.get(ecds.size() - 1);

	}

	public List<IEcritureDetail> getEcritureDetails(ISepaSddEcheance echeance) {
		NSArray eecds = echeance.toSepaSddEcheanceEcds();
		List<IEcritureDetail> ecds = new ArrayList<IEcritureDetail>();
		for (int i = 0; i < eecds.count(); i++) {
			ISepaSddEcheanceEcd array_element = (ISepaSddEcheanceEcd) eecds.objectAtIndex(i);
			ecds.add(array_element.toEcritureDetail());
		}

		return ecds;
	}

	public List<IEcritureDetail> getEcritureDetailDebits(ISepaSddEcheance echeance) {
		List<IEcritureDetail> ecds = getEcritureDetails(echeance);
		List<IEcritureDetail> debits = new ArrayList<IEcritureDetail>();
		for (IEcritureDetail iEcritureDetail : ecds) {
			if (EcritureDetailHelper.getSharedInstance().isDebit(iEcritureDetail)) {
				debits.add(iEcritureDetail);
			}
		}
		return debits;
	}

	/**
	 * @param echeance
	 * @return La date à utiliser pour le prélèvement (dEcheance ou dPreleve si dPreleve existe)
	 */
	public String getDatePrelevement(ISepaSddEcheance echeance) {
		if (!ERXStringUtilities.stringIsNullOrEmpty(echeance.dPreleve())) {
			return echeance.dPreleve();
		}
		return echeance.dPrevue();
	}


}
