package org.cocktail.fwkcktlcompta.common.sepasdd.helpers;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandatHisto;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class SepaSddMandatHistoHelper {

	public static final Map<String, String> DISPLAY_NAMES = new HashMap<String, String>();
	static {
		DISPLAY_NAMES.put("creancierIcs", "ICS");
		DISPLAY_NAMES.put("creancierNom", "Nom du créancier");
		DISPLAY_NAMES.put("cTypePrelevement", "Type de prélèvement");
		DISPLAY_NAMES.put("debiteurBic", "BIC du débiteur");
		DISPLAY_NAMES.put("debiteurIban", "IBAN du débiteur");
		DISPLAY_NAMES.put("debiteurNom", "Nom du débiteur");
		DISPLAY_NAMES.put("rum", "RUM du mandat");
		DISPLAY_NAMES.put("toCreancierPersonne", "Référence au créancier");
		DISPLAY_NAMES.put("toDebiteurPersonne", "Référence au débiteur");
		DISPLAY_NAMES.put("toTypeEtat", "Etat du mandat");
	}

	private static SepaSddMandatHistoHelper sharedInstance = new SepaSddMandatHistoHelper();

	public static SepaSddMandatHistoHelper getSharedInstance() {
		return sharedInstance;
	}

	public NSArray getListeModifications(ISepaSddMandat mandat) {
		EOSortOrdering sortDateModification = EOSortOrdering.sortOrderingWithKey(ISepaSddMandatHisto.DATE_CREATION, EOSortOrdering.CompareDescending);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(mandat.toSepaSddMandatHistos(), new NSArray(sortDateModification));
	}
}
