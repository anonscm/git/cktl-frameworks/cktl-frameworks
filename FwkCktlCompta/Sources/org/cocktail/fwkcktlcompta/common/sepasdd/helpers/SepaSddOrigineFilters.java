package org.cocktail.fwkcktlcompta.common.sepasdd.helpers;

import java.util.HashMap;

/**
 * Stocke les filtres utilisés pour la recherche d'origines de mandat SEPA SDD.
 *
 * @author rprin
 */
public class SepaSddOrigineFilters extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;

	public static final String DEBITEUR_PERSONNE_KEY = "debiteur";
	public static final String NUMERO_MIN = "numeroMin";
	public static final String NUMERO_MAX = "numeroMax";
	public static final String EXERCICE_KEY = "exercice";
	public static final String OBJET_KEY = "objet";
	public static final String MONTANT_MIN = "montantMin";
	public static final String MONTANT_MAX = "montantMax";

}
