/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcompta.common.sepasdd.helpers;

import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.common.entities.ctrl.IJefyAdminTypeEtatCtrl;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.rules.SepaSddMandatRule;
import org.cocktail.fwkcktlcompta.common.util.CktlEOControlUtilities;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.joda.time.LocalDateTime;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

/**
 * Methodes utilisées coté client ou serveur pour la gestion du mandat SEPA SDD
 *
 * @author rprin
 */
@SuppressWarnings({
		"unchecked", "rawtypes"
})
public class SepaSddMandatHelper {

	public static final String LABEL_MANDAT_SDD = "Mandat SDD";
	public static final String TYPE_PRELEVEMENT_LIBELLE_KEY = "libelle";
	public static final String TYPE_PRELEVEMENT_CODE_KEY = "code";
	public static final String TYPE_PRELEVEMENT_CODE_PONCTUEL = "P";
	public static final String TYPE_PRELEVEMENT_CODE_RECURRENT = "R";


	public static final String RUM_TEMP = "XXXXXX";
	public static final String NUMERO_TEMP = "000000";

	private static SepaSddMandatHelper sharedInstance = new SepaSddMandatHelper();

	public static SepaSddMandatHelper getSharedInstance() {
		return sharedInstance;
	}


	private static NSMutableArray typePrelevementPossible = new NSMutableArray();
	static {
		typePrelevementPossible.addObject(new NSDictionary(new NSArray(new String[] {
				"R (Récurrent)", TYPE_PRELEVEMENT_CODE_RECURRENT
		}), new NSArray(new String[] {
				TYPE_PRELEVEMENT_LIBELLE_KEY, TYPE_PRELEVEMENT_CODE_KEY
		})));
		typePrelevementPossible.addObject(new NSDictionary(new NSArray(new String[] {
				"P (Ponctuel)", TYPE_PRELEVEMENT_CODE_PONCTUEL
		}), new NSArray(new String[] {
				TYPE_PRELEVEMENT_LIBELLE_KEY, TYPE_PRELEVEMENT_CODE_KEY
		})));
	}

	public NSArray typePrelevementPossibles() {
		return typePrelevementPossible.immutableClone();
	}

	public boolean hasEcheanciers(ISepaSddMandat mandat) {
		List<ISepaSddEcheancier> echeanciers = mandat.echeanciers();
		return  echeanciers != null && echeanciers.size() > 0;
	}

	public boolean estSigne(ISepaSddMandat mandat) {
		return mandat != null && mandat.dMandatSignature() != null;
	}

	public NSArray getEcheanciersTriesParDateCreationASC(ISepaSddMandat mandat) {
		return SepaSddEcheancierHelper.getSharedInstance().triLesEcheanciersParDateCreationASC(new NSArray(mandat.echeanciers().toArray()));
	}

	public NSArray getEcheancesTrieesParEtatDifferentDe(ISepaSddMandat mandat, Etat etat, boolean fetch) {
		NSMutableArray res = new NSMutableArray();
		for (ISepaSddEcheancier echeancier : mandat.echeanciers()) {
			res.addObjectsFromArray(echeancier.getEcheancesTrieesParDateEcheanceASCAvecEtatDifferentDe(etat, fetch));
		}
		return SepaSddEcheanceHelper.getSharedInstance().triLesEcheancesParDateEcheanceASC(res).immutableClone();
	}

	public NSArray getEcheancesTrieesParEtat(ISepaSddMandat mandat, Etat etat, boolean fetch) {
		NSMutableArray res = new NSMutableArray();
		for (ISepaSddEcheancier echeancier : mandat.echeanciers()) {
			res.addObjectsFromArray(echeancier.getEcheancesTrieesParDateEcheanceASCAvecEtat(etat, fetch));
		}
		return SepaSddEcheanceHelper.getSharedInstance().triLesEcheancesParDateEcheanceASC(res).immutableClone();
	}


	public NSArray getEcheancesTrieesParDateEcheanceASC(ISepaSddMandat mandat, boolean fetch) {
		NSMutableArray res = new NSMutableArray();
		for (ISepaSddEcheancier echeancier : mandat.echeanciers()) {
			res.addObjectsFromArray(new NSArray(echeancier.echeances().toArray() ) );
		}
		return SepaSddEcheanceHelper.getSharedInstance().triLesEcheancesParDateEcheanceASC(res).immutableClone();
	}

	public void invalidateMandatsEtSesEcheances(NSArray mandats, EOEditingContext ec) {
		CktlEOControlUtilities.invalidateObjects(mandats);
		for (int i = 0; i < mandats.count(); i++) {
			ISepaSddMandat mandat = (ISepaSddMandat) mandats.objectAtIndex(i);
			NSArray echeanciers = new NSArray(mandat.echeanciers().toArray());
			CktlEOControlUtilities.invalidateObjects(echeanciers);

			for (int j = 0; j < echeanciers.count(); j++) {
				ISepaSddEcheancier echeancier = (ISepaSddEcheancier) echeanciers.objectAtIndex(j);
				CktlEOControlUtilities.invalidateObjects(new NSArray(echeancier.echeances().toArray()));
			}
		}
	}


	public void validerMandat(EOEditingContext edc, ISepaSddMandat mandat) throws Exception {
		SepaSddMandatRule.getSharedInstance().checkMandatValidable(mandat);
		mandat.setToTypeEtatRelationship(IJefyAdminTypeEtatCtrl.getSharedInstance().getTypeEtat(edc, IJefyAdminTypeEtat.ETAT_VALIDE));
	}


	public NSArray getEcheancesNonAnnuleesTrieesParDateEcheanceASC(ISepaSddMandat mandat, boolean b) {
		NSMutableArray res = new NSMutableArray();
		for (ISepaSddEcheancier echeancier : mandat.echeanciers()) {
			NSArray echeances = SepaSddEcheancierHelper.getSharedInstance().getEcheancesNonAnnuleesTrieesParDateEcheanceASC(echeancier, false);
			res.addObjectsFromArray(echeances);
		}
		return SepaSddEcheanceHelper.getSharedInstance().triLesEcheancesParDateEcheanceASC(res).immutableClone();

	}

	public NSArray getEcheancesAttenteTrieesParDateEcheanceASC(ISepaSddMandat mandat, boolean b) {
		NSMutableArray res = new NSMutableArray();
		for (ISepaSddEcheancier echeancier : mandat.echeanciers()) {
			NSArray echeances = SepaSddEcheancierHelper.getSharedInstance().getEcheancesAttenteTrieesParDateEcheanceASC(echeancier, false);
			res.addObjectsFromArray(echeances);
		}
		return SepaSddEcheanceHelper.getSharedInstance().triLesEcheancesParDateEcheanceASC(res).immutableClone();

	}

	public NSArray getEcheancesRemisesTrieesParDateEcheanceASC(ISepaSddMandat mandat, boolean fetch) {
		NSMutableArray res = new NSMutableArray();
		for (ISepaSddEcheancier echeancier : mandat.echeanciers()) {
			NSArray echeances = SepaSddEcheancierHelper.getSharedInstance().getEcheancesRemisesTrieesParDateEcheanceASC(echeancier, fetch);
			res.addObjectsFromArray(echeances);
		}
		return SepaSddEcheanceHelper.getSharedInstance().triLesEcheancesParDateEcheanceASC(res).immutableClone();

	}

	public NSArray getEcheancesPreleveesOuConfirmeesTrieesParDateEcheanceASC(ISepaSddMandat mandat, boolean fetch) {
		NSMutableArray res = new NSMutableArray();
		for (ISepaSddEcheancier echeancier : mandat.echeanciers()) {
			NSArray echeances = SepaSddEcheancierHelper.getSharedInstance().getEcheancesPreleveesOuConfirmeesTrieesParDateEcheanceASC(echeancier, fetch);
			res.addObjectsFromArray(echeances);
		}
		return SepaSddEcheanceHelper.getSharedInstance().triLesEcheancesParDateEcheanceASC(res).immutableClone();

	}

	public NSArray getEcheancesPreleveesOuConfirmeesOuRejeteesTrieesParDateEcheanceASC(ISepaSddMandat mandat, boolean fetch) {
		NSMutableArray res = new NSMutableArray();
		for (ISepaSddEcheancier echeancier : mandat.echeanciers()) {
			NSArray echeances = SepaSddEcheancierHelper.getSharedInstance().getEcheancesPreleveesOuConfirmeesOuRejeteesTrieesParDateEcheanceASC(echeancier, fetch);
			res.addObjectsFromArray(echeances);
		}
		return SepaSddEcheanceHelper.getSharedInstance().triLesEcheancesParDateEcheanceASC(res).immutableClone();
	}

	public boolean supprimerMandat(ISepaSddMandat mandat, EOEditingContext editingContext) throws Exception {
		if (editingContext == null) {
			throw new IllegalArgumentException("L'editingContext est obligatoire");
		}

		SepaSddMandatRule.getSharedInstance().checkMandatSupprimable(mandat);
		mandat.setToTypeEtatRelationship(
				IJefyAdminTypeEtatCtrl.getSharedInstance().getTypeEtat(editingContext, IJefyAdminTypeEtat.ETAT_ANNULE));

		return true;
	}

	public void prepareBeforeSave(ISepaSddMandat mandat) {
		if (mandat.toSepaSddParam() != null) {
			mandat.setToCreancierPersonneRelationship(mandat.toSepaSddParam().toPersonneCreancier());
		}
		if (mandat.dCreation() == null) {
			mandat.setDCreation(new DateConversionUtil().formatDateWithTimeISO(new LocalDateTime()));
		}
		mandat.setDModification(new DateConversionUtil().formatDateWithTimeISO(new LocalDateTime()));
	}

	public boolean isSuppressionPossible(ISepaSddMandat mandat) {
		if (mandat == null) {
			throw new IllegalArgumentException("Le mandat est obligatoire");
		}

		return mandat.echeanciers().isEmpty();
	}

	public Boolean isValide(ISepaSddMandat mandat) {
		return IJefyAdminTypeEtat.ETAT_VALIDE.equals(mandat.toTypeEtat().tyetLibelle());
	}

	public Boolean isAnnule(ISepaSddMandat mandat) {
		return IJefyAdminTypeEtat.ETAT_ANNULE.equals(mandat.toTypeEtat().tyetLibelle());
	}

	public Boolean isAValider(ISepaSddMandat mandat) {
		return IJefyAdminTypeEtat.ETAT_A_VALIDER.equals(mandat.toTypeEtat().tyetLibelle());
	}

	public String getLibelleMandatAvecLabel(ISepaSddMandat mandat) {
		return LABEL_MANDAT_SDD + " " + getLibelleMandat(mandat);
	}

	public String getLibelleMandat(ISepaSddMandat mandat) {
		return mandat.numero();
	}

	public String getLibelleMandatComplet(ISepaSddMandat mandat) {
		return mandat.nomPrenomDebiteur() + " / " + getLibelleMandatAvecLabel(mandat);
	}


}
