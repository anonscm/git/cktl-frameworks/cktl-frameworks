package org.cocktail.fwkcktlcompta.common.sepasdd.helpers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;
import org.cocktail.fwkcktlcompta.common.entities.IModeRecouvrement;
import org.cocktail.fwkcktlcompta.common.helpers.EcritureDetailHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.IGenerateurEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.rules.SepaSddEcheancierRule;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.MutableDateTime;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class SepaSddEcheancierHelper {

	public static final String LABEL_ECHEANCIER = "Echéancier";

	private static SepaSddEcheancierHelper sharedInstance = new SepaSddEcheancierHelper();

	public static SepaSddEcheancierHelper getSharedInstance() {
		return sharedInstance;
	}

	public void supprimerEcheancier(ISepaSddEcheancier echeancier) {
		if (echeancier == null) {
			throw new IllegalArgumentException("Echeancier obligatoire");
		}

		SepaSddEcheancierRule.getSharedInstance().checkEcheancesSupprimables(echeancier);

		echeancier.supprimerEcheances();
		echeancier.setMandat(null);
	}

	public void annulerEcheance(ISepaSddEcheance echeance) {
		if (echeance == null) {
			throw new IllegalArgumentException("Echeance obligatoire");
		}
		echeance.setEtatEnum(Etat.ANNULE);
	}


	public void reporteMontantEcheanceSurSuivanteEnAttente(ISepaSddEcheance echeance) {
		if (echeance == null) {
			throw new IllegalArgumentException("Echeance obligatoire");
		}

		List<ISepaSddEcheance> echeances = new ArrayList<ISepaSddEcheance>();
		echeances = echeance.echeancier().echeances();
		Collections.sort(echeances, new TriEcheancesParDatePrevue());
		for (int i = 1; i <= echeances.size() - echeances.indexOf(echeance) - 1; i++) {
			if (echeances.get(echeances.indexOf(echeance) + i).etatAsEnum().equals(Etat.ATTENTE)) {
				echeances.get(echeances.indexOf(echeance) + i).setMontantAPayer(echeance.montantAPayer().add(echeances.get(echeances.indexOf(echeance) + i).montantAPayer()));
				break;
			}
		}
	}

	public void initialiserEcheances(ISepaSddEcheancier echeancier, IGenerateurEcheancier generateurEcheancier) {
		if (generateurEcheancier == null) {
			return;
		}

		if (!generateurEcheancier.peutGenererEcheances(echeancier)) {
			throw new IllegalArgumentException("La génération des échéances est impossible : conditions de génération non satisfaites.");
		}
		echeancier.supprimerEcheances();
		echeancier.ajouterEcheances(generateurEcheancier.genererEcheances(echeancier));
	}
	
	public ISepaSddEcheance creerNouvelleEcheance(ISepaSddEcheancier echeancier, LocalDate dateEcheance, BigDecimal montantEcheance, IGrhumPersonne createur) {
		ISepaSddEcheance echeance = echeancier.creerEcheance();
		echeance.setEcheancier(echeancier);
		echeance.setCreateurId(createur.persId());
		echeance.setDerniereModificationPar(createur.persId());
		echeance.setDateCreation(DateConversionUtil.sharedInstance().formatDateWithTimeISO(new LocalDateTime()));
		echeance.setDateModification(DateConversionUtil.sharedInstance().formatDateWithTimeISO(new LocalDateTime()));
		echeance.setEtatEnum(Etat.ATTENTE);
		echeance.setDatePrevue(DateConversionUtil.sharedInstance().formatDateWithoutTimeISO(dateEcheance));
		echeance.setToModificateurPersonne(createur);

		BigDecimal montantEcheanceValidee = BigDecimal.ZERO;
		if (montantEcheance != null) {
			montantEcheanceValidee = montantEcheance;
		}
		echeance.setMontantAPayer(montantEcheanceValidee.setScale(2, RoundingMode.HALF_UP));

		return echeance;
	}

	/**
	 * @param nbjoursMoisSuivant - le nombre de jour minimum qui sépare du mois suivant
	 * @param numeroJourEcheance - le numéro du jour dans le mois ou sera prélevée l'échéance
	 * @return la date de première échéance (MutableDateTime)
	 */
	public LocalDate calculeDatePremiereEcheanceAvecDateDuJour(int nbjoursMoisSuivant, int numeroJourEcheance) {

		MutableDateTime datePremiereEcheance = MutableDateTime.now();
		LocalDate dateDuJour = LocalDate.now();

		datePremiereEcheance.dayOfMonth().set(numeroJourEcheance);

		if (dateDuJour.dayOfMonth().get() >= (dateDuJour.dayOfMonth().getMaximumValue() + 1 - nbjoursMoisSuivant)) {
			datePremiereEcheance.addMonths(2);
		} else {
			datePremiereEcheance.addMonths(1);
		}

		return new LocalDate(datePremiereEcheance.toDateTime());
	}

	/**
	 * @param date - la date souhaitée qui servira de base au calcul de la première échéance
	 * @param nbjoursMoisSuivant - le nombre de jour minimum qui sépare du mois suivant
	 * @param numeroJourEcheance - le numéro du jour dans le mois ou sera prélevée l'échéance
	 * @return la date de première échéance (MutableDateTime)
	 */
	public LocalDate calculeDatePremiereEcheanceAvecDate(LocalDate date, int nbjoursMoisSuivant, int numeroJourEcheance) {

		MutableDateTime datePremiereEcheance = new MutableDateTime(date.toDateTimeAtStartOfDay());

		datePremiereEcheance.dayOfMonth().set(numeroJourEcheance);

		if (date.dayOfMonth().get() >= (date.dayOfMonth().getMaximumValue() + 1 - nbjoursMoisSuivant)) {
			datePremiereEcheance.addMonths(2);
		} else {
			datePremiereEcheance.addMonths(1);
		}

		return new LocalDate(datePremiereEcheance.toDateTime());
	}

	public boolean hasEcheances(ISepaSddEcheancier echeancier) {
		return echeancier != null && echeancier.echeances() != null && echeancier.echeances().size() > 0;
	}
	

	public static class BuilderEcheancier {
		private DateConversionUtil dateUtil;
		private IGenerateurEcheancier generateurEcheancier;
		private ISepaSddEcheancier echeancier;
		private ISepaSddMandat mandat;
		private Integer mandatId;
		private ISepaSddOrigineType origineType;
		private ISepaSddOrigineEntity origineEntity;
		private Integer origineEntityIdentifiant;
		private Integer persIdCreation;
		private IGrhumPersonne modificateur;
		private boolean genererEcheances;
		
		public BuilderEcheancier(ISepaSddEcheancier echeancier, Integer persIdCreation, IGrhumPersonne modificateur) {
			this.dateUtil = new DateConversionUtil();
			this.generateurEcheancier = null;
			this.echeancier = echeancier;
			this.persIdCreation = persIdCreation;
			this.modificateur = modificateur;
			this.genererEcheances = false;
		}

		public BuilderEcheancier mandat(ISepaSddMandat mandat) {
			this.mandat = mandat;
			return this;
		}

		public BuilderEcheancier mandatId(Integer mandatId) {
            this.mandatId = mandatId;
            return this;
        }
		
		public BuilderEcheancier origine(ISepaSddOrigineType origineType, ISepaSddOrigineEntity origineEntity) {
			this.origineType = origineType;
			this.origineEntity = origineEntity;
			return this;
		}
		
	      public BuilderEcheancier origine(ISepaSddOrigineType origineType, Integer origineEntityIdentifiant) {
	            this.origineType = origineType;
	            this.origineEntityIdentifiant = origineEntityIdentifiant;
	            return this;
	        }

		public BuilderEcheancier genererEcheances() {
			this.genererEcheances = true;
			return this;
		}

		public BuilderEcheancier generateurEcheancier(IGenerateurEcheancier generateurEcheancier) {
			this.generateurEcheancier = generateurEcheancier;
			return this;
		}

		public ISepaSddEcheancier build() {
			String todayAsString = dateUtil.formatDateWithTimeISO(new LocalDateTime());
			echeancier.setDateModification(todayAsString);
			gererOrigine(echeancier);

			if (mandat != null) {
				echeancier.setMandat(mandat);
				echeancier.setIdSepaSddMandat(mandat.idMandat());
			}

			if (mandatId != null) {
			    echeancier.setIdSepaSddMandat(mandatId);
			}
			
			if (echeancier.persIdCreation() == null) {
				echeancier.setPersIdCreation(persIdCreation);
			}

			if (echeancier.modificateur() == null) {
				echeancier.setToModificateurPersonne(modificateur);
			}

			if (echeancier.dateCreation() == null) {
				echeancier.setDateCreation(todayAsString);
			}

			if (echeancier.dateModification() == null) {
				echeancier.setDateModification(todayAsString);
			}

			if (genererEcheances) {
				SepaSddEcheancierHelper.getSharedInstance().initialiserEcheances(echeancier, generateurEcheancier);
			}

			return echeancier;
		}

		protected void gererOrigine(ISepaSddEcheancier echeancier) {
			if (originePeutEtreGeneree()) {
				ISepaSddOrigine origine = echeancier.creerOrigine();
				origine.setToSepaSddOrigineTypeRelationship(origineType);
				if (origineIdentifiantEstRenseignee()) {
					origine.setOrigineId(origineIdentifiant());
				}
				echeancier.setToSepaSddOrigineRelationship(origine);
			}
		}
		
		private boolean originePeutEtreGeneree() {
		    return origineType != null || origineEntity != null || origineEntityIdentifiant != null;
		    
		}
		
		private boolean origineIdentifiantEstRenseignee() {
		    return origineEntity != null || origineEntityIdentifiant != null;
		}
		
		private Integer origineIdentifiant() {
		    if (origineEntity != null) {
		        return origineEntity.origineIdentifiant();
		    }
		    
		    return origineEntityIdentifiant;
		}
	}

	public NSArray getEcheancesNonAnnuleesTrieesParDateEcheanceASC(ISepaSddEcheancier echeancier, boolean fetch) {
		return getEcheancesTrieesParDateEcheanceASCAvecEtatDifferentDe(echeancier, ISepaSddEcheance.Etat.ANNULE, fetch);
	}

	public NSArray getEcheancesAttenteTrieesParDateEcheanceASC(ISepaSddEcheancier echeancier, boolean fetch) {
		return getEcheancesTrieesParDateEcheanceASCAvecEtat(echeancier, ISepaSddEcheance.Etat.ATTENTE, fetch);
	}

	/**
	 * @param echeancier
	 * @param fetch
	 * @return Les echeances prelevees, confirmees ou rejetees.
	 */
	public NSArray getEcheancesRemisesTrieesParDateEcheanceASC(ISepaSddEcheancier echeancier, boolean fetch) {
		EOQualifier qual1 = new EOKeyValueQualifier(ISepaSddEcheance.I_ETAT_KEY, EOQualifier.QualifierOperatorEqual, ISepaSddEcheance.Etat.PRELEVE.toString());
		EOQualifier qual2 = new EOKeyValueQualifier(ISepaSddEcheance.I_ETAT_KEY, EOQualifier.QualifierOperatorEqual, ISepaSddEcheance.Etat.CONFIRME.toString());
		EOQualifier qual3 = new EOKeyValueQualifier(ISepaSddEcheance.I_ETAT_KEY, EOQualifier.QualifierOperatorEqual, ISepaSddEcheance.Etat.REJETE.toString());
		return getEcheancesTrieesParDateEcheanceASCAvecQualifier(echeancier, new EOOrQualifier(new NSArray(new Object[] {
				qual1, qual2, qual3
		})), fetch);
	}

	/**
	 * @param echeancier
	 * @param fetch
	 * @return Les echeances prelevees ou confirmees.
	 */
	public NSArray getEcheancesPreleveesOuConfirmeesTrieesParDateEcheanceASC(ISepaSddEcheancier echeancier, boolean fetch) {
		EOQualifier qual1 = new EOKeyValueQualifier(ISepaSddEcheance.I_ETAT_KEY, EOQualifier.QualifierOperatorEqual, ISepaSddEcheance.Etat.PRELEVE.toString());
		EOQualifier qual2 = new EOKeyValueQualifier(ISepaSddEcheance.I_ETAT_KEY, EOQualifier.QualifierOperatorEqual, ISepaSddEcheance.Etat.CONFIRME.toString());
		return getEcheancesTrieesParDateEcheanceASCAvecQualifier(echeancier, new EOOrQualifier(new NSArray(new Object[] {
				qual1, qual2
		})), fetch);
	}

	public NSArray getEcheancesPreleveesOuConfirmeesOuRejeteesTrieesParDateEcheanceASC(ISepaSddEcheancier echeancier, boolean fetch) {
		EOQualifier qual1 = new EOKeyValueQualifier(ISepaSddEcheance.I_ETAT_KEY, EOQualifier.QualifierOperatorEqual, ISepaSddEcheance.Etat.PRELEVE.toString());
		EOQualifier qual2 = new EOKeyValueQualifier(ISepaSddEcheance.I_ETAT_KEY, EOQualifier.QualifierOperatorEqual, ISepaSddEcheance.Etat.CONFIRME.toString());
		EOQualifier qual3 = new EOKeyValueQualifier(ISepaSddEcheance.I_ETAT_KEY, EOQualifier.QualifierOperatorEqual, ISepaSddEcheance.Etat.REJETE.toString());
		return getEcheancesTrieesParDateEcheanceASCAvecQualifier(echeancier, new EOOrQualifier(new NSArray(new Object[] {
				qual1, qual2, qual3
		})), fetch);
	}

	public NSArray getEcheancesTrieesParDateEcheanceASCAvecEtat(ISepaSddEcheancier echeancier, Etat etat, boolean fetch) {
		return getEcheancesTrieesParDateEcheanceASCAvecQualifier(echeancier, new EOKeyValueQualifier(ISepaSddEcheance.I_ETAT_KEY, EOQualifier.QualifierOperatorEqual, etat.toString()), fetch);
	}

	public NSArray getEcheancesTrieesParDateEcheanceASCAvecEtatDifferentDe(ISepaSddEcheancier echeancier, Etat etat, boolean fetch) {
		return getEcheancesTrieesParDateEcheanceASCAvecQualifier(echeancier, new EOKeyValueQualifier(ISepaSddEcheance.I_ETAT_KEY, EOQualifier.QualifierOperatorNotEqual, etat.toString()), fetch);
	}


	public NSArray getEcheancesTrieesParDateEcheanceASCAvecQualifier(ISepaSddEcheancier echeancier, EOQualifier qual, boolean fetch) {
		return EOQualifier.filteredArrayWithQualifier(getEcheancesTrieesParDateEcheanceASC(echeancier, fetch), qual);
	}

	public NSArray getEcheancesTrieesParDateEcheanceASC(ISepaSddEcheancier echeancier, boolean fetch) {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(new NSArray(echeancier.echeances().toArray()), new NSArray(SepaSddEcheanceHelper.SORT_DATE_ECHEANCE_ASC));
	}



	public NSArray triLesEcheanciersParDateCreationASC(NSArray echeanciers) {
		NSArray sortedRes = EOSortOrdering.sortedArrayUsingKeyOrderArray(echeanciers, new NSArray(new Object[] {
				ISepaSddEcheancier.SORT_DATE_CREATION_ASC
		}));
		return sortedRes;
	}

	public ISepaSddEcheance getPremiereEcheanceNonAnnulee(ISepaSddEcheancier echeancier) {
		NSArray res = getEcheancesTrieesParDateEcheanceASCAvecEtatDifferentDe(echeancier, ISepaSddEcheance.Etat.ANNULE, false);
		if (res.count() == 0) {
			return null;
		}
		return (ISepaSddEcheance) res.objectAtIndex(0);
	}

	public Integer getNombreEcheancesNonAnnulees(ISepaSddEcheancier echeancier) {
		return getEcheancesNonAnnuleesTrieesParDateEcheanceASC(echeancier, false).count();
	}

	public String getLibelleEcheancier(ISepaSddEcheancier echeancier) {
		ISepaSddOrigineEntity origineEntity = echeancier.toSepaSddOrigine().toEntity();
		return origineEntity.getOrigineLibelleComplet();
	}

	public String getLibelleMandatEtEcheancier(ISepaSddEcheancier echeancier) {
		return SepaSddMandatHelper.getSharedInstance().getLibelleMandatAvecLabel(echeancier.mandat())
				+ " / " + LABEL_ECHEANCIER + " "
				+ getLibelleEcheancier(echeancier);
	}

	public String getLibelleEcheancierComplet(ISepaSddEcheancier echeancier) {
		return SepaSddMandatHelper.getSharedInstance().getLibelleMandatComplet(echeancier.mandat()) + " / " + LABEL_ECHEANCIER + " " + getLibelleEcheancier(echeancier);
	}

	public IModeRecouvrement getModeRecouvrement(ISepaSddEcheancier echeancier) {
		return echeancier.toSepaSddOrigine().toEntity().modeRecouvrement();
	}

	/**
	 * @param echeancier
	 * @return L'écriture de prise en charge la plus récente.
	 */
	public IEcritureDetail getLastEcritureDetailPriseEnCharge(ISepaSddEcheancier echeancier) {
		NSArray ecds = echeancier.toEcritureDetailsPriseEncharge();
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(IEcritureDetail.I_TO_EXERCICE_KEY + "." + IJefyAdminExercice.I_EXE_EXERCICE_KEY, EOSortOrdering.CompareDescending);
		ecds = EOSortOrdering.sortedArrayUsingKeyOrderArray(ecds, new NSArray(new Object[] {
			sort
		}));
		if (ecds.count() > 0) {
		return (IEcritureDetail) ecds.objectAtIndex(0);
		}
		return null;
	}

	public List<IEcritureDetail> getDebitsEcriturePriseEnChargeOrigineSurExercice(ISepaSddEcheancier echeancier, IJefyAdminExercice exercice) throws Exception {
		//on utilise comme compte celui de contrepartie de la prise en charge de l'origine de l'échéancier (passée donc en débit)
		//Exception si l'ecriture de prise en charge n'est pas trouvée
		List<IEcritureDetail> ecds = echeancier.toSepaSddOrigine().toEntity().ecritureDetailsContrepartiePriseEnCharge();
		ecds = EcritureDetailHelper.getSharedInstance().filtrerSelonExercice(ecds, exercice);
		if (ecds.size() == 0) {
			throw new Exception("Le(s) débit(s) d'écriture correspondant à la contrepartie de la prise en charge de l'origine de l'échéancier (" + SepaSddEcheancierHelper.getSharedInstance().getLibelleEcheancier(echeancier) + ") n'a pas été trouvée.");
		}
		return ecds;
	}


	public boolean isEcheancierPrisEnCharge(ISepaSddEcheancier echeancier) {
		return getLastEcritureDetailPriseEnCharge(echeancier) != null;
	}


}
