package org.cocktail.fwkcktlcompta.common.sepasdd.helpers;

import java.util.Comparator;

import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;

public class TriEcritureDetailParDate implements Comparator<IEcritureDetail> {

	public int compare(IEcritureDetail ecd1, IEcritureDetail ecd2) {
		if (ecd1 == ecd2) {
			return 0;
		}

		if (ecd1.toEcriture().toExercice().exeExercice().intValue() < ecd2.toEcriture().toExercice().exeExercice().intValue()) {
			return -1;
		}
		if (ecd1.toEcriture().toExercice().exeExercice().intValue() > ecd2.toEcriture().toExercice().exeExercice().intValue()) {
			return 1;
		}

		if (ecd1.toEcriture().ecrNumero().intValue() < ecd2.toEcriture().ecrNumero().intValue()) {
			return -1;
		}
		if (ecd1.toEcriture().ecrNumero().intValue() > ecd2.toEcriture().ecrNumero().intValue()) {
			return 1;
		}
		return ecd1.toEcriture().ecrDate().compareTo(ecd2.toEcriture().ecrDate());

	}

}
