package org.cocktail.fwkcktlcompta.common.sepasdd.helpers;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddParam;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ctrl.IsepaSddParamCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class SepaSddParamHelper {
	private static SepaSddParamHelper sharedInstance = new SepaSddParamHelper();

	public static SepaSddParamHelper getSharedInstance() {
		return sharedInstance;
	}

	public NSArray getAllParamsValides(EOEditingContext edc) throws Exception {
		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(ISepaSddParam.I_CREANCIER_ICS_KEY, EOSortOrdering.CompareAscending));
		return IsepaSddParamCtrl.getSharedInstance().fetchAll(edc, new EOKeyValueQualifier(ISepaSddParam.I_ETAT_KEY, EOQualifier.QualifierOperatorEqual, "VALIDE"), sorts);
	}

}
