package org.cocktail.fwkcktlcompta.common.sepasdd.origines;


/**
 * Classe controleur de l'entité Litchi.EnteteFacture, typee Hebergement, pour la gestion des origines. 
 * Cette classe peut etre instanciée coté client ou serveur et ne doit donc pas faire reference directe à une classe uniquement client ou serveur.
 *
 */
public class SepaSddOrigineCtrlLitchiEnteteFactureHebergement extends SepaSddOrigineCtrlLitchiEnteteFacture {

    private static final String TYPE_FACTURE = "HEBERGEMENT";

    @Override
    public String typeFacture() {
        return TYPE_FACTURE;
    }
}
