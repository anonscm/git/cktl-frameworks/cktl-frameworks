package org.cocktail.fwkcktlcompta.common.sepasdd.origines;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;

public enum OrigineComplements {

	TO_DEBITEUR_PERSONNE(ISepaSddMandat.TO_I_DEBITEUR_PERSONNE_KEY),
	TO_DEBITEUR_ADRESSE(ISepaSddMandat.TO_I_DEBITEUR_ADRESSE_KEY),
	TO_DEBITEUR_RIB(ISepaSddMandat.TO_I_DEBITEUR_RIB_KEY);

	private String cle;

	private OrigineComplements(String cle) {
		this.cle = cle;
	}

}
