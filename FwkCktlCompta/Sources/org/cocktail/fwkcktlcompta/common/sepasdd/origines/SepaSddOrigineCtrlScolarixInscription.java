package org.cocktail.fwkcktlcompta.common.sepasdd.origines;

import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddOrigineFilters;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe controleur de l'entité ScolarixInscription, pour la gestion des origines. Cette classe peut etre instanciée coté client ou serveur et ne
 * doit donc pas faire reference directe à une classe uniquement client ou serveur.
 * 
 * @author rprin
 */
public class SepaSddOrigineCtrlScolarixInscription extends CommonSepaSddOrigineEntityCtrl {
	public static final String ENTITY_NAME = "FwkCktlCompta_ScolarixInscription";
	public static final String PRIMARY_KEY_ATTRIBUTE_NAME = "identifiant";

	@Override
	protected String entityName() {
		return ENTITY_NAME;
	}


	@Override
	public String primaryKeyAttributeName() {
		return PRIMARY_KEY_ATTRIBUTE_NAME;
	}

	public String serverClassNameForEntity() {
		return "org.cocktail.fwkcktlcompta.server.metier.EOScolarixInscription";
	}

	public String clientClassNameForEntity() {
		return "org.cocktail.fwkcktlcompta.client.metier.EOScolarixInscription";
	}

	public EOQualifier buildQualifierFromFilters(SepaSddOrigineFilters filters) {
		NSMutableArray quals = new NSMutableArray();
		if (filters.get(SepaSddOrigineFilters.EXERCICE_KEY) != null) {
			quals.addObject(new EOKeyValueQualifier("toExercice.exeExercice", EOQualifier.QualifierOperatorEqual, filters.get(SepaSddOrigineFilters.EXERCICE_KEY)));
		}
		if (filters.get(SepaSddOrigineFilters.DEBITEUR_PERSONNE_KEY) != null) {
			quals.addObject(new EOKeyValueQualifier("toPersonne",  EOQualifier.QualifierOperatorEqual, filters.get(SepaSddOrigineFilters.DEBITEUR_PERSONNE_KEY)));
		}
		if (filters.get(SepaSddOrigineFilters.NUMERO_MIN) != null) {
			quals.addObject(new EOKeyValueQualifier("etudNumero", EOQualifier.QualifierOperatorGreaterThanOrEqualTo, (Number) (filters.get(SepaSddOrigineFilters.NUMERO_MIN))));
		}
		if (filters.get(SepaSddOrigineFilters.NUMERO_MAX) != null) {
			quals.addObject(new EOKeyValueQualifier("etudNumero", EOQualifier.QualifierOperatorLessThanOrEqualTo, (Number) (filters.get(SepaSddOrigineFilters.NUMERO_MAX))));
		}
		//		if (filters.get(SepaSddOrigineFilters.OBJET_KEY) != null) {
		//			quals.addObject(new EOKeyValueQualifier("fapLib", EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filters.get(SepaSddOrigineFilters.OBJET_KEY) + "*"));
		//		}
		if (filters.get(SepaSddOrigineFilters.MONTANT_MIN) != null) {
			quals.addObject(new EOKeyValueQualifier("paieSomme", EOQualifier.QualifierOperatorGreaterThanOrEqualTo, (Number) (filters.get(SepaSddOrigineFilters.MONTANT_MIN))));
		}
		if (filters.get(SepaSddOrigineFilters.MONTANT_MAX) != null) {
			quals.addObject(new EOKeyValueQualifier("paieSomme", EOQualifier.QualifierOperatorLessThanOrEqualTo, (Number) (filters.get(SepaSddOrigineFilters.MONTANT_MAX))));
		}

		EOQualifier qual = new EOAndQualifier(quals);
		return qual;
	}

	public NSArray buildSortOrderings() {
		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("histNumero", EOSortOrdering.CompareDescending));
		return sorts;
	}


	public NSArray fetchSpec(EOEditingContext editingContext, SepaSddOrigineFilters filters, int numberMaxOfResult) throws Exception {
		EOQualifier qual = buildQualifierFromFilters(filters);
		NSArray sorts = buildSortOrderings();
		EOFetchSpecification spec = new EOFetchSpecification(entityName(), qual, new NSArray(sorts));
		spec.setFetchLimit(numberMaxOfResult);
		NSArray res = editingContext.objectsWithFetchSpecification(spec);
		return res;
	}

}
