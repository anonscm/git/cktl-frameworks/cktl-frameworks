package org.cocktail.fwkcktlcompta.common.sepasdd.origines;

import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddOrigineFilters;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe controleur de l'entité Litchi.EnteteFacture, pour la gestion des origines. 
 * Cette classe peut etre instanciée coté client ou serveur et ne doit donc pas faire reference directe à une classe uniquement client ou serveur.
 *
 */
public abstract class SepaSddOrigineCtrlLitchiEnteteFacture extends CommonSepaSddOrigineEntityCtrl {

	public static final String ENTITY_NAME = "FwkCktlCompta_Litchi_EnteteFacture";
	public static final String PRIMARY_KEY_ATTRIBUTE_NAME = "idFacture";
	
	private static final String CLIENT_CLASSNAME_ENTITY = "org.cocktail.fwkcktlcompta.client.metier.EOLitchiEnteteFacture";
	private static final String SERVER_CLASSNAME_ENTITY = "org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture";
	
	public abstract String typeFacture();
	
	@Override
	protected String entityName() {
		return ENTITY_NAME;
	}

	@Override
	public EOQualifier buildQualifierFromFilters(SepaSddOrigineFilters filters) {
		NSMutableArray quals = new NSMutableArray();
		if (filters.get(SepaSddOrigineFilters.EXERCICE_KEY) != null) {
			
		}
		if (filters.get(SepaSddOrigineFilters.DEBITEUR_PERSONNE_KEY) != null) {
			quals.addObject(new EOKeyValueQualifier("toPersonne",  EOQualifier.QualifierOperatorEqual, filters.get(SepaSddOrigineFilters.DEBITEUR_PERSONNE_KEY)));
		}
		if (filters.get(SepaSddOrigineFilters.NUMERO_MIN) != null) {
			quals.addObject(new EOKeyValueQualifier("numFacture", EOQualifier.QualifierOperatorGreaterThanOrEqualTo, (Number) (filters.get(SepaSddOrigineFilters.NUMERO_MIN))));
		}
		if (filters.get(SepaSddOrigineFilters.NUMERO_MAX) != null) {
			quals.addObject(new EOKeyValueQualifier("numFacture", EOQualifier.QualifierOperatorLessThanOrEqualTo, (Number) (filters.get(SepaSddOrigineFilters.NUMERO_MAX))));
		}
		if (filters.get(SepaSddOrigineFilters.OBJET_KEY) != null) {
			quals.addObject(new EOKeyValueQualifier("libFacture", EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filters.get(SepaSddOrigineFilters.OBJET_KEY) + "*"));
		}
		if (filters.get(SepaSddOrigineFilters.MONTANT_MIN) != null) {
			quals.addObject(new EOKeyValueQualifier("mntAPayerTtc", EOQualifier.QualifierOperatorGreaterThanOrEqualTo, (Number) (filters.get(SepaSddOrigineFilters.MONTANT_MIN))));
		}
		if (filters.get(SepaSddOrigineFilters.MONTANT_MAX) != null) {
			quals.addObject(new EOKeyValueQualifier("mntAPayerTtc", EOQualifier.QualifierOperatorLessThanOrEqualTo, (Number) (filters.get(SepaSddOrigineFilters.MONTANT_MAX))));
		}
		
		String typeFacture = typeFacture();
		if (typeFacture != null) {
		    quals.addObject(new EOKeyValueQualifier("toTypeFacture.libTypeFacture", EOQualifier.QualifierOperatorEqual, typeFacture));
		}

		EOQualifier qual = new EOAndQualifier(quals);
		return qual;
	}

	@Override
	public NSArray buildSortOrderings() {
		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("numFacture", EOSortOrdering.CompareDescending));
		return sorts;
	}

	@Override
	public String primaryKeyAttributeName() {
		return PRIMARY_KEY_ATTRIBUTE_NAME;
	}

	@Override
	protected String clientClassNameForEntity() {
		return CLIENT_CLASSNAME_ENTITY;
	}

	@Override
	protected String serverClassNameForEntity() {
		return SERVER_CLASSNAME_ENTITY;
	}
}
