package org.cocktail.fwkcktlcompta.common.sepasdd.origines;

import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddOrigineFilters;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe controleur de l'entité JefyRecetteRecettePapier, pour la gestion des origines. Cette classe peut etre instanciée coté client ou serveur et ne
 * doit donc pas faire reference directe à une classe uniquement client ou serveur.
 *
 * @author rprin
 */
public class SepaSddOrigineCtrlJefyRecetteRecettePapier extends CommonSepaSddOrigineEntityCtrl {
	public static final String ENTITY_NAME = "FwkCktlCompta_JefyRecette_RecettePapier";
	public static final String PRIMARY_KEY_ATTRIBUTE_NAME = "rppId";

	@Override
	protected String entityName() {
		return ENTITY_NAME;
	}

	@Override
	public String primaryKeyAttributeName() {
		return PRIMARY_KEY_ATTRIBUTE_NAME;
	}

	public String serverClassNameForEntity() {
		return "org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier";
	}

	public String clientClassNameForEntity() {
		return "org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecettePapier";
	}

	public EOQualifier buildQualifierFromFilters(SepaSddOrigineFilters filters) {
		NSMutableArray quals = new NSMutableArray();
		if (filters.get(SepaSddOrigineFilters.EXERCICE_KEY) != null) {
			quals.addObject(new EOKeyValueQualifier("toExercice.exeExercice", EOQualifier.QualifierOperatorEqual, filters.get(SepaSddOrigineFilters.EXERCICE_KEY)));
		}
		if (filters.get(SepaSddOrigineFilters.DEBITEUR_PERSONNE_KEY) != null) {
			quals.addObject(new EOKeyValueQualifier("toPersonne",  EOQualifier.QualifierOperatorEqual, filters.get(SepaSddOrigineFilters.DEBITEUR_PERSONNE_KEY)));
		}
		if (filters.get(SepaSddOrigineFilters.NUMERO_MIN) != null) {
			quals.addObject(new EOKeyValueQualifier("rppNumero", EOQualifier.QualifierOperatorGreaterThanOrEqualTo, (Number)(filters.get(SepaSddOrigineFilters.NUMERO_MIN))));
		}
		if (filters.get(SepaSddOrigineFilters.NUMERO_MAX) != null) {
			quals.addObject(new EOKeyValueQualifier("rppNumero", EOQualifier.QualifierOperatorLessThanOrEqualTo, (Number)(filters.get(SepaSddOrigineFilters.NUMERO_MAX))));
		}
		if (filters.get(SepaSddOrigineFilters.OBJET_KEY) != null) {
			quals.addObject(new EOKeyValueQualifier("rppLib", EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filters.get(SepaSddOrigineFilters.OBJET_KEY) + "*"));
		}
		if (filters.get(SepaSddOrigineFilters.MONTANT_MIN) != null) {
			quals.addObject(new EOKeyValueQualifier("rppTotalTtc", EOQualifier.QualifierOperatorGreaterThanOrEqualTo, (Number)(filters.get(SepaSddOrigineFilters.MONTANT_MIN))));
		}
		if (filters.get(SepaSddOrigineFilters.MONTANT_MAX) != null) {
			quals.addObject(new EOKeyValueQualifier("rppTotalTtc", EOQualifier.QualifierOperatorLessThanOrEqualTo, (Number)(filters.get(SepaSddOrigineFilters.MONTANT_MAX))));
		}

		EOQualifier qual = new EOAndQualifier(quals);
		return qual;
	}

	public NSArray buildSortOrderings() {
		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("toExercice.exeExercice", EOSortOrdering.CompareDescending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("rppNumero", EOSortOrdering.CompareDescending));
		return sorts;
	}

}
