package org.cocktail.fwkcktlcompta.common.sepasdd.origines;

/**
 * Classe controleur de l'entité Litchi.EnteteFacture, typee Restauration, pour la gestion des origines. 
 * Cette classe peut etre instanciée coté client ou serveur et ne doit donc pas faire reference directe à une classe uniquement client ou serveur.
 *
 */
public class SepaSddOrigineCtrlLitchiEnteteFactureRestauration extends SepaSddOrigineCtrlLitchiEnteteFacture {
    
    private static final String TYPE_FACTURE = "RESTAURATION";

    @Override
    public String typeFacture() {
        return TYPE_FACTURE;
    }
}
