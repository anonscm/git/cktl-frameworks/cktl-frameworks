package org.cocktail.fwkcktlcompta.common.sepasdd.origines;

import org.cocktail.fwkcktlcompta.common.entities.ctrl.CommonEntityCtrl;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddOrigineFilters;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public abstract class CommonSepaSddOrigineEntityCtrl extends CommonEntityCtrl {

	public NSArray fetchAll(EOEditingContext editingContext, SepaSddOrigineFilters filters, NSArray sorts) throws Exception {
		EOQualifier qual = buildQualifierFromFilters(filters);
		return fetchAll(editingContext, qual, sorts);
	}

	public ISepaSddOrigineEntity fetchByPrimaryKey(EOEditingContext editingContext, Integer origineId) throws Exception {
		return (ISepaSddOrigineEntity) fetchByKeyValue(editingContext, primaryKeyAttributeName(), origineId);
	}


	public NSArray fetchSpec(EOEditingContext editingContext, SepaSddOrigineFilters filters, int numberMaxOfResult) throws Exception {
		EOQualifier qual = buildQualifierFromFilters(filters);
		NSArray sorts = buildSortOrderings();
		EOFetchSpecification spec = new EOFetchSpecification(entityName(), qual, new NSArray(sorts));
		spec.setFetchLimit(numberMaxOfResult);
		NSArray res = editingContext.objectsWithFetchSpecification(spec);
		return res;
	}

	public abstract EOQualifier buildQualifierFromFilters(SepaSddOrigineFilters filters);

	public abstract NSArray buildSortOrderings();

	public abstract String primaryKeyAttributeName();

}
