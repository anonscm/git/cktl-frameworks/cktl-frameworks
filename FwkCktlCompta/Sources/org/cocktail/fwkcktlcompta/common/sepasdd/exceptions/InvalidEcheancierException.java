package org.cocktail.fwkcktlcompta.common.sepasdd.exceptions;

public class InvalidEcheancierException extends Exception {

	public InvalidEcheancierException() {
		super();
	}

	public InvalidEcheancierException(String message) {
		super(message);
	}

}
