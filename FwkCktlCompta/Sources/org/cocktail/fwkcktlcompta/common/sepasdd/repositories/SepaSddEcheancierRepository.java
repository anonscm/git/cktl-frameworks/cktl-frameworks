package org.cocktail.fwkcktlcompta.common.sepasdd.repositories;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;

public class SepaSddEcheancierRepository {

    public static ISepaSddEcheancier creer(EOEditingContext editingContext) {
        return (ISepaSddEcheancier) EOUtilities.createAndInsertInstance(editingContext, ISepaSddEcheancier.I_ENTITY_NAME);
    }
    
}
