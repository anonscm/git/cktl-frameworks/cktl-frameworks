package org.cocktail.fwkcktlcompta.common.sepasdd.entities.ctrl;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.ctrl.CommonEntityCtrl;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.util.CktlEOControlUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public class ISepaSddMandatCtrl extends CommonEntityCtrl {

	private static final ISepaSddMandatCtrl sharedInstance = new ISepaSddMandatCtrl();

	public String serverClassNameForEntity() {
		return "org.cocktail.fwkcktlcompta.server.metier.EOSepaSddMandat";
	}

	public String clientClassNameForEntity() {
		return "org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat";
	}

	@Override
	protected String entityName() {
		return "FwkCktlCompta_SepaSddMandat";
	}

	public static final ISepaSddMandatCtrl getSharedInstance() {
		return sharedInstance;
	}

	public EOQualifier filtreMandatsAvecEcheanciers(ISepaSddOrigine origine) throws Exception {
		return new EOKeyValueQualifier(
				ISepaSddMandat.TO_I_SDD_ECHEANCIERS_KEY + "." + ISepaSddEcheancier.TO_ISEPA_SDD_ORIGINE_KEY,
				EOQualifier.QualifierOperatorEqual,
				origine);
	}

	public NSArray mandatsAvecEcheanciers(EOEditingContext ec, ISepaSddOrigine origine) throws Exception {
		return fetchAll(ec, filtreMandatsAvecEcheanciers(origine), null);
	}

	public EOQualifier filtreMandatsPourDebiteurOuTiersDebiteur(IGrhumPersonne debiteur) throws Exception {
		EOQualifier mandatsEnTantQueTiersDebiteur = new EOKeyValueQualifier(
				ISepaSddMandat.TO_I_TIERS_DEBITEUR_PERSONNE_KEY,
				EOQualifier.QualifierOperatorEqual,
				debiteur);

		EOQualifier mandatsEnTantQueDebiteur = new EOKeyValueQualifier(
				ISepaSddMandat.TO_I_DEBITEUR_PERSONNE_KEY,
				EOQualifier.QualifierOperatorEqual,
				debiteur);

		return CktlEOControlUtilities.orQualifier(mandatsEnTantQueTiersDebiteur, mandatsEnTantQueDebiteur);
	}

	public NSArray mandatsPourDebiteurOuTiersDebiteur(EOEditingContext ec, IGrhumPersonne debiteur) throws Exception {
		return fetchAll(ec, filtreMandatsPourDebiteurOuTiersDebiteur(debiteur), null);
	}
}
