package org.cocktail.fwkcktlcompta.common.sepasdd.entities;

import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IFwkCktlComptaEntity;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumAdresse;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumRib;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminTypeEtat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;

public interface ISepaSddMandat extends IFwkCktlComptaEntity {
	enum TypePrelevement {
		R, P
	}

	String DEBITEUR_NOM_PRENOM = "debiteurNomPrenom";
	String DEBITEUR_BIC_IBAN = "debiteurBicIban";
	String TIERS_DEBITEUR_NOM_PRENOM = "tiersDebiteurNomPrenom";
	String TO_I_TYPE_ETAT_KEY = "toTypeEtat";
	String TO_I_SDD_ECHEANCIERS_KEY = "toSepaSddEcheanciers";
	String TO_I_TIERS_DEBITEUR_PERSONNE_KEY = "toTiersDebiteurPersonne";
	String TO_I_DEBITEUR_PERSONNE_KEY = "toDebiteurPersonne";
	String TO_I_DEBITEUR_ADRESSE_KEY = "toDebiteurAdresse";
	String TO_I_DEBITEUR_RIB_KEY = "toDebiteurRib";

	String PRINT_RUM_KEY = "RUM_SEPA_SDD_MANDAT";

	Integer idMandat();
	
	String libelle();
	
	void setLibelle(String libelle);

	String commentaire();

	String rum();

	void setRum(String rum);

	String nomPrenomDebiteur();

	String tiersDebiteurNomPrenom();

	String bicIbanDebiteur();

	String dCreation();

	void setDCreation(String dateCreation);

	String dModification();

	void setDModification(String dateModification);

	String dMandatCreation();

	void setDMandatCreation(String dateCreation);

	String dMandatSignature();

	void setDMandatSignature(String dateSignature);

	String cTypePrelevement();

	List<ISepaSddEcheancier> echeanciers();

	EOEditingContext editingContext();

	String numero();

	ISepaSddParam toSepaSddParam();

	IGrhumPersonne toCreancierPersonne();

	String refAppliCreation();

	void setRefAppliCreation(String refAppliCreation);

	IGrhumPersonne toDebiteurPersonne();

	IGrhumPersonne toTiersDebiteurPersonne();

	IGrhumRib toDebiteurRib();

	NSArray toSepaSddMandatHistos();

	/**
	 * Retourne le client (debiteur) à l'origine du paiement.
	 * Il s'agit du Tiers Debiteur si renseigné ; du debiteur le cas echeant.
	 * @return le tiers debiteur si non vide ; le debiteur sinon.
	 */
	IGrhumPersonne clientPersonne();

	Boolean isRecurrent();

	void setToTypeEtatRelationship(IJefyAdminTypeEtat typeEtat);

	void setToModificateurRelationship(IGrhumPersonne personne);

	void setToCreancierPersonneRelationship(IGrhumPersonne personne);

	void setToDebiteurPersonneRelationship(IGrhumPersonne personne);
	
	void setToDebiteurRibRelationship(IGrhumRib rib);
	
	void setToSepaSddParamRelationship(ISepaSddParam param);

    IGrhumAdresse toDebiteurAdresse();

	void setToDebiteurAdresseRelationship(IGrhumAdresse adresse);

	void setToTiersDebiteurPersonneRelationship(IGrhumPersonne personne);

	IJefyAdminTypeEtat toTypeEtat();

	EOEnterpriseObject toEnterpriseObject();
	
	void setCTypePrelevement(String type);
}
