package org.cocktail.fwkcktlcompta.common.sepasdd.entities.ctrl;

import org.cocktail.fwkcktlcompta.common.entities.ctrl.CommonEntityCtrl;

public class IsepaSddParamCtrl extends CommonEntityCtrl {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddParam";
	public static final String CLIENT_CLASS_ENTITY_NAME = "org.cocktail.fwkcktlcompta.client.metier.EOSepaSddParam";
	public static final String SERVER_CLASS_ENTITY_NAME = "org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam";

	private static IsepaSddParamCtrl sharedInstance = new IsepaSddParamCtrl();

	public static final IsepaSddParamCtrl getSharedInstance() {
		return sharedInstance;
	}
	@Override
	protected String clientClassNameForEntity() {
		return CLIENT_CLASS_ENTITY_NAME;
	}

	@Override
	protected String serverClassNameForEntity() {
		return SERVER_CLASS_ENTITY_NAME;
	}

	@Override
	protected String entityName() {
		return ENTITY_NAME;
	}

}
