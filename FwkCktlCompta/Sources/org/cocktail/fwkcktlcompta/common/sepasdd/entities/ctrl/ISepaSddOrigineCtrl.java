package org.cocktail.fwkcktlcompta.common.sepasdd.entities.ctrl;

import org.cocktail.fwkcktlcompta.common.entities.ctrl.CommonEntityCtrl;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.util.CktlEOControlUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class ISepaSddOrigineCtrl extends CommonEntityCtrl {

	private static final ISepaSddOrigineCtrl sharedInstance = new ISepaSddOrigineCtrl();

	public String serverClassNameForEntity() {
		return "org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigine";
	}

	public String clientClassNameForEntity() {
		return "org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigine";
	}

	@Override
	protected String entityName() {
		return "FwkCktlCompta_SepaSddOrigine";
	}

	public static final ISepaSddOrigineCtrl getSharedInstance() {
		return sharedInstance;
	}

	public ISepaSddOrigine fetchOrigine(EOEditingContext ec, ISepaSddOrigineType origineType, ISepaSddOrigineEntity origineEntity) throws Exception {
		return fetchOrigine(ec, origineType, origineEntity.origineIdentifiant());
	}
	
	public ISepaSddOrigine fetchOrigine(EOEditingContext ec, ISepaSddOrigineType origineType, Integer origineId) throws Exception {
		EOQualifier qual1 = new EOKeyValueQualifier(ISepaSddOrigine.I_TO_SEPA_SDD_ORIGINE_TYPE_KEY, EOQualifier.QualifierOperatorEqual, origineType);
		EOQualifier qual2 = new EOKeyValueQualifier(ISepaSddOrigine.I_ORIGINE_ID_KEY, EOQualifier.QualifierOperatorEqual, origineId);
		NSArray origines = fetchAll(ec, CktlEOControlUtilities.andQualifier(qual1, qual2), null);
		if (origines.count() > 0) {
			return (ISepaSddOrigine) origines.objectAtIndex(0);
		}
		return null;
	}

	public ISepaSddOrigine creerInstance(EOEditingContext ec) {
		ISepaSddOrigine origine = (ISepaSddOrigine) createAndInsertInstance(ec, entityName());
		return origine;
	}



}
