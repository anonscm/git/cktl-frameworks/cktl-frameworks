package org.cocktail.fwkcktlcompta.common.sepasdd.entities;

import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.entities.IFwkCktlComptaEntity;

public interface ISepaSddEcheanceEcd extends IFwkCktlComptaEntity {
	IEcritureDetail toEcritureDetail();

	ISepaSddEcheance toSepaSddEcheance();
}
