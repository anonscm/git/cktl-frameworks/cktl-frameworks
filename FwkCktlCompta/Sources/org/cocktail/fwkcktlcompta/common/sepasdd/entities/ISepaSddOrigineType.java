package org.cocktail.fwkcktlcompta.common.sepasdd.entities;

import org.cocktail.fwkcktlcompta.common.entities.IFwkCktlComptaEntity;

public interface ISepaSddOrigineType extends IFwkCktlComptaEntity {

    String I_ENTITY_NAME = "FwkCktlCompta_SepaSddOrigineType";
	String I_TYPE_CODE_KEY = "typeCode";
	
	String FACTURE = "FAC";
	String RECETTE = "REC";
	String LITCHI_HEBERGEMENT   = "HEB";
	String LITCHI_RESTAURATION  = "RST";
	String ETAT_VALIDE = "O";
	String GENERATION_AUTOMATIQUE_ECRITURES = "O";

	String typeClass();

	String typeNom();

	String typeCode();

	boolean isGenerationEcrituresAutomatique();
}
