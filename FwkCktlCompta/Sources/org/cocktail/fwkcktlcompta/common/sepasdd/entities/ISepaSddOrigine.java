package org.cocktail.fwkcktlcompta.common.sepasdd.entities;

import org.cocktail.fwkcktlcompta.common.entities.IFwkCktlComptaEntity;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Interface implémentée par la classe serveur {@link org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigine}et la classe cliente
 * {@link org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigine}
 * 
 * @author rprin
 */
public interface ISepaSddOrigine extends IFwkCktlComptaEntity {
	static final String TYPE_AND_LIBELLE_COMPLET_OF_ORIGINE_KEY = "getTypeAndLibelleCompletOfOrigine";
	static final String TO_ENTITY_KEY = "toEntity";
	static final String I_TO_SEPA_SDD_ORIGINE_TYPE_KEY = "toSepaSddOrigineType";
	static final String I_ORIGINE_ID_KEY = "origineId";

	void setToSepaSddOrigineTypeRelationship(ISepaSddOrigineType origineType);

	void setOrigineId(Integer origineIdentifiant);

	ISepaSddOrigineType toSepaSddOrigineType();

	ISepaSddOrigineEntity toEntity();

	NSArray toSepaSddEcheanciers();

	NSArray toSepaSddEcheanciers(EOQualifier qual);

	void addToToSepaSddEcheanciersRelationship(ISepaSddEcheancier echeancier);

	/**
	 * @return Le préfixe du type d'origine et un libellé permettant à l'utilisateur d'identifier l'entité d'origine.
	 */
	String getTypeAndLibelleCompletOfOrigine();

	/**
	 * @return Un libellé qui figurera dans les fichiers de prélèvement (et donc sur le relevé de compte du débiteur)
	 */
	String getLibellePourDebiteur();

}
