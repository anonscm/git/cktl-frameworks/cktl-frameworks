package org.cocktail.fwkcktlcompta.common.sepasdd.entities;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IFwkCktlComptaEntity;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.ITracable;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public interface ISepaSddEcheancier extends ITracable, IFwkCktlComptaEntity {

    String I_ENTITY_NAME = "FwkCktlCompta_SepaSddEcheancier";
	String IECHE_ID_KEY = "idSepaSddEcheancier";
	String TO_ISEPA_SDD_ORIGINE_KEY = "toSepaSddOrigine";
	String MONTANT_KEY = "montantAPayer";
	String D_PREMIERE_ECHEANCE_PREVUE_KEY = "datePremiereEcheance";
	String NB_ECHEANCES_KEY = "nombreEcheancesPrevues";
	String MONTANT_PREMIERE_ECHEANCE_KEY = "montantPremiereEcheance";
	EOSortOrdering SORT_DATE_MODIFICATION_DESC = EOSortOrdering.sortOrderingWithKey(ITracable.D_MODIFICATION_KEY, EOSortOrdering.CompareDescending);
	EOSortOrdering SORT_DATE_CREATION_ASC = EOSortOrdering.sortOrderingWithKey(ITracable.D_CREATION_KEY, EOSortOrdering.CompareAscending);
	String TO_I_SEPA_SDD_MANDAT_KEY = "toSepaSddMandat";
	String I_MANDAT_KEY = "mandat";
	String I_ID_SEPA_SDD_MANDAT_KEY = "idSepaSddMandat";
	String LIBELLE_COMPLET_KEY = "libelleEcheancierComplet";
	
	Integer nombreEcheancesPrevues();

	BigDecimal montantAPayer();

	BigDecimal montantPremiereEcheance();

	String datePremiereEcheance();

	void ajouterEcheances(List<ISepaSddEcheance> echeances);

	List<ISepaSddEcheance> echeances();

	void setDateCreation(String dateCreation);

	String dateCreation();

	void setDateModification(String dateModification);

	String dateModification();

	void setPersIdCreation(Integer persIdCreation);
	
	Integer persIdCreation();
	
    void setToModificateurPersonne(IGrhumPersonne grhumPersonne);

    IGrhumPersonne modificateur();

	Integer derniereModificationPar();

	ISepaSddMandat mandat();

	ISepaSddMandat toSepaSddMandat();

	void setMandat(ISepaSddMandat mandat);
	
	void setIdSepaSddMandat(Integer idMandat);

	EOEnterpriseObject toEnterpriseObject();

	NSArray getEcheancesTrieesParDateEcheanceASC(Boolean fetch);

	NSArray getEcheancesTrieesParDateEcheanceASCAvecEtatDifferentDe(Etat etat, Boolean fetch);

	NSArray getEcheancesTrieesParDateEcheanceASCAvecEtat(Etat etat, Boolean fetch);

	ISepaSddEcheance creerEcheance();
	
	ISepaSddEcheance creerEcheance(EOEditingContext edc);

	void supprimerEcheances();

	ISepaSddOrigine creerOrigine();

	ISepaSddOrigine toSepaSddOrigine();

	void setToSepaSddOrigineRelationship(ISepaSddOrigine origine);

	String getReference();

	EOEditingContext editingContext();

	String primaryKey();

	NSArray toEcritureDetailsPriseEncharge();
}
