package org.cocktail.fwkcktlcompta.common.sepasdd.entities;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.common.entities.IFwkCktlComptaEntity;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IRecouvrement;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public interface ISepaSddEcheance extends IFwkCktlComptaEntity {

	enum Etat {
		ATTENTE,
		PRELEVE,
		CONFIRME,
		REJETE,
		ANNULE
	}

	enum TypeOperation {
		OOFF, FRST, FNAL, RCUR
	}

	String I_D_PREVUE_KEY = "dPrevue";
	String I_SDD_TYPE_OP_KEY = "sddTypeOp";
	String I_SDD_TYPE_OP_TMP_KEY = "sddTypeOpTmp";
	String I_NUMERO_KEY = "numero";
	String I_ETAT_KEY = "etat";
	String I_ECHEANCIER_KEY = "echeancier";

	static final EOQualifier QUAL_ETAT_NON_ANNULEES = new EOKeyValueQualifier(I_ETAT_KEY, EOQualifier.QualifierOperatorNotEqual, Etat.ANNULE.toString());

	ISepaSddEcheancier echeancier();

	void setEcheancier(ISepaSddEcheancier echeancier);

	BigDecimal montantAPayer();

	void setMontantAPayer(BigDecimal montant);

	String dPrevue();

	void setDatePrevue(String datePrevue);

	String dateCreation();

	void setDateCreation(String dateCreation);

	String dateModification();

	void setDateModification(String dateModification);

	Etat etatAsEnum();

	void setEtatEnum(Etat etat);

	Integer createurId();

	void setCreateurId(Integer persId);

	Integer derniereModificationPar();

	void setDerniereModificationPar(Integer persId);

	EOEnterpriseObject toEnterpriseObject();

	void setToModificateurPersonne(IGrhumPersonne grhumPersonne);

	IGrhumPersonne modficateur();

	String sddTypeOp();

	String sddTypeOpTmp();

	void setTypeOperation(TypeOperation typeOperation);

	void setTypeOperationTmp(TypeOperation typeOperation);

	boolean estAnnulable();

	boolean estModifiable();

	String rum();

	String debiteurBic();

	String debiteurIban();

	String debiteurId();

	String debiteurNom();

	String creancierIcs();

	String creancierNom();

	IRecouvrement toRecouvrement();

	void setDebiteurBic(String value);

	NSArray<? extends ISepaSddEcheanceEcd> toSepaSddEcheanceEcds();

	String dPreleve();


}
