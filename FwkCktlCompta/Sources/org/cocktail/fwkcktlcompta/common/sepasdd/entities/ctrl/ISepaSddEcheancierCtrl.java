package org.cocktail.fwkcktlcompta.common.sepasdd.entities.ctrl;

import org.cocktail.fwkcktlcompta.common.entities.ctrl.CommonEntityCtrl;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.util.CktlEOControlUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public class ISepaSddEcheancierCtrl extends CommonEntityCtrl {

	private static final ISepaSddEcheancierCtrl sharedInstance = new ISepaSddEcheancierCtrl();

	public String serverClassNameForEntity() {
		return "org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier";
	}

	public String clientClassNameForEntity() {
		return "org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier";
	}

	@Override
	protected String entityName() {
		return "FwkCktlCompta_SepaSddEcheancier";
	}

	public static final ISepaSddEcheancierCtrl getSharedInstance() {
		return sharedInstance;
	}

	protected EOQualifier filtreEcheanciersExistentPourOrigine(String origineType, Integer origineEntityId) throws Exception {
		EOQualifier qualOrigineType = new EOKeyValueQualifier(
				ISepaSddEcheancier.TO_ISEPA_SDD_ORIGINE_KEY + "." + ISepaSddOrigine.I_TO_SEPA_SDD_ORIGINE_TYPE_KEY + "." + ISepaSddOrigineType.I_TYPE_CODE_KEY,
				EOQualifier.QualifierOperatorEqual,
				origineType);

		EOQualifier qualOrigineEntityId = new EOKeyValueQualifier(
				ISepaSddEcheancier.TO_ISEPA_SDD_ORIGINE_KEY + "." + ISepaSddOrigine.I_ORIGINE_ID_KEY,
				EOQualifier.QualifierOperatorEqual,
				origineEntityId);

		return CktlEOControlUtilities.andQualifier(qualOrigineType, qualOrigineEntityId);
	}

	public NSArray echeanciersExistentPourOrigine(EOEditingContext ec, String origineType, Integer origineEntityId) throws Exception {
		return fetchAll(ec, filtreEcheanciersExistentPourOrigine(origineType, origineEntityId), null);
	}
}
