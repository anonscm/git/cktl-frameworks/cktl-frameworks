package org.cocktail.fwkcktlcompta.common.sepasdd.entities;

import org.cocktail.fwkcktlcompta.common.entities.IFwkCktlComptaEntity;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;

public interface ISepaSddParam extends IFwkCktlComptaEntity {
	public static final String LIBELLE_KEY = "libelle";
	public static final String I_CREANCIER_ICS_KEY = "creancierIcs";
	public static final String I_ETAT_KEY = "etat";

	String creancierIcs();

	String etat();

	String getLibelle();

	IGrhumPersonne toPersonneCreancier();
}
