package org.cocktail.fwkcktlcompta.common.sepasdd.entities;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.entities.IFwkCktlComptaEntity;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IModeRecouvrement;

import com.webobjects.foundation.NSArray;

/**
 * Cette interface doit etre implémentée par toutes les entités susceptibles d'être à l'origine d'un mandat SEPA SDD.
 * 
 * @author rprin
 */
public interface ISepaSddOrigineEntity extends IFwkCktlComptaEntity {

	public static final String ORIGINE_EXERCICE_KEY = "origineExercice";
	public static final String ORIGINE_DEBITEUR_NOM_COMPLET_KEY = "origineDebiteurNomComplet";
	public static final String ORIGINE_IDENTIFIANT_KEY = "origineIdentifiant";
	public static final String ORIGINE_NUMERO_KEY = "origineNumero";
	public static final String ORIGINE_LIBELLE_KEY = "origineLibelle";
	public static final String ORIGINE_MONTANT_KEY = "origineMontant";

	public Integer origineExercice();

	public String origineDebiteurNomComplet();

	public Integer origineIdentifiant();

	public String origineNumero();

	public String origineLibelle();

	public BigDecimal origineMontant();

	/**
	 * @return Une chaine permettant à l'utilisateur d'identifier l'origine.
	 */
	public String getOrigineLibelleComplet();

	/**
	 * @return Une chaine qui figurera sur le relevé de compte du débiteur
	 */
	public String getOrigineLibellePourDebiteur();

	public IGrhumPersonne origineDebiteurPersonne();

	public IModeRecouvrement modeRecouvrement();

	public void onEcheanceRejetee(ISepaSddEcheance echeance);

	public void onEcheancePrelevee(ISepaSddEcheance echeance);

	/**
	 * @return Toutes les ecritureDetails correspondant à la prise en charge comptable de l'entité.
	 */
	public List<IEcritureDetail> ecritureDetailsContrepartiePriseEnCharge();

}
