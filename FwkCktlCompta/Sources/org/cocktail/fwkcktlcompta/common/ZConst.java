/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
/* ZConst.java created by rprin on Tue 23-Sep-2003 */
package org.cocktail.fwkcktlcompta.common;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.cocktail.fwkcktlcompta.common.util.ISODateTextToLocalDateFormat;

/**
 * Classe contennat des constantes, commune client et serveur
 */
public abstract class ZConst {

	public static final String QUAL_POINT = ".";

	public static final String OUI = "OUI";
	public static final String O = "O";

	//formats à utiliser
	/** Format a utiliser pour afficher les valeurs numeraires */
	public static final Format FORMAT_DISPLAY_NUMBER = new DecimalFormat("#,##0.00");

	/** Format utilisé plutot pour analyser les saisies utilisateur */
	public static final Format FORMAT_EDIT_NUMBER = new DecimalFormat("#,##0.00");

	public static final Format FORMAT_ENTIER = NumberFormat.getIntegerInstance();

	/** Format entier sans separateur de milliers */
	public static final Format FORMAT_ENTIER_BRUT = new DecimalFormat("0");

	public static final Format FORMAT_CONVENTION_INDEX = new DecimalFormat("0000");

	/** Format decimal (suivant le parametre locale) */
	public static final Format FORMAT_DECIMAL = new DecimalFormat("#,##0.00");

	/** *Formatr decimal sans separateur de milliers */
	public static final Format FORMAT_DECIMAL_COURT = new DecimalFormat("###0.00");

	/** Format numerique americain */
	public static final NumberFormat FORMAT_NUMBERS_US = NumberFormat.getNumberInstance(Locale.US);

	public static final Format[] DECIMAL_EDIT_FORMATS = new Format[] {
			FORMAT_DECIMAL_COURT, FORMAT_EDIT_NUMBER
	};
	public static final Format[] ENTIER_EDIT_FORMATS = new Format[] {
			FORMAT_ENTIER_BRUT, FORMAT_ENTIER
	};

	/** Format de date courte dd/MM/yyyy */
	public static final DateFormat FORMAT_DATESHORT = new SimpleDateFormat("dd/MM/yyyy");
	/**
	 * Permet de formater des string de type yyyy-MM-dd en dd/MM/yyy
	 */
	public static final Format DATEISO_FORMAT_DATESHORT =
				new ISODateTextToLocalDateFormat(ISODateTextToLocalDateFormat.FRENCH_SHORT_DATE_PATTERN);
	public static final Format DATEISO_FORMAT_DATEHEURE =
			new ISODateTextToLocalDateFormat(ISODateTextToLocalDateFormat.FRENCH_DATETIME_PATTERN);

	public static final Format FORMAT_DATE_YYYYMMDD = new SimpleDateFormat("yyyyMMdd");
	public static final Format FORMAT_DATE_ISO = new SimpleDateFormat("yyyy-MM-dd");

	public static final String SENS_DEBIT = "D";
	public static final String SENS_CREDIT = "C";


	public static final String S_POINT = ".";

	public static final BigDecimal BIGDECIMAL_ZERO = new BigDecimal(0);
	public static final Integer INTEGER_ZERO = new Integer(0);

	/**
	 * Nom du parametre pour obtenir le delai de paiement par defaut d'une recette
	 */
	public static final String FACTURE_DELAI_PAIEMENT_NAME = "FACTURE_DELAI_PAIEMENT";
	/** Delai par defaut avant de faire une relance */
	public static final String DEFAULT_DELAI_RELANCE = "30";

	public static final String LIBELLE_NON_DEFINI = "Non renseigné";

}
