package org.cocktail.fwkcktlcompta.common.helpers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IEcriture;
import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;
import org.cocktail.fwkcktlcompta.common.util.WebObjectConversionUtil;
import org.cocktail.fwkcktlcompta.common.util.ZDateUtil;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class EcritureHelper {

	private static EcritureHelper sharedInstance = new EcritureHelper();

	public static EcritureHelper getSharedInstance() {
		return sharedInstance;
	}

	public NSArray getDebits(IEcriture ecriture) {
		return EcritureDetailHelper.getSharedInstance().getDebits(ecriture.toEcritureDetails());
	}

	public NSArray getCredits(IEcriture ecriture) {
		return EcritureDetailHelper.getSharedInstance().getCredits(ecriture.toEcritureDetails());
	}

	public BigDecimal getMontantDebits(IEcriture ecriture) {
		return EcritureDetailHelper.getSharedInstance().getMontantDebits(ecriture.toEcritureDetails());
	}

	public BigDecimal getMontantCredits(IEcriture ecriture) {
		return EcritureDetailHelper.getSharedInstance().getMontantCredits(ecriture.toEcritureDetails());
	}

	public String getNumeroEtLibelle(IEcriture ecriture) {
		String res = "";

		if (ecriture.ecrNumero() != null) {
			res += ecriture.ecrNumero();
		}

		if (ecriture.ecrLibelle() != null) {
			res += " " + ecriture.ecrLibelle();
		}

		return res;
	}

	/**
	 * @param exerciceEcriture
	 * @return La date de journée comptable déterminée à partir de la date du jour, suivant l'état de l'exercice
	 * @throws Exception
	 */
	public final Date getDateJourneeComptableCorrigeeSelonExercice(Date date, IJefyAdminExercice exerciceEcriture) throws Exception {
		if (JefyAdminExerciceHelper.getSharedInstance().estClos(exerciceEcriture)) {
			throw new Exception("L'exercice " + exerciceEcriture.exeExercice() + " est clôs. Impossible de passer une écriture sur cet exercice.");
		}
		Date dateReelle = ZDateUtil.getDateOnly(date);
		final Date dateFinExercice = ZDateUtil.getDateOnly(ZDateUtil.getLastDayOfYear(exerciceEcriture.exeExercice().intValue()));
		Date laDate = dateReelle;

		if (dateReelle.getTime() > dateFinExercice.getTime()) {
			laDate = dateFinExercice;
		}
		return laDate;
	}

	public List<Integer> getNumerosList(NSArray ecrituresGenerees) {
		List<Integer> numeros = new ArrayList<Integer>();
		if (ecrituresGenerees.count() > 0) {
			final NSArray lesEcrituresTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(ecrituresGenerees, new NSArray(IEcriture.SORT_ECR_NUMERO_ASC));
			for (int i = 0; i < lesEcrituresTriees.count(); i++) {
				final IEcriture element = (IEcriture) lesEcrituresTriees.objectAtIndex(i);
				numeros.add(element.ecrNumero());
			}
		}
		return numeros;
	}

	public List<IEcritureDetail> getEcritureDetails(IEcriture ecriture) {
		return WebObjectConversionUtil.asList(ecriture.toEcritureDetails());
	}

}
