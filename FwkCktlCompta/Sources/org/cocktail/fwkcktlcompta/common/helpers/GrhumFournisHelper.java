package org.cocktail.fwkcktlcompta.common.helpers;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumFournis;


public class GrhumFournisHelper {

	private static final String FOU_VALIDE_VALIDE = "O";
	private static final String FOU_VALIDE_EN_INSTANCE = "N";
	private static final String FOU_VALIDE_ANNULE = "A";

	private static GrhumFournisHelper sharedInstance = new GrhumFournisHelper();

	public static GrhumFournisHelper getSharedInstance() {
		return sharedInstance;
	}

	public boolean isValide(IGrhumFournis fournis) {
		return FOU_VALIDE_VALIDE.equals(fournis.fouValide());
	}

	public boolean isEnInstancedeValidation(IGrhumFournis fournis) {
		return FOU_VALIDE_EN_INSTANCE.equals(fournis.fouValide());
	}

	public boolean isAnnule(IGrhumFournis fournis) {
		return FOU_VALIDE_ANNULE.equals(fournis.fouValide());
	}
}
