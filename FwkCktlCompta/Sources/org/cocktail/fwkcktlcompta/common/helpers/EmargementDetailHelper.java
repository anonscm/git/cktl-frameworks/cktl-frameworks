package org.cocktail.fwkcktlcompta.common.helpers;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IEmargementDetail;
import org.cocktail.fwkcktlcompta.common.entities.IGestion;

public class EmargementDetailHelper {


	private static EmargementDetailHelper sharedInstance = new EmargementDetailHelper();

	public static EmargementDetailHelper getSharedInstance() {
		return sharedInstance;
	}

	public List<IGestion> getGestionsDesEcrituresDetailsEmarges(IEmargementDetail iEmargementDetail) {
		List<IGestion> gestions = new ArrayList<IGestion>();
		if (!gestions.contains(iEmargementDetail.toEcdSource().toGestion())) {
			gestions.add(iEmargementDetail.toEcdSource().toGestion());
		}
		if (!gestions.contains(iEmargementDetail.toEcdDestination().toGestion())) {
			gestions.add(iEmargementDetail.toEcdDestination().toGestion());
		}
		return gestions;
	}

}
