package org.cocktail.fwkcktlcompta.common.helpers;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IEcriture;
import org.cocktail.fwkcktlcompta.common.entities.IEmargement;
import org.cocktail.fwkcktlcompta.common.entities.IEmargementDetail;
import org.cocktail.fwkcktlcompta.common.entities.IGestion;
import org.cocktail.fwkcktlcompta.common.util.WebObjectConversionUtil;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class EmargementHelper {

	private static EmargementHelper sharedInstance = new EmargementHelper();

	public static EmargementHelper getSharedInstance() {
		return sharedInstance;
	}

	public List<IGestion> getGestionsDesEcrituresDetailsEmarges(IEmargement emargement) {
		List<IGestion> gestions = new ArrayList<IGestion>();
		List<IEmargementDetail> details = WebObjectConversionUtil.asList(emargement.toEmargementDetails());
		for (IEmargementDetail iEmargementDetail : details) {
			if (!gestions.contains(iEmargementDetail.toEcdSource().toGestion())) {
				gestions.add(iEmargementDetail.toEcdSource().toGestion());
			}
			if (!gestions.contains(iEmargementDetail.toEcdDestination().toGestion())) {
				gestions.add(iEmargementDetail.toEcdDestination().toGestion());
			}
		}
		return gestions;
	}

	public List<Integer> getNumerosList(NSArray emargements) {
		List<Integer> numeros = new ArrayList<Integer>();
		if (emargements.count() > 0) {
			final NSArray lesEcrituresTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(emargements, new NSArray(IEmargement.SORT_EMA_NUMERO_ASC));
			for (int i = 0; i < lesEcrituresTriees.count(); i++) {
				final IEmargement element = (IEmargement) lesEcrituresTriees.objectAtIndex(i);
				numeros.add(element.emaNumero());
			}
		}
		return numeros;
	}

}
