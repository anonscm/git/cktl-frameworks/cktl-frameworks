package org.cocktail.fwkcktlcompta.common.helpers;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGestion;

public class GestionHelper {

	private static GestionHelper sharedInstance = new GestionHelper();

	public static GestionHelper getSharedInstance() {
		return sharedInstance;
	}

	public static List<String> getGescodeList(List<IGestion> gestions) {
		List<String> gesCodes = new ArrayList<String>();
		for (IGestion iGestion : gestions) {
			if (!gesCodes.contains(iGestion.gesCode())) {
				gesCodes.add(iGestion.gesCode());
			}
		}
		return gesCodes;
	}

}
