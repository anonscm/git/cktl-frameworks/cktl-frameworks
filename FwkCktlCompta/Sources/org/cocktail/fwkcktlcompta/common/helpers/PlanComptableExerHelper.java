package org.cocktail.fwkcktlcompta.common.helpers;

import org.cocktail.fwkcktlcompta.common.entities.IPlanComptableExer;

public class PlanComptableExerHelper {

	private static PlanComptableExerHelper sharedInstance = new PlanComptableExerHelper();

	public static PlanComptableExerHelper getSharedInstance() {
		return sharedInstance;
	}

	public Boolean isValide(IPlanComptableExer planComptableExer) {
		return IPlanComptableExer.etatValide.equals(planComptableExer.pcoValidite());
	}

}
