package org.cocktail.fwkcktlcompta.common.helpers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;
import org.cocktail.fwkcktlcompta.common.util.CktlArrayUtilities;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class EcritureDetailHelper {

	public static final EOQualifier QUAL_DEBITS = new EOKeyValueQualifier(IEcritureDetail.I_ECD_SENS_KEY, EOQualifier.QualifierOperatorEqual, IEcritureDetail.ECD_SENS_DEBIT);
	public static final EOQualifier QUAL_CREDITS = new EOKeyValueQualifier(IEcritureDetail.I_ECD_SENS_KEY, EOQualifier.QualifierOperatorEqual, IEcritureDetail.ECD_SENS_CREDIT);

	private static EcritureDetailHelper sharedInstance = new EcritureDetailHelper();

	public static EcritureDetailHelper getSharedInstance() {
		return sharedInstance;
	}

	public NSArray getDebits(NSArray ecritureDetails) {
		return EOQualifier.filteredArrayWithQualifier(ecritureDetails, QUAL_DEBITS);
	}

	public NSArray getCredits(NSArray ecritureDetails) {
		return EOQualifier.filteredArrayWithQualifier(ecritureDetails, QUAL_CREDITS);
	}

	public BigDecimal getMontantDebits(NSArray ecritureDetails) {
		return getMontantTotal(getDebits(ecritureDetails));
	}

	public BigDecimal getMontantCredits(NSArray ecritureDetails) {
		return getMontantTotal(getCredits(ecritureDetails));
	}

	/**
	 * Ne pas utiliser en direct.
	 * 
	 * @param ecritureDetails
	 * @return
	 */
	private BigDecimal getMontantTotal(NSArray ecritureDetails) {
		List<IEcritureDetail> filtres = CktlArrayUtilities.toList(ecritureDetails);
		BigDecimal montant = BigDecimal.ZERO;
		for (IEcritureDetail iEcritureDetail : filtres) {
			montant = montant.add(iEcritureDetail.ecdMontant());
		}
		return montant;
	}

	public List<IEcritureDetail> filtrerSelonExercice(List<IEcritureDetail> ecds, IJefyAdminExercice exercice) {
		List<IEcritureDetail> results = new ArrayList<IEcritureDetail>();
		for (IEcritureDetail ecd : ecds) {
			if (ecd.toExercice().equals(exercice)) {
				results.add(ecd);
			}
		}
		return results;
	}

	public List<IEcritureDetail> filtrerSelonResteAEmargerSuperieurOuEgalA(List<IEcritureDetail> ecds, BigDecimal resteAEmarger) {
		List<IEcritureDetail> results = new ArrayList<IEcritureDetail>();
		for (IEcritureDetail ecd : ecds) {
			if (ecd.ecdResteEmarger().compareTo(resteAEmarger) >= 0) {
				results.add(ecd);
			}
		}
		return results;
	}

	public String getLibelleComplet(IEcritureDetail ecd) {
		StringBuilder res = new StringBuilder();

		res.append(getNumeroEcritureAvecExerciceEtIndex(ecd));
		res.append(" (");

		if (ecd.toGestion() != null) {
			res.append(ecd.toGestion().gesCode());
		}
		res.append(")");
		if (ecd.ecdSens() != null) {
			res.append(" " + ecd.ecdSens());
		}
		if (ecd.toPlanComptableExer() != null) {
			res.append(ecd.toPlanComptableExer().pcoNum());
		}
		if (ecd.ecdMontant() != null) {
			res.append(" " + ecd.ecdMontant());
		}

		if (ecd.ecdLibelle() != null) {
			res.append(" - " + ecd.ecdLibelle());
		}
		return res.toString();
	}

	public Boolean isTotalementEmargee(IEcritureDetail ecritureDetail) {
		return ecritureDetail.ecdResteEmarger().doubleValue() == 0d;
	}

	public String getNumeroEcritureAvecExerciceEtIndex(IEcritureDetail ecritureDetail) {
		if (ecritureDetail == null) {
			return "";
		}
		StringBuilder res = new StringBuilder();
		res.append("[");
		if (ecritureDetail.toEcriture() != null && ecritureDetail.toEcriture().toExercice() != null) {
			res.append(ecritureDetail.toEcriture().toExercice().exeExercice());
		}
		else {
			res.append("?");
		}
		res.append("-");
		if (ecritureDetail.toEcriture() != null) {
			res.append(ecritureDetail.toEcriture().ecrNumero());
		}
		else {
			res.append("?");
		}
		res.append("/");
		if (ecritureDetail.ecdIndex() != null) {
			res.append(ecritureDetail.ecdIndex());
		}
		else {
			res.append("?");
		}
		res.append("]");

		return res.toString();
	}

	public Boolean isDebit(IEcritureDetail ecritureDetail) {
		return IEcritureDetail.ECD_SENS_DEBIT.equals(ecritureDetail.ecdSens());
	}

	public Boolean isCredit(IEcritureDetail ecritureDetail) {
		return IEcritureDetail.ECD_SENS_CREDIT.equals(ecritureDetail.ecdSens());
	}

}
