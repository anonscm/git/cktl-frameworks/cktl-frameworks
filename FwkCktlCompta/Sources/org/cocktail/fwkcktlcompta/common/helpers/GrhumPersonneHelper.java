package org.cocktail.fwkcktlcompta.common.helpers;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumFournis;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumRib;

import com.webobjects.foundation.NSArray;

public class GrhumPersonneHelper {

	public static String getNomAndPrenom(IGrhumPersonne eoGrhumPersonne) {
		return (eoGrhumPersonne.persLc() != null ? eoGrhumPersonne.persLc() + " " : "") + eoGrhumPersonne.persLibelle();
	}

	public static String getDerivFouCode(IGrhumPersonne eoGrhumPersonne) {
		IGrhumFournis fou = eoGrhumPersonne.getDerivFournis();
		if (fou != null) {
			return fou.fouCode();
		}
		return null;
	}

	public static String getDerivFouType(IGrhumPersonne eoGrhumPersonne) {

		IGrhumFournis fou = eoGrhumPersonne.getDerivFournis();
		if (fou != null) {
			return fou.fouType();
		}
		return null;
	}

	public static String getDerivRibValides(IGrhumPersonne eoGrhumPersonne) {
		String res = "";
		IGrhumFournis fou = eoGrhumPersonne.getDerivFournis();
		if (fou != null) {
			NSArray ribsValides = fou.toRibsValide();
			if (ribsValides.count() == 0) {
				return null;
			}

			if (ribsValides.count() > 1) {
				res += "(*) ";
			}
			res += ((IGrhumRib) fou.toRibsValide().objectAtIndex(0)).bicEtIban();
		}
		return res;
	}

}
