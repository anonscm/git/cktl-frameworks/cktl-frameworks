package org.cocktail.fwkcktlcompta.common.helpers;

import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;

public class JefyAdminExerciceHelper {

	private static JefyAdminExerciceHelper sharedInstance = new JefyAdminExerciceHelper();

	public static JefyAdminExerciceHelper getSharedInstance() {
		return sharedInstance;
	}

	/**
	 * @return Le libellé correspondant au champ exeStat.
	 */
	public final String getEtatLibelle(IJefyAdminExercice exercice) {
		if (IJefyAdminExercice.EXE_ETAT_CLOS.equals(exercice.exeStat())) {
			return IJefyAdminExercice.EXE_ETAT_CLOS_LIBELLE;
		}
		else if (IJefyAdminExercice.EXE_ETAT_RESTREINT.equals(exercice.exeStat())) {
			return IJefyAdminExercice.EXE_ETAT_RESTREINT_LIBELLE;
		}
		else if (IJefyAdminExercice.EXE_ETAT_OUVERT.equals(exercice.exeStat())) {
			return IJefyAdminExercice.EXE_ETAT_OUVERT_LIBELLE;
		}
		else if (IJefyAdminExercice.EXE_ETAT_PREPARATION.equals(exercice.exeStat())) {
			return IJefyAdminExercice.EXE_ETAT_PREPARATION_LIBELLE;
		}
		else {
			return null;
		}
	}

	/**
	 * @return Le libelle correspondant au champ exeType.
	 */
	public final String getTypeLibelle(IJefyAdminExercice exercice) {
		if (IJefyAdminExercice.EXE_TYPE_COMPTABLE.equals(exercice.exeType())) {
			return IJefyAdminExercice.EXE_TYPE_COMPTABLE_LIBELLE;
		}
		else if (IJefyAdminExercice.EXE_TYPE_TRESORERIE.equals(exercice.exeType())) {
			return IJefyAdminExercice.EXE_TYPE_TRESORERIE_LIBELLE;
		}
		else {
			return null;
		}
	}

	public final boolean estClos(IJefyAdminExercice exercice) {
		return IJefyAdminExercice.EXE_ETAT_CLOS.equals(exercice.exeStat());
	}

	public final boolean estOuvert(IJefyAdminExercice exercice) {
		return IJefyAdminExercice.EXE_ETAT_OUVERT.equals(exercice.exeStat());
	}

	public final boolean estRestreint(IJefyAdminExercice exercice) {
		return IJefyAdminExercice.EXE_ETAT_RESTREINT.equals(exercice.exeStat());
	}

	public final boolean estPreparation(IJefyAdminExercice exercice) {
		return IJefyAdminExercice.EXE_ETAT_PREPARATION.equals(exercice.exeStat());
	}

	public final boolean estTresorerie(IJefyAdminExercice exercice) {
		return IJefyAdminExercice.EXE_TYPE_TRESORERIE.equals(exercice.exeType());
	}

	public final boolean estComptable(IJefyAdminExercice exercice) {
		return IJefyAdminExercice.EXE_TYPE_COMPTABLE.equals(exercice.exeType());
	}

}
