package org.cocktail.fwkcktlcompta.common.helpers;

import org.cocktail.fwkcktlcompta.common.entities.ITypeRecouvrement;

public class TypeRecouvrementHelper {

	private static TypeRecouvrementHelper sharedInstance = new TypeRecouvrementHelper();

	public static TypeRecouvrementHelper getSharedInstance() {
		return sharedInstance;
	}

	public String getFileExtension(ITypeRecouvrement typeRecouvrement) {
		if (isSepaSdd(typeRecouvrement)) {
			return "xml";
		}
		else if (isBdf(typeRecouvrement)) {
			return "txt";
		}
		return "txt";
	}

	public boolean isSepaSdd(ITypeRecouvrement typeRecouvrement) {
		return ITypeRecouvrement.TREC_FORMAT_SEPASDD.equals(typeRecouvrement.trecFormat());
	}

	public boolean isBdf(ITypeRecouvrement typeRecouvrement) {
		return ITypeRecouvrement.TREC_FORMAT_BDF.equals(typeRecouvrement.trecFormat());
	}


}
