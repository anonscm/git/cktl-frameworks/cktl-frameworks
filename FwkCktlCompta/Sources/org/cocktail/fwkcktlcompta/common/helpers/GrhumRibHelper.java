package org.cocktail.fwkcktlcompta.common.helpers;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumRib;


public class GrhumRibHelper {

	public static final String RIB_INVALIDE = "invalide";
	public static final String RIB_INCOMPLET = "incomplet/erroné";
	public static final String RIBCOMPLET_KEY = "ribCompletIntl";
	public static final String BIC_IBAN_KEY = "bicEtIban";

	private static GrhumRibHelper sharedInstance = new GrhumRibHelper();

	public static GrhumRibHelper getSharedInstance() {
		return sharedInstance;
	}

	public Boolean isRibValide(IGrhumRib rib) {
		return IGrhumRib.RIB_VALIDE.equals(rib.ribValide());
	}

	public boolean isRibIntlComplet(IGrhumRib rib) {
		return (rib.iban() != null && rib.toBanque() != null && rib.toBanque().bic() != null);
	}

	public String bicEtIban(IGrhumRib rib) {
		return ribBicFormate(rib) + " - " + ribIbanFormate(rib);
	}

	public final String getRibCompletIntl(IGrhumRib rib) {
		if (!isRibValide(rib)) {
			return RIB_INCOMPLET;
		}
		if (!isRibIntlComplet(rib)) {
			return RIB_INCOMPLET;
		}
		return ribBicFormate(rib) + " - " + ribIbanFormate(rib).concat(" - ").concat(rib.toBanque() == null ? "" : rib.toBanque().domiciliation()).concat(" (").concat(rib.ribTitco()).concat(")");

	}

	public String ribBicFormate(IGrhumRib rib) {
		if (rib.toBanque() == null || rib.toBanque().bic() == null || rib.toBanque().bic().length() == 0) {
			return "????????";
		}
		return rib.toBanque().bic();
	}

	public String ribIbanFormate(IGrhumRib rib) {
		String res = formatIban(rib.iban());
		if (res == null || res.length() == 0) {
			return "????????";
		}
		return res;
	}

	public String formatIban(String iban) {
		if (iban == null || iban.length() == 0) {
			return "";
		}
		iban = iban.trim().replaceAll(" ", "");
		iban = iban.toUpperCase();
		String res = "";

		BigDecimal nbgrappes = BigDecimal.valueOf(iban.length()).divide(BigDecimal.valueOf(4));
		int max = nbgrappes.intValue();
		String reste = iban;
		for (int i = 0; i < max; i++) {
			res = res.concat(reste.substring(0, (reste.length() < 4 ? reste.length() : 4))).concat(" ");
			reste = reste.substring((reste.length() > 4 ? 4 : reste.length()));
		}
		res = res.concat(reste);
		res = res.trim();
		return res;
	}
}
