package org.cocktail.fwkcktlcompta.common.helpers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Permet de récupérer des parametres de configuration qu'on soit indépendamment coté serveur ou client.
 * 
 * @author rprin
 */
public class ParamHelper {
	private static ParamHelper sharedInstance = new ParamHelper();

	public Object getConfig(EOEditingContext ec, String key) throws Exception {
		if (FwkCktlComptaMoteurCtrl.getSharedInstance().isOnClient()) {
			return getClientConfig(ec, key);
		}
		else {
			return getServerConfig(ec, key);
		}
	}

	private Object getClientConfig(EOEditingContext ec, String key) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		return FwkCktlComptaMoteurCtrl.getSharedInstance().getMethods().get(FwkCktlComptaMoteurCtrl.METHOD_GET_CONFIG).invoke(null, ec, key);
	}

	private Object getServerConfig(EOEditingContext ec, String key) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		Method configMethod = FwkCktlComptaMoteurCtrl.getSharedInstance().getMethods().get(FwkCktlComptaMoteurCtrl.METHOD_GET_CONFIG);
		//statique org.cocktail.fwkcktlcompta.server.remotes.RemoteDelegateCompta.getConfig(java.lang.String)
		return configMethod.invoke(null, key);
	}

	public static ParamHelper getSharedInstance() {
		return sharedInstance;
	}

}
