package org.cocktail.fwkcktlcompta.common.helpers;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.ITitreDetailEcriture;

public class TitreDetailEcritureHelper {
	private static TitreDetailEcritureHelper sharedInstance = new TitreDetailEcritureHelper();

	public static TitreDetailEcritureHelper getSharedInstance() {
		return sharedInstance;
	}

	public List<ITitreDetailEcriture> filtrerSelonTdeOrigineStartsWith(List<ITitreDetailEcriture> tdes, String tdeOrigineStartsWith) {
		List<ITitreDetailEcriture> results = new ArrayList<ITitreDetailEcriture>();
		for (ITitreDetailEcriture iTitreDetailEcriture : tdes) {
			if (iTitreDetailEcriture.tdeOrigine().startsWith(tdeOrigineStartsWith)) {
				results.add(iTitreDetailEcriture);
			}
		}
		return results;
	}
	
	public List<ITitreDetailEcriture> filtrerEcrituresVisaContrepartie(List<ITitreDetailEcriture> tdes) {
		return filtrerSelonTdeOrigineStartsWith(tdes, ITitreDetailEcriture.TDE_ORIGINE_VISA_CTP);
	}

}
