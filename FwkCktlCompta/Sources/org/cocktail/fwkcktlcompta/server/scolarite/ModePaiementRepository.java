package org.cocktail.fwkcktlcompta.server.scolarite;

import org.cocktail.fwkcktlcompta.server.metier.EOModePaiement;
import org.cocktail.fwkcktlcompta.server.scolarite.PaiementException.Code;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * Implémentation par défaut du repository des modes de paiement.
 *  
 * @see IModePaiementRepository
 * @author Pascal MACOUIN
 */
public class ModePaiementRepository implements IModePaiementRepository {
    private EOEditingContext editingContext;

    /**
     * @param editingContext l'editingContext
     */
    public ModePaiementRepository(EOEditingContext editingContext) {
        this.editingContext = editingContext;
    }
    
    /**
	 * {@inheritDoc}
	 */
    public EOModePaiement fetchRequiredModePaiementExercice(String modCode, Long exeOrdre) throws PaiementException {
		EOQualifier qualifier = EOModePaiement.MOD_VALIDITE.eq("VALIDE")
				.and(EOModePaiement.TO_EXERCICE.dot(EOExercice.EXE_EXERCICE_KEY).eq(exeOrdre))
				.and(EOModePaiement.MOD_CODE.eq(modCode));
		
		EOModePaiement modePaiement = EOModePaiement.fetchByQualifier(editingContext, qualifier);
		
        if (modePaiement == null) {
            String message = String.format("Aucun mode de paiement valide trouvé pour le modCode=%s et l'exeOrdre=%s (%s)", modCode, exeOrdre, EOModePaiement.ENTITY_TABLE_NAME);
            throw new PaiementException(message, Code.MODE_PAIEMENT);
        }
		
		return modePaiement;
    }
}
