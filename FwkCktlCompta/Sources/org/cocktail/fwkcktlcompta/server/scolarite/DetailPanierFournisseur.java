package org.cocktail.fwkcktlcompta.server.scolarite;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRib;

/**
 * Panier aggrégeant les articles et les paiements pour un fournisseur donné.
 * 
 * @author Pascal MACOUIN
 */
public class DetailPanierFournisseur implements Iterable<DetailPanier> {

	private Map<String, DetailPanier> details = new HashMap<String, DetailPanier>();
	
	private Integer numeroMandat;
	private IFournis fournisseur;
	private IRib ribFournisseur;
	private IAdresse adresseFournisseur;
	private String modeCodeRemboursement;
	private Paiement paiement;
	
	/**
	 * @param numeroMandat Le numéro de mandat => id du paiement
	 * @param fournisseur Le founisseur
	 * @param ribFournisseur Le rib du fournisseur à uiliser
	 * @param adresseFournisseur L'adresse du fournisseur à utiliser
	 * @param modeCodeRemboursement Le modCode du remboursement
	 */
    public DetailPanierFournisseur(Integer numeroMandat, IFournis fournisseur, IRib ribFournisseur, IAdresse adresseFournisseur, String modeCodeRemboursement) {
    	this.numeroMandat = numeroMandat;
    	this.fournisseur = fournisseur;
    	this.ribFournisseur = ribFournisseur;
    	this.adresseFournisseur = adresseFournisseur;
    	this.modeCodeRemboursement = modeCodeRemboursement;
	}

	/** 
     * @return l'itérateur sur les détails du panier
     */
	public Iterator<DetailPanier> iterator() {
		return details.values().iterator();
	}

    /**
     * Ajouter ces articles et ces paiements au panier du fournisseur.
     * 
     * @param articles la liste des articles
     * @param paiement le paiement
     * @throws PaiementException si la balance entre les articles et les paiements n'est pas bonne
     */
	public void ajouter(List<Article> articles, Paiement paiement) throws PaiementException {
        for (Article article : articles) {
            ajouterMouvement(article);
        }

        ajouterMouvement(paiement);
        this.paiement = paiement;
    	
        verifierBalance();
	}

    private void ajouterMouvement(ItemPanier mouvement) throws PaiementException {
        DetailPanier detailPanier = getOrCreateDetail(mouvement.getPcoNum());
        detailPanier.add(mouvement);
    }

    private DetailPanier getOrCreateDetail(String pcoNum) {
        DetailPanier detailPanier = getDetail(pcoNum);
        
        if (detailPanier == null) {
            detailPanier = new DetailPanier(pcoNum);
            details.put(pcoNum, detailPanier);
        }
        
        return detailPanier;
    }
    
    /**
     * @param clef la clé du détail.
     * @return le détail du panier en fonction de la clef
     */
    public DetailPanier getDetail(String clef) {
        return details.get(clef);
    }
    
    public Integer getNumeroMandat() {
		return numeroMandat;
	}

	public IFournis getFournisseur() {
		return fournisseur;
	}

	public IRib getRibFournisseur() {
		return ribFournisseur;
	}

	public IAdresse getAdresseFournisseur() {
		return adresseFournisseur;
	}

	public String getModeCodeRemboursement() {
		return modeCodeRemboursement;
	}

	public Paiement getPaiement() {
		return paiement;
	}

	/**
	 * @return Le total de ce panier
	 */
	public BigDecimal calculerTotal() {
		// On calcul le total des crédits (mais on aurai pu faire le total de débit, le résultat est le même)
        BigDecimal sommeCredit = BigDecimal.ZERO;
        
        for (DetailPanier detailPanier : details.values()) {
            if (detailPanier.isCrebit()) {
                sommeCredit = sommeCredit.add(detailPanier.getMontant());
            }
        }
        
        return sommeCredit;
	}
	
	private void verifierBalance() throws PaiementException {
        BigDecimal sommeDebit = BigDecimal.ZERO;
        BigDecimal sommeCredit = BigDecimal.ZERO;
        for (DetailPanier detailPanier : details.values()) {
            if (detailPanier.isCrebit()) {
                sommeCredit = sommeCredit.add(detailPanier.getMontant());
            } else {
                sommeDebit = sommeDebit.add(detailPanier.getMontant());
            }
        }
        if (sommeDebit.compareTo(sommeCredit) != 0) {
            String message = String.format("La somme des crédits (%s) est différentes de la somme des débits (%s)", sommeCredit, sommeDebit);
            throw new PaiementException(message, PaiementException.Code.BALANCE);
        }
    }
}
