package org.cocktail.fwkcktlcompta.server.scolarite;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;

/**
 * Panier aggrégeant les articles et les paiements.
 * 
 * @author Alexis Tual
 */
public class PanierPaiement extends Panier implements Iterable<DetailPanier> {
	
	private Map<String, DetailPanier> details = new HashMap<String, DetailPanier>();

    /**
     * @param codeRne le code de l'établissement
     * @param gesCode le code gestion
     * @param datePaiement la date de paiement
     */
    public PanierPaiement(String codeRne, String gesCode, Date datePaiement) {
		super(codeRne, gesCode, datePaiement);
	}
    
    /**
     * Ajouter ces articles et ces paiements au panier.
     * 
     * @param articles la liste des articles
     * @param paiements la liste des paiements
     * @throws PaiementException si la balance entre les articles et les paiements n'est pas bonne
     */
    public void ajouter(List<Article> articles, List<Paiement> paiements) throws PaiementException {
        for (Article article : articles) {
            ajouterMouvement(article);
        }
        for (Paiement paiement : paiements) {
        	ajouterMouvement(paiement);
        }
        verifierBalance();
    }

    /**
     * @return <code>true</code> si dans ce panier il y a des plan comptable avec un montant différent de zéro
     */
    public boolean isExistePlancoAvecMontantDifferentDeZero() {
        for (DetailPanier detailPanier : details.values()) {
            if (!detailPanier.isMontantZero()) {
            	return true;
            }
        }
        
        return false;
    }
    
    private void verifierBalance() throws PaiementException {
        BigDecimal sommeDebit = BigDecimal.ZERO;
        BigDecimal sommeCredit = BigDecimal.ZERO;
        for (DetailPanier detailPanier : details.values()) {
            if (detailPanier.isCrebit()) {
                sommeCredit = sommeCredit.add(detailPanier.getMontant());
            } else {
                sommeDebit = sommeDebit.add(detailPanier.getMontant());
            }
        }
        if (sommeDebit.compareTo(sommeCredit) != 0) {
            String message = String.format("La somme des crédits (%s) est différentes de la somme des débits (%s)", sommeCredit, sommeDebit);
            throw new PaiementException(message, PaiementException.Code.BALANCE);
        }
    }
    
    private void ajouterMouvement(ItemPanier mouvement) throws PaiementException {
        DetailPanier detailPanier = getOrCreateDetail(mouvement.getPcoNum(), mouvement.getEcheancier());
        detailPanier.add(mouvement);
    }

    private DetailPanier getOrCreateDetail(String pcoNum, ISepaSddEcheancier echeancier) {
    	String clef = getClefDetail(pcoNum, echeancier);
        DetailPanier detailPanier = getDetail(clef);
        
        if (detailPanier == null) {
            detailPanier = new DetailPanier(pcoNum, echeancier);
            details.put(clef, detailPanier);
        }
        
        return detailPanier;
    }
    
    /**
     * @param pcoNum un plan comptable
     * @param echeancier un echéancier (facultatif)
     * @return la clef du détail dans la Map.
     */
    public String getClefDetail(String pcoNum, ISepaSddEcheancier echeancier) {
    	String clef = pcoNum;
    	
    	if (echeancier != null) {
    		clef += "/" + echeancier.primaryKey();
    	}
    	
    	return clef;
    }
    
    /**
     * @param clef la clé du détail. Voir {@link #getClefDetail(String, ISepaSddEcheancier)}.
     * @return le détail du panier en fonction de la clef
     */
    public DetailPanier getDetail(String clef) {
        return details.get(clef);
    }
    
    /** 
     * @return l'itérateur sur les détails du panier
     */
    public Iterator<DetailPanier> iterator() {
        return details.values().iterator();
    }
    
}
