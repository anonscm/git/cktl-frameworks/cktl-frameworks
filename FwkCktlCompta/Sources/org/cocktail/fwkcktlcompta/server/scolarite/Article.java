package org.cocktail.fwkcktlcompta.server.scolarite;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;

/**
 * 
 * Un article de paiement.
 * 
 * @author Alexis Tual
 *
 */
public class Article implements ItemPanier {

	private Sens sensMontantPositif = Sens.CREDIT;
	private Sens sensMontantNegatif = Sens.DEBIT;
	
    private BigDecimal montant;
    private Sens sens;
    private String pcoNum;
    private String gesCode;
    
    
    /**
     * @param montant le montant de l'article
     * @param pcoNum le code nature
     * @param gesCode le code de gestion
     */
    public Article(BigDecimal montant, String pcoNum, String gesCode) {
        setMontant(montant);
        this.pcoNum = pcoNum;
        this.gesCode = gesCode;
    }

    /** 
     * {@inheritDoc}
     */
    public Sens getSens() {
        return sens;
    }
    
    /** 
     * {@inheritDoc}
     */
    public BigDecimal getMontant() {
        return montant;
    }
    
    /** 
     * {@inheritDoc}
     */
    public void setMontant(BigDecimal montant) {
        this.montant = montant.abs();
        this.sens = calculerSens(montant);
    }
    
    /**
     * @return toujours <code>null</code> (pas d'échéancier) pour les articles
     */
    public ISepaSddEcheancier getEcheancier() {
		return null;
	}

    /** 
     * {@inheritDoc}
     */
    public String getPcoNum() {
        return pcoNum;
    }
    
    /** 
     * {@inheritDoc}
     */
    public void setPcoNum(String pcoNum) {
        this.pcoNum = pcoNum;
    }
    
    /** 
     * {@inheritDoc}
     */
    public String getGesCode() {
        return gesCode;
    }
    
    /** 
     * {@inheritDoc}
     */
    public void setGesCode(String gesCode) {
        this.gesCode = gesCode;
    }
    
    
	private Sens calculerSens(BigDecimal montant) {
		if (montant.signum() >= 0) {
        	return sensMontantPositif;
        } else {
        	return sensMontantNegatif;
        }
	}
}
