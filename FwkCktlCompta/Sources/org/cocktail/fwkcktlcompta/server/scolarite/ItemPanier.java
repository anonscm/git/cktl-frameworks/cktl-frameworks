package org.cocktail.fwkcktlcompta.server.scolarite;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;

/**
 * 
 * Représente un item d'un panier : un article ou un paiement.
 * 
 * @author Alexis Tual
 *
 */
public interface ItemPanier {

    /**
     * Le sens de l'item du panier
     */
    public enum Sens { 
        DEBIT("D"), CREDIT("C");
        
        private String code;
        
        private Sens(String code) {
            this.code = code;
        }
        
        public String getCode() {
            return code;
        }
        
        /**
         * @return Le sens inverse
         */
        public Sens sensInverse() {
        	if (this.equals(CREDIT)) {
        		return DEBIT;
        	} else {
        		return CREDIT;
        	}
        }
    }
    
    /**
     * @return "D" pour débit, "C" pour crédit
     */
    Sens getSens();

    /**
     * @return le montant
     */
    BigDecimal getMontant();

    /**
     * @param montant le montant
     */
    void setMontant(BigDecimal montant);

    /**
     * @return l'échéancier de prélèvement (SEPA SDD)
     */
    ISepaSddEcheancier getEcheancier();

    /**
     * @return le code nature correspondant à l'item
     */
    String getPcoNum();

    /**
     * @param pcoNum le code nature
     */
    void setPcoNum(String pcoNum);

    /**
     * @return le code gestion
     */
    String getGesCode();

    /**
     * @param gesCode le code gestion
     */
    void setGesCode(String gesCode);

}