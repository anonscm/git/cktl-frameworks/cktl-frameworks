package org.cocktail.fwkcktlcompta.server.scolarite;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;

/**
 * Un paiement correspondant à un ensemble d'articles.
 * 
 * @author Alexis Tual
 */
public class Paiement implements ItemPanier {

	private static final Logger LOG = Logger.getLogger(Paiement.class);

	private Sens sensMontantPositif = Sens.DEBIT;
	private Sens sensMontantNegatif = Sens.CREDIT;
	
    private BigDecimal montant;
    private Sens sens;
    private ISepaSddEcheancier echeancier;
    private String pcoNum;
    private String gesCode;
    
    /**
     * Construit un paiement comptant.
     * @param montant le montant du paiement comptant
     * @param pcoNum le code nature
     * @param gesCode le code gestion
     */
    public Paiement(BigDecimal montant, String pcoNum, String gesCode) {
    	this(montant, null, pcoNum, gesCode);
    }

    /**
     * Construit un paiement différé.
     * @param echeancier l'échéancier SEPA SDD pour un paiement différé
     * @param pcoNum le code nature
     * @param gesCode le code gestion
     */
    public Paiement(ISepaSddEcheancier echeancier, String pcoNum, String gesCode) {
    	this(null, echeancier, pcoNum, gesCode);

    	if (echeancier == null) {
			String message = "Impossible d'instancier un paiement différé sans échéancier";
			LOG.error(message);
			throw new IllegalArgumentException(message);
    	}
    }
    
    /**
     * @param montant le montant
     * @param echeancier l'échéancier SEPA SDD pour un paiement différé
     * @param pcoNum le code nature
     * @param gesCode le code gestion
     */
    private Paiement(BigDecimal montant, ISepaSddEcheancier echeancier, String pcoNum, String gesCode) {
    	if (montant != null) {
    		setMontant(montant);
    	} else {
    		setEcheancier(echeancier);
    	}
        this.pcoNum = pcoNum;
        this.gesCode = gesCode;
    }
    
    /** 
     * {@inheritDoc}
     */
    public Sens getSens() {
        return sens;
    }
    
    /**
     * {@inheritDoc}
     */
    public BigDecimal getMontant() {
    	if (echeancier != null) {
    		return echeancier.montantAPayer();
    	}
    	
        return montant;
    }

    /**
     * {@inheritDoc}
     */
    public void setMontant(BigDecimal montant) {
    	if (echeancier != null) {
    		String message = String.format("Impossible d'affecter directement le montant lorsque le paiement est différé (payé par échéance). Echéancier id %s", echeancier.primaryKey());
    		LOG.error(message);
    		throw new IllegalStateException(message);
    	}
    	
        this.montant = montant.abs();
        this.sens = calculerSens(montant);
    }

    public ISepaSddEcheancier getEcheancier() {
		return echeancier;
	}

    /**
     * @param echeancier L'échéancier à affecter à ce paiement
     */
	public void setEcheancier(ISepaSddEcheancier echeancier) {
		this.echeancier = echeancier;
		this.sens = calculerSens(echeancier.montantAPayer());
	}

	public String getPcoNum() {
        return pcoNum;
    }
    
    public void setPcoNum(String pcoNum) {
        this.pcoNum = pcoNum;
    }
    
    public String getGesCode() {
        return gesCode;
    }
    
    public void setGesCode(String gesCode) {
        this.gesCode = gesCode;
    }
    
	private Sens calculerSens(BigDecimal montant) {
		if (montant.signum() >= 0) {
        	return sensMontantPositif;
        } else {
        	return sensMontantNegatif;
        }
	}
}
