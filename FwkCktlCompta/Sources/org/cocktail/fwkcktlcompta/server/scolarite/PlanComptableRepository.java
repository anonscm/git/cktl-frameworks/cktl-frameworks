package org.cocktail.fwkcktlcompta.server.scolarite;

import org.cocktail.fwkcktlcompta.common.entities.IPlanComptable;
import org.cocktail.fwkcktlcompta.common.entities.IPlanComptableExer;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlcompta.server.scolarite.PaiementException.Code;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * Implémentation par défaut du repository
 *  
 * @see IPlanComptableRepository
 * @author Alexis Tual
 *
 */
public class PlanComptableRepository implements IPlanComptableRepository {

    private EOEditingContext editingContext;
    
    /**
     * @param editingContext l'editingContext
     */
    public PlanComptableRepository(EOEditingContext editingContext) {
        this.editingContext = editingContext;
    }

    /** 
     * {@inheritDoc}
     */
    public IPlanComptable fetchPlanComptable(String pcoNum, Long exeOrdre) throws PaiementException {
    	fetchRequiredPlanComptableExercice(pcoNum, exeOrdre);
        EOQualifier qualPco = EOPlanComptable.PCO_NUM.eq(pcoNum);
        EOPlanComptable planCo = EOPlanComptable.fetchByQualifier(editingContext, qualPco);
        return planCo;
    }

    /** 
     * {@inheritDoc}
     */
    public IPlanComptableExer fetchRequiredPlanComptableExercice(String pcoNum, Long exeOrdre) throws PaiementException {
        EOQualifier qualPlancoExer = EOPlanComptableExer.TO_EXERCICE.dot(EOJefyAdminExercice.EXE_EXERCICE_KEY).eq(exeOrdre);
        EOQualifier qualPlancoValide = EOPlanComptableExer.PCO_NUM.eq(pcoNum).and(EOPlanComptableExer.PCO_VALIDITE.eq(EOPlanComptableExer.etatValide));
        EOPlanComptableExer planCoExer = EOPlanComptableExer.fetchByQualifier(editingContext,  ERXQ.and(qualPlancoValide, qualPlancoExer));
        
        if (planCoExer == null) {
            String message = String.format("Aucun plan comptable valide trouvé pour le pcoNum=%s et l'exeOrdre=%s (%s)", pcoNum, exeOrdre, EOPlanComptableExer.ENTITY_TABLE_NAME);
            throw new PaiementException(message, Code.PLANCO);
        }
        
        return planCoExer;
    }
    
}
