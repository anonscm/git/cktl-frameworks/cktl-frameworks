package org.cocktail.fwkcktlcompta.server.scolarite;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.server.scolarite.ItemPanier.Sens;

/**
 * Le détail du panier.
 */
public class DetailPanier {
    
    private String gesCode;
    private BigDecimal montant;
    private Sens sens;
    private String pcoNum;
    private ISepaSddEcheancier echeancier;
    
    /**
     * @param pcoNum le code nature
     */
    public DetailPanier(String pcoNum) {
    	this(pcoNum, null);
    }
    
    /**
     * @param pcoNum le code nature
     * @param echeancier l'échéancier de prélèvement (SEPA SDD)
     */
    public DetailPanier(String pcoNum, ISepaSddEcheancier echeancier) {
        this.pcoNum = pcoNum;
        this.echeancier = echeancier;
    }
    
    /**
     * @param itemPanier L'item du panier à ajouter
     * @throws PaiementException Si les contrôles sur l'ajout sont KO
     */
    public void add(ItemPanier itemPanier) throws PaiementException {
        verifierCoherenceGesCode(itemPanier);
        addMontant(itemPanier.getMontant(), itemPanier.getSens());
        setGesCode(itemPanier.getGesCode());
    }
    
    /**
     * @return <code>true</code> si le montant est <code>null</code> ou à zéro
     */
    public boolean isMontantZero() {
    	return montant == null || montant.compareTo(BigDecimal.ZERO) == 0;
    }
    
    /**
     * @param unMontant la valeur
     * @param unSens le sens
     */
    public void addMontant(BigDecimal unMontant, Sens unSens) {
    	if (montant == null) {
    		montant = unMontant;
    		sens = unSens;
    	} else {
    		// Si même sens, on additionne. Si sens différent, on soustrait
    		if (sens.equals(unSens)) {
    			montant = montant.add(unMontant);
    		} else {
    			montant = montant.subtract(unMontant);
    		}
    		
    		// On remet le montant en valeur absolue
    		if (montant.signum() < 0) {
    			montant = montant.abs();
    			sens = sens.sensInverse();
    		}
    	}
    }
    
    public boolean isDebit() {
        return Sens.DEBIT.equals(getSens());
    }
    
    public boolean isCrebit() {
        return Sens.CREDIT.equals(getSens());
    }
    
    /**
     * @return Le sens
     */
    public Sens getSens() {
    	return sens;
    }
    
    public BigDecimal getMontant() {
        return montant;
    }
    
    public String getGesCode() {
        return gesCode;
    }
    
    public void setGesCode(String gesCode) {
        this.gesCode = gesCode;
    }
    
    public String getPcoNum() {
        return pcoNum;
    }

	public ISepaSddEcheancier getEcheancier() {
		return echeancier;
	}

    /**
     * Vérifier la cohérence entre le code de gestion du mouvement ajouté et du détail.
     * 
     * @param mouvement Le mouvement à contrôler
     * @throws PaiementException Si incohérence dans le code de gestion
     */
    private void verifierCoherenceGesCode(ItemPanier mouvement) throws PaiementException {
        if (getGesCode() != null && !mouvement.getGesCode().equals(getGesCode())) {
            String message = String.format(
                    "Un article avec un autre gesCode existe déjà, planCo=%s, ancien gesCode=%s, nouveau gesCode=%s, sens=%s", 
                    mouvement.getPcoNum(), getGesCode(), mouvement.getGesCode(), mouvement.getSens());
            throw new PaiementException(message, PaiementException.Code.GES_CODE);
        }
    }
}
