package org.cocktail.fwkcktlcompta.server.scolarite;

import java.util.Date;

/**
 * Un panier pour les paiements ou les remboursements.
 * 
 * @author Pascal MACOUIN
 */
public class Panier {
    private String codeRne;
    private String gesCode;
    private Date datePaiement;

    /**
     * @param codeRne le code de l'établissement
     * @param gesCode le code gestion
     * @param datePaiement la date de paiement
     */
    public Panier(String codeRne, String gesCode, Date datePaiement) {
        this.codeRne = codeRne;
        this.gesCode = gesCode;
        this.datePaiement = datePaiement;
    }
    
    public String getCodeRne() {
        return codeRne;
    }
    
    public Date getDatePaiement() {
        return datePaiement;
    }
    
    public String getGesCode() {
        return gesCode;
    }
}
