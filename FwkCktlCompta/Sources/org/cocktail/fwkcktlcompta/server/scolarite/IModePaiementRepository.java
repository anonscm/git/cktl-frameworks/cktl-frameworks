package org.cocktail.fwkcktlcompta.server.scolarite;

import org.cocktail.fwkcktlcompta.server.metier.EOModePaiement;

/**
 * Repository des modes de paiement.
 *  
 * @author Pascal MACOUIN
 */
public interface IModePaiementRepository {

	/**
	 * @param modCode Le modCode du mode de paiement recherché
	 * @param exeOrdre L'exercice
	 * @return Un mode de paiement valide pour l'exercice
	 * @throws PaiementException Si aucun mode de paiement valide n'est trouvé pour l'exercice
	 */
	EOModePaiement fetchRequiredModePaiementExercice(String modCode, Long exeOrdre) throws PaiementException;

}