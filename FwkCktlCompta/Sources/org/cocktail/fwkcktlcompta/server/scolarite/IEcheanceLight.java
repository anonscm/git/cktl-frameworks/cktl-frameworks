package org.cocktail.fwkcktlcompta.server.scolarite;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Interface représentant une échéance "light" destiné à être transformé
 * en échéance sepa.
 * 
 * 
 * @author Alexis Tual
 *
 */
public interface IEcheanceLight {

    /**
     * @return le montant de l'échéance
     */
    BigDecimal montant();
    
    /**
     * @return la date de l'échéance
     */
    Date dateEcheance();
    
}
