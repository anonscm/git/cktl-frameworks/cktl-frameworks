package org.cocktail.fwkcktlcompta.server.scolarite;

import org.cocktail.fwkcktlcompta.common.entities.IBordereau;

import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * 
 * Service de bascule des paiements de la scol vers la compta.
 * Un bordereau est créé par jour contenant un brouillard par planco.
 * 
 * Les paiements à effectuer sont transmis via un objet panier que le client
 * de l'API doit construire.
 * 
 * @see PanierPaiement
 * 
 * @author Alexis Tual
 * @author Pascal Macouin
 */
public interface IPaiementService {

    /**
     * @param panier le panier contenant les paiements
     * @return le bordereau créé ainsi que tous les brouillards associés
     * @throws PaiementException si le paiement échoue pour les raisons suivantes  :
     * 
     * <ul>
     *  <li> Si le panier n'est pas équilibré (somme des crédits != somme des débits) </li>
     *  <li> Si le code de gestion varie </li>
     *  <li> Si l'utilisateur effectuant l'opération n'a pas les droits </li>
     * </ul>
     * 
     * Attention une {@link ValidationException} peut tout de même arriver si des infos sont manquantes.
     * Le client doit donc catcher {@link PaiementException} lors de l'appel et ValidationException plus tard
     * lors du saveChanges().
     * 
     */
    IBordereau enregistrerPaiement(PanierPaiement panier) throws PaiementException;
    
    /**
     * @param panier le panier contenant les paiements
     * @return le bordereau créé ainsi que tous les mandats, brouillards et depenses associés
     * @throws PaiementException si le paiement échoue pour les raisons suivantes  :
     * 
     * <ul>
     *  <li> Si le panier n'est pas équilibré (somme des crédits != somme des débits) </li>
     *  <li> Si le code de gestion varie </li>
     *  <li> Si l'utilisateur effectuant l'opération n'a pas les droits </li>
     * </ul>
     * 
     * Attention une {@link ValidationException} peut tout de même arriver si des infos sont manquantes.
     * Le client doit donc catcher {@link PaiementException} lors de l'appel et ValidationException plus tard
     * lors du saveChanges().
     * 
     */
    IBordereau enregistrerRemboursement(PanierRemboursement panier) throws PaiementException;
    
    /**
     * @param bordereau le bordereau à supprimer
     * @throws PaiementException
     * <ul>
     *  <li> si le borderau a été visé </li>
     *  <li> Si le borderau n'est pas trouvé </li>
     * </ul>    */
    void supprimerBordereau(IBordereau bordereau) throws PaiementException;
}
