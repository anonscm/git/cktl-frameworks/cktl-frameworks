package org.cocktail.fwkcktlcompta.server.scolarite;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumAdresse;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumRib;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddParam;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.SepaSddGenerateurEcheancierConstant;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHelper;
import org.cocktail.fwkcktlcompta.server.metier.EOCoriandreInscription;
import org.cocktail.fwkcktlcompta.server.metier.EOGrhumAdresse;
import org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam;
import org.cocktail.fwkcktlcompta.server.sepasdd.factories.SepaSddMandatFactory;
import org.joda.time.LocalDate;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * 
 * Service de manipulation des mandats sepa pour la scol.
 * 
 * @author Alexis Tual
 *
 */
public class PrelevementSepaService {

    private Integer persIdGestionnaire;
    private Integer idSepaSddParam;
    private EOEditingContext editingContext;
    private EOGrhumPersonne createur;
    
    /**
     * @param persIdGestionnaire le persId de la personne effecutant l'action
     * @param idSepaSddParam l'id du SepaSddParam à utiliser
     * @param editingContext l'ec
     */
    public PrelevementSepaService(Integer persIdGestionnaire, Integer idSepaSddParam, EOEditingContext editingContext) {
        this.editingContext = editingContext;
        this.persIdGestionnaire = persIdGestionnaire;
        this.idSepaSddParam = idSepaSddParam;
        this.createur = EOGrhumPersonne.fetchRequiredByKeyValue(editingContext, EOGrhumPersonne.PERS_ID_KEY, persIdGestionnaire);
    }

    /**
     * Crée un prélèvement Sepa pour la scolarité.
     * 
     * @param libelle un libellé pour le mandat
     * @param debiteurPersid l'id du débiteur
     * @param adresseDebiteurId l'id de l'adresse du débiteur
     * @param debiteurRibId l'id du rib du débiteur
     * @param persIdTiersDebiteur désignation du tiers débiteur pour le compte duquel le paiement est effectué
     * @return le mandat sepa créé
     */
    public ISepaSddMandat creerMandat(String libelle,
            Integer debiteurPersid, Integer adresseDebiteurId, Integer debiteurRibId, Integer persIdTiersDebiteur) {
        
        IGrhumPersonne debiteur = EOGrhumPersonne.fetchRequiredByKeyValue(editingContext, EOGrhumPersonne.PERS_ID_KEY, debiteurPersid);
        IGrhumAdresse adresse = EOGrhumAdresse.fetchRequiredByKeyValue(editingContext, EOGrhumAdresse.ADR_ORDRE_KEY, adresseDebiteurId);
        IGrhumRib rib = EOGrhumRib.fetchRequiredByKeyValue(editingContext, EOGrhumRib.RIB_ORDRE_KEY, debiteurRibId);
        ISepaSddParam paramSepa = EOSepaSddParam.fetchRequiredByKeyValue(editingContext, EOSepaSddParam.ID_SEPA_SDD_PARAM_KEY, idSepaSddParam);

        ISepaSddMandat sepaSddMandat = SepaSddMandatFactory.creerMandat(editingContext, createur);
        sepaSddMandat.setCTypePrelevement(SepaSddMandatHelper.TYPE_PRELEVEMENT_CODE_RECURRENT);
        sepaSddMandat.setLibelle(libelle);
        sepaSddMandat.setToSepaSddParamRelationship(paramSepa);
        sepaSddMandat.setToDebiteurPersonneRelationship(debiteur);
        sepaSddMandat.setToDebiteurAdresseRelationship(adresse);
        sepaSddMandat.setToDebiteurRibRelationship(rib);
        
        if (persIdTiersDebiteur != null) {
            IGrhumPersonne tiersDebiteur = EOGrhumPersonne.fetchRequiredByKeyValue(editingContext, EOGrhumPersonne.PERS_ID_KEY, persIdTiersDebiteur);
            sepaSddMandat.setToTiersDebiteurPersonneRelationship(tiersDebiteur);
        }
        
        SepaSddMandatHelper.getSharedInstance().prepareBeforeSave(sepaSddMandat);
        return sepaSddMandat;
    }
    
    /**
     * Créé un échéancier à partir d'un mandat et d'un id d'inscription
     * 
     * @param mandat le mandat
     * @param paiementId l'id de l'inscription (correspondant à l'id d'etudiant_annee)
     * @param echeances les échéances issues de la scol
     * @return l'échéancier
     */
    public ISepaSddEcheancier creerEcheancier(ISepaSddMandat mandat, List<IEcheanceLight> echeances, Integer paiementId) {
        ISepaSddEcheancier echeancier = EOSepaSddEcheancier.creerInstance(editingContext);
        ISepaSddOrigineType origineType = EOSepaSddOrigineType.fetchRequiredByKeyValue(editingContext, EOSepaSddOrigineType.TYPE_CODE_KEY, EOSepaSddOrigineType.TYPE_CODE_CORIANDRE);
        ISepaSddOrigineEntity origineEntity = EOCoriandreInscription.fetchRequiredByKeyValue(editingContext, EOCoriandreInscription.IDENTIFIANT_KEY, paiementId);

        echeancier = new SepaSddEcheancierHelper.BuilderEcheancier(echeancier, persIdGestionnaire, createur)
        .mandat(mandat)
        .origine(origineType, origineEntity)
        .build();

        for (IEcheanceLight echeance : echeances) {
            LocalDate localDate = new LocalDate(echeance.dateEcheance());
            creerEcheance(echeancier, localDate, echeance.montant());
        }
        return echeancier;
    }

    private ISepaSddEcheance creerEcheance(ISepaSddEcheancier echeancier, LocalDate dateEcheance, BigDecimal montant) {
        SepaSddGenerateurEcheancierConstant generateurEcheances = new SepaSddGenerateurEcheancierConstant(null, null, null, persIdGestionnaire, createur);
        ISepaSddEcheance echeance = generateurEcheances.creerEcheance(echeancier, dateEcheance, montant, createur);
        return echeance;
    }
    
    /**
     * @param echeancier Un échéancier
     * @return La dernière écheance de l'échéancier
     */
    public ISepaSddEcheance rechercherDerniereEcheance(ISepaSddEcheancier echeancier) {
		return (ISepaSddEcheance) echeancier.getEcheancesTrieesParDateEcheanceASC(false).lastObject();
    }
}
