package org.cocktail.fwkcktlcompta.server.scolarite;

import org.cocktail.fwkcktlcompta.common.entities.IPlanComptable;
import org.cocktail.fwkcktlcompta.common.entities.IPlanComptableExer;

/**
 * 
 * Repository des plan comptables
 * 
 * @author Alexis Tual
 *
 */
public interface IPlanComptableRepository {

    /**
     * @param pcoNum Un plan comptable
     * @param exeOrdre exercice
     * @return un objet plan comptable valide correspondant au <code>pcoNum</code> et <code>exeOrdre</code>
     * @throws PaiementException si aucun planco valide correspondant au <code>pcoNum</code> n'est trouvée sur l'exercice
     */
    IPlanComptable fetchPlanComptable(String pcoNum, Long exeOrdre) throws PaiementException;
    
    /**
     * @param pcoNum Un plan comptable
     * @param exeOrdre exercice
     * @return un objet plan comptable valide correspondant au <code>pcoNum</code> et <code>exeOrdre</code>
     * @throws PaiementException si aucun planco valide correspondant au <code>pcoNum</code> n'est trouvée sur l'exercice
     */
    IPlanComptableExer fetchRequiredPlanComptableExercice(String pcoNum, Long exeOrdre) throws PaiementException;
}
