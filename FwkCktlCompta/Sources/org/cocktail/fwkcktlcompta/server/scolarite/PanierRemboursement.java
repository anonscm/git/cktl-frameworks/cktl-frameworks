package org.cocktail.fwkcktlcompta.server.scolarite;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRib;

/**
 * Le panier pour les remboursements.
 * 
 * @author Pascal MACOUIN
 */
public class PanierRemboursement extends Panier implements Iterable<DetailPanierFournisseur> {

	private List<DetailPanierFournisseur> details = new ArrayList<DetailPanierFournisseur>();

    /**
     * @param codeRne le code de l'établissement
     * @param gesCode le code gestion
     * @param datePaiement la date de paiement
     */
	public PanierRemboursement(String codeRne, String gesCode, Date datePaiement) {
		super(codeRne, gesCode, datePaiement);
	}

    /** 
     * @return l'itérateur sur les détails du panier
     */
	public Iterator<DetailPanierFournisseur> iterator() {
		return details.iterator();
	}

    /**
     * Ajouter ces articles et ces paiements au panier.
     *
     * @param numeroMandat Le numéro de mandat => id du paiement
     * @param fournisseur Le fournisseur 
     * @param ribFournisseur Le RIB du fournisseur
     * @param adresseFournisseur L'ardresse du fournisseur
     * @param modeCodeRemboursement le modeCode du mode de paiement pour le remboursement (table MARACUJA.MODE_PAIEMENT)
     * @param articles la liste des articles
     * @param paiement le paiement
     * @throws PaiementException si la balance entre les articles et les paiements n'est pas bonne
     */
    public void ajouter(Integer numeroMandat, IFournis fournisseur, IRib ribFournisseur, IAdresse adresseFournisseur, String modeCodeRemboursement, List<Article> articles, Paiement paiement) throws PaiementException {
    	DetailPanierFournisseur detailPanierFournisseur = new DetailPanierFournisseur(numeroMandat, fournisseur, ribFournisseur, adresseFournisseur, modeCodeRemboursement);
    	detailPanierFournisseur.ajouter(articles, paiement);
    	
    	details.add(detailPanierFournisseur);
    }
}
