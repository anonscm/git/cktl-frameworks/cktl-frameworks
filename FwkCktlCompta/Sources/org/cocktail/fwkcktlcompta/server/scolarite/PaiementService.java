package org.cocktail.fwkcktlcompta.server.scolarite;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.common.entities.IBordereau;
import org.cocktail.fwkcktlcompta.common.entities.IGestion;
import org.cocktail.fwkcktlcompta.common.entities.IPlanComptable;
import org.cocktail.fwkcktlcompta.common.entities.IPlanComptableExer;
import org.cocktail.fwkcktlcompta.common.entities.ITypeBordereau;
import org.cocktail.fwkcktlcompta.server.metier.EOBordereau;
import org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard;
import org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo;
import org.cocktail.fwkcktlcompta.server.metier.EODepense;
import org.cocktail.fwkcktlcompta.server.metier.EOGestion;
import org.cocktail.fwkcktlcompta.server.metier.EOMandat;
import org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeBordereau;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau;
import org.cocktail.fwkcktlcompta.server.scolarite.ItemPanier.Sens;
import org.cocktail.fwkcktlcompta.server.scolarite.PaiementException.Code;
import org.cocktail.fwkcktljefyadmin.common.GFCApplicationUser;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXFetchSpecification;

/**
 * 
 * Implémentation du service de paiement dans la GFC.
 * 
 * @see IPaiementService
 * 
 * @author Alexis Tual
 *
 */
public class PaiementService implements IPaiementService {

	private static final Logger LOG = Logger.getLogger(PaiementService.class);
    
    private static final String LIBELLE_BORDEREAU_DROITS_UNIVERSITAIRES = "%s - BORDEREAU DROITS UNIVERSITAIRES pour %s N° %s en date du %s";
    private static final String LIBELLE_BORDEREAU_REMBOURSEMENT_DROITS_UNIVERSITAIRES = "%s - BORDEREAU REMBOURSEMENT DROITS UNIVERSITAIRES %s N° %s en date du %s";
	private static final String OPERATION_MANDAT_BROUILLARD_CREDIT = "VISA CTP";
	private static final String OPERATION_MANDAT_BROUILLARD_DEBIT = "VISA SCOLARITE";

    private UserInfo userInfo;
    private EOEditingContext editingContext;
    private IPlanComptableRepository planCoRepository;
    private IModePaiementRepository modePaiementRepository;
    private GFCApplicationUserProvider gfcApplicationUserProvider;
    private EOTypeOrigineBordereau typeOrigineBorderauScolarite;

    /**
     * @param userInfo le user connecté
     * @param editingContext l'editing context
     */
    public PaiementService(UserInfo userInfo, EOEditingContext editingContext) {
        this.userInfo = userInfo;
        this.editingContext = editingContext;
        this.planCoRepository = getDefaultPlanCoRepository();
        this.modePaiementRepository = getDefaultModePAiementRepository();
        this.gfcApplicationUserProvider = getDefaultGFCApplicationUserProvider();
    }

	private IPlanComptableRepository getDefaultPlanCoRepository() {
        return new PlanComptableRepository(editingContext);
    }

	private IModePaiementRepository getDefaultModePAiementRepository() {
		return new ModePaiementRepository(editingContext);
	}
	
    private GFCApplicationUserProvider getDefaultGFCApplicationUserProvider() {
        return new GFCApplicationUserProvider() {
            public GFCApplicationUser getGFCApplicationUser() {
                return new GFCApplicationUser(editingContext, userInfo.persId().intValue());
            }
        };
    }

    /**
     * {@inheritDoc}
     */
    public IBordereau enregistrerPaiement(PanierPaiement panier) throws PaiementException {
        EOBordereau bordereau = creerBordereauPaiement(panier);
        EOBordereauInfo bordereauInfo = EOBordereauInfo.creerInstance(editingContext);
        bordereauInfo.setBorLibelle(getBordereauLibelle(LIBELLE_BORDEREAU_DROITS_UNIVERSITAIRES, bordereau, panier.getDatePaiement(), panier.getCodeRne()));
        bordereauInfo.setToBordereauRelationship(bordereau);
        
        for (DetailPanier detail : panier) {
        	creerBordereauBrouillard(bordereau, bordereauInfo, detail);
        }
        
        return bordereau;
    }
    
    /**
     * {@inheritDoc}
     */
    public IBordereau enregistrerRemboursement(PanierRemboursement panier) throws PaiementException {
        EOBordereau bordereau = creerBordereauRemboursement(panier);
        EOBordereauInfo bordereauInfo = EOBordereauInfo.creerInstance(editingContext);
        bordereauInfo.setBorLibelle(getBordereauLibelle(LIBELLE_BORDEREAU_REMBOURSEMENT_DROITS_UNIVERSITAIRES, bordereau, panier.getDatePaiement(), panier.getCodeRne()));
        bordereauInfo.setToBordereauRelationship(bordereau);
        
        for (DetailPanierFournisseur detail : panier) {
        	creerMandat(bordereau, bordereauInfo, detail);
        }
        
        return bordereau;
    }
    
    /**
     * {@inheritDoc}
     */
    public void supprimerBordereau(IBordereau bordereau) throws PaiementException {
    	if (bordereau.toTypeBordereau().equals(getTypeBordereauScolInscription())) {
    		verifierDroitCreerBordereauPaiement(userInfo.persId().intValue());
    	} else if (bordereau.toTypeBordereau().equals(getTypeBordereauScolRemboursement())) {
    		verifierDroitCreerBordereauRemboursement(userInfo.persId().intValue());
    	} else {
    		ITypeBordereau typeBordereau = bordereau.toTypeBordereau();
    		String message = String.format("Le type de bordereau %s/%s n'est pas géré par cette méthode", typeBordereau.tboSousType(), typeBordereau.tboType());
    		throw new PaiementException(message, PaiementException.Code.TYPE_BORDEREAU);
    	}
    	
    	supprimerBordereauExistantSiNonVise(bordereau.borNum(), bordereau.toGestion(), bordereau.toTypeBordereau());
    }

    /**
     * Créer un bordereau de paiement.
     * @param panier le panier
     * @param rne le code RNE de l'établissement
     * @return le bordereau créé
     * @throws PaiementException
     */
    private EOBordereau creerBordereauPaiement(PanierPaiement panier) throws PaiementException {
    	GFCApplicationUser gfcUser = verifierDroitCreerBordereauPaiement(userInfo.persId().intValue());
    	ITypeBordereau typeBordereau = getTypeBordereauScolInscription();
    	
    	return creerBordereau(typeBordereau, panier, gfcUser);
    }
    
    /**
     * Créer un bordereau de remboursement.
     * @param panier le panier
     * @param rne le code RNE de l'établissement
     * @return le bordereau créé
     * @throws PaiementException
     */
    private EOBordereau creerBordereauRemboursement(PanierRemboursement panier) throws PaiementException {
    	GFCApplicationUser gfcUser = verifierDroitCreerBordereauRemboursement(userInfo.persId().intValue());
    	ITypeBordereau typeBordereau = getTypeBordereauScolRemboursement();
    	
    	return creerBordereau(typeBordereau, panier, gfcUser);
    }
    
    private EOBordereau creerBordereau(ITypeBordereau typeBordereau, Panier panier, GFCApplicationUser gfcUser) throws PaiementException {
        Integer borNum = getNumeroBordereau(panier.getDatePaiement(), panier.getCodeRne());
        EOGestion gestion = getGestion(panier.getGesCode());
        supprimerBordereauExistantSiNonVise(borNum, gestion, typeBordereau);
        
        EOBordereau bordereau = EOBordereau.creerInstance(editingContext);
        bordereau.setBorDateCreation(new NSTimestamp());
        bordereau.setBorEtat(EOBordereau.BOR_ETAT_VALIDE);
        bordereau.setBorOrdre(borNum);
        bordereau.setBorNum(borNum);
        bordereau.setToExerciceRelationship(getExercice(panier.getDatePaiement()));
		bordereau.setToGestionRelationship(gestion);
        bordereau.setToTypeBordereauRelationship((EOTypeBordereau) typeBordereau);
        bordereau.setToUtilisateurRelationship(gfcUser.getUtilisateur());
        
        return bordereau;
    }

    private EOBordereauBrouillard creerBordereauBrouillard(EOBordereau bordereau, EOBordereauInfo bordereauInfo, DetailPanier detail) throws PaiementException {
    	if (detail.isMontantZero()) {
    		return null;
    	}
    	
        EOBordereauBrouillard brouillard = EOBordereauBrouillard.creerInstance(editingContext);
        brouillard.setToBordereauRelationship(bordereau);
        brouillard.setToExerciceRelationship(bordereau.toExercice());
        brouillard.setToGestionRelationship(getGestion(detail.getGesCode()));
        brouillard.setBobSens(detail.getSens().getCode());
        brouillard.setBobMontant(detail.getMontant());
        brouillard.setBobEtat(EOBordereauBrouillard.ETAT_VALIDE);
        brouillard.setBobOperation(EOBordereauBrouillard.OPERATION_VISA);
        IPlanComptable planComptable = planCoRepository.fetchPlanComptable(detail.getPcoNum(), bordereau.toExercice().exeExercice());
        brouillard.setToPlanComptableRelationship((EOPlanComptable) planComptable);
        brouillard.setBobLibelle1(planComptable.libelleLong());
        brouillard.setBobLibelle2(bordereauInfo.borLibelle());
        
        // Si le détail du panier contient un échéancier, on génère un lien entre cet échéancier et le bordereau brouillard
        if (detail.getEcheancier() != null) {
        	EOSepaSddEcheancierBob.createEOSepaSddEcheancierBob(editingContext, brouillard, (EOSepaSddEcheancier) detail.getEcheancier());
        }
        
        return brouillard;
    }

    private EOMandat creerMandat(EOBordereau bordereau, EOBordereauInfo bordereauInfo, DetailPanierFournisseur detail) throws PaiementException {
    	EOMandat mandat = EOMandat.creerInstance(editingContext);
    	mandat.setToBordereauRelationship(bordereau);
    	mandat.setToExerciceRelationship(bordereau.toExercice());
    	mandat.setToFournisRelationship((EOFournis) detail.getFournisseur());
    	mandat.setToGestionRelationship(bordereau.toGestion());
    	mandat.setManEtat(EOMandat.MAN_ETAT_ATTENTE);
    	mandat.setManEtatRemise(EOMandat.MAN_ETAT_ATTENTE);
    	BigDecimal total = detail.calculerTotal();
		mandat.setManHt(total);
    	mandat.setManTtc(total);
    	mandat.setManTva(BigDecimal.ZERO);
    	mandat.setManNbPiece(Integer.valueOf(1));
    	mandat.setManNumero(detail.getNumeroMandat());
    	mandat.setManOrdre(Integer.valueOf(0));
    	mandat.setToModePaiementRelationship(modePaiementRepository.fetchRequiredModePaiementExercice(detail.getModeCodeRemboursement(), bordereau.toExercice().exeExercice()));
    	mandat.setToPlanComptableExerRelationship((EOPlanComptableExer) planCoRepository.fetchRequiredPlanComptableExercice(detail.getPaiement().getPcoNum(), bordereau.toExercice().exeExercice()));
    	mandat.setToTypeOrigineBordereauRelationship(getTypeOrigineBorderauScolarite());
    	mandat.setToRibRelationship((EORib) detail.getRibFournisseur());
    	mandat.setToRibOrdonnateurRelationship((EORib) detail.getRibFournisseur());
    	//mandat.setAttentePaiement(4); // Valeur par défaut de la colonne dans la BDD
    	
    	for (DetailPanier detailPanier : detail) {
			creerMandatBrouillard(mandat, detailPanier);
		}
    	
    	creerDepense(mandat, detail);
    	
    	return mandat;
    }
    
    private EOMandatBrouillard creerMandatBrouillard(EOMandat mandat, DetailPanier detail) throws PaiementException {
    	if (detail.isMontantZero()) {
    		return null;
    	}
    	
    	EOMandatBrouillard mandatBrouillard = EOMandatBrouillard.creerInstance(editingContext);
    	mandatBrouillard.setToMandatRelationship(mandat);
    	mandatBrouillard.setToExerciceRelationship(mandat.toExercice());
    	mandatBrouillard.setMabMontant(detail.getMontant());
    	mandatBrouillard.setMabSens(detail.getSens().getCode());
    	mandatBrouillard.setToGestionRelationship(getGestion(detail.getGesCode()));
        IPlanComptableExer planComptable = planCoRepository.fetchRequiredPlanComptableExercice(detail.getPcoNum(), mandat.toExercice().exeExercice());
    	mandatBrouillard.setToPlanComptableExerRelationship((EOPlanComptableExer) planComptable);
    	
    	if (Sens.CREDIT.equals(detail.getSens())) {
    		mandatBrouillard.setMabOperation(OPERATION_MANDAT_BROUILLARD_CREDIT);
    	} else {
    		mandatBrouillard.setMabOperation(OPERATION_MANDAT_BROUILLARD_DEBIT);
    	}
    	
    	return mandatBrouillard;
    }
    
    private EODepense creerDepense(EOMandat mandat, DetailPanierFournisseur detail) {
    	EODepense depense = EODepense.creerInstance(editingContext);
    	depense.setToMandatRelationship(mandat);
    	depense.setDepEtat(EODepense.DEP_ETAT_VALIDE);
    	depense.setDepDateReception(new NSTimestamp());
    	depense.setDepDateService(new NSTimestamp());
    	depense.setDepFournisseur(detail.getFournisseur().toIIndividu().getNomPrenomAffichage());
    	depense.setDepAdresse(formaterAdresse(detail.getAdresseFournisseur()));
    	depense.setDepHt(mandat.manHt());
    	depense.setDepMontantDisquette(mandat.manHt());
    	depense.setDepTtc(mandat.manTtc());
    	depense.setDepTva(mandat.manTva());
    	depense.setDepLigneBudgetaire(" ");
    	depense.setDepNumero(mandat.manNumero().toString());
    	depense.setDepOrdre(Integer.valueOf(0));
    	depense.setManOrdre(Integer.valueOf(0));
    	depense.setToFournisRelationship(mandat.toFournis());
    	depense.setToRibRelationship(mandat.toRib());
    	depense.setDepSuppression(EODepense.DEP_SUPPRESSION_NON);
    	depense.setToExerciceRelationship(mandat.toExercice());
    	depense.setToGestionRelationship(mandat.toGestion());
    	depense.setToModePaiementRelationship(mandat.toModePaiement());
    	depense.setToPlanComptableExerRelationship(mandat.toPlanComptableExer());
    	depense.setToUtilisateurRelationship(mandat.toBordereau().toUtilisateur());
    	depense.setDepDateFournis(new NSTimestamp());
    	
    	return depense;
    }
    
    /**
     * Formater l'adresse sur une seule ligne.
     * 
     * @param adresse Un adresse
     * @return L'adresse formatée sur une seule ligne
     */
    private String formaterAdresse(IAdresse adresse) {
    	ArrayList<String> tableauAdresse = new ArrayList<String>();
    	
    	if (adresse.adrAdresse1() != null) {
    		tableauAdresse.add(adresse.adrAdresse1().trim());
    	}
    	
    	if (adresse.adrAdresse2() != null) {
    		tableauAdresse.add(adresse.adrAdresse2().trim());
    	}
    	
    	if (adresse.codePostal() != null) {
    		tableauAdresse.add(adresse.codePostal().trim());
    	}
    	
    	if (adresse.cpEtranger() != null) {
    		tableauAdresse.add(adresse.cpEtranger().trim());
    	}
    	
    	if (adresse.ville() != null) {
    		tableauAdresse.add(adresse.ville().trim());
    	}
    	
    	return StringUtils.join(tableauAdresse, " ");
    }
    
    private String getBordereauLibelle(String modeleLibelle, EOBordereau bordereau, Date datePaiement, String codeRne) {
        String exercice = bordereau.toExercice().exeExercice().toString();
        String date = new SimpleDateFormat("dd/MM/yyyy").format(datePaiement);
        String borNum = bordereau.borNum().toString();
        
        return String.format(modeleLibelle, exercice, codeRne, borNum, date);
    }

    private GFCApplicationUser verifierDroitCreerBordereauPaiement(int persId) throws PaiementException {
        GFCApplicationUser gfcUser = gfcApplicationUserProvider.getGFCApplicationUser();
        if (!gfcUser.hasDroitCreerBordereauInscription()) {
            String message = String.format("L'utilisateur de persId=%s n'a pas les droits de générer des bordereaux d'inscription", persId);
            throw new PaiementException(message, Code.DROITS);
        }
        return gfcUser;
    }
    
    private GFCApplicationUser verifierDroitCreerBordereauRemboursement(int persId) throws PaiementException {
        GFCApplicationUser gfcUser = gfcApplicationUserProvider.getGFCApplicationUser();
        if (!gfcUser.hasDroitCreerBordereauRemboursement()) {
            String message = String.format("L'utilisateur de persId=%s n'a pas les droits de générer des bordereaux de remboursement", persId);
            throw new PaiementException(message, Code.DROITS);
        }
        return gfcUser;
    }
    
    /**
     * Retourne le bordereau de paiement.
     * 
     * @param borNum un numéro de bordereau
     * @param gesCode un code de gestion
     * @return le bordereau (ou <code>null</code> si non trouvé)
     * @throws PaiementException 
     */
    public IBordereau getBordereauPaiement(Integer borNum, String gesCode) throws PaiementException {
        EOGestion gestion = getGestion(gesCode);
        ITypeBordereau typeBordereau = getTypeBordereauScolInscription();
    	return getBordereauExistant(borNum, gestion, typeBordereau);
    }
    
    /**
     * Retourne le bordereau de remboursement.
     * 
     * @param borNum un numéro de bordereau
     * @param gesCode un code de gestion
     * @return le bordereau (ou <code>null</code> si non trouvé)
     * @throws PaiementException 
     */
    public IBordereau getBordereauRemboursement(Integer borNum, String gesCode) throws PaiementException {
        EOGestion gestion = getGestion(gesCode);
        ITypeBordereau typeBordereau = getTypeBordereauScolRemboursement();
    	return getBordereauExistant(borNum, gestion, typeBordereau);
    }
    
    /**
     * Retourne un bordereau.
     * 
     * @param borNum un numéro de bordereau
     * @param gestion un code gestion établissement
     * @param type un type de bordereau
     * @return le bordereau trouvé ou <code>null</code>
     */
    private EOBordereau getBordereauExistant(Integer borNum, IGestion gestion, ITypeBordereau type) {
        EOQualifier qualifier = EOBordereau.BOR_NUM.eq(borNum)
                                .and(EOBordereau.TO_TYPE_BORDEREAU.eq((EOTypeBordereau) type)
                                .and(EOBordereau.TO_GESTION.eq((EOGestion) gestion)));
        ERXFetchSpecification<EOBordereau> fspec = new ERXFetchSpecification<EOBordereau>(EOBordereau.ENTITY_NAME, qualifier, null);
        fspec.setRefreshesRefetchedObjects(true);
        
        return fspec.fetchObjects(editingContext).lastObject();
    }
        
    private void supprimerBordereauExistantSiNonVise(Integer borNum, IGestion gestion, ITypeBordereau type) throws PaiementException {
        EOBordereau bordereauExistant = getBordereauExistant(borNum, gestion, type);
        
        if (bordereauExistant != null) {
            if (bordereauExistant.isVise()) {
                String message = String.format("Un bordereau de type %s/%s avec le borNum=%s a déjà été visé", type.tboSousType(), type.tboType(), borNum);
                throw new PaiementException(message, Code.VISA_EXISTANT);
            }
            LOG.info(String.format("Le bordereau borNum=%s de type %s/%s existe et est non visé, il sera donc supprimé puis recréé", borNum, type.tboSousType(), type.tboType()));
            
            // Pour les bordereaux de paiement
            for (EOBordereauBrouillard bordereauBrouillard : bordereauExistant.toBordereauBrouillards()) {
				bordereauBrouillard.deleteAllToSepaSddEcheancierBobsRelationships();
			}
            bordereauExistant.deleteAllToBordereauBrouillardsRelationships();
            
            // Pour les bordereaux de remboursement
            for (EOMandat mandat : bordereauExistant.toMandats()) {
				mandat.deleteAllToMandatBrouillardsRelationships();
				mandat.deleteAllToDepensesRelationships();
			}
            bordereauExistant.deleteAllToMandatsRelationships();
            
            // Entités générales
            bordereauExistant.deleteAllToBordereauInfosRelationships();
            bordereauExistant.delete();
        }
    }
    
    private ITypeBordereau getTypeBordereauScolInscription() {
        return getTypeBordereauScol(EOTypeBordereau.TypeBordereauDroitsUniversitaires);
    }
    
    private ITypeBordereau getTypeBordereauScolRemboursement() {
        return getTypeBordereauScol(EOTypeBordereau.TypeBordereauRembousrementsDroitsUniversitaires);
    }
    
    private ITypeBordereau getTypeBordereauScol(String typeBordereauScolarite) {
        EOQualifier qualifier = EOTypeBordereau.TBO_TYPE.eq(typeBordereauScolarite)
                                    .and(EOTypeBordereau.TBO_SOUS_TYPE.eq(EOTypeBordereau.SOUS_TYPE_SCOLARITE));
        return EOTypeBordereau.fetchByQualifier(editingContext, qualifier);
    }

    private EOTypeOrigineBordereau getTypeOrigineBorderauScolarite() {
    	if (typeOrigineBorderauScolarite == null) {
    		typeOrigineBorderauScolarite = EOTypeOrigineBordereau.getTypeOrigineBordereauScolarite(editingContext);
    	}
    	
    	return typeOrigineBorderauScolarite;
    }
    
    private EOGestion getGestion(String gesCode) throws PaiementException {
        EOGestion gestion = EOGestion.fetchByQualifier(editingContext, EOGestion.GES_CODE.eq(gesCode));
        
        if (gestion == null) {
            String message = String.format("Le code gestion %s n'existe pas (%s)", gesCode, EOGestion.ENTITY_TABLE_NAME);
            throw new PaiementException(message, Code.GES_CODE);
        }
        
		return gestion;
    }

    private EOExercice getExercice(Date datePaiement) throws PaiementException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(datePaiement);
        Integer year = cal.get(Calendar.YEAR);
        EOExercice exercice = EOExercice.getExercice(editingContext, year);
        if (exercice == null || !(exercice.estOuvert() || exercice.estRestreint())) {
            String message = String.format("L'exercice %s n'est pas ouvert ou en préparation ou bien n'existe pas", year);
            throw new PaiementException(message, Code.EXERCICE);
        }
        return exercice;
    }

    /**
     * Retourne le numéro de bordereau.
     * 
     * @param datePaiement La date de paiement
     * @param codeRne Le code RNE de l'établissement
     * @return Le numéro du bordereau
     */
    public Integer getNumeroBordereau(Date datePaiement, String codeRne) {
    	// Sélection de la partie numérique du code RNE
    	String partieNumeriqueRne = StringUtils.substring(codeRne, 1, 3);
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        String dateFormatee = df.format(datePaiement);
        return Integer.valueOf(dateFormatee + partieNumeriqueRne);
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
    
    public void setGfcApplicationUserProvider(GFCApplicationUserProvider gfcApplicationUserProvider) {
        this.gfcApplicationUserProvider = gfcApplicationUserProvider;
    }
    
    public void setPlanCoRepository(IPlanComptableRepository planCoRepository) {
        this.planCoRepository = planCoRepository;
    }
    
    /**
     * Fournir un numéro d'ordre au bordereau, interface
     * pour rendre le code testable.
     */
    interface BordereauOrdreProvider {
        Integer provideFor(EOBordereau bordereau);
    }
    
    /**
     * Fournir un applicationUser GFC, interface pour rendre
     * le code testable.
     */
    interface GFCApplicationUserProvider {
        GFCApplicationUser getGFCApplicationUser();
    }
    
}
