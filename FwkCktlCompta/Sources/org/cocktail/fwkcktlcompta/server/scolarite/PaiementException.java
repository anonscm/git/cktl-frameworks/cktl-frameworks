package org.cocktail.fwkcktlcompta.server.scolarite;

/**
 * 
 * Exception lancée en cas de problème de paiement :
 * 
 * <ul>
 *  <li> Si le panier n'est pas équilibré (somme des crédits != somme des débits) </li>
 *  <li> Si le code de gestion varie </li>
 *  <li> Si l'utilisateur effectuant l'opération n'a pas les droits </li>
 * </ul>
 * 
 * @author Alexis Tual
 *
 */
public class PaiementException extends Exception {
  
    private static final long serialVersionUID = 1L;
    private Code code;
  
    /**
     * @param message le message de l'exception
     * @param code le code de l'exception
     */
    public PaiementException(String message, Code code) {
        super(message);
        this.code = code;
    }

    public Code getCode() {
        return code;
    }
    
    /**
     * les codes d'exception
     */
    public enum Code { SENS, GES_CODE, BALANCE, DROITS, PLANCO, VISA_EXISTANT, EXERCICE, TYPE_BORDEREAU, MODE_PAIEMENT }
    
}
