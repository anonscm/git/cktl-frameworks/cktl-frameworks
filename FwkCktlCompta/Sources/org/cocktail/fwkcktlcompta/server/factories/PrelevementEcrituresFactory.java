package org.cocktail.fwkcktlcompta.server.factories;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.common.entities.IPlanComptableExer;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheanceHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail;
import org.cocktail.fwkcktlcompta.server.metier.EOEmargement;
import org.cocktail.fwkcktlcompta.server.metier.EOGestion;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheanceEcd;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierEcd;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement;

import com.webobjects.eocontrol.EOEditingContext;

public class PrelevementEcrituresFactory {

	/**
	 * @param exercice
	 * @param gestion
	 * @param ecdIndex
	 * @param echeancier
	 * @return
	 * @throws Exception
	 */
	public EOEcritureDetail creerEcritureDetailDebitPourPriseEnChargeEcheancier(EOJefyAdminExercice exercice, EOGestion gestion, Integer ecdIndex, EOSepaSddEcheancier echeancier) throws Exception {
		try {
			EOEditingContext editingContext = echeancier.editingContext();
			//Récupérer le mode de recouvrement affecté à la recette (s'assurer que la recette est visée et que l'écriture de pec de l'echeancier n'existe pas deja)
			EOModeRecouvrement modeRecouvrement = (EOModeRecouvrement) SepaSddEcheancierHelper.getSharedInstance().getModeRecouvrement(echeancier);
			if (modeRecouvrement == null) {
				throw new Exception("Impossible de récupérer le mode de recouvrement associé à l'origine de l'échéancier");
			}
			if (!"ECHEANCIER".equals(modeRecouvrement.modDom())) {
				throw new Exception("Le mode de recouvrement " + modeRecouvrement.modCode() + " n'a pas comme domaine 'ECHEANCIER'.");
			}
			EOPlanComptableExer pco = modeRecouvrement.toPlanComptableExerVisa();
			if (pco == null) {
				throw new Exception("Le compte de 'paiement' associé au mode de recouvrement " + modeRecouvrement.modCode() + " affecté à l'origine de l'échéancier n'est pas défini.");
			}
			if (!pco.isValide()) {
				throw new Exception("Le compte de 'paiement' associé au mode de recouvrement " + modeRecouvrement.modCode() + " n'est pas VALIDE.");
			}
			BigDecimal montant = SepaSddEcheanceHelper.getSharedInstance().calculeMontantAPayerDesEcheancesNonAnnulees(echeancier.toSepaSddEcheances());
			String ecdLibelle = SepaSddEcheancierHelper.getSharedInstance().getLibelleEcheancierComplet(echeancier);

			//creer ecriture en debit 
			EOEcritureDetail ecDebitPriseEnchargeEcheancier = EOEcritureDetailFactory.getSharedInstance().creerEcritureDetailDebit(editingContext, null, exercice, pco, gestion, null, ecdIndex, montant, ecdLibelle);

			//Memoriser le lien entre l'echeancier et l'ecriture
			EOSepaSddEcheancierEcd.createEOSepaSddEcheancierEcd(editingContext, ecDebitPriseEnchargeEcheancier, echeancier);
			return ecDebitPriseEnchargeEcheancier;
		} catch (Exception e) {
			throw new Exception("Erreur lors de la création de l'écriture en débit pour la prise en charge prélèvement de l'échéancier \n" + SepaSddEcheancierHelper.getSharedInstance().getLibelleEcheancierComplet(echeancier) + " :\n" + e.getMessage(), e);
		}
	}

	/**
	 * @param exercice
	 * @param gestion
	 * @param ecdIndex
	 * @param echeancier
	 * @param pco
	 * @return
	 * @throws Exception
	 */
	public EOEcritureDetail creerEcritureDetailCreditPourPriseEnChargeEcheancier(EOJefyAdminExercice exercice, EOGestion gestion, Integer ecdIndex, EOSepaSddEcheancier echeancier, IPlanComptableExer pco) throws Exception {
		try {
			EOEditingContext editingContext = echeancier.editingContext();
			BigDecimal montant = SepaSddEcheanceHelper.getSharedInstance().calculeMontantAPayerDesEcheancesNonAnnulees(echeancier.toSepaSddEcheances());
			String ecdLibelle = SepaSddEcheancierHelper.getSharedInstance().getLibelleEcheancierComplet(echeancier);
			EOEcritureDetail ecCreditPriseEnchargeEcheancier = EOEcritureDetailFactory.getSharedInstance().creerEcritureDetailCredit(editingContext, null, exercice, (EOPlanComptableExer) pco, gestion, null, ecdIndex, montant, ecdLibelle);
			//On ne memoriser pas lien entre l'echeancier et l'ecriture en crédit, on peut le retrouver à partir du crédit
			return ecCreditPriseEnchargeEcheancier;
		} catch (Exception e) {
			throw new Exception("Erreur lors de la création de l'écriture en crédit pour la prise en charge prélèvement de l'échéancier \n" + SepaSddEcheancierHelper.getSharedInstance().getLibelleEcheancierComplet(echeancier) + " :\n" + e.getMessage(), e);
		}
	}

	private EOEcritureDetail getEcdDebitPriseEnChargeEcheancier(EOSepaSddEcheancier echeancier) {
		return (EOEcritureDetail) SepaSddEcheancierHelper.getSharedInstance().getLastEcritureDetailPriseEnCharge(echeancier);
	}

	public EOEcritureDetail creerEcritureDetailCreditPourEcheancePrelevee(EOJefyAdminExercice exercice, Integer ecdIndex, EOSepaSddEcheance echeance) throws Exception {
		try {
			EOEditingContext editingContext = echeance.editingContext();
			EOSepaSddEcheancier echeancier = echeance.toSepaSddEcheancier();
			BigDecimal montant = echeance.montant();
			String ecdLibelle = SepaSddEcheanceHelper.getSharedInstance().getLibelleEcheanceComplet(echeance);
			EOEcritureDetail ecdDebitPriseEnChargeEcheancier = getEcdDebitPriseEnChargeEcheancier(echeancier);

			if (ecdDebitPriseEnChargeEcheancier == null) {
				throw new Exception("Impossible de récupérer le débit de l'écriture de prise en charge de l'échéancier.");
			}

			if (ecdDebitPriseEnChargeEcheancier.ecdResteEmarger().compareTo(montant) < 0) {
				throw new Exception(
						"le montant de l'échéance à prélever (" + montant + ") est supérieur au reste à émarger du débit de l'écriture de prise en charge de l'échéancier (" + ecdDebitPriseEnChargeEcheancier.ecdResteEmarger() + ").");
			}

			//On prend le compte et le code gestion de l'ecriture de prise en charge de l'echeancier car les deux s'emargeront entre elles
			EOPlanComptableExer pco = ecdDebitPriseEnChargeEcheancier.toPlanComptableExer();
			EOGestion gestion = ecdDebitPriseEnChargeEcheancier.toGestion();

			// utilisation de la BE si besoin
			EOPlanComptableExer pcoAUtiliser = PlancoExerBeCache.getSharedInstance().getPlancomptableExerOuBe(editingContext, pco, exercice);			
			
			//creer ecriture en debit 
			EOEcritureDetail ecdCreditEcheance = EOEcritureDetailFactory.getSharedInstance().creerEcritureDetailCredit(editingContext, null, exercice, pcoAUtiliser, gestion, null, ecdIndex, montant, ecdLibelle);

			//Memoriser le lien entre l'echeance et l'ecriture
			EOSepaSddEcheanceEcd sepaSddEcheanceEcd = EOSepaSddEcheanceEcd.createEOSepaSddEcheanceEcd(editingContext, ecdCreditEcheance, echeance);

			return ecdCreditEcheance;
		} catch (Exception e) {
			throw new Exception("Erreur lors de la création du crédit de l'écriture de prélèvement pour l'échéance \n" + SepaSddEcheanceHelper.getSharedInstance().getLibelleEcheanceComplet(echeance) + " :\n" + e.getMessage(), e);
		}
	}

	public EOEmargement creerEmargementEntreEcritureEcheanceEtEcritureEcheancier(EOTypeEmargement typeEmargement, EOJefyAdminUtilisateur utilisateur, EOSepaSddEcheancier echeancier, EOEcritureDetail ecdEcheanceCredit) throws Exception {
		EOEcritureDetail ecdEcheancier = getEcdDebitPriseEnChargeEcheancier(echeancier);
		BigDecimal montant = ecdEcheanceCredit.ecdMontant().abs();
		return EOEmargementFactory.getSharedInstance().emargerEcritureDetailD1C1(ecdEcheanceCredit.editingContext(), utilisateur, typeEmargement, ecdEcheanceCredit.toExercice(), ecdEcheanceCredit, ecdEcheancier, Integer.valueOf(0), ecdEcheanceCredit.toEcriture().toComptabilite(), montant);
	}

	public EOEcritureDetail creerEcritureDetailDebitPourEcheancePrelevee(EOJefyAdminExercice exercice, EOGestion gestion, Integer ecdIndex, EOSepaSddEcheance echeance) throws Exception {
		try {
			EOEditingContext editingContext = echeance.editingContext();
			EOSepaSddEcheancier echeancier = echeance.toSepaSddEcheancier();
			BigDecimal montant = echeance.montant();
			String ecdLibelle = SepaSddEcheanceHelper.getSharedInstance().getLibelleEcheanceComplet(echeance);

			EOModeRecouvrement modeRecouvrement = (EOModeRecouvrement) SepaSddEcheancierHelper.getSharedInstance().getModeRecouvrement(echeancier);
			if (modeRecouvrement == null) {
				throw new Exception("Impossible de récupérer le mode de paiement associé à l'origine de l'échéancier (" + SepaSddEcheancierHelper.getSharedInstance().getLibelleEcheancier(echeancier) + ")");
			}

			// On prend le compte definit en tant que compte paiement pour le mode de recouvrement sur l'exercice de destination. 
			EOPlanComptableExer pcoe = modeRecouvrement.toPlanComptableExerPaiement();
			EOPlanComptableExer pco = EOPlanComptableExer.fetchWithPcoNumAndExercice(editingContext, pcoe.pcoNum(), exercice.exeExercice());
			if (pco == null) {
				throw new Exception("Le compte de 'paiement' associé au mode de recouvrement " + modeRecouvrement.modCode() + " n'est pas défini.");
			}
			if (!pco.isValide()) {
				throw new Exception("Le compte de 'paiement' associé au mode de recouvrement " + modeRecouvrement.modCode() + " n'est pas VALIDE.");
			}

			//creer ecriture en debit 
			EOEcritureDetail ecdCreditEcheance = EOEcritureDetailFactory.getSharedInstance().creerEcritureDetailDebit(editingContext, null, exercice, pco, gestion, null, ecdIndex, montant, ecdLibelle);

			//Memoriser le lien entre l'echeance et l'ecriture
			EOSepaSddEcheanceEcd sepaSddEcheanceEcd = EOSepaSddEcheanceEcd.createEOSepaSddEcheanceEcd(editingContext, ecdCreditEcheance, echeance);
			return ecdCreditEcheance;
		} catch (Exception e) {
			throw new Exception("Erreur lors de la création du débit de l'écriture de prélèvement pour l'échéance \n" + SepaSddEcheanceHelper.getSharedInstance().getLibelleEcheanceComplet(echeance) + " :\n" + e.getMessage(), e);
		}

	}

	public EOEcritureDetail creerEcritureDetailCreditPourEcheanceRejeteeOuConfirmee(EOJefyAdminExercice exercice, Integer ecdIndex, EOSepaSddEcheance echeance, EOGestion gestion, EOPlanComptableExer pco) throws Exception {
		try {
			EOEditingContext editingContext = echeance.editingContext();
			//EOSepaSddEcheancier echeancier = echeance.toSepaSddEcheancier();

			String ecdLibelle = SepaSddEcheanceHelper.getSharedInstance().getLibelleEcheanceComplet(echeance);

			BigDecimal montant = echeance.montant();
			EOEcritureDetail ecdCreditEcheance = EOEcritureDetailFactory.getSharedInstance().creerEcritureDetailCredit(editingContext, null, exercice, pco, gestion, null, ecdIndex, montant, ecdLibelle);

			//Memoriser le lien entre l'echeance et l'ecriture
			EOSepaSddEcheanceEcd sepaSddEcheanceEcd = EOSepaSddEcheanceEcd.createEOSepaSddEcheanceEcd(editingContext, ecdCreditEcheance, echeance);
			return ecdCreditEcheance;
		} catch (Exception e) {
			throw new Exception("Erreur lors de la création du débit de l'écriture pour l'échéance \n" + SepaSddEcheanceHelper.getSharedInstance().getLibelleEcheanceComplet(echeance) + " :\n" + e.getMessage(), e);
		}

	}

	public EOEcritureDetail creerEcritureDetailDebitPourEcheanceRejeteeOuConfirmee(EOJefyAdminExercice exercice, Integer ecdIndex, EOSepaSddEcheance echeance, EOGestion gestion, EOPlanComptableExer pco) throws Exception {
		try {
			EOEditingContext editingContext = echeance.editingContext();
			//EOSepaSddEcheancier echeancier = echeance.toSepaSddEcheancier();

			String ecdLibelle = SepaSddEcheanceHelper.getSharedInstance().getLibelleEcheanceComplet(echeance);
			BigDecimal montant = echeance.montant();

			EOEcritureDetail ecdEcheance = EOEcritureDetailFactory.getSharedInstance().creerEcritureDetailDebit(editingContext, null, exercice, pco, gestion, null, ecdIndex, montant, ecdLibelle);

			//Memoriser le lien entre l'echeance et l'ecriture
			EOSepaSddEcheanceEcd sepaSddEcheanceEcd = EOSepaSddEcheanceEcd.createEOSepaSddEcheanceEcd(editingContext, ecdEcheance, echeance);
			return ecdEcheance;
		} catch (Exception e) {
			throw new Exception("Erreur lors de la création du débit de l'écriture pour l'échéance \n" + SepaSddEcheanceHelper.getSharedInstance().getLibelleEcheanceComplet(echeance) + " :\n" + e.getMessage(), e);
		}

	}

	public String creerLibelleEcriturePrelevement(EOSepaSddEcheance echeance) {
		return SepaSddEcheanceHelper.getSharedInstance().getLibelleEcheanceComplet(echeance);
	}

}
