package org.cocktail.fwkcktlcompta.server.factories;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.server.metier.EOAccordsContrat;
import org.cocktail.fwkcktlcompta.server.metier.EOEcriture;
import org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail;
import org.cocktail.fwkcktlcompta.server.metier.EOGestion;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;

import com.webobjects.eocontrol.EOEditingContext;

public class EOEcritureDetailFactory {
	public static final String ECD_SENS_DEBIT = "D";
	public static final String ECD_SENS_CREDIT = "C";

	private static EOEcritureDetailFactory sharedInstance = new EOEcritureDetailFactory();

	public static EOEcritureDetailFactory getSharedInstance() {
		return sharedInstance;
	}

	public EOEcritureDetail creerEcritureDetail(EOEditingContext editingContext, EOEcriture ecriture, EOJefyAdminExercice exercice, EOPlanComptableExer planComptableExer, EOGestion gestion, EOAccordsContrat accordsContrat, Integer ecdIndex, BigDecimal ecdMontant, String ecdSens, String ecdLibelle) {
		EOEcritureDetail ecritureDetail = EOEcritureDetail.createEOEcritureDetail(editingContext, ecdIndex, ecdMontant, (ecdMontant != null ? ecdMontant.abs() : null), ecdSens, ecriture, exercice, gestion, planComptableExer);
		ecritureDetail.setToAccordsContratRelationship(accordsContrat);
		if (ecdLibelle == null) {
			ecdLibelle = ecriture.ecrLibelle();
		}
		ecritureDetail.setEcdLibelle(ecdLibelle);
		if (ECD_SENS_CREDIT.equals(ecdSens)) {
			ecritureDetail.setEcdCredit(ecdMontant);
			ecritureDetail.setEcdDebit(BigDecimal.ZERO);
		}
		else {
			ecritureDetail.setEcdDebit(ecdMontant);
			ecritureDetail.setEcdCredit(BigDecimal.ZERO);
		}
		return ecritureDetail;
	}

	public EOEcritureDetail creerEcritureDetailDebit(EOEditingContext editingContext, EOEcriture ecriture, EOJefyAdminExercice exercice, EOPlanComptableExer planComptableExer, EOGestion gestion, EOAccordsContrat accordsContrat, Integer ecdIndex, BigDecimal ecdMontant, String ecdLibelle) {
		return creerEcritureDetail(editingContext, ecriture, exercice, planComptableExer, gestion, accordsContrat, ecdIndex, ecdMontant, ECD_SENS_DEBIT, ecdLibelle);
	}

	public EOEcritureDetail creerEcritureDetailCredit(EOEditingContext editingContext, EOEcriture ecriture, EOJefyAdminExercice exercice, EOPlanComptableExer planComptableExer, EOGestion gestion, EOAccordsContrat accordsContrat, Integer ecdIndex, BigDecimal ecdMontant, String ecdLibelle) {
		return creerEcritureDetail(editingContext, ecriture, exercice, planComptableExer, gestion, accordsContrat, ecdIndex, ecdMontant, ECD_SENS_CREDIT, ecdLibelle);
	}

}
