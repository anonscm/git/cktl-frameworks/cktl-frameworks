package org.cocktail.fwkcktlcompta.server.factories;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.helpers.EcritureDetailHelper;
import org.cocktail.fwkcktlcompta.common.util.ZDateUtil;
import org.cocktail.fwkcktlcompta.server.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail;
import org.cocktail.fwkcktlcompta.server.metier.EOEmargement;
import org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

public class EOEmargementFactory {
	private static final String SP_NUMEROTER_EMARGEMENT = "FwkCktlCompta_numerotationObject.numeroter_emargement";

	private static EOEmargementFactory sharedInstance = new EOEmargementFactory();

	public static EOEmargementFactory getSharedInstance() {
		return sharedInstance;
	}
	public EOEmargementDetail creerEmargementDetail(EOEditingContext editingContext, BigDecimal emdMontant, EOEcritureDetail source, EOEcritureDetail destination, EOEmargement emargement, EOJefyAdminExercice exercice) throws Exception {

		if (source.ecdResteEmarger().compareTo(emdMontant.abs()) < 0) {
			throw new Exception("Le reste à émarger de l'écriture " + EcritureDetailHelper.getSharedInstance().getLibelleComplet(source) + " (" + source.ecdResteEmarger() + ") est inférieur au montant à émarger (" + emdMontant.abs() + "). Impossible de réaliser l'émargement.");
		}
		if (destination.ecdResteEmarger().compareTo(emdMontant.abs()) < 0) {
			throw new Exception("Le reste à émarger de l'écriture " + EcritureDetailHelper.getSharedInstance().getLibelleComplet(destination) + " (" + destination.ecdResteEmarger() + ") est inférieur au montant à émarger (" + emdMontant.abs() + "). Impossible de réaliser l'émargement.");
		}

		if (!source.toExercice().exeExercice().equals(destination.toExercice().exeExercice())) {
			throw new Exception("Les deux ecritures ne sont pas sur le même exercice ( " + EcritureDetailHelper.getSharedInstance().getNumeroEcritureAvecExerciceEtIndex(destination) + " / " +
					EcritureDetailHelper.getSharedInstance().getNumeroEcritureAvecExerciceEtIndex(source) + "). Impossible de réaliser l'émargement. Si vous êtes dans un processus de recouvrement, vous devez faire la BE du compte " + source.toPlanComptableExer().pcoNum() + " avant le recouvrement.");
		}

		source.setEcdResteEmarger(source.ecdResteEmarger().add(emdMontant.abs().negate()));
		destination.setEcdResteEmarger(destination.ecdResteEmarger().add(emdMontant.abs().negate()));

		EOEmargementDetail newEOEmargementDetail = EOEmargementDetail.creerInstance(editingContext);
		newEOEmargementDetail.setEmdMontant(emdMontant.abs());
		newEOEmargementDetail.setToEcdSourceRelationship(source);
		newEOEmargementDetail.setToEcdDestinationRelationship(destination);
		newEOEmargementDetail.setToEmargementRelationship(emargement);
		newEOEmargementDetail.setToExerciceRelationship(exercice);
		return newEOEmargementDetail;
	}

	protected EOEmargement creerEmargement(EOEditingContext editingContext, EOJefyAdminUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOJefyAdminExercice exercice, Integer numero, EOComptabilite comptabilite, BigDecimal montant) {
		EOEmargement newEOEmargement = EOEmargement.creerInstance(editingContext);
		newEOEmargement.setEmaDate(ZDateUtil.now());
		if (numero == null) {
			numero = Integer.valueOf(0);
		}

		newEOEmargement.setEmaNumero(numero);
		newEOEmargement.setEmaMontant(montant);
		newEOEmargement.setEmaEtat(EOEmargement.EMA_ETAT_VALIDE);

		newEOEmargement.setToUtilisateurRelationship(utilisateur);
		newEOEmargement.setToTypeEmargementRelationship(typeEmargement);
		newEOEmargement.setToExerciceRelationship(exercice);
		newEOEmargement.setToComptabiliteRelationship(comptabilite);
		return newEOEmargement;
	}

	public EOEmargement emargerEcritureDetailD1C1(EOEditingContext ed, EOJefyAdminUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOJefyAdminExercice exercice, EOEcritureDetail detailCrediteur, EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant)
			throws Exception {
		EOEmargement newEOEmargement = this.creerEmargement(ed, utilisateur, typeEmargement, exercice, numero, comptabilite, montant);
		creerEmargementDetail(ed, montant, detailCrediteur, detailDebiteur, newEOEmargement, exercice);
		return newEOEmargement;
	}

	public void numeroterEmargementViaProcedureStockee(EOEditingContext ed, EOEmargement emargement) {
		NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, emargement);
		EOUtilities.executeStoredProcedureNamed(ed, SP_NUMEROTER_EMARGEMENT, myDicoId);
	}

	public void numeroterEmargementsViaProcedureStockee(EOEditingContext editingContext, List<EOEmargement> emargementsGeneres) {
		for (EOEmargement eoEmargement : emargementsGeneres) {
			numeroterEmargementViaProcedureStockee(editingContext, eoEmargement);
		}

	}
}
