package org.cocktail.fwkcktlcompta.server.factories;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;

import er.extensions.foundation.ERXStringUtilities;

public class PlancoExerBeCache {
	private static Map<EOGlobalID, EOGlobalID> _compteBePourCompte = new HashMap<EOGlobalID, EOGlobalID>();
	private static PlancoExerBeCache sharedInstance = new PlancoExerBeCache();

	public static PlancoExerBeCache getSharedInstance() {
		return sharedInstance;
	}

	/**
	 * @param pco
	 * @param exercice
	 * @return Si exercice est celui de pco, renvoie pco. Sinon si le compte de BE est défini pour pco, renvoie le compte de BE (s'il existe).
	 */
	public EOPlanComptableExer getPlancomptableExerOuBe(EOEditingContext edc, EOPlanComptableExer pco, EOJefyAdminExercice exercice) {
		if (!pco.toExercice().exeExercice().equals(exercice.exeExercice())) {
			if (!ERXStringUtilities.stringIsNullOrEmpty(pco.pcoCompteBe())) {
				if (_compteBePourCompte.get(pco.globalID()) == null) {
					EOPlanComptableExer pcoe = EOPlanComptableExer.fetchWithPcoNumAndExercice(pco.editingContext(), pco.pcoCompteBe(), exercice.exeExercice());
					if (pcoe != null) {
						_compteBePourCompte.put(pco.globalID(), pcoe.globalID());
					} else {
					    throw new IllegalArgumentException("Le compte de balance d'entrée défini sur " + pco.toExercice().exeExercice() + " pour le compte " + pco.pcoNum() + " n'existe pas pour l'exercice " + exercice.exeExercice());
					}
				}
				EOPlanComptableExer pcoe = (EOPlanComptableExer) edc.faultForGlobalID(_compteBePourCompte.get(pco.globalID()), edc);
				if (pcoe != null) {
					return pcoe;
				}
			} else {
                throw new IllegalArgumentException("Le compte de balance d'entrée n'a pas été défini sur " + pco.toExercice().exeExercice() + " pour le compte " + pco.pcoNum() + " pour l'exercice " + exercice.exeExercice()); 
			}
		}
		return pco.localInstanceIn(edc);
	}

}
