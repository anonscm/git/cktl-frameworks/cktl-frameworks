package org.cocktail.fwkcktlcompta.server.factories;

import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IEcriture;
import org.cocktail.fwkcktlcompta.common.helpers.EcritureHelper;
import org.cocktail.fwkcktlcompta.server.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.server.metier.EOEcriture;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Attention, les méthodes ne gerent pas la création des ecritures SACD en auto.
 * 
 * @author rprin
 */
public class EOEcritureFactory {
	private static final String SP_NUMEROTER_ECRITURE = "FwkCktlCompta_numerotationObject.numeroter_ecriture";
	private static EOEcritureFactory sharedInstance = new EOEcritureFactory();

	public static EOEcritureFactory getSharedInstance() {
		return sharedInstance;
	}

	public EOEcriture creerEcriture(EOEditingContext editingContext, NSTimestamp ecrDate
			, String ecrLibelle
			, EOComptabilite toComptabilite, EOJefyAdminExercice toExercice, EOTypeJournal toTypeJournal,
			EOTypeOperation toTypeOperation, EOJefyAdminUtilisateur toUtilisateur) throws Exception {
		Date laDate = EcritureHelper.getSharedInstance().getDateJourneeComptableCorrigeeSelonExercice(ecrDate, toExercice);
		NSTimestamp dateCreationEcriture = new NSTimestamp(laDate);
		return EOEcriture.createEOEcriture(editingContext, ecrDate, dateCreationEcriture, IEcriture.ECR_ETAT_VALIDE, ecrLibelle, Integer.valueOf(0), toComptabilite, toExercice, toTypeJournal, toTypeOperation, toUtilisateur);
	}

	public void numeroterEcritureViaProcedureStockee(EOEditingContext ed, EOEcriture monEcriture) {
		NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, monEcriture);
		EOUtilities.executeStoredProcedureNamed(ed, SP_NUMEROTER_ECRITURE, myDicoId);
	}

	public void numeroterEcrituresViaProcedureStockee(EOEditingContext ed, List<EOEcriture> ecritures) {
		for (EOEcriture eoEcriture : ecritures) {
			numeroterEcritureViaProcedureStockee(ed, eoEcriture);
		}
	}


}
