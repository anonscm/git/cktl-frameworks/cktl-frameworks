package org.cocktail.fwkcktlcompta.server.remotes;


import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.CktlDefaultReportingTaskListener;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;
import org.cocktail.reporting.server.jxls.IJxlsReportListener;
import org.cocktail.reporting.server.jxls.JxlsReporter;

import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.jdbcadaptor.JDBCContext;

/**
 * Gestion des appels distants (clients) concernant les impressions.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class CktlComptaRemoteDelegatePrint extends RemoteDelegate {
	public final static Logger logger = Logger.getLogger(CktlComptaRemoteDelegatePrint.class);

	
	protected JrxmlReporter reporter;
	protected JxlsReporter jxlsReporter;
	protected MyListener myListener = new MyListener();

	protected class MyListener extends CktlDefaultReportingTaskListener implements IJrxmlReportListener, IJxlsReportListener {

	}

	public CktlComptaRemoteDelegatePrint(CktlWebSession session) {
		super(session);
	}

	public NSData clientSideRequestPrintAndWait(String idreport, String sqlQuery, NSDictionary parametres) throws Exception {
		resetReporters();
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			if (parametres != null) {
				params = new HashMap<String, Object>(parametres.hashtable());
				System.out.println("parametres : " + params);
			}
			String jasperId = idreport;
			final String realJasperFilName = getRealJasperFileName(jasperId);
			reporter = new JrxmlReporter();
			NSData res = reporter.printNow(getJDBCConnection(), sqlQuery, realJasperFilName, params, JrxmlReporter.EXPORT_FORMAT_PDF, false, true, myListener);

			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void clientSideRequestPrintByThread(String idreport, String sqlQuery, NSDictionary parametres, String customJasperId, Boolean printIfEmpty) throws Exception {
		resetReporters();
        try {
			Map<String, Object> params = new HashMap<String, Object>();
            
            if (parametres != null) {
				params = new HashMap<String, Object>(parametres.hashtable());
                System.out.println("parametres : " + params);
            }


            String jasperId = idreport;
            if (customJasperId != null) {
                jasperId = customJasperId;
				System.out.println("Utilisation d un report personnalise :" + customJasperId);
            }

            final String realJasperFilName = getRealJasperFileName(jasperId);

			reporter = new JrxmlReporter();
			reporter.printWithThread(idreport, getJDBCConnection(), sqlQuery, realJasperFilName, params, JrxmlReporter.EXPORT_FORMAT_PDF, printIfEmpty, true, myListener);

        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

	public void clientSideRequestPrintByThreadXls(String idreport, String sqlQuery, NSDictionary parametres, String customJasperId) throws Exception {
		resetReporters();
        try {
			Map<String, Object> params = new HashMap<String, Object>();
            if (parametres != null) {
				params = new HashMap<String, Object>(parametres.hashtable());
                System.out.println("parametres : " + params);
            }

            String jasperId = idreport;
            if (customJasperId != null) {
                jasperId = customJasperId;
                System.out.println("Utilisation dun report personnalise :" + customJasperId);
            }

            final String realJasperFilName = getRealJasperFileName(jasperId);
			reporter = new JrxmlReporter();
			reporter.printWithThread(idreport, getJDBCConnection(), sqlQuery, realJasperFilName, params, JrxmlReporter.EXPORT_FORMAT_XLS, false, true, myListener);
			logger.info(" Tache d'impression lancee pour" + realJasperFilName + " ...");

        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public final void clientSideRequestPrintKillCurrentTask() {
		if (getCurrentReporter() != null) {
			getCurrentReporter().stop();
		}
    }

	public final NSData clientSideRequestPrintDifferedGetPdf() throws Throwable {
		if (getCurrentReporter() != null) {
			return getCurrentReporter().printDifferedGetDataResult();
		}
		return null;
    }

    public final NSDictionary clientSideRequestGetPrintProgression() throws Exception {
		if (getCurrentReporter() == null || myListener == null) {
            throw new Exception("Pas de listener défini");
        }

		NSMutableDictionary<String, Object> dictionary = new NSMutableDictionary<String, Object>();
		dictionary.takeValueForKey(new Integer(myListener.getPageCount()), "PAGE_COUNT");
		dictionary.takeValueForKey(new Integer(myListener.getPageTotalCount()), "PAGE_TOTAL_COUNT");
		dictionary.takeValueForKey(new Integer(myListener.getPageNum()), "PAGE_NUM");
		dictionary.takeValueForKey(new Boolean(myListener.isDataSourceCreated()), "DATASOURCE_CREATED");
		dictionary.takeValueForKey(new Boolean(myListener.isReportExported()), "REPORT_EXPORTED");
		dictionary.takeValueForKey(new Boolean(myListener.isReportBuild()), "REPORT_BUILD");
        return dictionary.immutableClone();
    }
    
    
	public final void clientSideRequestPrintByThreadJXls(String idreport, String sqlQuery, NSDictionary parametres, String customJasperId) throws Exception {
		resetReporters();
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			if (parametres != null) {
				params = new HashMap<String, Object>(parametres.hashtable());
				logger.debug("parametres : " + params);
			}


			String jasperId = idreport;
			if (customJasperId != null) {
				jasperId = customJasperId;
				logger.debug("Utilisation dun report personnalise :" + customJasperId);
			}

			final String realJasperFilName = getRealJasperFileName(jasperId);
			jxlsReporter = new JxlsReporter();
			jxlsReporter.printWithThread(idreport, getJDBCConnection(), sqlQuery, realJasperFilName, params, JrxmlReporter.EXPORT_FORMAT_XLS, true, null);
			logger.debug("Tache d'export lancee pour" + realJasperFilName + " ...");

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	protected void resetReporters() {
		jxlsReporter = null;
		reporter = null;
		myListener = new MyListener();
	}

	protected CktlAbstractReporter getCurrentReporter() {
		if (reporter != null) {
			return reporter;
		}
		if (jxlsReporter != null) {
			return jxlsReporter;
		}
		return null;

	}

	protected Connection getJDBCConnection() {
		return ((JDBCContext) CktlDataBus.databaseContext().availableChannel().adaptorChannel().adaptorContext()).connection();
	}


	public String getRealJasperFileName(String rname) {
		return FwkCktlComptaMoteurCtrl.getSharedInstance().getReportsBaseLocation() + rname;
	}


}
