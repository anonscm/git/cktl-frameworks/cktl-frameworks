package org.cocktail.fwkcktlcompta.server.remotes;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ctrl.ISepaSddEcheancierCtrl;
import org.cocktail.fwkcktlcompta.server.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class RemoteDelegateCompta extends RemoteDelegate {

	public RemoteDelegateCompta(CktlWebSession session) {
		super(session);
	}

	public Object clientSideRequestGetConfig(String param) {
		return getMyApplication().config().get(param);
	}

	public static Object getConfig(String param) {
		return CktlWebApplication.application().config().get(param);
	}


	public final boolean clientSideRequestHasEcheanciers(String origineType, Integer origineEntityId) throws Exception {
		if (origineEntityId == null) {
			return false;
		}

		NSArray echeanciers = ISepaSddEcheancierCtrl.getSharedInstance().echeanciersExistentPourOrigine(defaultEditingContext(), origineType, origineEntityId);
        return echeanciers.count() > 0;
	}

	public final Integer clientSideRequestNextNumeroRecouvrement(final EOEnterpriseObject comptabilite, final EOEnterpriseObject exercice) throws Exception {
		try {
			EOComptabilite localCompta = (EOComptabilite) EOUtilities.localInstanceOfObject(defaultEditingContext(), comptabilite);
			EOJefyAdminExercice localExercice = (EOJefyAdminExercice) EOUtilities.localInstanceOfObject(defaultEditingContext(), exercice);
			NSMutableDictionary dico = new NSMutableDictionary();

			dico.takeValueForKey(EOUtilities.primaryKeyForObject(defaultEditingContext(), localCompta).valueForKey(EOComptabilite.COM_ORDRE_KEY), "001comordre");
			dico.takeValueForKey(EOUtilities.primaryKeyForObject(defaultEditingContext(), localExercice).valueForKey(EOJefyAdminExercice.EXE_ORDRE_KEY), "002exeordre");
			NSDictionary res = EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "FwkCktlCompta_numerotationObject.next_numero_recouvrement", dico);
			return (Integer) res.valueForKey("000result");

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * Permet de lancer des invalidates cotés serveur à partir du client.
	 * 
	 * @param globalIds
	 */
	public void clientSideRequestInvalidateObjectsOnServer(NSArray<EOGlobalID> globalIds) {
		defaultEditingContext().invalidateObjectsWithGlobalIDs(globalIds);
	}

}
