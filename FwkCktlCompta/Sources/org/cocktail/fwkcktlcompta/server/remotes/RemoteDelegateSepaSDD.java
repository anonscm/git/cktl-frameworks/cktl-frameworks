/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcompta.server.remotes;

import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

public class RemoteDelegateSepaSDD extends RemoteDelegate {

	public RemoteDelegateSepaSDD(CktlWebSession session) {
		super(session);
	}

	//	public final NSDictionary clientSideRequestPriseEnChargeEcheancier(EOEnterpriseObject sepaSddEcheancier, EOEnterpriseObject comptabilite, EOEnterpriseObject exercice, EOEnterpriseObject utilisateur, NSTimestamp dateCreation) throws Exception {
	//		EOEditingContext editingContext = ERXEC.newEditingContext();
	//		try {
	//		EOSepaSddEcheancier echeancier = (EOSepaSddEcheancier) ERXEOControlUtilities.localInstanceOfObject(editingContext, sepaSddEcheancier);
	//
	//		EOJefyAdminExercice exerciceLoc = (EOJefyAdminExercice) EOUtilities.localInstanceOfObject(editingContext, exercice);
	//		EOComptabilite comptabiliteLoc = (EOComptabilite) EOUtilities.localInstanceOfObject(editingContext, comptabilite);
	//		EOJefyAdminUtilisateur utilisateurLoc = (EOJefyAdminUtilisateur) EOUtilities.localInstanceOfObject(editingContext, utilisateur);
	//
	//		EOTypeEmargement typeEmargement = EOTypeEmargement.fetchByKeyValue(editingContext, EOTypeEmargement.TEM_LIBELLE_KEY, EOTypeEmargement.typeLettrageAutomatique);
	//		EOTypeJournal typeJournal = EOTypeJournal.fetchByKeyValue(editingContext, EOTypeJournal.TJO_LIBELLE_KEY, EOTypeJournal.typeJournalRecouvrement);
	//		EOTypeOperation typeOperation = EOTypeOperation.fetchByKeyValue(editingContext, EOTypeOperation.TOP_LIBELLE_KEY, EOTypeOperation.TOP_LIBELLE_RECOUVREMENT);
	//
	//
	//			SepaSddEcheancierVisaService service = new SepaSddEcheancierVisaService(editingContext, comptabiliteLoc, exerciceLoc, utilisateurLoc, dateCreation, typeEmargement, typeJournal, typeOperation);
	//			NSDictionary res = service.creerEcriturePriseEnchargeEcheancierEtEmargement(echeancier);
	//
	//			return res;
	//
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//			throw e;
	//		}
	//
	//
	//	}

}
