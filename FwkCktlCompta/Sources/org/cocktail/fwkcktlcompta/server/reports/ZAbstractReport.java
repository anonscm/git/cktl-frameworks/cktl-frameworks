package org.cocktail.fwkcktlcompta.server.reports;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Connection;
import java.util.Map;

import net.sf.jasperreports.engine.type.WhenNoDataTypeEnum;

public abstract class ZAbstractReport {

	public static final int MODE_SQL = 1;
	public static final int MODE_XML = 2;

	public static String NOPAGES_MSG = "Aucune donnée à imprimer. Vérifiez éventuellement les critères.";

	protected ReportFactory myReportFactory;

	private int _mode;

	private Connection _connection;
	private String _query;
	private Map _parameters;
	private String _fileName;
	private StringWriter _xmlDataSourceWriter;
	private String _xmlRecordPath;

	/**
	 * @param jasperFileName
	 * @param connection
	 * @param query
	 * @param parameters
	 * @throws Exception
	 */
	public ZAbstractReport(final String jasperFileName, final Connection connection, final String query, final Map parameters) throws Exception {
		super();
		_mode = MODE_SQL;
		_connection = connection;
		_query = query;
		_parameters = parameters;
		_fileName = jasperFileName;

		System.out.println("Impression avec " + jasperFileName);
		myReportFactory = new ReportFactory(jasperFileName);

		if (Thread.currentThread().isInterrupted()) {
			return;
		}

		myReportFactory.initEmptyDataSource();
	}

	public void prepareDataSource() throws Exception {
		if (_mode == MODE_XML) {
			if (_xmlDataSourceWriter != null) {
				//Créer un flux à partir du xml
				byte[] xmlbytes = _xmlDataSourceWriter.toString().getBytes();
				InputStream xmlStream = new ByteArrayInputStream(xmlbytes);
				myReportFactory.initDataSource(xmlStream, _xmlRecordPath);
			} else {
				myReportFactory.initEmptyDataSource();
			}

		}
	}

	public void printReport() throws Exception {
		if (!Thread.currentThread().isInterrupted()) {
			switch (_mode) {
			case MODE_SQL:
				System.out.println("Impression en cours - source de donnees SQL : " + _fileName + "...");
				System.out.println("Parametres : " + _parameters);
				myReportFactory.printReport(_connection, _query, _parameters);
				break;

			case MODE_XML:
				System.out.println("Impression en cours - source de donnees XML : " + _fileName + "...");
				System.out.println("Parametres : " + _parameters);
				myReportFactory.printReport(_parameters);
				break;

			default:
				break;
			}
		}
	}

	/**
	 * @return Le PDF généré par Jasper sous forme de OutputStream.
	 * @throws Exception
	 */
	public ByteArrayOutputStream getPdfOutputStream() throws Exception {
		return myReportFactory.exportToPdf();
	}

	public ByteArrayOutputStream getXlsOutputStream() throws Exception {
		return myReportFactory.exportToXls();
	}

	/**
	 * Parametre le report pour générer ou non des pages suivants la cas : JasperReport.WHEN_NO_DATA_TYPE_NO_PAGES,
	 * JasperReport.WHEN_NO_DATA_TYPE_ALL_SECTIONS_NO_DETAIL, JasperReport.WHEN_NO_DATA_TYPE_BLANK_PAGE
	 *
	 * @param whenNoData
	 */
	public void setWhenNoDataType(final WhenNoDataTypeEnum whenNoData) {
		myReportFactory.setWhenNoDataType(whenNoData);
	}
}
