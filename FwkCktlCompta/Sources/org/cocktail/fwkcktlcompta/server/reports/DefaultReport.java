package org.cocktail.fwkcktlcompta.server.reports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.util.Map;

import com.webobjects.eocontrol.EOEditingContext;

public class DefaultReport extends ZAbstractReport {

	public DefaultReport(final String jasperFileName, final Connection connection, final String query, final Map parameters)
			throws Exception {
		super(jasperFileName, connection, query, parameters);
	}

	public static final DefaultReport createReport(final Connection connection, final String idreport, final boolean wantTrace,
			final EOEditingContext ec, final String modelName, String sqlQuery, final String jasperFileName, final Map parameters)
					throws Exception {
		return DefaultReport.createDefaultReport(wantTrace, connection, sqlQuery, jasperFileName, parameters);
	}

	public static final DefaultReport createDefaultReport(final boolean wantTrace, Connection connection, String sqlQuery,
			final String jasperFileName, final Map parameters) throws Exception {
		if (wantTrace) {
			System.out.println();
			System.out.println("Generation d'un report avec connection JDBC");
			System.out.println("SQL = " + sqlQuery);
		}
		return new DefaultReport(jasperFileName, connection, sqlQuery, parameters);
	}

	public static final void saveInTempFile(StringWriter sw, final String fileName) throws Exception {
		String temporaryDir = System.getProperty("java.io.tmpdir");
		if (!temporaryDir.endsWith(File.separator)) {
			temporaryDir = temporaryDir + File.separator;
		}
		File f = new File(temporaryDir + fileName);
		if (f.exists()) {
			f.delete();
		}
		if (f.createNewFile()) {
			new FileWriter(f);
			BufferedWriter out = new BufferedWriter(new FileWriter(f));
			out.write(sw.toString());
			out.close();
		}
		System.out.println("Le fichier " + f.getAbsolutePath() + " a ete cree.");
	}
}
