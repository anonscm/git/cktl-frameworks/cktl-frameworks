package org.cocktail.fwkcktlcompta.server.reports;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.type.WhenNoDataTypeEnum;
import net.sf.jasperreports.engine.util.JRLoader;

public class ReportFactory {

	public static final String SQLQUERY_KEY = "REQUETE_SQL";

	private JasperReport jasperReport;
	private JRDataSource _jrxmlds;
	private JasperPrint printResult;
	private Integer stateLock = Integer.valueOf(0);
	private int pageCount = -1;

	/**
	 * @param jasperFileName le nom du fichier .jasper
	 */
	public ReportFactory(String jasperFileName) {
		super();
		try {
			pageCount = -1;

			final File f = new File(jasperFileName);
			if (!f.exists()) {
				throw new Exception("Le fichier " + jasperFileName + " n'existe pas.");
			}

			System.out.println("Chargement de " + jasperFileName + " ...");
			//jasperReport = (JasperReport) JRLoader.loadObject(jasperFileName);  Mouture 2.0.5 de jasperreport
			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(jasperFileName);
			//jasperReport.setWhenNoDataType(JasperReport.WHEN_NO_DATA_TYPE_NO_PAGES);	Mouture 2.0.5 de jasperreport
			//			jasperReport.setWhenNoDataType(WhenNoDataTypeEnum.NO_PAGES);
			//			jasperReport.setWhenNoDataType(WhenNoDataTypeEnum.BLANK_PAGE);


			System.out.println(jasperFileName + " charge.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Parametre le report pour générer ou non des pages suivants la cas : JasperReport.WHEN_NO_DATA_TYPE_NO_PAGES,
	 * JasperReport.WHEN_NO_DATA_TYPE_ALL_SECTIONS_NO_DETAIL, JasperReport.WHEN_NO_DATA_TYPE_BLANK_PAGE
	 *
	 * @param whenNoData
	 */
	public void setWhenNoDataType(final WhenNoDataTypeEnum whenNoData) {
		jasperReport.setWhenNoDataType(whenNoData);
	}

	/**
	 * Initialise la datasource à partir du flux xml
	 *
	 * @param xmlDataStream Flux des datas xml
	 * @param xmlRecordPath Le recordPath
	 * @throws JRException
	 */
	public void initDataSource(InputStream xmlDataStream, String xmlRecordPath) throws JRException {
		_jrxmlds = new JRXmlDataSource(xmlDataStream, xmlRecordPath);
		System.out.println("ReportFactory.initDataSource() " + "Datasource XML créée avec recordpath : " + xmlRecordPath);
	}

	/**
	 * Initialise une datasource vide.
	 *
	 * @throws JRException
	 */
	public void initEmptyDataSource() throws JRException {
		_jrxmlds = new JREmptyDataSource();
	}

	/**
	 * @param parameters Parametres à passer au report
	 * @throws JRException
	 */
	private void _printReport(final Map parameters) throws Exception {

		printResult = JasperFillManager.fillReport(jasperReport, parameters, _jrxmlds);
		pageCount = printResult.getPages().size();

		Thread.yield();
		System.out.println("ReportFactory.printReport() " + " report rempli - " + pageCount + " pages");

		if (pageCount == 0) {
			throw new Exception(ZAbstractReport.NOPAGES_MSG);
		}

	}

	/**
	 * Remplit le report à partir d'une requete sql spécifie en parametre. Fonctionne également si la requete est incluse dans le report (dans ce cas,
	 * spécifier null pour le parametre sqlQuery).
	 *
	 * @param connection Connection à la base de données.
	 * @param sqlQuery Requete SQL. Si la chaine n'est pas nulle, elle est ajoutée aux parametres passés au report (@see SQLQUERY_KEY)
	 * @param parameters Parametres à passer au report
	 * @throws JRException
	 */
	private void _printReport(final Connection connection, final String sqlQuery, final Map parameters) throws Exception {
		System.out.println("SQL = " + sqlQuery);

		//On ajoute la requete SQL aux parametres si celle-ci n'est pas nulle
		if (sqlQuery != null) {
			parameters.put(SQLQUERY_KEY, sqlQuery);
		}

		printResult = JasperFillManager.fillReport(jasperReport, parameters, connection);
		pageCount = printResult.getPages().size();

		Thread.yield();
		System.out.println("ReportFactory.printReport() " + " report rempli - " + pageCount + " pages");

		if (pageCount == 0) {
			throw new Exception(ZAbstractReport.NOPAGES_MSG);
		}

	}

	private Integer getStateLock() {
		return stateLock;
	}

	public void printReport(final Map parameters) throws Exception {
		_printReport(parameters);
	}

	public void printReport(final Connection connection, final String sqlQuery, final Map parameters) throws Exception {
		_printReport(connection, sqlQuery, parameters);
	}

	public ByteArrayOutputStream exportToPdf() throws JRException {
		final ByteArrayOutputStream tmpStream = new ByteArrayOutputStream();
		final JRExporter exporter = new JRPdfExporter();
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, tmpStream);
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, printResult);
		exporter.exportReport();

		return tmpStream;
	}

	public ByteArrayOutputStream exportToXls() throws JRException {
		final ByteArrayOutputStream tmpStream = new ByteArrayOutputStream();
		final JRExporter exporter = new JRXlsExporter();

		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, tmpStream);
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, printResult);
		exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);

		System.out.println("exportToXls");
		exporter.exportReport();
		System.out.println("exportToXls end");
		return tmpStream;
	}
}
