/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcompta.server.sp;

import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

public class SpGestionorigineTraiterOrgid extends StoredProcedure {
	public static String SP_NAME = "FwkCktlCompta_gestionorigine.traiter_orgid";
	public static String ORG_ID_KEY = EOOrgan.ORG_ID_KEY;
	public static String EXE_ORDRE_KEY = EOExercice.EXE_ORDRE_KEY;
	public static String RESULT_KEY = "result";

	public SpGestionorigineTraiterOrgid() {
		super(SP_NAME);
	}

	public void setOrgId(Integer val) {
		getParams().takeValueForKey(val, ORG_ID_KEY);
	}

	public void setExeOrdre(Integer val) {
		getParams().takeValueForKey(val, EXE_ORDRE_KEY);
	}

	public static void exec(EOEditingContext edc, EOOrgan organ, EOExercice exercice) throws Exception {
		SpGestionorigineTraiterOrgid sp = new SpGestionorigineTraiterOrgid();
		if (organ != null) {
			NSDictionary myDicoId = EOUtilities.primaryKeyForObject(edc, organ);
			sp.setOrgId(Integer.valueOf(((Number) myDicoId.valueForKey(EOOrgan.ORG_ID_KEY)).intValue()));
			myDicoId = EOUtilities.primaryKeyForObject(edc, exercice);
			sp.setExeOrdre(Integer.valueOf(((Number) myDicoId.valueForKey(EOExercice.EXE_ORDRE_KEY)).intValue()));
			NSDictionary res = sp.execute(edc);
		}
	}
}
