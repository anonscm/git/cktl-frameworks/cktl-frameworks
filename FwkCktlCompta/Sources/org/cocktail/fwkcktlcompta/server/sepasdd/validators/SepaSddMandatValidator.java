package org.cocktail.fwkcktlcompta.server.sepasdd.validators;

import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHelper;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.server.sepasdd.factories.SepaSddMandatFactory;

import com.webobjects.foundation.NSValidation;

public class SepaSddMandatValidator {

	public void validate(EOSepaSddMandat mandat) {
		//TODO verifier que les dates en string sont valides (cf. DateConversionUtil)

		//TODO verifier que les pers_id correspondent bien à des personnes

		//TODO verifier que le modificateur correspond à une personne qui a le droit de modifier le mandat

		//affecter numero et RUM
		if (mandat.isNewObject() || mandat.numero() == null || SepaSddMandatHelper.NUMERO_TEMP.equals(mandat.numero())) {
			try {
				mandat.setNumero(new SepaSddMandatFactory().creerNumeroUniquePourMandat(mandat));
			} catch (Exception e) {
				e.printStackTrace();
				throw new NSValidation.ValidationException(e.getMessage());
			}
		}

		if (mandat.isNewObject() || mandat.rum() == null || SepaSddMandatHelper.RUM_TEMP.equals(mandat.rum())) {
			try {
				mandat.setRum(new SepaSddMandatFactory().creerRUMPourMandat(mandat));
			} catch (Exception e) {
				e.printStackTrace();
				throw new NSValidation.ValidationException(e.getMessage());
			}
		}

		new SepaSddMandatFactory().creerOuNonHistoriqueDuMandat(mandat);

	}
}
