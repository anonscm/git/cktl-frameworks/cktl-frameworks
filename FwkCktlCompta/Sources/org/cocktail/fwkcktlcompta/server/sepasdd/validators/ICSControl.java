package org.cocktail.fwkcktlcompta.server.sepasdd.validators;

import org.cocktail.fwkcktlcompta.common.exception.SizeErrorException;

/**
 * Classe pour vérifier la validité d'un ICS (Identifiant Créancier SEPA)
 * 
 * @author rprin
 */
public class ICSControl {
	//Longueur pour les ICS en France
	public static final Integer MAX_LENGTH = 13;

	public void checkICS(String ics) throws Exception {
		if (ics == null) {
			throw new NullPointerException("L'ICS du créancier ne peut être nul.");
		}
		if (ics.length() != MAX_LENGTH) {
			throw new SizeErrorException("L'ICS doit comporter " + MAX_LENGTH + " caractères.");
		}

	}

}
