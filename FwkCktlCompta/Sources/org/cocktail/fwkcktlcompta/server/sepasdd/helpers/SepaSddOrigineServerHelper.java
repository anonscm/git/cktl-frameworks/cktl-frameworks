package org.cocktail.fwkcktlcompta.server.sepasdd.helpers;

import java.util.Map;

import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddOrigineHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.origines.CommonSepaSddOrigineEntityCtrl;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class SepaSddOrigineServerHelper extends SepaSddOrigineHelper {

	private static SepaSddOrigineServerHelper sharedInstance = new SepaSddOrigineServerHelper();

	public static SepaSddOrigineServerHelper getSharedInstance() {
		return sharedInstance;
	}

	public Map<String, ? extends CommonSepaSddOrigineEntityCtrl> getSepaSddOriginesMap(EOEditingContext editingContext) {
		if (sepaSddOriginesMap == null) {
			try {
				sepaSddOriginesMap = buildOriginesCtrl(EOSepaSddOrigineType.fetchAll(editingContext));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sepaSddOriginesMap;
	}

	public NSArray fetchOrigineTypesValides(EOEditingContext editingContext) {
		EOQualifier qualifier = new EOKeyValueQualifier(EOSepaSddOrigineType.VALIDE_KEY, EOQualifier.QualifierOperatorEqual, "O");
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(EOSepaSddOrigineType.TYPE_ORDRE_KEY, EOSortOrdering.CompareAscending);
		return EOSepaSddOrigineType.fetchAll(editingContext, qualifier, new NSArray(new Object[] {
				sort
		}));
	}

}
