package org.cocktail.fwkcktlcompta.server.sepasdd.factories;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHistoHelper;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddMandatHisto;
import org.joda.time.LocalDateTime;

/**
 * @author rprin
 */
public class SepaSddMandatHistoFactory {

	/**
	 * @param mandat le mandat
	 * @param mandatHisto le mandat histo précédent
	 * @return La liste des propriétés de {@link EOSepaSddMandatHisto} qui sont modifiées avec la version du mandat passée en parametre.
	 */
	public List<String> clesModifieesEntreMandatEtMandatHisto(EOSepaSddMandat mandat, EOSepaSddMandatHisto mandatHisto) {
		List<String> proprietesModifiees = new ArrayList<String>();
		if (!mandat.rum().equals(mandatHisto.rum())) {
			proprietesModifiees.add(EOSepaSddMandatHisto.RUM_KEY);
		}
		if (!mandat.toSepaSddParam().creancierIcs().equals(mandatHisto.creancierIcs())) {
			proprietesModifiees.add(EOSepaSddMandatHisto.CREANCIER_ICS_KEY);
		}
		if (!mandat.toCreancierPersonne().globalID().equals(mandatHisto.toCreancierPersonne().globalID())) {
			proprietesModifiees.add(EOSepaSddMandatHisto.TO_CREANCIER_PERSONNE_KEY);
		}
		if (!mandat.toCreancierPersonne().getNomAndPrenom().equals(mandatHisto.creancierNom())) {
			proprietesModifiees.add(EOSepaSddMandatHisto.CREANCIER_NOM_KEY);
		}
		if (!mandat.toDebiteurPersonne().globalID().equals(mandatHisto.toDebiteurPersonne().globalID())) {
			proprietesModifiees.add(EOSepaSddMandatHisto.TO_DEBITEUR_PERSONNE_KEY);
		}
		if (!mandat.toDebiteurPersonne().getNomAndPrenom().equals(mandatHisto.debiteurNom())) {
			proprietesModifiees.add(EOSepaSddMandatHisto.DEBITEUR_NOM_KEY);
		}
		if (!mandat.toDebiteurRib().toBanque().bic().equals(mandatHisto.debiteurBic())) {
			proprietesModifiees.add(EOSepaSddMandatHisto.DEBITEUR_BIC_KEY);
		}
		if (!mandat.toDebiteurRib().iban().equals(mandatHisto.debiteurIban())) {
			proprietesModifiees.add(EOSepaSddMandatHisto.DEBITEUR_IBAN_KEY);
		}
		if (!mandat.cTypePrelevement().equals(mandatHisto.cTypePrelevement())) {
			proprietesModifiees.add(EOSepaSddMandatHisto.C_TYPE_PRELEVEMENT_KEY);
		}
		if (!mandat.toTypeEtat().equals(mandatHisto.toTypeEtat())) {
			proprietesModifiees.add(EOSepaSddMandatHisto.TO_TYPE_ETAT_KEY);
		}
		return proprietesModifiees;
	}

	public EOSepaSddMandatHisto creerMandatHistoAPartirDuMandat(EOSepaSddMandat mandat) {
		EOSepaSddMandatHisto mandatHisto = EOSepaSddMandatHisto.creerInstance(mandat.editingContext());
		mandatHisto.setDCreation(new DateConversionUtil().formatDateWithTimeISO(new LocalDateTime()));
		mandatHisto.setToModificateurRelationship(mandat.toModificateur());
		mandatHisto.setRum(mandat.rum());
		mandatHisto.setCTypePrelevement(mandat.cTypePrelevement());
		mandatHisto.setCreancierIcs(mandat.toSepaSddParam().creancierIcs());
		mandatHisto.setToCreancierPersonneRelationship(mandat.toCreancierPersonne());
		mandatHisto.setCreancierNom(mandat.toCreancierPersonne().getNomAndPrenom());
		mandatHisto.setToDebiteurPersonneRelationship(mandat.toDebiteurPersonne());
		mandatHisto.setDebiteurNom(mandat.toDebiteurPersonne().getNomAndPrenom());
		mandatHisto.setDebiteurBic(mandat.toDebiteurRib().toBanque().bic());
		mandatHisto.setDebiteurIban(mandat.toDebiteurRib().iban());
		mandatHisto.setToTypeEtatRelationship(mandat.toTypeEtat());
		return mandatHisto;
	}

	public String construitMessageAPartirDesClesModifiees(List<String> clesModifiees) {
		String message = null;
		if (clesModifiees.size() > 0) {
			message = "";
		}
		for (String cle : clesModifiees) {

			if (message.length() < 0) {
				message = message.concat(", ");
			}
			message = message.concat(SepaSddMandatHistoHelper.DISPLAY_NAMES.get(cle));
		}
		if (message.length() > 0) {
			message = " Informations modifiées : ".concat(message);
		}
		return message;
	}
}
