package org.cocktail.fwkcktlcompta.server.sepasdd.factories;

import java.text.DecimalFormat;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.exception.SizeErrorException;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHelper;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddMandatHisto;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddMandatNumeroSeq;
import org.cocktail.fwkcktlcompta.server.sepasdd.validators.ICSControl;
import org.joda.time.LocalDate;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Méthodes pour la construction de données pour le mandat.
 * 
 * @author rprin
 */
public class SepaSddMandatFactory {

	public static final Integer MANDAT_NUMERO_MAX_LENGTH = Integer.valueOf(6);
	public static final Integer REFERENCE_INTERNE_MAX_LENGTH = Integer.valueOf(15);
	/** Longueur des RUM standard en oppsoition aux RUM de migration qui commportent ++ devant */
	public static final Integer RUM_STANDARD_MAX_LENGTH = Integer.valueOf(29);
	public static final DecimalFormat MANDAT_NUMERO_FORMAT = new DecimalFormat(StringUtils.leftPad("", MANDAT_NUMERO_MAX_LENGTH, "0"));
	private static final String EXERCICE_PREFIX = "E";
	private static final String MANDAT_PREFIX = "MAN";
	private static final String NUMERO_PREFIX = "";
	private static final String RUM_SEPARATEUR = "-";

	public static ISepaSddMandat creerMandat(EOEditingContext edc, IGrhumPersonne createur) {
		EOSepaSddMandat sepaSddMandat = EOSepaSddMandat.creerInstance(edc);
		sepaSddMandat.setPersIdCreation(createur.persId());
		sepaSddMandat.setPersIdModification(createur.persId());
		sepaSddMandat.setToModificateurRelationship(createur);

		sepaSddMandat.setDMandatCreation(new DateConversionUtil().formatDateWithoutTimeISO(new LocalDate()));
		sepaSddMandat.setNumero(SepaSddMandatHelper.NUMERO_TEMP);
		sepaSddMandat.setRum(SepaSddMandatHelper.RUM_TEMP);
		sepaSddMandat.setToTypeEtatRelationship(EOJefyAdminTypeEtat.fetchByKeyValue(edc, EOJefyAdminTypeEtat.TYET_LIBELLE_KEY, EOJefyAdminTypeEtat.ETAT_A_VALIDER));
		sepaSddMandat.setRefAppliCreation(FwkCktlComptaMoteurCtrl.getSharedInstance().getRefApplicationCreation());

		sepaSddMandat.setCTypePrelevement(SepaSddMandatHelper.TYPE_PRELEVEMENT_CODE_PONCTUEL);

		return sepaSddMandat;
	}

	/**
	 * Cette méthode renvoie une chaine de 15 caractères permettant à un utilisateur d'identifier le mandat. Cette référece sera utiliser pour
	 * construire la RUM du mandat SDD.<br>
	 * Format : "E" + exercice + -MAN + n° du mandat(sur 6 caracteres) <br>
	 * Exemple : E2013-MAN000002<br>
	 * 
	 * @param mandat instance du mandat coté serveur
	 * @throws Exception (erreurs si la reference ne peut pas être construite correctement)
	 * @return une chaine de 15 caractères.
	 */
	protected String genereReferenceInterne(ISepaSddMandat mandat) throws Exception {
		String exercice = String.valueOf(new LocalDate().getYear());
		if (mandat.numero() == null) {
			throw new Exception("Le numero du mandat ne peut pas être nul.");
		}
		if (mandat.numero().length() != MANDAT_NUMERO_MAX_LENGTH) {
			throw new Exception("Le nombre de caracteres pour le numero du mandat doit comporter " + MANDAT_NUMERO_MAX_LENGTH + " caracteres au lieu de " + mandat.numero().length() + " (" + mandat.numero() + ")");
		}
		String ref = EXERCICE_PREFIX.concat(exercice).concat(RUM_SEPARATEUR).concat(MANDAT_PREFIX).concat(NUMERO_PREFIX).concat(mandat.numero());
		if (ref.length() != REFERENCE_INTERNE_MAX_LENGTH) {
			throw new SizeErrorException("La référence interne doit comporter " + REFERENCE_INTERNE_MAX_LENGTH + " caracteres au lieu de " + ref.length() + " (" + ref + ")");
		}
		return ref;
	}

	/**
	 * <b>Extrait Spec DGFIP tome 1 / par. 6.4</b><br>
	 * Cette référence identifie pour un créancier donné, chaque mandat signé par chaque débiteur. Elle doit être unique pour chaque mandat et pour un
	 * identifiant créancier SEPA donné. L’organisme créancier est libre d’attribuer la référence qu’il souhaite (maximum 35 caractères sans espace et
	 * ne comportant que les caractères «latins»). <br>
	 * <b><i>Recommandation sur la structure de la RUM : </i></b>Toutefois, afin de respecter cette règle, il est fortement recommandé aux organismes
	 * créanciers d’inclure dans chacune des RUM attribuée aux mandats de l’émetteur, l’ICS qui leur a été attribué (cf. point 3 de la partie «
	 * Définitions » de la fiche n° 2 du présent document). Cet ICS comporte soit « ZZZ » dans son code activité, soit un code activité personnalisé
	 * (cas des régies de collectivités locales rattachées à une même collectivité, de différents services d’un même établissement public ou de régies
	 * d’établissements publics utilisant l’ICS attribué uniquement à l’établissement public en question).<br>
	 * 1/ Pour un établissement dont l’ICS comporte le code activité valorisé à « ZZZ » (FRCCZZZ414414), la RUM sera structurée de la manière
	 * suivante:
	 * <ul>
	 * <li>-le premier mandat comportera la RUM suivante : « FRCCZZZ414414NNNNNNNNNFD498… » (où « N » est la référence attribuée par l’établissement à
	 * ce mandat. 23 caractères sont dès lors disponibles pour cette référence « libre ») ;</li>
	 * <li>-le second mandat comportera la RUM suivante : « FRCCZZZ414414NNNNNNNNNFD758… » La même procédure doit être appliquée à l’ensemble des RUM
	 * attribuées aux mandats, l’impératif étant que ces RUM soient uniques pour un même créancier et par mandat.</li>
	 * </ul>
	 * <b>Implémentation dans Cocktail</b>
	 * <ul>
	 * <li>ICS : 13 caracteres</li>
	 * <li>Référence (unique) : 15 caractères : E + exercice + -MAN + numero mandat (6 caracteres)</li>
	 * </ul>
	 * Pour des raisons de sécurité, la RUM ne doit pas contenir d'identifiant de personne (directive DGFIP).
	 * 
	 * @throws Exception (erreurs si la RUM ne peut pas être construite correctement)
	 * @param mandat instance du mandat coté serveur
	 * @return Une RUM (Référence Unique du Mandat) sur 35 caracteres
	 */
	public String creerRUMPourMandat(ISepaSddMandat mandat) throws Exception {
		try {
			String ics = mandat.toSepaSddParam().creancierIcs();
			//String numero = mandat.numero();
			String referenceInterne = genereReferenceInterne(mandat);
			new ICSControl().checkICS(ics);

			String rum = ics.concat(RUM_SEPARATEUR).concat(referenceInterne);
			if (rum.length() != RUM_STANDARD_MAX_LENGTH) {
				throw new Exception("Le nombre de caracteres de la RUM doit comporter " + RUM_STANDARD_MAX_LENGTH + " caracteres au lieu de " + rum.length() + " (" + rum + ")");
			}
			return rum;
		} catch (Exception e) {
			throw new IllegalArgumentException("Erreur lors de la creation de la RUM : " + e.getMessage(), e);
		}

	}

	/**
	 * @param mandat Le mandat
	 * @return Un numero unique pour le mandat, sur NUMERO_MAX_LENGTH caractères, construit à partir de la sequence {@link EOSepaSddMandatNumeroSeq}.
	 */
	public String creerNumeroUniquePourMandat(EOSepaSddMandat mandat) {
		EOSepaSddMandatNumeroSeq seq = EOSepaSddMandatNumeroSeq.fetchAll(mandat.editingContext()).objectAtIndex(0);
		return MANDAT_NUMERO_FORMAT.format(seq.nextval());
	}

	/**
	 * Crée un historique pour le mandat s'il y a eu des changements probant. Consiste àen gros à dupliquer les infos actuelles du mandat en les
	 * placant dans mandatHisto.
	 * 
	 * @param mandat Le mandat
	 */
	public void creerOuNonHistoriqueDuMandat(EOSepaSddMandat mandat) {
		EOSepaSddMandatHisto prevMandatHisto = null;
		NSArray<EOSepaSddMandatHisto> mandatHistos = mandat.toSepaSddMandatHistos(null, new NSArray<EOSortOrdering>(EOSepaSddMandatHisto.D_CREATION.desc()), true);
		if (mandatHistos.count() > 0) {
			prevMandatHisto = mandatHistos.objectAtIndex(0);
		}
		SepaSddMandatHistoFactory factory = new SepaSddMandatHistoFactory();
		boolean historiser = true;
		String libelle = null;
		//s'il y a deja un historique on compare le mandat actuel avec le precedent et s'il y a des chngements, on enregistre un nouvel historique
		if (prevMandatHisto != null) {
			List<String> clesModifiees = factory.clesModifieesEntreMandatEtMandatHisto(mandat, prevMandatHisto);
			if (clesModifiees.size() == 0) {
				historiser = false;
			}
			else {
				libelle = factory.construitMessageAPartirDesClesModifiees(clesModifiees);
			}
		}

		if (historiser) {
			if (libelle == null) {
				libelle = "Création du mandat";
			}
			EOSepaSddMandatHisto mandatHisto = factory.creerMandatHistoAPartirDuMandat(mandat);
			mandatHisto.setLibelle(libelle);
			mandatHisto.setToSepaSddMandatRelationship(mandat);
		}
	}
}
