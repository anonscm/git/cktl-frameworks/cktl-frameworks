package org.cocktail.fwkcktlcompta.server;

import org.apache.log4j.Logger;

import er.extensions.ERXFrameworkPrincipal;

public class FwkCktlCompta extends ERXFrameworkPrincipal {
	public static final Logger LOG = Logger.getLogger(FwkCktlCompta.class);
	public static FwkCktlComptaParamManager paramManager = new FwkCktlComptaParamManager();

	static {
		setUpFrameworkPrincipalClass(FwkCktlCompta.class);
	}

	@Override
	public void didFinishInitialization() {
		super.didFinishInitialization();
		paramManager.checkAndInitParamsWithDefault();

	}

	@Override
	public void finishInitialization() {
		//	
	}
}
