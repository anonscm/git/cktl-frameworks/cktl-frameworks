package org.cocktail.fwkcktlcompta.server.services;

import org.cocktail.fwkcktlcompta.common.helpers.EcritureDetailHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.rules.SepaSddMandatRule;
import org.cocktail.fwkcktlcompta.common.util.ZDateUtil;
import org.cocktail.fwkcktlcompta.server.factories.EOEcritureFactory;
import org.cocktail.fwkcktlcompta.server.factories.PrelevementEcrituresFactory;
import org.cocktail.fwkcktlcompta.server.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.server.metier.EOEcriture;
import org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail;
import org.cocktail.fwkcktlcompta.server.metier.EOGestion;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;

public class SepaSddEcheancierVisaService {

	private static final String EMARGEMENTS_GENERES_GIDS_KEY = "emargementsGeneresGids";
	public static final String ERREURS_KEY = "erreurs";
	public static final String ECRITURES_GENERES_GIDS_KEY = "ecrituresGeneresGids";
	public static final String ECRITURES_GENERES_KEY = "ecrituresGeneres";
	private final EOEditingContext editingContext;
	private final EOComptabilite comptabilite;
	private final EOJefyAdminExercice exercice;
	private final EOJefyAdminUtilisateur utilisateur;
	private final NSTimestamp dateCreation;
	private final EOTypeEmargement typeEmargement;
	private final EOTypeJournal typeJournal;
	private final EOTypeOperation typeOperation;

	public SepaSddEcheancierVisaService(EOEditingContext editingContext, EOComptabilite comptabilite, EOJefyAdminExercice exercice, EOJefyAdminUtilisateur utilisateur, NSTimestamp dateCreation,
			EOTypeEmargement typeEmargement, EOTypeJournal typeJournal, EOTypeOperation typeOperation) {
		this.editingContext = editingContext;
		this.comptabilite = comptabilite;
		this.exercice = exercice;
		this.utilisateur = utilisateur;
		this.dateCreation = dateCreation;
		this.typeEmargement = typeEmargement;
		this.typeJournal = typeJournal;
		this.typeOperation = typeOperation;

	}

	/**
	 * Crée une iécriture de prise en charge d'echeancier sans création d'emargement, sans numerotation et sans enregistrement.
	 * 
	 * @param echeancier
	 * @param compteTiersCreditEcheancier
	 * @param compteAttenteDebitEcheancier
	 * @param gestion
	 * @return
	 * @throws Exception
	 */
	public NSDictionary<String, Object> creerEcriturePriseEnchargeEcheancierSansEmargement(EOSepaSddEcheancier echeancier, EOPlanComptableExer compteTiersCreditEcheancier, EOPlanComptableExer compteAttenteDebitEcheancier, EOGestion gestion) throws Exception {
		String erreurs = "";
		String warnings = "";
		NSMutableDictionary<String, Object> resultat = new NSMutableDictionary<String, Object>();
		try {
			EOSepaSddMandat mandat = EOSepaSddMandat.fetchByKeyValue(getEditingContext(), EOSepaSddMandat.ID_SEPA_SDD_MANDAT_KEY, echeancier.idSepaSddMandat());
			echeancier.setToSepaSddMandatRelationship(mandat);
			SepaSddMandatRule.getSharedInstance().checkMandatValide(echeancier.mandat());
			try {
				if (SepaSddEcheancierHelper.getSharedInstance().isEcheancierPrisEnCharge(echeancier)) {
					throw new Exception("L'échéancier est déjà pris en charge par l'écriture " + EcritureDetailHelper.getSharedInstance().getNumeroEcritureAvecExerciceEtIndex(SepaSddEcheancierHelper.getSharedInstance().getLastEcritureDetailPriseEnCharge(echeancier)));
				}
				String ecrLibelle = SepaSddEcheancierHelper.getSharedInstance().getLibelleEcheancierComplet(echeancier);
				EOEcritureFactory ecrFactory = new EOEcritureFactory();
				EOEcriture ecr = ecrFactory.creerEcriture(getEditingContext(), new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.now())), ecrLibelle, getComptabilite(), getExercice(), getTypeJournal(), getTypeOperation(), getUtilisateur());
				PrelevementEcrituresFactory prelevementEcrituresFactory = new PrelevementEcrituresFactory();

				Integer ecdIndex = Integer.valueOf(1);
				try {
					EOEcritureDetail credit = prelevementEcrituresFactory.creerEcritureDetailCreditPourPriseEnChargeEcheancier(getExercice(), (EOGestion) gestion, ecdIndex, echeancier, compteTiersCreditEcheancier);
					ecr.addToToEcritureDetailsRelationship(credit);
					ecdIndex++;
				} catch (Exception e) {
					e.printStackTrace();
					throw new Exception("Erreur lors de la création des crédits de l'écriture de prise en charge de l'échéancier \n" + SepaSddEcheancierHelper.getSharedInstance().getLibelleEcheancierComplet(echeancier) + " :\n" + e.getMessage(), e);
				}

				EOGestion gestionDebit = getComptabilite().toGestion();
				EOEcritureDetail ecdDebit = prelevementEcrituresFactory.creerEcritureDetailDebitPourPriseEnChargeEcheancier(getExercice(), gestionDebit, Integer.valueOf(ecdIndex), echeancier);
				ecr.addToToEcritureDetailsRelationship(ecdDebit);
				resultat.takeValueForKey(ERXEOControlUtilities.globalIDsForObjects(new NSArray<EOEcriture>(ecr)), ECRITURES_GENERES_GIDS_KEY);
				resultat.takeValueForKey(new NSArray<EOEcriture>(ecr), ECRITURES_GENERES_KEY);

			} catch (Exception e) {
				e.printStackTrace();
				erreurs += e.getMessage();
			}

			if (erreurs.length() > 0) {
				resultat.takeValueForKey(erreurs, ERREURS_KEY);
			}
			if (warnings.length() > 0) {
				resultat.takeValueForKey(erreurs, ERREURS_KEY);
			}
			return resultat.immutableClone();
		} catch (Exception e) {
			getEditingContext().revert();
			e.printStackTrace();
			throw e;
		}

	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public EOComptabilite getComptabilite() {
		return comptabilite;
	}

	public EOJefyAdminExercice getExercice() {
		return exercice;
	}

	public EOJefyAdminUtilisateur getUtilisateur() {
		return utilisateur;
	}

	public NSTimestamp getDateCreation() {
		return dateCreation;
	}

	public EOTypeEmargement getTypeEmargement() {
		return typeEmargement;
	}

	public EOTypeJournal getTypeJournal() {
		return typeJournal;
	}

	public EOTypeOperation getTypeOperation() {
		return typeOperation;
	}
}
