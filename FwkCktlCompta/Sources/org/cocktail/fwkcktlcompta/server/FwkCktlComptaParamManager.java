package org.cocktail.fwkcktlcompta.server;

import org.cocktail.fwkcktlcompta.common.IFwkCktlComptaParam;
import org.cocktail.fwkcktlcompta.server.finder.FinderParametre;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametresType;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;
import org.joda.time.LocalDate;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOAccessUtilities;

/**
 * a completer
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class FwkCktlComptaParamManager extends CktlParamManager implements IFwkCktlComptaParam {

	private EOEditingContext ec = ERXEC.newEditingContext();

	public FwkCktlComptaParamManager() {
		getParamList().add(PRELEVEMENTNATIONAL_ENABLED);
		getParamComments().put(PRELEVEMENTNATIONAL_ENABLED, "Autoriser la création de mandats de prélèvements Nationaux");
		getParamDefault().put(PRELEVEMENTNATIONAL_ENABLED, "OUI");
		getParamTypes().put(PRELEVEMENTNATIONAL_ENABLED, EOGrhumParametresType.codeActivation);

		getParamList().add(SEPASDDMANDAT_ENABLED);
		getParamComments().put(SEPASDDMANDAT_ENABLED, "Autoriser la création de mandats de prélèvements SEPA");
		getParamDefault().put(SEPASDDMANDAT_ENABLED, "OUI");
		getParamTypes().put(SEPASDDMANDAT_ENABLED, EOGrhumParametresType.codeActivation);

		getParamList().add(SEPASDDMANDAT_DATEFINMISENPLACE);
		getParamComments().put(SEPASDDMANDAT_DATEFINMISENPLACE, "Date à laquelle le SEPA devient obligatoire (hors TOM).");
		getParamDefault().put(SEPASDDMANDAT_DATEFINMISENPLACE, "01/02/2014");
		getParamTypes().put(SEPASDDMANDAT_DATEFINMISENPLACE, EOGrhumParametresType.dateFormatee);

		getParamList().add(SEPASDDMANDAT_NBECHEANCES);
		getParamComments().put(SEPASDDMANDAT_NBECHEANCES, "Nombre d''échéances proposées par défaut lors de la création d'un échéancier de prélèvements SEPA");
		getParamDefault().put(SEPASDDMANDAT_NBECHEANCES, "3");
		getParamTypes().put(SEPASDDMANDAT_NBECHEANCES, EOGrhumParametresType.valeurNumerique);


		getParamList().add(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_FIRST);
		getParamComments().put(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_FIRST, "Nombre de jours minimum à respecter avant la date de prélèvement pour la création des fichiers de prélèvements SEPA de type FIRST.");
		getParamDefault().put(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_FIRST, "6");
		getParamTypes().put(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_FIRST, EOGrhumParametresType.valeurNumerique);

		getParamList().add(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_OOFF);
		getParamComments().put(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_OOFF, "Nombre de jours minimum à respecter avant la date de prélèvement pour la création des fichiers de prélèvements SEPA de type OOFF.");
		getParamDefault().put(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_OOFF, "6");
		getParamTypes().put(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_OOFF, EOGrhumParametresType.valeurNumerique);

		getParamList().add(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_RCUR);
		getParamComments().put(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_RCUR, "Nombre de jours minimum à respecter avant la date de prélèvement pour la création des fichiers de prélèvements SEPA de type RCUR.");
		getParamDefault().put(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_RCUR, "3");
		getParamTypes().put(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_RCUR, EOGrhumParametresType.valeurNumerique);

		getParamList().add(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_FNAL);
		getParamComments().put(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_FNAL, "Nombre de jours minimum à respecter avant la date de prélèvement pour la création des fichiers de prélèvements SEPA de type FNAL.");
		getParamDefault().put(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_FNAL, "3");
		getParamTypes().put(SEPASDDMANDAT_TYPEOPERATION_NBJOURSAVANTREMISE_FNAL, EOGrhumParametresType.valeurNumerique);

		getParamList().add(SEPASDDMANDAT_NB_JOURS_MOIS_SUIVANT);
		getParamComments().put(SEPASDDMANDAT_NB_JOURS_MOIS_SUIVANT, "Nombre de jours avant le debut du mois suivant pour que la date calculée de la première échéance bascule sur le mois m+2 au lieu de m+1");
		getParamTypes().put(SEPASDDMANDAT_NB_JOURS_MOIS_SUIVANT, EOGrhumParametresType.valeurNumerique);

		getParamList().add(SEPASDDMANDAT_NUMERO_JOUR);
		getParamComments().put(SEPASDDMANDAT_NUMERO_JOUR, "Numero du jour dans le mois pour déterminer la date des échéances par défaut");
		getParamTypes().put(SEPASDDMANDAT_NUMERO_JOUR, EOGrhumParametresType.valeurNumerique);

		getParamList().add(SEPASDDMANDAT_NB_JOURS_MINIMUM_AVANT_DEUXIEME_ECHEANCE);
		getParamComments().put(SEPASDDMANDAT_NB_JOURS_MINIMUM_AVANT_DEUXIEME_ECHEANCE, "Délai à respecter en jours pour déterminer la deuxième date d'échéance sur un même mandat (alerte non blocante)");
		getParamDefault().put(SEPASDDMANDAT_NB_JOURS_MINIMUM_AVANT_DEUXIEME_ECHEANCE, "15");
		getParamTypes().put(SEPASDDMANDAT_NB_JOURS_MINIMUM_AVANT_DEUXIEME_ECHEANCE, EOGrhumParametresType.valeurNumerique);

		getParamList().add(SEPASDDMANDAT_NB_MOIS_AVANT_CADUCITE);
		getParamComments().put(SEPASDDMANDAT_NB_MOIS_AVANT_CADUCITE, "Nombre de mois sans prélèvements effectués sur un mandat avant que ce dernier ne soit considéré comme caduc (indiqué par DGFIP)");
		getParamDefault().put(SEPASDDMANDAT_NB_MOIS_AVANT_CADUCITE, "36");
		getParamTypes().put(SEPASDDMANDAT_NB_MOIS_AVANT_CADUCITE, EOGrhumParametresType.valeurNumerique);

		getParamList().add(SEPASDDMANDAT_SCOLARIX_MODERECOUVREMENTCODEDEFAUT);
		getParamComments().put(SEPASDDMANDAT_SCOLARIX_MODERECOUVREMENTCODEDEFAUT, "Code du mode de recouvrement ECHEANCIER à utiliser par défaut pour le paiement des droits d'inscription échélonnés via le SEPA. Cf Champ maracuja.mode_recouvrement.mod_code");
		getParamDefault().put(SEPASDDMANDAT_SCOLARIX_MODERECOUVREMENTCODEDEFAUT, "XXX"); //70
		getParamTypes().put(SEPASDDMANDAT_SCOLARIX_MODERECOUVREMENTCODEDEFAUT, EOGrhumParametresType.texteLibre);

		getParamList().add(SEPASDDMANDAT_CORIANDRE_MODERECOUVREMENTCODEDEFAUT);
		getParamComments().put(SEPASDDMANDAT_CORIANDRE_MODERECOUVREMENTCODEDEFAUT, "Code du mode de recouvrement ECHEANCIER à utiliser par défaut pour le paiement des droits d'inscription échélonnés via le SEPA. Cf Champ maracuja.mode_recouvrement.mod_code");
		getParamDefault().put(SEPASDDMANDAT_CORIANDRE_MODERECOUVREMENTCODEDEFAUT, "XXX"); //70
		getParamTypes().put(SEPASDDMANDAT_CORIANDRE_MODERECOUVREMENTCODEDEFAUT, EOGrhumParametresType.texteLibre);

	}

	@Override
	public void checkAndInitParamsWithDefault() {
		Integer numeroJourDefaut = FinderParametre.getNumeroJour(ec, new LocalDate().getYear());
		getParamDefault().put(SEPASDDMANDAT_NUMERO_JOUR, numeroJourDefaut.toString());
		Integer nbJoursDefaut = FinderParametre.getNbJoursMoisSuivant(ec, new LocalDate().getYear());
		getParamDefault().put(SEPASDDMANDAT_NB_JOURS_MOIS_SUIVANT, nbJoursDefaut.toString());
		super.checkAndInitParamsWithDefault();
	}

	/**
	 * Cree un nouveau parametre de type EOGrhumParametresType.codeActivation
	 */
	@Override
	public void createNewParam(String key, String value, String comment) {
		createNewParam(key, value, comment, EOGrhumParametresType.codeActivation);

	}

	@Override
	public void createNewParam(String key, String value, String comment,
			String type) {
		EOGrhumParametres newParametre = EOGrhumParametres.creerInstance(ec);
		newParametre.setParamKey(key);
		newParametre.setParamValue(value);
		newParametre.setParamCommentaires(comment);
		newParametre.setToParametresTypeRelationship(EOGrhumParametresType.fetchByKeyValue(ec, EOGrhumParametresType.TYPE_ID_INTERNE_KEY, type));
		if (ec.hasChanges()) {
			EOEntity entityParameter = ERXEOAccessUtilities.entityForEo(newParametre);
			try {

				// Avant de sauvegarder les données, nous modifions le modèle
				// pour que l'on puisse avoir accès aussi en écriture sur les données
				entityParameter.setReadOnly(false);
				ec.saveChanges();
				//ec.unlock();

			} catch (Exception e) {
				log.warn("Erreur lors de l'enregistrement des parametres.");
				e.printStackTrace();
			} finally {
				entityParameter.setReadOnly(true);
			}
		}

	}

	@Override
	public String getParam(String key) {
		String res = getApplication().config().stringForKey(key);
		return res;
	}

}
