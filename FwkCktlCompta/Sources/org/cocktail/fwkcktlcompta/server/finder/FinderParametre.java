package org.cocktail.fwkcktlcompta.server.finder;

import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOParametre;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * permet de recuperer des parametres dans la tale maracuja.parametre
 * 
 * @author rprin
 */
public class FinderParametre {

	public static final String NB_JOURS_MOIS_SUIVANT = "org.cocktail.gfc.echeancier.dateecheance.nbjoursmoissuivant";
	public static final String NUMERO_JOUR = "org.cocktail.gfc.echeancier.dateecheance.numerojour";
	public static final String NB_ECHEANCES = "org.cocktail.gfc.echeancier.nbecheances";

	public static final Integer NB_JOURS_MOIS_SUIVANT_PAR_DEFAUT = 1;
	public static final Integer NUMERO_JOUR_PAR_DEFAUT = 5;
	public static final Integer NB_ECHEANCES_PAR_DEFAUT = 3;

	public static int getNbJoursMoisSuivant(EOEditingContext ec, Integer exeOrdre) {
		EOParametre parametre = getParametre(ec, NB_JOURS_MOIS_SUIVANT, exeOrdre);
		Integer nbJoursMoisSuivant;
		if (parametre != null) {
			nbJoursMoisSuivant = Integer.valueOf(parametre.parValue());
			if (nbJoursMoisSuivant.intValue() <= 0) {
				nbJoursMoisSuivant = NB_JOURS_MOIS_SUIVANT_PAR_DEFAUT;
			}
		} else {
			nbJoursMoisSuivant = NB_JOURS_MOIS_SUIVANT_PAR_DEFAUT;
		}
		return nbJoursMoisSuivant;
	}

	public static int getNumeroJour(EOEditingContext ec, Integer exeOrdre) {
		EOParametre parametre = getParametre(ec, NUMERO_JOUR, exeOrdre);
		Integer numeroJour;
		if (parametre != null) {
			numeroJour = Integer.valueOf(parametre.parValue());
			if (numeroJour.intValue() <= 0) {
				numeroJour = NUMERO_JOUR_PAR_DEFAUT;
			}
		} else {
			numeroJour = NUMERO_JOUR_PAR_DEFAUT;
		}
		return numeroJour;
	}

	public static int getNombreEcheances(EOEditingContext ec, Integer exeOrdre) {
		EOParametre parametre = getParametre(ec, NB_ECHEANCES, exeOrdre);
		Integer nbEcheances = NB_ECHEANCES_PAR_DEFAUT;
		if (parametre != null) {
			nbEcheances = Integer.valueOf(parametre.parValue());
			if (nbEcheances.intValue() <= 0) {
				nbEcheances = NB_ECHEANCES_PAR_DEFAUT;
			}
		}
		return nbEcheances;
	}

	public static EOParametre getParametre(EOEditingContext ec, String cle, Integer exeOrdre) {
        ERXFetchSpecification<EOParametre> fetchSpec = new ERXFetchSpecification<EOParametre>(EOParametre.ENTITY_NAME);
        EOQualifier qualifierCle = EOParametre.PAR_KEY.eq(cle);
        EOQualifier qualifierExercice = EOParametre.EXERCICE.dot(EOJefyAdminExercice.EXE_ORDRE_KEY).eq(exeOrdre);

        EOQualifier qual = ERXQ.and(
                qualifierCle,
                qualifierExercice);
        fetchSpec.setQualifier(qual);
        fetchSpec.setUsesDistinct(true);
        NSArray<EOParametre> parametres = fetchSpec.fetchObjects(ec);
        return ERXArrayUtilities.firstObject(parametres);
    }

}
