/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBordereauBrouillard.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOBordereauBrouillard extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_BordereauBrouillard";
	public static final String ENTITY_TABLE_NAME = "maracuja.bordereau_brouillard";


//Attribute Keys
	public static final ERXKey<String> BOB_ETAT = new ERXKey<String>("bobEtat");
	public static final ERXKey<String> BOB_LIBELLE1 = new ERXKey<String>("bobLibelle1");
	public static final ERXKey<String> BOB_LIBELLE2 = new ERXKey<String>("bobLibelle2");
	public static final ERXKey<String> BOB_LIBELLE3 = new ERXKey<String>("bobLibelle3");
	public static final ERXKey<java.math.BigDecimal> BOB_MONTANT = new ERXKey<java.math.BigDecimal>("bobMontant");
	public static final ERXKey<String> BOB_OPERATION = new ERXKey<String>("bobOperation");
	public static final ERXKey<String> BOB_SENS = new ERXKey<String>("bobSens");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereau> TO_BORDEREAU = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereau>("toBordereau");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard> TO_ECHEANCIER_BROUILLARDS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard>("toEcheancierBrouillards");
	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestion> TO_GESTION = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestion>("toGestion");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable> TO_PLAN_COMPTABLE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable>("toPlanComptable");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> TO_SEPA_SDD_ECHEANCIER_BOBS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob>("toSepaSddEcheancierBobs");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bobOrdre";

	public static final String BOB_ETAT_KEY = "bobEtat";
	public static final String BOB_LIBELLE1_KEY = "bobLibelle1";
	public static final String BOB_LIBELLE2_KEY = "bobLibelle2";
	public static final String BOB_LIBELLE3_KEY = "bobLibelle3";
	public static final String BOB_MONTANT_KEY = "bobMontant";
	public static final String BOB_OPERATION_KEY = "bobOperation";
	public static final String BOB_SENS_KEY = "bobSens";

//Attributs non visibles
	public static final String BOB_ORDRE_KEY = "bobOrdre";
	public static final String BOR_ID_KEY = "borId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String BOB_ETAT_COLKEY = "bob_etat";
	public static final String BOB_LIBELLE1_COLKEY = "bob_libelle1";
	public static final String BOB_LIBELLE2_COLKEY = "bob_libelle2";
	public static final String BOB_LIBELLE3_COLKEY = "bob_libelle3";
	public static final String BOB_MONTANT_COLKEY = "bob_Montant";
	public static final String BOB_OPERATION_COLKEY = "bob_Operation";
	public static final String BOB_SENS_COLKEY = "bob_Sens";

	public static final String BOB_ORDRE_COLKEY = "bob_ordre";
	public static final String BOR_ID_COLKEY = "bor_id";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_Code";
	public static final String PCO_NUM_COLKEY = "pco_num";


	// Relationships
	public static final String TO_BORDEREAU_KEY = "toBordereau";
	public static final String TO_ECHEANCIER_BROUILLARDS_KEY = "toEcheancierBrouillards";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_GESTION_KEY = "toGestion";
	public static final String TO_PLAN_COMPTABLE_KEY = "toPlanComptable";
	public static final String TO_SEPA_SDD_ECHEANCIER_BOBS_KEY = "toSepaSddEcheancierBobs";



	// Accessors methods
	public String bobEtat() {
	 return (String) storedValueForKey(BOB_ETAT_KEY);
	}

	public void setBobEtat(String value) {
	 takeStoredValueForKey(value, BOB_ETAT_KEY);
	}

	public String bobLibelle1() {
	 return (String) storedValueForKey(BOB_LIBELLE1_KEY);
	}

	public void setBobLibelle1(String value) {
	 takeStoredValueForKey(value, BOB_LIBELLE1_KEY);
	}

	public String bobLibelle2() {
	 return (String) storedValueForKey(BOB_LIBELLE2_KEY);
	}

	public void setBobLibelle2(String value) {
	 takeStoredValueForKey(value, BOB_LIBELLE2_KEY);
	}

	public String bobLibelle3() {
	 return (String) storedValueForKey(BOB_LIBELLE3_KEY);
	}

	public void setBobLibelle3(String value) {
	 takeStoredValueForKey(value, BOB_LIBELLE3_KEY);
	}

	public java.math.BigDecimal bobMontant() {
	 return (java.math.BigDecimal) storedValueForKey(BOB_MONTANT_KEY);
	}

	public void setBobMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, BOB_MONTANT_KEY);
	}

	public String bobOperation() {
	 return (String) storedValueForKey(BOB_OPERATION_KEY);
	}

	public void setBobOperation(String value) {
	 takeStoredValueForKey(value, BOB_OPERATION_KEY);
	}

	public String bobSens() {
	 return (String) storedValueForKey(BOB_SENS_KEY);
	}

	public void setBobSens(String value) {
	 takeStoredValueForKey(value, BOB_SENS_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOBordereau toBordereau() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBordereau)storedValueForKey(TO_BORDEREAU_KEY);
	}

	public void setToBordereauRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereau value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOBordereau oldValue = toBordereau();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BORDEREAU_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_BORDEREAU_KEY);
	 }
	}

	public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
	 return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGestion toGestion() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
	}

	public void setToGestionRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGestion value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGestion oldValue = toGestion();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable toPlanComptable() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable)storedValueForKey(TO_PLAN_COMPTABLE_KEY);
	}

	public void setToPlanComptableRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable oldValue = toPlanComptable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard> toEcheancierBrouillards() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard>)storedValueForKey(TO_ECHEANCIER_BROUILLARDS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard> toEcheancierBrouillards(EOQualifier qualifier) {
	 return toEcheancierBrouillards(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard> toEcheancierBrouillards(EOQualifier qualifier, Boolean fetch) {
	 return toEcheancierBrouillards(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard> toEcheancierBrouillards(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard.TO_BORDEREAU_BROUILLARD_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toEcheancierBrouillards();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToEcheancierBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_ECHEANCIER_BROUILLARDS_KEY);
	}
	
	public void removeFromToEcheancierBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ECHEANCIER_BROUILLARDS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard createToEcheancierBrouillardsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_ECHEANCIER_BROUILLARDS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard) eo;
	}
	
	public void deleteToEcheancierBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ECHEANCIER_BROUILLARDS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToEcheancierBrouillardsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierBrouillard> objects = toEcheancierBrouillards().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToEcheancierBrouillardsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> toSepaSddEcheancierBobs() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob>)storedValueForKey(TO_SEPA_SDD_ECHEANCIER_BOBS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> toSepaSddEcheancierBobs(EOQualifier qualifier) {
	 return toSepaSddEcheancierBobs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> toSepaSddEcheancierBobs(EOQualifier qualifier, Boolean fetch) {
	 return toSepaSddEcheancierBobs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> toSepaSddEcheancierBobs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob.TO_BORDEREAU_BROUILLARD_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toSepaSddEcheancierBobs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToSepaSddEcheancierBobsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIER_BOBS_KEY);
	}
	
	public void removeFromToSepaSddEcheancierBobsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIER_BOBS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob createToSepaSddEcheancierBobsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_SEPA_SDD_ECHEANCIER_BOBS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob) eo;
	}
	
	public void deleteToSepaSddEcheancierBobsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIER_BOBS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToSepaSddEcheancierBobsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> objects = toSepaSddEcheancierBobs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToSepaSddEcheancierBobsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOBordereauBrouillard avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOBordereauBrouillard createEOBordereauBrouillard(EOEditingContext editingContext				, String bobEtat
													, java.math.BigDecimal bobMontant
									, String bobSens
					, org.cocktail.fwkcktlcompta.server.metier.EOBordereau toBordereau		, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice		, org.cocktail.fwkcktlcompta.server.metier.EOGestion toGestion							) {
	 EOBordereauBrouillard eo = (EOBordereauBrouillard) EOUtilities.createAndInsertInstance(editingContext, _EOBordereauBrouillard.ENTITY_NAME);	 
							eo.setBobEtat(bobEtat);
															eo.setBobMontant(bobMontant);
											eo.setBobSens(bobSens);
						 eo.setToBordereauRelationship(toBordereau);
				 eo.setToExerciceRelationship(toExercice);
				 eo.setToGestionRelationship(toGestion);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBordereauBrouillard creerInstance(EOEditingContext editingContext) {
		EOBordereauBrouillard object = (EOBordereauBrouillard)EOUtilities.createAndInsertInstance(editingContext, _EOBordereauBrouillard.ENTITY_NAME);
  		return object;
		}

	

  public EOBordereauBrouillard localInstanceIn(EOEditingContext editingContext) {
    EOBordereauBrouillard localInstance = (EOBordereauBrouillard)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBordereauBrouillard fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBordereauBrouillard fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOBordereauBrouillard> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBordereauBrouillard eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBordereauBrouillard)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBordereauBrouillard fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBordereauBrouillard fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOBordereauBrouillard> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBordereauBrouillard eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBordereauBrouillard)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBordereauBrouillard fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBordereauBrouillard eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBordereauBrouillard ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBordereauBrouillard fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}