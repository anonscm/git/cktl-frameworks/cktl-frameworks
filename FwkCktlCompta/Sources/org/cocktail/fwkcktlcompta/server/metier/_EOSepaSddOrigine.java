/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSepaSddOrigine.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOSepaSddOrigine extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddOrigine";
	public static final String ENTITY_TABLE_NAME = "maracuja.SEPA_SDD_ORIGINE";


//Attribute Keys
	public static final ERXKey<Integer> ORIGINE_ID = new ERXKey<Integer>("origineId");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier> TO_SEPA_SDD_ECHEANCIERS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier>("toSepaSddEcheanciers");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType> TO_SEPA_SDD_ORIGINE_TYPE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType>("toSepaSddOrigineType");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idSepaSddOrigine";

	public static final String ORIGINE_ID_KEY = "origineId";

//Attributs non visibles
	public static final String ID_SEPA_SDD_ORIGINE_KEY = "idSepaSddOrigine";
	public static final String ID_SEPA_SDD_ORIGINE_TYPE_KEY = "idSepaSddOrigineType";

//Colonnes dans la base de donnees
	public static final String ORIGINE_ID_COLKEY = "ORIGINE_ID";

	public static final String ID_SEPA_SDD_ORIGINE_COLKEY = "ID_SEPA_SDD_ORIGINE";
	public static final String ID_SEPA_SDD_ORIGINE_TYPE_COLKEY = "ID_SEPA_SDD_ORIGINE_TYPE";


	// Relationships
	public static final String TO_SEPA_SDD_ECHEANCIERS_KEY = "toSepaSddEcheanciers";
	public static final String TO_SEPA_SDD_ORIGINE_TYPE_KEY = "toSepaSddOrigineType";



	// Accessors methods
	public Integer origineId() {
	 return (Integer) storedValueForKey(ORIGINE_ID_KEY);
	}

	public void setOrigineId(Integer value) {
	 takeStoredValueForKey(value, ORIGINE_ID_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType toSepaSddOrigineType() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType)storedValueForKey(TO_SEPA_SDD_ORIGINE_TYPE_KEY);
	}

	public void setToSepaSddOrigineTypeRelationship(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType oldValue = toSepaSddOrigineType();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SEPA_SDD_ORIGINE_TYPE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_SEPA_SDD_ORIGINE_TYPE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier> toSepaSddEcheanciers() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier>)storedValueForKey(TO_SEPA_SDD_ECHEANCIERS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier> toSepaSddEcheanciers(EOQualifier qualifier) {
	 return toSepaSddEcheanciers(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier> toSepaSddEcheanciers(EOQualifier qualifier, Boolean fetch) {
	 return toSepaSddEcheanciers(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier> toSepaSddEcheanciers(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier.TO_SEPA_SDD_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toSepaSddEcheanciers();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToSepaSddEcheanciersRelationship(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIERS_KEY);
	}
	
	public void removeFromToSepaSddEcheanciersRelationship(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIERS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier createToSepaSddEcheanciersRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_SEPA_SDD_ECHEANCIERS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier) eo;
	}
	
	public void deleteToSepaSddEcheanciersRelationship(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIERS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToSepaSddEcheanciersRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier> objects = toSepaSddEcheanciers().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToSepaSddEcheanciersRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOSepaSddOrigine avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOSepaSddOrigine createEOSepaSddOrigine(EOEditingContext editingContext				, Integer origineId
					, org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType toSepaSddOrigineType					) {
	 EOSepaSddOrigine eo = (EOSepaSddOrigine) EOUtilities.createAndInsertInstance(editingContext, _EOSepaSddOrigine.ENTITY_NAME);	 
							eo.setOrigineId(origineId);
						 eo.setToSepaSddOrigineTypeRelationship(toSepaSddOrigineType);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSepaSddOrigine creerInstance(EOEditingContext editingContext) {
		EOSepaSddOrigine object = (EOSepaSddOrigine)EOUtilities.createAndInsertInstance(editingContext, _EOSepaSddOrigine.ENTITY_NAME);
  		return object;
		}

	

  public EOSepaSddOrigine localInstanceIn(EOEditingContext editingContext) {
    EOSepaSddOrigine localInstance = (EOSepaSddOrigine)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigine> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigine> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigine> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigine> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigine> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigine> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigine> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigine>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSepaSddOrigine fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSepaSddOrigine fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOSepaSddOrigine> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSepaSddOrigine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSepaSddOrigine)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSepaSddOrigine fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSepaSddOrigine fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOSepaSddOrigine> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSepaSddOrigine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSepaSddOrigine)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSepaSddOrigine fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSepaSddOrigine eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSepaSddOrigine ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSepaSddOrigine fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}