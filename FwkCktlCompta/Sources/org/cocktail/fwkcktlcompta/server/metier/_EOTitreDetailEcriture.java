/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTitreDetailEcriture.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOTitreDetailEcriture extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_TitreDetailEcriture";
	public static final String ENTITY_TABLE_NAME = "maracuja.Titre_Detail_Ecriture";


//Attribute Keys
	public static final ERXKey<NSTimestamp> TDE_DATE = new ERXKey<NSTimestamp>("tdeDate");
	public static final ERXKey<String> TDE_ORIGINE = new ERXKey<String>("tdeOrigine");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> TO_ECRITURE_DETAIL = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail>("toEcritureDetail");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOOrigine> TO_ORIGINE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOOrigine>("toOrigine");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EORecette> TO_RECETTE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EORecette>("toRecette");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTitre> TO_TITRE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTitre>("toTitre");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tdeOrdre";

	public static final String TDE_DATE_KEY = "tdeDate";
	public static final String TDE_ORIGINE_KEY = "tdeOrigine";

//Attributs non visibles
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String REC_ID_KEY = "recId";
	public static final String TDE_ORDRE_KEY = "tdeOrdre";
	public static final String TIT_ID_KEY = "titId";

//Colonnes dans la base de donnees
	public static final String TDE_DATE_COLKEY = "tde_date";
	public static final String TDE_ORIGINE_COLKEY = "TDE_ORIGINE";

	public static final String ECD_ORDRE_COLKEY = "ecd_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORI_ORDRE_COLKEY = "ori_Ordre";
	public static final String REC_ID_COLKEY = "rec_id";
	public static final String TDE_ORDRE_COLKEY = "tde_Ordre";
	public static final String TIT_ID_COLKEY = "TIT_ID";


	// Relationships
	public static final String TO_ECRITURE_DETAIL_KEY = "toEcritureDetail";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_ORIGINE_KEY = "toOrigine";
	public static final String TO_RECETTE_KEY = "toRecette";
	public static final String TO_TITRE_KEY = "toTitre";



	// Accessors methods
	public NSTimestamp tdeDate() {
	 return (NSTimestamp) storedValueForKey(TDE_DATE_KEY);
	}

	public void setTdeDate(NSTimestamp value) {
	 takeStoredValueForKey(value, TDE_DATE_KEY);
	}

	public String tdeOrigine() {
	 return (String) storedValueForKey(TDE_ORIGINE_KEY);
	}

	public void setTdeOrigine(String value) {
	 takeStoredValueForKey(value, TDE_ORIGINE_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail toEcritureDetail() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail)storedValueForKey(TO_ECRITURE_DETAIL_KEY);
	}

	public void setToEcritureDetailRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail oldValue = toEcritureDetail();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ECRITURE_DETAIL_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ECRITURE_DETAIL_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOOrigine toOrigine() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOOrigine)storedValueForKey(TO_ORIGINE_KEY);
	}

	public void setToOrigineRelationship(org.cocktail.fwkcktlcompta.server.metier.EOOrigine value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOOrigine oldValue = toOrigine();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORIGINE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORIGINE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EORecette toRecette() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EORecette)storedValueForKey(TO_RECETTE_KEY);
	}

	public void setToRecetteRelationship(org.cocktail.fwkcktlcompta.server.metier.EORecette value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EORecette oldValue = toRecette();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RECETTE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_RECETTE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOTitre toTitre() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTitre)storedValueForKey(TO_TITRE_KEY);
	}

	public void setToTitreRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTitre value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOTitre oldValue = toTitre();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TITRE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TITRE_KEY);
	 }
	}


	/**
	* Créer une instance de EOTitreDetailEcriture avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOTitreDetailEcriture createEOTitreDetailEcriture(EOEditingContext editingContext				, NSTimestamp tdeDate
							, org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail toEcritureDetail		, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice						, org.cocktail.fwkcktlcompta.server.metier.EOTitre toTitre					) {
	 EOTitreDetailEcriture eo = (EOTitreDetailEcriture) EOUtilities.createAndInsertInstance(editingContext, _EOTitreDetailEcriture.ENTITY_NAME);	 
							eo.setTdeDate(tdeDate);
								 eo.setToEcritureDetailRelationship(toEcritureDetail);
				 eo.setToExerciceRelationship(toExercice);
								 eo.setToTitreRelationship(toTitre);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTitreDetailEcriture creerInstance(EOEditingContext editingContext) {
		EOTitreDetailEcriture object = (EOTitreDetailEcriture)EOUtilities.createAndInsertInstance(editingContext, _EOTitreDetailEcriture.ENTITY_NAME);
  		return object;
		}

	

  public EOTitreDetailEcriture localInstanceIn(EOEditingContext editingContext) {
    EOTitreDetailEcriture localInstance = (EOTitreDetailEcriture)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTitreDetailEcriture fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTitreDetailEcriture fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTitreDetailEcriture> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTitreDetailEcriture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTitreDetailEcriture)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTitreDetailEcriture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTitreDetailEcriture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTitreDetailEcriture> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTitreDetailEcriture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTitreDetailEcriture)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTitreDetailEcriture fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTitreDetailEcriture eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTitreDetailEcriture ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTitreDetailEcriture fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}