/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBordereauInfo.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOBordereauInfo extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_BordereauInfo";
	public static final String ENTITY_TABLE_NAME = "maracuja.bordereau_info";


//Attribute Keys
	public static final ERXKey<String> BOR_LIBELLE = new ERXKey<String>("borLibelle");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereau> TO_BORDEREAU = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereau>("toBordereau");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable> TO_PLAN_COMPTABLE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable>("toPlanComptable");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "borId";

	public static final String BOR_LIBELLE_KEY = "borLibelle";

//Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String PCO_NUM_VISA_KEY = "pcoNumVisa";

//Colonnes dans la base de donnees
	public static final String BOR_LIBELLE_COLKEY = "bor_libelle";

	public static final String BOR_ID_COLKEY = "bor_id";
	public static final String PCO_NUM_VISA_COLKEY = "pco_num_visa";


	// Relationships
	public static final String TO_BORDEREAU_KEY = "toBordereau";
	public static final String TO_PLAN_COMPTABLE_KEY = "toPlanComptable";



	// Accessors methods
	public String borLibelle() {
	 return (String) storedValueForKey(BOR_LIBELLE_KEY);
	}

	public void setBorLibelle(String value) {
	 takeStoredValueForKey(value, BOR_LIBELLE_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOBordereau toBordereau() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBordereau)storedValueForKey(TO_BORDEREAU_KEY);
	}

	public void setToBordereauRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereau value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOBordereau oldValue = toBordereau();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BORDEREAU_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_BORDEREAU_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable toPlanComptable() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable)storedValueForKey(TO_PLAN_COMPTABLE_KEY);
	}

	public void setToPlanComptableRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable oldValue = toPlanComptable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_KEY);
	 }
	}


	/**
	* Créer une instance de EOBordereauInfo avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOBordereauInfo createEOBordereauInfo(EOEditingContext editingContext				, org.cocktail.fwkcktlcompta.server.metier.EOBordereau toBordereau							) {
	 EOBordereauInfo eo = (EOBordereauInfo) EOUtilities.createAndInsertInstance(editingContext, _EOBordereauInfo.ENTITY_NAME);	 
						 eo.setToBordereauRelationship(toBordereau);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBordereauInfo creerInstance(EOEditingContext editingContext) {
		EOBordereauInfo object = (EOBordereauInfo)EOUtilities.createAndInsertInstance(editingContext, _EOBordereauInfo.ENTITY_NAME);
  		return object;
		}

	

  public EOBordereauInfo localInstanceIn(EOEditingContext editingContext) {
    EOBordereauInfo localInstance = (EOBordereauInfo)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBordereauInfo fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBordereauInfo fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOBordereauInfo> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBordereauInfo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBordereauInfo)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBordereauInfo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBordereauInfo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOBordereauInfo> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBordereauInfo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBordereauInfo)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBordereauInfo fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBordereauInfo eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBordereauInfo ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBordereauInfo fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}