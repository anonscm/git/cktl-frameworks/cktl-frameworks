/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMandat.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOMandat extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Mandat";
	public static final String ENTITY_TABLE_NAME = "maracuja.Mandat";


//Attribute Keys
	public static final ERXKey<NSTimestamp> MAN_ATTENTE_DATE = new ERXKey<NSTimestamp>("manAttenteDate");
	public static final ERXKey<String> MAN_ATTENTE_OBJET = new ERXKey<String>("manAttenteObjet");
	public static final ERXKey<NSTimestamp> MAN_DATE_REMISE = new ERXKey<NSTimestamp>("manDateRemise");
	public static final ERXKey<NSTimestamp> MAN_DATE_VISA_PRINC = new ERXKey<NSTimestamp>("manDateVisaPrinc");
	public static final ERXKey<String> MAN_ETAT = new ERXKey<String>("manEtat");
	public static final ERXKey<String> MAN_ETAT_REMISE = new ERXKey<String>("manEtatRemise");
	public static final ERXKey<java.math.BigDecimal> MAN_HT = new ERXKey<java.math.BigDecimal>("manHt");
	public static final ERXKey<String> MAN_MOTIF_REJET = new ERXKey<String>("manMotifRejet");
	public static final ERXKey<Integer> MAN_NB_PIECE = new ERXKey<Integer>("manNbPiece");
	public static final ERXKey<Integer> MAN_NUMERO = new ERXKey<Integer>("manNumero");
	public static final ERXKey<Integer> MAN_NUMERO_REJET = new ERXKey<Integer>("manNumeroRejet");
	public static final ERXKey<Integer> MAN_ORDRE = new ERXKey<Integer>("manOrdre");
	public static final ERXKey<Integer> MAN_ORIGINE_KEY = new ERXKey<Integer>("manOrigineKey");
	public static final ERXKey<String> MAN_ORIGINE_LIB = new ERXKey<String>("manOrigineLib");
	public static final ERXKey<java.math.BigDecimal> MAN_TTC = new ERXKey<java.math.BigDecimal>("manTtc");
	public static final ERXKey<java.math.BigDecimal> MAN_TVA = new ERXKey<java.math.BigDecimal>("manTva");
	public static final ERXKey<Integer> PREST_ID = new ERXKey<Integer>("prestId");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereau> TO_BORDEREAU = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereau>("toBordereau");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereauRejet> TO_BORDEREAU_REJET = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereauRejet>("toBordereauRejet");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EODepense> TO_DEPENSES = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EODepense>("toDepenses");
	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> TO_FOURNIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>("toFournis");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestion> TO_GESTION = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestion>("toGestion");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard> TO_MANDAT_BROUILLARDS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard>("toMandatBrouillards");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOModePaiement> TO_MODE_PAIEMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOModePaiement>("toModePaiement");
	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan> TO_ORGAN = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan>("toOrgan");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOOrigine> TO_ORIGINE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOOrigine>("toOrigine");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPaiement> TO_PAIEMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPaiement>("toPaiement");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> TO_PLAN_COMPTABLE_EXER = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer>("toPlanComptableExer");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORib> TO_RIB = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORib>("toRib");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORib> TO_RIB_ORDONNATEUR = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORib>("toRibOrdonnateur");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau> TO_TYPE_ORIGINE_BORDEREAU = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau>("toTypeOrigineBordereau");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "manId";

	public static final String MAN_ATTENTE_DATE_KEY = "manAttenteDate";
	public static final String MAN_ATTENTE_OBJET_KEY = "manAttenteObjet";
	public static final String MAN_DATE_REMISE_KEY = "manDateRemise";
	public static final String MAN_DATE_VISA_PRINC_KEY = "manDateVisaPrinc";
	public static final String MAN_ETAT_KEY = "manEtat";
	public static final String MAN_ETAT_REMISE_KEY = "manEtatRemise";
	public static final String MAN_HT_KEY = "manHt";
	public static final String MAN_MOTIF_REJET_KEY = "manMotifRejet";
	public static final String MAN_NB_PIECE_KEY = "manNbPiece";
	public static final String MAN_NUMERO_KEY = "manNumero";
	public static final String MAN_NUMERO_REJET_KEY = "manNumeroRejet";
	public static final String MAN_ORDRE_KEY = "manOrdre";
	public static final String MAN_ORIGINE_KEY_KEY = "manOrigineKey";
	public static final String MAN_ORIGINE_LIB_KEY = "manOrigineLib";
	public static final String MAN_TTC_KEY = "manTtc";
	public static final String MAN_TVA_KEY = "manTva";
	public static final String PREST_ID_KEY = "prestId";

//Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String BRJ_ORDRE_KEY = "brjOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MAN_ATTENTE_PAIEMENT_KEY = "manAttentePaiement";
	public static final String MAN_ID_KEY = "manId";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String PAI_ORDRE_KEY = "paiOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String RIB_ORDRE_COMPTABLE_KEY = "ribOrdreComptable";
	public static final String RIB_ORDRE_ORDONNATEUR_KEY = "ribOrdreOrdonnateur";
	public static final String TOR_ORDRE_KEY = "torOrdre";
	public static final String UTL_ORDRE_ATTENTE_KEY = "utlOrdreAttente";

//Colonnes dans la base de donnees
	public static final String MAN_ATTENTE_DATE_COLKEY = "man_attente_date";
	public static final String MAN_ATTENTE_OBJET_COLKEY = "man_attente_objet";
	public static final String MAN_DATE_REMISE_COLKEY = "man_Date_Remise";
	public static final String MAN_DATE_VISA_PRINC_COLKEY = "man_Date_Visa_Princ";
	public static final String MAN_ETAT_COLKEY = "man_etat";
	public static final String MAN_ETAT_REMISE_COLKEY = "man_Etat_Remise";
	public static final String MAN_HT_COLKEY = "man_Ht";
	public static final String MAN_MOTIF_REJET_COLKEY = "man_Motif_Rejet";
	public static final String MAN_NB_PIECE_COLKEY = "man_NB_PIECE";
	public static final String MAN_NUMERO_COLKEY = "man_Numero";
	public static final String MAN_NUMERO_REJET_COLKEY = "man_Numero_rejet";
	public static final String MAN_ORDRE_COLKEY = "MAN_ORDRE";
	public static final String MAN_ORIGINE_KEY_COLKEY = "man_orgine_key";
	public static final String MAN_ORIGINE_LIB_COLKEY = "man_Origine_Lib";
	public static final String MAN_TTC_COLKEY = "man_Ttc";
	public static final String MAN_TVA_COLKEY = "man_Tva";
	public static final String PREST_ID_COLKEY = "prest_id";

	public static final String BOR_ID_COLKEY = "BOR_id";
	public static final String BRJ_ORDRE_COLKEY = "BRJ_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "fou_Ordre";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String MAN_ATTENTE_PAIEMENT_COLKEY = "man_attente_paiement";
	public static final String MAN_ID_COLKEY = "man_id";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String ORG_ORDRE_COLKEY = "org_ordre";
	public static final String ORI_ORDRE_COLKEY = "ori_ordre";
	public static final String PAI_ORDRE_COLKEY = "pai_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String RIB_ORDRE_COMPTABLE_COLKEY = "rib_ordre_comptable";
	public static final String RIB_ORDRE_ORDONNATEUR_COLKEY = "rib_ordre_ordonnateur";
	public static final String TOR_ORDRE_COLKEY = "tor_Ordre";
	public static final String UTL_ORDRE_ATTENTE_COLKEY = "utl_ordre_attente";


	// Relationships
	public static final String TO_BORDEREAU_KEY = "toBordereau";
	public static final String TO_BORDEREAU_REJET_KEY = "toBordereauRejet";
	public static final String TO_DEPENSES_KEY = "toDepenses";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_FOURNIS_KEY = "toFournis";
	public static final String TO_GESTION_KEY = "toGestion";
	public static final String TO_MANDAT_BROUILLARDS_KEY = "toMandatBrouillards";
	public static final String TO_MODE_PAIEMENT_KEY = "toModePaiement";
	public static final String TO_ORGAN_KEY = "toOrgan";
	public static final String TO_ORIGINE_KEY = "toOrigine";
	public static final String TO_PAIEMENT_KEY = "toPaiement";
	public static final String TO_PLAN_COMPTABLE_EXER_KEY = "toPlanComptableExer";
	public static final String TO_RIB_KEY = "toRib";
	public static final String TO_RIB_ORDONNATEUR_KEY = "toRibOrdonnateur";
	public static final String TO_TYPE_ORIGINE_BORDEREAU_KEY = "toTypeOrigineBordereau";



	// Accessors methods
	public NSTimestamp manAttenteDate() {
	 return (NSTimestamp) storedValueForKey(MAN_ATTENTE_DATE_KEY);
	}

	public void setManAttenteDate(NSTimestamp value) {
	 takeStoredValueForKey(value, MAN_ATTENTE_DATE_KEY);
	}

	public String manAttenteObjet() {
	 return (String) storedValueForKey(MAN_ATTENTE_OBJET_KEY);
	}

	public void setManAttenteObjet(String value) {
	 takeStoredValueForKey(value, MAN_ATTENTE_OBJET_KEY);
	}

	public NSTimestamp manDateRemise() {
	 return (NSTimestamp) storedValueForKey(MAN_DATE_REMISE_KEY);
	}

	public void setManDateRemise(NSTimestamp value) {
	 takeStoredValueForKey(value, MAN_DATE_REMISE_KEY);
	}

	public NSTimestamp manDateVisaPrinc() {
	 return (NSTimestamp) storedValueForKey(MAN_DATE_VISA_PRINC_KEY);
	}

	public void setManDateVisaPrinc(NSTimestamp value) {
	 takeStoredValueForKey(value, MAN_DATE_VISA_PRINC_KEY);
	}

	public String manEtat() {
	 return (String) storedValueForKey(MAN_ETAT_KEY);
	}

	public void setManEtat(String value) {
	 takeStoredValueForKey(value, MAN_ETAT_KEY);
	}

	public String manEtatRemise() {
	 return (String) storedValueForKey(MAN_ETAT_REMISE_KEY);
	}

	public void setManEtatRemise(String value) {
	 takeStoredValueForKey(value, MAN_ETAT_REMISE_KEY);
	}

	public java.math.BigDecimal manHt() {
	 return (java.math.BigDecimal) storedValueForKey(MAN_HT_KEY);
	}

	public void setManHt(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MAN_HT_KEY);
	}

	public String manMotifRejet() {
	 return (String) storedValueForKey(MAN_MOTIF_REJET_KEY);
	}

	public void setManMotifRejet(String value) {
	 takeStoredValueForKey(value, MAN_MOTIF_REJET_KEY);
	}

	public Integer manNbPiece() {
	 return (Integer) storedValueForKey(MAN_NB_PIECE_KEY);
	}

	public void setManNbPiece(Integer value) {
	 takeStoredValueForKey(value, MAN_NB_PIECE_KEY);
	}

	public Integer manNumero() {
	 return (Integer) storedValueForKey(MAN_NUMERO_KEY);
	}

	public void setManNumero(Integer value) {
	 takeStoredValueForKey(value, MAN_NUMERO_KEY);
	}

	public Integer manNumeroRejet() {
	 return (Integer) storedValueForKey(MAN_NUMERO_REJET_KEY);
	}

	public void setManNumeroRejet(Integer value) {
	 takeStoredValueForKey(value, MAN_NUMERO_REJET_KEY);
	}

	public Integer manOrdre() {
	 return (Integer) storedValueForKey(MAN_ORDRE_KEY);
	}

	public void setManOrdre(Integer value) {
	 takeStoredValueForKey(value, MAN_ORDRE_KEY);
	}

	public Integer manOrigineKey() {
	 return (Integer) storedValueForKey(MAN_ORIGINE_KEY_KEY);
	}

	public void setManOrigineKey(Integer value) {
	 takeStoredValueForKey(value, MAN_ORIGINE_KEY_KEY);
	}

	public String manOrigineLib() {
	 return (String) storedValueForKey(MAN_ORIGINE_LIB_KEY);
	}

	public void setManOrigineLib(String value) {
	 takeStoredValueForKey(value, MAN_ORIGINE_LIB_KEY);
	}

	public java.math.BigDecimal manTtc() {
	 return (java.math.BigDecimal) storedValueForKey(MAN_TTC_KEY);
	}

	public void setManTtc(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MAN_TTC_KEY);
	}

	public java.math.BigDecimal manTva() {
	 return (java.math.BigDecimal) storedValueForKey(MAN_TVA_KEY);
	}

	public void setManTva(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MAN_TVA_KEY);
	}

	public Integer prestId() {
	 return (Integer) storedValueForKey(PREST_ID_KEY);
	}

	public void setPrestId(Integer value) {
	 takeStoredValueForKey(value, PREST_ID_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOBordereau toBordereau() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBordereau)storedValueForKey(TO_BORDEREAU_KEY);
	}

	public void setToBordereauRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereau value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOBordereau oldValue = toBordereau();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BORDEREAU_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_BORDEREAU_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOBordereauRejet toBordereauRejet() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBordereauRejet)storedValueForKey(TO_BORDEREAU_REJET_KEY);
	}

	public void setToBordereauRejetRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereauRejet value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOBordereauRejet oldValue = toBordereauRejet();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BORDEREAU_REJET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_BORDEREAU_REJET_KEY);
	 }
	}

	public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
	 return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFournis() {
	 return (org.cocktail.fwkcktlpersonne.common.metier.EOFournis)storedValueForKey(TO_FOURNIS_KEY);
	}

	public void setToFournisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlpersonne.common.metier.EOFournis oldValue = toFournis();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FOURNIS_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_FOURNIS_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGestion toGestion() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
	}

	public void setToGestionRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGestion value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGestion oldValue = toGestion();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOModePaiement toModePaiement() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOModePaiement)storedValueForKey(TO_MODE_PAIEMENT_KEY);
	}

	public void setToModePaiementRelationship(org.cocktail.fwkcktlcompta.server.metier.EOModePaiement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOModePaiement oldValue = toModePaiement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODE_PAIEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODE_PAIEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan toOrgan() {
	 return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(TO_ORGAN_KEY);
	}

	public void setToOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
	 if (value == null) {
	 	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = toOrgan();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORGAN_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORGAN_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOOrigine toOrigine() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOOrigine)storedValueForKey(TO_ORIGINE_KEY);
	}

	public void setToOrigineRelationship(org.cocktail.fwkcktlcompta.server.metier.EOOrigine value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOOrigine oldValue = toOrigine();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORIGINE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORIGINE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOPaiement toPaiement() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOPaiement)storedValueForKey(TO_PAIEMENT_KEY);
	}

	public void setToPaiementRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPaiement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOPaiement oldValue = toPaiement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAIEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAIEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer toPlanComptableExer() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer)storedValueForKey(TO_PLAN_COMPTABLE_EXER_KEY);
	}

	public void setToPlanComptableExerRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer oldValue = toPlanComptableExer();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_EXER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_EXER_KEY);
	 }
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EORib toRib() {
	 return (org.cocktail.fwkcktlpersonne.common.metier.EORib)storedValueForKey(TO_RIB_KEY);
	}

	public void setToRibRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORib value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlpersonne.common.metier.EORib oldValue = toRib();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RIB_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_RIB_KEY);
	 }
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EORib toRibOrdonnateur() {
	 return (org.cocktail.fwkcktlpersonne.common.metier.EORib)storedValueForKey(TO_RIB_ORDONNATEUR_KEY);
	}

	public void setToRibOrdonnateurRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORib value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlpersonne.common.metier.EORib oldValue = toRibOrdonnateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RIB_ORDONNATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_RIB_ORDONNATEUR_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau toTypeOrigineBordereau() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau)storedValueForKey(TO_TYPE_ORIGINE_BORDEREAU_KEY);
	}

	public void setToTypeOrigineBordereauRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau oldValue = toTypeOrigineBordereau();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ORIGINE_BORDEREAU_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ORIGINE_BORDEREAU_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EODepense> toDepenses() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EODepense>)storedValueForKey(TO_DEPENSES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EODepense> toDepenses(EOQualifier qualifier) {
	 return toDepenses(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EODepense> toDepenses(EOQualifier qualifier, Boolean fetch) {
	 return toDepenses(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EODepense> toDepenses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EODepense> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EODepense.TO_MANDAT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EODepense.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toDepenses();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EODepense>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EODepense>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToDepensesRelationship(org.cocktail.fwkcktlcompta.server.metier.EODepense object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_DEPENSES_KEY);
	}
	
	public void removeFromToDepensesRelationship(org.cocktail.fwkcktlcompta.server.metier.EODepense object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DEPENSES_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EODepense createToDepensesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EODepense.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_DEPENSES_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EODepense) eo;
	}
	
	public void deleteToDepensesRelationship(org.cocktail.fwkcktlcompta.server.metier.EODepense object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DEPENSES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToDepensesRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EODepense> objects = toDepenses().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToDepensesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard> toMandatBrouillards() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard>)storedValueForKey(TO_MANDAT_BROUILLARDS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard> toMandatBrouillards(EOQualifier qualifier) {
	 return toMandatBrouillards(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard> toMandatBrouillards(EOQualifier qualifier, Boolean fetch) {
	 return toMandatBrouillards(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard> toMandatBrouillards(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard.TO_MANDAT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toMandatBrouillards();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToMandatBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_MANDAT_BROUILLARDS_KEY);
	}
	
	public void removeFromToMandatBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MANDAT_BROUILLARDS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard createToMandatBrouillardsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_MANDAT_BROUILLARDS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard) eo;
	}
	
	public void deleteToMandatBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MANDAT_BROUILLARDS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToMandatBrouillardsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOMandatBrouillard> objects = toMandatBrouillards().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToMandatBrouillardsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOMandat avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOMandat createEOMandat(EOEditingContext editingContext												, String manEtat
									, java.math.BigDecimal manHt
											, Integer manNumero
									, Integer manOrdre
											, java.math.BigDecimal manTtc
							, java.math.BigDecimal manTva
							, org.cocktail.fwkcktlcompta.server.metier.EOBordereau toBordereau				, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice		, org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFournis		, org.cocktail.fwkcktlcompta.server.metier.EOGestion toGestion		, org.cocktail.fwkcktlcompta.server.metier.EOModePaiement toModePaiement								, org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer toPlanComptableExer						, org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau toTypeOrigineBordereau					) {
	 EOMandat eo = (EOMandat) EOUtilities.createAndInsertInstance(editingContext, _EOMandat.ENTITY_NAME);	 
															eo.setManEtat(manEtat);
											eo.setManHt(manHt);
													eo.setManNumero(manNumero);
											eo.setManOrdre(manOrdre);
													eo.setManTtc(manTtc);
									eo.setManTva(manTva);
								 eo.setToBordereauRelationship(toBordereau);
						 eo.setToExerciceRelationship(toExercice);
				 eo.setToFournisRelationship(toFournis);
				 eo.setToGestionRelationship(toGestion);
				 eo.setToModePaiementRelationship(toModePaiement);
										 eo.setToPlanComptableExerRelationship(toPlanComptableExer);
								 eo.setToTypeOrigineBordereauRelationship(toTypeOrigineBordereau);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMandat creerInstance(EOEditingContext editingContext) {
		EOMandat object = (EOMandat)EOUtilities.createAndInsertInstance(editingContext, _EOMandat.ENTITY_NAME);
  		return object;
		}

	

  public EOMandat localInstanceIn(EOEditingContext editingContext) {
    EOMandat localInstance = (EOMandat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOMandat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMandat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOMandat> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMandat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMandat)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMandat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMandat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOMandat> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMandat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMandat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOMandat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMandat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMandat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMandat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}