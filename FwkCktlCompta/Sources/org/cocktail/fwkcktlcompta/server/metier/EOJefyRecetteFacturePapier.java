/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.server.metier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IModeRecouvrement;
import org.cocktail.fwkcktlcompta.common.entities.ITitreDetailEcriture;
import org.cocktail.fwkcktlcompta.common.helpers.TitreDetailEcritureHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.util.WebObjectConversionUtil;

import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.foundation.NSValidation;

public class EOJefyRecetteFacturePapier extends _EOJefyRecetteFacturePapier implements ISepaSddOrigineEntity {

	public EOJefyRecetteFacturePapier() {
		super();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	public Integer origineExercice() {
		return toExercice().exeExercice();
	}

	public String origineDebiteurNomComplet() {
		return toPersonne().getNomAndPrenomAndCode();
	}

	public Integer origineIdentifiant() {
		final EOKeyGlobalID gid = (EOKeyGlobalID) editingContext().globalIDForObject(this);
		return (Integer) gid.keyValuesArray().objectAtIndex(0);
	}

	public String origineNumero() {
		return fapNumero().toString();
	}

	public String origineLibelle() {
		return fapLib();
	}

	public BigDecimal origineMontant() {
		return fapTotalTtc();
	}


	public IGrhumPersonne origineDebiteurPersonne() {
		return toPersonne();
	}

	public IModeRecouvrement modeRecouvrement() {
		return toModeRecouvrement();
	}

	public void onEcheanceRejetee(ISepaSddEcheance echeance) {

	}

	public void onEcheancePrelevee(ISepaSddEcheance echeance) {

	}

	public String getOrigineLibelleComplet() {
		return "Facture n° " + origineExercice() + "-" + origineNumero() + " / " + fapLib() + " / Montant : " + origineMontant();
	}

	public String getOrigineLibellePourDebiteur() {
		return "Facture n° " + origineExercice() + "-" + origineNumero() + "/" + fapLib();
	}

	public List<EORecette> getMaracujaRecettes() {
		List<EORecette> maracujaRecettes = new ArrayList<EORecette>();
		List<EOJefyRecetteRecetteCtrlPlanco> recetteCtrlPlancos = new ArrayList<EOJefyRecetteRecetteCtrlPlanco>();
		if (toFacture() != null) {
			List<EOJefyRecetteRecette> recettes = WebObjectConversionUtil.asList(toFacture().toRecettes());
			for (EOJefyRecetteRecette eoJefyRecetteRecette : recettes) {
				recetteCtrlPlancos.addAll(eoJefyRecetteRecette.toRecetteCtrlPlancos());
			}
			for (EOJefyRecetteRecetteCtrlPlanco eoJefyRecetteRecetteCtrlPlanco : recetteCtrlPlancos) {
				for (int i = 0; i < eoJefyRecetteRecetteCtrlPlanco.toComptaRecettes().count(); i++) {
					EORecette maracujaRecette = (EORecette) eoJefyRecetteRecetteCtrlPlanco.toComptaRecettes().objectAtIndex(i);
					if (!maracujaRecettes.contains(maracujaRecette)) {
						maracujaRecettes.add(maracujaRecette);
					}
				}
			}
		}

		return maracujaRecettes;
	}

	public List<IEcritureDetail> ecritureDetailsContrepartiePriseEnCharge() {
		List<IEcritureDetail> ecds = new ArrayList<IEcritureDetail>();
		List<EORecette> maracujaRecettes = getMaracujaRecettes();
		List<ITitreDetailEcriture> tde = new ArrayList<ITitreDetailEcriture>();
		for (EORecette eoRecette : maracujaRecettes) {
			tde.addAll(eoRecette.toTitreDetailEcritures());
		}
		tde = TitreDetailEcritureHelper.getSharedInstance().filtrerEcrituresVisaContrepartie(tde);

		for (ITitreDetailEcriture eoTitreDetailEcriture : tde) {
			if (!ecds.contains(eoTitreDetailEcriture.toEcritureDetail())) {
				ecds.add(eoTitreDetailEcriture.toEcritureDetail());
			}
		}
		return ecds;
	}
}
