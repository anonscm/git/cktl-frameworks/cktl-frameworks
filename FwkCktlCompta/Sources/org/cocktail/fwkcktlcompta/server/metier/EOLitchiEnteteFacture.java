/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.server.metier;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IModeRecouvrement;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.joda.time.DateTime;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;


public class EOLitchiEnteteFacture extends _EOLitchiEnteteFacture implements ISepaSddOrigineEntity {

	private static final Long RESTAURATION_TYPE_ID = Long.valueOf(2);
	private static final Long HEBERGEMENT_TYPE_ID = Long.valueOf(1);
	private static final String RESTAURATION_LIB = "RESTAURATION";
	private static final String HEBERGEMENT_LIB = "HEBERGEMENT";

	private static EOGlobalID mrRestaurationGlobalId;
	private static EOGlobalID mrHebergementGlobalId;

	/**
	 * A initialiser par l'application
	 */
	public static String RESTAURATION_MODE_RECOUVREMENT_CODE = null;
	public static String HEBERGEMENT_MODE_RECOUVREMENT_CODE = null;

	public EOLitchiEnteteFacture() {
		super();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
	}

	public Integer origineExercice() {
		DateTime dateTimeCreation = new DateTime(dateCreation().getTime());
		return dateTimeCreation.getYear();
	}

	public String origineDebiteurNomComplet() {
		return toPersonne().getNomAndPrenomAndCode();
	}

	public Integer origineIdentifiant() {
		final EOKeyGlobalID gid = (EOKeyGlobalID) editingContext().globalIDForObject(this);
		return (Integer) gid.keyValuesArray().objectAtIndex(0);
	}

	public String origineNumero() {
		return numFacture();
	}

	public String origineLibelle() {
		return libFacture();
	}

	public BigDecimal origineMontant() {
		return mntAPayerTtc();
	}

	public String getOrigineLibelleComplet() {
		return "Facture n° " + origineExercice() + "-" + origineNumero() + " / " + origineLibelle() + " / Montant : " + origineMontant();
	}

	public String getOrigineLibellePourDebiteur() {
		return "Facture n° " + origineExercice() + "-" + origineNumero() + "/" + origineLibelle();
	}

	public IGrhumPersonne origineDebiteurPersonne() {
		return toPersonne();
	}

	public IModeRecouvrement modeRecouvrement() {
		if (isTypeFactureRestauration()) {
			return modeRecouvrementRestauration();
		}
		else if (isTypeFactureHebergement()) {
			return modeRecouvrementHebergement();
		}
		return null;
	}

	public void onEcheanceRejetee(ISepaSddEcheance echeance) {
	}

	public void onEcheancePrelevee(ISepaSddEcheance echeance) {
	}

	public List<IEcritureDetail> ecritureDetailsContrepartiePriseEnCharge() {
		return null;
	}

	public String typeFactureLibelle() {
		if (RESTAURATION_TYPE_ID.longValue() == idTypeFacture().longValue()) {
			return RESTAURATION_LIB;
		}
		if (HEBERGEMENT_TYPE_ID.longValue() == idTypeFacture().longValue()) {
			return HEBERGEMENT_LIB;
		}
		return null;
	}

	public Boolean isTypeFactureRestauration() {
		return RESTAURATION_LIB.equals(typeFactureLibelle());
	}

	public Boolean isTypeFactureHebergement() {
		return HEBERGEMENT_LIB.equals(typeFactureLibelle());
	}

	public EOModeRecouvrement modeRecouvrementHebergement() {
		if (mrHebergementGlobalId == null) {
			String code = HEBERGEMENT_MODE_RECOUVREMENT_CODE;
			if (code == null) {
				throw new RuntimeException("@developper : Champ statique HEBERGEMENT_MODE_RECOUVREMENT_CODE non renseigné. Vous devez le renseigner lors de l'initialisation de l'application.");
			}
			EOExercice exercice = EOExercice.getExerciceOuvert(editingContext());
			EOQualifier qual1 = new EOKeyValueQualifier(EOModeRecouvrement.MOD_CODE_KEY, EOQualifier.QualifierOperatorEqual, code);
			EOQualifier qual2 = new EOKeyValueQualifier(EOModeRecouvrement.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
			EOModeRecouvrement tmp = EOModeRecouvrement.fetchByQualifier(editingContext(), new EOAndQualifier(new NSArray(new Object[]{qual1, qual2})));
			if (tmp == null) {
				throw new RuntimeException("Impossible de trouver le mode de recouvrement " + code + " pour l'exercice " + exercice.exeExercice());
			}
			mrHebergementGlobalId = editingContext().globalIDForObject(tmp);
		}

		if (mrHebergementGlobalId != null) {
			return (EOModeRecouvrement) editingContext().faultForGlobalID(mrHebergementGlobalId, editingContext());
		}
		return null;

	}

	public EOModeRecouvrement modeRecouvrementRestauration() {
		if (mrRestaurationGlobalId == null) {
			String code = RESTAURATION_MODE_RECOUVREMENT_CODE;
			if (code == null) {
				throw new RuntimeException("@developper : Champ statique RESTAURATION_MODE_RECOUVREMENT_CODE non renseigné. Vous devez le renseigner lors de l'initialisation de l'application.");
			}
			EOExercice exercice = EOExercice.getExerciceOuvert(editingContext());
			EOQualifier qual1 = new EOKeyValueQualifier(EOModeRecouvrement.MOD_CODE_KEY, EOQualifier.QualifierOperatorEqual, code);
			EOQualifier qual2 = new EOKeyValueQualifier(EOModeRecouvrement.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
			EOModeRecouvrement tmp = EOModeRecouvrement.fetchByQualifier(editingContext(), new EOAndQualifier(new NSArray(new Object[] {
					qual1, qual2
			})));
			if (tmp == null) {
				throw new RuntimeException("Impossible de trouver le mode de recouvrement " + code + " pour l'exercice " + exercice.exeExercice());
			}
			mrRestaurationGlobalId = editingContext().globalIDForObject(tmp);
		}

		if (mrRestaurationGlobalId != null) {
			return (EOModeRecouvrement) editingContext().faultForGlobalID(mrRestaurationGlobalId, editingContext());
		}
		return null;
	}

}
