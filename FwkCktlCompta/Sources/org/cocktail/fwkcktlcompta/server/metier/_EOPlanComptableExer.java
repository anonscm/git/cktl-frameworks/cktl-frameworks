/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPlanComptableExer.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOPlanComptableExer extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_PlanComptableExer";
	public static final String ENTITY_TABLE_NAME = "maracuja.Plan_Comptable_exer";


//Attribute Keys
	public static final ERXKey<String> PCO_BUDGETAIRE = new ERXKey<String>("pcoBudgetaire");
	public static final ERXKey<String> PCO_COMPTE_BE = new ERXKey<String>("pcoCompteBe");
	public static final ERXKey<String> PCO_EMARGEMENT = new ERXKey<String>("pcoEmargement");
	public static final ERXKey<String> PCO_J_BE = new ERXKey<String>("pcoJBe");
	public static final ERXKey<String> PCO_J_EXERCICE = new ERXKey<String>("pcoJExercice");
	public static final ERXKey<String> PCO_J_FIN_EXERCICE = new ERXKey<String>("pcoJFinExercice");
	public static final ERXKey<String> PCO_LIBELLE = new ERXKey<String>("pcoLibelle");
	public static final ERXKey<String> PCO_NATURE = new ERXKey<String>("pcoNature");
	public static final ERXKey<Integer> PCO_NIVEAU = new ERXKey<Integer>("pcoNiveau");
	public static final ERXKey<String> PCO_NUM = new ERXKey<String>("pcoNum");
	public static final ERXKey<String> PCO_SENS_SOLDE = new ERXKey<String>("pcoSensSolde");
	public static final ERXKey<String> PCO_VALIDITE = new ERXKey<String>("pcoValidite");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement> TO_PLANCO_AMORTISSEMENTS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement>("toPlancoAmortissements");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit> TO_PLANCO_CREDITS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit>("toPlancoCredits");

	// Attributes


	public static final String PCO_BUDGETAIRE_KEY = "pcoBudgetaire";
	public static final String PCO_COMPTE_BE_KEY = "pcoCompteBe";
	public static final String PCO_EMARGEMENT_KEY = "pcoEmargement";
	public static final String PCO_J_BE_KEY = "pcoJBe";
	public static final String PCO_J_EXERCICE_KEY = "pcoJExercice";
	public static final String PCO_J_FIN_EXERCICE_KEY = "pcoJFinExercice";
	public static final String PCO_LIBELLE_KEY = "pcoLibelle";
	public static final String PCO_NATURE_KEY = "pcoNature";
	public static final String PCO_NIVEAU_KEY = "pcoNiveau";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PCO_SENS_SOLDE_KEY = "pcoSensSolde";
	public static final String PCO_VALIDITE_KEY = "pcoValidite";

//Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PCOE_ID_KEY = "pcoeId";

//Colonnes dans la base de donnees
	public static final String PCO_BUDGETAIRE_COLKEY = "pco_Budgetaire";
	public static final String PCO_COMPTE_BE_COLKEY = "PCO_COMPTE_BE";
	public static final String PCO_EMARGEMENT_COLKEY = "pco_Emargement";
	public static final String PCO_J_BE_COLKEY = "pco_j_be";
	public static final String PCO_J_EXERCICE_COLKEY = "pco_j_exercice";
	public static final String PCO_J_FIN_EXERCICE_COLKEY = "pco_j_fin_exercice";
	public static final String PCO_LIBELLE_COLKEY = "pco_libelle";
	public static final String PCO_NATURE_COLKEY = "pco_nature";
	public static final String PCO_NIVEAU_COLKEY = "pco_Niveau";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PCO_SENS_SOLDE_COLKEY = "pco_Sens_solde";
	public static final String PCO_VALIDITE_COLKEY = "pco_Validite";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PCOE_ID_COLKEY = "pcoe_id";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_PLANCO_AMORTISSEMENTS_KEY = "toPlancoAmortissements";
	public static final String TO_PLANCO_CREDITS_KEY = "toPlancoCredits";



	// Accessors methods
	public String pcoBudgetaire() {
	 return (String) storedValueForKey(PCO_BUDGETAIRE_KEY);
	}

	public void setPcoBudgetaire(String value) {
	 takeStoredValueForKey(value, PCO_BUDGETAIRE_KEY);
	}

	public String pcoCompteBe() {
	 return (String) storedValueForKey(PCO_COMPTE_BE_KEY);
	}

	public void setPcoCompteBe(String value) {
	 takeStoredValueForKey(value, PCO_COMPTE_BE_KEY);
	}

	public String pcoEmargement() {
	 return (String) storedValueForKey(PCO_EMARGEMENT_KEY);
	}

	public void setPcoEmargement(String value) {
	 takeStoredValueForKey(value, PCO_EMARGEMENT_KEY);
	}

	public String pcoJBe() {
	 return (String) storedValueForKey(PCO_J_BE_KEY);
	}

	public void setPcoJBe(String value) {
	 takeStoredValueForKey(value, PCO_J_BE_KEY);
	}

	public String pcoJExercice() {
	 return (String) storedValueForKey(PCO_J_EXERCICE_KEY);
	}

	public void setPcoJExercice(String value) {
	 takeStoredValueForKey(value, PCO_J_EXERCICE_KEY);
	}

	public String pcoJFinExercice() {
	 return (String) storedValueForKey(PCO_J_FIN_EXERCICE_KEY);
	}

	public void setPcoJFinExercice(String value) {
	 takeStoredValueForKey(value, PCO_J_FIN_EXERCICE_KEY);
	}

	public String pcoLibelle() {
	 return (String) storedValueForKey(PCO_LIBELLE_KEY);
	}

	public void setPcoLibelle(String value) {
	 takeStoredValueForKey(value, PCO_LIBELLE_KEY);
	}

	public String pcoNature() {
	 return (String) storedValueForKey(PCO_NATURE_KEY);
	}

	public void setPcoNature(String value) {
	 takeStoredValueForKey(value, PCO_NATURE_KEY);
	}

	public Integer pcoNiveau() {
	 return (Integer) storedValueForKey(PCO_NIVEAU_KEY);
	}

	public void setPcoNiveau(Integer value) {
	 takeStoredValueForKey(value, PCO_NIVEAU_KEY);
	}

	public String pcoNum() {
	 return (String) storedValueForKey(PCO_NUM_KEY);
	}

	public void setPcoNum(String value) {
	 takeStoredValueForKey(value, PCO_NUM_KEY);
	}

	public String pcoSensSolde() {
	 return (String) storedValueForKey(PCO_SENS_SOLDE_KEY);
	}

	public void setPcoSensSolde(String value) {
	 takeStoredValueForKey(value, PCO_SENS_SOLDE_KEY);
	}

	public String pcoValidite() {
	 return (String) storedValueForKey(PCO_VALIDITE_KEY);
	}

	public void setPcoValidite(String value) {
	 takeStoredValueForKey(value, PCO_VALIDITE_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement> toPlancoAmortissements() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement>)storedValueForKey(TO_PLANCO_AMORTISSEMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement> toPlancoAmortissements(EOQualifier qualifier) {
	 return toPlancoAmortissements(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement> toPlancoAmortissements(EOQualifier qualifier, Boolean fetch) {
	 return toPlancoAmortissements(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement> toPlancoAmortissements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement.TO_PLAN_COMPTABLE_EXER_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toPlancoAmortissements();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToPlancoAmortissementsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_PLANCO_AMORTISSEMENTS_KEY);
	}
	
	public void removeFromToPlancoAmortissementsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PLANCO_AMORTISSEMENTS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement createToPlancoAmortissementsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_PLANCO_AMORTISSEMENTS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement) eo;
	}
	
	public void deleteToPlancoAmortissementsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PLANCO_AMORTISSEMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToPlancoAmortissementsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOPlancoAmortissement> objects = toPlancoAmortissements().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToPlancoAmortissementsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit> toPlancoCredits() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit>)storedValueForKey(TO_PLANCO_CREDITS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit> toPlancoCredits(EOQualifier qualifier) {
	 return toPlancoCredits(qualifier, null);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit> toPlancoCredits(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit> results;
			   results = toPlancoCredits();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 return results;
	}
	
	public void addToToPlancoCreditsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_PLANCO_CREDITS_KEY);
	}
	
	public void removeFromToPlancoCreditsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PLANCO_CREDITS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit createToPlancoCreditsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_PLANCO_CREDITS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit) eo;
	}
	
	public void deleteToPlancoCreditsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PLANCO_CREDITS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToPlancoCreditsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOPlancoCredit> objects = toPlancoCredits().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToPlancoCreditsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOPlanComptableExer avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOPlanComptableExer createEOPlanComptableExer(EOEditingContext editingContext				, String pcoBudgetaire
									, String pcoEmargement
							, String pcoJBe
							, String pcoJExercice
							, String pcoJFinExercice
							, String pcoLibelle
							, String pcoNature
							, Integer pcoNiveau
							, String pcoNum
							, String pcoSensSolde
							, String pcoValidite
					, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice					) {
	 EOPlanComptableExer eo = (EOPlanComptableExer) EOUtilities.createAndInsertInstance(editingContext, _EOPlanComptableExer.ENTITY_NAME);	 
							eo.setPcoBudgetaire(pcoBudgetaire);
											eo.setPcoEmargement(pcoEmargement);
									eo.setPcoJBe(pcoJBe);
									eo.setPcoJExercice(pcoJExercice);
									eo.setPcoJFinExercice(pcoJFinExercice);
									eo.setPcoLibelle(pcoLibelle);
									eo.setPcoNature(pcoNature);
									eo.setPcoNiveau(pcoNiveau);
									eo.setPcoNum(pcoNum);
									eo.setPcoSensSolde(pcoSensSolde);
									eo.setPcoValidite(pcoValidite);
						 eo.setToExerciceRelationship(toExercice);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPlanComptableExer creerInstance(EOEditingContext editingContext) {
		EOPlanComptableExer object = (EOPlanComptableExer)EOUtilities.createAndInsertInstance(editingContext, _EOPlanComptableExer.ENTITY_NAME);
  		return object;
		}

	

  public EOPlanComptableExer localInstanceIn(EOEditingContext editingContext) {
    EOPlanComptableExer localInstance = (EOPlanComptableExer)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPlanComptableExer fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPlanComptableExer fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPlanComptableExer> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPlanComptableExer eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPlanComptableExer)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPlanComptableExer fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPlanComptableExer fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPlanComptableExer> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPlanComptableExer eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPlanComptableExer)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPlanComptableExer fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPlanComptableExer eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPlanComptableExer ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPlanComptableExer fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}