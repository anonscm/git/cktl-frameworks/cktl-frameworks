/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBordereau.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOBordereau extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Bordereau";
	public static final String ENTITY_TABLE_NAME = "maracuja.Bordereau";


//Attribute Keys
	public static final ERXKey<NSTimestamp> BOR_DATE_CREATION = new ERXKey<NSTimestamp>("borDateCreation");
	public static final ERXKey<NSTimestamp> BOR_DATE_VISA = new ERXKey<NSTimestamp>("borDateVisa");
	public static final ERXKey<String> BOR_ETAT = new ERXKey<String>("borEtat");
	public static final ERXKey<Integer> BOR_NUM = new ERXKey<Integer>("borNum");
	public static final ERXKey<Integer> BOR_ORDRE = new ERXKey<Integer>("borOrdre");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> TO_BORDEREAU_BROUILLARDS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard>("toBordereauBrouillards");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> TO_BORDEREAU_INFOS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo>("toBordereauInfos");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> TO_BROUILLARDS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard>("toBrouillards");
	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestion> TO_GESTION = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestion>("toGestion");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOMandat> TO_MANDATS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOMandat>("toMandats");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTitre> TO_TITRES = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTitre>("toTitres");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeBordereau> TO_TYPE_BORDEREAU = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeBordereau>("toTypeBordereau");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> TO_UTILISATEUR = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("toUtilisateur");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> TO_UTILISATEUR_VISA = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("toUtilisateurVisa");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "borId";

	public static final String BOR_DATE_CREATION_KEY = "borDateCreation";
	public static final String BOR_DATE_VISA_KEY = "borDateVisa";
	public static final String BOR_ETAT_KEY = "borEtat";
	public static final String BOR_NUM_KEY = "borNum";
	public static final String BOR_ORDRE_KEY = "borOrdre";

//Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String TBO_ORDRE_KEY = "tboOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";
	public static final String UTL_ORDRE_VISA_KEY = "utlOrdreVisa";

//Colonnes dans la base de donnees
	public static final String BOR_DATE_CREATION_COLKEY = "bor_Date_creation";
	public static final String BOR_DATE_VISA_COLKEY = "bor_Date_Visa";
	public static final String BOR_ETAT_COLKEY = "bor_etat";
	public static final String BOR_NUM_COLKEY = "bor_num";
	public static final String BOR_ORDRE_COLKEY = "BOR_ORDRE";

	public static final String BOR_ID_COLKEY = "bor_id";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";
	public static final String UTL_ORDRE_VISA_COLKEY = "utl_ORDRE_VISA";


	// Relationships
	public static final String TO_BORDEREAU_BROUILLARDS_KEY = "toBordereauBrouillards";
	public static final String TO_BORDEREAU_INFOS_KEY = "toBordereauInfos";
	public static final String TO_BROUILLARDS_KEY = "toBrouillards";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_GESTION_KEY = "toGestion";
	public static final String TO_MANDATS_KEY = "toMandats";
	public static final String TO_TITRES_KEY = "toTitres";
	public static final String TO_TYPE_BORDEREAU_KEY = "toTypeBordereau";
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";
	public static final String TO_UTILISATEUR_VISA_KEY = "toUtilisateurVisa";



	// Accessors methods
	public NSTimestamp borDateCreation() {
	 return (NSTimestamp) storedValueForKey(BOR_DATE_CREATION_KEY);
	}

	public void setBorDateCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, BOR_DATE_CREATION_KEY);
	}

	public NSTimestamp borDateVisa() {
	 return (NSTimestamp) storedValueForKey(BOR_DATE_VISA_KEY);
	}

	public void setBorDateVisa(NSTimestamp value) {
	 takeStoredValueForKey(value, BOR_DATE_VISA_KEY);
	}

	public String borEtat() {
	 return (String) storedValueForKey(BOR_ETAT_KEY);
	}

	public void setBorEtat(String value) {
	 takeStoredValueForKey(value, BOR_ETAT_KEY);
	}

	public Integer borNum() {
	 return (Integer) storedValueForKey(BOR_NUM_KEY);
	}

	public void setBorNum(Integer value) {
	 takeStoredValueForKey(value, BOR_NUM_KEY);
	}

	public Integer borOrdre() {
	 return (Integer) storedValueForKey(BOR_ORDRE_KEY);
	}

	public void setBorOrdre(Integer value) {
	 takeStoredValueForKey(value, BOR_ORDRE_KEY);
	}

	public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
	 return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGestion toGestion() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
	}

	public void setToGestionRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGestion value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGestion oldValue = toGestion();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOTypeBordereau toTypeBordereau() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTypeBordereau)storedValueForKey(TO_TYPE_BORDEREAU_KEY);
	}

	public void setToTypeBordereauRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTypeBordereau value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOTypeBordereau oldValue = toTypeBordereau();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_BORDEREAU_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_BORDEREAU_KEY);
	 }
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateur() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
	}

	public void setToUtilisateurRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = toUtilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
	 }
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateurVisa() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(TO_UTILISATEUR_VISA_KEY);
	}

	public void setToUtilisateurVisaRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = toUtilisateurVisa();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_VISA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_VISA_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> toBordereauBrouillards() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard>)storedValueForKey(TO_BORDEREAU_BROUILLARDS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> toBordereauBrouillards(EOQualifier qualifier) {
	 return toBordereauBrouillards(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> toBordereauBrouillards(EOQualifier qualifier, Boolean fetch) {
	 return toBordereauBrouillards(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> toBordereauBrouillards(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard.TO_BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toBordereauBrouillards();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToBordereauBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_BORDEREAU_BROUILLARDS_KEY);
	}
	
	public void removeFromToBordereauBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BORDEREAU_BROUILLARDS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard createToBordereauBrouillardsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_BORDEREAU_BROUILLARDS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard) eo;
	}
	
	public void deleteToBordereauBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BORDEREAU_BROUILLARDS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToBordereauBrouillardsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> objects = toBordereauBrouillards().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToBordereauBrouillardsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> toBordereauInfos() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo>)storedValueForKey(TO_BORDEREAU_INFOS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> toBordereauInfos(EOQualifier qualifier) {
	 return toBordereauInfos(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> toBordereauInfos(EOQualifier qualifier, Boolean fetch) {
	 return toBordereauInfos(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> toBordereauInfos(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo.TO_BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toBordereauInfos();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToBordereauInfosRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_BORDEREAU_INFOS_KEY);
	}
	
	public void removeFromToBordereauInfosRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BORDEREAU_INFOS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo createToBordereauInfosRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_BORDEREAU_INFOS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo) eo;
	}
	
	public void deleteToBordereauInfosRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BORDEREAU_INFOS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToBordereauInfosRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo> objects = toBordereauInfos().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToBordereauInfosRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> toBrouillards() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard>)storedValueForKey(TO_BROUILLARDS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> toBrouillards(EOQualifier qualifier) {
	 return toBrouillards(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> toBrouillards(EOQualifier qualifier, Boolean fetch) {
	 return toBrouillards(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> toBrouillards(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOBrouillard.TO_BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOBrouillard.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toBrouillards();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillard object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_BROUILLARDS_KEY);
	}
	
	public void removeFromToBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillard object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BROUILLARDS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOBrouillard createToBrouillardsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOBrouillard.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_BROUILLARDS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBrouillard) eo;
	}
	
	public void deleteToBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillard object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BROUILLARDS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToBrouillardsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> objects = toBrouillards().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToBrouillardsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat> toMandats() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat>)storedValueForKey(TO_MANDATS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat> toMandats(EOQualifier qualifier) {
	 return toMandats(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat> toMandats(EOQualifier qualifier, Boolean fetch) {
	 return toMandats(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat> toMandats(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOMandat.TO_BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOMandat.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toMandats();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOMandat>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToMandatsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOMandat object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_MANDATS_KEY);
	}
	
	public void removeFromToMandatsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOMandat object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MANDATS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOMandat createToMandatsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOMandat.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_MANDATS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOMandat) eo;
	}
	
	public void deleteToMandatsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOMandat object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MANDATS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToMandatsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOMandat> objects = toMandats().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToMandatsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre> toTitres() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre>)storedValueForKey(TO_TITRES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre> toTitres(EOQualifier qualifier) {
	 return toTitres(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre> toTitres(EOQualifier qualifier, Boolean fetch) {
	 return toTitres(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre> toTitres(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOTitre.TO_BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOTitre.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toTitres();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToTitresRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTitre object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_TITRES_KEY);
	}
	
	public void removeFromToTitresRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTitre object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_TITRES_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOTitre createToTitresRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOTitre.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_TITRES_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTitre) eo;
	}
	
	public void deleteToTitresRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTitre object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_TITRES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToTitresRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOTitre> objects = toTitres().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToTitresRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOBordereau avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOBordereau createEOBordereau(EOEditingContext editingContext				, NSTimestamp borDateCreation
													, Integer borOrdre
					, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice		, org.cocktail.fwkcktlcompta.server.metier.EOGestion toGestion		, org.cocktail.fwkcktlcompta.server.metier.EOTypeBordereau toTypeBordereau		, org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateur							) {
	 EOBordereau eo = (EOBordereau) EOUtilities.createAndInsertInstance(editingContext, _EOBordereau.ENTITY_NAME);	 
							eo.setBorDateCreation(borDateCreation);
															eo.setBorOrdre(borOrdre);
						 eo.setToExerciceRelationship(toExercice);
				 eo.setToGestionRelationship(toGestion);
				 eo.setToTypeBordereauRelationship(toTypeBordereau);
				 eo.setToUtilisateurRelationship(toUtilisateur);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBordereau creerInstance(EOEditingContext editingContext) {
		EOBordereau object = (EOBordereau)EOUtilities.createAndInsertInstance(editingContext, _EOBordereau.ENTITY_NAME);
  		return object;
		}

	

  public EOBordereau localInstanceIn(EOEditingContext editingContext) {
    EOBordereau localInstance = (EOBordereau)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereau> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereau> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereau> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereau> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereau> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereau> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereau> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBordereau>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBordereau fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBordereau fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOBordereau> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBordereau eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBordereau)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBordereau fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBordereau fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOBordereau> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBordereau eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBordereau)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBordereau fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBordereau eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBordereau ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBordereau fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}