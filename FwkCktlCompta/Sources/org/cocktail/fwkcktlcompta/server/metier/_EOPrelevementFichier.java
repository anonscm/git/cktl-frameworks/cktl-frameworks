/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrelevementFichier.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOPrelevementFichier extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_PrelevementFichier";
	public static final String ENTITY_TABLE_NAME = "maracuja.PRELEVEMENT_FICHIER";


//Attribute Keys
	public static final ERXKey<String> FICP_CODE_OP = new ERXKey<String>("ficpCodeOp");
	public static final ERXKey<String> FICP_COMPTE = new ERXKey<String>("ficpCompte");
	public static final ERXKey<String> FICP_CONTENU = new ERXKey<String>("ficpContenu");
	public static final ERXKey<NSTimestamp> FICP_DATE_CREATION = new ERXKey<NSTimestamp>("ficpDateCreation");
	public static final ERXKey<NSTimestamp> FICP_DATE_MODIF = new ERXKey<NSTimestamp>("ficpDateModif");
	public static final ERXKey<NSTimestamp> FICP_DATE_REGLEMENT = new ERXKey<NSTimestamp>("ficpDateReglement");
	public static final ERXKey<NSTimestamp> FICP_DATE_REMISE = new ERXKey<NSTimestamp>("ficpDateRemise");
	public static final ERXKey<String> FICP_LIBELLE = new ERXKey<String>("ficpLibelle");
	public static final ERXKey<java.math.BigDecimal> FICP_MONTANT = new ERXKey<java.math.BigDecimal>("ficpMontant");
	public static final ERXKey<Integer> FICP_NB_PRELEVEMENTS = new ERXKey<Integer>("ficpNbPrelevements");
	public static final ERXKey<Integer> FICP_NUMERO = new ERXKey<Integer>("ficpNumero");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EORecouvrement> TO_RECOUVREMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EORecouvrement>("toRecouvrement");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement> TO_TYPE_RECOUVREMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement>("toTypeRecouvrement");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur> TO_UTILISATEUR = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur>("toUtilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ficpOrdre";

	public static final String FICP_CODE_OP_KEY = "ficpCodeOp";
	public static final String FICP_COMPTE_KEY = "ficpCompte";
	public static final String FICP_CONTENU_KEY = "ficpContenu";
	public static final String FICP_DATE_CREATION_KEY = "ficpDateCreation";
	public static final String FICP_DATE_MODIF_KEY = "ficpDateModif";
	public static final String FICP_DATE_REGLEMENT_KEY = "ficpDateReglement";
	public static final String FICP_DATE_REMISE_KEY = "ficpDateRemise";
	public static final String FICP_LIBELLE_KEY = "ficpLibelle";
	public static final String FICP_MONTANT_KEY = "ficpMontant";
	public static final String FICP_NB_PRELEVEMENTS_KEY = "ficpNbPrelevements";
	public static final String FICP_NUMERO_KEY = "ficpNumero";

//Attributs non visibles
	public static final String FICP_ORDRE_KEY = "ficpOrdre";
	public static final String RECO_ORDRE_KEY = "recoOrdre";
	public static final String TREC_ORDRE_KEY = "trecOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String FICP_CODE_OP_COLKEY = "ficp_code_op";
	public static final String FICP_COMPTE_COLKEY = "FICP_COMPTE";
	public static final String FICP_CONTENU_COLKEY = "ficp_contenu";
	public static final String FICP_DATE_CREATION_COLKEY = "ficp_DATE_CREATION";
	public static final String FICP_DATE_MODIF_COLKEY = "ficp_DATE_MODIF";
	public static final String FICP_DATE_REGLEMENT_COLKEY = "ficp_DATE_reglement";
	public static final String FICP_DATE_REMISE_COLKEY = "ficp_DATE_REMISE";
	public static final String FICP_LIBELLE_COLKEY = "ficp_LIBELLE";
	public static final String FICP_MONTANT_COLKEY = "ficp_montant";
	public static final String FICP_NB_PRELEVEMENTS_COLKEY = "ficp_nb_prel";
	public static final String FICP_NUMERO_COLKEY = "ficp_NUMERO";

	public static final String FICP_ORDRE_COLKEY = "FICP_ORDRE";
	public static final String RECO_ORDRE_COLKEY = "reco_ordre";
	public static final String TREC_ORDRE_COLKEY = "trec_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String TO_RECOUVREMENT_KEY = "toRecouvrement";
	public static final String TO_TYPE_RECOUVREMENT_KEY = "toTypeRecouvrement";
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";



	// Accessors methods
	public String ficpCodeOp() {
	 return (String) storedValueForKey(FICP_CODE_OP_KEY);
	}

	public void setFicpCodeOp(String value) {
	 takeStoredValueForKey(value, FICP_CODE_OP_KEY);
	}

	public String ficpCompte() {
	 return (String) storedValueForKey(FICP_COMPTE_KEY);
	}

	public void setFicpCompte(String value) {
	 takeStoredValueForKey(value, FICP_COMPTE_KEY);
	}

	public String ficpContenu() {
	 return (String) storedValueForKey(FICP_CONTENU_KEY);
	}

	public void setFicpContenu(String value) {
	 takeStoredValueForKey(value, FICP_CONTENU_KEY);
	}

	public NSTimestamp ficpDateCreation() {
	 return (NSTimestamp) storedValueForKey(FICP_DATE_CREATION_KEY);
	}

	public void setFicpDateCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, FICP_DATE_CREATION_KEY);
	}

	public NSTimestamp ficpDateModif() {
	 return (NSTimestamp) storedValueForKey(FICP_DATE_MODIF_KEY);
	}

	public void setFicpDateModif(NSTimestamp value) {
	 takeStoredValueForKey(value, FICP_DATE_MODIF_KEY);
	}

	public NSTimestamp ficpDateReglement() {
	 return (NSTimestamp) storedValueForKey(FICP_DATE_REGLEMENT_KEY);
	}

	public void setFicpDateReglement(NSTimestamp value) {
	 takeStoredValueForKey(value, FICP_DATE_REGLEMENT_KEY);
	}

	public NSTimestamp ficpDateRemise() {
	 return (NSTimestamp) storedValueForKey(FICP_DATE_REMISE_KEY);
	}

	public void setFicpDateRemise(NSTimestamp value) {
	 takeStoredValueForKey(value, FICP_DATE_REMISE_KEY);
	}

	public String ficpLibelle() {
	 return (String) storedValueForKey(FICP_LIBELLE_KEY);
	}

	public void setFicpLibelle(String value) {
	 takeStoredValueForKey(value, FICP_LIBELLE_KEY);
	}

	public java.math.BigDecimal ficpMontant() {
	 return (java.math.BigDecimal) storedValueForKey(FICP_MONTANT_KEY);
	}

	public void setFicpMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, FICP_MONTANT_KEY);
	}

	public Integer ficpNbPrelevements() {
	 return (Integer) storedValueForKey(FICP_NB_PRELEVEMENTS_KEY);
	}

	public void setFicpNbPrelevements(Integer value) {
	 takeStoredValueForKey(value, FICP_NB_PRELEVEMENTS_KEY);
	}

	public Integer ficpNumero() {
	 return (Integer) storedValueForKey(FICP_NUMERO_KEY);
	}

	public void setFicpNumero(Integer value) {
	 takeStoredValueForKey(value, FICP_NUMERO_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EORecouvrement toRecouvrement() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EORecouvrement)storedValueForKey(TO_RECOUVREMENT_KEY);
	}

	public void setToRecouvrementRelationship(org.cocktail.fwkcktlcompta.server.metier.EORecouvrement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EORecouvrement oldValue = toRecouvrement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RECOUVREMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_RECOUVREMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement toTypeRecouvrement() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement)storedValueForKey(TO_TYPE_RECOUVREMENT_KEY);
	}

	public void setToTypeRecouvrementRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement oldValue = toTypeRecouvrement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_RECOUVREMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_RECOUVREMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur toUtilisateur() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
	}

	public void setToUtilisateurRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur oldValue = toUtilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
	 }
	}


	/**
	* Créer une instance de EOPrelevementFichier avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOPrelevementFichier createEOPrelevementFichier(EOEditingContext editingContext				, String ficpCodeOp
											, NSTimestamp ficpDateCreation
							, NSTimestamp ficpDateModif
							, NSTimestamp ficpDateReglement
							, NSTimestamp ficpDateRemise
							, String ficpLibelle
							, java.math.BigDecimal ficpMontant
							, Integer ficpNbPrelevements
							, Integer ficpNumero
					, org.cocktail.fwkcktlcompta.server.metier.EORecouvrement toRecouvrement		, org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement toTypeRecouvrement		, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur toUtilisateur					) {
	 EOPrelevementFichier eo = (EOPrelevementFichier) EOUtilities.createAndInsertInstance(editingContext, _EOPrelevementFichier.ENTITY_NAME);	 
							eo.setFicpCodeOp(ficpCodeOp);
													eo.setFicpDateCreation(ficpDateCreation);
									eo.setFicpDateModif(ficpDateModif);
									eo.setFicpDateReglement(ficpDateReglement);
									eo.setFicpDateRemise(ficpDateRemise);
									eo.setFicpLibelle(ficpLibelle);
									eo.setFicpMontant(ficpMontant);
									eo.setFicpNbPrelevements(ficpNbPrelevements);
									eo.setFicpNumero(ficpNumero);
						 eo.setToRecouvrementRelationship(toRecouvrement);
				 eo.setToTypeRecouvrementRelationship(toTypeRecouvrement);
				 eo.setToUtilisateurRelationship(toUtilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrelevementFichier creerInstance(EOEditingContext editingContext) {
		EOPrelevementFichier object = (EOPrelevementFichier)EOUtilities.createAndInsertInstance(editingContext, _EOPrelevementFichier.ENTITY_NAME);
  		return object;
		}

	

  public EOPrelevementFichier localInstanceIn(EOEditingContext editingContext) {
    EOPrelevementFichier localInstance = (EOPrelevementFichier)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrelevementFichier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrelevementFichier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPrelevementFichier> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrelevementFichier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrelevementFichier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrelevementFichier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrelevementFichier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPrelevementFichier> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrelevementFichier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrelevementFichier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrelevementFichier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrelevementFichier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrelevementFichier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrelevementFichier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}