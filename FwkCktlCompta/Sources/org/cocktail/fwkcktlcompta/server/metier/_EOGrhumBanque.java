/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGrhumBanque.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOGrhumBanque extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Grhum_Banque";
	public static final String ENTITY_TABLE_NAME = "GRHUM.BANQUE";


//Attribute Keys
	public static final ERXKey<String> ADRESSE = new ERXKey<String>("adresse");
	public static final ERXKey<String> BIC = new ERXKey<String>("bic");
	public static final ERXKey<String> C_BANQUE = new ERXKey<String>("cBanque");
	public static final ERXKey<String> C_GUICHET = new ERXKey<String>("cGuichet");
	public static final ERXKey<String> CODE_POSTAL = new ERXKey<String>("codePostal");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_FERMETURE = new ERXKey<NSTimestamp>("dFermeture");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> DOMICILIATION = new ERXKey<String>("domiciliation");
	public static final ERXKey<NSTimestamp> D_OUVERTURE = new ERXKey<NSTimestamp>("dOuverture");
	public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
	public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
	public static final ERXKey<String> TEM_LOCAL = new ERXKey<String>("temLocal");
	public static final ERXKey<String> VILLE = new ERXKey<String>("ville");
// Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "banqOrdre";

	public static final String ADRESSE_KEY = "adresse";
	public static final String BIC_KEY = "bic";
	public static final String C_BANQUE_KEY = "cBanque";
	public static final String C_GUICHET_KEY = "cGuichet";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DOMICILIATION_KEY = "domiciliation";
	public static final String D_OUVERTURE_KEY = "dOuverture";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String TEM_LOCAL_KEY = "temLocal";
	public static final String VILLE_KEY = "ville";

//Attributs non visibles
	public static final String BANQ_ORDRE_KEY = "banqOrdre";

//Colonnes dans la base de donnees
	public static final String ADRESSE_COLKEY = "ADRESSE";
	public static final String BIC_COLKEY = "BIC";
	public static final String C_BANQUE_COLKEY = "C_BANQUE";
	public static final String C_GUICHET_COLKEY = "C_GUICHET";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_FERMETURE_COLKEY = "D_FERMETURE";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String DOMICILIATION_COLKEY = "DOMICILIATION";
	public static final String D_OUVERTURE_COLKEY = "D_OUVERTURE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String TEM_LOCAL_COLKEY = "TEM_LOCAL";
	public static final String VILLE_COLKEY = "VILLE";

	public static final String BANQ_ORDRE_COLKEY = "BANQ_ORDRE";


	// Relationships



	// Accessors methods
	public String adresse() {
	 return (String) storedValueForKey(ADRESSE_KEY);
	}

	public void setAdresse(String value) {
	 takeStoredValueForKey(value, ADRESSE_KEY);
	}

	public String bic() {
	 return (String) storedValueForKey(BIC_KEY);
	}

	public void setBic(String value) {
	 takeStoredValueForKey(value, BIC_KEY);
	}

	public String cBanque() {
	 return (String) storedValueForKey(C_BANQUE_KEY);
	}

	public void setCBanque(String value) {
	 takeStoredValueForKey(value, C_BANQUE_KEY);
	}

	public String cGuichet() {
	 return (String) storedValueForKey(C_GUICHET_KEY);
	}

	public void setCGuichet(String value) {
	 takeStoredValueForKey(value, C_GUICHET_KEY);
	}

	public String codePostal() {
	 return (String) storedValueForKey(CODE_POSTAL_KEY);
	}

	public void setCodePostal(String value) {
	 takeStoredValueForKey(value, CODE_POSTAL_KEY);
	}

	public NSTimestamp dCreation() {
	 return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dFermeture() {
	 return (NSTimestamp) storedValueForKey(D_FERMETURE_KEY);
	}

	public void setDFermeture(NSTimestamp value) {
	 takeStoredValueForKey(value, D_FERMETURE_KEY);
	}

	public NSTimestamp dModification() {
	 return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
	 takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	public String domiciliation() {
	 return (String) storedValueForKey(DOMICILIATION_KEY);
	}

	public void setDomiciliation(String value) {
	 takeStoredValueForKey(value, DOMICILIATION_KEY);
	}

	public NSTimestamp dOuverture() {
	 return (NSTimestamp) storedValueForKey(D_OUVERTURE_KEY);
	}

	public void setDOuverture(NSTimestamp value) {
	 takeStoredValueForKey(value, D_OUVERTURE_KEY);
	}

	public Integer persIdCreation() {
	 return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}

	public Integer persIdModification() {
	 return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}

	public String temLocal() {
	 return (String) storedValueForKey(TEM_LOCAL_KEY);
	}

	public void setTemLocal(String value) {
	 takeStoredValueForKey(value, TEM_LOCAL_KEY);
	}

	public String ville() {
	 return (String) storedValueForKey(VILLE_KEY);
	}

	public void setVille(String value) {
	 takeStoredValueForKey(value, VILLE_KEY);
	}


	/**
	* Créer une instance de EOGrhumBanque avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOGrhumBanque createEOGrhumBanque(EOEditingContext editingContext														, NSTimestamp dCreation
									, NSTimestamp dModification
							, String domiciliation
																		) {
	 EOGrhumBanque eo = (EOGrhumBanque) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumBanque.ENTITY_NAME);	 
																	eo.setDCreation(dCreation);
											eo.setDModification(dModification);
									eo.setDomiciliation(domiciliation);
															 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGrhumBanque creerInstance(EOEditingContext editingContext) {
		EOGrhumBanque object = (EOGrhumBanque)EOUtilities.createAndInsertInstance(editingContext, _EOGrhumBanque.ENTITY_NAME);
  		return object;
		}

	

  public EOGrhumBanque localInstanceIn(EOEditingContext editingContext) {
    EOGrhumBanque localInstance = (EOGrhumBanque)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumBanque> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumBanque> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumBanque> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumBanque> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumBanque> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumBanque> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumBanque> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumBanque>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGrhumBanque fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGrhumBanque fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGrhumBanque> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGrhumBanque eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGrhumBanque)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGrhumBanque fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGrhumBanque fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGrhumBanque> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGrhumBanque eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGrhumBanque)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGrhumBanque fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGrhumBanque eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGrhumBanque ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGrhumBanque fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}