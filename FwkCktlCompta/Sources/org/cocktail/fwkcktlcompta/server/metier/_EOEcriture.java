/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEcriture.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOEcriture extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Ecriture";
	public static final String ENTITY_TABLE_NAME = "maracuja.Ecriture";


//Attribute Keys
	public static final ERXKey<NSTimestamp> ECR_DATE = new ERXKey<NSTimestamp>("ecrDate");
	public static final ERXKey<NSTimestamp> ECR_DATE_SAISIE = new ERXKey<NSTimestamp>("ecrDateSaisie");
	public static final ERXKey<String> ECR_ETAT = new ERXKey<String>("ecrEtat");
	public static final ERXKey<String> ECR_LIBELLE = new ERXKey<String>("ecrLibelle");
	public static final ERXKey<Integer> ECR_NUMERO = new ERXKey<Integer>("ecrNumero");
	public static final ERXKey<Integer> ECR_NUMERO_BROUILLARD = new ERXKey<Integer>("ecrNumeroBrouillard");
	public static final ERXKey<String> ECR_POSTIT = new ERXKey<String>("ecrPostit");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOComptabilite> TO_COMPTABILITE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOComptabilite>("toComptabilite");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> TO_ECRITURE_DETAILS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail>("toEcritureDetails");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOOrigine> TO_ORIGINE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOOrigine>("toOrigine");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal> TO_TYPE_JOURNAL = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal>("toTypeJournal");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation> TO_TYPE_OPERATION = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation>("toTypeOperation");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur> TO_UTILISATEUR = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur>("toUtilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ecrOrdre";

	public static final String ECR_DATE_KEY = "ecrDate";
	public static final String ECR_DATE_SAISIE_KEY = "ecrDateSaisie";
	public static final String ECR_ETAT_KEY = "ecrEtat";
	public static final String ECR_LIBELLE_KEY = "ecrLibelle";
	public static final String ECR_NUMERO_KEY = "ecrNumero";
	public static final String ECR_NUMERO_BROUILLARD_KEY = "ecrNumeroBrouillard";
	public static final String ECR_POSTIT_KEY = "ecrPostit";

//Attributs non visibles
	public static final String BRO_ORDRE_KEY = "broOrdre";
	public static final String COM_ORDRE_KEY = "comOrdre";
	public static final String ECR_ORDRE_KEY = "ecrOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String TJO_ORDRE_KEY = "tjoOrdre";
	public static final String TOP_ORDRE_KEY = "topOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ECR_DATE_COLKEY = "ecr_Date";
	public static final String ECR_DATE_SAISIE_COLKEY = "ecr_Date_Saisie";
	public static final String ECR_ETAT_COLKEY = "ecr_etat";
	public static final String ECR_LIBELLE_COLKEY = "ecr_Libelle";
	public static final String ECR_NUMERO_COLKEY = "ecr_numero";
	public static final String ECR_NUMERO_BROUILLARD_COLKEY = "ecr_numero_Brouillard";
	public static final String ECR_POSTIT_COLKEY = "ecr_Postit";

	public static final String BRO_ORDRE_COLKEY = "BRO_ORDRE";
	public static final String COM_ORDRE_COLKEY = "com_ordre";
	public static final String ECR_ORDRE_COLKEY = "ecr_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORI_ORDRE_COLKEY = "ori_ordre";
	public static final String TJO_ORDRE_COLKEY = "TJO_ORDRE";
	public static final String TOP_ORDRE_COLKEY = "TOP_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String TO_COMPTABILITE_KEY = "toComptabilite";
	public static final String TO_ECRITURE_DETAILS_KEY = "toEcritureDetails";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_ORIGINE_KEY = "toOrigine";
	public static final String TO_TYPE_JOURNAL_KEY = "toTypeJournal";
	public static final String TO_TYPE_OPERATION_KEY = "toTypeOperation";
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";



	// Accessors methods
	public NSTimestamp ecrDate() {
	 return (NSTimestamp) storedValueForKey(ECR_DATE_KEY);
	}

	public void setEcrDate(NSTimestamp value) {
	 takeStoredValueForKey(value, ECR_DATE_KEY);
	}

	public NSTimestamp ecrDateSaisie() {
	 return (NSTimestamp) storedValueForKey(ECR_DATE_SAISIE_KEY);
	}

	public void setEcrDateSaisie(NSTimestamp value) {
	 takeStoredValueForKey(value, ECR_DATE_SAISIE_KEY);
	}

	public String ecrEtat() {
	 return (String) storedValueForKey(ECR_ETAT_KEY);
	}

	public void setEcrEtat(String value) {
	 takeStoredValueForKey(value, ECR_ETAT_KEY);
	}

	public String ecrLibelle() {
	 return (String) storedValueForKey(ECR_LIBELLE_KEY);
	}

	public void setEcrLibelle(String value) {
	 takeStoredValueForKey(value, ECR_LIBELLE_KEY);
	}

	public Integer ecrNumero() {
	 return (Integer) storedValueForKey(ECR_NUMERO_KEY);
	}

	public void setEcrNumero(Integer value) {
	 takeStoredValueForKey(value, ECR_NUMERO_KEY);
	}

	public Integer ecrNumeroBrouillard() {
	 return (Integer) storedValueForKey(ECR_NUMERO_BROUILLARD_KEY);
	}

	public void setEcrNumeroBrouillard(Integer value) {
	 takeStoredValueForKey(value, ECR_NUMERO_BROUILLARD_KEY);
	}

	public String ecrPostit() {
	 return (String) storedValueForKey(ECR_POSTIT_KEY);
	}

	public void setEcrPostit(String value) {
	 takeStoredValueForKey(value, ECR_POSTIT_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOComptabilite toComptabilite() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOComptabilite)storedValueForKey(TO_COMPTABILITE_KEY);
	}

	public void setToComptabiliteRelationship(org.cocktail.fwkcktlcompta.server.metier.EOComptabilite value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOComptabilite oldValue = toComptabilite();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COMPTABILITE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_COMPTABILITE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOOrigine toOrigine() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOOrigine)storedValueForKey(TO_ORIGINE_KEY);
	}

	public void setToOrigineRelationship(org.cocktail.fwkcktlcompta.server.metier.EOOrigine value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOOrigine oldValue = toOrigine();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORIGINE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORIGINE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal toTypeJournal() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal)storedValueForKey(TO_TYPE_JOURNAL_KEY);
	}

	public void setToTypeJournalRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal oldValue = toTypeJournal();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_JOURNAL_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_JOURNAL_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation toTypeOperation() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation)storedValueForKey(TO_TYPE_OPERATION_KEY);
	}

	public void setToTypeOperationRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation oldValue = toTypeOperation();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_OPERATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_OPERATION_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur toUtilisateur() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
	}

	public void setToUtilisateurRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur oldValue = toUtilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> toEcritureDetails() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail>)storedValueForKey(TO_ECRITURE_DETAILS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> toEcritureDetails(EOQualifier qualifier) {
	 return toEcritureDetails(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> toEcritureDetails(EOQualifier qualifier, Boolean fetch) {
	 return toEcritureDetails(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> toEcritureDetails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail.TO_ECRITURE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toEcritureDetails();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToEcritureDetailsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_ECRITURE_DETAILS_KEY);
	}
	
	public void removeFromToEcritureDetailsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ECRITURE_DETAILS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail createToEcritureDetailsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_ECRITURE_DETAILS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail) eo;
	}
	
	public void deleteToEcritureDetailsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ECRITURE_DETAILS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToEcritureDetailsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> objects = toEcritureDetails().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToEcritureDetailsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOEcriture avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEcriture createEOEcriture(EOEditingContext editingContext				, NSTimestamp ecrDate
							, NSTimestamp ecrDateSaisie
							, String ecrEtat
							, String ecrLibelle
							, Integer ecrNumero
									, org.cocktail.fwkcktlcompta.server.metier.EOComptabilite toComptabilite		, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice				, org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal toTypeJournal		, org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation toTypeOperation		, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur toUtilisateur					) {
	 EOEcriture eo = (EOEcriture) EOUtilities.createAndInsertInstance(editingContext, _EOEcriture.ENTITY_NAME);	 
							eo.setEcrDate(ecrDate);
									eo.setEcrDateSaisie(ecrDateSaisie);
									eo.setEcrEtat(ecrEtat);
									eo.setEcrLibelle(ecrLibelle);
									eo.setEcrNumero(ecrNumero);
										 eo.setToComptabiliteRelationship(toComptabilite);
				 eo.setToExerciceRelationship(toExercice);
						 eo.setToTypeJournalRelationship(toTypeJournal);
				 eo.setToTypeOperationRelationship(toTypeOperation);
				 eo.setToUtilisateurRelationship(toUtilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEcriture creerInstance(EOEditingContext editingContext) {
		EOEcriture object = (EOEcriture)EOUtilities.createAndInsertInstance(editingContext, _EOEcriture.ENTITY_NAME);
  		return object;
		}

	

  public EOEcriture localInstanceIn(EOEditingContext editingContext) {
    EOEcriture localInstance = (EOEcriture)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcriture> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcriture> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcriture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcriture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcriture> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcriture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcriture> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcriture>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEcriture fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEcriture fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEcriture> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEcriture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEcriture)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEcriture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEcriture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEcriture> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEcriture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEcriture)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEcriture fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEcriture eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEcriture ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEcriture fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}