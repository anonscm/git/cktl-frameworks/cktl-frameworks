/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeVirement.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOTypeVirement extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_TypeVirement";
	public static final String ENTITY_TABLE_NAME = "maracuja.TYPE_VIREMENT";


//Attribute Keys
	public static final ERXKey<String> MOD_DOM = new ERXKey<String>("modDom");
	public static final ERXKey<String> TVI_FORMAT = new ERXKey<String>("tviFormat");
	public static final ERXKey<String> TVI_LIBELLE = new ERXKey<String>("tviLibelle");
	public static final ERXKey<String> TVI_VALIDITE = new ERXKey<String>("tviValidite");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa> TO_VIREMENT_PARAM_SEPAS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa>("toVirementParamSepas");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tviOrdre";

	public static final String MOD_DOM_KEY = "modDom";
	public static final String TVI_FORMAT_KEY = "tviFormat";
	public static final String TVI_LIBELLE_KEY = "tviLibelle";
	public static final String TVI_VALIDITE_KEY = "tviValidite";

//Attributs non visibles
	public static final String TVI_ORDRE_KEY = "tviOrdre";

//Colonnes dans la base de donnees
	public static final String MOD_DOM_COLKEY = "MOD_DOM";
	public static final String TVI_FORMAT_COLKEY = "TVI_FORMAT";
	public static final String TVI_LIBELLE_COLKEY = "TVI_LIBELLE";
	public static final String TVI_VALIDITE_COLKEY = "TVI_VALIDITE";

	public static final String TVI_ORDRE_COLKEY = "TVI_ORDRE";


	// Relationships
	public static final String TO_VIREMENT_PARAM_SEPAS_KEY = "toVirementParamSepas";



	// Accessors methods
	public String modDom() {
	 return (String) storedValueForKey(MOD_DOM_KEY);
	}

	public void setModDom(String value) {
	 takeStoredValueForKey(value, MOD_DOM_KEY);
	}

	public String tviFormat() {
	 return (String) storedValueForKey(TVI_FORMAT_KEY);
	}

	public void setTviFormat(String value) {
	 takeStoredValueForKey(value, TVI_FORMAT_KEY);
	}

	public String tviLibelle() {
	 return (String) storedValueForKey(TVI_LIBELLE_KEY);
	}

	public void setTviLibelle(String value) {
	 takeStoredValueForKey(value, TVI_LIBELLE_KEY);
	}

	public String tviValidite() {
	 return (String) storedValueForKey(TVI_VALIDITE_KEY);
	}

	public void setTviValidite(String value) {
	 takeStoredValueForKey(value, TVI_VALIDITE_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa> toVirementParamSepas() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa>)storedValueForKey(TO_VIREMENT_PARAM_SEPAS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa> toVirementParamSepas(EOQualifier qualifier) {
	 return toVirementParamSepas(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa> toVirementParamSepas(EOQualifier qualifier, Boolean fetch) {
	 return toVirementParamSepas(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa> toVirementParamSepas(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa.TO_TYPE_VIREMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toVirementParamSepas();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToVirementParamSepasRelationship(org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_VIREMENT_PARAM_SEPAS_KEY);
	}
	
	public void removeFromToVirementParamSepasRelationship(org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_VIREMENT_PARAM_SEPAS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa createToVirementParamSepasRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_VIREMENT_PARAM_SEPAS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa) eo;
	}
	
	public void deleteToVirementParamSepasRelationship(org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_VIREMENT_PARAM_SEPAS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToVirementParamSepasRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOVirementParamSepa> objects = toVirementParamSepas().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToVirementParamSepasRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOTypeVirement avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOTypeVirement createEOTypeVirement(EOEditingContext editingContext								, String tviLibelle
							, String tviValidite
								) {
	 EOTypeVirement eo = (EOTypeVirement) EOUtilities.createAndInsertInstance(editingContext, _EOTypeVirement.ENTITY_NAME);	 
											eo.setTviLibelle(tviLibelle);
									eo.setTviValidite(tviValidite);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeVirement creerInstance(EOEditingContext editingContext) {
		EOTypeVirement object = (EOTypeVirement)EOUtilities.createAndInsertInstance(editingContext, _EOTypeVirement.ENTITY_NAME);
  		return object;
		}

	

  public EOTypeVirement localInstanceIn(EOEditingContext editingContext) {
    EOTypeVirement localInstance = (EOTypeVirement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeVirement> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeVirement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeVirement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeVirement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeVirement> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeVirement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeVirement> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeVirement>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeVirement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeVirement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTypeVirement> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeVirement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeVirement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeVirement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeVirement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTypeVirement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeVirement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeVirement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeVirement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeVirement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeVirement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeVirement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}