/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.server.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOBrouillardDetail extends _EOBrouillardDetail {

	public static final EOQualifier QUAL_DEBITS = new EOKeyValueQualifier(EOBrouillardDetail.BROD_SENS_KEY, EOQualifier.QualifierOperatorEqual, IConst.SENS_DEBIT);
	public static final EOQualifier QUAL_CREDITS = new EOKeyValueQualifier(EOBrouillardDetail.BROD_SENS_KEY, EOQualifier.QualifierOperatorEqual, IConst.SENS_CREDIT);
	public static final EOSortOrdering SORT_BROD_INDEX_ASC = new EOSortOrdering(EOBrouillardDetail.BROD_INDEX_KEY, EOSortOrdering.CompareAscending);

	public EOBrouillardDetail() {
		super();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (toBrouillard() == null) {
			throw new NSValidation.ValidationException("Le brouillard doit etre renseigné.");
		}
		if ((IConst.SENS_CREDIT.equals(brodSens()) && brodDebit().doubleValue() != 0) || (IConst.SENS_DEBIT.equals(brodSens()) && brodCredit().doubleValue() != 0)) {
			throw new NSValidation.ValidationException("Sens (D/C) incohérent avec montants saisis en débit/crédit.");
		}

		//verifier code gestion actif pour exercice
		EOQualifier qual1 = new EOKeyValueQualifier(EOGestionExercice.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, toBrouillard().toExercice());
		EOQualifier qual2 = new EOKeyValueQualifier(EOGestionExercice.TO_GESTION_KEY, EOQualifier.QualifierOperatorEqual, toGestion());
		if (EOGestionExercice.fetchAll(editingContext(), new EOAndQualifier(new NSArray(new Object[] {
				qual1, qual2
		}))).count() == 0) {
			throw new NSValidation.ValidationException("Le code gestion " + toGestion().gesCode() + " n'est pas valide pour l'exercice " + toBrouillard().toExercice().exeExercice());
		}

		//		if (toBrouillard().isVise() && toEcritureDetail() == null) {
		//			throw new NSValidation.ValidationException("Un detail d'ecriture doit être relié au détail du brouillard si celui-ci est accepté.");
		//		}
		//		if (!toBrouillard().isVise() && toEcritureDetail() != null) {
		//			throw new NSValidation.ValidationException("Un detail d'ecriture ne peut être relié au détail du brouillard si celui-ci n'est pas accepté.");
		//		}

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public static EOBrouillardDetail creer(EOEditingContext editingContext, EOBrouillard brouillard, Integer index, EOGestion gestion, String pcoNum, String pcoLibelle, BigDecimal debit, BigDecimal credit, String sens, String libelle, String postIt, String commentaire) {
		EOBrouillardDetail brod = EOBrouillardDetail.creerInstance(editingContext);
		brod.setBrodIndex(index);
		brod.setBrodPcoNum(pcoNum);
		brod.setBrodPcoLibelle(pcoLibelle);
		brod.setBrodDebit(debit);
		brod.setBrodCredit(credit);
		brod.setBrodSens(sens);
		brod.setBrodLibelle(libelle);
		brod.setBrodPostit(postIt);
		brod.setBrodCommentaire(commentaire);
		brod.setToGestionRelationship(gestion);
		brod.setToBrouillardRelationship(brouillard);
		return brod;
	}
}
