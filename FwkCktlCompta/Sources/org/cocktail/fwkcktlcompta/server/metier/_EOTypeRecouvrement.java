/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeRecouvrement.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOTypeRecouvrement extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_TypeRecouvrement";
	public static final String ENTITY_TABLE_NAME = "maracuja.TYPE_RECOUVREMENT";


//Attribute Keys
	public static final ERXKey<String> MOD_DOM = new ERXKey<String>("modDom");
	public static final ERXKey<String> TREC_FORMAT = new ERXKey<String>("trecFormat");
	public static final ERXKey<String> TREC_LIBELLE = new ERXKey<String>("trecLibelle");
	public static final ERXKey<String> TREC_VALIDITE = new ERXKey<String>("trecValidite");
// Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "trecOrdre";

	public static final String MOD_DOM_KEY = "modDom";
	public static final String TREC_FORMAT_KEY = "trecFormat";
	public static final String TREC_LIBELLE_KEY = "trecLibelle";
	public static final String TREC_VALIDITE_KEY = "trecValidite";

//Attributs non visibles
	public static final String TREC_ORDRE_KEY = "trecOrdre";

//Colonnes dans la base de donnees
	public static final String MOD_DOM_COLKEY = "MOD_DOM";
	public static final String TREC_FORMAT_COLKEY = "TREC_FORMAT";
	public static final String TREC_LIBELLE_COLKEY = "TREC_LIBELLE";
	public static final String TREC_VALIDITE_COLKEY = "TREC_VALIDITE";

	public static final String TREC_ORDRE_COLKEY = "TREC_ORDRE";


	// Relationships



	// Accessors methods
	public String modDom() {
	 return (String) storedValueForKey(MOD_DOM_KEY);
	}

	public void setModDom(String value) {
	 takeStoredValueForKey(value, MOD_DOM_KEY);
	}

	public String trecFormat() {
	 return (String) storedValueForKey(TREC_FORMAT_KEY);
	}

	public void setTrecFormat(String value) {
	 takeStoredValueForKey(value, TREC_FORMAT_KEY);
	}

	public String trecLibelle() {
	 return (String) storedValueForKey(TREC_LIBELLE_KEY);
	}

	public void setTrecLibelle(String value) {
	 takeStoredValueForKey(value, TREC_LIBELLE_KEY);
	}

	public String trecValidite() {
	 return (String) storedValueForKey(TREC_VALIDITE_KEY);
	}

	public void setTrecValidite(String value) {
	 takeStoredValueForKey(value, TREC_VALIDITE_KEY);
	}


	/**
	* Créer une instance de EOTypeRecouvrement avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOTypeRecouvrement createEOTypeRecouvrement(EOEditingContext editingContext								, String trecLibelle
							, String trecValidite
								) {
	 EOTypeRecouvrement eo = (EOTypeRecouvrement) EOUtilities.createAndInsertInstance(editingContext, _EOTypeRecouvrement.ENTITY_NAME);	 
											eo.setTrecLibelle(trecLibelle);
									eo.setTrecValidite(trecValidite);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeRecouvrement creerInstance(EOEditingContext editingContext) {
		EOTypeRecouvrement object = (EOTypeRecouvrement)EOUtilities.createAndInsertInstance(editingContext, _EOTypeRecouvrement.ENTITY_NAME);
  		return object;
		}

	

  public EOTypeRecouvrement localInstanceIn(EOEditingContext editingContext) {
    EOTypeRecouvrement localInstance = (EOTypeRecouvrement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeRecouvrement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeRecouvrement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTypeRecouvrement> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeRecouvrement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeRecouvrement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeRecouvrement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeRecouvrement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTypeRecouvrement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeRecouvrement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeRecouvrement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeRecouvrement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeRecouvrement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeRecouvrement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeRecouvrement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}