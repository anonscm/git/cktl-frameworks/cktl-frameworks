/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCoriandreInscription.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCoriandreInscription extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_CoriandreInscription";
	public static final String ENTITY_TABLE_NAME = "SCO_SCOLARITE.V_INSCRIPTION";


//Attribute Keys
	public static final ERXKey<NSTimestamp> DATE_PAIEMENT = new ERXKey<NSTimestamp>("datePaiement");
	public static final ERXKey<Integer> ETUD_NUMERO = new ERXKey<Integer>("etudNumero");
	public static final ERXKey<Integer> IDENTIFIANT = new ERXKey<Integer>("identifiant");
	public static final ERXKey<Integer> ID_PAIEMENT = new ERXKey<Integer>("idPaiement");
	public static final ERXKey<java.math.BigDecimal> MONTANT_PAIEMENT = new ERXKey<java.math.BigDecimal>("montantPaiement");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne> TO_PERSONNE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne>("toPersonne");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "identifiant";

	public static final String DATE_PAIEMENT_KEY = "datePaiement";
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String IDENTIFIANT_KEY = "identifiant";
	public static final String ID_PAIEMENT_KEY = "idPaiement";
	public static final String MONTANT_PAIEMENT_KEY = "montantPaiement";

//Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PERS_ID_KEY = "persId";

//Colonnes dans la base de donnees
	public static final String DATE_PAIEMENT_COLKEY = "DATE_PAIEMENT";
	public static final String ETUD_NUMERO_COLKEY = "etud_numero";
	public static final String IDENTIFIANT_COLKEY = "identifiant";
	public static final String ID_PAIEMENT_COLKEY = "ID_PAIEMENT";
	public static final String MONTANT_PAIEMENT_COLKEY = "MONTANT_PAIEMENT";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PERS_ID_COLKEY = "PERS_ID";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_PERSONNE_KEY = "toPersonne";



	// Accessors methods
	public NSTimestamp datePaiement() {
	 return (NSTimestamp) storedValueForKey(DATE_PAIEMENT_KEY);
	}

	public void setDatePaiement(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_PAIEMENT_KEY);
	}

	public Integer etudNumero() {
	 return (Integer) storedValueForKey(ETUD_NUMERO_KEY);
	}

	public void setEtudNumero(Integer value) {
	 takeStoredValueForKey(value, ETUD_NUMERO_KEY);
	}

	public Integer identifiant() {
	 return (Integer) storedValueForKey(IDENTIFIANT_KEY);
	}

	public void setIdentifiant(Integer value) {
	 takeStoredValueForKey(value, IDENTIFIANT_KEY);
	}

	public Integer idPaiement() {
	 return (Integer) storedValueForKey(ID_PAIEMENT_KEY);
	}

	public void setIdPaiement(Integer value) {
	 takeStoredValueForKey(value, ID_PAIEMENT_KEY);
	}

	public java.math.BigDecimal montantPaiement() {
	 return (java.math.BigDecimal) storedValueForKey(MONTANT_PAIEMENT_KEY);
	}

	public void setMontantPaiement(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MONTANT_PAIEMENT_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne toPersonne() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne)storedValueForKey(TO_PERSONNE_KEY);
	}

	public void setToPersonneRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne oldValue = toPersonne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_KEY);
	 }
	}


	/**
	* Créer une instance de EOCoriandreInscription avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCoriandreInscription createEOCoriandreInscription(EOEditingContext editingContext						, Integer etudNumero
							, Integer identifiant
							, Integer idPaiement
							, java.math.BigDecimal montantPaiement
					, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice		, org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne toPersonne					) {
	 EOCoriandreInscription eo = (EOCoriandreInscription) EOUtilities.createAndInsertInstance(editingContext, _EOCoriandreInscription.ENTITY_NAME);	 
									eo.setEtudNumero(etudNumero);
									eo.setIdentifiant(identifiant);
									eo.setIdPaiement(idPaiement);
									eo.setMontantPaiement(montantPaiement);
						 eo.setToExerciceRelationship(toExercice);
				 eo.setToPersonneRelationship(toPersonne);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCoriandreInscription creerInstance(EOEditingContext editingContext) {
		EOCoriandreInscription object = (EOCoriandreInscription)EOUtilities.createAndInsertInstance(editingContext, _EOCoriandreInscription.ENTITY_NAME);
  		return object;
		}

	

  public EOCoriandreInscription localInstanceIn(EOEditingContext editingContext) {
    EOCoriandreInscription localInstance = (EOCoriandreInscription)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOCoriandreInscription> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOCoriandreInscription> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOCoriandreInscription> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOCoriandreInscription> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOCoriandreInscription> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOCoriandreInscription> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOCoriandreInscription> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOCoriandreInscription>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCoriandreInscription fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCoriandreInscription fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCoriandreInscription> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCoriandreInscription eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCoriandreInscription)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCoriandreInscription fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCoriandreInscription fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCoriandreInscription> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCoriandreInscription eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCoriandreInscription)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCoriandreInscription fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCoriandreInscription eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCoriandreInscription ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCoriandreInscription fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}