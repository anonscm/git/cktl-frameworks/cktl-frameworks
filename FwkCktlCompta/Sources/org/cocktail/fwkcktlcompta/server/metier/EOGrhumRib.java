/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.server.metier;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumRib;
import org.cocktail.fwkcktlcompta.common.helpers.GrhumRibHelper;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOGrhumRib extends _EOGrhumRib implements IGrhumRib {

	public EOGrhumRib() {
		super();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	public boolean isRibIntlComplet() {
		return GrhumRibHelper.getSharedInstance().isRibIntlComplet(this);
	}

	public String bicEtIban() {
		return GrhumRibHelper.getSharedInstance().bicEtIban(this);
	}

	public final String getRibCompletIntl() {
		return GrhumRibHelper.getSharedInstance().getRibCompletIntl(this);

	}

	@Override
	public String bic() {
		return (toBanque() == null ? null : toBanque().bic());
	}

	public String ribBicFormate() {
		return GrhumRibHelper.getSharedInstance().ribBicFormate(this);
	}

	public String ribIbanFormate() {
		return GrhumRibHelper.getSharedInstance().ribIbanFormate(this);
	}

	public static String formatIban(String iban) {
		return GrhumRibHelper.getSharedInstance().formatIban(iban);
	}
}
