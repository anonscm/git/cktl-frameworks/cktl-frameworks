/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGestion.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOGestion extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Gestion";
	public static final String ENTITY_TABLE_NAME = "maracuja.Gestion";


//Attribute Keys
	public static final ERXKey<String> GES_CODE = new ERXKey<String>("gesCode");
	public static final ERXKey<String> GES_ORDRE = new ERXKey<String>("gesOrdre");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOComptabilite> TO_COMPTABILITE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOComptabilite>("toComptabilite");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice> TO_GESTION_EXERCICES = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice>("toGestionExercices");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "gesCode";

	public static final String GES_CODE_KEY = "gesCode";
	public static final String GES_ORDRE_KEY = "gesOrdre";

//Attributs non visibles
	public static final String COM_ORDRE_KEY = "comOrdre";

//Colonnes dans la base de donnees
	public static final String GES_CODE_COLKEY = "ges_Code";
	public static final String GES_ORDRE_COLKEY = "ges_ordre";

	public static final String COM_ORDRE_COLKEY = "com_ordre";


	// Relationships
	public static final String TO_COMPTABILITE_KEY = "toComptabilite";
	public static final String TO_GESTION_EXERCICES_KEY = "toGestionExercices";



	// Accessors methods
	public String gesCode() {
	 return (String) storedValueForKey(GES_CODE_KEY);
	}

	public void setGesCode(String value) {
	 takeStoredValueForKey(value, GES_CODE_KEY);
	}

	public String gesOrdre() {
	 return (String) storedValueForKey(GES_ORDRE_KEY);
	}

	public void setGesOrdre(String value) {
	 takeStoredValueForKey(value, GES_ORDRE_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOComptabilite toComptabilite() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOComptabilite)storedValueForKey(TO_COMPTABILITE_KEY);
	}

	public void setToComptabiliteRelationship(org.cocktail.fwkcktlcompta.server.metier.EOComptabilite value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOComptabilite oldValue = toComptabilite();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COMPTABILITE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_COMPTABILITE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice> toGestionExercices() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice>)storedValueForKey(TO_GESTION_EXERCICES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice> toGestionExercices(EOQualifier qualifier) {
	 return toGestionExercices(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice> toGestionExercices(EOQualifier qualifier, Boolean fetch) {
	 return toGestionExercices(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice> toGestionExercices(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice.TO_GESTION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toGestionExercices();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToGestionExercicesRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_GESTION_EXERCICES_KEY);
	}
	
	public void removeFromToGestionExercicesRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GESTION_EXERCICES_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice createToGestionExercicesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_GESTION_EXERCICES_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice) eo;
	}
	
	public void deleteToGestionExercicesRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GESTION_EXERCICES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToGestionExercicesRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOGestionExercice> objects = toGestionExercices().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToGestionExercicesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOGestion avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOGestion createEOGestion(EOEditingContext editingContext				, String gesCode
							, org.cocktail.fwkcktlcompta.server.metier.EOComptabilite toComptabilite					) {
	 EOGestion eo = (EOGestion) EOUtilities.createAndInsertInstance(editingContext, _EOGestion.ENTITY_NAME);	 
							eo.setGesCode(gesCode);
								 eo.setToComptabiliteRelationship(toComptabilite);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGestion creerInstance(EOEditingContext editingContext) {
		EOGestion object = (EOGestion)EOUtilities.createAndInsertInstance(editingContext, _EOGestion.ENTITY_NAME);
  		return object;
		}

	

  public EOGestion localInstanceIn(EOEditingContext editingContext) {
    EOGestion localInstance = (EOGestion)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestion> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestion> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestion> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestion> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGestion>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGestion fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGestion fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGestion> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGestion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGestion)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGestion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGestion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGestion> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGestion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGestion)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGestion fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGestion eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGestion ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGestion fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}