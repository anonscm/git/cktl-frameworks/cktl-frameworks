/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.server.metier;

import org.cocktail.fwkcktlcompta.common.entities.IPlanComptable;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EOPlanComptable extends _EOPlanComptable implements IPlanComptable {

	public static final String PCO_NATURE_DEPENSE = "D";
	public static final String PCO_NATURE_RECETTE = "R";
	public static final String PCO_VALIDITE_VALIDE = "VALIDE";
	public static final String PCO_VALIDITE_ANNULE = "ANNULE";

	public EOPlanComptable() {
		super();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	public String libelleLong() {
		return pcoNum() + " (" + pcoLibelle() + ")";
	}

	public String libelleLongAbrege() {
		String libelleLong = libelleLong();
		if (libelleLong.length() > 60)
			return libelleLong().substring(0, 59);
		else
			return libelleLong();
	}

	@SuppressWarnings("unchecked")
	public static NSArray<EOPlanComptable> fetchWithExercice(EOEditingContext ec, EOExercice exercice, String type) {
		EOQualifier qual = ERXQ.equals(TO_PLANCO_EXERS_KEY + "." + EOPlanComptableExer.TO_EXERCICE_KEY, exercice).and(
				ERXQ.equals(PCO_VALIDITE_KEY, PCO_VALIDITE_VALIDE));
		if (type != null) {
			qual = ERXQ.and(qual, ERXQ.equals(PCO_NATURE_KEY, type));
		}
		NSArray<EOPlanComptable> plancos = EOPlanComptable.fetchAll(
				ec, qual, ERXS.asc(EOPlanComptable.PCO_NUM_KEY).array());
		return plancos;
	}

}
