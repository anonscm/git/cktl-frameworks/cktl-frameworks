/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORecouvrement.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EORecouvrement extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Recouvrement";
	public static final String ENTITY_TABLE_NAME = "maracuja.RECOUVREMENT";


//Attribute Keys
	public static final ERXKey<NSTimestamp> RECO_DATE_CREATION = new ERXKey<NSTimestamp>("recoDateCreation");
	public static final ERXKey<java.math.BigDecimal> RECO_MONTANT = new ERXKey<java.math.BigDecimal>("recoMontant");
	public static final ERXKey<Integer> RECO_NB = new ERXKey<Integer>("recoNb");
	public static final ERXKey<Integer> RECO_NUMERO = new ERXKey<Integer>("recoNumero");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOComptabilite> TO_COMPTABILITE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOComptabilite>("toComptabilite");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> TO_PRELEVEMENT_FICHIERS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier>("toPrelevementFichiers");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance> TO_SEPA_SDD_ECHEANCES = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance>("toSepaSddEcheances");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement> TO_TYPE_RECOUVREMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement>("toTypeRecouvrement");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur> TO_UTILISATEUR = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur>("toUtilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "recoOrdre";

	public static final String RECO_DATE_CREATION_KEY = "recoDateCreation";
	public static final String RECO_MONTANT_KEY = "recoMontant";
	public static final String RECO_NB_KEY = "recoNb";
	public static final String RECO_NUMERO_KEY = "recoNumero";

//Attributs non visibles
	public static final String COM_ORDRE_KEY = "comOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String RECO_ORDRE_KEY = "recoOrdre";
	public static final String TREC_ORDRE_KEY = "trecOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String RECO_DATE_CREATION_COLKEY = "RECO_DATE_CREATION";
	public static final String RECO_MONTANT_COLKEY = "RECO_MONTANT";
	public static final String RECO_NB_COLKEY = "RECO_NB";
	public static final String RECO_NUMERO_COLKEY = "RECO_NUMERO";

	public static final String COM_ORDRE_COLKEY = "COM_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String RECO_ORDRE_COLKEY = "RECO_ORDRE";
	public static final String TREC_ORDRE_COLKEY = "TREC_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String TO_COMPTABILITE_KEY = "toComptabilite";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_PRELEVEMENT_FICHIERS_KEY = "toPrelevementFichiers";
	public static final String TO_SEPA_SDD_ECHEANCES_KEY = "toSepaSddEcheances";
	public static final String TO_TYPE_RECOUVREMENT_KEY = "toTypeRecouvrement";
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";



	// Accessors methods
	public NSTimestamp recoDateCreation() {
	 return (NSTimestamp) storedValueForKey(RECO_DATE_CREATION_KEY);
	}

	public void setRecoDateCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, RECO_DATE_CREATION_KEY);
	}

	public java.math.BigDecimal recoMontant() {
	 return (java.math.BigDecimal) storedValueForKey(RECO_MONTANT_KEY);
	}

	public void setRecoMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, RECO_MONTANT_KEY);
	}

	public Integer recoNb() {
	 return (Integer) storedValueForKey(RECO_NB_KEY);
	}

	public void setRecoNb(Integer value) {
	 takeStoredValueForKey(value, RECO_NB_KEY);
	}

	public Integer recoNumero() {
	 return (Integer) storedValueForKey(RECO_NUMERO_KEY);
	}

	public void setRecoNumero(Integer value) {
	 takeStoredValueForKey(value, RECO_NUMERO_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOComptabilite toComptabilite() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOComptabilite)storedValueForKey(TO_COMPTABILITE_KEY);
	}

	public void setToComptabiliteRelationship(org.cocktail.fwkcktlcompta.server.metier.EOComptabilite value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOComptabilite oldValue = toComptabilite();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COMPTABILITE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_COMPTABILITE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement toTypeRecouvrement() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement)storedValueForKey(TO_TYPE_RECOUVREMENT_KEY);
	}

	public void setToTypeRecouvrementRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement oldValue = toTypeRecouvrement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_RECOUVREMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_RECOUVREMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur toUtilisateur() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
	}

	public void setToUtilisateurRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur oldValue = toUtilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> toPrelevementFichiers() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier>)storedValueForKey(TO_PRELEVEMENT_FICHIERS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> toPrelevementFichiers(EOQualifier qualifier) {
	 return toPrelevementFichiers(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> toPrelevementFichiers(EOQualifier qualifier, Boolean fetch) {
	 return toPrelevementFichiers(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> toPrelevementFichiers(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier.TO_RECOUVREMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toPrelevementFichiers();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToPrelevementFichiersRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_PRELEVEMENT_FICHIERS_KEY);
	}
	
	public void removeFromToPrelevementFichiersRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRELEVEMENT_FICHIERS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier createToPrelevementFichiersRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRELEVEMENT_FICHIERS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier) eo;
	}
	
	public void deleteToPrelevementFichiersRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRELEVEMENT_FICHIERS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToPrelevementFichiersRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier> objects = toPrelevementFichiers().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToPrelevementFichiersRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance> toSepaSddEcheances() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance>)storedValueForKey(TO_SEPA_SDD_ECHEANCES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance> toSepaSddEcheances(EOQualifier qualifier) {
	 return toSepaSddEcheances(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance> toSepaSddEcheances(EOQualifier qualifier, Boolean fetch) {
	 return toSepaSddEcheances(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance> toSepaSddEcheances(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance.TO_RECOUVREMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toSepaSddEcheances();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToSepaSddEcheancesRelationship(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCES_KEY);
	}
	
	public void removeFromToSepaSddEcheancesRelationship(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCES_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance createToSepaSddEcheancesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_SEPA_SDD_ECHEANCES_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance) eo;
	}
	
	public void deleteToSepaSddEcheancesRelationship(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToSepaSddEcheancesRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance> objects = toSepaSddEcheances().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToSepaSddEcheancesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EORecouvrement avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EORecouvrement createEORecouvrement(EOEditingContext editingContext				, NSTimestamp recoDateCreation
							, java.math.BigDecimal recoMontant
							, Integer recoNb
							, Integer recoNumero
					, org.cocktail.fwkcktlcompta.server.metier.EOComptabilite toComptabilite		, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice		, org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement toTypeRecouvrement		, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur toUtilisateur					) {
	 EORecouvrement eo = (EORecouvrement) EOUtilities.createAndInsertInstance(editingContext, _EORecouvrement.ENTITY_NAME);	 
							eo.setRecoDateCreation(recoDateCreation);
									eo.setRecoMontant(recoMontant);
									eo.setRecoNb(recoNb);
									eo.setRecoNumero(recoNumero);
						 eo.setToComptabiliteRelationship(toComptabilite);
				 eo.setToExerciceRelationship(toExercice);
				 eo.setToTypeRecouvrementRelationship(toTypeRecouvrement);
				 eo.setToUtilisateurRelationship(toUtilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORecouvrement creerInstance(EOEditingContext editingContext) {
		EORecouvrement object = (EORecouvrement)EOUtilities.createAndInsertInstance(editingContext, _EORecouvrement.ENTITY_NAME);
  		return object;
		}

	

  public EORecouvrement localInstanceIn(EOEditingContext editingContext) {
    EORecouvrement localInstance = (EORecouvrement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecouvrement> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecouvrement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecouvrement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecouvrement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecouvrement> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecouvrement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecouvrement> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecouvrement>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORecouvrement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORecouvrement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORecouvrement> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORecouvrement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORecouvrement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORecouvrement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORecouvrement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORecouvrement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORecouvrement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORecouvrement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORecouvrement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORecouvrement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORecouvrement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORecouvrement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}