/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEmargementDetail.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOEmargementDetail extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_EmargementDetail";
	public static final String ENTITY_TABLE_NAME = "maracuja.Emargement_Detail";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> EMD_MONTANT = new ERXKey<java.math.BigDecimal>("emdMontant");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> TO_ECD_DESTINATION = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail>("toEcdDestination");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> TO_ECD_SOURCE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail>("toEcdSource");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEmargement> TO_EMARGEMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEmargement>("toEmargement");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice>("toExercice");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "emdOrdre";

	public static final String EMD_MONTANT_KEY = "emdMontant";

//Attributs non visibles
	public static final String ECD_ORDRE_DESTINATION_KEY = "ecdOrdreDestination";
	public static final String ECD_ORDRE_SOURCE_KEY = "ecdOrdreSource";
	public static final String EMA_ORDRE_KEY = "emaOrdre";
	public static final String EMD_ORDRE_KEY = "emdOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String EMD_MONTANT_COLKEY = "emd_Montant";

	public static final String ECD_ORDRE_DESTINATION_COLKEY = "ecd_ORDRE_DESTINATION";
	public static final String ECD_ORDRE_SOURCE_COLKEY = "ecd_ORDRE_SOURCE";
	public static final String EMA_ORDRE_COLKEY = "EMA_ORDRE";
	public static final String EMD_ORDRE_COLKEY = "emd_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String TO_ECD_DESTINATION_KEY = "toEcdDestination";
	public static final String TO_ECD_SOURCE_KEY = "toEcdSource";
	public static final String TO_EMARGEMENT_KEY = "toEmargement";
	public static final String TO_EXERCICE_KEY = "toExercice";



	// Accessors methods
	public java.math.BigDecimal emdMontant() {
	 return (java.math.BigDecimal) storedValueForKey(EMD_MONTANT_KEY);
	}

	public void setEmdMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EMD_MONTANT_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail toEcdDestination() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail)storedValueForKey(TO_ECD_DESTINATION_KEY);
	}

	public void setToEcdDestinationRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail oldValue = toEcdDestination();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ECD_DESTINATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ECD_DESTINATION_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail toEcdSource() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail)storedValueForKey(TO_ECD_SOURCE_KEY);
	}

	public void setToEcdSourceRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail oldValue = toEcdSource();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ECD_SOURCE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ECD_SOURCE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOEmargement toEmargement() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOEmargement)storedValueForKey(TO_EMARGEMENT_KEY);
	}

	public void setToEmargementRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEmargement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOEmargement oldValue = toEmargement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EMARGEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EMARGEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}


	/**
	* Créer une instance de EOEmargementDetail avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEmargementDetail createEOEmargementDetail(EOEditingContext editingContext				, java.math.BigDecimal emdMontant
					, org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail toEcdDestination		, org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail toEcdSource		, org.cocktail.fwkcktlcompta.server.metier.EOEmargement toEmargement		, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice					) {
	 EOEmargementDetail eo = (EOEmargementDetail) EOUtilities.createAndInsertInstance(editingContext, _EOEmargementDetail.ENTITY_NAME);	 
							eo.setEmdMontant(emdMontant);
						 eo.setToEcdDestinationRelationship(toEcdDestination);
				 eo.setToEcdSourceRelationship(toEcdSource);
				 eo.setToEmargementRelationship(toEmargement);
				 eo.setToExerciceRelationship(toExercice);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEmargementDetail creerInstance(EOEditingContext editingContext) {
		EOEmargementDetail object = (EOEmargementDetail)EOUtilities.createAndInsertInstance(editingContext, _EOEmargementDetail.ENTITY_NAME);
  		return object;
		}

	

  public EOEmargementDetail localInstanceIn(EOEditingContext editingContext) {
    EOEmargementDetail localInstance = (EOEmargementDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEmargementDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEmargementDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEmargementDetail> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEmargementDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEmargementDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEmargementDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEmargementDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEmargementDetail> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEmargementDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEmargementDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEmargementDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEmargementDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEmargementDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEmargementDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}