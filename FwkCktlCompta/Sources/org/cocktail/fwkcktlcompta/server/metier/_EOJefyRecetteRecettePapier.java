/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJefyRecetteRecettePapier.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOJefyRecetteRecettePapier extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_JefyRecette_RecettePapier";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.RECETTE_PAPIER";


//Attribute Keys
	public static final ERXKey<NSTimestamp> RPP_DATE_RECEPTION = new ERXKey<NSTimestamp>("rppDateReception");
	public static final ERXKey<NSTimestamp> RPP_DATE_RECETTE = new ERXKey<NSTimestamp>("rppDateRecette");
	public static final ERXKey<NSTimestamp> RPP_DATE_SAISIE = new ERXKey<NSTimestamp>("rppDateSaisie");
	public static final ERXKey<NSTimestamp> RPP_DATE_SERVICE_FAIT = new ERXKey<NSTimestamp>("rppDateServiceFait");
	public static final ERXKey<java.math.BigDecimal> RPP_HT_SAISIE = new ERXKey<java.math.BigDecimal>("rppHtSaisie");
	public static final ERXKey<Integer> RPP_NB_PIECE = new ERXKey<Integer>("rppNbPiece");
	public static final ERXKey<String> RPP_NUMERO = new ERXKey<String>("rppNumero");
	public static final ERXKey<java.math.BigDecimal> RPP_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("rppTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> RPP_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("rppTvaSaisie");
	public static final ERXKey<String> RPP_VISIBLE = new ERXKey<String>("rppVisible");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement> TO_MODE_RECOUVREMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement>("toModeRecouvrement");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne> TO_PERSONNE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne>("toPersonne");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> TO_RECETTES = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette>("toRecettes");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib> TO_RIB = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib>("toRib");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rppId";

	public static final String RPP_DATE_RECEPTION_KEY = "rppDateReception";
	public static final String RPP_DATE_RECETTE_KEY = "rppDateRecette";
	public static final String RPP_DATE_SAISIE_KEY = "rppDateSaisie";
	public static final String RPP_DATE_SERVICE_FAIT_KEY = "rppDateServiceFait";
	public static final String RPP_HT_SAISIE_KEY = "rppHtSaisie";
	public static final String RPP_NB_PIECE_KEY = "rppNbPiece";
	public static final String RPP_NUMERO_KEY = "rppNumero";
	public static final String RPP_TTC_SAISIE_KEY = "rppTtcSaisie";
	public static final String RPP_TVA_SAISIE_KEY = "rppTvaSaisie";
	public static final String RPP_VISIBLE_KEY = "rppVisible";

//Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String PERS_ID_KEY = "persId";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String RPP_ID_KEY = "rppId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String RPP_DATE_RECEPTION_COLKEY = "RPP_DATE_RECEPTION";
	public static final String RPP_DATE_RECETTE_COLKEY = "RPP_DATE_RECETTE";
	public static final String RPP_DATE_SAISIE_COLKEY = "RPP_DATE_SAISIE";
	public static final String RPP_DATE_SERVICE_FAIT_COLKEY = "RPP_DATE_SERVICE_FAIT";
	public static final String RPP_HT_SAISIE_COLKEY = "RPP_HT_SAISIE";
	public static final String RPP_NB_PIECE_COLKEY = "RPP_NB_PIECE";
	public static final String RPP_NUMERO_COLKEY = "RPP_NUMERO";
	public static final String RPP_TTC_SAISIE_COLKEY = "RPP_TTC_SAISIE";
	public static final String RPP_TVA_SAISIE_COLKEY = "RPP_TVA_SAISIE";
	public static final String RPP_VISIBLE_COLKEY = "RPP_VISIBLE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
	public static final String RPP_ID_COLKEY = "RPP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_MODE_RECOUVREMENT_KEY = "toModeRecouvrement";
	public static final String TO_PERSONNE_KEY = "toPersonne";
	public static final String TO_RECETTES_KEY = "toRecettes";
	public static final String TO_RIB_KEY = "toRib";



	// Accessors methods
	public NSTimestamp rppDateReception() {
	 return (NSTimestamp) storedValueForKey(RPP_DATE_RECEPTION_KEY);
	}

	public void setRppDateReception(NSTimestamp value) {
	 takeStoredValueForKey(value, RPP_DATE_RECEPTION_KEY);
	}

	public NSTimestamp rppDateRecette() {
	 return (NSTimestamp) storedValueForKey(RPP_DATE_RECETTE_KEY);
	}

	public void setRppDateRecette(NSTimestamp value) {
	 takeStoredValueForKey(value, RPP_DATE_RECETTE_KEY);
	}

	public NSTimestamp rppDateSaisie() {
	 return (NSTimestamp) storedValueForKey(RPP_DATE_SAISIE_KEY);
	}

	public void setRppDateSaisie(NSTimestamp value) {
	 takeStoredValueForKey(value, RPP_DATE_SAISIE_KEY);
	}

	public NSTimestamp rppDateServiceFait() {
	 return (NSTimestamp) storedValueForKey(RPP_DATE_SERVICE_FAIT_KEY);
	}

	public void setRppDateServiceFait(NSTimestamp value) {
	 takeStoredValueForKey(value, RPP_DATE_SERVICE_FAIT_KEY);
	}

	public java.math.BigDecimal rppHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(RPP_HT_SAISIE_KEY);
	}

	public void setRppHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, RPP_HT_SAISIE_KEY);
	}

	public Integer rppNbPiece() {
	 return (Integer) storedValueForKey(RPP_NB_PIECE_KEY);
	}

	public void setRppNbPiece(Integer value) {
	 takeStoredValueForKey(value, RPP_NB_PIECE_KEY);
	}

	public String rppNumero() {
	 return (String) storedValueForKey(RPP_NUMERO_KEY);
	}

	public void setRppNumero(String value) {
	 takeStoredValueForKey(value, RPP_NUMERO_KEY);
	}

	public java.math.BigDecimal rppTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(RPP_TTC_SAISIE_KEY);
	}

	public void setRppTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, RPP_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal rppTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(RPP_TVA_SAISIE_KEY);
	}

	public void setRppTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, RPP_TVA_SAISIE_KEY);
	}

	public String rppVisible() {
	 return (String) storedValueForKey(RPP_VISIBLE_KEY);
	}

	public void setRppVisible(String value) {
	 takeStoredValueForKey(value, RPP_VISIBLE_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement toModeRecouvrement() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement)storedValueForKey(TO_MODE_RECOUVREMENT_KEY);
	}

	public void setToModeRecouvrementRelationship(org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement oldValue = toModeRecouvrement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODE_RECOUVREMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODE_RECOUVREMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne toPersonne() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne)storedValueForKey(TO_PERSONNE_KEY);
	}

	public void setToPersonneRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne oldValue = toPersonne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib toRib() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib)storedValueForKey(TO_RIB_KEY);
	}

	public void setToRibRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib oldValue = toRib();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RIB_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_RIB_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> toRecettes() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette>)storedValueForKey(TO_RECETTES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> toRecettes(EOQualifier qualifier) {
	 return toRecettes(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> toRecettes(EOQualifier qualifier, Boolean fetch) {
	 return toRecettes(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> toRecettes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette.TO_RECETTE_PAPIER_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toRecettes();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToRecettesRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_RECETTES_KEY);
	}
	
	public void removeFromToRecettesRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RECETTES_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette createToRecettesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_RECETTES_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette) eo;
	}
	
	public void deleteToRecettesRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RECETTES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToRecettesRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> objects = toRecettes().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToRecettesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOJefyRecetteRecettePapier avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOJefyRecetteRecettePapier createEOJefyRecetteRecettePapier(EOEditingContext editingContext								, NSTimestamp rppDateSaisie
									, java.math.BigDecimal rppHtSaisie
							, Integer rppNbPiece
							, String rppNumero
							, java.math.BigDecimal rppTtcSaisie
							, java.math.BigDecimal rppTvaSaisie
							, String rppVisible
					, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice				, org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne toPersonne							) {
	 EOJefyRecetteRecettePapier eo = (EOJefyRecetteRecettePapier) EOUtilities.createAndInsertInstance(editingContext, _EOJefyRecetteRecettePapier.ENTITY_NAME);	 
											eo.setRppDateSaisie(rppDateSaisie);
											eo.setRppHtSaisie(rppHtSaisie);
									eo.setRppNbPiece(rppNbPiece);
									eo.setRppNumero(rppNumero);
									eo.setRppTtcSaisie(rppTtcSaisie);
									eo.setRppTvaSaisie(rppTvaSaisie);
									eo.setRppVisible(rppVisible);
						 eo.setToExerciceRelationship(toExercice);
						 eo.setToPersonneRelationship(toPersonne);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyRecetteRecettePapier creerInstance(EOEditingContext editingContext) {
		EOJefyRecetteRecettePapier object = (EOJefyRecetteRecettePapier)EOUtilities.createAndInsertInstance(editingContext, _EOJefyRecetteRecettePapier.ENTITY_NAME);
  		return object;
		}

	

  public EOJefyRecetteRecettePapier localInstanceIn(EOEditingContext editingContext) {
    EOJefyRecetteRecettePapier localInstance = (EOJefyRecetteRecettePapier)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOJefyRecetteRecettePapier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOJefyRecetteRecettePapier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOJefyRecetteRecettePapier> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJefyRecetteRecettePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJefyRecetteRecettePapier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJefyRecetteRecettePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJefyRecetteRecettePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOJefyRecetteRecettePapier> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJefyRecetteRecettePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJefyRecetteRecettePapier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOJefyRecetteRecettePapier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJefyRecetteRecettePapier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJefyRecetteRecettePapier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJefyRecetteRecettePapier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}