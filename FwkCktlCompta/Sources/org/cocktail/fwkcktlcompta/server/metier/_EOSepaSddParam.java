/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSepaSddParam.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOSepaSddParam extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddParam";
	public static final String ENTITY_TABLE_NAME = "maracuja.SEPA_SDD_PARAM";


//Attribute Keys
	public static final ERXKey<String> CREANCIER_ICS = new ERXKey<String>("creancierIcs");
	public static final ERXKey<String> DEVISE = new ERXKey<String>("devise");
	public static final ERXKey<String> DFT_IBAN = new ERXKey<String>("dftIban");
	public static final ERXKey<String> DFT_TITULAIRE = new ERXKey<String>("dftTitulaire");
	public static final ERXKey<String> EMETTEUR_NOM = new ERXKey<String>("emetteurNom");
	public static final ERXKey<String> ETAT = new ERXKey<String>("etat");
	public static final ERXKey<Integer> ID_SEPA_SDD_PARAM = new ERXKey<Integer>("idSepaSddParam");
	public static final ERXKey<String> TENEUR_COMPTE_BIC = new ERXKey<String>("teneurCompteBic");
	public static final ERXKey<String> TENEUR_COMPTE_CODIQUE = new ERXKey<String>("teneurCompteCodique");
	public static final ERXKey<String> TENEUR_COMPTE_IBAN = new ERXKey<String>("teneurCompteIban");
	public static final ERXKey<String> TENEUR_COMPTE_NOM = new ERXKey<String>("teneurCompteNom");
	public static final ERXKey<String> TRANSFERT_ID = new ERXKey<String>("transfertId");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne> TO_PERSONNE_CREANCIER = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne>("toPersonneCreancier");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement> TO_TYPE_RECOUVREMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement>("toTypeRecouvrement");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idSepaSddParam";

	public static final String CREANCIER_ICS_KEY = "creancierIcs";
	public static final String DEVISE_KEY = "devise";
	public static final String DFT_IBAN_KEY = "dftIban";
	public static final String DFT_TITULAIRE_KEY = "dftTitulaire";
	public static final String EMETTEUR_NOM_KEY = "emetteurNom";
	public static final String ETAT_KEY = "etat";
	public static final String ID_SEPA_SDD_PARAM_KEY = "idSepaSddParam";
	public static final String TENEUR_COMPTE_BIC_KEY = "teneurCompteBic";
	public static final String TENEUR_COMPTE_CODIQUE_KEY = "teneurCompteCodique";
	public static final String TENEUR_COMPTE_IBAN_KEY = "teneurCompteIban";
	public static final String TENEUR_COMPTE_NOM_KEY = "teneurCompteNom";
	public static final String TRANSFERT_ID_KEY = "transfertId";

//Attributs non visibles
	public static final String CREANCIER_PERS_ID_KEY = "creancierPersId";
	public static final String TREC_ORDRE_KEY = "trecOrdre";

//Colonnes dans la base de donnees
	public static final String CREANCIER_ICS_COLKEY = "CREANCIER_ICS";
	public static final String DEVISE_COLKEY = "DEVISE";
	public static final String DFT_IBAN_COLKEY = "DFT_IBAN";
	public static final String DFT_TITULAIRE_COLKEY = "DFT_TITULAIRE";
	public static final String EMETTEUR_NOM_COLKEY = "EMETTEUR_NOM";
	public static final String ETAT_COLKEY = "ETAT";
	public static final String ID_SEPA_SDD_PARAM_COLKEY = "ID_SEPA_SDD_PARAM";
	public static final String TENEUR_COMPTE_BIC_COLKEY = "TENEUR_COMPTE_BIC";
	public static final String TENEUR_COMPTE_CODIQUE_COLKEY = "TENEUR_COMPTE_CODIQUE";
	public static final String TENEUR_COMPTE_IBAN_COLKEY = "TENEUR_COMPTE_IBAN";
	public static final String TENEUR_COMPTE_NOM_COLKEY = "TENEUR_COMPTE_NOM";
	public static final String TRANSFERT_ID_COLKEY = "TRANSFERT_ID";

	public static final String CREANCIER_PERS_ID_COLKEY = "CREANCIER_PERS_ID";
	public static final String TREC_ORDRE_COLKEY = "TREC_ORDRE";


	// Relationships
	public static final String TO_PERSONNE_CREANCIER_KEY = "toPersonneCreancier";
	public static final String TO_TYPE_RECOUVREMENT_KEY = "toTypeRecouvrement";



	// Accessors methods
	public String creancierIcs() {
	 return (String) storedValueForKey(CREANCIER_ICS_KEY);
	}

	public void setCreancierIcs(String value) {
	 takeStoredValueForKey(value, CREANCIER_ICS_KEY);
	}

	public String devise() {
	 return (String) storedValueForKey(DEVISE_KEY);
	}

	public void setDevise(String value) {
	 takeStoredValueForKey(value, DEVISE_KEY);
	}

	public String dftIban() {
	 return (String) storedValueForKey(DFT_IBAN_KEY);
	}

	public void setDftIban(String value) {
	 takeStoredValueForKey(value, DFT_IBAN_KEY);
	}

	public String dftTitulaire() {
	 return (String) storedValueForKey(DFT_TITULAIRE_KEY);
	}

	public void setDftTitulaire(String value) {
	 takeStoredValueForKey(value, DFT_TITULAIRE_KEY);
	}

	public String emetteurNom() {
	 return (String) storedValueForKey(EMETTEUR_NOM_KEY);
	}

	public void setEmetteurNom(String value) {
	 takeStoredValueForKey(value, EMETTEUR_NOM_KEY);
	}

	public String etat() {
	 return (String) storedValueForKey(ETAT_KEY);
	}

	public void setEtat(String value) {
	 takeStoredValueForKey(value, ETAT_KEY);
	}

	public Integer idSepaSddParam() {
	 return (Integer) storedValueForKey(ID_SEPA_SDD_PARAM_KEY);
	}

	public void setIdSepaSddParam(Integer value) {
	 takeStoredValueForKey(value, ID_SEPA_SDD_PARAM_KEY);
	}

	public String teneurCompteBic() {
	 return (String) storedValueForKey(TENEUR_COMPTE_BIC_KEY);
	}

	public void setTeneurCompteBic(String value) {
	 takeStoredValueForKey(value, TENEUR_COMPTE_BIC_KEY);
	}

	public String teneurCompteCodique() {
	 return (String) storedValueForKey(TENEUR_COMPTE_CODIQUE_KEY);
	}

	public void setTeneurCompteCodique(String value) {
	 takeStoredValueForKey(value, TENEUR_COMPTE_CODIQUE_KEY);
	}

	public String teneurCompteIban() {
	 return (String) storedValueForKey(TENEUR_COMPTE_IBAN_KEY);
	}

	public void setTeneurCompteIban(String value) {
	 takeStoredValueForKey(value, TENEUR_COMPTE_IBAN_KEY);
	}

	public String teneurCompteNom() {
	 return (String) storedValueForKey(TENEUR_COMPTE_NOM_KEY);
	}

	public void setTeneurCompteNom(String value) {
	 takeStoredValueForKey(value, TENEUR_COMPTE_NOM_KEY);
	}

	public String transfertId() {
	 return (String) storedValueForKey(TRANSFERT_ID_KEY);
	}

	public void setTransfertId(String value) {
	 takeStoredValueForKey(value, TRANSFERT_ID_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne toPersonneCreancier() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne)storedValueForKey(TO_PERSONNE_CREANCIER_KEY);
	}

	public void setToPersonneCreancierRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne oldValue = toPersonneCreancier();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_CREANCIER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_CREANCIER_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement toTypeRecouvrement() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement)storedValueForKey(TO_TYPE_RECOUVREMENT_KEY);
	}

	public void setToTypeRecouvrementRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement oldValue = toTypeRecouvrement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_RECOUVREMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_RECOUVREMENT_KEY);
	 }
	}


	/**
	* Créer une instance de EOSepaSddParam avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOSepaSddParam createEOSepaSddParam(EOEditingContext editingContext				, String creancierIcs
							, String devise
							, String dftIban
							, String dftTitulaire
							, String emetteurNom
							, String etat
							, Integer idSepaSddParam
							, String teneurCompteBic
							, String teneurCompteCodique
							, String teneurCompteIban
							, String teneurCompteNom
							, String transfertId
					, org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne toPersonneCreancier		, org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement toTypeRecouvrement					) {
	 EOSepaSddParam eo = (EOSepaSddParam) EOUtilities.createAndInsertInstance(editingContext, _EOSepaSddParam.ENTITY_NAME);	 
							eo.setCreancierIcs(creancierIcs);
									eo.setDevise(devise);
									eo.setDftIban(dftIban);
									eo.setDftTitulaire(dftTitulaire);
									eo.setEmetteurNom(emetteurNom);
									eo.setEtat(etat);
									eo.setIdSepaSddParam(idSepaSddParam);
									eo.setTeneurCompteBic(teneurCompteBic);
									eo.setTeneurCompteCodique(teneurCompteCodique);
									eo.setTeneurCompteIban(teneurCompteIban);
									eo.setTeneurCompteNom(teneurCompteNom);
									eo.setTransfertId(transfertId);
						 eo.setToPersonneCreancierRelationship(toPersonneCreancier);
				 eo.setToTypeRecouvrementRelationship(toTypeRecouvrement);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSepaSddParam creerInstance(EOEditingContext editingContext) {
		EOSepaSddParam object = (EOSepaSddParam)EOUtilities.createAndInsertInstance(editingContext, _EOSepaSddParam.ENTITY_NAME);
  		return object;
		}

	

  public EOSepaSddParam localInstanceIn(EOEditingContext editingContext) {
    EOSepaSddParam localInstance = (EOSepaSddParam)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSepaSddParam fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSepaSddParam fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOSepaSddParam> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSepaSddParam eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSepaSddParam)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSepaSddParam fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSepaSddParam fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOSepaSddParam> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSepaSddParam eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSepaSddParam)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSepaSddParam fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSepaSddParam eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSepaSddParam ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSepaSddParam fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}