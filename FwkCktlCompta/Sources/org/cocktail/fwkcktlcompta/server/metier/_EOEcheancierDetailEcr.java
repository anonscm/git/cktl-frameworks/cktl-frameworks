/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEcheancierDetailEcr.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOEcheancierDetailEcr extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_EcheancierDetailEcr";
	public static final String ENTITY_TABLE_NAME = "maracuja.echeancier_Detail_Ecr";


//Attribute Keys
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcheancier> TO_ECHEANCIER = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcheancier>("toEcheancier");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> TO_ECRITURE_DETAIL = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail>("toEcritureDetail");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "edeId";


//Attributs non visibles
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String ECHE_ECHEANCIER_ORDRE_KEY = "echeEcheancierOrdre";
	public static final String EDE_ID_KEY = "edeId";

//Colonnes dans la base de donnees

	public static final String ECD_ORDRE_COLKEY = "ecd_ORDRE";
	public static final String ECHE_ECHEANCIER_ORDRE_COLKEY = "ECHE_ECHEANCIER_ORDRE";
	public static final String EDE_ID_COLKEY = "EDE_ID";


	// Relationships
	public static final String TO_ECHEANCIER_KEY = "toEcheancier";
	public static final String TO_ECRITURE_DETAIL_KEY = "toEcritureDetail";



	// Accessors methods
	public org.cocktail.fwkcktlcompta.server.metier.EOEcheancier toEcheancier() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOEcheancier)storedValueForKey(TO_ECHEANCIER_KEY);
	}

	public void setToEcheancierRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcheancier value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOEcheancier oldValue = toEcheancier();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ECHEANCIER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ECHEANCIER_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail toEcritureDetail() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail)storedValueForKey(TO_ECRITURE_DETAIL_KEY);
	}

	public void setToEcritureDetailRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail oldValue = toEcritureDetail();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ECRITURE_DETAIL_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ECRITURE_DETAIL_KEY);
	 }
	}


	/**
	* Créer une instance de EOEcheancierDetailEcr avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEcheancierDetailEcr createEOEcheancierDetailEcr(EOEditingContext editingContext		, org.cocktail.fwkcktlcompta.server.metier.EOEcheancier toEcheancier		, org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail toEcritureDetail					) {
	 EOEcheancierDetailEcr eo = (EOEcheancierDetailEcr) EOUtilities.createAndInsertInstance(editingContext, _EOEcheancierDetailEcr.ENTITY_NAME);	 
				 eo.setToEcheancierRelationship(toEcheancier);
				 eo.setToEcritureDetailRelationship(toEcritureDetail);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEcheancierDetailEcr creerInstance(EOEditingContext editingContext) {
		EOEcheancierDetailEcr object = (EOEcheancierDetailEcr)EOUtilities.createAndInsertInstance(editingContext, _EOEcheancierDetailEcr.ENTITY_NAME);
  		return object;
		}

	

  public EOEcheancierDetailEcr localInstanceIn(EOEditingContext editingContext) {
    EOEcheancierDetailEcr localInstance = (EOEcheancierDetailEcr)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierDetailEcr> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierDetailEcr> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierDetailEcr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierDetailEcr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierDetailEcr> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierDetailEcr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierDetailEcr> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcheancierDetailEcr>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEcheancierDetailEcr fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEcheancierDetailEcr fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEcheancierDetailEcr> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEcheancierDetailEcr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEcheancierDetailEcr)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEcheancierDetailEcr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEcheancierDetailEcr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEcheancierDetailEcr> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEcheancierDetailEcr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEcheancierDetailEcr)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEcheancierDetailEcr fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEcheancierDetailEcr eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEcheancierDetailEcr ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEcheancierDetailEcr fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}