/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEmargement.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOEmargement extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Emargement";
	public static final String ENTITY_TABLE_NAME = "maracuja.Emargement";


//Attribute Keys
	public static final ERXKey<NSTimestamp> EMA_DATE = new ERXKey<NSTimestamp>("emaDate");
	public static final ERXKey<String> EMA_ETAT = new ERXKey<String>("emaEtat");
	public static final ERXKey<java.math.BigDecimal> EMA_MONTANT = new ERXKey<java.math.BigDecimal>("emaMontant");
	public static final ERXKey<Integer> EMA_NUMERO = new ERXKey<Integer>("emaNumero");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOComptabilite> TO_COMPTABILITE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOComptabilite>("toComptabilite");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> TO_EMARGEMENT_DETAILS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail>("toEmargementDetails");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement> TO_TYPE_EMARGEMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement>("toTypeEmargement");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur> TO_UTILISATEUR = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur>("toUtilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "emaOrdre";

	public static final String EMA_DATE_KEY = "emaDate";
	public static final String EMA_ETAT_KEY = "emaEtat";
	public static final String EMA_MONTANT_KEY = "emaMontant";
	public static final String EMA_NUMERO_KEY = "emaNumero";

//Attributs non visibles
	public static final String COM_ORDRE_KEY = "comOrdre";
	public static final String EMA_ORDRE_KEY = "emaOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TEM_ORDRE_KEY = "temOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String EMA_DATE_COLKEY = "ema_Date";
	public static final String EMA_ETAT_COLKEY = "ema_etat";
	public static final String EMA_MONTANT_COLKEY = "ema_montant";
	public static final String EMA_NUMERO_COLKEY = "ema_Numero";

	public static final String COM_ORDRE_COLKEY = "com_ordre";
	public static final String EMA_ORDRE_COLKEY = "EMA_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TEM_ORDRE_COLKEY = "tem_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String TO_COMPTABILITE_KEY = "toComptabilite";
	public static final String TO_EMARGEMENT_DETAILS_KEY = "toEmargementDetails";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_TYPE_EMARGEMENT_KEY = "toTypeEmargement";
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";



	// Accessors methods
	public NSTimestamp emaDate() {
	 return (NSTimestamp) storedValueForKey(EMA_DATE_KEY);
	}

	public void setEmaDate(NSTimestamp value) {
	 takeStoredValueForKey(value, EMA_DATE_KEY);
	}

	public String emaEtat() {
	 return (String) storedValueForKey(EMA_ETAT_KEY);
	}

	public void setEmaEtat(String value) {
	 takeStoredValueForKey(value, EMA_ETAT_KEY);
	}

	public java.math.BigDecimal emaMontant() {
	 return (java.math.BigDecimal) storedValueForKey(EMA_MONTANT_KEY);
	}

	public void setEmaMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EMA_MONTANT_KEY);
	}

	public Integer emaNumero() {
	 return (Integer) storedValueForKey(EMA_NUMERO_KEY);
	}

	public void setEmaNumero(Integer value) {
	 takeStoredValueForKey(value, EMA_NUMERO_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOComptabilite toComptabilite() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOComptabilite)storedValueForKey(TO_COMPTABILITE_KEY);
	}

	public void setToComptabiliteRelationship(org.cocktail.fwkcktlcompta.server.metier.EOComptabilite value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOComptabilite oldValue = toComptabilite();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COMPTABILITE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_COMPTABILITE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement toTypeEmargement() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement)storedValueForKey(TO_TYPE_EMARGEMENT_KEY);
	}

	public void setToTypeEmargementRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement oldValue = toTypeEmargement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_EMARGEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_EMARGEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur toUtilisateur() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
	}

	public void setToUtilisateurRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur oldValue = toUtilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> toEmargementDetails() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail>)storedValueForKey(TO_EMARGEMENT_DETAILS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> toEmargementDetails(EOQualifier qualifier) {
	 return toEmargementDetails(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> toEmargementDetails(EOQualifier qualifier, Boolean fetch) {
	 return toEmargementDetails(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> toEmargementDetails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail.TO_EMARGEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toEmargementDetails();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToEmargementDetailsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_EMARGEMENT_DETAILS_KEY);
	}
	
	public void removeFromToEmargementDetailsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EMARGEMENT_DETAILS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail createToEmargementDetailsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_EMARGEMENT_DETAILS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail) eo;
	}
	
	public void deleteToEmargementDetailsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EMARGEMENT_DETAILS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToEmargementDetailsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOEmargementDetail> objects = toEmargementDetails().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToEmargementDetailsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOEmargement avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEmargement createEOEmargement(EOEditingContext editingContext				, NSTimestamp emaDate
							, String emaEtat
							, java.math.BigDecimal emaMontant
							, Integer emaNumero
					, org.cocktail.fwkcktlcompta.server.metier.EOComptabilite toComptabilite		, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice		, org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement toTypeEmargement		, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur toUtilisateur					) {
	 EOEmargement eo = (EOEmargement) EOUtilities.createAndInsertInstance(editingContext, _EOEmargement.ENTITY_NAME);	 
							eo.setEmaDate(emaDate);
									eo.setEmaEtat(emaEtat);
									eo.setEmaMontant(emaMontant);
									eo.setEmaNumero(emaNumero);
						 eo.setToComptabiliteRelationship(toComptabilite);
				 eo.setToExerciceRelationship(toExercice);
				 eo.setToTypeEmargementRelationship(toTypeEmargement);
				 eo.setToUtilisateurRelationship(toUtilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEmargement creerInstance(EOEditingContext editingContext) {
		EOEmargement object = (EOEmargement)EOUtilities.createAndInsertInstance(editingContext, _EOEmargement.ENTITY_NAME);
  		return object;
		}

	

  public EOEmargement localInstanceIn(EOEditingContext editingContext) {
    EOEmargement localInstance = (EOEmargement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargement> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargement> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargement> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEmargement>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEmargement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEmargement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEmargement> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEmargement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEmargement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEmargement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEmargement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEmargement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEmargement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEmargement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEmargement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEmargement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEmargement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEmargement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}