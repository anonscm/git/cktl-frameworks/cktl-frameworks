/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGrhumFournis.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOGrhumFournis extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Grhum_Fournis";
	public static final String ENTITY_TABLE_NAME = "GRHUM.FOURNIS_ULR";


//Attribute Keys
	public static final ERXKey<Integer> CPT_ORDRE = new ERXKey<Integer>("cptOrdre");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> FOU_CODE = new ERXKey<String>("fouCode");
	public static final ERXKey<NSTimestamp> FOU_DATE = new ERXKey<NSTimestamp>("fouDate");
	public static final ERXKey<String> FOU_ETRANGER = new ERXKey<String>("fouEtranger");
	public static final ERXKey<String> FOU_MARCHE = new ERXKey<String>("fouMarche");
	public static final ERXKey<String> FOU_TYPE = new ERXKey<String>("fouType");
	public static final ERXKey<String> FOU_VALIDE = new ERXKey<String>("fouValide");
	public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne> TO_PERSONNE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne>("toPersonne");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib> TO_RIBS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib>("toRibs");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fouOrdre";

	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String FOU_CODE_KEY = "fouCode";
	public static final String FOU_DATE_KEY = "fouDate";
	public static final String FOU_ETRANGER_KEY = "fouEtranger";
	public static final String FOU_MARCHE_KEY = "fouMarche";
	public static final String FOU_TYPE_KEY = "fouType";
	public static final String FOU_VALIDE_KEY = "fouValide";
	public static final String PERS_ID_KEY = "persId";

//Attributs non visibles
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String AGT_ORDRE_KEY = "agtOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";

//Colonnes dans la base de donnees
	public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String FOU_CODE_COLKEY = "FOU_CODE";
	public static final String FOU_DATE_COLKEY = "FOU_DATE";
	public static final String FOU_ETRANGER_COLKEY = "FOU_ETRANGER";
	public static final String FOU_MARCHE_COLKEY = "FOU_MARCHE";
	public static final String FOU_TYPE_COLKEY = "FOU_TYPE";
	public static final String FOU_VALIDE_COLKEY = "FOU_VALIDE";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String AGT_ORDRE_COLKEY = "AGT_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";


	// Relationships
	public static final String TO_PERSONNE_KEY = "toPersonne";
	public static final String TO_RIBS_KEY = "toRibs";



	// Accessors methods
	public Integer cptOrdre() {
	 return (Integer) storedValueForKey(CPT_ORDRE_KEY);
	}

	public void setCptOrdre(Integer value) {
	 takeStoredValueForKey(value, CPT_ORDRE_KEY);
	}

	public NSTimestamp dCreation() {
	 return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dModification() {
	 return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
	 takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	public String fouCode() {
	 return (String) storedValueForKey(FOU_CODE_KEY);
	}

	public void setFouCode(String value) {
	 takeStoredValueForKey(value, FOU_CODE_KEY);
	}

	public NSTimestamp fouDate() {
	 return (NSTimestamp) storedValueForKey(FOU_DATE_KEY);
	}

	public void setFouDate(NSTimestamp value) {
	 takeStoredValueForKey(value, FOU_DATE_KEY);
	}

	public String fouEtranger() {
	 return (String) storedValueForKey(FOU_ETRANGER_KEY);
	}

	public void setFouEtranger(String value) {
	 takeStoredValueForKey(value, FOU_ETRANGER_KEY);
	}

	public String fouMarche() {
	 return (String) storedValueForKey(FOU_MARCHE_KEY);
	}

	public void setFouMarche(String value) {
	 takeStoredValueForKey(value, FOU_MARCHE_KEY);
	}

	public String fouType() {
	 return (String) storedValueForKey(FOU_TYPE_KEY);
	}

	public void setFouType(String value) {
	 takeStoredValueForKey(value, FOU_TYPE_KEY);
	}

	public String fouValide() {
	 return (String) storedValueForKey(FOU_VALIDE_KEY);
	}

	public void setFouValide(String value) {
	 takeStoredValueForKey(value, FOU_VALIDE_KEY);
	}

	public Integer persId() {
	 return (Integer) storedValueForKey(PERS_ID_KEY);
	}

	public void setPersId(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne toPersonne() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne)storedValueForKey(TO_PERSONNE_KEY);
	}

	public void setToPersonneRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne oldValue = toPersonne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib> toRibs() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib>)storedValueForKey(TO_RIBS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib> toRibs(EOQualifier qualifier) {
	 return toRibs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib> toRibs(EOQualifier qualifier, Boolean fetch) {
	 return toRibs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib> toRibs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib.TO_FOURNIS_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toRibs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToRibsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_RIBS_KEY);
	}
	
	public void removeFromToRibsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RIBS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib createToRibsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_RIBS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib) eo;
	}
	
	public void deleteToRibsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RIBS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToRibsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib> objects = toRibs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToRibsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOGrhumFournis avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOGrhumFournis createEOGrhumFournis(EOEditingContext editingContext						, NSTimestamp dCreation
							, NSTimestamp dModification
							, String fouCode
							, NSTimestamp fouDate
							, String fouEtranger
							, String fouMarche
							, String fouType
							, String fouValide
							, Integer persId
					, org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne toPersonne					) {
	 EOGrhumFournis eo = (EOGrhumFournis) EOUtilities.createAndInsertInstance(editingContext, _EOGrhumFournis.ENTITY_NAME);	 
									eo.setDCreation(dCreation);
									eo.setDModification(dModification);
									eo.setFouCode(fouCode);
									eo.setFouDate(fouDate);
									eo.setFouEtranger(fouEtranger);
									eo.setFouMarche(fouMarche);
									eo.setFouType(fouType);
									eo.setFouValide(fouValide);
									eo.setPersId(persId);
						 eo.setToPersonneRelationship(toPersonne);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGrhumFournis creerInstance(EOEditingContext editingContext) {
		EOGrhumFournis object = (EOGrhumFournis)EOUtilities.createAndInsertInstance(editingContext, _EOGrhumFournis.ENTITY_NAME);
  		return object;
		}

	

  public EOGrhumFournis localInstanceIn(EOEditingContext editingContext) {
    EOGrhumFournis localInstance = (EOGrhumFournis)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGrhumFournis fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGrhumFournis fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGrhumFournis> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGrhumFournis eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGrhumFournis)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGrhumFournis fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGrhumFournis fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGrhumFournis> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGrhumFournis eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGrhumFournis)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGrhumFournis fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGrhumFournis eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGrhumFournis ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGrhumFournis fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}