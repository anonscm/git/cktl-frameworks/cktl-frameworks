/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTitre.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOTitre extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Titre";
	public static final String ENTITY_TABLE_NAME = "maracuja.Titre";


//Attribute Keys
	public static final ERXKey<Integer> BOR_ORDRE = new ERXKey<Integer>("borOrdre");
	public static final ERXKey<Integer> ORG_ORDRE = new ERXKey<Integer>("orgOrdre");
	public static final ERXKey<Integer> PREST_ID = new ERXKey<Integer>("prestId");
	public static final ERXKey<NSTimestamp> TIT_DATE_REMISE = new ERXKey<NSTimestamp>("titDateRemise");
	public static final ERXKey<NSTimestamp> TIT_DATE_VISA_PRINC = new ERXKey<NSTimestamp>("titDateVisaPrinc");
	public static final ERXKey<String> TIT_ETAT = new ERXKey<String>("titEtat");
	public static final ERXKey<String> TIT_ETAT_REMISE = new ERXKey<String>("titEtatRemise");
	public static final ERXKey<java.math.BigDecimal> TIT_HT = new ERXKey<java.math.BigDecimal>("titHt");
	public static final ERXKey<String> TIT_LIBELLE = new ERXKey<String>("titLibelle");
	public static final ERXKey<String> TIT_MOTIF_REJET = new ERXKey<String>("titMotifRejet");
	public static final ERXKey<Integer> TIT_NB_PIECE = new ERXKey<Integer>("titNbPiece");
	public static final ERXKey<Integer> TIT_NUMERO = new ERXKey<Integer>("titNumero");
	public static final ERXKey<Integer> TIT_NUMERO_REJET = new ERXKey<Integer>("titNumeroRejet");
	public static final ERXKey<Integer> TIT_ORDRE = new ERXKey<Integer>("titOrdre");
	public static final ERXKey<Integer> TIT_ORIGINE_KEY = new ERXKey<Integer>("titOrigineKey");
	public static final ERXKey<String> TIT_ORIGINE_LIB = new ERXKey<String>("titOrigineLib");
	public static final ERXKey<java.math.BigDecimal> TIT_TTC = new ERXKey<java.math.BigDecimal>("titTtc");
	public static final ERXKey<java.math.BigDecimal> TIT_TVA = new ERXKey<java.math.BigDecimal>("titTva");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereau> TO_BORDEREAU = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereau>("toBordereau");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereauRejet> TO_BORDEREAU_REJET = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereauRejet>("toBordereauRejet");
	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> TO_FOURNIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>("toFournis");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestion> TO_GESTION = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestion>("toGestion");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOModePaiement> TO_MODE_PAIEMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOModePaiement>("toModePaiement");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement> TO_MODE_RECOUVREMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement>("toModeRecouvrement");
	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan> TO_ORGAN = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan>("toOrgan");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOOrigine> TO_ORIGINE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOOrigine>("toOrigine");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> TO_PLAN_COMPTABLE_EXER = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer>("toPlanComptableExer");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> TO_RECETTE_CTRL_PLANCOS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco>("toRecetteCtrlPlancos");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EORecette> TO_RECETTES = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EORecette>("toRecettes");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORib> TO_RIB = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORib>("toRib");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard> TO_TITRE_BROUILLARDS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard>("toTitreBrouillards");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> TO_TITRE_DETAIL_ECRITURES = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture>("toTitreDetailEcritures");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau> TO_TYPE_ORIGINE_BORDEREAU = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau>("toTypeOrigineBordereau");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "titId";

	public static final String BOR_ORDRE_KEY = "borOrdre";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String PREST_ID_KEY = "prestId";
	public static final String TIT_DATE_REMISE_KEY = "titDateRemise";
	public static final String TIT_DATE_VISA_PRINC_KEY = "titDateVisaPrinc";
	public static final String TIT_ETAT_KEY = "titEtat";
	public static final String TIT_ETAT_REMISE_KEY = "titEtatRemise";
	public static final String TIT_HT_KEY = "titHt";
	public static final String TIT_LIBELLE_KEY = "titLibelle";
	public static final String TIT_MOTIF_REJET_KEY = "titMotifRejet";
	public static final String TIT_NB_PIECE_KEY = "titNbPiece";
	public static final String TIT_NUMERO_KEY = "titNumero";
	public static final String TIT_NUMERO_REJET_KEY = "titNumeroRejet";
	public static final String TIT_ORDRE_KEY = "titOrdre";
	public static final String TIT_ORIGINE_KEY_KEY = "titOrigineKey";
	public static final String TIT_ORIGINE_LIB_KEY = "titOrigineLib";
	public static final String TIT_TTC_KEY = "titTtc";
	public static final String TIT_TVA_KEY = "titTva";

//Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String BRJ_ORDRE_KEY = "brjOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String PAI_ORDRE_KEY = "paiOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String RIB_ORDRE_COMPTABLE_KEY = "ribOrdreComptable";
	public static final String RIB_ORDRE_ORDONNATEUR_KEY = "ribOrdreOrdonnateur";
	public static final String TIT_ID_KEY = "titId";
	public static final String TOR_ORDRE_KEY = "torOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String BOR_ORDRE_COLKEY = "BOR_ORDRE";
	public static final String ORG_ORDRE_COLKEY = "org_ordre";
	public static final String PREST_ID_COLKEY = "prest_id";
	public static final String TIT_DATE_REMISE_COLKEY = "tit_Date_Remise";
	public static final String TIT_DATE_VISA_PRINC_COLKEY = "tit_Date_Visa_Princ";
	public static final String TIT_ETAT_COLKEY = "tit_etat";
	public static final String TIT_ETAT_REMISE_COLKEY = "tit_Etat_Remise";
	public static final String TIT_HT_COLKEY = "tit_Ht";
	public static final String TIT_LIBELLE_COLKEY = "tit_libelle";
	public static final String TIT_MOTIF_REJET_COLKEY = "tit_Motif_Rejet";
	public static final String TIT_NB_PIECE_COLKEY = "TIT_NB_PIECE";
	public static final String TIT_NUMERO_COLKEY = "tit_Numero";
	public static final String TIT_NUMERO_REJET_COLKEY = "tit_Numero_rejet";
	public static final String TIT_ORDRE_COLKEY = "TIT_ORDRE";
	public static final String TIT_ORIGINE_KEY_COLKEY = "tit_orgine_key";
	public static final String TIT_ORIGINE_LIB_COLKEY = "tit_Origine_Lib";
	public static final String TIT_TTC_COLKEY = "tit_Ttc";
	public static final String TIT_TVA_COLKEY = "tit_Tva";

	public static final String BOR_ID_COLKEY = "bor_id";
	public static final String BRJ_ORDRE_COLKEY = "BRJ_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String ORI_ORDRE_COLKEY = "ori_ordre";
	public static final String PAI_ORDRE_COLKEY = "pai_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String RIB_ORDRE_COMPTABLE_COLKEY = "rib_ordre_comptable";
	public static final String RIB_ORDRE_ORDONNATEUR_COLKEY = "rib_ordre_ordonnateur";
	public static final String TIT_ID_COLKEY = "TIT_ID";
	public static final String TOR_ORDRE_COLKEY = "tor_Ordre";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String TO_BORDEREAU_KEY = "toBordereau";
	public static final String TO_BORDEREAU_REJET_KEY = "toBordereauRejet";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_FOURNIS_KEY = "toFournis";
	public static final String TO_GESTION_KEY = "toGestion";
	public static final String TO_MODE_PAIEMENT_KEY = "toModePaiement";
	public static final String TO_MODE_RECOUVREMENT_KEY = "toModeRecouvrement";
	public static final String TO_ORGAN_KEY = "toOrgan";
	public static final String TO_ORIGINE_KEY = "toOrigine";
	public static final String TO_PLAN_COMPTABLE_EXER_KEY = "toPlanComptableExer";
	public static final String TO_RECETTE_CTRL_PLANCOS_KEY = "toRecetteCtrlPlancos";
	public static final String TO_RECETTES_KEY = "toRecettes";
	public static final String TO_RIB_KEY = "toRib";
	public static final String TO_TITRE_BROUILLARDS_KEY = "toTitreBrouillards";
	public static final String TO_TITRE_DETAIL_ECRITURES_KEY = "toTitreDetailEcritures";
	public static final String TO_TYPE_ORIGINE_BORDEREAU_KEY = "toTypeOrigineBordereau";



	// Accessors methods
	public Integer borOrdre() {
	 return (Integer) storedValueForKey(BOR_ORDRE_KEY);
	}

	public void setBorOrdre(Integer value) {
	 takeStoredValueForKey(value, BOR_ORDRE_KEY);
	}

	public Integer orgOrdre() {
	 return (Integer) storedValueForKey(ORG_ORDRE_KEY);
	}

	public void setOrgOrdre(Integer value) {
	 takeStoredValueForKey(value, ORG_ORDRE_KEY);
	}

	public Integer prestId() {
	 return (Integer) storedValueForKey(PREST_ID_KEY);
	}

	public void setPrestId(Integer value) {
	 takeStoredValueForKey(value, PREST_ID_KEY);
	}

	public NSTimestamp titDateRemise() {
	 return (NSTimestamp) storedValueForKey(TIT_DATE_REMISE_KEY);
	}

	public void setTitDateRemise(NSTimestamp value) {
	 takeStoredValueForKey(value, TIT_DATE_REMISE_KEY);
	}

	public NSTimestamp titDateVisaPrinc() {
	 return (NSTimestamp) storedValueForKey(TIT_DATE_VISA_PRINC_KEY);
	}

	public void setTitDateVisaPrinc(NSTimestamp value) {
	 takeStoredValueForKey(value, TIT_DATE_VISA_PRINC_KEY);
	}

	public String titEtat() {
	 return (String) storedValueForKey(TIT_ETAT_KEY);
	}

	public void setTitEtat(String value) {
	 takeStoredValueForKey(value, TIT_ETAT_KEY);
	}

	public String titEtatRemise() {
	 return (String) storedValueForKey(TIT_ETAT_REMISE_KEY);
	}

	public void setTitEtatRemise(String value) {
	 takeStoredValueForKey(value, TIT_ETAT_REMISE_KEY);
	}

	public java.math.BigDecimal titHt() {
	 return (java.math.BigDecimal) storedValueForKey(TIT_HT_KEY);
	}

	public void setTitHt(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, TIT_HT_KEY);
	}

	public String titLibelle() {
	 return (String) storedValueForKey(TIT_LIBELLE_KEY);
	}

	public void setTitLibelle(String value) {
	 takeStoredValueForKey(value, TIT_LIBELLE_KEY);
	}

	public String titMotifRejet() {
	 return (String) storedValueForKey(TIT_MOTIF_REJET_KEY);
	}

	public void setTitMotifRejet(String value) {
	 takeStoredValueForKey(value, TIT_MOTIF_REJET_KEY);
	}

	public Integer titNbPiece() {
	 return (Integer) storedValueForKey(TIT_NB_PIECE_KEY);
	}

	public void setTitNbPiece(Integer value) {
	 takeStoredValueForKey(value, TIT_NB_PIECE_KEY);
	}

	public Integer titNumero() {
	 return (Integer) storedValueForKey(TIT_NUMERO_KEY);
	}

	public void setTitNumero(Integer value) {
	 takeStoredValueForKey(value, TIT_NUMERO_KEY);
	}

	public Integer titNumeroRejet() {
	 return (Integer) storedValueForKey(TIT_NUMERO_REJET_KEY);
	}

	public void setTitNumeroRejet(Integer value) {
	 takeStoredValueForKey(value, TIT_NUMERO_REJET_KEY);
	}

	public Integer titOrdre() {
	 return (Integer) storedValueForKey(TIT_ORDRE_KEY);
	}

	public void setTitOrdre(Integer value) {
	 takeStoredValueForKey(value, TIT_ORDRE_KEY);
	}

	public Integer titOrigineKey() {
	 return (Integer) storedValueForKey(TIT_ORIGINE_KEY_KEY);
	}

	public void setTitOrigineKey(Integer value) {
	 takeStoredValueForKey(value, TIT_ORIGINE_KEY_KEY);
	}

	public String titOrigineLib() {
	 return (String) storedValueForKey(TIT_ORIGINE_LIB_KEY);
	}

	public void setTitOrigineLib(String value) {
	 takeStoredValueForKey(value, TIT_ORIGINE_LIB_KEY);
	}

	public java.math.BigDecimal titTtc() {
	 return (java.math.BigDecimal) storedValueForKey(TIT_TTC_KEY);
	}

	public void setTitTtc(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, TIT_TTC_KEY);
	}

	public java.math.BigDecimal titTva() {
	 return (java.math.BigDecimal) storedValueForKey(TIT_TVA_KEY);
	}

	public void setTitTva(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, TIT_TVA_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOBordereau toBordereau() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBordereau)storedValueForKey(TO_BORDEREAU_KEY);
	}

	public void setToBordereauRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereau value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOBordereau oldValue = toBordereau();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BORDEREAU_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_BORDEREAU_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOBordereauRejet toBordereauRejet() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBordereauRejet)storedValueForKey(TO_BORDEREAU_REJET_KEY);
	}

	public void setToBordereauRejetRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereauRejet value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOBordereauRejet oldValue = toBordereauRejet();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BORDEREAU_REJET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_BORDEREAU_REJET_KEY);
	 }
	}

	public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
	 return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFournis() {
	 return (org.cocktail.fwkcktlpersonne.common.metier.EOFournis)storedValueForKey(TO_FOURNIS_KEY);
	}

	public void setToFournisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlpersonne.common.metier.EOFournis oldValue = toFournis();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FOURNIS_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_FOURNIS_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGestion toGestion() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
	}

	public void setToGestionRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGestion value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGestion oldValue = toGestion();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOModePaiement toModePaiement() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOModePaiement)storedValueForKey(TO_MODE_PAIEMENT_KEY);
	}

	public void setToModePaiementRelationship(org.cocktail.fwkcktlcompta.server.metier.EOModePaiement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOModePaiement oldValue = toModePaiement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODE_PAIEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODE_PAIEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement toModeRecouvrement() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement)storedValueForKey(TO_MODE_RECOUVREMENT_KEY);
	}

	public void setToModeRecouvrementRelationship(org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement oldValue = toModeRecouvrement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODE_RECOUVREMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODE_RECOUVREMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan toOrgan() {
	 return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(TO_ORGAN_KEY);
	}

	public void setToOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
	 if (value == null) {
	 	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = toOrgan();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORGAN_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORGAN_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOOrigine toOrigine() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOOrigine)storedValueForKey(TO_ORIGINE_KEY);
	}

	public void setToOrigineRelationship(org.cocktail.fwkcktlcompta.server.metier.EOOrigine value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOOrigine oldValue = toOrigine();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORIGINE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORIGINE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer toPlanComptableExer() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer)storedValueForKey(TO_PLAN_COMPTABLE_EXER_KEY);
	}

	public void setToPlanComptableExerRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer oldValue = toPlanComptableExer();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_EXER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_EXER_KEY);
	 }
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EORib toRib() {
	 return (org.cocktail.fwkcktlpersonne.common.metier.EORib)storedValueForKey(TO_RIB_KEY);
	}

	public void setToRibRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORib value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlpersonne.common.metier.EORib oldValue = toRib();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RIB_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_RIB_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau toTypeOrigineBordereau() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau)storedValueForKey(TO_TYPE_ORIGINE_BORDEREAU_KEY);
	}

	public void setToTypeOrigineBordereauRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau oldValue = toTypeOrigineBordereau();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ORIGINE_BORDEREAU_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ORIGINE_BORDEREAU_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> toRecetteCtrlPlancos() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco>)storedValueForKey(TO_RECETTE_CTRL_PLANCOS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> toRecetteCtrlPlancos(EOQualifier qualifier) {
	 return toRecetteCtrlPlancos(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> toRecetteCtrlPlancos(EOQualifier qualifier, Boolean fetch) {
	 return toRecetteCtrlPlancos(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> toRecetteCtrlPlancos(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco.TO_TITRE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toRecetteCtrlPlancos();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToRecetteCtrlPlancosRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_RECETTE_CTRL_PLANCOS_KEY);
	}
	
	public void removeFromToRecetteCtrlPlancosRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RECETTE_CTRL_PLANCOS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco createToRecetteCtrlPlancosRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_RECETTE_CTRL_PLANCOS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco) eo;
	}
	
	public void deleteToRecetteCtrlPlancosRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RECETTE_CTRL_PLANCOS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToRecetteCtrlPlancosRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> objects = toRecetteCtrlPlancos().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToRecetteCtrlPlancosRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecette> toRecettes() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecette>)storedValueForKey(TO_RECETTES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecette> toRecettes(EOQualifier qualifier) {
	 return toRecettes(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecette> toRecettes(EOQualifier qualifier, Boolean fetch) {
	 return toRecettes(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecette> toRecettes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecette> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EORecette.TO_TITRE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EORecette.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toRecettes();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecette>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EORecette>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToRecettesRelationship(org.cocktail.fwkcktlcompta.server.metier.EORecette object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_RECETTES_KEY);
	}
	
	public void removeFromToRecettesRelationship(org.cocktail.fwkcktlcompta.server.metier.EORecette object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RECETTES_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EORecette createToRecettesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EORecette.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_RECETTES_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EORecette) eo;
	}
	
	public void deleteToRecettesRelationship(org.cocktail.fwkcktlcompta.server.metier.EORecette object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RECETTES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToRecettesRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EORecette> objects = toRecettes().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToRecettesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard> toTitreBrouillards() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard>)storedValueForKey(TO_TITRE_BROUILLARDS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard> toTitreBrouillards(EOQualifier qualifier) {
	 return toTitreBrouillards(qualifier, null);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard> toTitreBrouillards(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard> results;
			   results = toTitreBrouillards();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 return results;
	}
	
	public void addToToTitreBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_TITRE_BROUILLARDS_KEY);
	}
	
	public void removeFromToTitreBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_TITRE_BROUILLARDS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard createToTitreBrouillardsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_TITRE_BROUILLARDS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard) eo;
	}
	
	public void deleteToTitreBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_TITRE_BROUILLARDS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToTitreBrouillardsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOTitreBrouillard> objects = toTitreBrouillards().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToTitreBrouillardsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> toTitreDetailEcritures() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture>)storedValueForKey(TO_TITRE_DETAIL_ECRITURES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> toTitreDetailEcritures(EOQualifier qualifier) {
	 return toTitreDetailEcritures(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> toTitreDetailEcritures(EOQualifier qualifier, Boolean fetch) {
	 return toTitreDetailEcritures(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> toTitreDetailEcritures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture.TO_TITRE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toTitreDetailEcritures();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToTitreDetailEcrituresRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_TITRE_DETAIL_ECRITURES_KEY);
	}
	
	public void removeFromToTitreDetailEcrituresRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_TITRE_DETAIL_ECRITURES_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture createToTitreDetailEcrituresRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_TITRE_DETAIL_ECRITURES_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture) eo;
	}
	
	public void deleteToTitreDetailEcrituresRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_TITRE_DETAIL_ECRITURES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToTitreDetailEcrituresRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOTitreDetailEcriture> objects = toTitreDetailEcritures().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToTitreDetailEcrituresRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOTitre avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOTitre createEOTitre(EOEditingContext editingContext				, Integer borOrdre
															, String titEtat
									, java.math.BigDecimal titHt
													, Integer titNumero
									, Integer titOrdre
											, java.math.BigDecimal titTtc
							, java.math.BigDecimal titTva
					, org.cocktail.fwkcktlcompta.server.metier.EOBordereau toBordereau				, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice		, org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFournis		, org.cocktail.fwkcktlcompta.server.metier.EOGestion toGestion										, org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer toPlanComptableExer				, org.cocktail.fwkcktlcompta.server.metier.EOTypeOrigineBordereau toTypeOrigineBordereau					) {
	 EOTitre eo = (EOTitre) EOUtilities.createAndInsertInstance(editingContext, _EOTitre.ENTITY_NAME);	 
							eo.setBorOrdre(borOrdre);
																	eo.setTitEtat(titEtat);
											eo.setTitHt(titHt);
															eo.setTitNumero(titNumero);
											eo.setTitOrdre(titOrdre);
													eo.setTitTtc(titTtc);
									eo.setTitTva(titTva);
						 eo.setToBordereauRelationship(toBordereau);
						 eo.setToExerciceRelationship(toExercice);
				 eo.setToFournisRelationship(toFournis);
				 eo.setToGestionRelationship(toGestion);
												 eo.setToPlanComptableExerRelationship(toPlanComptableExer);
						 eo.setToTypeOrigineBordereauRelationship(toTypeOrigineBordereau);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTitre creerInstance(EOEditingContext editingContext) {
		EOTitre object = (EOTitre)EOUtilities.createAndInsertInstance(editingContext, _EOTitre.ENTITY_NAME);
  		return object;
		}

	

  public EOTitre localInstanceIn(EOEditingContext editingContext) {
    EOTitre localInstance = (EOTitre)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOTitre>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTitre fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTitre fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTitre> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTitre eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTitre)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTitre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTitre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTitre> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTitre eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTitre)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTitre fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTitre eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTitre ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTitre fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}