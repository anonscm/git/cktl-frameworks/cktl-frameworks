/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

public class EOTypeJournal extends _EOTypeJournal {
	// permet d assurer la possibilite de faire un like JOURNAL%
	// pour selectionner toutes les ecritures des JOURNAUX
	private static final String journal = "JOURNAL ";
	// type a afficher dans l interface de saisie des  ecritures.
	public static final String typeJournalExercice = journal + "EXERCICE";
	public static final String typeJournalBalanceEntree = journal + "BALANCE ENTREE";
	public static final String typeJournalFinExercice = journal + "FIN EXERCICE";
	public static final String typeJournalVisaMandat = typeJournalExercice + " VISA MANDAT";
	public static final String typeJournalVisaTitre = typeJournalExercice + " VISA TITRE";
	public static final String typeJournalVisaOp = typeJournalExercice + " VISA ORDRE DE PAIEMENT";
	public static final String typeJournalOrdreDePaiement = typeJournalExercice + " ORDRE DE PAIEMENT";
	public static final String typeJournalReimputation = typeJournalExercice + " REIMPUTATION";
	public static final String typeJournalPaiement = typeJournalExercice + " PAIEMENT";
	public static final String typeJournalVisaPrestationInterne = typeJournalExercice + " VISA PRESTATION INTERNE";
	public static final String typeJournalModifPaiement = typeJournalExercice + " MODIF MODE PAIEMENT";
	public static final String typeJournalBordCheque = typeJournalExercice + " VISA BORDEREAU CHEQUE";
	public static final String typeJournalVisaDU = typeJournalExercice + " VISA D.U.";
	public static final String typeJournalVisaRembDU = typeJournalExercice + " VISA REMB. D.U.";
	public static final String typeJournalVisaTaxe = typeJournalExercice + " VISA TAXE APPRENTISSAGE";
	public static final String typeJournalRecouvrement = typeJournalExercice + " RECOUVREMENT";
	public static final String typeJournalSalaires = typeJournalExercice + " SALAIRES";
	public static final String typeJournalGrandLivreValeursInactives = journal + "GRAND LIVRE DES VALEURS INACTIVES";

	public EOTypeJournal() {
		super();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	public static EOTypeJournal getTypeJournalDefaut(EOEditingContext edc) {
		return getTypeJournalExercice(edc);
	}

	public static EOTypeJournal getTypeJournalExercice(EOEditingContext edc) {
		return EOTypeJournal.fetchByKeyValue(edc, EOTypeJournal.TJO_LIBELLE_KEY, EOTypeJournal.typeJournalExercice);
	}

}
