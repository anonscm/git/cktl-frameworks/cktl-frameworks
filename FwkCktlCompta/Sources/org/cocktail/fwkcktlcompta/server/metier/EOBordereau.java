/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.common.entities.IBordereau;
import org.cocktail.fwkcktlcompta.server.sp.SpNumeroterBordereau;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOBordereau extends _EOBordereau implements IBordereau {
	public static final String BOR_NB_KEY = "borNb";
	public static final String BOR_MONTANT_KEY = "borMontant";

	public static final String BOR_ETAT_VALIDE = "VALIDE";
	public static final String BOR_ETAT_VISE = "VISE";
	public static final String BOR_ETAT_ANNULE = "ANNULE";
	public static final String BOR_ETAT_A_VISER = "A VISER";
	public static final String BOR_ETAT_PAIEMENT = "PAIEMENT";
	public static final String BOR_ETAT_RETENUES = "RETENUES";
	public static final String BOR_ETAT_PAYE = "PAYE";

	public static final EOSortOrdering SORT_GES_CODE = EOSortOrdering.sortOrderingWithKey(EOBordereau.GES_CODE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_BOR_NUM_DESC = EOSortOrdering.sortOrderingWithKey(EOBordereau.BOR_NUM_KEY, EOSortOrdering.CompareDescending);
	public static final EOSortOrdering SORT_BOR_DATE_CREATION_DESC = EOSortOrdering.sortOrderingWithKey(EOBordereau.BOR_DATE_CREATION_KEY, EOSortOrdering.CompareDescending);

	public static final EOQualifier QUAL_SOUS_TYPE_DOSSIER_LIQ = new EOKeyValueQualifier(EOBordereau.TO_TYPE_BORDEREAU_KEY + "." + EOTypeBordereau.TBO_SOUS_TYPE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeBordereau.SOUS_TYPE_DOSSIER_LIQ);
	public static final EOQualifier QUAL_BOR_ETAT_VALIDE = new EOKeyValueQualifier(EOBordereau.BOR_ETAT_KEY, EOQualifier.QualifierOperatorEqual, BOR_ETAT_VALIDE);

	public EOBordereau() {
		super();
	}

	/**
	 * Peut etre appele a partir des factories. Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();

	}

	/**
	 * @param key
	 * @param values
	 * @return Un OrQualifier construit à partir des valeurs passées en parametre (par exemple générer un orqualifier à partir d'un nsarray de clés
	 *         d'enregistrement).
	 */
	public static EOOrQualifier qualifierOrForNSArrayOfValues(String key, NSArray values) {
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		for (int i = 0; i < values.count(); i++) {
			Object value = values.objectAtIndex(i);
			quals.addObject(new EOKeyValueQualifier(key, EOQualifier.QualifierOperatorEqual, value));
		}
		return new EOOrQualifier(quals);
	}

	public static NSArray<EOBordereau> fetchAllDossiersLiquidation(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		EOQualifier qual = QUAL_SOUS_TYPE_DOSSIER_LIQ;
		if (qualifier != null) {
			qual = new EOAndQualifier(new NSArray(new Object[] {
					qual, qualifier
			}));
		}
		return fetchAll(editingContext, qual, sortOrderings);
	}

	/**
	 * @return Le montant du bordereau (en fonction du type de bordereau)
	 * @see EOBordereau#isBordereauDepenses()
	 * @see EOBordereau#isBordereauRecettes()
	 */
	public BigDecimal borMontant() {
		if (isBordereauDepenses()) {
			return calcSommeOfBigDecimals(toMandats(), "manTtc");
		}
		else if (isBordereauRecettes()) {
			return calcSommeOfBigDecimals(toTitres(), "titTtc");
		}

		else {
			return null;
		}
	}

	public final Integer getNbMandats() {
		return new Integer(toMandats().count());
	}

	public final Integer getNbTitres() {
		return new Integer(toTitres().count());
	}

	public final Integer borNb() {
		if (isBordereauDepenses()) {
			return getNbMandats();
		}
		else {
			return getNbTitres();
		}
	}

	public boolean isBordereauBTME() {
		return EOTypeBordereau.TypeBordereauBTME.equals(toTypeBordereau().tboType());
	}

	public String gescodeAndNum() {
		return toGestion().gesCode() + "/" + borNum();
	}

	/**
	 * @return true si le bordereau a des mandats associés.
	 */
	public boolean isBordereauDepenses() {
		return (toMandats() != null && toMandats().count() != 0);
	}

	/**
	 * @return true si le bordereau a des titres associes.
	 */
	public boolean isBordereauRecettes() {
		return (toTitres() != null && toTitres().count() != 0);
	}

	/**
	 * @return Les dépenses du bordereau (en passant par les mandats).
	 */
	public NSArray toDepenses() {
		NSMutableArray<EODepense> res = new NSMutableArray<EODepense>();
		NSArray mandats = toMandats();
		for (int i = 0; i < mandats.count(); i++) {
			EOMandat mandat = (EOMandat) mandats.objectAtIndex(i);
			res.addObjectsFromArray(mandat.toDepenses());
		}
		return res.immutableClone();
	}

	public boolean isRejete() {
		return BOR_ETAT_ANNULE.equals(borEtat());
	}

	public boolean isValide() {
		return BOR_ETAT_VALIDE.equals(borEtat());
	}

	public boolean isVise() {
		return BOR_ETAT_VISE.equals(borEtat());
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setBorEtat(BOR_ETAT_VALIDE);
		setBorDateCreation(DateCtrl.now());
		setBorOrdre(Integer.valueOf(0));
	}

	/**
	 * @param editingContext
	 * @param exercice
	 * @param borOrdre
	 * @param gestion
	 * @param typeBordereau
	 * @param utilisateur
	 * @return
	 */
	public static final EOBordereau creer(EOEditingContext editingContext, EOExercice exercice, EOGestion gestion, EOTypeBordereau typeBordereau, EOUtilisateur utilisateur) {
		EOBordereau bord = EOBordereau.creerInstance(editingContext);

		bord.setToExerciceRelationship(exercice);
		bord.setToGestionRelationship(gestion);
		bord.setToTypeBordereauRelationship(typeBordereau);
		bord.setToUtilisateurRelationship(utilisateur);
		return bord;
	}

	/**
	 * Numerote le bordereau (en passant par une procedure stockée. L'objet doit etre enregistré en base avant de faire appel à cette méthode).
	 * 
	 * @throws Exception
	 */
	public void numeroter() throws Exception {
		if (editingContext().hasChanges()) {
			throw new Exception("Veuillez enregistrer les modifications en attente avant de numeroter.");
		}
		SpNumeroterBordereau.numeroter(editingContext(), this);
	}

}
