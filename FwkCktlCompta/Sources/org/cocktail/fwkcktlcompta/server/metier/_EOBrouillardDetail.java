/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBrouillardDetail.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOBrouillardDetail extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_BrouillardDetail";
	public static final String ENTITY_TABLE_NAME = "maracuja.brouillard_Detail";


//Attribute Keys
	public static final ERXKey<String> BROD_COMMENTAIRE = new ERXKey<String>("brodCommentaire");
	public static final ERXKey<java.math.BigDecimal> BROD_CREDIT = new ERXKey<java.math.BigDecimal>("brodCredit");
	public static final ERXKey<java.math.BigDecimal> BROD_DEBIT = new ERXKey<java.math.BigDecimal>("brodDebit");
	public static final ERXKey<Integer> BROD_INDEX = new ERXKey<Integer>("brodIndex");
	public static final ERXKey<String> BROD_LIBELLE = new ERXKey<String>("brodLibelle");
	public static final ERXKey<String> BROD_PCO_LIBELLE = new ERXKey<String>("brodPcoLibelle");
	public static final ERXKey<String> BROD_PCO_NUM = new ERXKey<String>("brodPcoNum");
	public static final ERXKey<String> BROD_POSTIT = new ERXKey<String>("brodPostit");
	public static final ERXKey<String> BROD_SENS = new ERXKey<String>("brodSens");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> TO_BROUILLARD = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard>("toBrouillard");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> TO_ECRITURE_DETAIL = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail>("toEcritureDetail");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestion> TO_GESTION = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestion>("toGestion");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "brodId";

	public static final String BROD_COMMENTAIRE_KEY = "brodCommentaire";
	public static final String BROD_CREDIT_KEY = "brodCredit";
	public static final String BROD_DEBIT_KEY = "brodDebit";
	public static final String BROD_INDEX_KEY = "brodIndex";
	public static final String BROD_LIBELLE_KEY = "brodLibelle";
	public static final String BROD_PCO_LIBELLE_KEY = "brodPcoLibelle";
	public static final String BROD_PCO_NUM_KEY = "brodPcoNum";
	public static final String BROD_POSTIT_KEY = "brodPostit";
	public static final String BROD_SENS_KEY = "brodSens";

//Attributs non visibles
	public static final String BROD_ID_KEY = "brodId";
	public static final String BRO_ID_KEY = "broId";
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String GES_CODE_KEY = "gesCode";

//Colonnes dans la base de donnees
	public static final String BROD_COMMENTAIRE_COLKEY = "brod_commentaire";
	public static final String BROD_CREDIT_COLKEY = "brod_credit";
	public static final String BROD_DEBIT_COLKEY = "brod_debit";
	public static final String BROD_INDEX_COLKEY = "brod_index";
	public static final String BROD_LIBELLE_COLKEY = "brod_libelle";
	public static final String BROD_PCO_LIBELLE_COLKEY = "brod_pco_libelle";
	public static final String BROD_PCO_NUM_COLKEY = "brod_PCO_NUM";
	public static final String BROD_POSTIT_COLKEY = "brod_Postit";
	public static final String BROD_SENS_COLKEY = "brod_sens";

	public static final String BROD_ID_COLKEY = "brod_id";
	public static final String BRO_ID_COLKEY = "bro_id";
	public static final String ECD_ORDRE_COLKEY = "ecd_Ordre";
	public static final String GES_CODE_COLKEY = "ges_code";


	// Relationships
	public static final String TO_BROUILLARD_KEY = "toBrouillard";
	public static final String TO_ECRITURE_DETAIL_KEY = "toEcritureDetail";
	public static final String TO_GESTION_KEY = "toGestion";



	// Accessors methods
	public String brodCommentaire() {
	 return (String) storedValueForKey(BROD_COMMENTAIRE_KEY);
	}

	public void setBrodCommentaire(String value) {
	 takeStoredValueForKey(value, BROD_COMMENTAIRE_KEY);
	}

	public java.math.BigDecimal brodCredit() {
	 return (java.math.BigDecimal) storedValueForKey(BROD_CREDIT_KEY);
	}

	public void setBrodCredit(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, BROD_CREDIT_KEY);
	}

	public java.math.BigDecimal brodDebit() {
	 return (java.math.BigDecimal) storedValueForKey(BROD_DEBIT_KEY);
	}

	public void setBrodDebit(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, BROD_DEBIT_KEY);
	}

	public Integer brodIndex() {
	 return (Integer) storedValueForKey(BROD_INDEX_KEY);
	}

	public void setBrodIndex(Integer value) {
	 takeStoredValueForKey(value, BROD_INDEX_KEY);
	}

	public String brodLibelle() {
	 return (String) storedValueForKey(BROD_LIBELLE_KEY);
	}

	public void setBrodLibelle(String value) {
	 takeStoredValueForKey(value, BROD_LIBELLE_KEY);
	}

	public String brodPcoLibelle() {
	 return (String) storedValueForKey(BROD_PCO_LIBELLE_KEY);
	}

	public void setBrodPcoLibelle(String value) {
	 takeStoredValueForKey(value, BROD_PCO_LIBELLE_KEY);
	}

	public String brodPcoNum() {
	 return (String) storedValueForKey(BROD_PCO_NUM_KEY);
	}

	public void setBrodPcoNum(String value) {
	 takeStoredValueForKey(value, BROD_PCO_NUM_KEY);
	}

	public String brodPostit() {
	 return (String) storedValueForKey(BROD_POSTIT_KEY);
	}

	public void setBrodPostit(String value) {
	 takeStoredValueForKey(value, BROD_POSTIT_KEY);
	}

	public String brodSens() {
	 return (String) storedValueForKey(BROD_SENS_KEY);
	}

	public void setBrodSens(String value) {
	 takeStoredValueForKey(value, BROD_SENS_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOBrouillard toBrouillard() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBrouillard)storedValueForKey(TO_BROUILLARD_KEY);
	}

	public void setToBrouillardRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillard value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOBrouillard oldValue = toBrouillard();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BROUILLARD_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_BROUILLARD_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail toEcritureDetail() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail)storedValueForKey(TO_ECRITURE_DETAIL_KEY);
	}

	public void setToEcritureDetailRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail oldValue = toEcritureDetail();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ECRITURE_DETAIL_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ECRITURE_DETAIL_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGestion toGestion() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
	}

	public void setToGestionRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGestion value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGestion oldValue = toGestion();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
	 }
	}


	/**
	* Créer une instance de EOBrouillardDetail avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOBrouillardDetail createEOBrouillardDetail(EOEditingContext editingContext						, java.math.BigDecimal brodCredit
							, java.math.BigDecimal brodDebit
							, Integer brodIndex
											, String brodPcoNum
									, String brodSens
					, org.cocktail.fwkcktlcompta.server.metier.EOBrouillard toBrouillard				, org.cocktail.fwkcktlcompta.server.metier.EOGestion toGestion					) {
	 EOBrouillardDetail eo = (EOBrouillardDetail) EOUtilities.createAndInsertInstance(editingContext, _EOBrouillardDetail.ENTITY_NAME);	 
									eo.setBrodCredit(brodCredit);
									eo.setBrodDebit(brodDebit);
									eo.setBrodIndex(brodIndex);
													eo.setBrodPcoNum(brodPcoNum);
											eo.setBrodSens(brodSens);
						 eo.setToBrouillardRelationship(toBrouillard);
						 eo.setToGestionRelationship(toGestion);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBrouillardDetail creerInstance(EOEditingContext editingContext) {
		EOBrouillardDetail object = (EOBrouillardDetail)EOUtilities.createAndInsertInstance(editingContext, _EOBrouillardDetail.ENTITY_NAME);
  		return object;
		}

	

  public EOBrouillardDetail localInstanceIn(EOEditingContext editingContext) {
    EOBrouillardDetail localInstance = (EOBrouillardDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBrouillardDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBrouillardDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOBrouillardDetail> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBrouillardDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBrouillardDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBrouillardDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBrouillardDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOBrouillardDetail> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBrouillardDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBrouillardDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBrouillardDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBrouillardDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBrouillardDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBrouillardDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}