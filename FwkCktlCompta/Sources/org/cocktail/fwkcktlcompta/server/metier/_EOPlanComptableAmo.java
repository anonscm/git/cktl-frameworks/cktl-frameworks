/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPlanComptableAmo.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOPlanComptableAmo extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_PlanComptableAmo";
	public static final String ENTITY_TABLE_NAME = "maracuja.Plan_Comptable_amo";


//Attribute Keys
	public static final ERXKey<String> PCOA_LIBELLE = new ERXKey<String>("pcoaLibelle");
	public static final ERXKey<String> PCOA_NUM = new ERXKey<String>("pcoaNum");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat> TO_TYPE_ETAT = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat>("toTypeEtat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pcoaId";

	public static final String PCOA_LIBELLE_KEY = "pcoaLibelle";
	public static final String PCOA_NUM_KEY = "pcoaNum";

//Attributs non visibles
	public static final String PCOA_ID_KEY = "pcoaId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String PCOA_LIBELLE_COLKEY = "pcoa_libelle";
	public static final String PCOA_NUM_COLKEY = "PCOa_NUM";

	public static final String PCOA_ID_COLKEY = "pcoa_id";
	public static final String TYET_ID_COLKEY = "tyet_id";


	// Relationships
	public static final String TO_TYPE_ETAT_KEY = "toTypeEtat";



	// Accessors methods
	public String pcoaLibelle() {
	 return (String) storedValueForKey(PCOA_LIBELLE_KEY);
	}

	public void setPcoaLibelle(String value) {
	 takeStoredValueForKey(value, PCOA_LIBELLE_KEY);
	}

	public String pcoaNum() {
	 return (String) storedValueForKey(PCOA_NUM_KEY);
	}

	public void setPcoaNum(String value) {
	 takeStoredValueForKey(value, PCOA_NUM_KEY);
	}

	public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat toTypeEtat() {
	 return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat)storedValueForKey(TO_TYPE_ETAT_KEY);
	}

	public void setToTypeEtatRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat oldValue = toTypeEtat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ETAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ETAT_KEY);
	 }
	}


	/**
	* Créer une instance de EOPlanComptableAmo avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOPlanComptableAmo createEOPlanComptableAmo(EOEditingContext editingContext				, String pcoaLibelle
							, String pcoaNum
					, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat toTypeEtat					) {
	 EOPlanComptableAmo eo = (EOPlanComptableAmo) EOUtilities.createAndInsertInstance(editingContext, _EOPlanComptableAmo.ENTITY_NAME);	 
							eo.setPcoaLibelle(pcoaLibelle);
									eo.setPcoaNum(pcoaNum);
						 eo.setToTypeEtatRelationship(toTypeEtat);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPlanComptableAmo creerInstance(EOEditingContext editingContext) {
		EOPlanComptableAmo object = (EOPlanComptableAmo)EOUtilities.createAndInsertInstance(editingContext, _EOPlanComptableAmo.ENTITY_NAME);
  		return object;
		}

	

  public EOPlanComptableAmo localInstanceIn(EOEditingContext editingContext) {
    EOPlanComptableAmo localInstance = (EOPlanComptableAmo)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableAmo> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableAmo> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableAmo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableAmo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableAmo> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableAmo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableAmo> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableAmo>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPlanComptableAmo fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPlanComptableAmo fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPlanComptableAmo> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPlanComptableAmo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPlanComptableAmo)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPlanComptableAmo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPlanComptableAmo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPlanComptableAmo> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPlanComptableAmo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPlanComptableAmo)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPlanComptableAmo fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPlanComptableAmo eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPlanComptableAmo ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPlanComptableAmo fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}