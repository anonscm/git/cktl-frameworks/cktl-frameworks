/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSepaSddOrigineType.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOSepaSddOrigineType extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddOrigineType";
	public static final String ENTITY_TABLE_NAME = "maracuja.SEPA_SDD_ORIGINE_TYPE";


//Attribute Keys
	public static final ERXKey<String> GENERATION_ECRITURES = new ERXKey<String>("generationEcritures");
	public static final ERXKey<String> TYPE_CLASS = new ERXKey<String>("typeClass");
	public static final ERXKey<String> TYPE_CODE = new ERXKey<String>("typeCode");
	public static final ERXKey<String> TYPE_COMMENTAIRE = new ERXKey<String>("typeCommentaire");
	public static final ERXKey<String> TYPE_NOM = new ERXKey<String>("typeNom");
	public static final ERXKey<Integer> TYPE_ORDRE = new ERXKey<Integer>("typeOrdre");
	public static final ERXKey<String> TYPE_TABLE = new ERXKey<String>("typeTable");
	public static final ERXKey<String> TYPE_TABLE_CLE = new ERXKey<String>("typeTableCle");
	public static final ERXKey<String> VALIDE = new ERXKey<String>("valide");
// Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idSepaSddOrigineType";

	public static final String GENERATION_ECRITURES_KEY = "generationEcritures";
	public static final String TYPE_CLASS_KEY = "typeClass";
	public static final String TYPE_CODE_KEY = "typeCode";
	public static final String TYPE_COMMENTAIRE_KEY = "typeCommentaire";
	public static final String TYPE_NOM_KEY = "typeNom";
	public static final String TYPE_ORDRE_KEY = "typeOrdre";
	public static final String TYPE_TABLE_KEY = "typeTable";
	public static final String TYPE_TABLE_CLE_KEY = "typeTableCle";
	public static final String VALIDE_KEY = "valide";

//Attributs non visibles
	public static final String ID_SEPA_SDD_ORIGINE_TYPE_KEY = "idSepaSddOrigineType";

//Colonnes dans la base de donnees
	public static final String GENERATION_ECRITURES_COLKEY = "GEN_ECRITURES";
	public static final String TYPE_CLASS_COLKEY = "TYPE_CLASS";
	public static final String TYPE_CODE_COLKEY = "TYPE_CODE";
	public static final String TYPE_COMMENTAIRE_COLKEY = "TYPE_COMMENTAIRE";
	public static final String TYPE_NOM_COLKEY = "TYPE_NOM";
	public static final String TYPE_ORDRE_COLKEY = "TYPE_ordre";
	public static final String TYPE_TABLE_COLKEY = "TYPE_TABLE";
	public static final String TYPE_TABLE_CLE_COLKEY = "TYPE_TABLE_CLE";
	public static final String VALIDE_COLKEY = "VALIDE";

	public static final String ID_SEPA_SDD_ORIGINE_TYPE_COLKEY = "ID_SEPA_SDD_ORIGINE_TYPE";


	// Relationships



	// Accessors methods
	public String generationEcritures() {
	 return (String) storedValueForKey(GENERATION_ECRITURES_KEY);
	}

	public void setGenerationEcritures(String value) {
	 takeStoredValueForKey(value, GENERATION_ECRITURES_KEY);
	}

	public String typeClass() {
	 return (String) storedValueForKey(TYPE_CLASS_KEY);
	}

	public void setTypeClass(String value) {
	 takeStoredValueForKey(value, TYPE_CLASS_KEY);
	}

	public String typeCode() {
	 return (String) storedValueForKey(TYPE_CODE_KEY);
	}

	public void setTypeCode(String value) {
	 takeStoredValueForKey(value, TYPE_CODE_KEY);
	}

	public String typeCommentaire() {
	 return (String) storedValueForKey(TYPE_COMMENTAIRE_KEY);
	}

	public void setTypeCommentaire(String value) {
	 takeStoredValueForKey(value, TYPE_COMMENTAIRE_KEY);
	}

	public String typeNom() {
	 return (String) storedValueForKey(TYPE_NOM_KEY);
	}

	public void setTypeNom(String value) {
	 takeStoredValueForKey(value, TYPE_NOM_KEY);
	}

	public Integer typeOrdre() {
	 return (Integer) storedValueForKey(TYPE_ORDRE_KEY);
	}

	public void setTypeOrdre(Integer value) {
	 takeStoredValueForKey(value, TYPE_ORDRE_KEY);
	}

	public String typeTable() {
	 return (String) storedValueForKey(TYPE_TABLE_KEY);
	}

	public void setTypeTable(String value) {
	 takeStoredValueForKey(value, TYPE_TABLE_KEY);
	}

	public String typeTableCle() {
	 return (String) storedValueForKey(TYPE_TABLE_CLE_KEY);
	}

	public void setTypeTableCle(String value) {
	 takeStoredValueForKey(value, TYPE_TABLE_CLE_KEY);
	}

	public String valide() {
	 return (String) storedValueForKey(VALIDE_KEY);
	}

	public void setValide(String value) {
	 takeStoredValueForKey(value, VALIDE_KEY);
	}


	/**
	* Créer une instance de EOSepaSddOrigineType avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOSepaSddOrigineType createEOSepaSddOrigineType(EOEditingContext editingContext				, String generationEcritures
							, String typeClass
							, String typeCode
							, String typeCommentaire
							, String typeNom
							, Integer typeOrdre
							, String typeTable
							, String typeTableCle
							, String valide
								) {
	 EOSepaSddOrigineType eo = (EOSepaSddOrigineType) EOUtilities.createAndInsertInstance(editingContext, _EOSepaSddOrigineType.ENTITY_NAME);	 
							eo.setGenerationEcritures(generationEcritures);
									eo.setTypeClass(typeClass);
									eo.setTypeCode(typeCode);
									eo.setTypeCommentaire(typeCommentaire);
									eo.setTypeNom(typeNom);
									eo.setTypeOrdre(typeOrdre);
									eo.setTypeTable(typeTable);
									eo.setTypeTableCle(typeTableCle);
									eo.setValide(valide);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSepaSddOrigineType creerInstance(EOEditingContext editingContext) {
		EOSepaSddOrigineType object = (EOSepaSddOrigineType)EOUtilities.createAndInsertInstance(editingContext, _EOSepaSddOrigineType.ENTITY_NAME);
  		return object;
		}

	

  public EOSepaSddOrigineType localInstanceIn(EOEditingContext editingContext) {
    EOSepaSddOrigineType localInstance = (EOSepaSddOrigineType)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSepaSddOrigineType fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSepaSddOrigineType fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOSepaSddOrigineType> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSepaSddOrigineType eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSepaSddOrigineType)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSepaSddOrigineType fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSepaSddOrigineType fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOSepaSddOrigineType> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSepaSddOrigineType eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSepaSddOrigineType)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSepaSddOrigineType fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSepaSddOrigineType eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSepaSddOrigineType ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSepaSddOrigineType fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}