/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJefyAdminUtilisateur.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOJefyAdminUtilisateur extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_JefyAdmin_Utilisateur";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.Utilisateur";


//Attribute Keys
	public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
	public static final ERXKey<Integer> PERSONNE_PERS_ID = new ERXKey<Integer>("personne_persId");
	public static final ERXKey<String> PERSONNE_PERS_LC = new ERXKey<String>("personne_persLc");
	public static final ERXKey<String> PERSONNE_PERS_LIBELLE = new ERXKey<String>("personne_persLibelle");
	public static final ERXKey<String> PERSONNE_PERS_NOMPTR = new ERXKey<String>("personne_persNomptr");
	public static final ERXKey<NSTimestamp> UTL_FERMETURE = new ERXKey<NSTimestamp>("utlFermeture");
	public static final ERXKey<NSTimestamp> UTL_OUVERTURE = new ERXKey<NSTimestamp>("utlOuverture");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne> PERSONNE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne>("personne");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminTypeEtat> TYPE_ETAT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminTypeEtat>("typeEtat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "utlOrdre";

	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String PERSONNE_PERS_ID_KEY = "personne_persId";
	public static final String PERSONNE_PERS_LC_KEY = "personne_persLc";
	public static final String PERSONNE_PERS_LIBELLE_KEY = "personne_persLibelle";
	public static final String PERSONNE_PERS_NOMPTR_KEY = "personne_persNomptr";
	public static final String UTL_FERMETURE_KEY = "utlFermeture";
	public static final String UTL_OUVERTURE_KEY = "utlOuverture";

//Attributs non visibles
	public static final String PERS_ID_KEY = "persId";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String PERSONNE_PERS_ID_COLKEY = "$attribute.columnName";
	public static final String PERSONNE_PERS_LC_COLKEY = "$attribute.columnName";
	public static final String PERSONNE_PERS_LIBELLE_COLKEY = "$attribute.columnName";
	public static final String PERSONNE_PERS_NOMPTR_COLKEY = "$attribute.columnName";
	public static final String UTL_FERMETURE_COLKEY = "utl_Fermeture";
	public static final String UTL_OUVERTURE_COLKEY = "utl_ouverture";

	public static final String PERS_ID_COLKEY = "pers_Id";
	public static final String TYET_ID_COLKEY = "tyet_id";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String PERSONNE_KEY = "personne";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
	public Integer noIndividu() {
	 return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
	}

	public void setNoIndividu(Integer value) {
	 takeStoredValueForKey(value, NO_INDIVIDU_KEY);
	}

	public Integer personne_persId() {
	 return (Integer) storedValueForKey(PERSONNE_PERS_ID_KEY);
	}

	public void setPersonne_persId(Integer value) {
	 takeStoredValueForKey(value, PERSONNE_PERS_ID_KEY);
	}

	public String personne_persLc() {
	 return (String) storedValueForKey(PERSONNE_PERS_LC_KEY);
	}

	public void setPersonne_persLc(String value) {
	 takeStoredValueForKey(value, PERSONNE_PERS_LC_KEY);
	}

	public String personne_persLibelle() {
	 return (String) storedValueForKey(PERSONNE_PERS_LIBELLE_KEY);
	}

	public void setPersonne_persLibelle(String value) {
	 takeStoredValueForKey(value, PERSONNE_PERS_LIBELLE_KEY);
	}

	public String personne_persNomptr() {
	 return (String) storedValueForKey(PERSONNE_PERS_NOMPTR_KEY);
	}

	public void setPersonne_persNomptr(String value) {
	 takeStoredValueForKey(value, PERSONNE_PERS_NOMPTR_KEY);
	}

	public NSTimestamp utlFermeture() {
	 return (NSTimestamp) storedValueForKey(UTL_FERMETURE_KEY);
	}

	public void setUtlFermeture(NSTimestamp value) {
	 takeStoredValueForKey(value, UTL_FERMETURE_KEY);
	}

	public NSTimestamp utlOuverture() {
	 return (NSTimestamp) storedValueForKey(UTL_OUVERTURE_KEY);
	}

	public void setUtlOuverture(NSTimestamp value) {
	 takeStoredValueForKey(value, UTL_OUVERTURE_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne personne() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne)storedValueForKey(PERSONNE_KEY);
	}

	public void setPersonneRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne oldValue = personne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminTypeEtat typeEtat() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
	}

	public void setTypeEtatRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminTypeEtat oldValue = typeEtat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
	 }
	}


	/**
	* Créer une instance de EOJefyAdminUtilisateur avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOJefyAdminUtilisateur createEOJefyAdminUtilisateur(EOEditingContext editingContext						, Integer personne_persId
															, NSTimestamp utlOuverture
					, org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne personne		, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminTypeEtat typeEtat					) {
	 EOJefyAdminUtilisateur eo = (EOJefyAdminUtilisateur) EOUtilities.createAndInsertInstance(editingContext, _EOJefyAdminUtilisateur.ENTITY_NAME);	 
									eo.setPersonne_persId(personne_persId);
																	eo.setUtlOuverture(utlOuverture);
						 eo.setPersonneRelationship(personne);
				 eo.setTypeEtatRelationship(typeEtat);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyAdminUtilisateur creerInstance(EOEditingContext editingContext) {
		EOJefyAdminUtilisateur object = (EOJefyAdminUtilisateur)EOUtilities.createAndInsertInstance(editingContext, _EOJefyAdminUtilisateur.ENTITY_NAME);
  		return object;
		}

	

  public EOJefyAdminUtilisateur localInstanceIn(EOEditingContext editingContext) {
    EOJefyAdminUtilisateur localInstance = (EOJefyAdminUtilisateur)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOJefyAdminUtilisateur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOJefyAdminUtilisateur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOJefyAdminUtilisateur> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJefyAdminUtilisateur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJefyAdminUtilisateur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJefyAdminUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJefyAdminUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOJefyAdminUtilisateur> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJefyAdminUtilisateur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJefyAdminUtilisateur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOJefyAdminUtilisateur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJefyAdminUtilisateur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJefyAdminUtilisateur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJefyAdminUtilisateur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}