/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJefyRecetteRecette.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOJefyRecetteRecette extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_JefyRecette_Recette";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.RECETTE";


//Attribute Keys
	public static final ERXKey<NSTimestamp> REC_DATE_SAISIE = new ERXKey<NSTimestamp>("recDateSaisie");
	public static final ERXKey<java.math.BigDecimal> REC_HT_SAISIE = new ERXKey<java.math.BigDecimal>("recHtSaisie");
	public static final ERXKey<String> REC_LIB = new ERXKey<String>("recLib");
	public static final ERXKey<java.math.BigDecimal> REC_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("recMontantBudgetaire");
	public static final ERXKey<Integer> REC_NUMERO = new ERXKey<Integer>("recNumero");
	public static final ERXKey<java.math.BigDecimal> REC_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("recTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> REC_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("recTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteFacture> TO_FACTURE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteFacture>("toFacture");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> TO_RECETTE_CTRL_PLANCOS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco>("toRecetteCtrlPlancos");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier> TO_RECETTE_PAPIER = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier>("toRecettePapier");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "recId";

	public static final String REC_DATE_SAISIE_KEY = "recDateSaisie";
	public static final String REC_HT_SAISIE_KEY = "recHtSaisie";
	public static final String REC_LIB_KEY = "recLib";
	public static final String REC_MONTANT_BUDGETAIRE_KEY = "recMontantBudgetaire";
	public static final String REC_NUMERO_KEY = "recNumero";
	public static final String REC_TTC_SAISIE_KEY = "recTtcSaisie";
	public static final String REC_TVA_SAISIE_KEY = "recTvaSaisie";

//Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FAC_ID_KEY = "facId";
	public static final String REC_ID_KEY = "recId";
	public static final String REC_ID_REDUCTION_KEY = "recIdReduction";
	public static final String RPP_ID_KEY = "rppId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String REC_DATE_SAISIE_COLKEY = "REC_DATE_SAISIE";
	public static final String REC_HT_SAISIE_COLKEY = "REC_HT_SAISIE";
	public static final String REC_LIB_COLKEY = "REC_LIB";
	public static final String REC_MONTANT_BUDGETAIRE_COLKEY = "REC_MONTANT_BUDGETAIRE";
	public static final String REC_NUMERO_COLKEY = "REC_NUMERO";
	public static final String REC_TTC_SAISIE_COLKEY = "REC_TTC_SAISIE";
	public static final String REC_TVA_SAISIE_COLKEY = "REC_TVA_SAISIE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FAC_ID_COLKEY = "FAC_ID";
	public static final String REC_ID_COLKEY = "REC_ID";
	public static final String REC_ID_REDUCTION_COLKEY = "REC_ID_REDUCTION";
	public static final String RPP_ID_COLKEY = "RPP_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String TO_FACTURE_KEY = "toFacture";
	public static final String TO_RECETTE_CTRL_PLANCOS_KEY = "toRecetteCtrlPlancos";
	public static final String TO_RECETTE_PAPIER_KEY = "toRecettePapier";



	// Accessors methods
	public NSTimestamp recDateSaisie() {
	 return (NSTimestamp) storedValueForKey(REC_DATE_SAISIE_KEY);
	}

	public void setRecDateSaisie(NSTimestamp value) {
	 takeStoredValueForKey(value, REC_DATE_SAISIE_KEY);
	}

	public java.math.BigDecimal recHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(REC_HT_SAISIE_KEY);
	}

	public void setRecHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REC_HT_SAISIE_KEY);
	}

	public String recLib() {
	 return (String) storedValueForKey(REC_LIB_KEY);
	}

	public void setRecLib(String value) {
	 takeStoredValueForKey(value, REC_LIB_KEY);
	}

	public java.math.BigDecimal recMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(REC_MONTANT_BUDGETAIRE_KEY);
	}

	public void setRecMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REC_MONTANT_BUDGETAIRE_KEY);
	}

	public Integer recNumero() {
	 return (Integer) storedValueForKey(REC_NUMERO_KEY);
	}

	public void setRecNumero(Integer value) {
	 takeStoredValueForKey(value, REC_NUMERO_KEY);
	}

	public java.math.BigDecimal recTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(REC_TTC_SAISIE_KEY);
	}

	public void setRecTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REC_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal recTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(REC_TVA_SAISIE_KEY);
	}

	public void setRecTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REC_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteFacture toFacture() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteFacture)storedValueForKey(TO_FACTURE_KEY);
	}

	public void setToFactureRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteFacture value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteFacture oldValue = toFacture();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FACTURE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_FACTURE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier toRecettePapier() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier)storedValueForKey(TO_RECETTE_PAPIER_KEY);
	}

	public void setToRecettePapierRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier oldValue = toRecettePapier();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RECETTE_PAPIER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_RECETTE_PAPIER_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> toRecetteCtrlPlancos() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco>)storedValueForKey(TO_RECETTE_CTRL_PLANCOS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> toRecetteCtrlPlancos(EOQualifier qualifier) {
	 return toRecetteCtrlPlancos(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> toRecetteCtrlPlancos(EOQualifier qualifier, Boolean fetch) {
	 return toRecetteCtrlPlancos(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> toRecetteCtrlPlancos(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco.TO_JEFY_RECETTE_RECETTE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toRecetteCtrlPlancos();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToRecetteCtrlPlancosRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_RECETTE_CTRL_PLANCOS_KEY);
	}
	
	public void removeFromToRecetteCtrlPlancosRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RECETTE_CTRL_PLANCOS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco createToRecetteCtrlPlancosRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_RECETTE_CTRL_PLANCOS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco) eo;
	}
	
	public void deleteToRecetteCtrlPlancosRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RECETTE_CTRL_PLANCOS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToRecetteCtrlPlancosRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecetteCtrlPlanco> objects = toRecetteCtrlPlancos().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToRecetteCtrlPlancosRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOJefyRecetteRecette avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOJefyRecetteRecette createEOJefyRecetteRecette(EOEditingContext editingContext				, NSTimestamp recDateSaisie
							, java.math.BigDecimal recHtSaisie
									, java.math.BigDecimal recMontantBudgetaire
							, Integer recNumero
							, java.math.BigDecimal recTtcSaisie
							, java.math.BigDecimal recTvaSaisie
					, org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteFacture toFacture		, org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecettePapier toRecettePapier					) {
	 EOJefyRecetteRecette eo = (EOJefyRecetteRecette) EOUtilities.createAndInsertInstance(editingContext, _EOJefyRecetteRecette.ENTITY_NAME);	 
							eo.setRecDateSaisie(recDateSaisie);
									eo.setRecHtSaisie(recHtSaisie);
											eo.setRecMontantBudgetaire(recMontantBudgetaire);
									eo.setRecNumero(recNumero);
									eo.setRecTtcSaisie(recTtcSaisie);
									eo.setRecTvaSaisie(recTvaSaisie);
						 eo.setToFactureRelationship(toFacture);
				 eo.setToRecettePapierRelationship(toRecettePapier);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJefyRecetteRecette creerInstance(EOEditingContext editingContext) {
		EOJefyRecetteRecette object = (EOJefyRecetteRecette)EOUtilities.createAndInsertInstance(editingContext, _EOJefyRecetteRecette.ENTITY_NAME);
  		return object;
		}

	

  public EOJefyRecetteRecette localInstanceIn(EOEditingContext editingContext) {
    EOJefyRecetteRecette localInstance = (EOJefyRecetteRecette)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOJefyRecetteRecette>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOJefyRecetteRecette fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOJefyRecetteRecette fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOJefyRecetteRecette> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJefyRecetteRecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJefyRecetteRecette)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJefyRecetteRecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJefyRecetteRecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOJefyRecetteRecette> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJefyRecetteRecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJefyRecetteRecette)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOJefyRecetteRecette fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJefyRecetteRecette eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJefyRecetteRecette ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJefyRecetteRecette fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}