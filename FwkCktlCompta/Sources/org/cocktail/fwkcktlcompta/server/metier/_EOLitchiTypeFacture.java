/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOLitchiTypeFacture.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOLitchiTypeFacture extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Litchi_TypeFacture";
	public static final String ENTITY_TABLE_NAME = "LITCHI.TYPE_FACTURE";


//Attribute Keys
	public static final ERXKey<String> LIB_TYPE_FACTURE = new ERXKey<String>("libTypeFacture");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture> FWK_CKTL_COMPTA__LITCHI__ENTETE_FACTURES = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture>("fwkCktlCompta_Litchi_EnteteFactures");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idTypeFacture";

	public static final String LIB_TYPE_FACTURE_KEY = "libTypeFacture";

//Attributs non visibles
	public static final String ID_TYPE_FACTURE_KEY = "idTypeFacture";

//Colonnes dans la base de donnees
	public static final String LIB_TYPE_FACTURE_COLKEY = "LIB_TYPE_FACTURE";

	public static final String ID_TYPE_FACTURE_COLKEY = "ID_TYPE_FACTURE";


	// Relationships
	public static final String FWK_CKTL_COMPTA__LITCHI__ENTETE_FACTURES_KEY = "fwkCktlCompta_Litchi_EnteteFactures";



	// Accessors methods
	public String libTypeFacture() {
	 return (String) storedValueForKey(LIB_TYPE_FACTURE_KEY);
	}

	public void setLibTypeFacture(String value) {
	 takeStoredValueForKey(value, LIB_TYPE_FACTURE_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture> fwkCktlCompta_Litchi_EnteteFactures() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture>)storedValueForKey(FWK_CKTL_COMPTA__LITCHI__ENTETE_FACTURES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture> fwkCktlCompta_Litchi_EnteteFactures(EOQualifier qualifier) {
	 return fwkCktlCompta_Litchi_EnteteFactures(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture> fwkCktlCompta_Litchi_EnteteFactures(EOQualifier qualifier, Boolean fetch) {
	 return fwkCktlCompta_Litchi_EnteteFactures(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture> fwkCktlCompta_Litchi_EnteteFactures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture.TO_TYPE_FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = fwkCktlCompta_Litchi_EnteteFactures();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToFwkCktlCompta_Litchi_EnteteFacturesRelationship(org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, FWK_CKTL_COMPTA__LITCHI__ENTETE_FACTURES_KEY);
	}
	
	public void removeFromFwkCktlCompta_Litchi_EnteteFacturesRelationship(org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, FWK_CKTL_COMPTA__LITCHI__ENTETE_FACTURES_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture createFwkCktlCompta_Litchi_EnteteFacturesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, FWK_CKTL_COMPTA__LITCHI__ENTETE_FACTURES_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture) eo;
	}
	
	public void deleteFwkCktlCompta_Litchi_EnteteFacturesRelationship(org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, FWK_CKTL_COMPTA__LITCHI__ENTETE_FACTURES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllFwkCktlCompta_Litchi_EnteteFacturesRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture> objects = fwkCktlCompta_Litchi_EnteteFactures().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteFwkCktlCompta_Litchi_EnteteFacturesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOLitchiTypeFacture avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOLitchiTypeFacture createEOLitchiTypeFacture(EOEditingContext editingContext							) {
	 EOLitchiTypeFacture eo = (EOLitchiTypeFacture) EOUtilities.createAndInsertInstance(editingContext, _EOLitchiTypeFacture.ENTITY_NAME);	 
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOLitchiTypeFacture creerInstance(EOEditingContext editingContext) {
		EOLitchiTypeFacture object = (EOLitchiTypeFacture)EOUtilities.createAndInsertInstance(editingContext, _EOLitchiTypeFacture.ENTITY_NAME);
  		return object;
		}

	

  public EOLitchiTypeFacture localInstanceIn(EOEditingContext editingContext) {
    EOLitchiTypeFacture localInstance = (EOLitchiTypeFacture)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiTypeFacture> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiTypeFacture> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiTypeFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiTypeFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiTypeFacture> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiTypeFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiTypeFacture> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOLitchiTypeFacture>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOLitchiTypeFacture fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOLitchiTypeFacture fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOLitchiTypeFacture> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOLitchiTypeFacture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOLitchiTypeFacture)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOLitchiTypeFacture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOLitchiTypeFacture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOLitchiTypeFacture> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOLitchiTypeFacture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOLitchiTypeFacture)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOLitchiTypeFacture fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOLitchiTypeFacture eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOLitchiTypeFacture ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOLitchiTypeFacture fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}