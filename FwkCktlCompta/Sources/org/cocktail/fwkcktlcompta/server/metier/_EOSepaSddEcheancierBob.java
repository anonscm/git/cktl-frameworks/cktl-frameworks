/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSepaSddEcheancierBob.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOSepaSddEcheancierBob extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_SepaSddEcheancierBob";
	public static final String ENTITY_TABLE_NAME = "maracuja.sepa_Sdd_Echeancier_Bob";


//Attribute Keys
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard> TO_BORDEREAU_BROUILLARD = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard>("toBordereauBrouillard");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier> TO_SEPA_SDD_ECHEANCIER = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier>("toSepaSddEcheancier");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "sebId";


//Attributs non visibles
	public static final String BOB_ORDRE_KEY = "bobOrdre";
	public static final String ID_SEPA_SDD_ECHEANCIER_KEY = "idSepaSddEcheancier";
	public static final String SEB_ID_KEY = "sebId";

//Colonnes dans la base de donnees

	public static final String BOB_ORDRE_COLKEY = "bob_ORDRE";
	public static final String ID_SEPA_SDD_ECHEANCIER_COLKEY = "ID_sepa_sdd_echeancier";
	public static final String SEB_ID_COLKEY = "seb_id";


	// Relationships
	public static final String TO_BORDEREAU_BROUILLARD_KEY = "toBordereauBrouillard";
	public static final String TO_SEPA_SDD_ECHEANCIER_KEY = "toSepaSddEcheancier";



	// Accessors methods
	public org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard toBordereauBrouillard() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard)storedValueForKey(TO_BORDEREAU_BROUILLARD_KEY);
	}

	public void setToBordereauBrouillardRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard oldValue = toBordereauBrouillard();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BORDEREAU_BROUILLARD_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_BORDEREAU_BROUILLARD_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier toSepaSddEcheancier() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier)storedValueForKey(TO_SEPA_SDD_ECHEANCIER_KEY);
	}

	public void setToSepaSddEcheancierRelationship(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier oldValue = toSepaSddEcheancier();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SEPA_SDD_ECHEANCIER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_SEPA_SDD_ECHEANCIER_KEY);
	 }
	}


	/**
	* Créer une instance de EOSepaSddEcheancierBob avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOSepaSddEcheancierBob createEOSepaSddEcheancierBob(EOEditingContext editingContext		, org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard toBordereauBrouillard		, org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier toSepaSddEcheancier					) {
	 EOSepaSddEcheancierBob eo = (EOSepaSddEcheancierBob) EOUtilities.createAndInsertInstance(editingContext, _EOSepaSddEcheancierBob.ENTITY_NAME);	 
				 eo.setToBordereauBrouillardRelationship(toBordereauBrouillard);
				 eo.setToSepaSddEcheancierRelationship(toSepaSddEcheancier);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSepaSddEcheancierBob creerInstance(EOEditingContext editingContext) {
		EOSepaSddEcheancierBob object = (EOSepaSddEcheancierBob)EOUtilities.createAndInsertInstance(editingContext, _EOSepaSddEcheancierBob.ENTITY_NAME);
  		return object;
		}

	

  public EOSepaSddEcheancierBob localInstanceIn(EOEditingContext editingContext) {
    EOSepaSddEcheancierBob localInstance = (EOSepaSddEcheancierBob)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancierBob>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSepaSddEcheancierBob fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSepaSddEcheancierBob fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOSepaSddEcheancierBob> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSepaSddEcheancierBob eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSepaSddEcheancierBob)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSepaSddEcheancierBob fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSepaSddEcheancierBob fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOSepaSddEcheancierBob> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSepaSddEcheancierBob eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSepaSddEcheancierBob)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSepaSddEcheancierBob fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSepaSddEcheancierBob eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSepaSddEcheancierBob ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSepaSddEcheancierBob fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}