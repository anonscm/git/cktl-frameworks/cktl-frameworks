/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBrouillard.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOBrouillard extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Brouillard";
	public static final String ENTITY_TABLE_NAME = "maracuja.Brouillard";


//Attribute Keys
	public static final ERXKey<NSTimestamp> BRO_DATE_VISA = new ERXKey<NSTimestamp>("broDateVisa");
	public static final ERXKey<String> BRO_ETAT = new ERXKey<String>("broEtat");
	public static final ERXKey<String> BRO_LIBELLE = new ERXKey<String>("broLibelle");
	public static final ERXKey<String> BRO_MOTIF_REJET = new ERXKey<String>("broMotifRejet");
	public static final ERXKey<Integer> BRO_NUMERO = new ERXKey<Integer>("broNumero");
	public static final ERXKey<String> BRO_POSTIT = new ERXKey<String>("broPostit");
	public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereau> TO_BORDEREAU = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBordereau>("toBordereau");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> TO_BROUILLARD_DETAILS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail>("toBrouillardDetails");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOComptabilite> TO_COMPTABILITE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOComptabilite>("toComptabilite");
	public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOOrigine> TO_ORIGINE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOOrigine>("toOrigine");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneCreation");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_VISA = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneVisa");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal> TO_TYPE_JOURNAL = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal>("toTypeJournal");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation> TO_TYPE_OPERATION = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation>("toTypeOperation");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "broId";

	public static final String BRO_DATE_VISA_KEY = "broDateVisa";
	public static final String BRO_ETAT_KEY = "broEtat";
	public static final String BRO_LIBELLE_KEY = "broLibelle";
	public static final String BRO_MOTIF_REJET_KEY = "broMotifRejet";
	public static final String BRO_NUMERO_KEY = "broNumero";
	public static final String BRO_POSTIT_KEY = "broPostit";
	public static final String DATE_CREATION_KEY = "dateCreation";

//Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String BRO_ID_KEY = "broId";
	public static final String COM_ORDRE_KEY = "comOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_VISA_KEY = "persIdVisa";
	public static final String TJO_ORDRE_KEY = "tjoOrdre";
	public static final String TOP_ORDRE_KEY = "topOrdre";

//Colonnes dans la base de donnees
	public static final String BRO_DATE_VISA_COLKEY = "bro_date_visa";
	public static final String BRO_ETAT_COLKEY = "bro_etat";
	public static final String BRO_LIBELLE_COLKEY = "bro_libelle";
	public static final String BRO_MOTIF_REJET_COLKEY = "bro_motif_rejet";
	public static final String BRO_NUMERO_COLKEY = "BRO_NUMERO";
	public static final String BRO_POSTIT_COLKEY = "bro_postit";
	public static final String DATE_CREATION_COLKEY = "date_creation";

	public static final String BOR_ID_COLKEY = "bor_id";
	public static final String BRO_ID_COLKEY = "BRO_ID";
	public static final String COM_ORDRE_COLKEY = "com_ordre";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORI_ORDRE_COLKEY = "ori_ordre";
	public static final String PERS_ID_CREATION_COLKEY = "pers_id_creation";
	public static final String PERS_ID_VISA_COLKEY = "pers_id_visa";
	public static final String TJO_ORDRE_COLKEY = "TJO_ORDRE";
	public static final String TOP_ORDRE_COLKEY = "TOP_ORDRE";


	// Relationships
	public static final String TO_BORDEREAU_KEY = "toBordereau";
	public static final String TO_BROUILLARD_DETAILS_KEY = "toBrouillardDetails";
	public static final String TO_COMPTABILITE_KEY = "toComptabilite";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_ORIGINE_KEY = "toOrigine";
	public static final String TO_PERSONNE_CREATION_KEY = "toPersonneCreation";
	public static final String TO_PERSONNE_VISA_KEY = "toPersonneVisa";
	public static final String TO_TYPE_JOURNAL_KEY = "toTypeJournal";
	public static final String TO_TYPE_OPERATION_KEY = "toTypeOperation";



	// Accessors methods
	public NSTimestamp broDateVisa() {
	 return (NSTimestamp) storedValueForKey(BRO_DATE_VISA_KEY);
	}

	public void setBroDateVisa(NSTimestamp value) {
	 takeStoredValueForKey(value, BRO_DATE_VISA_KEY);
	}

	public String broEtat() {
	 return (String) storedValueForKey(BRO_ETAT_KEY);
	}

	public void setBroEtat(String value) {
	 takeStoredValueForKey(value, BRO_ETAT_KEY);
	}

	public String broLibelle() {
	 return (String) storedValueForKey(BRO_LIBELLE_KEY);
	}

	public void setBroLibelle(String value) {
	 takeStoredValueForKey(value, BRO_LIBELLE_KEY);
	}

	public String broMotifRejet() {
	 return (String) storedValueForKey(BRO_MOTIF_REJET_KEY);
	}

	public void setBroMotifRejet(String value) {
	 takeStoredValueForKey(value, BRO_MOTIF_REJET_KEY);
	}

	public Integer broNumero() {
	 return (Integer) storedValueForKey(BRO_NUMERO_KEY);
	}

	public void setBroNumero(Integer value) {
	 takeStoredValueForKey(value, BRO_NUMERO_KEY);
	}

	public String broPostit() {
	 return (String) storedValueForKey(BRO_POSTIT_KEY);
	}

	public void setBroPostit(String value) {
	 takeStoredValueForKey(value, BRO_POSTIT_KEY);
	}

	public NSTimestamp dateCreation() {
	 return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
	}

	public void setDateCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_CREATION_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOBordereau toBordereau() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBordereau)storedValueForKey(TO_BORDEREAU_KEY);
	}

	public void setToBordereauRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBordereau value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOBordereau oldValue = toBordereau();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BORDEREAU_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_BORDEREAU_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOComptabilite toComptabilite() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOComptabilite)storedValueForKey(TO_COMPTABILITE_KEY);
	}

	public void setToComptabiliteRelationship(org.cocktail.fwkcktlcompta.server.metier.EOComptabilite value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOComptabilite oldValue = toComptabilite();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COMPTABILITE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_COMPTABILITE_KEY);
	 }
	}

	public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice() {
	 return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOOrigine toOrigine() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOOrigine)storedValueForKey(TO_ORIGINE_KEY);
	}

	public void setToOrigineRelationship(org.cocktail.fwkcktlcompta.server.metier.EOOrigine value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOOrigine oldValue = toOrigine();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORIGINE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORIGINE_KEY);
	 }
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(TO_PERSONNE_CREATION_KEY);
	}

	public void setToPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneCreation();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_CREATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_CREATION_KEY);
	 }
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneVisa() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(TO_PERSONNE_VISA_KEY);
	}

	public void setToPersonneVisaRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneVisa();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_VISA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_VISA_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal toTypeJournal() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal)storedValueForKey(TO_TYPE_JOURNAL_KEY);
	}

	public void setToTypeJournalRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal oldValue = toTypeJournal();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_JOURNAL_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_JOURNAL_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation toTypeOperation() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation)storedValueForKey(TO_TYPE_OPERATION_KEY);
	}

	public void setToTypeOperationRelationship(org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation oldValue = toTypeOperation();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_OPERATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_OPERATION_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> toBrouillardDetails() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail>)storedValueForKey(TO_BROUILLARD_DETAILS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> toBrouillardDetails(EOQualifier qualifier) {
	 return toBrouillardDetails(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> toBrouillardDetails(EOQualifier qualifier, Boolean fetch) {
	 return toBrouillardDetails(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> toBrouillardDetails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail.TO_BROUILLARD_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toBrouillardDetails();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToBrouillardDetailsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_BROUILLARD_DETAILS_KEY);
	}
	
	public void removeFromToBrouillardDetailsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BROUILLARD_DETAILS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail createToBrouillardDetailsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_BROUILLARD_DETAILS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail) eo;
	}
	
	public void deleteToBrouillardDetailsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BROUILLARD_DETAILS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToBrouillardDetailsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> objects = toBrouillardDetails().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToBrouillardDetailsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOBrouillard avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOBrouillard createEOBrouillard(EOEditingContext editingContext						, String broEtat
							, String broLibelle
									, Integer broNumero
									, NSTimestamp dateCreation
							, org.cocktail.fwkcktlcompta.server.metier.EOComptabilite toComptabilite		, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice toExercice				, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation				, org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal toTypeJournal		, org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation toTypeOperation					) {
	 EOBrouillard eo = (EOBrouillard) EOUtilities.createAndInsertInstance(editingContext, _EOBrouillard.ENTITY_NAME);	 
									eo.setBroEtat(broEtat);
									eo.setBroLibelle(broLibelle);
											eo.setBroNumero(broNumero);
											eo.setDateCreation(dateCreation);
								 eo.setToComptabiliteRelationship(toComptabilite);
				 eo.setToExerciceRelationship(toExercice);
						 eo.setToPersonneCreationRelationship(toPersonneCreation);
						 eo.setToTypeJournalRelationship(toTypeJournal);
				 eo.setToTypeOperationRelationship(toTypeOperation);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBrouillard creerInstance(EOEditingContext editingContext) {
		EOBrouillard object = (EOBrouillard)EOUtilities.createAndInsertInstance(editingContext, _EOBrouillard.ENTITY_NAME);
  		return object;
		}

	

  public EOBrouillard localInstanceIn(EOEditingContext editingContext) {
    EOBrouillard localInstance = (EOBrouillard)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBrouillard fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBrouillard fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOBrouillard> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBrouillard eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBrouillard)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBrouillard fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBrouillard fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOBrouillard> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBrouillard eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBrouillard)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBrouillard fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBrouillard eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBrouillard ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBrouillard fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}