/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcompta.server.metier;

import org.cocktail.fwkcktlcompta.common.entities.ITypeBordereau;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOTypeBordereau extends _EOTypeBordereau implements ITypeBordereau {

	public static final EOSortOrdering SORT_TBO_LIBELLE_ASC = EOSortOrdering.sortOrderingWithKey(EOTypeBordereau.TBO_LIBELLE_KEY, EOSortOrdering.CompareAscending);

	public static final String TypeBordereauBTME = "BTME";
	public static final String TypeBordereauBTTE = "BTTE";
	public static final String TypeBordereauBTODP = "BTODP";
	public static final String TypeBordereauBTTNA = "BTTNA";
	public static final String TypeBordereauBTMNA = "BTMNA";
	public static final String TypeBordereauPrestationInterne = "BTPI";
	//    public static final String TypeBordereauPrestationExterne = "BTPE";
	public static final String TypeBordereauBTMS = "BTMS";

	public static final String TYPE_BORDEREAU_PRESTATION_INTERNE_D = "BTPID";
	public static final String TYPE_BORDEREAU_PRESTATION_INTERNE_R = "BTPIR";

	// Bordereau de Transmission de Cheques Comptable
	public static final String TypeBordereauCheque = "BTCC";

	public static final String SOUS_TYPE_SCOLARITE = "SCOLARITE";
	public static final String SOUS_TYPE_PAPAYE = "SALAIRES";
	public static final String SOUS_TYPE_PAYEPAF = "SALAIRES PAF";
	public static final String SOUS_TYPE_RECETTES = "RECETTES";
	public static final String SOUS_TYPE_REVERSEMENTS = "REVERSEMENTS";
	public static final String SOUS_TYPE_REDUCTION = "REDUCTIONS";
	public static final String SOUS_TYPE_PEC_CONVRA = "PECCONVRA";
	public static final String SOUS_TYPE_DOSSIER_LIQ = "DOSSIER LIQ.";
	public static final String TypeBordereauDroitsUniversitaires = "BTDU";
	public static final String TypeBordereauRembousrementsDroitsUniversitaires = "BTRU";
	public static final String TYPEBORDEREAU_TAXE_APPRENTISSAGE = "BTTA";
	public static final String TYPEBORDEREAU_BROUILLARDS = "BTBROUILLARD";

	public EOTypeBordereau() {
		super();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	public static EOTypeBordereau getTypeBordereau(EOEditingContext editingContext, String type, String sousType) {
		EOQualifier qual1 = new EOKeyValueQualifier(EOTypeBordereau.TBO_TYPE_KEY, EOQualifier.QualifierOperatorEqual, type);
		EOQualifier qual2 = new EOKeyValueQualifier(EOTypeBordereau.TBO_SOUS_TYPE_KEY, EOQualifier.QualifierOperatorEqual, sousType);
		return fetchByQualifier(editingContext, new EOAndQualifier(new NSArray(new EOQualifier[] {
				qual1, qual2
		})));
	}
}
