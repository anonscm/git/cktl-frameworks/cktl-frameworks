/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrelevement.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOPrelevement extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_Prelevement";
	public static final String ENTITY_TABLE_NAME = "maracuja.Prelevement";


//Attribute Keys
	public static final ERXKey<String> PRELEV_COMMENTAIRE = new ERXKey<String>("prelevCommentaire");
	public static final ERXKey<NSTimestamp> PRELEV_DATE_MODIF = new ERXKey<NSTimestamp>("prelevDateModif");
	public static final ERXKey<NSTimestamp> PRELEV_DATE_PRELEVEMENT = new ERXKey<NSTimestamp>("prelevDatePrelevement");
	public static final ERXKey<NSTimestamp> PRELEV_DATE_SAISIE = new ERXKey<NSTimestamp>("prelevDateSaisie");
	public static final ERXKey<String> PRELEV_ETAT_MARACUJA = new ERXKey<String>("prelevEtatMaracuja");
	public static final ERXKey<Integer> PRELEV_INDEX = new ERXKey<Integer>("prelevIndex");
	public static final ERXKey<java.math.BigDecimal> PRELEV_MONTANT = new ERXKey<java.math.BigDecimal>("prelevMontant");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcheancier> TO_ECHEANCIER = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcheancier>("toEcheancier");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis> TO_FOURNIS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis>("toFournis");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr> TO_PRELEVEMENT_DETAIL_ECRS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr>("toPrelevementDetailEcrs");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EORecouvrement> TO_RECOUVREMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EORecouvrement>("toRecouvrement");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib> TO_RIB = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib>("toRib");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prelevOrdre";

	public static final String PRELEV_COMMENTAIRE_KEY = "prelevCommentaire";
	public static final String PRELEV_DATE_MODIF_KEY = "prelevDateModif";
	public static final String PRELEV_DATE_PRELEVEMENT_KEY = "prelevDatePrelevement";
	public static final String PRELEV_DATE_SAISIE_KEY = "prelevDateSaisie";
	public static final String PRELEV_ETAT_MARACUJA_KEY = "prelevEtatMaracuja";
	public static final String PRELEV_INDEX_KEY = "prelevIndex";
	public static final String PRELEV_MONTANT_KEY = "prelevMontant";

//Attributs non visibles
	public static final String ECHEANCIER_ORDRE_KEY = "echeancierOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String PRELEV_ORDRE_KEY = "prelevOrdre";
	public static final String RECO_ORDRE_KEY = "reco_ordre";
	public static final String RIB_ORDRE_KEY = "ribOrdre";

//Colonnes dans la base de donnees
	public static final String PRELEV_COMMENTAIRE_COLKEY = "prel_COMMENTAIRE";
	public static final String PRELEV_DATE_MODIF_COLKEY = "prel_DATE_MODIF";
	public static final String PRELEV_DATE_PRELEVEMENT_COLKEY = "prel_DATE_PRELEVEMENT";
	public static final String PRELEV_DATE_SAISIE_COLKEY = "prel_prelev_Date_saisie";
	public static final String PRELEV_ETAT_MARACUJA_COLKEY = "PREL_ETAT_MARACUJA";
	public static final String PRELEV_INDEX_COLKEY = "prel_NUMERO_INDEX";
	public static final String PRELEV_MONTANT_COLKEY = "prel_prelev_Montant";

	public static final String ECHEANCIER_ORDRE_COLKEY = "ECHE_ECHEANCIER_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String PRELEV_ORDRE_COLKEY = "PREL_PRELEV_ORDRE";
	public static final String RECO_ORDRE_COLKEY = "reco_ORDRE";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";


	// Relationships
	public static final String TO_ECHEANCIER_KEY = "toEcheancier";
	public static final String TO_FOURNIS_KEY = "toFournis";
	public static final String TO_PRELEVEMENT_DETAIL_ECRS_KEY = "toPrelevementDetailEcrs";
	public static final String TO_RECOUVREMENT_KEY = "toRecouvrement";
	public static final String TO_RIB_KEY = "toRib";



	// Accessors methods
	public String prelevCommentaire() {
	 return (String) storedValueForKey(PRELEV_COMMENTAIRE_KEY);
	}

	public void setPrelevCommentaire(String value) {
	 takeStoredValueForKey(value, PRELEV_COMMENTAIRE_KEY);
	}

	public NSTimestamp prelevDateModif() {
	 return (NSTimestamp) storedValueForKey(PRELEV_DATE_MODIF_KEY);
	}

	public void setPrelevDateModif(NSTimestamp value) {
	 takeStoredValueForKey(value, PRELEV_DATE_MODIF_KEY);
	}

	public NSTimestamp prelevDatePrelevement() {
	 return (NSTimestamp) storedValueForKey(PRELEV_DATE_PRELEVEMENT_KEY);
	}

	public void setPrelevDatePrelevement(NSTimestamp value) {
	 takeStoredValueForKey(value, PRELEV_DATE_PRELEVEMENT_KEY);
	}

	public NSTimestamp prelevDateSaisie() {
	 return (NSTimestamp) storedValueForKey(PRELEV_DATE_SAISIE_KEY);
	}

	public void setPrelevDateSaisie(NSTimestamp value) {
	 takeStoredValueForKey(value, PRELEV_DATE_SAISIE_KEY);
	}

	public String prelevEtatMaracuja() {
	 return (String) storedValueForKey(PRELEV_ETAT_MARACUJA_KEY);
	}

	public void setPrelevEtatMaracuja(String value) {
	 takeStoredValueForKey(value, PRELEV_ETAT_MARACUJA_KEY);
	}

	public Integer prelevIndex() {
	 return (Integer) storedValueForKey(PRELEV_INDEX_KEY);
	}

	public void setPrelevIndex(Integer value) {
	 takeStoredValueForKey(value, PRELEV_INDEX_KEY);
	}

	public java.math.BigDecimal prelevMontant() {
	 return (java.math.BigDecimal) storedValueForKey(PRELEV_MONTANT_KEY);
	}

	public void setPrelevMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, PRELEV_MONTANT_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOEcheancier toEcheancier() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOEcheancier)storedValueForKey(TO_ECHEANCIER_KEY);
	}

	public void setToEcheancierRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcheancier value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOEcheancier oldValue = toEcheancier();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ECHEANCIER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ECHEANCIER_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis toFournis() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis)storedValueForKey(TO_FOURNIS_KEY);
	}

	public void setToFournisRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis oldValue = toFournis();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FOURNIS_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_FOURNIS_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EORecouvrement toRecouvrement() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EORecouvrement)storedValueForKey(TO_RECOUVREMENT_KEY);
	}

	public void setToRecouvrementRelationship(org.cocktail.fwkcktlcompta.server.metier.EORecouvrement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EORecouvrement oldValue = toRecouvrement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RECOUVREMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_RECOUVREMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib toRib() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib)storedValueForKey(TO_RIB_KEY);
	}

	public void setToRibRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib oldValue = toRib();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RIB_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_RIB_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr> toPrelevementDetailEcrs() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr>)storedValueForKey(TO_PRELEVEMENT_DETAIL_ECRS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr> toPrelevementDetailEcrs(EOQualifier qualifier) {
	 return toPrelevementDetailEcrs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr> toPrelevementDetailEcrs(EOQualifier qualifier, Boolean fetch) {
	 return toPrelevementDetailEcrs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr> toPrelevementDetailEcrs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr.TO_PRELEVEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toPrelevementDetailEcrs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToPrelevementDetailEcrsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_PRELEVEMENT_DETAIL_ECRS_KEY);
	}
	
	public void removeFromToPrelevementDetailEcrsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRELEVEMENT_DETAIL_ECRS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr createToPrelevementDetailEcrsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRELEVEMENT_DETAIL_ECRS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr) eo;
	}
	
	public void deleteToPrelevementDetailEcrsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRELEVEMENT_DETAIL_ECRS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToPrelevementDetailEcrsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOPrelevementDetailEcr> objects = toPrelevementDetailEcrs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToPrelevementDetailEcrsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOPrelevement avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOPrelevement createEOPrelevement(EOEditingContext editingContext								, NSTimestamp prelevDatePrelevement
							, NSTimestamp prelevDateSaisie
							, String prelevEtatMaracuja
							, Integer prelevIndex
							, java.math.BigDecimal prelevMontant
					, org.cocktail.fwkcktlcompta.server.metier.EOEcheancier toEcheancier											) {
	 EOPrelevement eo = (EOPrelevement) EOUtilities.createAndInsertInstance(editingContext, _EOPrelevement.ENTITY_NAME);	 
											eo.setPrelevDatePrelevement(prelevDatePrelevement);
									eo.setPrelevDateSaisie(prelevDateSaisie);
									eo.setPrelevEtatMaracuja(prelevEtatMaracuja);
									eo.setPrelevIndex(prelevIndex);
									eo.setPrelevMontant(prelevMontant);
						 eo.setToEcheancierRelationship(toEcheancier);
									 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrelevement creerInstance(EOEditingContext editingContext) {
		EOPrelevement object = (EOPrelevement)EOUtilities.createAndInsertInstance(editingContext, _EOPrelevement.ENTITY_NAME);
  		return object;
		}

	

  public EOPrelevement localInstanceIn(EOEditingContext editingContext) {
    EOPrelevement localInstance = (EOPrelevement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevement> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevement> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevement> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOPrelevement>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrelevement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrelevement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPrelevement> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrelevement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrelevement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrelevement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrelevement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPrelevement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrelevement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrelevement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrelevement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrelevement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrelevement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrelevement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}