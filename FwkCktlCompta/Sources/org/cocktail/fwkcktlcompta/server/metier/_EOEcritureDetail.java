/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEcritureDetail.java instead.
package org.cocktail.fwkcktlcompta.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOEcritureDetail extends  _AfwkComptaRecord {
	public static final String ENTITY_NAME = "FwkCktlCompta_EcritureDetail";
	public static final String ENTITY_TABLE_NAME = "maracuja.Ecriture_Detail";


//Attribute Keys
	public static final ERXKey<String> ECD_COMMENTAIRE = new ERXKey<String>("ecdCommentaire");
	public static final ERXKey<java.math.BigDecimal> ECD_CREDIT = new ERXKey<java.math.BigDecimal>("ecdCredit");
	public static final ERXKey<java.math.BigDecimal> ECD_DEBIT = new ERXKey<java.math.BigDecimal>("ecdDebit");
	public static final ERXKey<Integer> ECD_INDEX = new ERXKey<Integer>("ecdIndex");
	public static final ERXKey<String> ECD_LIBELLE = new ERXKey<String>("ecdLibelle");
	public static final ERXKey<java.math.BigDecimal> ECD_MONTANT = new ERXKey<java.math.BigDecimal>("ecdMontant");
	public static final ERXKey<String> ECD_POSTIT = new ERXKey<String>("ecdPostit");
	public static final ERXKey<java.math.BigDecimal> ECD_RESTE_EMARGER = new ERXKey<java.math.BigDecimal>("ecdResteEmarger");
	public static final ERXKey<String> ECD_SECONDAIRE = new ERXKey<String>("ecdSecondaire");
	public static final ERXKey<String> ECD_SENS = new ERXKey<String>("ecdSens");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOAccordsContrat> TO_ACCORDS_CONTRAT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOAccordsContrat>("toAccordsContrat");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> TO_BROUILLARD_DETAILS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail>("toBrouillardDetails");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcriture> TO_ECRITURE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOEcriture>("toEcriture");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestion> TO_GESTION = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOGestion>("toGestion");
	public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> TO_PLAN_COMPTABLE_EXER = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer>("toPlanComptableExer");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ecdOrdre";

	public static final String ECD_COMMENTAIRE_KEY = "ecdCommentaire";
	public static final String ECD_CREDIT_KEY = "ecdCredit";
	public static final String ECD_DEBIT_KEY = "ecdDebit";
	public static final String ECD_INDEX_KEY = "ecdIndex";
	public static final String ECD_LIBELLE_KEY = "ecdLibelle";
	public static final String ECD_MONTANT_KEY = "ecdMontant";
	public static final String ECD_POSTIT_KEY = "ecdPostit";
	public static final String ECD_RESTE_EMARGER_KEY = "ecdResteEmarger";
	public static final String ECD_SECONDAIRE_KEY = "ecdSecondaire";
	public static final String ECD_SENS_KEY = "ecdSens";

//Attributs non visibles
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String ECR_ORDRE_KEY = "ecrOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String ECD_COMMENTAIRE_COLKEY = "ecd_commentaire";
	public static final String ECD_CREDIT_COLKEY = "ecd_credit";
	public static final String ECD_DEBIT_COLKEY = "ecd_debit";
	public static final String ECD_INDEX_COLKEY = "ecd_index";
	public static final String ECD_LIBELLE_COLKEY = "ecd_libelle";
	public static final String ECD_MONTANT_COLKEY = "ecd_montant";
	public static final String ECD_POSTIT_COLKEY = "ecd_Postit";
	public static final String ECD_RESTE_EMARGER_COLKEY = "ecd_Reste_Emarger";
	public static final String ECD_SECONDAIRE_COLKEY = "ecd_Secondaire";
	public static final String ECD_SENS_COLKEY = "ecd_sens";

	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String ECD_ORDRE_COLKEY = "ecd_ORDRE";
	public static final String ECR_ORDRE_COLKEY = "ecr_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String TO_ACCORDS_CONTRAT_KEY = "toAccordsContrat";
	public static final String TO_BROUILLARD_DETAILS_KEY = "toBrouillardDetails";
	public static final String TO_ECRITURE_KEY = "toEcriture";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_GESTION_KEY = "toGestion";
	public static final String TO_PLAN_COMPTABLE_EXER_KEY = "toPlanComptableExer";



	// Accessors methods
	public String ecdCommentaire() {
	 return (String) storedValueForKey(ECD_COMMENTAIRE_KEY);
	}

	public void setEcdCommentaire(String value) {
	 takeStoredValueForKey(value, ECD_COMMENTAIRE_KEY);
	}

	public java.math.BigDecimal ecdCredit() {
	 return (java.math.BigDecimal) storedValueForKey(ECD_CREDIT_KEY);
	}

	public void setEcdCredit(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ECD_CREDIT_KEY);
	}

	public java.math.BigDecimal ecdDebit() {
	 return (java.math.BigDecimal) storedValueForKey(ECD_DEBIT_KEY);
	}

	public void setEcdDebit(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ECD_DEBIT_KEY);
	}

	public Integer ecdIndex() {
	 return (Integer) storedValueForKey(ECD_INDEX_KEY);
	}

	public void setEcdIndex(Integer value) {
	 takeStoredValueForKey(value, ECD_INDEX_KEY);
	}

	public String ecdLibelle() {
	 return (String) storedValueForKey(ECD_LIBELLE_KEY);
	}

	public void setEcdLibelle(String value) {
	 takeStoredValueForKey(value, ECD_LIBELLE_KEY);
	}

	public java.math.BigDecimal ecdMontant() {
	 return (java.math.BigDecimal) storedValueForKey(ECD_MONTANT_KEY);
	}

	public void setEcdMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ECD_MONTANT_KEY);
	}

	public String ecdPostit() {
	 return (String) storedValueForKey(ECD_POSTIT_KEY);
	}

	public void setEcdPostit(String value) {
	 takeStoredValueForKey(value, ECD_POSTIT_KEY);
	}

	public java.math.BigDecimal ecdResteEmarger() {
	 return (java.math.BigDecimal) storedValueForKey(ECD_RESTE_EMARGER_KEY);
	}

	public void setEcdResteEmarger(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ECD_RESTE_EMARGER_KEY);
	}

	public String ecdSecondaire() {
	 return (String) storedValueForKey(ECD_SECONDAIRE_KEY);
	}

	public void setEcdSecondaire(String value) {
	 takeStoredValueForKey(value, ECD_SECONDAIRE_KEY);
	}

	public String ecdSens() {
	 return (String) storedValueForKey(ECD_SENS_KEY);
	}

	public void setEcdSens(String value) {
	 takeStoredValueForKey(value, ECD_SENS_KEY);
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOAccordsContrat toAccordsContrat() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOAccordsContrat)storedValueForKey(TO_ACCORDS_CONTRAT_KEY);
	}

	public void setToAccordsContratRelationship(org.cocktail.fwkcktlcompta.server.metier.EOAccordsContrat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOAccordsContrat oldValue = toAccordsContrat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ACCORDS_CONTRAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ACCORDS_CONTRAT_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOEcriture toEcriture() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOEcriture)storedValueForKey(TO_ECRITURE_KEY);
	}

	public void setToEcritureRelationship(org.cocktail.fwkcktlcompta.server.metier.EOEcriture value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOEcriture oldValue = toEcriture();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ECRITURE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ECRITURE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOGestion toGestion() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
	}

	public void setToGestionRelationship(org.cocktail.fwkcktlcompta.server.metier.EOGestion value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOGestion oldValue = toGestion();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
	 }
	}

	public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer toPlanComptableExer() {
	 return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer)storedValueForKey(TO_PLAN_COMPTABLE_EXER_KEY);
	}

	public void setToPlanComptableExerRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer oldValue = toPlanComptableExer();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_EXER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_EXER_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> toBrouillardDetails() {
	 return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail>)storedValueForKey(TO_BROUILLARD_DETAILS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> toBrouillardDetails(EOQualifier qualifier) {
	 return toBrouillardDetails(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> toBrouillardDetails(EOQualifier qualifier, Boolean fetch) {
	 return toBrouillardDetails(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> toBrouillardDetails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean fetch) {
		 NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail.TO_ECRITURE_DETAIL_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toBrouillardDetails();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToBrouillardDetailsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_BROUILLARD_DETAILS_KEY);
	}
	
	public void removeFromToBrouillardDetailsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BROUILLARD_DETAILS_KEY);
	}
	
	public org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail createToBrouillardDetailsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_BROUILLARD_DETAILS_KEY);
	 return (org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail) eo;
	}
	
	public void deleteToBrouillardDetailsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BROUILLARD_DETAILS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToBrouillardDetailsRelationships() {
	 Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail> objects = toBrouillardDetails().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToBrouillardDetailsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOEcritureDetail avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEcritureDetail createEOEcritureDetail(EOEditingContext editingContext										, Integer ecdIndex
									, java.math.BigDecimal ecdMontant
									, java.math.BigDecimal ecdResteEmarger
									, String ecdSens
							, org.cocktail.fwkcktlcompta.server.metier.EOEcriture toEcriture		, org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice toExercice		, org.cocktail.fwkcktlcompta.server.metier.EOGestion toGestion		, org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer toPlanComptableExer					) {
	 EOEcritureDetail eo = (EOEcritureDetail) EOUtilities.createAndInsertInstance(editingContext, _EOEcritureDetail.ENTITY_NAME);	 
													eo.setEcdIndex(ecdIndex);
											eo.setEcdMontant(ecdMontant);
											eo.setEcdResteEmarger(ecdResteEmarger);
											eo.setEcdSens(ecdSens);
								 eo.setToEcritureRelationship(toEcriture);
				 eo.setToExerciceRelationship(toExercice);
				 eo.setToGestionRelationship(toGestion);
				 eo.setToPlanComptableExerRelationship(toPlanComptableExer);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEcritureDetail creerInstance(EOEditingContext editingContext) {
		EOEcritureDetail object = (EOEcritureDetail)EOUtilities.createAndInsertInstance(editingContext, _EOEcritureDetail.ENTITY_NAME);
  		return object;
		}

	

  public EOEcritureDetail localInstanceIn(EOEditingContext editingContext) {
    EOEcritureDetail localInstance = (EOEcritureDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail> eoObjects = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, Boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEcritureDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEcritureDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEcritureDetail> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEcritureDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEcritureDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEcritureDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEcritureDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEcritureDetail> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEcritureDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEcritureDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEcritureDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEcritureDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEcritureDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEcritureDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}