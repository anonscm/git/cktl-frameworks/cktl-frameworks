package org.cocktail.fwkcktlcompta.server.scolarite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.server.metier.EOCoriandreInscription;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigine;
import org.cocktail.fwkcktlcompta.server.scolarite.ItemPanier.Sens;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;

public class PanierTest {

	@Rule
	public MockEditingContext editingContext = new MockEditingContext("FwkCktlCompta");
	
	@Mock
	private EOSepaSddOrigine origine;
	@Mock
	private EOCoriandreInscription entity;
	@Dummy
	private EOSepaSddEcheancier echeancier1, echeancier2;
	@Dummy
	private EOSepaSddEcheance echeancier1Echeance1, echeancier1Echeance2, echeancier2Echeance1, echeancier2Echeance2;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(origine.toEntity()).thenReturn(entity);
        echeancier1.setToSepaSddOrigineRelationship(origine);
        echeancier2.setToSepaSddOrigineRelationship(origine);
	}
	
    @Test
    public void testAjouterPaiement() throws PaiementException {
        Article articleEtudiant1Licence1 = new Article(new BigDecimal("300.00"), "471328", "964");
        Article articleEtudiant1BU = new Article(new BigDecimal("20.00"), "471327", "900");
        
        Paiement paiementEtudiant1 = new Paiement(new BigDecimal("320.00"), "5115", "900");
        
        PanierPaiement panier = new PanierPaiement("RNEINUTILE", "900", new Date());
        panier.ajouter(Arrays.asList(articleEtudiant1BU, articleEtudiant1Licence1), Arrays.asList(paiementEtudiant1));
        
        DetailPanier detailPanier = panier.getDetail(panier.getClefDetail("471328", null));
        assertEquals(new BigDecimal("300.00"), detailPanier.getMontant());
        assertEquals(Sens.CREDIT, detailPanier.getSens());
        
        DetailPanier detailPanier2 = panier.getDetail(panier.getClefDetail("471327", null));
        assertEquals(new BigDecimal("20.00"), detailPanier2.getMontant());
        assertEquals(Sens.CREDIT, detailPanier2.getSens());
        
        DetailPanier detailPanier3 = panier.getDetail(panier.getClefDetail("5115", null));
        assertEquals(new BigDecimal("320.00"), detailPanier3.getMontant());
        assertEquals(Sens.DEBIT, detailPanier3.getSens());
        
    }
    
    @Test
    public void testAjouterPaiementSensDifferentSurPlancoDifferent() throws PaiementException {
        Article articleEtudiant1Licence1 = new Article(new BigDecimal("300.00"), "471328", "964");
        Article articleEtudiant1BU = new Article(new BigDecimal("-20.00"), "471327", "900");
        
        Paiement paiementEtudiant1 = new Paiement(new BigDecimal("280.00"), "5115", "900");
        
        PanierPaiement panier = new PanierPaiement("RNEINUTILE", "900", new Date());
        panier.ajouter(Arrays.asList(articleEtudiant1BU, articleEtudiant1Licence1), Arrays.asList(paiementEtudiant1));
        
        DetailPanier detailPanier = panier.getDetail(panier.getClefDetail("471328", null));
        assertEquals(new BigDecimal("300.00"), detailPanier.getMontant());
        assertEquals(Sens.CREDIT, detailPanier.getSens());
        
        DetailPanier detailPanier2 = panier.getDetail(panier.getClefDetail("471327", null));
        assertEquals(new BigDecimal("20.00"), detailPanier2.getMontant());
        assertEquals(Sens.DEBIT, detailPanier2.getSens());
        
        DetailPanier detailPanier3 = panier.getDetail(panier.getClefDetail("5115", null));
        assertEquals(new BigDecimal("280.00"), detailPanier3.getMontant());
        assertEquals(Sens.DEBIT, detailPanier3.getSens());
    }
    
    @Test
    public void testAjouterPaiementSensDifferentSurMemePlanco() throws PaiementException {
        Article articleEtudiant1Licence1 = new Article(new BigDecimal("300.00"), "471328", "964");
        Article articleEtudiant1BU = new Article(new BigDecimal("-20.00"), "471328", "964");
        
        Paiement paiementEtudiant1 = new Paiement(new BigDecimal("280.00"), "5115", "900");
        
        PanierPaiement panier = new PanierPaiement("RNEINUTILE", "900", new Date());
        panier.ajouter(Arrays.asList(articleEtudiant1BU, articleEtudiant1Licence1), Arrays.asList(paiementEtudiant1));
        
        DetailPanier detailPanier = panier.getDetail(panier.getClefDetail("471328", null));
        assertEquals(new BigDecimal("280.00"), detailPanier.getMontant());
        assertEquals(Sens.CREDIT, detailPanier.getSens());
        
        DetailPanier detailPanier3 = panier.getDetail(panier.getClefDetail("5115", null));
        assertEquals(new BigDecimal("280.00"), detailPanier3.getMontant());
        assertEquals(Sens.DEBIT, detailPanier3.getSens());
    }
    
    @Test
    public void testAjouterPaiementGesCodeDifferent() {
        Article articleEtudiant1Licence1 = new Article(new BigDecimal("300.00"), "471328", "964");
        Article articleEtudiant1Licence2 = new Article(new BigDecimal("400.00"), "471328", "900");
        
        Paiement paiementEtudiant1 = new Paiement(new BigDecimal("700.00"), "5115", "900");
        
        PanierPaiement panier = new PanierPaiement("RNEINUTILE", "900", new Date());
        boolean exception = false;
        try {
            panier.ajouter(Arrays.asList(articleEtudiant1Licence1, articleEtudiant1Licence2), Arrays.asList(paiementEtudiant1));
        } catch (PaiementException e) {
            exception = true;
            assertTrue(e.getCode() == PaiementException.Code.GES_CODE);
        }
        assertTrue("Une exception aurait du être levée pour cause de gesCode différent", exception);
    }
    
    @Test
    public void testAjouterPaiementBalanceFausse() {
        Article articleEtudiant1Licence1 = new Article(new BigDecimal("300.00"), "471328", "964");
        Article articleEtudiant1Licence2 = new Article(new BigDecimal("400.00"), "471328", "964");
        
        Paiement paiementEtudiant1 = new Paiement(new BigDecimal("600.00"), "5115", "900");
        
        PanierPaiement panier = new PanierPaiement("RNEINUTILE", "900", new Date());
        boolean exception = false;
        try {
            panier.ajouter(Arrays.asList(articleEtudiant1Licence1, articleEtudiant1Licence2), Arrays.asList(paiementEtudiant1));
        } catch (PaiementException e) {
            exception = true;
            assertTrue(e.getCode() == PaiementException.Code.BALANCE);
        }
        assertTrue("Une exception aurait du être levée pour cause de balance deséquilibrée", exception);
    }
    
    @Test
    public void testAjouterPaiementsGroupes() throws PaiementException {
        PanierPaiement panier = new PanierPaiement("RNEINUTILE", "900", new Date());
        Article articleEtudiant1Licence1 = new Article(new BigDecimal("300.00"), "471328", "964");
        Article articleEtudiant1BU = new Article(new BigDecimal("20.00"), "471327", "900");
        
        Paiement paiementEtudiant1 = new Paiement(new BigDecimal("320.00"), "5115", "900");

        panier.ajouter(Arrays.asList(articleEtudiant1BU, articleEtudiant1Licence1), Arrays.asList(paiementEtudiant1));
        
        Article articleEtudiant2Licence1 = new Article(new BigDecimal("350.00"), "471328", "964");
        Article articleEtudiant2BU = new Article(new BigDecimal("20.00"), "471327", "900");
        
        Paiement paiementEtudiant2 = new Paiement(new BigDecimal("370.00"), "5115", "900");
        
        panier.ajouter(Arrays.asList(articleEtudiant2BU, articleEtudiant2Licence1), Arrays.asList(paiementEtudiant2));
        
        DetailPanier detailPanier = panier.getDetail(panier.getClefDetail("471328", null));
        assertEquals(new BigDecimal("650.00"), detailPanier.getMontant());
        assertEquals(Sens.CREDIT, detailPanier.getSens());
        
        DetailPanier detailPanier2 = panier.getDetail(panier.getClefDetail("471327", null));
        assertEquals(new BigDecimal("40.00"), detailPanier2.getMontant());
        assertEquals(Sens.CREDIT, detailPanier2.getSens());
        
        DetailPanier detailPanier3 = panier.getDetail(panier.getClefDetail("5115", null));
        assertEquals(new BigDecimal("690.00"), detailPanier3.getMontant());
        assertEquals(Sens.DEBIT, detailPanier3.getSens());
        
    }
    
    @Test
    public void testAjouterPaiementsGroupesDont2Differes() throws PaiementException {
        PanierPaiement panier = new PanierPaiement("RNEINUTILE", "900", new Date());
        Article articleEtudiant1Licence1 = new Article(new BigDecimal("300.00"), "471328", "964");
        Article articleEtudiant1BU = new Article(new BigDecimal("20.00"), "471327", "900");
        
        Paiement paiementEtudiant1 = new Paiement(new BigDecimal("320.00"), "5115", "900");

        panier.ajouter(Arrays.asList(articleEtudiant1BU, articleEtudiant1Licence1), Arrays.asList(paiementEtudiant1));
        
        Article articleEtudiant2Licence1 = new Article(new BigDecimal("350.00"), "471328", "964");
        Article articleEtudiant2BU = new Article(new BigDecimal("20.00"), "471327", "900");
        
        Paiement paiementEtudiant2 = new Paiement(new BigDecimal("370.00"), "5115", "900");
        
        panier.ajouter(Arrays.asList(articleEtudiant2BU, articleEtudiant2Licence1), Arrays.asList(paiementEtudiant2));

        Article articleEtudiant3Licence1 = new Article(new BigDecimal("325.00"), "471328", "964");
        Article articleEtudiant3BU = new Article(new BigDecimal("18.00"), "471327", "900");
        
        echeancier1Echeance1.setToSepaSddEcheancierRelationship(echeancier1);
        echeancier1Echeance1.setMontant(new BigDecimal("173.00"));
        echeancier1Echeance1.setEtatEnum(ISepaSddEcheance.Etat.ATTENTE);
        echeancier1Echeance2.setToSepaSddEcheancierRelationship(echeancier1);
        echeancier1Echeance2.setMontant(new BigDecimal("170.00"));
        echeancier1Echeance2.setEtatEnum(ISepaSddEcheance.Etat.ATTENTE);
        
        Paiement paiementEtudiant3 = new Paiement(echeancier1, "5115", "900");
        
        panier.ajouter(Arrays.asList(articleEtudiant3BU, articleEtudiant3Licence1), Arrays.asList(paiementEtudiant3));

        Article articleEtudiant4Licence1 = new Article(new BigDecimal("325.00"), "471328", "964");
        
        echeancier2Echeance1.setToSepaSddEcheancierRelationship(echeancier2);
        echeancier2Echeance1.setMontant(new BigDecimal("175.00"));
        echeancier2Echeance1.setEtatEnum(ISepaSddEcheance.Etat.ATTENTE);
        echeancier2Echeance2.setToSepaSddEcheancierRelationship(echeancier2);
        echeancier2Echeance2.setMontant(new BigDecimal("150.00"));
        echeancier2Echeance2.setEtatEnum(ISepaSddEcheance.Etat.ATTENTE);
        
        Paiement paiementEtudiant4 = new Paiement(echeancier2, "5115", "900");
        
        panier.ajouter(Arrays.asList(articleEtudiant4Licence1), Arrays.asList(paiementEtudiant4));

        DetailPanier detailPanier = panier.getDetail(panier.getClefDetail("471328", null));
        assertEquals(new BigDecimal("1300.00"), detailPanier.getMontant());
        assertEquals(Sens.CREDIT, detailPanier.getSens());
        
        DetailPanier detailPanier2 = panier.getDetail(panier.getClefDetail("471327", null));
        assertEquals(new BigDecimal("58.00"), detailPanier2.getMontant());
        assertEquals(Sens.CREDIT, detailPanier2.getSens());
        
        DetailPanier detailPanier3 = panier.getDetail(panier.getClefDetail("5115", null));
        assertEquals(new BigDecimal("690.00"), detailPanier3.getMontant());
        assertEquals(Sens.DEBIT, detailPanier3.getSens());
        
        DetailPanier detailPanier4 = panier.getDetail(panier.getClefDetail("5115", echeancier1));
        assertEquals(new BigDecimal("343.00"), detailPanier4.getMontant());
        assertEquals(Sens.DEBIT, detailPanier4.getSens());

        DetailPanier detailPanier5 = panier.getDetail(panier.getClefDetail("5115", echeancier2));
        assertEquals(new BigDecimal("325.00"), detailPanier5.getMontant());
        assertEquals(Sens.DEBIT, detailPanier5.getSens());
    }
}
