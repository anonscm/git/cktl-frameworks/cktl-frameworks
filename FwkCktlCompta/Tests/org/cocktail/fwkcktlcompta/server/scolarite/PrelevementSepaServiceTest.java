package org.cocktail.fwkcktlcompta.server.scolarite;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHelper;
import org.cocktail.fwkcktlcompta.server.metier.EOCoriandreInscription;
import org.cocktail.fwkcktlcompta.server.metier.EOGrhumAdresse;
import org.cocktail.fwkcktlcompta.server.metier.EOGrhumBanque;
import org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcompta.server.metier.EOGrhumRib;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddMandatNumeroSeq;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddOrigineType;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;

@RunWith(value = MockitoJUnitRunner.class)
public class PrelevementSepaServiceTest {

    private static final int ID_SEPA_SDD_PARAM = 666;
    private static final int RIBORDRE_DEBITEUR = 777;
    private static final int ADRORDRE_DEBITEUR = 888;
    private static final int PERSID_DEBITEUR = 999;
    private static final int PERSID_GESTIONNAIRE = 999;
    private static final String LIBELLE_MANDAT = "Frais d'inscription 2013 - Etudiant n° 179478";
    
    @Rule
    public MockEditingContext mockEditingContext = new MockEditingContext("FwkCktlCompta");
    @Dummy
    EOGrhumPersonne createur, debiteur, creancier;
    @Dummy
    EOGrhumAdresse adresseDebiteur;
    @Dummy
    EOGrhumBanque banque;
    @Dummy
    EOGrhumRib ribDebiteur;
    @Dummy
    EOSepaSddParam sepaParam;
    @Dummy
    EOJefyAdminTypeEtat typeEtatAValider;
    @Dummy
    EOSepaSddMandatNumeroSeq sequence;
    
    PrelevementSepaService prelevementSepaService;
    
    @Before
    public void setUp() {
        FwkCktlComptaMoteurCtrl.getSharedInstance().setRefApplicationCreation("CORIANDRE");
        createur.setPersId(PERSID_GESTIONNAIRE);
        debiteur.setPersId(PERSID_DEBITEUR);
        adresseDebiteur.setAdrOrdre(ADRORDRE_DEBITEUR);
        banque.setBic("EFZFZFZF");
        
        ribDebiteur.setRibOrdre(RIBORDRE_DEBITEUR);
        ribDebiteur.setToBanqueRelationship(banque);
        ribDebiteur.setIban("FR7630004008751522996610181");
        
        sepaParam.setIdSepaSddParam(ID_SEPA_SDD_PARAM);
        sepaParam.setEtat("VALIDE");
        sepaParam.setToPersonneCreancierRelationship(creancier);
        sepaParam.setCreancierIcs("FR44ZZZ214214");
        sequence.setNextval(258);
        
        typeEtatAValider.setTyetLibelle(EOJefyAdminTypeEtat.ETAT_A_VALIDER);
        prelevementSepaService = new PrelevementSepaService(PERSID_GESTIONNAIRE, ID_SEPA_SDD_PARAM, mockEditingContext);
    }
    
    @Test
    public void testCreerMandatSepa() {
        prelevementSepaService.creerMandat(LIBELLE_MANDAT, PERSID_DEBITEUR, ADRORDRE_DEBITEUR, RIBORDRE_DEBITEUR, null);
        mockEditingContext.saveChanges();
    }
    
    @Test
    public void testCreerEcheancier() throws ParseException {
        FwkCktlComptaMoteurCtrl.getSharedInstance().init(FwkCktlComptaMoteurCtrl.Contexte.SERVEUR);
        
        EOSepaSddMandat mandat = mockEditingContext.createSavedObject(EOSepaSddMandat.class);
        mandat.setCTypePrelevement(SepaSddMandatHelper.TYPE_PRELEVEMENT_CODE_RECURRENT);
        mandat.setToDebiteurPersonneRelationship(debiteur);
        EOSepaSddOrigineType typeOrigine = mockEditingContext.createSavedObject(EOSepaSddOrigineType.class);
        typeOrigine.setTypeCode(EOSepaSddOrigineType.TYPE_CODE_CORIANDRE);
        typeOrigine.setTypeNom("Droit d'inscription Coriandre");
        typeOrigine.setTypeClass("org.cocktail.fwkcktlcompta.common.sepasdd.origines.SepaSddOrigineCtrlCoriandreInscription");
        EOCoriandreInscription coriandreInscription = mockEditingContext.createSavedObject(EOCoriandreInscription.class);
        Integer inscriptionId = Integer.valueOf(coriandreInscription.primaryKey());
        coriandreInscription.setIdentifiant(inscriptionId);
        coriandreInscription.setToPersonneRelationship(debiteur);
        
        List<IEcheanceLight> echeances = new ArrayList<IEcheanceLight>();
        Calendar cal = Calendar.getInstance();
        echeances.add(new EcheanceDummy(new BigDecimal("200.00"), nextDateEcheance(cal)));
        echeances.add(new EcheanceDummy(new BigDecimal("200.00"), nextDateEcheance(cal)));
        echeances.add(new EcheanceDummy(new BigDecimal("200.00"), nextDateEcheance(cal)));
        
        prelevementSepaService.creerEcheancier(mandat, echeances, inscriptionId);
        mockEditingContext.saveChanges();
    }
    
    private Date nextDateEcheance(Calendar cal) {
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        return cal.getTime();
    }
    
    public static class EcheanceDummy implements IEcheanceLight {

        private BigDecimal montant;
        private Date dateEcheance;
        
        public EcheanceDummy(BigDecimal montant, Date dateEcheance) {
            this.montant = montant;
            this.dateEcheance = dateEcheance;
        }

        public BigDecimal montant() {
            return montant;
        }

        public Date dateEcheance() {
            return dateEcheance;
        }
        
    }
    
}
