package org.cocktail.fwkcktlcompta.server.scolarite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import org.cocktail.fwkcktlcompta.common.entities.IBordereau;
import org.cocktail.fwkcktlcompta.server.metier.EOBordereau;
import org.cocktail.fwkcktlcompta.server.metier.EOBordereauBrouillard;
import org.cocktail.fwkcktlcompta.server.metier.EOBordereauInfo;
import org.cocktail.fwkcktlcompta.server.metier.EOGestion;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeBordereau;
import org.cocktail.fwkcktlcompta.server.scolarite.PaiementService.GFCApplicationUserProvider;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktljefyadmin.common.GFCApplicationUser;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;

@RunWith(value = MockitoJUnitRunner.class)
public class PaiementServiceTest {

    PaiementService paiementService;
    @Mock
    UserInfo userInfo;
    @Mock
    GFCApplicationUser gfcApplicationUser;
    @Mock
    IPlanComptableRepository planCoRepository;
    
    @Dummy
    EOExercice exercice;
    @Dummy
    EOJefyAdminExercice jefyAdminExercice;
    @Dummy
    EOTypeBordereau typeBordereau;
    @Dummy
    EOGestion gestion1, gestion2;
    @Dummy
    EOPlanComptable planco1, planco2, planco3;
    @Dummy
    EOPlanComptableExer plancoExer1, plancoExer2, plancoExer3;
    @Dummy
    EOUtilisateur utilisateur;
    
    @Rule
    public MockEditingContext mockEditingContext = new MockEditingContext("FwkCktlCompta");
    
    private String codeRne = "0171463Y";
    
    @Before
    public void setUp() throws PaiementException {
        Mockito.when(userInfo.persId()).thenReturn(960);
        Mockito.when(gfcApplicationUser.hasDroitCreerBordereauInscription()).thenReturn(true);
        Mockito.when(gfcApplicationUser.getUtilisateur()).thenReturn(utilisateur);
        paiementService = new PaiementService(userInfo, mockEditingContext);
        paiementService.setUserInfo(userInfo);
        paiementService.setGfcApplicationUserProvider(getGFCApplicationUserProviderBouchon());
        exercice.setExeExercice(Long.valueOf(2014));
        exercice.setExeStat(EOExercice.EXE_ETAT_OUVERT);
        typeBordereau.setTboType(EOTypeBordereau.TypeBordereauDroitsUniversitaires);
        typeBordereau.setTboSousType(EOTypeBordereau.SOUS_TYPE_SCOLARITE);
        gestion1.setGesCode("900");
        gestion2.setGesCode("964");
        
        planco1.setPcoNum("471328");
        planco2.setPcoNum("471327");
        planco3.setPcoNum("5115");

        Mockito.when(planCoRepository.fetchPlanComptable("471328", Long.valueOf(2014))).thenReturn(planco1);
        Mockito.when(planCoRepository.fetchPlanComptable("471327", Long.valueOf(2014))).thenReturn(planco2);
        Mockito.when(planCoRepository.fetchPlanComptable("5115", Long.valueOf(2014))).thenReturn(planco3);
        paiementService.setPlanCoRepository(planCoRepository);
    }

    private GFCApplicationUserProvider getGFCApplicationUserProviderBouchon() {
        return new GFCApplicationUserProvider() {
            
            public GFCApplicationUser getGFCApplicationUser() {
                return gfcApplicationUser;
            }
        };
    }

    @Test
    public void testEnregistrerPaiement() throws PaiementException {
        PanierPaiement panier = creerPanierDeTest();

        IBordereau bordereau = paiementService.enregistrerPaiement(panier);
        assertTrue(bordereau.toBordereauBrouillards().size() == 3);
        assertEquals("2014 - BORDEREAU DROITS UNIVERSITAIRES pour " + codeRne + " N° 2014041017 en date du 10/04/2014", bordereau.toBordereauInfos().get(0).borLibelle());
        BigDecimal somme = EOBordereauBrouillard.BOB_MONTANT.atSum().valueInObject(bordereau.toBordereauBrouillards());
        assertEquals(new BigDecimal("1080.00"), somme);
        
        mockEditingContext.saveChanges();
    }

    private PanierPaiement creerPanierDeTest() throws PaiementException {
        
        String gesCode = "900";
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2014);
        cal.set(Calendar.DAY_OF_MONTH, 10);
        cal.set(Calendar.MONTH, Calendar.APRIL);
        Date datePaiement = cal.getTime();
        PanierPaiement panier = new PanierPaiement(codeRne, gesCode, datePaiement);

        Article articleEtudiant1Licence1 = new Article(new BigDecimal("300.00"), "471328", "964");
        Article articleEtudiant1BU = new Article(new BigDecimal("20.00"), "471327", "900");
        Paiement paiementEtudiant1 = new Paiement(new BigDecimal("320.00"), "5115", "900");
        panier.ajouter(Arrays.asList(articleEtudiant1Licence1, articleEtudiant1BU), Arrays.asList(paiementEtudiant1));

        Article articleEtudiant2Licence1 = new Article(new BigDecimal("200.00"), "471328", "964");
        Article articleEtudiant2BU = new Article(new BigDecimal("20.00"), "471327", "900");
        Paiement paiementEtudiant2 = new Paiement(new BigDecimal("220.00"), "5115", "900");
        panier.ajouter(Arrays.asList(articleEtudiant2Licence1, articleEtudiant2BU), Arrays.asList(paiementEtudiant2));
        return panier;
    }

    @Test
    public void testEnregistrementPaiementSiExistantEtNonVise() throws PaiementException {
        EOBordereauBrouillard brouillard = mockEditingContext.createSavedObject(EOBordereauBrouillard.class);
        EOBordereauInfo info = mockEditingContext.createSavedObject(EOBordereauInfo.class);
        info.setBorLibelle("2014 - BORDEREAU DROITS UNIVERSITAIRES pour " + codeRne + "N° 20140410 en date du 10/04/2014");
        EOBordereau bordereauExistant = mockEditingContext.createSavedObject(EOBordereau.class);
        bordereauExistant.setBorEtat(EOBordereau.BOR_ETAT_VALIDE);
        bordereauExistant.setBorNum(2014041017);
        bordereauExistant.setToTypeBordereauRelationship(typeBordereau);
        bordereauExistant.addToToBordereauBrouillardsRelationship(brouillard);
        bordereauExistant.addToToBordereauInfosRelationship(info);
        bordereauExistant.setToGestionRelationship(gestion1);
        
        Calendar cal = Calendar.getInstance();
        cal.set(2014, 3, 10);
        bordereauExistant.setBorDateCreation(new NSTimestamp(cal.getTime()));
        
        
        PanierPaiement panier = creerPanierDeTest();
        paiementService.enregistrerPaiement(panier);
        
        mockEditingContext.saveChanges();
        assertTrue(bordereauExistant.isDeletedEO());
        assertTrue(brouillard.isDeletedEO());
        assertTrue(info.isDeletedEO());
        assertTrue(bordereauExistant.toBordereauBrouillards().isEmpty());
        assertTrue(bordereauExistant.toBordereauInfos().isEmpty());
    }
    
    @Test
    public void testEnregistrementPaiementSiExistantVise() throws PaiementException {
        EOBordereau bordereauExistant = mockEditingContext.createSavedObject(EOBordereau.class);
        bordereauExistant.setBorEtat(EOBordereau.BOR_ETAT_VISE);
        bordereauExistant.setBorNum(2014041017);
        bordereauExistant.setToTypeBordereauRelationship(typeBordereau);
        bordereauExistant.setToGestionRelationship(gestion1);
        
        Calendar cal = Calendar.getInstance();
        cal.set(2014, 3, 10);
        bordereauExistant.setBorDateCreation(new NSTimestamp(cal.getTime()));
        
        EOBordereauInfo bordereauInfoExistant = mockEditingContext.createSavedObject(EOBordereauInfo.class);
        bordereauInfoExistant.setBorLibelle("2014 - BORDEREAU DROITS UNIVERSITAIRES pour " + codeRne + "N° 20140410 en date du 10/04/2014");
        bordereauExistant.addToToBordereauInfosRelationship(bordereauInfoExistant);
        
        PanierPaiement panier = creerPanierDeTest();
        boolean exception = false;
        try {
            paiementService.enregistrerPaiement(panier);
        } catch (PaiementException e) {
            exception = true;
            assertTrue(e.getCode() == PaiementException.Code.VISA_EXISTANT);
        }
        assertTrue("Une exception aurait du être levée pour cause de visa existant", exception);
    }
    
    @Test
    public void testEnregistrementPaiementSiExerciceNonValide() throws PaiementException {
        exercice.setExeStat(EOExercice.EXE_ETAT_CLOS);
        PanierPaiement panier = creerPanierDeTest();
        
        boolean exception = false;
        try {
            paiementService.enregistrerPaiement(panier);
        } catch (PaiementException e) {
            exception = true;
            assertTrue(e.getCode() == PaiementException.Code.EXERCICE);
        }
        assertTrue("Une exception aurait du être levée pour cause d'exercice invalide", exception);
    }
    
    @Test
    public void testExistanceBordereau() throws PaiementException {
        EOBordereau bordereauExistant = mockEditingContext.createSavedObject(EOBordereau.class);
        bordereauExistant.setBorEtat(EOBordereau.BOR_ETAT_VISE);
        bordereauExistant.setBorNum(2014041017);
        bordereauExistant.setToTypeBordereauRelationship(typeBordereau);
        bordereauExistant.setToGestionRelationship(gestion1);
        
        
        Calendar cal = Calendar.getInstance();
        cal.set(2014, 3, 10);
        bordereauExistant.setBorDateCreation(new NSTimestamp(cal.getTime()));
        
        EOBordereauInfo bordereauInfoExistant = mockEditingContext.createSavedObject(EOBordereauInfo.class);
        bordereauInfoExistant.setBorLibelle("2014 - BORDEREAU DROITS UNIVERSITAIRES pour " + codeRne + "N° 20140410 en date du 10/04/2014");
        bordereauExistant.addToToBordereauInfosRelationship(bordereauInfoExistant);

        
        boolean existe = paiementService.getBordereauPaiement(2014041017, gestion1.gesCode()) != null;
        
        assertTrue("Un bordereau aurait dû être trouvé", existe);
    }
    
    @Test
    public void testNonExistanceBordereau() throws PaiementException {
        EOBordereau bordereauExistant = mockEditingContext.createSavedObject(EOBordereau.class);
        bordereauExistant.setBorEtat(EOBordereau.BOR_ETAT_VISE);
        bordereauExistant.setBorNum(2014041017);
        bordereauExistant.setToTypeBordereauRelationship(typeBordereau);
        bordereauExistant.setToGestionRelationship(gestion1);
        
        Calendar cal = Calendar.getInstance();
        cal.set(2014, 3, 11);
        
        boolean existe = paiementService.getBordereauPaiement(2014041117, gestion1.gesCode()) != null;
        
        assertFalse("Un bordereau n'aurait pas dû être trouvé", existe);
    }
    
    @Test
    public void testSupprimerBordereau() throws PaiementException {
 
        EOBordereau bordereauExistant = mockEditingContext.createSavedObject(EOBordereau.class);
        bordereauExistant.setBorEtat(EOBordereau.BOR_ETAT_VALIDE);
        bordereauExistant.setBorNum(2014041017);
        bordereauExistant.setToTypeBordereauRelationship(typeBordereau);
        bordereauExistant.setToGestionRelationship(gestion1);
        
        
        Calendar cal = Calendar.getInstance();
        cal.set(2014, 3, 10);
        bordereauExistant.setBorDateCreation(new NSTimestamp(cal.getTime()));
        
        EOBordereauInfo bordereauInfoExistant = mockEditingContext.createSavedObject(EOBordereauInfo.class);
        bordereauInfoExistant.setBorLibelle("2014 - BORDEREAU DROITS UNIVERSITAIRES pour " + codeRne + "N° 20140410 en date du 10/04/2014");
        bordereauExistant.addToToBordereauInfosRelationship(bordereauInfoExistant);

        boolean existe = paiementService.getBordereauPaiement(2014041017, gestion1.gesCode()) != null;
        assertTrue("Un bordereau aurait dû être trouvé", existe);
        paiementService.supprimerBordereau(bordereauExistant);

        existe = paiementService.getBordereauPaiement(2014041017, gestion1.gesCode()) != null;

        assertFalse("Un bordereau n'aurait pas dû être trouvé", existe);
    }
}
