package org.cocktail.fwkcktlcompta.server.scolarite;

import static org.junit.Assert.assertTrue;

import org.cocktail.fwkcktlcompta.common.entities.IPlanComptable;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;

public class PlanComptableRepositoryTest {
    
    @Dummy
    EOPlanComptable planco1;
    @Dummy
    EOPlanComptableExer plancoExer1;
    @Dummy
    EOJefyAdminExercice jefyAdminExercice;
    
    @Rule
    public MockEditingContext mockEditingContext = new MockEditingContext("FwkCktlCompta");
    
    private PlanComptableRepository planComptableRepository;
    
    @Before
    public void setup() {
        jefyAdminExercice.setExeExercice(2014);
        planco1.setPcoNum("471328");
        plancoExer1.setToExerciceRelationship(jefyAdminExercice);
        plancoExer1.setPcoNum("471328");
        plancoExer1.setPcoValidite(EOPlanComptableExer.etatValide);
        planco1.addToToPlancoExersRelationship(plancoExer1);
    }
    
    @Test
    public void testFetchPlanComptable() throws PaiementException {
        planComptableRepository = new PlanComptableRepository(mockEditingContext);
        IPlanComptable planCo = planComptableRepository.fetchPlanComptable("471328", Long.valueOf(2014));
        assertTrue(planco1.equals(planCo));
    }
    
    @Test
    public void testFetchPlanComptableInexistant() throws PaiementException {
        planComptableRepository = new PlanComptableRepository(mockEditingContext);
        boolean exception = false;
        try {
            planComptableRepository.fetchPlanComptable("9999", Long.valueOf(2014));
        } catch (PaiementException e) {
            exception = true;
            assertTrue(e.getCode() == PaiementException.Code.PLANCO);
        }
        assertTrue("Une exception aurait du être levée pour cause de planco inexistant", exception);
        
    }
    
}
