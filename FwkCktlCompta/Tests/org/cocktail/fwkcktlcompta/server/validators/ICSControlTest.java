package org.cocktail.fwkcktlcompta.server.validators;

import static org.junit.Assert.assertNull;

import org.cocktail.fwkcktlcompta.server.sepasdd.validators.ICSControl;
import org.junit.Test;

/**
 * Classe pour vérifier la validité d'un ICS (Identifiant Créancier SEPA)
 * 
 * @author rprin
 */
public class ICSControlTest {
	private static final String ICS_OK = "FR44ZZZ214214";

	@Test(expected = java.lang.NullPointerException.class)
	public void testICSNull() throws Exception {
		ICSControl ctrl = new ICSControl();
		ctrl.checkICS(null);
	}

	@Test(expected = org.cocktail.fwkcktlcompta.common.exception.SizeErrorException.class)
	public void testICSTooLong() throws Exception {
		ICSControl ctrl = new ICSControl();
		ctrl.checkICS("12345678910123456458");
	}

	@Test
	public void testICSOk() {
		ICSControl ctrl = new ICSControl();
		String erreur = null;
		try {
			ctrl.checkICS(ICS_OK);
		} catch (Exception e) {
			erreur = "erreur :  " + e.getMessage();
		}

		assertNull("Il ne devrait pas y avoir d'erreur, l'ICS fourni (" + ICS_OK + ") est valide", erreur);
	}

}
