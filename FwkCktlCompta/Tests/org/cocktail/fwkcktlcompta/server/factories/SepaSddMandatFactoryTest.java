package org.cocktail.fwkcktlcompta.server.factories;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumAdresse;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumRib;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddParam;
import org.cocktail.fwkcktlcompta.server.sepasdd.factories.SepaSddMandatFactory;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;

/**
 * Test pour verifier les algos de regroupement d'echeances.
 *
 * @author rprin
 */
public class SepaSddMandatFactoryTest {

	ISepaSddParam param = new ISepaSddParam() {

		public String creancierIcs() {
			return "FRCCZZZ414414";
		}

		public String etat() {
			return null;
		}

		public String getLibelle() {
			return null;
		}

		public IGrhumPersonne toPersonneCreancier() {
			return null;
		}
	};

	ISepaSddMandat mandat = new ISepaSddMandat() {

		public ISepaSddParam toSepaSddParam() {
			return param;
		}

		public String libelle() {
			return null;
		}

		public void setLibelle(String libelle) {
		}
		
		public String rum() {
			return null;
		}

		public String numero() {
			return SepaSddMandatFactory.MANDAT_NUMERO_FORMAT.format(2);
		}

		public String nomPrenomDebiteur() {
			return null;
		}

		public EOEditingContext editingContext() {
			return null;
		}

		public List<ISepaSddEcheancier> echeanciers() {
			return null;
		}

		public String dMandatSignature() {
			return null;
		}

		public String dMandatCreation() {
			return null;
		}

		public String cTypePrelevement() {
			return null;
		}

		public String bicIbanDebiteur() {
			return null;
		}

		public IGrhumPersonne toCreancierPersonne() {
			return null;
		}

		public String refAppliCreation() {
			return null;
		}

		public IGrhumPersonne toDebiteurPersonne() {
			return null;
		}

		public IGrhumPersonne toTiersDebiteurPersonne() {
			return null;
		}

		public String tiersDebiteurNomPrenom() {
			return null;
		}

		public IGrhumPersonne clientPersonne() {
			return null;
		}

		public Boolean isRecurrent() {
			return true;
		}

		public NSArray getEcheancesTrieesParEtatDifferentDe(Etat etat, boolean fetch) {
			return null;
		}

		public NSArray getEcheancesTrieesParEtat(Etat etat, boolean fetch) {
			return null;
		}

		public NSArray getEcheancesTriees(boolean fetch) {
			return null;
		}

		public void setToTypeEtatRelationship(IJefyAdminTypeEtat typeEtat) {
		}

		public IJefyAdminTypeEtat toTypeEtat() {
			return null;
		}

		public IGrhumRib toDebiteurRib() {
			return null;
		}

		public EOEnterpriseObject toEnterpriseObject() {
			return null;
		}

		public NSArray toSepaSddMandatHistos() {
			return NSArray.emptyArray();
		}

		public String commentaire() {
			return null;
		}

		public void setToModificateurRelationship(IGrhumPersonne personne) {
		}

		public String dCreation() {
			return null;
		}

		public String dModification() {
			return null;
		}

		public void setDCreation(String dateCreation) {
		}

		public void setDModification(String dateModification) {
		}

		public void setToCreancierPersonneRelationship(IGrhumPersonne personne) {
		}

		public void setToDebiteurPersonneRelationship(IGrhumPersonne personne) {
		}

		public void setToDebiteurAdresseRelationship(IGrhumAdresse adresse) {
		}

		public void setToTiersDebiteurPersonneRelationship(IGrhumPersonne personne) {
		}

		public void setDMandatCreation(String dateCreation) {
		}

		public void setDMandatSignature(String dateSignature) {
		}

		public void setRum(String rum) {
		}

		public void setRefAppliCreation(String refAppliCreation) {
		}

        public Integer idMandat() {
            return null;
        }

        public IGrhumAdresse toDebiteurAdresse() {
            return null;
        }

        public void setToDebiteurRibRelationship(IGrhumRib rib) {
        }

        public void setToSepaSddParamRelationship(ISepaSddParam param) {
        }

        public void setCTypePrelevement(String type) {
        }
	};

	/**
	 *
	 */
	@Test
	public void testCreerRUMPourMandat() {
		SepaSddMandatFactory factory = new SepaSddMandatFactory();
		Exception lastException = null;
		try {
			String rum = factory.creerRUMPourMandat(mandat);
			assertEquals(SepaSddMandatFactory.RUM_STANDARD_MAX_LENGTH, Integer.valueOf(rum.length()));
			assertEquals("E", rum.substring(14, 15));
			assertEquals("MAN", rum.substring(20, 23));

		} catch (Exception e) {
			e.printStackTrace();
			lastException = e;
		}
		assertNull("Il ne devrait pas y avoir d'exception", lastException);
	}

}
