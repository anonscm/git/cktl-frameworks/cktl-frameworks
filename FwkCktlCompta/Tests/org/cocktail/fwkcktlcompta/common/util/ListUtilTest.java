package org.cocktail.fwkcktlcompta.common.util;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;
import java.util.List;

import org.junit.Test;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class ListUtilTest {



	@Test
	public void testCreateListFromCommaSeparatedString() {
		String string = "AA, BB,       J;J,ZZ   ";

		List<String> list = ZListUtil.createListFromCommaSeparatedStringAndTrim(string);
		assertEquals("AA", list.get(0));
		assertEquals("BB", list.get(1));
		assertEquals("J;J", list.get(2));
		assertEquals("ZZ", list.get(3));
	}

}
