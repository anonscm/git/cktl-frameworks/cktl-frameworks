package org.cocktail.fwkcktlcompta.common.util;

import static org.junit.Assert.assertEquals;

import java.util.GregorianCalendar;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Test;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class DateConversionUtilTest {

	private static final String STR_DATE_WITHOUT_TIME = "2013-06-25";
	private static final String STR_DATE_SHORT = "25/06/2013";
	private static final String STR_DATE_WITH_TIME = "2013-06-25T12:11:59.200";

	/**
	 * @return La Date/Heure correspondant au 25/06/2013
	 */
	public static LocalDateTime buildNewDateTimeWithoutTime() {
		GregorianCalendar cal = new GregorianCalendar();
		cal.set(GregorianCalendar.YEAR, 2013);
		cal.set(GregorianCalendar.MONTH, 5);
		cal.set(GregorianCalendar.DAY_OF_MONTH, 25);

		LocalDateTime lcdt = new LocalDateTime(cal.getTime());
		return lcdt;
	}

	/**
	 * @return La Date/Heure correspondant au 25/06/2013 12:11:59 200ms
	 */
	public static LocalDateTime buildNewDateTimeWithTime() {
		GregorianCalendar cal = new GregorianCalendar();
		cal.set(GregorianCalendar.YEAR, 2013);
		cal.set(GregorianCalendar.MONTH, 5);
		cal.set(GregorianCalendar.DAY_OF_MONTH, 25);
		cal.set(GregorianCalendar.HOUR_OF_DAY, 12);
		cal.set(GregorianCalendar.MINUTE, 11);
		cal.set(GregorianCalendar.SECOND, 59);
		cal.set(GregorianCalendar.MILLISECOND, 200);

		LocalDateTime lcdt = new LocalDateTime(cal.getTime());
		return lcdt;
	}

	@Test
	public void testFormatDateOnlyISO() {
		LocalDateTime ldt = buildNewDateTimeWithoutTime();
		DateConversionUtil dcu = new DateConversionUtil();
		String res = dcu.formatDateWithoutTimeISO(ldt);
		assertEquals(STR_DATE_WITHOUT_TIME, res);
	}

	@Test
	public void testFormatDateTimeISO() {
		LocalDateTime ldt = buildNewDateTimeWithTime();
		DateConversionUtil dcu = new DateConversionUtil();
		String dateOnly = dcu.formatDateWithoutTimeISO(ldt);
		assertEquals(STR_DATE_WITHOUT_TIME, dateOnly);
		String dateTime = dcu.formatDateWithTimeISO(ldt);
		assertEquals(STR_DATE_WITH_TIME, dateTime);
	}

	@Test
	public void testParseDateOnlyISO() {
		DateConversionUtil dcu = new DateConversionUtil();
		LocalDate dt = dcu.parseDate(STR_DATE_WITH_TIME);
		assertEquals(STR_DATE_WITHOUT_TIME, dcu.formatDateWithoutTimeISO(dt));
	}

	@Test
	public void testParseDateTimeISO() {
		DateConversionUtil dcu = new DateConversionUtil();
		LocalDateTime dt = dcu.parseDateTime(STR_DATE_WITH_TIME);
		assertEquals(STR_DATE_WITH_TIME, dcu.formatDateWithTimeISO(dt));
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testParseDateTimeISOErreur() {
		DateConversionUtil dcu = new DateConversionUtil();
		dcu.parseDateTime("201315-");
	}

	public void testparseDateShort() {
		LocalDateTime ldt = buildNewDateTimeWithoutTime();
		DateConversionUtil dcu = new DateConversionUtil();
		String res = dcu.formatDateWithoutTimeISO(ldt);
		assertEquals(STR_DATE_SHORT, res);
	}

}
