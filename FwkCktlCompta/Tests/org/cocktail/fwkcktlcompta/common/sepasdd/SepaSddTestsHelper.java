package org.cocktail.fwkcktlcompta.common.sepasdd;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumFournis;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;

import com.webobjects.foundation.NSArray;

public class SepaSddTestsHelper {

	private final static IGrhumPersonne modificateur = new IGrhumPersonne() {
		IGrhumFournis fournis = new IGrhumFournis() {

			public NSArray toRibsValide() {
				return null;
			}

			public String fouType() {
				return "T";
			}

			public String fouCode() {
				return "TESTCODE";
			}

			public String fouValide() {
				return null;
			}
		};

		public String persLibelle() {
			return "Nom Prénom";
		}

		public String persLc() {
			return "Nom Prénom";
		}

		public IGrhumFournis getDerivFournis() {
			return fournis;
		}

		public Integer persId() {
			return 1;
		}
	};

	public static IGrhumPersonne getModificateur() {
		return modificateur;
	}

	public static ISepaSddEcheancier mockEcheancier(final BigDecimal montant, final Integer nbEcheances, final String datePremiereEcheance) {
		return new MockSepaSddEcheancier() {
			@Override
			public BigDecimal montantAPayer() {
				return montant;
			}

			@Override
			public Integer nombreEcheancesPrevues() {
				return nbEcheances;
			}

			@Override
			public String datePremiereEcheance() {
				return datePremiereEcheance;
			}
		};
	}


}
