package org.cocktail.fwkcktlcompta.common.sepasdd.helpers;

import static org.junit.Assert.assertEquals;

import org.cocktail.fwkcktlcompta.common.entities.IEcriture;
import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;
import org.cocktail.fwkcktlcompta.common.metier.MockEcriture;
import org.cocktail.fwkcktlcompta.common.metier.MockEcritureDetail;
import org.cocktail.fwkcktlcompta.common.metier.MockJefyAdminExercice;
import org.junit.Test;

import com.ibm.icu.text.SimpleDateFormat;
import com.webobjects.foundation.NSTimestamp;

/**
 * Test pour verifier le tri.
 * 
 * @author rprin
 */
public class TriEcritureDetailParDateTest {


	@Test
	public void testCompare() {
		try {
			TriEcritureDetailParDate tri = new TriEcritureDetailParDate();

			final IJefyAdminExercice exercice2013 = new MockJefyAdminExercice(2013, null, null);
			final IJefyAdminExercice exercice2014 = new MockJefyAdminExercice(2014, null, null);
			final NSTimestamp date1 = new NSTimestamp(new SimpleDateFormat("dd/MM/yyyy").parse("31/12/2013"));
			final NSTimestamp date2 = new NSTimestamp(new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2014"));
			final NSTimestamp date3 = new NSTimestamp(new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2014"));

			final IEcriture ecr1 = new MockEcriture() {
				@Override
				public IJefyAdminExercice toExercice() {
					return exercice2013;
				}

				@Override
				public NSTimestamp ecrDate() {
					return date1;
				}

				@Override
				public Integer ecrNumero() {
					return Integer.valueOf(50);
				}
			};
			final IEcriture ecr2 = new MockEcriture() {
				@Override
				public IJefyAdminExercice toExercice() {
					return exercice2013;
				}

				@Override
				public NSTimestamp ecrDate() {
					return date2;
				}

				@Override
				public Integer ecrNumero() {
					return Integer.valueOf(60);
				}
			};
			final IEcriture ecr3 = new MockEcriture() {
				@Override
				public IJefyAdminExercice toExercice() {
					return exercice2014;
				}

				@Override
				public NSTimestamp ecrDate() {
					return date3;
				}

				@Override
				public Integer ecrNumero() {
					return Integer.valueOf(1);
				}
			};

			
			
			final IEcritureDetail ecd1 = new MockEcritureDetail(ecr1);
			final IEcritureDetail ecd2 = new MockEcritureDetail(ecr2);
			final IEcritureDetail ecd3 = new MockEcritureDetail(ecr3);
			final IEcritureDetail ecd4 = new MockEcritureDetail(ecr3);
			

			
			assertEquals(-1, tri.compare(ecd1, ecd2));
			assertEquals(1, tri.compare(ecd2, ecd1));
			assertEquals(0, tri.compare(ecd1, ecd1));

			assertEquals(-1, tri.compare(ecd1, ecd3));
			assertEquals(-1, tri.compare(ecd2, ecd3));
			assertEquals(-1, tri.compare(ecd1, ecd4));
			assertEquals(-1, tri.compare(ecd2, ecd4));

			assertEquals(1, tri.compare(ecd3, ecd1));
			assertEquals(1, tri.compare(ecd3, ecd2));
			assertEquals(1, tri.compare(ecd4, ecd1));
			assertEquals(1, tri.compare(ecd4, ecd2));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
