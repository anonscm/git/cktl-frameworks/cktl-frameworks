package org.cocktail.fwkcktlcompta.common.sepasdd.helpers;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;
import org.cocktail.fwkcktlcompta.common.entities.IEcriture;
import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumBanque;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumRib;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;
import org.cocktail.fwkcktlcompta.common.metier.MockEcriture;
import org.cocktail.fwkcktlcompta.common.metier.MockEcritureDetail;
import org.cocktail.fwkcktlcompta.common.metier.MockJefyAdminExercice;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.SepaSddTestsHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheanceEcd;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.SepaSddGenerateurEcheancierConstant;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.joda.time.LocalDate;
import org.junit.Test;

import com.ibm.icu.text.SimpleDateFormat;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Test pour verifier les algos de regroupement d'echeances.
 *
 * @author rprin
 */
public class SepaSddEcheanceHelperTest {

	private static final DateConversionUtil DATE_CONVERSION_UTIL = new DateConversionUtil();
	private static final String DATE_PREMIERE_ECHEANCE_AS_STRING = "2013-01-20";
	private static final LocalDate DATE_PREMIERE_ECHEANCE = DATE_CONVERSION_UTIL.parseDateSilent("2013-01-20");
	private static final Integer PERS_ID = Integer.valueOf(1);
	private static final IGrhumPersonne MODIFICATEUR = SepaSddTestsHelper.getModificateur();




	/**
	 * Verifie qu'un echeancier sur un mandat de type ponctuel est en OOFF.
	 */
	@Test
	public void testDetermineTypeOperation1Echeance() {
		ISepaSddMandat mockMandat1 = new MockSepaSddMandat() {
			public Boolean isRecurrent() {
				return Boolean.FALSE;
			};

		};
		MockSepaSddEcheancier echeancier1 = (MockSepaSddEcheancier) SepaSddTestsHelper.mockEcheancier(BigDecimal.valueOf(100), 1, DATE_PREMIERE_ECHEANCE_AS_STRING);
		echeancier1.setMandat(mockMandat1);
		mockMandat1.echeanciers().add(echeancier1);

		SepaSddGenerateurEcheancierConstant generateurEcheancier = new SepaSddGenerateurEcheancierConstant(BigDecimal.valueOf(100), 1, DATE_PREMIERE_ECHEANCE, PERS_ID, MODIFICATEUR);

		List<ISepaSddEcheance> echeances = generateurEcheancier.genererEcheances(echeancier1);
		echeancier1.setEcheances(echeances);
		assertEquals(ISepaSddEcheance.TypeOperation.OOFF, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeances.get(0), false));

		SepaSddGenerateurEcheancierConstant generateurEcheancier2 = new SepaSddGenerateurEcheancierConstant(BigDecimal.valueOf(100), 2, DATE_PREMIERE_ECHEANCE, PERS_ID, MODIFICATEUR);
		List<ISepaSddEcheance> echeances2 = generateurEcheancier2.genererEcheances(echeancier1);
		echeancier1.setEcheances(echeances2);
		assertEquals(ISepaSddEcheance.TypeOperation.OOFF, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeances2.get(0), false));

		echeances2.get(0).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		assertEquals(ISepaSddEcheance.TypeOperation.OOFF, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeances2.get(1), false));

	}

	/**
	 * Verifie les types d'opérations de chaque echeance pour un echeancier avec 5 echeances.
	 */
	@Test
	public void testDetermineTypeOperation5Echeances() {
		ISepaSddMandat mockMandat = giveMeMandatAvec5Echeances();
		List<ISepaSddEcheance> echeancesAll = SepaSddMandatHelper.getSharedInstance().getEcheancesTrieesParDateEcheanceASC(mockMandat, false);

		assertEquals("Le type d'operation doit etre FRST si rien n'a été remis (preleve, confirme ou rejete)", ISepaSddEcheance.TypeOperation.FRST, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(0), false));
		assertEquals("Le type d'operation doit etre FRST si rien n'a été remis (preleve, confirme ou rejete)", ISepaSddEcheance.TypeOperation.FRST, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(1), false));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		echeancesAll.get(0).setTypeOperation(ISepaSddEcheance.TypeOperation.FRST);
		assertEquals("Le type d'operation doit etre RCUR si la premiere echeance est a l'etat preleve", ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(1), false));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.ANNULE);
		assertEquals("Le type d'operation doit etre FRST si la premiere echeance est annulee", ISepaSddEcheance.TypeOperation.FRST, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(1), false));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.REJETE);
		echeancesAll.get(0).setTypeOperation(ISepaSddEcheance.TypeOperation.FRST);
		assertEquals("Le type d'operation doit etre FRST si la premiere echeance est rejetee", ISepaSddEcheance.TypeOperation.FRST, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(1), false));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		echeancesAll.get(0).setTypeOperation(ISepaSddEcheance.TypeOperation.FRST);
		echeancesAll.get(1).setEtatEnum(ISepaSddEcheance.Etat.REJETE);
		echeancesAll.get(1).setTypeOperation(ISepaSddEcheance.TypeOperation.RCUR);
		assertEquals("Le type d'operation doit etre RCUR si la premiere echeance est prelevee et la deuxieme rejetee", ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(2), false));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		echeancesAll.get(0).setTypeOperation(ISepaSddEcheance.TypeOperation.FRST);
		echeancesAll.get(1).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		assertEquals("Le type d'operation doit etre RCUR si la premiere echeance est prelevee", ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(1), false));
		assertEquals("Le type d'operation doit etre RCUR si la premiere et la deuxieme echeance sont prelevees", ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(2), false));

		assertEquals(ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(2), false));
		assertEquals(ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(3), false));
		assertEquals(ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(4), false));
	}

	@Test
	public void testDetermineTypeOperationTmp5Echeances() {
		ISepaSddMandat mockMandat = giveMeMandatAvec5Echeances();
		List<ISepaSddEcheance> echeancesAll = SepaSddMandatHelper.getSharedInstance().getEcheancesTrieesParDateEcheanceASC(mockMandat, false);

		assertEquals("Le type d'operation doit etre FRST si rien n'a été remis (preleve, confirme ou rejete)", ISepaSddEcheance.TypeOperation.FRST, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(0), false));
		assertEquals("Le type d'operation doit etre FRST si rien n'a été remis (preleve, confirme ou rejete)", ISepaSddEcheance.TypeOperation.FRST, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(1), false));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		echeancesAll.get(0).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.FRST);
		assertEquals("Le type d'operation doit etre RCUR si la premiere echeance est a l'etat preleve", ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(1), false));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.ANNULE);
		assertEquals("Le type d'operation doit etre FRST si la premiere echeance est annulee", ISepaSddEcheance.TypeOperation.FRST, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(1), false));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.REJETE);
		echeancesAll.get(0).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.FRST);
		assertEquals("Le type d'operation doit etre FRST si la premiere echeance est rejetee", ISepaSddEcheance.TypeOperation.FRST, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(1), false));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		echeancesAll.get(0).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.FRST);
		echeancesAll.get(1).setEtatEnum(ISepaSddEcheance.Etat.REJETE);
		echeancesAll.get(1).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.RCUR);
		assertEquals("Le type d'operation doit etre RCUR si la premiere echeance est prelevee et la deuxieme rejetee", ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(2), false));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		echeancesAll.get(0).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.FRST);
		echeancesAll.get(1).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		assertEquals("Le type d'operation doit etre RCUR si la premiere echeance est prelevee", ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(1), false));
		assertEquals("Le type d'operation doit etre RCUR si la premiere et la deuxieme echeance sont prelevees", ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(2), false));

		assertEquals(ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(2), false));
		assertEquals(ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(3), false));
		assertEquals(ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(4), false));
	}

	@Test
	public void testDetermineTypeOperationEcheanceAvecChangementBicPrelevee() {
		ISepaSddMandat mockMandat = giveMeMandatAvec5Echeances();
		List<ISepaSddEcheance> echeancesAll = SepaSddMandatHelper.getSharedInstance().getEcheancesTrieesParDateEcheanceASC(mockMandat, false);

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		echeancesAll.get(0).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.FRST);

		echeancesAll.get(1).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		echeancesAll.get(1).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.RCUR);

		echeancesAll.get(2).echeancier().mandat().toDebiteurRib().toBanque().setBic("BNPAFRPPXXX");
		assertEquals("Le type d'operation doit etre FRST si le BIC du debiteur change", ISepaSddEcheance.TypeOperation.FRST, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(2), false));

	}

	@Test
	public void testDetermineTypeOperationEcheanceAvecChangementBicConfirme() {
		ISepaSddMandat mockMandat = giveMeMandatAvec5Echeances();
		List<ISepaSddEcheance> echeancesAll = SepaSddMandatHelper.getSharedInstance().getEcheancesTrieesParDateEcheanceASC(mockMandat, false);

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.CONFIRME);
		echeancesAll.get(0).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.FRST);

		echeancesAll.get(1).setEtatEnum(ISepaSddEcheance.Etat.CONFIRME);
		echeancesAll.get(1).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.RCUR);

		echeancesAll.get(2).echeancier().mandat().toDebiteurRib().toBanque().setBic("BNPAFRPPXXX");
		assertEquals("Le type d'operation doit etre FRST si le BIC du debiteur change", ISepaSddEcheance.TypeOperation.FRST, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(2), false));

	}

	@Test
	public void testDetermineTypeOperationEcheanceAvecChangementBicRejete() {
		ISepaSddMandat mockMandat = giveMeMandatAvec5Echeances();
		List<ISepaSddEcheance> echeancesAll = SepaSddMandatHelper.getSharedInstance().getEcheancesTrieesParDateEcheanceASC(mockMandat, false);

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.CONFIRME);
		echeancesAll.get(0).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.FRST);

		echeancesAll.get(1).setEtatEnum(ISepaSddEcheance.Etat.REJETE);
		echeancesAll.get(1).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.RCUR);

		echeancesAll.get(2).echeancier().mandat().toDebiteurRib().toBanque().setBic("BNPAFRPPXXX");
		assertEquals("Le type d'operation doit etre FRST si le BIC du debiteur change", ISepaSddEcheance.TypeOperation.FRST, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(2), false));

	}

	@Test
	public void testDetermineTypeOperationEcheanceSansChangementBic() {
		ISepaSddMandat mockMandat = giveMeMandatAvec5Echeances();
		List<ISepaSddEcheance> echeancesAll = SepaSddMandatHelper.getSharedInstance().getEcheancesTrieesParDateEcheanceASC(mockMandat, false);

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.CONFIRME);
		echeancesAll.get(0).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.FRST);

		echeancesAll.get(1).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		echeancesAll.get(1).setTypeOperationTmp(ISepaSddEcheance.TypeOperation.RCUR);

		echeancesAll.get(2).echeancier().mandat().toDebiteurRib().toBanque().setBic("BNPAFRPP");
		assertEquals("Le type d'operation doit etre RCUR si le BIC du debiteur ne change pas", ISepaSddEcheance.TypeOperation.RCUR, SepaSddEcheanceHelper.getSharedInstance().determineTypeOperation(echeancesAll.get(2), false));

	}

	private ISepaSddMandat giveMeMandatAvec5Echeances() {


		ISepaSddMandat mockMandat = new MockSepaSddMandat() {
			public Boolean isRecurrent() {
				return Boolean.TRUE;
			};


		};

		MockSepaSddEcheancier echeancier = (MockSepaSddEcheancier) SepaSddTestsHelper.mockEcheancier(BigDecimal.valueOf(100), 3, DATE_PREMIERE_ECHEANCE_AS_STRING);
		echeancier.setMandat(mockMandat);
		MockSepaSddEcheancier echeancier2 = (MockSepaSddEcheancier) SepaSddTestsHelper.mockEcheancier(BigDecimal.valueOf(100), 2, DATE_PREMIERE_ECHEANCE_AS_STRING);
		echeancier2.setMandat(mockMandat);
		mockMandat.echeanciers().add(echeancier);
		mockMandat.echeanciers().add(echeancier2);

		SepaSddGenerateurEcheancierConstant generateurEcheancier = new SepaSddGenerateurEcheancierConstant(BigDecimal.valueOf(100), 3, DATE_PREMIERE_ECHEANCE, PERS_ID, MODIFICATEUR);
		SepaSddGenerateurEcheancierConstant generateurEcheancier2 = new SepaSddGenerateurEcheancierConstant(BigDecimal.valueOf(100), 2, DATE_PREMIERE_ECHEANCE, PERS_ID, MODIFICATEUR);

		List<ISepaSddEcheance> echeances = generateurEcheancier.genererEcheances(echeancier);
		echeancier.setEcheances(echeances);

		List<ISepaSddEcheance> echeances2 = generateurEcheancier2.genererEcheances(echeancier2);
		echeancier2.setEcheances(echeances2);
		return mockMandat;

	}

	private ISepaSddEcheancier giveMeEcheancierAvec3EcheancesSurMandat5Echeances() {

		ISepaSddMandat mockMandat = new MockSepaSddMandat() {
			public Boolean isRecurrent() {
				return Boolean.TRUE;
			};

		};

		MockSepaSddEcheancier echeancier = (MockSepaSddEcheancier) SepaSddTestsHelper.mockEcheancier(BigDecimal.valueOf(100), 3, DATE_PREMIERE_ECHEANCE_AS_STRING);
		echeancier.setMandat(mockMandat);
		mockMandat.echeanciers().add(echeancier);

		MockSepaSddEcheancier echeancier2 = (MockSepaSddEcheancier) SepaSddTestsHelper.mockEcheancier(BigDecimal.valueOf(100), 2, DATE_PREMIERE_ECHEANCE_AS_STRING);
		echeancier2.setMandat(mockMandat);
		mockMandat.echeanciers().add(echeancier2);

		SepaSddGenerateurEcheancierConstant generateurEcheancier = new SepaSddGenerateurEcheancierConstant(BigDecimal.valueOf(100), 3, DATE_PREMIERE_ECHEANCE, PERS_ID, MODIFICATEUR);
		SepaSddGenerateurEcheancierConstant generateurEcheancier2 = new SepaSddGenerateurEcheancierConstant(BigDecimal.valueOf(100), 2, DATE_PREMIERE_ECHEANCE, PERS_ID, MODIFICATEUR);


		List<ISepaSddEcheance> echeances = generateurEcheancier.genererEcheances(echeancier);
		echeancier.setEcheances(echeances);

		List<ISepaSddEcheance> echeances2 = generateurEcheancier2.genererEcheances(echeancier2);
		echeancier2.setEcheances(echeances2);

		return echeancier;

	}

	@Test
	public void testEcheancePrecedentePreleveeDuMandat() {

		FwkCktlComptaMoteurCtrl.getSharedInstance().init(FwkCktlComptaMoteurCtrl.Contexte.SERVEUR);

		ISepaSddMandat mockMandat = giveMeMandatAvec5Echeances();

		List<ISepaSddEcheance> echeancesAll = SepaSddMandatHelper.getSharedInstance().getEcheancesTrieesParDateEcheanceASC(mockMandat, false);

		assertEquals(null, SepaSddEcheanceHelper.getSharedInstance().echeancePrecedenteRemiseDuMandat(echeancesAll.get(0)));
		assertEquals(null, SepaSddEcheanceHelper.getSharedInstance().echeancePrecedenteRemiseDuMandat(echeancesAll.get(3)));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		echeancesAll.get(0).setTypeOperation(ISepaSddEcheance.TypeOperation.FRST);
		echeancesAll.get(1).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		echeancesAll.get(1).setTypeOperation(ISepaSddEcheance.TypeOperation.RCUR);
		echeancesAll.get(2).setEtatEnum(ISepaSddEcheance.Etat.ANNULE);

		assertEquals(echeancesAll.get(1), SepaSddEcheanceHelper.getSharedInstance().echeancePrecedenteRemiseDuMandat(echeancesAll.get(3)));
		assertEquals(echeancesAll.get(1), SepaSddEcheanceHelper.getSharedInstance().echeancePrecedenteRemiseDuMandat(echeancesAll.get(4)));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.CONFIRME);
		echeancesAll.get(0).setTypeOperation(ISepaSddEcheance.TypeOperation.FRST);
		echeancesAll.get(1).setEtatEnum(ISepaSddEcheance.Etat.REJETE);
		echeancesAll.get(1).setTypeOperation(ISepaSddEcheance.TypeOperation.RCUR);
		echeancesAll.get(2).setEtatEnum(ISepaSddEcheance.Etat.ANNULE);

		assertEquals(echeancesAll.get(1), SepaSddEcheanceHelper.getSharedInstance().echeancePrecedenteRemiseDuMandat(echeancesAll.get(3)));
		assertEquals(echeancesAll.get(1), SepaSddEcheanceHelper.getSharedInstance().echeancePrecedenteRemiseDuMandat(echeancesAll.get(4)));

	}

	@Test
	public void testGetNumeroEcheanceAPreleverPourEcheancier() {
		ISepaSddEcheancier echeancier = giveMeEcheancierAvec3EcheancesSurMandat5Echeances();

		List<ISepaSddEcheance> echeancesAll = SepaSddEcheancierHelper.getSharedInstance().getEcheancesTrieesParDateEcheanceASC(echeancier, false);

		assertEquals("Le numero doit etre 1 si rien n'a été remis (preleve, confirme ou rejete)", Integer.valueOf(1), SepaSddEcheanceHelper.getSharedInstance().getNumeroEcheanceAPreleverPourEcheancier(echeancesAll.get(0)));
		assertEquals("Le numero doit etre 1 si rien n'a été remis (preleve, confirme ou rejete)", Integer.valueOf(1), SepaSddEcheanceHelper.getSharedInstance().getNumeroEcheanceAPreleverPourEcheancier(echeancesAll.get(2)));


		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.PRELEVE);
		assertEquals("Le numero doit etre 2 si la premiere echeance est prélevée", Integer.valueOf(2), SepaSddEcheanceHelper.getSharedInstance().getNumeroEcheanceAPreleverPourEcheancier(echeancesAll.get(1)));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.ANNULE);
		assertEquals("Si la première échéance est annulée, la deuxième doit avoir le numéro 1", Integer.valueOf(1), SepaSddEcheanceHelper.getSharedInstance().getNumeroEcheanceAPreleverPourEcheancier(echeancesAll.get(1)));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.ATTENTE);
		assertEquals("Si la première échéance est à l'état ATTENTE, la deuxième doit avoir le numero 1", Integer.valueOf(1), SepaSddEcheanceHelper.getSharedInstance().getNumeroEcheanceAPreleverPourEcheancier(echeancesAll.get(1)));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.REJETE);
		assertEquals("Si la première échéance est à l'état REJETE, la deuxième doit avoir le numero 2", Integer.valueOf(2), SepaSddEcheanceHelper.getSharedInstance().getNumeroEcheanceAPreleverPourEcheancier(echeancesAll.get(1)));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.CONFIRME);
		assertEquals("Si la première échéance est à l'état CONFIRME, la deuxième doit avoir le numero 2", Integer.valueOf(2), SepaSddEcheanceHelper.getSharedInstance().getNumeroEcheanceAPreleverPourEcheancier(echeancesAll.get(1)));

		echeancesAll.get(0).setEtatEnum(ISepaSddEcheance.Etat.CONFIRME);
		echeancesAll.get(1).setEtatEnum(ISepaSddEcheance.Etat.REJETE);
		assertEquals("Si la première échéance est à l'état CONFIRME, la deuxième à REJETE, la troisieme doit avoir le numero 2", Integer.valueOf(3), SepaSddEcheanceHelper.getSharedInstance().getNumeroEcheanceAPreleverPourEcheancier(echeancesAll.get(2)));

	}


}
