package org.cocktail.fwkcktlcompta.common.sepasdd.rules;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddMandat;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.SepaSddTestsHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.junit.Test;

import com.webobjects.foundation.NSValidation;

public class SepaSddEcheancierRuleTest {
	private static final Integer PERS_ID = Integer.valueOf(1);
	private static final BigDecimal MONTANT_A_PAYER = BigDecimal.valueOf(100);
	private static final IGrhumPersonne MODIFICATEUR = SepaSddTestsHelper.getModificateur();

	@Test
	public void testIsOrigineUtiliseePourAutresEcheanciers() {
		ISepaSddOrigine origine1 = new MockSepaSddOrigine();
		ISepaSddOrigine origine2 = new MockSepaSddOrigine();
		
		
		ISepaSddMandat mockMandat1 = new MockSepaSddMandat();

		ISepaSddEcheancier echeancier1 = new MockSepaSddEcheancier();
		echeancier1.setMandat(mockMandat1);
		mockMandat1.echeanciers().add(echeancier1);

		ISepaSddMandat mockMandat2 = new MockSepaSddMandat();
		ISepaSddEcheancier echeancier2 = new MockSepaSddEcheancier();
		echeancier2.setMandat(mockMandat2);
		mockMandat2.echeanciers().add(echeancier2);


		ISepaSddMandat mockMandat3 = new MockSepaSddMandat();
		ISepaSddEcheancier echeancier3 = new MockSepaSddEcheancier();
		echeancier3.setMandat(mockMandat3);
		mockMandat3.echeanciers().add(echeancier3);

		origine1.addToToSepaSddEcheanciersRelationship(echeancier1);
		origine2.addToToSepaSddEcheanciersRelationship(echeancier2);
		origine2.addToToSepaSddEcheanciersRelationship(echeancier3);

		StringBuilder sb = new StringBuilder();
		assertFalse("L'échancier 1 est le seul avec cette origine", SepaSddEcheancierRule.getSharedInstance().isOrigineUtiliseePourAutresEcheanciers(echeancier1, sb));
		sb = new StringBuilder();
		assertTrue("L'échancier 2 a la meme origine que l'echeancier 3", SepaSddEcheancierRule.getSharedInstance().isOrigineUtiliseePourAutresEcheanciers(echeancier2, sb));
		sb = new StringBuilder();
		assertTrue("L'échancier 2 a la meme origine que l'echeancier 3", SepaSddEcheancierRule.getSharedInstance().isOrigineUtiliseePourAutresEcheanciers(echeancier3, sb));

		Exception lastException = null;
		try {
			SepaSddEcheancierRule.getSharedInstance().checkOrigineUtiliseePourAutresEcheanciers(echeancier2);
		}
		catch (Exception e) {
			lastException = e;
		}
		assertEquals("Il devrait y avoir une exception", NSValidation.ValidationException.class, lastException.getClass());
		
		
		
		
	}
}
