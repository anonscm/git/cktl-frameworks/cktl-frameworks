package org.cocktail.fwkcktlcompta.common.sepasdd.rules;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.SepaSddTestsHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.IGenerateurEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.SepaSddGenerateurEcheancierPremiereEcheanceFixe;
import org.joda.time.LocalDate;
import org.junit.Test;

import com.webobjects.foundation.NSArray;

public class SepaSddEcheanceRuleTest {
	private static final Integer PERS_ID = Integer.valueOf(1);
	private static final BigDecimal MONTANT_A_PAYER = BigDecimal.valueOf(100);
	private static final IGrhumPersonne MODIFICATEUR = SepaSddTestsHelper.getModificateur();

	@Test
	public void testCheckPlusieursEcheancesPourMemeMandatFalse() {
		ISepaSddMandat mockMandat1 = new MockSepaSddMandat();

		ISepaSddEcheancier echeancier1 = new MockSepaSddEcheancier();
		echeancier1.setMandat(mockMandat1);
		mockMandat1.echeanciers().add(echeancier1);
		IGenerateurEcheancier generateur1 = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(BigDecimal.valueOf(100), Integer.valueOf(7), LocalDate.now(), BigDecimal.valueOf(30), PERS_ID, MODIFICATEUR);
		List<ISepaSddEcheance> echeances1 = generateur1.genererEcheances(echeancier1);

		ISepaSddMandat mockMandat2 = new MockSepaSddMandat();
		ISepaSddEcheancier echeancier2 = new MockSepaSddEcheancier();
		echeancier2.setMandat(mockMandat2);
		mockMandat2.echeanciers().add(echeancier2);
		IGenerateurEcheancier generateur2 = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(BigDecimal.valueOf(200), Integer.valueOf(7), LocalDate.now(), BigDecimal.valueOf(30), PERS_ID, MODIFICATEUR);
		List<ISepaSddEcheance> echeances2 = generateur2.genererEcheances(echeancier2);
		
		ISepaSddMandat mockMandat3 = new MockSepaSddMandat();
		ISepaSddEcheancier echeancier3 = new MockSepaSddEcheancier();
		echeancier3.setMandat(mockMandat3);
		mockMandat3.echeanciers().add(echeancier3);
		IGenerateurEcheancier generateur3 = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(BigDecimal.valueOf(300), Integer.valueOf(7), LocalDate.now(), BigDecimal.valueOf(30), PERS_ID, MODIFICATEUR);
		List<ISepaSddEcheance> echeances3 = generateur3.genererEcheances(echeancier3);

		List<ISepaSddEcheance> echeancesATester = new ArrayList<ISepaSddEcheance>();
		echeancesATester.add(echeances1.get(0));
		echeancesATester.add(echeances2.get(0));
		echeancesATester.add(echeances3.get(0));

		StringBuilder sb = new StringBuilder();
		Boolean res = SepaSddEcheanceRule.getSharedInstance().isPlusieursEcheancesPourMemeMandat(new NSArray(echeancesATester.toArray()), sb);
		assertFalse("Il n'y a pas plusieurs echeances pour le meme mandat", res);

		Exception lastException = null;
		try {

			SepaSddEcheanceRule.getSharedInstance().checkPlusieursEcheancesPourMemeMandat(new NSArray(echeancesATester.toArray()));
		} catch (Exception e) {
			e.printStackTrace();
			lastException = e;
		}
		assertEquals("Il ne devrait pas y avoir d'exception", null, lastException);

	}

	@Test
	public void testCheckPlusieursEcheancesPourMemeMandatTrue() {
		ISepaSddMandat mockMandat1 = new MockSepaSddMandat();

		ISepaSddEcheancier echeancier1 = new MockSepaSddEcheancier();
		echeancier1.setMandat(mockMandat1);
		mockMandat1.echeanciers().add(echeancier1);
		IGenerateurEcheancier generateur1 = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(BigDecimal.valueOf(100), Integer.valueOf(7), LocalDate.now(), BigDecimal.valueOf(30), PERS_ID, MODIFICATEUR);
		List<ISepaSddEcheance> echeances1 = generateur1.genererEcheances(echeancier1);

		ISepaSddMandat mockMandat2 = new MockSepaSddMandat();
		ISepaSddEcheancier echeancier2 = new MockSepaSddEcheancier();
		echeancier2.setMandat(mockMandat1);
		mockMandat2.echeanciers().add(echeancier2);
		IGenerateurEcheancier generateur2 = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(BigDecimal.valueOf(200), Integer.valueOf(7), LocalDate.now(), BigDecimal.valueOf(30), PERS_ID, MODIFICATEUR);
		List<ISepaSddEcheance> echeances2 = generateur2.genererEcheances(echeancier2);

		ISepaSddMandat mockMandat3 = new MockSepaSddMandat();
		ISepaSddEcheancier echeancier3 = new MockSepaSddEcheancier();
		echeancier3.setMandat(mockMandat3);
		mockMandat3.echeanciers().add(echeancier3);
		IGenerateurEcheancier generateur3 = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(BigDecimal.valueOf(300), Integer.valueOf(7), LocalDate.now(), BigDecimal.valueOf(30), PERS_ID, MODIFICATEUR);
		List<ISepaSddEcheance> echeances3 = generateur3.genererEcheances(echeancier3);

		List<ISepaSddEcheance> echeancesATester = new ArrayList<ISepaSddEcheance>();
		echeancesATester.add(echeances1.get(0));
		echeancesATester.add(echeances3.get(0));
		echeancesATester.add(echeances2.get(0));
		
		StringBuilder sb = new StringBuilder();
		Boolean res = SepaSddEcheanceRule.getSharedInstance().isPlusieursEcheancesPourMemeMandat(new NSArray(echeancesATester.toArray()), sb);
		assertTrue("Il y a plusieurs echeances pour le meme mandat", res);

		Exception lastException = null;
		try {
			SepaSddEcheanceRule.getSharedInstance().checkPlusieursEcheancesPourMemeMandat(new NSArray(echeancesATester.toArray()));
		} catch (Exception e) {
			e.printStackTrace();
			lastException = e;
		}
		assertNotNull("Il devrait y avoir une exception", lastException);
		
	}
}
