package org.cocktail.fwkcktlcompta.common.sepasdd.generateurs;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.IGenerateurDatesEcheances;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.SepaSddGenerateurDatesEcheancesMensuels;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

public class SepaSddGenerateurDatesEcheancesMensuelsTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPeutGenererDatesEcheances() {
		IGenerateurDatesEcheances generateurDates = new SepaSddGenerateurDatesEcheancesMensuels(null, null);
		assertFalse(generateurDates.peutGenererDatesEcheances());

		generateurDates = new SepaSddGenerateurDatesEcheancesMensuels(LocalDate.now(), null);
		assertFalse(generateurDates.peutGenererDatesEcheances());

		generateurDates = new SepaSddGenerateurDatesEcheancesMensuels(LocalDate.now(), Integer.valueOf(0));
		assertFalse(generateurDates.peutGenererDatesEcheances());

		generateurDates = new SepaSddGenerateurDatesEcheancesMensuels(LocalDate.now(), Integer.valueOf(1));
		assertTrue(generateurDates.peutGenererDatesEcheances());
	}

	@Test
	public void testGenererDatesEcheances() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGenererDatesEcheancesLocalDateInt() {
		final LocalDate DATE_PREMIERE_ECHEANCE = LocalDate.parse("2013-01-20");
		final LocalDate DATE_DEUXIEME_ECHEANCE = LocalDate.parse("2013-02-20");
		final LocalDate DATE_TROISIEME_ECHEANCE = LocalDate.parse("2013-03-20");
		final int NB_ECHEANCES = 3;
		SepaSddGenerateurDatesEcheancesMensuels generateurDates = new SepaSddGenerateurDatesEcheancesMensuels(LocalDate.now(), Integer.valueOf(1));

		List<LocalDate> datesEcheances = new ArrayList<LocalDate>();
		generateurDates.genererDatesEcheances(datesEcheances, DATE_PREMIERE_ECHEANCE, NB_ECHEANCES);
		assertEquals(NB_ECHEANCES, datesEcheances.size());
		assertEquals(DATE_PREMIERE_ECHEANCE, datesEcheances.get(0));
		assertEquals(DATE_DEUXIEME_ECHEANCE, datesEcheances.get(1));
		assertEquals(DATE_TROISIEME_ECHEANCE, datesEcheances.get(2));
	}

	@Test
	public void testGenererProchaineDate() {
		SepaSddGenerateurDatesEcheancesMensuels generateurDates = new SepaSddGenerateurDatesEcheancesMensuels(LocalDate.now(), Integer.valueOf(1));
		LocalDate prochaineDate = generateurDates.genererProchaineDate(LocalDate.parse("2013-01-20"));
		assertEquals(LocalDate.parse("2013-02-20"), prochaineDate);

		prochaineDate = generateurDates.genererProchaineDate(LocalDate.parse("2013-12-20"));
		assertEquals(LocalDate.parse("2014-01-20"), prochaineDate);

		prochaineDate = generateurDates.genererProchaineDate(LocalDate.parse("2013-01-30"));
		assertEquals(LocalDate.parse("2013-02-28"), prochaineDate);
	}

}
