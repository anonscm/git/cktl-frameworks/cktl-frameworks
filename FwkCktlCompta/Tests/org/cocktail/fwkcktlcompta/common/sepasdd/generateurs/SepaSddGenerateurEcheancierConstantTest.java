package org.cocktail.fwkcktlcompta.common.sepasdd.generateurs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.SepaSddTestsHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

public class SepaSddGenerateurEcheancierConstantTest {

	private static final DateConversionUtil DATE_CONVERSION_UTIL = new DateConversionUtil();
	private static final String DATE_PREMIERE_ECHEANCE_AS_STRING = "2013-01-20";
	private static final LocalDate DATE_PREMIERE_ECHEANCE = DATE_CONVERSION_UTIL.parseDateSilent("2013-01-20");
	private static final  Integer PERS_ID = Integer.valueOf(1);
	private static final IGrhumPersonne MODIFICATEUR = SepaSddTestsHelper.getModificateur();

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPeutGenererEcheances() {
		// echeancier indefini
		SepaSddGenerateurEcheancierConstant generateurEcheancier = new SepaSddGenerateurEcheancierConstant(null, null, null, PERS_ID, MODIFICATEUR);
		assertFalse(generateurEcheancier.peutGenererEcheances(null));

		// pas de persId
		generateurEcheancier = new SepaSddGenerateurEcheancierConstant(BigDecimal.ONE, null, null, null, MODIFICATEUR);
		assertFalse(generateurEcheancier.peutGenererEcheances(mockEcheancier(BigDecimal.ONE, null, null)));

		// montant a payer indefini
		generateurEcheancier = new SepaSddGenerateurEcheancierConstant(null, null, null, PERS_ID, MODIFICATEUR);
		assertFalse(generateurEcheancier.peutGenererEcheances(new MockSepaSddEcheancier()));

		//  montant a payer defini et date premiere echeance indefinie
		generateurEcheancier = new SepaSddGenerateurEcheancierConstant(BigDecimal.ONE, null, null, PERS_ID, MODIFICATEUR);
		assertFalse(generateurEcheancier.peutGenererEcheances(mockEcheancier(BigDecimal.ONE, null, null)));

		// montant a payer defini et date premiere echeance definie
		generateurEcheancier = new SepaSddGenerateurEcheancierConstant(BigDecimal.ONE, null, DATE_PREMIERE_ECHEANCE, PERS_ID, MODIFICATEUR);
		assertTrue(generateurEcheancier.peutGenererEcheances(mockEcheancier(BigDecimal.ONE, null, DATE_PREMIERE_ECHEANCE_AS_STRING)));
	}

	@Test
	public void testGenererEcheancesSansEcheancier() throws Exception {
		SepaSddGenerateurEcheancierConstant generateurEcheancier = new SepaSddGenerateurEcheancierConstant(null, null, null, null, MODIFICATEUR);
		assertTrue(generateurEcheancier.genererEcheances(null).isEmpty());
	}

	@Test
	public void testGenererEcheancesQuandNbEcheancesIndefini() throws Exception {
		final BigDecimal MONTANT_ECHEANCE_UNIQUE = BigDecimal.TEN.setScale(IGenerateurEcheancier.PRECISION_MONETAIRE_PAR_DEFAUT);
		SepaSddGenerateurEcheancierConstant generateurEcheancier = new SepaSddGenerateurEcheancierConstant(MONTANT_ECHEANCE_UNIQUE, null, DATE_PREMIERE_ECHEANCE, PERS_ID, MODIFICATEUR);
		ISepaSddEcheancier mockEcheancier = mockEcheancier(MONTANT_ECHEANCE_UNIQUE, null, DATE_PREMIERE_ECHEANCE_AS_STRING);
		mockEcheancier.setMandat(new MockSepaSddMandat());
		mockEcheancier.mandat().echeanciers().add(mockEcheancier);
		
		List<ISepaSddEcheance> echeances = generateurEcheancier.genererEcheances(mockEcheancier);
		assertEquals(1, echeances.size());
		assertEquals(MONTANT_ECHEANCE_UNIQUE, echeances.get(0).montantAPayer());
	}

	@Test
	public void testGenererEcheancesSansReordonnancement() throws Exception {
		final BigDecimal MONTANT_ECHEANCIER = BigDecimal.valueOf(1000L);
		final BigDecimal MONTANT_CONSTANT = BigDecimal.valueOf(90.91d).setScale(IGenerateurEcheancier.PRECISION_MONETAIRE_PAR_DEFAUT);
		final BigDecimal MONTANT_RATTRAPAGE = BigDecimal.valueOf(90.9d).setScale(IGenerateurEcheancier.PRECISION_MONETAIRE_PAR_DEFAUT);
		final Integer NB_ECHEANCES = 11;

		ISepaSddEcheancier mockEcheancier = mockEcheancier(MONTANT_ECHEANCIER, NB_ECHEANCES, DATE_PREMIERE_ECHEANCE_AS_STRING);
		mockEcheancier.setMandat(new MockSepaSddMandat());
		mockEcheancier.mandat().echeanciers().add(mockEcheancier);

		SepaSddGenerateurEcheancierConstant generateurEcheancier = new SepaSddGenerateurEcheancierConstant(MONTANT_ECHEANCIER, NB_ECHEANCES, DATE_PREMIERE_ECHEANCE, PERS_ID, MODIFICATEUR);
		List<ISepaSddEcheance> echeances = generateurEcheancier.genererEcheances(mockEcheancier);
		assertEquals(NB_ECHEANCES, Integer.valueOf(echeances.size()));
		assertEquals(MONTANT_CONSTANT, echeances.get(0).montantAPayer());
		assertEquals(MONTANT_CONSTANT, echeances.get(1).montantAPayer());
		assertEquals(MONTANT_CONSTANT, echeances.get(2).montantAPayer());
		assertEquals(MONTANT_CONSTANT, echeances.get(3).montantAPayer());
		assertEquals(MONTANT_CONSTANT, echeances.get(4).montantAPayer());
		assertEquals(MONTANT_CONSTANT, echeances.get(5).montantAPayer());
		assertEquals(MONTANT_CONSTANT, echeances.get(6).montantAPayer());
		assertEquals(MONTANT_CONSTANT, echeances.get(7).montantAPayer());
		assertEquals(MONTANT_CONSTANT, echeances.get(8).montantAPayer());
		assertEquals(MONTANT_CONSTANT, echeances.get(9).montantAPayer());
		assertEquals(MONTANT_RATTRAPAGE, echeances.get(10).montantAPayer());

	}

	@Test
	public void testGenererEcheancesAvecEcheanceUnique() throws Exception {
		final BigDecimal MONTANT_ECHEANCIER = BigDecimal.valueOf(1000L).setScale(IGenerateurEcheancier.PRECISION_MONETAIRE_PAR_DEFAUT);
		final Integer NB_ECHEANCES = 1;

		ISepaSddEcheancier mockEcheancier = mockEcheancier(MONTANT_ECHEANCIER, NB_ECHEANCES, DATE_PREMIERE_ECHEANCE_AS_STRING);

		mockEcheancier.setMandat(new MockSepaSddMandat());
		mockEcheancier.mandat().echeanciers().add(mockEcheancier);

		SepaSddGenerateurEcheancierConstant generateurEcheancier = new SepaSddGenerateurEcheancierConstant(MONTANT_ECHEANCIER, NB_ECHEANCES, DATE_PREMIERE_ECHEANCE, PERS_ID, MODIFICATEUR);
		List<ISepaSddEcheance> echeances = generateurEcheancier.genererEcheances(mockEcheancier);
		assertEquals(NB_ECHEANCES, Integer.valueOf(echeances.size()));
		assertEquals(MONTANT_ECHEANCIER, echeances.get(0).montantAPayer());
		assertEquals(DATE_PREMIERE_ECHEANCE_AS_STRING, echeances.get(0).dPrevue());
	}

	@Test
	public void testGenererEcheancesAvecReordonnancement() throws Exception {
		final BigDecimal MONTANT_ECHEANCIER = BigDecimal.valueOf(100L);
		final BigDecimal MONTANT_CONSTANT = BigDecimal.valueOf(33.33d).setScale(IGenerateurEcheancier.PRECISION_MONETAIRE_PAR_DEFAUT);
		final BigDecimal MONTANT_RATTRAPAGE = BigDecimal.valueOf(33.34d).setScale(IGenerateurEcheancier.PRECISION_MONETAIRE_PAR_DEFAUT);
		final Integer NB_ECHEANCES = 3;

		ISepaSddEcheancier mockEcheancier = mockEcheancier(MONTANT_ECHEANCIER, NB_ECHEANCES, DATE_PREMIERE_ECHEANCE_AS_STRING);
		mockEcheancier.setMandat(new MockSepaSddMandat());
		mockEcheancier.mandat().echeanciers().add(mockEcheancier);

		SepaSddGenerateurEcheancierConstant generateurEcheancier = new SepaSddGenerateurEcheancierConstant(MONTANT_ECHEANCIER, NB_ECHEANCES, DATE_PREMIERE_ECHEANCE, PERS_ID, MODIFICATEUR);
		List<ISepaSddEcheance> echeances = generateurEcheancier.genererEcheances(mockEcheancier);
		assertEquals(NB_ECHEANCES, Integer.valueOf(echeances.size()));
		assertEquals(MONTANT_RATTRAPAGE, echeances.get(0).montantAPayer());
		assertEquals(MONTANT_CONSTANT, echeances.get(1).montantAPayer());
		assertEquals(MONTANT_CONSTANT, echeances.get(2).montantAPayer());

	}



	protected ISepaSddEcheancier mockEcheancier(final BigDecimal montant, final Integer nbEcheances, final String datePremiereEcheance) {
		return SepaSddTestsHelper.mockEcheancier(montant, nbEcheances, datePremiereEcheance);
	}
}
