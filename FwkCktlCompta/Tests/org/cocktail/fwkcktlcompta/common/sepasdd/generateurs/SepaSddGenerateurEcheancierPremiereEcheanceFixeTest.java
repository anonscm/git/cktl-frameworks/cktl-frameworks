package org.cocktail.fwkcktlcompta.common.sepasdd.generateurs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.SepaSddTestsHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;


public class SepaSddGenerateurEcheancierPremiereEcheanceFixeTest {

	private static final Integer PERS_ID = Integer.valueOf(1);
	private static final BigDecimal MONTANT_A_PAYER = BigDecimal.valueOf(100);
	private static final IGrhumPersonne MODIFICATEUR = SepaSddTestsHelper.getModificateur();

	private ISepaSddMandat mockMandat = new MockSepaSddMandat() {
		public Boolean isRecurrent() {
			return Boolean.TRUE;
		};

	};

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPeutGenererEcheances() {
		IGenerateurEcheancier generateur = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(null, null, null, null, null, MODIFICATEUR);
		assertFalse(generateur.peutGenererEcheances(null));

		generateur = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(null, null, null, null, null, MODIFICATEUR);
		assertFalse(generateur.peutGenererEcheances(new MockSepaSddEcheancier()));

		generateur = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(MONTANT_A_PAYER, null, null, null, PERS_ID, MODIFICATEUR);
		assertFalse(generateur.peutGenererEcheances(new MockSepaSddEcheancier()));

		generateur = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(MONTANT_A_PAYER, null, null, BigDecimal.TEN, PERS_ID, MODIFICATEUR);
		assertFalse(generateur.peutGenererEcheances(new MockSepaSddEcheancier()));

		generateur = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(MONTANT_A_PAYER, Integer.valueOf(1), null, BigDecimal.TEN, PERS_ID, MODIFICATEUR);
		assertFalse(generateur.peutGenererEcheances(new MockSepaSddEcheancier()));

		generateur = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(MONTANT_A_PAYER, Integer.valueOf(1), LocalDate.now(), BigDecimal.valueOf(150d), PERS_ID, MODIFICATEUR);
		assertFalse(generateur.peutGenererEcheances(new MockSepaSddEcheancier()));

		generateur = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(MONTANT_A_PAYER, Integer.valueOf(1), LocalDate.now(), BigDecimal.TEN, PERS_ID, MODIFICATEUR);
		assertTrue(generateur.peutGenererEcheances(new MockSepaSddEcheancier()));
	}

	@Test
	public void testGenererEcheancesSansEcheancier() throws Exception {
		IGenerateurEcheancier generateur = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(null, null, null, null, null, MODIFICATEUR);
		assertTrue(generateur.genererEcheances(null).isEmpty());
	}

	@Test
	public void testGenererEcheancesUnique() throws Exception {
		final BigDecimal MONTANT_ECHEANCE_UNIQUE = BigDecimal.TEN.setScale(IGenerateurEcheancier.PRECISION_MONETAIRE_PAR_DEFAUT);
		IGenerateurEcheancier generateur = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(MONTANT_A_PAYER, Integer.valueOf(1), LocalDate.now(), BigDecimal.TEN, PERS_ID, MODIFICATEUR);
		ISepaSddEcheancier echeancier = new MockSepaSddEcheancier();
		echeancier.setMandat(mockMandat);
		mockMandat.echeanciers().add(echeancier);

		List<ISepaSddEcheance> echeances = generateur.genererEcheances(echeancier);
		assertEquals(1, echeances.size());
		assertEquals(MONTANT_ECHEANCE_UNIQUE, echeances.get(0).montantAPayer());
	}

	@Test
	public void testGenererEcheancesMultiples() throws Exception {
		final BigDecimal MONTANT_1ERE_ECHEANCE = BigDecimal.valueOf(17d).setScale(IGenerateurEcheancier.PRECISION_MONETAIRE_PAR_DEFAUT);
		final BigDecimal MONTANT_ECHEANCES_SUIVANTES = BigDecimal.valueOf(13.83d).setScale(IGenerateurEcheancier.PRECISION_MONETAIRE_PAR_DEFAUT);
		final BigDecimal MONTANT_DERNIERE_ECHEANCE = BigDecimal.valueOf(13.85d).setScale(IGenerateurEcheancier.PRECISION_MONETAIRE_PAR_DEFAUT);

		ISepaSddEcheancier echeancier = new MockSepaSddEcheancier();
		echeancier.setMandat(mockMandat);
		mockMandat.echeanciers().add(echeancier);

		IGenerateurEcheancier generateur = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(MONTANT_A_PAYER, Integer.valueOf(7), LocalDate.now(), MONTANT_1ERE_ECHEANCE, PERS_ID, MODIFICATEUR);

		List<ISepaSddEcheance> echeances = generateur.genererEcheances(echeancier);
		assertEquals(7, echeances.size());
		assertEquals(MONTANT_1ERE_ECHEANCE, echeances.get(0).montantAPayer());
		assertEquals(MONTANT_ECHEANCES_SUIVANTES, echeances.get(1).montantAPayer());
		assertEquals(MONTANT_ECHEANCES_SUIVANTES, echeances.get(2).montantAPayer());
		assertEquals(MONTANT_ECHEANCES_SUIVANTES, echeances.get(3).montantAPayer());
		assertEquals(MONTANT_ECHEANCES_SUIVANTES, echeances.get(4).montantAPayer());
		assertEquals(MONTANT_ECHEANCES_SUIVANTES, echeances.get(5).montantAPayer());
		assertEquals(MONTANT_DERNIERE_ECHEANCE, echeances.get(6).montantAPayer());
	}
}
