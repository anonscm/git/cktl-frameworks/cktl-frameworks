package org.cocktail.fwkcktlcompta.common.metier;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class MockSepaSddOrigine implements ISepaSddOrigine {

	private List<ISepaSddEcheancier> echeanciers = new ArrayList<ISepaSddEcheancier>();

	public void setToSepaSddOrigineTypeRelationship(ISepaSddOrigineType origineType) {
		// TODO Auto-generated method stub

	}

	public void setOrigineId(Integer origineIdentifiant) {
		// TODO Auto-generated method stub

	}

	public ISepaSddOrigineType toSepaSddOrigineType() {
		// TODO Auto-generated method stub
		return null;
	}

	public ISepaSddOrigineEntity toEntity() {
		// TODO Auto-generated method stub
		return null;
	}

	public NSArray toSepaSddEcheanciers(EOQualifier qual) {
		NSArray results = toSepaSddEcheanciers();
		if (qual != null) {
			results = (NSArray) EOQualifier.filteredArrayWithQualifier(results, qual);
		}
		return results;
	}

	public NSArray toSepaSddEcheanciers() {
		return new NSArray(echeanciers.toArray());
	}

	public String getTypeAndLibelleCompletOfOrigine() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getLibellePourDebiteur() {
		// TODO Auto-generated method stub
		return null;
	}

	public void addToToSepaSddEcheanciersRelationship(ISepaSddEcheancier echeancier) {
		echeanciers.add(echeancier);
		echeancier.setToSepaSddOrigineRelationship(this);

	}

}
