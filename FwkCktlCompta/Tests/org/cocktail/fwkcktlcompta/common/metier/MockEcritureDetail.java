package org.cocktail.fwkcktlcompta.common.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.common.entities.IEcriture;
import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.entities.IGestion;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;
import org.cocktail.fwkcktlcompta.common.entities.IPlanComptableExer;

public class MockEcritureDetail implements IEcritureDetail {
	private IEcriture ecriture;

	public MockEcritureDetail() {
		// TODO Auto-generated constructor stub
	}

	public MockEcritureDetail(IEcriture ecriture) {
		this.ecriture = ecriture;
	}

	public BigDecimal ecdMontant() {
		// TODO Auto-generated method stub
		return null;
	}

	public IJefyAdminExercice toExercice() {
		// TODO Auto-generated method stub
		return null;
	}

	public BigDecimal ecdResteEmarger() {
		// TODO Auto-generated method stub
		return null;
	}

	public IPlanComptableExer toPlanComptableExer() {
		// TODO Auto-generated method stub
		return null;
	}

	public IEcriture toEcriture() {
		return ecriture;
	}

	public Integer ecdIndex() {
		// TODO Auto-generated method stub
		return null;
	}

	public String ecdSens() {
		// TODO Auto-generated method stub
		return null;
	}

	public String ecdLibelle() {
		// TODO Auto-generated method stub
		return null;
	}

	public IGestion toGestion() {
		// TODO Auto-generated method stub
		return null;
	}

	public BigDecimal ecdCredit() {
		// TODO Auto-generated method stub
		return null;
	}

	public BigDecimal ecdDebit() {
		// TODO Auto-generated method stub
		return null;
	}

}
