package org.cocktail.fwkcktlcompta.common.metier;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumAdresse;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumBanque;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumFournis;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumRib;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddParam;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheanceHelper;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class MockSepaSddMandat implements ISepaSddMandat {

	String rum;
	String nomPrenomDebiteur;
	String tiersDebiteurNomPrenom;
	String bicIbanDebiteur;
	String dMandatCreation;
	String dMandatSignature;
	String cTypePrelevement;
	List<ISepaSddEcheancier> echeanciers = new ArrayList<ISepaSddEcheancier>();
	EOEditingContext editingContext;
	String numero;
	ISepaSddParam toSepaSddParam;
	IGrhumPersonne toDebiteurPersonne;
	IGrhumPersonne toTiersDebiteurPersonne;
	Boolean isRecurrent = Boolean.FALSE;
	String libelle;
	IGrhumPersonne clientPersonne;
	IGrhumPersonne creancierPersonne;
	String refAppliCreation;

	final IGrhumBanque banque = new IGrhumBanque() {
		String bic = "BNPAFRPP";

		public void setBic(String value) {
			this.bic = value;

		}

		public String domiciliation() {
			return null;
		}

		public String bic() {
			return bic;
		}
	};

	final IGrhumRib debiteurRib = new IGrhumRib() {

		public IGrhumBanque toBanque() {
			return banque;
		}

		public String ribValide() {
			return null;
		}

		public String ribTitco() {
			return null;
		}

		public String iban() {
			return null;
		}

		public String bicEtIban() {
			return null;
		}

		public boolean isRibIntlComplet() {
			return false;
		}

		public IGrhumFournis toFournis() {
			return null;
		}
	};

	public String rum() {
		return rum;
	}


	public void setRum(String rum) {
		this.rum = rum;
	}

	public String nomPrenomDebiteur() {
		return nomPrenomDebiteur;
	}

	public void setNomPrenomDebiteur(String nomPrenomDebiteur) {
		this.nomPrenomDebiteur = nomPrenomDebiteur;
	}

	public String tiersDebiteurNomPrenom() {
		return tiersDebiteurNomPrenom;
	}

	public void setTiersDebiteurNomPrenom(String tiersDebiteurNomPrenom) {
		this.tiersDebiteurNomPrenom = tiersDebiteurNomPrenom;
	}

	public String bicIbanDebiteur() {
		return bicIbanDebiteur;
	}

	public void setBicIbanDebiteur(String bicIbanDebiteur) {
		this.bicIbanDebiteur = bicIbanDebiteur;
	}

	public String dMandatCreation() {
		return dMandatCreation;
	}

	public void setdMandatCreation(String dMandatCreation) {
		this.dMandatCreation = dMandatCreation;
	}

	public String dMandatSignature() {
		return dMandatSignature;
	}

	public void setdMandatSignature(String dMandatSignature) {
		this.dMandatSignature = dMandatSignature;
	}

	public String cTypePrelevement() {
		return cTypePrelevement;
	}

	public void setcTypePrelevement(String cTypePrelevement) {
		this.cTypePrelevement = cTypePrelevement;
	}

	public List<ISepaSddEcheancier> echeanciers() {
		return echeanciers;
	}

	public void setEcheanciers(List<ISepaSddEcheancier> echeanciers) {
		this.echeanciers = echeanciers;
	}

	public EOEditingContext editingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	public String numero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public ISepaSddParam toSepaSddParam() {
		return toSepaSddParam;
	}

	public void setToSepaSddParam(ISepaSddParam toSepaSddParam) {
		this.toSepaSddParam = toSepaSddParam;
	}

	public IGrhumPersonne toDebiteurPersonne() {
		return toDebiteurPersonne;
	}

	public void setToDebiteurPersonne(IGrhumPersonne toDebiteurPersonne) {
		this.toDebiteurPersonne = toDebiteurPersonne;
	}

	public IGrhumPersonne toTiersDebiteurPersonne() {
		return toTiersDebiteurPersonne;
	}

	public void setToTiersDebiteurPersonne(IGrhumPersonne toTiersDebiteurPersonne) {
		this.toTiersDebiteurPersonne = toTiersDebiteurPersonne;
	}

	public Boolean isRecurrent() {
		return isRecurrent;
	}

	public void setIsRecurrent(Boolean isRecurrent) {
		this.isRecurrent = isRecurrent;
	}

	public String libelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public IGrhumPersonne clientPersonne() {
		return clientPersonne;
	}

	public void setClientPersonne(IGrhumPersonne clientPersonne) {
		this.clientPersonne = clientPersonne;
	}

	public IGrhumPersonne toCreancierPersonne() {
		return creancierPersonne;
	}

	public String refAppliCreation() {
		return refAppliCreation;
	}

	public void setCreancierPersonne(IGrhumPersonne creancierPersonne) {
		this.creancierPersonne = creancierPersonne;
	}

	public NSArray getEcheancesTriees(boolean fetch) {
		NSMutableArray res = new NSMutableArray();
		for (ISepaSddEcheancier echeancier : echeanciers()) {
			res.addObjectsFromArray(echeancier.getEcheancesTrieesParDateEcheanceASC(fetch));
		}
		return SepaSddEcheanceHelper.getSharedInstance().triLesEcheancesParDateEcheanceASC(res).immutableClone();
	}

	public void setToTypeEtatRelationship(IJefyAdminTypeEtat typeEtat) {

	}

	public IJefyAdminTypeEtat toTypeEtat() {
		return null;
	}

	public IGrhumRib toDebiteurRib() {
		return debiteurRib;
	}


	public EOEnterpriseObject toEnterpriseObject() {
		return null;
	}

	public NSArray toSepaSddMandatHistos() {
		return NSArray.emptyArray();
	}


	public String commentaire() {
		return null;
	}


	public void setToModificateurRelationship(IGrhumPersonne personne) {
	}

	public String dCreation() {
		return null;
	}

	public String dModification() {
		return null;
	}

	public void setDCreation(String dateCreation) {
	}

	public void setDModification(String dateModification) {
	}


	public void setToCreancierPersonneRelationship(IGrhumPersonne personne) {
	}

	public void setToDebiteurPersonneRelationship(IGrhumPersonne personne) {
	}

	public void setToDebiteurAdresseRelationship(IGrhumAdresse adresse) {
	}

	public void setToTiersDebiteurPersonneRelationship(IGrhumPersonne personne) {
	}

	public void setDMandatCreation(String dateCreation) {
	}

	public void setDMandatSignature(String dateSignature) {
	}

	public void setRefAppliCreation(String refAppliCreation) {
	}

    public Integer idMandat() {
        return null;
    }

    public IGrhumAdresse toDebiteurAdresse() {
        return null;
    }

    public void setToDebiteurRibRelationship(IGrhumRib rib) {
    }

    public void setToSepaSddParamRelationship(ISepaSddParam param) {
    }


    public void setCTypePrelevement(String type) {
    }
}
