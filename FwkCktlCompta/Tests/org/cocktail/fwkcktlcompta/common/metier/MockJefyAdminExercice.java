package org.cocktail.fwkcktlcompta.common.metier;

import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;

public class MockJefyAdminExercice implements IJefyAdminExercice {

	private String exeStat;
	private String exeType;
	private Integer exeExercice;

	public MockJefyAdminExercice(Integer exeExercice, String exeType, String exeStat) {
		setExeExercice(exeExercice);
		setExeStat(exeStat);
		setExeType(exeType);
	}

	public String exeStat() {
		return exeStat;
	}

	public void setExeStat(String exeStat) {
		this.exeStat = exeStat;
	}

	public String exeType() {
		return exeType;
	}

	public void setExeType(String exeType) {
		this.exeType = exeType;
	}

	public Integer exeExercice() {
		return exeExercice;
	}

	public void setExeExercice(Integer exeExercice) {
		this.exeExercice = exeExercice;
	}

}
