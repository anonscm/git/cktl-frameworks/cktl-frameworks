package org.cocktail.fwkcktlcompta.common.metier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

public class SepaSddEcheancierHelperTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCacluleDatePremiereEcheanceAvecDateDuJour() {
		LocalDate datePremiereEcheance = SepaSddEcheancierHelper.getSharedInstance().calculeDatePremiereEcheanceAvecDateDuJour(4, 5);
		assertNotNull("Date de première échéance : " + datePremiereEcheance.toString(), datePremiereEcheance);
		assertEquals(datePremiereEcheance.dayOfMonth().get(), 5);
		assertFalse(datePremiereEcheance.getMonthOfYear()==LocalDate.now().getMonthOfYear());
		
	}
	
	@Test
	public void testCacluleDatePremiereEcheanceAvecDateSurExerciceSuivant() {
		LocalDate uneDateATester = new LocalDate(2012, 12, 21);
		LocalDate datePremiereEcheance = SepaSddEcheancierHelper.getSharedInstance().calculeDatePremiereEcheanceAvecDate(uneDateATester, 4, 5);
		assertNotNull("Date de première échéance : " + datePremiereEcheance.toString(), datePremiereEcheance);
		assertEquals(datePremiereEcheance, new LocalDate(2013,01,05));
		assertFalse(datePremiereEcheance.getMonthOfYear() == uneDateATester.getMonthOfYear());
		
	}

	@Test
	public void testCacluleDatePremiereEcheanceAvecDateSurLeMemeExercice() {
		LocalDate uneDateATester = new LocalDate(2012, 11, 21);
		LocalDate datePremiereEcheance = SepaSddEcheancierHelper.getSharedInstance().calculeDatePremiereEcheanceAvecDate(uneDateATester, 4, 5);
		assertNotNull("Date de première échéance : " + datePremiereEcheance.toString(), datePremiereEcheance);
		assertEquals(datePremiereEcheance, new LocalDate(2012, 12, 05));
		assertFalse(datePremiereEcheance.getMonthOfYear() == uneDateATester.getMonthOfYear());

	}
}
