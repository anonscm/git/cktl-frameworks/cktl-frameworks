package org.cocktail.fwkcktlcompta.common.metier;

import org.cocktail.fwkcktlcompta.common.entities.IEcriture;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class MockEcriture implements IEcriture {

	NSTimestamp date;

	public NSArray toEcritureDetails() {
		return null;
	}

	public IJefyAdminExercice toExercice() {
		return null;
	}

	public Integer ecrNumero() {
		return null;
	}

	public String ecrLibelle() {
		return null;
	}

	public NSTimestamp ecrDate() {
		return date;
	}

}
