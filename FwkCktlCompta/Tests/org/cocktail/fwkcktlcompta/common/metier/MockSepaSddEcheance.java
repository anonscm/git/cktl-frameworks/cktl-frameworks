package org.cocktail.fwkcktlcompta.common.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IRecouvrement;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheanceEcd;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.TypeOperation;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;

public class MockSepaSddEcheance implements ISepaSddEcheance {

	private ISepaSddEcheancier echeancier;
	private BigDecimal montant;
	private String datePrevue;
	private String dateCreation;
	private String dateModification;
	private Etat etat;
	private Integer createurId;
	private Integer derniereModificationPar;
	private Integer numero;
	private IGrhumPersonne grhumPersonne;
	private TypeOperation typeOperation;
	private String sddTypeOpTmp;
	private String debiteurBic = "BNPAFRPP";

	public ISepaSddEcheancier echeancier() {
		return echeancier;
	}

	public void setEcheancier(ISepaSddEcheancier echeancier) {
		this.echeancier = echeancier;
	}

	public BigDecimal montantAPayer() {
		return montant;
	}

	public void setMontantAPayer(BigDecimal montant) {
		this.montant = montant;
	}

	public String dateCreation() {
		return dateCreation;
	}

	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String dateModification() {
		return dateModification;
	}

	public void setDateModification(String dateModification) {
		this.dateModification = dateModification;
	}

	public String dPrevue() {
		return datePrevue;
	}

	public void setDatePrevue(String datePrevue) {
		this.datePrevue = datePrevue;
	}

	public Etat etatAsEnum() {
		return etat;
	}

	public void setEtatEnum(Etat etat) {
		this.etat = etat;
	}

	public Integer createurId() {
		return createurId;
	}

	public void setCreateurId(Integer persId) {
		this.createurId = persId;
	}

	public Integer derniereModificationPar() {
		return derniereModificationPar;
	}

	public void setDerniereModificationPar(Integer persId) {
		this.derniereModificationPar = persId;
	}

	public Integer numero() {
		return numero;
	}

	public void setNumero(Integer value) {
		this.numero = value;
	}

	public EOEnterpriseObject toEnterpriseObject() {
		return null;
	}

	public void setToModificateurPersonne(IGrhumPersonne grhumPersonne) {
		this.grhumPersonne = grhumPersonne;

	}

	public IGrhumPersonne modficateur() {
		return grhumPersonne;
	}

	public String sddTypeOp() {
		if (typeOperation == null) {
			return null;
		}
		return typeOperation.toString();
	}

	public void setTypeOperation(TypeOperation typeOperation) {
		this.typeOperation = typeOperation;
	}

	public boolean estAnnulable() {
		return (etatAsEnum().equals(Etat.ATTENTE));
	}

	public boolean estSupprimable() {
		return (etatAsEnum().equals(Etat.ANNULE) || etatAsEnum().equals(Etat.ATTENTE));
	}

	public boolean estModifiable() {
		return (etatAsEnum().equals(Etat.ATTENTE));
	}

	public String getEtat() {
		return etat.toString();
	}

	public String rum() {
		// TODO Auto-generated method stub
		return null;
	}

	public String debiteurBic() {
		return debiteurBic;
	}

	public String debiteurIban() {
		// TODO Auto-generated method stub
		return null;
	}

	public String debiteurId() {
		// TODO Auto-generated method stub
		return null;
	}

	public String debiteurNom() {
		// TODO Auto-generated method stub
		return null;
	}

	public String creancierIcs() {
		// TODO Auto-generated method stub
		return null;
	}

	public String creancierNom() {
		// TODO Auto-generated method stub
		return null;
	}

	public IRecouvrement toRecouvrement() {
		// TODO Auto-generated method stub
		return null;
	}

	public String sddTypeOpTmp() {
		if (sddTypeOpTmp == null) {
			sddTypeOpTmp = sddTypeOp();
		}
		return sddTypeOpTmp;
	}

	public void setTypeOperationTmp(TypeOperation typeOperation) {
		if (typeOperation == null) {
			sddTypeOpTmp = null;
		}
		else {
			sddTypeOpTmp = typeOperation.toString();
		}

	}

	public void setDebiteurBic(String value) {
		this.debiteurBic = value;
	}

	public NSArray<? extends ISepaSddEcheanceEcd> toSepaSddEcheanceEcds() {
		// TODO Auto-generated method stub
		return null;
	}

	public String dPreleve() {
		// TODO Auto-generated method stub
		return null;
	}
}
