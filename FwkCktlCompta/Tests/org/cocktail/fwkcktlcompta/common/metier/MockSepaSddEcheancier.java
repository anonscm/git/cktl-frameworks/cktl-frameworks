package org.cocktail.fwkcktlcompta.common.metier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class MockSepaSddEcheancier implements ISepaSddEcheancier {

	private Integer nbEcheances;
	private BigDecimal montantAPayer;
	private String datePremiereEcheance;
	private List<ISepaSddEcheance> echeances = new ArrayList<ISepaSddEcheance>();
	private String dateCreation;
	private String dateModification;
	private Integer createurId;
	private Integer derniereModificationPar;
	private ISepaSddMandat mandat;
	private IGrhumPersonne grhumPersonne;
	private ISepaSddOrigine origine;

	public ISepaSddEcheance creerEcheance() {
		return new MockSepaSddEcheance();
	}

	public ISepaSddEcheance creerEcheance(EOEditingContext edc) {
		return creerEcheance();
	}
	
	public Integer nombreEcheancesPrevues() {
		return nbEcheances;
	}

	public BigDecimal montantAPayer() {
		return montantAPayer;
	}

	public BigDecimal montantPremiereEcheance() {
		return null;
	}

	public List<ISepaSddEcheance> echeances() {
		return echeances;
	}

	public BigDecimal sommeEcheances() {
		return null;
	}

	public void setMontantAPayer(BigDecimal montantAPayer) {
		this.montantAPayer = montantAPayer;
	}

	public void setEcheances(List<ISepaSddEcheance> echeances) {
		this.echeances = echeances;
	}

	public void setDatePremiereEcheance(String datePremiereEcheance) {
		this.datePremiereEcheance = datePremiereEcheance;
	}

	public String datePremiereEcheance() {
		return this.datePremiereEcheance;
	}

	public void ajouterEcheances(List<ISepaSddEcheance> echeances) {
		if (echeances != null) {
			echeances.addAll(echeances);
		}
	}

	public void setNombreEcheancesPrevues(Integer nombreEcheancesPrevues) {
		this.nbEcheances = nombreEcheancesPrevues;
	}

	public String dateCreation() {
		return dateCreation;
	}

	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String dateModification() {
		return dateModification;
	}

	public void setDateModification(String dateModification) {
		this.dateModification = dateModification;
	}

	public Integer createurId() {
		return createurId;
	}

	public void setCreateurId(Integer createurId) {
		this.createurId = createurId;
	}

	public Integer derniereModificationPar() {
		return derniereModificationPar;
	}

	public void setDerniereModificationPar(Integer derniereModificationPar) {
		this.derniereModificationPar = derniereModificationPar;
	}

	public ISepaSddMandat mandat() {
		return mandat;
	}

	public void setMandat(ISepaSddMandat mandat) {
		this.mandat = mandat;
	}

	public void supprimerEcheances() {
		if (echeances != null) {
			echeances.clear();
		}
	}

	public EOEnterpriseObject toEnterpriseObject() {
		return null;
	}

	public void setToModificateurPersonne(IGrhumPersonne grhumPersonne) {
		this.grhumPersonne = grhumPersonne;

	}

	public IGrhumPersonne modificateur() {
		return grhumPersonne;
	}

	public NSArray getEcheancesTrieesParDateEcheanceASCAvecEtatDifferentDe(Etat etat, Boolean fetch) {
		NSMutableArray res = new NSMutableArray();
		for (Iterator itecheances = echeances.iterator(); itecheances.hasNext();) {
			ISepaSddEcheance echeance = (ISepaSddEcheance) itecheances.next();
			if (!etat.equals(echeance.etatAsEnum())) {
				res.addObject(echeance);
			}
		}
		return res.immutableClone();
	}

	public NSArray getEcheancesTrieesParDateEcheanceASCAvecEtat(Etat etat, Boolean fetch) {
		NSMutableArray res = new NSMutableArray();
		for (Iterator itecheances = echeances.iterator(); itecheances.hasNext();) {
			ISepaSddEcheance echeance = (ISepaSddEcheance) itecheances.next();
			if (etat.equals(echeance.etatAsEnum())) {
				res.addObject(echeance);
			}
		}
		return res.immutableClone();
	}

	public ISepaSddOrigine creerOrigine() {
		return null;
	}

	public ISepaSddOrigine toSepaSddOrigine() {
		return origine;
	}

	public void setToSepaSddOrigineRelationship(ISepaSddOrigine origine) {
		this.origine = origine;
	}

	public boolean echeancesSupprimables() {
		return false;
	}

	public NSArray getEcheancesTrieesParDateEcheanceASC(Boolean fetch) {
		return null;
	}

	public String dCreation() {
		return null;
	}

	public String dModification() {
		return null;
	}

	public Integer persIdCreation() {
		return null;
	}

	public IGrhumPersonne toModificateur() {
		return null;
	}

	public String getReference() {
		return mandat().rum();
	}

	public EOEditingContext editingContext() {
		return null;
	}

	public String primaryKey() {
		return null;
	}

	public ISepaSddMandat toSepaSddMandat() {
		return mandat;
	}

	public NSArray toEcritureDetailsPriseEncharge() {
		return null;
	}

    public void setPersIdCreation(Integer persIdCreation) {
    }

    public void setIdSepaSddMandat(Integer idMandat) {
    }

}
