package org.cocktail.fwkcktlcompta.common.helpers;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;
import org.cocktail.fwkcktlcompta.common.metier.MockJefyAdminExercice;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.metier.MockSepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.SepaSddTestsHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.SepaSddGenerateurEcheancierConstant;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.joda.time.LocalDate;
import org.junit.Test;


/**
 * Test pour verifier les algos de regroupement d'echeances.
 *
 * @author rprin
 */
public class EcritureHelperTest {

	private static final DateConversionUtil DATE_CONVERSION_UTIL = new DateConversionUtil();
	private static final String DATE_PREMIERE_ECHEANCE_AS_STRING = "2013-01-20";
	private static final LocalDate DATE_PREMIERE_ECHEANCE = DATE_CONVERSION_UTIL.parseDateSilent("2013-01-20");
	private static final Integer PERS_ID = Integer.valueOf(1);
	private static final IGrhumPersonne MODIFICATEUR = SepaSddTestsHelper.getModificateur();





	@Test
	public void testGetDateJourneeComptableSelonExercice() {
		IJefyAdminExercice exerciceOuvert2013 = new MockJefyAdminExercice(2013, IJefyAdminExercice.EXE_TYPE_TRESORERIE, IJefyAdminExercice.EXE_ETAT_OUVERT);
		IJefyAdminExercice exerciceRestreint2013 = new MockJefyAdminExercice(2013, IJefyAdminExercice.EXE_TYPE_COMPTABLE, IJefyAdminExercice.EXE_ETAT_RESTREINT);
		Date dateBrute;
		Date dateCorrigee;
		SimpleDateFormat formatCourt = new SimpleDateFormat("dd/MM/yyyy");
		try {
			dateBrute = formatCourt.parse("30/12/2013");
			dateCorrigee = EcritureHelper.getSharedInstance().getDateJourneeComptableCorrigeeSelonExercice(dateBrute, exerciceOuvert2013);
			assertEquals("30/12/2013", formatCourt.format(dateCorrigee));

			dateBrute = formatCourt.parse("31/12/2013");
			dateCorrigee = EcritureHelper.getSharedInstance().getDateJourneeComptableCorrigeeSelonExercice(dateBrute, exerciceOuvert2013);
			assertEquals("31/12/2013", formatCourt.format(dateCorrigee));

			dateBrute = formatCourt.parse("01/01/2014");
			dateCorrigee = EcritureHelper.getSharedInstance().getDateJourneeComptableCorrigeeSelonExercice(dateBrute, exerciceOuvert2013);
			assertEquals("31/12/2013", formatCourt.format(dateCorrigee));
			
			dateBrute = formatCourt.parse("01/12/2014");
			dateCorrigee = EcritureHelper.getSharedInstance().getDateJourneeComptableCorrigeeSelonExercice(dateBrute, exerciceOuvert2013);
			assertEquals("31/12/2013", formatCourt.format(dateCorrigee));
			
			dateBrute = formatCourt.parse("01/12/2014");
			dateCorrigee = EcritureHelper.getSharedInstance().getDateJourneeComptableCorrigeeSelonExercice(dateBrute, exerciceRestreint2013);
			assertEquals("31/12/2013", formatCourt.format(dateCorrigee));

			////

			dateBrute = formatCourt.parse("01/12/2014");
			dateCorrigee = EcritureHelper.getSharedInstance().getDateJourneeComptableCorrigeeSelonExercice(dateBrute, exerciceRestreint2013);
			assertEquals("31/12/2013", formatCourt.format(dateCorrigee));

			dateBrute = formatCourt.parse("31/12/2013");
			dateCorrigee = EcritureHelper.getSharedInstance().getDateJourneeComptableCorrigeeSelonExercice(dateBrute, exerciceRestreint2013);
			assertEquals("31/12/2013", formatCourt.format(dateCorrigee));

			dateBrute = formatCourt.parse("01/01/2013");
			dateCorrigee = EcritureHelper.getSharedInstance().getDateJourneeComptableCorrigeeSelonExercice(dateBrute, exerciceRestreint2013);
			assertEquals("01/01/2013", formatCourt.format(dateCorrigee));

		} catch (Exception e) {
			e.printStackTrace();
			assertNull(e);
		}
	}


}
