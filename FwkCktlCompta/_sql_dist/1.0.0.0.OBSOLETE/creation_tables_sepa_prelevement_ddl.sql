exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.4.0', 'MARACUJA' );


-- instructions de suppression (pour le dev)
drop table maracuja.SEPA_SDD_ECHEANCE_ECD cascade constraints;
drop table maracuja.SEPA_SDD_ECHEANCE cascade constraints;
drop table maracuja.SEPA_SDD_ECHEANCIER_ECD cascade constraints;
drop table maracuja.SEPA_SDD_ECHEANCIER cascade constraints;
drop table maracuja.SEPA_SDD_origine cascade constraints;
drop table maracuja.SEPA_SDD_MANDAT_HISTO cascade constraints;
drop table maracuja.SEPA_SDD_MANDAT cascade constraints;
drop table maracuja.SEPA_SDD_origine_type cascade constraints;
drop table maracuja.SEPA_SDD_PARAM cascade constraints;

drop sequence MARACUJA.sepa_sdd_echeance_ecd_SEQ;
drop sequence MARACUJA.SEPA_SDD_ECHEANCE_SEQ;
drop sequence MARACUJA.SEPA_SDD_ECHEANCIER_ECD_SEQ;
drop sequence MARACUJA.SEPA_SDD_ECHEANCIER_SEQ;
drop sequence MARACUJA.SEPA_SDD_MANDAT_HISTO_SEQ;
drop sequence MARACUJA.SEPA_SDD_MANDAT_SEQ;
drop sequence MARACUJA.SEPA_SDD_origine_type_SEQ;
drop sequence MARACUJA.SEPA_SDD_origine_SEQ;
drop sequence MARACUJA.SEPA_SDD_MANDAT_NUMERO_SEQ;
---
grant references on grhum.adresse to maracuja;

--
CREATE SEQUENCE MARACUJA.SEPA_SDD_ECHEANCE_SEQ START WITH 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE MARACUJA.SEPA_SDD_ECHEANCIER_SEQ START WITH 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE MARACUJA.SEPA_SDD_MANDAT_SEQ START WITH 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE MARACUJA.SEPA_SDD_MANDAT_NUMERO_SEQ START WITH 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE MARACUJA.SEPA_SDD_MANDAT_HISTO_SEQ START WITH 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE MARACUJA.SEPA_SDD_origine_type_SEQ START WITH 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE MARACUJA.SEPA_SDD_origine_SEQ START WITH 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE MARACUJA.sepa_sdd_echeance_ecd_SEQ START WITH 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCYCLE NOCACHE NOORDER;
CREATE SEQUENCE MARACUJA.sepa_sdd_echeancier_ecd_SEQ START WITH 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCYCLE NOCACHE NOORDER;


CREATE TABLE MARACUJA.SEPA_SDD_PARAM
(
  ID_SEPA_SDD_PARAM             NUMBER                  NOT NULL,
  TREC_ORDRE         NUMBER                     NOT NULL,
  TENEUR_COMPTE_NOM            VARCHAR2(70)       NOT NULL,
  TENEUR_COMPTE_BIC            VARCHAR2(11)       NOT NULL,
  TENEUR_COMPTE_IBAN           VARCHAR2(34)       NOT NULL,
  TENEUR_COMPTE_CODIQUE        VARCHAR2(7)        NOT NULL,
  DFT_IBAN          VARCHAR2(34)       NOT NULL,
  DFT_TITULAIRE     VARCHAR2(70)       NOT NULL,
  EMETTEUR_NOM      VARCHAR2(70)       NOT NULL,
  TRANSFERT_ID  VARCHAR2(8)        NOT NULL,
  DEVISE        VARCHAR2(3)        NOT NULL,
  creancier_ics  VARCHAR2(30) NOT NULL,
  creancier_pers_id 	number not null,
  ETAT              VARCHAR2(10)       NOT NULL
)
TABLESPACE GFC;

COMMENT ON TABLE MARACUJA.SEPA_SDD_PARAM IS 'Les parametres utilises pour la creation des fichiers de virement de type SEPA';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.ID_SEPA_SDD_PARAM IS 'Identifiant de l''enregistrement';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.TREC_ORDRE IS 'Identifiant du type de format, référence maracuja.type_recouvrement';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.TENEUR_COMPTE_NOM IS 'Nom du teneur de compte (la DDFIP ou la RF)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.TENEUR_COMPTE_BIC IS 'BIC du teneur de compte (BDFEFRPPCCT pour la banque de France / IDDOFRP1 pour l''IEDOM)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.TENEUR_COMPTE_IBAN IS 'IBAN du teneur de compte';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.TENEUR_COMPTE_CODIQUE IS 'Transmis par la DGFIP, permet d''identifier le teneur de compte';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.DFT_IBAN IS 'IBAN du compte au tresor';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.DFT_TITULAIRE IS 'Titulaire du compte au tresor';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.EMETTEUR_NOM IS 'Nom de l''établissement qui emet le prelevement';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.TRANSFERT_ID IS 'Identification de l''établissement coté teneur de compte. Permet au teneur de compte d''identifier l''émetteur du fichier de prélèvement. Utilisé dans le tag MessageIdentification';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.DEVISE IS 'Code de la devise pour les prélèvements';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.creancier_ics IS 'Identifiant créancier SEPA';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.creancier_pers_id  IS 'Reference a la personne morale creancier (normalement l''etablissement)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_PARAM.ETAT IS 'Enregistrement valide ou non (VALIDE/ANNULE)';


ALTER TABLE MARACUJA.SEPA_SDD_PARAM ADD (PRIMARY KEY (ID_SEPA_SDD_PARAM) USING INDEX TABLESPACE GFC_INDX);

ALTER TABLE MARACUJA.SEPA_SDD_PARAM ADD ( CONSTRAINT FK_SEPA_SDD_PARAM_CREANC  foreign key (creancier_pers_id) references grhum.personne(pers_id));
ALTER TABLE MARACUJA.SEPA_SDD_PARAM ADD ( CONSTRAINT FK_SEPA_SDD_PARAM_TREC  foreign key (TREC_ORDRE) references maracuja.type_recouvrement(trec_ordre));




-- creation des tables necessaires pour la gestion des prelevements SEPA (SDD)


CREATE TABLE MARACUJA.SEPA_SDD_origine_type (
  ID_SEPA_SDD_origine_type  number not null,
  type_nom			        varchar2(100) not null,
  type_ordre				number(2,0) not null,
  type_code                 varchar2(3) not null,
  type_class		        varchar2(500) not null,
  type_table		        varchar2(200) not null,
  type_table_cle		    varchar2(50) not null,
  type_commentaire          varchar2(500) not null,
  valide					varchar2(1) not null					    
)
TABLESPACE GFC;

CREATE UNIQUE INDEX MARACUJA.PK_SEPA_SDD_origine_type ON MARACUJA.SEPA_SDD_origine_type(ID_SEPA_SDD_origine_type) LOGGING TABLESPACE GFC_INDX;
ALTER TABLE MARACUJA.SEPA_SDD_origine_type ADD ( CONSTRAINT PK_SEPA_SDD_origine_type  PRIMARY KEY (ID_SEPA_SDD_origine_type));

CREATE UNIQUE INDEX MARACUJA.UNIQ_SEPA_SDD_o_type_class ON MARACUJA.SEPA_SDD_origine_type(type_class) LOGGING TABLESPACE GFC_INDX;
alter table MARACUJA.SEPA_SDD_origine_type add constraint UNIQ_SEPA_SDD_o_type_class unique(type_class) using index;

CREATE UNIQUE INDEX MARACUJA.UNIQ_SEPA_SDD_o_type_nom ON MARACUJA.SEPA_SDD_origine_type(type_nom) LOGGING TABLESPACE GFC_INDX;
alter table MARACUJA.SEPA_SDD_origine_type add constraint UNIQ_SEPA_SDD_o_type_nom unique(type_nom) using index;

CREATE UNIQUE INDEX MARACUJA.UNIQ_SEPA_SDD_o_type_code ON MARACUJA.SEPA_SDD_origine_type(type_code) LOGGING TABLESPACE GFC_INDX;
alter table MARACUJA.SEPA_SDD_origine_type add constraint UNIQ_SEPA_SDD_o_type_code unique(type_code) using index;


COMMENT ON TABLE MARACUJA.SEPA_SDD_origine_type IS 'Types d''origines possibles pour les mandats de prelevement SEPA (SDD).';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_origine_type.ID_SEPA_SDD_origine_type IS 'Cle de la table';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_origine_type.type_nom IS 'Nom du type d''origine, unique';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_origine_type.type_code IS 'Code du type d''origine sur 3 caractères, unique';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_origine_type.type_table IS 'Nom de la table de la base qui stocke les objets de ce type d''origine dois inclure le user, ex. jefy_depense.facture_papier)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_origine_type.type_table_cle IS 'Nom du champ de type_table qui sert d''identifiant.';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_origine_type.type_class IS 'Nom complet de la classe java qui controle les entités de ce type. La classe doit implementer org.cocktail.fwkcktlcompta.common.metier.ISepaSddOrigineCtrl';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_origine_type.TYPE_COMMENTAIRE IS 'Commentaire sur le type d''origine.';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_origine_type.valide IS 'O/N. Ce type d''origine est autorisé ou non';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_origine_type.type_ordre IS 'Ordre de présentation du type dans une liste';

---------------------
CREATE TABLE MARACUJA.SEPA_SDD_origine (
  ID_SEPA_SDD_origine      NUMBER NOT NULL,
  ID_SEPA_SDD_origine_type NUMBER NOT NULL,
  origine_id			   NUMBER NOT NULL
)
TABLESPACE GFC;
CREATE UNIQUE INDEX MARACUJA.PK_SEPA_SDD_origine ON MARACUJA.SEPA_SDD_origine(ID_SEPA_SDD_origine) LOGGING TABLESPACE GFC_INDX;
ALTER TABLE MARACUJA.SEPA_SDD_origine ADD ( CONSTRAINT PK_SEPA_SDD_origine  PRIMARY KEY (ID_SEPA_SDD_origine));
--
ALTER TABLE MARACUJA.SEPA_SDD_origine ADD ( CONSTRAINT FK_SEPA_SDD_origine_typ  foreign key (ID_SEPA_SDD_origine_type) references maracuja.SEPA_SDD_origine_type(ID_SEPA_SDD_origine_type));

CREATE UNIQUE INDEX MARACUJA.UNIQ_SEPA_SDD_origine ON MARACUJA.SEPA_SDD_origine(ID_SEPA_SDD_origine_type,origine_id) LOGGING TABLESPACE GFC_INDX;
alter table MARACUJA.SEPA_SDD_origine add constraint UNIQ_SEPA_SDD_origine unique(ID_SEPA_SDD_origine_type,origine_id) using index;



--
COMMENT ON TABLE MARACUJA.SEPA_SDD_origine IS 'Origines des mandats de prelevement SEPA (SDD).';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_origine.ID_SEPA_SDD_origine IS 'Cle de la table';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_origine.ID_SEPA_SDD_origine_type IS 'Reference au type d''origine';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_origine.origine_id IS 'Identifiant de l''objet origine';

---------------------

CREATE TABLE MARACUJA.SEPA_SDD_MANDAT (
  ID_SEPA_SDD_MANDAT      NUMBER NOT NULL,
  PERS_ID_CREATION        NUMBER NOT NULL,
  PERS_ID_MODIFICATION    NUMBER NOT NULL,
  D_CREATION            VARCHAR2(100) NOT NULL,
  D_MODIFICATION        VARCHAR2(100) NOT NULL,
  D_MANDAT_CREATION		VARCHAR2(100) NOT NULL,
  NUMERO				number(9,0) not null,
  RUM                    VARCHAR2(35) NOT NULL,
  c_type_prelevement        varchar2(1) not null,
  tyet_id				number not null,
  ref_appli_creation   varchar2(100) not null,
  ID_SEPA_SDD_PARAM			 number not null,
  creancier_pers_id 	number not null,
  debiteur_pers_id		number not null,
  debiteur_rib_ordre		number not null,
  d_mandat_signature			VARCHAR2(100) null,
  debiteur_adr_ordre		number null,
  tiers_debiteur_pers_id	number null,
  tiers_creancier_pers_id	number null,
   libelle				varchar2(500) null,
  commentaire			varchar2(4000) null
)
TABLESPACE GFC;

CREATE UNIQUE INDEX MARACUJA.PK_SEPA_SDD_MANDAT ON MARACUJA.SEPA_SDD_MANDAT(ID_SEPA_SDD_MANDAT) LOGGING TABLESPACE GFC_INDX;
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT PK_SEPA_SDD_MANDAT  PRIMARY KEY (ID_SEPA_SDD_MANDAT));
--
CREATE UNIQUE INDEX MARACUJA.UNIQ_SEPA_SDD_numero ON MARACUJA.SEPA_SDD_MANDAT(numero) LOGGING TABLESPACE GFC_INDX;
alter table MARACUJA.SEPA_SDD_MANDAT add constraint UNIQ_SEPA_SDD_numero unique(numero) using index;
CREATE UNIQUE INDEX MARACUJA.UNIQ_SEPA_SDD_rum ON MARACUJA.SEPA_SDD_MANDAT(rum) LOGGING TABLESPACE GFC_INDX;
alter table MARACUJA.SEPA_SDD_MANDAT add constraint UNIQ_SEPA_SDD_rum unique(rum) using index;

--
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_creation  foreign key (PERS_ID_CREATION) references grhum.personne(PERS_ID));
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_modif  foreign key (PERS_ID_MODIFICATION) references grhum.personne(PERS_ID));
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_type_etat  foreign key (tyet_id) references jefy_admin.type_etat(tyet_id));
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_creancier  foreign key (creancier_pers_id) references grhum.personne(pers_id));
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_debiteur  foreign key (debiteur_pers_id) references grhum.personne(pers_id));
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_tiers_cr  foreign key (tiers_creancier_pers_id) references grhum.personne(pers_id));
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_tiers_deb  foreign key (tiers_debiteur_pers_id) references grhum.personne(pers_id));
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_deb_rib  foreign key (debiteur_rib_ordre) references grhum.ribfour_ulr(rib_ordre));
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_deb_adr  foreign key (debiteur_adr_ordre) references grhum.adresse(adr_ordre));
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_param  foreign key (ID_SEPA_SDD_PARAM) references maracuja.SEPA_SDD_PARAM(ID_SEPA_SDD_PARAM));
--
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT CHK_SEPA_SDD_MANDAT_type  CHECK (c_type_prelevement ='R' or c_type_prelevement='P'));
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT ADD ( CONSTRAINT CHK_SEPA_SDD_MANDAT_etat  CHECK (tyet_id in (1,2,7)));


COMMENT ON TABLE MARACUJA.SEPA_SDD_MANDAT IS 'Mandat de prelevement SEPA (SDD)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.ID_SEPA_SDD_MANDAT IS 'Cle de la table';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.PERS_ID_CREATION IS 'Reference a la personne qui a cree l''enregistrement';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.PERS_ID_MODIFICATION IS 'Reference a la derniere personne qui a modifie l''enregistrement';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.D_CREATION IS 'Date de creation de l''enregistrement (stockee au format ISO8601 yyyy-mm-ddTHH:MM:SS.SSS)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.D_MODIfICATION IS 'Date de derniere modification de l''enregistrement (stockee au format ISO8601 yyyy-mm-ddTHH:MM:SS.SSS)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.NUMERO IS 'Numero (unique) du mandat';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.RUM IS 'Reference unique du mandat, construite à partir de l''ICS, du numero et d''une reference construite à partir de l''origine';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.c_type_prelevement IS 'Type de prelevement : R/P (Recurrent/Ponctuel)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.tyet_id IS 'Etat du mandat (references à jefy_admin.type_etat). 1: valide, 2: annule, 7: a valider';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.creancier_pers_id IS 'Reference au creancier (structure qui recouvre la creance), dispose d''un compte au tresor et d''un ICS';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.ID_SEPA_SDD_PARAM IS 'Reference a sepa_sdd_param (les parametres du compte au tresor du creancier). Permet notamment de recuperer l''ICS';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.debiteur_pers_id IS 'Reference au debiteur';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.debiteur_rib_ordre IS 'Reference au coordonnees bancaire du debiteur';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.debiteur_adr_ordre IS 'Reference a l''adresse postale du debiteur. Si null, on récupère l''adresse pprincipale de facturation';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.tiers_creancier_pers_id IS 'Reference au tiers creancier. Structure pour qui la creance est emise mais qui n''est pas charge de la recouvrer';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.tiers_debiteur_pers_id IS 'Reference au tiers debiteur (lorsque le debiteur paye la dette pour le compte d''un tiers d''un tiers)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.D_MANDAT_CREATION IS 'Date de creation du mandat SDD (stockee au format ISO8601 yyyy-mm-dd)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.D_MANDAT_signature IS 'Date de signature du mandat SDD (stockee au format ISO8601 yyyy-mm-dd) par le debiteur';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.commentaire IS 'Commentaire interne réservé aux utilisateurs';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.libelle IS 'Libellé du mandat (sera visible par le débiteur)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT.ref_appli_creation IS 'Référence interne gérée par l''application qui crée le mandat';

---------------------

CREATE TABLE MARACUJA.SEPA_SDD_MANDAT_HISTO (
  ID_SEPA_SDD_MANDAT_HISTO NUMBER NOT NULL,
  ID_SEPA_SDD_MANDAT      NUMBER NOT NULL,
  PERS_ID_CREATION        NUMBER NOT NULL,
  D_CREATION            VARCHAR2(100) NOT NULL,
  libelle   		        varchar2(500) NOT NULL,
  rum                   varchar2(35) not null,
  c_type_prelevement        varchar2(1) not null,
  creancier_id number not null,
  creancier_nom varchar2(200) not null,
  creancier_ics	varchar2(30) not null,
  debiteur_id number   not null,
  debiteur_nom varchar2(200) not null,
  debiteur_bic varchar2(11)  not null,
  debiteur_iban varchar2(35) not null,
  tyet_id				number not null
  
)
TABLESPACE GFC;
CREATE UNIQUE INDEX MARACUJA.PK_SEPA_SDD_MANDAT_HISTO ON MARACUJA.SEPA_SDD_MANDAT_HISTO(ID_SEPA_SDD_MANDAT_HISTO) LOGGING TABLESPACE GFC_INDX;
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT_HISTO ADD ( CONSTRAINT PK_SEPA_SDD_MANDAT_HISTO  PRIMARY KEY (ID_SEPA_SDD_MANDAT_HISTO));
--
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT_HISTO ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_histo_creat  foreign key (PERS_ID_CREATION) references grhum.personne(PERS_ID));
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT_HISTO ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_HISTO_MAND FOREIGN KEY (ID_SEPA_SDD_MANDAT) REFERENCES maracuja.SEPA_SDD_MANDAT(ID_SEPA_SDD_MANDAT)  deferrable initially deferred);
ALTER TABLE MARACUJA.SEPA_SDD_MANDAT_HISTO ADD ( CONSTRAINT FK_SEPA_SDD_MANDAT_HISTO_TYET FOREIGN KEY (TYET_ID) REFERENCES jefy_admin.type_etat(tyet_id));
--
COMMENT ON TABLE MARACUJA.SEPA_SDD_MANDAT_HISTO IS 'Historique du mandat de prelevement SEPA (SDD)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.ID_SEPA_SDD_MANDAT_HISTO IS 'Cle de la table';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.ID_SEPA_SDD_MANDAT IS 'Reference au mandat SDD';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.PERS_ID_CREATION IS 'Reference a la personne qui a cree l''enregistrement';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.D_CREATION IS 'Date de creation de l''enregistrement (stockee au format ISO8601 yyyy-mm-ddTHH:MM:SS.SSS)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.libelle IS 'Libelle de l''operation a l''origine de l''historisation';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.rum IS 'Reference unique du mandat';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.c_type_prelevement IS 'Type de prelevement : R/P (Recurrent/Ponctuel)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.creancier_id IS 'Identifiant du creancier (= pers_id)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.creancier_nom IS 'Nom du creancier (concatenation de personne.libelle et personne.pers_lc)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.creancier_ics IS 'Identifiant créancier SEPA';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.debiteur_id IS 'Identifiant du debiteur (= pers_id)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.debiteur_nom IS 'Nom du debiteur (concatenation de personne.libelle et personne.pers_lc)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.debiteur_bic IS 'BIC de la banque du débiteur';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.debiteur_iban IS 'IBAN du débiteur';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_MANDAT_HISTO.tyet_id IS 'Etat du mandat';

----------------------

CREATE TABLE MARACUJA.SEPA_SDD_ECHEANCIER (
  ID_SEPA_SDD_ECHEANCIER  NUMBER NOT NULL,
  ID_SEPA_SDD_MANDAT      NUMBER NOT NULL,
  PERS_ID_CREATION        NUMBER NOT NULL,
  PERS_ID_MODIFICATION    NUMBER NOT NULL,
  D_CREATION            VARCHAR2(100) NOT NULL,
  D_MODIFICATION        VARCHAR2(100) NOT NULL,
  ID_SEPA_SDD_origine       number not null
)
TABLESPACE GFC;
CREATE UNIQUE INDEX MARACUJA.PK_SEPA_SDD_ECHEANCIER ON MARACUJA.SEPA_SDD_ECHEANCIER(ID_SEPA_SDD_ECHEANCIER) LOGGING TABLESPACE GFC_INDX;
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCIER ADD ( CONSTRAINT PK_SEPA_SDD_ECHEANCIER  PRIMARY KEY (ID_SEPA_SDD_ECHEANCIER));
--
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCIER ADD ( CONSTRAINT FK_SEPA_SDD_ECHEANCIER_creat  foreign key (PERS_ID_CREATION) references grhum.personne(PERS_ID));
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCIER ADD ( CONSTRAINT FK_SEPA_SDD_ECHEANCIER_modif  foreign key (PERS_ID_MODIFICATION) references grhum.personne(PERS_ID));
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCIER ADD ( CONSTRAINT FK_SEPA_SDD_ECHEANCIER_MANDAT FOREIGN KEY (ID_SEPA_SDD_MANDAT) REFERENCES maracuja.SEPA_SDD_MANDAT(ID_SEPA_SDD_MANDAT));
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCIER ADD ( CONSTRAINT FK_SEPA_SDD_ECHEANCIER_origine  foreign key (ID_SEPA_SDD_origine) references maracuja.sepa_sdd_origine(ID_SEPA_SDD_origine) deferrable initially deferred);
--ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCIER ADD ( CONSTRAINT FK_SEPA_SDD_ECHEANCIER_ecd  foreign key (ecd_ordre) references maracuja.ecriture_detail(ecd_ordre) deferrable initially deferred);

--
COMMENT ON TABLE MARACUJA.SEPA_SDD_ECHEANCIER IS 'Echeancier d''un mandat SEPA';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCIER.ID_SEPA_SDD_ECHEANCIER IS 'Cle de la table';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCIER.ID_SEPA_SDD_MANDAT IS 'Reference au mandat de prelevement SEPA (SDD)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCIER.PERS_ID_CREATION IS 'Reference a la personne qui a cree l''enregistrement';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCIER.PERS_ID_MODIFICATION IS 'Reference a la derniere personne qui a modifie l''enregistrement';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCIER.D_CREATION IS 'Date de creation de l''enregistrement (stockee au format ISO8601 yyyy-mm-ddTHH:MM:SS.SSS)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCIER.D_MODIfICATION IS 'Date de derniere modification de l''enregistrement (stockee au format ISO8601 yyyy-mm-ddTHH:MM:SS.SSS)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCIER.ID_SEPA_SDD_origine IS 'Reference l''origine de l''echeancier';
--COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCIER.ecd_ordre IS 'Reference a l''ecriture_detail de prise en charge de l''echeancier';

----------------------
CREATE TABLE MARACUJA.SEPA_SDD_ECHEANCIER_ECD (
ID_SEPA_SDD_ECHEANCIER_ECD  NUMBER NOT NULL,
  ID_SEPA_SDD_ECHEANCIER  NUMBER NOT NULL,
  ecd_ordre				number not null
)
TABLESPACE GFC;
CREATE UNIQUE INDEX MARACUJA.PK_SEPA_SDD_ECHEANCIER_ECD ON MARACUJA.SEPA_SDD_ECHEANCIER_ECD(ID_SEPA_SDD_ECHEANCIER_ECD) LOGGING TABLESPACE GFC_INDX;
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCIER_ECD ADD ( CONSTRAINT PK_SEPA_SDD_ECHEANCIER_ECD  PRIMARY KEY (ID_SEPA_SDD_ECHEANCIER_ECD));
--
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCIER_ECD ADD ( CONSTRAINT FK_SEPA_SDD_ECHEANCIER_ECD_ECH  foreign key (ID_SEPA_SDD_ECHEANCIER) references maracuja.SEPA_SDD_ECHEANCIER(ID_SEPA_SDD_ECHEANCIER) deferrable initially deferred);
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCIER_ECD ADD ( CONSTRAINT FK_SEPA_SDD_ECHEANCIER_ecd_ecd  foreign key (ecd_ordre) references maracuja.ecriture_detail(ecd_ordre) deferrable initially deferred);

--
COMMENT ON TABLE MARACUJA.SEPA_SDD_ECHEANCIER_ECD IS 'Lien entre un Echeancier d''un mandat SEPA et ses ecriture_detail de prise en charge (uniquement les debits), eventuellmeent sur plusieurs exercices';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCIER_ECD.ID_SEPA_SDD_ECHEANCIER_ECD IS 'Cle de la table';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCIER_ECD.ID_SEPA_SDD_ECHEANCIER IS 'Reference a l''echeancier';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCIER_ECD.ecd_ordre IS 'Reference a l''ecriture_detail de prise en charge de l''echeancier';

----------------------

CREATE TABLE MARACUJA.SEPA_SDD_ECHEANCE (
  ID_SEPA_SDD_ECHEANCE    NUMBER NOT NULL,
  ID_SEPA_SDD_ECHEANCIER  NUMBER NOT NULL,
  PERS_ID_CREATION        NUMBER NOT NULL,
  PERS_ID_MODIFICATION    NUMBER NOT NULL,
  D_CREATION		    VARCHAR2(100) NOT NULL,
  D_MODIFICATION    	VARCHAR2(100) NOT NULL,
  D_prevue    	VARCHAR2(100) NOT NULL,
  d_preleve VARCHAR2(100) NULL,
  etat		varchar2(20) not null,
  montant		number(12,2) not null,
  reco_ordre			number null,
  RUM                VARCHAR2(35) NULL,
  creancier_id varchar2(35) null,
  creancier_nom varchar2(200) null,
  creancier_ics	varchar2(30) null,
  debiteur_id varchar2(35)   null,
  debiteur_nom varchar2(200) null,
  debiteur_bic varchar2(11)  null,
  debiteur_iban varchar2(35) null,
  sdd_type_op varchar2(4) null,
  commentaire varchar2(500) null
)
TABLESPACE GFC;

CREATE UNIQUE INDEX MARACUJA.PK_SEPA_SDD_ECHEANCE ON MARACUJA.SEPA_SDD_ECHEANCE(ID_SEPA_SDD_ECHEANCE) LOGGING TABLESPACE GFC_INDX;
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCE ADD ( CONSTRAINT PK_SEPA_SDD_ECHEANCE  PRIMARY KEY (ID_SEPA_SDD_ECHEANCE));
--
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCE ADD ( CONSTRAINT FK_SEPA_SDD_ECH_creation  foreign key (PERS_ID_CREATION) references grhum.personne(PERS_ID));
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCE ADD ( CONSTRAINT FK_SEPA_SDD_ECH_modif  foreign key (PERS_ID_MODIFICATION) references grhum.personne(PERS_ID));
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCE ADD ( CONSTRAINT FK_SEPA_SDD_ECH_ECHEANCIER FOREIGN KEY (ID_SEPA_SDD_ECHEANCIER) REFERENCES maracuja.SEPA_SDD_ECHEANCIER(ID_SEPA_SDD_ECHEANCIER)  deferrable initially deferred);
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCE ADD ( CONSTRAINT FK_SEPA_SDD_ECH_RECOUV FOREIGN KEY (RECO_ORDRE) REFERENCES maracuja.RECOUVREMENT(RECO_ORDRE) );
ALTER TABLE MARACUJA.SEPA_SDD_ECHEANCE ADD ( CONSTRAINT CK_SEPA_SDD_ECH_ETAT CHECK (etat IN ('ATTENTE', 'PRELEVE', 'CONFIRME', 'REJETE', 'ANNULE')) );

--
COMMENT ON TABLE MARACUJA.SEPA_SDD_ECHEANCE IS 'Echeances d''un echeancier SEPA';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.ID_SEPA_SDD_ECHEANCE IS 'Cle de la table';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.ID_SEPA_SDD_ECHEANCIER IS 'Reference a l''echeancier';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.PERS_ID_CREATION IS 'Reference a la personne qui a cree l''enregistrement';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.PERS_ID_MODIFICATION IS 'Reference a la derniere personne qui a modifie l''enregistrement';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.D_CREATION IS 'Date de creation de l''enregistrement (stockee au format ISO8601 yyyy-mm-ddTHH:MM:SS.SSS)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.D_MODIfICATION IS 'Date de derniere modification de l''enregistrement (stockee au format ISO8601 yyyy-mm-ddTHH:MM:SS.SSS)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.d_prevue IS 'Date de prélèvement prevue';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.d_preleve IS 'Date de prélèvement effective';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.etat IS 'Etat de l''echeance (ATTENTE / PRELEVE / CONFIRME / REJETE / ANNULE)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.montant IS 'Montant de l''echeance';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.reco_ordre IS 'Reference au RECOUVREMENT une fois le prelevement effectue';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.rum IS 'Reference unique du mandat (une fois le prelevement effectue)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.creancier_id IS 'Identifiant du creancier (= pers_id)  (une fois le prelevement effectue)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.creancier_nom IS 'Nom du creancier SEPA (Nom du teneur de compte DDFIP ou la RF)  (une fois le prelevement effectue)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.creancier_ics IS 'Identifiant créancier SEPA  (une fois le prelevement effectue)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.debiteur_id IS 'Identifiant du debiteur (= pers_id)  (une fois le prelevement effectue)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.debiteur_nom IS 'Nom du debiteur (concatenation de personne.libelle et personne.pers_lc)  (une fois le prelevement effectue)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.debiteur_bic IS 'BIC de la banque du débiteur  (une fois le prelevement effectue)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.debiteur_iban IS 'IBAN du débiteur  (une fois le prelevement effectue)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.sdd_type_op IS 'Type operation (OOFF, FRST, FNAL, RCUR)  (une fois le prelevement effectue)';
COMMENT ON COLUMN MARACUJA.SEPA_SDD_ECHEANCE.commentaire IS 'Commentaire libre sur l''echeance';




----
CREATE TABLE MARACUJA.sepa_sdd_echeance_ecd (
  ID_sepa_sdd_echeance_ecd  NUMBER NOT NULL,
  ID_SEPA_SDD_ECHEANCE  NUMBER NOT NULL,
  ECD_ORDRE    NUMBER    NOT NULL
)
TABLESPACE GFC;
CREATE UNIQUE INDEX MARACUJA.PK_sepa_sdd_echeance_ecd ON MARACUJA.sepa_sdd_echeance_ecd(ID_sepa_sdd_echeance_ecd) LOGGING TABLESPACE GFC_INDX;
ALTER TABLE MARACUJA.sepa_sdd_echeance_ecd ADD ( CONSTRAINT PK_sepa_sdd_echeance_ecd  PRIMARY KEY (ID_sepa_sdd_echeance_ecd));
ALTER TABLE MARACUJA.sepa_sdd_echeance_ecd ADD ( CONSTRAINT FK_SEPA_SDD_Eche_ecd  foreign key (ECD_ORDRE) references maracuja.ecriture_detail(ecd_ordre) deferrable initially deferred);
ALTER TABLE MARACUJA.sepa_sdd_echeance_ecd ADD ( CONSTRAINT FK_SEPA_SDD_Eche_ech  foreign key (ID_SEPA_SDD_ECHEANCE) references maracuja.sepa_sdd_echeance(ID_SEPA_SDD_ECHEANCE));

COMMENT ON TABLE MARACUJA.sepa_sdd_echeance_ecd IS 'Liens entre une echeance et les details d''ecriture lies';
COMMENT ON COLUMN MARACUJA.sepa_sdd_echeance_ecd.ID_sepa_sdd_echeance_ecd IS 'Cle de la table';
COMMENT ON COLUMN MARACUJA.sepa_sdd_echeance_ecd.ID_SEPA_SDD_ECHEANCE IS 'Reference a l''echeance';
COMMENT ON COLUMN MARACUJA.sepa_sdd_echeance_ecd.ECD_ORDRE IS 'Reference a l''ecriture detail';




----
ALTER TABLE MARACUJA.PRELEVEMENT_FICHIER MODIFY(FICP_CODE_OP VARCHAR2(4));
COMMENT ON COLUMN MARACUJA.PRELEVEMENT_FICHIER.FICP_CODE_OP IS 'Code operation, correspond a la vitesse de traitement du prelevement. in (80, 85) / Ou type operation sepa sdd';

ALTER TABLE MARACUJA.PRELEVEMENT_FICHIER ADD (FICP_compte VARCHAR2(35));
COMMENT ON COLUMN MARACUJA.PRELEVEMENT_FICHIER.FICP_compte IS 'Numero du compte beneficiaire des prelevements';



