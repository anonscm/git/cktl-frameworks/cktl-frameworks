--delete from maracuja.SEPA_SDD_ORIGINE_TYPE;

Insert into maracuja.SEPA_SDD_ORIGINE_TYPE (ID_SEPA_SDD_ORIGINE_TYPE,TYPE_NOM, type_ordre, type_code, TYPE_CLASS,TYPE_TABLE,TYPE_TABLE_CLE,TYPE_COMMENTAIRE, valide) values
(maracuja.SEPA_SDD_ORIGINE_TYPE_seq.nextval,'Recette de PIE',2,'REC','org.cocktail.fwkcktlcompta.common.sepasdd.origines.SepaSddOrigineCtrlJefyRecetteRecettePapier','jefy_recette.recette_papier','RPP_ID','Recettes issues de PIE', 'N');

Insert into maracuja.SEPA_SDD_ORIGINE_TYPE (ID_SEPA_SDD_ORIGINE_TYPE,TYPE_NOM, type_ordre, type_code, TYPE_CLASS,TYPE_TABLE,TYPE_TABLE_CLE,TYPE_COMMENTAIRE, valide) values
(maracuja.SEPA_SDD_ORIGINE_TYPE_seq.nextval,'Facture de PIE',1,'FAC','org.cocktail.fwkcktlcompta.common.sepasdd.origines.SepaSddOrigineCtrlJefyRecetteFacturePapier','jefy_recette.facture_papier','FAP_ID','Factures issues de PIE', 'O');


Insert into MARACUJA.TYPE_RECOUVREMENT (TREC_ORDRE, TREC_LIBELLE, TREC_VALIDITE, MOD_DOM, TREC_FORMAT) Values (2, 'SEPA SDD', 'O', 'ECHEANCIER', 'SEPASDD');


commit;