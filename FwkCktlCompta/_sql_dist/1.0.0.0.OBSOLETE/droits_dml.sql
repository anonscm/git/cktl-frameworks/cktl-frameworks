begin
	
	
	
jefy_admin.api_application.creerfonction( 568 ,
             'SEPASDD',
             'Prélèvements',
             'Gestion des mandats de prélèvements SEPA SDD',
             'Mandats SEPA SDD',
             'N',
             'N',
             4);
             
jefy_admin.api_application.creerfonction( 569 ,
             'SSDDVAL',
             'Prélèvements',
             'Validation des mandats SEPA SDD',
             'Validation des mandats SEPA SDD',
             'N',
             'N',
             4);
             
jefy_admin.api_application.creerfonction( 318 ,
             'RESEPA',
             'Echéanciers',
             'Gestion des mandats de prélèvements SEPA SDD',
             'Mandats SEPA SDD',
             'N',
             'N',
             6);
             
             
commit;
	update jefy_admin.fonction set fon_description='Gestion des recouvrements', fon_libelle='Gestion des recouvrements' where fon_ordre=558;
-- donner autorisation aux utilisateurs qui possedent deja un droit sur les prelèvements
          INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT (UF_ORDRE, UTL_ORDRE, FON_ORDRE)
          select jefy_admin.UTILISATEUR_FONCT_SEQ.NEXTVAL, utl_ordre, 568 from JEFY_ADMIN.UTILISATEUR_FONCT where fon_ordre=558;
          
          INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT (UF_ORDRE, UTL_ORDRE, FON_ORDRE)
          select jefy_admin.UTILISATEUR_FONCT_SEQ.NEXTVAL, utl_ordre, 569 from JEFY_ADMIN.UTILISATEUR_FONCT where fon_ordre=558;

          -- donner autorisation aux utilisateurs qui possedent deja un droit sur la création de factures
          INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT (UF_ORDRE, UTL_ORDRE, FON_ORDRE)
          select jefy_admin.UTILISATEUR_FONCT_SEQ.NEXTVAL, utl_ordre, 318 from JEFY_ADMIN.UTILISATEUR_FONCT where fon_ordre=300;

commit;

end;