package org.cocktail.fwkcktlcomptaguiajax.controllers.brouillard;

import org.cocktail.fwkcktlcomptaguiajax.components.brouillard.BrouillardList;
import org.cocktail.fwkcktlcomptaguiajax.controllers.AComponentCtrl;

public class BrouillardListCtrl extends AComponentCtrl {

	public BrouillardListCtrl(BrouillardList brouillardList) {
		super(brouillardList);
	}

}
