package org.cocktail.fwkcktlcomptaguiajax.controllers.brouillard;

import org.cocktail.fwkcktlcomptaguiajax.components.brouillard.BrouillardDetailList;
import org.cocktail.fwkcktlcomptaguiajax.controllers.AComponentCtrl;

public class BrouillardDetailListCtrl extends AComponentCtrl {

	public BrouillardDetailListCtrl(BrouillardDetailList brouillardDetailList) {
		super(brouillardDetailList);
	}

}
