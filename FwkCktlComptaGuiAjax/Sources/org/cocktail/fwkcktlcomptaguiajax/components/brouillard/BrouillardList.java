package org.cocktail.fwkcktlcomptaguiajax.components.brouillard;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlcompta.server.metier.EOBrouillard;
import org.cocktail.fwkcktlcompta.server.metier.EOOrigine;
import org.cocktail.fwkcktlcomptaguiajax.components.AComponent;
import org.cocktail.fwkcktlcomptaguiajax.controllers.brouillard.BrouillardListCtrl;
import org.cocktail.fwkcktldroitsutils.common.metier.EOExercice;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Affiche une liste de brouillards (au sens comptable)
 * 
 * @binding ctrl
 * @binding colonnesKeys
 * @binding dg Obligatoire. DisplayGroup contenant les données.
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class BrouillardList extends AComponent {
	private static final long serialVersionUID = 4192475223930049390L;
	private static final String OBJ_KEY = "unObjet.";
	private static final String COL_BRO_NUMERO = EOBrouillard.BRO_NUMERO_KEY;
	private static final String COL_EXE_EXERCICE = EOBrouillard.TO_EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY;
	private static final String COL_BRO_LIBELLE = EOBrouillard.BRO_LIBELLE_KEY;
	private static final String COL_DATE_CREATION = EOBrouillard.DATE_CREATION_KEY;
	private static final String COL_PERS_CREATION = EOBrouillard.TO_PERSONNE_CREATION_KEY + "." + EOPersonne.NOM_AND_PRENOM_KEY;
	private static final String COL_BRO_ETAT = EOBrouillard.BRO_ETAT_KEY;
	private static final String COL_ORIGINE = EOBrouillard.TO_ORIGINE_KEY + "." + EOOrigine.ORI_LIBELLE_KEY;
	private static final String COL_MOTIF_REJET = EOBrouillard.BRO_MOTIF_REJET_KEY;

	private static final String BINDING_ctrl = "ctrl";
	/** Bindings pour les colonnes a afficher, {@link BrouillardList#DEFAULT_COLONNES_KEYS} */
	public static final String BINDING_colonnesKeys = "colonnesKeys";

	private NSArray<CktlAjaxTableViewColumn> colonnes;
	public static final NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();

	private BrouillardListCtrl ctrl;

	public EOBrouillard unObjet;
	private EOBrouillard selectedObjet;

	public BrouillardList(WOContext context) {
		super(context);
	}

	public BrouillardListCtrl getCtrl() {
		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (BrouillardListCtrl) valueForBinding(BINDING_ctrl);
			}
			if (ctrl == null) {
				ctrl = new BrouillardListCtrl(this);
			}
		}
		if (ctrl.getMyComponent() == null) {
			ctrl.setMyComponent(this);
		}
		return ctrl;
	}

	static {

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Exercice");
		col1.setOrderKeyPath(COL_EXE_EXERCICE);
		//		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(IND_KEY+COL_NUMERO_KEY, " ");
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_EXE_EXERCICE, " ");
		col1.setAssociations(ass1);
		_colonnesMap.takeValueForKey(col1, COL_EXE_EXERCICE);

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Numéro");
		col2.setOrderKeyPath(COL_BRO_NUMERO);
		//CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(IND_KEY+COL_NOM_KEY, " ");
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_BRO_NUMERO, " ");
		col2.setAssociations(ass2);
		_colonnesMap.takeValueForKey(col2, COL_BRO_NUMERO);

		CktlAjaxTableViewColumn col4 = new CktlAjaxTableViewColumn();
		col4.setLibelle("Libellé");
		//col4.setComponent(FouValideColonne.class.getName());
		col4.setOrderKeyPath(COL_BRO_LIBELLE);
		CktlAjaxTableViewColumnAssociation ass4 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_BRO_LIBELLE, null);
		col4.setAssociations(ass4);
		_colonnesMap.takeValueForKey(col4, COL_BRO_LIBELLE);

		CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
		col5.setLibelle("Créé par");
		col5.setOrderKeyPath(COL_PERS_CREATION);
		CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_PERS_CREATION, " ");
		col5.setAssociations(ass5);
		_colonnesMap.takeValueForKey(col5, COL_PERS_CREATION);

		CktlAjaxTableViewColumn col6 = new CktlAjaxTableViewColumn();
		col6.setLibelle("Créé le");
		//col6.setComponent(CktlAjaxTVCheckBoxCell.class.getName());
		col6.setOrderKeyPath(COL_DATE_CREATION);
		col6.setRowCssClass("alignToCenter useMinWidth nowrap");
		CktlAjaxTableViewColumnAssociation ass6 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_DATE_CREATION, " ");
		ass6.setDateformat("dd/MM/yyyy");
		//ass6.setObjectForKey(PERS_KEY + COL_IS_INTERNE_KEY, "checked");
		col6.setAssociations(ass6);
		_colonnesMap.takeValueForKey(col6, COL_DATE_CREATION);

		CktlAjaxTableViewColumn col7 = new CktlAjaxTableViewColumn();
		col7.setLibelle("Etat");
		col7.setOrderKeyPath(COL_BRO_ETAT);
		//CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(STR_KEY+COL_SIRET_KEY, " ");
		CktlAjaxTableViewColumnAssociation ass7 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_BRO_ETAT, " ");
		col7.setAssociations(ass7);
		_colonnesMap.takeValueForKey(col7, COL_BRO_ETAT);

		CktlAjaxTableViewColumn col8 = new CktlAjaxTableViewColumn();
		col8.setLibelle("Origine");
		col8.setOrderKeyPath(COL_ORIGINE);
		//CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(STR_KEY+COL_SIRET_KEY, " ");
		CktlAjaxTableViewColumnAssociation ass8 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_ORIGINE, " ");
		col8.setAssociations(ass8);
		_colonnesMap.takeValueForKey(col8, COL_ORIGINE);

		CktlAjaxTableViewColumn col9 = new CktlAjaxTableViewColumn();
		col9.setLibelle("Motif rejet");
		col9.setOrderKeyPath(COL_MOTIF_REJET);
		//CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(STR_KEY+COL_SIRET_KEY, " ");
		CktlAjaxTableViewColumnAssociation ass9 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_MOTIF_REJET, " ");
		col9.setAssociations(ass9);
		_colonnesMap.takeValueForKey(col9, COL_MOTIF_REJET);

		//		CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
		//		col5.setLibelle("Test");
		//		col5.setOrderKeyPath(COL_DATE_KEY);
		//		CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_DATE_KEY, "emptyValue");
		//		ass5.setDateFormat("%d/%m/%Y");
		//		//ass5.setFormatter("dateFormatter");
		//		col5.setAssociations(ass5);
		//		_colonnesMap.takeValueForKey(col5, COL_DATE_KEY);

	}

	//	public NSTimestampFormatter dateFormatter() {
	//		return new NSTimestampFormatter("%d/%m/%Y");
	//	}

	/** Tableau contenant les clés identiofiant les colonnes à afficher par défaut. */
	public static NSArray<String> DEFAULT_COLONNES_KEYS = new NSArray<String>(new String[] {
			COL_EXE_EXERCICE, COL_ORIGINE, COL_BRO_NUMERO, COL_BRO_LIBELLE, COL_PERS_CREATION, COL_DATE_CREATION, COL_BRO_ETAT, COL_MOTIF_REJET
	});

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		if (colonnes == null) {
			NSMutableArray<CktlAjaxTableViewColumn> res = new NSMutableArray<CktlAjaxTableViewColumn>();
			NSArray<String> colkeys = getColonnesKeys();
			for (int i = 0; i < colkeys.count(); i++) {
				res.addObject((CktlAjaxTableViewColumn) _colonnesMap.valueForKey(colkeys.objectAtIndex(i)));
			}
			colonnes = res.immutableClone();
		}
		return colonnes;
	}

	public NSArray<String> getColonnesKeys() {
		NSArray<String> keys = DEFAULT_COLONNES_KEYS;
		if (hasBinding(BINDING_colonnesKeys)) {
			String keysStr = (String) valueForBinding(BINDING_colonnesKeys);
			keys = NSArray.componentsSeparatedByString(keysStr, ",");
		}
		return keys;
	}

}
