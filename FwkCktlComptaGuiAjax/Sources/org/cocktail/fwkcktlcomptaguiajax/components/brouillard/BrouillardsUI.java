package org.cocktail.fwkcktlcomptaguiajax.components.brouillard;

import org.cocktail.fwkcktlcompta.server.metier.EOBrouillard;
import org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail;
import org.cocktail.fwkcktlcomptaguiajax.components.AComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;

/**
 * Gère l'affichage d'une liste de brouillards et de leurs détails.
 * 
 * @binding dgBrouillardList Obligatoire, doit être rempli avec des objets EOBrouillard.
 * @binding dgBrouillardDetailList Facultatif
 * @binding selectedBrouillard Le brouillard sélectionné.
 * @binding selectedBrouillardDetail La ligne de brouillard sélectionnée
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class BrouillardsUI extends AComponent {

	private static final long serialVersionUID = -171558495654926710L;
	public static final String BINDING_dgBrouillardDetailList = "dgBrouillardDetailList";
	public static final String BINDING_dgBrouillardList = "dgBrouillardList";
	public static final String BINDING_selectedBrouillard = "selectedBrouillard";
	public static final String BINDING_selectedBrouillardDetail = "selectedBrouillardDetail";

	//public static final String BINDING_brouillardListCtrl = "dgBrouillardList";
	private EOBrouillard selectedBrouillard;
	private EOBrouillardDetail selectedBrouillardDetail;
	private final DgBrouillardListDelegate dgBrouillardListDelegate = new DgBrouillardListDelegate();
	private final DgBrouillardDetailListDelegate dgBrouillardDetailListDelegate = new DgBrouillardDetailListDelegate();

	private ERXDisplayGroup dgBrouillardDetailList = new ERXDisplayGroup();

	public BrouillardsUI(WOContext arg0) {
		super(arg0);
	}

	public String getBrouillardListId() {
		return getComponentId() + "_BrouillardListId";
	}

	public String getBrouillardDetailListId() {
		return getComponentId() + "_BrouillardDetailListId";
	}

	public String getBrouillardDetailListContainerId() {
		return getComponentId() + "_BrouillardDetailListContainerId";
	}

	public ERXDisplayGroup dgBrouillardDetailList() {
		ERXDisplayGroup dg = (ERXDisplayGroup) valueForBinding(BINDING_dgBrouillardDetailList);
		if (dg == null) {
			dg = dgBrouillardDetailList;
		}
		if (dg.delegate() == null) {
			dg.setDelegate(dgBrouillardDetailListDelegate);
		}
		return dg;
	}

	public ERXDisplayGroup dgBrouillardList() {
		ERXDisplayGroup dg = (ERXDisplayGroup) valueForBinding(BINDING_dgBrouillardList);
		if (dg.delegate() == null) {
			dg.setDelegate(dgBrouillardListDelegate);
		}
		return dg;
	}

	public EOBrouillard getSelectedBrouillard() {
		return selectedBrouillard;
	}

	public void setSelectedBrouillard(EOBrouillard selectedBrouillard) {
		this.selectedBrouillard = selectedBrouillard;
		if (hasBinding(BINDING_selectedBrouillard)) {
			setValueForBinding(selectedBrouillard, BINDING_selectedBrouillard);
		}
		updateBrouillardDetails();
	}

	public EOBrouillardDetail getSelectedBrouillardDetail() {
		return selectedBrouillardDetail;
	}

	public void setSelectedBrouillardDetail(EOBrouillardDetail selectedBrouillardDetail) {
		this.selectedBrouillardDetail = selectedBrouillardDetail;
		if (hasBinding(BINDING_selectedBrouillardDetail)) {
			setValueForBinding(selectedBrouillardDetail, BINDING_selectedBrouillardDetail);
		}
	}

	private void updateBrouillardDetails() {
		if (dgBrouillardDetailList() != null) {
			dgBrouillardDetailList().setObjectArray(NSArray.emptyArray());
			if (getSelectedBrouillard() != null) {
				NSArray res = EOSortOrdering.sortedArrayUsingKeyOrderArray(getSelectedBrouillard().toBrouillardDetails(), new NSArray(new Object[] {
						EOBrouillardDetail.SORT_BROD_INDEX_ASC
				}));

				dgBrouillardDetailList().setObjectArray(res);
			}
		}
	}

	public class DgBrouillardListDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedBrouillard((EOBrouillard) group.selectedObject());
		}
	}

	public class DgBrouillardDetailListDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedBrouillardDetail((EOBrouillardDetail) group.selectedObject());
		}
	}

}
