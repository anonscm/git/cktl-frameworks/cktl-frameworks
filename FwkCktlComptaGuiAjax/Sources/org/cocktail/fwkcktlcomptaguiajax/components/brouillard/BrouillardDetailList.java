package org.cocktail.fwkcktlcomptaguiajax.components.brouillard;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlcompta.server.metier.EOBrouillardDetail;
import org.cocktail.fwkcktlcompta.server.metier.EOGestion;
import org.cocktail.fwkcktlcomptaguiajax.components.AComponent;
import org.cocktail.fwkcktlcomptaguiajax.controllers.brouillard.BrouillardDetailListCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Affiche une liste de details de brouillards (au sens comptable)
 * 
 * @binding ctrl De la classe BrouillardDetailListCtrl
 * @binding colonnesKeys
 * @binding dg Obligatoire. DisplayGroup contenant les données.
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class BrouillardDetailList extends AComponent {
	private static final long serialVersionUID = -5709539389870060915L;
	private static final String OBJ_KEY = "unObjet.";
	private static final String COL_BROD_INDEX = EOBrouillardDetail.BROD_INDEX_KEY;
	private static final String COL_GES_CODE = EOBrouillardDetail.TO_GESTION_KEY + "." + EOGestion.GES_CODE_KEY;
	private static final String COL_BROD_PCO_NUM = EOBrouillardDetail.BROD_PCO_NUM_KEY;
	private static final String COL_BROD_PCO_LIBELLE = EOBrouillardDetail.BROD_PCO_LIBELLE_KEY;
	private static final String COL_BROD_DEBIT = EOBrouillardDetail.BROD_DEBIT_KEY;
	private static final String COL_BROD_CREDIT = EOBrouillardDetail.BROD_CREDIT_KEY;
	private static final String COL_BROD_LIBELLE = EOBrouillardDetail.BROD_LIBELLE_KEY;

	private static final String BINDING_ctrl = "ctrl";
	/** Bindings pour les colonnes a afficher, {@link BrouillardDetailList#DEFAULT_COLONNES_KEYS} */
	public static final String BINDING_colonnesKeys = "colonnesKeys";

	private NSArray<CktlAjaxTableViewColumn> colonnes;
	public static final NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();

	private BrouillardDetailListCtrl ctrl;

	public EOBrouillardDetail unObjet;

	public BrouillardDetailList(WOContext context) {
		super(context);
	}

	public BrouillardDetailListCtrl getCtrl() {
		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (BrouillardDetailListCtrl) valueForBinding(BINDING_ctrl);
			}
			if (ctrl == null) {
				ctrl = new BrouillardDetailListCtrl(this);
			}
		}
		if (ctrl.getMyComponent() == null) {
			ctrl.setMyComponent(this);
		}
		return ctrl;
	}

	static {

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("#");
		col1.setOrderKeyPath(COL_BROD_INDEX);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_BROD_INDEX, " ");
		col1.setAssociations(ass1);
		_colonnesMap.takeValueForKey(col1, COL_BROD_INDEX);

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Gestion");
		col2.setOrderKeyPath(COL_GES_CODE);
		//CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(IND_KEY+COL_NOM_KEY, " ");
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_GES_CODE, " ");
		col2.setAssociations(ass2);
		_colonnesMap.takeValueForKey(col2, COL_GES_CODE);

		CktlAjaxTableViewColumn col4 = new CktlAjaxTableViewColumn();
		col4.setLibelle("Compte");
		//col4.setComponent(FouValideColonne.class.getName());
		col4.setOrderKeyPath(COL_BROD_PCO_NUM);
		CktlAjaxTableViewColumnAssociation ass4 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_BROD_PCO_NUM, null);
		col4.setAssociations(ass4);
		_colonnesMap.takeValueForKey(col4, COL_BROD_PCO_NUM);

		CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
		col5.setLibelle("Libellé compte");
		col5.setOrderKeyPath(COL_BROD_PCO_LIBELLE);
		CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_BROD_PCO_LIBELLE, " ");
		col5.setAssociations(ass5);
		_colonnesMap.takeValueForKey(col5, COL_BROD_PCO_LIBELLE);

		CktlAjaxTableViewColumn col7 = new CktlAjaxTableViewColumn();
		col7.setLibelle("Libellé");
		col7.setOrderKeyPath(COL_BROD_LIBELLE);
		CktlAjaxTableViewColumnAssociation ass7 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_BROD_LIBELLE, " ");
		col7.setAssociations(ass7);
		_colonnesMap.takeValueForKey(col7, COL_BROD_LIBELLE);

		CktlAjaxTableViewColumn col8 = new CktlAjaxTableViewColumn();
		col8.setLibelle("Débit");
		col8.setOrderKeyPath(COL_BROD_DEBIT);
		CktlAjaxTableViewColumnAssociation ass8 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_BROD_DEBIT, " ");
		col8.setAssociations(ass8);
		col8.setRowCssClass("alignToRight");
		ass8.setNumberformat("0.00");
		_colonnesMap.takeValueForKey(col8, COL_BROD_DEBIT);

		CktlAjaxTableViewColumn col9 = new CktlAjaxTableViewColumn();
		col9.setLibelle("Crédit");
		col9.setOrderKeyPath(COL_BROD_CREDIT);
		CktlAjaxTableViewColumnAssociation ass9 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + COL_BROD_CREDIT, " ");
		col9.setAssociations(ass9);
		col9.setRowCssClass("alignToRight");
		ass9.setNumberformat("0.00");
		_colonnesMap.takeValueForKey(col9, COL_BROD_CREDIT);

		//		CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
		//		col5.setLibelle("Test");
		//		col5.setOrderKeyPath(COL_DATE_KEY);
		//		CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_DATE_KEY, "emptyValue");
		//		ass5.setDateFormat("%d/%m/%Y");
		//		//ass5.setFormatter("dateFormatter");
		//		col5.setAssociations(ass5);
		//		_colonnesMap.takeValueForKey(col5, COL_DATE_KEY);

	}

	//	public NSTimestampFormatter dateFormatter() {
	//		return new NSTimestampFormatter("%d/%m/%Y");
	//	}

	/** Tableau contenant les clés identifiant les colonnes à afficher par défaut. */
	public static NSArray<String> DEFAULT_COLONNES_KEYS = new NSArray<String>(new String[] {
			COL_BROD_INDEX, COL_GES_CODE, COL_BROD_PCO_NUM, COL_BROD_PCO_LIBELLE, COL_BROD_DEBIT, COL_BROD_CREDIT
	});

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		if (colonnes == null) {
			NSMutableArray<CktlAjaxTableViewColumn> res = new NSMutableArray<CktlAjaxTableViewColumn>();
			NSArray<String> colkeys = getColonnesKeys();
			for (int i = 0; i < colkeys.count(); i++) {
				res.addObject((CktlAjaxTableViewColumn) _colonnesMap.valueForKey(colkeys.objectAtIndex(i)));
			}
			colonnes = res.immutableClone();
		}
		return colonnes;
	}

	public NSArray<String> getColonnesKeys() {
		NSArray<String> keys = DEFAULT_COLONNES_KEYS;
		if (hasBinding(BINDING_colonnesKeys)) {
			String keysStr = (String) valueForBinding(BINDING_colonnesKeys);
			keys = NSArray.componentsSeparatedByString(keysStr, ",");
		}
		return keys;
	}

}
