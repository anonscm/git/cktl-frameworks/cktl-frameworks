/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiajax.components.sepasddmandat;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.entities.IGrhumAdresse;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumRib;
import org.cocktail.fwkcktlcompta.common.finders.FinderGrhumRib;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddParam;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddParamHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.rules.SepaSddMandatRule;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcompta.server.metier.EOGrhumAdresse;
import org.cocktail.fwkcktlcompta.server.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcomptaguiajax.components.AComponent;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.joda.time.LocalDate;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class SepaSddMandatSaisie extends AComponent {

	/** Serial version ID. */
	private static final long serialVersionUID = 1L;

	public static final String BINDING_SEPA_SDD_MANDAT = "sepaSddMandat";
	public static final String BINDING_ENREGISTRER_CALLBACK = "enregistrerCallback";

	private static final DateConversionUtil DATE_CONVERSION = DateConversionUtil.sharedInstance();

	/** Element ID. */
	private static final String DEBITEUR_CONTAINER_ID = "containerDebiteurPrsId";
	private static final String DEBITEUR_ADRESSE_CONTAINER_ID = "containerDebiteurAdresseId";
	private static final String DEBITEUR_LISTE_RIBS_CONTAINER_ID = "containerDebiteurListeRibsId";

	/** Referentiels. */
	private List<ISepaSddParam> lesSepaSddParams;
	private ISepaSddParam unSepaSddParam;
	private List<IGrhumRib> lesRibsDebiteur;
	private IGrhumRib unRibDebiteur;

	/** Formulaire bean (en plus du mandat binde). */
	private IPersonne debiteurPersonne;
	private IPersonne tiersDebiteurPersonne;
	private EORepartPersonneAdresse debiteurRepartAdresse;
	private NSTimestamp dateCreation;
	private NSTimestamp dateSignature;


    public SepaSddMandatSaisie(WOContext context) {
        super(context);
        init();
    }

    @Override
	public void _awakeInContext(WOContext arg0) {
		super._awakeInContext(arg0);
		lazyInit();
	}

	public ISepaSddMandat sepaSddMandat() {
    	return (ISepaSddMandat) valueForBinding(BINDING_SEPA_SDD_MANDAT);
	}

    protected void init() {
    	this.lesSepaSddParams = null;
    	this.lesRibsDebiteur = Collections.emptyList();
    }

    protected void lazyInit() {
    	if (lesSepaSddParams == null) {
    		lesSepaSddParams = loadSepaSddParams();
    	}
    	
    	if (dateCreation == null) {
	    	LocalDate dMandatCreation = DATE_CONVERSION.parseDateSilent(sepaSddMandat().dMandatCreation());
	    	if (dMandatCreation != null) {
	    		this.dateCreation = new NSTimestamp(dMandatCreation.toDate().getTime());
	    	}
    	}
    }

    public boolean isMandatSigne() {
    	ISepaSddMandat mandat = sepaSddMandat();

    	return (mandat != null && mandat.dMandatSignature() != null);
    }

	public List<ISepaSddParam> getLesSepaSddParams() {
		return lesSepaSddParams;
	}

	public ISepaSddParam getUnSepaSddParam() {
		return unSepaSddParam;
	}

	public void setUnSepaSddParam(ISepaSddParam unSepaSddparam) {
		this.unSepaSddParam = unSepaSddparam;
	}

	public WOActionResults onEnregistrer() {
		try {
			ISepaSddMandat mandat = sepaSddMandat();
			EOGrhumPersonne currentUser = EOGrhumPersonne.fetchByKeyValue(edc(), EOGrhumPersonne.PERS_ID_KEY, getUtilisateurPersId());
			mandat.setToModificateurRelationship(currentUser);

			bindSubmittedValuesToMandat(mandat);

			SepaSddMandatHelper.getSharedInstance().prepareBeforeSave(mandat);
			SepaSddMandatRule.getSharedInstance().validateSepaSddMandat(mandat);
			
			return (WOActionResults) valueForBinding(BINDING_ENREGISTRER_CALLBACK);
			
		} catch (NSValidation.ValidationException ve) {
		    ve.printStackTrace();
			mySession().addSimpleErrorMessage(ve.getMessage());
		} catch (Exception e) {
		    e.printStackTrace();
			mySession().addSimpleErrorMessage(e);
		}

		return null;
	}

	protected void bindSubmittedValuesToMandat(ISepaSddMandat mandat) {
		bindSubmittedDebiteur(mandat);
		bindSubmittedDebiteurAdresse(mandat);
		bindSubmittedTiersDebiteur(mandat);
		bindSubmittedDateCreation(mandat);
		bindSubmittedDateSignature(mandat);
	}

	protected void bindSubmittedDebiteur(ISepaSddMandat mandat) {
		if (getDebiteurPersonne() == null) {
			return;
		}

		IGrhumPersonne debiteur = EOGrhumPersonne.fetchByKeyValue(edc(), EOGrhumPersonne.PERS_ID_KEY, getDebiteurPersonne().persId());
		mandat.setToDebiteurPersonneRelationship(debiteur);
	}

	protected void bindSubmittedDebiteurAdresse(ISepaSddMandat mandat) {
		if (getDebiteurRepartAdresse() == null) {
			return;
		}

		IGrhumAdresse adresse = EOGrhumAdresse.fetchByKeyValue(edc(), EOGrhumAdresse.ADR_ORDRE_KEY, getDebiteurRepartAdresse().toAdresse().adrOrdre());
		mandat.setToDebiteurAdresseRelationship(adresse);
	}

	protected void bindSubmittedTiersDebiteur(ISepaSddMandat mandat) {
		if (getTiersDebiteurPersonne() == null) {
			return;
		}

		IGrhumPersonne tiersDebiteur = EOGrhumPersonne.fetchByKeyValue(edc(), EOGrhumPersonne.PERS_ID_KEY, getTiersDebiteurPersonne().persId());
		mandat.setToTiersDebiteurPersonneRelationship(tiersDebiteur);
	}

	protected void bindSubmittedDateCreation(ISepaSddMandat mandat) {
		if (getDateCreation() == null) {
			return;
		}

		String dateCreationAsString = DATE_CONVERSION.formatDateWithoutTimeISO(getDateCreation().getTime());
		mandat.setDMandatCreation(dateCreationAsString);
	}

	protected void bindSubmittedDateSignature(ISepaSddMandat mandat) {
		if (getDateSignature() == null) {
			return;
		}

		String dateSignatureAsString = DATE_CONVERSION.formatDateWithoutTimeISO(getDateSignature().getTime());
		mandat.setDMandatSignature(dateSignatureAsString);
	}

	protected List<ISepaSddParam> loadSepaSddParams() {
		List<ISepaSddParam> params = Collections.emptyList();
		try {
			params = SepaSddParamHelper.getSharedInstance().getAllParamsValides(edc());
		} catch (Exception e) {
			// logger
			System.err.println(e);
		}
		return params;
	}

	@SuppressWarnings("unchecked")
	protected List<IGrhumRib> loadDebiteurRibs() {
		if (getDebiteurPersonne() == null) {
			return Collections.emptyList();
		}

		List<IGrhumRib> ribs = Collections.emptyList();
		try {
			EOGrhumPersonne debiteur = EOGrhumPersonne.fetchByKeyValue(edc(), EOGrhumPersonne.PERS_ID_KEY, getDebiteurPersonne().persId());
			ribs = (List<IGrhumRib>) FinderGrhumRib.fetchRibsValidesPourPersonne(edc(), debiteur);
		} catch (Exception e) {
			// logger
			System.err.println(e);
		}

		return ribs;
	}

	public void updateDebiteurRibsCombobox() {
		this.lesRibsDebiteur = loadDebiteurRibs();
	}

	public NSArray<String> updateDebiteurInformationsContainerId() {
		return new NSArray<String>(getDebiteurContainerId(), getDebiteurAdresseContainerId(), getDebiteurListeRibsContainerId());
	}

	public EOQualifier qualifierForIndividus() {
		EOQualifier qualFournisseurValide = ERXQ.notEquals(EOIndividu.TO_FOURNISS.dot(EOFournis.FOU_VALIDE).key(), EOFournis.FOU_VALIDE_ANNULE);
		EOQualifier qualFournisseurNonTypeFournisseur = ERXQ.notEquals(EOIndividu.TO_FOURNISS.dot(EOFournis.FOU_TYPE).key(), EOFournis.FOU_TYPE_FOURNISSEUR);

		return ERXQ.and(qualFournisseurValide, qualFournisseurNonTypeFournisseur);
	}

	public boolean debiteurADesRibs() {
		return lesRibsDebiteur.isEmpty();
	}

	public String getDebiteurContainerId() {
		return DEBITEUR_CONTAINER_ID;
	}

	public String getDebiteurAdresseContainerId() {
		return DEBITEUR_ADRESSE_CONTAINER_ID;
	}

	public String getDebiteurListeRibsContainerId() {
		return DEBITEUR_LISTE_RIBS_CONTAINER_ID;
	}

	public IPersonne getDebiteurPersonne() {
		return debiteurPersonne;
	}

	public void setDebiteurPersonne(IPersonne debiteurPersonne) {
		this.debiteurPersonne = debiteurPersonne;
	}

	public IPersonne getTiersDebiteurPersonne() {
		return tiersDebiteurPersonne;
	}

	public void setTiersDebiteurPersonne(IPersonne tiersDebiteurPersonne) {
		this.tiersDebiteurPersonne = tiersDebiteurPersonne;
	}

	public EORepartPersonneAdresse getDebiteurRepartAdresse() {
		return debiteurRepartAdresse;
	}

	public void setDebiteurRepartAdresse(EORepartPersonneAdresse debiteurRepartAdresse) {
		this.debiteurRepartAdresse = debiteurRepartAdresse;
	}

	public NSTimestamp getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(NSTimestamp dateCreation) {
		this.dateCreation = dateCreation;
	}

	public NSTimestamp getDateSignature() {
		return dateSignature;
	}

	public void setDateSignature(NSTimestamp dateSignature) {
		this.dateSignature = dateSignature;
	}

	public List<IGrhumRib> getLesRibsDebiteur() {
		return lesRibsDebiteur;
	}

	public IGrhumRib getUnRibDebiteur() {
		return unRibDebiteur;
	}

	public void setUnRibDebiteur(IGrhumRib unRibDebiteur) {
		this.unRibDebiteur = unRibDebiteur;
	}
}
