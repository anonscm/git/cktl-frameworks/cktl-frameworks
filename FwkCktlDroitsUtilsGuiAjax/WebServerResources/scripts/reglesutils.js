function isInnerMost(event) {
	var target = event.target || event.srcElement;
	return target == event.currentTarget;
}

function selectRegleSimple(div) {
	$$('.regleSelected').each(function(item) { item.removeClassName('regleSelected'); });
	$(div).addClassName('regleSelected');
}

function selectRegleComplexe(div) {
	$$('.regleSelected').each(function(item) { item.removeClassName('regleSelected'); });
	$(div).addClassName('regleSelected');
}
