/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldroitsutilsguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;

/**
 * Composant d'administration des droits lies à un profil donne.
 * Il filtre les droits en fonction du domaine et de l'application selectionnes.
 * Il affiche ensuite les listing des 3 types de droits :
 * <ul>
 * <li>Droits sur les fonctions {@link DroitFonctionForm}</li>
 * <li>Droits sur les perimètres de donnees {@link DroitDonneeForm}</li>
 * <li>Droits sur les donnees statiques {@link DroitDonneeStatiqueForm}</li>
 * </ul>
 * binding profil le profil dont on veut editer les droits
 * binding utilisateurPersId l'utilisateur editant
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class DroitAdminUI extends DroitsUtilsComponent {

	private static final long serialVersionUID = 1581227658407361467L;
	public static final String BINDING_PROFIL = "profil";
	private EOGdDomaine currentDomaine;
	private EOGdDomaine selectedDomaine;
	private EOGdApplication currentApplication;
	private EOGdApplication selectedApplication;

	private static final String FONCTION_PERIMETRE = "cocktail.feature.perimetre";
	
	/**
	 * @param context :contexte d'edition
	 */
	public DroitAdminUI(WOContext context) {
		super(context);
		setSelectedDomaine(EOGdDomaine.getDomaineAdministratif());
	}

	public EOGdProfil getProfil() {
		return (EOGdProfil) valueForBinding(BINDING_PROFIL);
	}

	public NSArray<EOGdDomaine> getDomaines() {
		return ERXEOControlUtilities.localInstancesOfObjects(getProfil().editingContext(), EOGdDomaine.getDomaines());
	}

	public EOGdDomaine getCurrentDomaine() {
		return currentDomaine;
	}

	public void setCurrentDomaine(EOGdDomaine currentDomaine) {
		this.currentDomaine = currentDomaine;
	}

	public EOGdDomaine getSelectedDomaine() {
		return selectedDomaine;
	}

	/**
	 * met à jour le domaine selectionne et selectionne la première application si elle existe
	 * @param selectedDomaine : domaine selectionne
	 */
	public void setSelectedDomaine(EOGdDomaine selectedDomaine) {
		this.selectedDomaine = selectedDomaine;
		NSArray<EOGdApplication> apps = getSelectedDomaine().toGdApplications();
		if (apps.count() > 0) {
			setSelectedApplication(apps.objectAtIndex(0));
		}
	}

	public EOGdApplication getCurrentApplication() {
		return currentApplication;
	}

	public void setCurrentApplication(EOGdApplication currentApplication) {
		this.currentApplication = currentApplication;
	}

	public EOGdApplication getSelectedApplication() {
		return selectedApplication;
	}

	public void setSelectedApplication(EOGdApplication selectedApplication) {
		this.selectedApplication = selectedApplication;
	}

	/**
	 * @return la vue du perimetre en fonction de l'application
	 */
	public String getPerimetre() {

		if (getSelectedApplication().appStrId().equals("GIROFLE")) {
			return myApp().config().stringForKey("org.cocktail.droitsUtilsGuiAjax.perimetreVue.Girofle.className");
		}
		if (getSelectedApplication().appStrId().equals("SANGRIA")) {
			return myApp().config().stringForKey("org.cocktail.droitsUtilsGuiAjax.perimetreVue.Sangria.className");
		}
		return myApp().config().stringForKey("org.cocktail.droitsUtilsGuiAjax.perimetreVue.Generique.className");
	}	

}