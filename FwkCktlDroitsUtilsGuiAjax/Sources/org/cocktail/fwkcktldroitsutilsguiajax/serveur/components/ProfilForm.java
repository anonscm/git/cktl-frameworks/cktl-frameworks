/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldroitsutilsguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique;
import org.cocktail.fwkcktlpersonne.common.metier.EORegle;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxUtils;
import er.extensions.eof.ERXEOControlUtilities;

/**
 * 
 * Composant d'édition d'un profil.
 * 
 * @binding profil le profil sélectionné à éditer
 * @binding utilisateurPersId le persId de l'utilisateur courant
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class ProfilForm extends DroitsUtilsComponent {

    private static final long serialVersionUID = -5245257209285073444L;
    private static final String BINDING_PROFIL = "profil";
    private boolean isTabInfosSelected;
    private boolean isTabReglesSelected;
    private boolean isTabDroitsSelected;
    private EOGdProfil _profil;
    private EORegle selectedRegle;

    public ProfilForm(WOContext context) {
        super(context);
    }
    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
        EOGdProfil profilTmp = (EOGdProfil) valueForBinding(BINDING_PROFIL);
        if (!ERXEOControlUtilities.eoEquals(_profil, profilTmp)) {
            resetTabs();
        }
        _profil = profilTmp;
        super.appendToResponse(response, context);
        AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlDroitsUtilsGuiAjax", "scripts/reglesutils.js");
    }

    public WOActionResults creerRegle() {
    	
    	EOGroupeDynamique groupeDynamique = EOGroupeDynamique.creerInstance(_profil.editingContext());
		groupeDynamique.setGrpdLc("Groupe dynamique pour profil");
		groupeDynamique.setGrpdDescription("Groupe dynamique pour profil");
		EORegle regleRacine = EORegle.creerRegleSimple(_profil.editingContext(), utilisateurPersId());
		groupeDynamique.setToRegleRelationship(regleRacine);
		_profil.setToGroupeDynamiqueRelationship(groupeDynamique);
    	
        return null;
    }
    
    private void resetTabs() {
        setTabInfosSelected(true);
        setTabDroitsSelected(false);
        setTabReglesSelected(false);
        setSelectedRegle(null);
    }

    public EOGdProfil profil() {
        return _profil;
    }
    
    public EORegle regleRacine() {
        return profil().toGroupeDynamique() != null ? profil().toGroupeDynamique().toRegle() : null;
    }
    
    public String tabsId() {
        return "Tabs_" + componentId();
    }
    
    public String tabInfosId() {
        return "TabInfos_" + componentId();
    }
    
    public String tabReglesId() {
        return "TabRegles_" + componentId();
    }
    
    public String tabDroitsId() {
        return "TabDroits_" + componentId();
    }

    public String reglesContainerId() {
        return "Regles_" + componentId();
    }
    
    public boolean peutEditerProfil() {
        return profil() != null && applicationUser().hasDroitModificationProfil(profil());
    }
    
    public boolean nePeutEditerProfil() {
        return profil() != null && !applicationUser().hasDroitModificationProfil(profil());
    }
    
    public boolean isTabInfosSelected() {
        return isTabInfosSelected;
    }

    public void setTabInfosSelected(boolean isTabInfosSelected) {
        this.isTabInfosSelected = isTabInfosSelected;
    }

    public boolean isTabReglesSelected() {
        return isTabReglesSelected;
    }

    public void setTabReglesSelected(boolean isTabReglesSelected) {
        this.isTabReglesSelected = isTabReglesSelected;
    }

    public boolean isTabDroitsSelected() {
        return isTabDroitsSelected;
    }

    public void setTabDroitsSelected(boolean isTabDroitsSelected) {
        this.isTabDroitsSelected = isTabDroitsSelected;
    }
    
    public EORegle getSelectedRegle() {
        return selectedRegle;
    }
    
    public void setSelectedRegle(EORegle selectedRegle) {
        this.selectedRegle = selectedRegle;
    }
    
}