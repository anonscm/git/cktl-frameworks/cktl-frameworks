/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldroitsutilsguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitFonction;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

/**
 * Affichage et édition des droits sur les fonctions.
 * 
 * @binding application l'application sélectionnée servant à filtrer les droits
 * @binding profil le profil dont on veut éditer les droits
 * @binding utilisateurPersId l'utilisateur éditant les droits
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class DroitFonctionForm extends DroitsUtilsComponent {

    private static final long serialVersionUID = 5623983593332612803L;
    public static final String BINDING_PROFIL = "profil";
    public static final String BINDING_APPLICATION = "application";
    private EOGdProfilDroitFonction currentDroitFonction;
    private EOGdTypeDroitFonction currentTypeDroitFonction;
    private EOGdTypeDroitFonction selectedTypeDroitFonction;
    private EOGdFonction currentFonction;
    private EOGdFonction selectedFonction;
    private EOGdProfilDroitFonction newDroitFonction;
    private EOEditingContext ecForCreation;
    private int indexRep;
    
    public DroitFonctionForm(WOContext context) {
        super(context);
    }
    
    @Override
    public void awake() {
        super.awake();
        // Si pas crée, on crée un ec nested pour rajouter un droit
        if (ecForCreation == null) {
            ecForCreation = ERXEC.newEditingContext(profil().editingContext());
        }

    }
    
    public WOActionResults creerDroit() {
        try {
        	newDroitFonction = EOGdProfilDroitFonction.creer(edc(), utilisateurPersId());
        	newDroitFonction.setToGdFonctionRelationship(getSelectedFonction());
        	newDroitFonction.setToGdTypeDroitFonctionRelationship(getSelectedTypeDroitFonction());
            newDroitFonction.setToGdProfilRelationship(profil().localInstanceIn(edc()));
            profil().setDateModification(new NSTimestamp());
            profil().setPersIdModification(utilisateurPersId());
            edc().saveChanges();
            // si ok on met à null pour une prochaine création
        } catch (ValidationException e) {
            LOG.warn(e.getMessage(), e);
            ecForCreation.revert();
            session().addSimpleErrorMessage(e.getMessage(), "");
        } catch (Exception e) {
            throw new NSForwardException(e);
        }
        newDroitFonction = null;
        return null;
    }
    
    public WOActionResults supprimerDroit() {
        profil().removeFromToGdProfilDroitFonctionsRelationship(currentDroitFonction);
        profil().editingContext().deleteObject(currentDroitFonction);
        return null;
    }
    
    public String cssDroit() {
        String css = isHerite() ? "droitHerite " : "";
        css = indexRep % 2 == 0 ? css + "even" : css + "odd";
        return css;
    }
    
    public String droitHeriteExp() {
        return "Hérité de " + getCurrentDroitFonction().toGdProfil().prLc();
    }
    
    public boolean isHerite() {
        return !profil().equals(getCurrentDroitFonction().toGdProfil());
    }
    
    public EOGdProfil profil() {
        return (EOGdProfil)valueForBinding(BINDING_PROFIL);
    }
    
    public EOGdApplication gdApplication() {
        return (EOGdApplication)valueForBinding(BINDING_APPLICATION);
    }
    
    public NSArray<EOGdTypeDroitFonction> getTypesDroitFonction() {
        return ERXEOControlUtilities.localInstancesOfObjects(profil().editingContext(), EOGdTypeDroitFonction.getTypesDroitFonction());
    }
    
    public NSArray<EOGdTypeDroitFonction> getTypesDroitFonctionForNew() {
        return ERXEOControlUtilities.localInstancesOfObjects(ecForCreation, EOGdTypeDroitFonction.getTypesDroitFonction());
    }
    
    private EOQualifier qualApplication() {
        return ERXQ.equals(EOGdFonction.TO_GD_APPLICATION_KEY, gdApplication().localInstanceIn(ecForCreation));
    }
    
    public NSArray<EOGdFonction> getFonctionsForNew() {
        // On filtre selon le domaine
        return ERXQ.filtered(
                ERXEOControlUtilities.localInstancesOfObjects(ecForCreation, EOGdFonction.getFonctions()),
                qualApplication());
    }
    
    public NSArray<EOGdProfilDroitFonction> getAllDroitFonctions() {
        return profil().allDroitsFonction(gdApplication());
    }
    
    public EOGdProfilDroitFonction getCurrentDroitFonction() {
        return currentDroitFonction;
    }
    
    public void setCurrentDroitFonction(EOGdProfilDroitFonction currentDroitFonction) {
        this.currentDroitFonction = currentDroitFonction;
    }
    
    public EOGdTypeDroitFonction getCurrentTypeDroitFonction() {
        return currentTypeDroitFonction;
    }
    
    public void setCurrentTypeDroitFonction(EOGdTypeDroitFonction currentTypeDroitFonction) {
        this.currentTypeDroitFonction = currentTypeDroitFonction;
    }
    
    public EOGdFonction getCurrentFonction() {
        return currentFonction;
    }
    
    public void setCurrentFonction(EOGdFonction currentFonction) {
        this.currentFonction = currentFonction;
    }
    
    public EOGdProfilDroitFonction getNewDroitFonction() {
        return newDroitFonction;
    }
    
    public void setNewDroitFonction(EOGdProfilDroitFonction newDroitFonction) {
        this.newDroitFonction = newDroitFonction;
    }
    
    public int getIndexRep() {
        return indexRep;
    }
    
    public void setIndexRep(int indexRep) {
        this.indexRep = indexRep;
    }

	public EOGdTypeDroitFonction getSelectedTypeDroitFonction() {
	    return selectedTypeDroitFonction;
    }

	public void setSelectedTypeDroitFonction(EOGdTypeDroitFonction selectedTypeDroitFonction) {
	    this.selectedTypeDroitFonction = selectedTypeDroitFonction;
    }

	public EOGdFonction getSelectedFonction() {
	    return selectedFonction;
    }

	public void setSelectedFonction(EOGdFonction selectedFonction) {
	    this.selectedFonction = selectedFonction;
    }
}