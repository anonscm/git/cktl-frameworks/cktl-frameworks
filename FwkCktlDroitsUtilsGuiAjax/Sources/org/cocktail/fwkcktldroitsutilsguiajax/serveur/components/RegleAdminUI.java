/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldroitsutilsguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORegle;
import org.cocktail.fwkcktlpersonne.common.metier.EORegleNode;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.droits.DroitsHelper;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSRange;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEOControlUtilities;

/**
 * 
 * Composant d'édition d'un règle racine.
 * 
 * @binding regleRacine la règle racine à éditer
 * @binding selectedRegle "willSet" la règle sélectionnée
 * @binding utilisateurPersId le persId de l'utilisateur faisant la modif
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class RegleAdminUI extends DroitsUtilsComponent {

    private static final long serialVersionUID = 1181395190073339148L;
    private static final String BINDING_SELECTED_REGLE = "selectedRegle";
    private static final String BINDING_REGLE_RACINE = "regleRacine";
    private static final Integer MAX_PERSIDS = 100;
    private ERXDisplayGroup persCalculeesDg;
    
    public RegleAdminUI(WOContext context) {
        super(context);
    }
    
    public String detailContainerId() {
        return "Detail_" + componentId();
    }
    
    public WOActionResults ajouterRegleAnd() {
        setSelectedRegle(getSelectedRegle().andRegleSimple(utilisateurPersId()));
        return null;
    }
    
    public WOActionResults ajouterRegleOr() {
        setSelectedRegle(getSelectedRegle().orRegleSimple(utilisateurPersId()));
        return null;
    }
    
    public WOActionResults ajouterRegleNot() {
        setSelectedRegle(getSelectedRegle().not(utilisateurPersId()));
        return null;
    }
    
    public WOActionResults supprimerRegle() {
        EORegle.supprimerRegle(getSelectedRegle());
        setSelectedRegle(null);
        return null;
    }
    
    public WOActionResults calculer() {
        persCalculeesDg = new ERXDisplayGroup();
        persCalculeesDg.setNumberOfObjectsPerBatch(20);
        EORegle regleRacine = (EORegle)valueForBinding(BINDING_REGLE_RACINE);
        try {
            NSArray<Integer> persIds = regleRacine.groupeDynamique().calculerPersIds();
            if (persIds.count() > MAX_PERSIDS) {
                session().addSimpleInfoMessage("Cocolight", 
                        "Le calcul de cette règle remonte " + persIds.count() +
                        " personnes, seules les " + MAX_PERSIDS + " premières sont affichées.");
                persIds = persIds.subarrayWithRange(new NSRange(0, MAX_PERSIDS));
            }
            NSArray<? extends IPersonne> personnes = DroitsHelper.personnesForPersIds(edc(), persIds);
            persCalculeesDg.setObjectArray(personnes);
        } catch (ValidationException e) {
            session().addSimpleErrorMessage("Cocolight", e);
        }
        return null;
    }
    
    public boolean isRegleNotSelected() {
        return getSelectedRegle() == null;
    }
    
    public EORegle getSelectedRegle() {
        return (EORegle)valueForBinding(BINDING_SELECTED_REGLE);
    }
    
    public void setSelectedRegle(EORegle selectedRegle) {
        setValueForBinding(selectedRegle, BINDING_SELECTED_REGLE);
    }
    
    public EOEditingContext edc() {
        return getSelectedRegle().editingContext();
    }
    
    private String labelForGroupe(String value) {
        if (value != null) {
            EOStructure structure = 
                (EOStructure)ERXEOControlUtilities.objectWithPrimaryKeyValue(
                        edc(), EOStructure.ENTITY_NAME, value, null);
            return structure.libelle();
        } else {
            return "-";
        }
    }
    
    private String labelForAssociation(String value) {
        if (value != null) {
            EOAssociation association =
                (EOAssociation)ERXEOControlUtilities.objectWithPrimaryKeyValue(
                        edc(), EOAssociation.ENTITY_NAME, Integer.valueOf(value), null);
            return association.assLibelle();
        } else {
            return "-";
        }
    }
    
    private String labelForNode(String rnNode) {
        return mySession().localizer().localizedStringForKey("node." + rnNode.toLowerCase());
    }
    
    public String labelForNodeAnd() {
        return labelForNode(EORegleNode.STR_ID_AND);
    }
    
    public String labelForNodeOr() {
        return labelForNode(EORegleNode.STR_ID_OR);
    }
    
    public String labelForNodeNot() {
        return labelForNode(EORegleNode.STR_ID_NOT);
    }
    
    private String labelForRegle(EORegle regle) {
        if (regle.isRegleSimple()) {
            if (regle.hasOperateurRoleDansGroupe()) {
                return regle.toRegleKey().rkLc() + " a pour rôle " + labelForAssociation(regle.rValue()) + 
                       " dans le groupe " + labelForGroupe(regle.rValue2());
            } else if (regle.hasOperateurMembre()){
                return regle.toRegleKey().rkLc() + " est membre de " + labelForGroupe(regle.rValue());
            } else if (regle.hasOperateurRole()) {
                return regle.toRegleKey().rkLc() + " a pour rôle " + labelForAssociation(regle.rValue());
            } else
                return "[Operateur Inconnu]";
        } else {
            if (regle.isRegleNot()) {
                return regle.toRegleNode().rnNode() + "(" + labelForRegle(regle.toReglesEnfantsOrdered().lastObject()) +")";
            }
            NSArray<EORegle> fils = regle.toReglesEnfantsOrdered();
            StringBuilder label = new StringBuilder();
            for (int i = 0; i < fils.count(); i++) {
                if (i > 0)
                    label.append(" " + labelForNode(regle.toRegleNode().rnNode()) + " ");
                label.append("(" + labelForRegle(fils.objectAtIndex(i)) + ")");
            }
            return label.toString();
        }
    }
    
    public String labelForSelectedRegle() {
        return labelForRegle(getSelectedRegle());
    }
    
    public ERXDisplayGroup getPersCalculeesDg() {
        return persCalculeesDg;
    }
    
}