/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldroitsutilsguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORegle;
import org.cocktail.fwkcktlpersonne.common.metier.EORegleKey;
import org.cocktail.fwkcktlpersonne.common.metier.EORegleNode;
import org.cocktail.fwkcktlpersonne.common.metier.EORegleOperateur;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;

/**
 * Composant d'édition des caractéristiques d'une règle.
 * Si c'est une règle simple, on affiche les champs d'édition de la clef, opérateur, valeur(s) de la règle.
 * Si c'est une règle complexe (avec règles filles), on affiche la node (AND, OR, NOT) et
 * on utilise ce même composant sur les règles filles. (Récursion).
 * @binding regle la règle
 * @binding selectedRegle "willSet" de la règle sélectionnée par l'utilisateur
 * @binding updateContainerID id du container à rafraichir après sélection d'une règle
 * @binding utilisateurPersId persId de l'utilisateur éditant la règle
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class RegleForm extends DroitsUtilsComponent {

	private static final long serialVersionUID = 5961210758234996930L;
	public static final String BINDING_REGLE = "regle";
	public static final String BINDING_SELECTED_REGLE = "selectedRegle";
	public static final String BINDING_UPDATE_CONTAINER_ID = "updateContainerID";

	private EORegle currentRegle;
	private EORegleKey currentRegleKey;
	private EORegleOperateur currentRegleOp;
	private EORegleNode currentRegleNode;

	public RegleForm(WOContext context) {
		super(context);
	}

	public EOEditingContext ec() {
		return regle().editingContext();
	}

	public WOActionResults selectionnerRegle() {
		setSelectedRegle(regle());
		return null;
	}

	public void setSelectedRegle(EORegle regle) {
		setValueForBinding(regle, BINDING_SELECTED_REGLE);
	}

	public EORegle selectedRegle() {
		return (EORegle) valueForBinding(BINDING_SELECTED_REGLE);
	}

	public EORegle regle() {
		return (EORegle) valueForBinding(BINDING_REGLE);
	}

	public String updateContainerID() {
		return stringValueForBinding(BINDING_UPDATE_CONTAINER_ID, null);
	}

	public String containerId() {
		return "RegleForm_" + componentId();
	}

	public String regleSimpleId() {
		return "RegleSimple_" + componentId();
	}

	public String regleComplexeId() {
		return "RegleComplexe_" + componentId();
	}

	public String associationSelectId() {
		return "AssociationSelect_" + componentId();
	}

	public String groupeSelectId() {
		return "GroupeSelect_" + componentId();
	}

	public String onSuccessRegleSimpleJs() {
		return "function() { selectRegleSimple('" + regleSimpleId() + "'); }";
	}

	public String onSuccessRegleComplexeJs() {
		return "function() { selectRegleSimple('" + regleComplexeId() + "'); }";
	}

	public String regleSimpleCss() {
		return isRegleSelected() ? "regleSimple regleSelected" : "regleSimple";
	}

	public String regleComplexeCss() {
		return isRegleSelected() ? "regleComplexe regleSelected" : "regleComplexe";
	}

	public boolean isRegleSelected() {
		return regle().equals(selectedRegle());
	}

	public String regleValue1() {
		return regle().rValue();
	}

	public void setRegleValue1(String value) {

	}

	public String regleValue2() {
		return "";
	}

	public void setRegleValue2(String value) {

	}

	public NSArray<EORegleKey> regleKeys() {
		return ERXEOControlUtilities.localInstancesOfObjects(ec(), EORegleKey.getRegleKeys());
	}

	public NSArray<EORegleOperateur> regleOps() {
		return ERXEOControlUtilities.localInstancesOfObjects(ec(), EORegleOperateur.getRegleOperateurs());
	}

	public NSArray<EORegleNode> regleNodes() {
		return ERXEOControlUtilities.localInstancesOfObjects(ec(), EORegleNode.getRegleNodes());
	}

	public EORegle getCurrentRegle() {
		return currentRegle;
	}

	public void setCurrentRegle(EORegle currentRegle) {
		this.currentRegle = currentRegle;
	}

	public EORegleKey getCurrentRegleKey() {
		return currentRegleKey;
	}

	public void setCurrentRegleKey(EORegleKey currentRegleKey) {
		this.currentRegleKey = currentRegleKey;
	}

	public EORegleOperateur getCurrentRegleOp() {
		return currentRegleOp;
	}

	public void setCurrentRegleOp(EORegleOperateur currentRegleOp) {
		this.currentRegleOp = currentRegleOp;
	}

	public EORegleNode getCurrentRegleNode() {
		return currentRegleNode;
	}

	public void setCurrentRegleNode(EORegleNode currentRegleNode) {
		this.currentRegleNode = currentRegleNode;
	}

	public EOAssociation selectedAssociation() {
		if (regle().rValue() != null) {
			return (EOAssociation) ERXEOControlUtilities.objectWithPrimaryKeyValue(ec(), EOAssociation.ENTITY_NAME, Integer.valueOf(regle().rValue()), null);
		}
		return null;
	}

	public void setSelectedAssociation(EOAssociation association) {
		if (association != null) {
			Integer pk = (Integer) ERXEOControlUtilities.primaryKeyObjectForObject(association);
			regle().setRValue(pk.toString());
		}
	}

	public EOStructure selectedGroupe() {
		if (regle().toRegleOperateur().isOperateurMembre() && regle().rValue() != null) {
			return (EOStructure) ERXEOControlUtilities.objectWithPrimaryKeyValue(ec(), EOStructure.ENTITY_NAME, regle().rValue(), null);
		} else if (regle().toRegleOperateur().isOperateurRoleDansGroupe() && regle().rValue2() != null) {
			return (EOStructure) ERXEOControlUtilities.objectWithPrimaryKeyValue(ec(), EOStructure.ENTITY_NAME, regle().rValue2(), null);
		}
		return null;
	}

	public void setSelectedGroupe(EOStructure structure) {
		if (structure != null) {
			String pk = ERXEOControlUtilities.primaryKeyStringForObject(structure);
			if (regle().toRegleOperateur().isOperateurMembre())
				regle().setRValue(pk);
			else if (regle().toRegleOperateur().isOperateurRoleDansGroupe())
				regle().setRValue2(pk);
		}
	}

	public String labelForNode() {
		return mySession().localizer().localizedStringForKey("node." + regle().toRegleNode().rnNode().toLowerCase());
	}

}