/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldroitsutilsguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEOControlUtilities;

/**
 * Affichage de l'arborescence des profils.
 * La racine de l'arborescence est le premier profil (supposé unique) n'ayant pas de père.
 * 
 * @binding selection une variable settable de type {@link EOGdProfil} destinée à contenir le profil sélectionné dans 
 *          l'arbre
 * @binding editingContext ec utilisé pour fetcher les profils. A noter que ce composant ne gère pas d'ec interne
 *          c'est au composant parent à le gérer.
 * @binding onSuccessSelect (optionnel) action callback appelée lorsqu'un profil a été sélectionné
 * @binding cssSelectedProfil (optionnel) classe css du lien correspondant au profil sélectionné
 * @binding updateContainerID (optionnel) id du container à rafraîchir lorsqu'un groupe a été sélectionné
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class CktlAjaxProfilTreeView extends DroitsUtilsComponent {

    private static final long serialVersionUID = -3129530219661034118L;
    private static String BINDING_SELECTION = "selection";
    private static String BINDING_ON_SUCCESS_SELECT = "onSuccessSelect";
    private static String BINDING_CSS_SELECTED_PROFIL = "cssSelectedProfil";

    private EOGdProfil currentProfil;
    
    public CktlAjaxProfilTreeView(WOContext context) {
        super(context);
    }
   

    public WOActionResults selectionner() {
        setValueForBinding(currentProfil, BINDING_SELECTION);
        return (WOActionResults) valueForBinding(BINDING_ON_SUCCESS_SELECT);
    }
    
    private EOGdProfil selection() {
        return (EOGdProfil) valueForBinding(BINDING_SELECTION);
    }
    
    public String treeViewId() {
        return "TreeView_" + componentId();
    }
    
    public EOGdProfil rootProfil() {
        return EOGdProfil.rootProfil(edc());
    }
 
    public String toParent() {
        return EOGdProfil.TO_GD_PROFIL_PERE_KEY;
    }
    
    public String toChildren() {
        return EOGdProfil.TO_GD_PROFILS_ENFANTS_KEY;
    }
    
    public String cssClassForSelectedLink() {
        if (hasBinding(BINDING_CSS_SELECTED_PROFIL) &&
                selection() != null && ERXEOControlUtilities.eoEquals(selection(), currentProfil)) {
            return (String) valueForBinding(BINDING_CSS_SELECTED_PROFIL);
        }
        else {
            return String.valueOf("");
        }
    }
    
    public EOGdProfil getCurrentProfil() {
        return currentProfil;
    }
    
    public void setCurrentProfil(EOGdProfil currentProfil) {
        this.currentProfil = currentProfil;
    }
    
}