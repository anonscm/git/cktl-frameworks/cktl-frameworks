/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldroitsutilsguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique;
import org.cocktail.fwkcktlpersonne.common.metier.EORegle;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXEC;

/**
 * Composant d'édition des droits.
 * @see CktlAjaxProfilTreeView
 * @see ProfilForm
 *      binding utilisateurPersId le persid de l'utilisateur courant
 *      binding editingContext (optionnel) l'ec parent éventuel surlequel sera crée des nested
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class ProfilAdminUI extends DroitsUtilsComponent {

	private static final long serialVersionUID = -2718054602876371412L;
	private EOGdProfil profil;
	private EOGdProfil profilParentForMove;
	private EOEditingContext editingContextForViewing;
	private EOEditingContext editingContextForEdition;

	/**
	 * @param context : contexte d'edition
	 */
	public ProfilAdminUI(WOContext context) {
		super(context);
	}

	/**
	 * selectionne le profil pour edition
	 * @return null (reste sur la page)
	 */
	public WOActionResults selectionner() {
		getEditingContextForEdition().revert();
		setProfil(getProfil().localInstanceIn(getEditingContextForEdition()));
		return null;
	}

	/**
	 * ajoute un profil
	 * @return null (reste sur la page)
	 */
	public WOActionResults ajouterProfil() {
		if (getProfil() == null) {
			session().addSimpleErrorMessage(mySession().localizer().localizedStringForKey("need.profil"), "");
		} else {
			EOGdProfil newProfil = EOGdProfil.creerInstance(getEditingContextForEdition());
			newProfil.setPrLc("Nouveau profil");
//			EOGroupeDynamique groupeDynamique = EOGroupeDynamique.creerInstance(getEditingContextForEdition());
//			groupeDynamique.setGrpdLc("Groupe dynamique pour profil");
//			groupeDynamique.setGrpdDescription("Groupe dynamique pour profil");
//			EORegle regleRacine = EORegle.creerRegleSimple(newProfil.editingContext(), utilisateurPersId());
//			groupeDynamique.setToRegleRelationship(regleRacine);
//			newProfil.setToGroupeDynamiqueRelationship(groupeDynamique);
			newProfil.setToGdProfilPereRelationship(getProfil());
			getProfil().addToToGdProfilsEnfantsRelationship(newProfil);
			newProfil.setPersIdCreation(utilisateurPersId());
			newProfil.setPersIdModification(utilisateurPersId());
			setProfil(newProfil);
		}
		return null;
	}

	/**
	 * enregistre les modifications effectuees dans la page
	 * @return null (reste sur la page)
	 */
	public WOActionResults enregistrer() {
		try {
			// TODO JLA Trop moche à changer !!!!!
			if (getProfil() != null) {
				getProfil().setPersIdModification(utilisateurPersId());
				getEditingContextForEdition().saveChanges();
				edc().saveChanges();
				
				if (getProfil().toGroupeDynamique() != null) {
					getProfil().toGroupeDynamique().calculerGroupeDynamique();
				}
				session().addSimpleInfoMessage(mySession().localizer().localizedStringForKey("confirmation"), "");
			}
		} catch (ValidationException e) {
			LOG.warn(e.getMessage(), e);
			session().addSimpleErrorMessage(e.getMessage(), "");
		}
		return null;
	}

	/**
	 * annule les modifications effectuees dans la page
	 * @return null (reste sur la page)
	 */
	public WOActionResults annuler() {
		setProfil(null);
		return null;
	}

	/**
	 * deplace le profil selectionne vers le nouveau profil parent, puis ferme la fenêtre
	 * @return null (reste sur la page)
	 */
	public WOActionResults deplacerProfil() {
		if (getProfilParentForMove() != null) {
			if (profilPeutEtreDeplace()) {
				EOGdProfil parentProfil = getProfilParentForMove().localInstanceIn(getEditingContextForEdition());
				getProfil().setToGdProfilPereRelationship(parentProfil);
				enregistrer();
				AjaxUpdateContainer.updateContainerWithID(getContainerId(), context());
			} else {
				session().addSimpleErrorMessage(mySession().localizer().localizedStringForKey("bad.profil.parent"));
			}

			CktlAjaxWindow.close(context(), getTreeMoveModalId());
		}
		return null;
	}

	private boolean profilPeutEtreDeplace() {
		//un profil ne peut être son propre parent
		if (getProfilParentForMove().primaryKey().equals(getProfil().primaryKey())) {
			return false; 
		}
		
		//un profil ne peut être le parent de son parent
		if ((getProfilParentForMove().toGdProfilPere() != null)
		        && (getProfilParentForMove().toGdProfilPere().primaryKey().equals(getProfil().primaryKey()))) {
			return false;
		}
		return true;
	}

	/**
	 * Supprime le profil selectionne
	 * @return  null (reste sur la page)
	 */
	public WOActionResults supprimerProfil() {
		try {
			if (getProfil().toGdProfilsEnfants().count() > 0) {
				throw new ValidationException(mySession().localizer().localizedStringForKey("bad.delete"));
			}

			// Suppression du lien entre le profil pere et la profil a supprimer
			getProfil().toGdProfilPere().removeFromToGdProfilsEnfantsRelationship(getProfil());
			getProfil().setToGdProfilPereRelationship(null);
			
			// Suppression des perimetres associes
			if (isFeatureFlippingPerimetreDonnees()) {
				getProfil().deleteAllPerimetresRelationships();
			}
			
			// Ajout le 05/03/2015 de la suppression vers les fonctions s'il y en a
			if (getProfil().toGdProfilDroitFonctions().count() > 0) {
				getProfil().deleteAllToGdProfilDroitFonctionsRelationships();
			}
			if (getProfil().toGdProfilDroitDonnees() .count() > 0) {
				getProfil().deleteAllToGdProfilDroitDonneesRelationships();
			}
			if (getProfil().toGdProfilDroitDonneeSts().count() > 0) {
				getProfil().deleteAllToGdProfilDroitDonneeStsRelationships();
			}
			
			getEditingContextForEdition().saveChanges();
			getEditingContextForViewing().saveChanges();
			edc().saveChanges();
			
			getProfil().supprimerEnSQL(isFeatureFlippingPerimetreDonnees());

			session().addSimpleInfoMessage(mySession().localizer().localizedStringForKey("confirmation.delete"), "");
			setProfil(null);
		} catch (ValidationException e) {
			LOG.warn(e.getMessage(), e);
			session().addSimpleErrorMessage(e.getMessage(), "");
		} catch (Exception e) {
			LOG.warn(e.getMessage(), e);
			session().addSimpleErrorMessage(e.getMessage(), "");
		}
		return null;
	}

	/**
	 * @return true si l'utilisateur peut ajouter un profil
	 */
	public boolean peutAjouterProfil() {
		return getProfil() != null && applicationUser().hasDroitCreationProfil(getProfil());
	}

	/**
	 * @return true si l'utilisateur peut supprimer un profil
	 */
	public boolean peutSupprimerProfil() {
		return getProfil() != null && applicationUser().hasDroitSuppressionProfil(getProfil());
	}

	/**
	 * @return true si l'utilisateur peut déplacer un profil
	 */
	public boolean peutDeplacerProfil() {
		return getProfil() != null && applicationUser().hasDroitModificationProfil(getProfil());
	}

	public String getTreeMoveContainerId() {
		return "TreeMove_" + componentId();
	}

	public String getTreeMoveModalId() {
		return "TreeMoveModal_" + componentId();
	}

	public String getContainerId() {
		return "Container_" + componentId();
	}

	public EOGdProfil getProfil() {
		return profil;
	}

	public void setProfil(EOGdProfil profil) {
		this.profil = profil;
	}

	/**
	 * @return contexte d'édition pour l'édition
	 */
	public EOEditingContext getEditingContextForEdition() {
		if (editingContextForEdition == null)
			editingContextForEdition = ERXEC.newEditingContext(edc());
		return editingContextForEdition;
	}

	/**
	 * @return contexte d'édition pour l'affichage
	 */
	public EOEditingContext getEditingContextForViewing() {
		if (editingContextForViewing == null)
			editingContextForViewing = ERXEC.newEditingContext(edc());
		return editingContextForViewing;
	}

	public EOGdProfil getProfilParentForMove() {
		return profilParentForMove;
	}

	public void setProfilParentForMove(EOGdProfil profilParentForMove) {
		this.profilParentForMove = profilParentForMove;
	}

}