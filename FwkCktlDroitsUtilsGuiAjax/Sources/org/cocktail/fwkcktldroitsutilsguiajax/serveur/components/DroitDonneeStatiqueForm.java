/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldroitsutilsguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

/**
 * 
 * Affichage et édition des droits sur les données statiques.
 * 
 * @binding application l'application sélectionnée servant à filtrer les droits
 * @binding profil le profil dont on veut éditer les droits
 * @binding utilisateurPersId l'utilisateur éditant les droits
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class DroitDonneeStatiqueForm extends DroitsUtilsComponent {
    
    private static final long serialVersionUID = 6422786342707473351L;

    public static final String BINDING_PROFIL = "profil";
    public static final String BINDING_APPLICATION = "application";
    private EOGdProfilDroitDonneeSt currentDroitDonneeSt;
    private EOGdTypeDroitDonnee currentTypeDroitDonnee;
    private EOGdDonneeStat currentDonneeStat;
    private EOGdProfilDroitDonneeSt newDroitDonneeSt;
    private EOEditingContext ecForCreation;
    private int indexRep;
    
    public DroitDonneeStatiqueForm(WOContext context) {
        super(context);
    }
    
    @Override
    public void awake() {
        super.awake();
        // Si pas crée, on crée un ec nested pour rajouter un droit
        if (ecForCreation == null) {
            ecForCreation = ERXEC.newEditingContext(profil().editingContext());
        }
        if (newDroitDonneeSt == null) {
            newDroitDonneeSt = EOGdProfilDroitDonneeSt.creer(ecForCreation, utilisateurPersId());
        }
    }
    
    public WOActionResults creerDroit() {
        try {
            newDroitDonneeSt.setToGdProfilRelationship(profil().localInstanceIn(ecForCreation));
            ecForCreation.saveChanges();
            // si ok on met à null pour une prochaine création
            newDroitDonneeSt = null;
        } catch (ValidationException e) {
            LOG.warn(e.getMessage(), e);
            session().addSimpleErrorMessage(e.getMessage(), "");
        } catch (Exception e) {
            throw new NSForwardException(e);
        }
        return null;
    }
    
    public WOActionResults supprimerDroit() {
        profil().removeFromToGdProfilDroitDonneeStsRelationship(currentDroitDonneeSt);
        profil().editingContext().deleteObject(currentDroitDonneeSt);
        return null;
    }
    
    public String cssDroit() {
        String css = isHerite() ? "droitHerite " : "";
        css = indexRep % 2 == 0 ? css + "even" : css + "odd";
        return css;
    }
    
    public String droitHeriteExp() {
        return "Hérité de " + getCurrentDroitDonneeSt().toGdProfil().prLc();
    }
    
    public boolean isHerite() {
        return !profil().equals(getCurrentDroitDonneeSt().toGdProfil());
    }
    
    public EOGdProfil profil() {
        return (EOGdProfil)valueForBinding(BINDING_PROFIL);
    }
    
    public EOGdApplication gdApplication() {
        return (EOGdApplication)valueForBinding(BINDING_APPLICATION);
    }
    
    public NSArray<EOGdTypeDroitDonnee> getTypesDroitDonnee() {
        return ERXEOControlUtilities.localInstancesOfObjects(profil().editingContext(), EOGdTypeDroitDonnee.getTypesDroitDonnee());
    }
    
    public NSArray<EOGdTypeDroitDonnee> getTypesDroitDonneeForNew() {
        return ERXEOControlUtilities.localInstancesOfObjects(ecForCreation, EOGdTypeDroitDonnee.getTypesDroitDonnee());
    }
    
    private EOQualifier qualApplication() {
        return ERXQ.equals(EOGdDonnee.TO_GD_APPLICATION_KEY, gdApplication());
    }
    
    public NSArray<EOGdDonneeStat> getDonneesForNew() {
        // On filtre selon le domaine
        return ERXQ.filtered(
                ERXEOControlUtilities.localInstancesOfObjects(ecForCreation, EOGdDonneeStat.getDonnees()),
                qualApplication());
    }
    
    public NSArray<EOGdProfilDroitDonneeSt> getAllDroitsDonneeSt() {
        return profil().allDroitsDonneeSt(gdApplication());
    }
    
    public EOGdTypeDroitDonnee getCurrentTypeDroitDonnee() {
        return currentTypeDroitDonnee;
    }
    
    public void setCurrentTypeDroitDonnee(EOGdTypeDroitDonnee currentTypeDroitDonnee) {
        this.currentTypeDroitDonnee = currentTypeDroitDonnee;
    }
    
    public EOGdProfilDroitDonneeSt getCurrentDroitDonneeSt() {
        return currentDroitDonneeSt;
    }
    
    public void setCurrentDroitDonneeSt(EOGdProfilDroitDonneeSt currentDroitDonneeSt) {
        this.currentDroitDonneeSt = currentDroitDonneeSt;
    }

    public EOGdDonneeStat getCurrentDonneeStat() {
        return currentDonneeStat;
    }
    
    public void setCurrentDonneeStat(EOGdDonneeStat currentDonneeStat) {
        this.currentDonneeStat = currentDonneeStat;
    }
    
    public int getIndexRep() {
        return indexRep;
    }
    
    public void setIndexRep(int indexRep) {
        this.indexRep = indexRep;
    }
    
    public EOGdProfilDroitDonneeSt getNewDroitDonneeSt() {
        return newDroitDonneeSt;
    }
    
}