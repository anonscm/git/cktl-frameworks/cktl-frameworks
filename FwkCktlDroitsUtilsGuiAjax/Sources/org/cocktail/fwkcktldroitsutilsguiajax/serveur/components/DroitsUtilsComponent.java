/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldroitsutilsguiajax.serveur.components;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXProperties;

/**
 * Composant générique de gestion des droits.
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class DroitsUtilsComponent extends CktlAjaxWOComponent {

	private static final long serialVersionUID = 5065071214977035999L;
	public static final String BINDING_EDITING_CONTEXT = "editingContext";
	public static final String BINDING_WANT_RESET = "wantReset";
	public static final String BINDING_UTILISATEUR_PERSID = "utilisateurPersId";
	public static final Logger LOG = Logger.getLogger(DroitsUtilsComponent.class);

	private String componentId;
	private EOEditingContext edc;
	private PersonneApplicationUser appUser;
	
	private static final String FONCTION_PERIMETRE = "cocktail.feature.perimetre";
	

	public DroitsUtilsComponent(WOContext context) {
		super(context);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public int utilisateurPersId() {
		return (Integer) valueForBinding(BINDING_UTILISATEUR_PERSID);
	}

	public PersonneApplicationUser applicationUser() {
		if (appUser == null) {
			appUser = new PersonneApplicationUser(session().defaultEditingContext(), utilisateurPersId());
		}
		return appUser;
	}

	public String componentId() {
		if (componentId == null)
			componentId = ERXWOContext.safeIdentifierName(context(), true);	
		return componentId;
	}

	@Override
	public EOEditingContext edc() {
		if (edc == null) {
			if (hasBinding(BINDING_EDITING_CONTEXT)) {
				edc = (EOEditingContext) valueForBinding(BINDING_EDITING_CONTEXT);
			} else {
				edc = ERXEC.newEditingContext();
			}
		}
		return edc;
	}

	public CocktailAjaxSession session() {
		return (CocktailAjaxSession) super.session();
	}

	public boolean wantReset() {
		return booleanValueForBinding(BINDING_WANT_RESET, false);
	}

	public void setWantReset(boolean value) {
		if (hasBinding(BINDING_WANT_RESET) && canSetValueForBinding(BINDING_WANT_RESET))
			setValueForBinding(value, BINDING_WANT_RESET);
	}

	public boolean isFeatureFlippingPerimetreDonnees() {
		return isFonctionActive(FONCTION_PERIMETRE);
	}
	
	private boolean isFonctionActive(String param) {
		// Par defaut fonction active
		return ERXProperties.booleanForKeyWithDefault(param, false);
	}
	
}
