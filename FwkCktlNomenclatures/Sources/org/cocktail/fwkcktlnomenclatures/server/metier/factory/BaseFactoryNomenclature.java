package org.cocktail.fwkcktlnomenclatures.server.metier.factory;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclature;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;

import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXSelectorUtilities;
import er.extensions.localization.ERXLocalizer;

public class BaseFactoryNomenclature {

	private Logger logger = Logger.getLogger(getClass());

	private static BaseFactoryNomenclature shared = null;

	private NSMutableDictionary<String, EONomenclature> nomenclatures = new NSMutableDictionary<String, EONomenclature>();
	private EOEditingContext editingContext = null;

	private EOEditingContext editingContext() {
		if (editingContext == null) {
			editingContext = ERXEC.newEditingContext();
		}
		return editingContext;
	}

	public static BaseFactoryNomenclature shared() {
		if (shared == null) {
			shared = new BaseFactoryNomenclature();
			NSNotificationCenter.defaultCenter().addObserver(shared, ERXSelectorUtilities.notificationSelector("reset"), "resetNomenclatures", null);
		}
		return shared;
	}

	public EONomenclature nomenclatureWithStringId(String stringId) {
		if (!nomenclatures.containsKey(stringId)) {
			try {
				nomenclatures.takeValueForKey(EONomenclature.fetchFirstRequiredByQualifier(editingContext(), EONomenclature.STRING_ID.eq(stringId)), stringId);
			} catch (NoSuchElementException e) {
				NSDictionary<String, String> values = new NSDictionary<String, String>(stringId, "id");
				logger.error(ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("NomenclatureInexistantePourId", values));
				return null;
			}
		}
		return nomenclatures.get(stringId);
	}

	public void reset(NSNotification notification) {
		nomenclatures.clear();
	}
}
