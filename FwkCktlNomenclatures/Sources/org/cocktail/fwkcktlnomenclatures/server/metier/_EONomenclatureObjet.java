/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EONomenclatureObjet.java instead.
package org.cocktail.fwkcktlnomenclatures.server.metier;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.*;
import er.extensions.foundation.*;

public abstract class _EONomenclatureObjet extends  ERXGenericRecord {
	  public static final String ENTITY_NAME = "FwkCktlNomenclature_Objet";



	// Attributes

				public static final String ENTITY_PRIMARY_KEY = "id";
	
	    public static final ERXKey<String> CODE = new ERXKey<String>("code");
      public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
      public static final ERXKey<NSTimestamp> D_DEBUT_VALIDITE = new ERXKey<NSTimestamp>("dDebutValidite");
      public static final ERXKey<NSTimestamp> D_FIN_VALIDITE = new ERXKey<NSTimestamp>("dFinValidite");
      public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
      public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
      public static final ERXKey<String> LIBELLE_COURT_CUSTOM = new ERXKey<String>("libelleCourtCustom");
      public static final ERXKey<String> LIBELLE_LONG = new ERXKey<String>("libelleLong");
      public static final ERXKey<String> LIBELLE_LONG_CUSTOM = new ERXKey<String>("libelleLongCustom");
      public static final ERXKey<String> ORIGINE = new ERXKey<String>("origine");
      public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
      public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
      public static final ERXKey<Boolean> PERSONNALISEE = new ERXKey<Boolean>("personnalisee");
      public static final ERXKey<Integer> POSITION = new ERXKey<Integer>("position");
      public static final ERXKey<String> VALEUR = new ERXKey<String>("valeur");
      public static final ERXKey<Boolean> VISIBLE = new ERXKey<Boolean>("visible");
  	
			public static final String CODE_KEY = CODE.key();
			public static final String D_CREATION_KEY = D_CREATION.key();
			public static final String D_DEBUT_VALIDITE_KEY = D_DEBUT_VALIDITE.key();
			public static final String D_FIN_VALIDITE_KEY = D_FIN_VALIDITE.key();
			public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
			public static final String LIBELLE_COURT_KEY = LIBELLE_COURT.key();
			public static final String LIBELLE_COURT_CUSTOM_KEY = LIBELLE_COURT_CUSTOM.key();
			public static final String LIBELLE_LONG_KEY = LIBELLE_LONG.key();
			public static final String LIBELLE_LONG_CUSTOM_KEY = LIBELLE_LONG_CUSTOM.key();
			public static final String ORIGINE_KEY = ORIGINE.key();
			public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
			public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
			public static final String PERSONNALISEE_KEY = PERSONNALISEE.key();
			public static final String POSITION_KEY = POSITION.key();
			public static final String VALEUR_KEY = VALEUR.key();
			public static final String VISIBLE_KEY = VISIBLE.key();
	
	// Attributs non visibles
			public static final String ID_KEY = "id";
			public static final String NOMENCLATURE_ID_KEY = "nomenclatureId";
	
	//Colonnes dans la base de donnees
			public static final String CODE_COLKEY = "CODE";
			public static final String D_CREATION_COLKEY = "D_CREATION";
			public static final String D_DEBUT_VALIDITE_COLKEY = "DATE_DEBUT_VALIDITE";
			public static final String D_FIN_VALIDITE_COLKEY = "DATE_FIN_VALIDITE";
			public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
			public static final String LIBELLE_COURT_COLKEY = "LIBELLE_COURT";
			public static final String LIBELLE_COURT_CUSTOM_COLKEY = "LIBELLE_COURT_CUSTOM";
			public static final String LIBELLE_LONG_COLKEY = "LIBELLE_LONG";
			public static final String LIBELLE_LONG_CUSTOM_COLKEY = "LIBELLE_LONG_CUSTOM";
			public static final String ORIGINE_COLKEY = "ORIGINE";
			public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
			public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
			public static final String PERSONNALISEE_COLKEY = "PERSONNALISEE";
			public static final String POSITION_COLKEY = "POSITION";
			public static final String VALEUR_COLKEY = "VALEUR";
			public static final String VISIBLE_COLKEY = "VISIBLE";
	
			public static final String ID_COLKEY = "ID";
			public static final String NOMENCLATURE_ID_COLKEY = "NOMENCLATURE_ID";
	

		// Relationships
	
	    public static final ERXKey<org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclature> NOMENCLATURE = new ERXKey<org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclature>("nomenclature");
  	
			public static final String NOMENCLATURE_KEY = NOMENCLATURE.key();
	


	// Accessors methods
			  public String code() {
	    return (String) storedValueForKey(CODE_KEY);
	  }

	  public void setCode(String value) {
	    takeStoredValueForKey(value, CODE_KEY);
	  }
	
			  public NSTimestamp dCreation() {
	    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	  }

	  public void setDCreation(NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	  }
	
			  public NSTimestamp dDebutValidite() {
	    return (NSTimestamp) storedValueForKey(D_DEBUT_VALIDITE_KEY);
	  }

	  public void setDDebutValidite(NSTimestamp value) {
	    takeStoredValueForKey(value, D_DEBUT_VALIDITE_KEY);
	  }
	
			  public NSTimestamp dFinValidite() {
	    return (NSTimestamp) storedValueForKey(D_FIN_VALIDITE_KEY);
	  }

	  public void setDFinValidite(NSTimestamp value) {
	    takeStoredValueForKey(value, D_FIN_VALIDITE_KEY);
	  }
	
			  public NSTimestamp dModification() {
	    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	  }

	  public void setDModification(NSTimestamp value) {
	    takeStoredValueForKey(value, D_MODIFICATION_KEY);
	  }
	
			  public String libelleCourt() {
	    return (String) storedValueForKey(LIBELLE_COURT_KEY);
	  }

	  public void setLibelleCourt(String value) {
	    takeStoredValueForKey(value, LIBELLE_COURT_KEY);
	  }
	
			  public String libelleCourtCustom() {
	    return (String) storedValueForKey(LIBELLE_COURT_CUSTOM_KEY);
	  }

	  public void setLibelleCourtCustom(String value) {
	    takeStoredValueForKey(value, LIBELLE_COURT_CUSTOM_KEY);
	  }
	
			  public String libelleLong() {
	    return (String) storedValueForKey(LIBELLE_LONG_KEY);
	  }

	  public void setLibelleLong(String value) {
	    takeStoredValueForKey(value, LIBELLE_LONG_KEY);
	  }
	
			  public String libelleLongCustom() {
	    return (String) storedValueForKey(LIBELLE_LONG_CUSTOM_KEY);
	  }

	  public void setLibelleLongCustom(String value) {
	    takeStoredValueForKey(value, LIBELLE_LONG_CUSTOM_KEY);
	  }
	
			  public String origine() {
	    return (String) storedValueForKey(ORIGINE_KEY);
	  }

	  public void setOrigine(String value) {
	    takeStoredValueForKey(value, ORIGINE_KEY);
	  }
	
			  public Integer persIdCreation() {
	    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	  }

	  public void setPersIdCreation(Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	  }
	
			  public Integer persIdModification() {
	    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	  }

	  public void setPersIdModification(Integer value) {
	    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	  }
	
			  public Boolean personnalisee() {
	    return (Boolean) storedValueForKey(PERSONNALISEE_KEY);
	  }

	  public void setPersonnalisee(Boolean value) {
	    takeStoredValueForKey(value, PERSONNALISEE_KEY);
	  }
	
			  public Integer position() {
	    return (Integer) storedValueForKey(POSITION_KEY);
	  }

	  public void setPosition(Integer value) {
	    takeStoredValueForKey(value, POSITION_KEY);
	  }
	
			  public String valeur() {
	    return (String) storedValueForKey(VALEUR_KEY);
	  }

	  public void setValeur(String value) {
	    takeStoredValueForKey(value, VALEUR_KEY);
	  }
	
			  public Boolean visible() {
	    return (Boolean) storedValueForKey(VISIBLE_KEY);
	  }

	  public void setVisible(Boolean value) {
	    takeStoredValueForKey(value, VISIBLE_KEY);
	  }
	
				  public org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclature nomenclature() {
	    return (org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclature)storedValueForKey(NOMENCLATURE_KEY);
	  }

	  public void setNomenclatureRelationship(org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclature value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclature oldValue = nomenclature();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, NOMENCLATURE_KEY);
	      }
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, NOMENCLATURE_KEY);
	    }
	  }
	  
			


  
	  public EONomenclatureObjet localInstanceIn(EOEditingContext editingContext) {
	  		return (EONomenclatureObjet)ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONomenclatureObjet creerInstance(EOEditingContext editingContext) {
      return (EONomenclatureObjet)ERXEOControlUtilities.createAndInsertObject(editingContext, _EONomenclatureObjet.ENTITY_NAME);
		}

	/* Finders */

	  public static NSArray<EONomenclatureObjet> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null, null);
	  }

	  public static NSArray<EONomenclatureObjet> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EONomenclatureObjet> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	  }

	  public static NSArray<EONomenclatureObjet> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EONomenclatureObjet> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EONomenclatureObjet> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throw IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EONomenclatureObjet fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EONomenclatureObjet fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EONomenclatureObjet eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EONomenclatureObjet)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EONomenclatureObjet fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EONomenclatureObjet fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EONomenclatureObjet eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EONomenclatureObjet)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EONomenclatureObjet fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EONomenclatureObjet eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EONomenclatureObjet ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EONomenclatureObjet fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
