package org.cocktail.fwkcktlnomenclatures.server.metier;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.foundation.ERXStringUtilities;
import er.extensions.localization.ERXLocalizer;

public class EONomenclature extends _EONomenclature {
	private static Logger logger = Logger.getLogger(EONomenclature.class);

	/*
	 * Types de valeurs possible
	 */
	public static final String TYPE_VALEUR_STRING = "String";
	public static final String TYPE_VALEUR_FLOAT = "Float";
	public static final String TYPE_VALEUR_INTEGER = "Integer";

	public Boolean isNomenclatureDeString() {
		return this.typeValeurs().equalsIgnoreCase(TYPE_VALEUR_STRING);
	}

	public Boolean isNomenclatureDeFloat() {
		return this.typeValeurs().equalsIgnoreCase(TYPE_VALEUR_FLOAT);
	}

	public Boolean isNomenclatureDeInteger() {
		return this.typeValeurs().equalsIgnoreCase(TYPE_VALEUR_INTEGER);
	}

	/*
	 * Types de nomenclatures possibles
	 */
	public static final String TYPE_NOMENCLATURE_LISTE = "LISTE";
	public static final String TYPE_NOMENCLATURE_DICO = "DICO";

	public Boolean isListe() {
		return this.typeNomenclature().equalsIgnoreCase(TYPE_NOMENCLATURE_LISTE);
	}

	public Boolean isDico() {
		return this.typeNomenclature().equalsIgnoreCase(TYPE_NOMENCLATURE_DICO);
	}

	public NSArray<String> asArrayOfStrings() {

		if (!isListe()) {
			NSDictionary<String, String> values = new NSDictionary<String, String>(stringId(), "id");
			logger.error(ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("EstPasUneListe", values));
			return null;
		}
		if (!isNomenclatureDeString()) {
			NSDictionary<String, String> values = new NSDictionary<String, String>(stringId(), "id");
			logger.error(ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("EstPasUneNomenclatureDeStrings", values));
			return null;
		}

		NSArray<EONomenclatureObjet> objets = objetsVisibles();
		NSArray<String> array = new NSMutableArray<String>();
		for (EONomenclatureObjet objet : objets) {

			if (StringUtils.isBlank(objet.libelleLongCustom())) {
				array.add(objet.libelleLong());
			} else {
				array.add(objet.libelleLongCustom());
			}

		}
		return array;

	}

	private NSArray<EONomenclatureObjet> objetsVisibles() {
		return objets(EONomenclatureObjet.VISIBLE.isTrue());
	}

	public NSDictionary<String, String> asMapOfStrings() {

		if (!isDico()) {
			NSDictionary<String, String> values = new NSDictionary<String, String>(stringId(), "id");
			logger.error(ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("EstPasUnDico", values));
			return null;
		}
		if (!isNomenclatureDeString()) {
			NSDictionary<String, String> values = new NSDictionary<String, String>(stringId(), "id");
			logger.error(ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("EstPasUneNomenclatureDeStrings", values));
			return null;
		}

		NSArray<EONomenclatureObjet> objets = objetsVisibles();
		NSDictionary<String, String> dico = new NSMutableDictionary<String, String>();
		for (EONomenclatureObjet objet : objets) {
			if (ERXStringUtilities.stringIsNullOrEmpty(objet.libelleLongCustom())) {
				dico.takeValueForKey(objet.libelleLong(), objet.code());
			} else {
				dico.takeValueForKey(objet.libelleLongCustom(), objet.code());
			}
		}
		return dico;

	}

	public static Object objetsWithNomenclatureStringId(String nomenclatureStringId, EOEditingContext ec) {

		EONomenclature nomenclature = EONomenclature.fetchFirstByQualifier(ec, EONomenclature.STRING_ID.eq(nomenclatureStringId));
		if (nomenclature.isDico()) {
			return (NSDictionary<String, String>) nomenclature.getObjetsAsObject();
		}
		if (nomenclature.isListe()) {
			return (NSArray<String>) nomenclature.getObjetsAsObject();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public Object getObjetsAsObject() {

		NSArray<EONomenclatureObjet> objets = this.objets(EONomenclatureObjet.VISIBLE.isTrue());
		Object resultats = null;

		if (isListe()) {
			NSArray listeResultats = null;
			if (isNomenclatureDeString()) {
				listeResultats = new NSMutableArray<String>();
				for (EONomenclatureObjet objet : objets) {
					if (ERXStringUtilities.stringIsNullOrEmpty(objet.libelleLongCustom())) {
						listeResultats.add(objet.libelleLong());
					} else {
						listeResultats.add(objet.libelleLongCustom());
					}

				}
			} else if (isNomenclatureDeFloat()) {
				listeResultats = new NSMutableArray<Float>();
				for (EONomenclatureObjet objet : objets) {
					listeResultats.add(Float.valueOf(objet.valeur()));
				}
			} else if (isNomenclatureDeInteger()) {
				listeResultats = new NSMutableArray<Integer>();
				for (EONomenclatureObjet objet : objets) {
					listeResultats.add(Integer.valueOf(objet.valeur()));
				}
			}
			return listeResultats;
		} else if (isDico()) {
			NSDictionary dicoResultats = null;
			if (isNomenclatureDeString()) {
				dicoResultats = new NSMutableDictionary<String, String>();
				for (EONomenclatureObjet objet : objets) {
					if (ERXStringUtilities.stringIsNullOrEmpty(objet.libelleLongCustom())) {
						dicoResultats.takeValueForKey(objet.libelleLong(), objet.code());
					} else {
						dicoResultats.takeValueForKey(objet.libelleLongCustom(), objet.code());
					}

				}
			} else if (isNomenclatureDeFloat()) {
				dicoResultats = new NSMutableDictionary<String, Float>();
				for (EONomenclatureObjet objet : objets) {
					dicoResultats.takeValueForKey(Float.valueOf(objet.valeur()), objet.code());
				}
			} else if (isNomenclatureDeInteger()) {
				dicoResultats = new NSMutableDictionary<String, Integer>();
				for (EONomenclatureObjet objet : objets) {
					dicoResultats.takeValueForKey(Integer.valueOf(objet.valeur()), objet.code());
				}
			}
			return dicoResultats;
		}

		return null;

	}

	public static NSArray<EONomenclature> nomenclaturesWithFilter(String filter, EOEditingContext editingContext) {
		return EONomenclature.fetchAll(editingContext, EONomenclature.STRING_ID.startsWith(filter), null);
	}

}
