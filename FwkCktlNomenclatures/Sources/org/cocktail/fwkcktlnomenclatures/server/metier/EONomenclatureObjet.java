package org.cocktail.fwkcktlnomenclatures.server.metier;

import org.apache.log4j.Logger;

import com.webobjects.foundation.NSTimestamp;

import er.extensions.foundation.ERXStringUtilities;

public class EONomenclatureObjet extends _EONomenclatureObjet {

	private static final long serialVersionUID = 2467982606880373727L;
	private static Logger log = Logger.getLogger(EONomenclatureObjet.class);

	private static String STRING_REFERENTIEL = "R";
	private static String STRING_ETABLISSEMENT = "E";
	
	public Boolean isReferentiel() {
		if(ERXStringUtilities.stringEqualsString(origine(), STRING_REFERENTIEL)) {
			return true;
		} else {
			return false;
		}
	}
	
	public Boolean isEtablissement() {
		if(ERXStringUtilities.stringEqualsString(origine(), STRING_ETABLISSEMENT)) {
			return true;
		} else {
			return false;
		}
	}
	
	public Boolean isVisible() {
		
		if(visible()) {
			return true;
		} else {
			return false;
		}
	}
	
	public EONomenclatureObjet addNomenclatureObjet(String code, String libelleCourt, String libelleCourtCustom, String libelleLong, String libelleLongCustom, String valeur, 
			NSTimestamp dateDebutValidite, NSTimestamp dateFinValidite, EONomenclature nomenclature, Integer persId, Integer position, Boolean visible) {
		
		setCode(code);
		setLibelleCourt(libelleCourt);
		setLibelleCourtCustom(libelleCourtCustom);
		setLibelleLong(libelleLong);
		setLibelleLongCustom(libelleLongCustom);
		setValeur(valeur);
		setDDebutValidite(dateDebutValidite);
		setDFinValidite(dateFinValidite);
		NSTimestamp date = new NSTimestamp();
		setDCreation(date);
		setDModification(date);
		setOrigine(STRING_ETABLISSEMENT);
		setPersIdCreation(persId);
		setPersIdModification(persId);
		setPersonnalisee(true);
		setPosition(position);
		setVisible(visible);
		
		return this;
		
	}
	
	public EONomenclatureObjet editNomenclatureObjet(String libelleCourtCustom, String libelleLongCustom, String valeur, 
			NSTimestamp dateFinValidite, Integer persId, Boolean visible) {
		
		setLibelleCourtCustom(libelleCourtCustom);
		setLibelleLongCustom(libelleLongCustom);
		setValeur(valeur);
		setDFinValidite(dateFinValidite);
		setDModification(new NSTimestamp());
		setPersIdModification(persId);
		setPersonnalisee(true);
		setVisible(visible);
		
		return this;
	}
	
	public EONomenclatureObjet setPosition(Integer position, Integer persId) {
		
		setPosition(position);
		setPersIdModification(persId);
		setDModification(new NSTimestamp());
		setPersonnalisee(true);
		
		return this;
	}
	
	public String libelle() {

		if(!ERXStringUtilities.stringIsNullOrEmpty(libelleLongCustom())) {
			return libelleLongCustom();
		} else if(!ERXStringUtilities.stringIsNullOrEmpty(libelleCourtCustom())) {
			return libelleCourtCustom();
		} else if(!ERXStringUtilities.stringIsNullOrEmpty(libelleLong())) {
			return libelleLong();
		} else if(!ERXStringUtilities.stringIsNullOrEmpty(libelleCourt())) {
			return libelleCourt();
		}
		
		return null;
	}
	
}
