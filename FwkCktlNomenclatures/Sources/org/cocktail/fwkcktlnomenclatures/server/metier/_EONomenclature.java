/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EONomenclature.java instead.
package org.cocktail.fwkcktlnomenclatures.server.metier;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.*;
import er.extensions.foundation.*;

public abstract class _EONomenclature extends  ERXGenericRecord {
	  public static final String ENTITY_NAME = "FwkCktlNomenclature_Nomenclature";



	// Attributes

				public static final String ENTITY_PRIMARY_KEY = "id";
	
	    public static final ERXKey<Boolean> COMPLETABLE = new ERXKey<Boolean>("completable");
      public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
      public static final ERXKey<String> DESCRIPTION = new ERXKey<String>("description");
      public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
      public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
      public static final ERXKey<Boolean> MODIFIABLE = new ERXKey<Boolean>("modifiable");
      public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
      public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
      public static final ERXKey<String> STRING_ID = new ERXKey<String>("stringId");
      public static final ERXKey<String> TYPE_NOMENCLATURE = new ERXKey<String>("typeNomenclature");
      public static final ERXKey<String> TYPE_VALEURS = new ERXKey<String>("typeValeurs");
      public static final ERXKey<Boolean> VALIDE = new ERXKey<Boolean>("valide");
  	
			public static final String COMPLETABLE_KEY = COMPLETABLE.key();
			public static final String D_CREATION_KEY = D_CREATION.key();
			public static final String DESCRIPTION_KEY = DESCRIPTION.key();
			public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
			public static final String LIBELLE_KEY = LIBELLE.key();
			public static final String MODIFIABLE_KEY = MODIFIABLE.key();
			public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
			public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
			public static final String STRING_ID_KEY = STRING_ID.key();
			public static final String TYPE_NOMENCLATURE_KEY = TYPE_NOMENCLATURE.key();
			public static final String TYPE_VALEURS_KEY = TYPE_VALEURS.key();
			public static final String VALIDE_KEY = VALIDE.key();
	
	// Attributs non visibles
			public static final String ID_KEY = "id";
	
	//Colonnes dans la base de donnees
			public static final String COMPLETABLE_COLKEY = "COMPLETABLE";
			public static final String D_CREATION_COLKEY = "D_CREATION";
			public static final String DESCRIPTION_COLKEY = "DESCRIPTION";
			public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
			public static final String LIBELLE_COLKEY = "LIBELLE";
			public static final String MODIFIABLE_COLKEY = "MODIFIABLE";
			public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
			public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
			public static final String STRING_ID_COLKEY = "STR_ID";
			public static final String TYPE_NOMENCLATURE_COLKEY = "TYPE_NOMENCLATURE";
			public static final String TYPE_VALEURS_COLKEY = "TYPE_VALEURS";
			public static final String VALIDE_COLKEY = "VALID";
	
			public static final String ID_COLKEY = "ID";
	

		// Relationships
	
	    public static final ERXKey<org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet> OBJETS = new ERXKey<org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet>("objets");
  	
			public static final String OBJETS_KEY = OBJETS.key();
	


	// Accessors methods
			  public Boolean completable() {
	    return (Boolean) storedValueForKey(COMPLETABLE_KEY);
	  }

	  public void setCompletable(Boolean value) {
	    takeStoredValueForKey(value, COMPLETABLE_KEY);
	  }
	
			  public NSTimestamp dCreation() {
	    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	  }

	  public void setDCreation(NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	  }
	
			  public String description() {
	    return (String) storedValueForKey(DESCRIPTION_KEY);
	  }

	  public void setDescription(String value) {
	    takeStoredValueForKey(value, DESCRIPTION_KEY);
	  }
	
			  public NSTimestamp dModification() {
	    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	  }

	  public void setDModification(NSTimestamp value) {
	    takeStoredValueForKey(value, D_MODIFICATION_KEY);
	  }
	
			  public String libelle() {
	    return (String) storedValueForKey(LIBELLE_KEY);
	  }

	  public void setLibelle(String value) {
	    takeStoredValueForKey(value, LIBELLE_KEY);
	  }
	
			  public Boolean modifiable() {
	    return (Boolean) storedValueForKey(MODIFIABLE_KEY);
	  }

	  public void setModifiable(Boolean value) {
	    takeStoredValueForKey(value, MODIFIABLE_KEY);
	  }
	
			  public Integer persIdCreation() {
	    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	  }

	  public void setPersIdCreation(Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	  }
	
			  public Integer persIdModification() {
	    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	  }

	  public void setPersIdModification(Integer value) {
	    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	  }
	
			  public String stringId() {
	    return (String) storedValueForKey(STRING_ID_KEY);
	  }

	  public void setStringId(String value) {
	    takeStoredValueForKey(value, STRING_ID_KEY);
	  }
	
			  public String typeNomenclature() {
	    return (String) storedValueForKey(TYPE_NOMENCLATURE_KEY);
	  }

	  public void setTypeNomenclature(String value) {
	    takeStoredValueForKey(value, TYPE_NOMENCLATURE_KEY);
	  }
	
			  public String typeValeurs() {
	    return (String) storedValueForKey(TYPE_VALEURS_KEY);
	  }

	  public void setTypeValeurs(String value) {
	    takeStoredValueForKey(value, TYPE_VALEURS_KEY);
	  }
	
			  public Boolean valide() {
	    return (Boolean) storedValueForKey(VALIDE_KEY);
	  }

	  public void setValide(Boolean value) {
	    takeStoredValueForKey(value, VALIDE_KEY);
	  }
	
					  public NSArray<org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet> objets() {
	    return (NSArray)storedValueForKey(OBJETS_KEY);
	  }

		  public NSArray<org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet> objets(EOQualifier qualifier) {
	    return objets(qualifier, null, false);
	  }

	  public NSArray<org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet> objets(EOQualifier qualifier, boolean fetch) {
	    return objets(qualifier, null, fetch);
	  }
	
	  public NSArray<org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet> objets(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
	    NSArray results;
		    if (fetch) {
	      EOQualifier fullQualifier;
		      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet.NOMENCLATURE_KEY, EOQualifier.QualifierOperatorEqual, this);
		    	
	      if (qualifier == null) {
	        fullQualifier = inverseQualifier;
	      }
	      else {
	        NSMutableArray qualifiers = new NSMutableArray();
	        qualifiers.addObject(qualifier);
	        qualifiers.addObject(inverseQualifier);
	        fullQualifier = new EOAndQualifier(qualifiers);
	      }

		      results = org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet.fetchAll(editingContext(), fullQualifier, sortOrderings);
		    }
	    else {
		      results = objets();
	      if (qualifier != null) {
	        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	      }
	      if (sortOrderings != null) {
	        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	      }
		    }
		    return results;
	  }
	  
	  public void addToObjetsRelationship(org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet object) {
	    addObjectToBothSidesOfRelationshipWithKey(object, OBJETS_KEY);
	  }

	  public void removeFromObjetsRelationship(org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, OBJETS_KEY);
	  }

	  public org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet createObjetsRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlNomenclature_Objet");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, OBJETS_KEY);
	    return (org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet) eo;
	  }

	  public void deleteObjetsRelationship(org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, OBJETS_KEY);
		    editingContext().deleteObject(object);
		  }

	  public void deleteAllObjetsRelationships() {
	    Enumeration objects = objets().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	      deleteObjetsRelationship((org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet)objects.nextElement());
	    }
	  }

		


  
	  public EONomenclature localInstanceIn(EOEditingContext editingContext) {
	  		return (EONomenclature)ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONomenclature creerInstance(EOEditingContext editingContext) {
      return (EONomenclature)ERXEOControlUtilities.createAndInsertObject(editingContext, _EONomenclature.ENTITY_NAME);
		}

	/* Finders */

	  public static NSArray<EONomenclature> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null, null);
	  }

	  public static NSArray<EONomenclature> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EONomenclature> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	  }

	  public static NSArray<EONomenclature> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EONomenclature> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EONomenclature> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throw IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EONomenclature fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EONomenclature fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EONomenclature eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EONomenclature)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EONomenclature fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EONomenclature fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EONomenclature eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EONomenclature)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EONomenclature fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EONomenclature eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EONomenclature ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EONomenclature fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
