/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.identite;

import java.util.List;

import org.cocktail.gfc.admin.identite.support.FonctionRepository;
import org.cocktail.gfc.support.FwkCktlGFCSpringConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=FwkCktlGFCSpringConfig.class)
@ActiveProfiles("normal")
@Transactional
public class FonctionRepositoryTest {

    @Autowired
    FonctionRepository repository;
    
    static long ID = 1;
    static String APPLICATION_ID = "GFCADMIN";

    @Test
    public void repositoryPresent() {
        Assert.notNull(repository);
    }

    @Test
    public void findFonction() {
        List<Fonction> fonctions = repository.findAll();
        Assert.notNull(fonctions, "Liste de fonctions null ! Elle pourrait être vide mais pas null.");
    }
	
    @Test
    public void findFonctionByApplication() {
        List<Fonction> fonctions = repository.findByApplication(APPLICATION_ID);
        Assert.notNull(fonctions, "Liste de fonctions null ! Elle pourrait être vide mais pas null.");
        int nb = fonctions.size();
        int all = repository.findAll().size();
        Assert.isTrue(all >= nb, "La liste de toutes les fonctions n'est pas plus grande que la liste des fonctions liée un l'application " + APPLICATION_ID + " !");
    }
	
    @Test
    public void findFonctionByUtilisateur() {
    	List<Fonction> fonctions = repository.findByUtilisateur(ID);
        Assert.notNull(fonctions, "Liste de fonctions null ! Elle pourrait être vide mais pas null.");
        int nb = fonctions.size();
        int all = repository.findAll().size();
        Assert.isTrue(all >= nb, "La liste de toutes les fonctions n'est pas plus grande que la liste des fonctions liée un l'utilisateur " + ID + " !");
    }

    @Test
    public void findByUtilisateurAndApplication() {
    	List<Fonction> fonctions = repository.findByUtilisateurAndApplication(ID, APPLICATION_ID);
        Assert.notNull(fonctions, "Liste de fonctions null ! Elle pourrait être vide mais pas null.");
        int nb = fonctions.size();
        int all = repository.findAll().size();
        Assert.isTrue(all >= nb, "La liste de toutes les fonctions n'est pas plus grande que la liste des fonctions liée un l'utilisateur et l'application !");
        int nb2 = repository.findByApplication(APPLICATION_ID).size();
        Assert.isTrue(nb2 >= nb, "La liste de toutes les fonctions de l'application n'est pas plus grande que la liste des fonctions liée un l'utilisateur et l'application !");
        int nb3 = repository.findByUtilisateur(ID).size();
        Assert.isTrue(nb3 >= nb, "La liste de toutes les fonctions de l'utilisateur n'est pas plus grande que la liste des fonctions liée un l'utilisateur et l'application !");
    }

}
