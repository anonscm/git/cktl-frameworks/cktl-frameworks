/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.comptabudgetaire.support.querydsl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.cocktail.common.support.CktlGFCDate;
import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.comptabudgetaire.ComptaBudgetaireEventMiseEnExecutionService;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEvent;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventHash;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventMessageLigne.TypeEcritureEnum;
import org.cocktail.gfc.comptabudgetaire.beans.ComptaBudgetaireEventType;
import org.cocktail.gfc.comptabudgetaire.support.ComptaBudgetaireEcritureRepository;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.support.FwkCktlGFCSpringConfig;
import org.cocktail.gfc.support.exception.GfcValidationException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FwkCktlGFCSpringConfig.class)
@ActiveProfiles("normal")
@Transactional
public class QueryDSLComptaBudgetaireEventRepositoryTest {

    @Autowired
    private QueryDSLComptaBudgetaireEventRepository repo;

    @Autowired
    private ComptaBudgetaireEcritureRepository ecritureRepo;

    @Autowired
    private ComptaBudgetaireEventMiseEnExecutionService comptaBudgetaireEventMiseEnExecutionService;

    @Test
    public void confOK() {
        Assert.assertNotNull(repo);
    }

    @Test(expected = TriggerRuntimeException.class)
    public void createAvecEventTropVide() {
        ComptaBudgetaireEvent event = ComptaBudgetaireEvent.builder()
                .cptbudEventType(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION)
                .srcHash(getUnqSrcHash())
                .build();
        try {
            repo.createComptaBudgetaireEvent(event);
        } catch (DataIntegrityViolationException e) {
            assertExceptionFromTriggerError(e);
        }
    }

    @Test
    public void createAvecEventOK() throws JsonProcessingException, GfcValidationException {
        ComptaBudgetaireEvent event = getOneEvent();
        try {
            repo.createComptaBudgetaireEvent(event);
        } catch (DataIntegrityViolationException e) {
            // Ok pour cette exeption 
        }
    }

    // TODO RPR 16/03/2015 : les contraintes sur les foreign key inexistantes
    // sont
    // declenchées, difficile de tester la creation des evenements.
    @Ignore
    @Test
    public void verifEnregistrementDesEcritures() throws JsonProcessingException, GfcValidationException {
        ComptaBudgetaireEvent event = getOneEvent();
        Long nbAvant = ecritureRepo.count();
        repo.createComptaBudgetaireEvent(event);
        Long nbApres = ecritureRepo.count();
        Assert.assertTrue("On devrait avoir une ligne de plus dans la table des écritures !", (nbApres == nbAvant + 1));
    }

    private ComptaBudgetaireEvent getOneEvent() throws JsonProcessingException, GfcValidationException {
        List<ComptaBudgetaireEventMessageLigne> lignes = new ArrayList<ComptaBudgetaireEventMessageLigne>();
        ComptaBudgetaireEventMessageLigne ligneAECredit = comptaBudgetaireEventMiseEnExecutionService.creerEvtMessageLigneDepAECredit(2015,
                TypeEcritureEnum.BI, new EntiteBudgetaireId(1L), new OperationId(1L), new Montant(1d), new NatureDepenseId(1L), new DestinationDepenseId(1L));
        lignes.add(ligneAECredit);
        ComptaBudgetaireEvent event = ComptaBudgetaireEvent.builder().libelle("test")
                .cptbudEventType(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION)
                .message(comptaBudgetaireEventMiseEnExecutionService.convertToJson(lignes)).srcAppName("Tests").srcHash(getUnqSrcHash())
                .dCreation(CktlGFCDate.today().toDateWithoutTime()).persIdCreation(13L).build();
        return event;
    }

    private ComptaBudgetaireEventHash getUnqSrcHash() {
        Random r = new Random(10);
        Integer id = r.nextInt();
        return ComptaBudgetaireEventHash.valueOf(ComptaBudgetaireEventType.Constants.MISE_EN_EXECUTION.getKey() + "$test/" + "." + id + "$"
                + CktlGFCDate.today().toDateWithTime());
    }

    private void assertExceptionFromTriggerError(DataIntegrityViolationException e) {
        List<String> mess = Arrays.asList(e.getMostSpecificCause().getMessage().split("\\r?\\n"));
        for (String mes : mess) {
            if (mes.matches("ORA-04088.*CPTBUD_EVENT_LISTENER'")) {
                throw new TriggerRuntimeException();
            }
        }
        throw new RuntimeException("Le trigger devrait provoquer une erreur !");
    }

}
