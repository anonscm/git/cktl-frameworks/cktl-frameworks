package org.cocktail.gfc.operation.support.querydsl;

import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TrancheId;
import org.cocktail.gfc.support.FwkCktlGFCSpringConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FwkCktlGFCSpringConfig.class)
@ActiveProfiles("normal")
@Transactional
public class QueryDSLTrancheOperationRepositoryTest {

    @Autowired
    private QueryDSLTrancheOperationRepository repository;
    
    @Test
    public void repositoryPresent() {
        Assert.assertNotNull(repository);
    }

    @Test
    public void findById() {
        TrancheId id = new TrancheId(1L);
        repository.findById(id );
    }
    
    @Test
    public void findByOperation() {
        OperationId operationId = new OperationId(1L);
        repository.findByOperation(operationId );
    }
}
