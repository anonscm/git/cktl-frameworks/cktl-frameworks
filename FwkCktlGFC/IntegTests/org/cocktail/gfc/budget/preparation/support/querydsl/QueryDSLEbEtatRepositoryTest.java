/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.TypeEtatService;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.EbEtat;
import org.cocktail.gfc.budget.preparation.EbEtatId;
import org.cocktail.gfc.support.FwkCktlGFCSpringConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FwkCktlGFCSpringConfig.class)
@ActiveProfiles("normal")
@Transactional
public class QueryDSLEbEtatRepositoryTest {

    @Autowired
    @Spy
    private QueryDSLEbEtatRepository repository;

    private long ebEtat1Id;
    private Integer niveauEbEtat1 = new Integer(1);
    private BudgetId budId = new BudgetId(1L);
    private EntiteBudgetaireId ebId1 = new EntiteBudgetaireId(1L);
    private EntiteBudgetaireId ebId2 = new EntiteBudgetaireId(1L);

    @Before
    public void initRepositories() {
        MockitoAnnotations.initMocks(this);
        Mockito.doReturn(niveauEbEtat1).when(repository).getNiveauEb(ebId1);
    }

    @Test
    public void repositoryPresent() {
        Assert.assertNotNull(repository);
    }

    @Test
    public void addBudgetEbEtat() {
        EbEtat ebEtat1 = new EbEtat(null, budId, ebId1, TypeEtatService.Code.NON_RENSEIGNEE.etat());
        EbEtat ebEtat2 = new EbEtat(null, budId, ebId2, TypeEtatService.Code.NON_RENSEIGNEE.etat());
        try {
            ebEtat1Id = repository.addBudgetEbEtat(ebEtat1);
            repository.addBudgetEbEtat(ebEtat2);
        } catch (DataIntegrityViolationException e) {
            // Ok pour cette exeption dans la mesure où l'on met des ID bidons
        }
    }

    @Test
    public void updateBudgetEbEtat() {
        EbEtat ebEtat = new EbEtat(ebEtat1Id, budId, ebId1, TypeEtatService.Code.VERROUILLEE.etat());
        repository.updateBudgetEbEtat(ebEtat);
    }

    @Test
    public void findBudgetEbEtaById() {
        repository.findBudgetEbEtaById(new EbEtatId(ebEtat1Id));
    }

    @Test
    public void findByBudgetIdAndEntiteBudgetaireId() {
        repository.findByBudgetIdAndEntiteBudgetaireId(budId, ebId1);
    }

    @Test
    public void findChildrenByBudgetIdAndEntiteBudgetaireId() {
        repository.findChildrenByBudgetIdAndEntiteBudgetaireId(budId, ebId1);
    }

    @Test
    public void findSubTreeByBudgetIdAndEbId() {
        repository.findSubTreeByBudgetIdAndEbId(budId, ebId1);
    }

    @Test
    public void updateBudgetEbEtatList() {
        List<EntiteBudgetaireId> listeEbId = new ArrayList<EntiteBudgetaireId>();
        listeEbId.add(ebId1);
        listeEbId.add(ebId2);
        repository.updateBudgetEbEtatList(budId, listeEbId, TypeEtatService.Code.CONTROLEE.etat());
    }
}
