/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.util.Date;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.TypeEtatService;
import org.cocktail.gfc.budget.preparation.Budget;
import org.cocktail.gfc.budget.preparation.BudgetFactory;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.CalendrierBudget;
import org.cocktail.gfc.budget.preparation.EbEtat;
import org.cocktail.gfc.budget.preparation.EtatBudget;
import org.cocktail.gfc.budget.preparation.VersionBudget;
import org.cocktail.gfc.budget.preparation.VersionBudgetCode;
import org.cocktail.gfc.support.FwkCktlGFCSpringConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FwkCktlGFCSpringConfig.class)
@ActiveProfiles("normal")
@Transactional
public class QueryDSLBudgetRepositoryTest {

    @Autowired
    private QueryDSLBudgetRepository repository;

    private int exercice = 2015;
    private BudgetId budId1;
    private BudgetId budId2;
    private Date now = new Date();
    private Budget bud1 = Budget.builder().exeOrdre(exercice).numeroBudget(0L).libelle("Budget 1").dateCreation(now).persIdCreateur(4L)
            .etatBudget(EtatBudget.NON_APPROUVE)
            .versionBudget(new VersionBudget(10000l, VersionBudgetCode.BI.toString(), "")).calendrierBudget(new CalendrierBudget()).build();
    private Budget bud2 = Budget.builder().exeOrdre(exercice).numeroBudget(1L).libelle("Budget 1").dateCreation(now).persIdCreateur(4L)
            .etatBudget(EtatBudget.OUVERT)
            .versionBudget(new VersionBudget(10001l, VersionBudgetCode.BR.toString(), "")).calendrierBudget(new CalendrierBudget()).build();

    @Before
    public void initRepositories() {
        try {
            bud1 = repository.create(bud1);
            bud2 = repository.create(bud2);
            budId1 = bud1.getIdBudget();
            budId2 = bud2.getIdBudget();
        } catch (DataIntegrityViolationException e) {
            // Ok pour cette exeption dans la mesure où l'on met des ID bidons
        }
    }

    @Test
    public void repositoryPresent() {
        Assert.assertNotNull(repository);
    }

    @Test
    public void findAll() {
        List<Budget> liste = repository.findAll();
        Assert.assertNotNull(liste);
    }

    @Test
    public void findByExercice() {
        List<Budget> liste = repository.findByExercice(exercice);
        Assert.assertNotNull(liste);
    }

    @Test
    public void findById() {
        repository.findById(budId1);
    }

    @Test
    public void findByIdNumber() {
        repository.findById(budId1.getId());
    }

    @Test
    public void countByExercice() {
        repository.countByExercice(exercice);
    }

    @Test
    public void countByExerciceAndVersion() {
        repository.countByExerciceAndVersion(exercice, VersionBudgetCode.BI.toString());
    }

    @Test
    public void enregistrerBudget() {
        bud1.setLibelle("bud1Modif");
        repository.enregistrerBudget(bud1, 4L);
    }

}
