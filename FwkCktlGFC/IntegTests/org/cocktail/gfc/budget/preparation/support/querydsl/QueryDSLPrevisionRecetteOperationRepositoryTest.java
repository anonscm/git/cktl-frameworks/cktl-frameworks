/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.budget.preparation.support.querydsl;

import java.util.Date;

import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaireId;
import org.cocktail.gfc.budget.preparation.BudgetId;
import org.cocktail.gfc.budget.preparation.CombinaisonAxes;
import org.cocktail.gfc.budget.preparation.CombinaisonAxesRecette;
import org.cocktail.gfc.budget.preparation.PrevisionDepenseAeOperation;
import org.cocktail.gfc.budget.preparation.PrevisionRecetteOperation;
import org.cocktail.gfc.budget.preparation.support.querydsl.QueryDSLPrevisionDepenseAeOperationRepository;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TrancheDepenseAeId;
import org.cocktail.gfc.operation.TrancheId;
import org.cocktail.gfc.operation.TrancheRecetteId;
import org.cocktail.gfc.support.FwkCktlGFCSpringConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FwkCktlGFCSpringConfig.class)
@ActiveProfiles("normal")
@Transactional
public class QueryDSLPrevisionRecetteOperationRepositoryTest {

    @Autowired
    QueryDSLPrevisionRecetteOperationRepository repository;

    @Test
    public void repositoryPresent() {
        Assert.notNull(repository);
    }

    @Test
    public void createPrevisionDepenseOperation() {
        PrevisionRecetteOperation previsionRecetteOperation = PrevisionRecetteOperation.builder()
                .combinaison(CombinaisonAxesRecette.builder()
                        .entiteBudgetaireId(new EntiteBudgetaireId(1L))
                        .sectionRecette(new SectionRecette("F", "Fonctionnement"))
                        .natureRecetteId(new NatureRecetteId(1L))
                        .origineRecetteId(new OrigineRecetteId(1L))
                        .operationId(new OperationId(1L))
                        .build())
                .budgetId(new BudgetId(1L))
                .trancheRecetteId(new TrancheRecetteId(1L))
                .montantRecette(Montant.ZERO)
                .montantTitre(Montant.ZERO)
                .persIdCreation(1L)
                .dateCreation(new Date())
                .build();
        try {
            repository.createPrevisionRecetteOperation(previsionRecetteOperation);
        } catch (DataIntegrityViolationException e) {
            // Ok pour cette exeption dans la mesure où l'on met des ID bidons
        }
    }

    @Test
    public void createPrevisionDepenseOperationOnIntegrationTranche() {
        try {
            repository.createPrevisionRecetteOperationOnIntegrationTranche(
                    new TrancheId(1L), new BudgetId(1L), 1L);
        } catch (DataIntegrityViolationException e) {
            // Ok pour cette exeption dans la mesure où l'on met des ID bidons
        }
    }

    @Test
    public void removeByTrancheAndBudget() {
        repository.removeByTrancheAndBudget(new TrancheId(1L), new BudgetId(1L));
    }

}
