package org.cocktail.gfc.operation;

import org.cocktail.gfc.common.montant.Montant;

/**
 * @author bourges
 *
 */
public interface OperationService {
    
    /**
     * @param operation - opération de référence
     * @param exercice - exercice de référence
     * @param nature - se restreindre à cette nature de dépese (toutes les natures si null)
     * @return le montant des AE prévues en N + 1
     */
    public Montant getAEprevuesEnNPlus1(Operation operation, int exercice, String nature);

    /**
     * @param operation - opération de référence
     * @param exercice - exercice de référence
     * @param nature - se restreindre à cette nature de dépese (toutes les natures si null)
     * @return le montant des AE prévues en N + 2
     */
    public Montant getAEprevuesEnNPlus2(Operation operation, int exercice, String nature);
    
    /**
     * @param operation - opération de référence
     * @param exercice - exercice de référence
     * @param nature - se restreindre à cette nature de dépese (toutes les natures si null)
     * @return le montant des AE prévues en N + 3 et suivants
     */
    public Montant getAEprevuesEnNPlus3EtSuivants(Operation operation, int exercice, String nature);

    /**
     * @param operation - opération de référence
     * @param exercice - exercice de référence
     * @param nature - se restreindre à cette nature de dépese (toutes les natures si null)
     * @return le montant des CP prévus en N + 1
     */
    public Montant getCPprevuesEnNPlus1(Operation operation, int exercice, String nature);

    /**
     * @param operation - opération de référence
     * @param exercice - exercice de référence
     * @param nature - se restreindre à cette nature de dépese (toutes les natures si null)
     * @return le montant des CP prévus en N + 2
     */
    public Montant getCPprevuesEnNPlus2(Operation operation, int exercice, String nature);

    /**
     * @param operation - opération de référence
     * @param exercice - exercice de référence
     * @param nature - se restreindre à cette nature de dépese (toutes les natures si null)
     * @return le montant des CP prévus en N + 2 et suivants
     */
    public Montant getCPprevuesEnNPlus3EtSuivants(Operation operation, int exercice, String nature);

}
