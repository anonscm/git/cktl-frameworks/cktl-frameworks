/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.operation;

import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.Builder;

import org.cocktail.gfc.common.montant.Montant;

@Getter
public class Operation implements Comparable<Operation> {

    private OperationId operationId;
    private String libelle;
    private boolean flechee;
    private TypeOperation type;
    private String code; //au format OPE-AAAA-nnnn
    private boolean inclusePPIEtablissement;
    private boolean incluseTableauxBudgetaires;
    private Montant montantHT;
    private Montant montantNet;
    
    public Operation(OperationId operationId, String libelle, boolean estFlechee, TypeOperation type) {
        this.operationId = operationId;
        this.libelle = libelle;
        this.flechee = estFlechee;
        this.type = type;
    }
    
    public Operation(OperationId operationId, String libelle, int estFlechee, TypeOperation type) {
        this.operationId = operationId;
        this.libelle = libelle;
        this.flechee = (estFlechee == 1);
        this.type = type;
    }
    
    @Builder
    public Operation(@NonNull OperationId operationId,
            @NonNull String libelle, 
            @NonNull boolean flechee, 
            @NonNull TypeOperation type, 
            @NonNull String code, 
            @NonNull boolean inclusePPIEtablissement,
            @NonNull boolean incluseTableauxBudgetaires, 
            @NonNull Montant montantHT, 
            @NonNull Montant montantNet) {
        this.operationId = operationId;
        this.libelle = libelle;
        this.flechee = flechee;
        this.type = type;
        this.code = code;
        this.inclusePPIEtablissement = inclusePPIEtablissement;
        this.incluseTableauxBudgetaires = incluseTableauxBudgetaires;
        this.montantHT = montantHT;
        this.montantNet = montantNet;
    }

    public int compareTo(Operation o) {
        return operationId.getId().compareTo(o.operationId.getId());
    }
 
}
