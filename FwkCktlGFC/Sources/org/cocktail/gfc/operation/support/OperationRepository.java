package org.cocktail.gfc.operation.support;

import java.util.List;

import org.cocktail.gfc.operation.Operation;
import org.cocktail.gfc.operation.OperationId;

public interface OperationRepository {
    
    Operation findById(OperationId id);

    List<Operation> findByExercice(int exercice);

}
