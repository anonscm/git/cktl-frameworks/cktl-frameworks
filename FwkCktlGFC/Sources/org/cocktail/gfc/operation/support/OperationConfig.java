/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.operation.support;

import org.cocktail.gfc.operation.CreditsService;
import org.cocktail.gfc.operation.OperationService;
import org.cocktail.gfc.operation.controller.OperationFacade;
import org.cocktail.gfc.operation.controller.SyntheseOperationFacade;
import org.cocktail.gfc.operation.support.querydsl.QueryDSLCountRepository;
import org.cocktail.gfc.operation.support.querydsl.QueryDSLDetailOperationDepenseRepository;
import org.cocktail.gfc.operation.support.querydsl.QueryDSLDetailOperationRecetteRepository;
import org.cocktail.gfc.operation.support.querydsl.QueryDSLOperationRepository;
import org.cocktail.gfc.operation.support.querydsl.QueryDSLSyntheseOperationRepository;
import org.cocktail.gfc.operation.support.querydsl.QueryDSLTrancheOperationRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

@Configuration
public class OperationConfig {
    
    @Bean
    public OperationService operationService(
            TrancheOperationRepository trancheOperationRepository,
            DetailOperationDepenseRepository detailOperationDepenseRepository) {
        return new OperationServiceImpl(trancheOperationRepository, detailOperationDepenseRepository);
    }
    
    @Bean
    public OperationRepository operationRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLOperationRepository(template);
    }

    @Bean
    public CountRepository opeCountRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLCountRepository(template);
    }
    
    @Bean 
    public CreditsService opeCreditsService(CountRepository countRepository) {
        return new CreditsService(countRepository);
    }
    
    @Bean
    public SyntheseOperationRepository syntheseOperationRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLSyntheseOperationRepository(template);
    }
	
	@Bean
	public SyntheseOperationFacade syntheseOperationFacade(SyntheseOperationRepository syntheseOperationRepository) {
		return new SyntheseOperationFacade(syntheseOperationRepository);
	}
	
	@Bean
	public TrancheOperationRepository trancheRepository(QueryDslJdbcTemplate template) {
	    return new QueryDSLTrancheOperationRepository(template);
	}
	
	@Bean
    public DetailOperationDepenseRepository detailOperationDepenseRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLDetailOperationDepenseRepository(template);
    }
	
    @Bean
    public DetailOperationRecetteRepository detailOperationRecetteRepository(QueryDslJdbcTemplate template) {
        return new QueryDSLDetailOperationRecetteRepository(template);
    }
    
    @Bean
	public OperationFacade operationFacade(DetailOperationDepenseRepository detailOperationDepenseRepository, DetailOperationRecetteRepository detailOperationRecetteRepository, TrancheOperationRepository trancheRepository) {
		return new OperationFacade(detailOperationDepenseRepository, detailOperationRecetteRepository, trancheRepository);
	}
}