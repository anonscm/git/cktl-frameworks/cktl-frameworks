package org.cocktail.gfc.operation.support;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.DetailOperationDepense;
import org.cocktail.gfc.operation.Operation;
import org.cocktail.gfc.operation.OperationService;
import org.cocktail.gfc.operation.TrancheOperation;

public class OperationServiceImpl implements OperationService {
    
    private enum Dep {AE, CP};
    private TrancheOperationRepository trancheOperationRepository;
    private DetailOperationDepenseRepository detailOperationDepenseRepository;

    public OperationServiceImpl(TrancheOperationRepository trancheOperationRepository,
            DetailOperationDepenseRepository detailOperationDepenseRepository) {
        this.trancheOperationRepository = trancheOperationRepository;
        this.detailOperationDepenseRepository = detailOperationDepenseRepository;
    }

    public Montant getAEprevuesEnNPlus1(Operation operation, int exercice, String nature) {
        return getMontant(operation, exercice, 1, 1, Dep.AE, nature);
    }

    public Montant getAEprevuesEnNPlus2(Operation operation, int exercice, String nature) {
        return getMontant(operation, exercice, 2, 2, Dep.AE, nature);
    }

    public Montant getAEprevuesEnNPlus3EtSuivants(Operation operation, int exercice, String nature) {
        return getMontant(operation, exercice, 3, Integer.MAX_VALUE, Dep.AE, nature);
    }

    public Montant getCPprevuesEnNPlus1(Operation operation, int exercice, String nature) {
        return getMontant(operation, exercice, 1, 1, Dep.CP, nature);
    }

    public Montant getCPprevuesEnNPlus2(Operation operation, int exercice, String nature) {
        return getMontant(operation, exercice, 2, 2, Dep.CP, nature);
    }

    public Montant getCPprevuesEnNPlus3EtSuivants(Operation operation, int exercice, String nature) {
        return getMontant(operation, exercice, 3, Integer.MAX_VALUE, Dep.CP, nature);
    }

    private Montant getMontant(Operation operation, int exercice, int min, int max, Dep dep, String nature) {
        boolean allNatures = (nature == null);
        BigDecimal ret = new BigDecimal(0);
        List<TrancheOperation> tranches = trancheOperationRepository.findByOperation(operation.getOperationId());
        for (TrancheOperation tranche : tranches) {
            if ((tranche.getExeOrdre() >= exercice + min) && (tranche.getExeOrdre() <= exercice + max)) {
                //FIXME RB : DetailOperationDepense n'est pas ce que je pensais une ligne de QOpeTrancheBudDepAe !
                List<DetailOperationDepense> depenses = detailOperationDepenseRepository.findDetailOperationDepenseByTrancheId(tranche.getId());
                for (DetailOperationDepense depense : depenses) {
                    if (allNatures || depense.getNature().getCode().equals(nature)) {
                        if (dep == Dep.AE) {
                            ret.add(depense.getMontantAE().value());
                        } 
                        if (dep == Dep.CP) {
                            ret.add(depense.getMontantCP().value());
                        }
                    }
                }
            }
        }
        return new Montant(ret);
    }

}
