/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.operation.support.querydsl;

import static com.mysema.query.support.Expressions.as;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.gfc.operation.SyntheseOperation;
import org.cocktail.gfc.operation.support.SyntheseOperationRepository;
import org.cocktail.gfc.support.querydsl.QAdmTypeEtat;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBud;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudDepAe;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudDepCp;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudRec;
import org.cocktail.gfc.support.querydsl.QOpeTypeOperation;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Projections;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.query.ListSubQuery;

public class QueryDSLSyntheseOperationRepository implements SyntheseOperationRepository {

	// Tables
	private final QOpeOperation operation = QOpeOperation.opeOperation;
	private final QOpeTypeOperation typeOperation = QOpeTypeOperation.opeTypeOperation;
	private final QOpeTrancheBud tranche = QOpeTrancheBud.opeTrancheBud;
	private final QOpeTrancheBudDepAe trancheDepAE = QOpeTrancheBudDepAe.opeTrancheBudDepAe;
	private final QOpeTrancheBudDepCp trancheDepCP = QOpeTrancheBudDepCp.opeTrancheBudDepCp;
	private final QOpeTrancheBudRec trancheRec = QOpeTrancheBudRec.opeTrancheBudRec;
	private final QAdmTypeEtat typeEtat = QAdmTypeEtat.admTypeEtat;
	
	private final NumberPath<Long> idOpeTrancheBudTmp = Expressions.numberPath(Long.class, "idOpeTrancheBudTmp");
	private final NumberPath<BigDecimal> montantAeSum = Expressions.numberPath(BigDecimal.class, "montantAeSum");
	private final NumberPath<BigDecimal> montantCpSum = Expressions.numberPath(BigDecimal.class, "montantCpSum");
	private final NumberPath<BigDecimal> montantBudSum = Expressions.numberPath(BigDecimal.class, "montantBudSum");
	private PathBuilder<Tuple> sousReqPath = new PathBuilder<Tuple>(Tuple.class, "sr");
    
	// Cases expressions
	Expression<Boolean> estValide = typeEtat.tyetLibelle
	    .when("VALIDE").then(true)
	    .otherwise(false);
	
	// Projections
	//private Expression<SyntheseOperation> factorySyntheseOperation = Projections.constructor(SyntheseOperation.class, tranche.idOpeOperation, tranche.idOpeTrancheBud, operation.llOperation, tranche.exeOrdre, montantAeSum, montantCpSum, montantBudSum, estValide);
	private QueryDSLSyntheseOperationProjection projection = new QueryDSLSyntheseOperationProjection();
	
	private QueryDslJdbcTemplate template;
    
	public QueryDSLSyntheseOperationRepository(QueryDslJdbcTemplate template) {
        this.template = template;
    }
    
	public List<SyntheseOperation> findTrancheOperationByExercice(Integer exercice) {
		SQLQuery query = addByExerciceClause(getSyntheseOperationQuery(), exercice);
		return template.query(query, projection);
	}
	
	// Queries
	private ListSubQuery<Tuple> getSommeTrancheDepAe() {
		SQLSubQuery subQ = new SQLSubQuery();
		return subQ.from(trancheDepAE)
				.groupBy(trancheDepAE.idOpeTrancheBud)
				.list(trancheDepAE.idOpeTrancheBud.as(idOpeTrancheBudTmp), trancheDepAE.montantAe.sum().as(montantAeSum), as(ConstantImpl.create(BigDecimal.ZERO), montantCpSum), as(ConstantImpl.create(BigDecimal.ZERO), montantBudSum));
	}
	
	   private ListSubQuery<Tuple> getSommeTrancheDepCp() {
	        SQLSubQuery subQ = new SQLSubQuery();
	        return subQ.from(trancheDepCP)
	                .groupBy(trancheDepCP.idOpeTrancheBud)
	                .list(trancheDepCP.idOpeTrancheBud.as(idOpeTrancheBudTmp), as(ConstantImpl.create(BigDecimal.ZERO), montantAeSum), trancheDepCP.montantCp.sum().as(montantCpSum), as(ConstantImpl.create(BigDecimal.ZERO), montantBudSum));
	    }

	private ListSubQuery<Tuple> getSommeTrancheRec() {
		SQLSubQuery subQ = new SQLSubQuery();
		return subQ.from(trancheRec)
				.groupBy(trancheRec.idOpeTrancheBud)
				.list(trancheRec.idOpeTrancheBud.as(idOpeTrancheBudTmp), as(ConstantImpl.create(BigDecimal.ZERO), montantAeSum), as(ConstantImpl.create(BigDecimal.ZERO), montantCpSum), trancheRec.montantRec.sum().as(montantBudSum));
	}
	
	private ListSubQuery<Tuple> getSommeTranche() {
	    SQLSubQuery subQ = new SQLSubQuery();	    
	    return subQ.from(new SQLSubQuery().union(getSommeTrancheDepAe(), getSommeTrancheDepCp(), getSommeTrancheRec()).as(sousReqPath))
	            .groupBy(idOpeTrancheBudTmp)
	            .list(idOpeTrancheBudTmp, montantAeSum.sum().as(montantAeSum), montantCpSum.sum().as(montantCpSum), montantBudSum.sum().as(montantBudSum));
	}
	
	private SQLQuery getSyntheseOperationQuery() {
		return template.newSqlQuery()
				.from(operation)
				.join(tranche)				
                .on(operation.idOpeOperation.eq(tranche.idOpeOperation))
                .join(typeOperation)
                .on(operation.idOpeTypeOperation.eq(typeOperation.idOpeTypeOperation))
                .join(typeEtat)
                .on(tranche.tyetId.eq(typeEtat.tyetId))
                .from(getSommeTranche().as(sousReqPath))
				.where(tranche.idOpeTrancheBud.eq(idOpeTrancheBudTmp))
				.orderBy(operation.idOpeOperation.asc());
	}
		
	private SQLQuery addByExerciceClause(SQLQuery sqlQuery, Integer exercice) {
		return sqlQuery.where(tranche.exeOrdre.eq(exercice));
	}

}