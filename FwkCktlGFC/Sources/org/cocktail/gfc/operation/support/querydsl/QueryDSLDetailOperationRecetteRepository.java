/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.operation.support.querydsl;

import java.util.List;

import org.cocktail.gfc.admin.nomenclature.NatureRecette;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecette;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLEntiteBudgetaireProjection;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLSectionRecetteCacheProjection;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.DetailOperationRecette;
import org.cocktail.gfc.operation.TrancheId;
import org.cocktail.gfc.operation.support.DetailOperationRecetteRepository;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QAdmNatureRec;
import org.cocktail.gfc.support.querydsl.QAdmOrigineRecette;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBud;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudRec;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Projections;

public class QueryDSLDetailOperationRecetteRepository implements DetailOperationRecetteRepository {
    // Tables
    private final QOpeOperation operation = QOpeOperation.opeOperation;
    private final QOpeTrancheBud tranche = QOpeTrancheBud.opeTrancheBud;
    private final QOpeTrancheBudRec trancheRec = QOpeTrancheBudRec.opeTrancheBudRec;
    private final QAdmEbExer ebExer = new QAdmEbExer("ebex");
    private final QAdmEb eb = new QAdmEb("eb");
    private final QAdmOrigineRecette origineRecette = QAdmOrigineRecette.admOrigineRecette;
    private final QAdmNatureRec natureRecette = QAdmNatureRec.admNatureRec;

    // Projections
    private QueryDSLEntiteBudgetaireProjection projectionEb = new QueryDSLEntiteBudgetaireProjection();
    private QueryDSLSectionRecetteCacheProjection queryDSLSectionRecetteCacheProjection = new QueryDSLSectionRecetteCacheProjection(
            trancheRec.section);

    private final Expression<OrigineRecette> constructeurOrigineRecette = Projections.constructor(
            OrigineRecette.class,
            Projections.constructor(OrigineRecetteId.class, origineRecette.idAdmOrigineRecette),
            origineRecette.code, origineRecette.libelle, origineRecette.abreviation);

    private final Expression<NatureRecette> constructeurNatureRec = Projections.constructor(
            NatureRecette.class,
            Projections.constructor(NatureRecetteId.class, natureRecette.idAdmNatureRec), natureRecette.code, natureRecette.libelle,
            natureRecette.tyetId);

    private final Expression<DetailOperationRecette> constructeurDetailOperationRecette = Projections.constructor(DetailOperationRecette.class,
            operation.idOpeOperation, tranche.idOpeTrancheBud, trancheRec.idOpeTrancheBudRec,
            projectionEb, constructeurOrigineRecette, constructeurNatureRec, trancheRec.montantRec,
            queryDSLSectionRecetteCacheProjection, trancheRec.commentaire,
            Projections.constructor(Montant.class, trancheRec.montantTitre));

    private QueryDslJdbcTemplate template;

    public QueryDSLDetailOperationRecetteRepository(QueryDslJdbcTemplate template) {
        this.template = template;
    }

    public List<DetailOperationRecette> findDetailOperationRecetteByTrancheId(TrancheId trancheId) {
        SQLQuery query = addByTrancheClause(getDetailOperationRecetteQuery(), trancheId.getId());
        return template.query(query, constructeurDetailOperationRecette);
    }

    // Queries
    private SQLQuery getDetailOperationRecetteQuery() {
        return template.newSqlQuery().from(operation).from(tranche).from(trancheRec).from(ebExer).from(eb).from(origineRecette).from(natureRecette)
                .where(operation.idOpeOperation.eq(tranche.idOpeOperation)
                        .and(tranche.idOpeTrancheBud.eq(trancheRec.idOpeTrancheBud))
                        .and(trancheRec.idAdmEb.eq(eb.idAdmEb))
                        .and(ebExer.idAdmEb.eq(eb.idAdmEb))
                        .and(ebExer.exeOrdre.intValue().eq(tranche.exeOrdre))
                        .and(origineRecette.idAdmOrigineRecette.eq(trancheRec.idAdmOrigineRecette))
                        .and(natureRecette.idAdmNatureRec.eq(trancheRec.idAdmNatureRec)));
    }

    private SQLQuery addByTrancheClause(SQLQuery sqlQuery, Long idTranche) {
        return sqlQuery.where(tranche.idOpeTrancheBud.eq(idTranche));
    }

}
