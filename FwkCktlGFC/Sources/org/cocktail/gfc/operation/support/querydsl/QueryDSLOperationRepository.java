/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.operation.support.querydsl;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.Operation;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TypeOperation;
import org.cocktail.gfc.operation.TypeOperationId;
import org.cocktail.gfc.operation.support.OperationRepository;
import org.cocktail.gfc.support.querydsl.QAvenant;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBud;
import org.cocktail.gfc.support.querydsl.QOpeTypeOperation;
import org.cocktail.gfc.support.querydsl.QVOpeNumeroEtLibelle;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.MappingProjection;
import com.mysema.query.types.path.NumberPath;

/**
 * @author bourges
 *
 */
public class QueryDSLOperationRepository implements OperationRepository {
    
    private QueryDslJdbcTemplate template;
    private QOpeOperation operation = QOpeOperation.opeOperation;
    private QVOpeNumeroEtLibelle numeroEtLibelle = QVOpeNumeroEtLibelle.vOpeNumeroEtLibelle;
    private QOpeTypeOperation typeOperation = QOpeTypeOperation.opeTypeOperation;
    private QAvenant avenant = QAvenant.avenant;  
    private QAvenant avenant2 = new QAvenant("AVENANT2");
    private QOpeTrancheBud trancheBud = QOpeTrancheBud.opeTrancheBud;
    private final NumberPath<BigDecimal> montantHtSum = Expressions.numberPath(BigDecimal.class, "montantHtSum");
    private OperationProjection projection = new OperationProjection();
    
    public QueryDSLOperationRepository(QueryDslJdbcTemplate template) {
        this.template = template;
    }

    public Operation findById(OperationId operationId) {
        Operation ret = null;
        SQLQuery sqlQuery = getBaseQuery().where(operation.idOpeOperation.eq(operationId.getId()));
        return template.queryForObject(sqlQuery, projection);
    }

    public List<Operation> findByExercice(int exercice) {
        SQLQuery sqlQuery = getBaseQuery()
                .join(trancheBud).on(operation.idOpeOperation.eq(trancheBud.idOpeOperation))
                .where(trancheBud.exeOrdre.eq(exercice));
        return template.query(sqlQuery, projection);
    }

    private SQLQuery getBaseQuery() {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(operation)
                .innerJoin(numeroEtLibelle).on(operation.idOpeOperation.eq(numeroEtLibelle.idOpeOperation))
                .innerJoin(typeOperation).on(operation.idOpeTypeOperation.eq(typeOperation.idOpeTypeOperation))
                .innerJoin(avenant).on(operation.idOpeOperation.eq(avenant.idOpeOperation))
                .innerJoin(avenant2).on(operation.idOpeOperation.eq(avenant2.idOpeOperation))
                .where(typeOperation.codeTypeOperation.ne("AUT")
                        .and(avenant.avtIndex.eq(0L))
                        .and(avenant2.avtSuppr.eq("N"))
                        )
                .groupBy(operation.idOpeOperation, numeroEtLibelle.opeNumero, operation.llOperation, 
                        typeOperation.idOpeTypeOperation, typeOperation.codeTypeOperation, typeOperation.llTypeOperation,
                        operation.estInclusePpiEtab, operation.estIncluseTbxBudgetaires, operation.estFlechee,
                        avenant.avtMontantHt)
                .orderBy(numeroEtLibelle.opeNumero.asc());
        return sqlQuery;
    }

    public class OperationProjection extends MappingProjection<Operation> {

        public OperationProjection() {
            super(Operation.class, operation.idOpeOperation, numeroEtLibelle.opeNumero, operation.llOperation, 
                    typeOperation.idOpeTypeOperation, typeOperation.codeTypeOperation, typeOperation.llTypeOperation,
                    operation.estInclusePpiEtab, operation.estIncluseTbxBudgetaires, operation.estFlechee,
                    avenant.avtMontantHt, avenant2.avtMontantHt.sum().as(montantHtSum));
        }

        @Override
        protected Operation map(Tuple row) {
            return Operation.builder()
                    .operationId(new OperationId(row.get(operation.idOpeOperation)))
                    .libelle(row.get(operation.llOperation))
                    .flechee(row.get(operation.estFlechee) == 1)
                    .type(TypeOperation.builder()
                            .code(row.get(typeOperation.codeTypeOperation))
                            .typeOperationId(new TypeOperationId(row.get(typeOperation.idOpeTypeOperation)))
                            .libelle(row.get(typeOperation.llTypeOperation))
                            .build())
                    .code(row.get(numeroEtLibelle.opeNumero))
                    .inclusePPIEtablissement(row.get(operation.estInclusePpiEtab) == 1)
                    .incluseTableauxBudgetaires(row.get(operation.estIncluseTbxBudgetaires) == 1)
                    .montantHT(new Montant(row.get(avenant.avtMontantHt)))
                    .montantNet(new Montant(row.get(montantHtSum)))
                    .build();
        }
        
    }

}
