/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.operation.support.querydsl;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.operation.support.CountRepository;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBud;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudDepAe;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudDepCp;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudRec;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;

/**
 * @author bourges
 *
 */
public class QueryDSLCountRepository implements CountRepository {
    
    private QueryDslJdbcTemplate template;
    private QAdmEb admEb = QAdmEb.admEb;
    private QOpeTrancheBudDepAe aeTranche = QOpeTrancheBudDepAe.opeTrancheBudDepAe;
    private QOpeTrancheBudDepCp cpTranche = QOpeTrancheBudDepCp.opeTrancheBudDepCp;
    private QOpeTrancheBudRec opeTrancheBudRec = QOpeTrancheBudRec.opeTrancheBudRec;
    private QOpeTrancheBud opeTrancheBud = QOpeTrancheBud.opeTrancheBud;
    
    public QueryDSLCountRepository(QueryDslJdbcTemplate template) {
        this.template = template;
    }

    public long countDepensesAePositionneesSurOperation(EntiteBudgetaireId ebId, int exercice) {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(admEb)
                .join(aeTranche)
                    .on(aeTranche.idAdmEb.eq(admEb.idAdmEb))
                .join(opeTrancheBud)
                    .on(opeTrancheBud.idOpeTrancheBud.eq(aeTranche.idOpeTrancheBud))
                .where(admEb.idAdmEb.eq(ebId.getId())
                        .and(opeTrancheBud.exeOrdre.eq(exercice)));
        return template.count(sqlQuery);
    }
    
    public long countDepensesCpPositionneesSurOperation(EntiteBudgetaireId ebId, int exercice) {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(admEb)
                .join(cpTranche)
                    .on(cpTranche.idAdmEb.eq(admEb.idAdmEb))
                .join(opeTrancheBud)
                    .on(opeTrancheBud.idOpeTrancheBud.eq(cpTranche.idOpeTrancheBud))
                .where(admEb.idAdmEb.eq(ebId.getId())
                        .and(opeTrancheBud.exeOrdre.eq(exercice)));
        return template.count(sqlQuery);
    }

    public long countRecettesPositionneesSurOperation(EntiteBudgetaireId ebId, int exercice) {
        SQLQuery sqlQuery = template.newSqlQuery()
                .from(admEb)
                .join(opeTrancheBudRec)
                    .on(opeTrancheBudRec.idAdmEb.eq(admEb.idAdmEb))
                .join(opeTrancheBud)
                    .on(opeTrancheBud.idOpeTrancheBud.eq(opeTrancheBudRec.idOpeTrancheBud))
                .where(admEb.idAdmEb.eq(ebId.getId())
                        .and(opeTrancheBud.exeOrdre.eq(exercice)));
        return template.count(sqlQuery);
    }

}
