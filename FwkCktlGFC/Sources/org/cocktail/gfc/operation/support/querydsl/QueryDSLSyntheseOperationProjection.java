package org.cocktail.gfc.operation.support.querydsl;

import java.math.BigDecimal;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.common.montant.Montant;
import org.cocktail.gfc.operation.Operation;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.SyntheseOperation;
import org.cocktail.gfc.operation.TrancheId;
import org.cocktail.gfc.operation.TypeOperation;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QAdmTypeEtat;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBud;
import org.cocktail.gfc.support.querydsl.QOpeTypeOperation;

import com.mysema.query.Tuple;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.Expression;
import com.mysema.query.types.MappingProjection;
import com.mysema.query.types.path.NumberPath;

public class QueryDSLSyntheseOperationProjection extends MappingProjection<SyntheseOperation> {
    
    private static final long serialVersionUID = 1L;
    private static final QOpeOperation OPE = QOpeOperation.opeOperation;
    private static final QOpeTypeOperation TYPE_OPE = QOpeTypeOperation.opeTypeOperation;
    private static final QOpeTrancheBud TRANCHE = QOpeTrancheBud.opeTrancheBud;
    private static final QAdmTypeEtat TYPE_ETAT = QAdmTypeEtat.admTypeEtat;
    
    private static final NumberPath<BigDecimal> MONTANT_AE = Expressions.numberPath(BigDecimal.class, "montantAeSum");
    private static final NumberPath<BigDecimal> MONTANT_CP = Expressions.numberPath(BigDecimal.class, "montantCpSum");
    private static final NumberPath<BigDecimal> MONTANT_BUD = Expressions.numberPath(BigDecimal.class, "montantBudSum");
    
    // Cases expressions
    private static final Expression<Boolean> EST_VALIDE = TYPE_ETAT.tyetLibelle
        .when("VALIDE").then(true)
        .otherwise(false);
    
    public QueryDSLSyntheseOperationProjection() {
        super(SyntheseOperation.class, TRANCHE.idOpeOperation, OPE.llOperation, OPE.estFlechee, TYPE_OPE.idOpeTypeOperation, TYPE_OPE.codeTypeOperation, TYPE_OPE.llTypeOperation, TRANCHE.idOpeTrancheBud, TRANCHE.exeOrdre, MONTANT_AE, MONTANT_CP, MONTANT_BUD, EST_VALIDE);
    }
    
    @Override
    protected SyntheseOperation map(Tuple row) {
        TypeOperation typeOperation = new TypeOperation(row.get(TYPE_OPE.idOpeTypeOperation), row.get(TYPE_OPE.codeTypeOperation), row.get(TYPE_OPE.llTypeOperation));
        OperationId operationId = new OperationId(row.get(TRANCHE.idOpeOperation));
        boolean estFlechee = (row.get(OPE.estFlechee) == 1);
        Operation operation = new Operation(operationId, row.get(OPE.llOperation), estFlechee, typeOperation);
        
        return SyntheseOperation.builder()
                .operation(operation)
                .trancheId(new TrancheId(row.get(TRANCHE.idOpeTrancheBud)))
                .exercice(row.get(TRANCHE.exeOrdre))
                .sommeAE(new Montant(row.get(MONTANT_AE)))
                .sommeCP(new Montant(row.get(MONTANT_CP)))
                .sommeBudgetaire(new Montant(row.get(MONTANT_BUD)))
                .valide(row.get(EST_VALIDE))
                .build();
    }
}
