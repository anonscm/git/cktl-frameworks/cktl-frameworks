/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.operation.support.querydsl;

import static com.mysema.query.support.Expressions.as;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.gfc.admin.nomenclature.DestinationDepense;
import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.NatureDepense;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLEntiteBudgetaireProjection;
import org.cocktail.gfc.budget.nomenclature.EnveloppeBudgetaireId;
import org.cocktail.gfc.operation.DetailOperationDepense;
import org.cocktail.gfc.operation.TrancheId;
import org.cocktail.gfc.operation.support.DetailOperationDepenseRepository;
import org.cocktail.gfc.support.querydsl.QAdmDestinationDepense;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QAdmNatureDep;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBud;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudDepAe;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBudDepCp;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Projections;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.query.ListSubQuery;

public class QueryDSLDetailOperationDepenseRepository implements DetailOperationDepenseRepository {
	// Tables
	private final QOpeOperation operation = QOpeOperation.opeOperation;
	private final QOpeTrancheBud tranche = QOpeTrancheBud.opeTrancheBud;
	private final QOpeTrancheBudDepAe aeTranche = QOpeTrancheBudDepAe.opeTrancheBudDepAe;
	private final QOpeTrancheBudDepCp cpTranche = QOpeTrancheBudDepCp.opeTrancheBudDepCp;
	private final QAdmEbExer ebExer = new QAdmEbExer("ebex");
	private final QAdmEb eb = new QAdmEb("eb");
	private final QAdmDestinationDepense destinationDepense = QAdmDestinationDepense.admDestinationDepense;
	private final QAdmNatureDep natureDepense = QAdmNatureDep.admNatureDep;
	
	// Paths
	private final NumberPath<Long> ebId = Expressions.numberPath(Long.class, "ebId");
	private final NumberPath<Long> destinationId = Expressions.numberPath(Long.class, "destinationId");
	private final NumberPath<Long> natureDepId = Expressions.numberPath(Long.class, "natureDepId");
	private final NumberPath<Long> opeId = Expressions.numberPath(Long.class, "opeId");
	private final NumberPath<Long> trancheId = Expressions.numberPath(Long.class, "trancheId");
	private final NumberPath<Long> enveloppeId = Expressions.numberPath(Long.class, "enveloppeId");
	private final NumberPath<Integer> exeOrdre = Expressions.numberPath(Integer.class, "exeOrdre");
	private final NumberPath<BigDecimal> montantAeSum = Expressions.numberPath(BigDecimal.class, "montantAeSum");
    private final NumberPath<BigDecimal> montantCp = Expressions.numberPath(BigDecimal.class, "montantCp");
    private PathBuilder<Tuple> sousReqPath = new PathBuilder<Tuple>(Tuple.class, "sr");
	
	// Projections
	private QueryDSLEntiteBudgetaireProjection projectionEb = new QueryDSLEntiteBudgetaireProjection();
		
	private final Expression<DestinationDepense> constructeurDestinationDepense = Projections.constructor(
	            DestinationDepense.class, 
	            Projections.constructor(DestinationDepenseId.class, destinationDepense.idAdmDestinationDepense),
	            destinationDepense.code, destinationDepense.abreviation, destinationDepense.libelle);

	private final Expression<NatureDepense> constructeurNatureDep = Projections.constructor(
            NatureDepense.class, 
            Projections.constructor(NatureDepenseId.class, natureDepense.idAdmNatureDep), natureDepense.code, natureDepense.libelle, natureDepense.fongible);

 	private final Expression<DetailOperationDepense> constructeurDetailOperationDepense = Projections.constructor(DetailOperationDepense.class, 
 	       opeId, trancheId, projectionEb, 
 	        constructeurDestinationDepense, constructeurNatureDep, montantAeSum, montantCp, 
 	        Projections.constructor(EnveloppeBudgetaireId.class, enveloppeId));
	
	private QueryDslJdbcTemplate template;
    
	public QueryDSLDetailOperationDepenseRepository(QueryDslJdbcTemplate template) {
        this.template = template;
    }
	
	public List<DetailOperationDepense> findDetailOperationDepenseByTrancheId(TrancheId trancheId) {
		SQLQuery query = addByTrancheClause(getDetailOperationDepenseQuery(), trancheId.getId());
		return template.query(query, constructeurDetailOperationDepense);
	}
	
    private ListSubQuery<Tuple> getMontantsAe() {
        SQLSubQuery subQuery = new SQLSubQuery().from(operation)
                .join(tranche)
                .on(operation.idOpeOperation.eq(tranche.idOpeOperation))
                .join(aeTranche)
                .on(tranche.idOpeTrancheBud.eq(aeTranche.idOpeTrancheBud));
        ListSubQuery<Tuple> listSubQueryAe = subQuery.groupBy(operation.idOpeOperation, tranche.idOpeTrancheBud, tranche.exeOrdre,
                aeTranche.idAdmEb,
                aeTranche.idAdmDestinationDepense, aeTranche.idAdmNatureDep, aeTranche.idBudEnveloppe, aeTranche.montantCp)
                .list(operation.idOpeOperation.as(opeId), tranche.idOpeTrancheBud.as(trancheId), tranche.exeOrdre.as(exeOrdre),
                        aeTranche.idAdmEb.as(ebId),
                        aeTranche.idAdmDestinationDepense.as(destinationId), aeTranche.idAdmNatureDep.as(natureDepId),
                        aeTranche.idBudEnveloppe.as(enveloppeId), aeTranche.montantAe.sum().as(montantAeSum),
                        as(ConstantImpl.create(BigDecimal.ZERO), montantCp));
        return listSubQueryAe;
    }

    private ListSubQuery<Tuple> getMontantsCp() {
        SQLSubQuery subQuery = new SQLSubQuery().from(operation)
                .join(tranche)
                .on(operation.idOpeOperation.eq(tranche.idOpeOperation))
                .join(cpTranche)
                .on(tranche.idOpeTrancheBud.eq(cpTranche.idOpeTrancheBud));
        ListSubQuery<Tuple> listSubQueryCp = subQuery.groupBy(operation.idOpeOperation, tranche.idOpeTrancheBud, tranche.exeOrdre,
                cpTranche.idAdmEb,
                cpTranche.idAdmDestinationDepense, cpTranche.idAdmNatureDep, cpTranche.idBudEnveloppe, cpTranche.montantCp)
                .list(operation.idOpeOperation.as(opeId), tranche.idOpeTrancheBud.as(trancheId), tranche.exeOrdre.as(exeOrdre),
                        cpTranche.idAdmEb.as(ebId),
                        cpTranche.idAdmDestinationDepense.as(destinationId), cpTranche.idAdmNatureDep.as(natureDepId),
                        cpTranche.idBudEnveloppe.as(enveloppeId), as(ConstantImpl.create(BigDecimal.ZERO), montantAeSum), cpTranche.montantCp);
        return listSubQueryCp;
    }

    private ListSubQuery<Tuple> getMontantsDep() {
        SQLSubQuery subQ = new SQLSubQuery();
        return subQ.from(new SQLSubQuery().union(getMontantsAe(), getMontantsCp()).as(sousReqPath))
                .groupBy(opeId, trancheId, exeOrdre, ebId, destinationId, natureDepId, enveloppeId)
                .list(opeId, trancheId, exeOrdre, ebId, destinationId, natureDepId, enveloppeId, montantAeSum.sum().as(montantAeSum), montantCp.sum().as(montantCp));
    }
    
    // Queries
	private SQLQuery getDetailOperationDepenseQuery() {
	    return template.newSqlQuery()
	            .from(destinationDepense)
	            .from(natureDepense)
	            .from(ebExer)
	            .from(eb)
	            .from(getMontantsDep().as(sousReqPath))
	            .where(ebId.eq(eb.idAdmEb)
	            .and(ebExer.idAdmEb.eq(eb.idAdmEb))
	            .and(ebExer.exeOrdre.intValue().eq(exeOrdre))
	            .and(destinationDepense.idAdmDestinationDepense.eq(destinationId))
	            .and(natureDepense.idAdmNatureDep.eq(natureDepId)));
	}
	
	private SQLQuery addByTrancheClause(SQLQuery sqlQuery, Long idTranche) {
		return sqlQuery.where(trancheId.eq(idTranche));
	}
		
}
