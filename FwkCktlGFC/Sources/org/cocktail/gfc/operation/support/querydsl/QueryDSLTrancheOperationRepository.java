/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.operation.support.querydsl;

import java.util.List;

import org.cocktail.gfc.admin.nomenclature.TypeEtatService;
import org.cocktail.gfc.operation.OperationId;
import org.cocktail.gfc.operation.TrancheId;
import org.cocktail.gfc.operation.TrancheOperation;
import org.cocktail.gfc.operation.support.TrancheOperationRepository;
import org.cocktail.gfc.support.querydsl.QOpeOperation;
import org.cocktail.gfc.support.querydsl.QOpeTrancheBud;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Optional;
import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.MappingProjection;

public class QueryDSLTrancheOperationRepository implements TrancheOperationRepository {

    // Tables
    private final QOpeTrancheBud tranche = new QOpeTrancheBud("tranche");
    private final QOpeOperation operation = new QOpeOperation("operation");

    // Projections
    private QueryDSLTrancheProjection projection = new QueryDSLTrancheProjection();

    private QueryDslJdbcTemplate template;

    public QueryDSLTrancheOperationRepository(QueryDslJdbcTemplate template) {
        this.template = template;
    }

    public Optional<TrancheOperation> findById(TrancheId id) {
        SQLQuery query = addByIdClause(getTranches(), id);
        TrancheOperation uneTranche = template.queryForObject(query, projection);
        return Optional.fromNullable(uneTranche);
    }

    public List<TrancheOperation> findByOperation(OperationId operationId) {
        SQLQuery query = getTranches().where(operation.idOpeOperation.eq(operationId.getId()));
        return template.query(query, projection);
    }

    // Queries
    private SQLQuery getTranches() {
        SQLQuery query = template.newSqlQuery().from(tranche).join(operation).on(tranche.idOpeOperation.eq(operation.idOpeOperation));
        return query;
    }

    private SQLQuery addByIdClause(SQLQuery query, TrancheId id) {
        SQLQuery queryResult = query.where(tranche.idOpeTrancheBud.eq(id.getId()));
        return queryResult;
    }

    public static class QueryDSLTrancheProjection extends MappingProjection<TrancheOperation> {

        private static final long serialVersionUID = 1L;
        private static final QOpeTrancheBud TRANCHE = new QOpeTrancheBud("tranche");
        private static final QOpeOperation OPE = new QOpeOperation("operation");

        public QueryDSLTrancheProjection() {
            super(TrancheOperation.class, TRANCHE.idOpeTrancheBud, TRANCHE.idOpeOperation, OPE.llOperation, TRANCHE.tyetId, TRANCHE.exeOrdre);
        }

        @Override
        protected TrancheOperation map(Tuple row) {
            return TrancheOperation.builder()
                    .id(new TrancheId(row.get(TRANCHE.idOpeTrancheBud)))
                    .operationId(new OperationId(row.get(TRANCHE.idOpeOperation)))
                    .operationLibelle(row.get(OPE.llOperation))
                    .exeOrdre(row.get(TRANCHE.exeOrdre))
                    .etat(TypeEtatService.getTypeEtatById(row.get(TRANCHE.tyetId)))
                    .build();
        }
    }
}
