/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc;

import static org.apache.commons.lang.Validate.notNull;

/**
 * @author bourges
 *
 */
public abstract class AbstractId {
	private Long id;
	
	public AbstractId(Long id) {
		this.setId(id);
	}
	
	private void setId(Long id) {
		notNull(id, "L'identifiant ne peut pas être null");
		this.id = id;
	}
	
    public Long id() {
        return this.id;
    }
    
    public Long getId() {
        return this.id;
    }
    
	@Override
    public String toString() {
        return this.getClass().getSimpleName() + " [id=" + id + "]";
    }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractId other = (AbstractId) obj;
		if (id() == null) {
			if (other.id() != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
