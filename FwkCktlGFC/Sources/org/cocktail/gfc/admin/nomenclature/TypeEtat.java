/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature;

import lombok.Getter;

/**
 * 
 * Cette classe represente un etat.
 * Nous gérons l'état sous al forme d'une classe mais il faut imaginer que la liste des etats est fixe.
 * Pour renforcer le focntionnement de cette classe comme une enum ou des instances statiques
 * la classe est marquée final et le constructeur n'est pas visible en dehors du package. 
 *
 */
@Getter
public final class TypeEtat {
	
	private Long idTypeEtat;
	private String libelleTypeEtat;
	
	TypeEtat(Long id, String libelle) {
		this.idTypeEtat = id;
		this.libelleTypeEtat = libelle;
	}
	
    public boolean estValide() {
        return estDeType(TypeEtatConstants.VALIDE);
    }
	
    public boolean estNonRenseignee() {
    	return estDeType(TypeEtatConstants.NON_RENSEIGNEE);
    }
    
    public boolean estVerrouillee() {
        return estDeType(TypeEtatConstants.VERROUILLEE);
    }
    
    public boolean estControlee() {
        return estDeType(TypeEtatConstants.CONTROLEE);
    }
    
    public boolean estValidee() {
        return estDeType(TypeEtatConstants.VALIDE);
    }
    
    public boolean estDeType(TypeEtat typeEtat) {
    	return this.equals(typeEtat);
    }

	@Override
	public String toString() {
		return "TypeEtat [idTypeEtat=" + idTypeEtat + ", libelleTypeEtat=" + libelleTypeEtat + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idTypeEtat == null) ? 0 : idTypeEtat.hashCode());
		result = prime * result
				+ ((libelleTypeEtat == null) ? 0 : libelleTypeEtat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		TypeEtat other = (TypeEtat) obj;
		if (idTypeEtat == null) {
			if (other.idTypeEtat != null)
				return false;
		} else if (!idTypeEtat.equals(other.idTypeEtat)) {
			return false;
		}
		if (libelleTypeEtat == null) {
			if (other.libelleTypeEtat != null) {
				return false;
			}
		} else if (!libelleTypeEtat.equals(other.libelleTypeEtat)) {
			return false;
		}
		
		return true;
	}
}
