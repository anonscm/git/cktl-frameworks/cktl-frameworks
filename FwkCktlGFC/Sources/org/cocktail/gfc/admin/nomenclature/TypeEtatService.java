/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature;

import java.util.HashMap;
import java.util.Map;

public class TypeEtatService {

	public enum Code {
		VALIDE(TypeEtatConstants.VALIDE),
		ANNULE(TypeEtatConstants.ANNULE),
		OUI(TypeEtatConstants.OUI),
		NON(TypeEtatConstants.NON),
		SUPPRIME(TypeEtatConstants.SUPPRIME),
		EN_ATTENTE(TypeEtatConstants.EN_ATTENTE),
		A_VALIDER(TypeEtatConstants.A_VALIDER),
		OPERATION_DECAISSABLE(TypeEtatConstants.OPERATION_DECAISSABLE),
		OPERATION_NON_DECAISSABLE(TypeEtatConstants.OPERATION_NON_DECAISSABLE),
		NE_RIEN_FAIRE(TypeEtatConstants.NE_RIEN_FAIRE),
		ALERTE(TypeEtatConstants.ALERTE),
		BLOCAGE(TypeEtatConstants.BLOCAGE),
		TOUTES(TypeEtatConstants.TOUTES),
		TOUS(TypeEtatConstants.TOUS),
		AUCUN(TypeEtatConstants.AUCUN),
		AUCUNE(TypeEtatConstants.AUCUNE),
		RECETTES(TypeEtatConstants.RECETTES),
		DEPENSES(TypeEtatConstants.DEPENSES),
		DETERMINE_PAR_PARENT(TypeEtatConstants.DETERMINE_PAR_PARENT),
		EN_COURS(TypeEtatConstants.EN_COURS),
		CONTROLE(TypeEtatConstants.CONTROLE),
		CLOTURE(TypeEtatConstants.CLOTURE),
		CALCULE(TypeEtatConstants.CALCULE),
		VOTE(TypeEtatConstants.VOTE),
		TRAITE(TypeEtatConstants.TRAITE),
		NON_RENSEIGNEE(TypeEtatConstants.NON_RENSEIGNEE),
		VERROUILLEE(TypeEtatConstants.VERROUILLEE),
		CONTROLEE(TypeEtatConstants.CONTROLEE),
		VALIDEE(TypeEtatConstants.VALIDEE),
		O(TypeEtatConstants.O),
		N(TypeEtatConstants.N),
		PRECOMMANDE(TypeEtatConstants.PRECOMMANDE),
		PART_ENGAGEE(TypeEtatConstants.PART_ENGAGEE),
		ENGAGEE(TypeEtatConstants.ENGAGEE),
		PART_SOLDEE(TypeEtatConstants.PART_SOLDEE),
		SOLDEE(TypeEtatConstants.SOLDEE),
		ANNULEE(TypeEtatConstants.ANNULEE),
		ENGAGE(TypeEtatConstants.ENGAGE),
		PART_SOLDE(TypeEtatConstants.PART_SOLDE),
		SOLDE(TypeEtatConstants.SOLDE),
		SUR_EXTOURNE(TypeEtatConstants.SUR_EXTOURNE),
		SUR_BUDGET_EXERCICE(TypeEtatConstants.SUR_BUDGET_EXERCICE),
		PREPARATION(TypeEtatConstants.PREPARATION),
		ACCEPTE(TypeEtatConstants.ACCEPTE),
		REJETE(TypeEtatConstants.REJETE),
		LIQUIDE(TypeEtatConstants.LIQUIDE),
		GLOBALISEE(TypeEtatConstants.GLOBALISEE),
		FLECHEE(TypeEtatConstants.FLECHEE);
		
		private TypeEtat typeEtat;
		private Code(TypeEtat typeEtat) {
			this.typeEtat = typeEtat;
		}
		
		public TypeEtat etat() {
			return this.typeEtat;
		}
		
		public Long id() {
			return this.typeEtat.getIdTypeEtat();
		}
	}
	
	private static final Map<Long, TypeEtat> CACHED_TYPE_ETAT_BY_ID = new HashMap<Long, TypeEtat>();

    public static TypeEtat getTypeEtatById(Long id) {
    	if (!CACHED_TYPE_ETAT_BY_ID.containsKey(id)) {
    		CACHED_TYPE_ETAT_BY_ID.put(id, findTypeEtatById(id));
    	}
    	return CACHED_TYPE_ETAT_BY_ID.get(id);
    }
    
    protected static TypeEtat findTypeEtatById(Long id) {
    	for (Code typeEtatCode : Code.values()) {
			if (typeEtatCode.etat().getIdTypeEtat().equals(id)) {
				return typeEtatCode.etat();
			}
		}
    	// lancer une erreur ?
    	return null;
    }
	
}


