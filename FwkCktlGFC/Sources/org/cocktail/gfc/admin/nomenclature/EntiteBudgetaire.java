/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Builder;
import lombok.experimental.Wither;

@AllArgsConstructor
@Builder
@Wither
@Getter
// TODO FLA 06.03.2015 modifier (nom?, heritage?, deplacement dans un autre package?) pour prendre en compte que cet objet represente une entite budgetaire par exercice.
public class EntiteBudgetaire implements Comparable<EntiteBudgetaire> {

	// TODO FLA 06.03.2015 Maj pour avoir un EntiteBudgetaireId (RBO : En attendant j'ai mis un getEntiteBudgetaireId pour stopper l'hémorragie)
	private Long id;
	private Long idPere;
	private Integer niveau;
	private String universite;
	private String etablissement;
	private String uniteBudgetaire;
	private String centreDeResponsabilite;
	private String sousCentreDeResponsabilite;
	private String libelle;
	private Boolean saisissable;
	
    public EntiteBudgetaire(Long id, String universite, String etablissement, String uniteBudgetaire, String centreDeResponsabilite, String sousCentreDeResponsabilite,
            String libelle, int isSaisissable) {
        this(id, universite, etablissement, uniteBudgetaire, centreDeResponsabilite, sousCentreDeResponsabilite, libelle);
        this.saisissable = (isSaisissable == 1);
    }
    
    public EntiteBudgetaire(Long id, String universite, String etablissement, String uniteBudgetaire, String centreDeResponsabilite, String sousCentreDeResponsabilite,
            String libelle) {
        super();
        this.id = id;
        this.universite = universite;
        this.etablissement = etablissement;
        this.uniteBudgetaire = uniteBudgetaire;
        this.centreDeResponsabilite = centreDeResponsabilite;
        this.sousCentreDeResponsabilite = sousCentreDeResponsabilite;
        this.libelle = libelle;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EntiteBudgetaire other = (EntiteBudgetaire) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public Boolean isSaisissable() {
        return this.saisissable;
    }
    
    public EntiteBudgetaireId getEntiteBudgetaireId() {
        return new EntiteBudgetaireId(id);
    }
    
    public String getConcat() {
        String concat = this.etablissement
                + (this.uniteBudgetaire != null ? "/" + this.uniteBudgetaire : "")
                + (this.centreDeResponsabilite != null ? "/" + this.centreDeResponsabilite : "")
                + (this.sousCentreDeResponsabilite != null ? "/" + this.sousCentreDeResponsabilite : "");
        return concat;
    }

    public int compareTo(EntiteBudgetaire eb) {
        return id.compareTo(eb.id);
    }

}
