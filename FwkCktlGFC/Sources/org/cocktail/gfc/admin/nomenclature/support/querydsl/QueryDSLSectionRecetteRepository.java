/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature.support.querydsl;

import java.util.List;

import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.admin.nomenclature.support.SectionRecetteRepository;
import org.cocktail.gfc.support.querydsl.QAdmSectionRecette;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.MappingProjection;

public class QueryDSLSectionRecetteRepository implements SectionRecetteRepository {

    @Autowired
    private QueryDslJdbcTemplate template;

    // Tables
    private QAdmSectionRecette sectionRecetteQ = QAdmSectionRecette.admSectionRecette;

    private QueryDSLSectionRecetteProjection projection = new QueryDSLSectionRecetteProjection();

    // Queries
    private SQLQuery getBaseQuery() {
        SQLQuery sqlQuery = template.newSqlQuery().from(sectionRecetteQ);
        return sqlQuery;
    }

    public SectionRecette findByCode(String code) {
        SQLQuery query = getBaseQuery().where(sectionRecetteQ.code.eq(code));
        return template.queryForObject(query, projection);
    }

    public List<SectionRecette> findAll() {
        SQLQuery query = getBaseQuery();
        return template.query(query, projection);
    }

    public class QueryDSLSectionRecetteProjection extends MappingProjection<SectionRecette> {

        private static final long serialVersionUID = 1L;

        public QueryDSLSectionRecetteProjection() {
            super(SectionRecette.class,
                    sectionRecetteQ.code,
                    sectionRecetteQ.libelle);
        }

        @Override
        protected SectionRecette map(Tuple row) {
            return new SectionRecette(row.get(sectionRecetteQ.code), row.get(sectionRecetteQ.libelle));
        }

    }

}
