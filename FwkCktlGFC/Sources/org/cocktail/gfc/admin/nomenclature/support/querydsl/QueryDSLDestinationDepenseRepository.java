/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature.support.querydsl;

import java.util.List;

import org.cocktail.gfc.admin.nomenclature.DestinationDepense;
import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.support.DestinationDepenseRepository;
import org.cocktail.gfc.support.querydsl.QAdmDestinationDepExer;
import org.cocktail.gfc.support.querydsl.QAdmDestinationDepense;
import org.cocktail.gfc.support.querydsl.QAdmExercice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Optional;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.ConstructorExpression;

public class QueryDSLDestinationDepenseRepository implements DestinationDepenseRepository {

	private static final long VALIDE = 1L;

	private static final Integer SAISISSABLE = 1;

    @Autowired
	private QueryDslJdbcTemplate template;

	// Tables
	private QAdmDestinationDepense destinationDepense = new QAdmDestinationDepense("dd");
	private QAdmDestinationDepExer destinationDepExer = new QAdmDestinationDepExer("dde");
	private QAdmExercice exercice = new QAdmExercice("e");

	// Queries
	private SQLQuery getBaseQuery() {
		SQLQuery sqlQuery = template.newSqlQuery().from(destinationDepense);
		return sqlQuery;
	}
	
	private SQLQuery addJoinDepenseExerciceClause(SQLQuery sqlQuery) {
		sqlQuery = sqlQuery.
        		leftJoin(destinationDepExer).on(destinationDepExer.idAdmDestinationDepense.eq(destinationDepense.idAdmDestinationDepense));
		return sqlQuery;
	}

	private SQLQuery addIdClause(SQLQuery sqlQuery, DestinationDepenseId destinationDepenseId) {
	    sqlQuery = sqlQuery.where(destinationDepense.idAdmDestinationDepense.eq(destinationDepenseId.getId()));
        return sqlQuery;
    }

    private SQLQuery addActiveClause(SQLQuery sqlQuery) {
        sqlQuery = sqlQuery.where(destinationDepense.tyetId.eq(VALIDE));
        return sqlQuery;
    }

    private SQLQuery addSaisissableClause(SQLQuery sqlQuery) {
        sqlQuery = sqlQuery.where(destinationDepExer.saisieBudgetaire.eq(SAISISSABLE));
        return sqlQuery;
    }

    private SQLQuery addExerciceClause(SQLQuery sqlQuery, int exer) {
        sqlQuery = addJoinDepenseExerciceClause(sqlQuery).
                join(exercice).on(exercice.exeOrdre.eq(destinationDepExer.exeOrdre.intValue())).
                where(exercice.exeExercice.eq(exer));
        return sqlQuery;
    }

	public List<DestinationDepense> findAll() {
		SQLQuery query = getBaseQuery();
		query = addJoinDepenseExerciceClause(query);
		List<DestinationDepense> destinationDepenses = getFromQuery(query);
		return destinationDepenses;
	}

    public List<DestinationDepense> findValides() {
        SQLQuery query = getBaseQuery();
		query = addJoinDepenseExerciceClause(query);
        query = addActiveClause(query);
        List<DestinationDepense> destinationDepenses = getFromQuery(query);
        return destinationDepenses;
    }

    public List<DestinationDepense> findValidesByExercice(int exercice) {
        SQLQuery query = getBaseQuery();
        query = addActiveClause(query);
        query = addExerciceClause(query, exercice);
        List<DestinationDepense> destinationDepenses = getFromQuery(query);
        return destinationDepenses;
    }
    
    public List<DestinationDepense> findDestSaisissablesAndByExercie(int exercice) {
    	// TODO Auto-generated method stub
    	SQLQuery query = getBaseQuery();
        query = addExerciceClause(query, exercice);
        query = addSaisissableClause(query);
        List<DestinationDepense> destinationDepenses = getFromQuery(query);
        return destinationDepenses;
    }
    
    public Optional<DestinationDepense> findByIdAndExercice(DestinationDepenseId id,
    		int exercice) {
    	// TODO Auto-generated method stub
    	Optional<DestinationDepense> ret = Optional.absent();
		SQLQuery query = getBaseQuery();
        query = addExerciceClause(query, exercice);
		query = addIdClause(query, id);
        List<DestinationDepense> destinationDepenses = getFromQuery(query);
        if(destinationDepenses.size() == 1)
        	ret = Optional.of(destinationDepenses.get(0));
    	return ret;
    }
	
    private List<DestinationDepense> getFromQuery(SQLQuery sqlQuery) {
        List<DestinationDepense> res = template.query(
                sqlQuery,
                ConstructorExpression.create(
                        DestinationDepense.class, 
                        ConstructorExpression.create(DestinationDepenseId.class, destinationDepense.idAdmDestinationDepense),
                        destinationDepense.code,
                        destinationDepense.abreviation,
                        destinationDepense.libelle,
                        destinationDepExer.saisieBudgetaire.coalesce(0).as("saisissable")
                        )
                );
        return res;
    }
}
