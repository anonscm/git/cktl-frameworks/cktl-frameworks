/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature.support;

import org.cocktail.gfc.admin.nomenclature.SectionRecetteCacheService;
import org.cocktail.gfc.admin.nomenclature.controller.NomenclatureFacade;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLDestinationDepenseRepository;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLEntiteBudgetaireRepository;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLExerciceRepository;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLNatureDepenseRepository;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLNatureRecetteRepository;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLOrigineRecetteRepository;
import org.cocktail.gfc.admin.nomenclature.support.querydsl.QueryDSLSectionRecetteRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NomenclatureConfig {

    @Bean
    public EntiteBudgetaireRepository entiteBugetaireRepository() {
        return new QueryDSLEntiteBudgetaireRepository();
    }

    @Bean
    public ExerciceRepository exerciceRepository() {
        return new QueryDSLExerciceRepository();
    }

    @Bean
    DestinationDepenseRepository destinationDepenseRepository() {
        return new QueryDSLDestinationDepenseRepository();
    }

    @Bean
    public NatureRecetteRepository natureRecetteRepository() {
        return new QueryDSLNatureRecetteRepository();
    }

    @Bean
    public OrigineRecetteRepository origineRecetteRepository() {
        return new QueryDSLOrigineRecetteRepository();
    }

    @Bean
    public NatureDepenseRepository natureDepenseRepository() {
        return new QueryDSLNatureDepenseRepository();
    }

    @Bean
    public SectionRecetteRepository sectionRecetteRepository() {
        return new QueryDSLSectionRecetteRepository();
    }

    @Bean
    public SectionRecetteCacheService sectionRecetteService(SectionRecetteRepository sectionRecetteRepository) {
        return new SectionRecetteCacheService(sectionRecetteRepository);
    }

    @Bean
    public NomenclatureFacade nomenclatureController(
            DestinationDepenseRepository destinationDepenseRepository,
            EntiteBudgetaireRepository entiteBudgetaireRepository,
            ExerciceRepository exerciceRepository,
            NatureRecetteRepository natureRecetteRepository,
            OrigineRecetteRepository origineRecetteRepository,
            NatureDepenseRepository natureDepenseRepository) {
        return new NomenclatureFacade(destinationDepenseRepository, entiteBudgetaireRepository, exerciceRepository, natureRecetteRepository,
                origineRecetteRepository, natureDepenseRepository);
    }
}
