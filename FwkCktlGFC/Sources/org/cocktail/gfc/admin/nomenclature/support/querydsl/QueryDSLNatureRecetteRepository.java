/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature.support.querydsl;

import java.util.List;

import org.cocktail.gfc.admin.nomenclature.NatureRecette;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.support.NatureRecetteRepository;
import org.cocktail.gfc.support.querydsl.QAdmExercice;
import org.cocktail.gfc.support.querydsl.QAdmNatureRec;
import org.cocktail.gfc.support.querydsl.QAdmNatureRecExercice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Optional;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.ConstructorExpression;

public class QueryDSLNatureRecetteRepository implements NatureRecetteRepository {

	@Autowired
	private QueryDslJdbcTemplate template;

	// Tables
	private QAdmNatureRec natureRecette = new QAdmNatureRec("nr");
	private QAdmNatureRecExercice natureRecExer = new QAdmNatureRecExercice(
			"nre");
	private QAdmExercice exercice = new QAdmExercice("e");

	// Queries
	private SQLQuery getBaseQuery() {
		SQLQuery sqlQuery = template.newSqlQuery().from(natureRecette);
		return sqlQuery;
	}

	private SQLQuery addExerciceClause(SQLQuery sqlQuery, int exer) {
		sqlQuery = sqlQuery
				.join(natureRecExer)
				.on(natureRecExer.idAdmNatureRec
						.eq(natureRecette.idAdmNatureRec)).join(exercice)
				.on(exercice.exeOrdre.eq(natureRecExer.exeOrdre.intValue()))
				.where(exercice.exeExercice.eq(exer));
		return sqlQuery;
	}

	public List<NatureRecette> findAll() {
		SQLQuery query = getBaseQuery();
		List<NatureRecette> natureRecettes = getFromQuery(query);
		return natureRecettes;
	}

	public Optional<NatureRecette> findByIdAndByExercice(NatureRecetteId id,
			Integer exercice) {
		// TODO Auto-generated method stub
    	Optional<NatureRecette> ret = Optional.absent();
		SQLQuery query = getBaseQuery().where(natureRecette.idAdmNatureRec.eq(id.getId()));
		query = addExerciceClause(query, exercice);
		List<NatureRecette> natureRecettes = getFromQuery(query);
        if(natureRecettes.size() == 1)
        	ret = Optional.of(natureRecettes.get(0));
    	return ret;
	}

	public List<NatureRecette> findByExercice(Integer exercice) {
		SQLQuery query = getBaseQuery();
		query = addExerciceClause(query, exercice);
		List<NatureRecette> natureRecettes = getFromQuery(query);
		return natureRecettes;
	}

	private List<NatureRecette> getFromQuery(SQLQuery sqlQuery) {
		List<NatureRecette> res = template.query(sqlQuery,
				ConstructorExpression.create(NatureRecette.class,
						ConstructorExpression.create(NatureRecetteId.class,
								natureRecette.idAdmNatureRec),
						natureRecette.code,
						natureRecette.libelle,
						natureRecette.tyetId));
		return res;
	}

}
