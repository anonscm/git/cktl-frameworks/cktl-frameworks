/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature.support.querydsl;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;

import com.mysema.query.Tuple;
import com.mysema.query.types.MappingProjection;

public class QueryDSLEntiteBudgetaireProjection extends MappingProjection<EntiteBudgetaire> {

    private static final long serialVersionUID = 1L;
    private static final QAdmEb EB = new QAdmEb("eb");
    private static final QAdmEbExer EB_EXER = new QAdmEbExer("ebex");

    public QueryDSLEntiteBudgetaireProjection() {
        super(EntiteBudgetaire.class, EB.idAdmEb, EB.orgPere, EB.orgNiv, EB.orgUniv, EB.orgEtab, EB.orgUb, EB.orgCr, EB.orgSouscr,
                EB.orgLib, EB_EXER.saisieBudgetaire);
    }
    
    @Override
    protected EntiteBudgetaire map(Tuple row) {
        return EntiteBudgetaire.builder()
                .id(row.get(EB.idAdmEb))
                .idPere(row.get(EB.orgPere))
                .niveau(row.get(EB.orgNiv))
                .universite(row.get(EB.orgUniv))
                .etablissement(row.get(EB.orgEtab))
                .uniteBudgetaire(row.get(EB.orgUb))
                .centreDeResponsabilite(row.get(EB.orgCr))
                .sousCentreDeResponsabilite(row.get(EB.orgSouscr))
                .libelle(row.get(EB.orgLib))
                .saisissable(estSaisissable(row.get(EB_EXER.saisieBudgetaire)))
                .build();
    }
    
    private Boolean estSaisissable(Integer saisissableAsNumber) {
    	if (saisissableAsNumber == null) {
    		return false;
    	}
    	return Integer.valueOf(1).equals(saisissableAsNumber);
    }
}
