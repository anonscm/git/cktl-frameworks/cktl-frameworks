/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature.support.querydsl;

import java.util.List;

import org.cocktail.gfc.admin.nomenclature.NatureDepense;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.admin.nomenclature.support.NatureDepenseRepository;
import org.cocktail.gfc.support.querydsl.QAdmExercice;
import org.cocktail.gfc.support.querydsl.QAdmNatureDep;
import org.cocktail.gfc.support.querydsl.QAdmNatureDepExercice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Optional;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Projections;

public class QueryDSLNatureDepenseRepository implements NatureDepenseRepository {

	@Autowired
	private QueryDslJdbcTemplate template;

	// Tables
	private QAdmNatureDep natureDepense = new QAdmNatureDep("nd");
	private QAdmNatureDepExercice natureDepExer = new QAdmNatureDepExercice("nde");
	private QAdmExercice exercice = new QAdmExercice("e");

	// Projections
	private final ConstructorExpression<NatureDepense> constructeurNatureDep = Projections.constructor(
			NatureDepense.class,
			Projections.constructor(NatureDepenseId.class, natureDepense.idAdmNatureDep), 
            natureDepense.code,	natureDepense.libelle, natureDepense.fongible);
	
	// Queries
	private SQLQuery getBaseQuery() {
		SQLQuery sqlQuery = template.newSqlQuery().from(natureDepense);
		return sqlQuery;
	}
	
	private SQLQuery addExerciceClause(SQLQuery sqlQuery, int exer) {
		sqlQuery = sqlQuery
				.join(natureDepExer)
				    .on(natureDepExer.idAdmNatureDep.eq(natureDepense.idAdmNatureDep))
				.join(exercice)
				    .on(exercice.exeOrdre.eq(natureDepExer.exeOrdre.intValue()))
				.where(exercice.exeExercice.eq(exer));
		return sqlQuery;
	}
	
	public Optional<NatureDepense> findById(NatureDepenseId id) {
		SQLQuery query = getBaseQuery().where(natureDepense.idAdmNatureDep.eq(id.getId()));
		return Optional.of(template.queryForObject(query, constructeurNatureDep));
	}

	public Optional<NatureDepense> findByIdAndByExercice(NatureDepenseId id,
			Integer exercice) {
		// TODO Auto-generated method stub
		SQLQuery query = getBaseQuery().where(natureDepense.idAdmNatureDep.eq(id.getId()));
		query = addExerciceClause(query, exercice);
		return Optional.of(template.queryForObject(query, constructeurNatureDep));
	}

	public List<NatureDepense> findAll() {
		SQLQuery query = getBaseQuery();
		List<NatureDepense> natureDepenses = getFromQuery(query);
		return natureDepenses;
	}

	public List<NatureDepense> findByExercice(Integer exercice) {
		SQLQuery query = getBaseQuery();
		query = addExerciceClause(query, exercice);
		List<NatureDepense> natureDepenses = getFromQuery(query);
		return natureDepenses;
	}

	private List<NatureDepense> getFromQuery(SQLQuery sqlQuery) {
		List<NatureDepense> res = template.query(sqlQuery, constructeurNatureDep);
		return res;
	}

}
