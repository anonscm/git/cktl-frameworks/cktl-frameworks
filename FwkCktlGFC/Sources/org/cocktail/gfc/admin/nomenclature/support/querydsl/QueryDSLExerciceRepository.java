/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature.support.querydsl;

import java.util.List;

import org.cocktail.gfc.admin.nomenclature.Exercice;
import org.cocktail.gfc.admin.nomenclature.support.ExerciceRepository;
import org.cocktail.gfc.support.querydsl.QAdmExercice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Optional;
import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.MappingProjection;

public class QueryDSLExerciceRepository implements ExerciceRepository {

    @Autowired
    private QueryDslJdbcTemplate template;

    // Tables
    private QAdmExercice exer = new QAdmExercice("exer");

    // Queries
    private SQLQuery getExerQuery() {
        SQLQuery sqlQuery = template.newSqlQuery().from(exer);
        return sqlQuery;
    }

    private SQLQuery addByExerciceClause(SQLQuery sqlQuery, Integer exercice) {
        sqlQuery = sqlQuery.where(exer.exeExercice.eq(exercice));
        return sqlQuery;
    }

    // Projection
    private Projection projection = new Projection(exer);

    public List<Exercice> findAll() {
        SQLQuery query = getExerQuery().orderBy(exer.exeExercice.desc());
        List<Exercice> exercices = template.query(query, projection);
        return exercices;
    }

    public Optional<Exercice> findByExercice(int exercice) {
        SQLQuery query = addByExerciceClause(getExerQuery(), exercice);
        Exercice exercices = template.queryForObject(query, projection);
        return Optional.of(exercices);
    }

    private class Projection extends MappingProjection<Exercice> {

        private static final long serialVersionUID = 1L;

        private QAdmExercice exer;

        public Projection(QAdmExercice e) {
            super(Exercice.class, e.exeOrdre, e.exeExercice, e.exeOuverture, e.exeCloture, e.exeType, e.exeStat
            // , e.exeStatEng
            // , e.exeStatFac
            // , e.exeStatLiq
            // , e.exeStatRec
            // , e.exeInventaire
            );
            this.exer = e;
        }

        @Override
        protected Exercice map(Tuple row) {
            return Exercice.builder().exeOrdre(row.get(exer.exeOrdre)).exercice(row.get(exer.exeExercice)).ouverture(row.get(exer.exeOuverture))
                    .cloture(row.get(exer.exeCloture)).type(row.get(exer.exeType)).statut(row.get(exer.exeStat))
                    // .statutEngagement(row.get(exer.exeStatEng))
                    // .statutFacture(row.get(exer.exeStatFac))
                    // .statutLiquidation(row.get(exer.exeStatLiq))
                    // .statutRecette(row.get(exer.exeStatRec))
                    // .inventaire(row.get(exer.exeInventaire))
                    .build();
        }
    }
}
