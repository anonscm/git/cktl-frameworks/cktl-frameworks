/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature.support.querydsl;

import java.util.List;

import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.support.EntiteBudgetaireRepository;
import org.cocktail.gfc.support.querydsl.QAdmEb;
import org.cocktail.gfc.support.querydsl.QAdmEbExer;
import org.cocktail.gfc.support.querydsl.QAdmUtilisateur;
import org.cocktail.gfc.support.querydsl.QAdmUtilisateurEb;
import org.cocktail.gfc.support.querydsl.QVAdmUtilisateurInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Optional;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.OrderSpecifier;

public class QueryDSLEntiteBudgetaireRepository implements EntiteBudgetaireRepository {

	@Autowired
	private QueryDslJdbcTemplate template;

	//Tables
	private QAdmEb eb = new QAdmEb("eb");
	private QAdmEbExer ebExercice = new QAdmEbExer("ebex");
	private QAdmUtilisateurEb utilisateurEb = new QAdmUtilisateurEb("ueb");
	private QAdmUtilisateur utilisateur = new QAdmUtilisateur("u");
	private QVAdmUtilisateurInfo utilisateurInfo = new QVAdmUtilisateurInfo("ui");

	//Queries
	
    private SQLQuery getEBQuery() {
        SQLQuery sqlQuery = template.newSqlQuery().from(eb);
        // On ne prend pas la racine technique
        sqlQuery = sqlQuery.where(eb.orgNiv.ne(0));
        // ajout d'un tri par defaut
        sqlQuery.orderBy(orderByOrgUnivAsc(), orderByOrgEtabAsc(), orderByOrgUbAsc(), orderByOrgCrAsc(), orderBySouscrCrAsc());

        return sqlQuery;
    }
	
	private SQLQuery getEBQuery(Integer exercice) {
		SQLQuery sqlQuery = template.newSqlQuery().from(eb);
		//on sélectionne toujours via un exercice
		sqlQuery = sqlQuery.join(ebExercice).on(eb.idAdmEb.eq(ebExercice.idAdmEb))
                .where(ebExercice.exeOrdre.eq(exercice));
		//On ne prend pas la racine technique
        sqlQuery = sqlQuery.where(eb.orgNiv.ne(0));
		//ajout d'un tri par defaut
		sqlQuery.orderBy(
				orderByOrgUnivAsc(),
				orderByOrgEtabAsc(),
				orderByOrgUbAsc(),
				orderByOrgCrAsc(),
				orderBySouscrCrAsc()
				);

		return sqlQuery;
	}
	
	private SQLQuery addSaisissableClause(SQLQuery sqlQuery, boolean isSaisissable) {
	    int flag = 0;
	    if (isSaisissable) flag = 1;
        sqlQuery = sqlQuery.where(ebExercice.saisieBudgetaire.eq(flag));
        return sqlQuery;
    }

    private SQLQuery addByUtilisateurClause(SQLQuery sqlQuery, Long userId, Integer exercice) {
		sqlQuery = sqlQuery.join(utilisateurEb).on(eb.idAdmEb.eq(utilisateurEb.idAdmEb))
				.join(utilisateur).on(utilisateur.utlOrdre.eq(utilisateurEb.utlOrdre))
				.join(utilisateurInfo).on(utilisateurInfo.utlOrdre.eq(utilisateur.utlOrdre))
				.where(utilisateur.persId.eq(userId))
				.where(utilisateurEb.exeOrdre.eq(exercice));
		return sqlQuery;
	}

	private SQLQuery addIdClause(SQLQuery sqlQuery, EntiteBudgetaireId entiteBudgetaireId) {
	    sqlQuery = sqlQuery.where(eb.idAdmEb.eq(entiteBudgetaireId.getId()));
        return sqlQuery;
    }

    //Orders
	private OrderSpecifier<String> orderByOrgUnivAsc() {
		return eb.orgUniv.asc();
	}

	private OrderSpecifier<String> orderByOrgEtabAsc() {
		return eb.orgEtab.asc().nullsFirst();
	}

	private OrderSpecifier<String> orderByOrgUbAsc() {
		return eb.orgUb.asc().nullsFirst();
	}

	private OrderSpecifier<String> orderByOrgCrAsc() {
		return eb.orgCr.asc().nullsFirst();
	}

	private OrderSpecifier<String> orderBySouscrCrAsc() {
		return eb.orgSouscr.asc().nullsFirst();
	}

	//Projection
	private QueryDSLEntiteBudgetaireProjection projection = new QueryDSLEntiteBudgetaireProjection();
	
	public List<EntiteBudgetaire> findByExercice(Integer exercice) {
		SQLQuery sqlQuery = getEBQuery(exercice);
		List<EntiteBudgetaire> entiteBudgetaires = template.query(sqlQuery, projection);
		return entiteBudgetaires;
	}

	public List<EntiteBudgetaire> findByExerciceAndSaisissable(int exercice, boolean isSaisissable) {
        SQLQuery sqlQuery = getEBQuery(exercice);
        sqlQuery = addSaisissableClause(sqlQuery, isSaisissable);
        List<EntiteBudgetaire> entiteBudgetaires = template.query(sqlQuery, projection);
        return entiteBudgetaires;
    }

	public Optional<EntiteBudgetaire> findByIdAndExercice(EntiteBudgetaireId entiteBudgetaireId, int exercice) {
	    Optional<EntiteBudgetaire> ret = Optional.absent();
        SQLQuery sqlQuery = getEBQuery(exercice);
        sqlQuery = addIdClause(sqlQuery, entiteBudgetaireId);
        List<EntiteBudgetaire> ebs = template.query(sqlQuery, projection);
        if (ebs.size() == 1)  {
            ret = Optional.of(ebs.get(0));
        }
        return ret;
    }

    public List<EntiteBudgetaire> findByUtilisateurAndExercice(Long userId,
			Integer exercice) {
		SQLQuery sqlQuery = getEBQuery(exercice);
		sqlQuery = addByUtilisateurClause(sqlQuery, userId, exercice);
		List<EntiteBudgetaire> entiteBudgetaires = template.query(sqlQuery, projection);
		return entiteBudgetaires;
	}
}
