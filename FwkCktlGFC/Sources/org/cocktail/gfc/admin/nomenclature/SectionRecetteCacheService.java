/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.gfc.admin.nomenclature.support.SectionRecetteRepository;

public class SectionRecetteCacheService {
    private static Logger LOG = Logger.getLogger(SectionRecetteCacheService.class.getName());
    // private static List<SectionRecette> ALL = new
    // ArrayList<SectionRecette>();

    private static Map<String, SectionRecette> ALL_MAP = new HashMap<String, SectionRecette>();

    private SectionRecetteRepository sectionRecetteRepository;
    private static SectionRecetteCacheService sharedInstance;

    public SectionRecetteCacheService(SectionRecetteRepository sectionRecetteRepository) {
        this.sectionRecetteRepository = sectionRecetteRepository;
        sharedInstance = this;
    }

    public static SectionRecetteCacheService sharedInstance() {
        return sharedInstance;
    }

    public static SectionRecette findByAbreviation(String abreviation) {
        SectionRecette result = null;
        if (sharedInstance() != null) {
            if (abreviation == null || abreviation.length() == 0) {
                return null;
            }
            if (ALL_MAP.containsKey(abreviation)) {
                result = ALL_MAP.get(abreviation);
            }
            if (result == null) {
                result = sharedInstance().sectionRecetteRepository.findByCode(abreviation);
                if (result != null) {
                    ALL_MAP.put(abreviation, result);
                }
            }
        }
        else {
            LOG.warn("Le cache n'est pas correctement initialisé (sectionRecetteRepository=null)");
        }
        return result;
    }

    public static List<SectionRecette> findAll() {
        if (sharedInstance() != null) {
            if (ALL_MAP.isEmpty()) {
                addSectionsRecetteInCache(sharedInstance().sectionRecetteRepository.findAll());
            }
        }
        return new ArrayList<SectionRecette>(ALL_MAP.values());
    }

    private static void addSectionsRecetteInCache(List<SectionRecette> sectionRecettes) {
        for (SectionRecette sectionRecette : sectionRecettes) {
            ALL_MAP.put(sectionRecette.getAbreviation(), sectionRecette);
        }
    }

    public static void clear() {
        ALL_MAP.clear();
    }

}
