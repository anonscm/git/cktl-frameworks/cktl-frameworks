/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature.controller;

import java.util.List;

import org.cocktail.gfc.admin.nomenclature.DestinationDepense;
import org.cocktail.gfc.admin.nomenclature.DestinationDepenseId;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaire;
import org.cocktail.gfc.admin.nomenclature.EntiteBudgetaireId;
import org.cocktail.gfc.admin.nomenclature.Exercice;
import org.cocktail.gfc.admin.nomenclature.NatureDepense;
import org.cocktail.gfc.admin.nomenclature.NatureDepenseId;
import org.cocktail.gfc.admin.nomenclature.NatureRecette;
import org.cocktail.gfc.admin.nomenclature.NatureRecetteId;
import org.cocktail.gfc.admin.nomenclature.OrigineRecette;
import org.cocktail.gfc.admin.nomenclature.OrigineRecetteId;
import org.cocktail.gfc.admin.nomenclature.SectionRecette;
import org.cocktail.gfc.admin.nomenclature.SectionRecetteCacheService;
import org.cocktail.gfc.admin.nomenclature.support.DestinationDepenseRepository;
import org.cocktail.gfc.admin.nomenclature.support.EntiteBudgetaireRepository;
import org.cocktail.gfc.admin.nomenclature.support.ExerciceRepository;
import org.cocktail.gfc.admin.nomenclature.support.NatureDepenseRepository;
import org.cocktail.gfc.admin.nomenclature.support.NatureRecetteRepository;
import org.cocktail.gfc.admin.nomenclature.support.OrigineRecetteRepository;
import org.cocktail.gfc.support.exception.UnauthorizedException;

import com.google.common.base.Optional;

public class NomenclatureFacade {

    private DestinationDepenseRepository destinationDepenseRepository;
    private EntiteBudgetaireRepository entiteBudgetaireRepository;
    private ExerciceRepository exerciceRepository;
    private NatureRecetteRepository natureRecetteRepository;
    private OrigineRecetteRepository origineRecetteRepository;
    private NatureDepenseRepository natureDepenseRepository;

    public NomenclatureFacade(
            DestinationDepenseRepository destinationDepenseRepo,
            EntiteBudgetaireRepository entiteBudgetaireRepository,
            ExerciceRepository exerciceRepository,
            NatureRecetteRepository natureRecetteRepository,
            OrigineRecetteRepository origineRecetteRepository,
            NatureDepenseRepository natureDepenseRepository) {
        this.destinationDepenseRepository = destinationDepenseRepo;
        this.entiteBudgetaireRepository = entiteBudgetaireRepository;
        this.exerciceRepository = exerciceRepository;
        this.natureRecetteRepository = natureRecetteRepository;
        this.origineRecetteRepository = origineRecetteRepository;
        this.natureDepenseRepository = natureDepenseRepository;
    }

    public List<DestinationDepense> getAllDestinationDepense() {
        return destinationDepenseRepository.findValides();
    }

    public List<DestinationDepense> getDestinationDepenseByExercice(int exercice) {
        return destinationDepenseRepository.findValidesByExercice(exercice);
    }
    
    public List<DestinationDepense> getDestinationDepenseSaisissablesAndByExercie(int exercice) {
    	return destinationDepenseRepository.findDestSaisissablesAndByExercie(exercice);
    }
    
    public Optional<DestinationDepense> getDestinationDepenseByIdAndExercie(DestinationDepenseId destinationDepenseId, int exercice) {
    	return destinationDepenseRepository.findByIdAndExercice(destinationDepenseId, exercice);
    }

    public List<EntiteBudgetaire> getEntiteBudgetaireByExercice(int exercice) {
        return entiteBudgetaireRepository.findByExercice(exercice);
    }

    public List<EntiteBudgetaire> getEntiteBudgetaireByExerciceAndSaisissable(int exercice, boolean isSaisissable) {
        return entiteBudgetaireRepository.findByExerciceAndSaisissable(exercice, isSaisissable);
    }

    public List<EntiteBudgetaire> getEntiteBudgetaireByUtilisateurAndExercice(
            String demandeur, Long userId, int exercice) throws UnauthorizedException {
        return entiteBudgetaireRepository.findByUtilisateurAndExercice(userId, exercice);
    }

    public Optional<EntiteBudgetaire> findByIdAndExercice(EntiteBudgetaireId entiteBudgetaireId, int exercice) {
        return entiteBudgetaireRepository.findByIdAndExercice(entiteBudgetaireId, exercice);
    }

    public List<Exercice> getAllExercice() {
        return exerciceRepository.findAll();
    }

    public Optional<Exercice> getExercice(int exercice) {
        Optional<Exercice> optionalExercice = exerciceRepository.findByExercice(exercice);
        return optionalExercice;
    }

    public List<NatureRecette> getAllNatureRecette() {
        return natureRecetteRepository.findAll();
    }

    public Optional<NatureRecette> getNatureRecetteByIdAndByExercice(NatureRecetteId natureRecetteId, int exercice) {
        return natureRecetteRepository.findByIdAndByExercice(natureRecetteId, exercice);
    }

    public List<NatureRecette> getNatureRecetteByExercice(int exercice) {
        return natureRecetteRepository.findByExercice(exercice);
    }

    public List<NatureDepense> getAllNatureDepense() {
        return natureDepenseRepository.findAll();
    }

    public Optional<NatureDepense> getNatureDepenseById(NatureDepenseId natureDepenseId) {
        return natureDepenseRepository.findById(natureDepenseId);
    }

    public Optional<NatureDepense> getNatureDepenseByIdAndByExercice(NatureDepenseId natureDepenseId, int exercice) {
        return natureDepenseRepository.findByIdAndByExercice(natureDepenseId, exercice);
    }

    public List<NatureDepense> getNatureDepenseByExercice(int exercice) {
        return natureDepenseRepository.findByExercice(exercice);
    }

    public List<OrigineRecette> getAllOrigineRecettes() {
        return origineRecetteRepository.findValides();
    }

    public Optional<OrigineRecette> getOrigineRecetteByIdAndByExercice(OrigineRecetteId origineRecetteId, int exercice) {
        return origineRecetteRepository.findByIdAndByExercice(origineRecetteId, exercice);
    }

    public List<OrigineRecette> getOrigineRecetteByExercice(int exercice) {
        return origineRecetteRepository.findValidesByExercice(exercice);
    }

    public List<SectionRecette> getSectionRecette() {
        return SectionRecetteCacheService.findAll();
    }
}
