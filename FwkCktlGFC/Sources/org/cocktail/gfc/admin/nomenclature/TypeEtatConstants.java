/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.nomenclature;

/**
 * Cette classe est immutable.
 * Elle correspond a une liste definie et maintenue par l'association Cocktail.
 * 
 * Sa visibilite est package et les classes souhaitant manipuler des etats doivent passer par TypeEtatService.
 */
final class TypeEtatConstants {

	public static final TypeEtat VALIDE = new TypeEtat(1L, "VALIDE");
	public static final TypeEtat ANNULE = new TypeEtat(2L, "ANNULE");
	public static final TypeEtat OUI = new TypeEtat(3L, "OUI");
	public static final TypeEtat NON = new TypeEtat(4L, "NON");
	public static final TypeEtat SUPPRIME = new TypeEtat(5L, "SUPPRIME");
	public static final TypeEtat EN_ATTENTE = new TypeEtat(6L, "EN ATTENTE");
	public static final TypeEtat A_VALIDER = new TypeEtat(7L, "A VALIDER");
	public static final TypeEtat OPERATION_DECAISSABLE = new TypeEtat(11L, "OPERATION DECAISSABLE");
	public static final TypeEtat OPERATION_NON_DECAISSABLE = new TypeEtat(12L, "OPERATION NON DECAISSABLE");
	public static final TypeEtat NE_RIEN_FAIRE = new TypeEtat(13L, "NE RIEN FAIRE");
	public static final TypeEtat ALERTE = new TypeEtat(14L, "ALERTE");
	public static final TypeEtat BLOCAGE = new TypeEtat(15L, "BLOCAGE");
	public static final TypeEtat TOUTES = new TypeEtat(16L, "TOUTES");
	public static final TypeEtat TOUS = new TypeEtat(17L, "TOUS");
	public static final TypeEtat AUCUN = new TypeEtat(18L, "AUCUN");
	public static final TypeEtat AUCUNE = new TypeEtat(19L, "AUCUNE");
	public static final TypeEtat RECETTES = new TypeEtat(20L, "RECETTES");
	public static final TypeEtat DEPENSES = new TypeEtat(21L, "DEPENSES");
	public static final TypeEtat DETERMINE_PAR_PARENT = new TypeEtat(22L, "DETERMINE PAR PARENT");
	public static final TypeEtat EN_COURS = new TypeEtat(100L, "EN COURS");
	public static final TypeEtat CONTROLE = new TypeEtat(101L, "CONTROLE");
	public static final TypeEtat CLOTURE = new TypeEtat(102L, "CLOTURE");
	public static final TypeEtat CALCULE = new TypeEtat(103L, "CALCULE");
	public static final TypeEtat VOTE = new TypeEtat(104L, "VOTE");
	public static final TypeEtat TRAITE = new TypeEtat(105L, "TRAITE");
	public static final TypeEtat NON_RENSEIGNEE = new TypeEtat(106L, "NON RENSEIGNEE");
	public static final TypeEtat VERROUILLEE = new TypeEtat(107L, "VERROUILLEE");
	public static final TypeEtat CONTROLEE = new TypeEtat(108L, "CONTROLEE");
	public static final TypeEtat VALIDEE = new TypeEtat(109L, "VALIDEE");
	public static final TypeEtat O = new TypeEtat(200L, "O");
	public static final TypeEtat N = new TypeEtat(201L, "N");
	public static final TypeEtat PRECOMMANDE = new TypeEtat(202L, "PRECOMMANDE");
	public static final TypeEtat PART_ENGAGEE = new TypeEtat(203L, "PART_ENGAGEE");
	public static final TypeEtat ENGAGEE = new TypeEtat(204L, "ENGAGEE");
	public static final TypeEtat PART_SOLDEE = new TypeEtat(205L, "PART_SOLDEE");
	public static final TypeEtat SOLDEE = new TypeEtat(206L, "SOLDEE");
	public static final TypeEtat ANNULEE = new TypeEtat(207L, "ANNULEE");
	public static final TypeEtat ENGAGE = new TypeEtat(208L, "ENGAGE");
	public static final TypeEtat PART_SOLDE = new TypeEtat(209L, "PART_SOLDE");
	public static final TypeEtat SOLDE = new TypeEtat(210L, "SOLDE");
	public static final TypeEtat SUR_EXTOURNE = new TypeEtat(220L, "sur extourne");
	public static final TypeEtat SUR_BUDGET_EXERCICE = new TypeEtat(221L, "sur budget exercice");
	public static final TypeEtat PREPARATION = new TypeEtat(300L, "PREPARATION");
	public static final TypeEtat ACCEPTE = new TypeEtat(401L, "ACCEPTE");
	public static final TypeEtat REJETE = new TypeEtat(402L, "REJETE");
	public static final TypeEtat LIQUIDE = new TypeEtat(403L, "LIQUIDE");
	public static final TypeEtat GLOBALISEE = new TypeEtat(500L, "GLOBALISEE");
	public static final TypeEtat FLECHEE = new TypeEtat(501L, "FLECHEE");
}
