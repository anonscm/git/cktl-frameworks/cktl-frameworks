/**
 * <img src="doc-files/Utilisateur.png">
 */
/*
 * @startuml doc-files/Utilisateur.png
 * Utilisateur-Fonction
 * Fonction-Exercice
 * 
 * class Utilisateur {
 *  id
 *  login
 * }
 * 
 * class Fonction {
 *  id
 *  codeInterne
 *  categorie
 *  description
 *  libelle
 *  idApplication
 *  restreinteParExercice
 * }
 * 
 * class Exercice {
 *  cod_exer
 * }
 * @enduml
 */
package org.cocktail.gfc.admin.identite; 