/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.identite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Getter;
import lombok.experimental.Builder;
import lombok.experimental.Wither;

import org.cocktail.gfc.admin.identite.support.FiltreFonctionParCode;
import org.cocktail.gfc.admin.identite.support.FiltreFonctionParExercice;
import org.cocktail.gfc.common.specification.Specification;

import com.google.common.base.Optional;

/**
 * Created by bourges on 17/10/14.
 */
@Getter
@Wither
@Builder
public class Utilisateur {

    private Long id;
    private String login;
    private List<Fonction> fonctions;

    public Utilisateur(Long id, String login, List<Fonction> fonctions) {
        super();
        this.id = id;
        this.login = login;
        this.fonctions = fonctions;
    }
    
    public boolean haveLogin(String login) {
        Optional<String> l = Optional.fromNullable(this.login);
        if (l.isPresent()) {
            return l.get().equals(login);
        }
        return false;
    }

    public List<Fonction> getFonctions() {
        return fonctions;
    }

    public List<Fonction> getFonctions(Specification<Fonction> filtre) {
        if (fonctions == null) {
            return Collections.emptyList();
        }

        List<Fonction> ret = new ArrayList<Fonction>();
        for (Fonction fonction : fonctions) {
            if (filtre.isSatisfiedBy(fonction)) {
                ret.add(fonction);
            }
        }
        return ret;
    }
	
    public Fonction getFonction(CodeFonction codeFonction, Integer exercice) {
    	FiltreFonctionParExercice filtreExe = new FiltreFonctionParExercice(exercice);
		FiltreFonctionParCode filtreCode = new FiltreFonctionParCode(codeFonction.toString());
		
		List<Fonction> fonctionsFiltrees = getFonctions(filtreExe.and(filtreCode));
		Fonction fonction = null;
		if (fonctionsFiltrees.size() > 0) {
			fonction = fonctionsFiltrees.get(0);
		}
		return fonction;
    }
    
	/**
	 * Vérifie si l'utilisateur à ce droit (code fonction)
	 * @param codeFonction - Droit à vérifier
	 * @param exercice - Exercice de vérification du droit (null si on teste un droit non lié à un exercice)
	 * @return vrai si l'utilisateur a ce droit
	 */
	public boolean hasPermission(CodeFonction codeFonction, Integer exercice) {
		return getFonction(codeFonction, exercice) != null;
	}

    public Utilisateur(Long id, String login) {
        super();
        this.id = id;
        this.login = login;
    }
}
