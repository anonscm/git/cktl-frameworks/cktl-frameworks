/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.identite;

import java.util.List;

import lombok.Getter;
import lombok.experimental.Builder;
import lombok.experimental.Wither;

/**
 * Created by bourges on 17/10/14.
 * Modif by RNA 30/04/2015
 */
@Builder
@Wither
@Getter
public class Fonction {

    private Long id;
    private String codeInterne;
    private String categorie;
    private String description;
    private String libelle;
    private String idApplication;
    private String applicationLibelle;
    private boolean restreinteParExercice;
    private List<Integer> exercices;
	
    public Fonction(Long id, String categorie, String description, String libelle, String idApplication) {
    	this(id, null, categorie, description, libelle, idApplication);
    }	
	
    public Fonction(Long id, String codeInterne, String categorie, String description, String libelle, String idApplication) {
    	this(id, codeInterne, categorie, description, libelle, idApplication,null, false, null);
    }
    
    public Fonction (Long id, String codeInterne, String categorie, String description, String libelle, String idApplication, String applicationLibelle) {
    	this(id, codeInterne, categorie, description, libelle, idApplication, applicationLibelle, false, null);
    }
    
    public Fonction(Long id, String codeInterne, String categorie,
            String description, String libelle, String idApplication,
            String restreinteParExercice, List<Integer> exercices) {
        this(id, codeInterne, categorie, description, libelle, idApplication, null,restreinteParExercice.equals("O"), exercices);
    }

    public Fonction(Long id, String codeInterne, String categorie,
            String description, String libelle, String idApplication,
            String applicationLibelle,
            boolean restreinteParExercice, List<Integer> exercices) {
        super();
        this.id = id;
        this.codeInterne = codeInterne;
        this.categorie = categorie;
        this.description = description;
        this.libelle = libelle;
        this.idApplication = idApplication;
        this.applicationLibelle = applicationLibelle;
        this.restreinteParExercice = restreinteParExercice;
        this.exercices = exercices;
    }

    @Override
	public String toString() {
		return "Fonction [id=" + id + ", codeInterne=" + codeInterne
				+ ", categorie=" + categorie + ", idApplication=" + idApplication
				+ ", applicationLibelle="+applicationLibelle
				+ ", restreinteParExercice=" + restreinteParExercice + "]";
	}
}
