/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.identite.support.querydsl;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.gfc.admin.identite.Fonction;
import org.cocktail.gfc.admin.identite.Utilisateur;
import org.cocktail.gfc.admin.identite.support.UtilisateurRepository;
import org.cocktail.gfc.support.querydsl.QAdmExercice;
import org.cocktail.gfc.support.querydsl.QAdmFonction;
import org.cocktail.gfc.support.querydsl.QAdmTypeApplication;
import org.cocktail.gfc.support.querydsl.QAdmUtilisateur;
import org.cocktail.gfc.support.querydsl.QAdmUtilisateurFonct;
import org.cocktail.gfc.support.querydsl.QAdmUtilisateurFonctExer;
import org.cocktail.gfc.support.querydsl.QVAdmUtilisateurInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.RelationalPathBase;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Path;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.StringPath;

/**
 * Created by bourges on 17/10/14.
 */
public class QueryDSLUtilisateurRepository implements UtilisateurRepository {

    //Les tables
    private QAdmUtilisateur utilisateur = new QAdmUtilisateur("u");
    private QVAdmUtilisateurInfo utilisateurInfo = new QVAdmUtilisateurInfo("ui");
    private QAdmUtilisateurFonct utilisateurFonct = new QAdmUtilisateurFonct("uf");
    private QAdmFonction fonction = new QAdmFonction("f");
    private QAdmTypeApplication typeApplication = new QAdmTypeApplication("ta");
    private QAdmUtilisateurFonctExer utilisateurFonctExer = new QAdmUtilisateurFonctExer("ufe");
    private QAdmExercice exercice = new QAdmExercice("e");

    //les colonnes issues de l'union
    private PathBuilder<Utilisateur> ens = new PathBuilder<Utilisateur>(Utilisateur.class, "ens");
    private NumberPath<Long> persId = ens.getNumber(getPropertyName(utilisateur, utilisateur.persId), Long.class);
    private StringPath cptLogin = ens.getString(getPropertyName(utilisateurInfo, utilisateurInfo.cptLogin));
    private NumberPath<Long> fonOrdre = ens.getNumber(getPropertyName(fonction, fonction.fonOrdre), Long.class);
    private StringPath fonIdInterne = ens.getString(getPropertyName(fonction, fonction.fonIdInterne));
    private StringPath fonCategorie = ens.getString(getPropertyName(fonction, fonction.fonCategorie));
    private StringPath fonDescription = ens.getString(getPropertyName(fonction, fonction.fonDescription));
    private StringPath fonLibelle = ens.getString(getPropertyName(fonction, fonction.fonLibelle));
    private StringPath tyapStrid = ens.getString(getPropertyName(typeApplication, typeApplication.tyapStrid));
    private StringPath fonSpecExercice = ens.getString(getPropertyName(fonction, fonction.fonSpecExercice));
    private NumberPath<Integer> exeExercice = ens.getNumber(getPropertyName(exercice, exercice.exeExercice), Integer.class);

    @Autowired
	private QueryDslJdbcTemplate template;
    
    public List<Utilisateur> findAll() {
        SQLQuery sqlQuery = getSQLQuery();
        List<Utilisateur> utilisateurs = getUtilisateursFromSQLQuery(sqlQuery);
        return utilisateurs;
    }

    public Utilisateur findByID(Long id) {
        
        SQLQuery sqlQuery = getSQLQuery().where(persId.eq(id));
        
        List<Utilisateur> utilisateurs = getUtilisateursFromSQLQuery(sqlQuery);
        
        if (utilisateurs.size() > 0) {
            return utilisateurs.get(0);
        }
        return null;
    }

    public Utilisateur findByLogin(String login) {
        SQLQuery sqlQuery = getSQLQuery();
        sqlQuery.where(cptLogin.eq(login));
        List<Utilisateur> utilisateurs = getUtilisateursFromSQLQuery(sqlQuery);
        if (utilisateurs.size() > 0) {
            return utilisateurs.get(0);
        }
        return null;
    }

    private SQLQuery getSQLQuery() {
        SQLSubQuery sq1 = new SQLSubQuery()
                .from(utilisateur)
                .innerJoin(utilisateurInfo).on(utilisateur.persId.eq(utilisateurInfo.persId))
                .leftJoin(utilisateurFonct).on(utilisateur.utlOrdre.eq(utilisateurFonct.utlOrdre))
                .innerJoin(fonction).on(utilisateurFonct.fonOrdre.eq(fonction.fonOrdre))
                .innerJoin(typeApplication).on(fonction.tyapId.eq(typeApplication.tyapId))
                //Les fonctions NON liées à un exercice
                .where(fonction.fonSpecExercice.eq("N")); 
        SQLSubQuery sq2 = new SQLSubQuery()
                .from(utilisateur)
                .innerJoin(utilisateurInfo).on(utilisateur.persId.eq(utilisateurInfo.persId))
                .leftJoin(utilisateurFonct).on(utilisateur.utlOrdre.eq(utilisateurFonct.utlOrdre))
                .innerJoin(fonction).on(utilisateurFonct.fonOrdre.eq(fonction.fonOrdre))
                .innerJoin(typeApplication).on(fonction.tyapId.eq(typeApplication.tyapId))
                .innerJoin(utilisateurFonctExer).on(utilisateurFonct.ufOrdre.eq(utilisateurFonctExer.ufOrdre))
                .innerJoin(exercice).on(utilisateurFonctExer.exeOrdre.eq(exercice.exeOrdre))
                //Les fonctions liées à un exercice
                .where(fonction.fonSpecExercice.eq("O")); 
        
        @SuppressWarnings("rawtypes")
        Expression[] cols1 = {utilisateur.utlOrdre, utilisateur.persId, utilisateurInfo.cptLogin, fonction.fonOrdre, 
                fonction.fonIdInterne, fonction.fonCategorie, fonction.fonDescription, fonction.fonLibelle,
                fonction.fonSpecExercice, typeApplication.tyapStrid,
                //Pour avoir le même nb de collone que pour le 2eme select on revoit toujours 0
                Expressions.as(ConstantImpl.create(0), "EXE_EXERCICE")}; 
        @SuppressWarnings("rawtypes")
        Expression[] cols2 = {utilisateur.utlOrdre, utilisateur.persId, utilisateurInfo.cptLogin, fonction.fonOrdre, 
                fonction.fonIdInterne, fonction.fonCategorie, fonction.fonDescription, fonction.fonLibelle,
                fonction.fonSpecExercice, typeApplication.tyapStrid, exercice.exeExercice};               
        
        SQLQuery union = template.newSqlQuery().union(ens, sq1.list(cols1), sq2.list(cols2))
              //NB : Le order by est important pour la construction du graphe
                .orderBy(persId.asc(), fonOrdre.asc());
        
        return union;
    }

    private List<Utilisateur> getUtilisateursFromSQLQuery(SQLQuery sqlQuery) {	
    	QTuple selectExpr = new QTuple(persId, cptLogin, fonOrdre, fonIdInterne, fonCategorie, fonDescription, 
    			fonLibelle, tyapStrid, fonSpecExercice, exeExercice);
    	List<Tuple> tuples = template.query(sqlQuery, selectExpr);
        List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
        
        Utilisateur unUtilisateur = Utilisateur.builder().id(-1l).build();
        Fonction uneFonction = Fonction.builder().id(-1l).build();
        Integer unExercice = -1;
        for (Tuple tuple : tuples) {
            Long tPersId = tuple.get(persId);
            Long tFonOrdre = tuple.get(fonOrdre);
            String tFonSpecExercice = tuple.get(fonSpecExercice);
            Integer tExeExercice = tuple.get(exeExercice);
            if (tPersId != null && tFonOrdre != null && tFonSpecExercice != null && tExeExercice != null) {
                if (!tPersId.equals(unUtilisateur.getId())) {
                    unUtilisateur = Utilisateur.builder().id(tPersId).login(tuple.get(cptLogin))
                            .fonctions(new ArrayList<Fonction>()).build();
                    utilisateurs.add(unUtilisateur);
                    uneFonction = Fonction.builder().id(-1l).build();
                }
                if (!tFonOrdre.equals(uneFonction.getId())) {
                    uneFonction = Fonction.builder().id(tFonOrdre).codeInterne(tuple.get(fonIdInterne))
                            .categorie(tuple.get(fonCategorie)).description(tuple.get(fonDescription))
                            .libelle(tuple.get(fonLibelle)).idApplication(tuple.get(tyapStrid))
                            //On transforme ici en true les fonSpecExercice="O"
                            .restreinteParExercice(tFonSpecExercice.equals("O"))
                            .exercices(new ArrayList<Integer>()).build();
                    unUtilisateur.getFonctions().add(uneFonction);                
                    unExercice = -1;
                }
                //On ne tient pas compte des exeExercice=0 car ils correspondent à des fonctions non liées à un exercice
                if (!tExeExercice.equals(0) && !tExeExercice.equals(unExercice)) {
                    uneFonction.getExercices().add(tExeExercice);
                }                
            }
        }
        return utilisateurs;
    }

    private String getPropertyName(RelationalPathBase base, Path path) {
        return base.getMetadata(path).getName();
    }    

}
