/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.admin.identite.support.querydsl;

import java.util.List;

import org.cocktail.gfc.admin.identite.Fonction;
import org.cocktail.gfc.admin.identite.support.FonctionRepository;
import org.cocktail.gfc.support.querydsl.QAdmFonction;
import org.cocktail.gfc.support.querydsl.QAdmTypeApplication;
import org.cocktail.gfc.support.querydsl.QAdmUtilisateur;
import org.cocktail.gfc.support.querydsl.QAdmUtilisateurFonct;
import org.cocktail.gfc.support.querydsl.QVAdmUtilisateurInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.MappingProjection;

/**
 * Created by bourges on 17/10/14.
 */
public class QueryDSLFonctionRepository implements FonctionRepository {

    @Autowired
    QueryDslJdbcTemplate template;
    
    //Les tables
    QAdmFonction fonction = new QAdmFonction("f");
    QAdmTypeApplication typeApplication = new QAdmTypeApplication("ta");
    QAdmUtilisateurFonct utilisateurFonct = new QAdmUtilisateurFonct("uf");
    QAdmUtilisateur utilisateur = new QAdmUtilisateur("u");
    private QVAdmUtilisateurInfo utilisateurInfo = new QVAdmUtilisateurInfo("ui");
    
    //Projection
    Projection projection = new Projection(fonction, typeApplication);
    
    //Queries
    /**
     * @return le SQLQuery de base pour les fonctions
     */
    private SQLQuery getFonctionsQuery() {
    	SQLQuery sqlQuery = template.newSqlQuery().from(fonction)
      		.leftJoin(fonction.admFonctionTyapIdFk, typeApplication);
    	return sqlQuery;
    }
    
    /**
     * @param sqlQuery de départ
     * @param login de l'utilisateur
     * @return le SQL query avec le where sur l'utilsateur
     */
    private SQLQuery addByUtilisateurClause(SQLQuery sqlQuery, Long userId) {
		sqlQuery = sqlQuery
				.join(utilisateurFonct).on(fonction.fonOrdre.eq(utilisateurFonct.fonOrdre.longValue()))
				.join(utilisateur).on(utilisateurFonct.utlOrdre.eq(utilisateur.utlOrdre))
				.where(utilisateur.persId.eq(userId));
		return sqlQuery;
	}

	/**
	 * @param sqlQuery de départ
	 * @param applicationID
	 * @return le SQL query avec le where sur l'application
	 */
	private SQLQuery addByApplicationClause(SQLQuery sqlQuery, String applicationID) {
		sqlQuery = sqlQuery
        		.where(typeApplication.tyapStrid.eq(applicationID));
		return sqlQuery;
	}

	//Finders
	public List<Fonction> findAll() {
        List<Fonction> fonctions = template.query(getFonctionsQuery(), projection);
        return fonctions;
    }
    
	public List<Fonction> findByApplication(String applicationID) {
        SQLQuery sqlQuery = addByApplicationClause(getFonctionsQuery(), applicationID);
        List<Fonction> fonctions = template.query(sqlQuery, projection);
        return fonctions;
	}    

    public List<Fonction> findByUtilisateur(Long userId) {
    	SQLQuery sqlQuery = addByUtilisateurClause(getFonctionsQuery(), userId);
        List<Fonction> fonctions = template.query(sqlQuery, projection);
        return fonctions;
	}

	public List<Fonction> findByUtilisateurAndApplication(Long userId,
			String applicationID) {
    	SQLQuery sqlQuery = addByUtilisateurClause(getFonctionsQuery(), userId);
    	sqlQuery = addByApplicationClause(sqlQuery, applicationID);
        List<Fonction> fonctions = template.query(sqlQuery, projection);
        return fonctions;
	}

	private static class Projection extends MappingProjection<Fonction> {

		private static final long serialVersionUID = 1L;
		private QAdmFonction f;
		private QAdmTypeApplication ta;

        public Projection(QAdmFonction f, QAdmTypeApplication ta) {
            super(Fonction.class, f.fonIdInterne, f.fonCategorie, f.fonDescription, f.fonLibelle, ta.tyapStrid, ta.tyapLibelle);
            this.f = f;
            this.ta = ta;
        }

        @Override
        protected Fonction map(Tuple row) {
            return Fonction.builder()
            		.id(row.get(f.fonOrdre))
            		.codeInterne(row.get(f.fonIdInterne))
            		.categorie( row.get(f.fonCategorie))
            		.description(row.get(f.fonDescription))
            		.libelle(row.get(f.fonLibelle))
            		.idApplication(row.get(ta.tyapStrid))
            		.applicationLibelle(row.get(ta.tyapLibelle))
            		.build();
        }
    }

}
