/*
 * Copyright COCKTAIL (www.asso-cocktail.fr), 1995, 2015 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.support;

import javax.sql.DataSource;

import org.cocktail.common.support.DefaultValidationNotificationHandler;
import org.cocktail.common.support.ValidationNotificationHandler;
import org.cocktail.gfc.admin.identite.support.IdentiteConfig;
import org.cocktail.gfc.admin.nomenclature.support.NomenclatureConfig;
import org.cocktail.gfc.budget.gestion.support.BudgetGestionConfig;
import org.cocktail.gfc.budget.nomenclature.support.NomenclatureBudgetaireConfig;
import org.cocktail.gfc.budget.preparation.support.PreparationBudgetaireConfig;
import org.cocktail.gfc.budget.validation.support.ValidationConfig;
import org.cocktail.gfc.budget.virement.support.VirementConfig;
import org.cocktail.gfc.comptabudgetaire.support.ComptaBudgetaireConfig;

import org.cocktail.gfc.integration.pilotage.support.PilotageIntegrationConfig;
import org.cocktail.gfc.operation.support.OperationConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mysema.query.sql.OracleTemplates;
import com.mysema.query.sql.SQLTemplates;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@Import({ IdentiteConfig.class, NomenclatureConfig.class, PreparationBudgetaireConfig.class, NomenclatureBudgetaireConfig.class,
        OperationConfig.class, ValidationConfig.class, PilotageIntegrationConfig.class, ComptaBudgetaireConfig.class, VirementConfig.class, BudgetGestionConfig.class})
@Profile("normal")
public class FwkCktlGFCSpringConfigNormal {

    @Value("${dbConnectURLGLOBAL:systemProperties['dbConnectURLGLOBAL']}")
    private String url;

    @Value("${dbConnectUserGLOBAL:systemProperties['dbConnectUserGLOBAL']}")
    private String userName;

    @Value("${dbConnectPasswordGLOBAL:systemProperties['dbConnectPasswordGLOBAL']}")
    private String password;

    @Value("${maximumPoolSize:systemProperties['maximumPoolSize']}")
    private Integer maximumPoolSize;

    protected SQLTemplates oracleTmpl;

    public FwkCktlGFCSpringConfigNormal() {
        this.oracleTmpl = OracleTemplates.builder().printSchema().build();
    }

    @Bean
    public DataSource dataSource() {
        final HikariDataSource ds = new HikariDataSource();
        ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        ds.setJdbcUrl(url);
        ds.setUsername(userName);
        ds.setPassword(password);
        ds.setMaximumPoolSize(maximumPoolSize);
        ds.setAutoCommit(false);
        ds.setConnectionTestQuery("select 1 from dual");
        return ds;
    }

    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    public QueryDslJdbcTemplate queryDslJdbcTemplate(DataSource dataSource) {
        return new QueryDslJdbcTemplate(new JdbcTemplate(dataSource), oracleTmpl);
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public ValidationNotificationHandler validationNotificationHandler() {
        return new DefaultValidationNotificationHandler();
    }

}
