package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmInventImp is a Querydsl query type for QImmInventImp
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmInventImp extends com.mysema.query.sql.RelationalPathBase<QImmInventImp> {

    private static final long serialVersionUID = 1483979722;

    public static final QImmInventImp immInventImp = new QImmInventImp("IMM_INVENT_IMP");

    public final StringPath bordereau = createString("bordereau");

    public final StringPath cr = createString("cr");

    public final DateTimePath<java.sql.Timestamp> dateAcquisition = createDateTime("dateAcquisition", java.sql.Timestamp.class);

    public final StringPath duree = createString("duree");

    public final StringPath exercice = createString("exercice");

    public final StringPath facture = createString("facture");

    public final StringPath fournisseur = createString("fournisseur");

    public final StringPath gestion = createString("gestion");

    public final StringPath idExterne = createString("idExterne");

    public final StringPath imputation = createString("imputation");

    public final StringPath informations = createString("informations");

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final StringPath mandat = createString("mandat");

    public final NumberPath<Long> montant = createNumber("montant", Long.class);

    public final NumberPath<Long> montantResiduel = createNumber("montantResiduel", Long.class);

    public final StringPath numero = createString("numero");

    public final StringPath numeroSerie = createString("numeroSerie");

    public final StringPath typeAmort = createString("typeAmort");

    public final StringPath typeFinancement = createString("typeFinancement");

    public final StringPath ub = createString("ub");

    public final com.mysema.query.sql.PrimaryKey<QImmInventImp> immInventImpPk = createPrimaryKey(idExterne);

    public QImmInventImp(String variable) {
        super(QImmInventImp.class, forVariable(variable), "GFC", "IMM_INVENT_IMP");
        addMetadata();
    }

    public QImmInventImp(String variable, String schema, String table) {
        super(QImmInventImp.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmInventImp(Path<? extends QImmInventImp> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_INVENT_IMP");
        addMetadata();
    }

    public QImmInventImp(PathMetadata<?> metadata) {
        super(QImmInventImp.class, metadata, "GFC", "IMM_INVENT_IMP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bordereau, ColumnMetadata.named("BORDEREAU").withIndex(15).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(cr, ColumnMetadata.named("CR").withIndex(5).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(dateAcquisition, ColumnMetadata.named("DATE_ACQUISITION").withIndex(20).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(duree, ColumnMetadata.named("DUREE").withIndex(7).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(exercice, ColumnMetadata.named("EXERCICE").withIndex(3).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(facture, ColumnMetadata.named("FACTURE").withIndex(18).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(fournisseur, ColumnMetadata.named("FOURNISSEUR").withIndex(17).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(gestion, ColumnMetadata.named("GESTION").withIndex(16).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(idExterne, ColumnMetadata.named("ID_EXTERNE").withIndex(1).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(imputation, ColumnMetadata.named("IMPUTATION").withIndex(6).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(informations, ColumnMetadata.named("INFORMATIONS").withIndex(13).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(19).ofType(Types.DECIMAL).withSize(0));
        addMetadata(mandat, ColumnMetadata.named("MANDAT").withIndex(14).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(montantResiduel, ColumnMetadata.named("MONTANT_RESIDUEL").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(numero, ColumnMetadata.named("NUMERO").withIndex(2).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(numeroSerie, ColumnMetadata.named("NUMERO_SERIE").withIndex(12).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(typeAmort, ColumnMetadata.named("TYPE_AMORT").withIndex(8).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(typeFinancement, ColumnMetadata.named("TYPE_FINANCEMENT").withIndex(11).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(ub, ColumnMetadata.named("UB").withIndex(4).ofType(Types.VARCHAR).withSize(1000).notNull());
    }

}

