package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEcriture is a Querydsl query type for QEcriture
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEcriture extends com.mysema.query.sql.RelationalPathBase<QEcriture> {

    private static final long serialVersionUID = 2117605706;

    public static final QEcriture ecriture = new QEcriture("ECRITURE");

    public final NumberPath<Long> broOrdre = createNumber("broOrdre", Long.class);

    public final NumberPath<Long> comOrdre = createNumber("comOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> ecrDate = createDateTime("ecrDate", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> ecrDateSaisie = createDateTime("ecrDateSaisie", java.sql.Timestamp.class);

    public final StringPath ecrEtat = createString("ecrEtat");

    public final StringPath ecrLibelle = createString("ecrLibelle");

    public final NumberPath<Long> ecrNumero = createNumber("ecrNumero", Long.class);

    public final NumberPath<Long> ecrNumeroBrouillard = createNumber("ecrNumeroBrouillard", Long.class);

    public final NumberPath<Long> ecrOrdre = createNumber("ecrOrdre", Long.class);

    public final StringPath ecrPostit = createString("ecrPostit");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> oriOrdre = createNumber("oriOrdre", Long.class);

    public final NumberPath<Long> tjoOrdre = createNumber("tjoOrdre", Long.class);

    public final NumberPath<Long> topOrdre = createNumber("topOrdre", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QEcriture> ecriturePk = createPrimaryKey(ecrOrdre);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> ecritureExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QTypeJournal> ecritureTjoOrdreFk = createForeignKey(tjoOrdre, "TJO_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QComptabilite> ecritureComOrdreFk = createForeignKey(comOrdre, "COM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QOrdreDePaiement> _ordreDePaiementEcrOrdreFk = createInvForeignKey(ecrOrdre, "ECR_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEcritureDetail> _ecrDetailFk1 = createInvForeignKey(ecrOrdre, "ECR_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCptReimputation> _cptReimputationEcrOrdreFk = createInvForeignKey(ecrOrdre, "ECR_ORDRE");

    public QEcriture(String variable) {
        super(QEcriture.class, forVariable(variable), "GFC", "ECRITURE");
        addMetadata();
    }

    public QEcriture(String variable, String schema, String table) {
        super(QEcriture.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEcriture(Path<? extends QEcriture> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ECRITURE");
        addMetadata();
    }

    public QEcriture(PathMetadata<?> metadata) {
        super(QEcriture.class, metadata, "GFC", "ECRITURE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(broOrdre, ColumnMetadata.named("BRO_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(comOrdre, ColumnMetadata.named("COM_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ecrDate, ColumnMetadata.named("ECR_DATE").withIndex(6).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(ecrDateSaisie, ColumnMetadata.named("ECR_DATE_SAISIE").withIndex(7).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(ecrEtat, ColumnMetadata.named("ECR_ETAT").withIndex(8).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(ecrLibelle, ColumnMetadata.named("ECR_LIBELLE").withIndex(9).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(ecrNumero, ColumnMetadata.named("ECR_NUMERO").withIndex(3).ofType(Types.DECIMAL).withSize(32));
        addMetadata(ecrNumeroBrouillard, ColumnMetadata.named("ECR_NUMERO_BROUILLARD").withIndex(10).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ecrOrdre, ColumnMetadata.named("ECR_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ecrPostit, ColumnMetadata.named("ECR_POSTIT").withIndex(11).ofType(Types.VARCHAR).withSize(200));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(oriOrdre, ColumnMetadata.named("ORI_ORDRE").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tjoOrdre, ColumnMetadata.named("TJO_ORDRE").withIndex(13).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(topOrdre, ColumnMetadata.named("TOP_ORDRE").withIndex(14).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(15).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

