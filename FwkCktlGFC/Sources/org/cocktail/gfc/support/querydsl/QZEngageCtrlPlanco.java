package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZEngageCtrlPlanco is a Querydsl query type for QZEngageCtrlPlanco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZEngageCtrlPlanco extends com.mysema.query.sql.RelationalPathBase<QZEngageCtrlPlanco> {

    private static final long serialVersionUID = -33900646;

    public static final QZEngageCtrlPlanco zEngageCtrlPlanco = new QZEngageCtrlPlanco("Z_ENGAGE_CTRL_PLANCO");

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final DateTimePath<java.sql.Timestamp> epcoDateSaisie = createDateTime("epcoDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> epcoHtSaisie = createNumber("epcoHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> epcoId = createNumber("epcoId", Long.class);

    public final NumberPath<java.math.BigDecimal> epcoMontantBudgetaire = createNumber("epcoMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> epcoMontantBudgetaireReste = createNumber("epcoMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> epcoTtcSaisie = createNumber("epcoTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> epcoTvaSaisie = createNumber("epcoTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> zengId = createNumber("zengId", Long.class);

    public final NumberPath<Long> zepcoId = createNumber("zepcoId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZEngageCtrlPlanco> sysC0075276 = createPrimaryKey(zepcoId);

    public final com.mysema.query.sql.ForeignKey<QZEngageBudget> zEngageCtrlPlanZengIdFk = createForeignKey(zengId, "ZENG_ID");

    public QZEngageCtrlPlanco(String variable) {
        super(QZEngageCtrlPlanco.class, forVariable(variable), "GFC", "Z_ENGAGE_CTRL_PLANCO");
        addMetadata();
    }

    public QZEngageCtrlPlanco(String variable, String schema, String table) {
        super(QZEngageCtrlPlanco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZEngageCtrlPlanco(Path<? extends QZEngageCtrlPlanco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_ENGAGE_CTRL_PLANCO");
        addMetadata();
    }

    public QZEngageCtrlPlanco(PathMetadata<?> metadata) {
        super(QZEngageCtrlPlanco.class, metadata, "GFC", "Z_ENGAGE_CTRL_PLANCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(epcoDateSaisie, ColumnMetadata.named("EPCO_DATE_SAISIE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(epcoHtSaisie, ColumnMetadata.named("EPCO_HT_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(epcoId, ColumnMetadata.named("EPCO_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(epcoMontantBudgetaire, ColumnMetadata.named("EPCO_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(epcoMontantBudgetaireReste, ColumnMetadata.named("EPCO_MONTANT_BUDGETAIRE_RESTE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(epcoTtcSaisie, ColumnMetadata.named("EPCO_TTC_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(epcoTvaSaisie, ColumnMetadata.named("EPCO_TVA_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(5).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(zengId, ColumnMetadata.named("ZENG_ID").withIndex(12).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zepcoId, ColumnMetadata.named("ZEPCO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

