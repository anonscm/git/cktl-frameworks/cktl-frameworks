package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEvtEvenement is a Querydsl query type for QEvtEvenement
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEvtEvenement extends com.mysema.query.sql.RelationalPathBase<QEvtEvenement> {

    private static final long serialVersionUID = -1656666297;

    public static final QEvtEvenement evtEvenement = new QEvtEvenement("EVT_EVENEMENT");

    public final StringPath evtAppName = createString("evtAppName");

    public final DateTimePath<java.sql.Timestamp> evtDateCreation = createDateTime("evtDateCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> evtDateModif = createDateTime("evtDateModif", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> evtDatePrevue = createDateTime("evtDatePrevue", java.sql.Timestamp.class);

    public final NumberPath<Integer> evteId = createNumber("evteId", Integer.class);

    public final NumberPath<Integer> evtId = createNumber("evtId", Integer.class);

    public final StringPath evtObjet = createString("evtObjet");

    public final StringPath evtObservations = createString("evtObservations");

    public final StringPath evtStatut = createString("evtStatut");

    public final NumberPath<Integer> evttId = createNumber("evttId", Integer.class);

    public final NumberPath<Integer> freqId = createNumber("freqId", Integer.class);

    public final NumberPath<Long> persidCreation = createNumber("persidCreation", Long.class);

    public final NumberPath<Long> persidModif = createNumber("persidModif", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QEvtEvenement> evtEvenementPk = createPrimaryKey(evtId);

    public QEvtEvenement(String variable) {
        super(QEvtEvenement.class, forVariable(variable), "GRHUM", "EVT_EVENEMENT");
        addMetadata();
    }

    public QEvtEvenement(String variable, String schema, String table) {
        super(QEvtEvenement.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEvtEvenement(Path<? extends QEvtEvenement> path) {
        super(path.getType(), path.getMetadata(), "GRHUM", "EVT_EVENEMENT");
        addMetadata();
    }

    public QEvtEvenement(PathMetadata<?> metadata) {
        super(QEvtEvenement.class, metadata, "GRHUM", "EVT_EVENEMENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(evtAppName, ColumnMetadata.named("EVT_APP_NAME").withIndex(13).ofType(Types.VARCHAR).withSize(64));
        addMetadata(evtDateCreation, ColumnMetadata.named("EVT_DATE_CREATION").withIndex(7).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(evtDateModif, ColumnMetadata.named("EVT_DATE_MODIF").withIndex(9).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(evtDatePrevue, ColumnMetadata.named("EVT_DATE_PREVUE").withIndex(8).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(evteId, ColumnMetadata.named("EVTE_ID").withIndex(3).ofType(Types.DECIMAL).withSize(8).notNull());
        addMetadata(evtId, ColumnMetadata.named("EVT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(8).notNull());
        addMetadata(evtObjet, ColumnMetadata.named("EVT_OBJET").withIndex(10).ofType(Types.VARCHAR).withSize(1024));
        addMetadata(evtObservations, ColumnMetadata.named("EVT_OBSERVATIONS").withIndex(11).ofType(Types.VARCHAR).withSize(1024));
        addMetadata(evtStatut, ColumnMetadata.named("EVT_STATUT").withIndex(12).ofType(Types.VARCHAR).withSize(64));
        addMetadata(evttId, ColumnMetadata.named("EVTT_ID").withIndex(2).ofType(Types.DECIMAL).withSize(8).notNull());
        addMetadata(freqId, ColumnMetadata.named("FREQ_ID").withIndex(4).ofType(Types.DECIMAL).withSize(8));
        addMetadata(persidCreation, ColumnMetadata.named("PERSID_CREATION").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(persidModif, ColumnMetadata.named("PERSID_MODIF").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

