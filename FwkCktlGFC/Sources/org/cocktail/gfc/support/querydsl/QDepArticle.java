package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepArticle is a Querydsl query type for QDepArticle
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepArticle extends com.mysema.query.sql.RelationalPathBase<QDepArticle> {

    private static final long serialVersionUID = -308353624;

    public static final QDepArticle depArticle = new QDepArticle("DEP_ARTICLE");

    public final NumberPath<Long> artcId = createNumber("artcId", Long.class);

    public final NumberPath<Long> artId = createNumber("artId", Long.class);

    public final NumberPath<Long> artIdPere = createNumber("artIdPere", Long.class);

    public final StringPath artLibelle = createString("artLibelle");

    public final NumberPath<java.math.BigDecimal> artPrixHt = createNumber("artPrixHt", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> artPrixTotalHt = createNumber("artPrixTotalHt", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> artPrixTotalTtc = createNumber("artPrixTotalTtc", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> artPrixTtc = createNumber("artPrixTtc", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> artQuantite = createNumber("artQuantite", java.math.BigDecimal.class);

    public final StringPath artReference = createString("artReference");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> ceOrdre = createNumber("ceOrdre", Long.class);

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final NumberPath<Long> tvaId = createNumber("tvaId", Long.class);

    public final NumberPath<Long> typaId = createNumber("typaId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QDepArticle> depArticlePk = createPrimaryKey(artId);

    public final com.mysema.query.sql.ForeignKey<QAdmTva> depArticleTvaIdFk = createForeignKey(tvaId, "TVA_ID");

    public final com.mysema.query.sql.ForeignKey<QAttribution> depArticleAttOrdreFk = createForeignKey(attOrdre, "ATT_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCodeExer> depArticleCeOrdreFk = createForeignKey(ceOrdre, "CE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCatCatalogueArticle> depArticleArtcIdFk = createForeignKey(artcId, "CAAR_ID");

    public final com.mysema.query.sql.ForeignKey<QCommande> depArticleCommIdFk = createForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QDepArticle> depArticleArtIdPereFk = createForeignKey(artIdPere, "ART_ID");

    public final com.mysema.query.sql.ForeignKey<QTypeAchat> depArticleTypaIdFk = createForeignKey(typaId, "TYPA_ID");

    public final com.mysema.query.sql.ForeignKey<QXlabCommandeLigne> _xlabCommligArtIdFk = createInvForeignKey(artId, "ART_ID");

    public final com.mysema.query.sql.ForeignKey<QB2bCxmlItem> _bciArtIdFk = createInvForeignKey(artId, "ART_ID");

    public final com.mysema.query.sql.ForeignKey<QDepArticle> _depArticleArtIdPereFk = createInvForeignKey(artId, "ART_ID_PERE");

    public QDepArticle(String variable) {
        super(QDepArticle.class, forVariable(variable), "GFC", "DEP_ARTICLE");
        addMetadata();
    }

    public QDepArticle(String variable, String schema, String table) {
        super(QDepArticle.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepArticle(Path<? extends QDepArticle> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEP_ARTICLE");
        addMetadata();
    }

    public QDepArticle(PathMetadata<?> metadata) {
        super(QDepArticle.class, metadata, "GFC", "DEP_ARTICLE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(artcId, ColumnMetadata.named("ARTC_ID").withIndex(10).ofType(Types.DECIMAL).withSize(0));
        addMetadata(artId, ColumnMetadata.named("ART_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(artIdPere, ColumnMetadata.named("ART_ID_PERE").withIndex(14).ofType(Types.DECIMAL).withSize(0));
        addMetadata(artLibelle, ColumnMetadata.named("ART_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(artPrixHt, ColumnMetadata.named("ART_PRIX_HT").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(artPrixTotalHt, ColumnMetadata.named("ART_PRIX_TOTAL_HT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(artPrixTotalTtc, ColumnMetadata.named("ART_PRIX_TOTAL_TTC").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(artPrixTtc, ColumnMetadata.named("ART_PRIX_TTC").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(artQuantite, ColumnMetadata.named("ART_QUANTITE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(artReference, ColumnMetadata.named("ART_REFERENCE").withIndex(9).ofType(Types.VARCHAR).withSize(50));
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(11).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ceOrdre, ColumnMetadata.named("CE_ORDRE").withIndex(12).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tvaId, ColumnMetadata.named("TVA_ID").withIndex(13).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(typaId, ColumnMetadata.named("TYPA_ID").withIndex(15).ofType(Types.DECIMAL).withSize(0));
    }

}

