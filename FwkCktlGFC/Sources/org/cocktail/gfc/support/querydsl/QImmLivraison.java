package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmLivraison is a Querydsl query type for QImmLivraison
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmLivraison extends com.mysema.query.sql.RelationalPathBase<QImmLivraison> {

    private static final long serialVersionUID = 592352457;

    public static final QImmLivraison immLivraison = new QImmLivraison("IMM_LIVRAISON");

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final NumberPath<Long> devId = createNumber("devId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final DateTimePath<java.sql.Timestamp> livDate = createDateTime("livDate", java.sql.Timestamp.class);

    public final StringPath livDivers = createString("livDivers");

    public final StringPath livNumero = createString("livNumero");

    public final NumberPath<Long> livOrdre = createNumber("livOrdre", Long.class);

    public final StringPath livReception = createString("livReception");

    public final NumberPath<Long> magId = createNumber("magId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmLivraison> immLivraisonPk = createPrimaryKey(livOrdre);

    public final com.mysema.query.sql.ForeignKey<QImmMagasin> magasinFk2 = createForeignKey(magId, "MAG_ID");

    public final com.mysema.query.sql.ForeignKey<QImmLivraisonDetail> _livraisonFk = createInvForeignKey(livOrdre, "LIV_ORDRE");

    public QImmLivraison(String variable) {
        super(QImmLivraison.class, forVariable(variable), "GFC", "IMM_LIVRAISON");
        addMetadata();
    }

    public QImmLivraison(String variable, String schema, String table) {
        super(QImmLivraison.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmLivraison(Path<? extends QImmLivraison> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_LIVRAISON");
        addMetadata();
    }

    public QImmLivraison(PathMetadata<?> metadata) {
        super(QImmLivraison.class, metadata, "GFC", "IMM_LIVRAISON");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(devId, ColumnMetadata.named("DEV_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(livDate, ColumnMetadata.named("LIV_DATE").withIndex(5).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(livDivers, ColumnMetadata.named("LIV_DIVERS").withIndex(6).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(livNumero, ColumnMetadata.named("LIV_NUMERO").withIndex(7).ofType(Types.VARCHAR).withSize(100));
        addMetadata(livOrdre, ColumnMetadata.named("LIV_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(livReception, ColumnMetadata.named("LIV_RECEPTION").withIndex(8).ofType(Types.VARCHAR).withSize(200));
        addMetadata(magId, ColumnMetadata.named("MAG_ID").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(0));
    }

}

