package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZEngageBudget is a Querydsl query type for QZEngageBudget
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZEngageBudget extends com.mysema.query.sql.RelationalPathBase<QZEngageBudget> {

    private static final long serialVersionUID = -1513156545;

    public static final QZEngageBudget zEngageBudget = new QZEngageBudget("Z_ENGAGE_BUDGET");

    public final DateTimePath<java.sql.Timestamp> engDateSaisie = createDateTime("engDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> engHtSaisie = createNumber("engHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final StringPath engLibelle = createString("engLibelle");

    public final NumberPath<java.math.BigDecimal> engMontantBudgetaire = createNumber("engMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> engMontantBudgetaireReste = createNumber("engMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<Long> engNumero = createNumber("engNumero", Long.class);

    public final NumberPath<java.math.BigDecimal> engTtcSaisie = createNumber("engTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> engTvaSaisie = createNumber("engTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<Long> tyapId = createNumber("tyapId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> zengDate = createDateTime("zengDate", java.sql.Timestamp.class);

    public final NumberPath<Long> zengId = createNumber("zengId", Long.class);

    public final NumberPath<Long> zengUtlOrdre = createNumber("zengUtlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZEngageBudget> sysC0078002 = createPrimaryKey(zengId);

    public final com.mysema.query.sql.ForeignKey<QZEngageCtrlAction> _zEngageCtrlActiZengIdFk = createInvForeignKey(zengId, "ZENG_ID");

    public final com.mysema.query.sql.ForeignKey<QZEngageCtrlPlanco> _zEngageCtrlPlanZengIdFk = createInvForeignKey(zengId, "ZENG_ID");

    public final com.mysema.query.sql.ForeignKey<QZEngageCtrlMarche> _zEngageCtrlMarcZengIdFk = createInvForeignKey(zengId, "ZENG_ID");

    public final com.mysema.query.sql.ForeignKey<QZEngageCtrlHorsMarche> _zEngageCtrlHmZengIdFk = createInvForeignKey(zengId, "ZENG_ID");

    public final com.mysema.query.sql.ForeignKey<QZEngageCtrlConvention> _zEngageCtrlConZengIdFk = createInvForeignKey(zengId, "ZENG_ID");

    public final com.mysema.query.sql.ForeignKey<QZEngageCtrlAnalytique> _zEngageCtrlAnalZengIdFk = createInvForeignKey(zengId, "ZENG_ID");

    public QZEngageBudget(String variable) {
        super(QZEngageBudget.class, forVariable(variable), "GFC", "Z_ENGAGE_BUDGET");
        addMetadata();
    }

    public QZEngageBudget(String variable, String schema, String table) {
        super(QZEngageBudget.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZEngageBudget(Path<? extends QZEngageBudget> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_ENGAGE_BUDGET");
        addMetadata();
    }

    public QZEngageBudget(PathMetadata<?> metadata) {
        super(QZEngageBudget.class, metadata, "GFC", "Z_ENGAGE_BUDGET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(engDateSaisie, ColumnMetadata.named("ENG_DATE_SAISIE").withIndex(17).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(engHtSaisie, ColumnMetadata.named("ENG_HT_SAISIE").withIndex(14).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(engLibelle, ColumnMetadata.named("ENG_LIBELLE").withIndex(11).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(engMontantBudgetaire, ColumnMetadata.named("ENG_MONTANT_BUDGETAIRE").withIndex(12).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engMontantBudgetaireReste, ColumnMetadata.named("ENG_MONTANT_BUDGETAIRE_RESTE").withIndex(13).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engNumero, ColumnMetadata.named("ENG_NUMERO").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(engTtcSaisie, ColumnMetadata.named("ENG_TTC_SAISIE").withIndex(16).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engTvaSaisie, ColumnMetadata.named("ENG_TVA_SAISIE").withIndex(15).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyapId, ColumnMetadata.named("TYAP_ID").withIndex(18).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(19).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zengDate, ColumnMetadata.named("ZENG_DATE").withIndex(2).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(zengId, ColumnMetadata.named("ZENG_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zengUtlOrdre, ColumnMetadata.named("ZENG_UTL_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

