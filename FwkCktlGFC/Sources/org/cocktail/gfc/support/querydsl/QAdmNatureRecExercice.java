package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmNatureRecExercice is a Querydsl query type for QAdmNatureRecExercice
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmNatureRecExercice extends com.mysema.query.sql.RelationalPathBase<QAdmNatureRecExercice> {

    private static final long serialVersionUID = 417263720;

    public static final QAdmNatureRecExercice admNatureRecExercice = new QAdmNatureRecExercice("ADM_NATURE_REC_EXERCICE");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> idAdmNatureRec = createNumber("idAdmNatureRec", Long.class);

    public final NumberPath<Long> idAdmNatureRecExer = createNumber("idAdmNatureRecExer", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmNatureRecExercice> admNatureRecExercicePk = createPrimaryKey(idAdmNatureRecExer);

    public final com.mysema.query.sql.ForeignKey<QAdmNatureRec> admNatureRecNatRecExerFk = createForeignKey(idAdmNatureRec, "ID_ADM_NATURE_REC");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> admExerciceNatRecExerFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmOriNatRecPcptExer> _admOriNatRecPcptExNatFk = createInvForeignKey(idAdmNatureRecExer, "ID_ADM_NATURE_REC_EXER");

    public QAdmNatureRecExercice(String variable) {
        super(QAdmNatureRecExercice.class, forVariable(variable), "GFC", "ADM_NATURE_REC_EXERCICE");
        addMetadata();
    }

    public QAdmNatureRecExercice(String variable, String schema, String table) {
        super(QAdmNatureRecExercice.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmNatureRecExercice(Path<? extends QAdmNatureRecExercice> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_NATURE_REC_EXERCICE");
        addMetadata();
    }

    public QAdmNatureRecExercice(PathMetadata<?> metadata) {
        super(QAdmNatureRecExercice.class, metadata, "GFC", "ADM_NATURE_REC_EXERCICE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(idAdmNatureRec, ColumnMetadata.named("ID_ADM_NATURE_REC").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idAdmNatureRecExer, ColumnMetadata.named("ID_ADM_NATURE_REC_EXER").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
    }

}

