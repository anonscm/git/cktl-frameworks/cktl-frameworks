package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmCodeAnalytiqueEb is a Querydsl query type for QAdmCodeAnalytiqueEb
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmCodeAnalytiqueEb extends com.mysema.query.sql.RelationalPathBase<QAdmCodeAnalytiqueEb> {

    private static final long serialVersionUID = -270100194;

    public static final QAdmCodeAnalytiqueEb admCodeAnalytiqueEb = new QAdmCodeAnalytiqueEb("ADM_CODE_ANALYTIQUE_EB");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<Long> caoId = createNumber("caoId", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmCodeAnalytiqueEb> sysC0075329 = createPrimaryKey(caoId);

    public final com.mysema.query.sql.ForeignKey<QAdmEb> admCaoIdAdmEbFk = createForeignKey(idAdmEb, "ID_ADM_EB");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> admCaoCanIdFk = createForeignKey(canId, "CAN_ID");

    public QAdmCodeAnalytiqueEb(String variable) {
        super(QAdmCodeAnalytiqueEb.class, forVariable(variable), "GFC", "ADM_CODE_ANALYTIQUE_EB");
        addMetadata();
    }

    public QAdmCodeAnalytiqueEb(String variable, String schema, String table) {
        super(QAdmCodeAnalytiqueEb.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmCodeAnalytiqueEb(Path<? extends QAdmCodeAnalytiqueEb> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_CODE_ANALYTIQUE_EB");
        addMetadata();
    }

    public QAdmCodeAnalytiqueEb(PathMetadata<?> metadata) {
        super(QAdmCodeAnalytiqueEb.class, metadata, "GFC", "ADM_CODE_ANALYTIQUE_EB");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(caoId, ColumnMetadata.named("CAO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

