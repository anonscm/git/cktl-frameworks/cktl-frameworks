package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVMandatReimp is a Querydsl query type for QVMandatReimp
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVMandatReimp extends com.mysema.query.sql.RelationalPathBase<QVMandatReimp> {

    private static final long serialVersionUID = -860966937;

    public static final QVMandatReimp vMandatReimp = new QVMandatReimp("V_MANDAT_REIMP");

    public final SimplePath<Object> borDateVisa = createSimple("borDateVisa", Object.class);

    public final SimplePath<Object> borId = createSimple("borId", Object.class);

    public final SimplePath<Object> borNum = createSimple("borNum", Object.class);

    public final SimplePath<Object> ecrDate = createSimple("ecrDate", Object.class);

    public final SimplePath<Object> ecrDateSaisie = createSimple("ecrDateSaisie", Object.class);

    public final SimplePath<Object> ecrOrdre = createSimple("ecrOrdre", Object.class);

    public final SimplePath<Object> ecrSacd = createSimple("ecrSacd", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> fouOrdre = createSimple("fouOrdre", Object.class);

    public final SimplePath<Object> gesCode = createSimple("gesCode", Object.class);

    public final SimplePath<Object> gesLib = createSimple("gesLib", Object.class);

    public final SimplePath<Object> impLib = createSimple("impLib", Object.class);

    public final SimplePath<Object> manEtat = createSimple("manEtat", Object.class);

    public final SimplePath<Object> manId = createSimple("manId", Object.class);

    public final SimplePath<Object> manLib = createSimple("manLib", Object.class);

    public final SimplePath<Object> manMont = createSimple("manMont", Object.class);

    public final SimplePath<Object> manNum = createSimple("manNum", Object.class);

    public final SimplePath<Object> manTtc = createSimple("manTtc", Object.class);

    public final SimplePath<Object> manTva = createSimple("manTva", Object.class);

    public final SimplePath<Object> pcoNum = createSimple("pcoNum", Object.class);

    public final SimplePath<Object> tboOrdre = createSimple("tboOrdre", Object.class);

    public QVMandatReimp(String variable) {
        super(QVMandatReimp.class, forVariable(variable), "GFC", "V_MANDAT_REIMP");
        addMetadata();
    }

    public QVMandatReimp(String variable, String schema, String table) {
        super(QVMandatReimp.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVMandatReimp(Path<? extends QVMandatReimp> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_MANDAT_REIMP");
        addMetadata();
    }

    public QVMandatReimp(PathMetadata<?> metadata) {
        super(QVMandatReimp.class, metadata, "GFC", "V_MANDAT_REIMP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(borDateVisa, ColumnMetadata.named("BOR_DATE_VISA").withIndex(19).ofType(Types.OTHER).withSize(0));
        addMetadata(borId, ColumnMetadata.named("BOR_ID").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(borNum, ColumnMetadata.named("BOR_NUM").withIndex(20).ofType(Types.OTHER).withSize(0));
        addMetadata(ecrDate, ColumnMetadata.named("ECR_DATE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(ecrDateSaisie, ColumnMetadata.named("ECR_DATE_SAISIE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(ecrOrdre, ColumnMetadata.named("ECR_ORDRE").withIndex(15).ofType(Types.OTHER).withSize(0));
        addMetadata(ecrSacd, ColumnMetadata.named("ECR_SACD").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(gesLib, ColumnMetadata.named("GES_LIB").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(impLib, ColumnMetadata.named("IMP_LIB").withIndex(18).ofType(Types.OTHER).withSize(0));
        addMetadata(manEtat, ColumnMetadata.named("MAN_ETAT").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(manId, ColumnMetadata.named("MAN_ID").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(manLib, ColumnMetadata.named("MAN_LIB").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(manMont, ColumnMetadata.named("MAN_MONT").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(manNum, ColumnMetadata.named("MAN_NUM").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(manTtc, ColumnMetadata.named("MAN_TTC").withIndex(21).ofType(Types.OTHER).withSize(0));
        addMetadata(manTva, ColumnMetadata.named("MAN_TVA").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(7).ofType(Types.OTHER).withSize(0));
    }

}

