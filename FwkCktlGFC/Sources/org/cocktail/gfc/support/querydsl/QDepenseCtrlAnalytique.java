package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepenseCtrlAnalytique is a Querydsl query type for QDepenseCtrlAnalytique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepenseCtrlAnalytique extends com.mysema.query.sql.RelationalPathBase<QDepenseCtrlAnalytique> {

    private static final long serialVersionUID = -1116732225;

    public static final QDepenseCtrlAnalytique depenseCtrlAnalytique = new QDepenseCtrlAnalytique("DEPENSE_CTRL_ANALYTIQUE");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<java.math.BigDecimal> danaHtSaisie = createNumber("danaHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> danaId = createNumber("danaId", Long.class);

    public final NumberPath<java.math.BigDecimal> danaMontantBudgetaire = createNumber("danaMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> danaTtcSaisie = createNumber("danaTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> danaTvaSaisie = createNumber("danaTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QDepenseCtrlAnalytique> depenseCtrlAnalytiquePk = createPrimaryKey(danaId);

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> depenseCtrlAnalDepIdFk = createForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> depenseCtrlAnalCanIdFk = createForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> depenseCtrlAnalExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QDepenseCtrlAnalytique(String variable) {
        super(QDepenseCtrlAnalytique.class, forVariable(variable), "GFC", "DEPENSE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public QDepenseCtrlAnalytique(String variable, String schema, String table) {
        super(QDepenseCtrlAnalytique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepenseCtrlAnalytique(Path<? extends QDepenseCtrlAnalytique> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEPENSE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public QDepenseCtrlAnalytique(PathMetadata<?> metadata) {
        super(QDepenseCtrlAnalytique.class, metadata, "GFC", "DEPENSE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(danaHtSaisie, ColumnMetadata.named("DANA_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(danaId, ColumnMetadata.named("DANA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(danaMontantBudgetaire, ColumnMetadata.named("DANA_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(danaTtcSaisie, ColumnMetadata.named("DANA_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(danaTvaSaisie, ColumnMetadata.named("DANA_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

