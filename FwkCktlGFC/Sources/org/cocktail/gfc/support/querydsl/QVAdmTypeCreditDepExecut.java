package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAdmTypeCreditDepExecut is a Querydsl query type for QVAdmTypeCreditDepExecut
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAdmTypeCreditDepExecut extends com.mysema.query.sql.RelationalPathBase<QVAdmTypeCreditDepExecut> {

    private static final long serialVersionUID = 1838748471;

    public static final QVAdmTypeCreditDepExecut vAdmTypeCreditDepExecut = new QVAdmTypeCreditDepExecut("V_ADM_TYPE_CREDIT_DEP_EXECUT");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath tcdAbrege = createString("tcdAbrege");

    public final StringPath tcdBudget = createString("tcdBudget");

    public final StringPath tcdCode = createString("tcdCode");

    public final StringPath tcdLibelle = createString("tcdLibelle");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> tcdPresident = createNumber("tcdPresident", java.math.BigDecimal.class);

    public final StringPath tcdSect = createString("tcdSect");

    public final StringPath tcdType = createString("tcdType");

    public QVAdmTypeCreditDepExecut(String variable) {
        super(QVAdmTypeCreditDepExecut.class, forVariable(variable), "GFC", "V_ADM_TYPE_CREDIT_DEP_EXECUT");
        addMetadata();
    }

    public QVAdmTypeCreditDepExecut(String variable, String schema, String table) {
        super(QVAdmTypeCreditDepExecut.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAdmTypeCreditDepExecut(Path<? extends QVAdmTypeCreditDepExecut> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ADM_TYPE_CREDIT_DEP_EXECUT");
        addMetadata();
    }

    public QVAdmTypeCreditDepExecut(PathMetadata<?> metadata) {
        super(QVAdmTypeCreditDepExecut.class, metadata, "GFC", "V_ADM_TYPE_CREDIT_DEP_EXECUT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(tcdAbrege, ColumnMetadata.named("TCD_ABREGE").withIndex(5).ofType(Types.VARCHAR).withSize(12).notNull());
        addMetadata(tcdBudget, ColumnMetadata.named("TCD_BUDGET").withIndex(9).ofType(Types.VARCHAR).withSize(20));
        addMetadata(tcdCode, ColumnMetadata.named("TCD_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(2).notNull());
        addMetadata(tcdLibelle, ColumnMetadata.named("TCD_LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(40).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcdPresident, ColumnMetadata.named("TCD_PRESIDENT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(tcdSect, ColumnMetadata.named("TCD_SECT").withIndex(6).ofType(Types.CHAR).withSize(1).notNull());
        addMetadata(tcdType, ColumnMetadata.named("TCD_TYPE").withIndex(8).ofType(Types.VARCHAR).withSize(20));
    }

}

