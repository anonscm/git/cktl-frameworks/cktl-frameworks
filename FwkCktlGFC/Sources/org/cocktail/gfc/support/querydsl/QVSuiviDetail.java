package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviDetail is a Querydsl query type for QVSuiviDetail
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviDetail extends com.mysema.query.sql.RelationalPathBase<QVSuiviDetail> {

    private static final long serialVersionUID = 513958230;

    public static final QVSuiviDetail vSuiviDetail = new QVSuiviDetail("V_SUIVI_DETAIL");

    public final SimplePath<Object> commId = createSimple("commId", Object.class);

    public final SimplePath<Object> commNumero = createSimple("commNumero", Object.class);

    public final SimplePath<Object> commNumeroStr = createSimple("commNumeroStr", Object.class);

    public final SimplePath<Object> dconRconHtSaisie = createSimple("dconRconHtSaisie", Object.class);

    public final SimplePath<Object> dconRconTtcSaisie = createSimple("dconRconTtcSaisie", Object.class);

    public final SimplePath<Object> depRecId = createSimple("depRecId", Object.class);

    public final SimplePath<Object> dppRppDateSaisie = createSimple("dppRppDateSaisie", Object.class);

    public final SimplePath<Object> dppRppId = createSimple("dppRppId", Object.class);

    public final SimplePath<Object> dppRppNatureLib = createSimple("dppRppNatureLib", Object.class);

    public final SimplePath<Object> dppRppNumero = createSimple("dppRppNumero", Object.class);

    public final SimplePath<Object> econFconDateSaisie = createSimple("econFconDateSaisie", Object.class);

    public final SimplePath<Object> econFconMontantBudgetaire = createSimple("econFconMontantBudgetaire", Object.class);

    public final SimplePath<Object> econFconMontantBudgetReste = createSimple("econFconMontantBudgetReste", Object.class);

    public final SimplePath<Object> engFacId = createSimple("engFacId", Object.class);

    public final SimplePath<Object> engFacNatureLib = createSimple("engFacNatureLib", Object.class);

    public final SimplePath<Object> engFacNumero = createSimple("engFacNumero", Object.class);

    public final SimplePath<Object> engFacNumeroStr = createSimple("engFacNumeroStr", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> nature = createSimple("nature", Object.class);

    public final SimplePath<Object> orgIdPosit = createSimple("orgIdPosit", Object.class);

    public final SimplePath<Object> orgIdPropo = createSimple("orgIdPropo", Object.class);

    public final SimplePath<Object> tapId = createSimple("tapId", Object.class);

    public final SimplePath<Object> tcdOrdrePosit = createSimple("tcdOrdrePosit", Object.class);

    public final SimplePath<Object> totalPosit = createSimple("totalPosit", Object.class);

    public final SimplePath<Object> totalPropo = createSimple("totalPropo", Object.class);

    public QVSuiviDetail(String variable) {
        super(QVSuiviDetail.class, forVariable(variable), "GFC", "V_SUIVI_DETAIL");
        addMetadata();
    }

    public QVSuiviDetail(String variable, String schema, String table) {
        super(QVSuiviDetail.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviDetail(Path<? extends QVSuiviDetail> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_DETAIL");
        addMetadata();
    }

    public QVSuiviDetail(PathMetadata<?> metadata) {
        super(QVSuiviDetail.class, metadata, "GFC", "V_SUIVI_DETAIL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(commNumero, ColumnMetadata.named("COMM_NUMERO").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(commNumeroStr, ColumnMetadata.named("COMM_NUMERO_STR").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(dconRconHtSaisie, ColumnMetadata.named("DCON_RCON_HT_SAISIE").withIndex(23).ofType(Types.OTHER).withSize(0));
        addMetadata(dconRconTtcSaisie, ColumnMetadata.named("DCON_RCON_TTC_SAISIE").withIndex(22).ofType(Types.OTHER).withSize(0));
        addMetadata(depRecId, ColumnMetadata.named("DEP_REC_ID").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(dppRppDateSaisie, ColumnMetadata.named("DPP_RPP_DATE_SAISIE").withIndex(24).ofType(Types.OTHER).withSize(0));
        addMetadata(dppRppId, ColumnMetadata.named("DPP_RPP_ID").withIndex(15).ofType(Types.OTHER).withSize(0));
        addMetadata(dppRppNatureLib, ColumnMetadata.named("DPP_RPP_NATURE_LIB").withIndex(20).ofType(Types.OTHER).withSize(0));
        addMetadata(dppRppNumero, ColumnMetadata.named("DPP_RPP_NUMERO").withIndex(21).ofType(Types.OTHER).withSize(0));
        addMetadata(econFconDateSaisie, ColumnMetadata.named("ECON_FCON_DATE_SAISIE").withIndex(19).ofType(Types.OTHER).withSize(0));
        addMetadata(econFconMontantBudgetaire, ColumnMetadata.named("ECON_FCON_MONTANT_BUDGETAIRE").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(econFconMontantBudgetReste, ColumnMetadata.named("ECON_FCON_MONTANT_BUDGET_RESTE").withIndex(18).ofType(Types.OTHER).withSize(0));
        addMetadata(engFacId, ColumnMetadata.named("ENG_FAC_ID").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(engFacNatureLib, ColumnMetadata.named("ENG_FAC_NATURE_LIB").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(engFacNumero, ColumnMetadata.named("ENG_FAC_NUMERO").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(engFacNumeroStr, ColumnMetadata.named("ENG_FAC_NUMERO_STR").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(nature, ColumnMetadata.named("NATURE").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdPosit, ColumnMetadata.named("ORG_ID_POSIT").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdPropo, ColumnMetadata.named("ORG_ID_PROPO").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdrePosit, ColumnMetadata.named("TCD_ORDRE_POSIT").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPosit, ColumnMetadata.named("TOTAL_POSIT").withIndex(26).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPropo, ColumnMetadata.named("TOTAL_PROPO").withIndex(25).ofType(Types.OTHER).withSize(0));
    }

}

