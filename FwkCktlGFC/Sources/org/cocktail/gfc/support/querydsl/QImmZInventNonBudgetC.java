package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmZInventNonBudgetC is a Querydsl query type for QImmZInventNonBudgetC
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmZInventNonBudgetC extends com.mysema.query.sql.RelationalPathBase<QImmZInventNonBudgetC> {

    private static final long serialVersionUID = 2123035485;

    public static final QImmZInventNonBudgetC immZInventNonBudgetC = new QImmZInventNonBudgetC("IMM_Z_INVENT_NON_BUDGET_C");

    public final NumberPath<Long> inbId = createNumber("inbId", Long.class);

    public final StringPath inbNumeroInventaire = createString("inbNumeroInventaire");

    public final NumberPath<Long> zinbId = createNumber("zinbId", Long.class);

    public QImmZInventNonBudgetC(String variable) {
        super(QImmZInventNonBudgetC.class, forVariable(variable), "GFC", "IMM_Z_INVENT_NON_BUDGET_C");
        addMetadata();
    }

    public QImmZInventNonBudgetC(String variable, String schema, String table) {
        super(QImmZInventNonBudgetC.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmZInventNonBudgetC(Path<? extends QImmZInventNonBudgetC> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_Z_INVENT_NON_BUDGET_C");
        addMetadata();
    }

    public QImmZInventNonBudgetC(PathMetadata<?> metadata) {
        super(QImmZInventNonBudgetC.class, metadata, "GFC", "IMM_Z_INVENT_NON_BUDGET_C");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(inbId, ColumnMetadata.named("INB_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(inbNumeroInventaire, ColumnMetadata.named("INB_NUMERO_INVENTAIRE").withIndex(3).ofType(Types.VARCHAR).withSize(200));
        addMetadata(zinbId, ColumnMetadata.named("ZINB_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
    }

}

