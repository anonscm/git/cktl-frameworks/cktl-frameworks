package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QConventionRa is a Querydsl query type for QConventionRa
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QConventionRa extends com.mysema.query.sql.RelationalPathBase<QConventionRa> {

    private static final long serialVersionUID = 597181697;

    public static final QConventionRa conventionRa = new QConventionRa("CONVENTION_RA");

    public final SimplePath<Object> conObjetCourt = createSimple("conObjetCourt", Object.class);

    public final SimplePath<Object> conReferenceExterne = createSimple("conReferenceExterne", Object.class);

    public final SimplePath<Object> craConReference = createSimple("craConReference", Object.class);

    public final SimplePath<Object> craExeCreation = createSimple("craExeCreation", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> orgCr = createSimple("orgCr", Object.class);

    public final SimplePath<Object> orgEtab = createSimple("orgEtab", Object.class);

    public final SimplePath<Object> orgLib = createSimple("orgLib", Object.class);

    public final SimplePath<Object> orgNiv = createSimple("orgNiv", Object.class);

    public final SimplePath<Object> orgSouscr = createSimple("orgSouscr", Object.class);

    public final SimplePath<Object> orgUb = createSimple("orgUb", Object.class);

    public final SimplePath<Object> orgUniv = createSimple("orgUniv", Object.class);

    public QConventionRa(String variable) {
        super(QConventionRa.class, forVariable(variable), "GFC", "CONVENTION_RA");
        addMetadata();
    }

    public QConventionRa(String variable, String schema, String table) {
        super(QConventionRa.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QConventionRa(Path<? extends QConventionRa> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CONVENTION_RA");
        addMetadata();
    }

    public QConventionRa(PathMetadata<?> metadata) {
        super(QConventionRa.class, metadata, "GFC", "CONVENTION_RA");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(conObjetCourt, ColumnMetadata.named("CON_OBJET_COURT").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(conReferenceExterne, ColumnMetadata.named("CON_REFERENCE_EXTERNE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(craConReference, ColumnMetadata.named("CRA_CON_REFERENCE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(craExeCreation, ColumnMetadata.named("CRA_EXE_CREATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(orgCr, ColumnMetadata.named("ORG_CR").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(orgEtab, ColumnMetadata.named("ORG_ETAB").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(orgLib, ColumnMetadata.named("ORG_LIB").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(orgNiv, ColumnMetadata.named("ORG_NIV").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(orgSouscr, ColumnMetadata.named("ORG_SOUSCR").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(orgUb, ColumnMetadata.named("ORG_UB").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(orgUniv, ColumnMetadata.named("ORG_UNIV").withIndex(8).ofType(Types.OTHER).withSize(0));
    }

}

