package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QXlabFournisseur is a Querydsl query type for QXlabFournisseur
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QXlabFournisseur extends com.mysema.query.sql.RelationalPathBase<QXlabFournisseur> {

    private static final long serialVersionUID = 601309739;

    public static final QXlabFournisseur xlabFournisseur = new QXlabFournisseur("XLAB_FOURNISSEUR");

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final StringPath xfouCode = createString("xfouCode");

    public final StringPath xfouCodeTva = createString("xfouCodeTva");

    public final StringPath xfouInsee = createString("xfouInsee");

    public final NumberPath<Long> xfouOrdre = createNumber("xfouOrdre", Long.class);

    public final StringPath xfouRaisonSoc = createString("xfouRaisonSoc");

    public final StringPath xfouSiret = createString("xfouSiret");

    public final com.mysema.query.sql.PrimaryKey<QXlabFournisseur> xlabFournisseurPk = createPrimaryKey(xfouOrdre);

    public final com.mysema.query.sql.ForeignKey<QFournisUlr> xlabFournisseurFouOrdreFk = createForeignKey(fouOrdre, "FOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QXlabCommande> _xlabCommandeXfouOrdreFk = createInvForeignKey(xfouOrdre, "XFOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QXlabMarche> _xlabMarcheXfouOrdreFk = createInvForeignKey(xfouOrdre, "XFOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QXlabFacture> _xlabFactureXfouOrdreFk = createInvForeignKey(xfouOrdre, "XFOU_ORDRE");

    public QXlabFournisseur(String variable) {
        super(QXlabFournisseur.class, forVariable(variable), "GFC", "XLAB_FOURNISSEUR");
        addMetadata();
    }

    public QXlabFournisseur(String variable, String schema, String table) {
        super(QXlabFournisseur.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QXlabFournisseur(Path<? extends QXlabFournisseur> path) {
        super(path.getType(), path.getMetadata(), "GFC", "XLAB_FOURNISSEUR");
        addMetadata();
    }

    public QXlabFournisseur(PathMetadata<?> metadata) {
        super(QXlabFournisseur.class, metadata, "GFC", "XLAB_FOURNISSEUR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(xfouCode, ColumnMetadata.named("XFOU_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(7));
        addMetadata(xfouCodeTva, ColumnMetadata.named("XFOU_CODE_TVA").withIndex(6).ofType(Types.VARCHAR).withSize(15));
        addMetadata(xfouInsee, ColumnMetadata.named("XFOU_INSEE").withIndex(5).ofType(Types.VARCHAR).withSize(13));
        addMetadata(xfouOrdre, ColumnMetadata.named("XFOU_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(xfouRaisonSoc, ColumnMetadata.named("XFOU_RAISON_SOC").withIndex(4).ofType(Types.VARCHAR).withSize(40).notNull());
        addMetadata(xfouSiret, ColumnMetadata.named("XFOU_SIRET").withIndex(7).ofType(Types.VARCHAR).withSize(14));
    }

}

