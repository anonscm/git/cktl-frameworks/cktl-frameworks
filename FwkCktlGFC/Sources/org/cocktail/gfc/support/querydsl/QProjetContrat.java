package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QProjetContrat is a Querydsl query type for QProjetContrat
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QProjetContrat extends com.mysema.query.sql.RelationalPathBase<QProjetContrat> {

    private static final long serialVersionUID = -33035802;

    public static final QProjetContrat projetContrat = new QProjetContrat("PROJET_CONTRAT");

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public final NumberPath<Long> pjtId = createNumber("pjtId", Long.class);

    public final NumberPath<Long> prcoId = createNumber("prcoId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QProjetContrat> projetContratPk = createPrimaryKey(prcoId);

    public QProjetContrat(String variable) {
        super(QProjetContrat.class, forVariable(variable), "GFC", "PROJET_CONTRAT");
        addMetadata();
    }

    public QProjetContrat(String variable, String schema, String table) {
        super(QProjetContrat.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QProjetContrat(Path<? extends QProjetContrat> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PROJET_CONTRAT");
        addMetadata();
    }

    public QProjetContrat(PathMetadata<?> metadata) {
        super(QProjetContrat.class, metadata, "GFC", "PROJET_CONTRAT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pjtId, ColumnMetadata.named("PJT_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(prcoId, ColumnMetadata.named("PRCO_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

