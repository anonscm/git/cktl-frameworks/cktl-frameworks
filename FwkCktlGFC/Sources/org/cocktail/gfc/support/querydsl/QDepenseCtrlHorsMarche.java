package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepenseCtrlHorsMarche is a Querydsl query type for QDepenseCtrlHorsMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepenseCtrlHorsMarche extends com.mysema.query.sql.RelationalPathBase<QDepenseCtrlHorsMarche> {

    private static final long serialVersionUID = 198773886;

    public static final QDepenseCtrlHorsMarche depenseCtrlHorsMarche = new QDepenseCtrlHorsMarche("DEPENSE_CTRL_HORS_MARCHE");

    public final NumberPath<Long> ceOrdre = createNumber("ceOrdre", Long.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<java.math.BigDecimal> dhomHtSaisie = createNumber("dhomHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dhomId = createNumber("dhomId", Long.class);

    public final NumberPath<java.math.BigDecimal> dhomMontantBudgetaire = createNumber("dhomMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dhomTtcSaisie = createNumber("dhomTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dhomTvaSaisie = createNumber("dhomTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> typaId = createNumber("typaId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QDepenseCtrlHorsMarche> depenseCtrlHorsMarchePk = createPrimaryKey(dhomId);

    public final com.mysema.query.sql.ForeignKey<QCodeExer> depenseCtrlHmCeOrdreFk = createForeignKey(ceOrdre, "CE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> depenseCtrlHmDepIdFk = createForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QTypeAchat> depenseCtrlHmTypaIdFk = createForeignKey(typaId, "TYPA_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> depenseCtrlHmExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QDepenseCtrlHorsMarche(String variable) {
        super(QDepenseCtrlHorsMarche.class, forVariable(variable), "GFC", "DEPENSE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public QDepenseCtrlHorsMarche(String variable, String schema, String table) {
        super(QDepenseCtrlHorsMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepenseCtrlHorsMarche(Path<? extends QDepenseCtrlHorsMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEPENSE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public QDepenseCtrlHorsMarche(PathMetadata<?> metadata) {
        super(QDepenseCtrlHorsMarche.class, metadata, "GFC", "DEPENSE_CTRL_HORS_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ceOrdre, ColumnMetadata.named("CE_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dhomHtSaisie, ColumnMetadata.named("DHOM_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dhomId, ColumnMetadata.named("DHOM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dhomMontantBudgetaire, ColumnMetadata.named("DHOM_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dhomTtcSaisie, ColumnMetadata.named("DHOM_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dhomTvaSaisie, ColumnMetadata.named("DHOM_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(typaId, ColumnMetadata.named("TYPA_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

