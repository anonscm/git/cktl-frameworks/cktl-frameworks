package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviDepense is a Querydsl query type for QVSuiviDepense
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviDepense extends com.mysema.query.sql.RelationalPathBase<QVSuiviDepense> {

    private static final long serialVersionUID = -1250733851;

    public static final QVSuiviDepense vSuiviDepense = new QVSuiviDepense("V_SUIVI_DEPENSE");

    public final NumberPath<Long> dispoReel = createNumber("dispoReel", Long.class);

    public final NumberPath<Long> dispoReelPosit = createNumber("dispoReelPosit", Long.class);

    public final NumberPath<Long> dispoVirtuel = createNumber("dispoVirtuel", Long.class);

    public final NumberPath<Long> exeOrdre = createNumber("exeOrdre", Long.class);

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<Long> totalEngage = createNumber("totalEngage", Long.class);

    public final NumberPath<Long> totalLiquide = createNumber("totalLiquide", Long.class);

    public final NumberPath<Long> totalMandate = createNumber("totalMandate", Long.class);

    public final NumberPath<Long> totalPositionne = createNumber("totalPositionne", Long.class);

    public final NumberPath<Long> totalPropose = createNumber("totalPropose", Long.class);

    public final NumberPath<Long> totalResteAPosit = createNumber("totalResteAPosit", Long.class);

    public final NumberPath<Long> totalReverse = createNumber("totalReverse", Long.class);

    public final NumberPath<Long> traOrdre = createNumber("traOrdre", Long.class);

    public QVSuiviDepense(String variable) {
        super(QVSuiviDepense.class, forVariable(variable), "GFC", "V_SUIVI_DEPENSE");
        addMetadata();
    }

    public QVSuiviDepense(String variable, String schema, String table) {
        super(QVSuiviDepense.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviDepense(Path<? extends QVSuiviDepense> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_DEPENSE");
        addMetadata();
    }

    public QVSuiviDepense(PathMetadata<?> metadata) {
        super(QVSuiviDepense.class, metadata, "GFC", "V_SUIVI_DEPENSE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dispoReel, ColumnMetadata.named("DISPO_REEL").withIndex(14).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dispoReelPosit, ColumnMetadata.named("DISPO_REEL_POSIT").withIndex(15).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dispoVirtuel, ColumnMetadata.named("DISPO_VIRTUEL").withIndex(13).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(5).ofType(Types.CHAR).withSize(1));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(totalEngage, ColumnMetadata.named("TOTAL_ENGAGE").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(totalLiquide, ColumnMetadata.named("TOTAL_LIQUIDE").withIndex(10).ofType(Types.DECIMAL).withSize(0));
        addMetadata(totalMandate, ColumnMetadata.named("TOTAL_MANDATE").withIndex(11).ofType(Types.DECIMAL).withSize(0));
        addMetadata(totalPositionne, ColumnMetadata.named("TOTAL_POSITIONNE").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(totalPropose, ColumnMetadata.named("TOTAL_PROPOSE").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(totalResteAPosit, ColumnMetadata.named("TOTAL_RESTE_A_POSIT").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(totalReverse, ColumnMetadata.named("TOTAL_REVERSE").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(traOrdre, ColumnMetadata.named("TRA_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
    }

}

