package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOpeTrancheBudRec is a Querydsl query type for QOpeTrancheBudRec
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOpeTrancheBudRec extends com.mysema.query.sql.RelationalPathBase<QOpeTrancheBudRec> {

    private static final long serialVersionUID = -2041938671;

    public static final QOpeTrancheBudRec opeTrancheBudRec = new QOpeTrancheBudRec("OPE_TRANCHE_BUD_REC");

    public final StringPath commentaire = createString("commentaire");

    public final DateTimePath<java.sql.Timestamp> dCreation = createDateTime("dCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dModification = createDateTime("dModification", java.sql.Timestamp.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> idAdmNatureRec = createNumber("idAdmNatureRec", Long.class);

    public final NumberPath<Long> idAdmOrigineRecette = createNumber("idAdmOrigineRecette", Long.class);

    public final NumberPath<Long> idOpeTrancheBud = createNumber("idOpeTrancheBud", Long.class);

    public final NumberPath<Long> idOpeTrancheBudRec = createNumber("idOpeTrancheBudRec", Long.class);

    public final NumberPath<java.math.BigDecimal> montantRec = createNumber("montantRec", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> montantTitre = createNumber("montantTitre", java.math.BigDecimal.class);

    public final NumberPath<Long> persIdCreation = createNumber("persIdCreation", Long.class);

    public final NumberPath<Long> persIdModification = createNumber("persIdModification", Long.class);

    public final StringPath section = createString("section");

    public final StringPath tbrSuppr = createString("tbrSuppr");

    public final com.mysema.query.sql.PrimaryKey<QOpeTrancheBudRec> opeTrancheBudRecPk = createPrimaryKey(idOpeTrancheBudRec);

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBud> opeTrancheBudRecTraFk = createForeignKey(idOpeTrancheBud, "ID_OPE_TRANCHE_BUD");

    public final com.mysema.query.sql.ForeignKey<QAdmNatureRec> opeTrancheBudRecNatFk = createForeignKey(idAdmNatureRec, "ID_ADM_NATURE_REC");

    public final com.mysema.query.sql.ForeignKey<QAdmOrigineRecette> opeTrancheBudRecOrigFk = createForeignKey(idAdmOrigineRecette, "ID_ADM_ORIGINE_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QAdmEb> opeTrancheBudRecEbFk = createForeignKey(idAdmEb, "ID_ADM_EB");

    public QOpeTrancheBudRec(String variable) {
        super(QOpeTrancheBudRec.class, forVariable(variable), "GFC", "OPE_TRANCHE_BUD_REC");
        addMetadata();
    }

    public QOpeTrancheBudRec(String variable, String schema, String table) {
        super(QOpeTrancheBudRec.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOpeTrancheBudRec(Path<? extends QOpeTrancheBudRec> path) {
        super(path.getType(), path.getMetadata(), "GFC", "OPE_TRANCHE_BUD_REC");
        addMetadata();
    }

    public QOpeTrancheBudRec(PathMetadata<?> metadata) {
        super(QOpeTrancheBudRec.class, metadata, "GFC", "OPE_TRANCHE_BUD_REC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(commentaire, ColumnMetadata.named("COMMENTAIRE").withIndex(8).ofType(Types.VARCHAR).withSize(4000));
        addMetadata(dCreation, ColumnMetadata.named("D_CREATION").withIndex(10).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dModification, ColumnMetadata.named("D_MODIFICATION").withIndex(12).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idAdmNatureRec, ColumnMetadata.named("ID_ADM_NATURE_REC").withIndex(6).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idAdmOrigineRecette, ColumnMetadata.named("ID_ADM_ORIGINE_RECETTE").withIndex(7).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idOpeTrancheBud, ColumnMetadata.named("ID_OPE_TRANCHE_BUD").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idOpeTrancheBudRec, ColumnMetadata.named("ID_OPE_TRANCHE_BUD_REC").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(montantRec, ColumnMetadata.named("MONTANT_REC").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(montantTitre, ColumnMetadata.named("MONTANT_TITRE").withIndex(13).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(persIdCreation, ColumnMetadata.named("PERS_ID_CREATION").withIndex(9).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(persIdModification, ColumnMetadata.named("PERS_ID_MODIFICATION").withIndex(11).ofType(Types.DECIMAL).withSize(38));
        addMetadata(section, ColumnMetadata.named("SECTION").withIndex(5).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(tbrSuppr, ColumnMetadata.named("TBR_SUPPR").withIndex(14).ofType(Types.VARCHAR).withSize(1));
    }

}

