package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepVInventaire is a Querydsl query type for QDepVInventaire
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepVInventaire extends com.mysema.query.sql.RelationalPathBase<QDepVInventaire> {

    private static final long serialVersionUID = -454081667;

    public static final QDepVInventaire depVInventaire = new QDepVInventaire("DEP_V_INVENTAIRE");

    public final StringPath clicNumComplet = createString("clicNumComplet");

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final NumberPath<Long> dpcoId = createNumber("dpcoId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final NumberPath<java.math.BigDecimal> lidMontant = createNumber("lidMontant", java.math.BigDecimal.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public QDepVInventaire(String variable) {
        super(QDepVInventaire.class, forVariable(variable), "GFC", "DEP_V_INVENTAIRE");
        addMetadata();
    }

    public QDepVInventaire(String variable, String schema, String table) {
        super(QDepVInventaire.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepVInventaire(Path<? extends QDepVInventaire> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEP_V_INVENTAIRE");
        addMetadata();
    }

    public QDepVInventaire(PathMetadata<?> metadata) {
        super(QDepVInventaire.class, metadata, "GFC", "DEP_V_INVENTAIRE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clicNumComplet, ColumnMetadata.named("CLIC_NUM_COMPLET").withIndex(2).ofType(Types.VARCHAR).withSize(200));
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dpcoId, ColumnMetadata.named("DPCO_ID").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(9).ofType(Types.DECIMAL).withSize(4));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lidMontant, ColumnMetadata.named("LID_MONTANT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(6).ofType(Types.VARCHAR).withSize(20));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0));
    }

}

