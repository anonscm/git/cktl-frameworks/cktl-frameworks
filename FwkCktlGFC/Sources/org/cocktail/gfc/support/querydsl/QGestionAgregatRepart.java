package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import java.util.*;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QGestionAgregatRepart is a Querydsl query type for QGestionAgregatRepart
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QGestionAgregatRepart extends com.mysema.query.sql.RelationalPathBase<QGestionAgregatRepart> {

    private static final long serialVersionUID = -988965761;

    public static final QGestionAgregatRepart gestionAgregatRepart = new QGestionAgregatRepart("GESTION_AGREGAT_REPART");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> gaId = createNumber("gaId", Long.class);

    public final NumberPath<Long> garId = createNumber("garId", Long.class);

    public final StringPath gesCode = createString("gesCode");

    public final com.mysema.query.sql.PrimaryKey<QGestionAgregatRepart> gestionAgregatRepartPk = createPrimaryKey(garId);

    public final com.mysema.query.sql.ForeignKey<QGestionAgregat> garGaIdFk = createForeignKey(gaId, "GA_ID");

    public final com.mysema.query.sql.ForeignKey<QGestionExercice> garGesCodeFk = createForeignKey(Arrays.asList(exeOrdre, gesCode), Arrays.asList("EXE_ORDRE", "GES_CODE"));

    public QGestionAgregatRepart(String variable) {
        super(QGestionAgregatRepart.class, forVariable(variable), "GFC", "GESTION_AGREGAT_REPART");
        addMetadata();
    }

    public QGestionAgregatRepart(String variable, String schema, String table) {
        super(QGestionAgregatRepart.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QGestionAgregatRepart(Path<? extends QGestionAgregatRepart> path) {
        super(path.getType(), path.getMetadata(), "GFC", "GESTION_AGREGAT_REPART");
        addMetadata();
    }

    public QGestionAgregatRepart(PathMetadata<?> metadata) {
        super(QGestionAgregatRepart.class, metadata, "GFC", "GESTION_AGREGAT_REPART");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(gaId, ColumnMetadata.named("GA_ID").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(garId, ColumnMetadata.named("GAR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(4).ofType(Types.VARCHAR).withSize(10).notNull());
    }

}

