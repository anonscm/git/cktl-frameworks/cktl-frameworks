package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRecetteInfo is a Querydsl query type for QRecetteInfo
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRecetteInfo extends com.mysema.query.sql.RelationalPathBase<QRecetteInfo> {

    private static final long serialVersionUID = 1752731933;

    public static final QRecetteInfo recetteInfo = new QRecetteInfo("RECETTE_INFO");

    public final StringPath recEtatRelance = createString("recEtatRelance");

    public final NumberPath<Long> recId = createNumber("recId", Long.class);

    public final StringPath recSuivi = createString("recSuivi");

    public final com.mysema.query.sql.PrimaryKey<QRecetteInfo> recetteInfoPk = createPrimaryKey(recId);

    public final com.mysema.query.sql.ForeignKey<QCptRecette> recetteInfoRecIdFk = createForeignKey(recId, "REC_ID");

    public QRecetteInfo(String variable) {
        super(QRecetteInfo.class, forVariable(variable), "GFC", "RECETTE_INFO");
        addMetadata();
    }

    public QRecetteInfo(String variable, String schema, String table) {
        super(QRecetteInfo.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRecetteInfo(Path<? extends QRecetteInfo> path) {
        super(path.getType(), path.getMetadata(), "GFC", "RECETTE_INFO");
        addMetadata();
    }

    public QRecetteInfo(PathMetadata<?> metadata) {
        super(QRecetteInfo.class, metadata, "GFC", "RECETTE_INFO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(recEtatRelance, ColumnMetadata.named("REC_ETAT_RELANCE").withIndex(3).ofType(Types.VARCHAR).withSize(30));
        addMetadata(recId, ColumnMetadata.named("REC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(recSuivi, ColumnMetadata.named("REC_SUIVI").withIndex(2).ofType(Types.VARCHAR).withSize(1000));
    }

}

