package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAbricotBordSelection is a Querydsl query type for QAbricotBordSelection
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAbricotBordSelection extends com.mysema.query.sql.RelationalPathBase<QAbricotBordSelection> {

    private static final long serialVersionUID = 1866417406;

    public static final QAbricotBordSelection abricotBordSelection = new QAbricotBordSelection("ABRICOT_BORD_SELECTION");

    public final StringPath abrEtat = createString("abrEtat");

    public final StringPath abrGroupBy = createString("abrGroupBy");

    public final NumberPath<Long> abrId = createNumber("abrId", Long.class);

    public final NumberPath<Long> borId = createNumber("borId", Long.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> recId = createNumber("recId", Long.class);

    public final NumberPath<Long> tboOrdre = createNumber("tboOrdre", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> abricotBordSelectionExe_fk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QTypeBordereau> abricotBordSelectionTbo_fk = createForeignKey(tboOrdre, "TBO_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QGestion> abricotBordSelectionGes_fk = createForeignKey(gesCode, "GES_CODE");

    public QAbricotBordSelection(String variable) {
        super(QAbricotBordSelection.class, forVariable(variable), "GFC", "ABRICOT_BORD_SELECTION");
        addMetadata();
    }

    public QAbricotBordSelection(String variable, String schema, String table) {
        super(QAbricotBordSelection.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAbricotBordSelection(Path<? extends QAbricotBordSelection> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ABRICOT_BORD_SELECTION");
        addMetadata();
    }

    public QAbricotBordSelection(PathMetadata<?> metadata) {
        super(QAbricotBordSelection.class, metadata, "GFC", "ABRICOT_BORD_SELECTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(abrEtat, ColumnMetadata.named("ABR_ETAT").withIndex(6).ofType(Types.VARCHAR).withSize(25));
        addMetadata(abrGroupBy, ColumnMetadata.named("ABR_GROUP_BY").withIndex(7).ofType(Types.VARCHAR).withSize(200));
        addMetadata(abrId, ColumnMetadata.named("ABR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(borId, ColumnMetadata.named("BOR_ID").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(8).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(recId, ColumnMetadata.named("REC_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

