package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmAmortissementOrigine is a Querydsl query type for QImmAmortissementOrigine
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmAmortissementOrigine extends com.mysema.query.sql.RelationalPathBase<QImmAmortissementOrigine> {

    private static final long serialVersionUID = -591971928;

    public static final QImmAmortissementOrigine immAmortissementOrigine = new QImmAmortissementOrigine("IMM_AMORTISSEMENT_ORIGINE");

    public final NumberPath<java.math.BigDecimal> amooAnnuite = createNumber("amooAnnuite", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> amooCumul = createNumber("amooCumul", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> amooMontant = createNumber("amooMontant", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> amooResiduel = createNumber("amooResiduel", java.math.BigDecimal.class);

    public final DateTimePath<java.sql.Timestamp> dCalcul = createDateTime("dCalcul", java.sql.Timestamp.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> icorId = createNumber("icorId", Long.class);

    public final NumberPath<Long> idAmortissementOrigine = createNumber("idAmortissementOrigine", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmAmortissementOrigine> immAmortissementOriginePk = createPrimaryKey(idAmortissementOrigine);

    public final com.mysema.query.sql.ForeignKey<QImmInventComptOrig> amooInvComptableOrigFk = createForeignKey(icorId, "ICOR_ID");

    public QImmAmortissementOrigine(String variable) {
        super(QImmAmortissementOrigine.class, forVariable(variable), "GFC", "IMM_AMORTISSEMENT_ORIGINE");
        addMetadata();
    }

    public QImmAmortissementOrigine(String variable, String schema, String table) {
        super(QImmAmortissementOrigine.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmAmortissementOrigine(Path<? extends QImmAmortissementOrigine> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_AMORTISSEMENT_ORIGINE");
        addMetadata();
    }

    public QImmAmortissementOrigine(PathMetadata<?> metadata) {
        super(QImmAmortissementOrigine.class, metadata, "GFC", "IMM_AMORTISSEMENT_ORIGINE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(amooAnnuite, ColumnMetadata.named("AMOO_ANNUITE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(amooCumul, ColumnMetadata.named("AMOO_CUMUL").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(amooMontant, ColumnMetadata.named("AMOO_MONTANT").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(amooResiduel, ColumnMetadata.named("AMOO_RESIDUEL").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dCalcul, ColumnMetadata.named("D_CALCUL").withIndex(8).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(icorId, ColumnMetadata.named("ICOR_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idAmortissementOrigine, ColumnMetadata.named("ID_AMORTISSEMENT_ORIGINE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

