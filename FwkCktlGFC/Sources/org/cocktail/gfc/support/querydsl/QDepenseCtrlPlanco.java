package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepenseCtrlPlanco is a Querydsl query type for QDepenseCtrlPlanco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepenseCtrlPlanco extends com.mysema.query.sql.RelationalPathBase<QDepenseCtrlPlanco> {

    private static final long serialVersionUID = -1433160279;

    public static final QDepenseCtrlPlanco depenseCtrlPlanco = new QDepenseCtrlPlanco("DEPENSE_CTRL_PLANCO");

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<java.math.BigDecimal> dpcoHtSaisie = createNumber("dpcoHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dpcoId = createNumber("dpcoId", Long.class);

    public final NumberPath<java.math.BigDecimal> dpcoMontantBudgetaire = createNumber("dpcoMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dpcoTtcSaisie = createNumber("dpcoTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dpcoTvaSaisie = createNumber("dpcoTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> ecdOrdre = createNumber("ecdOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> manId = createNumber("manId", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tboOrdre = createNumber("tboOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QDepenseCtrlPlanco> depenseCtrlPlancoPk = createPrimaryKey(dpcoId);

    public final com.mysema.query.sql.ForeignKey<QMandat> depenseCtrlPlanManIdFk = createForeignKey(manId, "MAN_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> depenseCtrlPlanDepIdFk = createForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> depenseCtrlPlanExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QTypeBordereau> depenseCtrlPlanTboOrdreFk = createForeignKey(tboOrdre, "TBO_ORDRE");

    public QDepenseCtrlPlanco(String variable) {
        super(QDepenseCtrlPlanco.class, forVariable(variable), "GFC", "DEPENSE_CTRL_PLANCO");
        addMetadata();
    }

    public QDepenseCtrlPlanco(String variable, String schema, String table) {
        super(QDepenseCtrlPlanco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepenseCtrlPlanco(Path<? extends QDepenseCtrlPlanco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEPENSE_CTRL_PLANCO");
        addMetadata();
    }

    public QDepenseCtrlPlanco(PathMetadata<?> metadata) {
        super(QDepenseCtrlPlanco.class, metadata, "GFC", "DEPENSE_CTRL_PLANCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dpcoHtSaisie, ColumnMetadata.named("DPCO_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dpcoId, ColumnMetadata.named("DPCO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dpcoMontantBudgetaire, ColumnMetadata.named("DPCO_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dpcoTtcSaisie, ColumnMetadata.named("DPCO_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dpcoTvaSaisie, ColumnMetadata.named("DPCO_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(ecdOrdre, ColumnMetadata.named("ECD_ORDRE").withIndex(11).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(manId, ColumnMetadata.named("MAN_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

