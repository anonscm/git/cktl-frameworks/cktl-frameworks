package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOrigine is a Querydsl query type for QOrigine
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOrigine extends com.mysema.query.sql.RelationalPathBase<QOrigine> {

    private static final long serialVersionUID = 1605751262;

    public static final QOrigine origine = new QOrigine("ORIGINE");

    public final StringPath oriEntite = createString("oriEntite");

    public final NumberPath<Long> oriKeyEntite = createNumber("oriKeyEntite", Long.class);

    public final StringPath oriKeyName = createString("oriKeyName");

    public final StringPath oriLibelle = createString("oriLibelle");

    public final NumberPath<Long> oriOrdre = createNumber("oriOrdre", Long.class);

    public final NumberPath<Long> topOrdre = createNumber("topOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QOrigine> originePk = createPrimaryKey(oriOrdre);

    public final com.mysema.query.sql.ForeignKey<QBrouillard> _brouillardOriOrdreFk = createInvForeignKey(oriOrdre, "ORI_ORDRE");

    public QOrigine(String variable) {
        super(QOrigine.class, forVariable(variable), "GFC", "ORIGINE");
        addMetadata();
    }

    public QOrigine(String variable, String schema, String table) {
        super(QOrigine.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOrigine(Path<? extends QOrigine> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ORIGINE");
        addMetadata();
    }

    public QOrigine(PathMetadata<?> metadata) {
        super(QOrigine.class, metadata, "GFC", "ORIGINE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(oriEntite, ColumnMetadata.named("ORI_ENTITE").withIndex(2).ofType(Types.VARCHAR).withSize(50));
        addMetadata(oriKeyEntite, ColumnMetadata.named("ORI_KEY_ENTITE").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(oriKeyName, ColumnMetadata.named("ORI_KEY_NAME").withIndex(3).ofType(Types.VARCHAR).withSize(50));
        addMetadata(oriLibelle, ColumnMetadata.named("ORI_LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(oriOrdre, ColumnMetadata.named("ORI_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(topOrdre, ColumnMetadata.named("TOP_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0));
    }

}

