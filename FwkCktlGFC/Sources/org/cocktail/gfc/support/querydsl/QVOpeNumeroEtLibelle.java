package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVOpeNumeroEtLibelle is a Querydsl query type for QVOpeNumeroEtLibelle
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVOpeNumeroEtLibelle extends com.mysema.query.sql.RelationalPathBase<QVOpeNumeroEtLibelle> {

    private static final long serialVersionUID = 1455141163;

    public static final QVOpeNumeroEtLibelle vOpeNumeroEtLibelle = new QVOpeNumeroEtLibelle("V_OPE_NUMERO_ET_LIBELLE");

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public final StringPath llOperation = createString("llOperation");

    public final StringPath opeNumero = createString("opeNumero");

    public QVOpeNumeroEtLibelle(String variable) {
        super(QVOpeNumeroEtLibelle.class, forVariable(variable), "GFC", "V_OPE_NUMERO_ET_LIBELLE");
        addMetadata();
    }

    public QVOpeNumeroEtLibelle(String variable, String schema, String table) {
        super(QVOpeNumeroEtLibelle.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVOpeNumeroEtLibelle(Path<? extends QVOpeNumeroEtLibelle> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_OPE_NUMERO_ET_LIBELLE");
        addMetadata();
    }

    public QVOpeNumeroEtLibelle(PathMetadata<?> metadata) {
        super(QVOpeNumeroEtLibelle.class, metadata, "GFC", "V_OPE_NUMERO_ET_LIBELLE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(llOperation, ColumnMetadata.named("LL_OPERATION").withIndex(3).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(opeNumero, ColumnMetadata.named("OPE_NUMERO").withIndex(2).ofType(Types.VARCHAR).withSize(57));
    }

}

