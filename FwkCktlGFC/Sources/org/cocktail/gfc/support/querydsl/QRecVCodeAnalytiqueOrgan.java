package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRecVCodeAnalytiqueOrgan is a Querydsl query type for QRecVCodeAnalytiqueOrgan
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRecVCodeAnalytiqueOrgan extends com.mysema.query.sql.RelationalPathBase<QRecVCodeAnalytiqueOrgan> {

    private static final long serialVersionUID = 1433587442;

    public static final QRecVCodeAnalytiqueOrgan recVCodeAnalytiqueOrgan = new QRecVCodeAnalytiqueOrgan("REC_V_CODE_ANALYTIQUE_ORGAN");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public QRecVCodeAnalytiqueOrgan(String variable) {
        super(QRecVCodeAnalytiqueOrgan.class, forVariable(variable), "GFC", "REC_V_CODE_ANALYTIQUE_ORGAN");
        addMetadata();
    }

    public QRecVCodeAnalytiqueOrgan(String variable, String schema, String table) {
        super(QRecVCodeAnalytiqueOrgan.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRecVCodeAnalytiqueOrgan(Path<? extends QRecVCodeAnalytiqueOrgan> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REC_V_CODE_ANALYTIQUE_ORGAN");
        addMetadata();
    }

    public QRecVCodeAnalytiqueOrgan(PathMetadata<?> metadata) {
        super(QRecVCodeAnalytiqueOrgan.class, metadata, "GFC", "REC_V_CODE_ANALYTIQUE_ORGAN");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(2).ofType(Types.DECIMAL).withSize(38));
    }

}

