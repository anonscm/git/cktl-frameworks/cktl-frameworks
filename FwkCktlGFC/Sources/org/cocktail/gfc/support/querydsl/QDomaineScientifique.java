package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDomaineScientifique is a Querydsl query type for QDomaineScientifique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDomaineScientifique extends com.mysema.query.sql.RelationalPathBase<QDomaineScientifique> {

    private static final long serialVersionUID = 865147175;

    public static final QDomaineScientifique domaineScientifique = new QDomaineScientifique("DOMAINE_SCIENTIFIQUE");

    public final NumberPath<Long> dsAncienOrdre = createNumber("dsAncienOrdre", Long.class);

    public final NumberPath<Long> dsCode = createNumber("dsCode", Long.class);

    public final StringPath dsLibelle = createString("dsLibelle");

    public final StringPath dsNouveauCode = createString("dsNouveauCode");

    public final NumberPath<Long> dsOrdre = createNumber("dsOrdre", Long.class);

    public final NumberPath<Long> dsParentOrdre = createNumber("dsParentOrdre", Long.class);

    public final NumberPath<Long> dsVersion = createNumber("dsVersion", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QDomaineScientifique> sysC0021832 = createPrimaryKey(dsOrdre);

    public QDomaineScientifique(String variable) {
        super(QDomaineScientifique.class, forVariable(variable), "GRHUM", "DOMAINE_SCIENTIFIQUE");
        addMetadata();
    }

    public QDomaineScientifique(String variable, String schema, String table) {
        super(QDomaineScientifique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDomaineScientifique(Path<? extends QDomaineScientifique> path) {
        super(path.getType(), path.getMetadata(), "GRHUM", "DOMAINE_SCIENTIFIQUE");
        addMetadata();
    }

    public QDomaineScientifique(PathMetadata<?> metadata) {
        super(QDomaineScientifique.class, metadata, "GRHUM", "DOMAINE_SCIENTIFIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dsAncienOrdre, ColumnMetadata.named("DS_ANCIEN_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dsCode, ColumnMetadata.named("DS_CODE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dsLibelle, ColumnMetadata.named("DS_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(100));
        addMetadata(dsNouveauCode, ColumnMetadata.named("DS_NOUVEAU_CODE").withIndex(6).ofType(Types.VARCHAR).withSize(20));
        addMetadata(dsOrdre, ColumnMetadata.named("DS_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dsParentOrdre, ColumnMetadata.named("DS_PARENT_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dsVersion, ColumnMetadata.named("DS_VERSION").withIndex(7).ofType(Types.DECIMAL).withSize(0));
    }

}

