package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVReimputationNewMarche is a Querydsl query type for QVReimputationNewMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVReimputationNewMarche extends com.mysema.query.sql.RelationalPathBase<QVReimputationNewMarche> {

    private static final long serialVersionUID = -1922216112;

    public static final QVReimputationNewMarche vReimputationNewMarche = new QVReimputationNewMarche("V_REIMPUTATION_NEW_MARCHE");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final NumberPath<java.math.BigDecimal> remaHtSaisie = createNumber("remaHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> remaId = createNumber("remaId", Long.class);

    public final NumberPath<java.math.BigDecimal> remaMontantBudgetaire = createNumber("remaMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> remaTtcSaisie = createNumber("remaTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> remaTvaSaisie = createNumber("remaTvaSaisie", java.math.BigDecimal.class);

    public QVReimputationNewMarche(String variable) {
        super(QVReimputationNewMarche.class, forVariable(variable), "GFC", "V_REIMPUTATION_NEW_MARCHE");
        addMetadata();
    }

    public QVReimputationNewMarche(String variable, String schema, String table) {
        super(QVReimputationNewMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVReimputationNewMarche(Path<? extends QVReimputationNewMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_REIMPUTATION_NEW_MARCHE");
        addMetadata();
    }

    public QVReimputationNewMarche(PathMetadata<?> metadata) {
        super(QVReimputationNewMarche.class, metadata, "GFC", "V_REIMPUTATION_NEW_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(remaHtSaisie, ColumnMetadata.named("REMA_HT_SAISIE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(remaId, ColumnMetadata.named("REMA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(remaMontantBudgetaire, ColumnMetadata.named("REMA_MONTANT_BUDGETAIRE").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(remaTtcSaisie, ColumnMetadata.named("REMA_TTC_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(remaTvaSaisie, ColumnMetadata.named("REMA_TVA_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
    }

}

