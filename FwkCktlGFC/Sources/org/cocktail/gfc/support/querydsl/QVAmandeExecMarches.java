package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAmandeExecMarches is a Querydsl query type for QVAmandeExecMarches
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAmandeExecMarches extends com.mysema.query.sql.RelationalPathBase<QVAmandeExecMarches> {

    private static final long serialVersionUID = 946773329;

    public static final QVAmandeExecMarches vAmandeExecMarches = new QVAmandeExecMarches("V_AMANDE_EXEC_MARCHES");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> engHt = createNumber("engHt", Long.class);

    public final NumberPath<Long> executionAnt = createNumber("executionAnt", Long.class);

    public final NumberPath<Integer> exerciceExec = createNumber("exerciceExec", Integer.class);

    public final NumberPath<Integer> exerciceMarches = createNumber("exerciceMarches", Integer.class);

    public final NumberPath<Long> liqHt = createNumber("liqHt", Long.class);

    public final StringPath lotIndex = createString("lotIndex");

    public final StringPath lotLibelle = createString("lotLibelle");

    public final NumberPath<Long> lotOrdre = createNumber("lotOrdre", Long.class);

    public final StringPath marIndex = createString("marIndex");

    public final StringPath marLibelle = createString("marLibelle");

    public final NumberPath<Long> marOrdre = createNumber("marOrdre", Long.class);

    public final StringPath numMarche = createString("numMarche");

    public final NumberPath<Long> seuilAttribution = createNumber("seuilAttribution", Long.class);

    public final NumberPath<Long> seuilReste = createNumber("seuilReste", Long.class);

    public final NumberPath<Long> totalExec = createNumber("totalExec", Long.class);

    public QVAmandeExecMarches(String variable) {
        super(QVAmandeExecMarches.class, forVariable(variable), "GFC", "V_AMANDE_EXEC_MARCHES");
        addMetadata();
    }

    public QVAmandeExecMarches(String variable, String schema, String table) {
        super(QVAmandeExecMarches.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAmandeExecMarches(Path<? extends QVAmandeExecMarches> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_AMANDE_EXEC_MARCHES");
        addMetadata();
    }

    public QVAmandeExecMarches(PathMetadata<?> metadata) {
        super(QVAmandeExecMarches.class, metadata, "GFC", "V_AMANDE_EXEC_MARCHES");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(13).ofType(Types.DECIMAL).withSize(0));
        addMetadata(engHt, ColumnMetadata.named("ENG_HT").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(executionAnt, ColumnMetadata.named("EXECUTION_ANT").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exerciceExec, ColumnMetadata.named("EXERCICE_EXEC").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(exerciceMarches, ColumnMetadata.named("EXERCICE_MARCHES").withIndex(11).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(liqHt, ColumnMetadata.named("LIQ_HT").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lotIndex, ColumnMetadata.named("LOT_INDEX").withIndex(8).ofType(Types.VARCHAR).withSize(5));
        addMetadata(lotLibelle, ColumnMetadata.named("LOT_LIBELLE").withIndex(9).ofType(Types.VARCHAR).withSize(512));
        addMetadata(lotOrdre, ColumnMetadata.named("LOT_ORDRE").withIndex(14).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(marIndex, ColumnMetadata.named("MAR_INDEX").withIndex(12).ofType(Types.VARCHAR).withSize(8));
        addMetadata(marLibelle, ColumnMetadata.named("MAR_LIBELLE").withIndex(10).ofType(Types.VARCHAR).withSize(512));
        addMetadata(marOrdre, ColumnMetadata.named("MAR_ORDRE").withIndex(15).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(numMarche, ColumnMetadata.named("NUM_MARCHE").withIndex(16).ofType(Types.VARCHAR).withSize(49));
        addMetadata(seuilAttribution, ColumnMetadata.named("SEUIL_ATTRIBUTION").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(seuilReste, ColumnMetadata.named("SEUIL_RESTE").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(totalExec, ColumnMetadata.named("TOTAL_EXEC").withIndex(4).ofType(Types.DECIMAL).withSize(0));
    }

}

