package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVCodeAnalytiqueSuivi is a Querydsl query type for QVCodeAnalytiqueSuivi
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVCodeAnalytiqueSuivi extends com.mysema.query.sql.RelationalPathBase<QVCodeAnalytiqueSuivi> {

    private static final long serialVersionUID = -1834452979;

    public static final QVCodeAnalytiqueSuivi vCodeAnalytiqueSuivi = new QVCodeAnalytiqueSuivi("V_CODE_ANALYTIQUE_SUIVI");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<Long> montantBudgetaire = createNumber("montantBudgetaire", Long.class);

    public QVCodeAnalytiqueSuivi(String variable) {
        super(QVCodeAnalytiqueSuivi.class, forVariable(variable), "GFC", "V_CODE_ANALYTIQUE_SUIVI");
        addMetadata();
    }

    public QVCodeAnalytiqueSuivi(String variable, String schema, String table) {
        super(QVCodeAnalytiqueSuivi.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVCodeAnalytiqueSuivi(Path<? extends QVCodeAnalytiqueSuivi> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_CODE_ANALYTIQUE_SUIVI");
        addMetadata();
    }

    public QVCodeAnalytiqueSuivi(PathMetadata<?> metadata) {
        super(QVCodeAnalytiqueSuivi.class, metadata, "GFC", "V_CODE_ANALYTIQUE_SUIVI");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(montantBudgetaire, ColumnMetadata.named("MONTANT_BUDGETAIRE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
    }

}

