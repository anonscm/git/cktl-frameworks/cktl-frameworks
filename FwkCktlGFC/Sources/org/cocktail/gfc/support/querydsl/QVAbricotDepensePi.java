package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAbricotDepensePi is a Querydsl query type for QVAbricotDepensePi
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAbricotDepensePi extends com.mysema.query.sql.RelationalPathBase<QVAbricotDepensePi> {

    private static final long serialVersionUID = 2145946632;

    public static final QVAbricotDepensePi vAbricotDepensePi = new QVAbricotDepensePi("V_ABRICOT_DEPENSE_PI");

    public final StringPath adrAdresse1 = createString("adrAdresse1");

    public final StringPath adrAdresse2 = createString("adrAdresse2");

    public final StringPath adrCivilite = createString("adrCivilite");

    public final StringPath adrCp = createString("adrCp");

    public final StringPath adrNom = createString("adrNom");

    public final StringPath adrPrenom = createString("adrPrenom");

    public final StringPath adrVille = createString("adrVille");

    public final StringPath bic = createString("bic");

    public final StringPath cBanque = createString("cBanque");

    public final StringPath cGuichet = createString("cGuichet");

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final StringPath domiciliation = createString("domiciliation");

    public final NumberPath<java.math.BigDecimal> dpcoHtSaisie = createNumber("dpcoHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dpcoId = createNumber("dpcoId", Long.class);

    public final NumberPath<java.math.BigDecimal> dpcoTtcSaisie = createNumber("dpcoTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dpcoTvaSaisie = createNumber("dpcoTvaSaisie", java.math.BigDecimal.class);

    public final DateTimePath<java.sql.Timestamp> dppDateFacture = createDateTime("dppDateFacture", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateReception = createDateTime("dppDateReception", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateSaisie = createDateTime("dppDateSaisie", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateServiceFait = createDateTime("dppDateServiceFait", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> dppHtSaisie = createNumber("dppHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dppId = createNumber("dppId", Long.class);

    public final NumberPath<Long> dppNbPiece = createNumber("dppNbPiece", Long.class);

    public final StringPath dppNumeroFacture = createString("dppNumeroFacture");

    public final NumberPath<java.math.BigDecimal> dppTtcSaisie = createNumber("dppTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dppTvaSaisie = createNumber("dppTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeExercice = createNumber("exeExercice", Integer.class);

    public final StringPath iban = createString("iban");

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final StringPath modCode = createString("modCode");

    public final StringPath modDom = createString("modDom");

    public final StringPath modLibelle = createString("modLibelle");

    public final StringPath noCompte = createString("noCompte");

    public final StringPath orgCr = createString("orgCr");

    public final StringPath orgSouscr = createString("orgSouscr");

    public final StringPath orgUb = createString("orgUb");

    public final StringPath pcoNum = createString("pcoNum");

    public final StringPath persLc = createString("persLc");

    public final StringPath persLibelle = createString("persLibelle");

    public final StringPath persType = createString("persType");

    public final NumberPath<Long> tboOrdre = createNumber("tboOrdre", Long.class);

    public final StringPath tcdCode = createString("tcdCode");

    public final StringPath tcdLibelle = createString("tcdLibelle");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final StringPath utlnomprenom = createString("utlnomprenom");

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public QVAbricotDepensePi(String variable) {
        super(QVAbricotDepensePi.class, forVariable(variable), "GFC", "V_ABRICOT_DEPENSE_PI");
        addMetadata();
    }

    public QVAbricotDepensePi(String variable, String schema, String table) {
        super(QVAbricotDepensePi.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAbricotDepensePi(Path<? extends QVAbricotDepensePi> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ABRICOT_DEPENSE_PI");
        addMetadata();
    }

    public QVAbricotDepensePi(PathMetadata<?> metadata) {
        super(QVAbricotDepensePi.class, metadata, "GFC", "V_ABRICOT_DEPENSE_PI");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(adrAdresse1, ColumnMetadata.named("ADR_ADRESSE1").withIndex(42).ofType(Types.VARCHAR).withSize(100));
        addMetadata(adrAdresse2, ColumnMetadata.named("ADR_ADRESSE2").withIndex(43).ofType(Types.VARCHAR).withSize(300));
        addMetadata(adrCivilite, ColumnMetadata.named("ADR_CIVILITE").withIndex(39).ofType(Types.VARCHAR).withSize(5).notNull());
        addMetadata(adrCp, ColumnMetadata.named("ADR_CP").withIndex(45).ofType(Types.VARCHAR).withSize(10));
        addMetadata(adrNom, ColumnMetadata.named("ADR_NOM").withIndex(40).ofType(Types.VARCHAR).withSize(120).notNull());
        addMetadata(adrPrenom, ColumnMetadata.named("ADR_PRENOM").withIndex(41).ofType(Types.VARCHAR).withSize(40));
        addMetadata(adrVille, ColumnMetadata.named("ADR_VILLE").withIndex(44).ofType(Types.VARCHAR).withSize(60));
        addMetadata(bic, ColumnMetadata.named("BIC").withIndex(5).ofType(Types.VARCHAR).withSize(11));
        addMetadata(cBanque, ColumnMetadata.named("C_BANQUE").withIndex(1).ofType(Types.VARCHAR).withSize(5));
        addMetadata(cGuichet, ColumnMetadata.named("C_GUICHET").withIndex(2).ofType(Types.VARCHAR).withSize(5));
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(33).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(domiciliation, ColumnMetadata.named("DOMICILIATION").withIndex(6).ofType(Types.VARCHAR).withSize(200));
        addMetadata(dpcoHtSaisie, ColumnMetadata.named("DPCO_HT_SAISIE").withIndex(36).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dpcoId, ColumnMetadata.named("DPCO_ID").withIndex(35).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dpcoTtcSaisie, ColumnMetadata.named("DPCO_TTC_SAISIE").withIndex(38).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dpcoTvaSaisie, ColumnMetadata.named("DPCO_TVA_SAISIE").withIndex(37).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppDateFacture, ColumnMetadata.named("DPP_DATE_FACTURE").withIndex(26).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateReception, ColumnMetadata.named("DPP_DATE_RECEPTION").withIndex(28).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateSaisie, ColumnMetadata.named("DPP_DATE_SAISIE").withIndex(27).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateServiceFait, ColumnMetadata.named("DPP_DATE_SERVICE_FAIT").withIndex(29).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dppHtSaisie, ColumnMetadata.named("DPP_HT_SAISIE").withIndex(23).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(22).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dppNbPiece, ColumnMetadata.named("DPP_NB_PIECE").withIndex(30).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dppNumeroFacture, ColumnMetadata.named("DPP_NUMERO_FACTURE").withIndex(21).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(dppTtcSaisie, ColumnMetadata.named("DPP_TTC_SAISIE").withIndex(25).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppTvaSaisie, ColumnMetadata.named("DPP_TVA_SAISIE").withIndex(24).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeExercice, ColumnMetadata.named("EXE_EXERCICE").withIndex(13).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(iban, ColumnMetadata.named("IBAN").withIndex(4).ofType(Types.VARCHAR).withSize(34));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(14).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(modCode, ColumnMetadata.named("MOD_CODE").withIndex(8).ofType(Types.VARCHAR).withSize(3).notNull());
        addMetadata(modDom, ColumnMetadata.named("MOD_DOM").withIndex(9).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(modLibelle, ColumnMetadata.named("MOD_LIBELLE").withIndex(7).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(noCompte, ColumnMetadata.named("NO_COMPTE").withIndex(3).ofType(Types.VARCHAR).withSize(11));
        addMetadata(orgCr, ColumnMetadata.named("ORG_CR").withIndex(16).ofType(Types.VARCHAR).withSize(50));
        addMetadata(orgSouscr, ColumnMetadata.named("ORG_SOUSCR").withIndex(17).ofType(Types.VARCHAR).withSize(50));
        addMetadata(orgUb, ColumnMetadata.named("ORG_UB").withIndex(15).ofType(Types.VARCHAR).withSize(10));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(34).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(persLc, ColumnMetadata.named("PERS_LC").withIndex(12).ofType(Types.VARCHAR).withSize(40));
        addMetadata(persLibelle, ColumnMetadata.named("PERS_LIBELLE").withIndex(11).ofType(Types.VARCHAR).withSize(120).notNull());
        addMetadata(persType, ColumnMetadata.named("PERS_TYPE").withIndex(10).ofType(Types.VARCHAR).withSize(5).notNull());
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(32).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcdCode, ColumnMetadata.named("TCD_CODE").withIndex(19).ofType(Types.VARCHAR).withSize(2).notNull());
        addMetadata(tcdLibelle, ColumnMetadata.named("TCD_LIBELLE").withIndex(20).ofType(Types.VARCHAR).withSize(40).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(18).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlnomprenom, ColumnMetadata.named("UTLNOMPRENOM").withIndex(46).ofType(Types.VARCHAR).withSize(161));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(31).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

