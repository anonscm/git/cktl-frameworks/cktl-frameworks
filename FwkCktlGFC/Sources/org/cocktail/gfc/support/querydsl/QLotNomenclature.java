package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QLotNomenclature is a Querydsl query type for QLotNomenclature
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QLotNomenclature extends com.mysema.query.sql.RelationalPathBase<QLotNomenclature> {

    private static final long serialVersionUID = 2004833863;

    public static final QLotNomenclature lotNomenclature = new QLotNomenclature("LOT_NOMENCLATURE");

    public final NumberPath<Long> cmOrdre = createNumber("cmOrdre", Long.class);

    public final NumberPath<Long> lnId = createNumber("lnId", Long.class);

    public final NumberPath<Long> lotOrdre = createNumber("lotOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QLotNomenclature> lotNomenclaturePk = createPrimaryKey(cmOrdre, lotOrdre);

    public QLotNomenclature(String variable) {
        super(QLotNomenclature.class, forVariable(variable), "GFC", "LOT_NOMENCLATURE");
        addMetadata();
    }

    public QLotNomenclature(String variable, String schema, String table) {
        super(QLotNomenclature.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QLotNomenclature(Path<? extends QLotNomenclature> path) {
        super(path.getType(), path.getMetadata(), "GFC", "LOT_NOMENCLATURE");
        addMetadata();
    }

    public QLotNomenclature(PathMetadata<?> metadata) {
        super(QLotNomenclature.class, metadata, "GFC", "LOT_NOMENCLATURE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cmOrdre, ColumnMetadata.named("CM_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(22).notNull());
        addMetadata(lnId, ColumnMetadata.named("LN_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lotOrdre, ColumnMetadata.named("LOT_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(22).notNull());
    }

}

