package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVDepenseRecetteCtrlPlanco is a Querydsl query type for QVDepenseRecetteCtrlPlanco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVDepenseRecetteCtrlPlanco extends com.mysema.query.sql.RelationalPathBase<QVDepenseRecetteCtrlPlanco> {

    private static final long serialVersionUID = -249194437;

    public static final QVDepenseRecetteCtrlPlanco vDepenseRecetteCtrlPlanco = new QVDepenseRecetteCtrlPlanco("V_DEPENSE_RECETTE_CTRL_PLANCO");

    public final NumberPath<Long> depRecId = createNumber("depRecId", Long.class);

    public final NumberPath<java.math.BigDecimal> dpcoRpcoHtSaisie = createNumber("dpcoRpcoHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dpcoRpcoId = createNumber("dpcoRpcoId", Long.class);

    public final NumberPath<java.math.BigDecimal> dpcoRpcoMontantBudget = createNumber("dpcoRpcoMontantBudget", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dpcoRpcoTtcSaisie = createNumber("dpcoRpcoTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dpcoRpcoTvaSaisie = createNumber("dpcoRpcoTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> ecdOrdre = createNumber("ecdOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> manTitId = createNumber("manTitId", Long.class);

    public final StringPath manTitNatureLib = createString("manTitNatureLib");

    public final StringPath natureCtrl = createString("natureCtrl");

    public final StringPath pcoLibelle = createString("pcoLibelle");

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tboOrdre = createNumber("tboOrdre", Long.class);

    public QVDepenseRecetteCtrlPlanco(String variable) {
        super(QVDepenseRecetteCtrlPlanco.class, forVariable(variable), "GFC", "V_DEPENSE_RECETTE_CTRL_PLANCO");
        addMetadata();
    }

    public QVDepenseRecetteCtrlPlanco(String variable, String schema, String table) {
        super(QVDepenseRecetteCtrlPlanco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVDepenseRecetteCtrlPlanco(Path<? extends QVDepenseRecetteCtrlPlanco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_DEPENSE_RECETTE_CTRL_PLANCO");
        addMetadata();
    }

    public QVDepenseRecetteCtrlPlanco(PathMetadata<?> metadata) {
        super(QVDepenseRecetteCtrlPlanco.class, metadata, "GFC", "V_DEPENSE_RECETTE_CTRL_PLANCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(depRecId, ColumnMetadata.named("DEP_REC_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dpcoRpcoHtSaisie, ColumnMetadata.named("DPCO_RPCO_HT_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(dpcoRpcoId, ColumnMetadata.named("DPCO_RPCO_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dpcoRpcoMontantBudget, ColumnMetadata.named("DPCO_RPCO_MONTANT_BUDGET").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(dpcoRpcoTtcSaisie, ColumnMetadata.named("DPCO_RPCO_TTC_SAISIE").withIndex(12).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(dpcoRpcoTvaSaisie, ColumnMetadata.named("DPCO_RPCO_TVA_SAISIE").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(ecdOrdre, ColumnMetadata.named("ECD_ORDRE").withIndex(14).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4));
        addMetadata(manTitId, ColumnMetadata.named("MAN_TIT_ID").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(manTitNatureLib, ColumnMetadata.named("MAN_TIT_NATURE_LIB").withIndex(7).ofType(Types.VARCHAR).withSize(6));
        addMetadata(natureCtrl, ColumnMetadata.named("NATURE_CTRL").withIndex(1).ofType(Types.CHAR).withSize(7));
        addMetadata(pcoLibelle, ColumnMetadata.named("PCO_LIBELLE").withIndex(6).ofType(Types.VARCHAR).withSize(200));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(5).ofType(Types.VARCHAR).withSize(20));
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(13).ofType(Types.DECIMAL).withSize(0));
    }

}

