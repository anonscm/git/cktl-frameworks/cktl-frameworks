package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEngageCtrlAnalytique is a Querydsl query type for QEngageCtrlAnalytique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEngageCtrlAnalytique extends com.mysema.query.sql.RelationalPathBase<QEngageCtrlAnalytique> {

    private static final long serialVersionUID = 1920298872;

    public static final QEngageCtrlAnalytique engageCtrlAnalytique = new QEngageCtrlAnalytique("ENGAGE_CTRL_ANALYTIQUE");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final DateTimePath<java.sql.Timestamp> eanaDateSaisie = createDateTime("eanaDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> eanaHtSaisie = createNumber("eanaHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> eanaId = createNumber("eanaId", Long.class);

    public final NumberPath<java.math.BigDecimal> eanaMontantBudgetaire = createNumber("eanaMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> eanaMontantBudgetaireReste = createNumber("eanaMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> eanaTtcSaisie = createNumber("eanaTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> eanaTvaSaisie = createNumber("eanaTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QEngageCtrlAnalytique> engageCtrlAnalytiquePk = createPrimaryKey(eanaId);

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> engageCtrlAnalEngIdFk = createForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> engageCtrlAnalExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> engageCtrlAnalCanIdFk = createForeignKey(canId, "CAN_ID");

    public QEngageCtrlAnalytique(String variable) {
        super(QEngageCtrlAnalytique.class, forVariable(variable), "GFC", "ENGAGE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public QEngageCtrlAnalytique(String variable, String schema, String table) {
        super(QEngageCtrlAnalytique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEngageCtrlAnalytique(Path<? extends QEngageCtrlAnalytique> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ENGAGE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public QEngageCtrlAnalytique(PathMetadata<?> metadata) {
        super(QEngageCtrlAnalytique.class, metadata, "GFC", "ENGAGE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(eanaDateSaisie, ColumnMetadata.named("EANA_DATE_SAISIE").withIndex(10).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(eanaHtSaisie, ColumnMetadata.named("EANA_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eanaId, ColumnMetadata.named("EANA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(eanaMontantBudgetaire, ColumnMetadata.named("EANA_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eanaMontantBudgetaireReste, ColumnMetadata.named("EANA_MONTANT_BUDGETAIRE_RESTE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eanaTtcSaisie, ColumnMetadata.named("EANA_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(eanaTvaSaisie, ColumnMetadata.named("EANA_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

