package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBoutiqueCatalogue is a Querydsl query type for QBoutiqueCatalogue
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBoutiqueCatalogue extends com.mysema.query.sql.RelationalPathBase<QBoutiqueCatalogue> {

    private static final long serialVersionUID = 1535219332;

    public static final QBoutiqueCatalogue boutiqueCatalogue = new QBoutiqueCatalogue("BOUTIQUE_CATALOGUE");

    public final NumberPath<Long> boutiqueId = createNumber("boutiqueId", Long.class);

    public final NumberPath<Long> catId = createNumber("catId", Long.class);

    public final StringPath commentaire = createString("commentaire");

    public final DateTimePath<java.sql.Timestamp> dateDebut = createDateTime("dateDebut", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dateFin = createDateTime("dateFin", java.sql.Timestamp.class);

    public final com.mysema.query.sql.PrimaryKey<QBoutiqueCatalogue> boutiqueCataloguePk = createPrimaryKey(boutiqueId, catId);

    public final com.mysema.query.sql.ForeignKey<QBoutique> boutiqueCatalogueBoutiqFk1 = createForeignKey(boutiqueId, "BOUTIQUE_ID");

    public final com.mysema.query.sql.ForeignKey<QCatCatalogue> boutiqueCatalogueCataloFk1 = createForeignKey(catId, "CAT_ID");

    public QBoutiqueCatalogue(String variable) {
        super(QBoutiqueCatalogue.class, forVariable(variable), "GFC", "BOUTIQUE_CATALOGUE");
        addMetadata();
    }

    public QBoutiqueCatalogue(String variable, String schema, String table) {
        super(QBoutiqueCatalogue.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBoutiqueCatalogue(Path<? extends QBoutiqueCatalogue> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BOUTIQUE_CATALOGUE");
        addMetadata();
    }

    public QBoutiqueCatalogue(PathMetadata<?> metadata) {
        super(QBoutiqueCatalogue.class, metadata, "GFC", "BOUTIQUE_CATALOGUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(boutiqueId, ColumnMetadata.named("BOUTIQUE_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(catId, ColumnMetadata.named("CAT_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commentaire, ColumnMetadata.named("COMMENTAIRE").withIndex(5).ofType(Types.VARCHAR).withSize(500));
        addMetadata(dateDebut, ColumnMetadata.named("DATE_DEBUT").withIndex(3).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dateFin, ColumnMetadata.named("DATE_FIN").withIndex(4).ofType(Types.TIMESTAMP).withSize(7));
    }

}

