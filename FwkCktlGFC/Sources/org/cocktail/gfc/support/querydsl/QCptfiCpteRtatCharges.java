package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCptfiCpteRtatCharges is a Querydsl query type for QCptfiCpteRtatCharges
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCptfiCpteRtatCharges extends com.mysema.query.sql.RelationalPathBase<QCptfiCpteRtatCharges> {

    private static final long serialVersionUID = 334587523;

    public static final QCptfiCpteRtatCharges cptfiCpteRtatCharges = new QCptfiCpteRtatCharges("CPTFI_CPTE_RTAT_CHARGES");

    public final NumberPath<Long> crcOrdre = createNumber("crcOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final StringPath groupe1 = createString("groupe1");

    public final StringPath groupe2 = createString("groupe2");

    public final StringPath libelle = createString("libelle");

    public final NumberPath<java.math.BigDecimal> montant = createNumber("montant", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> montantAnt = createNumber("montantAnt", java.math.BigDecimal.class);

    public final com.mysema.query.sql.PrimaryKey<QCptfiCpteRtatCharges> cptfiCpteRtatChargesPk = createPrimaryKey(crcOrdre);

    public QCptfiCpteRtatCharges(String variable) {
        super(QCptfiCpteRtatCharges.class, forVariable(variable), "GFC", "CPTFI_CPTE_RTAT_CHARGES");
        addMetadata();
    }

    public QCptfiCpteRtatCharges(String variable, String schema, String table) {
        super(QCptfiCpteRtatCharges.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCptfiCpteRtatCharges(Path<? extends QCptfiCpteRtatCharges> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CPTFI_CPTE_RTAT_CHARGES");
        addMetadata();
    }

    public QCptfiCpteRtatCharges(PathMetadata<?> metadata) {
        super(QCptfiCpteRtatCharges.class, metadata, "GFC", "CPTFI_CPTE_RTAT_CHARGES");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(crcOrdre, ColumnMetadata.named("CRC_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(8).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(groupe1, ColumnMetadata.named("GROUPE1").withIndex(3).ofType(Types.VARCHAR).withSize(50));
        addMetadata(groupe2, ColumnMetadata.named("GROUPE2").withIndex(4).ofType(Types.VARCHAR).withSize(50));
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(5).ofType(Types.VARCHAR).withSize(100));
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(montantAnt, ColumnMetadata.named("MONTANT_ANT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
    }

}

