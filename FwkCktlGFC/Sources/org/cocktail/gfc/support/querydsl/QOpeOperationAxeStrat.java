package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOpeOperationAxeStrat is a Querydsl query type for QOpeOperationAxeStrat
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOpeOperationAxeStrat extends com.mysema.query.sql.RelationalPathBase<QOpeOperationAxeStrat> {

    private static final long serialVersionUID = 667305594;

    public static final QOpeOperationAxeStrat opeOperationAxeStrat = new QOpeOperationAxeStrat("OPE_OPERATION_AXE_STRAT");

    public final NumberPath<Long> asId = createNumber("asId", Long.class);

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QOpeOperationAxeStrat> opeOperationAxeStratPk = createPrimaryKey(asId, idOpeOperation);

    public QOpeOperationAxeStrat(String variable) {
        super(QOpeOperationAxeStrat.class, forVariable(variable), "GFC", "OPE_OPERATION_AXE_STRAT");
        addMetadata();
    }

    public QOpeOperationAxeStrat(String variable, String schema, String table) {
        super(QOpeOperationAxeStrat.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOpeOperationAxeStrat(Path<? extends QOpeOperationAxeStrat> path) {
        super(path.getType(), path.getMetadata(), "GFC", "OPE_OPERATION_AXE_STRAT");
        addMetadata();
    }

    public QOpeOperationAxeStrat(PathMetadata<?> metadata) {
        super(QOpeOperationAxeStrat.class, metadata, "GFC", "OPE_OPERATION_AXE_STRAT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(asId, ColumnMetadata.named("AS_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

