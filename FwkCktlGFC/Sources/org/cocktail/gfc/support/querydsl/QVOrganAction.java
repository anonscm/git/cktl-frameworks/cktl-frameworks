package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVOrganAction is a Querydsl query type for QVOrganAction
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVOrganAction extends com.mysema.query.sql.RelationalPathBase<QVOrganAction> {

    private static final long serialVersionUID = 1131824338;

    public static final QVOrganAction vOrganAction = new QVOrganAction("V_ORGAN_ACTION");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public final SimplePath<Object> tyacId = createSimple("tyacId", Object.class);

    public QVOrganAction(String variable) {
        super(QVOrganAction.class, forVariable(variable), "GFC", "V_ORGAN_ACTION");
        addMetadata();
    }

    public QVOrganAction(String variable, String schema, String table) {
        super(QVOrganAction.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVOrganAction(Path<? extends QVOrganAction> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ORGAN_ACTION");
        addMetadata();
    }

    public QVOrganAction(PathMetadata<?> metadata) {
        super(QVOrganAction.class, metadata, "GFC", "V_ORGAN_ACTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(tyacId, ColumnMetadata.named("TYAC_ID").withIndex(4).ofType(Types.OTHER).withSize(0));
    }

}

