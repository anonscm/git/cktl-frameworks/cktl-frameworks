package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPiDepRec is a Querydsl query type for QPiDepRec
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPiDepRec extends com.mysema.query.sql.RelationalPathBase<QPiDepRec> {

    private static final long serialVersionUID = -477050533;

    public static final QPiDepRec piDepRec = new QPiDepRec("PI_DEP_REC");

    public final DateTimePath<java.sql.Timestamp> dCreation = createDateTime("dCreation", java.sql.Timestamp.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Long> pdrId = createNumber("pdrId", Long.class);

    public final NumberPath<Long> pefId = createNumber("pefId", Long.class);

    public final NumberPath<Long> recId = createNumber("recId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QPiDepRec> piDepRecPk = createPrimaryKey(pdrId);

    public final com.mysema.query.sql.ForeignKey<QRecRecette> pdrRecIdFk = createForeignKey(recId, "REC_ID");

    public final com.mysema.query.sql.ForeignKey<QPiEngFac> pdrPefIdFk = createForeignKey(pefId, "PEF_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> pdrDepIdFk = createForeignKey(depId, "DEP_ID");

    public QPiDepRec(String variable) {
        super(QPiDepRec.class, forVariable(variable), "GFC", "PI_DEP_REC");
        addMetadata();
    }

    public QPiDepRec(String variable, String schema, String table) {
        super(QPiDepRec.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPiDepRec(Path<? extends QPiDepRec> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PI_DEP_REC");
        addMetadata();
    }

    public QPiDepRec(PathMetadata<?> metadata) {
        super(QPiDepRec.class, metadata, "GFC", "PI_DEP_REC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dCreation, ColumnMetadata.named("D_CREATION").withIndex(5).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdrId, ColumnMetadata.named("PDR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pefId, ColumnMetadata.named("PEF_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(recId, ColumnMetadata.named("REC_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

