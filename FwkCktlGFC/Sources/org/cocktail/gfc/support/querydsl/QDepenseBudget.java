package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepenseBudget is a Querydsl query type for QDepenseBudget
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepenseBudget extends com.mysema.query.sql.RelationalPathBase<QDepenseBudget> {

    private static final long serialVersionUID = -1006670386;

    public static final QDepenseBudget depenseBudget = new QDepenseBudget("DEPENSE_BUDGET");

    public final NumberPath<java.math.BigDecimal> depHtSaisie = createNumber("depHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Long> depIdReversement = createNumber("depIdReversement", Long.class);

    public final NumberPath<java.math.BigDecimal> depMontantBudgetaire = createNumber("depMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> depTtcSaisie = createNumber("depTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> depTvaSaisie = createNumber("depTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dppId = createNumber("dppId", Long.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QDepenseBudget> depenseBudgetPk = createPrimaryKey(depId);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> depenseBudgetExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> depenseBudgetDepIdRevFk = createForeignKey(depIdReversement, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QDepensePapier> depenseBudgetDppIdFk = createForeignKey(dppId, "DPP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> depenseBudgetUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTauxProrata> depenseBudgetTapIdFk = createForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> depenseBudgetEngIdFk = createForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlAction> _depenseCtrlActDepIdFk = createInvForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlAnalytique> _depenseCtrlAnalDepIdFk = createInvForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QXlabFacture> _xlabFactureCommIdFk = createInvForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlHorsMarche> _depenseCtrlHmDepIdFk = createInvForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QPiDepRec> _pdrDepIdFk = createInvForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlMarche> _depenseCtrlMarDepIdFk = createInvForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QReimputation> _reimputationDepIdFk = createInvForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlPlanco> _depenseCtrlPlanDepIdFk = createInvForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QExtourneLiqDef> _extourneLiqDefDepidFk = createInvForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlConvention> _depenseCtrlConDepIdFk = createInvForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> _depenseBudgetDepIdRevFk = createInvForeignKey(depId, "DEP_ID_REVERSEMENT");

    public final com.mysema.query.sql.ForeignKey<QExtourneLiq> _extourneLiqDepidnFk = createInvForeignKey(depId, "DEP_ID_N");

    public final com.mysema.query.sql.ForeignKey<QExtourneLiq> _extourneLiqDepidn1Fk = createInvForeignKey(depId, "DEP_ID_N1");

    public QDepenseBudget(String variable) {
        super(QDepenseBudget.class, forVariable(variable), "GFC", "DEPENSE_BUDGET");
        addMetadata();
    }

    public QDepenseBudget(String variable, String schema, String table) {
        super(QDepenseBudget.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepenseBudget(Path<? extends QDepenseBudget> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEPENSE_BUDGET");
        addMetadata();
    }

    public QDepenseBudget(PathMetadata<?> metadata) {
        super(QDepenseBudget.class, metadata, "GFC", "DEPENSE_BUDGET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(depHtSaisie, ColumnMetadata.named("DEP_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(depIdReversement, ColumnMetadata.named("DEP_ID_REVERSEMENT").withIndex(11).ofType(Types.DECIMAL).withSize(0));
        addMetadata(depMontantBudgetaire, ColumnMetadata.named("DEP_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depTtcSaisie, ColumnMetadata.named("DEP_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(depTvaSaisie, ColumnMetadata.named("DEP_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

