package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmCleInventCompt is a Querydsl query type for QImmCleInventCompt
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmCleInventCompt extends com.mysema.query.sql.RelationalPathBase<QImmCleInventCompt> {

    private static final long serialVersionUID = 991652503;

    public static final QImmCleInventCompt immCleInventCompt = new QImmCleInventCompt("IMM_CLE_INVENT_COMPT");

    public final StringPath clicComp = createString("clicComp");

    public final StringPath clicCr = createString("clicCr");

    public final NumberPath<Long> clicDureeAmort = createNumber("clicDureeAmort", Long.class);

    public final StringPath clicEtat = createString("clicEtat");

    public final NumberPath<Long> clicId = createNumber("clicId", Long.class);

    public final NumberPath<Long> clicNbEtiquette = createNumber("clicNbEtiquette", Long.class);

    public final StringPath clicNumComplet = createString("clicNumComplet");

    public final StringPath clicNumero = createString("clicNumero");

    public final NumberPath<Long> clicProRata = createNumber("clicProRata", Long.class);

    public final StringPath clicTypeAmort = createString("clicTypeAmort");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> pcoaId = createNumber("pcoaId", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final com.mysema.query.sql.PrimaryKey<QImmCleInventCompt> immCleInventComptPk = createPrimaryKey(clicId);

    public final com.mysema.query.sql.ForeignKey<QImmCleInventComptModif> _cleInvComptMClicIdFk = createInvForeignKey(clicId, "CLIC_ID");

    public final com.mysema.query.sql.ForeignKey<QImmInventCompt> _cleinventairecomptableFk = createInvForeignKey(clicId, "CLIC_ID");

    public QImmCleInventCompt(String variable) {
        super(QImmCleInventCompt.class, forVariable(variable), "GFC", "IMM_CLE_INVENT_COMPT");
        addMetadata();
    }

    public QImmCleInventCompt(String variable, String schema, String table) {
        super(QImmCleInventCompt.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmCleInventCompt(Path<? extends QImmCleInventCompt> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_CLE_INVENT_COMPT");
        addMetadata();
    }

    public QImmCleInventCompt(PathMetadata<?> metadata) {
        super(QImmCleInventCompt.class, metadata, "GFC", "IMM_CLE_INVENT_COMPT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clicComp, ColumnMetadata.named("CLIC_COMP").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(clicCr, ColumnMetadata.named("CLIC_CR").withIndex(3).ofType(Types.VARCHAR).withSize(50));
        addMetadata(clicDureeAmort, ColumnMetadata.named("CLIC_DUREE_AMORT").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(clicEtat, ColumnMetadata.named("CLIC_ETAT").withIndex(5).ofType(Types.VARCHAR).withSize(10));
        addMetadata(clicId, ColumnMetadata.named("CLIC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(clicNbEtiquette, ColumnMetadata.named("CLIC_NB_ETIQUETTE").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(clicNumComplet, ColumnMetadata.named("CLIC_NUM_COMPLET").withIndex(13).ofType(Types.VARCHAR).withSize(200));
        addMetadata(clicNumero, ColumnMetadata.named("CLIC_NUMERO").withIndex(7).ofType(Types.VARCHAR).withSize(30));
        addMetadata(clicProRata, ColumnMetadata.named("CLIC_PRO_RATA").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(clicTypeAmort, ColumnMetadata.named("CLIC_TYPE_AMORT").withIndex(9).ofType(Types.VARCHAR).withSize(20));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(4));
        addMetadata(pcoaId, ColumnMetadata.named("PCOA_ID").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(11).ofType(Types.VARCHAR).withSize(20));
    }

}

