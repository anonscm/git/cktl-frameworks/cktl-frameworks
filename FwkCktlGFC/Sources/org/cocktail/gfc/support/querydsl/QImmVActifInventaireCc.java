package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVActifInventaireCc is a Querydsl query type for QImmVActifInventaireCc
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVActifInventaireCc extends com.mysema.query.sql.RelationalPathBase<QImmVActifInventaireCc> {

    private static final long serialVersionUID = -741660618;

    public static final QImmVActifInventaireCc immVActifInventaireCc = new QImmVActifInventaireCc("IMM_V_ACTIF_INVENTAIRE_CC");

    public final NumberPath<Long> amoAcquisition = createNumber("amoAcquisition", Long.class);

    public final NumberPath<Long> amoAnnuite = createNumber("amoAnnuite", Long.class);

    public final NumberPath<Long> amoCumul = createNumber("amoCumul", Long.class);

    public final NumberPath<Long> amoResiduel = createNumber("amoResiduel", Long.class);

    public final NumberPath<Long> exer = createNumber("exer", Long.class);

    public final NumberPath<Long> exercice = createNumber("exercice", Long.class);

    public final StringPath gesCode = createString("gesCode");

    public final StringPath numero = createString("numero");

    public final StringPath pcoNum = createString("pcoNum");

    public QImmVActifInventaireCc(String variable) {
        super(QImmVActifInventaireCc.class, forVariable(variable), "GFC", "IMM_V_ACTIF_INVENTAIRE_CC");
        addMetadata();
    }

    public QImmVActifInventaireCc(String variable, String schema, String table) {
        super(QImmVActifInventaireCc.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVActifInventaireCc(Path<? extends QImmVActifInventaireCc> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_ACTIF_INVENTAIRE_CC");
        addMetadata();
    }

    public QImmVActifInventaireCc(PathMetadata<?> metadata) {
        super(QImmVActifInventaireCc.class, metadata, "GFC", "IMM_V_ACTIF_INVENTAIRE_CC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(amoAcquisition, ColumnMetadata.named("AMO_ACQUISITION").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(amoAnnuite, ColumnMetadata.named("AMO_ANNUITE").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(amoCumul, ColumnMetadata.named("AMO_CUMUL").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(amoResiduel, ColumnMetadata.named("AMO_RESIDUEL").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exer, ColumnMetadata.named("EXER").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exercice, ColumnMetadata.named("EXERCICE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(numero, ColumnMetadata.named("NUMERO").withIndex(3).ofType(Types.VARCHAR).withSize(200));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(9).ofType(Types.VARCHAR).withSize(20));
    }

}

