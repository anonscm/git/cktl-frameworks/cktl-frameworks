package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QComptabilite is a Querydsl query type for QComptabilite
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QComptabilite extends com.mysema.query.sql.RelationalPathBase<QComptabilite> {

    private static final long serialVersionUID = -42192462;

    public static final QComptabilite comptabilite = new QComptabilite("COMPTABILITE");

    public final StringPath comLibelle = createString("comLibelle");

    public final NumberPath<Long> comOrdre = createNumber("comOrdre", Long.class);

    public final StringPath gesCode = createString("gesCode");

    public final com.mysema.query.sql.PrimaryKey<QComptabilite> comptabilitePk = createPrimaryKey(comOrdre);

    public final com.mysema.query.sql.ForeignKey<QGestion> comptabiliteGesCodeFk = createForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QRetenue> _retenueComOrdreFk = createInvForeignKey(comOrdre, "COM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPaiement> _paiementComOrdreFk = createInvForeignKey(comOrdre, "COM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBrouillard> _brouillardComOrdreFk = createInvForeignKey(comOrdre, "COM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QOrdreDePaiement> _ordreDePaiementComOrdreFk = createInvForeignKey(comOrdre, "COM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QNumerotation> _numerotationComOrdreFk = createInvForeignKey(comOrdre, "COM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QGestion> _gestionComOrdreFk = createInvForeignKey(comOrdre, "COM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEmargement> _emargementComOrdreFk = createInvForeignKey(comOrdre, "COM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEcriture> _ecritureComOrdreFk = createInvForeignKey(comOrdre, "COM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRecouvrement> _recouvrementComOrdreFk = createInvForeignKey(comOrdre, "COM_ORDRE");

    public QComptabilite(String variable) {
        super(QComptabilite.class, forVariable(variable), "GFC", "COMPTABILITE");
        addMetadata();
    }

    public QComptabilite(String variable, String schema, String table) {
        super(QComptabilite.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QComptabilite(Path<? extends QComptabilite> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMPTABILITE");
        addMetadata();
    }

    public QComptabilite(PathMetadata<?> metadata) {
        super(QComptabilite.class, metadata, "GFC", "COMPTABILITE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(comLibelle, ColumnMetadata.named("COM_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(50));
        addMetadata(comOrdre, ColumnMetadata.named("COM_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(10).notNull());
    }

}

