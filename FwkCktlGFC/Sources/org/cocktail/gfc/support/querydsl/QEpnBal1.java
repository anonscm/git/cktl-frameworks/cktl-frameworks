package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEpnBal1 is a Querydsl query type for QEpnBal1
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEpnBal1 extends com.mysema.query.sql.RelationalPathBase<QEpnBal1> {

    private static final long serialVersionUID = 1266898278;

    public static final QEpnBal1 epnBal1 = new QEpnBal1("EPN_BAL_1");

    public final StringPath epnb1CodBud = createString("epnb1CodBud");

    public final StringPath epnb1CodNomen = createString("epnb1CodNomen");

    public final StringPath epnb1Date = createString("epnb1Date");

    public final NumberPath<Integer> epnb1Exercice = createNumber("epnb1Exercice", Integer.class);

    public final StringPath epnb1GesCode = createString("epnb1GesCode");

    public final NumberPath<Long> epnb1Identifiant = createNumber("epnb1Identifiant", Long.class);

    public final NumberPath<Integer> epnb1NbEnreg = createNumber("epnb1NbEnreg", Integer.class);

    public final NumberPath<Integer> epnb1Numero = createNumber("epnb1Numero", Integer.class);

    public final StringPath epnb1Rang = createString("epnb1Rang");

    public final NumberPath<Integer> epnb1Siren = createNumber("epnb1Siren", Integer.class);

    public final NumberPath<Long> epnb1Siret = createNumber("epnb1Siret", Long.class);

    public final StringPath epnb1Type = createString("epnb1Type");

    public final StringPath epnb1TypeDoc = createString("epnb1TypeDoc");

    public final StringPath epnbOrdre = createString("epnbOrdre");

    public QEpnBal1(String variable) {
        super(QEpnBal1.class, forVariable(variable), "GFC", "EPN_BAL_1");
        addMetadata();
    }

    public QEpnBal1(String variable, String schema, String table) {
        super(QEpnBal1.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEpnBal1(Path<? extends QEpnBal1> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EPN_BAL_1");
        addMetadata();
    }

    public QEpnBal1(PathMetadata<?> metadata) {
        super(QEpnBal1.class, metadata, "GFC", "EPN_BAL_1");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(epnb1CodBud, ColumnMetadata.named("EPNB1_COD_BUD").withIndex(8).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnb1CodNomen, ColumnMetadata.named("EPNB1_COD_NOMEN").withIndex(7).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnb1Date, ColumnMetadata.named("EPNB1_DATE").withIndex(11).ofType(Types.VARCHAR).withSize(8));
        addMetadata(epnb1Exercice, ColumnMetadata.named("EPNB1_EXERCICE").withIndex(9).ofType(Types.DECIMAL).withSize(4));
        addMetadata(epnb1GesCode, ColumnMetadata.named("EPNB1_GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(epnb1Identifiant, ColumnMetadata.named("EPNB1_IDENTIFIANT").withIndex(5).ofType(Types.DECIMAL).withSize(10));
        addMetadata(epnb1NbEnreg, ColumnMetadata.named("EPNB1_NB_ENREG").withIndex(14).ofType(Types.DECIMAL).withSize(5));
        addMetadata(epnb1Numero, ColumnMetadata.named("EPNB1_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(5));
        addMetadata(epnb1Rang, ColumnMetadata.named("EPNB1_RANG").withIndex(10).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnb1Siren, ColumnMetadata.named("EPNB1_SIREN").withIndex(12).ofType(Types.DECIMAL).withSize(9));
        addMetadata(epnb1Siret, ColumnMetadata.named("EPNB1_SIRET").withIndex(13).ofType(Types.DECIMAL).withSize(14));
        addMetadata(epnb1Type, ColumnMetadata.named("EPNB1_TYPE").withIndex(3).ofType(Types.VARCHAR).withSize(1));
        addMetadata(epnb1TypeDoc, ColumnMetadata.named("EPNB1_TYPE_DOC").withIndex(6).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnbOrdre, ColumnMetadata.named("EPNB_ORDRE").withIndex(1).ofType(Types.VARCHAR).withSize(2));
    }

}

