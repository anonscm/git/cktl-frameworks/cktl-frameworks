package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmPrmEb is a Querydsl query type for QAdmPrmEb
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmPrmEb extends com.mysema.query.sql.RelationalPathBase<QAdmPrmEb> {

    private static final long serialVersionUID = 162532895;

    public static final QAdmPrmEb admPrmEb = new QAdmPrmEb("ADM_PRM_EB");

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> pjId = createNumber("pjId", Long.class);

    public final NumberPath<Long> proId = createNumber("proId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmPrmEb> prmEbPk = createPrimaryKey(proId);

    public final com.mysema.query.sql.ForeignKey<QAdmEb> admPrmEbIdAdmEbFk = createForeignKey(idAdmEb, "ID_ADM_EB");

    public final com.mysema.query.sql.ForeignKey<QAdmPersjur> admPrmEbPjIdFk = createForeignKey(pjId, "PJ_ID");

    public QAdmPrmEb(String variable) {
        super(QAdmPrmEb.class, forVariable(variable), "GFC", "ADM_PRM_EB");
        addMetadata();
    }

    public QAdmPrmEb(String variable, String schema, String table) {
        super(QAdmPrmEb.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmPrmEb(Path<? extends QAdmPrmEb> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_PRM_EB");
        addMetadata();
    }

    public QAdmPrmEb(PathMetadata<?> metadata) {
        super(QAdmPrmEb.class, metadata, "GFC", "ADM_PRM_EB");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pjId, ColumnMetadata.named("PJ_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(proId, ColumnMetadata.named("PRO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

