package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVJefyExecutionDetailEng is a Querydsl query type for QVJefyExecutionDetailEng
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVJefyExecutionDetailEng extends com.mysema.query.sql.RelationalPathBase<QVJefyExecutionDetailEng> {

    private static final long serialVersionUID = -131750792;

    public static final QVJefyExecutionDetailEng vJefyExecutionDetailEng = new QVJefyExecutionDetailEng("V_JEFY_EXECUTION_DETAIL_ENG");

    public final NumberPath<java.math.BigDecimal> aeeEngHt = createNumber("aeeEngHt", java.math.BigDecimal.class);

    public final NumberPath<Long> aeeExecution = createNumber("aeeExecution", Long.class);

    public final NumberPath<Long> aeeLiqHt = createNumber("aeeLiqHt", Long.class);

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final StringPath cdeLib = createString("cdeLib");

    public final NumberPath<Long> cdeOrdre = createNumber("cdeOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> dateEng = createDateTime("dateEng", java.sql.Timestamp.class);

    public final NumberPath<Long> engNumero = createNumber("engNumero", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath orgUb = createString("orgUb");

    public QVJefyExecutionDetailEng(String variable) {
        super(QVJefyExecutionDetailEng.class, forVariable(variable), "GFC", "V_JEFY_EXECUTION_DETAIL_ENG");
        addMetadata();
    }

    public QVJefyExecutionDetailEng(String variable, String schema, String table) {
        super(QVJefyExecutionDetailEng.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVJefyExecutionDetailEng(Path<? extends QVJefyExecutionDetailEng> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_JEFY_EXECUTION_DETAIL_ENG");
        addMetadata();
    }

    public QVJefyExecutionDetailEng(PathMetadata<?> metadata) {
        super(QVJefyExecutionDetailEng.class, metadata, "GFC", "V_JEFY_EXECUTION_DETAIL_ENG");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(aeeEngHt, ColumnMetadata.named("AEE_ENG_HT").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(aeeExecution, ColumnMetadata.named("AEE_EXECUTION").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(aeeLiqHt, ColumnMetadata.named("AEE_LIQ_HT").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(cdeLib, ColumnMetadata.named("CDE_LIB").withIndex(4).ofType(Types.VARCHAR).withSize(500));
        addMetadata(cdeOrdre, ColumnMetadata.named("CDE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dateEng, ColumnMetadata.named("DATE_ENG").withIndex(8).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(engNumero, ColumnMetadata.named("ENG_NUMERO").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4));
        addMetadata(orgUb, ColumnMetadata.named("ORG_UB").withIndex(10).ofType(Types.VARCHAR).withSize(10));
    }

}

