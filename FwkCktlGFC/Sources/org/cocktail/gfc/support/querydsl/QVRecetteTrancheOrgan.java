package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVRecetteTrancheOrgan is a Querydsl query type for QVRecetteTrancheOrgan
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVRecetteTrancheOrgan extends com.mysema.query.sql.RelationalPathBase<QVRecetteTrancheOrgan> {

    private static final long serialVersionUID = 670802871;

    public static final QVRecetteTrancheOrgan vRecetteTrancheOrgan = new QVRecetteTrancheOrgan("V_RECETTE_TRANCHE_ORGAN");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> exeOrdreSb = createSimple("exeOrdreSb", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> pcoNum = createSimple("pcoNum", Object.class);

    public final SimplePath<Object> traOrdre = createSimple("traOrdre", Object.class);

    public QVRecetteTrancheOrgan(String variable) {
        super(QVRecetteTrancheOrgan.class, forVariable(variable), "GFC", "V_RECETTE_TRANCHE_ORGAN");
        addMetadata();
    }

    public QVRecetteTrancheOrgan(String variable, String schema, String table) {
        super(QVRecetteTrancheOrgan.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVRecetteTrancheOrgan(Path<? extends QVRecetteTrancheOrgan> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_RECETTE_TRANCHE_ORGAN");
        addMetadata();
    }

    public QVRecetteTrancheOrgan(PathMetadata<?> metadata) {
        super(QVRecetteTrancheOrgan.class, metadata, "GFC", "V_RECETTE_TRANCHE_ORGAN");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdreSb, ColumnMetadata.named("EXE_ORDRE_SB").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(traOrdre, ColumnMetadata.named("TRA_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
    }

}

