package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEpnRecBud is a Querydsl query type for QEpnRecBud
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEpnRecBud extends com.mysema.query.sql.RelationalPathBase<QEpnRecBud> {

    private static final long serialVersionUID = -1819954461;

    public static final QEpnRecBud epnRecBud = new QEpnRecBud("EPN_REC_BUD");

    public final NumberPath<Long> annulTitRec = createNumber("annulTitRec", Long.class);

    public final DateTimePath<java.sql.Timestamp> epnDate = createDateTime("epnDate", java.sql.Timestamp.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> montRec = createNumber("montRec", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public QEpnRecBud(String variable) {
        super(QEpnRecBud.class, forVariable(variable), "GFC", "EPN_REC_BUD");
        addMetadata();
    }

    public QEpnRecBud(String variable, String schema, String table) {
        super(QEpnRecBud.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEpnRecBud(Path<? extends QEpnRecBud> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EPN_REC_BUD");
        addMetadata();
    }

    public QEpnRecBud(PathMetadata<?> metadata) {
        super(QEpnRecBud.class, metadata, "GFC", "EPN_REC_BUD");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(annulTitRec, ColumnMetadata.named("ANNUL_TIT_REC").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(epnDate, ColumnMetadata.named("EPN_DATE").withIndex(4).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(10));
        addMetadata(montRec, ColumnMetadata.named("MONT_REC").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(2).ofType(Types.VARCHAR).withSize(20));
    }

}

