package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEpnCreBud2 is a Querydsl query type for QEpnCreBud2
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEpnCreBud2 extends com.mysema.query.sql.RelationalPathBase<QEpnCreBud2> {

    private static final long serialVersionUID = -637640715;

    public static final QEpnCreBud2 epnCreBud2 = new QEpnCreBud2("EPN_CRE_BUD_2");

    public final StringPath epnccb2CodBud = createString("epnccb2CodBud");

    public final StringPath epnccb2Compte = createString("epnccb2Compte");

    public final NumberPath<Double> epnccb2Creprevextnoempl = createNumber("epnccb2Creprevextnoempl", Double.class);

    public final NumberPath<Double> epnccb2Creprevnoempl = createNumber("epnccb2Creprevnoempl", Double.class);

    public final NumberPath<Double> epnccb2Creprevob = createNumber("epnccb2Creprevob", Double.class);

    public final NumberPath<Double> epnccb2Creprevouv = createNumber("epnccb2Creprevouv", Double.class);

    public final NumberPath<Integer> epnccb2Exercice = createNumber("epnccb2Exercice", Integer.class);

    public final StringPath epnccb2GesCode = createString("epnccb2GesCode");

    public final NumberPath<Integer> epnccb2Numero = createNumber("epnccb2Numero", Integer.class);

    public final StringPath epnccb2Senscpt = createString("epnccb2Senscpt");

    public final StringPath epnccb2Type = createString("epnccb2Type");

    public final NumberPath<Integer> epnccb2TypeCpt = createNumber("epnccb2TypeCpt", Integer.class);

    public final StringPath epnccb2TypeDoc = createString("epnccb2TypeDoc");

    public final StringPath epnccbOrdre = createString("epnccbOrdre");

    public QEpnCreBud2(String variable) {
        super(QEpnCreBud2.class, forVariable(variable), "GFC", "EPN_CRE_BUD_2");
        addMetadata();
    }

    public QEpnCreBud2(String variable, String schema, String table) {
        super(QEpnCreBud2.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEpnCreBud2(Path<? extends QEpnCreBud2> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EPN_CRE_BUD_2");
        addMetadata();
    }

    public QEpnCreBud2(PathMetadata<?> metadata) {
        super(QEpnCreBud2.class, metadata, "GFC", "EPN_CRE_BUD_2");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(epnccb2CodBud, ColumnMetadata.named("EPNCCB2_COD_BUD").withIndex(13).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnccb2Compte, ColumnMetadata.named("EPNCCB2_COMPTE").withIndex(6).ofType(Types.VARCHAR).withSize(15));
        addMetadata(epnccb2Creprevextnoempl, ColumnMetadata.named("EPNCCB2_CREPREVEXTNOEMPL").withIndex(10).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnccb2Creprevnoempl, ColumnMetadata.named("EPNCCB2_CREPREVNOEMPL").withIndex(9).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnccb2Creprevob, ColumnMetadata.named("EPNCCB2_CREPREVOB").withIndex(8).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnccb2Creprevouv, ColumnMetadata.named("EPNCCB2_CREPREVOUV").withIndex(7).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epnccb2Exercice, ColumnMetadata.named("EPNCCB2_EXERCICE").withIndex(12).ofType(Types.DECIMAL).withSize(4));
        addMetadata(epnccb2GesCode, ColumnMetadata.named("EPNCCB2_GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(epnccb2Numero, ColumnMetadata.named("EPNCCB2_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(5));
        addMetadata(epnccb2Senscpt, ColumnMetadata.named("EPNCCB2_SENSCPT").withIndex(11).ofType(Types.VARCHAR).withSize(1));
        addMetadata(epnccb2Type, ColumnMetadata.named("EPNCCB2_TYPE").withIndex(3).ofType(Types.VARCHAR).withSize(1));
        addMetadata(epnccb2TypeCpt, ColumnMetadata.named("EPNCCB2_TYPE_CPT").withIndex(5).ofType(Types.DECIMAL).withSize(1));
        addMetadata(epnccb2TypeDoc, ColumnMetadata.named("EPNCCB2_TYPE_DOC").withIndex(14).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnccbOrdre, ColumnMetadata.named("EPNCCB_ORDRE").withIndex(1).ofType(Types.VARCHAR).withSize(2));
    }

}

