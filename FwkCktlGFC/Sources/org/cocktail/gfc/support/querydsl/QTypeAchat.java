package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTypeAchat is a Querydsl query type for QTypeAchat
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QTypeAchat extends com.mysema.query.sql.RelationalPathBase<QTypeAchat> {

    private static final long serialVersionUID = 1924173886;

    public static final QTypeAchat typeAchat = new QTypeAchat("TYPE_ACHAT");

    public final NumberPath<Long> typaId = createNumber("typaId", Long.class);

    public final StringPath typaLibelle = createString("typaLibelle");

    public final com.mysema.query.sql.PrimaryKey<QTypeAchat> typeAchatPk = createPrimaryKey(typaId);

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlHorsMarche> _engageCtrlHmTypaIdFk = createInvForeignKey(typaId, "TYPA_ID");

    public final com.mysema.query.sql.ForeignKey<QReimputationHorsMarche> _reimpHmTypaIdFk = createInvForeignKey(typaId, "TYPA_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlHorsMarche> _pdepenseCtrlHmTypaIdFk = createInvForeignKey(typaId, "TYPA_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlHorsMarche> _depenseCtrlHmTypaIdFk = createInvForeignKey(typaId, "TYPA_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlHorsMarche> _commandeCtrlHomTypaIdFk = createInvForeignKey(typaId, "TYPA_ID");

    public final com.mysema.query.sql.ForeignKey<QDepArticle> _depArticleTypaIdFk = createInvForeignKey(typaId, "TYPA_ID");

    public QTypeAchat(String variable) {
        super(QTypeAchat.class, forVariable(variable), "GFC", "TYPE_ACHAT");
        addMetadata();
    }

    public QTypeAchat(String variable, String schema, String table) {
        super(QTypeAchat.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTypeAchat(Path<? extends QTypeAchat> path) {
        super(path.getType(), path.getMetadata(), "GFC", "TYPE_ACHAT");
        addMetadata();
    }

    public QTypeAchat(PathMetadata<?> metadata) {
        super(QTypeAchat.class, metadata, "GFC", "TYPE_ACHAT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(typaId, ColumnMetadata.named("TYPA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(typaLibelle, ColumnMetadata.named("TYPA_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
    }

}

