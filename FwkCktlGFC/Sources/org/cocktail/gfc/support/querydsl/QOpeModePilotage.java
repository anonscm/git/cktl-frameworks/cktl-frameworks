package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOpeModePilotage is a Querydsl query type for QOpeModePilotage
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOpeModePilotage extends com.mysema.query.sql.RelationalPathBase<QOpeModePilotage> {

    private static final long serialVersionUID = -524125267;

    public static final QOpeModePilotage opeModePilotage = new QOpeModePilotage("OPE_MODE_PILOTAGE");

    public final StringPath codeModePilotage = createString("codeModePilotage");

    public final NumberPath<Long> idOpeModePilotage = createNumber("idOpeModePilotage", Long.class);

    public final StringPath llModePilotage = createString("llModePilotage");

    public final com.mysema.query.sql.PrimaryKey<QOpeModePilotage> idOpeModePilotagePk = createPrimaryKey(idOpeModePilotage);

    public final com.mysema.query.sql.ForeignKey<QOpeOperation> _opeModePilotageOpeFk = createInvForeignKey(idOpeModePilotage, "ID_OPE_MODE_PILOTAGE");

    public QOpeModePilotage(String variable) {
        super(QOpeModePilotage.class, forVariable(variable), "GFC", "OPE_MODE_PILOTAGE");
        addMetadata();
    }

    public QOpeModePilotage(String variable, String schema, String table) {
        super(QOpeModePilotage.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOpeModePilotage(Path<? extends QOpeModePilotage> path) {
        super(path.getType(), path.getMetadata(), "GFC", "OPE_MODE_PILOTAGE");
        addMetadata();
    }

    public QOpeModePilotage(PathMetadata<?> metadata) {
        super(QOpeModePilotage.class, metadata, "GFC", "OPE_MODE_PILOTAGE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(codeModePilotage, ColumnMetadata.named("CODE_MODE_PILOTAGE").withIndex(2).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(idOpeModePilotage, ColumnMetadata.named("ID_OPE_MODE_PILOTAGE").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(llModePilotage, ColumnMetadata.named("LL_MODE_PILOTAGE").withIndex(3).ofType(Types.VARCHAR).withSize(100).notNull());
    }

}

