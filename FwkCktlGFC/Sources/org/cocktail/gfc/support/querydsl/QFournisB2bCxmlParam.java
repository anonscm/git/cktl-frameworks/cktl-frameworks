package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QFournisB2bCxmlParam is a Querydsl query type for QFournisB2bCxmlParam
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QFournisB2bCxmlParam extends com.mysema.query.sql.RelationalPathBase<QFournisB2bCxmlParam> {

    private static final long serialVersionUID = -256048232;

    public static final QFournisB2bCxmlParam fournisB2bCxmlParam = new QFournisB2bCxmlParam("FOURNIS_B2B_CXML_PARAM");

    public final DateTimePath<java.sql.Timestamp> dateModification = createDateTime("dateModification", java.sql.Timestamp.class);

    public final StringPath fbcpCommentaires = createString("fbcpCommentaires");

    public final StringPath fbcpDeploymentMode = createString("fbcpDeploymentMode");

    public final NumberPath<Integer> fbcpDureeValidPanier = createNumber("fbcpDureeValidPanier", Integer.class);

    public final NumberPath<Long> fbcpId = createNumber("fbcpId", Long.class);

    public final StringPath fbcpIdentity = createString("fbcpIdentity");

    public final StringPath fbcpName = createString("fbcpName");

    public final StringPath fbcpSharedSecret = createString("fbcpSharedSecret");

    public final StringPath fbcpSupplierCredDomain = createString("fbcpSupplierCredDomain");

    public final StringPath fbcpSupplierCredId = createString("fbcpSupplierCredId");

    public final StringPath fbcpSupplierSetupUrl = createString("fbcpSupplierSetupUrl");

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> persIdModification = createNumber("persIdModification", Long.class);

    public final NumberPath<Long> tvaId = createNumber("tvaId", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QFournisB2bCxmlParam> fournisB2bCxmlParamPk = createPrimaryKey(fbcpId);

    public QFournisB2bCxmlParam(String variable) {
        super(QFournisB2bCxmlParam.class, forVariable(variable), "GRHUM", "FOURNIS_B2B_CXML_PARAM");
        addMetadata();
    }

    public QFournisB2bCxmlParam(String variable, String schema, String table) {
        super(QFournisB2bCxmlParam.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QFournisB2bCxmlParam(Path<? extends QFournisB2bCxmlParam> path) {
        super(path.getType(), path.getMetadata(), "GRHUM", "FOURNIS_B2B_CXML_PARAM");
        addMetadata();
    }

    public QFournisB2bCxmlParam(PathMetadata<?> metadata) {
        super(QFournisB2bCxmlParam.class, metadata, "GRHUM", "FOURNIS_B2B_CXML_PARAM");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dateModification, ColumnMetadata.named("DATE_MODIFICATION").withIndex(15).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(fbcpCommentaires, ColumnMetadata.named("FBCP_COMMENTAIRES").withIndex(10).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(fbcpDeploymentMode, ColumnMetadata.named("FBCP_DEPLOYMENT_MODE").withIndex(9).ofType(Types.VARCHAR).withSize(50));
        addMetadata(fbcpDureeValidPanier, ColumnMetadata.named("FBCP_DUREE_VALID_PANIER").withIndex(11).ofType(Types.DECIMAL).withSize(3).notNull());
        addMetadata(fbcpId, ColumnMetadata.named("FBCP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(fbcpIdentity, ColumnMetadata.named("FBCP_IDENTITY").withIndex(7).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(fbcpName, ColumnMetadata.named("FBCP_NAME").withIndex(3).ofType(Types.VARCHAR).withSize(100).notNull());
        addMetadata(fbcpSharedSecret, ColumnMetadata.named("FBCP_SHARED_SECRET").withIndex(8).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(fbcpSupplierCredDomain, ColumnMetadata.named("FBCP_SUPPLIER_CRED_DOMAIN").withIndex(5).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(fbcpSupplierCredId, ColumnMetadata.named("FBCP_SUPPLIER_CRED_ID").withIndex(6).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(fbcpSupplierSetupUrl, ColumnMetadata.named("FBCP_SUPPLIER_SETUP_URL").withIndex(4).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(persIdModification, ColumnMetadata.named("PERS_ID_MODIFICATION").withIndex(14).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tvaId, ColumnMetadata.named("TVA_ID").withIndex(12).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(13).ofType(Types.DECIMAL).withSize(0));
    }

}

