package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRepartSbDepenseCAnalytique is a Querydsl query type for QRepartSbDepenseCAnalytique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRepartSbDepenseCAnalytique extends com.mysema.query.sql.RelationalPathBase<QRepartSbDepenseCAnalytique> {

    private static final long serialVersionUID = 1567662010;

    public static final QRepartSbDepenseCAnalytique repartSbDepenseCAnalytique = new QRepartSbDepenseCAnalytique("REPART_SB_DEPENSE_C_ANALYTIQUE");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<java.math.BigDecimal> montantHt = createNumber("montantHt", java.math.BigDecimal.class);

    public final NumberPath<Long> sdOrdre = createNumber("sdOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QRepartSbDepenseCAnalytique> repartSbDepenseCAnalytiPk = createPrimaryKey(canId, sdOrdre);

    public QRepartSbDepenseCAnalytique(String variable) {
        super(QRepartSbDepenseCAnalytique.class, forVariable(variable), "GFC", "REPART_SB_DEPENSE_C_ANALYTIQUE");
        addMetadata();
    }

    public QRepartSbDepenseCAnalytique(String variable, String schema, String table) {
        super(QRepartSbDepenseCAnalytique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRepartSbDepenseCAnalytique(Path<? extends QRepartSbDepenseCAnalytique> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REPART_SB_DEPENSE_C_ANALYTIQUE");
        addMetadata();
    }

    public QRepartSbDepenseCAnalytique(PathMetadata<?> metadata) {
        super(QRepartSbDepenseCAnalytique.class, metadata, "GFC", "REPART_SB_DEPENSE_C_ANALYTIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(montantHt, ColumnMetadata.named("MONTANT_HT").withIndex(2).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(sdOrdre, ColumnMetadata.named("SD_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

