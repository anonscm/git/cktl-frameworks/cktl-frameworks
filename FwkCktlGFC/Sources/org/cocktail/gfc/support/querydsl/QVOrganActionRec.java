package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVOrganActionRec is a Querydsl query type for QVOrganActionRec
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVOrganActionRec extends com.mysema.query.sql.RelationalPathBase<QVOrganActionRec> {

    private static final long serialVersionUID = -1609305506;

    public static final QVOrganActionRec vOrganActionRec = new QVOrganActionRec("V_ORGAN_ACTION_REC");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> lolfId = createSimple("lolfId", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public QVOrganActionRec(String variable) {
        super(QVOrganActionRec.class, forVariable(variable), "GFC", "V_ORGAN_ACTION_REC");
        addMetadata();
    }

    public QVOrganActionRec(String variable, String schema, String table) {
        super(QVOrganActionRec.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVOrganActionRec(Path<? extends QVOrganActionRec> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ORGAN_ACTION_REC");
        addMetadata();
    }

    public QVOrganActionRec(PathMetadata<?> metadata) {
        super(QVOrganActionRec.class, metadata, "GFC", "V_ORGAN_ACTION_REC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(lolfId, ColumnMetadata.named("LOLF_ID").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(3).ofType(Types.OTHER).withSize(0));
    }

}

