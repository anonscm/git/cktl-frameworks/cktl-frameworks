package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVDepenseUnifiee is a Querydsl query type for QVDepenseUnifiee
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVDepenseUnifiee extends com.mysema.query.sql.RelationalPathBase<QVDepenseUnifiee> {

    private static final long serialVersionUID = -853160994;

    public static final QVDepenseUnifiee vDepenseUnifiee = new QVDepenseUnifiee("V_DEPENSE_UNIFIEE");

    public final NumberPath<Long> ceOrdre = createNumber("ceOrdre", Long.class);

    public final NumberPath<Long> cmCode = createNumber("cmCode", Long.class);

    public final NumberPath<Long> cmCodeFam = createNumber("cmCodeFam", Long.class);

    public final NumberPath<Integer> cmLib = createNumber("cmLib", Integer.class);

    public final NumberPath<Long> cmLibFam = createNumber("cmLibFam", Long.class);

    public final NumberPath<Long> exeOrdre = createNumber("exeOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> montantHt = createNumber("montantHt", java.math.BigDecimal.class);

    public final StringPath seuilMin = createString("seuilMin");

    public QVDepenseUnifiee(String variable) {
        super(QVDepenseUnifiee.class, forVariable(variable), "GFC", "V_DEPENSE_UNIFIEE");
        addMetadata();
    }

    public QVDepenseUnifiee(String variable, String schema, String table) {
        super(QVDepenseUnifiee.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVDepenseUnifiee(Path<? extends QVDepenseUnifiee> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_DEPENSE_UNIFIEE");
        addMetadata();
    }

    public QVDepenseUnifiee(PathMetadata<?> metadata) {
        super(QVDepenseUnifiee.class, metadata, "GFC", "V_DEPENSE_UNIFIEE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ceOrdre, ColumnMetadata.named("CE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cmCode, ColumnMetadata.named("CM_CODE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cmCodeFam, ColumnMetadata.named("CM_CODE_FAM").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(cmLib, ColumnMetadata.named("CM_LIB").withIndex(4).ofType(Types.DECIMAL).withSize(4));
        addMetadata(cmLibFam, ColumnMetadata.named("CM_LIB_FAM").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(montantHt, ColumnMetadata.named("MONTANT_HT").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(seuilMin, ColumnMetadata.named("SEUIL_MIN").withIndex(6).ofType(Types.VARCHAR).withSize(20).notNull());
    }

}

