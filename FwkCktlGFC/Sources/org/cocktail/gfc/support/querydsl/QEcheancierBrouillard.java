package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEcheancierBrouillard is a Querydsl query type for QEcheancierBrouillard
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEcheancierBrouillard extends com.mysema.query.sql.RelationalPathBase<QEcheancierBrouillard> {

    private static final long serialVersionUID = 1276995516;

    public static final QEcheancierBrouillard echeancierBrouillard = new QEcheancierBrouillard("ECHEANCIER_BROUILLARD");

    public final NumberPath<Long> bobOrdre = createNumber("bobOrdre", Long.class);

    public final NumberPath<Long> ebrId = createNumber("ebrId", Long.class);

    public final NumberPath<Long> echeEcheancierOrdre = createNumber("echeEcheancierOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QEcheancierBrouillard> echeancierBrouillardPk = createPrimaryKey(ebrId);

    public final com.mysema.query.sql.ForeignKey<QEcheancier> echeancierBrouillardEchFk = createForeignKey(echeEcheancierOrdre, "ECHE_ECHEANCIER_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBordereauBrouillard> echeancierBrouillardBobFk = createForeignKey(bobOrdre, "BOB_ORDRE");

    public QEcheancierBrouillard(String variable) {
        super(QEcheancierBrouillard.class, forVariable(variable), "GFC", "ECHEANCIER_BROUILLARD");
        addMetadata();
    }

    public QEcheancierBrouillard(String variable, String schema, String table) {
        super(QEcheancierBrouillard.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEcheancierBrouillard(Path<? extends QEcheancierBrouillard> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ECHEANCIER_BROUILLARD");
        addMetadata();
    }

    public QEcheancierBrouillard(PathMetadata<?> metadata) {
        super(QEcheancierBrouillard.class, metadata, "GFC", "ECHEANCIER_BROUILLARD");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bobOrdre, ColumnMetadata.named("BOB_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ebrId, ColumnMetadata.named("EBR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(echeEcheancierOrdre, ColumnMetadata.named("ECHE_ECHEANCIER_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

