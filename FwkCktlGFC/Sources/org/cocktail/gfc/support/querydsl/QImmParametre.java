package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmParametre is a Querydsl query type for QImmParametre
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmParametre extends com.mysema.query.sql.RelationalPathBase<QImmParametre> {

    private static final long serialVersionUID = 1164109543;

    public static final QImmParametre immParametre = new QImmParametre("IMM_PARAMETRE");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath parDescription = createString("parDescription");

    public final StringPath parKey = createString("parKey");

    public final StringPath parValue = createString("parValue");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> immParametreExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QImmParametre(String variable) {
        super(QImmParametre.class, forVariable(variable), "GFC", "IMM_PARAMETRE");
        addMetadata();
    }

    public QImmParametre(String variable, String schema, String table) {
        super(QImmParametre.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmParametre(Path<? extends QImmParametre> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_PARAMETRE");
        addMetadata();
    }

    public QImmParametre(PathMetadata<?> metadata) {
        super(QImmParametre.class, metadata, "GFC", "IMM_PARAMETRE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(4));
        addMetadata(parDescription, ColumnMetadata.named("PAR_DESCRIPTION").withIndex(3).ofType(Types.VARCHAR).withSize(200));
        addMetadata(parKey, ColumnMetadata.named("PAR_KEY").withIndex(1).ofType(Types.VARCHAR).withSize(80).notNull());
        addMetadata(parValue, ColumnMetadata.named("PAR_VALUE").withIndex(2).ofType(Types.VARCHAR).withSize(200));
    }

}

