package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QIndicateursContrat is a Querydsl query type for QIndicateursContrat
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QIndicateursContrat extends com.mysema.query.sql.RelationalPathBase<QIndicateursContrat> {

    private static final long serialVersionUID = 183508887;

    public static final QIndicateursContrat indicateursContrat = new QIndicateursContrat("INDICATEURS_CONTRAT");

    public final StringPath icCode = createString("icCode");

    public final NumberPath<Long> icId = createNumber("icId", Long.class);

    public final StringPath icLibelle = createString("icLibelle");

    public final NumberPath<Long> icOrdre = createNumber("icOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QIndicateursContrat> indicateursContratPk = createPrimaryKey(icId);

    public QIndicateursContrat(String variable) {
        super(QIndicateursContrat.class, forVariable(variable), "GFC", "INDICATEURS_CONTRAT");
        addMetadata();
    }

    public QIndicateursContrat(String variable, String schema, String table) {
        super(QIndicateursContrat.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QIndicateursContrat(Path<? extends QIndicateursContrat> path) {
        super(path.getType(), path.getMetadata(), "GFC", "INDICATEURS_CONTRAT");
        addMetadata();
    }

    public QIndicateursContrat(PathMetadata<?> metadata) {
        super(QIndicateursContrat.class, metadata, "GFC", "INDICATEURS_CONTRAT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(icCode, ColumnMetadata.named("IC_CODE").withIndex(1).ofType(Types.VARCHAR).withSize(10));
        addMetadata(icId, ColumnMetadata.named("IC_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(icLibelle, ColumnMetadata.named("IC_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(256));
        addMetadata(icOrdre, ColumnMetadata.named("IC_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

