package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QStructureUlr is a Querydsl query type for QStructureUlr
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QStructureUlr extends com.mysema.query.sql.RelationalPathBase<QStructureUlr> {

    private static final long serialVersionUID = 870137289;

    public static final QStructureUlr structureUlr = new QStructureUlr("STRUCTURE_ULR");

    public final StringPath cAcademie = createString("cAcademie");

    public final StringPath cNaf = createString("cNaf");

    public final StringPath cNic = createString("cNic");

    public final StringPath cRne = createString("cRne");

    public final StringPath cStatutJuridique = createString("cStatutJuridique");

    public final StringPath cStructure = createString("cStructure");

    public final StringPath cStructurePere = createString("cStructurePere");

    public final StringPath cTypeDecisionStr = createString("cTypeDecisionStr");

    public final StringPath cTypeEtablissemen = createString("cTypeEtablissemen");

    public final StringPath cTypeStructure = createString("cTypeStructure");

    public final DateTimePath<java.sql.Timestamp> dateDecision = createDateTime("dateDecision", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dateFermeture = createDateTime("dateFermeture", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dateOuverture = createDateTime("dateOuverture", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dCreation = createDateTime("dCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dModification = createDateTime("dModification", java.sql.Timestamp.class);

    public final StringPath etabCodurssaf = createString("etabCodurssaf");

    public final StringPath etabNumurssaf = createString("etabNumurssaf");

    public final NumberPath<Long> export = createNumber("export", Long.class);

    public final StringPath gencod = createString("gencod");

    public final StringPath grpAcces = createString("grpAcces");

    public final StringPath grpAlias = createString("grpAlias");

    public final StringPath grpApeCode = createString("grpApeCode");

    public final StringPath grpApeCodeBis = createString("grpApeCodeBis");

    public final StringPath grpApeCodeComp = createString("grpApeCodeComp");

    public final NumberPath<Long> grpCa = createNumber("grpCa", Long.class);

    public final NumberPath<Long> grpCapital = createNumber("grpCapital", Long.class);

    public final StringPath grpCentreDecision = createString("grpCentreDecision");

    public final NumberPath<Long> grpEffectifs = createNumber("grpEffectifs", Long.class);

    public final StringPath grpFonction1 = createString("grpFonction1");

    public final StringPath grpFonction2 = createString("grpFonction2");

    public final StringPath grpFormeJuridique = createString("grpFormeJuridique");

    public final StringPath grpMotsClefs = createString("grpMotsClefs");

    public final NumberPath<Long> grpOwner = createNumber("grpOwner", Long.class);

    public final StringPath grpResponsabilite = createString("grpResponsabilite");

    public final NumberPath<Long> grpResponsable = createNumber("grpResponsable", Long.class);

    public final StringPath grpTrademarque = createString("grpTrademarque");

    public final StringPath grpWebmestre = createString("grpWebmestre");

    public final StringPath idRnsr = createString("idRnsr");

    public final StringPath lcStructure = createString("lcStructure");

    public final StringPath llStructure = createString("llStructure");

    public final NumberPath<Long> moyenneAge = createNumber("moyenneAge", Long.class);

    public final StringPath numAssedic = createString("numAssedic");

    public final StringPath numCnracl = createString("numCnracl");

    public final StringPath numIrcantec = createString("numIrcantec");

    public final StringPath numRafp = createString("numRafp");

    public final NumberPath<Long> orgOrdre = createNumber("orgOrdre", Long.class);

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public final NumberPath<Long> persIdCreation = createNumber("persIdCreation", Long.class);

    public final NumberPath<Long> persIdModification = createNumber("persIdModification", Long.class);

    public final NumberPath<Long> psecOrdre = createNumber("psecOrdre", Long.class);

    public final StringPath refDecision = createString("refDecision");

    public final StringPath refExtComp = createString("refExtComp");

    public final StringPath refExtCr = createString("refExtCr");

    public final StringPath refExtEtab = createString("refExtEtab");

    public final StringPath risqueAccTrav = createString("risqueAccTrav");

    public final NumberPath<Long> romId = createNumber("romId", Long.class);

    public final NumberPath<Long> sactId = createNumber("sactId", Long.class);

    public final StringPath siren = createString("siren");

    public final StringPath siret = createString("siret");

    public final StringPath strAccueil = createString("strAccueil");

    public final StringPath strActivite = createString("strActivite");

    public final StringPath strAffichage = createString("strAffichage");

    public final StringPath strDescription = createString("strDescription");

    public final StringPath strOrigine = createString("strOrigine");

    public final StringPath strPhoto = createString("strPhoto");

    public final StringPath strRecherche = createString("strRecherche");

    public final StringPath strStatut = createString("strStatut");

    public final StringPath tauxAccTrav = createString("tauxAccTrav");

    public final StringPath tauxExonerationTva = createString("tauxExonerationTva");

    public final StringPath tauxIr = createString("tauxIr");

    public final StringPath tauxTransport = createString("tauxTransport");

    public final StringPath temCotisAssedic = createString("temCotisAssedic");

    public final StringPath temDads = createString("temDads");

    public final StringPath temEtablissementPaye = createString("temEtablissementPaye");

    public final StringPath temPlafondReduit = createString("temPlafondReduit");

    public final StringPath temSectorise = createString("temSectorise");

    public final StringPath temSiretProvisoire = createString("temSiretProvisoire");

    public final StringPath temSoumisTva = createString("temSoumisTva");

    public final StringPath temValide = createString("temValide");

    public final StringPath tvaIntracom = createString("tvaIntracom");

    public final StringPath uaiHebergeur = createString("uaiHebergeur");

    public final com.mysema.query.sql.PrimaryKey<QStructureUlr> structurePk = createPrimaryKey(cStructure);

    public QStructureUlr(String variable) {
        super(QStructureUlr.class, forVariable(variable), "GRHUM", "STRUCTURE_ULR");
        addMetadata();
    }

    public QStructureUlr(String variable, String schema, String table) {
        super(QStructureUlr.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QStructureUlr(Path<? extends QStructureUlr> path) {
        super(path.getType(), path.getMetadata(), "GRHUM", "STRUCTURE_ULR");
        addMetadata();
    }

    public QStructureUlr(PathMetadata<?> metadata) {
        super(QStructureUlr.class, metadata, "GRHUM", "STRUCTURE_ULR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cAcademie, ColumnMetadata.named("C_ACADEMIE").withIndex(8).ofType(Types.VARCHAR).withSize(3));
        addMetadata(cNaf, ColumnMetadata.named("C_NAF").withIndex(13).ofType(Types.VARCHAR).withSize(5));
        addMetadata(cNic, ColumnMetadata.named("C_NIC").withIndex(55).ofType(Types.VARCHAR).withSize(1));
        addMetadata(cRne, ColumnMetadata.named("C_RNE").withIndex(10).ofType(Types.VARCHAR).withSize(8));
        addMetadata(cStatutJuridique, ColumnMetadata.named("C_STATUT_JURIDIQUE").withIndex(9).ofType(Types.VARCHAR).withSize(2));
        addMetadata(cStructure, ColumnMetadata.named("C_STRUCTURE").withIndex(1).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(cStructurePere, ColumnMetadata.named("C_STRUCTURE_PERE").withIndex(6).ofType(Types.VARCHAR).withSize(10));
        addMetadata(cTypeDecisionStr, ColumnMetadata.named("C_TYPE_DECISION_STR").withIndex(14).ofType(Types.VARCHAR).withSize(2));
        addMetadata(cTypeEtablissemen, ColumnMetadata.named("C_TYPE_ETABLISSEMEN").withIndex(7).ofType(Types.VARCHAR).withSize(5));
        addMetadata(cTypeStructure, ColumnMetadata.named("C_TYPE_STRUCTURE").withIndex(5).ofType(Types.VARCHAR).withSize(2).notNull());
        addMetadata(dateDecision, ColumnMetadata.named("DATE_DECISION").withIndex(19).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dateFermeture, ColumnMetadata.named("DATE_FERMETURE").withIndex(21).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dateOuverture, ColumnMetadata.named("DATE_OUVERTURE").withIndex(20).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dCreation, ColumnMetadata.named("D_CREATION").withIndex(44).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dModification, ColumnMetadata.named("D_MODIFICATION").withIndex(45).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(etabCodurssaf, ColumnMetadata.named("ETAB_CODURSSAF").withIndex(46).ofType(Types.VARCHAR).withSize(4));
        addMetadata(etabNumurssaf, ColumnMetadata.named("ETAB_NUMURSSAF").withIndex(49).ofType(Types.VARCHAR).withSize(18));
        addMetadata(export, ColumnMetadata.named("EXPORT").withIndex(62).ofType(Types.DECIMAL).withSize(0));
        addMetadata(gencod, ColumnMetadata.named("GENCOD").withIndex(72).ofType(Types.VARCHAR).withSize(13));
        addMetadata(grpAcces, ColumnMetadata.named("GRP_ACCES").withIndex(35).ofType(Types.VARCHAR).withSize(1));
        addMetadata(grpAlias, ColumnMetadata.named("GRP_ALIAS").withIndex(36).ofType(Types.VARCHAR).withSize(200));
        addMetadata(grpApeCode, ColumnMetadata.named("GRP_APE_CODE").withIndex(32).ofType(Types.VARCHAR).withSize(10));
        addMetadata(grpApeCodeBis, ColumnMetadata.named("GRP_APE_CODE_BIS").withIndex(33).ofType(Types.VARCHAR).withSize(10));
        addMetadata(grpApeCodeComp, ColumnMetadata.named("GRP_APE_CODE_COMP").withIndex(34).ofType(Types.VARCHAR).withSize(10));
        addMetadata(grpCa, ColumnMetadata.named("GRP_CA").withIndex(29).ofType(Types.DECIMAL).withSize(0));
        addMetadata(grpCapital, ColumnMetadata.named("GRP_CAPITAL").withIndex(28).ofType(Types.DECIMAL).withSize(0));
        addMetadata(grpCentreDecision, ColumnMetadata.named("GRP_CENTRE_DECISION").withIndex(31).ofType(Types.VARCHAR).withSize(60));
        addMetadata(grpEffectifs, ColumnMetadata.named("GRP_EFFECTIFS").withIndex(30).ofType(Types.DECIMAL).withSize(0));
        addMetadata(grpFonction1, ColumnMetadata.named("GRP_FONCTION1").withIndex(40).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(grpFonction2, ColumnMetadata.named("GRP_FONCTION2").withIndex(41).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(grpFormeJuridique, ColumnMetadata.named("GRP_FORME_JURIDIQUE").withIndex(27).ofType(Types.VARCHAR).withSize(4));
        addMetadata(grpMotsClefs, ColumnMetadata.named("GRP_MOTS_CLEFS").withIndex(43).ofType(Types.VARCHAR).withSize(255));
        addMetadata(grpOwner, ColumnMetadata.named("GRP_OWNER").withIndex(25).ofType(Types.DECIMAL).withSize(0));
        addMetadata(grpResponsabilite, ColumnMetadata.named("GRP_RESPONSABILITE").withIndex(37).ofType(Types.VARCHAR).withSize(120));
        addMetadata(grpResponsable, ColumnMetadata.named("GRP_RESPONSABLE").withIndex(26).ofType(Types.DECIMAL).withSize(0));
        addMetadata(grpTrademarque, ColumnMetadata.named("GRP_TRADEMARQUE").withIndex(38).ofType(Types.VARCHAR).withSize(50));
        addMetadata(grpWebmestre, ColumnMetadata.named("GRP_WEBMESTRE").withIndex(39).ofType(Types.VARCHAR).withSize(60));
        addMetadata(idRnsr, ColumnMetadata.named("ID_RNSR").withIndex(79).ofType(Types.VARCHAR).withSize(10));
        addMetadata(lcStructure, ColumnMetadata.named("LC_STRUCTURE").withIndex(4).ofType(Types.VARCHAR).withSize(30));
        addMetadata(llStructure, ColumnMetadata.named("LL_STRUCTURE").withIndex(3).ofType(Types.VARCHAR).withSize(200));
        addMetadata(moyenneAge, ColumnMetadata.named("MOYENNE_AGE").withIndex(61).ofType(Types.DECIMAL).withSize(0));
        addMetadata(numAssedic, ColumnMetadata.named("NUM_ASSEDIC").withIndex(47).ofType(Types.VARCHAR).withSize(13));
        addMetadata(numCnracl, ColumnMetadata.named("NUM_CNRACL").withIndex(66).ofType(Types.VARCHAR).withSize(13));
        addMetadata(numIrcantec, ColumnMetadata.named("NUM_IRCANTEC").withIndex(48).ofType(Types.VARCHAR).withSize(13));
        addMetadata(numRafp, ColumnMetadata.named("NUM_RAFP").withIndex(70).ofType(Types.VARCHAR).withSize(10));
        addMetadata(orgOrdre, ColumnMetadata.named("ORG_ORDRE").withIndex(42).ofType(Types.DECIMAL).withSize(0));
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(persIdCreation, ColumnMetadata.named("PERS_ID_CREATION").withIndex(73).ofType(Types.DECIMAL).withSize(0));
        addMetadata(persIdModification, ColumnMetadata.named("PERS_ID_MODIFICATION").withIndex(74).ofType(Types.DECIMAL).withSize(0));
        addMetadata(psecOrdre, ColumnMetadata.named("PSEC_ORDRE").withIndex(50).ofType(Types.DECIMAL).withSize(0));
        addMetadata(refDecision, ColumnMetadata.named("REF_DECISION").withIndex(18).ofType(Types.VARCHAR).withSize(20));
        addMetadata(refExtComp, ColumnMetadata.named("REF_EXT_COMP").withIndex(16).ofType(Types.VARCHAR).withSize(10));
        addMetadata(refExtCr, ColumnMetadata.named("REF_EXT_CR").withIndex(17).ofType(Types.VARCHAR).withSize(10));
        addMetadata(refExtEtab, ColumnMetadata.named("REF_EXT_ETAB").withIndex(15).ofType(Types.VARCHAR).withSize(10));
        addMetadata(risqueAccTrav, ColumnMetadata.named("RISQUE_ACC_TRAV").withIndex(57).ofType(Types.VARCHAR).withSize(8));
        addMetadata(romId, ColumnMetadata.named("ROM_ID").withIndex(76).ofType(Types.DECIMAL).withSize(0));
        addMetadata(sactId, ColumnMetadata.named("SACT_ID").withIndex(80).ofType(Types.DECIMAL).withSize(0));
        addMetadata(siren, ColumnMetadata.named("SIREN").withIndex(12).ofType(Types.VARCHAR).withSize(9));
        addMetadata(siret, ColumnMetadata.named("SIRET").withIndex(11).ofType(Types.VARCHAR).withSize(14));
        addMetadata(strAccueil, ColumnMetadata.named("STR_ACCUEIL").withIndex(68).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(strActivite, ColumnMetadata.named("STR_ACTIVITE").withIndex(24).ofType(Types.VARCHAR).withSize(80));
        addMetadata(strAffichage, ColumnMetadata.named("STR_AFFICHAGE").withIndex(71).ofType(Types.VARCHAR).withSize(120));
        addMetadata(strDescription, ColumnMetadata.named("STR_DESCRIPTION").withIndex(77).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(strOrigine, ColumnMetadata.named("STR_ORIGINE").withIndex(22).ofType(Types.VARCHAR).withSize(80));
        addMetadata(strPhoto, ColumnMetadata.named("STR_PHOTO").withIndex(23).ofType(Types.VARCHAR).withSize(1));
        addMetadata(strRecherche, ColumnMetadata.named("STR_RECHERCHE").withIndex(69).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(strStatut, ColumnMetadata.named("STR_STATUT").withIndex(75).ofType(Types.VARCHAR).withSize(2));
        addMetadata(tauxAccTrav, ColumnMetadata.named("TAUX_ACC_TRAV").withIndex(56).ofType(Types.VARCHAR).withSize(8));
        addMetadata(tauxExonerationTva, ColumnMetadata.named("TAUX_EXONERATION_TVA").withIndex(63).ofType(Types.VARCHAR).withSize(8));
        addMetadata(tauxIr, ColumnMetadata.named("TAUX_IR").withIndex(59).ofType(Types.VARCHAR).withSize(8));
        addMetadata(tauxTransport, ColumnMetadata.named("TAUX_TRANSPORT").withIndex(58).ofType(Types.VARCHAR).withSize(8));
        addMetadata(temCotisAssedic, ColumnMetadata.named("TEM_COTIS_ASSEDIC").withIndex(60).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(temDads, ColumnMetadata.named("TEM_DADS").withIndex(52).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(temEtablissementPaye, ColumnMetadata.named("TEM_ETABLISSEMENT_PAYE").withIndex(64).ofType(Types.VARCHAR).withSize(1));
        addMetadata(temPlafondReduit, ColumnMetadata.named("TEM_PLAFOND_REDUIT").withIndex(65).ofType(Types.VARCHAR).withSize(1));
        addMetadata(temSectorise, ColumnMetadata.named("TEM_SECTORISE").withIndex(51).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(temSiretProvisoire, ColumnMetadata.named("TEM_SIRET_PROVISOIRE").withIndex(78).ofType(Types.VARCHAR).withSize(1));
        addMetadata(temSoumisTva, ColumnMetadata.named("TEM_SOUMIS_TVA").withIndex(53).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(temValide, ColumnMetadata.named("TEM_VALIDE").withIndex(54).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(tvaIntracom, ColumnMetadata.named("TVA_INTRACOM").withIndex(67).ofType(Types.VARCHAR).withSize(13));
        addMetadata(uaiHebergeur, ColumnMetadata.named("UAI_HEBERGEUR").withIndex(81).ofType(Types.VARCHAR).withSize(8));
    }

}

