package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZlogUtil is a Querydsl query type for QZlogUtil
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZlogUtil extends com.mysema.query.sql.RelationalPathBase<QZlogUtil> {

    private static final long serialVersionUID = -629143635;

    public static final QZlogUtil zlogUtil = new QZlogUtil("ZLOG_UTIL");

    public final StringPath zlogDesc = createString("zlogDesc");

    public final NumberPath<Long> zlogId = createNumber("zlogId", Long.class);

    public final DateTimePath<java.sql.Timestamp> zlogWhen = createDateTime("zlogWhen", java.sql.Timestamp.class);

    public final com.mysema.query.sql.PrimaryKey<QZlogUtil> zlogUtilPk = createPrimaryKey(zlogId);

    public QZlogUtil(String variable) {
        super(QZlogUtil.class, forVariable(variable), "GFC", "ZLOG_UTIL");
        addMetadata();
    }

    public QZlogUtil(String variable, String schema, String table) {
        super(QZlogUtil.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZlogUtil(Path<? extends QZlogUtil> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ZLOG_UTIL");
        addMetadata();
    }

    public QZlogUtil(PathMetadata<?> metadata) {
        super(QZlogUtil.class, metadata, "GFC", "ZLOG_UTIL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(zlogDesc, ColumnMetadata.named("ZLOG_DESC").withIndex(3).ofType(Types.VARCHAR).withSize(500));
        addMetadata(zlogId, ColumnMetadata.named("ZLOG_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zlogWhen, ColumnMetadata.named("ZLOG_WHEN").withIndex(2).ofType(Types.TIMESTAMP).withSize(7));
    }

}

