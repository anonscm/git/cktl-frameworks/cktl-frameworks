package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmDevise is a Querydsl query type for QAdmDevise
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmDevise extends com.mysema.query.sql.RelationalPathBase<QAdmDevise> {

    private static final long serialVersionUID = 388300207;

    public static final QAdmDevise admDevise = new QAdmDevise("ADM_DEVISE");

    public final StringPath devCode = createString("devCode");

    public final NumberPath<Long> devId = createNumber("devId", Long.class);

    public final StringPath devLibelle = createString("devLibelle");

    public final NumberPath<Long> devNbDecimales = createNumber("devNbDecimales", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmDevise> devisePk = createPrimaryKey(devId);

    public final com.mysema.query.sql.ForeignKey<QCommande> _commandeDevIdFk = createInvForeignKey(devId, "DEV_ID");

    public final com.mysema.query.sql.ForeignKey<QCatImportDevise> _importDeviseDevIdFk = createInvForeignKey(devId, "DEV_ID");

    public final com.mysema.query.sql.ForeignKey<QCatArticleMarche> _catArticleMarcheDevIdFk = createInvForeignKey(devId, "DEV_ID");

    public QAdmDevise(String variable) {
        super(QAdmDevise.class, forVariable(variable), "GFC", "ADM_DEVISE");
        addMetadata();
    }

    public QAdmDevise(String variable, String schema, String table) {
        super(QAdmDevise.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmDevise(Path<? extends QAdmDevise> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_DEVISE");
        addMetadata();
    }

    public QAdmDevise(PathMetadata<?> metadata) {
        super(QAdmDevise.class, metadata, "GFC", "ADM_DEVISE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(devCode, ColumnMetadata.named("DEV_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(3).notNull());
        addMetadata(devId, ColumnMetadata.named("DEV_ID").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(devLibelle, ColumnMetadata.named("DEV_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(devNbDecimales, ColumnMetadata.named("DEV_NB_DECIMALES").withIndex(4).ofType(Types.DECIMAL).withSize(0));
    }

}

