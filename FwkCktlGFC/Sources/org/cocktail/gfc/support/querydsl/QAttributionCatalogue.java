package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAttributionCatalogue is a Querydsl query type for QAttributionCatalogue
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAttributionCatalogue extends com.mysema.query.sql.RelationalPathBase<QAttributionCatalogue> {

    private static final long serialVersionUID = -802660053;

    public static final QAttributionCatalogue attributionCatalogue = new QAttributionCatalogue("ATTRIBUTION_CATALOGUE");

    public final NumberPath<Long> attcId = createNumber("attcId", Long.class);

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> catId = createNumber("catId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAttributionCatalogue> attributionCataloguePk = createPrimaryKey(attOrdre, catId);

    public final com.mysema.query.sql.ForeignKey<QAttribution> attFk = createForeignKey(attOrdre, "ATT_ORDRE");

    public QAttributionCatalogue(String variable) {
        super(QAttributionCatalogue.class, forVariable(variable), "GFC", "ATTRIBUTION_CATALOGUE");
        addMetadata();
    }

    public QAttributionCatalogue(String variable, String schema, String table) {
        super(QAttributionCatalogue.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAttributionCatalogue(Path<? extends QAttributionCatalogue> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ATTRIBUTION_CATALOGUE");
        addMetadata();
    }

    public QAttributionCatalogue(PathMetadata<?> metadata) {
        super(QAttributionCatalogue.class, metadata, "GFC", "ATTRIBUTION_CATALOGUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attcId, ColumnMetadata.named("ATTC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(catId, ColumnMetadata.named("CAT_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

