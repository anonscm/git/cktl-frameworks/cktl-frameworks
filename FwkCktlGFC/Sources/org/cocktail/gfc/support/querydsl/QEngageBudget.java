package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEngageBudget is a Querydsl query type for QEngageBudget
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEngageBudget extends com.mysema.query.sql.RelationalPathBase<QEngageBudget> {

    private static final long serialVersionUID = 1702076807;

    public static final QEngageBudget engageBudget = new QEngageBudget("ENGAGE_BUDGET");

    public final DateTimePath<java.sql.Timestamp> engDateSaisie = createDateTime("engDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> engHtSaisie = createNumber("engHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final StringPath engLibelle = createString("engLibelle");

    public final NumberPath<java.math.BigDecimal> engMontantBudgetaire = createNumber("engMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> engMontantBudgetaireReste = createNumber("engMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<Long> engNumero = createNumber("engNumero", Long.class);

    public final NumberPath<java.math.BigDecimal> engTtcSaisie = createNumber("engTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> engTvaSaisie = createNumber("engTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<Long> tyapId = createNumber("tyapId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QEngageBudget> engageBudgetPk = createPrimaryKey(engId);

    public final com.mysema.query.sql.ForeignKey<QAdmTauxProrata> engageBudgetTapIdFk = createForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeApplication> engageBudgetTyapIdFk = createForeignKey(tyapId, "TYAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmEb> engageBudgetOrgIdFk = createForeignKey(idAdmEb, "ID_ADM_EB");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> engageBudgetUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFournisUlr> engageBudgetFouOrdreFk = createForeignKey(fouOrdre, "FOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> engageBudgetExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeCredit> engageBudgetTcdOrdreFk = createForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapEngIdFk = createInvForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeEngagement> _commandeEngagementEngIdFk = createInvForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QPrestationBudgetClient> _pbcEngIdFk = createInvForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlAnalytique> _engageCtrlAnalEngIdFk = createInvForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlAction> _engageCtrlActionEngIdFk = createInvForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlPlanco> _engageCtrlPlanEngIdFk = createInvForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlConvention> _engageCtrlConEngIdFk = createInvForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlHorsMarche> _engageCtrlHmEngIdFk = createInvForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlMarche> _engageCtrlMarcEngIdFk = createInvForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QPiEngFac> _pefEngIdFk = createInvForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseBudget> _pdepenseBudgetEngIdFk = createInvForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> _depenseBudgetEngIdFk = createInvForeignKey(engId, "ENG_ID");

    public QEngageBudget(String variable) {
        super(QEngageBudget.class, forVariable(variable), "GFC", "ENGAGE_BUDGET");
        addMetadata();
    }

    public QEngageBudget(String variable, String schema, String table) {
        super(QEngageBudget.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEngageBudget(Path<? extends QEngageBudget> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ENGAGE_BUDGET");
        addMetadata();
    }

    public QEngageBudget(PathMetadata<?> metadata) {
        super(QEngageBudget.class, metadata, "GFC", "ENGAGE_BUDGET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(engDateSaisie, ColumnMetadata.named("ENG_DATE_SAISIE").withIndex(14).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(engHtSaisie, ColumnMetadata.named("ENG_HT_SAISIE").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(engLibelle, ColumnMetadata.named("ENG_LIBELLE").withIndex(8).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(engMontantBudgetaire, ColumnMetadata.named("ENG_MONTANT_BUDGETAIRE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engMontantBudgetaireReste, ColumnMetadata.named("ENG_MONTANT_BUDGETAIRE_RESTE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engNumero, ColumnMetadata.named("ENG_NUMERO").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(engTtcSaisie, ColumnMetadata.named("ENG_TTC_SAISIE").withIndex(13).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engTvaSaisie, ColumnMetadata.named("ENG_TVA_SAISIE").withIndex(12).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyapId, ColumnMetadata.named("TYAP_ID").withIndex(15).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(16).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

