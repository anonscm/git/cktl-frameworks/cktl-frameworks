package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEchEcheancier is a Querydsl query type for QEchEcheancier
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEchEcheancier extends com.mysema.query.sql.RelationalPathBase<QEchEcheancier> {

    private static final long serialVersionUID = -776935708;

    public static final QEchEcheancier echEcheancier = new QEchEcheancier("ECH_ECHEANCIER");

    public final StringPath echEtat = createString("echEtat");

    public final NumberPath<Long> echId = createNumber("echId", Long.class);

    public final StringPath echLibelle = createString("echLibelle");

    public final NumberPath<java.math.BigDecimal> echMontant = createNumber("echMontant", java.math.BigDecimal.class);

    public final StringPath echMontantLettres = createString("echMontantLettres");

    public final NumberPath<Long> echNbEcheances = createNumber("echNbEcheances", Long.class);

    public final NumberPath<Long> echtId = createNumber("echtId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QEchEcheancier> echEcheancierPk = createPrimaryKey(echId);

    public final com.mysema.query.sql.ForeignKey<QEchEcheancierType> echEcheancierTypeFkFk = createForeignKey(echtId, "ECHT_ID");

    public final com.mysema.query.sql.ForeignKey<QEchEcheancierDetail> _echEcheancierDetailEchFk = createInvForeignKey(echId, "ECH_ID");

    public final com.mysema.query.sql.ForeignKey<QFacture> _factureEcheIdFk = createInvForeignKey(echId, "ECHE_ID");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapEcheIdFk = createInvForeignKey(echId, "ECHE_ID");

    public final com.mysema.query.sql.ForeignKey<QEchEcheancierPrelev> _echEcheancierPrelevEchFk = createInvForeignKey(echId, "ECH_ID");

    public QEchEcheancier(String variable) {
        super(QEchEcheancier.class, forVariable(variable), "GFC", "ECH_ECHEANCIER");
        addMetadata();
    }

    public QEchEcheancier(String variable, String schema, String table) {
        super(QEchEcheancier.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEchEcheancier(Path<? extends QEchEcheancier> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ECH_ECHEANCIER");
        addMetadata();
    }

    public QEchEcheancier(PathMetadata<?> metadata) {
        super(QEchEcheancier.class, metadata, "GFC", "ECH_ECHEANCIER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(echEtat, ColumnMetadata.named("ECH_ETAT").withIndex(7).ofType(Types.VARCHAR).withSize(32).notNull());
        addMetadata(echId, ColumnMetadata.named("ECH_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(echLibelle, ColumnMetadata.named("ECH_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(echMontant, ColumnMetadata.named("ECH_MONTANT").withIndex(3).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(echMontantLettres, ColumnMetadata.named("ECH_MONTANT_LETTRES").withIndex(4).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(echNbEcheances, ColumnMetadata.named("ECH_NB_ECHEANCES").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(echtId, ColumnMetadata.named("ECHT_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

