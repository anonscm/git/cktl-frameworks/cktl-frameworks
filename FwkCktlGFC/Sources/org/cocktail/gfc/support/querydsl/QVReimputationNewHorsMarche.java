package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVReimputationNewHorsMarche is a Querydsl query type for QVReimputationNewHorsMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVReimputationNewHorsMarche extends com.mysema.query.sql.RelationalPathBase<QVReimputationNewHorsMarche> {

    private static final long serialVersionUID = -483273736;

    public static final QVReimputationNewHorsMarche vReimputationNewHorsMarche = new QVReimputationNewHorsMarche("V_REIMPUTATION_NEW_HORS_MARCHE");

    public final NumberPath<Long> ceOrdre = createNumber("ceOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> rehmHtSaisie = createNumber("rehmHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> rehmId = createNumber("rehmId", Long.class);

    public final NumberPath<java.math.BigDecimal> rehmMontantBudgetaire = createNumber("rehmMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> rehmTtcSaisie = createNumber("rehmTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> rehmTvaSaisie = createNumber("rehmTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final NumberPath<Long> typaId = createNumber("typaId", Long.class);

    public QVReimputationNewHorsMarche(String variable) {
        super(QVReimputationNewHorsMarche.class, forVariable(variable), "GFC", "V_REIMPUTATION_NEW_HORS_MARCHE");
        addMetadata();
    }

    public QVReimputationNewHorsMarche(String variable, String schema, String table) {
        super(QVReimputationNewHorsMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVReimputationNewHorsMarche(Path<? extends QVReimputationNewHorsMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_REIMPUTATION_NEW_HORS_MARCHE");
        addMetadata();
    }

    public QVReimputationNewHorsMarche(PathMetadata<?> metadata) {
        super(QVReimputationNewHorsMarche.class, metadata, "GFC", "V_REIMPUTATION_NEW_HORS_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ceOrdre, ColumnMetadata.named("CE_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(rehmHtSaisie, ColumnMetadata.named("REHM_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(rehmId, ColumnMetadata.named("REHM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(rehmMontantBudgetaire, ColumnMetadata.named("REHM_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(rehmTtcSaisie, ColumnMetadata.named("REHM_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(rehmTvaSaisie, ColumnMetadata.named("REHM_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(typaId, ColumnMetadata.named("TYPA_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
    }

}

