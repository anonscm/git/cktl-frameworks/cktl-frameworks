package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVDepensesTranche is a Querydsl query type for QVDepensesTranche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVDepensesTranche extends com.mysema.query.sql.RelationalPathBase<QVDepensesTranche> {

    private static final long serialVersionUID = -1280551785;

    public static final QVDepensesTranche vDepensesTranche = new QVDepensesTranche("V_DEPENSES_TRANCHE");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> montant = createSimple("montant", Object.class);

    public final SimplePath<Object> pcoNum = createSimple("pcoNum", Object.class);

    public final SimplePath<Object> traOrdre = createSimple("traOrdre", Object.class);

    public QVDepensesTranche(String variable) {
        super(QVDepensesTranche.class, forVariable(variable), "GFC", "V_DEPENSES_TRANCHE");
        addMetadata();
    }

    public QVDepensesTranche(String variable, String schema, String table) {
        super(QVDepensesTranche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVDepensesTranche(Path<? extends QVDepensesTranche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_DEPENSES_TRANCHE");
        addMetadata();
    }

    public QVDepensesTranche(PathMetadata<?> metadata) {
        super(QVDepensesTranche.class, metadata, "GFC", "V_DEPENSES_TRANCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(traOrdre, ColumnMetadata.named("TRA_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
    }

}

