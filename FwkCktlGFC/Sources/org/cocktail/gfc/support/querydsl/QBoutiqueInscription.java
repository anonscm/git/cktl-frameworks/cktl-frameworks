package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBoutiqueInscription is a Querydsl query type for QBoutiqueInscription
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBoutiqueInscription extends com.mysema.query.sql.RelationalPathBase<QBoutiqueInscription> {

    private static final long serialVersionUID = -709776589;

    public static final QBoutiqueInscription boutiqueInscription = new QBoutiqueInscription("BOUTIQUE_INSCRIPTION");

    public final NumberPath<Long> biId = createNumber("biId", Long.class);

    public final NumberPath<Long> boutiqueId = createNumber("boutiqueId", Long.class);

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public final NumberPath<Long> prestId = createNumber("prestId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QBoutiqueInscription> boutiqueInscriptionPk = createPrimaryKey(biId);

    public final com.mysema.query.sql.ForeignKey<QPersonne> boutiqueInscriptionPersFk1 = createForeignKey(persId, "PERS_ID");

    public final com.mysema.query.sql.ForeignKey<QBoutique> boutiqueInscriptionBoutFk1 = createForeignKey(boutiqueId, "BOUTIQUE_ID");

    public final com.mysema.query.sql.ForeignKey<QPrestation> boutiqueInscriptionPresFk1 = createForeignKey(prestId, "PREST_ID");

    public QBoutiqueInscription(String variable) {
        super(QBoutiqueInscription.class, forVariable(variable), "GFC", "BOUTIQUE_INSCRIPTION");
        addMetadata();
    }

    public QBoutiqueInscription(String variable, String schema, String table) {
        super(QBoutiqueInscription.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBoutiqueInscription(Path<? extends QBoutiqueInscription> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BOUTIQUE_INSCRIPTION");
        addMetadata();
    }

    public QBoutiqueInscription(PathMetadata<?> metadata) {
        super(QBoutiqueInscription.class, metadata, "GFC", "BOUTIQUE_INSCRIPTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(biId, ColumnMetadata.named("BI_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(boutiqueId, ColumnMetadata.named("BOUTIQUE_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(prestId, ColumnMetadata.named("PREST_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
    }

}

