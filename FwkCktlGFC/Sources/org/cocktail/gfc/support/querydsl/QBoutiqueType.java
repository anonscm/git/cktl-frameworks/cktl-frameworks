package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBoutiqueType is a Querydsl query type for QBoutiqueType
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBoutiqueType extends com.mysema.query.sql.RelationalPathBase<QBoutiqueType> {

    private static final long serialVersionUID = -519593921;

    public static final QBoutiqueType boutiqueType = new QBoutiqueType("BOUTIQUE_TYPE");

    public final StringPath btCode = createString("btCode");

    public final StringPath btLibelle = createString("btLibelle");

    public final StringPath btPrive = createString("btPrive");

    public final com.mysema.query.sql.PrimaryKey<QBoutiqueType> boutiqueTypePk = createPrimaryKey(btCode);

    public final com.mysema.query.sql.ForeignKey<QBoutique> _btCodeFk = createInvForeignKey(btCode, "BT_CODE");

    public QBoutiqueType(String variable) {
        super(QBoutiqueType.class, forVariable(variable), "GFC", "BOUTIQUE_TYPE");
        addMetadata();
    }

    public QBoutiqueType(String variable, String schema, String table) {
        super(QBoutiqueType.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBoutiqueType(Path<? extends QBoutiqueType> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BOUTIQUE_TYPE");
        addMetadata();
    }

    public QBoutiqueType(PathMetadata<?> metadata) {
        super(QBoutiqueType.class, metadata, "GFC", "BOUTIQUE_TYPE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(btCode, ColumnMetadata.named("BT_CODE").withIndex(1).ofType(Types.VARCHAR).withSize(5).notNull());
        addMetadata(btLibelle, ColumnMetadata.named("BT_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(btPrive, ColumnMetadata.named("BT_PRIVE").withIndex(3).ofType(Types.VARCHAR).withSize(1).notNull());
    }

}

