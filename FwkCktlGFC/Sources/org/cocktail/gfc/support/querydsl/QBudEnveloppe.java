package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBudEnveloppe is a Querydsl query type for QBudEnveloppe
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBudEnveloppe extends com.mysema.query.sql.RelationalPathBase<QBudEnveloppe> {

    private static final long serialVersionUID = -503761926;

    public static final QBudEnveloppe budEnveloppe = new QBudEnveloppe("BUD_ENVELOPPE");

    public final StringPath codeEnveloppe = createString("codeEnveloppe");

    public final NumberPath<Long> idBudEnveloppe = createNumber("idBudEnveloppe", Long.class);

    public final NumberPath<Long> idBudTypeEnveloppe = createNumber("idBudTypeEnveloppe", Long.class);

    public final StringPath llEnveloppe = createString("llEnveloppe");

    public final com.mysema.query.sql.PrimaryKey<QBudEnveloppe> budEnveloppePk = createPrimaryKey(idBudEnveloppe);

    public final com.mysema.query.sql.ForeignKey<QBudTypeEnveloppe> budTypeEnveloppeEnvFk = createForeignKey(idBudTypeEnveloppe, "ID_BUD_TYPE_ENVELOPPE");

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBudDepAe> _opeTrancheBudDepAeEnvFk = createInvForeignKey(idBudEnveloppe, "ID_BUD_ENVELOPPE");

    public final com.mysema.query.sql.ForeignKey<QBudPrevOpeTraDepCp> _budPrevOpeCpEnvFk = createInvForeignKey(idBudEnveloppe, "ID_BUD_ENVELOPPE");

    public final com.mysema.query.sql.ForeignKey<QBudEnvExerNatDep> _budEnveloppeEnvExerNdFk = createInvForeignKey(idBudEnveloppe, "ID_BUD_ENVELOPPE");

    public final com.mysema.query.sql.ForeignKey<QBudBudgetDep> _budBudgetDepEnvFk = createInvForeignKey(idBudEnveloppe, "ID_BUD_ENVELOPPE");

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBudDepCp> _opeTrancheBudDepCpEnvFk = createInvForeignKey(idBudEnveloppe, "ID_BUD_ENVELOPPE");

    public final com.mysema.query.sql.ForeignKey<QBudPrevHorsOpDep> _budPrevHorsOpDepEnvFk = createInvForeignKey(idBudEnveloppe, "ID_BUD_ENVELOPPE");

    public final com.mysema.query.sql.ForeignKey<QBudPrevOpeTraDepAe> _budPrevOpeAeEnvFk = createInvForeignKey(idBudEnveloppe, "ID_BUD_ENVELOPPE");

    public QBudEnveloppe(String variable) {
        super(QBudEnveloppe.class, forVariable(variable), "GFC", "BUD_ENVELOPPE");
        addMetadata();
    }

    public QBudEnveloppe(String variable, String schema, String table) {
        super(QBudEnveloppe.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBudEnveloppe(Path<? extends QBudEnveloppe> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BUD_ENVELOPPE");
        addMetadata();
    }

    public QBudEnveloppe(PathMetadata<?> metadata) {
        super(QBudEnveloppe.class, metadata, "GFC", "BUD_ENVELOPPE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(codeEnveloppe, ColumnMetadata.named("CODE_ENVELOPPE").withIndex(2).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(idBudEnveloppe, ColumnMetadata.named("ID_BUD_ENVELOPPE").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idBudTypeEnveloppe, ColumnMetadata.named("ID_BUD_TYPE_ENVELOPPE").withIndex(4).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(llEnveloppe, ColumnMetadata.named("LL_ENVELOPPE").withIndex(3).ofType(Types.VARCHAR).withSize(500).notNull());
    }

}

