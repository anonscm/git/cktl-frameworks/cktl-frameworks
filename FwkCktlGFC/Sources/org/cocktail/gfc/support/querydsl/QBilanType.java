package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBilanType is a Querydsl query type for QBilanType
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBilanType extends com.mysema.query.sql.RelationalPathBase<QBilanType> {

    private static final long serialVersionUID = -1344981397;

    public static final QBilanType bilanType = new QBilanType("BILAN_TYPE");

    public final NumberPath<Long> btId = createNumber("btId", Long.class);

    public final StringPath btLibelle = createString("btLibelle");

    public final StringPath btStrId = createString("btStrId");

    public final com.mysema.query.sql.PrimaryKey<QBilanType> bilanTypePk = createPrimaryKey(btId);

    public final com.mysema.query.sql.ForeignKey<QCptfiBilanPassif> _cptfiBilanPassifBtidFk = createInvForeignKey(btId, "BT_ID");

    public final com.mysema.query.sql.ForeignKey<QBilanPoste> _bilanPosteBtIdFk = createInvForeignKey(btId, "BT_ID");

    public final com.mysema.query.sql.ForeignKey<QCptfiBilanActif> _cptfiBilanActifBtidFk = createInvForeignKey(btId, "BT_ID");

    public QBilanType(String variable) {
        super(QBilanType.class, forVariable(variable), "GFC", "BILAN_TYPE");
        addMetadata();
    }

    public QBilanType(String variable, String schema, String table) {
        super(QBilanType.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBilanType(Path<? extends QBilanType> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BILAN_TYPE");
        addMetadata();
    }

    public QBilanType(PathMetadata<?> metadata) {
        super(QBilanType.class, metadata, "GFC", "BILAN_TYPE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(btId, ColumnMetadata.named("BT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(btLibelle, ColumnMetadata.named("BT_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(btStrId, ColumnMetadata.named("BT_STR_ID").withIndex(2).ofType(Types.VARCHAR).withSize(200).notNull());
    }

}

