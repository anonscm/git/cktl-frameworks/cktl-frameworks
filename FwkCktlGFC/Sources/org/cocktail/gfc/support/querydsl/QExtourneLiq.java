package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QExtourneLiq is a Querydsl query type for QExtourneLiq
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QExtourneLiq extends com.mysema.query.sql.RelationalPathBase<QExtourneLiq> {

    private static final long serialVersionUID = 330144529;

    public static final QExtourneLiq extourneLiq = new QExtourneLiq("EXTOURNE_LIQ");

    public final NumberPath<Long> depIdN = createNumber("depIdN", Long.class);

    public final NumberPath<Long> depIdN1 = createNumber("depIdN1", Long.class);

    public final NumberPath<Long> elId = createNumber("elId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QExtourneLiq> sysC0075771 = createPrimaryKey(elId);

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> extourneLiqDepidnFk = createForeignKey(depIdN, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> extourneLiqDepidn1Fk = createForeignKey(depIdN1, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QExtourneLiqRepart> _extourneLiqRepartElidFk = createInvForeignKey(elId, "EL_ID");

    public QExtourneLiq(String variable) {
        super(QExtourneLiq.class, forVariable(variable), "GFC", "EXTOURNE_LIQ");
        addMetadata();
    }

    public QExtourneLiq(String variable, String schema, String table) {
        super(QExtourneLiq.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QExtourneLiq(Path<? extends QExtourneLiq> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EXTOURNE_LIQ");
        addMetadata();
    }

    public QExtourneLiq(PathMetadata<?> metadata) {
        super(QExtourneLiq.class, metadata, "GFC", "EXTOURNE_LIQ");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(depIdN, ColumnMetadata.named("DEP_ID_N").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(depIdN1, ColumnMetadata.named("DEP_ID_N1").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(elId, ColumnMetadata.named("EL_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

