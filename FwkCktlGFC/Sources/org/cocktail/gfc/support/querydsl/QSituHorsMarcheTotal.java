package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QSituHorsMarcheTotal is a Querydsl query type for QSituHorsMarcheTotal
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QSituHorsMarcheTotal extends com.mysema.query.sql.RelationalPathBase<QSituHorsMarcheTotal> {

    private static final long serialVersionUID = -119683006;

    public static final QSituHorsMarcheTotal situHorsMarcheTotal = new QSituHorsMarcheTotal("SITU_HORS_MARCHE_TOTAL");

    public final StringPath cmCode = createString("cmCode");

    public final StringPath cmLib = createString("cmLib");

    public final NumberPath<Long> cmOrdre = createNumber("cmOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> contr = createNumber("contr", java.math.BigDecimal.class);

    public final NumberPath<Long> eng = createNumber("eng", Long.class);

    public final NumberPath<Integer> exer = createNumber("exer", Integer.class);

    public final NumberPath<Long> liq = createNumber("liq", Long.class);

    public final NumberPath<Long> pcontr = createNumber("pcontr", Long.class);

    public final NumberPath<Long> total = createNumber("total", Long.class);

    public QSituHorsMarcheTotal(String variable) {
        super(QSituHorsMarcheTotal.class, forVariable(variable), "GFC", "SITU_HORS_MARCHE_TOTAL");
        addMetadata();
    }

    public QSituHorsMarcheTotal(String variable, String schema, String table) {
        super(QSituHorsMarcheTotal.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QSituHorsMarcheTotal(Path<? extends QSituHorsMarcheTotal> path) {
        super(path.getType(), path.getMetadata(), "GFC", "SITU_HORS_MARCHE_TOTAL");
        addMetadata();
    }

    public QSituHorsMarcheTotal(PathMetadata<?> metadata) {
        super(QSituHorsMarcheTotal.class, metadata, "GFC", "SITU_HORS_MARCHE_TOTAL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cmCode, ColumnMetadata.named("CM_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(10));
        addMetadata(cmLib, ColumnMetadata.named("CM_LIB").withIndex(4).ofType(Types.VARCHAR).withSize(150));
        addMetadata(cmOrdre, ColumnMetadata.named("CM_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(contr, ColumnMetadata.named("CONTR").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(eng, ColumnMetadata.named("ENG").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exer, ColumnMetadata.named("EXER").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(liq, ColumnMetadata.named("LIQ").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcontr, ColumnMetadata.named("PCONTR").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(total, ColumnMetadata.named("TOTAL").withIndex(9).ofType(Types.DECIMAL).withSize(0));
    }

}

