package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmDegressifCoef is a Querydsl query type for QImmDegressifCoef
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmDegressifCoef extends com.mysema.query.sql.RelationalPathBase<QImmDegressifCoef> {

    private static final long serialVersionUID = 100635803;

    public static final QImmDegressifCoef immDegressifCoef = new QImmDegressifCoef("IMM_DEGRESSIF_COEF");

    public final NumberPath<java.math.BigDecimal> dgcoCoef = createNumber("dgcoCoef", java.math.BigDecimal.class);

    public final NumberPath<Long> dgcoDureeMax = createNumber("dgcoDureeMax", Long.class);

    public final NumberPath<Long> dgcoDureeMin = createNumber("dgcoDureeMin", Long.class);

    public final NumberPath<Long> dgcoId = createNumber("dgcoId", Long.class);

    public final NumberPath<Long> dgrfId = createNumber("dgrfId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmDegressifCoef> immDegressifCoefPk = createPrimaryKey(dgcoId);

    public final com.mysema.query.sql.ForeignKey<QImmDegressif> degressifDgrfIdFk = createForeignKey(dgrfId, "DGRF_ID");

    public final com.mysema.query.sql.ForeignKey<QImmInventCompt> _invcDgcoIdFk = createInvForeignKey(dgcoId, "DGCO_ID");

    public QImmDegressifCoef(String variable) {
        super(QImmDegressifCoef.class, forVariable(variable), "GFC", "IMM_DEGRESSIF_COEF");
        addMetadata();
    }

    public QImmDegressifCoef(String variable, String schema, String table) {
        super(QImmDegressifCoef.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmDegressifCoef(Path<? extends QImmDegressifCoef> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_DEGRESSIF_COEF");
        addMetadata();
    }

    public QImmDegressifCoef(PathMetadata<?> metadata) {
        super(QImmDegressifCoef.class, metadata, "GFC", "IMM_DEGRESSIF_COEF");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dgcoCoef, ColumnMetadata.named("DGCO_COEF").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dgcoDureeMax, ColumnMetadata.named("DGCO_DUREE_MAX").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dgcoDureeMin, ColumnMetadata.named("DGCO_DUREE_MIN").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dgcoId, ColumnMetadata.named("DGCO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dgrfId, ColumnMetadata.named("DGRF_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

