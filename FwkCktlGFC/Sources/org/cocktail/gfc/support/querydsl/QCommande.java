package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommande is a Querydsl query type for QCommande
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommande extends com.mysema.query.sql.RelationalPathBase<QCommande> {

    private static final long serialVersionUID = 552069755;

    public static final QCommande commande = new QCommande("COMMANDE");

    public final DateTimePath<java.sql.Timestamp> commDate = createDateTime("commDate", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> commDateCreation = createDateTime("commDateCreation", java.sql.Timestamp.class);

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final StringPath commLibelle = createString("commLibelle");

    public final NumberPath<Long> commNumero = createNumber("commNumero", Long.class);

    public final StringPath commReference = createString("commReference");

    public final NumberPath<Long> devId = createNumber("devId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final NumberPath<Long> tyetIdImprimable = createNumber("tyetIdImprimable", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCommande> commandePk = createPrimaryKey(commId);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> commandeTyetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmDevise> commandeDevIdFk = createForeignKey(devId, "DEV_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> commandeUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFournisUlr> commandeFouOrdreFk = createForeignKey(fouOrdre, "FOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> commandeExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> commandeTyetIdImprimableFk = createForeignKey(tyetIdImprimable, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeBascule> _commandeBascOrigFk = createInvForeignKey(commId, "COMM_ID_ORIGINE");

    public final com.mysema.query.sql.ForeignKey<QCommandeUtilisateur> _commandeUtilCommidFk = createInvForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeDocument> _commandeDocumentCommIdFk = createInvForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeBudget> _commandeBudgetCommIdFk = createInvForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeBascule> _commandeBascDestFk = createInvForeignKey(commId, "COMM_ID_DESTINATION");

    public final com.mysema.query.sql.ForeignKey<QCommandeEngagement> _commandeEngagementCommIdFk = createInvForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QDepArticle> _depArticleCommIdFk = createInvForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QXlabCommande> _xlabCommandeCommIdFk = createInvForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QB2bCxmlOrderrequest> _bcorCommIdFk = createInvForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeImpression> _commandeImpresCommIdFk = createInvForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeDepPapier> _commandeDepPapierCommIdFk = createInvForeignKey(commId, "COMM_ID");

    public QCommande(String variable) {
        super(QCommande.class, forVariable(variable), "GFC", "COMMANDE");
        addMetadata();
    }

    public QCommande(String variable, String schema, String table) {
        super(QCommande.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommande(Path<? extends QCommande> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE");
        addMetadata();
    }

    public QCommande(PathMetadata<?> metadata) {
        super(QCommande.class, metadata, "GFC", "COMMANDE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(commDate, ColumnMetadata.named("COMM_DATE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(commDateCreation, ColumnMetadata.named("COMM_DATE_CREATION").withIndex(12).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commLibelle, ColumnMetadata.named("COMM_LIBELLE").withIndex(7).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(commNumero, ColumnMetadata.named("COMM_NUMERO").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commReference, ColumnMetadata.named("COMM_REFERENCE").withIndex(6).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(devId, ColumnMetadata.named("DEV_ID").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetIdImprimable, ColumnMetadata.named("TYET_ID_IMPRIMABLE").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

