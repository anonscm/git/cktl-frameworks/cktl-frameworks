package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOpeParametres is a Querydsl query type for QOpeParametres
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOpeParametres extends com.mysema.query.sql.RelationalPathBase<QOpeParametres> {

    private static final long serialVersionUID = 1845196679;

    public static final QOpeParametres opeParametres = new QOpeParametres("OPE_PARAMETRES");

    public final StringPath paramCommentaires = createString("paramCommentaires");

    public final StringPath paramKey = createString("paramKey");

    public final NumberPath<Long> paramOrdre = createNumber("paramOrdre", Long.class);

    public final StringPath paramValue = createString("paramValue");

    public final com.mysema.query.sql.PrimaryKey<QOpeParametres> opeParametresPk = createPrimaryKey(paramOrdre);

    public QOpeParametres(String variable) {
        super(QOpeParametres.class, forVariable(variable), "GFC", "OPE_PARAMETRES");
        addMetadata();
    }

    public QOpeParametres(String variable, String schema, String table) {
        super(QOpeParametres.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOpeParametres(Path<? extends QOpeParametres> path) {
        super(path.getType(), path.getMetadata(), "GFC", "OPE_PARAMETRES");
        addMetadata();
    }

    public QOpeParametres(PathMetadata<?> metadata) {
        super(QOpeParametres.class, metadata, "GFC", "OPE_PARAMETRES");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(paramCommentaires, ColumnMetadata.named("PARAM_COMMENTAIRES").withIndex(4).ofType(Types.VARCHAR).withSize(250));
        addMetadata(paramKey, ColumnMetadata.named("PARAM_KEY").withIndex(2).ofType(Types.VARCHAR).withSize(80).notNull());
        addMetadata(paramOrdre, ColumnMetadata.named("PARAM_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(paramValue, ColumnMetadata.named("PARAM_VALUE").withIndex(3).ofType(Types.VARCHAR).withSize(160).notNull());
    }

}

