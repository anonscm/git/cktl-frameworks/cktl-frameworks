package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTitulaire is a Querydsl query type for QTitulaire
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QTitulaire extends com.mysema.query.sql.RelationalPathBase<QTitulaire> {

    private static final long serialVersionUID = -438220560;

    public static final QTitulaire titulaire = new QTitulaire("TITULAIRE");

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> lotOrdre = createNumber("lotOrdre", Long.class);

    public final NumberPath<Long> titOrdre = createNumber("titOrdre", Long.class);

    public final NumberPath<Long> titPere = createNumber("titPere", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QTitulaire> titulairePk = createPrimaryKey(titOrdre);

    public final com.mysema.query.sql.ForeignKey<QLot> titulaireLotFk = createForeignKey(lotOrdre, "LOT_ORDRE");

    public QTitulaire(String variable) {
        super(QTitulaire.class, forVariable(variable), "GFC", "TITULAIRE");
        addMetadata();
    }

    public QTitulaire(String variable, String schema, String table) {
        super(QTitulaire.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTitulaire(Path<? extends QTitulaire> path) {
        super(path.getType(), path.getMetadata(), "GFC", "TITULAIRE");
        addMetadata();
    }

    public QTitulaire(PathMetadata<?> metadata) {
        super(QTitulaire.class, metadata, "GFC", "TITULAIRE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lotOrdre, ColumnMetadata.named("LOT_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(titOrdre, ColumnMetadata.named("TIT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(titPere, ColumnMetadata.named("TIT_PERE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
    }

}

