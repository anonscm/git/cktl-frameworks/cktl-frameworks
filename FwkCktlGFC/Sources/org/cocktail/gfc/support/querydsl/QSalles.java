package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QSalles is a Querydsl query type for QSalles
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QSalles extends com.mysema.query.sql.RelationalPathBase<QSalles> {

    private static final long serialVersionUID = -1511858755;

    public static final QSalles salles = new QSalles("SALLES");

    public final StringPath cLocal = createString("cLocal");

    public final DateTimePath<java.sql.Timestamp> dAnnulation = createDateTime("dAnnulation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dCreation = createDateTime("dCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dModification = createDateTime("dModification", java.sql.Timestamp.class);

    public final StringPath salAcces = createString("salAcces");

    public final StringPath salBadgeur = createString("salBadgeur");

    public final NumberPath<Long> salCapacite = createNumber("salCapacite", Long.class);

    public final NumberPath<Long> salCapaExam = createNumber("salCapaExam", Long.class);

    public final StringPath salCoordonnees = createString("salCoordonnees");

    public final StringPath salDescriptif = createString("salDescriptif");

    public final StringPath salEcran = createString("salEcran");

    public final StringPath salEtage = createString("salEtage");

    public final StringPath salIdentAcces = createString("salIdentAcces");

    public final StringPath salInsonorisee = createString("salInsonorisee");

    public final StringPath salNature = createString("salNature");

    public final NumberPath<Long> salNbArmoires = createNumber("salNbArmoires", Long.class);

    public final NumberPath<Long> salNbBureaux = createNumber("salNbBureaux", Long.class);

    public final NumberPath<Long> salNbChaises = createNumber("salNbChaises", Long.class);

    public final NumberPath<Long> salNbFenetres = createNumber("salNbFenetres", Long.class);

    public final NumberPath<Long> salNbPlacesExamLib = createNumber("salNbPlacesExamLib", Long.class);

    public final NumberPath<Long> salNbTables = createNumber("salNbTables", Long.class);

    public final StringPath salNoPoste = createString("salNoPoste");

    public final NumberPath<Long> salNumDepart = createNumber("salNumDepart", Long.class);

    public final NumberPath<Long> salNumero = createNumber("salNumero", Long.class);

    public final StringPath salObscur = createString("salObscur");

    public final NumberPath<Integer> salPasNum = createNumber("salPasNum", Integer.class);

    public final StringPath salPorte = createString("salPorte");

    public final NumberPath<java.math.BigDecimal> salPourAdministration = createNumber("salPourAdministration", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> salPourDocumentation = createNumber("salPourDocumentation", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> salPourEnseignement = createNumber("salPourEnseignement", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> salPourRecherche = createNumber("salPourRecherche", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> salPourTechnique = createNumber("salPourTechnique", java.math.BigDecimal.class);

    public final StringPath salReservable = createString("salReservable");

    public final StringPath salRetro = createString("salRetro");

    public final NumberPath<Long> salSalleProcheTel = createNumber("salSalleProcheTel", Long.class);

    public final NumberPath<java.math.BigDecimal> salSuperficie = createNumber("salSuperficie", java.math.BigDecimal.class);

    public final StringPath salTableau = createString("salTableau");

    public final StringPath salTableauBlanc = createString("salTableauBlanc");

    public final StringPath salTableauPapier = createString("salTableauPapier");

    public final StringPath salTelevision = createString("salTelevision");

    public final StringPath salTemEnService = createString("salTemEnService");

    public final StringPath salTemHandicap = createString("salTemHandicap");

    public final NumberPath<Long> tsalNumero = createNumber("tsalNumero", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QSalles> sallePk = createPrimaryKey(salNumero);

    public QSalles(String variable) {
        super(QSalles.class, forVariable(variable), "GRHUM", "SALLES");
        addMetadata();
    }

    public QSalles(String variable, String schema, String table) {
        super(QSalles.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QSalles(Path<? extends QSalles> path) {
        super(path.getType(), path.getMetadata(), "GRHUM", "SALLES");
        addMetadata();
    }

    public QSalles(PathMetadata<?> metadata) {
        super(QSalles.class, metadata, "GRHUM", "SALLES");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cLocal, ColumnMetadata.named("C_LOCAL").withIndex(25).ofType(Types.VARCHAR).withSize(5));
        addMetadata(dAnnulation, ColumnMetadata.named("D_ANNULATION").withIndex(42).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dCreation, ColumnMetadata.named("D_CREATION").withIndex(38).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dModification, ColumnMetadata.named("D_MODIFICATION").withIndex(39).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(salAcces, ColumnMetadata.named("SAL_ACCES").withIndex(41).ofType(Types.VARCHAR).withSize(3));
        addMetadata(salBadgeur, ColumnMetadata.named("SAL_BADGEUR").withIndex(40).ofType(Types.VARCHAR).withSize(1));
        addMetadata(salCapacite, ColumnMetadata.named("SAL_CAPACITE").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(salCapaExam, ColumnMetadata.named("SAL_CAPA_EXAM").withIndex(34).ofType(Types.DECIMAL).withSize(0));
        addMetadata(salCoordonnees, ColumnMetadata.named("SAL_COORDONNEES").withIndex(43).ofType(Types.VARCHAR).withSize(256));
        addMetadata(salDescriptif, ColumnMetadata.named("SAL_DESCRIPTIF").withIndex(14).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(salEcran, ColumnMetadata.named("SAL_ECRAN").withIndex(21).ofType(Types.VARCHAR).withSize(1));
        addMetadata(salEtage, ColumnMetadata.named("SAL_ETAGE").withIndex(3).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(salIdentAcces, ColumnMetadata.named("SAL_IDENT_ACCES").withIndex(19).ofType(Types.VARCHAR).withSize(12));
        addMetadata(salInsonorisee, ColumnMetadata.named("SAL_INSONORISEE").withIndex(9).ofType(Types.VARCHAR).withSize(1));
        addMetadata(salNature, ColumnMetadata.named("SAL_NATURE").withIndex(4).ofType(Types.VARCHAR).withSize(18));
        addMetadata(salNbArmoires, ColumnMetadata.named("SAL_NB_ARMOIRES").withIndex(18).ofType(Types.DECIMAL).withSize(0));
        addMetadata(salNbBureaux, ColumnMetadata.named("SAL_NB_BUREAUX").withIndex(17).ofType(Types.DECIMAL).withSize(0));
        addMetadata(salNbChaises, ColumnMetadata.named("SAL_NB_CHAISES").withIndex(15).ofType(Types.DECIMAL).withSize(0));
        addMetadata(salNbFenetres, ColumnMetadata.named("SAL_NB_FENETRES").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(salNbPlacesExamLib, ColumnMetadata.named("SAL_NB_PLACES_EXAM_LIB").withIndex(35).ofType(Types.DECIMAL).withSize(0));
        addMetadata(salNbTables, ColumnMetadata.named("SAL_NB_TABLES").withIndex(16).ofType(Types.DECIMAL).withSize(0));
        addMetadata(salNoPoste, ColumnMetadata.named("SAL_NO_POSTE").withIndex(13).ofType(Types.VARCHAR).withSize(5));
        addMetadata(salNumDepart, ColumnMetadata.named("SAL_NUM_DEPART").withIndex(37).ofType(Types.DECIMAL).withSize(0));
        addMetadata(salNumero, ColumnMetadata.named("SAL_NUMERO").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(salObscur, ColumnMetadata.named("SAL_OBSCUR").withIndex(20).ofType(Types.VARCHAR).withSize(1));
        addMetadata(salPasNum, ColumnMetadata.named("SAL_PAS_NUM").withIndex(31).ofType(Types.DECIMAL).withSize(1));
        addMetadata(salPorte, ColumnMetadata.named("SAL_PORTE").withIndex(2).ofType(Types.VARCHAR).withSize(150).notNull());
        addMetadata(salPourAdministration, ColumnMetadata.named("SAL_POUR_ADMINISTRATION").withIndex(28).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(salPourDocumentation, ColumnMetadata.named("SAL_POUR_DOCUMENTATION").withIndex(29).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(salPourEnseignement, ColumnMetadata.named("SAL_POUR_ENSEIGNEMENT").withIndex(26).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(salPourRecherche, ColumnMetadata.named("SAL_POUR_RECHERCHE").withIndex(27).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(salPourTechnique, ColumnMetadata.named("SAL_POUR_TECHNIQUE").withIndex(30).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(salReservable, ColumnMetadata.named("SAL_RESERVABLE").withIndex(22).ofType(Types.VARCHAR).withSize(1));
        addMetadata(salRetro, ColumnMetadata.named("SAL_RETRO").withIndex(11).ofType(Types.VARCHAR).withSize(1));
        addMetadata(salSalleProcheTel, ColumnMetadata.named("SAL_SALLE_PROCHE_TEL").withIndex(36).ofType(Types.DECIMAL).withSize(0));
        addMetadata(salSuperficie, ColumnMetadata.named("SAL_SUPERFICIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(salTableau, ColumnMetadata.named("SAL_TABLEAU").withIndex(10).ofType(Types.VARCHAR).withSize(1));
        addMetadata(salTableauBlanc, ColumnMetadata.named("SAL_TABLEAU_BLANC").withIndex(23).ofType(Types.VARCHAR).withSize(1));
        addMetadata(salTableauPapier, ColumnMetadata.named("SAL_TABLEAU_PAPIER").withIndex(24).ofType(Types.VARCHAR).withSize(1));
        addMetadata(salTelevision, ColumnMetadata.named("SAL_TELEVISION").withIndex(12).ofType(Types.VARCHAR).withSize(1));
        addMetadata(salTemEnService, ColumnMetadata.named("SAL_TEM_EN_SERVICE").withIndex(33).ofType(Types.VARCHAR).withSize(1));
        addMetadata(salTemHandicap, ColumnMetadata.named("SAL_TEM_HANDICAP").withIndex(32).ofType(Types.VARCHAR).withSize(1));
        addMetadata(tsalNumero, ColumnMetadata.named("TSAL_NUMERO").withIndex(5).ofType(Types.DECIMAL).withSize(38));
    }

}

