package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QB2bCxmlOrderrequest is a Querydsl query type for QB2bCxmlOrderrequest
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QB2bCxmlOrderrequest extends com.mysema.query.sql.RelationalPathBase<QB2bCxmlOrderrequest> {

    private static final long serialVersionUID = -950183354;

    public static final QB2bCxmlOrderrequest b2bCxmlOrderrequest = new QB2bCxmlOrderrequest("B2B_CXML_ORDERREQUEST");

    public final NumberPath<Long> bcorId = createNumber("bcorId", Long.class);

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final StringPath cxml = createString("cxml");

    public final StringPath cxmlReponse = createString("cxmlReponse");

    public final DateTimePath<java.sql.Timestamp> dateCreation = createDateTime("dateCreation", java.sql.Timestamp.class);

    public final NumberPath<Long> fbcpId = createNumber("fbcpId", Long.class);

    public final NumberPath<Long> persIdCreation = createNumber("persIdCreation", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QB2bCxmlOrderrequest> b2bCxmlOrderrequestPk = createPrimaryKey(bcorId);

    public final com.mysema.query.sql.ForeignKey<QFournisB2bCxmlParam> bcorFbcpIdFk = createForeignKey(fbcpId, "FBCP_ID");

    public final com.mysema.query.sql.ForeignKey<QCommande> bcorCommIdFk = createForeignKey(commId, "COMM_ID");

    public QB2bCxmlOrderrequest(String variable) {
        super(QB2bCxmlOrderrequest.class, forVariable(variable), "GFC", "B2B_CXML_ORDERREQUEST");
        addMetadata();
    }

    public QB2bCxmlOrderrequest(String variable, String schema, String table) {
        super(QB2bCxmlOrderrequest.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QB2bCxmlOrderrequest(Path<? extends QB2bCxmlOrderrequest> path) {
        super(path.getType(), path.getMetadata(), "GFC", "B2B_CXML_ORDERREQUEST");
        addMetadata();
    }

    public QB2bCxmlOrderrequest(PathMetadata<?> metadata) {
        super(QB2bCxmlOrderrequest.class, metadata, "GFC", "B2B_CXML_ORDERREQUEST");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bcorId, ColumnMetadata.named("BCOR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cxml, ColumnMetadata.named("CXML").withIndex(6).ofType(Types.CLOB).withSize(4000).notNull());
        addMetadata(cxmlReponse, ColumnMetadata.named("CXML_REPONSE").withIndex(7).ofType(Types.CLOB).withSize(4000));
        addMetadata(dateCreation, ColumnMetadata.named("DATE_CREATION").withIndex(5).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(fbcpId, ColumnMetadata.named("FBCP_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(persIdCreation, ColumnMetadata.named("PERS_ID_CREATION").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

