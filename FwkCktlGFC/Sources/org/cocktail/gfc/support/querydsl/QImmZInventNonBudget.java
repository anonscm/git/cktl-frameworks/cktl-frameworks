package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmZInventNonBudget is a Querydsl query type for QImmZInventNonBudget
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmZInventNonBudget extends com.mysema.query.sql.RelationalPathBase<QImmZInventNonBudget> {

    private static final long serialVersionUID = 622674342;

    public static final QImmZInventNonBudget immZInventNonBudget = new QImmZInventNonBudget("IMM_Z_INVENT_NON_BUDGET");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final DateTimePath<java.sql.Timestamp> inbDateAcquisition = createDateTime("inbDateAcquisition", java.sql.Timestamp.class);

    public final NumberPath<Long> inbDuree = createNumber("inbDuree", Long.class);

    public final StringPath inbFacture = createString("inbFacture");

    public final StringPath inbFournisseur = createString("inbFournisseur");

    public final NumberPath<Long> inbId = createNumber("inbId", Long.class);

    public final StringPath inbInformations = createString("inbInformations");

    public final NumberPath<java.math.BigDecimal> inbMontant = createNumber("inbMontant", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> inbMontantResiduel = createNumber("inbMontantResiduel", java.math.BigDecimal.class);

    public final StringPath inbNumeroSerie = createString("inbNumeroSerie");

    public final StringPath inbTypeAmort = createString("inbTypeAmort");

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final NumberPath<Long> origId = createNumber("origId", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> zinbDate = createDateTime("zinbDate", java.sql.Timestamp.class);

    public final NumberPath<Long> zinbId = createNumber("zinbId", Long.class);

    public final NumberPath<Long> zinbUtlOrdre = createNumber("zinbUtlOrdre", Long.class);

    public QImmZInventNonBudget(String variable) {
        super(QImmZInventNonBudget.class, forVariable(variable), "GFC", "IMM_Z_INVENT_NON_BUDGET");
        addMetadata();
    }

    public QImmZInventNonBudget(String variable, String schema, String table) {
        super(QImmZInventNonBudget.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmZInventNonBudget(Path<? extends QImmZInventNonBudget> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_Z_INVENT_NON_BUDGET");
        addMetadata();
    }

    public QImmZInventNonBudget(PathMetadata<?> metadata) {
        super(QImmZInventNonBudget.class, metadata, "GFC", "IMM_Z_INVENT_NON_BUDGET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(inbDateAcquisition, ColumnMetadata.named("INB_DATE_ACQUISITION").withIndex(19).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(inbDuree, ColumnMetadata.named("INB_DUREE").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(inbFacture, ColumnMetadata.named("INB_FACTURE").withIndex(17).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(inbFournisseur, ColumnMetadata.named("INB_FOURNISSEUR").withIndex(16).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(inbId, ColumnMetadata.named("INB_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(inbInformations, ColumnMetadata.named("INB_INFORMATIONS").withIndex(15).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(inbMontant, ColumnMetadata.named("INB_MONTANT").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(inbMontantResiduel, ColumnMetadata.named("INB_MONTANT_RESIDUEL").withIndex(12).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(inbNumeroSerie, ColumnMetadata.named("INB_NUMERO_SERIE").withIndex(14).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(inbTypeAmort, ColumnMetadata.named("INB_TYPE_AMORT").withIndex(10).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(18).ofType(Types.DECIMAL).withSize(0));
        addMetadata(origId, ColumnMetadata.named("ORIG_ID").withIndex(13).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(8).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zinbDate, ColumnMetadata.named("ZINB_DATE").withIndex(2).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(zinbId, ColumnMetadata.named("ZINB_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zinbUtlOrdre, ColumnMetadata.named("ZINB_UTL_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
    }

}

