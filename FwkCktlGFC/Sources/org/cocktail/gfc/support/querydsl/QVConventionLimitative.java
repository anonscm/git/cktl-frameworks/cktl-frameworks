package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVConventionLimitative is a Querydsl query type for QVConventionLimitative
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVConventionLimitative extends com.mysema.query.sql.RelationalPathBase<QVConventionLimitative> {

    private static final long serialVersionUID = 786018928;

    public static final QVConventionLimitative vConventionLimitative = new QVConventionLimitative("V_CONVENTION_LIMITATIVE");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> montant = createSimple("montant", Object.class);

    public QVConventionLimitative(String variable) {
        super(QVConventionLimitative.class, forVariable(variable), "GFC", "V_CONVENTION_LIMITATIVE");
        addMetadata();
    }

    public QVConventionLimitative(String variable, String schema, String table) {
        super(QVConventionLimitative.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVConventionLimitative(Path<? extends QVConventionLimitative> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_CONVENTION_LIMITATIVE");
        addMetadata();
    }

    public QVConventionLimitative(PathMetadata<?> metadata) {
        super(QVConventionLimitative.class, metadata, "GFC", "V_CONVENTION_LIMITATIVE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(4).ofType(Types.OTHER).withSize(0));
    }

}

