package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBbBudgetDepenseCtrlCredits is a Querydsl query type for QBbBudgetDepenseCtrlCredits
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBbBudgetDepenseCtrlCredits extends com.mysema.query.sql.RelationalPathBase<QBbBudgetDepenseCtrlCredits> {

    private static final long serialVersionUID = -471654837;

    public static final QBbBudgetDepenseCtrlCredits bbBudgetDepenseCtrlCredits = new QBbBudgetDepenseCtrlCredits("BB_BUDGET_DEPENSE_CTRL_CREDITS");

    public final NumberPath<Long> dep = createNumber("dep", Long.class);

    public final NumberPath<Long> depOrv = createNumber("depOrv", Long.class);

    public final NumberPath<Long> eng = createNumber("eng", Long.class);

    public final NumberPath<Long> engOrv = createNumber("engOrv", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public QBbBudgetDepenseCtrlCredits(String variable) {
        super(QBbBudgetDepenseCtrlCredits.class, forVariable(variable), "GFC", "BB_BUDGET_DEPENSE_CTRL_CREDITS");
        addMetadata();
    }

    public QBbBudgetDepenseCtrlCredits(String variable, String schema, String table) {
        super(QBbBudgetDepenseCtrlCredits.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBbBudgetDepenseCtrlCredits(Path<? extends QBbBudgetDepenseCtrlCredits> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BB_BUDGET_DEPENSE_CTRL_CREDITS");
        addMetadata();
    }

    public QBbBudgetDepenseCtrlCredits(PathMetadata<?> metadata) {
        super(QBbBudgetDepenseCtrlCredits.class, metadata, "GFC", "BB_BUDGET_DEPENSE_CTRL_CREDITS");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dep, ColumnMetadata.named("DEP").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(depOrv, ColumnMetadata.named("DEP_ORV").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(eng, ColumnMetadata.named("ENG").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(engOrv, ColumnMetadata.named("ENG_ORV").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
    }

}

