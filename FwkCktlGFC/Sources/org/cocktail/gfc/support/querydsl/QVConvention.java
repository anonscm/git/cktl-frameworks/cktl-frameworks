package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVConvention is a Querydsl query type for QVConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVConvention extends com.mysema.query.sql.RelationalPathBase<QVConvention> {

    private static final long serialVersionUID = 892350694;

    public static final QVConvention vConvention = new QVConvention("V_CONVENTION");

    public final SimplePath<Object> conIndex = createSimple("conIndex", Object.class);

    public final SimplePath<Object> conObjet = createSimple("conObjet", Object.class);

    public final SimplePath<Object> conObjetCourt = createSimple("conObjetCourt", Object.class);

    public final SimplePath<Object> conReferenceExterne = createSimple("conReferenceExterne", Object.class);

    public final SimplePath<Object> conSuppr = createSimple("conSuppr", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public QVConvention(String variable) {
        super(QVConvention.class, forVariable(variable), "GFC", "V_CONVENTION");
        addMetadata();
    }

    public QVConvention(String variable, String schema, String table) {
        super(QVConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVConvention(Path<? extends QVConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_CONVENTION");
        addMetadata();
    }

    public QVConvention(PathMetadata<?> metadata) {
        super(QVConvention.class, metadata, "GFC", "V_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(conIndex, ColumnMetadata.named("CON_INDEX").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(conObjet, ColumnMetadata.named("CON_OBJET").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(conObjetCourt, ColumnMetadata.named("CON_OBJET_COURT").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(conReferenceExterne, ColumnMetadata.named("CON_REFERENCE_EXTERNE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(conSuppr, ColumnMetadata.named("CON_SUPPR").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(1).ofType(Types.OTHER).withSize(0));
    }

}

