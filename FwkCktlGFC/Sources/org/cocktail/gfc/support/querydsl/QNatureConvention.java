package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QNatureConvention is a Querydsl query type for QNatureConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QNatureConvention extends com.mysema.query.sql.RelationalPathBase<QNatureConvention> {

    private static final long serialVersionUID = -937320839;

    public static final QNatureConvention natureConvention = new QNatureConvention("NATURE_CONVENTION");

    public final StringPath natConvLibelle = createString("natConvLibelle");

    public final NumberPath<Long> natConvOrdre = createNumber("natConvOrdre", Long.class);

    public final StringPath natConvRecherche = createString("natConvRecherche");

    public final com.mysema.query.sql.PrimaryKey<QNatureConvention> natureConventionPk = createPrimaryKey(natConvOrdre);

    public QNatureConvention(String variable) {
        super(QNatureConvention.class, forVariable(variable), "GFC", "NATURE_CONVENTION");
        addMetadata();
    }

    public QNatureConvention(String variable, String schema, String table) {
        super(QNatureConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QNatureConvention(Path<? extends QNatureConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "NATURE_CONVENTION");
        addMetadata();
    }

    public QNatureConvention(PathMetadata<?> metadata) {
        super(QNatureConvention.class, metadata, "GFC", "NATURE_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(natConvLibelle, ColumnMetadata.named("NAT_CONV_LIBELLE").withIndex(1).ofType(Types.VARCHAR).withSize(70).notNull());
        addMetadata(natConvOrdre, ColumnMetadata.named("NAT_CONV_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(natConvRecherche, ColumnMetadata.named("NAT_CONV_RECHERCHE").withIndex(3).ofType(Types.VARCHAR).withSize(1));
    }

}

