package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmInventNonBudComp is a Querydsl query type for QImmInventNonBudComp
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmInventNonBudComp extends com.mysema.query.sql.RelationalPathBase<QImmInventNonBudComp> {

    private static final long serialVersionUID = -763715627;

    public static final QImmInventNonBudComp immInventNonBudComp = new QImmInventNonBudComp("IMM_INVENT_NON_BUD_COMP");

    public final NumberPath<Long> inbId = createNumber("inbId", Long.class);

    public final StringPath inbNumeroInventaire = createString("inbNumeroInventaire");

    public final com.mysema.query.sql.PrimaryKey<QImmInventNonBudComp> immInventNonBudCompPk = createPrimaryKey(inbId);

    public QImmInventNonBudComp(String variable) {
        super(QImmInventNonBudComp.class, forVariable(variable), "GFC", "IMM_INVENT_NON_BUD_COMP");
        addMetadata();
    }

    public QImmInventNonBudComp(String variable, String schema, String table) {
        super(QImmInventNonBudComp.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmInventNonBudComp(Path<? extends QImmInventNonBudComp> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_INVENT_NON_BUD_COMP");
        addMetadata();
    }

    public QImmInventNonBudComp(PathMetadata<?> metadata) {
        super(QImmInventNonBudComp.class, metadata, "GFC", "IMM_INVENT_NON_BUD_COMP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(inbId, ColumnMetadata.named("INB_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(inbNumeroInventaire, ColumnMetadata.named("INB_NUMERO_INVENTAIRE").withIndex(2).ofType(Types.VARCHAR).withSize(200));
    }

}

