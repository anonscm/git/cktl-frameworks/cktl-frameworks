package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTypeOrigineBordereau is a Querydsl query type for QTypeOrigineBordereau
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QTypeOrigineBordereau extends com.mysema.query.sql.RelationalPathBase<QTypeOrigineBordereau> {

    private static final long serialVersionUID = 1710477897;

    public static final QTypeOrigineBordereau typeOrigineBordereau = new QTypeOrigineBordereau("TYPE_ORIGINE_BORDEREAU");

    public final StringPath torLibelle = createString("torLibelle");

    public final NumberPath<Long> torOrdre = createNumber("torOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QTypeOrigineBordereau> typeOrigineBordereauPk = createPrimaryKey(torOrdre);

    public final com.mysema.query.sql.ForeignKey<QTitre> _titreTorOrdreFk = createInvForeignKey(torOrdre, "TOR_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QMandat> _mandatTorOrdreFk = createInvForeignKey(torOrdre, "TOR_ORDRE");

    public QTypeOrigineBordereau(String variable) {
        super(QTypeOrigineBordereau.class, forVariable(variable), "GFC", "TYPE_ORIGINE_BORDEREAU");
        addMetadata();
    }

    public QTypeOrigineBordereau(String variable, String schema, String table) {
        super(QTypeOrigineBordereau.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTypeOrigineBordereau(Path<? extends QTypeOrigineBordereau> path) {
        super(path.getType(), path.getMetadata(), "GFC", "TYPE_ORIGINE_BORDEREAU");
        addMetadata();
    }

    public QTypeOrigineBordereau(PathMetadata<?> metadata) {
        super(QTypeOrigineBordereau.class, metadata, "GFC", "TYPE_ORIGINE_BORDEREAU");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(torLibelle, ColumnMetadata.named("TOR_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(torOrdre, ColumnMetadata.named("TOR_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

