package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEpnCreBud1 is a Querydsl query type for QEpnCreBud1
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEpnCreBud1 extends com.mysema.query.sql.RelationalPathBase<QEpnCreBud1> {

    private static final long serialVersionUID = -637640716;

    public static final QEpnCreBud1 epnCreBud1 = new QEpnCreBud1("EPN_CRE_BUD_1");

    public final StringPath epnccb1CodBud = createString("epnccb1CodBud");

    public final StringPath epnccb1CodNomen = createString("epnccb1CodNomen");

    public final StringPath epnccb1Date = createString("epnccb1Date");

    public final NumberPath<Integer> epnccb1Exercice = createNumber("epnccb1Exercice", Integer.class);

    public final StringPath epnccb1GesCode = createString("epnccb1GesCode");

    public final NumberPath<Long> epnccb1Identifiant = createNumber("epnccb1Identifiant", Long.class);

    public final NumberPath<Integer> epnccb1Nbenreg = createNumber("epnccb1Nbenreg", Integer.class);

    public final NumberPath<Integer> epnccb1Numero = createNumber("epnccb1Numero", Integer.class);

    public final StringPath epnccb1Rang = createString("epnccb1Rang");

    public final NumberPath<Integer> epnccb1Siren = createNumber("epnccb1Siren", Integer.class);

    public final NumberPath<Long> epnccb1Siret = createNumber("epnccb1Siret", Long.class);

    public final StringPath epnccb1Type = createString("epnccb1Type");

    public final StringPath epnccb1TypeDoc = createString("epnccb1TypeDoc");

    public final StringPath epnccbOrdre = createString("epnccbOrdre");

    public QEpnCreBud1(String variable) {
        super(QEpnCreBud1.class, forVariable(variable), "GFC", "EPN_CRE_BUD_1");
        addMetadata();
    }

    public QEpnCreBud1(String variable, String schema, String table) {
        super(QEpnCreBud1.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEpnCreBud1(Path<? extends QEpnCreBud1> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EPN_CRE_BUD_1");
        addMetadata();
    }

    public QEpnCreBud1(PathMetadata<?> metadata) {
        super(QEpnCreBud1.class, metadata, "GFC", "EPN_CRE_BUD_1");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(epnccb1CodBud, ColumnMetadata.named("EPNCCB1_COD_BUD").withIndex(8).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnccb1CodNomen, ColumnMetadata.named("EPNCCB1_COD_NOMEN").withIndex(7).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnccb1Date, ColumnMetadata.named("EPNCCB1_DATE").withIndex(11).ofType(Types.VARCHAR).withSize(8));
        addMetadata(epnccb1Exercice, ColumnMetadata.named("EPNCCB1_EXERCICE").withIndex(9).ofType(Types.DECIMAL).withSize(4));
        addMetadata(epnccb1GesCode, ColumnMetadata.named("EPNCCB1_GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(epnccb1Identifiant, ColumnMetadata.named("EPNCCB1_IDENTIFIANT").withIndex(5).ofType(Types.DECIMAL).withSize(10));
        addMetadata(epnccb1Nbenreg, ColumnMetadata.named("EPNCCB1_NBENREG").withIndex(14).ofType(Types.DECIMAL).withSize(5));
        addMetadata(epnccb1Numero, ColumnMetadata.named("EPNCCB1_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(5));
        addMetadata(epnccb1Rang, ColumnMetadata.named("EPNCCB1_RANG").withIndex(10).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnccb1Siren, ColumnMetadata.named("EPNCCB1_SIREN").withIndex(12).ofType(Types.DECIMAL).withSize(9));
        addMetadata(epnccb1Siret, ColumnMetadata.named("EPNCCB1_SIRET").withIndex(13).ofType(Types.DECIMAL).withSize(14));
        addMetadata(epnccb1Type, ColumnMetadata.named("EPNCCB1_TYPE").withIndex(3).ofType(Types.VARCHAR).withSize(1));
        addMetadata(epnccb1TypeDoc, ColumnMetadata.named("EPNCCB1_TYPE_DOC").withIndex(6).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epnccbOrdre, ColumnMetadata.named("EPNCCB_ORDRE").withIndex(1).ofType(Types.VARCHAR).withSize(2));
    }

}

