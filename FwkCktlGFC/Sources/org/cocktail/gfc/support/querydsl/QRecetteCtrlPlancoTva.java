package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRecetteCtrlPlancoTva is a Querydsl query type for QRecetteCtrlPlancoTva
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRecetteCtrlPlancoTva extends com.mysema.query.sql.RelationalPathBase<QRecetteCtrlPlancoTva> {

    private static final long serialVersionUID = -1915084080;

    public static final QRecetteCtrlPlancoTva recetteCtrlPlancoTva = new QRecetteCtrlPlancoTva("RECETTE_CTRL_PLANCO_TVA");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> rpcoId = createNumber("rpcoId", Long.class);

    public final DateTimePath<java.sql.Timestamp> rpcotvaDateSaisie = createDateTime("rpcotvaDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<Long> rpcotvaId = createNumber("rpcotvaId", Long.class);

    public final NumberPath<java.math.BigDecimal> rpcotvaTvaSaisie = createNumber("rpcotvaTvaSaisie", java.math.BigDecimal.class);

    public final com.mysema.query.sql.PrimaryKey<QRecetteCtrlPlancoTva> recetteCtrlPlancoTvaPk = createPrimaryKey(rpcotvaId);

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> rpcotvaPcoNumFk = createForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlPlanco> rpcotvaRpcoIdFk = createForeignKey(rpcoId, "RPCO_ID");

    public final com.mysema.query.sql.ForeignKey<QGestion> rpcotvaGesCodeFk = createForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> rpcotvaExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QRecetteCtrlPlancoTva(String variable) {
        super(QRecetteCtrlPlancoTva.class, forVariable(variable), "GFC", "RECETTE_CTRL_PLANCO_TVA");
        addMetadata();
    }

    public QRecetteCtrlPlancoTva(String variable, String schema, String table) {
        super(QRecetteCtrlPlancoTva.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRecetteCtrlPlancoTva(Path<? extends QRecetteCtrlPlancoTva> path) {
        super(path.getType(), path.getMetadata(), "GFC", "RECETTE_CTRL_PLANCO_TVA");
        addMetadata();
    }

    public QRecetteCtrlPlancoTva(PathMetadata<?> metadata) {
        super(QRecetteCtrlPlancoTva.class, metadata, "GFC", "RECETTE_CTRL_PLANCO_TVA");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(7).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(rpcoId, ColumnMetadata.named("RPCO_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(rpcotvaDateSaisie, ColumnMetadata.named("RPCOTVA_DATE_SAISIE").withIndex(6).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(rpcotvaId, ColumnMetadata.named("RPCOTVA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(rpcotvaTvaSaisie, ColumnMetadata.named("RPCOTVA_TVA_SAISIE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
    }

}

