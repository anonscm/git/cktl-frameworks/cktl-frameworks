package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZDepenseCtrlMarche is a Querydsl query type for QZDepenseCtrlMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZDepenseCtrlMarche extends com.mysema.query.sql.RelationalPathBase<QZDepenseCtrlMarche> {

    private static final long serialVersionUID = 1417068318;

    public static final QZDepenseCtrlMarche zDepenseCtrlMarche = new QZDepenseCtrlMarche("Z_DEPENSE_CTRL_MARCHE");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<java.math.BigDecimal> dmarHtSaisie = createNumber("dmarHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dmarId = createNumber("dmarId", Long.class);

    public final NumberPath<java.math.BigDecimal> dmarMontantBudgetaire = createNumber("dmarMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dmarTtcSaisie = createNumber("dmarTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dmarTvaSaisie = createNumber("dmarTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> zdepId = createNumber("zdepId", Long.class);

    public final NumberPath<Long> zdmarId = createNumber("zdmarId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZDepenseCtrlMarche> sysC0076999 = createPrimaryKey(zdmarId);

    public final com.mysema.query.sql.ForeignKey<QZDepenseBudget> zDepenseCtrlMarcZdepIdFk = createForeignKey(zdepId, "ZDEP_ID");

    public QZDepenseCtrlMarche(String variable) {
        super(QZDepenseCtrlMarche.class, forVariable(variable), "GFC", "Z_DEPENSE_CTRL_MARCHE");
        addMetadata();
    }

    public QZDepenseCtrlMarche(String variable, String schema, String table) {
        super(QZDepenseCtrlMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZDepenseCtrlMarche(Path<? extends QZDepenseCtrlMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_DEPENSE_CTRL_MARCHE");
        addMetadata();
    }

    public QZDepenseCtrlMarche(PathMetadata<?> metadata) {
        super(QZDepenseCtrlMarche.class, metadata, "GFC", "Z_DEPENSE_CTRL_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dmarHtSaisie, ColumnMetadata.named("DMAR_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dmarId, ColumnMetadata.named("DMAR_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dmarMontantBudgetaire, ColumnMetadata.named("DMAR_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dmarTtcSaisie, ColumnMetadata.named("DMAR_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dmarTvaSaisie, ColumnMetadata.named("DMAR_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(zdepId, ColumnMetadata.named("ZDEP_ID").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zdmarId, ColumnMetadata.named("ZDMAR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

