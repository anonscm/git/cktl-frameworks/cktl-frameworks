package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPdepenseCtrlConvention is a Querydsl query type for QPdepenseCtrlConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPdepenseCtrlConvention extends com.mysema.query.sql.RelationalPathBase<QPdepenseCtrlConvention> {

    private static final long serialVersionUID = -627876873;

    public static final QPdepenseCtrlConvention pdepenseCtrlConvention = new QPdepenseCtrlConvention("PDEPENSE_CTRL_CONVENTION");

    public final NumberPath<Long> convOrdre = createNumber("convOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<java.math.BigDecimal> pdconHtSaisie = createNumber("pdconHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> pdconId = createNumber("pdconId", Long.class);

    public final NumberPath<java.math.BigDecimal> pdconMontantBudgetaire = createNumber("pdconMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdconTtcSaisie = createNumber("pdconTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdconTvaSaisie = createNumber("pdconTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> pdepId = createNumber("pdepId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QPdepenseCtrlConvention> pdepenseCtrlConventionPk = createPrimaryKey(pdconId);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> pdepenseCtrlConExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseBudget> pdepenseCtrlConPdepIdFk = createForeignKey(pdepId, "PDEP_ID");

    public QPdepenseCtrlConvention(String variable) {
        super(QPdepenseCtrlConvention.class, forVariable(variable), "GFC", "PDEPENSE_CTRL_CONVENTION");
        addMetadata();
    }

    public QPdepenseCtrlConvention(String variable, String schema, String table) {
        super(QPdepenseCtrlConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPdepenseCtrlConvention(Path<? extends QPdepenseCtrlConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PDEPENSE_CTRL_CONVENTION");
        addMetadata();
    }

    public QPdepenseCtrlConvention(PathMetadata<?> metadata) {
        super(QPdepenseCtrlConvention.class, metadata, "GFC", "PDEPENSE_CTRL_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(pdconHtSaisie, ColumnMetadata.named("PDCON_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdconId, ColumnMetadata.named("PDCON_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdconMontantBudgetaire, ColumnMetadata.named("PDCON_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdconTtcSaisie, ColumnMetadata.named("PDCON_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdconTvaSaisie, ColumnMetadata.named("PDCON_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdepId, ColumnMetadata.named("PDEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

