package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QReimputationPlanco is a Querydsl query type for QReimputationPlanco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QReimputationPlanco extends com.mysema.query.sql.RelationalPathBase<QReimputationPlanco> {

    private static final long serialVersionUID = -1749613741;

    public static final QReimputationPlanco reimputationPlanco = new QReimputationPlanco("REIMPUTATION_PLANCO");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final NumberPath<java.math.BigDecimal> repcHtSaisie = createNumber("repcHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> repcId = createNumber("repcId", Long.class);

    public final NumberPath<java.math.BigDecimal> repcMontantBudgetaire = createNumber("repcMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> repcTtcSaisie = createNumber("repcTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> repcTvaSaisie = createNumber("repcTvaSaisie", java.math.BigDecimal.class);

    public final com.mysema.query.sql.PrimaryKey<QReimputationPlanco> reimputationPlancoPk = createPrimaryKey(repcId);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> reimpPlancoExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> reimpPlancoPcoNumFk = createForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QReimputation> reimpPlancoReimIdFk = createForeignKey(reimId, "REIM_ID");

    public QReimputationPlanco(String variable) {
        super(QReimputationPlanco.class, forVariable(variable), "GFC", "REIMPUTATION_PLANCO");
        addMetadata();
    }

    public QReimputationPlanco(String variable, String schema, String table) {
        super(QReimputationPlanco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QReimputationPlanco(Path<? extends QReimputationPlanco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REIMPUTATION_PLANCO");
        addMetadata();
    }

    public QReimputationPlanco(PathMetadata<?> metadata) {
        super(QReimputationPlanco.class, metadata, "GFC", "REIMPUTATION_PLANCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(repcHtSaisie, ColumnMetadata.named("REPC_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(repcId, ColumnMetadata.named("REPC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(repcMontantBudgetaire, ColumnMetadata.named("REPC_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(repcTtcSaisie, ColumnMetadata.named("REPC_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(repcTvaSaisie, ColumnMetadata.named("REPC_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
    }

}

