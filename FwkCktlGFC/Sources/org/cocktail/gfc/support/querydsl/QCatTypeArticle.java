package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCatTypeArticle is a Querydsl query type for QCatTypeArticle
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCatTypeArticle extends com.mysema.query.sql.RelationalPathBase<QCatTypeArticle> {

    private static final long serialVersionUID = -455917081;

    public static final QCatTypeArticle catTypeArticle = new QCatTypeArticle("CAT_TYPE_ARTICLE");

    public final NumberPath<Long> tyarId = createNumber("tyarId", Long.class);

    public final StringPath tyarLibelle = createString("tyarLibelle");

    public final com.mysema.query.sql.PrimaryKey<QCatTypeArticle> catTypeArticlePk = createPrimaryKey(tyarId);

    public final com.mysema.query.sql.ForeignKey<QCatArticle> _catArticleTyarIdFk = createInvForeignKey(tyarId, "TYAR_ID");

    public final com.mysema.query.sql.ForeignKey<QFacturePapierLigne> _fligTyarIdFk = createInvForeignKey(tyarId, "TYAR_ID");

    public final com.mysema.query.sql.ForeignKey<QPrestationLigne> _prligTyarIdFk = createInvForeignKey(tyarId, "TYAR_ID");

    public QCatTypeArticle(String variable) {
        super(QCatTypeArticle.class, forVariable(variable), "GFC", "CAT_TYPE_ARTICLE");
        addMetadata();
    }

    public QCatTypeArticle(String variable, String schema, String table) {
        super(QCatTypeArticle.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCatTypeArticle(Path<? extends QCatTypeArticle> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CAT_TYPE_ARTICLE");
        addMetadata();
    }

    public QCatTypeArticle(PathMetadata<?> metadata) {
        super(QCatTypeArticle.class, metadata, "GFC", "CAT_TYPE_ARTICLE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tyarId, ColumnMetadata.named("TYAR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyarLibelle, ColumnMetadata.named("TYAR_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(500).notNull());
    }

}

