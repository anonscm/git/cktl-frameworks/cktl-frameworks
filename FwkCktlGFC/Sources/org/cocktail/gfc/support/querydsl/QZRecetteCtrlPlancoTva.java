package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZRecetteCtrlPlancoTva is a Querydsl query type for QZRecetteCtrlPlancoTva
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZRecetteCtrlPlancoTva extends com.mysema.query.sql.RelationalPathBase<QZRecetteCtrlPlancoTva> {

    private static final long serialVersionUID = 1008163208;

    public static final QZRecetteCtrlPlancoTva zRecetteCtrlPlancoTva = new QZRecetteCtrlPlancoTva("Z_RECETTE_CTRL_PLANCO_TVA");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> rpcoId = createNumber("rpcoId", Long.class);

    public final DateTimePath<java.sql.Timestamp> rpcotvaDateSaisie = createDateTime("rpcotvaDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<Long> rpcotvaId = createNumber("rpcotvaId", Long.class);

    public final NumberPath<java.math.BigDecimal> rpcotvaTvaSaisie = createNumber("rpcotvaTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> zrpcoId = createNumber("zrpcoId", Long.class);

    public final NumberPath<Long> zrpcotvaId = createNumber("zrpcotvaId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZRecetteCtrlPlancoTva> zRecetteCtrlPlancoTvaPk = createPrimaryKey(zrpcotvaId);

    public final com.mysema.query.sql.ForeignKey<QZRecetteCtrlPlanco> zrpcotvaZrpcoIdFk = createForeignKey(zrpcoId, "ZRPCO_ID");

    public QZRecetteCtrlPlancoTva(String variable) {
        super(QZRecetteCtrlPlancoTva.class, forVariable(variable), "GFC", "Z_RECETTE_CTRL_PLANCO_TVA");
        addMetadata();
    }

    public QZRecetteCtrlPlancoTva(String variable, String schema, String table) {
        super(QZRecetteCtrlPlancoTva.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZRecetteCtrlPlancoTva(Path<? extends QZRecetteCtrlPlancoTva> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_RECETTE_CTRL_PLANCO_TVA");
        addMetadata();
    }

    public QZRecetteCtrlPlancoTva(PathMetadata<?> metadata) {
        super(QZRecetteCtrlPlancoTva.class, metadata, "GFC", "Z_RECETTE_CTRL_PLANCO_TVA");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(8).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(5).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(rpcoId, ColumnMetadata.named("RPCO_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(rpcotvaDateSaisie, ColumnMetadata.named("RPCOTVA_DATE_SAISIE").withIndex(7).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(rpcotvaId, ColumnMetadata.named("RPCOTVA_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(rpcotvaTvaSaisie, ColumnMetadata.named("RPCOTVA_TVA_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(zrpcoId, ColumnMetadata.named("ZRPCO_ID").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zrpcotvaId, ColumnMetadata.named("ZRPCOTVA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

