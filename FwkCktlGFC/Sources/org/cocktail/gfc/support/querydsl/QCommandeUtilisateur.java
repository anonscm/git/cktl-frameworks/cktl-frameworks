package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeUtilisateur is a Querydsl query type for QCommandeUtilisateur
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeUtilisateur extends com.mysema.query.sql.RelationalPathBase<QCommandeUtilisateur> {

    private static final long serialVersionUID = 1824269832;

    public static final QCommandeUtilisateur commandeUtilisateur = new QCommandeUtilisateur("COMMANDE_UTILISATEUR");

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final NumberPath<Long> comuId = createNumber("comuId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCommandeUtilisateur> commandeUtilisateurPk = createPrimaryKey(comuId);

    public final com.mysema.query.sql.ForeignKey<QCommande> commandeUtilCommidFk = createForeignKey(commId, "COMM_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> commandeUtilUtlordreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public QCommandeUtilisateur(String variable) {
        super(QCommandeUtilisateur.class, forVariable(variable), "GFC", "COMMANDE_UTILISATEUR");
        addMetadata();
    }

    public QCommandeUtilisateur(String variable, String schema, String table) {
        super(QCommandeUtilisateur.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeUtilisateur(Path<? extends QCommandeUtilisateur> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_UTILISATEUR");
        addMetadata();
    }

    public QCommandeUtilisateur(PathMetadata<?> metadata) {
        super(QCommandeUtilisateur.class, metadata, "GFC", "COMMANDE_UTILISATEUR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(comuId, ColumnMetadata.named("COMU_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

