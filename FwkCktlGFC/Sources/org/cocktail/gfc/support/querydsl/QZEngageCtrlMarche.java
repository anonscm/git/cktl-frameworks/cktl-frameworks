package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QZEngageCtrlMarche is a Querydsl query type for QZEngageCtrlMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QZEngageCtrlMarche extends com.mysema.query.sql.RelationalPathBase<QZEngageCtrlMarche> {

    private static final long serialVersionUID = -129450809;

    public static final QZEngageCtrlMarche zEngageCtrlMarche = new QZEngageCtrlMarche("Z_ENGAGE_CTRL_MARCHE");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> emarDateSaisie = createDateTime("emarDateSaisie", java.sql.Timestamp.class);

    public final NumberPath<java.math.BigDecimal> emarHtReste = createNumber("emarHtReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> emarHtSaisie = createNumber("emarHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> emarId = createNumber("emarId", Long.class);

    public final NumberPath<java.math.BigDecimal> emarMontantBudgetaire = createNumber("emarMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> emarMontantBudgetaireReste = createNumber("emarMontantBudgetaireReste", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> emarTtcSaisie = createNumber("emarTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> emarTvaSaisie = createNumber("emarTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> zemarId = createNumber("zemarId", Long.class);

    public final NumberPath<Long> zengId = createNumber("zengId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QZEngageCtrlMarche> sysC0077406 = createPrimaryKey(zemarId);

    public final com.mysema.query.sql.ForeignKey<QZEngageBudget> zEngageCtrlMarcZengIdFk = createForeignKey(zengId, "ZENG_ID");

    public QZEngageCtrlMarche(String variable) {
        super(QZEngageCtrlMarche.class, forVariable(variable), "GFC", "Z_ENGAGE_CTRL_MARCHE");
        addMetadata();
    }

    public QZEngageCtrlMarche(String variable, String schema, String table) {
        super(QZEngageCtrlMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QZEngageCtrlMarche(Path<? extends QZEngageCtrlMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "Z_ENGAGE_CTRL_MARCHE");
        addMetadata();
    }

    public QZEngageCtrlMarche(PathMetadata<?> metadata) {
        super(QZEngageCtrlMarche.class, metadata, "GFC", "Z_ENGAGE_CTRL_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(emarDateSaisie, ColumnMetadata.named("EMAR_DATE_SAISIE").withIndex(12).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(emarHtReste, ColumnMetadata.named("EMAR_HT_RESTE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(emarHtSaisie, ColumnMetadata.named("EMAR_HT_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(emarId, ColumnMetadata.named("EMAR_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(emarMontantBudgetaire, ColumnMetadata.named("EMAR_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(emarMontantBudgetaireReste, ColumnMetadata.named("EMAR_MONTANT_BUDGETAIRE_RESTE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(emarTtcSaisie, ColumnMetadata.named("EMAR_TTC_SAISIE").withIndex(11).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(emarTvaSaisie, ColumnMetadata.named("EMAR_TVA_SAISIE").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(zemarId, ColumnMetadata.named("ZEMAR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(zengId, ColumnMetadata.named("ZENG_ID").withIndex(13).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

