package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmTypeApplication is a Querydsl query type for QAdmTypeApplication
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmTypeApplication extends com.mysema.query.sql.RelationalPathBase<QAdmTypeApplication> {

    private static final long serialVersionUID = 1306877549;

    public static final QAdmTypeApplication admTypeApplication = new QAdmTypeApplication("ADM_TYPE_APPLICATION");

    public final NumberPath<Long> domId = createNumber("domId", Long.class);

    public final NumberPath<Long> tyapId = createNumber("tyapId", Long.class);

    public final StringPath tyapLibelle = createString("tyapLibelle");

    public final StringPath tyapStrid = createString("tyapStrid");

    public final com.mysema.query.sql.PrimaryKey<QAdmTypeApplication> sysC0075643 = createPrimaryKey(tyapId);

    public final com.mysema.query.sql.ForeignKey<QAdmDomaine> admTyapDomIdFk = createForeignKey(domId, "DOM_ID");

    public final com.mysema.query.sql.ForeignKey<QTypePublic> _typuTyapIdFk = createInvForeignKey(tyapId, "TYAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeApplicationPrc> _admTyapTyapIdFk = createInvForeignKey(tyapId, "TYAP_ID");

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> _engageBudgetTyapIdFk = createInvForeignKey(tyapId, "TYAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypapVersion> _admTyavTyapFk = createInvForeignKey(tyapId, "TYAP_ID");

    public final com.mysema.query.sql.ForeignKey<QFacture> _factureTyapIdFk = createInvForeignKey(tyapId, "TYAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmPreference> _admPreferenceTyapIdFk = createInvForeignKey(tyapId, "TYAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmFonction> _admFonctionTyapIdFk = createInvForeignKey(tyapId, "TYAP_ID");

    public final com.mysema.query.sql.ForeignKey<QCatCatalogue> _catCatalogueTyapIdFk = createInvForeignKey(tyapId, "TYAP_ID");

    public QAdmTypeApplication(String variable) {
        super(QAdmTypeApplication.class, forVariable(variable), "GFC", "ADM_TYPE_APPLICATION");
        addMetadata();
    }

    public QAdmTypeApplication(String variable, String schema, String table) {
        super(QAdmTypeApplication.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmTypeApplication(Path<? extends QAdmTypeApplication> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_TYPE_APPLICATION");
        addMetadata();
    }

    public QAdmTypeApplication(PathMetadata<?> metadata) {
        super(QAdmTypeApplication.class, metadata, "GFC", "ADM_TYPE_APPLICATION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(domId, ColumnMetadata.named("DOM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyapId, ColumnMetadata.named("TYAP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(12).notNull());
        addMetadata(tyapLibelle, ColumnMetadata.named("TYAP_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(tyapStrid, ColumnMetadata.named("TYAP_STRID").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
    }

}

