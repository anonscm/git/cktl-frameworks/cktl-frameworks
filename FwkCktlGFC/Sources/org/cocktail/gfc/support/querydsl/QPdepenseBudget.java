package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPdepenseBudget is a Querydsl query type for QPdepenseBudget
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPdepenseBudget extends com.mysema.query.sql.RelationalPathBase<QPdepenseBudget> {

    private static final long serialVersionUID = -1932999168;

    public static final QPdepenseBudget pdepenseBudget = new QPdepenseBudget("PDEPENSE_BUDGET");

    public final NumberPath<Long> dppId = createNumber("dppId", Long.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<java.math.BigDecimal> pdepHtSaisie = createNumber("pdepHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> pdepId = createNumber("pdepId", Long.class);

    public final NumberPath<java.math.BigDecimal> pdepMontantBudgetaire = createNumber("pdepMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdepTtcSaisie = createNumber("pdepTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdepTvaSaisie = createNumber("pdepTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QPdepenseBudget> pdepenseBudgetPk = createPrimaryKey(pdepId);

    public final com.mysema.query.sql.ForeignKey<QDepensePapier> pdepenseBudgetDppIdFk = createForeignKey(dppId, "DPP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTauxProrata> pdepenseBudgetTapIdFk = createForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> pdepenseBudgetExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> pdepenseBudgetUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> pdepenseBudgetEngIdFk = createForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlHorsMarche> _pdepenseCtrlHmPdepIdFk = createInvForeignKey(pdepId, "PDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlPlanco> _pdepenseCtrlPlanPdepIdFk = createInvForeignKey(pdepId, "PDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlAnalytique> _pdepenseCtrlAnalPdepIdFk = createInvForeignKey(pdepId, "PDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlAction> _pdepenseCtrlActPdepIdFk = createInvForeignKey(pdepId, "PDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlMarche> _pdepenseCtrlMarPdepIdFk = createInvForeignKey(pdepId, "PDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlConvention> _pdepenseCtrlConPdepIdFk = createInvForeignKey(pdepId, "PDEP_ID");

    public QPdepenseBudget(String variable) {
        super(QPdepenseBudget.class, forVariable(variable), "GFC", "PDEPENSE_BUDGET");
        addMetadata();
    }

    public QPdepenseBudget(String variable, String schema, String table) {
        super(QPdepenseBudget.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPdepenseBudget(Path<? extends QPdepenseBudget> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PDEPENSE_BUDGET");
        addMetadata();
    }

    public QPdepenseBudget(PathMetadata<?> metadata) {
        super(QPdepenseBudget.class, metadata, "GFC", "PDEPENSE_BUDGET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(pdepHtSaisie, ColumnMetadata.named("PDEP_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdepId, ColumnMetadata.named("PDEP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdepMontantBudgetaire, ColumnMetadata.named("PDEP_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdepTtcSaisie, ColumnMetadata.named("PDEP_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdepTvaSaisie, ColumnMetadata.named("PDEP_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(9).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

