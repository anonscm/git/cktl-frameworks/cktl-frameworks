package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVTitreBordereau is a Querydsl query type for QImmVTitreBordereau
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVTitreBordereau extends com.mysema.query.sql.RelationalPathBase<QImmVTitreBordereau> {

    private static final long serialVersionUID = -1226591607;

    public static final QImmVTitreBordereau immVTitreBordereau = new QImmVTitreBordereau("IMM_V_TITRE_BORDEREAU");

    public final NumberPath<Long> borId = createNumber("borId", Long.class);

    public final NumberPath<Long> borNum = createNumber("borNum", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> orgOrdre = createNumber("orgOrdre", Long.class);

    public final NumberPath<Long> titId = createNumber("titId", Long.class);

    public final NumberPath<Long> titNumero = createNumber("titNumero", Long.class);

    public QImmVTitreBordereau(String variable) {
        super(QImmVTitreBordereau.class, forVariable(variable), "GFC", "IMM_V_TITRE_BORDEREAU");
        addMetadata();
    }

    public QImmVTitreBordereau(String variable, String schema, String table) {
        super(QImmVTitreBordereau.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVTitreBordereau(Path<? extends QImmVTitreBordereau> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_TITRE_BORDEREAU");
        addMetadata();
    }

    public QImmVTitreBordereau(PathMetadata<?> metadata) {
        super(QImmVTitreBordereau.class, metadata, "GFC", "IMM_V_TITRE_BORDEREAU");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(borId, ColumnMetadata.named("BOR_ID").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(borNum, ColumnMetadata.named("BOR_NUM").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(5).ofType(Types.VARCHAR).withSize(112));
        addMetadata(orgOrdre, ColumnMetadata.named("ORG_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(38));
        addMetadata(titId, ColumnMetadata.named("TIT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(titNumero, ColumnMetadata.named("TIT_NUMERO").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

