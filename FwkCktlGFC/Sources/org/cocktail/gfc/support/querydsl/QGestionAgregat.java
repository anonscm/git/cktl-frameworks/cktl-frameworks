package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QGestionAgregat is a Querydsl query type for QGestionAgregat
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QGestionAgregat extends com.mysema.query.sql.RelationalPathBase<QGestionAgregat> {

    private static final long serialVersionUID = 1986463353;

    public static final QGestionAgregat gestionAgregat = new QGestionAgregat("GESTION_AGREGAT");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gaDescription = createString("gaDescription");

    public final NumberPath<Long> gaId = createNumber("gaId", Long.class);

    public final StringPath gaLibelle = createString("gaLibelle");

    public final com.mysema.query.sql.PrimaryKey<QGestionAgregat> gestionAgregatPk = createPrimaryKey(gaId);

    public final com.mysema.query.sql.ForeignKey<QGestionAgregatRepart> _garGaIdFk = createInvForeignKey(gaId, "GA_ID");

    public QGestionAgregat(String variable) {
        super(QGestionAgregat.class, forVariable(variable), "GFC", "GESTION_AGREGAT");
        addMetadata();
    }

    public QGestionAgregat(String variable, String schema, String table) {
        super(QGestionAgregat.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QGestionAgregat(Path<? extends QGestionAgregat> path) {
        super(path.getType(), path.getMetadata(), "GFC", "GESTION_AGREGAT");
        addMetadata();
    }

    public QGestionAgregat(PathMetadata<?> metadata) {
        super(QGestionAgregat.class, metadata, "GFC", "GESTION_AGREGAT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(gaDescription, ColumnMetadata.named("GA_DESCRIPTION").withIndex(4).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(gaId, ColumnMetadata.named("GA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(gaLibelle, ColumnMetadata.named("GA_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(200).notNull());
    }

}

