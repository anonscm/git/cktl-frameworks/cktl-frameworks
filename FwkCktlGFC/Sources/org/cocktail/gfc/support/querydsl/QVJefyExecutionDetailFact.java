package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVJefyExecutionDetailFact is a Querydsl query type for QVJefyExecutionDetailFact
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVJefyExecutionDetailFact extends com.mysema.query.sql.RelationalPathBase<QVJefyExecutionDetailFact> {

    private static final long serialVersionUID = 210710034;

    public static final QVJefyExecutionDetailFact vJefyExecutionDetailFact = new QVJefyExecutionDetailFact("V_JEFY_EXECUTION_DETAIL_FACT");

    public final NumberPath<Long> aeeExecution = createNumber("aeeExecution", Long.class);

    public final NumberPath<java.math.BigDecimal> aeeLiqHt = createNumber("aeeLiqHt", java.math.BigDecimal.class);

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> cdeOrdre = createNumber("cdeOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> dateExec = createDateTime("dateExec", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dateLiqu = createDateTime("dateLiqu", java.sql.Timestamp.class);

    public final NumberPath<Long> engNumero = createNumber("engNumero", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath libelle = createString("libelle");

    public final StringPath orgUb = createString("orgUb");

    public QVJefyExecutionDetailFact(String variable) {
        super(QVJefyExecutionDetailFact.class, forVariable(variable), "GFC", "V_JEFY_EXECUTION_DETAIL_FACT");
        addMetadata();
    }

    public QVJefyExecutionDetailFact(String variable, String schema, String table) {
        super(QVJefyExecutionDetailFact.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVJefyExecutionDetailFact(Path<? extends QVJefyExecutionDetailFact> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_JEFY_EXECUTION_DETAIL_FACT");
        addMetadata();
    }

    public QVJefyExecutionDetailFact(PathMetadata<?> metadata) {
        super(QVJefyExecutionDetailFact.class, metadata, "GFC", "V_JEFY_EXECUTION_DETAIL_FACT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(aeeExecution, ColumnMetadata.named("AEE_EXECUTION").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(aeeLiqHt, ColumnMetadata.named("AEE_LIQ_HT").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(cdeOrdre, ColumnMetadata.named("CDE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dateExec, ColumnMetadata.named("DATE_EXEC").withIndex(8).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dateLiqu, ColumnMetadata.named("DATE_LIQU").withIndex(7).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(engNumero, ColumnMetadata.named("ENG_NUMERO").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4));
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(500));
        addMetadata(orgUb, ColumnMetadata.named("ORG_UB").withIndex(10).ofType(Types.VARCHAR).withSize(10));
    }

}

