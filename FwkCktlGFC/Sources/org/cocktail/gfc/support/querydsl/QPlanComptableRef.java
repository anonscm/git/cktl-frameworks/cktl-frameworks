package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPlanComptableRef is a Querydsl query type for QPlanComptableRef
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPlanComptableRef extends com.mysema.query.sql.RelationalPathBase<QPlanComptableRef> {

    private static final long serialVersionUID = 1097642686;

    public static final QPlanComptableRef planComptableRef = new QPlanComptableRef("PLAN_COMPTABLE_REF");

    public final StringPath refPcoLibelle = createString("refPcoLibelle");

    public final StringPath refPcoNum = createString("refPcoNum");

    public final com.mysema.query.sql.PrimaryKey<QPlanComptableRef> planComptableRefPk = createPrimaryKey(refPcoNum);

    public QPlanComptableRef(String variable) {
        super(QPlanComptableRef.class, forVariable(variable), "GFC", "PLAN_COMPTABLE_REF");
        addMetadata();
    }

    public QPlanComptableRef(String variable, String schema, String table) {
        super(QPlanComptableRef.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPlanComptableRef(Path<? extends QPlanComptableRef> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PLAN_COMPTABLE_REF");
        addMetadata();
    }

    public QPlanComptableRef(PathMetadata<?> metadata) {
        super(QPlanComptableRef.class, metadata, "GFC", "PLAN_COMPTABLE_REF");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(refPcoLibelle, ColumnMetadata.named("REF_PCO_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(500));
        addMetadata(refPcoNum, ColumnMetadata.named("REF_PCO_NUM").withIndex(1).ofType(Types.VARCHAR).withSize(30).notNull());
    }

}

