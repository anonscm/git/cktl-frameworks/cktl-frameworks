package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAvenantEvenement is a Querydsl query type for QAvenantEvenement
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAvenantEvenement extends com.mysema.query.sql.RelationalPathBase<QAvenantEvenement> {

    private static final long serialVersionUID = -472853023;

    public static final QAvenantEvenement avenantEvenement = new QAvenantEvenement("AVENANT_EVENEMENT");

    public final NumberPath<Long> aeOrdre = createNumber("aeOrdre", Long.class);

    public final NumberPath<Long> avtOrdre = createNumber("avtOrdre", Long.class);

    public final NumberPath<Long> evtOrdre = createNumber("evtOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAvenantEvenement> avenantEvenementPk = createPrimaryKey(aeOrdre);

    public QAvenantEvenement(String variable) {
        super(QAvenantEvenement.class, forVariable(variable), "GFC", "AVENANT_EVENEMENT");
        addMetadata();
    }

    public QAvenantEvenement(String variable, String schema, String table) {
        super(QAvenantEvenement.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAvenantEvenement(Path<? extends QAvenantEvenement> path) {
        super(path.getType(), path.getMetadata(), "GFC", "AVENANT_EVENEMENT");
        addMetadata();
    }

    public QAvenantEvenement(PathMetadata<?> metadata) {
        super(QAvenantEvenement.class, metadata, "GFC", "AVENANT_EVENEMENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(aeOrdre, ColumnMetadata.named("AE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(avtOrdre, ColumnMetadata.named("AVT_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(evtOrdre, ColumnMetadata.named("EVT_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(38).notNull());
    }

}

