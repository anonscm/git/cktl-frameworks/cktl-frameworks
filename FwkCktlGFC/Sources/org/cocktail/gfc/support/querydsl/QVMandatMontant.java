package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVMandatMontant is a Querydsl query type for QVMandatMontant
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVMandatMontant extends com.mysema.query.sql.RelationalPathBase<QVMandatMontant> {

    private static final long serialVersionUID = 1688017325;

    public static final QVMandatMontant vMandatMontant = new QVMandatMontant("V_MANDAT_MONTANT");

    public final NumberPath<Long> htSaisie = createNumber("htSaisie", Long.class);

    public final NumberPath<Long> manId = createNumber("manId", Long.class);

    public final NumberPath<Long> montantBudgetaire = createNumber("montantBudgetaire", Long.class);

    public final NumberPath<Long> ttcSaisie = createNumber("ttcSaisie", Long.class);

    public final NumberPath<Long> tvaSaisie = createNumber("tvaSaisie", Long.class);

    public QVMandatMontant(String variable) {
        super(QVMandatMontant.class, forVariable(variable), "GFC", "V_MANDAT_MONTANT");
        addMetadata();
    }

    public QVMandatMontant(String variable, String schema, String table) {
        super(QVMandatMontant.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVMandatMontant(Path<? extends QVMandatMontant> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_MANDAT_MONTANT");
        addMetadata();
    }

    public QVMandatMontant(PathMetadata<?> metadata) {
        super(QVMandatMontant.class, metadata, "GFC", "V_MANDAT_MONTANT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(htSaisie, ColumnMetadata.named("HT_SAISIE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(manId, ColumnMetadata.named("MAN_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(montantBudgetaire, ColumnMetadata.named("MONTANT_BUDGETAIRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ttcSaisie, ColumnMetadata.named("TTC_SAISIE").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tvaSaisie, ColumnMetadata.named("TVA_SAISIE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
    }

}

