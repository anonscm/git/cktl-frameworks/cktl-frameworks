package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QXlabMarche is a Querydsl query type for QXlabMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QXlabMarche extends com.mysema.query.sql.RelationalPathBase<QXlabMarche> {

    private static final long serialVersionUID = -1936994120;

    public static final QXlabMarche xlabMarche = new QXlabMarche("XLAB_MARCHE");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> xfouOrdre = createNumber("xfouOrdre", Long.class);

    public final StringPath xmarCode = createString("xmarCode");

    public final NumberPath<Long> xmarId = createNumber("xmarId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QXlabMarche> xlabMarchePk = createPrimaryKey(xmarId);

    public final com.mysema.query.sql.ForeignKey<QAttribution> xlabMarcheAttOrdreFk = createForeignKey(attOrdre, "ATT_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QXlabFournisseur> xlabMarcheXfouOrdreFk = createForeignKey(xfouOrdre, "XFOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QXlabCommande> _xlabCommandeXmarIdFk = createInvForeignKey(xmarId, "XMAR_ID");

    public QXlabMarche(String variable) {
        super(QXlabMarche.class, forVariable(variable), "GFC", "XLAB_MARCHE");
        addMetadata();
    }

    public QXlabMarche(String variable, String schema, String table) {
        super(QXlabMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QXlabMarche(Path<? extends QXlabMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "XLAB_MARCHE");
        addMetadata();
    }

    public QXlabMarche(PathMetadata<?> metadata) {
        super(QXlabMarche.class, metadata, "GFC", "XLAB_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(xfouOrdre, ColumnMetadata.named("XFOU_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(xmarCode, ColumnMetadata.named("XMAR_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(7).notNull());
        addMetadata(xmarId, ColumnMetadata.named("XMAR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

