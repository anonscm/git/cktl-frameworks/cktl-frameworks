package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTypeBrouillard is a Querydsl query type for QTypeBrouillard
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QTypeBrouillard extends com.mysema.query.sql.RelationalPathBase<QTypeBrouillard> {

    private static final long serialVersionUID = 1875239387;

    public static final QTypeBrouillard typeBrouillard = new QTypeBrouillard("TYPE_BROUILLARD");

    public final StringPath tbrLibelle = createString("tbrLibelle");

    public final NumberPath<Long> tbrOrdre = createNumber("tbrOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QTypeBrouillard> typeBrouillardPk = createPrimaryKey(tbrOrdre);

    public QTypeBrouillard(String variable) {
        super(QTypeBrouillard.class, forVariable(variable), "GFC", "TYPE_BROUILLARD");
        addMetadata();
    }

    public QTypeBrouillard(String variable, String schema, String table) {
        super(QTypeBrouillard.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTypeBrouillard(Path<? extends QTypeBrouillard> path) {
        super(path.getType(), path.getMetadata(), "GFC", "TYPE_BROUILLARD");
        addMetadata();
    }

    public QTypeBrouillard(PathMetadata<?> metadata) {
        super(QTypeBrouillard.class, metadata, "GFC", "TYPE_BROUILLARD");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tbrLibelle, ColumnMetadata.named("TBR_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(tbrOrdre, ColumnMetadata.named("TBR_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

