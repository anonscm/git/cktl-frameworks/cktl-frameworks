package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmExerciceCocktail is a Querydsl query type for QAdmExerciceCocktail
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmExerciceCocktail extends com.mysema.query.sql.RelationalPathBase<QAdmExerciceCocktail> {

    private static final long serialVersionUID = -712392107;

    public static final QAdmExerciceCocktail admExerciceCocktail = new QAdmExerciceCocktail("ADM_EXERCICE_COCKTAIL");

    public final NumberPath<Integer> exeExercice = createNumber("exeExercice", Integer.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmExerciceCocktail> exerciceCocktailPk = createPrimaryKey(exeOrdre);

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBud> _opeTrancheBudExeFk = createInvForeignKey(exeOrdre, "EXE_ORDRE");

    public QAdmExerciceCocktail(String variable) {
        super(QAdmExerciceCocktail.class, forVariable(variable), "GFC", "ADM_EXERCICE_COCKTAIL");
        addMetadata();
    }

    public QAdmExerciceCocktail(String variable, String schema, String table) {
        super(QAdmExerciceCocktail.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmExerciceCocktail(Path<? extends QAdmExerciceCocktail> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_EXERCICE_COCKTAIL");
        addMetadata();
    }

    public QAdmExerciceCocktail(PathMetadata<?> metadata) {
        super(QAdmExerciceCocktail.class, metadata, "GFC", "ADM_EXERCICE_COCKTAIL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeExercice, ColumnMetadata.named("EXE_EXERCICE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4).notNull());
    }

}

