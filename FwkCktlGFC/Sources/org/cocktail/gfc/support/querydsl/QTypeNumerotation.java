package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTypeNumerotation is a Querydsl query type for QTypeNumerotation
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QTypeNumerotation extends com.mysema.query.sql.RelationalPathBase<QTypeNumerotation> {

    private static final long serialVersionUID = -683544200;

    public static final QTypeNumerotation typeNumerotation = new QTypeNumerotation("TYPE_NUMEROTATION");

    public final StringPath tnuEntite = createString("tnuEntite");

    public final StringPath tnuLibelle = createString("tnuLibelle");

    public final NumberPath<Long> tnuOrdre = createNumber("tnuOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QTypeNumerotation> typeNumerotationPk = createPrimaryKey(tnuOrdre);

    public final com.mysema.query.sql.ForeignKey<QTypeBordereau> _typeBordereauTnuOrdreFk = createInvForeignKey(tnuOrdre, "TNU_ORDRE");

    public QTypeNumerotation(String variable) {
        super(QTypeNumerotation.class, forVariable(variable), "GFC", "TYPE_NUMEROTATION");
        addMetadata();
    }

    public QTypeNumerotation(String variable, String schema, String table) {
        super(QTypeNumerotation.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTypeNumerotation(Path<? extends QTypeNumerotation> path) {
        super(path.getType(), path.getMetadata(), "GFC", "TYPE_NUMEROTATION");
        addMetadata();
    }

    public QTypeNumerotation(PathMetadata<?> metadata) {
        super(QTypeNumerotation.class, metadata, "GFC", "TYPE_NUMEROTATION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tnuEntite, ColumnMetadata.named("TNU_ENTITE").withIndex(2).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(tnuLibelle, ColumnMetadata.named("TNU_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(tnuOrdre, ColumnMetadata.named("TNU_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

