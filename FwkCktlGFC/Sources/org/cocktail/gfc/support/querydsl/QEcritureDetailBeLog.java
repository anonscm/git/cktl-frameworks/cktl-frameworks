package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEcritureDetailBeLog is a Querydsl query type for QEcritureDetailBeLog
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEcritureDetailBeLog extends com.mysema.query.sql.RelationalPathBase<QEcritureDetailBeLog> {

    private static final long serialVersionUID = 1759531654;

    public static final QEcritureDetailBeLog ecritureDetailBeLog = new QEcritureDetailBeLog("ECRITURE_DETAIL_BE_LOG");

    public final NumberPath<Long> ecdOrdre = createNumber("ecdOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> edbDate = createDateTime("edbDate", java.sql.Timestamp.class);

    public final NumberPath<Long> edbOrdre = createNumber("edbOrdre", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.ForeignKey<QEcritureDetail> ecdFkEcdOrdre = createForeignKey(ecdOrdre, "ECD_ORDRE");

    public QEcritureDetailBeLog(String variable) {
        super(QEcritureDetailBeLog.class, forVariable(variable), "GFC", "ECRITURE_DETAIL_BE_LOG");
        addMetadata();
    }

    public QEcritureDetailBeLog(String variable, String schema, String table) {
        super(QEcritureDetailBeLog.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEcritureDetailBeLog(Path<? extends QEcritureDetailBeLog> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ECRITURE_DETAIL_BE_LOG");
        addMetadata();
    }

    public QEcritureDetailBeLog(PathMetadata<?> metadata) {
        super(QEcritureDetailBeLog.class, metadata, "GFC", "ECRITURE_DETAIL_BE_LOG");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ecdOrdre, ColumnMetadata.named("ECD_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(edbDate, ColumnMetadata.named("EDB_DATE").withIndex(2).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(edbOrdre, ColumnMetadata.named("EDB_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
    }

}

