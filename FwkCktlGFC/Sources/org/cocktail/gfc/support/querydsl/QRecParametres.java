package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QRecParametres is a Querydsl query type for QRecParametres
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QRecParametres extends com.mysema.query.sql.RelationalPathBase<QRecParametres> {

    private static final long serialVersionUID = -1155986829;

    public static final QRecParametres recParametres = new QRecParametres("REC_PARAMETRES");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath parDescription = createString("parDescription");

    public final StringPath parKey = createString("parKey");

    public final NumberPath<Long> parOrdre = createNumber("parOrdre", Long.class);

    public final StringPath parValue = createString("parValue");

    public final com.mysema.query.sql.PrimaryKey<QRecParametres> recParametresPk = createPrimaryKey(parOrdre);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> parametresExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QRecParametres(String variable) {
        super(QRecParametres.class, forVariable(variable), "GFC", "REC_PARAMETRES");
        addMetadata();
    }

    public QRecParametres(String variable, String schema, String table) {
        super(QRecParametres.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QRecParametres(Path<? extends QRecParametres> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REC_PARAMETRES");
        addMetadata();
    }

    public QRecParametres(PathMetadata<?> metadata) {
        super(QRecParametres.class, metadata, "GFC", "REC_PARAMETRES");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(4));
        addMetadata(parDescription, ColumnMetadata.named("PAR_DESCRIPTION").withIndex(4).ofType(Types.VARCHAR).withSize(300));
        addMetadata(parKey, ColumnMetadata.named("PAR_KEY").withIndex(2).ofType(Types.VARCHAR).withSize(80));
        addMetadata(parOrdre, ColumnMetadata.named("PAR_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(parValue, ColumnMetadata.named("PAR_VALUE").withIndex(3).ofType(Types.VARCHAR).withSize(500));
    }

}

