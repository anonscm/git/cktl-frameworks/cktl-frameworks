package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCptReport is a Querydsl query type for QCptReport
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCptReport extends com.mysema.query.sql.RelationalPathBase<QCptReport> {

    private static final long serialVersionUID = -1469527590;

    public static final QCptReport cptReport = new QCptReport("CPT_REPORT");

    public final StringPath repId = createString("repId");

    public final StringPath repLocation = createString("repLocation");

    public final StringPath repNom = createString("repNom");

    public final com.mysema.query.sql.PrimaryKey<QCptReport> cptReportPk = createPrimaryKey(repId);

    public QCptReport(String variable) {
        super(QCptReport.class, forVariable(variable), "GFC", "CPT_REPORT");
        addMetadata();
    }

    public QCptReport(String variable, String schema, String table) {
        super(QCptReport.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCptReport(Path<? extends QCptReport> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CPT_REPORT");
        addMetadata();
    }

    public QCptReport(PathMetadata<?> metadata) {
        super(QCptReport.class, metadata, "GFC", "CPT_REPORT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(repId, ColumnMetadata.named("REP_ID").withIndex(1).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(repLocation, ColumnMetadata.named("REP_LOCATION").withIndex(3).ofType(Types.VARCHAR).withSize(500));
        addMetadata(repNom, ColumnMetadata.named("REP_NOM").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
    }

}

