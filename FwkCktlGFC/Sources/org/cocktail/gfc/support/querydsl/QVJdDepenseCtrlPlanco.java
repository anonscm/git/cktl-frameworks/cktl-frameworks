package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVJdDepenseCtrlPlanco is a Querydsl query type for QVJdDepenseCtrlPlanco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVJdDepenseCtrlPlanco extends com.mysema.query.sql.RelationalPathBase<QVJdDepenseCtrlPlanco> {

    private static final long serialVersionUID = -266424165;

    public static final QVJdDepenseCtrlPlanco vJdDepenseCtrlPlanco = new QVJdDepenseCtrlPlanco("V_JD_DEPENSE_CTRL_PLANCO");

    public final DateTimePath<java.sql.Timestamp> dateDebutDgp = createDateTime("dateDebutDgp", java.sql.Timestamp.class);

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<java.math.BigDecimal> dpcoHtSaisie = createNumber("dpcoHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> dpcoId = createNumber("dpcoId", Long.class);

    public final NumberPath<java.math.BigDecimal> dpcoMontantBudgetaire = createNumber("dpcoMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dpcoTtcSaisie = createNumber("dpcoTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> dpcoTvaSaisie = createNumber("dpcoTvaSaisie", java.math.BigDecimal.class);

    public final DateTimePath<java.sql.Timestamp> dppDateFacture = createDateTime("dppDateFacture", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateReception = createDateTime("dppDateReception", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dppDateServiceFait = createDateTime("dppDateServiceFait", java.sql.Timestamp.class);

    public final NumberPath<Long> dppId = createNumber("dppId", Long.class);

    public final NumberPath<Long> dppImDgp = createNumber("dppImDgp", Long.class);

    public final StringPath dppNumeroFacture = createString("dppNumeroFacture");

    public final NumberPath<Long> ecdOrdre = createNumber("ecdOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> manId = createNumber("manId", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tboOrdre = createNumber("tboOrdre", Long.class);

    public QVJdDepenseCtrlPlanco(String variable) {
        super(QVJdDepenseCtrlPlanco.class, forVariable(variable), "GFC", "V_JD_DEPENSE_CTRL_PLANCO");
        addMetadata();
    }

    public QVJdDepenseCtrlPlanco(String variable, String schema, String table) {
        super(QVJdDepenseCtrlPlanco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVJdDepenseCtrlPlanco(Path<? extends QVJdDepenseCtrlPlanco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_JD_DEPENSE_CTRL_PLANCO");
        addMetadata();
    }

    public QVJdDepenseCtrlPlanco(PathMetadata<?> metadata) {
        super(QVJdDepenseCtrlPlanco.class, metadata, "GFC", "V_JD_DEPENSE_CTRL_PLANCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dateDebutDgp, ColumnMetadata.named("DATE_DEBUT_DGP").withIndex(18).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dpcoHtSaisie, ColumnMetadata.named("DPCO_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dpcoId, ColumnMetadata.named("DPCO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dpcoMontantBudgetaire, ColumnMetadata.named("DPCO_MONTANT_BUDGETAIRE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dpcoTtcSaisie, ColumnMetadata.named("DPCO_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dpcoTvaSaisie, ColumnMetadata.named("DPCO_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(dppDateFacture, ColumnMetadata.named("DPP_DATE_FACTURE").withIndex(16).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateReception, ColumnMetadata.named("DPP_DATE_RECEPTION").withIndex(13).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dppDateServiceFait, ColumnMetadata.named("DPP_DATE_SERVICE_FAIT").withIndex(14).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(12).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dppImDgp, ColumnMetadata.named("DPP_IM_DGP").withIndex(17).ofType(Types.DECIMAL).withSize(10));
        addMetadata(dppNumeroFacture, ColumnMetadata.named("DPP_NUMERO_FACTURE").withIndex(15).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(ecdOrdre, ColumnMetadata.named("ECD_ORDRE").withIndex(11).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(manId, ColumnMetadata.named("MAN_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

