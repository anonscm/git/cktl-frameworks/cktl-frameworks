package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBbBudgetDepenseCtrlPco is a Querydsl query type for QBbBudgetDepenseCtrlPco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBbBudgetDepenseCtrlPco extends com.mysema.query.sql.RelationalPathBase<QBbBudgetDepenseCtrlPco> {

    private static final long serialVersionUID = -1125672179;

    public static final QBbBudgetDepenseCtrlPco bbBudgetDepenseCtrlPco = new QBbBudgetDepenseCtrlPco("BB_BUDGET_DEPENSE_CTRL_PCO");

    public final NumberPath<Long> dep = createNumber("dep", Long.class);

    public final NumberPath<Long> depOrv = createNumber("depOrv", Long.class);

    public final NumberPath<Long> eng = createNumber("eng", Long.class);

    public final NumberPath<Long> engOrv = createNumber("engOrv", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public QBbBudgetDepenseCtrlPco(String variable) {
        super(QBbBudgetDepenseCtrlPco.class, forVariable(variable), "GFC", "BB_BUDGET_DEPENSE_CTRL_PCO");
        addMetadata();
    }

    public QBbBudgetDepenseCtrlPco(String variable, String schema, String table) {
        super(QBbBudgetDepenseCtrlPco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBbBudgetDepenseCtrlPco(Path<? extends QBbBudgetDepenseCtrlPco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BB_BUDGET_DEPENSE_CTRL_PCO");
        addMetadata();
    }

    public QBbBudgetDepenseCtrlPco(PathMetadata<?> metadata) {
        super(QBbBudgetDepenseCtrlPco.class, metadata, "GFC", "BB_BUDGET_DEPENSE_CTRL_PCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dep, ColumnMetadata.named("DEP").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(depOrv, ColumnMetadata.named("DEP_ORV").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(eng, ColumnMetadata.named("ENG").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(engOrv, ColumnMetadata.named("ENG_ORV").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(20));
    }

}

