package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QGestion is a Querydsl query type for QGestion
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QGestion extends com.mysema.query.sql.RelationalPathBase<QGestion> {

    private static final long serialVersionUID = -1561867320;

    public static final QGestion gestion = new QGestion("GESTION");

    public final NumberPath<Long> comOrdre = createNumber("comOrdre", Long.class);

    public final StringPath gesCode = createString("gesCode");

    public final com.mysema.query.sql.PrimaryKey<QGestion> gestionPk = createPrimaryKey(gesCode);

    public final com.mysema.query.sql.ForeignKey<QComptabilite> gestionComOrdreFk = createForeignKey(comOrdre, "COM_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QTitre> _titreGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QEcritureDetail> _ecrDetailFk3 = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QDepNumerotation> _depNumerotationGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QComptabilite> _comptabiliteGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QBrouillardDetail> _brodGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QNumerotation> _numerotationGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QEpnCodeBudgetSacd> _epnCodeBudgetSacdGestFk = createInvForeignKey(gesCode, "SACD");

    public final com.mysema.query.sql.ForeignKey<QBordereauRejet> _bordereauRejetGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlPlancoTva> _rpcotvaGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QAbricotBordSelection> _abricotBordSelectionGes_fk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QChequeBrouillard> _chequeBrouillardGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QMandatBrouillard> _mandatBrouillardGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QGestionExercice> _gestionGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QRecNumerotation> _gesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QCptRecette> _recetteGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QBordereauBrouillard> _bordereauBrouillardGesCoFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QTitreBrouillard> _titreBrouillardGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QOdpaiementBrouillard> _odpaiementBrouillardGesCFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlPlancoCtp> _rpcoctpGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QMandat> _mandatGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateurFonctGest> _admUfgGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QBordereau> _bordereauGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public final com.mysema.query.sql.ForeignKey<QCptDepense> _depenseGesCodeFk = createInvForeignKey(gesCode, "GES_CODE");

    public QGestion(String variable) {
        super(QGestion.class, forVariable(variable), "GFC", "GESTION");
        addMetadata();
    }

    public QGestion(String variable, String schema, String table) {
        super(QGestion.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QGestion(Path<? extends QGestion> path) {
        super(path.getType(), path.getMetadata(), "GFC", "GESTION");
        addMetadata();
    }

    public QGestion(PathMetadata<?> metadata) {
        super(QGestion.class, metadata, "GFC", "GESTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(comOrdre, ColumnMetadata.named("COM_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(1).ofType(Types.VARCHAR).withSize(10).notNull());
    }

}

