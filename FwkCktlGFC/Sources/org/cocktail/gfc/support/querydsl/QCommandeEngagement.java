package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeEngagement is a Querydsl query type for QCommandeEngagement
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeEngagement extends com.mysema.query.sql.RelationalPathBase<QCommandeEngagement> {

    private static final long serialVersionUID = 452690202;

    public static final QCommandeEngagement commandeEngagement = new QCommandeEngagement("COMMANDE_ENGAGEMENT");

    public final NumberPath<Long> comeId = createNumber("comeId", Long.class);

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCommandeEngagement> commandeEngagementPk = createPrimaryKey(comeId);

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> commandeEngagementEngIdFk = createForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QCommande> commandeEngagementCommIdFk = createForeignKey(commId, "COMM_ID");

    public QCommandeEngagement(String variable) {
        super(QCommandeEngagement.class, forVariable(variable), "GFC", "COMMANDE_ENGAGEMENT");
        addMetadata();
    }

    public QCommandeEngagement(String variable, String schema, String table) {
        super(QCommandeEngagement.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeEngagement(Path<? extends QCommandeEngagement> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_ENGAGEMENT");
        addMetadata();
    }

    public QCommandeEngagement(PathMetadata<?> metadata) {
        super(QCommandeEngagement.class, metadata, "GFC", "COMMANDE_ENGAGEMENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(comeId, ColumnMetadata.named("COME_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

