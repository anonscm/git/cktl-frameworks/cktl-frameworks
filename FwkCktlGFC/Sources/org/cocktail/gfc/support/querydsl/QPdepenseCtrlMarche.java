package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPdepenseCtrlMarche is a Querydsl query type for QPdepenseCtrlMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPdepenseCtrlMarche extends com.mysema.query.sql.RelationalPathBase<QPdepenseCtrlMarche> {

    private static final long serialVersionUID = -1140872696;

    public static final QPdepenseCtrlMarche pdepenseCtrlMarche = new QPdepenseCtrlMarche("PDEPENSE_CTRL_MARCHE");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> pdepId = createNumber("pdepId", Long.class);

    public final NumberPath<java.math.BigDecimal> pdmarHtSaisie = createNumber("pdmarHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> pdmarId = createNumber("pdmarId", Long.class);

    public final NumberPath<java.math.BigDecimal> pdmarMontantBudgetaire = createNumber("pdmarMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdmarTtcSaisie = createNumber("pdmarTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdmarTvaSaisie = createNumber("pdmarTvaSaisie", java.math.BigDecimal.class);

    public final com.mysema.query.sql.PrimaryKey<QPdepenseCtrlMarche> pdepenseCtrlMarchePk = createPrimaryKey(pdmarId);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> pdepenseCtrlMarExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseBudget> pdepenseCtrlMarPdepIdFk = createForeignKey(pdepId, "PDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QAttribution> pdepenseCtrlMarAttOrdreFk = createForeignKey(attOrdre, "ATT_ORDRE");

    public QPdepenseCtrlMarche(String variable) {
        super(QPdepenseCtrlMarche.class, forVariable(variable), "GFC", "PDEPENSE_CTRL_MARCHE");
        addMetadata();
    }

    public QPdepenseCtrlMarche(String variable, String schema, String table) {
        super(QPdepenseCtrlMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPdepenseCtrlMarche(Path<? extends QPdepenseCtrlMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PDEPENSE_CTRL_MARCHE");
        addMetadata();
    }

    public QPdepenseCtrlMarche(PathMetadata<?> metadata) {
        super(QPdepenseCtrlMarche.class, metadata, "GFC", "PDEPENSE_CTRL_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(pdepId, ColumnMetadata.named("PDEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdmarHtSaisie, ColumnMetadata.named("PDMAR_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdmarId, ColumnMetadata.named("PDMAR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdmarMontantBudgetaire, ColumnMetadata.named("PDMAR_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdmarTtcSaisie, ColumnMetadata.named("PDMAR_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdmarTvaSaisie, ColumnMetadata.named("PDMAR_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
    }

}

