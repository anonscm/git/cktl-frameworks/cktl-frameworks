package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTypeBordereau is a Querydsl query type for QTypeBordereau
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QTypeBordereau extends com.mysema.query.sql.RelationalPathBase<QTypeBordereau> {

    private static final long serialVersionUID = 329489970;

    public static final QTypeBordereau typeBordereau = new QTypeBordereau("TYPE_BORDEREAU");

    public final StringPath tboLibelle = createString("tboLibelle");

    public final NumberPath<Long> tboOrdre = createNumber("tboOrdre", Long.class);

    public final StringPath tboSousType = createString("tboSousType");

    public final StringPath tboType = createString("tboType");

    public final StringPath tboTypeCreation = createString("tboTypeCreation");

    public final NumberPath<Long> tnuOrdre = createNumber("tnuOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QTypeBordereau> typeBordereauPk = createPrimaryKey(tboOrdre);

    public final com.mysema.query.sql.ForeignKey<QTypeNumerotation> typeBordereauTnuOrdreFk = createForeignKey(tnuOrdre, "TNU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBordereau> _bordereauTboOrdreFk = createInvForeignKey(tboOrdre, "TBO_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAbricotBordSelection> _abricotBordSelectionTbo_fk = createInvForeignKey(tboOrdre, "TBO_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QBordereauRejet> _bordereauRejetTboOrdreFk = createInvForeignKey(tboOrdre, "TBO_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlPlanco> _depenseCtrlPlanTboOrdreFk = createInvForeignKey(tboOrdre, "TBO_ORDRE");

    public QTypeBordereau(String variable) {
        super(QTypeBordereau.class, forVariable(variable), "GFC", "TYPE_BORDEREAU");
        addMetadata();
    }

    public QTypeBordereau(String variable, String schema, String table) {
        super(QTypeBordereau.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTypeBordereau(Path<? extends QTypeBordereau> path) {
        super(path.getType(), path.getMetadata(), "GFC", "TYPE_BORDEREAU");
        addMetadata();
    }

    public QTypeBordereau(PathMetadata<?> metadata) {
        super(QTypeBordereau.class, metadata, "GFC", "TYPE_BORDEREAU");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tboLibelle, ColumnMetadata.named("TBO_LIBELLE").withIndex(4).ofType(Types.VARCHAR).withSize(60).notNull());
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tboSousType, ColumnMetadata.named("TBO_SOUS_TYPE").withIndex(3).ofType(Types.VARCHAR).withSize(30).notNull());
        addMetadata(tboType, ColumnMetadata.named("TBO_TYPE").withIndex(2).ofType(Types.VARCHAR).withSize(30).notNull());
        addMetadata(tboTypeCreation, ColumnMetadata.named("TBO_TYPE_CREATION").withIndex(5).ofType(Types.VARCHAR).withSize(20));
        addMetadata(tnuOrdre, ColumnMetadata.named("TNU_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0));
    }

}

