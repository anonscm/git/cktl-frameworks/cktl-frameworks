package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCorrespondanceCn is a Querydsl query type for QCorrespondanceCn
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCorrespondanceCn extends com.mysema.query.sql.RelationalPathBase<QCorrespondanceCn> {

    private static final long serialVersionUID = 1676813866;

    public static final QCorrespondanceCn correspondanceCn = new QCorrespondanceCn("CORRESPONDANCE_CN");

    public final StringPath ancienCn = createString("ancienCn");

    public final StringPath nouveauCn = createString("nouveauCn");

    public final com.mysema.query.sql.PrimaryKey<QCorrespondanceCn> correspondanceCnPk = createPrimaryKey(ancienCn, nouveauCn);

    public QCorrespondanceCn(String variable) {
        super(QCorrespondanceCn.class, forVariable(variable), "GFC", "CORRESPONDANCE_CN");
        addMetadata();
    }

    public QCorrespondanceCn(String variable, String schema, String table) {
        super(QCorrespondanceCn.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCorrespondanceCn(Path<? extends QCorrespondanceCn> path) {
        super(path.getType(), path.getMetadata(), "GFC", "CORRESPONDANCE_CN");
        addMetadata();
    }

    public QCorrespondanceCn(PathMetadata<?> metadata) {
        super(QCorrespondanceCn.class, metadata, "GFC", "CORRESPONDANCE_CN");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ancienCn, ColumnMetadata.named("ANCIEN_CN").withIndex(1).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(nouveauCn, ColumnMetadata.named("NOUVEAU_CN").withIndex(2).ofType(Types.VARCHAR).withSize(10).notNull());
    }

}

