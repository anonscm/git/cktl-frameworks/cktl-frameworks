package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTypePartenaire is a Querydsl query type for QTypePartenaire
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QTypePartenaire extends com.mysema.query.sql.RelationalPathBase<QTypePartenaire> {

    private static final long serialVersionUID = -281024430;

    public static final QTypePartenaire typePartenaire = new QTypePartenaire("TYPE_PARTENAIRE");

    public final StringPath typePartIdInterne = createString("typePartIdInterne");

    public final StringPath typePartLibelle = createString("typePartLibelle");

    public final NumberPath<Long> typePartOrdre = createNumber("typePartOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QTypePartenaire> typePartenairePk = createPrimaryKey(typePartOrdre);

    public QTypePartenaire(String variable) {
        super(QTypePartenaire.class, forVariable(variable), "GFC", "TYPE_PARTENAIRE");
        addMetadata();
    }

    public QTypePartenaire(String variable, String schema, String table) {
        super(QTypePartenaire.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTypePartenaire(Path<? extends QTypePartenaire> path) {
        super(path.getType(), path.getMetadata(), "GFC", "TYPE_PARTENAIRE");
        addMetadata();
    }

    public QTypePartenaire(PathMetadata<?> metadata) {
        super(QTypePartenaire.class, metadata, "GFC", "TYPE_PARTENAIRE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(typePartIdInterne, ColumnMetadata.named("TYPE_PART_ID_INTERNE").withIndex(3).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(typePartLibelle, ColumnMetadata.named("TYPE_PART_LIBELLE").withIndex(1).ofType(Types.VARCHAR).withSize(70).notNull());
        addMetadata(typePartOrdre, ColumnMetadata.named("TYPE_PART_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

