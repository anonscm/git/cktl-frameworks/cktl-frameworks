package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVEditionTitre is a Querydsl query type for QVEditionTitre
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVEditionTitre extends com.mysema.query.sql.RelationalPathBase<QVEditionTitre> {

    private static final long serialVersionUID = 780300329;

    public static final QVEditionTitre vEditionTitre = new QVEditionTitre("V_EDITION_TITRE");

    public final SimplePath<Object> adrAdresse1 = createSimple("adrAdresse1", Object.class);

    public final SimplePath<Object> adrAdresse2 = createSimple("adrAdresse2", Object.class);

    public final SimplePath<Object> adrOrdre = createSimple("adrOrdre", Object.class);

    public final SimplePath<Object> borId = createSimple("borId", Object.class);

    public final SimplePath<Object> borNum = createSimple("borNum", Object.class);

    public final SimplePath<Object> codePostal = createSimple("codePostal", Object.class);

    public final SimplePath<Object> cPays = createSimple("cPays", Object.class);

    public final SimplePath<Object> cpEtranger = createSimple("cpEtranger", Object.class);

    public final SimplePath<Object> debiteur = createSimple("debiteur", Object.class);

    public final SimplePath<Object> debiteurSiret = createSimple("debiteurSiret", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> facNumero = createSimple("facNumero", Object.class);

    public final SimplePath<Object> fouOrdre = createSimple("fouOrdre", Object.class);

    public final SimplePath<Object> gesCode = createSimple("gesCode", Object.class);

    public final SimplePath<Object> ligneBudg = createSimple("ligneBudg", Object.class);

    public final SimplePath<Object> montantLettre = createSimple("montantLettre", Object.class);

    public final SimplePath<Object> orgLib = createSimple("orgLib", Object.class);

    public final SimplePath<Object> pcoChapitre = createSimple("pcoChapitre", Object.class);

    public final SimplePath<Object> pcoNum = createSimple("pcoNum", Object.class);

    public final SimplePath<Object> recNumero = createSimple("recNumero", Object.class);

    public final SimplePath<Object> tboOrdre = createSimple("tboOrdre", Object.class);

    public final SimplePath<Object> titAdresse = createSimple("titAdresse", Object.class);

    public final SimplePath<Object> titDate = createSimple("titDate", Object.class);

    public final SimplePath<Object> titHt = createSimple("titHt", Object.class);

    public final SimplePath<Object> titId = createSimple("titId", Object.class);

    public final SimplePath<Object> titLibelle = createSimple("titLibelle", Object.class);

    public final SimplePath<Object> titNbPiece = createSimple("titNbPiece", Object.class);

    public final SimplePath<Object> titNumero = createSimple("titNumero", Object.class);

    public final SimplePath<Object> titTtc = createSimple("titTtc", Object.class);

    public final SimplePath<Object> titTva = createSimple("titTva", Object.class);

    public final SimplePath<Object> ville = createSimple("ville", Object.class);

    public QVEditionTitre(String variable) {
        super(QVEditionTitre.class, forVariable(variable), "GFC", "V_EDITION_TITRE");
        addMetadata();
    }

    public QVEditionTitre(String variable, String schema, String table) {
        super(QVEditionTitre.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVEditionTitre(Path<? extends QVEditionTitre> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_EDITION_TITRE");
        addMetadata();
    }

    public QVEditionTitre(PathMetadata<?> metadata) {
        super(QVEditionTitre.class, metadata, "GFC", "V_EDITION_TITRE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(adrAdresse1, ColumnMetadata.named("ADR_ADRESSE1").withIndex(25).ofType(Types.OTHER).withSize(0));
        addMetadata(adrAdresse2, ColumnMetadata.named("ADR_ADRESSE2").withIndex(26).ofType(Types.OTHER).withSize(0));
        addMetadata(adrOrdre, ColumnMetadata.named("ADR_ORDRE").withIndex(24).ofType(Types.OTHER).withSize(0));
        addMetadata(borId, ColumnMetadata.named("BOR_ID").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(borNum, ColumnMetadata.named("BOR_NUM").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(codePostal, ColumnMetadata.named("CODE_POSTAL").withIndex(27).ofType(Types.OTHER).withSize(0));
        addMetadata(cPays, ColumnMetadata.named("C_PAYS").withIndex(30).ofType(Types.OTHER).withSize(0));
        addMetadata(cpEtranger, ColumnMetadata.named("CP_ETRANGER").withIndex(29).ofType(Types.OTHER).withSize(0));
        addMetadata(debiteur, ColumnMetadata.named("DEBITEUR").withIndex(18).ofType(Types.OTHER).withSize(0));
        addMetadata(debiteurSiret, ColumnMetadata.named("DEBITEUR_SIRET").withIndex(31).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(facNumero, ColumnMetadata.named("FAC_NUMERO").withIndex(23).ofType(Types.OTHER).withSize(0));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(19).ofType(Types.OTHER).withSize(0));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(ligneBudg, ColumnMetadata.named("LIGNE_BUDG").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(montantLettre, ColumnMetadata.named("MONTANT_LETTRE").withIndex(21).ofType(Types.OTHER).withSize(0));
        addMetadata(orgLib, ColumnMetadata.named("ORG_LIB").withIndex(15).ofType(Types.OTHER).withSize(0));
        addMetadata(pcoChapitre, ColumnMetadata.named("PCO_CHAPITRE").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(recNumero, ColumnMetadata.named("REC_NUMERO").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(tboOrdre, ColumnMetadata.named("TBO_ORDRE").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(titAdresse, ColumnMetadata.named("TIT_ADRESSE").withIndex(22).ofType(Types.OTHER).withSize(0));
        addMetadata(titDate, ColumnMetadata.named("TIT_DATE").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(titHt, ColumnMetadata.named("TIT_HT").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(titId, ColumnMetadata.named("TIT_ID").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(titLibelle, ColumnMetadata.named("TIT_LIBELLE").withIndex(20).ofType(Types.OTHER).withSize(0));
        addMetadata(titNbPiece, ColumnMetadata.named("TIT_NB_PIECE").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(titNumero, ColumnMetadata.named("TIT_NUMERO").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(titTtc, ColumnMetadata.named("TIT_TTC").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(titTva, ColumnMetadata.named("TIT_TVA").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(ville, ColumnMetadata.named("VILLE").withIndex(28).ofType(Types.OTHER).withSize(0));
    }

}

