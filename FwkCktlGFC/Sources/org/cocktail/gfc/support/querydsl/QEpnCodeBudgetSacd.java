package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEpnCodeBudgetSacd is a Querydsl query type for QEpnCodeBudgetSacd
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEpnCodeBudgetSacd extends com.mysema.query.sql.RelationalPathBase<QEpnCodeBudgetSacd> {

    private static final long serialVersionUID = -2116277501;

    public static final QEpnCodeBudgetSacd epnCodeBudgetSacd = new QEpnCodeBudgetSacd("EPN_CODE_BUDGET_SACD");

    public final StringPath codBud = createString("codBud");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath sacd = createString("sacd");

    public final com.mysema.query.sql.PrimaryKey<QEpnCodeBudgetSacd> epnCodeBudgetSacdPk = createPrimaryKey(exeOrdre, sacd);

    public final com.mysema.query.sql.ForeignKey<QGestion> epnCodeBudgetSacdGestFk = createForeignKey(sacd, "GES_CODE");

    public QEpnCodeBudgetSacd(String variable) {
        super(QEpnCodeBudgetSacd.class, forVariable(variable), "GFC", "EPN_CODE_BUDGET_SACD");
        addMetadata();
    }

    public QEpnCodeBudgetSacd(String variable, String schema, String table) {
        super(QEpnCodeBudgetSacd.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEpnCodeBudgetSacd(Path<? extends QEpnCodeBudgetSacd> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EPN_CODE_BUDGET_SACD");
        addMetadata();
    }

    public QEpnCodeBudgetSacd(PathMetadata<?> metadata) {
        super(QEpnCodeBudgetSacd.class, metadata, "GFC", "EPN_CODE_BUDGET_SACD");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(codBud, ColumnMetadata.named("COD_BUD").withIndex(2).ofType(Types.VARCHAR).withSize(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(sacd, ColumnMetadata.named("SACD").withIndex(1).ofType(Types.VARCHAR).withSize(10).notNull());
    }

}

