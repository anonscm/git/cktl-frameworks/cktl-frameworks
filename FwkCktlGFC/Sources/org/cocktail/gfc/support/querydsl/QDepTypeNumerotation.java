package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDepTypeNumerotation is a Querydsl query type for QDepTypeNumerotation
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QDepTypeNumerotation extends com.mysema.query.sql.RelationalPathBase<QDepTypeNumerotation> {

    private static final long serialVersionUID = 825307653;

    public static final QDepTypeNumerotation depTypeNumerotation = new QDepTypeNumerotation("DEP_TYPE_NUMEROTATION");

    public final StringPath tnuEntite = createString("tnuEntite");

    public final NumberPath<Long> tnuId = createNumber("tnuId", Long.class);

    public final StringPath tnuLibelle = createString("tnuLibelle");

    public final com.mysema.query.sql.PrimaryKey<QDepTypeNumerotation> sysC0078006 = createPrimaryKey(tnuId);

    public final com.mysema.query.sql.ForeignKey<QDepNumerotation> _depNumerotationTnuIdFk = createInvForeignKey(tnuId, "TNU_ID");

    public QDepTypeNumerotation(String variable) {
        super(QDepTypeNumerotation.class, forVariable(variable), "GFC", "DEP_TYPE_NUMEROTATION");
        addMetadata();
    }

    public QDepTypeNumerotation(String variable, String schema, String table) {
        super(QDepTypeNumerotation.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDepTypeNumerotation(Path<? extends QDepTypeNumerotation> path) {
        super(path.getType(), path.getMetadata(), "GFC", "DEP_TYPE_NUMEROTATION");
        addMetadata();
    }

    public QDepTypeNumerotation(PathMetadata<?> metadata) {
        super(QDepTypeNumerotation.class, metadata, "GFC", "DEP_TYPE_NUMEROTATION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tnuEntite, ColumnMetadata.named("TNU_ENTITE").withIndex(2).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(tnuId, ColumnMetadata.named("TNU_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tnuLibelle, ColumnMetadata.named("TNU_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(200).notNull());
    }

}

