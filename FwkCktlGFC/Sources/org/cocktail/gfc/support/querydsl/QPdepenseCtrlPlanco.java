package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPdepenseCtrlPlanco is a Querydsl query type for QPdepenseCtrlPlanco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPdepenseCtrlPlanco extends com.mysema.query.sql.RelationalPathBase<QPdepenseCtrlPlanco> {

    private static final long serialVersionUID = -1045322533;

    public static final QPdepenseCtrlPlanco pdepenseCtrlPlanco = new QPdepenseCtrlPlanco("PDEPENSE_CTRL_PLANCO");

    public final NumberPath<Long> ecdOrdre = createNumber("ecdOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> pdepId = createNumber("pdepId", Long.class);

    public final NumberPath<java.math.BigDecimal> pdpcoHtSaisie = createNumber("pdpcoHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> pdpcoId = createNumber("pdpcoId", Long.class);

    public final NumberPath<java.math.BigDecimal> pdpcoMontantBudgetaire = createNumber("pdpcoMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdpcoTtcSaisie = createNumber("pdpcoTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdpcoTvaSaisie = createNumber("pdpcoTvaSaisie", java.math.BigDecimal.class);

    public final com.mysema.query.sql.PrimaryKey<QPdepenseCtrlPlanco> pdepenseCtrlPlancoPk = createPrimaryKey(pdpcoId);

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> pdepenseCtrlPlaPcoNumFk = createForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QPdepenseBudget> pdepenseCtrlPlanPdepIdFk = createForeignKey(pdepId, "PDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> pdepenseCtrlPlaExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlPlancoInve> _pdepenseCtrlPlinvPdpcoidFk = createInvForeignKey(pdpcoId, "PDPCO_ID");

    public QPdepenseCtrlPlanco(String variable) {
        super(QPdepenseCtrlPlanco.class, forVariable(variable), "GFC", "PDEPENSE_CTRL_PLANCO");
        addMetadata();
    }

    public QPdepenseCtrlPlanco(String variable, String schema, String table) {
        super(QPdepenseCtrlPlanco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPdepenseCtrlPlanco(Path<? extends QPdepenseCtrlPlanco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PDEPENSE_CTRL_PLANCO");
        addMetadata();
    }

    public QPdepenseCtrlPlanco(PathMetadata<?> metadata) {
        super(QPdepenseCtrlPlanco.class, metadata, "GFC", "PDEPENSE_CTRL_PLANCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ecdOrdre, ColumnMetadata.named("ECD_ORDRE").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(pdepId, ColumnMetadata.named("PDEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdpcoHtSaisie, ColumnMetadata.named("PDPCO_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdpcoId, ColumnMetadata.named("PDPCO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdpcoMontantBudgetaire, ColumnMetadata.named("PDPCO_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdpcoTtcSaisie, ColumnMetadata.named("PDPCO_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdpcoTvaSaisie, ColumnMetadata.named("PDPCO_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
    }

}

