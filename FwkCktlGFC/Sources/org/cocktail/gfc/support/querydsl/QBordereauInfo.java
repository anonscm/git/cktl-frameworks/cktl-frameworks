package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBordereauInfo is a Querydsl query type for QBordereauInfo
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBordereauInfo extends com.mysema.query.sql.RelationalPathBase<QBordereauInfo> {

    private static final long serialVersionUID = -342374;

    public static final QBordereauInfo bordereauInfo = new QBordereauInfo("BORDEREAU_INFO");

    public final NumberPath<Long> borId = createNumber("borId", Long.class);

    public final StringPath borLibelle = createString("borLibelle");

    public final StringPath pcoNumVisa = createString("pcoNumVisa");

    public final com.mysema.query.sql.PrimaryKey<QBordereauInfo> bordereauInfoPk = createPrimaryKey(borId);

    public final com.mysema.query.sql.ForeignKey<QBordereau> bordereauInfoBorIdFk = createForeignKey(borId, "BOR_ID");

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> bordereauInfoPcoNumVisaFk = createForeignKey(pcoNumVisa, "PCO_NUM");

    public QBordereauInfo(String variable) {
        super(QBordereauInfo.class, forVariable(variable), "GFC", "BORDEREAU_INFO");
        addMetadata();
    }

    public QBordereauInfo(String variable, String schema, String table) {
        super(QBordereauInfo.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBordereauInfo(Path<? extends QBordereauInfo> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BORDEREAU_INFO");
        addMetadata();
    }

    public QBordereauInfo(PathMetadata<?> metadata) {
        super(QBordereauInfo.class, metadata, "GFC", "BORDEREAU_INFO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(borId, ColumnMetadata.named("BOR_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(borLibelle, ColumnMetadata.named("BOR_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(100));
        addMetadata(pcoNumVisa, ColumnMetadata.named("PCO_NUM_VISA").withIndex(3).ofType(Types.VARCHAR).withSize(20));
    }

}

