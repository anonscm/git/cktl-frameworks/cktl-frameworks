package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEpnBalance is a Querydsl query type for QEpnBalance
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEpnBalance extends com.mysema.query.sql.RelationalPathBase<QEpnBalance> {

    private static final long serialVersionUID = -2004458502;

    public static final QEpnBalance epnBalance = new QEpnBalance("EPN_BALANCE");

    public final NumberPath<Long> beCr = createNumber("beCr", Long.class);

    public final NumberPath<Long> beDb = createNumber("beDb", Long.class);

    public final StringPath ecrSacd = createString("ecrSacd");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gestion = createString("gestion");

    public final StringPath imputation = createString("imputation");

    public final DateTimePath<java.sql.Timestamp> jouDate = createDateTime("jouDate", java.sql.Timestamp.class);

    public final StringPath libelle = createString("libelle");

    public final NumberPath<Long> montantCr = createNumber("montantCr", Long.class);

    public final NumberPath<Long> montantDb = createNumber("montantDb", Long.class);

    public QEpnBalance(String variable) {
        super(QEpnBalance.class, forVariable(variable), "GFC", "EPN_BALANCE");
        addMetadata();
    }

    public QEpnBalance(String variable, String schema, String table) {
        super(QEpnBalance.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEpnBalance(Path<? extends QEpnBalance> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EPN_BALANCE");
        addMetadata();
    }

    public QEpnBalance(PathMetadata<?> metadata) {
        super(QEpnBalance.class, metadata, "GFC", "EPN_BALANCE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(beCr, ColumnMetadata.named("BE_CR").withIndex(7).ofType(Types.DECIMAL).withSize(0));
        addMetadata(beDb, ColumnMetadata.named("BE_DB").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ecrSacd, ColumnMetadata.named("ECR_SACD").withIndex(6).ofType(Types.VARCHAR).withSize(1));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gestion, ColumnMetadata.named("GESTION").withIndex(3).ofType(Types.VARCHAR).withSize(10));
        addMetadata(imputation, ColumnMetadata.named("IMPUTATION").withIndex(4).ofType(Types.VARCHAR).withSize(20));
        addMetadata(jouDate, ColumnMetadata.named("JOU_DATE").withIndex(2).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(5).ofType(Types.VARCHAR).withSize(4000));
        addMetadata(montantCr, ColumnMetadata.named("MONTANT_CR").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(montantDb, ColumnMetadata.named("MONTANT_DB").withIndex(10).ofType(Types.DECIMAL).withSize(0));
    }

}

