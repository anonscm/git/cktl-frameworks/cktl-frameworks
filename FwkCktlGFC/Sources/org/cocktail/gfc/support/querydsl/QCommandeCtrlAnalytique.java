package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeCtrlAnalytique is a Querydsl query type for QCommandeCtrlAnalytique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeCtrlAnalytique extends com.mysema.query.sql.RelationalPathBase<QCommandeCtrlAnalytique> {

    private static final long serialVersionUID = 1757618833;

    public static final QCommandeCtrlAnalytique commandeCtrlAnalytique = new QCommandeCtrlAnalytique("COMMANDE_CTRL_ANALYTIQUE");

    public final NumberPath<java.math.BigDecimal> canaHtSaisie = createNumber("canaHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> canaId = createNumber("canaId", Long.class);

    public final NumberPath<java.math.BigDecimal> canaMontantBudgetaire = createNumber("canaMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<Double> canaPourcentage = createNumber("canaPourcentage", Double.class);

    public final NumberPath<java.math.BigDecimal> canaTtcSaisie = createNumber("canaTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> canaTvaSaisie = createNumber("canaTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<Long> cbudId = createNumber("cbudId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<QCommandeCtrlAnalytique> commandeCtrlAnalytiquePk = createPrimaryKey(canaId);

    public final com.mysema.query.sql.ForeignKey<QCommandeBudget> commandeCtrlAnCbudIdFk = createForeignKey(cbudId, "CBUD_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> commandeCtrlAnExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> commandeCtrlAnCanIdFk = createForeignKey(canId, "CAN_ID");

    public QCommandeCtrlAnalytique(String variable) {
        super(QCommandeCtrlAnalytique.class, forVariable(variable), "GFC", "COMMANDE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public QCommandeCtrlAnalytique(String variable, String schema, String table) {
        super(QCommandeCtrlAnalytique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeCtrlAnalytique(Path<? extends QCommandeCtrlAnalytique> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public QCommandeCtrlAnalytique(PathMetadata<?> metadata) {
        super(QCommandeCtrlAnalytique.class, metadata, "GFC", "COMMANDE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canaHtSaisie, ColumnMetadata.named("CANA_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(canaId, ColumnMetadata.named("CANA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(canaMontantBudgetaire, ColumnMetadata.named("CANA_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(canaPourcentage, ColumnMetadata.named("CANA_POURCENTAGE").withIndex(6).ofType(Types.DECIMAL).withSize(15).withDigits(5).notNull());
        addMetadata(canaTtcSaisie, ColumnMetadata.named("CANA_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(canaTvaSaisie, ColumnMetadata.named("CANA_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cbudId, ColumnMetadata.named("CBUD_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
    }

}

