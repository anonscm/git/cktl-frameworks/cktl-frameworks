package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QSaValidationCommandeCm is a Querydsl query type for QSaValidationCommandeCm
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QSaValidationCommandeCm extends com.mysema.query.sql.RelationalPathBase<QSaValidationCommandeCm> {

    private static final long serialVersionUID = -1706600564;

    public static final QSaValidationCommandeCm saValidationCommandeCm = new QSaValidationCommandeCm("SA_VALIDATION_COMMANDE_CM");

    public final NumberPath<Long> attrOrdre = createNumber("attrOrdre", Long.class);

    public final NumberPath<Long> cmOrdre = createNumber("cmOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> vccmDate = createDateTime("vccmDate", java.sql.Timestamp.class);

    public final StringPath vccmEtat = createString("vccmEtat");

    public final NumberPath<Long> vccmId = createNumber("vccmId", Long.class);

    public final NumberPath<java.math.BigDecimal> vccmMontant = createNumber("vccmMontant", java.math.BigDecimal.class);

    public final NumberPath<Long> vlcoId = createNumber("vlcoId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QSaValidationCommandeCm> saValidationCommandeCmPk = createPrimaryKey(vccmId);

    public QSaValidationCommandeCm(String variable) {
        super(QSaValidationCommandeCm.class, forVariable(variable), "GFC", "SA_VALIDATION_COMMANDE_CM");
        addMetadata();
    }

    public QSaValidationCommandeCm(String variable, String schema, String table) {
        super(QSaValidationCommandeCm.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QSaValidationCommandeCm(Path<? extends QSaValidationCommandeCm> path) {
        super(path.getType(), path.getMetadata(), "GFC", "SA_VALIDATION_COMMANDE_CM");
        addMetadata();
    }

    public QSaValidationCommandeCm(PathMetadata<?> metadata) {
        super(QSaValidationCommandeCm.class, metadata, "GFC", "SA_VALIDATION_COMMANDE_CM");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attrOrdre, ColumnMetadata.named("ATTR_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(cmOrdre, ColumnMetadata.named("CM_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0));
        addMetadata(vccmDate, ColumnMetadata.named("VCCM_DATE").withIndex(4).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(vccmEtat, ColumnMetadata.named("VCCM_ETAT").withIndex(5).ofType(Types.VARCHAR).withSize(25));
        addMetadata(vccmId, ColumnMetadata.named("VCCM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(vccmMontant, ColumnMetadata.named("VCCM_MONTANT").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(vlcoId, ColumnMetadata.named("VLCO_ID").withIndex(7).ofType(Types.DECIMAL).withSize(0));
    }

}

