package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPdepenseCtrlPlancoInve is a Querydsl query type for QPdepenseCtrlPlancoInve
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPdepenseCtrlPlancoInve extends com.mysema.query.sql.RelationalPathBase<QPdepenseCtrlPlancoInve> {

    private static final long serialVersionUID = -1804559857;

    public static final QPdepenseCtrlPlancoInve pdepenseCtrlPlancoInve = new QPdepenseCtrlPlancoInve("PDEPENSE_CTRL_PLANCO_INVE");

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final NumberPath<Long> pdpcoId = createNumber("pdpcoId", Long.class);

    public final NumberPath<Long> pdpinId = createNumber("pdpinId", Long.class);

    public final NumberPath<java.math.BigDecimal> pdpinMontantBudgetaire = createNumber("pdpinMontantBudgetaire", java.math.BigDecimal.class);

    public final com.mysema.query.sql.PrimaryKey<QPdepenseCtrlPlancoInve> pdepenseCtrlPlancoInvPk = createPrimaryKey(pdpinId);

    public final com.mysema.query.sql.ForeignKey<QImmInventCompt> pdepenseCtrlPlinvInvcidFk = createForeignKey(invcId, "INVC_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlPlanco> pdepenseCtrlPlinvPdpcoidFk = createForeignKey(pdpcoId, "PDPCO_ID");

    public QPdepenseCtrlPlancoInve(String variable) {
        super(QPdepenseCtrlPlancoInve.class, forVariable(variable), "GFC", "PDEPENSE_CTRL_PLANCO_INVE");
        addMetadata();
    }

    public QPdepenseCtrlPlancoInve(String variable, String schema, String table) {
        super(QPdepenseCtrlPlancoInve.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPdepenseCtrlPlancoInve(Path<? extends QPdepenseCtrlPlancoInve> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PDEPENSE_CTRL_PLANCO_INVE");
        addMetadata();
    }

    public QPdepenseCtrlPlancoInve(PathMetadata<?> metadata) {
        super(QPdepenseCtrlPlancoInve.class, metadata, "GFC", "PDEPENSE_CTRL_PLANCO_INVE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdpcoId, ColumnMetadata.named("PDPCO_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdpinId, ColumnMetadata.named("PDPIN_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdpinMontantBudgetaire, ColumnMetadata.named("PDPIN_MONTANT_BUDGETAIRE").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
    }

}

