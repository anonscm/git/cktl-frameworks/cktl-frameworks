package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QReimputationConvention is a Querydsl query type for QReimputationConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QReimputationConvention extends com.mysema.query.sql.RelationalPathBase<QReimputationConvention> {

    private static final long serialVersionUID = 1498725999;

    public static final QReimputationConvention reimputationConvention = new QReimputationConvention("REIMPUTATION_CONVENTION");

    public final NumberPath<Long> convOrdre = createNumber("convOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> recoHtSaisie = createNumber("recoHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> recoId = createNumber("recoId", Long.class);

    public final NumberPath<java.math.BigDecimal> recoMontantBudgetaire = createNumber("recoMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> recoTtcSaisie = createNumber("recoTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> recoTvaSaisie = createNumber("recoTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QReimputationConvention> reimputationConventionPk = createPrimaryKey(recoId);

    public final com.mysema.query.sql.ForeignKey<QReimputation> reimpConventionReimIdFk = createForeignKey(reimId, "REIM_ID");

    public QReimputationConvention(String variable) {
        super(QReimputationConvention.class, forVariable(variable), "GFC", "REIMPUTATION_CONVENTION");
        addMetadata();
    }

    public QReimputationConvention(String variable, String schema, String table) {
        super(QReimputationConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QReimputationConvention(Path<? extends QReimputationConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REIMPUTATION_CONVENTION");
        addMetadata();
    }

    public QReimputationConvention(PathMetadata<?> metadata) {
        super(QReimputationConvention.class, metadata, "GFC", "REIMPUTATION_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(recoHtSaisie, ColumnMetadata.named("RECO_HT_SAISIE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(recoId, ColumnMetadata.named("RECO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(recoMontantBudgetaire, ColumnMetadata.named("RECO_MONTANT_BUDGETAIRE").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(recoTtcSaisie, ColumnMetadata.named("RECO_TTC_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(recoTvaSaisie, ColumnMetadata.named("RECO_TVA_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

