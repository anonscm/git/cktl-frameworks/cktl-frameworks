package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmNatureDep is a Querydsl query type for QAdmNatureDep
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmNatureDep extends com.mysema.query.sql.RelationalPathBase<QAdmNatureDep> {

    private static final long serialVersionUID = 796243935;

    public static final QAdmNatureDep admNatureDep = new QAdmNatureDep("ADM_NATURE_DEP");

    public final StringPath code = createString("code");

    public final NumberPath<Integer> fongible = createNumber("fongible", Integer.class);

    public final NumberPath<Long> idAdmNatureDep = createNumber("idAdmNatureDep", Long.class);

    public final StringPath libelle = createString("libelle");

    public final com.mysema.query.sql.PrimaryKey<QAdmNatureDep> admNatureDepPk = createPrimaryKey(idAdmNatureDep);

    public final com.mysema.query.sql.ForeignKey<QAdmEbSignataireNatdep> _ebSignataireNdNatdepFk = createInvForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBudDepCp> _opeTrancheBudDepCpNatFk = createInvForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public final com.mysema.query.sql.ForeignKey<QBudPrevOpeTraDepAe> _budPrevOpeAeNatFk = createInvForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public final com.mysema.query.sql.ForeignKey<QOpeTrancheBudDepAe> _opeTrancheBudDepAeNatFk = createInvForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public final com.mysema.query.sql.ForeignKey<QBudPrevHorsOpDep> _budPrevHorsOpDepNatFk = createInvForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public final com.mysema.query.sql.ForeignKey<QBudBudgetDep> _budBudgetDepNatFk = createInvForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public final com.mysema.query.sql.ForeignKey<QBudPrevOpeTraDepCp> _budPrevOpeCpNatFk = createInvForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public final com.mysema.query.sql.ForeignKey<QAdmNatureDepExercice> _admNatureDepFk = createInvForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public final com.mysema.query.sql.ForeignKey<QCptbudEcriture> _cptbudEcrNatdFk = createInvForeignKey(idAdmNatureDep, "ID_ADM_NATURE_DEP");

    public QAdmNatureDep(String variable) {
        super(QAdmNatureDep.class, forVariable(variable), "GFC", "ADM_NATURE_DEP");
        addMetadata();
    }

    public QAdmNatureDep(String variable, String schema, String table) {
        super(QAdmNatureDep.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmNatureDep(Path<? extends QAdmNatureDep> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_NATURE_DEP");
        addMetadata();
    }

    public QAdmNatureDep(PathMetadata<?> metadata) {
        super(QAdmNatureDep.class, metadata, "GFC", "ADM_NATURE_DEP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(code, ColumnMetadata.named("CODE").withIndex(2).ofType(Types.VARCHAR).withSize(5).notNull());
        addMetadata(fongible, ColumnMetadata.named("FONGIBLE").withIndex(4).ofType(Types.DECIMAL).withSize(1).notNull());
        addMetadata(idAdmNatureDep, ColumnMetadata.named("ID_ADM_NATURE_DEP").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(500).notNull());
    }

}

