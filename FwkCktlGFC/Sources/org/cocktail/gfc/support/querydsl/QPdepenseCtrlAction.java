package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPdepenseCtrlAction is a Querydsl query type for QPdepenseCtrlAction
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPdepenseCtrlAction extends com.mysema.query.sql.RelationalPathBase<QPdepenseCtrlAction> {

    private static final long serialVersionUID = -1482509892;

    public static final QPdepenseCtrlAction pdepenseCtrlAction = new QPdepenseCtrlAction("PDEPENSE_CTRL_ACTION");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<java.math.BigDecimal> pdactHtSaisie = createNumber("pdactHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> pdactId = createNumber("pdactId", Long.class);

    public final NumberPath<java.math.BigDecimal> pdactMontantBudgetaire = createNumber("pdactMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdactTtcSaisie = createNumber("pdactTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdactTvaSaisie = createNumber("pdactTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> pdepId = createNumber("pdepId", Long.class);

    public final NumberPath<Long> tyacId = createNumber("tyacId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QPdepenseCtrlAction> pdepenseCtrlActionPk = createPrimaryKey(pdactId);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> pdepenseCtrlActExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseBudget> pdepenseCtrlActPdepIdFk = createForeignKey(pdepId, "PDEP_ID");

    public QPdepenseCtrlAction(String variable) {
        super(QPdepenseCtrlAction.class, forVariable(variable), "GFC", "PDEPENSE_CTRL_ACTION");
        addMetadata();
    }

    public QPdepenseCtrlAction(String variable, String schema, String table) {
        super(QPdepenseCtrlAction.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPdepenseCtrlAction(Path<? extends QPdepenseCtrlAction> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PDEPENSE_CTRL_ACTION");
        addMetadata();
    }

    public QPdepenseCtrlAction(PathMetadata<?> metadata) {
        super(QPdepenseCtrlAction.class, metadata, "GFC", "PDEPENSE_CTRL_ACTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(pdactHtSaisie, ColumnMetadata.named("PDACT_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdactId, ColumnMetadata.named("PDACT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdactMontantBudgetaire, ColumnMetadata.named("PDACT_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdactTtcSaisie, ColumnMetadata.named("PDACT_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdactTvaSaisie, ColumnMetadata.named("PDACT_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdepId, ColumnMetadata.named("PDEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyacId, ColumnMetadata.named("TYAC_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

