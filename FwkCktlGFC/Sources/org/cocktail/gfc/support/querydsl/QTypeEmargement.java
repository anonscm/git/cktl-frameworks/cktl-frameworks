package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTypeEmargement is a Querydsl query type for QTypeEmargement
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QTypeEmargement extends com.mysema.query.sql.RelationalPathBase<QTypeEmargement> {

    private static final long serialVersionUID = 377295216;

    public static final QTypeEmargement typeEmargement = new QTypeEmargement("TYPE_EMARGEMENT");

    public final StringPath temLibelle = createString("temLibelle");

    public final NumberPath<Long> temOrdre = createNumber("temOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QTypeEmargement> typeEmargementPk = createPrimaryKey(temOrdre);

    public final com.mysema.query.sql.ForeignKey<QEmargement> _emargementTemOrdreFk = createInvForeignKey(temOrdre, "TEM_ORDRE");

    public QTypeEmargement(String variable) {
        super(QTypeEmargement.class, forVariable(variable), "GFC", "TYPE_EMARGEMENT");
        addMetadata();
    }

    public QTypeEmargement(String variable, String schema, String table) {
        super(QTypeEmargement.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTypeEmargement(Path<? extends QTypeEmargement> path) {
        super(path.getType(), path.getMetadata(), "GFC", "TYPE_EMARGEMENT");
        addMetadata();
    }

    public QTypeEmargement(PathMetadata<?> metadata) {
        super(QTypeEmargement.class, metadata, "GFC", "TYPE_EMARGEMENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(temLibelle, ColumnMetadata.named("TEM_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(30).notNull());
        addMetadata(temOrdre, ColumnMetadata.named("TEM_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

