package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVEcritureDetailRa is a Querydsl query type for QVEcritureDetailRa
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVEcritureDetailRa extends com.mysema.query.sql.RelationalPathBase<QVEcritureDetailRa> {

    private static final long serialVersionUID = 56934526;

    public static final QVEcritureDetailRa vEcritureDetailRa = new QVEcritureDetailRa("V_ECRITURE_DETAIL_RA");

    public final NumberPath<Long> convOrdre = createNumber("convOrdre", Long.class);

    public final StringPath ecdCommentaire = createString("ecdCommentaire");

    public final NumberPath<java.math.BigDecimal> ecdCredit = createNumber("ecdCredit", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> ecdDebit = createNumber("ecdDebit", java.math.BigDecimal.class);

    public final NumberPath<Long> ecdIndex = createNumber("ecdIndex", Long.class);

    public final StringPath ecdLibelle = createString("ecdLibelle");

    public final NumberPath<java.math.BigDecimal> ecdMontant = createNumber("ecdMontant", java.math.BigDecimal.class);

    public final NumberPath<Long> ecdOrdre = createNumber("ecdOrdre", Long.class);

    public final StringPath ecdPostit = createString("ecdPostit");

    public final NumberPath<java.math.BigDecimal> ecdResteEmarger = createNumber("ecdResteEmarger", java.math.BigDecimal.class);

    public final StringPath ecdSens = createString("ecdSens");

    public final DateTimePath<java.sql.Timestamp> ecrDate = createDateTime("ecrDate", java.sql.Timestamp.class);

    public final NumberPath<Long> ecrNumero = createNumber("ecrNumero", Long.class);

    public final NumberPath<Long> ecrOrdre = createNumber("ecrOrdre", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tyorId = createNumber("tyorId", Long.class);

    public QVEcritureDetailRa(String variable) {
        super(QVEcritureDetailRa.class, forVariable(variable), "GFC", "V_ECRITURE_DETAIL_RA");
        addMetadata();
    }

    public QVEcritureDetailRa(String variable, String schema, String table) {
        super(QVEcritureDetailRa.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVEcritureDetailRa(Path<? extends QVEcritureDetailRa> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ECRITURE_DETAIL_RA");
        addMetadata();
    }

    public QVEcritureDetailRa(PathMetadata<?> metadata) {
        super(QVEcritureDetailRa.class, metadata, "GFC", "V_ECRITURE_DETAIL_RA");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(19).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ecdCommentaire, ColumnMetadata.named("ECD_COMMENTAIRE").withIndex(2).ofType(Types.VARCHAR).withSize(200));
        addMetadata(ecdCredit, ColumnMetadata.named("ECD_CREDIT").withIndex(3).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(ecdDebit, ColumnMetadata.named("ECD_DEBIT").withIndex(4).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(ecdIndex, ColumnMetadata.named("ECD_INDEX").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ecdLibelle, ColumnMetadata.named("ECD_LIBELLE").withIndex(6).ofType(Types.VARCHAR).withSize(200));
        addMetadata(ecdMontant, ColumnMetadata.named("ECD_MONTANT").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(ecdOrdre, ColumnMetadata.named("ECD_ORDRE").withIndex(8).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ecdPostit, ColumnMetadata.named("ECD_POSTIT").withIndex(9).ofType(Types.VARCHAR).withSize(200));
        addMetadata(ecdResteEmarger, ColumnMetadata.named("ECD_RESTE_EMARGER").withIndex(10).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(ecdSens, ColumnMetadata.named("ECD_SENS").withIndex(11).ofType(Types.VARCHAR).withSize(1));
        addMetadata(ecrDate, ColumnMetadata.named("ECR_DATE").withIndex(13).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(ecrNumero, ColumnMetadata.named("ECR_NUMERO").withIndex(14).ofType(Types.DECIMAL).withSize(32));
        addMetadata(ecrOrdre, ColumnMetadata.named("ECR_ORDRE").withIndex(12).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(15).ofType(Types.DECIMAL).withSize(4));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(16).ofType(Types.VARCHAR).withSize(10));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(17).ofType(Types.VARCHAR).withSize(20));
        addMetadata(tyorId, ColumnMetadata.named("TYOR_ID").withIndex(18).ofType(Types.DECIMAL).withSize(0));
    }

}

