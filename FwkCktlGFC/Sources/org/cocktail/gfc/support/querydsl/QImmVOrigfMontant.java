package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVOrigfMontant is a Querydsl query type for QImmVOrigfMontant
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVOrigfMontant extends com.mysema.query.sql.RelationalPathBase<QImmVOrigfMontant> {

    private static final long serialVersionUID = 65090888;

    public static final QImmVOrigfMontant immVOrigfMontant = new QImmVOrigfMontant("IMM_V_ORIGF_MONTANT");

    public final NumberPath<Long> clicId = createNumber("clicId", Long.class);

    public final StringPath montant = createString("montant");

    public final StringPath orgfLibelle = createString("orgfLibelle");

    public final NumberPath<Long> titId = createNumber("titId", Long.class);

    public QImmVOrigfMontant(String variable) {
        super(QImmVOrigfMontant.class, forVariable(variable), "GFC", "IMM_V_ORIGF_MONTANT");
        addMetadata();
    }

    public QImmVOrigfMontant(String variable, String schema, String table) {
        super(QImmVOrigfMontant.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVOrigfMontant(Path<? extends QImmVOrigfMontant> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_ORIGF_MONTANT");
        addMetadata();
    }

    public QImmVOrigfMontant(PathMetadata<?> metadata) {
        super(QImmVOrigfMontant.class, metadata, "GFC", "IMM_V_ORIGF_MONTANT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clicId, ColumnMetadata.named("CLIC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(4).ofType(Types.VARCHAR).withSize(40));
        addMetadata(orgfLibelle, ColumnMetadata.named("ORGF_LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(200));
        addMetadata(titId, ColumnMetadata.named("TIT_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0));
    }

}

