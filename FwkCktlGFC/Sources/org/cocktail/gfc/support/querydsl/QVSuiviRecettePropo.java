package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviRecettePropo is a Querydsl query type for QVSuiviRecettePropo
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviRecettePropo extends com.mysema.query.sql.RelationalPathBase<QVSuiviRecettePropo> {

    private static final long serialVersionUID = 1907025057;

    public static final QVSuiviRecettePropo vSuiviRecettePropo = new QVSuiviRecettePropo("V_SUIVI_RECETTE_PROPO");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> totalPropo = createSimple("totalPropo", Object.class);

    public QVSuiviRecettePropo(String variable) {
        super(QVSuiviRecettePropo.class, forVariable(variable), "GFC", "V_SUIVI_RECETTE_PROPO");
        addMetadata();
    }

    public QVSuiviRecettePropo(String variable, String schema, String table) {
        super(QVSuiviRecettePropo.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviRecettePropo(Path<? extends QVSuiviRecettePropo> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_RECETTE_PROPO");
        addMetadata();
    }

    public QVSuiviRecettePropo(PathMetadata<?> metadata) {
        super(QVSuiviRecettePropo.class, metadata, "GFC", "V_SUIVI_RECETTE_PROPO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPropo, ColumnMetadata.named("TOTAL_PROPO").withIndex(4).ofType(Types.OTHER).withSize(0));
    }

}

