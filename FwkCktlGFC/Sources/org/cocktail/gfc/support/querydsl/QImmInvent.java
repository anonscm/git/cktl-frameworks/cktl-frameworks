package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmInvent is a Querydsl query type for QImmInvent
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmInvent extends com.mysema.query.sql.RelationalPathBase<QImmInvent> {

    private static final long serialVersionUID = 1765410978;

    public static final QImmInvent immInvent = new QImmInvent("IMM_INVENT");

    public final NumberPath<Long> clipId = createNumber("clipId", Long.class);

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final StringPath invCommentaire = createString("invCommentaire");

    public final DateTimePath<java.sql.Timestamp> invDateMaint = createDateTime("invDateMaint", java.sql.Timestamp.class);

    public final StringPath invDoc = createString("invDoc");

    public final StringPath invGar = createString("invGar");

    public final NumberPath<Long> invId = createNumber("invId", Long.class);

    public final StringPath invMaint = createString("invMaint");

    public final StringPath invModele = createString("invModele");

    public final NumberPath<java.math.BigDecimal> invMontantAcquisition = createNumber("invMontantAcquisition", java.math.BigDecimal.class);

    public final StringPath invSerie = createString("invSerie");

    public final NumberPath<Long> lidOrdre = createNumber("lidOrdre", Long.class);

    public final NumberPath<Long> magId = createNumber("magId", Long.class);

    public final NumberPath<Long> noIndividuResp = createNumber("noIndividuResp", Long.class);

    public final NumberPath<Long> noIndividuTitulaire = createNumber("noIndividuTitulaire", Long.class);

    public final NumberPath<Long> salNumero = createNumber("salNumero", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmInvent> immInventPk = createPrimaryKey(invId);

    public final com.mysema.query.sql.ForeignKey<QSalles> salleFk = createForeignKey(salNumero, "SAL_NUMERO");

    public final com.mysema.query.sql.ForeignKey<QImmInventCompt> inventairecomptableFk2 = createForeignKey(invcId, "INVC_ID");

    public final com.mysema.query.sql.ForeignKey<QImmLivraisonDetail> livraisondetailFk = createForeignKey(lidOrdre, "LID_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QImmCleInventPhysique> cleinventairephysiqueFk = createForeignKey(clipId, "CLIP_ID");

    public final com.mysema.query.sql.ForeignKey<QImmMagasin> magasinFk = createForeignKey(magId, "MAG_ID");

    public final com.mysema.query.sql.ForeignKey<QImmInventSortie> _sortieInvIdFk = createInvForeignKey(invId, "INV_ID");

    public final com.mysema.query.sql.ForeignKey<QImmInventCb> _inventaireInvIdFk = createInvForeignKey(invId, "INV_ID");

    public QImmInvent(String variable) {
        super(QImmInvent.class, forVariable(variable), "GFC", "IMM_INVENT");
        addMetadata();
    }

    public QImmInvent(String variable, String schema, String table) {
        super(QImmInvent.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmInvent(Path<? extends QImmInvent> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_INVENT");
        addMetadata();
    }

    public QImmInvent(PathMetadata<?> metadata) {
        super(QImmInvent.class, metadata, "GFC", "IMM_INVENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(clipId, ColumnMetadata.named("CLIP_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(11).ofType(Types.DECIMAL).withSize(0));
        addMetadata(invCommentaire, ColumnMetadata.named("INV_COMMENTAIRE").withIndex(3).ofType(Types.VARCHAR).withSize(2000));
        addMetadata(invDateMaint, ColumnMetadata.named("INV_DATE_MAINT").withIndex(4).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(invDoc, ColumnMetadata.named("INV_DOC").withIndex(5).ofType(Types.VARCHAR).withSize(100));
        addMetadata(invGar, ColumnMetadata.named("INV_GAR").withIndex(6).ofType(Types.VARCHAR).withSize(100));
        addMetadata(invId, ColumnMetadata.named("INV_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(invMaint, ColumnMetadata.named("INV_MAINT").withIndex(7).ofType(Types.VARCHAR).withSize(100));
        addMetadata(invModele, ColumnMetadata.named("INV_MODELE").withIndex(8).ofType(Types.VARCHAR).withSize(200));
        addMetadata(invMontantAcquisition, ColumnMetadata.named("INV_MONTANT_ACQUISITION").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(invSerie, ColumnMetadata.named("INV_SERIE").withIndex(10).ofType(Types.VARCHAR).withSize(100));
        addMetadata(lidOrdre, ColumnMetadata.named("LID_ORDRE").withIndex(12).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(magId, ColumnMetadata.named("MAG_ID").withIndex(13).ofType(Types.DECIMAL).withSize(0));
        addMetadata(noIndividuResp, ColumnMetadata.named("NO_INDIVIDU_RESP").withIndex(14).ofType(Types.DECIMAL).withSize(0));
        addMetadata(noIndividuTitulaire, ColumnMetadata.named("NO_INDIVIDU_TITULAIRE").withIndex(15).ofType(Types.DECIMAL).withSize(0));
        addMetadata(salNumero, ColumnMetadata.named("SAL_NUMERO").withIndex(16).ofType(Types.DECIMAL).withSize(0));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(17).ofType(Types.DECIMAL).withSize(0));
    }

}

