package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPdepenseCtrlAnalytique is a Querydsl query type for QPdepenseCtrlAnalytique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QPdepenseCtrlAnalytique extends com.mysema.query.sql.RelationalPathBase<QPdepenseCtrlAnalytique> {

    private static final long serialVersionUID = 683608817;

    public static final QPdepenseCtrlAnalytique pdepenseCtrlAnalytique = new QPdepenseCtrlAnalytique("PDEPENSE_CTRL_ANALYTIQUE");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<java.math.BigDecimal> pdanaHtSaisie = createNumber("pdanaHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> pdanaId = createNumber("pdanaId", Long.class);

    public final NumberPath<java.math.BigDecimal> pdanaMontantBudgetaire = createNumber("pdanaMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdanaTtcSaisie = createNumber("pdanaTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> pdanaTvaSaisie = createNumber("pdanaTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> pdepId = createNumber("pdepId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QPdepenseCtrlAnalytique> pdepenseCtrlAnalytiquePk = createPrimaryKey(pdanaId);

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> pdepenseCtrlAnalCanIdFk = createForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseBudget> pdepenseCtrlAnalPdepIdFk = createForeignKey(pdepId, "PDEP_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> pdepenseCtrlAnaExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QPdepenseCtrlAnalytique(String variable) {
        super(QPdepenseCtrlAnalytique.class, forVariable(variable), "GFC", "PDEPENSE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public QPdepenseCtrlAnalytique(String variable, String schema, String table) {
        super(QPdepenseCtrlAnalytique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPdepenseCtrlAnalytique(Path<? extends QPdepenseCtrlAnalytique> path) {
        super(path.getType(), path.getMetadata(), "GFC", "PDEPENSE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public QPdepenseCtrlAnalytique(PathMetadata<?> metadata) {
        super(QPdepenseCtrlAnalytique.class, metadata, "GFC", "PDEPENSE_CTRL_ANALYTIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(pdanaHtSaisie, ColumnMetadata.named("PDANA_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdanaId, ColumnMetadata.named("PDANA_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pdanaMontantBudgetaire, ColumnMetadata.named("PDANA_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdanaTtcSaisie, ColumnMetadata.named("PDANA_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdanaTvaSaisie, ColumnMetadata.named("PDANA_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(pdepId, ColumnMetadata.named("PDEP_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

