package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeCtrlAction is a Querydsl query type for QCommandeCtrlAction
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeCtrlAction extends com.mysema.query.sql.RelationalPathBase<QCommandeCtrlAction> {

    private static final long serialVersionUID = 665033052;

    public static final QCommandeCtrlAction commandeCtrlAction = new QCommandeCtrlAction("COMMANDE_CTRL_ACTION");

    public final NumberPath<java.math.BigDecimal> cactHtSaisie = createNumber("cactHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> cactId = createNumber("cactId", Long.class);

    public final NumberPath<java.math.BigDecimal> cactMontantBudgetaire = createNumber("cactMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<Double> cactPourcentage = createNumber("cactPourcentage", Double.class);

    public final NumberPath<java.math.BigDecimal> cactTtcSaisie = createNumber("cactTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> cactTvaSaisie = createNumber("cactTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> cbudId = createNumber("cbudId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> tyacId = createNumber("tyacId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QCommandeCtrlAction> commandeCtrlActionPk = createPrimaryKey(cactId);

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> commandeCtrlActExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeBudget> commandeCtrlActCbudIdFk = createForeignKey(cbudId, "CBUD_ID");

    public QCommandeCtrlAction(String variable) {
        super(QCommandeCtrlAction.class, forVariable(variable), "GFC", "COMMANDE_CTRL_ACTION");
        addMetadata();
    }

    public QCommandeCtrlAction(String variable, String schema, String table) {
        super(QCommandeCtrlAction.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeCtrlAction(Path<? extends QCommandeCtrlAction> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_CTRL_ACTION");
        addMetadata();
    }

    public QCommandeCtrlAction(PathMetadata<?> metadata) {
        super(QCommandeCtrlAction.class, metadata, "GFC", "COMMANDE_CTRL_ACTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cactHtSaisie, ColumnMetadata.named("CACT_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cactId, ColumnMetadata.named("CACT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cactMontantBudgetaire, ColumnMetadata.named("CACT_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cactPourcentage, ColumnMetadata.named("CACT_POURCENTAGE").withIndex(6).ofType(Types.DECIMAL).withSize(15).withDigits(5).notNull());
        addMetadata(cactTtcSaisie, ColumnMetadata.named("CACT_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cactTvaSaisie, ColumnMetadata.named("CACT_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cbudId, ColumnMetadata.named("CBUD_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(tyacId, ColumnMetadata.named("TYAC_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

