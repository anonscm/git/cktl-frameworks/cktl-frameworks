package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEchEcheancierType is a Querydsl query type for QEchEcheancierType
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEchEcheancierType extends com.mysema.query.sql.RelationalPathBase<QEchEcheancierType> {

    private static final long serialVersionUID = 797104190;

    public static final QEchEcheancierType echEcheancierType = new QEchEcheancierType("ECH_ECHEANCIER_TYPE");

    public final StringPath echtCode = createString("echtCode");

    public final NumberPath<Long> echtId = createNumber("echtId", Long.class);

    public final StringPath echtLibelle = createString("echtLibelle");

    public final com.mysema.query.sql.PrimaryKey<QEchEcheancierType> echEcheancierTypePk = createPrimaryKey(echtId);

    public final com.mysema.query.sql.ForeignKey<QEchEcheancier> _echEcheancierTypeFkFk = createInvForeignKey(echtId, "ECHT_ID");

    public QEchEcheancierType(String variable) {
        super(QEchEcheancierType.class, forVariable(variable), "GFC", "ECH_ECHEANCIER_TYPE");
        addMetadata();
    }

    public QEchEcheancierType(String variable, String schema, String table) {
        super(QEchEcheancierType.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEchEcheancierType(Path<? extends QEchEcheancierType> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ECH_ECHEANCIER_TYPE");
        addMetadata();
    }

    public QEchEcheancierType(PathMetadata<?> metadata) {
        super(QEchEcheancierType.class, metadata, "GFC", "ECH_ECHEANCIER_TYPE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(echtCode, ColumnMetadata.named("ECHT_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(echtId, ColumnMetadata.named("ECHT_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(echtLibelle, ColumnMetadata.named("ECHT_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(1000));
    }

}

