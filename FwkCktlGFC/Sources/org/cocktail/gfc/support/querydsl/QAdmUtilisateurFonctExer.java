package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmUtilisateurFonctExer is a Querydsl query type for QAdmUtilisateurFonctExer
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmUtilisateurFonctExer extends com.mysema.query.sql.RelationalPathBase<QAdmUtilisateurFonctExer> {

    private static final long serialVersionUID = 322150172;

    public static final QAdmUtilisateurFonctExer admUtilisateurFonctExer = new QAdmUtilisateurFonctExer("ADM_UTILISATEUR_FONCT_EXER");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> ufeId = createNumber("ufeId", Long.class);

    public final NumberPath<Long> ufOrdre = createNumber("ufOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmUtilisateurFonctExer> sysC0075325 = createPrimaryKey(ufeId);

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateurFonct> admUfeUfOrdreFk = createForeignKey(ufOrdre, "UF_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> admUfeExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QAdmUtilisateurFonctExer(String variable) {
        super(QAdmUtilisateurFonctExer.class, forVariable(variable), "GFC", "ADM_UTILISATEUR_FONCT_EXER");
        addMetadata();
    }

    public QAdmUtilisateurFonctExer(String variable, String schema, String table) {
        super(QAdmUtilisateurFonctExer.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmUtilisateurFonctExer(Path<? extends QAdmUtilisateurFonctExer> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_UTILISATEUR_FONCT_EXER");
        addMetadata();
    }

    public QAdmUtilisateurFonctExer(PathMetadata<?> metadata) {
        super(QAdmUtilisateurFonctExer.class, metadata, "GFC", "ADM_UTILISATEUR_FONCT_EXER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(ufeId, ColumnMetadata.named("UFE_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ufOrdre, ColumnMetadata.named("UF_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

