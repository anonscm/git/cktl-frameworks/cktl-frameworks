package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBoutiqueClient is a Querydsl query type for QBoutiqueClient
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QBoutiqueClient extends com.mysema.query.sql.RelationalPathBase<QBoutiqueClient> {

    private static final long serialVersionUID = -1612458096;

    public static final QBoutiqueClient boutiqueClient = new QBoutiqueClient("BOUTIQUE_CLIENT");

    public final NumberPath<Long> boutiqueId = createNumber("boutiqueId", Long.class);

    public final DateTimePath<java.sql.Timestamp> dateDebut = createDateTime("dateDebut", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dateFin = createDateTime("dateFin", java.sql.Timestamp.class);

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QBoutiqueClient> boutiqueClientPk = createPrimaryKey(boutiqueId, persId);

    public final com.mysema.query.sql.ForeignKey<QPersonne> boutiqueClientPersonneFk1 = createForeignKey(persId, "PERS_ID");

    public final com.mysema.query.sql.ForeignKey<QBoutique> boutiqueClientBoutiqueFk1 = createForeignKey(boutiqueId, "BOUTIQUE_ID");

    public QBoutiqueClient(String variable) {
        super(QBoutiqueClient.class, forVariable(variable), "GFC", "BOUTIQUE_CLIENT");
        addMetadata();
    }

    public QBoutiqueClient(String variable, String schema, String table) {
        super(QBoutiqueClient.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBoutiqueClient(Path<? extends QBoutiqueClient> path) {
        super(path.getType(), path.getMetadata(), "GFC", "BOUTIQUE_CLIENT");
        addMetadata();
    }

    public QBoutiqueClient(PathMetadata<?> metadata) {
        super(QBoutiqueClient.class, metadata, "GFC", "BOUTIQUE_CLIENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(boutiqueId, ColumnMetadata.named("BOUTIQUE_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(dateDebut, ColumnMetadata.named("DATE_DEBUT").withIndex(3).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dateFin, ColumnMetadata.named("DATE_FIN").withIndex(4).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

