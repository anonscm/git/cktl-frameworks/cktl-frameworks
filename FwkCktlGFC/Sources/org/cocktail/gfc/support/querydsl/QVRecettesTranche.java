package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVRecettesTranche is a Querydsl query type for QVRecettesTranche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVRecettesTranche extends com.mysema.query.sql.RelationalPathBase<QVRecettesTranche> {

    private static final long serialVersionUID = -223710915;

    public static final QVRecettesTranche vRecettesTranche = new QVRecettesTranche("V_RECETTES_TRANCHE");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> montant = createSimple("montant", Object.class);

    public final SimplePath<Object> pcoNum = createSimple("pcoNum", Object.class);

    public final SimplePath<Object> traOrdre = createSimple("traOrdre", Object.class);

    public QVRecettesTranche(String variable) {
        super(QVRecettesTranche.class, forVariable(variable), "GFC", "V_RECETTES_TRANCHE");
        addMetadata();
    }

    public QVRecettesTranche(String variable, String schema, String table) {
        super(QVRecettesTranche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVRecettesTranche(Path<? extends QVRecettesTranche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_RECETTES_TRANCHE");
        addMetadata();
    }

    public QVRecettesTranche(PathMetadata<?> metadata) {
        super(QVRecettesTranche.class, metadata, "GFC", "V_RECETTES_TRANCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(traOrdre, ColumnMetadata.named("TRA_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
    }

}

