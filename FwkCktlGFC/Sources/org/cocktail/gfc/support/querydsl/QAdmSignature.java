package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmSignature is a Querydsl query type for QAdmSignature
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmSignature extends com.mysema.query.sql.RelationalPathBase<QAdmSignature> {

    private static final long serialVersionUID = -1756735825;

    public static final QAdmSignature admSignature = new QAdmSignature("ADM_SIGNATURE");

    public final NumberPath<Long> noIndividu = createNumber("noIndividu", Long.class);

    public final DateTimePath<java.sql.Timestamp> signDateModification = createDateTime("signDateModification", java.sql.Timestamp.class);

    public final SimplePath<java.sql.Blob> signImg = createSimple("signImg", java.sql.Blob.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmSignature> signaturePk = createPrimaryKey(noIndividu);

    public final com.mysema.query.sql.ForeignKey<QIndividuUlr> signatureNoIndividuFk = createForeignKey(noIndividu, "NO_INDIVIDU");

    public QAdmSignature(String variable) {
        super(QAdmSignature.class, forVariable(variable), "GFC", "ADM_SIGNATURE");
        addMetadata();
    }

    public QAdmSignature(String variable, String schema, String table) {
        super(QAdmSignature.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmSignature(Path<? extends QAdmSignature> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_SIGNATURE");
        addMetadata();
    }

    public QAdmSignature(PathMetadata<?> metadata) {
        super(QAdmSignature.class, metadata, "GFC", "ADM_SIGNATURE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(noIndividu, ColumnMetadata.named("NO_INDIVIDU").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(signDateModification, ColumnMetadata.named("SIGN_DATE_MODIFICATION").withIndex(3).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(signImg, ColumnMetadata.named("SIGN_IMG").withIndex(2).ofType(Types.BLOB).withSize(4000).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

