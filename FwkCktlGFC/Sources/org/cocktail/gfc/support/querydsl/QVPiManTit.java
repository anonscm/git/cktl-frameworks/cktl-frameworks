package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVPiManTit is a Querydsl query type for QVPiManTit
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVPiManTit extends com.mysema.query.sql.RelationalPathBase<QVPiManTit> {

    private static final long serialVersionUID = 1588655859;

    public static final QVPiManTit vPiManTit = new QVPiManTit("V_PI_MAN_TIT");

    public final NumberPath<Long> borIdDep = createNumber("borIdDep", Long.class);

    public final NumberPath<Long> borIdRec = createNumber("borIdRec", Long.class);

    public final NumberPath<Long> manId = createNumber("manId", Long.class);

    public final NumberPath<Long> prestId = createNumber("prestId", Long.class);

    public final NumberPath<Long> titId = createNumber("titId", Long.class);

    public QVPiManTit(String variable) {
        super(QVPiManTit.class, forVariable(variable), "GFC", "V_PI_MAN_TIT");
        addMetadata();
    }

    public QVPiManTit(String variable, String schema, String table) {
        super(QVPiManTit.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVPiManTit(Path<? extends QVPiManTit> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_PI_MAN_TIT");
        addMetadata();
    }

    public QVPiManTit(PathMetadata<?> metadata) {
        super(QVPiManTit.class, metadata, "GFC", "V_PI_MAN_TIT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(borIdDep, ColumnMetadata.named("BOR_ID_DEP").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(borIdRec, ColumnMetadata.named("BOR_ID_REC").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(manId, ColumnMetadata.named("MAN_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(prestId, ColumnMetadata.named("PREST_ID").withIndex(5).ofType(Types.DECIMAL).withSize(0));
        addMetadata(titId, ColumnMetadata.named("TIT_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

