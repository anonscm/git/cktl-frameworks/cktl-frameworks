package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCommandeCtrlPlanco is a Querydsl query type for QCommandeCtrlPlanco
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QCommandeCtrlPlanco extends com.mysema.query.sql.RelationalPathBase<QCommandeCtrlPlanco> {

    private static final long serialVersionUID = 1102220411;

    public static final QCommandeCtrlPlanco commandeCtrlPlanco = new QCommandeCtrlPlanco("COMMANDE_CTRL_PLANCO");

    public final NumberPath<Long> cbudId = createNumber("cbudId", Long.class);

    public final NumberPath<java.math.BigDecimal> cpcoHtSaisie = createNumber("cpcoHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> cpcoId = createNumber("cpcoId", Long.class);

    public final NumberPath<java.math.BigDecimal> cpcoMontantBudgetaire = createNumber("cpcoMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<Double> cpcoPourcentage = createNumber("cpcoPourcentage", Double.class);

    public final NumberPath<java.math.BigDecimal> cpcoTtcSaisie = createNumber("cpcoTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> cpcoTvaSaisie = createNumber("cpcoTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final com.mysema.query.sql.PrimaryKey<QCommandeCtrlPlanco> commandeCtrlPlancoPk = createPrimaryKey(cpcoId);

    public final com.mysema.query.sql.ForeignKey<QCommandeBudget> commandeCtrlPlCbudIdFk = createForeignKey(cbudId, "CBUD_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> commandeCtrlPlExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public QCommandeCtrlPlanco(String variable) {
        super(QCommandeCtrlPlanco.class, forVariable(variable), "GFC", "COMMANDE_CTRL_PLANCO");
        addMetadata();
    }

    public QCommandeCtrlPlanco(String variable, String schema, String table) {
        super(QCommandeCtrlPlanco.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCommandeCtrlPlanco(Path<? extends QCommandeCtrlPlanco> path) {
        super(path.getType(), path.getMetadata(), "GFC", "COMMANDE_CTRL_PLANCO");
        addMetadata();
    }

    public QCommandeCtrlPlanco(PathMetadata<?> metadata) {
        super(QCommandeCtrlPlanco.class, metadata, "GFC", "COMMANDE_CTRL_PLANCO");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cbudId, ColumnMetadata.named("CBUD_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cpcoHtSaisie, ColumnMetadata.named("CPCO_HT_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cpcoId, ColumnMetadata.named("CPCO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cpcoMontantBudgetaire, ColumnMetadata.named("CPCO_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cpcoPourcentage, ColumnMetadata.named("CPCO_POURCENTAGE").withIndex(6).ofType(Types.DECIMAL).withSize(15).withDigits(5).notNull());
        addMetadata(cpcoTtcSaisie, ColumnMetadata.named("CPCO_TTC_SAISIE").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(cpcoTvaSaisie, ColumnMetadata.named("CPCO_TVA_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
    }

}

