package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmInventNonBud is a Querydsl query type for QImmInventNonBud
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmInventNonBud extends com.mysema.query.sql.RelationalPathBase<QImmInventNonBud> {

    private static final long serialVersionUID = 1286520742;

    public static final QImmInventNonBud immInventNonBud = new QImmInventNonBud("IMM_INVENT_NON_BUD");

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final DateTimePath<java.sql.Timestamp> inbDateAcquisition = createDateTime("inbDateAcquisition", java.sql.Timestamp.class);

    public final NumberPath<Long> inbDuree = createNumber("inbDuree", Long.class);

    public final StringPath inbFacture = createString("inbFacture");

    public final StringPath inbFournisseur = createString("inbFournisseur");

    public final NumberPath<Long> inbId = createNumber("inbId", Long.class);

    public final StringPath inbInformations = createString("inbInformations");

    public final NumberPath<java.math.BigDecimal> inbMontant = createNumber("inbMontant", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> inbMontantResiduel = createNumber("inbMontantResiduel", java.math.BigDecimal.class);

    public final StringPath inbNumeroSerie = createString("inbNumeroSerie");

    public final StringPath inbTypeAmort = createString("inbTypeAmort");

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public final NumberPath<Long> origId = createNumber("origId", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmInventNonBud> immInventNonBudPk = createPrimaryKey(inbId);

    public QImmInventNonBud(String variable) {
        super(QImmInventNonBud.class, forVariable(variable), "GFC", "IMM_INVENT_NON_BUD");
        addMetadata();
    }

    public QImmInventNonBud(String variable, String schema, String table) {
        super(QImmInventNonBud.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmInventNonBud(Path<? extends QImmInventNonBud> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_INVENT_NON_BUD");
        addMetadata();
    }

    public QImmInventNonBud(PathMetadata<?> metadata) {
        super(QImmInventNonBud.class, metadata, "GFC", "IMM_INVENT_NON_BUD");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(3).ofType(Types.DECIMAL).withSize(4).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(inbDateAcquisition, ColumnMetadata.named("INB_DATE_ACQUISITION").withIndex(16).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(inbDuree, ColumnMetadata.named("INB_DUREE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(inbFacture, ColumnMetadata.named("INB_FACTURE").withIndex(14).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(inbFournisseur, ColumnMetadata.named("INB_FOURNISSEUR").withIndex(13).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(inbId, ColumnMetadata.named("INB_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(inbInformations, ColumnMetadata.named("INB_INFORMATIONS").withIndex(12).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(inbMontant, ColumnMetadata.named("INB_MONTANT").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(inbMontantResiduel, ColumnMetadata.named("INB_MONTANT_RESIDUEL").withIndex(9).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(inbNumeroSerie, ColumnMetadata.named("INB_NUMERO_SERIE").withIndex(11).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(inbTypeAmort, ColumnMetadata.named("INB_TYPE_AMORT").withIndex(7).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(15).ofType(Types.DECIMAL).withSize(0));
        addMetadata(origId, ColumnMetadata.named("ORIG_ID").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(5).ofType(Types.VARCHAR).withSize(1000).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

