package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviRecetteDetail is a Querydsl query type for QVSuiviRecetteDetail
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviRecetteDetail extends com.mysema.query.sql.RelationalPathBase<QVSuiviRecetteDetail> {

    private static final long serialVersionUID = -1367186500;

    public static final QVSuiviRecetteDetail vSuiviRecetteDetail = new QVSuiviRecetteDetail("V_SUIVI_RECETTE_DETAIL");

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> facId = createSimple("facId", Object.class);

    public final SimplePath<Object> facNumero = createSimple("facNumero", Object.class);

    public final SimplePath<Object> fconDateSaisie = createSimple("fconDateSaisie", Object.class);

    public final SimplePath<Object> fconMontantBudgetaire = createSimple("fconMontantBudgetaire", Object.class);

    public final SimplePath<Object> fconMontantBudgetaireReste = createSimple("fconMontantBudgetaireReste", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> rconHtSaisie = createSimple("rconHtSaisie", Object.class);

    public final SimplePath<Object> rconTtcSaisie = createSimple("rconTtcSaisie", Object.class);

    public final SimplePath<Object> recId = createSimple("recId", Object.class);

    public final SimplePath<Object> recNumero = createSimple("recNumero", Object.class);

    public final SimplePath<Object> rppDateSaisie = createSimple("rppDateSaisie", Object.class);

    public final SimplePath<Object> rppId = createSimple("rppId", Object.class);

    public final SimplePath<Object> rppNumero = createSimple("rppNumero", Object.class);

    public final SimplePath<Object> tapId = createSimple("tapId", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public QVSuiviRecetteDetail(String variable) {
        super(QVSuiviRecetteDetail.class, forVariable(variable), "GFC", "V_SUIVI_RECETTE_DETAIL");
        addMetadata();
    }

    public QVSuiviRecetteDetail(String variable, String schema, String table) {
        super(QVSuiviRecetteDetail.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviRecetteDetail(Path<? extends QVSuiviRecetteDetail> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_RECETTE_DETAIL");
        addMetadata();
    }

    public QVSuiviRecetteDetail(PathMetadata<?> metadata) {
        super(QVSuiviRecetteDetail.class, metadata, "GFC", "V_SUIVI_RECETTE_DETAIL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(facId, ColumnMetadata.named("FAC_ID").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(facNumero, ColumnMetadata.named("FAC_NUMERO").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(fconDateSaisie, ColumnMetadata.named("FCON_DATE_SAISIE").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(fconMontantBudgetaire, ColumnMetadata.named("FCON_MONTANT_BUDGETAIRE").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(fconMontantBudgetaireReste, ColumnMetadata.named("FCON_MONTANT_BUDGETAIRE_RESTE").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(rconHtSaisie, ColumnMetadata.named("RCON_HT_SAISIE").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(rconTtcSaisie, ColumnMetadata.named("RCON_TTC_SAISIE").withIndex(15).ofType(Types.OTHER).withSize(0));
        addMetadata(recId, ColumnMetadata.named("REC_ID").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(recNumero, ColumnMetadata.named("REC_NUMERO").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(rppDateSaisie, ColumnMetadata.named("RPP_DATE_SAISIE").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(rppId, ColumnMetadata.named("RPP_ID").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(rppNumero, ColumnMetadata.named("RPP_NUMERO").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(4).ofType(Types.OTHER).withSize(0));
    }

}

