package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmUtilisateur is a Querydsl query type for QAdmUtilisateur
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmUtilisateur extends com.mysema.query.sql.RelationalPathBase<QAdmUtilisateur> {

    private static final long serialVersionUID = -1809100870;

    public static final QAdmUtilisateur admUtilisateur = new QAdmUtilisateur("ADM_UTILISATEUR");

    public final NumberPath<Long> noIndividu = createNumber("noIndividu", Long.class);

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final DateTimePath<java.sql.Timestamp> utlFermeture = createDateTime("utlFermeture", java.sql.Timestamp.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final DateTimePath<java.sql.Timestamp> utlOuverture = createDateTime("utlOuverture", java.sql.Timestamp.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmUtilisateur> sysC0075866 = createPrimaryKey(utlOrdre);

    public final com.mysema.query.sql.ForeignKey<QIndividuUlr> utilisateurNoIndividuFk = createForeignKey(noIndividu, "NO_INDIVIDU");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> utilisateurTyetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QCommande> _commandeUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapUtlValPrestFk = createInvForeignKey(utlOrdre, "FAP_UTL_VALIDATION_PREST");

    public final com.mysema.query.sql.ForeignKey<QPrestation> _prestationUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QXlabImport> _xlabImportUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QVisaAvMission> _visaAvMissionUtlValFk = createInvForeignKey(utlOrdre, "UTL_ORDRE_VALIDEUR");

    public final com.mysema.query.sql.ForeignKey<QRecRecette> _recRecetteUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapUtlValClientFk = createInvForeignKey(utlOrdre, "FAP_UTL_VALIDATION_CLIENT");

    public final com.mysema.query.sql.ForeignKey<QRecettePapier> _recettePapierUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeUtilisateur> _commandeUtilUtlordreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> _depenseBudgetUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateurFonct> _admUfUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QImmCleInventComptModif> _cleInvComptMUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeImpression> _commandeImpresUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QReimputation> _reimputationUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepensePapier> _depensePapierUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateurPreference> _admUpUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCommandeDocument> _commandeDocumentUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QCatalogueResponsable> _carUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> _canUtlOrdre = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QVisaAvMission> _visaAvMissionUtlDemFk = createInvForeignKey(utlOrdre, "UTL_ORDRE_DEMANDEUR");

    public final com.mysema.query.sql.ForeignKey<QPrelevementFichier> _prelevementUtilisateurFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateurEb> _admUtilisateurEbUtlFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmImTaux> _admImTauxUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFacture> _factureUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> _engageBudgetUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPdepenseBudget> _pdepenseBudgetUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QRetenue> _retenueUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapUtlOrdreFk = createInvForeignKey(utlOrdre, "UTL_ORDRE");

    public QAdmUtilisateur(String variable) {
        super(QAdmUtilisateur.class, forVariable(variable), "GFC", "ADM_UTILISATEUR");
        addMetadata();
    }

    public QAdmUtilisateur(String variable, String schema, String table) {
        super(QAdmUtilisateur.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmUtilisateur(Path<? extends QAdmUtilisateur> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_UTILISATEUR");
        addMetadata();
    }

    public QAdmUtilisateur(PathMetadata<?> metadata) {
        super(QAdmUtilisateur.class, metadata, "GFC", "ADM_UTILISATEUR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(noIndividu, ColumnMetadata.named("NO_INDIVIDU").withIndex(2).ofType(Types.DECIMAL).withSize(0));
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlFermeture, ColumnMetadata.named("UTL_FERMETURE").withIndex(5).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOuverture, ColumnMetadata.named("UTL_OUVERTURE").withIndex(4).ofType(Types.TIMESTAMP).withSize(7).notNull());
    }

}

