package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAxesStrategiques is a Querydsl query type for QAxesStrategiques
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAxesStrategiques extends com.mysema.query.sql.RelationalPathBase<QAxesStrategiques> {

    private static final long serialVersionUID = -987015557;

    public static final QAxesStrategiques axesStrategiques = new QAxesStrategiques("AXES_STRATEGIQUES");

    public final DateTimePath<java.sql.Timestamp> dCreation = createDateTime("dCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dModification = createDateTime("dModification", java.sql.Timestamp.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath libelle = createString("libelle");

    public final NumberPath<Long> persIdCreation = createNumber("persIdCreation", Long.class);

    public final NumberPath<Long> persIdModification = createNumber("persIdModification", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAxesStrategiques> axeStrategiquePk = createPrimaryKey(id);

    public QAxesStrategiques(String variable) {
        super(QAxesStrategiques.class, forVariable(variable), "GRHUM", "AXES_STRATEGIQUES");
        addMetadata();
    }

    public QAxesStrategiques(String variable, String schema, String table) {
        super(QAxesStrategiques.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAxesStrategiques(Path<? extends QAxesStrategiques> path) {
        super(path.getType(), path.getMetadata(), "GRHUM", "AXES_STRATEGIQUES");
        addMetadata();
    }

    public QAxesStrategiques(PathMetadata<?> metadata) {
        super(QAxesStrategiques.class, metadata, "GRHUM", "AXES_STRATEGIQUES");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dCreation, ColumnMetadata.named("D_CREATION").withIndex(5).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dModification, ColumnMetadata.named("D_MODIFICATION").withIndex(6).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(id, ColumnMetadata.named("ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(libelle, ColumnMetadata.named("LIBELLE").withIndex(2).ofType(Types.VARCHAR).withSize(100).notNull());
        addMetadata(persIdCreation, ColumnMetadata.named("PERS_ID_CREATION").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(persIdModification, ColumnMetadata.named("PERS_ID_MODIFICATION").withIndex(4).ofType(Types.DECIMAL).withSize(0));
    }

}

