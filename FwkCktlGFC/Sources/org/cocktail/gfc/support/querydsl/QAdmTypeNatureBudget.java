package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmTypeNatureBudget is a Querydsl query type for QAdmTypeNatureBudget
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmTypeNatureBudget extends com.mysema.query.sql.RelationalPathBase<QAdmTypeNatureBudget> {

    private static final long serialVersionUID = -53507761;

    public static final QAdmTypeNatureBudget admTypeNatureBudget = new QAdmTypeNatureBudget("ADM_TYPE_NATURE_BUDGET");

    public final NumberPath<Long> tnbCategorie = createNumber("tnbCategorie", Long.class);

    public final StringPath tnbCode = createString("tnbCode");

    public final NumberPath<Long> tnbId = createNumber("tnbId", Long.class);

    public final StringPath tnbLibelle = createString("tnbLibelle");

    public final StringPath tnbNiveauCofisup = createString("tnbNiveauCofisup");

    public final NumberPath<Long> tnbNiveauEb = createNumber("tnbNiveauEb", Long.class);

    public final NumberPath<Long> tnbOrdreAffichage = createNumber("tnbOrdreAffichage", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmTypeNatureBudget> sysC0077817 = createPrimaryKey(tnbId);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> admTypeNatureBudgetTyetFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmEbNatureBudget> _admEbTypeNatureBudgeFk = createInvForeignKey(tnbId, "TNB_ID");

    public QAdmTypeNatureBudget(String variable) {
        super(QAdmTypeNatureBudget.class, forVariable(variable), "GFC", "ADM_TYPE_NATURE_BUDGET");
        addMetadata();
    }

    public QAdmTypeNatureBudget(String variable, String schema, String table) {
        super(QAdmTypeNatureBudget.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmTypeNatureBudget(Path<? extends QAdmTypeNatureBudget> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_TYPE_NATURE_BUDGET");
        addMetadata();
    }

    public QAdmTypeNatureBudget(PathMetadata<?> metadata) {
        super(QAdmTypeNatureBudget.class, metadata, "GFC", "ADM_TYPE_NATURE_BUDGET");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(tnbCategorie, ColumnMetadata.named("TNB_CATEGORIE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tnbCode, ColumnMetadata.named("TNB_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(30).notNull());
        addMetadata(tnbId, ColumnMetadata.named("TNB_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tnbLibelle, ColumnMetadata.named("TNB_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(tnbNiveauCofisup, ColumnMetadata.named("TNB_NIVEAU_COFISUP").withIndex(5).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(tnbNiveauEb, ColumnMetadata.named("TNB_NIVEAU_EB").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tnbOrdreAffichage, ColumnMetadata.named("TNB_ORDRE_AFFICHAGE").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

