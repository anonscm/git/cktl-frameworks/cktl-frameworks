package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QOpeFraisGestion is a Querydsl query type for QOpeFraisGestion
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QOpeFraisGestion extends com.mysema.query.sql.RelationalPathBase<QOpeFraisGestion> {

    private static final long serialVersionUID = -12170355;

    public static final QOpeFraisGestion opeFraisGestion = new QOpeFraisGestion("OPE_FRAIS_GESTION");

    public final NumberPath<java.math.BigDecimal> chargesNonBudgetaires = createNumber("chargesNonBudgetaires", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> coutsIndirectsNonImputes = createNumber("coutsIndirectsNonImputes", java.math.BigDecimal.class);

    public final NumberPath<Long> idOpeFraisGestion = createNumber("idOpeFraisGestion", Long.class);

    public final NumberPath<Long> idOpeRepartPartenaire = createNumber("idOpeRepartPartenaire", Long.class);

    public final NumberPath<java.math.BigDecimal> montant = createNumber("montant", java.math.BigDecimal.class);

    public final NumberPath<Double> montantPourcentageTranche = createNumber("montantPourcentageTranche", Double.class);

    public final com.mysema.query.sql.PrimaryKey<QOpeFraisGestion> idOpeFraisGestionPk = createPrimaryKey(idOpeFraisGestion);

    public final com.mysema.query.sql.ForeignKey<QOpeRepartPartenaire> opeFraisGestionRepPartFk = createForeignKey(idOpeRepartPartenaire, "ID_OPE_REPART_PARTENAIRE");

    public QOpeFraisGestion(String variable) {
        super(QOpeFraisGestion.class, forVariable(variable), "GFC", "OPE_FRAIS_GESTION");
        addMetadata();
    }

    public QOpeFraisGestion(String variable, String schema, String table) {
        super(QOpeFraisGestion.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QOpeFraisGestion(Path<? extends QOpeFraisGestion> path) {
        super(path.getType(), path.getMetadata(), "GFC", "OPE_FRAIS_GESTION");
        addMetadata();
    }

    public QOpeFraisGestion(PathMetadata<?> metadata) {
        super(QOpeFraisGestion.class, metadata, "GFC", "OPE_FRAIS_GESTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(chargesNonBudgetaires, ColumnMetadata.named("CHARGES_NON_BUDGETAIRES").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(coutsIndirectsNonImputes, ColumnMetadata.named("COUTS_INDIRECTS_NON_IMPUTES").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(idOpeFraisGestion, ColumnMetadata.named("ID_OPE_FRAIS_GESTION").withIndex(1).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(idOpeRepartPartenaire, ColumnMetadata.named("ID_OPE_REPART_PARTENAIRE").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
        addMetadata(montant, ColumnMetadata.named("MONTANT").withIndex(3).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(montantPourcentageTranche, ColumnMetadata.named("MONTANT_POURCENTAGE_TRANCHE").withIndex(4).ofType(Types.DECIMAL).withSize(5).withDigits(2));
    }

}

