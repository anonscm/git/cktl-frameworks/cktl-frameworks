package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QReimputationHorsMarche is a Querydsl query type for QReimputationHorsMarche
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QReimputationHorsMarche extends com.mysema.query.sql.RelationalPathBase<QReimputationHorsMarche> {

    private static final long serialVersionUID = -169249496;

    public static final QReimputationHorsMarche reimputationHorsMarche = new QReimputationHorsMarche("REIMPUTATION_HORS_MARCHE");

    public final NumberPath<Long> ceOrdre = createNumber("ceOrdre", Long.class);

    public final NumberPath<java.math.BigDecimal> rehmHtSaisie = createNumber("rehmHtSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> rehmId = createNumber("rehmId", Long.class);

    public final NumberPath<java.math.BigDecimal> rehmMontantBudgetaire = createNumber("rehmMontantBudgetaire", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> rehmTtcSaisie = createNumber("rehmTtcSaisie", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> rehmTvaSaisie = createNumber("rehmTvaSaisie", java.math.BigDecimal.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final NumberPath<Long> typaId = createNumber("typaId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QReimputationHorsMarche> reimputationHorsMarchePk = createPrimaryKey(rehmId);

    public final com.mysema.query.sql.ForeignKey<QTypeAchat> reimpHmTypaIdFk = createForeignKey(typaId, "TYPA_ID");

    public final com.mysema.query.sql.ForeignKey<QCodeExer> reimpHmCeOrdreFk = createForeignKey(ceOrdre, "CE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QReimputation> reimpHmReimIdFk = createForeignKey(reimId, "REIM_ID");

    public QReimputationHorsMarche(String variable) {
        super(QReimputationHorsMarche.class, forVariable(variable), "GFC", "REIMPUTATION_HORS_MARCHE");
        addMetadata();
    }

    public QReimputationHorsMarche(String variable, String schema, String table) {
        super(QReimputationHorsMarche.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QReimputationHorsMarche(Path<? extends QReimputationHorsMarche> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REIMPUTATION_HORS_MARCHE");
        addMetadata();
    }

    public QReimputationHorsMarche(PathMetadata<?> metadata) {
        super(QReimputationHorsMarche.class, metadata, "GFC", "REIMPUTATION_HORS_MARCHE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(ceOrdre, ColumnMetadata.named("CE_ORDRE").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(rehmHtSaisie, ColumnMetadata.named("REHM_HT_SAISIE").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(rehmId, ColumnMetadata.named("REHM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(rehmMontantBudgetaire, ColumnMetadata.named("REHM_MONTANT_BUDGETAIRE").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(rehmTtcSaisie, ColumnMetadata.named("REHM_TTC_SAISIE").withIndex(8).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(rehmTvaSaisie, ColumnMetadata.named("REHM_TVA_SAISIE").withIndex(7).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(typaId, ColumnMetadata.named("TYPA_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

