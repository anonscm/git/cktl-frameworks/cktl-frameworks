package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QFacturePapier is a Querydsl query type for QFacturePapier
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QFacturePapier extends com.mysema.query.sql.RelationalPathBase<QFacturePapier> {

    private static final long serialVersionUID = -4403950;

    public static final QFacturePapier facturePapier = new QFacturePapier("FACTURE_PAPIER");

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<Long> echeId = createNumber("echeId", Long.class);

    public final NumberPath<Long> engId = createNumber("engId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final NumberPath<Long> facId = createNumber("facId", Long.class);

    public final StringPath fapApplyTva = createString("fapApplyTva");

    public final StringPath fapCommentaireClient = createString("fapCommentaireClient");

    public final StringPath fapCommentairePrest = createString("fapCommentairePrest");

    public final DateTimePath<java.sql.Timestamp> fapDate = createDateTime("fapDate", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> fapDateLimitePaiement = createDateTime("fapDateLimitePaiement", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> fapDateReglement = createDateTime("fapDateReglement", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> fapDateValidationClient = createDateTime("fapDateValidationClient", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> fapDateValidationPrest = createDateTime("fapDateValidationPrest", java.sql.Timestamp.class);

    public final NumberPath<Long> fapId = createNumber("fapId", Long.class);

    public final StringPath fapLib = createString("fapLib");

    public final NumberPath<Long> fapNumero = createNumber("fapNumero", Long.class);

    public final StringPath fapRef = createString("fapRef");

    public final StringPath fapReferenceReglement = createString("fapReferenceReglement");

    public final NumberPath<java.math.BigDecimal> fapRemiseGlobale = createNumber("fapRemiseGlobale", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> fapTotalHt = createNumber("fapTotalHt", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> fapTotalTtc = createNumber("fapTotalTtc", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> fapTotalTva = createNumber("fapTotalTva", java.math.BigDecimal.class);

    public final NumberPath<Long> fapUtlValidationClient = createNumber("fapUtlValidationClient", Long.class);

    public final NumberPath<Long> fapUtlValidationPrest = createNumber("fapUtlValidationPrest", Long.class);

    public final NumberPath<Long> fouOrdre = createNumber("fouOrdre", Long.class);

    public final NumberPath<Long> fouOrdrePrest = createNumber("fouOrdrePrest", Long.class);

    public final NumberPath<Long> idAdmEb = createNumber("idAdmEb", Long.class);

    public final NumberPath<Long> idOpeOperation = createNumber("idOpeOperation", Long.class);

    public final NumberPath<Long> lolfId = createNumber("lolfId", Long.class);

    public final NumberPath<Long> morOrdre = createNumber("morOrdre", Long.class);

    public final NumberPath<Long> noIndividu = createNumber("noIndividu", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final StringPath pcoNumCtp = createString("pcoNumCtp");

    public final StringPath pcoNumTva = createString("pcoNumTva");

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public final NumberPath<Long> prestId = createNumber("prestId", Long.class);

    public final NumberPath<Long> ribOrdre = createNumber("ribOrdre", Long.class);

    public final NumberPath<Long> tapId = createNumber("tapId", Long.class);

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final NumberPath<Long> typuId = createNumber("typuId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QFacturePapier> facturePapierPk = createPrimaryKey(fapId);

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> fapPcoNumTvaFk = createForeignKey(pcoNumTva, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> fapCanIdFk = createForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QEngageBudget> fapEngIdFk = createForeignKey(engId, "ENG_ID");

    public final com.mysema.query.sql.ForeignKey<QFournisUlr> fapFouOrdreFk = createForeignKey(fouOrdre, "FOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> fapTyetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> fapUtlValPrestFk = createForeignKey(fapUtlValidationPrest, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> fapPcoNumFk = createForeignKey(pcoNum, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QRibfourUlr> fapRibOrdreFk = createForeignKey(ribOrdre, "RIB_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QEchEcheancier> fapEcheIdFk = createForeignKey(echeId, "ECH_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeCredit> fapTcdOrdreFk = createForeignKey(tcdOrdre, "TCD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTauxProrata> fapTapIdFk = createForeignKey(tapId, "TAP_ID");

    public final com.mysema.query.sql.ForeignKey<QPlanComptable> fapPcoNumCtpFk = createForeignKey(pcoNumCtp, "PCO_NUM");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> fapExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QPrestation> fapPrestIdFk = createForeignKey(prestId, "PREST_ID");

    public final com.mysema.query.sql.ForeignKey<QFacture> fapFacIdFk = createForeignKey(facId, "FAC_ID");

    public final com.mysema.query.sql.ForeignKey<QPersonne> fapPersIdFk = createForeignKey(persId, "PERS_ID");

    public final com.mysema.query.sql.ForeignKey<QIndividuUlr> fapNoIndividuFk = createForeignKey(noIndividu, "NO_INDIVIDU");

    public final com.mysema.query.sql.ForeignKey<QAdmOrigineRecette> fapLolfIdFk = createForeignKey(lolfId, "ID_ADM_ORIGINE_RECETTE");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> fapUtlValClientFk = createForeignKey(fapUtlValidationClient, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QFournisUlr> fapFouOrdrePrestFk = createForeignKey(fouOrdrePrest, "FOU_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QModeRecouvrement> fapMorOrdreFk = createForeignKey(morOrdre, "MOD_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmEb> fapOrgIdFk = createForeignKey(idAdmEb, "ID_ADM_EB");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> fapUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QTypePublic> fapTypuIdFk = createForeignKey(typuId, "TYPU_ID");

    public final com.mysema.query.sql.ForeignKey<QFacturePapierLigne> _fligFapIdFk = createInvForeignKey(fapId, "FAP_ID");

    public final com.mysema.query.sql.ForeignKey<QFacturePapierAdrClient> _fapAdrClientFapIdFk = createInvForeignKey(fapId, "FAP_ID");

    public QFacturePapier(String variable) {
        super(QFacturePapier.class, forVariable(variable), "GFC", "FACTURE_PAPIER");
        addMetadata();
    }

    public QFacturePapier(String variable, String schema, String table) {
        super(QFacturePapier.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QFacturePapier(Path<? extends QFacturePapier> path) {
        super(path.getType(), path.getMetadata(), "GFC", "FACTURE_PAPIER");
        addMetadata();
    }

    public QFacturePapier(PathMetadata<?> metadata) {
        super(QFacturePapier.class, metadata, "GFC", "FACTURE_PAPIER");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(32).ofType(Types.DECIMAL).withSize(0));
        addMetadata(echeId, ColumnMetadata.named("ECHE_ID").withIndex(34).ofType(Types.DECIMAL).withSize(0));
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(42).ofType(Types.DECIMAL).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(facId, ColumnMetadata.named("FAC_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0));
        addMetadata(fapApplyTva, ColumnMetadata.named("FAP_APPLY_TVA").withIndex(22).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(fapCommentaireClient, ColumnMetadata.named("FAP_COMMENTAIRE_CLIENT").withIndex(20).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(fapCommentairePrest, ColumnMetadata.named("FAP_COMMENTAIRE_PREST").withIndex(19).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(fapDate, ColumnMetadata.named("FAP_DATE").withIndex(11).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(fapDateLimitePaiement, ColumnMetadata.named("FAP_DATE_LIMITE_PAIEMENT").withIndex(16).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(fapDateReglement, ColumnMetadata.named("FAP_DATE_REGLEMENT").withIndex(17).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(fapDateValidationClient, ColumnMetadata.named("FAP_DATE_VALIDATION_CLIENT").withIndex(14).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(fapDateValidationPrest, ColumnMetadata.named("FAP_DATE_VALIDATION_PREST").withIndex(15).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(fapId, ColumnMetadata.named("FAP_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(fapLib, ColumnMetadata.named("FAP_LIB").withIndex(13).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(fapNumero, ColumnMetadata.named("FAP_NUMERO").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(fapRef, ColumnMetadata.named("FAP_REF").withIndex(12).ofType(Types.VARCHAR).withSize(100));
        addMetadata(fapReferenceReglement, ColumnMetadata.named("FAP_REFERENCE_REGLEMENT").withIndex(18).ofType(Types.VARCHAR).withSize(20));
        addMetadata(fapRemiseGlobale, ColumnMetadata.named("FAP_REMISE_GLOBALE").withIndex(21).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(fapTotalHt, ColumnMetadata.named("FAP_TOTAL_HT").withIndex(39).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(fapTotalTtc, ColumnMetadata.named("FAP_TOTAL_TTC").withIndex(41).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(fapTotalTva, ColumnMetadata.named("FAP_TOTAL_TVA").withIndex(40).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(fapUtlValidationClient, ColumnMetadata.named("FAP_UTL_VALIDATION_CLIENT").withIndex(37).ofType(Types.DECIMAL).withSize(0));
        addMetadata(fapUtlValidationPrest, ColumnMetadata.named("FAP_UTL_VALIDATION_PREST").withIndex(38).ofType(Types.DECIMAL).withSize(0));
        addMetadata(fouOrdre, ColumnMetadata.named("FOU_ORDRE").withIndex(9).ofType(Types.DECIMAL).withSize(0));
        addMetadata(fouOrdrePrest, ColumnMetadata.named("FOU_ORDRE_PREST").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(25).ofType(Types.DECIMAL).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(33).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lolfId, ColumnMetadata.named("LOLF_ID").withIndex(28).ofType(Types.DECIMAL).withSize(0));
        addMetadata(morOrdre, ColumnMetadata.named("MOR_ORDRE").withIndex(23).ofType(Types.DECIMAL).withSize(0));
        addMetadata(noIndividu, ColumnMetadata.named("NO_INDIVIDU").withIndex(10).ofType(Types.DECIMAL).withSize(0));
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(29).ofType(Types.VARCHAR).withSize(20));
        addMetadata(pcoNumCtp, ColumnMetadata.named("PCO_NUM_CTP").withIndex(31).ofType(Types.VARCHAR).withSize(20));
        addMetadata(pcoNumTva, ColumnMetadata.named("PCO_NUM_TVA").withIndex(30).ofType(Types.VARCHAR).withSize(20));
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(8).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(prestId, ColumnMetadata.named("PREST_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(ribOrdre, ColumnMetadata.named("RIB_ORDRE").withIndex(24).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(26).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(27).ofType(Types.DECIMAL).withSize(0));
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(36).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(typuId, ColumnMetadata.named("TYPU_ID").withIndex(35).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

