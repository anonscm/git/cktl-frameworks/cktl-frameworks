package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QMigrationAttribution is a Querydsl query type for QMigrationAttribution
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QMigrationAttribution extends com.mysema.query.sql.RelationalPathBase<QMigrationAttribution> {

    private static final long serialVersionUID = 40181330;

    public static final QMigrationAttribution migrationAttribution = new QMigrationAttribution("MIGRATION_ATTRIBUTION");

    public final NumberPath<Long> attOrdre = createNumber("attOrdre", Long.class);

    public final NumberPath<Long> lotOrdre = createNumber("lotOrdre", Long.class);

    public QMigrationAttribution(String variable) {
        super(QMigrationAttribution.class, forVariable(variable), "GFC", "MIGRATION_ATTRIBUTION");
        addMetadata();
    }

    public QMigrationAttribution(String variable, String schema, String table) {
        super(QMigrationAttribution.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QMigrationAttribution(Path<? extends QMigrationAttribution> path) {
        super(path.getType(), path.getMetadata(), "GFC", "MIGRATION_ATTRIBUTION");
        addMetadata();
    }

    public QMigrationAttribution(PathMetadata<?> metadata) {
        super(QMigrationAttribution.class, metadata, "GFC", "MIGRATION_ATTRIBUTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(attOrdre, ColumnMetadata.named("ATT_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(lotOrdre, ColumnMetadata.named("LOT_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0));
    }

}

