package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmCodeAnalytique is a Querydsl query type for QAdmCodeAnalytique
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmCodeAnalytique extends com.mysema.query.sql.RelationalPathBase<QAdmCodeAnalytique> {

    private static final long serialVersionUID = 1130443937;

    public static final QAdmCodeAnalytique admCodeAnalytique = new QAdmCodeAnalytique("ADM_CODE_ANALYTIQUE");

    public final StringPath canCode = createString("canCode");

    public final DateTimePath<java.sql.Timestamp> canFermeture = createDateTime("canFermeture", java.sql.Timestamp.class);

    public final NumberPath<Long> canId = createNumber("canId", Long.class);

    public final NumberPath<Long> canIdPere = createNumber("canIdPere", Long.class);

    public final StringPath canLibelle = createString("canLibelle");

    public final NumberPath<java.math.BigDecimal> canMontant = createNumber("canMontant", java.math.BigDecimal.class);

    public final NumberPath<Long> canMontantDepassement = createNumber("canMontantDepassement", Long.class);

    public final NumberPath<Long> canNiveau = createNumber("canNiveau", Long.class);

    public final DateTimePath<java.sql.Timestamp> canOuverture = createDateTime("canOuverture", java.sql.Timestamp.class);

    public final NumberPath<Long> canPublic = createNumber("canPublic", Long.class);

    public final NumberPath<Long> canUtilisable = createNumber("canUtilisable", Long.class);

    public final StringPath grpRespCStructure = createString("grpRespCStructure");

    public final NumberPath<Long> tyetId = createNumber("tyetId", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmCodeAnalytique> codeAnalytiquePk = createPrimaryKey(canId);

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> canUtilisableFk = createForeignKey(canUtilisable, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> admCoanTyetIdFk = createForeignKey(tyetId, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> codeAnalytiqueCanIdPereFk = createForeignKey(canIdPere, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> canUtlOrdre = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> canPublicFk = createForeignKey(canPublic, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmTypeEtat> admCodeAnalytiqueCmdepasFk = createForeignKey(canMontantDepassement, "TYET_ID");

    public final com.mysema.query.sql.ForeignKey<QStructureUlr> admCodeAnalytiqueGrpResFk = createForeignKey(grpRespCStructure, "C_STRUCTURE");

    public final com.mysema.query.sql.ForeignKey<QReimputationAnalytique> _reimpAnalytiqueCanIdFk = createInvForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QPdepenseCtrlAnalytique> _pdepenseCtrlAnalCanIdFk = createInvForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QFacturePapier> _fapCanIdFk = createInvForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QEngageCtrlAnalytique> _engageCtrlAnalCanIdFk = createInvForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QPrestationBudgetClient> _pbcCanIdFk = createInvForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytiqueEb> _admCaoCanIdFk = createInvForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QAdmCodeAnalytique> _codeAnalytiqueCanIdPereFk = createInvForeignKey(canId, "CAN_ID_PERE");

    public final com.mysema.query.sql.ForeignKey<QFactureCtrlAnalytique> _fanaCanIdFk = createInvForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QDepenseCtrlAnalytique> _depenseCtrlAnalCanIdFk = createInvForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QCommandeCtrlAnalytique> _commandeCtrlAnCanIdFk = createInvForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QPrestation> _prestationCanIdFk = createInvForeignKey(canId, "CAN_ID");

    public final com.mysema.query.sql.ForeignKey<QRecetteCtrlAnalytique> _ranaCanIdFk = createInvForeignKey(canId, "CAN_ID");

    public QAdmCodeAnalytique(String variable) {
        super(QAdmCodeAnalytique.class, forVariable(variable), "GFC", "ADM_CODE_ANALYTIQUE");
        addMetadata();
    }

    public QAdmCodeAnalytique(String variable, String schema, String table) {
        super(QAdmCodeAnalytique.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmCodeAnalytique(Path<? extends QAdmCodeAnalytique> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_CODE_ANALYTIQUE");
        addMetadata();
    }

    public QAdmCodeAnalytique(PathMetadata<?> metadata) {
        super(QAdmCodeAnalytique.class, metadata, "GFC", "ADM_CODE_ANALYTIQUE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(canCode, ColumnMetadata.named("CAN_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(canFermeture, ColumnMetadata.named("CAN_FERMETURE").withIndex(9).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(canId, ColumnMetadata.named("CAN_ID").withIndex(1).ofType(Types.DECIMAL).withSize(12).notNull());
        addMetadata(canIdPere, ColumnMetadata.named("CAN_ID_PERE").withIndex(7).ofType(Types.DECIMAL).withSize(12));
        addMetadata(canLibelle, ColumnMetadata.named("CAN_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(500).notNull());
        addMetadata(canMontant, ColumnMetadata.named("CAN_MONTANT").withIndex(12).ofType(Types.DECIMAL).withSize(12).withDigits(2));
        addMetadata(canMontantDepassement, ColumnMetadata.named("CAN_MONTANT_DEPASSEMENT").withIndex(13).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(canNiveau, ColumnMetadata.named("CAN_NIVEAU").withIndex(10).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(canOuverture, ColumnMetadata.named("CAN_OUVERTURE").withIndex(8).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(canPublic, ColumnMetadata.named("CAN_PUBLIC").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(canUtilisable, ColumnMetadata.named("CAN_UTILISABLE").withIndex(5).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(grpRespCStructure, ColumnMetadata.named("GRP_RESP_C_STRUCTURE").withIndex(14).ofType(Types.VARCHAR).withSize(10));
        addMetadata(tyetId, ColumnMetadata.named("TYET_ID").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(11).ofType(Types.DECIMAL).withSize(0));
    }

}

