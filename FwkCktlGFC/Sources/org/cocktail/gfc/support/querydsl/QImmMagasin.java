package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmMagasin is a Querydsl query type for QImmMagasin
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmMagasin extends com.mysema.query.sql.RelationalPathBase<QImmMagasin> {

    private static final long serialVersionUID = 2057033826;

    public static final QImmMagasin immMagasin = new QImmMagasin("IMM_MAGASIN");

    public final StringPath cStructure = createString("cStructure");

    public final NumberPath<Long> magId = createNumber("magId", Long.class);

    public final StringPath magLibelle = createString("magLibelle");

    public final com.mysema.query.sql.PrimaryKey<QImmMagasin> immMagasinPk = createPrimaryKey(magId);

    public final com.mysema.query.sql.ForeignKey<QImmLivraison> _magasinFk2 = createInvForeignKey(magId, "MAG_ID");

    public final com.mysema.query.sql.ForeignKey<QImmInvent> _magasinFk = createInvForeignKey(magId, "MAG_ID");

    public QImmMagasin(String variable) {
        super(QImmMagasin.class, forVariable(variable), "GFC", "IMM_MAGASIN");
        addMetadata();
    }

    public QImmMagasin(String variable, String schema, String table) {
        super(QImmMagasin.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmMagasin(Path<? extends QImmMagasin> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_MAGASIN");
        addMetadata();
    }

    public QImmMagasin(PathMetadata<?> metadata) {
        super(QImmMagasin.class, metadata, "GFC", "IMM_MAGASIN");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cStructure, ColumnMetadata.named("C_STRUCTURE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(magId, ColumnMetadata.named("MAG_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(magLibelle, ColumnMetadata.named("MAG_LIBELLE").withIndex(3).ofType(Types.VARCHAR).withSize(200));
    }

}

