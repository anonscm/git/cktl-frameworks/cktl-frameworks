package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEpnDepBud2 is a Querydsl query type for QEpnDepBud2
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QEpnDepBud2 extends com.mysema.query.sql.RelationalPathBase<QEpnDepBud2> {

    private static final long serialVersionUID = -112157266;

    public static final QEpnDepBud2 epnDepBud2 = new QEpnDepBud2("EPN_DEP_BUD_2");

    public final StringPath epndb2CodBud = createString("epndb2CodBud");

    public final StringPath epndb2Compte = createString("epndb2Compte");

    public final NumberPath<Double> epndb2DepCreext = createNumber("epndb2DepCreext", Double.class);

    public final NumberPath<Double> epndb2DepDepext = createNumber("epndb2DepDepext", Double.class);

    public final NumberPath<Integer> epndb2Exercice = createNumber("epndb2Exercice", Integer.class);

    public final StringPath epndb2GesCode = createString("epndb2GesCode");

    public final NumberPath<Double> epndb2Mntbrut = createNumber("epndb2Mntbrut", Double.class);

    public final NumberPath<Double> epndb2Mntnet = createNumber("epndb2Mntnet", Double.class);

    public final NumberPath<Double> epndb2Mntrevers = createNumber("epndb2Mntrevers", Double.class);

    public final NumberPath<Integer> epndb2Numero = createNumber("epndb2Numero", Integer.class);

    public final StringPath epndb2Type = createString("epndb2Type");

    public final NumberPath<Integer> epndb2TypeCpt = createNumber("epndb2TypeCpt", Integer.class);

    public final StringPath epndb2TypeDoc = createString("epndb2TypeDoc");

    public final StringPath epndbOrdre = createString("epndbOrdre");

    public QEpnDepBud2(String variable) {
        super(QEpnDepBud2.class, forVariable(variable), "GFC", "EPN_DEP_BUD_2");
        addMetadata();
    }

    public QEpnDepBud2(String variable, String schema, String table) {
        super(QEpnDepBud2.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEpnDepBud2(Path<? extends QEpnDepBud2> path) {
        super(path.getType(), path.getMetadata(), "GFC", "EPN_DEP_BUD_2");
        addMetadata();
    }

    public QEpnDepBud2(PathMetadata<?> metadata) {
        super(QEpnDepBud2.class, metadata, "GFC", "EPN_DEP_BUD_2");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(epndb2CodBud, ColumnMetadata.named("EPNDB2_COD_BUD").withIndex(13).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epndb2Compte, ColumnMetadata.named("EPNDB2_COMPTE").withIndex(6).ofType(Types.VARCHAR).withSize(15));
        addMetadata(epndb2DepCreext, ColumnMetadata.named("EPNDB2_DEP_CREEXT").withIndex(8).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epndb2DepDepext, ColumnMetadata.named("EPNDB2_DEP_DEPEXT").withIndex(10).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epndb2Exercice, ColumnMetadata.named("EPNDB2_EXERCICE").withIndex(12).ofType(Types.DECIMAL).withSize(4));
        addMetadata(epndb2GesCode, ColumnMetadata.named("EPNDB2_GES_CODE").withIndex(2).ofType(Types.VARCHAR).withSize(10));
        addMetadata(epndb2Mntbrut, ColumnMetadata.named("EPNDB2_MNTBRUT").withIndex(7).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epndb2Mntnet, ColumnMetadata.named("EPNDB2_MNTNET").withIndex(11).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epndb2Mntrevers, ColumnMetadata.named("EPNDB2_MNTREVERS").withIndex(9).ofType(Types.DECIMAL).withSize(15).withDigits(2));
        addMetadata(epndb2Numero, ColumnMetadata.named("EPNDB2_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(5));
        addMetadata(epndb2Type, ColumnMetadata.named("EPNDB2_TYPE").withIndex(3).ofType(Types.VARCHAR).withSize(1));
        addMetadata(epndb2TypeCpt, ColumnMetadata.named("EPNDB2_TYPE_CPT").withIndex(5).ofType(Types.DECIMAL).withSize(1));
        addMetadata(epndb2TypeDoc, ColumnMetadata.named("EPNDB2_TYPE_DOC").withIndex(14).ofType(Types.VARCHAR).withSize(2));
        addMetadata(epndbOrdre, ColumnMetadata.named("EPNDB_ORDRE").withIndex(1).ofType(Types.VARCHAR).withSize(2));
    }

}

