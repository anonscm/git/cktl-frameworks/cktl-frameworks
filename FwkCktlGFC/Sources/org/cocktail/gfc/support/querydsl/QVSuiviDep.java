package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviDep is a Querydsl query type for QVSuiviDep
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviDep extends com.mysema.query.sql.RelationalPathBase<QVSuiviDep> {

    private static final long serialVersionUID = 1108107562;

    public static final QVSuiviDep vSuiviDep = new QVSuiviDep("V_SUIVI_DEP");

    public final SimplePath<Object> dconHtSaisie = createSimple("dconHtSaisie", Object.class);

    public final SimplePath<Object> dconMontantBudgetaire = createSimple("dconMontantBudgetaire", Object.class);

    public final SimplePath<Object> dconTtcSaisie = createSimple("dconTtcSaisie", Object.class);

    public final SimplePath<Object> depId = createSimple("depId", Object.class);

    public final SimplePath<Object> dppDateSaisie = createSimple("dppDateSaisie", Object.class);

    public final SimplePath<Object> dppId = createSimple("dppId", Object.class);

    public final SimplePath<Object> dppNumeroFacture = createSimple("dppNumeroFacture", Object.class);

    public final SimplePath<Object> engId = createSimple("engId", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> tapId = createSimple("tapId", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public QVSuiviDep(String variable) {
        super(QVSuiviDep.class, forVariable(variable), "GFC", "V_SUIVI_DEP");
        addMetadata();
    }

    public QVSuiviDep(String variable, String schema, String table) {
        super(QVSuiviDep.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviDep(Path<? extends QVSuiviDep> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_DEP");
        addMetadata();
    }

    public QVSuiviDep(PathMetadata<?> metadata) {
        super(QVSuiviDep.class, metadata, "GFC", "V_SUIVI_DEP");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(dconHtSaisie, ColumnMetadata.named("DCON_HT_SAISIE").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(dconMontantBudgetaire, ColumnMetadata.named("DCON_MONTANT_BUDGETAIRE").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(dconTtcSaisie, ColumnMetadata.named("DCON_TTC_SAISIE").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(dppDateSaisie, ColumnMetadata.named("DPP_DATE_SAISIE").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(dppNumeroFacture, ColumnMetadata.named("DPP_NUMERO_FACTURE").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(13).ofType(Types.OTHER).withSize(0));
    }

}

