package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVRespAdminUniq is a Querydsl query type for QVRespAdminUniq
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVRespAdminUniq extends com.mysema.query.sql.RelationalPathBase<QVRespAdminUniq> {

    private static final long serialVersionUID = 330783211;

    public static final QVRespAdminUniq vRespAdminUniq = new QVRespAdminUniq("V_RESP_ADMIN_UNIQ");

    public final NumberPath<Long> apcOrdre = createNumber("apcOrdre", Long.class);

    public final NumberPath<Long> avtOrdre = createNumber("avtOrdre", Long.class);

    public QVRespAdminUniq(String variable) {
        super(QVRespAdminUniq.class, forVariable(variable), "GFC", "V_RESP_ADMIN_UNIQ");
        addMetadata();
    }

    public QVRespAdminUniq(String variable, String schema, String table) {
        super(QVRespAdminUniq.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVRespAdminUniq(Path<? extends QVRespAdminUniq> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_RESP_ADMIN_UNIQ");
        addMetadata();
    }

    public QVRespAdminUniq(PathMetadata<?> metadata) {
        super(QVRespAdminUniq.class, metadata, "GFC", "V_RESP_ADMIN_UNIQ");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(apcOrdre, ColumnMetadata.named("APC_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0));
        addMetadata(avtOrdre, ColumnMetadata.named("AVT_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(38).notNull());
    }

}

