package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QAdmUtilisateurFonctGest is a Querydsl query type for QAdmUtilisateurFonctGest
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QAdmUtilisateurFonctGest extends com.mysema.query.sql.RelationalPathBase<QAdmUtilisateurFonctGest> {

    private static final long serialVersionUID = 322191931;

    public static final QAdmUtilisateurFonctGest admUtilisateurFonctGest = new QAdmUtilisateurFonctGest("ADM_UTILISATEUR_FONCT_GEST");

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> ufgId = createNumber("ufgId", Long.class);

    public final NumberPath<Long> ufOrdre = createNumber("ufOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QAdmUtilisateurFonctGest> sysC0076582 = createPrimaryKey(ufgId);

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateurFonct> admUfgUfOrdreFk = createForeignKey(ufOrdre, "UF_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QGestion> admUfgGesCodeFk = createForeignKey(gesCode, "GES_CODE");

    public QAdmUtilisateurFonctGest(String variable) {
        super(QAdmUtilisateurFonctGest.class, forVariable(variable), "GFC", "ADM_UTILISATEUR_FONCT_GEST");
        addMetadata();
    }

    public QAdmUtilisateurFonctGest(String variable, String schema, String table) {
        super(QAdmUtilisateurFonctGest.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QAdmUtilisateurFonctGest(Path<? extends QAdmUtilisateurFonctGest> path) {
        super(path.getType(), path.getMetadata(), "GFC", "ADM_UTILISATEUR_FONCT_GEST");
        addMetadata();
    }

    public QAdmUtilisateurFonctGest(PathMetadata<?> metadata) {
        super(QAdmUtilisateurFonctGest.class, metadata, "GFC", "ADM_UTILISATEUR_FONCT_GEST");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(ufgId, ColumnMetadata.named("UFG_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(ufOrdre, ColumnMetadata.named("UF_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

