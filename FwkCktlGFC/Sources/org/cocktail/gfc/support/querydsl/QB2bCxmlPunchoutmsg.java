package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QB2bCxmlPunchoutmsg is a Querydsl query type for QB2bCxmlPunchoutmsg
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QB2bCxmlPunchoutmsg extends com.mysema.query.sql.RelationalPathBase<QB2bCxmlPunchoutmsg> {

    private static final long serialVersionUID = -1057487684;

    public static final QB2bCxmlPunchoutmsg b2bCxmlPunchoutmsg = new QB2bCxmlPunchoutmsg("B2B_CXML_PUNCHOUTMSG");

    public final NumberPath<Long> bcpoId = createNumber("bcpoId", Long.class);

    public final NumberPath<Long> commId = createNumber("commId", Long.class);

    public final StringPath cxml = createString("cxml");

    public final DateTimePath<java.sql.Timestamp> dateCreation = createDateTime("dateCreation", java.sql.Timestamp.class);

    public final NumberPath<Long> fbcpId = createNumber("fbcpId", Long.class);

    public final NumberPath<Long> persIdCreation = createNumber("persIdCreation", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QB2bCxmlPunchoutmsg> b2bCxmlPunchoutmsgPk = createPrimaryKey(bcpoId);

    public QB2bCxmlPunchoutmsg(String variable) {
        super(QB2bCxmlPunchoutmsg.class, forVariable(variable), "GFC", "B2B_CXML_PUNCHOUTMSG");
        addMetadata();
    }

    public QB2bCxmlPunchoutmsg(String variable, String schema, String table) {
        super(QB2bCxmlPunchoutmsg.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QB2bCxmlPunchoutmsg(Path<? extends QB2bCxmlPunchoutmsg> path) {
        super(path.getType(), path.getMetadata(), "GFC", "B2B_CXML_PUNCHOUTMSG");
        addMetadata();
    }

    public QB2bCxmlPunchoutmsg(PathMetadata<?> metadata) {
        super(QB2bCxmlPunchoutmsg.class, metadata, "GFC", "B2B_CXML_PUNCHOUTMSG");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bcpoId, ColumnMetadata.named("BCPO_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(cxml, ColumnMetadata.named("CXML").withIndex(6).ofType(Types.CLOB).withSize(4000).notNull());
        addMetadata(dateCreation, ColumnMetadata.named("DATE_CREATION").withIndex(5).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(fbcpId, ColumnMetadata.named("FBCP_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(persIdCreation, ColumnMetadata.named("PERS_ID_CREATION").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

