package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QIndividuUlr is a Querydsl query type for QIndividuUlr
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QIndividuUlr extends com.mysema.query.sql.RelationalPathBase<QIndividuUlr> {

    private static final long serialVersionUID = 1306073164;

    public static final QIndividuUlr individuUlr = new QIndividuUlr("INDIVIDU_ULR");

    public final NumberPath<Integer> categoriePrinc = createNumber("categoriePrinc", Integer.class);

    public final StringPath cCivilite = createString("cCivilite");

    public final StringPath cDeptNaissance = createString("cDeptNaissance");

    public final StringPath cPaysNaissance = createString("cPaysNaissance");

    public final StringPath cPaysNationalite = createString("cPaysNationalite");

    public final DateTimePath<java.sql.Timestamp> dCreation = createDateTime("dCreation", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dDeces = createDateTime("dDeces", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dModification = createDateTime("dModification", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dNaissance = createDateTime("dNaissance", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dNaturalisation = createDateTime("dNaturalisation", java.sql.Timestamp.class);

    public final StringPath indActivite = createString("indActivite");

    public final StringPath indAgenda = createString("indAgenda");

    public final NumberPath<Integer> indCleInsee = createNumber("indCleInsee", Integer.class);

    public final NumberPath<Integer> indCleInseeProv = createNumber("indCleInseeProv", Integer.class);

    public final StringPath indCSitMilitaire = createString("indCSitMilitaire");

    public final StringPath indCSituationFamille = createString("indCSituationFamille");

    public final StringPath indNoInsee = createString("indNoInsee");

    public final StringPath indNoInseeProv = createString("indNoInseeProv");

    public final StringPath indOrigine = createString("indOrigine");

    public final StringPath indPhoto = createString("indPhoto");

    public final StringPath indQualite = createString("indQualite");

    public final StringPath languePref = createString("languePref");

    public final StringPath lieuDeces = createString("lieuDeces");

    public final StringPath listeRouge = createString("listeRouge");

    public final NumberPath<Integer> noIndividu = createNumber("noIndividu", Integer.class);

    public final NumberPath<Integer> noIndividuCreateur = createNumber("noIndividuCreateur", Integer.class);

    public final StringPath nomAffichage = createString("nomAffichage");

    public final StringPath nomPatronymique = createString("nomPatronymique");

    public final StringPath nomPatronymiqueAffichage = createString("nomPatronymiqueAffichage");

    public final StringPath nomUsuel = createString("nomUsuel");

    public final StringPath orgaCode = createString("orgaCode");

    public final StringPath orgaLibelle = createString("orgaLibelle");

    public final NumberPath<Long> persId = createNumber("persId", Long.class);

    public final NumberPath<Long> persIdCreation = createNumber("persIdCreation", Long.class);

    public final NumberPath<Long> persIdModification = createNumber("persIdModification", Long.class);

    public final StringPath prenom = createString("prenom");

    public final StringPath prenom2 = createString("prenom2");

    public final StringPath prenomAffichage = createString("prenomAffichage");

    public final StringPath priseCptInsee = createString("priseCptInsee");

    public final StringPath temSsDiplome = createString("temSsDiplome");

    public final StringPath temValide = createString("temValide");

    public final StringPath villeDeNaissance = createString("villeDeNaissance");

    public final com.mysema.query.sql.PrimaryKey<QIndividuUlr> individuUlrPk = createPrimaryKey(noIndividu);

    public QIndividuUlr(String variable) {
        super(QIndividuUlr.class, forVariable(variable), "GRHUM", "INDIVIDU_ULR");
        addMetadata();
    }

    public QIndividuUlr(String variable, String schema, String table) {
        super(QIndividuUlr.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QIndividuUlr(Path<? extends QIndividuUlr> path) {
        super(path.getType(), path.getMetadata(), "GRHUM", "INDIVIDU_ULR");
        addMetadata();
    }

    public QIndividuUlr(PathMetadata<?> metadata) {
        super(QIndividuUlr.class, metadata, "GRHUM", "INDIVIDU_ULR");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(categoriePrinc, ColumnMetadata.named("CATEGORIE_PRINC").withIndex(30).ofType(Types.DECIMAL).withSize(2));
        addMetadata(cCivilite, ColumnMetadata.named("C_CIVILITE").withIndex(5).ofType(Types.VARCHAR).withSize(5).notNull());
        addMetadata(cDeptNaissance, ColumnMetadata.named("C_DEPT_NAISSANCE").withIndex(10).ofType(Types.VARCHAR).withSize(3));
        addMetadata(cPaysNaissance, ColumnMetadata.named("C_PAYS_NAISSANCE").withIndex(11).ofType(Types.VARCHAR).withSize(3));
        addMetadata(cPaysNationalite, ColumnMetadata.named("C_PAYS_NATIONALITE").withIndex(12).ofType(Types.VARCHAR).withSize(3));
        addMetadata(dCreation, ColumnMetadata.named("D_CREATION").withIndex(23).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dDeces, ColumnMetadata.named("D_DECES").withIndex(14).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dModification, ColumnMetadata.named("D_MODIFICATION").withIndex(24).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(dNaissance, ColumnMetadata.named("D_NAISSANCE").withIndex(8).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dNaturalisation, ColumnMetadata.named("D_NATURALISATION").withIndex(13).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(indActivite, ColumnMetadata.named("IND_ACTIVITE").withIndex(21).ofType(Types.VARCHAR).withSize(100));
        addMetadata(indAgenda, ColumnMetadata.named("IND_AGENDA").withIndex(27).ofType(Types.VARCHAR).withSize(1));
        addMetadata(indCleInsee, ColumnMetadata.named("IND_CLE_INSEE").withIndex(18).ofType(Types.DECIMAL).withSize(2));
        addMetadata(indCleInseeProv, ColumnMetadata.named("IND_CLE_INSEE_PROV").withIndex(38).ofType(Types.DECIMAL).withSize(2));
        addMetadata(indCSitMilitaire, ColumnMetadata.named("IND_C_SIT_MILITAIRE").withIndex(15).ofType(Types.VARCHAR).withSize(1));
        addMetadata(indCSituationFamille, ColumnMetadata.named("IND_C_SITUATION_FAMILLE").withIndex(16).ofType(Types.VARCHAR).withSize(1));
        addMetadata(indNoInsee, ColumnMetadata.named("IND_NO_INSEE").withIndex(17).ofType(Types.VARCHAR).withSize(13));
        addMetadata(indNoInseeProv, ColumnMetadata.named("IND_NO_INSEE_PROV").withIndex(37).ofType(Types.VARCHAR).withSize(13));
        addMetadata(indOrigine, ColumnMetadata.named("IND_ORIGINE").withIndex(22).ofType(Types.VARCHAR).withSize(80));
        addMetadata(indPhoto, ColumnMetadata.named("IND_PHOTO").withIndex(20).ofType(Types.VARCHAR).withSize(1));
        addMetadata(indQualite, ColumnMetadata.named("IND_QUALITE").withIndex(19).ofType(Types.VARCHAR).withSize(120));
        addMetadata(languePref, ColumnMetadata.named("LANGUE_PREF").withIndex(28).ofType(Types.VARCHAR).withSize(3));
        addMetadata(lieuDeces, ColumnMetadata.named("LIEU_DECES").withIndex(42).ofType(Types.VARCHAR).withSize(100));
        addMetadata(listeRouge, ColumnMetadata.named("LISTE_ROUGE").withIndex(29).ofType(Types.CHAR).withSize(1).notNull());
        addMetadata(noIndividu, ColumnMetadata.named("NO_INDIVIDU").withIndex(1).ofType(Types.DECIMAL).withSize(8).notNull());
        addMetadata(noIndividuCreateur, ColumnMetadata.named("NO_INDIVIDU_CREATEUR").withIndex(31).ofType(Types.DECIMAL).withSize(8));
        addMetadata(nomAffichage, ColumnMetadata.named("NOM_AFFICHAGE").withIndex(34).ofType(Types.VARCHAR).withSize(80));
        addMetadata(nomPatronymique, ColumnMetadata.named("NOM_PATRONYMIQUE").withIndex(3).ofType(Types.VARCHAR).withSize(80));
        addMetadata(nomPatronymiqueAffichage, ColumnMetadata.named("NOM_PATRONYMIQUE_AFFICHAGE").withIndex(36).ofType(Types.VARCHAR).withSize(80));
        addMetadata(nomUsuel, ColumnMetadata.named("NOM_USUEL").withIndex(6).ofType(Types.VARCHAR).withSize(80).notNull());
        addMetadata(orgaCode, ColumnMetadata.named("ORGA_CODE").withIndex(32).ofType(Types.VARCHAR).withSize(10));
        addMetadata(orgaLibelle, ColumnMetadata.named("ORGA_LIBELLE").withIndex(33).ofType(Types.VARCHAR).withSize(120));
        addMetadata(persId, ColumnMetadata.named("PERS_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(persIdCreation, ColumnMetadata.named("PERS_ID_CREATION").withIndex(40).ofType(Types.DECIMAL).withSize(0));
        addMetadata(persIdModification, ColumnMetadata.named("PERS_ID_MODIFICATION").withIndex(41).ofType(Types.DECIMAL).withSize(0));
        addMetadata(prenom, ColumnMetadata.named("PRENOM").withIndex(4).ofType(Types.VARCHAR).withSize(40).notNull());
        addMetadata(prenom2, ColumnMetadata.named("PRENOM2").withIndex(7).ofType(Types.VARCHAR).withSize(40));
        addMetadata(prenomAffichage, ColumnMetadata.named("PRENOM_AFFICHAGE").withIndex(35).ofType(Types.VARCHAR).withSize(40));
        addMetadata(priseCptInsee, ColumnMetadata.named("PRISE_CPT_INSEE").withIndex(39).ofType(Types.CHAR).withSize(1));
        addMetadata(temSsDiplome, ColumnMetadata.named("TEM_SS_DIPLOME").withIndex(25).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(temValide, ColumnMetadata.named("TEM_VALIDE").withIndex(26).ofType(Types.VARCHAR).withSize(1).notNull());
        addMetadata(villeDeNaissance, ColumnMetadata.named("VILLE_DE_NAISSANCE").withIndex(9).ofType(Types.VARCHAR).withSize(60));
    }

}

