package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmInventSortie is a Querydsl query type for QImmInventSortie
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmInventSortie extends com.mysema.query.sql.RelationalPathBase<QImmInventSortie> {

    private static final long serialVersionUID = 1429833340;

    public static final QImmInventSortie immInventSortie = new QImmInventSortie("IMM_INVENT_SORTIE");

    public final NumberPath<Long> invId = createNumber("invId", Long.class);

    public final DateTimePath<java.sql.Timestamp> invsDate = createDateTime("invsDate", java.sql.Timestamp.class);

    public final NumberPath<Long> invsId = createNumber("invsId", Long.class);

    public final StringPath invsMotif = createString("invsMotif");

    public final NumberPath<java.math.BigDecimal> invsValeurCession = createNumber("invsValeurCession", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> invsVnc = createNumber("invsVnc", java.math.BigDecimal.class);

    public final NumberPath<Long> tysoId = createNumber("tysoId", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QImmInventSortie> immInventSortiePk = createPrimaryKey(invsId);

    public final com.mysema.query.sql.ForeignKey<QImmInvent> sortieInvIdFk = createForeignKey(invId, "INV_ID");

    public final com.mysema.query.sql.ForeignKey<QImmTypeSortie> sortieTysoIdFk = createForeignKey(tysoId, "TYSO_ID");

    public QImmInventSortie(String variable) {
        super(QImmInventSortie.class, forVariable(variable), "GFC", "IMM_INVENT_SORTIE");
        addMetadata();
    }

    public QImmInventSortie(String variable, String schema, String table) {
        super(QImmInventSortie.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmInventSortie(Path<? extends QImmInventSortie> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_INVENT_SORTIE");
        addMetadata();
    }

    public QImmInventSortie(PathMetadata<?> metadata) {
        super(QImmInventSortie.class, metadata, "GFC", "IMM_INVENT_SORTIE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(invId, ColumnMetadata.named("INV_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(invsDate, ColumnMetadata.named("INVS_DATE").withIndex(4).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(invsId, ColumnMetadata.named("INVS_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(invsMotif, ColumnMetadata.named("INVS_MOTIF").withIndex(3).ofType(Types.VARCHAR).withSize(200).notNull());
        addMetadata(invsValeurCession, ColumnMetadata.named("INVS_VALEUR_CESSION").withIndex(6).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(invsVnc, ColumnMetadata.named("INVS_VNC").withIndex(5).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(tysoId, ColumnMetadata.named("TYSO_ID").withIndex(7).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

