package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVSuiviPrevuDepenseDetail is a Querydsl query type for QVSuiviPrevuDepenseDetail
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVSuiviPrevuDepenseDetail extends com.mysema.query.sql.RelationalPathBase<QVSuiviPrevuDepenseDetail> {

    private static final long serialVersionUID = 272984638;

    public static final QVSuiviPrevuDepenseDetail vSuiviPrevuDepenseDetail = new QVSuiviPrevuDepenseDetail("V_SUIVI_PREVU_DEPENSE_DETAIL");

    public final SimplePath<Object> commId = createSimple("commId", Object.class);

    public final SimplePath<Object> commNumero = createSimple("commNumero", Object.class);

    public final SimplePath<Object> dconHtSaisie = createSimple("dconHtSaisie", Object.class);

    public final SimplePath<Object> dconTtcSaisie = createSimple("dconTtcSaisie", Object.class);

    public final SimplePath<Object> depId = createSimple("depId", Object.class);

    public final SimplePath<Object> dppDateSaisie = createSimple("dppDateSaisie", Object.class);

    public final SimplePath<Object> dppId = createSimple("dppId", Object.class);

    public final SimplePath<Object> dppNumeroFacture = createSimple("dppNumeroFacture", Object.class);

    public final SimplePath<Object> econDateSaisie = createSimple("econDateSaisie", Object.class);

    public final SimplePath<Object> econMontantBudgetaire = createSimple("econMontantBudgetaire", Object.class);

    public final SimplePath<Object> econMontantBudgetaireReste = createSimple("econMontantBudgetaireReste", Object.class);

    public final SimplePath<Object> engId = createSimple("engId", Object.class);

    public final SimplePath<Object> engNumero = createSimple("engNumero", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idOpeOperation = createSimple("idOpeOperation", Object.class);

    public final SimplePath<Object> orgIdPosit = createSimple("orgIdPosit", Object.class);

    public final SimplePath<Object> orgIdPropo = createSimple("orgIdPropo", Object.class);

    public final SimplePath<Object> tapId = createSimple("tapId", Object.class);

    public final SimplePath<Object> tcdOrdrePosit = createSimple("tcdOrdrePosit", Object.class);

    public final SimplePath<Object> totalPosit = createSimple("totalPosit", Object.class);

    public final SimplePath<Object> totalPropo = createSimple("totalPropo", Object.class);

    public QVSuiviPrevuDepenseDetail(String variable) {
        super(QVSuiviPrevuDepenseDetail.class, forVariable(variable), "GFC", "V_SUIVI_PREVU_DEPENSE_DETAIL");
        addMetadata();
    }

    public QVSuiviPrevuDepenseDetail(String variable, String schema, String table) {
        super(QVSuiviPrevuDepenseDetail.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVSuiviPrevuDepenseDetail(Path<? extends QVSuiviPrevuDepenseDetail> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_SUIVI_PREVU_DEPENSE_DETAIL");
        addMetadata();
    }

    public QVSuiviPrevuDepenseDetail(PathMetadata<?> metadata) {
        super(QVSuiviPrevuDepenseDetail.class, metadata, "GFC", "V_SUIVI_PREVU_DEPENSE_DETAIL");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(commId, ColumnMetadata.named("COMM_ID").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(commNumero, ColumnMetadata.named("COMM_NUMERO").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(dconHtSaisie, ColumnMetadata.named("DCON_HT_SAISIE").withIndex(18).ofType(Types.OTHER).withSize(0));
        addMetadata(dconTtcSaisie, ColumnMetadata.named("DCON_TTC_SAISIE").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(dppDateSaisie, ColumnMetadata.named("DPP_DATE_SAISIE").withIndex(19).ofType(Types.OTHER).withSize(0));
        addMetadata(dppId, ColumnMetadata.named("DPP_ID").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(dppNumeroFacture, ColumnMetadata.named("DPP_NUMERO_FACTURE").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(econDateSaisie, ColumnMetadata.named("ECON_DATE_SAISIE").withIndex(15).ofType(Types.OTHER).withSize(0));
        addMetadata(econMontantBudgetaire, ColumnMetadata.named("ECON_MONTANT_BUDGETAIRE").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(econMontantBudgetaireReste, ColumnMetadata.named("ECON_MONTANT_BUDGETAIRE_RESTE").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(engId, ColumnMetadata.named("ENG_ID").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(engNumero, ColumnMetadata.named("ENG_NUMERO").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idOpeOperation, ColumnMetadata.named("ID_OPE_OPERATION").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdPosit, ColumnMetadata.named("ORG_ID_POSIT").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(orgIdPropo, ColumnMetadata.named("ORG_ID_PROPO").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(tapId, ColumnMetadata.named("TAP_ID").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdrePosit, ColumnMetadata.named("TCD_ORDRE_POSIT").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPosit, ColumnMetadata.named("TOTAL_POSIT").withIndex(21).ofType(Types.OTHER).withSize(0));
        addMetadata(totalPropo, ColumnMetadata.named("TOTAL_PROPO").withIndex(20).ofType(Types.OTHER).withSize(0));
    }

}

