package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVAdmEb is a Querydsl query type for QVAdmEb
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVAdmEb extends com.mysema.query.sql.RelationalPathBase<QVAdmEb> {

    private static final long serialVersionUID = -1455762350;

    public static final QVAdmEb vAdmEb = new QVAdmEb("V_ADM_EB");

    public final SimplePath<Object> cStructure = createSimple("cStructure", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> logOrdre = createSimple("logOrdre", Object.class);

    public final SimplePath<Object> onbId = createSimple("onbId", Object.class);

    public final SimplePath<Object> orgCr = createSimple("orgCr", Object.class);

    public final SimplePath<Object> orgDateCloture = createSimple("orgDateCloture", Object.class);

    public final SimplePath<Object> orgDateOuverture = createSimple("orgDateOuverture", Object.class);

    public final SimplePath<Object> orgEtab = createSimple("orgEtab", Object.class);

    public final SimplePath<Object> orgLib = createSimple("orgLib", Object.class);

    public final SimplePath<Object> orgLucrativite = createSimple("orgLucrativite", Object.class);

    public final SimplePath<Object> orgNiv = createSimple("orgNiv", Object.class);

    public final SimplePath<Object> orgPere = createSimple("orgPere", Object.class);

    public final SimplePath<Object> orgSouscr = createSimple("orgSouscr", Object.class);

    public final SimplePath<Object> orgUb = createSimple("orgUb", Object.class);

    public final SimplePath<Object> orgUniv = createSimple("orgUniv", Object.class);

    public final SimplePath<Object> tyorId = createSimple("tyorId", Object.class);

    public QVAdmEb(String variable) {
        super(QVAdmEb.class, forVariable(variable), "GFC", "V_ADM_EB");
        addMetadata();
    }

    public QVAdmEb(String variable, String schema, String table) {
        super(QVAdmEb.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVAdmEb(Path<? extends QVAdmEb> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_ADM_EB");
        addMetadata();
    }

    public QVAdmEb(PathMetadata<?> metadata) {
        super(QVAdmEb.class, metadata, "GFC", "V_ADM_EB");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(cStructure, ColumnMetadata.named("C_STRUCTURE").withIndex(13).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(logOrdre, ColumnMetadata.named("LOG_ORDRE").withIndex(14).ofType(Types.OTHER).withSize(0));
        addMetadata(onbId, ColumnMetadata.named("ONB_ID").withIndex(17).ofType(Types.OTHER).withSize(0));
        addMetadata(orgCr, ColumnMetadata.named("ORG_CR").withIndex(8).ofType(Types.OTHER).withSize(0));
        addMetadata(orgDateCloture, ColumnMetadata.named("ORG_DATE_CLOTURE").withIndex(12).ofType(Types.OTHER).withSize(0));
        addMetadata(orgDateOuverture, ColumnMetadata.named("ORG_DATE_OUVERTURE").withIndex(11).ofType(Types.OTHER).withSize(0));
        addMetadata(orgEtab, ColumnMetadata.named("ORG_ETAB").withIndex(6).ofType(Types.OTHER).withSize(0));
        addMetadata(orgLib, ColumnMetadata.named("ORG_LIB").withIndex(9).ofType(Types.OTHER).withSize(0));
        addMetadata(orgLucrativite, ColumnMetadata.named("ORG_LUCRATIVITE").withIndex(10).ofType(Types.OTHER).withSize(0));
        addMetadata(orgNiv, ColumnMetadata.named("ORG_NIV").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(orgPere, ColumnMetadata.named("ORG_PERE").withIndex(4).ofType(Types.OTHER).withSize(0));
        addMetadata(orgSouscr, ColumnMetadata.named("ORG_SOUSCR").withIndex(16).ofType(Types.OTHER).withSize(0));
        addMetadata(orgUb, ColumnMetadata.named("ORG_UB").withIndex(7).ofType(Types.OTHER).withSize(0));
        addMetadata(orgUniv, ColumnMetadata.named("ORG_UNIV").withIndex(5).ofType(Types.OTHER).withSize(0));
        addMetadata(tyorId, ColumnMetadata.named("TYOR_ID").withIndex(15).ofType(Types.OTHER).withSize(0));
    }

}

