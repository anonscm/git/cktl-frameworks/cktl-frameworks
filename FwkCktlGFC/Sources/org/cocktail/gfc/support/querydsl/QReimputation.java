package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QReimputation is a Querydsl query type for QReimputation
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QReimputation extends com.mysema.query.sql.RelationalPathBase<QReimputation> {

    private static final long serialVersionUID = -1295719330;

    public static final QReimputation reimputation = new QReimputation("REIMPUTATION");

    public final NumberPath<Long> depId = createNumber("depId", Long.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final DateTimePath<java.sql.Timestamp> reimDate = createDateTime("reimDate", java.sql.Timestamp.class);

    public final NumberPath<Long> reimId = createNumber("reimId", Long.class);

    public final StringPath reimLibelle = createString("reimLibelle");

    public final NumberPath<Long> reimNumero = createNumber("reimNumero", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public final com.mysema.query.sql.PrimaryKey<QReimputation> reimputationPk = createPrimaryKey(reimId);

    public final com.mysema.query.sql.ForeignKey<QAdmUtilisateur> reimputationUtlOrdreFk = createForeignKey(utlOrdre, "UTL_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QAdmExercice> reimputationExeOrdreFk = createForeignKey(exeOrdre, "EXE_ORDRE");

    public final com.mysema.query.sql.ForeignKey<QDepenseBudget> reimputationDepIdFk = createForeignKey(depId, "DEP_ID");

    public final com.mysema.query.sql.ForeignKey<QReimputationConvention> _reimpConventionReimIdFk = createInvForeignKey(reimId, "REIM_ID");

    public final com.mysema.query.sql.ForeignKey<QReimputationMarche> _reimpMarcheReimIdFk = createInvForeignKey(reimId, "REIM_ID");

    public final com.mysema.query.sql.ForeignKey<QReimputationBudget> _reimpBudgetReimIdFk = createInvForeignKey(reimId, "REIM_ID");

    public final com.mysema.query.sql.ForeignKey<QReimputationAnalytique> _reimpAnalytiqueReimIdFk = createInvForeignKey(reimId, "REIM_ID");

    public final com.mysema.query.sql.ForeignKey<QReimputationAction> _reimpActionReimIdFk = createInvForeignKey(reimId, "REIM_ID");

    public final com.mysema.query.sql.ForeignKey<QReimputationPlanco> _reimpPlancoReimIdFk = createInvForeignKey(reimId, "REIM_ID");

    public final com.mysema.query.sql.ForeignKey<QReimputationHorsMarche> _reimpHmReimIdFk = createInvForeignKey(reimId, "REIM_ID");

    public QReimputation(String variable) {
        super(QReimputation.class, forVariable(variable), "GFC", "REIMPUTATION");
        addMetadata();
    }

    public QReimputation(String variable, String schema, String table) {
        super(QReimputation.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QReimputation(Path<? extends QReimputation> path) {
        super(path.getType(), path.getMetadata(), "GFC", "REIMPUTATION");
        addMetadata();
    }

    public QReimputation(PathMetadata<?> metadata) {
        super(QReimputation.class, metadata, "GFC", "REIMPUTATION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(depId, ColumnMetadata.named("DEP_ID").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(4));
        addMetadata(reimDate, ColumnMetadata.named("REIM_DATE").withIndex(7).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(reimId, ColumnMetadata.named("REIM_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(reimLibelle, ColumnMetadata.named("REIM_LIBELLE").withIndex(5).ofType(Types.VARCHAR).withSize(500));
        addMetadata(reimNumero, ColumnMetadata.named("REIM_NUMERO").withIndex(3).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(6).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

