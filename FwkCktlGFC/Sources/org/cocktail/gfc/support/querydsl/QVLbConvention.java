package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVLbConvention is a Querydsl query type for QVLbConvention
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVLbConvention extends com.mysema.query.sql.RelationalPathBase<QVLbConvention> {

    private static final long serialVersionUID = 2116072892;

    public static final QVLbConvention vLbConvention = new QVLbConvention("V_LB_CONVENTION");

    public final SimplePath<Object> convOrdre = createSimple("convOrdre", Object.class);

    public final SimplePath<Object> exeOrdre = createSimple("exeOrdre", Object.class);

    public final SimplePath<Object> idAdmEb = createSimple("idAdmEb", Object.class);

    public final SimplePath<Object> tcdOrdre = createSimple("tcdOrdre", Object.class);

    public QVLbConvention(String variable) {
        super(QVLbConvention.class, forVariable(variable), "GFC", "V_LB_CONVENTION");
        addMetadata();
    }

    public QVLbConvention(String variable, String schema, String table) {
        super(QVLbConvention.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVLbConvention(Path<? extends QVLbConvention> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_LB_CONVENTION");
        addMetadata();
    }

    public QVLbConvention(PathMetadata<?> metadata) {
        super(QVLbConvention.class, metadata, "GFC", "V_LB_CONVENTION");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(convOrdre, ColumnMetadata.named("CONV_ORDRE").withIndex(1).ofType(Types.OTHER).withSize(0));
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(2).ofType(Types.OTHER).withSize(0));
        addMetadata(idAdmEb, ColumnMetadata.named("ID_ADM_EB").withIndex(3).ofType(Types.OTHER).withSize(0));
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(4).ofType(Types.OTHER).withSize(0));
    }

}

