package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVPlancoCreditRec is a Querydsl query type for QVPlancoCreditRec
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVPlancoCreditRec extends com.mysema.query.sql.RelationalPathBase<QVPlancoCreditRec> {

    private static final long serialVersionUID = 2122264749;

    public static final QVPlancoCreditRec vPlancoCreditRec = new QVPlancoCreditRec("V_PLANCO_CREDIT_REC");

    public final StringPath pccEtat = createString("pccEtat");

    public final NumberPath<Long> pccOrdre = createNumber("pccOrdre", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final NumberPath<Long> tcdOrdre = createNumber("tcdOrdre", Long.class);

    public QVPlancoCreditRec(String variable) {
        super(QVPlancoCreditRec.class, forVariable(variable), "GFC", "V_PLANCO_CREDIT_REC");
        addMetadata();
    }

    public QVPlancoCreditRec(String variable, String schema, String table) {
        super(QVPlancoCreditRec.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVPlancoCreditRec(Path<? extends QVPlancoCreditRec> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_PLANCO_CREDIT_REC");
        addMetadata();
    }

    public QVPlancoCreditRec(PathMetadata<?> metadata) {
        super(QVPlancoCreditRec.class, metadata, "GFC", "V_PLANCO_CREDIT_REC");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(pccEtat, ColumnMetadata.named("PCC_ETAT").withIndex(4).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(pccOrdre, ColumnMetadata.named("PCC_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(3).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(tcdOrdre, ColumnMetadata.named("TCD_ORDRE").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

