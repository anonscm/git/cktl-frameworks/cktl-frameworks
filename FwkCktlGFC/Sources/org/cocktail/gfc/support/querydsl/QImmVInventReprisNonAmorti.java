package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QImmVInventReprisNonAmorti is a Querydsl query type for QImmVInventReprisNonAmorti
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QImmVInventReprisNonAmorti extends com.mysema.query.sql.RelationalPathBase<QImmVInventReprisNonAmorti> {

    private static final long serialVersionUID = -937373174;

    public static final QImmVInventReprisNonAmorti immVInventReprisNonAmorti = new QImmVInventReprisNonAmorti("IMM_V_INVENT_REPRIS_NON_AMORTI");

    public final NumberPath<java.math.BigDecimal> inbMontant = createNumber("inbMontant", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> inbMontantResiduel = createNumber("inbMontantResiduel", java.math.BigDecimal.class);

    public final NumberPath<Long> invcId = createNumber("invcId", Long.class);

    public QImmVInventReprisNonAmorti(String variable) {
        super(QImmVInventReprisNonAmorti.class, forVariable(variable), "GFC", "IMM_V_INVENT_REPRIS_NON_AMORTI");
        addMetadata();
    }

    public QImmVInventReprisNonAmorti(String variable, String schema, String table) {
        super(QImmVInventReprisNonAmorti.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QImmVInventReprisNonAmorti(Path<? extends QImmVInventReprisNonAmorti> path) {
        super(path.getType(), path.getMetadata(), "GFC", "IMM_V_INVENT_REPRIS_NON_AMORTI");
        addMetadata();
    }

    public QImmVInventReprisNonAmorti(PathMetadata<?> metadata) {
        super(QImmVInventReprisNonAmorti.class, metadata, "GFC", "IMM_V_INVENT_REPRIS_NON_AMORTI");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(inbMontant, ColumnMetadata.named("INB_MONTANT").withIndex(2).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(inbMontantResiduel, ColumnMetadata.named("INB_MONTANT_RESIDUEL").withIndex(3).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(invcId, ColumnMetadata.named("INVC_ID").withIndex(1).ofType(Types.DECIMAL).withSize(0));
    }

}

