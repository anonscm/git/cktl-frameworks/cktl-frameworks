package org.cocktail.gfc.support.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;

import com.mysema.query.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVTitreRejete is a Querydsl query type for QVTitreRejete
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QVTitreRejete extends com.mysema.query.sql.RelationalPathBase<QVTitreRejete> {

    private static final long serialVersionUID = 1137199228;

    public static final QVTitreRejete vTitreRejete = new QVTitreRejete("V_TITRE_REJETE");

    public final NumberPath<Long> borNumOrigine = createNumber("borNumOrigine", Long.class);

    public final NumberPath<Long> brjNum = createNumber("brjNum", Long.class);

    public final DateTimePath<java.sql.Timestamp> dateRejet = createDateTime("dateRejet", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> dateTitre = createDateTime("dateTitre", java.sql.Timestamp.class);

    public final NumberPath<Integer> exeOrdre = createNumber("exeOrdre", Integer.class);

    public final StringPath fouCode = createString("fouCode");

    public final StringPath fournisseur = createString("fournisseur");

    public final StringPath gesCode = createString("gesCode");

    public final NumberPath<Long> manId = createNumber("manId", Long.class);

    public final StringPath pcoNum = createString("pcoNum");

    public final StringPath tboLibelleOrigine = createString("tboLibelleOrigine");

    public final StringPath tboLibelleRejet = createString("tboLibelleRejet");

    public final NumberPath<java.math.BigDecimal> titHt = createNumber("titHt", java.math.BigDecimal.class);

    public final StringPath titMotifRejet = createString("titMotifRejet");

    public final NumberPath<Long> titNumero = createNumber("titNumero", Long.class);

    public final NumberPath<Long> utlOrdre = createNumber("utlOrdre", Long.class);

    public QVTitreRejete(String variable) {
        super(QVTitreRejete.class, forVariable(variable), "GFC", "V_TITRE_REJETE");
        addMetadata();
    }

    public QVTitreRejete(String variable, String schema, String table) {
        super(QVTitreRejete.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVTitreRejete(Path<? extends QVTitreRejete> path) {
        super(path.getType(), path.getMetadata(), "GFC", "V_TITRE_REJETE");
        addMetadata();
    }

    public QVTitreRejete(PathMetadata<?> metadata) {
        super(QVTitreRejete.class, metadata, "GFC", "V_TITRE_REJETE");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(borNumOrigine, ColumnMetadata.named("BOR_NUM_ORIGINE").withIndex(6).ofType(Types.DECIMAL).withSize(0));
        addMetadata(brjNum, ColumnMetadata.named("BRJ_NUM").withIndex(11).ofType(Types.DECIMAL).withSize(0));
        addMetadata(dateRejet, ColumnMetadata.named("DATE_REJET").withIndex(13).ofType(Types.TIMESTAMP).withSize(7));
        addMetadata(dateTitre, ColumnMetadata.named("DATE_TITRE").withIndex(5).ofType(Types.TIMESTAMP).withSize(7).notNull());
        addMetadata(exeOrdre, ColumnMetadata.named("EXE_ORDRE").withIndex(1).ofType(Types.DECIMAL).withSize(4));
        addMetadata(fouCode, ColumnMetadata.named("FOU_CODE").withIndex(9).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(fournisseur, ColumnMetadata.named("FOURNISSEUR").withIndex(10).ofType(Types.VARCHAR).withSize(161));
        addMetadata(gesCode, ColumnMetadata.named("GES_CODE").withIndex(3).ofType(Types.VARCHAR).withSize(10).notNull());
        addMetadata(manId, ColumnMetadata.named("MAN_ID").withIndex(2).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(pcoNum, ColumnMetadata.named("PCO_NUM").withIndex(8).ofType(Types.VARCHAR).withSize(20).notNull());
        addMetadata(tboLibelleOrigine, ColumnMetadata.named("TBO_LIBELLE_ORIGINE").withIndex(7).ofType(Types.VARCHAR).withSize(60).notNull());
        addMetadata(tboLibelleRejet, ColumnMetadata.named("TBO_LIBELLE_REJET").withIndex(12).ofType(Types.VARCHAR).withSize(60).notNull());
        addMetadata(titHt, ColumnMetadata.named("TIT_HT").withIndex(15).ofType(Types.DECIMAL).withSize(12).withDigits(2).notNull());
        addMetadata(titMotifRejet, ColumnMetadata.named("TIT_MOTIF_REJET").withIndex(14).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(titNumero, ColumnMetadata.named("TIT_NUMERO").withIndex(4).ofType(Types.DECIMAL).withSize(0).notNull());
        addMetadata(utlOrdre, ColumnMetadata.named("UTL_ORDRE").withIndex(16).ofType(Types.DECIMAL).withSize(0).notNull());
    }

}

